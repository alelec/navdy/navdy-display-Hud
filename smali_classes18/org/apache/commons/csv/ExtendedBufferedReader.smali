.class final Lorg/apache/commons/csv/ExtendedBufferedReader;
.super Ljava/io/BufferedReader;
.source "ExtendedBufferedReader.java"


# instance fields
.field private closed:Z

.field private eolCounter:J

.field private lastChar:I


# direct methods
.method constructor <init>(Ljava/io/Reader;)V
    .locals 2
    .param p1, "reader"    # Ljava/io/Reader;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 40
    const/4 v0, -0x2

    iput v0, p0, Lorg/apache/commons/csv/ExtendedBufferedReader;->lastChar:I

    .line 43
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/commons/csv/ExtendedBufferedReader;->eolCounter:J

    .line 52
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 173
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/commons/csv/ExtendedBufferedReader;->closed:Z

    .line 174
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/commons/csv/ExtendedBufferedReader;->lastChar:I

    .line 175
    invoke-super {p0}, Ljava/io/BufferedReader;->close()V

    .line 176
    return-void
.end method

.method getCurrentLineNumber()J
    .locals 4

    .prologue
    .line 154
    iget v0, p0, Lorg/apache/commons/csv/ExtendedBufferedReader;->lastChar:I

    const/16 v1, 0xd

    if-eq v0, v1, :cond_0

    iget v0, p0, Lorg/apache/commons/csv/ExtendedBufferedReader;->lastChar:I

    const/16 v1, 0xa

    if-eq v0, v1, :cond_0

    iget v0, p0, Lorg/apache/commons/csv/ExtendedBufferedReader;->lastChar:I

    const/4 v1, -0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lorg/apache/commons/csv/ExtendedBufferedReader;->lastChar:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 155
    :cond_0
    iget-wide v0, p0, Lorg/apache/commons/csv/ExtendedBufferedReader;->eolCounter:J

    .line 157
    :goto_0
    return-wide v0

    :cond_1
    iget-wide v0, p0, Lorg/apache/commons/csv/ExtendedBufferedReader;->eolCounter:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    goto :goto_0
.end method

.method getLastChar()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lorg/apache/commons/csv/ExtendedBufferedReader;->lastChar:I

    return v0
.end method

.method public isClosed()Z
    .locals 1

    .prologue
    .line 161
    iget-boolean v0, p0, Lorg/apache/commons/csv/ExtendedBufferedReader;->closed:Z

    return v0
.end method

.method lookAhead()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 140
    const/4 v1, 0x1

    invoke-super {p0, v1}, Ljava/io/BufferedReader;->mark(I)V

    .line 141
    invoke-super {p0}, Ljava/io/BufferedReader;->read()I

    move-result v0

    .line 142
    .local v0, "c":I
    invoke-super {p0}, Ljava/io/BufferedReader;->reset()V

    .line 144
    return v0
.end method

.method public read()I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v2, 0xd

    .line 56
    invoke-super {p0}, Ljava/io/BufferedReader;->read()I

    move-result v0

    .line 57
    .local v0, "current":I
    if-eq v0, v2, :cond_0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_1

    iget v1, p0, Lorg/apache/commons/csv/ExtendedBufferedReader;->lastChar:I

    if-eq v1, v2, :cond_1

    .line 58
    :cond_0
    iget-wide v2, p0, Lorg/apache/commons/csv/ExtendedBufferedReader;->eolCounter:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lorg/apache/commons/csv/ExtendedBufferedReader;->eolCounter:J

    .line 60
    :cond_1
    iput v0, p0, Lorg/apache/commons/csv/ExtendedBufferedReader;->lastChar:I

    .line 61
    iget v1, p0, Lorg/apache/commons/csv/ExtendedBufferedReader;->lastChar:I

    return v1
.end method

.method public read([CII)I
    .locals 10
    .param p1, "buf"    # [C
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v8, 0x1

    const/16 v6, 0xd

    const/4 v3, -0x1

    .line 78
    if-nez p3, :cond_1

    .line 79
    const/4 v2, 0x0

    .line 103
    :cond_0
    :goto_0
    return v2

    .line 82
    :cond_1
    invoke-super {p0, p1, p2, p3}, Ljava/io/BufferedReader;->read([CII)I

    move-result v2

    .line 84
    .local v2, "len":I
    if-lez v2, :cond_6

    .line 86
    move v1, p2

    .local v1, "i":I
    :goto_1
    add-int v3, p2, v2

    if-ge v1, v3, :cond_5

    .line 87
    aget-char v0, p1, v1

    .line 88
    .local v0, "ch":C
    const/16 v3, 0xa

    if-ne v0, v3, :cond_4

    .line 89
    if-lez v1, :cond_3

    add-int/lit8 v3, v1, -0x1

    aget-char v3, p1, v3

    :goto_2
    if-eq v6, v3, :cond_2

    .line 90
    iget-wide v4, p0, Lorg/apache/commons/csv/ExtendedBufferedReader;->eolCounter:J

    add-long/2addr v4, v8

    iput-wide v4, p0, Lorg/apache/commons/csv/ExtendedBufferedReader;->eolCounter:J

    .line 86
    :cond_2
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 89
    :cond_3
    iget v3, p0, Lorg/apache/commons/csv/ExtendedBufferedReader;->lastChar:I

    goto :goto_2

    .line 92
    :cond_4
    if-ne v0, v6, :cond_2

    .line 93
    iget-wide v4, p0, Lorg/apache/commons/csv/ExtendedBufferedReader;->eolCounter:J

    add-long/2addr v4, v8

    iput-wide v4, p0, Lorg/apache/commons/csv/ExtendedBufferedReader;->eolCounter:J

    goto :goto_3

    .line 97
    .end local v0    # "ch":C
    :cond_5
    add-int v3, p2, v2

    add-int/lit8 v3, v3, -0x1

    aget-char v3, p1, v3

    iput v3, p0, Lorg/apache/commons/csv/ExtendedBufferedReader;->lastChar:I

    goto :goto_0

    .line 99
    .end local v1    # "i":I
    :cond_6
    if-ne v2, v3, :cond_0

    .line 100
    iput v3, p0, Lorg/apache/commons/csv/ExtendedBufferedReader;->lastChar:I

    goto :goto_0
.end method

.method public readLine()Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 118
    invoke-super {p0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    .line 120
    .local v0, "line":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 121
    const/16 v1, 0xa

    iput v1, p0, Lorg/apache/commons/csv/ExtendedBufferedReader;->lastChar:I

    .line 122
    iget-wide v2, p0, Lorg/apache/commons/csv/ExtendedBufferedReader;->eolCounter:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lorg/apache/commons/csv/ExtendedBufferedReader;->eolCounter:J

    .line 127
    :goto_0
    return-object v0

    .line 124
    :cond_0
    const/4 v1, -0x1

    iput v1, p0, Lorg/apache/commons/csv/ExtendedBufferedReader;->lastChar:I

    goto :goto_0
.end method
