.class public final Lorg/apache/commons/csv/CSVParser;
.super Ljava/lang/Object;
.source "CSVParser.java"

# interfaces
.implements Ljava/lang/Iterable;
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/commons/csv/CSVParser$2;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "Lorg/apache/commons/csv/CSVRecord;",
        ">;",
        "Ljava/io/Closeable;"
    }
.end annotation


# instance fields
.field private final format:Lorg/apache/commons/csv/CSVFormat;

.field private final headerMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final lexer:Lorg/apache/commons/csv/Lexer;

.field private final record:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private recordNumber:J

.field private final reusableToken:Lorg/apache/commons/csv/Token;


# direct methods
.method public constructor <init>(Ljava/io/Reader;Lorg/apache/commons/csv/CSVFormat;)V
    .locals 2
    .param p1, "reader"    # Ljava/io/Reader;
    .param p2, "format"    # Lorg/apache/commons/csv/CSVFormat;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 242
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 219
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/commons/csv/CSVParser;->record:Ljava/util/List;

    .line 223
    new-instance v0, Lorg/apache/commons/csv/Token;

    invoke-direct {v0}, Lorg/apache/commons/csv/Token;-><init>()V

    iput-object v0, p0, Lorg/apache/commons/csv/CSVParser;->reusableToken:Lorg/apache/commons/csv/Token;

    .line 243
    const-string v0, "reader"

    invoke-static {p1, v0}, Lorg/apache/commons/csv/Assertions;->notNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 244
    const-string v0, "format"

    invoke-static {p2, v0}, Lorg/apache/commons/csv/Assertions;->notNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 246
    iput-object p2, p0, Lorg/apache/commons/csv/CSVParser;->format:Lorg/apache/commons/csv/CSVFormat;

    .line 247
    new-instance v0, Lorg/apache/commons/csv/Lexer;

    new-instance v1, Lorg/apache/commons/csv/ExtendedBufferedReader;

    invoke-direct {v1, p1}, Lorg/apache/commons/csv/ExtendedBufferedReader;-><init>(Ljava/io/Reader;)V

    invoke-direct {v0, p2, v1}, Lorg/apache/commons/csv/Lexer;-><init>(Lorg/apache/commons/csv/CSVFormat;Lorg/apache/commons/csv/ExtendedBufferedReader;)V

    iput-object v0, p0, Lorg/apache/commons/csv/CSVParser;->lexer:Lorg/apache/commons/csv/Lexer;

    .line 248
    invoke-direct {p0}, Lorg/apache/commons/csv/CSVParser;->initializeHeader()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/commons/csv/CSVParser;->headerMap:Ljava/util/Map;

    .line 249
    return-void
.end method

.method private addRecordValue()V
    .locals 4

    .prologue
    .line 252
    iget-object v2, p0, Lorg/apache/commons/csv/CSVParser;->reusableToken:Lorg/apache/commons/csv/Token;

    iget-object v2, v2, Lorg/apache/commons/csv/Token;->content:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 253
    .local v0, "input":Ljava/lang/String;
    iget-object v2, p0, Lorg/apache/commons/csv/CSVParser;->format:Lorg/apache/commons/csv/CSVFormat;

    invoke-virtual {v2}, Lorg/apache/commons/csv/CSVFormat;->getNullString()Ljava/lang/String;

    move-result-object v1

    .line 254
    .local v1, "nullString":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 255
    iget-object v2, p0, Lorg/apache/commons/csv/CSVParser;->record:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 259
    .end local v0    # "input":Ljava/lang/String;
    :goto_0
    return-void

    .line 257
    .restart local v0    # "input":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lorg/apache/commons/csv/CSVParser;->record:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v0, 0x0

    .end local v0    # "input":Ljava/lang/String;
    :cond_1
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private initializeHeader()Ljava/util/Map;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 340
    const/4 v3, 0x0

    .line 341
    .local v3, "hdrMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v8, p0, Lorg/apache/commons/csv/CSVParser;->format:Lorg/apache/commons/csv/CSVFormat;

    invoke-virtual {v8}, Lorg/apache/commons/csv/CSVFormat;->getHeader()[Ljava/lang/String;

    move-result-object v2

    .line 342
    .local v2, "formatHeader":[Ljava/lang/String;
    if-eqz v2, :cond_7

    .line 343
    new-instance v3, Ljava/util/LinkedHashMap;

    .end local v3    # "hdrMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-direct {v3}, Ljava/util/LinkedHashMap;-><init>()V

    .line 345
    .restart local v3    # "hdrMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    const/4 v5, 0x0

    .line 346
    .local v5, "headerRecord":[Ljava/lang/String;
    array-length v8, v2

    if-nez v8, :cond_3

    .line 348
    invoke-virtual {p0}, Lorg/apache/commons/csv/CSVParser;->nextRecord()Lorg/apache/commons/csv/CSVRecord;

    move-result-object v7

    .line 349
    .local v7, "nextRecord":Lorg/apache/commons/csv/CSVRecord;
    if-eqz v7, :cond_0

    .line 350
    invoke-virtual {v7}, Lorg/apache/commons/csv/CSVRecord;->values()[Ljava/lang/String;

    move-result-object v5

    .line 360
    .end local v7    # "nextRecord":Lorg/apache/commons/csv/CSVRecord;
    :cond_0
    :goto_0
    if-eqz v5, :cond_7

    .line 361
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    array-length v8, v5

    if-ge v6, v8, :cond_7

    .line 362
    aget-object v4, v5, v6

    .line 363
    .local v4, "header":Ljava/lang/String;
    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    .line 364
    .local v0, "containsHeader":Z
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_5

    :cond_1
    const/4 v1, 0x1

    .line 365
    .local v1, "emptyHeader":Z
    :goto_2
    if-eqz v0, :cond_6

    if-eqz v1, :cond_2

    if-eqz v1, :cond_6

    iget-object v8, p0, Lorg/apache/commons/csv/CSVParser;->format:Lorg/apache/commons/csv/CSVFormat;

    .line 366
    invoke-virtual {v8}, Lorg/apache/commons/csv/CSVFormat;->getAllowMissingColumnNames()Z

    move-result v8

    if-nez v8, :cond_6

    .line 367
    :cond_2
    new-instance v8, Ljava/lang/IllegalArgumentException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "The header contains a duplicate name: \""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\" in "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 368
    invoke-static {v5}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 353
    .end local v0    # "containsHeader":Z
    .end local v1    # "emptyHeader":Z
    .end local v4    # "header":Ljava/lang/String;
    .end local v6    # "i":I
    :cond_3
    iget-object v8, p0, Lorg/apache/commons/csv/CSVParser;->format:Lorg/apache/commons/csv/CSVFormat;

    invoke-virtual {v8}, Lorg/apache/commons/csv/CSVFormat;->getSkipHeaderRecord()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 354
    invoke-virtual {p0}, Lorg/apache/commons/csv/CSVParser;->nextRecord()Lorg/apache/commons/csv/CSVRecord;

    .line 356
    :cond_4
    move-object v5, v2

    goto :goto_0

    .line 364
    .restart local v0    # "containsHeader":Z
    .restart local v4    # "header":Ljava/lang/String;
    .restart local v6    # "i":I
    :cond_5
    const/4 v1, 0x0

    goto :goto_2

    .line 370
    .restart local v1    # "emptyHeader":Z
    :cond_6
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v3, v4, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 361
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 374
    .end local v0    # "containsHeader":Z
    .end local v1    # "emptyHeader":Z
    .end local v4    # "header":Ljava/lang/String;
    .end local v5    # "headerRecord":[Ljava/lang/String;
    .end local v6    # "i":I
    :cond_7
    return-object v3
.end method

.method public static parse(Ljava/io/File;Ljava/nio/charset/Charset;Lorg/apache/commons/csv/CSVFormat;)Lorg/apache/commons/csv/CSVParser;
    .locals 3
    .param p0, "file"    # Ljava/io/File;
    .param p1, "charset"    # Ljava/nio/charset/Charset;
    .param p2, "format"    # Lorg/apache/commons/csv/CSVFormat;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 155
    const-string v0, "file"

    invoke-static {p0, v0}, Lorg/apache/commons/csv/Assertions;->notNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    const-string v0, "format"

    invoke-static {p2, v0}, Lorg/apache/commons/csv/Assertions;->notNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 158
    new-instance v0, Lorg/apache/commons/csv/CSVParser;

    new-instance v1, Ljava/io/InputStreamReader;

    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v2, p1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V

    invoke-direct {v0, v1, p2}, Lorg/apache/commons/csv/CSVParser;-><init>(Ljava/io/Reader;Lorg/apache/commons/csv/CSVFormat;)V

    return-object v0
.end method

.method public static parse(Ljava/lang/String;Lorg/apache/commons/csv/CSVFormat;)Lorg/apache/commons/csv/CSVParser;
    .locals 2
    .param p0, "string"    # Ljava/lang/String;
    .param p1, "format"    # Lorg/apache/commons/csv/CSVFormat;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 175
    const-string v0, "string"

    invoke-static {p0, v0}, Lorg/apache/commons/csv/Assertions;->notNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 176
    const-string v0, "format"

    invoke-static {p1, v0}, Lorg/apache/commons/csv/Assertions;->notNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 178
    new-instance v0, Lorg/apache/commons/csv/CSVParser;

    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, p0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1, p1}, Lorg/apache/commons/csv/CSVParser;-><init>(Ljava/io/Reader;Lorg/apache/commons/csv/CSVFormat;)V

    return-object v0
.end method

.method public static parse(Ljava/net/URL;Ljava/nio/charset/Charset;Lorg/apache/commons/csv/CSVFormat;)Lorg/apache/commons/csv/CSVParser;
    .locals 3
    .param p0, "url"    # Ljava/net/URL;
    .param p1, "charset"    # Ljava/nio/charset/Charset;
    .param p2, "format"    # Lorg/apache/commons/csv/CSVFormat;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 202
    const-string v0, "url"

    invoke-static {p0, v0}, Lorg/apache/commons/csv/Assertions;->notNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 203
    const-string v0, "charset"

    invoke-static {p1, v0}, Lorg/apache/commons/csv/Assertions;->notNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 204
    const-string v0, "format"

    invoke-static {p2, v0}, Lorg/apache/commons/csv/Assertions;->notNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 206
    new-instance v0, Lorg/apache/commons/csv/CSVParser;

    new-instance v1, Ljava/io/InputStreamReader;

    invoke-virtual {p0}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V

    invoke-direct {v0, v1, p2}, Lorg/apache/commons/csv/CSVParser;-><init>(Ljava/io/Reader;Lorg/apache/commons/csv/CSVFormat;)V

    return-object v0
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 268
    iget-object v0, p0, Lorg/apache/commons/csv/CSVParser;->lexer:Lorg/apache/commons/csv/Lexer;

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Lorg/apache/commons/csv/CSVParser;->lexer:Lorg/apache/commons/csv/Lexer;

    invoke-virtual {v0}, Lorg/apache/commons/csv/Lexer;->close()V

    .line 271
    :cond_0
    return-void
.end method

.method public getCurrentLineNumber()J
    .locals 2

    .prologue
    .line 284
    iget-object v0, p0, Lorg/apache/commons/csv/CSVParser;->lexer:Lorg/apache/commons/csv/Lexer;

    invoke-virtual {v0}, Lorg/apache/commons/csv/Lexer;->getCurrentLineNumber()J

    move-result-wide v0

    return-wide v0
.end method

.method public getHeaderMap()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 295
    iget-object v0, p0, Lorg/apache/commons/csv/CSVParser;->headerMap:Ljava/util/Map;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/LinkedHashMap;

    iget-object v1, p0, Lorg/apache/commons/csv/CSVParser;->headerMap:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    goto :goto_0
.end method

.method public getRecordNumber()J
    .locals 2

    .prologue
    .line 309
    iget-wide v0, p0, Lorg/apache/commons/csv/CSVParser;->recordNumber:J

    return-wide v0
.end method

.method public getRecords()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/commons/csv/CSVRecord;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 326
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 327
    .local v1, "records":Ljava/util/List;, "Ljava/util/List<Lorg/apache/commons/csv/CSVRecord;>;"
    :goto_0
    invoke-virtual {p0}, Lorg/apache/commons/csv/CSVParser;->nextRecord()Lorg/apache/commons/csv/CSVRecord;

    move-result-object v0

    .local v0, "rec":Lorg/apache/commons/csv/CSVRecord;
    if-eqz v0, :cond_0

    .line 328
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 330
    :cond_0
    return-object v1
.end method

.method public isClosed()Z
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lorg/apache/commons/csv/CSVParser;->lexer:Lorg/apache/commons/csv/Lexer;

    invoke-virtual {v0}, Lorg/apache/commons/csv/Lexer;->isClosed()Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/commons/csv/CSVRecord;",
            ">;"
        }
    .end annotation

    .prologue
    .line 390
    new-instance v0, Lorg/apache/commons/csv/CSVParser$1;

    invoke-direct {v0, p0}, Lorg/apache/commons/csv/CSVParser$1;-><init>(Lorg/apache/commons/csv/CSVParser;)V

    return-object v0
.end method

.method nextRecord()Lorg/apache/commons/csv/CSVRecord;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 445
    const/4 v0, 0x0

    .line 446
    .local v0, "result":Lorg/apache/commons/csv/CSVRecord;
    iget-object v1, p0, Lorg/apache/commons/csv/CSVParser;->record:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 447
    const/4 v6, 0x0

    .line 449
    .local v6, "sb":Ljava/lang/StringBuilder;
    :cond_0
    iget-object v1, p0, Lorg/apache/commons/csv/CSVParser;->reusableToken:Lorg/apache/commons/csv/Token;

    invoke-virtual {v1}, Lorg/apache/commons/csv/Token;->reset()V

    .line 450
    iget-object v1, p0, Lorg/apache/commons/csv/CSVParser;->lexer:Lorg/apache/commons/csv/Lexer;

    iget-object v2, p0, Lorg/apache/commons/csv/CSVParser;->reusableToken:Lorg/apache/commons/csv/Token;

    invoke-virtual {v1, v2}, Lorg/apache/commons/csv/Lexer;->nextToken(Lorg/apache/commons/csv/Token;)Lorg/apache/commons/csv/Token;

    .line 451
    sget-object v1, Lorg/apache/commons/csv/CSVParser$2;->$SwitchMap$org$apache$commons$csv$Token$Type:[I

    iget-object v2, p0, Lorg/apache/commons/csv/CSVParser;->reusableToken:Lorg/apache/commons/csv/Token;

    iget-object v2, v2, Lorg/apache/commons/csv/Token;->type:Lorg/apache/commons/csv/Token$Type;

    invoke-virtual {v2}, Lorg/apache/commons/csv/Token$Type;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 475
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unexpected Token type: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lorg/apache/commons/csv/CSVParser;->reusableToken:Lorg/apache/commons/csv/Token;

    iget-object v4, v4, Lorg/apache/commons/csv/Token;->type:Lorg/apache/commons/csv/Token$Type;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 453
    :pswitch_0
    invoke-direct {p0}, Lorg/apache/commons/csv/CSVParser;->addRecordValue()V

    .line 477
    :cond_1
    :goto_0
    iget-object v1, p0, Lorg/apache/commons/csv/CSVParser;->reusableToken:Lorg/apache/commons/csv/Token;

    iget-object v1, v1, Lorg/apache/commons/csv/Token;->type:Lorg/apache/commons/csv/Token$Type;

    sget-object v2, Lorg/apache/commons/csv/Token$Type;->TOKEN:Lorg/apache/commons/csv/Token$Type;

    if-eq v1, v2, :cond_0

    .line 479
    iget-object v1, p0, Lorg/apache/commons/csv/CSVParser;->record:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 480
    iget-wide v4, p0, Lorg/apache/commons/csv/CSVParser;->recordNumber:J

    const-wide/16 v8, 0x1

    add-long/2addr v4, v8

    iput-wide v4, p0, Lorg/apache/commons/csv/CSVParser;->recordNumber:J

    .line 481
    if-nez v6, :cond_4

    const/4 v3, 0x0

    .line 482
    .local v3, "comment":Ljava/lang/String;
    :goto_1
    new-instance v0, Lorg/apache/commons/csv/CSVRecord;

    .end local v0    # "result":Lorg/apache/commons/csv/CSVRecord;
    iget-object v1, p0, Lorg/apache/commons/csv/CSVParser;->record:Ljava/util/List;

    iget-object v2, p0, Lorg/apache/commons/csv/CSVParser;->record:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/commons/csv/CSVParser;->headerMap:Ljava/util/Map;

    iget-wide v4, p0, Lorg/apache/commons/csv/CSVParser;->recordNumber:J

    invoke-direct/range {v0 .. v5}, Lorg/apache/commons/csv/CSVRecord;-><init>([Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;J)V

    .line 485
    .end local v3    # "comment":Ljava/lang/String;
    .restart local v0    # "result":Lorg/apache/commons/csv/CSVRecord;
    :cond_2
    return-object v0

    .line 456
    :pswitch_1
    invoke-direct {p0}, Lorg/apache/commons/csv/CSVParser;->addRecordValue()V

    goto :goto_0

    .line 459
    :pswitch_2
    iget-object v1, p0, Lorg/apache/commons/csv/CSVParser;->reusableToken:Lorg/apache/commons/csv/Token;

    iget-boolean v1, v1, Lorg/apache/commons/csv/Token;->isReady:Z

    if-eqz v1, :cond_1

    .line 460
    invoke-direct {p0}, Lorg/apache/commons/csv/CSVParser;->addRecordValue()V

    goto :goto_0

    .line 464
    :pswitch_3
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "(line "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/commons/csv/CSVParser;->getCurrentLineNumber()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ") invalid parse sequence"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 466
    :pswitch_4
    if-nez v6, :cond_3

    .line 467
    new-instance v6, Ljava/lang/StringBuilder;

    .end local v6    # "sb":Ljava/lang/StringBuilder;
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 471
    .restart local v6    # "sb":Ljava/lang/StringBuilder;
    :goto_2
    iget-object v1, p0, Lorg/apache/commons/csv/CSVParser;->reusableToken:Lorg/apache/commons/csv/Token;

    iget-object v1, v1, Lorg/apache/commons/csv/Token;->content:Ljava/lang/StringBuilder;

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 472
    iget-object v1, p0, Lorg/apache/commons/csv/CSVParser;->reusableToken:Lorg/apache/commons/csv/Token;

    sget-object v2, Lorg/apache/commons/csv/Token$Type;->TOKEN:Lorg/apache/commons/csv/Token$Type;

    iput-object v2, v1, Lorg/apache/commons/csv/Token;->type:Lorg/apache/commons/csv/Token$Type;

    goto :goto_0

    .line 469
    :cond_3
    const/16 v1, 0xa

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 481
    :cond_4
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 451
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
