.class public final Lorg/apache/commons/csv/CSVFormat;
.super Ljava/lang/Object;
.source "CSVFormat.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final DEFAULT:Lorg/apache/commons/csv/CSVFormat;

.field public static final EXCEL:Lorg/apache/commons/csv/CSVFormat;

.field public static final MYSQL:Lorg/apache/commons/csv/CSVFormat;

.field public static final RFC4180:Lorg/apache/commons/csv/CSVFormat;

.field public static final TDF:Lorg/apache/commons/csv/CSVFormat;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final allowMissingColumnNames:Z

.field private final commentMarker:Ljava/lang/Character;

.field private final delimiter:C

.field private final escapeCharacter:Ljava/lang/Character;

.field private final header:[Ljava/lang/String;

.field private final ignoreEmptyLines:Z

.field private final ignoreSurroundingSpaces:Z

.field private final nullString:Ljava/lang/String;

.field private final quoteCharacter:Ljava/lang/Character;

.field private final quoteMode:Lorg/apache/commons/csv/QuoteMode;

.field private final recordSeparator:Ljava/lang/String;

.field private final skipHeaderRecord:Z


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const/16 v13, 0x9

    const/4 v7, 0x1

    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 175
    new-instance v0, Lorg/apache/commons/csv/CSVFormat;

    const/16 v1, 0x2c

    sget-object v2, Lorg/apache/commons/csv/Constants;->DOUBLE_QUOTE_CHAR:Ljava/lang/Character;

    const-string v8, "\r\n"

    move-object v4, v3

    move-object v5, v3

    move-object v9, v3

    move-object v10, v3

    move v11, v6

    move v12, v6

    invoke-direct/range {v0 .. v12}, Lorg/apache/commons/csv/CSVFormat;-><init>(CLjava/lang/Character;Lorg/apache/commons/csv/QuoteMode;Ljava/lang/Character;Ljava/lang/Character;ZZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;ZZ)V

    sput-object v0, Lorg/apache/commons/csv/CSVFormat;->DEFAULT:Lorg/apache/commons/csv/CSVFormat;

    .line 191
    sget-object v0, Lorg/apache/commons/csv/CSVFormat;->DEFAULT:Lorg/apache/commons/csv/CSVFormat;

    invoke-virtual {v0, v6}, Lorg/apache/commons/csv/CSVFormat;->withIgnoreEmptyLines(Z)Lorg/apache/commons/csv/CSVFormat;

    move-result-object v0

    sput-object v0, Lorg/apache/commons/csv/CSVFormat;->RFC4180:Lorg/apache/commons/csv/CSVFormat;

    .line 218
    sget-object v0, Lorg/apache/commons/csv/CSVFormat;->DEFAULT:Lorg/apache/commons/csv/CSVFormat;

    invoke-virtual {v0, v6}, Lorg/apache/commons/csv/CSVFormat;->withIgnoreEmptyLines(Z)Lorg/apache/commons/csv/CSVFormat;

    move-result-object v0

    sput-object v0, Lorg/apache/commons/csv/CSVFormat;->EXCEL:Lorg/apache/commons/csv/CSVFormat;

    .line 233
    sget-object v0, Lorg/apache/commons/csv/CSVFormat;->DEFAULT:Lorg/apache/commons/csv/CSVFormat;

    .line 235
    invoke-virtual {v0, v13}, Lorg/apache/commons/csv/CSVFormat;->withDelimiter(C)Lorg/apache/commons/csv/CSVFormat;

    move-result-object v0

    .line 236
    invoke-virtual {v0, v7}, Lorg/apache/commons/csv/CSVFormat;->withIgnoreSurroundingSpaces(Z)Lorg/apache/commons/csv/CSVFormat;

    move-result-object v0

    sput-object v0, Lorg/apache/commons/csv/CSVFormat;->TDF:Lorg/apache/commons/csv/CSVFormat;

    .line 259
    sget-object v0, Lorg/apache/commons/csv/CSVFormat;->DEFAULT:Lorg/apache/commons/csv/CSVFormat;

    .line 261
    invoke-virtual {v0, v13}, Lorg/apache/commons/csv/CSVFormat;->withDelimiter(C)Lorg/apache/commons/csv/CSVFormat;

    move-result-object v0

    const/16 v1, 0x5c

    .line 262
    invoke-virtual {v0, v1}, Lorg/apache/commons/csv/CSVFormat;->withEscape(C)Lorg/apache/commons/csv/CSVFormat;

    move-result-object v0

    .line 263
    invoke-virtual {v0, v6}, Lorg/apache/commons/csv/CSVFormat;->withIgnoreEmptyLines(Z)Lorg/apache/commons/csv/CSVFormat;

    move-result-object v0

    .line 264
    invoke-virtual {v0, v3}, Lorg/apache/commons/csv/CSVFormat;->withQuote(Ljava/lang/Character;)Lorg/apache/commons/csv/CSVFormat;

    move-result-object v0

    const/16 v1, 0xa

    .line 265
    invoke-virtual {v0, v1}, Lorg/apache/commons/csv/CSVFormat;->withRecordSeparator(C)Lorg/apache/commons/csv/CSVFormat;

    move-result-object v0

    sput-object v0, Lorg/apache/commons/csv/CSVFormat;->MYSQL:Lorg/apache/commons/csv/CSVFormat;

    .line 259
    return-void
.end method

.method private constructor <init>(CLjava/lang/Character;Lorg/apache/commons/csv/QuoteMode;Ljava/lang/Character;Ljava/lang/Character;ZZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;ZZ)V
    .locals 6
    .param p1, "delimiter"    # C
    .param p2, "quoteChar"    # Ljava/lang/Character;
    .param p3, "quoteMode"    # Lorg/apache/commons/csv/QuoteMode;
    .param p4, "commentStart"    # Ljava/lang/Character;
    .param p5, "escape"    # Ljava/lang/Character;
    .param p6, "ignoreSurroundingSpaces"    # Z
    .param p7, "ignoreEmptyLines"    # Z
    .param p8, "recordSeparator"    # Ljava/lang/String;
    .param p9, "nullString"    # Ljava/lang/String;
    .param p10, "header"    # [Ljava/lang/String;
    .param p11, "skipHeaderRecord"    # Z
    .param p12, "allowMissingColumnNames"    # Z

    .prologue
    .line 344
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 345
    invoke-static {p1}, Lorg/apache/commons/csv/CSVFormat;->isLineBreak(C)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 346
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "The delimiter cannot be a line break"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 348
    :cond_0
    iput-char p1, p0, Lorg/apache/commons/csv/CSVFormat;->delimiter:C

    .line 349
    iput-object p2, p0, Lorg/apache/commons/csv/CSVFormat;->quoteCharacter:Ljava/lang/Character;

    .line 350
    iput-object p3, p0, Lorg/apache/commons/csv/CSVFormat;->quoteMode:Lorg/apache/commons/csv/QuoteMode;

    .line 351
    iput-object p4, p0, Lorg/apache/commons/csv/CSVFormat;->commentMarker:Ljava/lang/Character;

    .line 352
    iput-object p5, p0, Lorg/apache/commons/csv/CSVFormat;->escapeCharacter:Ljava/lang/Character;

    .line 353
    iput-boolean p6, p0, Lorg/apache/commons/csv/CSVFormat;->ignoreSurroundingSpaces:Z

    .line 354
    move/from16 v0, p12

    iput-boolean v0, p0, Lorg/apache/commons/csv/CSVFormat;->allowMissingColumnNames:Z

    .line 355
    iput-boolean p7, p0, Lorg/apache/commons/csv/CSVFormat;->ignoreEmptyLines:Z

    .line 356
    iput-object p8, p0, Lorg/apache/commons/csv/CSVFormat;->recordSeparator:Ljava/lang/String;

    .line 357
    iput-object p9, p0, Lorg/apache/commons/csv/CSVFormat;->nullString:Ljava/lang/String;

    .line 358
    if-nez p10, :cond_1

    .line 359
    const/4 v3, 0x0

    iput-object v3, p0, Lorg/apache/commons/csv/CSVFormat;->header:[Ljava/lang/String;

    .line 370
    :goto_0
    move/from16 v0, p11

    iput-boolean v0, p0, Lorg/apache/commons/csv/CSVFormat;->skipHeaderRecord:Z

    .line 371
    invoke-direct {p0}, Lorg/apache/commons/csv/CSVFormat;->validate()V

    .line 372
    return-void

    .line 361
    :cond_1
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 362
    .local v1, "dupCheck":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    move-object/from16 v0, p10

    array-length v4, v0

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v4, :cond_3

    aget-object v2, p10, v3

    .line 363
    .local v2, "hdr":Ljava/lang/String;
    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 364
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "The header contains a duplicate entry: \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\' in "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 365
    invoke-static/range {p10 .. p10}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 362
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 368
    .end local v2    # "hdr":Ljava/lang/String;
    :cond_3
    invoke-virtual/range {p10 .. p10}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    iput-object v3, p0, Lorg/apache/commons/csv/CSVFormat;->header:[Ljava/lang/String;

    goto :goto_0
.end method

.method private static isLineBreak(C)Z
    .locals 1
    .param p0, "c"    # C

    .prologue
    .line 276
    const/16 v0, 0xa

    if-eq p0, v0, :cond_0

    const/16 v0, 0xd

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isLineBreak(Ljava/lang/Character;)Z
    .locals 1
    .param p0, "c"    # Ljava/lang/Character;

    .prologue
    .line 288
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Character;->charValue()C

    move-result v0

    invoke-static {v0}, Lorg/apache/commons/csv/CSVFormat;->isLineBreak(C)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static newFormat(C)Lorg/apache/commons/csv/CSVFormat;
    .locals 13
    .param p0, "delimiter"    # C

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 309
    new-instance v0, Lorg/apache/commons/csv/CSVFormat;

    move v1, p0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move v7, v6

    move-object v8, v2

    move-object v9, v2

    move-object v10, v2

    move v11, v6

    move v12, v6

    invoke-direct/range {v0 .. v12}, Lorg/apache/commons/csv/CSVFormat;-><init>(CLjava/lang/Character;Lorg/apache/commons/csv/QuoteMode;Ljava/lang/Character;Ljava/lang/Character;ZZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;ZZ)V

    return-object v0
.end method

.method private validate()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 716
    iget-object v0, p0, Lorg/apache/commons/csv/CSVFormat;->quoteCharacter:Ljava/lang/Character;

    if-eqz v0, :cond_0

    iget-char v0, p0, Lorg/apache/commons/csv/CSVFormat;->delimiter:C

    iget-object v1, p0, Lorg/apache/commons/csv/CSVFormat;->quoteCharacter:Ljava/lang/Character;

    invoke-virtual {v1}, Ljava/lang/Character;->charValue()C

    move-result v1

    if-ne v0, v1, :cond_0

    .line 717
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The quoteChar character and the delimiter cannot be the same (\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/commons/csv/CSVFormat;->quoteCharacter:Ljava/lang/Character;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\')"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 721
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/csv/CSVFormat;->escapeCharacter:Ljava/lang/Character;

    if-eqz v0, :cond_1

    iget-char v0, p0, Lorg/apache/commons/csv/CSVFormat;->delimiter:C

    iget-object v1, p0, Lorg/apache/commons/csv/CSVFormat;->escapeCharacter:Ljava/lang/Character;

    invoke-virtual {v1}, Ljava/lang/Character;->charValue()C

    move-result v1

    if-ne v0, v1, :cond_1

    .line 722
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The escape character and the delimiter cannot be the same (\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/commons/csv/CSVFormat;->escapeCharacter:Ljava/lang/Character;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\')"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 726
    :cond_1
    iget-object v0, p0, Lorg/apache/commons/csv/CSVFormat;->commentMarker:Ljava/lang/Character;

    if-eqz v0, :cond_2

    iget-char v0, p0, Lorg/apache/commons/csv/CSVFormat;->delimiter:C

    iget-object v1, p0, Lorg/apache/commons/csv/CSVFormat;->commentMarker:Ljava/lang/Character;

    invoke-virtual {v1}, Ljava/lang/Character;->charValue()C

    move-result v1

    if-ne v0, v1, :cond_2

    .line 727
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The comment start character and the delimiter cannot be the same (\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/commons/csv/CSVFormat;->commentMarker:Ljava/lang/Character;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\')"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 731
    :cond_2
    iget-object v0, p0, Lorg/apache/commons/csv/CSVFormat;->quoteCharacter:Ljava/lang/Character;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lorg/apache/commons/csv/CSVFormat;->quoteCharacter:Ljava/lang/Character;

    iget-object v1, p0, Lorg/apache/commons/csv/CSVFormat;->commentMarker:Ljava/lang/Character;

    invoke-virtual {v0, v1}, Ljava/lang/Character;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 732
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The comment start character and the quoteChar cannot be the same (\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/commons/csv/CSVFormat;->commentMarker:Ljava/lang/Character;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\')"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 736
    :cond_3
    iget-object v0, p0, Lorg/apache/commons/csv/CSVFormat;->escapeCharacter:Ljava/lang/Character;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lorg/apache/commons/csv/CSVFormat;->escapeCharacter:Ljava/lang/Character;

    iget-object v1, p0, Lorg/apache/commons/csv/CSVFormat;->commentMarker:Ljava/lang/Character;

    invoke-virtual {v0, v1}, Ljava/lang/Character;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 737
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The comment start and the escape character cannot be the same (\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/commons/csv/CSVFormat;->commentMarker:Ljava/lang/Character;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\')"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 741
    :cond_4
    iget-object v0, p0, Lorg/apache/commons/csv/CSVFormat;->escapeCharacter:Ljava/lang/Character;

    if-nez v0, :cond_5

    iget-object v0, p0, Lorg/apache/commons/csv/CSVFormat;->quoteMode:Lorg/apache/commons/csv/QuoteMode;

    sget-object v1, Lorg/apache/commons/csv/QuoteMode;->NONE:Lorg/apache/commons/csv/QuoteMode;

    if-ne v0, v1, :cond_5

    .line 742
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No quotes mode set but no escape character is set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 744
    :cond_5
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 376
    if-ne p0, p1, :cond_1

    .line 440
    :cond_0
    :goto_0
    return v1

    .line 379
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 380
    goto :goto_0

    .line 382
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 383
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 386
    check-cast v0, Lorg/apache/commons/csv/CSVFormat;

    .line 387
    .local v0, "other":Lorg/apache/commons/csv/CSVFormat;
    iget-char v3, p0, Lorg/apache/commons/csv/CSVFormat;->delimiter:C

    iget-char v4, v0, Lorg/apache/commons/csv/CSVFormat;->delimiter:C

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 388
    goto :goto_0

    .line 390
    :cond_4
    iget-object v3, p0, Lorg/apache/commons/csv/CSVFormat;->quoteMode:Lorg/apache/commons/csv/QuoteMode;

    iget-object v4, v0, Lorg/apache/commons/csv/CSVFormat;->quoteMode:Lorg/apache/commons/csv/QuoteMode;

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 391
    goto :goto_0

    .line 393
    :cond_5
    iget-object v3, p0, Lorg/apache/commons/csv/CSVFormat;->quoteCharacter:Ljava/lang/Character;

    if-nez v3, :cond_6

    .line 394
    iget-object v3, v0, Lorg/apache/commons/csv/CSVFormat;->quoteCharacter:Ljava/lang/Character;

    if-eqz v3, :cond_7

    move v1, v2

    .line 395
    goto :goto_0

    .line 397
    :cond_6
    iget-object v3, p0, Lorg/apache/commons/csv/CSVFormat;->quoteCharacter:Ljava/lang/Character;

    iget-object v4, v0, Lorg/apache/commons/csv/CSVFormat;->quoteCharacter:Ljava/lang/Character;

    invoke-virtual {v3, v4}, Ljava/lang/Character;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    move v1, v2

    .line 398
    goto :goto_0

    .line 400
    :cond_7
    iget-object v3, p0, Lorg/apache/commons/csv/CSVFormat;->commentMarker:Ljava/lang/Character;

    if-nez v3, :cond_8

    .line 401
    iget-object v3, v0, Lorg/apache/commons/csv/CSVFormat;->commentMarker:Ljava/lang/Character;

    if-eqz v3, :cond_9

    move v1, v2

    .line 402
    goto :goto_0

    .line 404
    :cond_8
    iget-object v3, p0, Lorg/apache/commons/csv/CSVFormat;->commentMarker:Ljava/lang/Character;

    iget-object v4, v0, Lorg/apache/commons/csv/CSVFormat;->commentMarker:Ljava/lang/Character;

    invoke-virtual {v3, v4}, Ljava/lang/Character;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    move v1, v2

    .line 405
    goto :goto_0

    .line 407
    :cond_9
    iget-object v3, p0, Lorg/apache/commons/csv/CSVFormat;->escapeCharacter:Ljava/lang/Character;

    if-nez v3, :cond_a

    .line 408
    iget-object v3, v0, Lorg/apache/commons/csv/CSVFormat;->escapeCharacter:Ljava/lang/Character;

    if-eqz v3, :cond_b

    move v1, v2

    .line 409
    goto :goto_0

    .line 411
    :cond_a
    iget-object v3, p0, Lorg/apache/commons/csv/CSVFormat;->escapeCharacter:Ljava/lang/Character;

    iget-object v4, v0, Lorg/apache/commons/csv/CSVFormat;->escapeCharacter:Ljava/lang/Character;

    invoke-virtual {v3, v4}, Ljava/lang/Character;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_b

    move v1, v2

    .line 412
    goto :goto_0

    .line 414
    :cond_b
    iget-object v3, p0, Lorg/apache/commons/csv/CSVFormat;->nullString:Ljava/lang/String;

    if-nez v3, :cond_c

    .line 415
    iget-object v3, v0, Lorg/apache/commons/csv/CSVFormat;->nullString:Ljava/lang/String;

    if-eqz v3, :cond_d

    move v1, v2

    .line 416
    goto :goto_0

    .line 418
    :cond_c
    iget-object v3, p0, Lorg/apache/commons/csv/CSVFormat;->nullString:Ljava/lang/String;

    iget-object v4, v0, Lorg/apache/commons/csv/CSVFormat;->nullString:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_d

    move v1, v2

    .line 419
    goto :goto_0

    .line 421
    :cond_d
    iget-object v3, p0, Lorg/apache/commons/csv/CSVFormat;->header:[Ljava/lang/String;

    iget-object v4, v0, Lorg/apache/commons/csv/CSVFormat;->header:[Ljava/lang/String;

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_e

    move v1, v2

    .line 422
    goto/16 :goto_0

    .line 424
    :cond_e
    iget-boolean v3, p0, Lorg/apache/commons/csv/CSVFormat;->ignoreSurroundingSpaces:Z

    iget-boolean v4, v0, Lorg/apache/commons/csv/CSVFormat;->ignoreSurroundingSpaces:Z

    if-eq v3, v4, :cond_f

    move v1, v2

    .line 425
    goto/16 :goto_0

    .line 427
    :cond_f
    iget-boolean v3, p0, Lorg/apache/commons/csv/CSVFormat;->ignoreEmptyLines:Z

    iget-boolean v4, v0, Lorg/apache/commons/csv/CSVFormat;->ignoreEmptyLines:Z

    if-eq v3, v4, :cond_10

    move v1, v2

    .line 428
    goto/16 :goto_0

    .line 430
    :cond_10
    iget-boolean v3, p0, Lorg/apache/commons/csv/CSVFormat;->skipHeaderRecord:Z

    iget-boolean v4, v0, Lorg/apache/commons/csv/CSVFormat;->skipHeaderRecord:Z

    if-eq v3, v4, :cond_11

    move v1, v2

    .line 431
    goto/16 :goto_0

    .line 433
    :cond_11
    iget-object v3, p0, Lorg/apache/commons/csv/CSVFormat;->recordSeparator:Ljava/lang/String;

    if-nez v3, :cond_12

    .line 434
    iget-object v3, v0, Lorg/apache/commons/csv/CSVFormat;->recordSeparator:Ljava/lang/String;

    if-eqz v3, :cond_0

    move v1, v2

    .line 435
    goto/16 :goto_0

    .line 437
    :cond_12
    iget-object v3, p0, Lorg/apache/commons/csv/CSVFormat;->recordSeparator:Ljava/lang/String;

    iget-object v4, v0, Lorg/apache/commons/csv/CSVFormat;->recordSeparator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 438
    goto/16 :goto_0
.end method

.method public varargs format([Ljava/lang/Object;)Ljava/lang/String;
    .locals 3
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    .line 451
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 453
    .local v1, "out":Ljava/io/StringWriter;
    :try_start_0
    new-instance v2, Lorg/apache/commons/csv/CSVPrinter;

    invoke-direct {v2, v1, p0}, Lorg/apache/commons/csv/CSVPrinter;-><init>(Ljava/lang/Appendable;Lorg/apache/commons/csv/CSVFormat;)V

    invoke-virtual {v2, p1}, Lorg/apache/commons/csv/CSVPrinter;->printRecord([Ljava/lang/Object;)V

    .line 454
    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    return-object v2

    .line 455
    :catch_0
    move-exception v0

    .line 457
    .local v0, "e":Ljava/io/IOException;
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public getAllowMissingColumnNames()Z
    .locals 1

    .prologue
    .line 504
    iget-boolean v0, p0, Lorg/apache/commons/csv/CSVFormat;->allowMissingColumnNames:Z

    return v0
.end method

.method public getCommentMarker()Ljava/lang/Character;
    .locals 1

    .prologue
    .line 467
    iget-object v0, p0, Lorg/apache/commons/csv/CSVFormat;->commentMarker:Ljava/lang/Character;

    return-object v0
.end method

.method public getDelimiter()C
    .locals 1

    .prologue
    .line 476
    iget-char v0, p0, Lorg/apache/commons/csv/CSVFormat;->delimiter:C

    return v0
.end method

.method public getEscapeCharacter()Ljava/lang/Character;
    .locals 1

    .prologue
    .line 485
    iget-object v0, p0, Lorg/apache/commons/csv/CSVFormat;->escapeCharacter:Ljava/lang/Character;

    return-object v0
.end method

.method public getHeader()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 494
    iget-object v0, p0, Lorg/apache/commons/csv/CSVFormat;->header:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/commons/csv/CSVFormat;->header:[Ljava/lang/String;

    invoke-virtual {v0}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIgnoreEmptyLines()Z
    .locals 1

    .prologue
    .line 514
    iget-boolean v0, p0, Lorg/apache/commons/csv/CSVFormat;->ignoreEmptyLines:Z

    return v0
.end method

.method public getIgnoreSurroundingSpaces()Z
    .locals 1

    .prologue
    .line 524
    iget-boolean v0, p0, Lorg/apache/commons/csv/CSVFormat;->ignoreSurroundingSpaces:Z

    return v0
.end method

.method public getNullString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 541
    iget-object v0, p0, Lorg/apache/commons/csv/CSVFormat;->nullString:Ljava/lang/String;

    return-object v0
.end method

.method public getQuoteCharacter()Ljava/lang/Character;
    .locals 1

    .prologue
    .line 550
    iget-object v0, p0, Lorg/apache/commons/csv/CSVFormat;->quoteCharacter:Ljava/lang/Character;

    return-object v0
.end method

.method public getQuoteMode()Lorg/apache/commons/csv/QuoteMode;
    .locals 1

    .prologue
    .line 559
    iget-object v0, p0, Lorg/apache/commons/csv/CSVFormat;->quoteMode:Lorg/apache/commons/csv/QuoteMode;

    return-object v0
.end method

.method public getRecordSeparator()Ljava/lang/String;
    .locals 1

    .prologue
    .line 568
    iget-object v0, p0, Lorg/apache/commons/csv/CSVFormat;->recordSeparator:Ljava/lang/String;

    return-object v0
.end method

.method public getSkipHeaderRecord()Z
    .locals 1

    .prologue
    .line 577
    iget-boolean v0, p0, Lorg/apache/commons/csv/CSVFormat;->skipHeaderRecord:Z

    return v0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/16 v5, 0x4d5

    const/16 v4, 0x4cf

    const/4 v3, 0x0

    .line 583
    const/16 v0, 0x1f

    .line 584
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 586
    .local v1, "result":I
    iget-char v2, p0, Lorg/apache/commons/csv/CSVFormat;->delimiter:C

    add-int/lit8 v1, v2, 0x1f

    .line 587
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lorg/apache/commons/csv/CSVFormat;->quoteMode:Lorg/apache/commons/csv/QuoteMode;

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int v1, v6, v2

    .line 588
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lorg/apache/commons/csv/CSVFormat;->quoteCharacter:Ljava/lang/Character;

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    add-int v1, v6, v2

    .line 589
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lorg/apache/commons/csv/CSVFormat;->commentMarker:Ljava/lang/Character;

    if-nez v2, :cond_2

    move v2, v3

    :goto_2
    add-int v1, v6, v2

    .line 590
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lorg/apache/commons/csv/CSVFormat;->escapeCharacter:Ljava/lang/Character;

    if-nez v2, :cond_3

    move v2, v3

    :goto_3
    add-int v1, v6, v2

    .line 591
    mul-int/lit8 v6, v1, 0x1f

    iget-object v2, p0, Lorg/apache/commons/csv/CSVFormat;->nullString:Ljava/lang/String;

    if-nez v2, :cond_4

    move v2, v3

    :goto_4
    add-int v1, v6, v2

    .line 592
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lorg/apache/commons/csv/CSVFormat;->ignoreSurroundingSpaces:Z

    if-eqz v2, :cond_5

    move v2, v4

    :goto_5
    add-int v1, v6, v2

    .line 593
    mul-int/lit8 v6, v1, 0x1f

    iget-boolean v2, p0, Lorg/apache/commons/csv/CSVFormat;->ignoreEmptyLines:Z

    if-eqz v2, :cond_6

    move v2, v4

    :goto_6
    add-int v1, v6, v2

    .line 594
    mul-int/lit8 v2, v1, 0x1f

    iget-boolean v6, p0, Lorg/apache/commons/csv/CSVFormat;->skipHeaderRecord:Z

    if-eqz v6, :cond_7

    :goto_7
    add-int v1, v2, v4

    .line 595
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lorg/apache/commons/csv/CSVFormat;->recordSeparator:Ljava/lang/String;

    if-nez v4, :cond_8

    :goto_8
    add-int v1, v2, v3

    .line 596
    mul-int/lit8 v2, v1, 0x1f

    iget-object v3, p0, Lorg/apache/commons/csv/CSVFormat;->header:[Ljava/lang/String;

    invoke-static {v3}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v3

    add-int v1, v2, v3

    .line 597
    return v1

    .line 587
    :cond_0
    iget-object v2, p0, Lorg/apache/commons/csv/CSVFormat;->quoteMode:Lorg/apache/commons/csv/QuoteMode;

    invoke-virtual {v2}, Lorg/apache/commons/csv/QuoteMode;->hashCode()I

    move-result v2

    goto :goto_0

    .line 588
    :cond_1
    iget-object v2, p0, Lorg/apache/commons/csv/CSVFormat;->quoteCharacter:Ljava/lang/Character;

    invoke-virtual {v2}, Ljava/lang/Character;->hashCode()I

    move-result v2

    goto :goto_1

    .line 589
    :cond_2
    iget-object v2, p0, Lorg/apache/commons/csv/CSVFormat;->commentMarker:Ljava/lang/Character;

    invoke-virtual {v2}, Ljava/lang/Character;->hashCode()I

    move-result v2

    goto :goto_2

    .line 590
    :cond_3
    iget-object v2, p0, Lorg/apache/commons/csv/CSVFormat;->escapeCharacter:Ljava/lang/Character;

    invoke-virtual {v2}, Ljava/lang/Character;->hashCode()I

    move-result v2

    goto :goto_3

    .line 591
    :cond_4
    iget-object v2, p0, Lorg/apache/commons/csv/CSVFormat;->nullString:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_5
    move v2, v5

    .line 592
    goto :goto_5

    :cond_6
    move v2, v5

    .line 593
    goto :goto_6

    :cond_7
    move v4, v5

    .line 594
    goto :goto_7

    .line 595
    :cond_8
    iget-object v3, p0, Lorg/apache/commons/csv/CSVFormat;->recordSeparator:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto :goto_8
.end method

.method public isCommentMarkerSet()Z
    .locals 1

    .prologue
    .line 608
    iget-object v0, p0, Lorg/apache/commons/csv/CSVFormat;->commentMarker:Ljava/lang/Character;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEscapeCharacterSet()Z
    .locals 1

    .prologue
    .line 617
    iget-object v0, p0, Lorg/apache/commons/csv/CSVFormat;->escapeCharacter:Ljava/lang/Character;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNullStringSet()Z
    .locals 1

    .prologue
    .line 626
    iget-object v0, p0, Lorg/apache/commons/csv/CSVFormat;->nullString:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isQuoteCharacterSet()Z
    .locals 1

    .prologue
    .line 635
    iget-object v0, p0, Lorg/apache/commons/csv/CSVFormat;->quoteCharacter:Ljava/lang/Character;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public parse(Ljava/io/Reader;)Lorg/apache/commons/csv/CSVParser;
    .locals 1
    .param p1, "in"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 652
    new-instance v0, Lorg/apache/commons/csv/CSVParser;

    invoke-direct {v0, p1, p0}, Lorg/apache/commons/csv/CSVParser;-><init>(Ljava/io/Reader;Lorg/apache/commons/csv/CSVFormat;)V

    return-object v0
.end method

.method public print(Ljava/lang/Appendable;)Lorg/apache/commons/csv/CSVPrinter;
    .locals 1
    .param p1, "out"    # Ljava/lang/Appendable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 669
    new-instance v0, Lorg/apache/commons/csv/CSVPrinter;

    invoke-direct {v0, p1, p0}, Lorg/apache/commons/csv/CSVPrinter;-><init>(Ljava/lang/Appendable;Lorg/apache/commons/csv/CSVFormat;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v4, 0x3e

    const/16 v3, 0x20

    .line 674
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 675
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "Delimiter=<"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-char v2, p0, Lorg/apache/commons/csv/CSVFormat;->delimiter:C

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 676
    invoke-virtual {p0}, Lorg/apache/commons/csv/CSVFormat;->isEscapeCharacterSet()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 677
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 678
    const-string v1, "Escape=<"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/commons/csv/CSVFormat;->escapeCharacter:Ljava/lang/Character;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 680
    :cond_0
    invoke-virtual {p0}, Lorg/apache/commons/csv/CSVFormat;->isQuoteCharacterSet()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 681
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 682
    const-string v1, "QuoteChar=<"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/commons/csv/CSVFormat;->quoteCharacter:Ljava/lang/Character;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 684
    :cond_1
    invoke-virtual {p0}, Lorg/apache/commons/csv/CSVFormat;->isCommentMarkerSet()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 685
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 686
    const-string v1, "CommentStart=<"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/commons/csv/CSVFormat;->commentMarker:Ljava/lang/Character;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 688
    :cond_2
    invoke-virtual {p0}, Lorg/apache/commons/csv/CSVFormat;->isNullStringSet()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 689
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 690
    const-string v1, "NullString=<"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/commons/csv/CSVFormat;->nullString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 692
    :cond_3
    iget-object v1, p0, Lorg/apache/commons/csv/CSVFormat;->recordSeparator:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 693
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 694
    const-string v1, "RecordSeparator=<"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/commons/csv/CSVFormat;->recordSeparator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 696
    :cond_4
    invoke-virtual {p0}, Lorg/apache/commons/csv/CSVFormat;->getIgnoreEmptyLines()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 697
    const-string v1, " EmptyLines:ignored"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 699
    :cond_5
    invoke-virtual {p0}, Lorg/apache/commons/csv/CSVFormat;->getIgnoreSurroundingSpaces()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 700
    const-string v1, " SurroundingSpaces:ignored"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 702
    :cond_6
    const-string v1, " SkipHeaderRecord:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lorg/apache/commons/csv/CSVFormat;->skipHeaderRecord:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 703
    iget-object v1, p0, Lorg/apache/commons/csv/CSVFormat;->header:[Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 704
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 705
    const-string v1, "Header:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/commons/csv/CSVFormat;->header:[Ljava/lang/String;

    invoke-static {v2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 707
    :cond_7
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public withAllowMissingColumnNames(Z)Lorg/apache/commons/csv/CSVFormat;
    .locals 13
    .param p1, "allowMissingColumnNames"    # Z

    .prologue
    .line 862
    new-instance v0, Lorg/apache/commons/csv/CSVFormat;

    iget-char v1, p0, Lorg/apache/commons/csv/CSVFormat;->delimiter:C

    iget-object v2, p0, Lorg/apache/commons/csv/CSVFormat;->quoteCharacter:Ljava/lang/Character;

    iget-object v3, p0, Lorg/apache/commons/csv/CSVFormat;->quoteMode:Lorg/apache/commons/csv/QuoteMode;

    iget-object v4, p0, Lorg/apache/commons/csv/CSVFormat;->commentMarker:Ljava/lang/Character;

    iget-object v5, p0, Lorg/apache/commons/csv/CSVFormat;->escapeCharacter:Ljava/lang/Character;

    iget-boolean v6, p0, Lorg/apache/commons/csv/CSVFormat;->ignoreSurroundingSpaces:Z

    iget-boolean v7, p0, Lorg/apache/commons/csv/CSVFormat;->ignoreEmptyLines:Z

    iget-object v8, p0, Lorg/apache/commons/csv/CSVFormat;->recordSeparator:Ljava/lang/String;

    iget-object v9, p0, Lorg/apache/commons/csv/CSVFormat;->nullString:Ljava/lang/String;

    iget-object v10, p0, Lorg/apache/commons/csv/CSVFormat;->header:[Ljava/lang/String;

    iget-boolean v11, p0, Lorg/apache/commons/csv/CSVFormat;->skipHeaderRecord:Z

    move v12, p1

    invoke-direct/range {v0 .. v12}, Lorg/apache/commons/csv/CSVFormat;-><init>(CLjava/lang/Character;Lorg/apache/commons/csv/QuoteMode;Ljava/lang/Character;Ljava/lang/Character;ZZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;ZZ)V

    return-object v0
.end method

.method public withCommentMarker(C)Lorg/apache/commons/csv/CSVFormat;
    .locals 1
    .param p1, "commentMarker"    # C

    .prologue
    .line 758
    invoke-static {p1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/commons/csv/CSVFormat;->withCommentMarker(Ljava/lang/Character;)Lorg/apache/commons/csv/CSVFormat;

    move-result-object v0

    return-object v0
.end method

.method public withCommentMarker(Ljava/lang/Character;)Lorg/apache/commons/csv/CSVFormat;
    .locals 13
    .param p1, "commentMarker"    # Ljava/lang/Character;

    .prologue
    .line 773
    invoke-static {p1}, Lorg/apache/commons/csv/CSVFormat;->isLineBreak(Ljava/lang/Character;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 774
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The comment start marker character cannot be a line break"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 776
    :cond_0
    new-instance v0, Lorg/apache/commons/csv/CSVFormat;

    iget-char v1, p0, Lorg/apache/commons/csv/CSVFormat;->delimiter:C

    iget-object v2, p0, Lorg/apache/commons/csv/CSVFormat;->quoteCharacter:Ljava/lang/Character;

    iget-object v3, p0, Lorg/apache/commons/csv/CSVFormat;->quoteMode:Lorg/apache/commons/csv/QuoteMode;

    iget-object v5, p0, Lorg/apache/commons/csv/CSVFormat;->escapeCharacter:Ljava/lang/Character;

    iget-boolean v6, p0, Lorg/apache/commons/csv/CSVFormat;->ignoreSurroundingSpaces:Z

    iget-boolean v7, p0, Lorg/apache/commons/csv/CSVFormat;->ignoreEmptyLines:Z

    iget-object v8, p0, Lorg/apache/commons/csv/CSVFormat;->recordSeparator:Ljava/lang/String;

    iget-object v9, p0, Lorg/apache/commons/csv/CSVFormat;->nullString:Ljava/lang/String;

    iget-object v10, p0, Lorg/apache/commons/csv/CSVFormat;->header:[Ljava/lang/String;

    iget-boolean v11, p0, Lorg/apache/commons/csv/CSVFormat;->skipHeaderRecord:Z

    iget-boolean v12, p0, Lorg/apache/commons/csv/CSVFormat;->allowMissingColumnNames:Z

    move-object v4, p1

    invoke-direct/range {v0 .. v12}, Lorg/apache/commons/csv/CSVFormat;-><init>(CLjava/lang/Character;Lorg/apache/commons/csv/QuoteMode;Ljava/lang/Character;Ljava/lang/Character;ZZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;ZZ)V

    return-object v0
.end method

.method public withDelimiter(C)Lorg/apache/commons/csv/CSVFormat;
    .locals 13
    .param p1, "delimiter"    # C

    .prologue
    .line 791
    invoke-static {p1}, Lorg/apache/commons/csv/CSVFormat;->isLineBreak(C)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 792
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The delimiter cannot be a line break"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 794
    :cond_0
    new-instance v0, Lorg/apache/commons/csv/CSVFormat;

    iget-object v2, p0, Lorg/apache/commons/csv/CSVFormat;->quoteCharacter:Ljava/lang/Character;

    iget-object v3, p0, Lorg/apache/commons/csv/CSVFormat;->quoteMode:Lorg/apache/commons/csv/QuoteMode;

    iget-object v4, p0, Lorg/apache/commons/csv/CSVFormat;->commentMarker:Ljava/lang/Character;

    iget-object v5, p0, Lorg/apache/commons/csv/CSVFormat;->escapeCharacter:Ljava/lang/Character;

    iget-boolean v6, p0, Lorg/apache/commons/csv/CSVFormat;->ignoreSurroundingSpaces:Z

    iget-boolean v7, p0, Lorg/apache/commons/csv/CSVFormat;->ignoreEmptyLines:Z

    iget-object v8, p0, Lorg/apache/commons/csv/CSVFormat;->recordSeparator:Ljava/lang/String;

    iget-object v9, p0, Lorg/apache/commons/csv/CSVFormat;->nullString:Ljava/lang/String;

    iget-object v10, p0, Lorg/apache/commons/csv/CSVFormat;->header:[Ljava/lang/String;

    iget-boolean v11, p0, Lorg/apache/commons/csv/CSVFormat;->skipHeaderRecord:Z

    iget-boolean v12, p0, Lorg/apache/commons/csv/CSVFormat;->allowMissingColumnNames:Z

    move v1, p1

    invoke-direct/range {v0 .. v12}, Lorg/apache/commons/csv/CSVFormat;-><init>(CLjava/lang/Character;Lorg/apache/commons/csv/QuoteMode;Ljava/lang/Character;Ljava/lang/Character;ZZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;ZZ)V

    return-object v0
.end method

.method public withEscape(C)Lorg/apache/commons/csv/CSVFormat;
    .locals 1
    .param p1, "escape"    # C

    .prologue
    .line 809
    invoke-static {p1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/commons/csv/CSVFormat;->withEscape(Ljava/lang/Character;)Lorg/apache/commons/csv/CSVFormat;

    move-result-object v0

    return-object v0
.end method

.method public withEscape(Ljava/lang/Character;)Lorg/apache/commons/csv/CSVFormat;
    .locals 13
    .param p1, "escape"    # Ljava/lang/Character;

    .prologue
    .line 822
    invoke-static {p1}, Lorg/apache/commons/csv/CSVFormat;->isLineBreak(Ljava/lang/Character;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 823
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The escape character cannot be a line break"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 825
    :cond_0
    new-instance v0, Lorg/apache/commons/csv/CSVFormat;

    iget-char v1, p0, Lorg/apache/commons/csv/CSVFormat;->delimiter:C

    iget-object v2, p0, Lorg/apache/commons/csv/CSVFormat;->quoteCharacter:Ljava/lang/Character;

    iget-object v3, p0, Lorg/apache/commons/csv/CSVFormat;->quoteMode:Lorg/apache/commons/csv/QuoteMode;

    iget-object v4, p0, Lorg/apache/commons/csv/CSVFormat;->commentMarker:Ljava/lang/Character;

    iget-boolean v6, p0, Lorg/apache/commons/csv/CSVFormat;->ignoreSurroundingSpaces:Z

    iget-boolean v7, p0, Lorg/apache/commons/csv/CSVFormat;->ignoreEmptyLines:Z

    iget-object v8, p0, Lorg/apache/commons/csv/CSVFormat;->recordSeparator:Ljava/lang/String;

    iget-object v9, p0, Lorg/apache/commons/csv/CSVFormat;->nullString:Ljava/lang/String;

    iget-object v10, p0, Lorg/apache/commons/csv/CSVFormat;->header:[Ljava/lang/String;

    iget-boolean v11, p0, Lorg/apache/commons/csv/CSVFormat;->skipHeaderRecord:Z

    iget-boolean v12, p0, Lorg/apache/commons/csv/CSVFormat;->allowMissingColumnNames:Z

    move-object v5, p1

    invoke-direct/range {v0 .. v12}, Lorg/apache/commons/csv/CSVFormat;-><init>(CLjava/lang/Character;Lorg/apache/commons/csv/QuoteMode;Ljava/lang/Character;Ljava/lang/Character;ZZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;ZZ)V

    return-object v0
.end method

.method public varargs withHeader([Ljava/lang/String;)Lorg/apache/commons/csv/CSVFormat;
    .locals 13
    .param p1, "header"    # [Ljava/lang/String;

    .prologue
    .line 848
    new-instance v0, Lorg/apache/commons/csv/CSVFormat;

    iget-char v1, p0, Lorg/apache/commons/csv/CSVFormat;->delimiter:C

    iget-object v2, p0, Lorg/apache/commons/csv/CSVFormat;->quoteCharacter:Ljava/lang/Character;

    iget-object v3, p0, Lorg/apache/commons/csv/CSVFormat;->quoteMode:Lorg/apache/commons/csv/QuoteMode;

    iget-object v4, p0, Lorg/apache/commons/csv/CSVFormat;->commentMarker:Ljava/lang/Character;

    iget-object v5, p0, Lorg/apache/commons/csv/CSVFormat;->escapeCharacter:Ljava/lang/Character;

    iget-boolean v6, p0, Lorg/apache/commons/csv/CSVFormat;->ignoreSurroundingSpaces:Z

    iget-boolean v7, p0, Lorg/apache/commons/csv/CSVFormat;->ignoreEmptyLines:Z

    iget-object v8, p0, Lorg/apache/commons/csv/CSVFormat;->recordSeparator:Ljava/lang/String;

    iget-object v9, p0, Lorg/apache/commons/csv/CSVFormat;->nullString:Ljava/lang/String;

    iget-boolean v11, p0, Lorg/apache/commons/csv/CSVFormat;->skipHeaderRecord:Z

    iget-boolean v12, p0, Lorg/apache/commons/csv/CSVFormat;->allowMissingColumnNames:Z

    move-object v10, p1

    invoke-direct/range {v0 .. v12}, Lorg/apache/commons/csv/CSVFormat;-><init>(CLjava/lang/Character;Lorg/apache/commons/csv/QuoteMode;Ljava/lang/Character;Ljava/lang/Character;ZZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;ZZ)V

    return-object v0
.end method

.method public withIgnoreEmptyLines(Z)Lorg/apache/commons/csv/CSVFormat;
    .locals 13
    .param p1, "ignoreEmptyLines"    # Z

    .prologue
    .line 876
    new-instance v0, Lorg/apache/commons/csv/CSVFormat;

    iget-char v1, p0, Lorg/apache/commons/csv/CSVFormat;->delimiter:C

    iget-object v2, p0, Lorg/apache/commons/csv/CSVFormat;->quoteCharacter:Ljava/lang/Character;

    iget-object v3, p0, Lorg/apache/commons/csv/CSVFormat;->quoteMode:Lorg/apache/commons/csv/QuoteMode;

    iget-object v4, p0, Lorg/apache/commons/csv/CSVFormat;->commentMarker:Ljava/lang/Character;

    iget-object v5, p0, Lorg/apache/commons/csv/CSVFormat;->escapeCharacter:Ljava/lang/Character;

    iget-boolean v6, p0, Lorg/apache/commons/csv/CSVFormat;->ignoreSurroundingSpaces:Z

    iget-object v8, p0, Lorg/apache/commons/csv/CSVFormat;->recordSeparator:Ljava/lang/String;

    iget-object v9, p0, Lorg/apache/commons/csv/CSVFormat;->nullString:Ljava/lang/String;

    iget-object v10, p0, Lorg/apache/commons/csv/CSVFormat;->header:[Ljava/lang/String;

    iget-boolean v11, p0, Lorg/apache/commons/csv/CSVFormat;->skipHeaderRecord:Z

    iget-boolean v12, p0, Lorg/apache/commons/csv/CSVFormat;->allowMissingColumnNames:Z

    move v7, p1

    invoke-direct/range {v0 .. v12}, Lorg/apache/commons/csv/CSVFormat;-><init>(CLjava/lang/Character;Lorg/apache/commons/csv/QuoteMode;Ljava/lang/Character;Ljava/lang/Character;ZZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;ZZ)V

    return-object v0
.end method

.method public withIgnoreSurroundingSpaces(Z)Lorg/apache/commons/csv/CSVFormat;
    .locals 13
    .param p1, "ignoreSurroundingSpaces"    # Z

    .prologue
    .line 890
    new-instance v0, Lorg/apache/commons/csv/CSVFormat;

    iget-char v1, p0, Lorg/apache/commons/csv/CSVFormat;->delimiter:C

    iget-object v2, p0, Lorg/apache/commons/csv/CSVFormat;->quoteCharacter:Ljava/lang/Character;

    iget-object v3, p0, Lorg/apache/commons/csv/CSVFormat;->quoteMode:Lorg/apache/commons/csv/QuoteMode;

    iget-object v4, p0, Lorg/apache/commons/csv/CSVFormat;->commentMarker:Ljava/lang/Character;

    iget-object v5, p0, Lorg/apache/commons/csv/CSVFormat;->escapeCharacter:Ljava/lang/Character;

    iget-boolean v7, p0, Lorg/apache/commons/csv/CSVFormat;->ignoreEmptyLines:Z

    iget-object v8, p0, Lorg/apache/commons/csv/CSVFormat;->recordSeparator:Ljava/lang/String;

    iget-object v9, p0, Lorg/apache/commons/csv/CSVFormat;->nullString:Ljava/lang/String;

    iget-object v10, p0, Lorg/apache/commons/csv/CSVFormat;->header:[Ljava/lang/String;

    iget-boolean v11, p0, Lorg/apache/commons/csv/CSVFormat;->skipHeaderRecord:Z

    iget-boolean v12, p0, Lorg/apache/commons/csv/CSVFormat;->allowMissingColumnNames:Z

    move v6, p1

    invoke-direct/range {v0 .. v12}, Lorg/apache/commons/csv/CSVFormat;-><init>(CLjava/lang/Character;Lorg/apache/commons/csv/QuoteMode;Ljava/lang/Character;Ljava/lang/Character;ZZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;ZZ)V

    return-object v0
.end method

.method public withNullString(Ljava/lang/String;)Lorg/apache/commons/csv/CSVFormat;
    .locals 13
    .param p1, "nullString"    # Ljava/lang/String;

    .prologue
    .line 911
    new-instance v0, Lorg/apache/commons/csv/CSVFormat;

    iget-char v1, p0, Lorg/apache/commons/csv/CSVFormat;->delimiter:C

    iget-object v2, p0, Lorg/apache/commons/csv/CSVFormat;->quoteCharacter:Ljava/lang/Character;

    iget-object v3, p0, Lorg/apache/commons/csv/CSVFormat;->quoteMode:Lorg/apache/commons/csv/QuoteMode;

    iget-object v4, p0, Lorg/apache/commons/csv/CSVFormat;->commentMarker:Ljava/lang/Character;

    iget-object v5, p0, Lorg/apache/commons/csv/CSVFormat;->escapeCharacter:Ljava/lang/Character;

    iget-boolean v6, p0, Lorg/apache/commons/csv/CSVFormat;->ignoreSurroundingSpaces:Z

    iget-boolean v7, p0, Lorg/apache/commons/csv/CSVFormat;->ignoreEmptyLines:Z

    iget-object v8, p0, Lorg/apache/commons/csv/CSVFormat;->recordSeparator:Ljava/lang/String;

    iget-object v10, p0, Lorg/apache/commons/csv/CSVFormat;->header:[Ljava/lang/String;

    iget-boolean v11, p0, Lorg/apache/commons/csv/CSVFormat;->skipHeaderRecord:Z

    iget-boolean v12, p0, Lorg/apache/commons/csv/CSVFormat;->allowMissingColumnNames:Z

    move-object v9, p1

    invoke-direct/range {v0 .. v12}, Lorg/apache/commons/csv/CSVFormat;-><init>(CLjava/lang/Character;Lorg/apache/commons/csv/QuoteMode;Ljava/lang/Character;Ljava/lang/Character;ZZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;ZZ)V

    return-object v0
.end method

.method public withQuote(C)Lorg/apache/commons/csv/CSVFormat;
    .locals 1
    .param p1, "quoteChar"    # C

    .prologue
    .line 926
    invoke-static {p1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/commons/csv/CSVFormat;->withQuote(Ljava/lang/Character;)Lorg/apache/commons/csv/CSVFormat;

    move-result-object v0

    return-object v0
.end method

.method public withQuote(Ljava/lang/Character;)Lorg/apache/commons/csv/CSVFormat;
    .locals 13
    .param p1, "quoteChar"    # Ljava/lang/Character;

    .prologue
    .line 939
    invoke-static {p1}, Lorg/apache/commons/csv/CSVFormat;->isLineBreak(Ljava/lang/Character;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 940
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The quoteChar cannot be a line break"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 942
    :cond_0
    new-instance v0, Lorg/apache/commons/csv/CSVFormat;

    iget-char v1, p0, Lorg/apache/commons/csv/CSVFormat;->delimiter:C

    iget-object v3, p0, Lorg/apache/commons/csv/CSVFormat;->quoteMode:Lorg/apache/commons/csv/QuoteMode;

    iget-object v4, p0, Lorg/apache/commons/csv/CSVFormat;->commentMarker:Ljava/lang/Character;

    iget-object v5, p0, Lorg/apache/commons/csv/CSVFormat;->escapeCharacter:Ljava/lang/Character;

    iget-boolean v6, p0, Lorg/apache/commons/csv/CSVFormat;->ignoreSurroundingSpaces:Z

    iget-boolean v7, p0, Lorg/apache/commons/csv/CSVFormat;->ignoreEmptyLines:Z

    iget-object v8, p0, Lorg/apache/commons/csv/CSVFormat;->recordSeparator:Ljava/lang/String;

    iget-object v9, p0, Lorg/apache/commons/csv/CSVFormat;->nullString:Ljava/lang/String;

    iget-object v10, p0, Lorg/apache/commons/csv/CSVFormat;->header:[Ljava/lang/String;

    iget-boolean v11, p0, Lorg/apache/commons/csv/CSVFormat;->skipHeaderRecord:Z

    iget-boolean v12, p0, Lorg/apache/commons/csv/CSVFormat;->allowMissingColumnNames:Z

    move-object v2, p1

    invoke-direct/range {v0 .. v12}, Lorg/apache/commons/csv/CSVFormat;-><init>(CLjava/lang/Character;Lorg/apache/commons/csv/QuoteMode;Ljava/lang/Character;Ljava/lang/Character;ZZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;ZZ)V

    return-object v0
.end method

.method public withQuoteMode(Lorg/apache/commons/csv/QuoteMode;)Lorg/apache/commons/csv/CSVFormat;
    .locals 13
    .param p1, "quoteModePolicy"    # Lorg/apache/commons/csv/QuoteMode;

    .prologue
    .line 956
    new-instance v0, Lorg/apache/commons/csv/CSVFormat;

    iget-char v1, p0, Lorg/apache/commons/csv/CSVFormat;->delimiter:C

    iget-object v2, p0, Lorg/apache/commons/csv/CSVFormat;->quoteCharacter:Ljava/lang/Character;

    iget-object v4, p0, Lorg/apache/commons/csv/CSVFormat;->commentMarker:Ljava/lang/Character;

    iget-object v5, p0, Lorg/apache/commons/csv/CSVFormat;->escapeCharacter:Ljava/lang/Character;

    iget-boolean v6, p0, Lorg/apache/commons/csv/CSVFormat;->ignoreSurroundingSpaces:Z

    iget-boolean v7, p0, Lorg/apache/commons/csv/CSVFormat;->ignoreEmptyLines:Z

    iget-object v8, p0, Lorg/apache/commons/csv/CSVFormat;->recordSeparator:Ljava/lang/String;

    iget-object v9, p0, Lorg/apache/commons/csv/CSVFormat;->nullString:Ljava/lang/String;

    iget-object v10, p0, Lorg/apache/commons/csv/CSVFormat;->header:[Ljava/lang/String;

    iget-boolean v11, p0, Lorg/apache/commons/csv/CSVFormat;->skipHeaderRecord:Z

    iget-boolean v12, p0, Lorg/apache/commons/csv/CSVFormat;->allowMissingColumnNames:Z

    move-object v3, p1

    invoke-direct/range {v0 .. v12}, Lorg/apache/commons/csv/CSVFormat;-><init>(CLjava/lang/Character;Lorg/apache/commons/csv/QuoteMode;Ljava/lang/Character;Ljava/lang/Character;ZZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;ZZ)V

    return-object v0
.end method

.method public withRecordSeparator(C)Lorg/apache/commons/csv/CSVFormat;
    .locals 1
    .param p1, "recordSeparator"    # C

    .prologue
    .line 973
    invoke-static {p1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/commons/csv/CSVFormat;->withRecordSeparator(Ljava/lang/String;)Lorg/apache/commons/csv/CSVFormat;

    move-result-object v0

    return-object v0
.end method

.method public withRecordSeparator(Ljava/lang/String;)Lorg/apache/commons/csv/CSVFormat;
    .locals 13
    .param p1, "recordSeparator"    # Ljava/lang/String;

    .prologue
    .line 990
    new-instance v0, Lorg/apache/commons/csv/CSVFormat;

    iget-char v1, p0, Lorg/apache/commons/csv/CSVFormat;->delimiter:C

    iget-object v2, p0, Lorg/apache/commons/csv/CSVFormat;->quoteCharacter:Ljava/lang/Character;

    iget-object v3, p0, Lorg/apache/commons/csv/CSVFormat;->quoteMode:Lorg/apache/commons/csv/QuoteMode;

    iget-object v4, p0, Lorg/apache/commons/csv/CSVFormat;->commentMarker:Ljava/lang/Character;

    iget-object v5, p0, Lorg/apache/commons/csv/CSVFormat;->escapeCharacter:Ljava/lang/Character;

    iget-boolean v6, p0, Lorg/apache/commons/csv/CSVFormat;->ignoreSurroundingSpaces:Z

    iget-boolean v7, p0, Lorg/apache/commons/csv/CSVFormat;->ignoreEmptyLines:Z

    iget-object v9, p0, Lorg/apache/commons/csv/CSVFormat;->nullString:Ljava/lang/String;

    iget-object v10, p0, Lorg/apache/commons/csv/CSVFormat;->header:[Ljava/lang/String;

    iget-boolean v11, p0, Lorg/apache/commons/csv/CSVFormat;->skipHeaderRecord:Z

    iget-boolean v12, p0, Lorg/apache/commons/csv/CSVFormat;->allowMissingColumnNames:Z

    move-object v8, p1

    invoke-direct/range {v0 .. v12}, Lorg/apache/commons/csv/CSVFormat;-><init>(CLjava/lang/Character;Lorg/apache/commons/csv/QuoteMode;Ljava/lang/Character;Ljava/lang/Character;ZZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;ZZ)V

    return-object v0
.end method

.method public withSkipHeaderRecord(Z)Lorg/apache/commons/csv/CSVFormat;
    .locals 13
    .param p1, "skipHeaderRecord"    # Z

    .prologue
    .line 1005
    new-instance v0, Lorg/apache/commons/csv/CSVFormat;

    iget-char v1, p0, Lorg/apache/commons/csv/CSVFormat;->delimiter:C

    iget-object v2, p0, Lorg/apache/commons/csv/CSVFormat;->quoteCharacter:Ljava/lang/Character;

    iget-object v3, p0, Lorg/apache/commons/csv/CSVFormat;->quoteMode:Lorg/apache/commons/csv/QuoteMode;

    iget-object v4, p0, Lorg/apache/commons/csv/CSVFormat;->commentMarker:Ljava/lang/Character;

    iget-object v5, p0, Lorg/apache/commons/csv/CSVFormat;->escapeCharacter:Ljava/lang/Character;

    iget-boolean v6, p0, Lorg/apache/commons/csv/CSVFormat;->ignoreSurroundingSpaces:Z

    iget-boolean v7, p0, Lorg/apache/commons/csv/CSVFormat;->ignoreEmptyLines:Z

    iget-object v8, p0, Lorg/apache/commons/csv/CSVFormat;->recordSeparator:Ljava/lang/String;

    iget-object v9, p0, Lorg/apache/commons/csv/CSVFormat;->nullString:Ljava/lang/String;

    iget-object v10, p0, Lorg/apache/commons/csv/CSVFormat;->header:[Ljava/lang/String;

    iget-boolean v12, p0, Lorg/apache/commons/csv/CSVFormat;->allowMissingColumnNames:Z

    move v11, p1

    invoke-direct/range {v0 .. v12}, Lorg/apache/commons/csv/CSVFormat;-><init>(CLjava/lang/Character;Lorg/apache/commons/csv/QuoteMode;Ljava/lang/Character;Ljava/lang/Character;ZZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;ZZ)V

    return-object v0
.end method
