.class public final Lorg/apache/commons/csv/CSVPrinter;
.super Ljava/lang/Object;
.source "CSVPrinter.java"

# interfaces
.implements Ljava/io/Flushable;
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/commons/csv/CSVPrinter$1;
    }
.end annotation


# instance fields
.field private final format:Lorg/apache/commons/csv/CSVFormat;

.field private newRecord:Z

.field private final out:Ljava/lang/Appendable;


# direct methods
.method public constructor <init>(Ljava/lang/Appendable;Lorg/apache/commons/csv/CSVFormat;)V
    .locals 1
    .param p1, "out"    # Ljava/lang/Appendable;
    .param p2, "format"    # Lorg/apache/commons/csv/CSVFormat;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/commons/csv/CSVPrinter;->newRecord:Z

    .line 62
    const-string v0, "out"

    invoke-static {p1, v0}, Lorg/apache/commons/csv/Assertions;->notNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    const-string v0, "format"

    invoke-static {p2, v0}, Lorg/apache/commons/csv/Assertions;->notNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    iput-object p1, p0, Lorg/apache/commons/csv/CSVPrinter;->out:Ljava/lang/Appendable;

    .line 66
    iput-object p2, p0, Lorg/apache/commons/csv/CSVPrinter;->format:Lorg/apache/commons/csv/CSVFormat;

    .line 69
    invoke-virtual {p2}, Lorg/apache/commons/csv/CSVFormat;->getHeader()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 70
    invoke-virtual {p2}, Lorg/apache/commons/csv/CSVFormat;->getHeader()[Ljava/lang/String;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lorg/apache/commons/csv/CSVPrinter;->printRecord([Ljava/lang/Object;)V

    .line 72
    :cond_0
    return-void
.end method

.method private print(Ljava/lang/Object;Ljava/lang/CharSequence;II)V
    .locals 2
    .param p1, "object"    # Ljava/lang/Object;
    .param p2, "value"    # Ljava/lang/CharSequence;
    .param p3, "offset"    # I
    .param p4, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 118
    iget-boolean v0, p0, Lorg/apache/commons/csv/CSVPrinter;->newRecord:Z

    if-nez v0, :cond_0

    .line 119
    iget-object v0, p0, Lorg/apache/commons/csv/CSVPrinter;->out:Ljava/lang/Appendable;

    iget-object v1, p0, Lorg/apache/commons/csv/CSVPrinter;->format:Lorg/apache/commons/csv/CSVFormat;

    invoke-virtual {v1}, Lorg/apache/commons/csv/CSVFormat;->getDelimiter()C

    move-result v1

    invoke-interface {v0, v1}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 121
    :cond_0
    iget-object v0, p0, Lorg/apache/commons/csv/CSVPrinter;->format:Lorg/apache/commons/csv/CSVFormat;

    invoke-virtual {v0}, Lorg/apache/commons/csv/CSVFormat;->isQuoteCharacterSet()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 123
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/apache/commons/csv/CSVPrinter;->printAndQuote(Ljava/lang/Object;Ljava/lang/CharSequence;II)V

    .line 129
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/commons/csv/CSVPrinter;->newRecord:Z

    .line 130
    return-void

    .line 124
    :cond_1
    iget-object v0, p0, Lorg/apache/commons/csv/CSVPrinter;->format:Lorg/apache/commons/csv/CSVFormat;

    invoke-virtual {v0}, Lorg/apache/commons/csv/CSVFormat;->isEscapeCharacterSet()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 125
    invoke-direct {p0, p2, p3, p4}, Lorg/apache/commons/csv/CSVPrinter;->printAndEscape(Ljava/lang/CharSequence;II)V

    goto :goto_0

    .line 127
    :cond_2
    iget-object v0, p0, Lorg/apache/commons/csv/CSVPrinter;->out:Ljava/lang/Appendable;

    add-int v1, p3, p4

    invoke-interface {v0, p2, p3, v1}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;II)Ljava/lang/Appendable;

    goto :goto_0
.end method

.method private printAndEscape(Ljava/lang/CharSequence;II)V
    .locals 9
    .param p1, "value"    # Ljava/lang/CharSequence;
    .param p2, "offset"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v8, 0xd

    const/16 v7, 0xa

    .line 136
    move v5, p2

    .line 137
    .local v5, "start":I
    move v4, p2

    .line 138
    .local v4, "pos":I
    add-int v2, p2, p3

    .line 140
    .local v2, "end":I
    iget-object v6, p0, Lorg/apache/commons/csv/CSVPrinter;->format:Lorg/apache/commons/csv/CSVFormat;

    invoke-virtual {v6}, Lorg/apache/commons/csv/CSVFormat;->getDelimiter()C

    move-result v1

    .line 141
    .local v1, "delim":C
    iget-object v6, p0, Lorg/apache/commons/csv/CSVPrinter;->format:Lorg/apache/commons/csv/CSVFormat;

    invoke-virtual {v6}, Lorg/apache/commons/csv/CSVFormat;->getEscapeCharacter()Ljava/lang/Character;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Character;->charValue()C

    move-result v3

    .line 143
    .local v3, "escape":C
    :goto_0
    if-ge v4, v2, :cond_5

    .line 144
    invoke-interface {p1, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 145
    .local v0, "c":C
    if-eq v0, v8, :cond_0

    if-eq v0, v7, :cond_0

    if-eq v0, v1, :cond_0

    if-ne v0, v3, :cond_3

    .line 147
    :cond_0
    if-le v4, v5, :cond_1

    .line 148
    iget-object v6, p0, Lorg/apache/commons/csv/CSVPrinter;->out:Ljava/lang/Appendable;

    invoke-interface {v6, p1, v5, v4}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;II)Ljava/lang/Appendable;

    .line 150
    :cond_1
    if-ne v0, v7, :cond_4

    .line 151
    const/16 v0, 0x6e

    .line 156
    :cond_2
    :goto_1
    iget-object v6, p0, Lorg/apache/commons/csv/CSVPrinter;->out:Ljava/lang/Appendable;

    invoke-interface {v6, v3}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 157
    iget-object v6, p0, Lorg/apache/commons/csv/CSVPrinter;->out:Ljava/lang/Appendable;

    invoke-interface {v6, v0}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 159
    add-int/lit8 v5, v4, 0x1

    .line 162
    :cond_3
    add-int/lit8 v4, v4, 0x1

    .line 163
    goto :goto_0

    .line 152
    :cond_4
    if-ne v0, v8, :cond_2

    .line 153
    const/16 v0, 0x72

    goto :goto_1

    .line 166
    .end local v0    # "c":C
    :cond_5
    if-le v4, v5, :cond_6

    .line 167
    iget-object v6, p0, Lorg/apache/commons/csv/CSVPrinter;->out:Ljava/lang/Appendable;

    invoke-interface {v6, p1, v5, v4}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;II)Ljava/lang/Appendable;

    .line 169
    :cond_6
    return-void
.end method

.method private printAndQuote(Ljava/lang/Object;Ljava/lang/CharSequence;II)V
    .locals 11
    .param p1, "object"    # Ljava/lang/Object;
    .param p2, "value"    # Ljava/lang/CharSequence;
    .param p3, "offset"    # I
    .param p4, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 177
    const/4 v4, 0x0

    .line 178
    .local v4, "quote":Z
    move v7, p3

    .line 179
    .local v7, "start":I
    move v3, p3

    .line 180
    .local v3, "pos":I
    add-int v2, p3, p4

    .line 182
    .local v2, "end":I
    iget-object v8, p0, Lorg/apache/commons/csv/CSVPrinter;->format:Lorg/apache/commons/csv/CSVFormat;

    invoke-virtual {v8}, Lorg/apache/commons/csv/CSVFormat;->getDelimiter()C

    move-result v1

    .line 183
    .local v1, "delimChar":C
    iget-object v8, p0, Lorg/apache/commons/csv/CSVPrinter;->format:Lorg/apache/commons/csv/CSVFormat;

    invoke-virtual {v8}, Lorg/apache/commons/csv/CSVFormat;->getQuoteCharacter()Ljava/lang/Character;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Character;->charValue()C

    move-result v5

    .line 185
    .local v5, "quoteChar":C
    iget-object v8, p0, Lorg/apache/commons/csv/CSVPrinter;->format:Lorg/apache/commons/csv/CSVFormat;

    invoke-virtual {v8}, Lorg/apache/commons/csv/CSVFormat;->getQuoteMode()Lorg/apache/commons/csv/QuoteMode;

    move-result-object v6

    .line 186
    .local v6, "quoteModePolicy":Lorg/apache/commons/csv/QuoteMode;
    if-nez v6, :cond_0

    .line 187
    sget-object v6, Lorg/apache/commons/csv/QuoteMode;->MINIMAL:Lorg/apache/commons/csv/QuoteMode;

    .line 189
    :cond_0
    sget-object v8, Lorg/apache/commons/csv/CSVPrinter$1;->$SwitchMap$org$apache$commons$csv$QuoteMode:[I

    invoke-virtual {v6}, Lorg/apache/commons/csv/QuoteMode;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    .line 249
    new-instance v8, Ljava/lang/IllegalStateException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Unexpected Quote value: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 191
    :pswitch_0
    const/4 v4, 0x1

    .line 252
    :cond_1
    :goto_0
    if-nez v4, :cond_d

    .line 254
    iget-object v8, p0, Lorg/apache/commons/csv/CSVPrinter;->out:Ljava/lang/Appendable;

    invoke-interface {v8, p2, v7, v2}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;II)Ljava/lang/Appendable;

    .line 280
    :goto_1
    return-void

    .line 194
    :pswitch_1
    instance-of v8, p1, Ljava/lang/Number;

    if-nez v8, :cond_2

    const/4 v4, 0x1

    .line 195
    :goto_2
    goto :goto_0

    .line 194
    :cond_2
    const/4 v4, 0x0

    goto :goto_2

    .line 198
    :pswitch_2
    invoke-direct {p0, p2, p3, p4}, Lorg/apache/commons/csv/CSVPrinter;->printAndEscape(Ljava/lang/CharSequence;II)V

    goto :goto_1

    .line 201
    :pswitch_3
    if-gtz p4, :cond_4

    .line 206
    iget-boolean v8, p0, Lorg/apache/commons/csv/CSVPrinter;->newRecord:Z

    if-eqz v8, :cond_3

    .line 207
    const/4 v4, 0x1

    .line 242
    :cond_3
    :goto_3
    if-nez v4, :cond_1

    .line 244
    iget-object v8, p0, Lorg/apache/commons/csv/CSVPrinter;->out:Ljava/lang/Appendable;

    invoke-interface {v8, p2, v7, v2}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;II)Ljava/lang/Appendable;

    goto :goto_1

    .line 210
    :cond_4
    invoke-interface {p2, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 213
    .local v0, "c":C
    iget-boolean v8, p0, Lorg/apache/commons/csv/CSVPrinter;->newRecord:Z

    if-eqz v8, :cond_8

    const/16 v8, 0x30

    if-lt v0, v8, :cond_7

    const/16 v8, 0x39

    if-le v0, v8, :cond_5

    const/16 v8, 0x41

    if-lt v0, v8, :cond_7

    :cond_5
    const/16 v8, 0x5a

    if-le v0, v8, :cond_6

    const/16 v8, 0x61

    if-lt v0, v8, :cond_7

    :cond_6
    const/16 v8, 0x7a

    if-le v0, v8, :cond_8

    .line 214
    :cond_7
    const/4 v4, 0x1

    goto :goto_3

    .line 215
    :cond_8
    const/16 v8, 0x23

    if-gt v0, v8, :cond_a

    .line 219
    const/4 v4, 0x1

    goto :goto_3

    .line 227
    :cond_9
    add-int/lit8 v3, v3, 0x1

    .line 221
    :cond_a
    if-ge v3, v2, :cond_c

    .line 222
    invoke-interface {p2, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 223
    const/16 v8, 0xa

    if-eq v0, v8, :cond_b

    const/16 v8, 0xd

    if-eq v0, v8, :cond_b

    if-eq v0, v5, :cond_b

    if-ne v0, v1, :cond_9

    .line 224
    :cond_b
    const/4 v4, 0x1

    .line 230
    :cond_c
    if-nez v4, :cond_3

    .line 231
    add-int/lit8 v3, v2, -0x1

    .line 232
    invoke-interface {p2, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 235
    const/16 v8, 0x20

    if-gt v0, v8, :cond_3

    .line 236
    const/4 v4, 0x1

    goto :goto_3

    .line 259
    .end local v0    # "c":C
    :cond_d
    iget-object v8, p0, Lorg/apache/commons/csv/CSVPrinter;->out:Ljava/lang/Appendable;

    invoke-interface {v8, v5}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 263
    :goto_4
    if-ge v3, v2, :cond_f

    .line 264
    invoke-interface {p2, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 265
    .restart local v0    # "c":C
    if-ne v0, v5, :cond_e

    .line 269
    iget-object v8, p0, Lorg/apache/commons/csv/CSVPrinter;->out:Ljava/lang/Appendable;

    add-int/lit8 v9, v3, 0x1

    invoke-interface {v8, p2, v7, v9}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;II)Ljava/lang/Appendable;

    .line 272
    move v7, v3

    .line 274
    :cond_e
    add-int/lit8 v3, v3, 0x1

    .line 275
    goto :goto_4

    .line 278
    .end local v0    # "c":C
    :cond_f
    iget-object v8, p0, Lorg/apache/commons/csv/CSVPrinter;->out:Ljava/lang/Appendable;

    invoke-interface {v8, p2, v7, v3}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;II)Ljava/lang/Appendable;

    .line 279
    iget-object v8, p0, Lorg/apache/commons/csv/CSVPrinter;->out:Ljava/lang/Appendable;

    invoke-interface {v8, v5}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    goto/16 :goto_1

    .line 189
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lorg/apache/commons/csv/CSVPrinter;->out:Ljava/lang/Appendable;

    instance-of v0, v0, Ljava/io/Closeable;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lorg/apache/commons/csv/CSVPrinter;->out:Ljava/lang/Appendable;

    check-cast v0, Ljava/io/Closeable;

    invoke-interface {v0}, Ljava/io/Closeable;->close()V

    .line 82
    :cond_0
    return-void
.end method

.method public flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 91
    iget-object v0, p0, Lorg/apache/commons/csv/CSVPrinter;->out:Ljava/lang/Appendable;

    instance-of v0, v0, Ljava/io/Flushable;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lorg/apache/commons/csv/CSVPrinter;->out:Ljava/lang/Appendable;

    check-cast v0, Ljava/io/Flushable;

    invoke-interface {v0}, Ljava/io/Flushable;->flush()V

    .line 94
    :cond_0
    return-void
.end method

.method public getOut()Ljava/lang/Appendable;
    .locals 1

    .prologue
    .line 492
    iget-object v0, p0, Lorg/apache/commons/csv/CSVPrinter;->out:Ljava/lang/Appendable;

    return-object v0
.end method

.method public print(Ljava/lang/Object;)V
    .locals 4
    .param p1, "value"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 107
    if-nez p1, :cond_1

    .line 108
    iget-object v2, p0, Lorg/apache/commons/csv/CSVPrinter;->format:Lorg/apache/commons/csv/CSVFormat;

    invoke-virtual {v2}, Lorg/apache/commons/csv/CSVFormat;->getNullString()Ljava/lang/String;

    move-result-object v0

    .line 109
    .local v0, "nullString":Ljava/lang/String;
    if-nez v0, :cond_0

    const-string v1, ""

    .line 113
    .end local v0    # "nullString":Ljava/lang/String;
    .local v1, "strValue":Ljava/lang/String;
    :goto_0
    const/4 v2, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-direct {p0, p1, v1, v2, v3}, Lorg/apache/commons/csv/CSVPrinter;->print(Ljava/lang/Object;Ljava/lang/CharSequence;II)V

    .line 114
    return-void

    .end local v1    # "strValue":Ljava/lang/String;
    .restart local v0    # "nullString":Ljava/lang/String;
    :cond_0
    move-object v1, v0

    .line 109
    goto :goto_0

    .line 111
    .end local v0    # "nullString":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "strValue":Ljava/lang/String;
    goto :goto_0
.end method

.method public printComment(Ljava/lang/String;)V
    .locals 5
    .param p1, "comment"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x20

    .line 298
    iget-object v2, p0, Lorg/apache/commons/csv/CSVPrinter;->format:Lorg/apache/commons/csv/CSVFormat;

    invoke-virtual {v2}, Lorg/apache/commons/csv/CSVFormat;->isCommentMarkerSet()Z

    move-result v2

    if-nez v2, :cond_0

    .line 325
    :goto_0
    return-void

    .line 301
    :cond_0
    iget-boolean v2, p0, Lorg/apache/commons/csv/CSVPrinter;->newRecord:Z

    if-nez v2, :cond_1

    .line 302
    invoke-virtual {p0}, Lorg/apache/commons/csv/CSVPrinter;->println()V

    .line 304
    :cond_1
    iget-object v2, p0, Lorg/apache/commons/csv/CSVPrinter;->out:Ljava/lang/Appendable;

    iget-object v3, p0, Lorg/apache/commons/csv/CSVPrinter;->format:Lorg/apache/commons/csv/CSVFormat;

    invoke-virtual {v3}, Lorg/apache/commons/csv/CSVFormat;->getCommentMarker()Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Character;->charValue()C

    move-result v3

    invoke-interface {v2, v3}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 305
    iget-object v2, p0, Lorg/apache/commons/csv/CSVPrinter;->out:Ljava/lang/Appendable;

    invoke-interface {v2, v4}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 306
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 307
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 308
    .local v0, "c":C
    packed-switch v0, :pswitch_data_0

    .line 320
    :pswitch_0
    iget-object v2, p0, Lorg/apache/commons/csv/CSVPrinter;->out:Ljava/lang/Appendable;

    invoke-interface {v2, v0}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 306
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 310
    :pswitch_1
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v2, v3, :cond_2

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0xa

    if-ne v2, v3, :cond_2

    .line 311
    add-int/lit8 v1, v1, 0x1

    .line 315
    :cond_2
    :pswitch_2
    invoke-virtual {p0}, Lorg/apache/commons/csv/CSVPrinter;->println()V

    .line 316
    iget-object v2, p0, Lorg/apache/commons/csv/CSVPrinter;->out:Ljava/lang/Appendable;

    iget-object v3, p0, Lorg/apache/commons/csv/CSVPrinter;->format:Lorg/apache/commons/csv/CSVFormat;

    invoke-virtual {v3}, Lorg/apache/commons/csv/CSVFormat;->getCommentMarker()Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Character;->charValue()C

    move-result v3

    invoke-interface {v2, v3}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 317
    iget-object v2, p0, Lorg/apache/commons/csv/CSVPrinter;->out:Ljava/lang/Appendable;

    invoke-interface {v2, v4}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    goto :goto_2

    .line 324
    .end local v0    # "c":C
    :cond_3
    invoke-virtual {p0}, Lorg/apache/commons/csv/CSVPrinter;->println()V

    goto :goto_0

    .line 308
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public printRecord(Ljava/lang/Iterable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<*>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 355
    .local p1, "values":Ljava/lang/Iterable;, "Ljava/lang/Iterable<*>;"
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 356
    .local v0, "value":Ljava/lang/Object;
    invoke-virtual {p0, v0}, Lorg/apache/commons/csv/CSVPrinter;->print(Ljava/lang/Object;)V

    goto :goto_0

    .line 358
    .end local v0    # "value":Ljava/lang/Object;
    :cond_0
    invoke-virtual {p0}, Lorg/apache/commons/csv/CSVPrinter;->println()V

    .line 359
    return-void
.end method

.method public varargs printRecord([Ljava/lang/Object;)V
    .locals 3
    .param p1, "values"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 375
    array-length v2, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v0, p1, v1

    .line 376
    .local v0, "value":Ljava/lang/Object;
    invoke-virtual {p0, v0}, Lorg/apache/commons/csv/CSVPrinter;->print(Ljava/lang/Object;)V

    .line 375
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 378
    .end local v0    # "value":Ljava/lang/Object;
    :cond_0
    invoke-virtual {p0}, Lorg/apache/commons/csv/CSVPrinter;->println()V

    .line 379
    return-void
.end method

.method public printRecords(Ljava/lang/Iterable;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<*>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 413
    .local p1, "values":Ljava/lang/Iterable;, "Ljava/lang/Iterable<*>;"
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 414
    .local v0, "value":Ljava/lang/Object;
    instance-of v2, v0, [Ljava/lang/Object;

    if-eqz v2, :cond_0

    .line 415
    check-cast v0, [Ljava/lang/Object;

    .end local v0    # "value":Ljava/lang/Object;
    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lorg/apache/commons/csv/CSVPrinter;->printRecord([Ljava/lang/Object;)V

    goto :goto_0

    .line 416
    .restart local v0    # "value":Ljava/lang/Object;
    :cond_0
    instance-of v2, v0, Ljava/lang/Iterable;

    if-eqz v2, :cond_1

    .line 417
    check-cast v0, Ljava/lang/Iterable;

    .end local v0    # "value":Ljava/lang/Object;
    invoke-virtual {p0, v0}, Lorg/apache/commons/csv/CSVPrinter;->printRecord(Ljava/lang/Iterable;)V

    goto :goto_0

    .line 419
    .restart local v0    # "value":Ljava/lang/Object;
    :cond_1
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {p0, v2}, Lorg/apache/commons/csv/CSVPrinter;->printRecord([Ljava/lang/Object;)V

    goto :goto_0

    .line 422
    .end local v0    # "value":Ljava/lang/Object;
    :cond_2
    return-void
.end method

.method public printRecords(Ljava/sql/ResultSet;)V
    .locals 3
    .param p1, "resultSet"    # Ljava/sql/ResultSet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/sql/SQLException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 477
    invoke-interface {p1}, Ljava/sql/ResultSet;->getMetaData()Ljava/sql/ResultSetMetaData;

    move-result-object v2

    invoke-interface {v2}, Ljava/sql/ResultSetMetaData;->getColumnCount()I

    move-result v0

    .line 478
    .local v0, "columnCount":I
    :goto_0
    invoke-interface {p1}, Ljava/sql/ResultSet;->next()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 479
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_1
    if-gt v1, v0, :cond_0

    .line 480
    invoke-interface {p1, v1}, Ljava/sql/ResultSet;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/apache/commons/csv/CSVPrinter;->print(Ljava/lang/Object;)V

    .line 479
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 482
    :cond_0
    invoke-virtual {p0}, Lorg/apache/commons/csv/CSVPrinter;->println()V

    goto :goto_0

    .line 484
    .end local v1    # "i":I
    :cond_1
    return-void
.end method

.method public varargs printRecords([Ljava/lang/Object;)V
    .locals 5
    .param p1, "values"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 456
    array-length v3, p1

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v0, p1, v1

    .line 457
    .local v0, "value":Ljava/lang/Object;
    instance-of v4, v0, [Ljava/lang/Object;

    if-eqz v4, :cond_0

    .line 458
    check-cast v0, [Ljava/lang/Object;

    .end local v0    # "value":Ljava/lang/Object;
    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lorg/apache/commons/csv/CSVPrinter;->printRecord([Ljava/lang/Object;)V

    .line 456
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 459
    .restart local v0    # "value":Ljava/lang/Object;
    :cond_0
    instance-of v4, v0, Ljava/lang/Iterable;

    if-eqz v4, :cond_1

    .line 460
    check-cast v0, Ljava/lang/Iterable;

    .end local v0    # "value":Ljava/lang/Object;
    invoke-virtual {p0, v0}, Lorg/apache/commons/csv/CSVPrinter;->printRecord(Ljava/lang/Iterable;)V

    goto :goto_1

    .line 462
    .restart local v0    # "value":Ljava/lang/Object;
    :cond_1
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v2

    invoke-virtual {p0, v4}, Lorg/apache/commons/csv/CSVPrinter;->printRecord([Ljava/lang/Object;)V

    goto :goto_1

    .line 465
    .end local v0    # "value":Ljava/lang/Object;
    :cond_2
    return-void
.end method

.method public println()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 334
    iget-object v1, p0, Lorg/apache/commons/csv/CSVPrinter;->format:Lorg/apache/commons/csv/CSVFormat;

    invoke-virtual {v1}, Lorg/apache/commons/csv/CSVFormat;->getRecordSeparator()Ljava/lang/String;

    move-result-object v0

    .line 335
    .local v0, "recordSeparator":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 336
    iget-object v1, p0, Lorg/apache/commons/csv/CSVPrinter;->out:Ljava/lang/Appendable;

    invoke-interface {v1, v0}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 338
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/commons/csv/CSVPrinter;->newRecord:Z

    .line 339
    return-void
.end method
