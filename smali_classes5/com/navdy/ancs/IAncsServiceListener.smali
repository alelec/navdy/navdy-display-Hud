.class public interface abstract Lcom/navdy/ancs/IAncsServiceListener;
.super Ljava/lang/Object;
.source "IAncsServiceListener.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/ancs/IAncsServiceListener$Stub;
    }
.end annotation


# virtual methods
.method public abstract onConnectionStateChange(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract onNotification(Lcom/navdy/ancs/AppleNotification;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
