.class public Lcom/navdy/ancs/AppleNotification;
.super Ljava/lang/Object;
.source "AppleNotification.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final ACTION_ID_NEGATIVE:I = 0x1

.field public static final ACTION_ID_POSITIVE:I = 0x0

.field public static final ATTRIBUTE_APP_IDENTIFIER:I = 0x0

.field public static final ATTRIBUTE_DATE:I = 0x5

.field public static final ATTRIBUTE_MESSAGE:I = 0x3

.field public static final ATTRIBUTE_MESSAGE_SIZE:I = 0x4

.field public static final ATTRIBUTE_NEGATIVE_ACTION_LABEL:I = 0x7

.field public static final ATTRIBUTE_POSITIVE_ACTION_LABEL:I = 0x6

.field public static final ATTRIBUTE_SUBTITLE:I = 0x2

.field public static final ATTRIBUTE_TITLE:I = 0x1

.field public static final CATEGORY_BUSINESS_AND_FINANCE:I = 0x9

.field public static final CATEGORY_EMAIL:I = 0x6

.field public static final CATEGORY_ENTERTAINMENT:I = 0xb

.field public static final CATEGORY_HEALTH_AND_FITNESS:I = 0x8

.field public static final CATEGORY_INCOMING_CALL:I = 0x1

.field public static final CATEGORY_LOCATION:I = 0xa

.field public static final CATEGORY_MISSED_CALL:I = 0x2

.field public static final CATEGORY_NEWS:I = 0x7

.field public static final CATEGORY_OTHER:I = 0x0

.field public static final CATEGORY_SCHEDULE:I = 0x5

.field public static final CATEGORY_SOCIAL:I = 0x4

.field public static final CATEGORY_VOICE_MAIL:I = 0x3

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/navdy/ancs/AppleNotification;",
            ">;"
        }
    .end annotation
.end field

.field public static final EVENT_ADDED:I = 0x0

.field static final EVENT_FLAG_IMPORTANT:I = 0x2

.field static final EVENT_FLAG_NEGATIVE_ACTION:I = 0x10

.field static final EVENT_FLAG_POSITIVE_ACTION:I = 0x8

.field static final EVENT_FLAG_PRE_EXISTING:I = 0x4

.field static final EVENT_FLAG_SILENT:I = 0x1

.field public static final EVENT_MODIFIED:I = 0x1

.field public static final EVENT_REMOVED:I = 0x2

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private appId:Ljava/lang/String;

.field private appName:Ljava/lang/String;

.field private categoryCount:I

.field private categoryId:I

.field private date:Ljava/util/Date;

.field private eventFlags:I

.field private eventId:I

.field private message:Ljava/lang/String;

.field private negativeActionLabel:Ljava/lang/String;

.field private notificationUid:I

.field private positiveActionLabel:Ljava/lang/String;

.field private subTitle:Ljava/lang/String;

.field private title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/navdy/ancs/AppleNotification;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/ancs/AppleNotification;->TAG:Ljava/lang/String;

    .line 341
    new-instance v0, Lcom/navdy/ancs/AppleNotification$1;

    invoke-direct {v0}, Lcom/navdy/ancs/AppleNotification$1;-><init>()V

    sput-object v0, Lcom/navdy/ancs/AppleNotification;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IIIII)V
    .locals 0
    .param p1, "eventId"    # I
    .param p2, "eventFlags"    # I
    .param p3, "categoryId"    # I
    .param p4, "categoryCount"    # I
    .param p5, "notificationUid"    # I

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput p1, p0, Lcom/navdy/ancs/AppleNotification;->eventId:I

    .line 81
    iput p3, p0, Lcom/navdy/ancs/AppleNotification;->categoryId:I

    .line 82
    iput p4, p0, Lcom/navdy/ancs/AppleNotification;->categoryCount:I

    .line 83
    iput p2, p0, Lcom/navdy/ancs/AppleNotification;->eventFlags:I

    .line 84
    iput p5, p0, Lcom/navdy/ancs/AppleNotification;->notificationUid:I

    .line 85
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 8
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 308
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/ancs/AppleNotification;-><init>(IIIII)V

    .line 310
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/navdy/ancs/AppleNotification;->setTitle(Ljava/lang/String;)V

    .line 311
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/navdy/ancs/AppleNotification;->setSubTitle(Ljava/lang/String;)V

    .line 312
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/navdy/ancs/AppleNotification;->setMessage(Ljava/lang/String;)V

    .line 313
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    .line 314
    .local v6, "dateMs":J
    const-wide/16 v0, 0x0

    cmp-long v0, v6, v0

    if-eqz v0, :cond_0

    .line 315
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {p0, v0}, Lcom/navdy/ancs/AppleNotification;->setDate(Ljava/util/Date;)V

    .line 317
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/navdy/ancs/AppleNotification;->setAppId(Ljava/lang/String;)V

    .line 318
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/navdy/ancs/AppleNotification;->setPositiveActionLabel(Ljava/lang/String;)V

    .line 319
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/navdy/ancs/AppleNotification;->setNegativeActionLabel(Ljava/lang/String;)V

    .line 320
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/navdy/ancs/AppleNotification;->setAppName(Ljava/lang/String;)V

    .line 321
    return-void
.end method

.method public static byteToInt(B)I
    .locals 1
    .param p0, "b"    # B

    .prologue
    .line 282
    and-int/lit16 v0, p0, 0xff

    return v0
.end method

.method public static bytesToInt(BBBB)I
    .locals 2
    .param p0, "b0"    # B
    .param p1, "b1"    # B
    .param p2, "b2"    # B
    .param p3, "b3"    # B

    .prologue
    .line 294
    invoke-static {p0}, Lcom/navdy/ancs/AppleNotification;->byteToInt(B)I

    move-result v0

    invoke-static {p1}, Lcom/navdy/ancs/AppleNotification;->byteToInt(B)I

    move-result v1

    shl-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    invoke-static {p2}, Lcom/navdy/ancs/AppleNotification;->byteToInt(B)I

    move-result v1

    shl-int/lit8 v1, v1, 0x10

    add-int/2addr v0, v1

    invoke-static {p3}, Lcom/navdy/ancs/AppleNotification;->byteToInt(B)I

    move-result v1

    shl-int/lit8 v1, v1, 0x18

    add-int/2addr v0, v1

    return v0
.end method

.method public static bytesToInt([BI)I
    .locals 4
    .param p0, "bytes"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 298
    aget-byte v0, p0, p1

    add-int/lit8 v1, p1, 0x1

    aget-byte v1, p0, v1

    add-int/lit8 v2, p1, 0x2

    aget-byte v2, p0, v2

    add-int/lit8 v3, p1, 0x3

    aget-byte v3, p0, v3

    invoke-static {v0, v1, v2, v3}, Lcom/navdy/ancs/AppleNotification;->bytesToInt(BBBB)I

    move-result v0

    return v0
.end method

.method public static bytesToShort(BB)I
    .locals 2
    .param p0, "b0"    # B
    .param p1, "b1"    # B

    .prologue
    .line 286
    invoke-static {p0}, Lcom/navdy/ancs/AppleNotification;->byteToInt(B)I

    move-result v0

    invoke-static {p1}, Lcom/navdy/ancs/AppleNotification;->byteToInt(B)I

    move-result v1

    shl-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    return v0
.end method

.method public static bytesToShort([BI)I
    .locals 2
    .param p0, "bytes"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 290
    aget-byte v0, p0, p1

    add-int/lit8 v1, p1, 0x1

    aget-byte v1, p0, v1

    invoke-static {v0, v1}, Lcom/navdy/ancs/AppleNotification;->bytesToShort(BB)I

    move-result v0

    return v0
.end method

.method public static parse([B)Lcom/navdy/ancs/AppleNotification;
    .locals 6
    .param p0, "bytes"    # [B

    .prologue
    .line 71
    const/4 v0, 0x0

    aget-byte v0, p0, v0

    and-int/lit16 v1, v0, 0xff

    .line 72
    .local v1, "eventId":I
    const/4 v0, 0x1

    aget-byte v0, p0, v0

    and-int/lit16 v2, v0, 0xff

    .line 73
    .local v2, "eventFlags":I
    const/4 v0, 0x2

    aget-byte v0, p0, v0

    and-int/lit16 v3, v0, 0xff

    .line 74
    .local v3, "categoryId":I
    const/4 v0, 0x3

    aget-byte v0, p0, v0

    and-int/lit16 v4, v0, 0xff

    .line 75
    .local v4, "categoryCount":I
    const/4 v0, 0x4

    invoke-static {p0, v0}, Lcom/navdy/ancs/AppleNotification;->bytesToInt([BI)I

    move-result v5

    .line 76
    .local v5, "notificationUid":I
    new-instance v0, Lcom/navdy/ancs/AppleNotification;

    invoke-direct/range {v0 .. v5}, Lcom/navdy/ancs/AppleNotification;-><init>(IIIII)V

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 303
    const/4 v0, 0x0

    return v0
.end method

.method public getAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/navdy/ancs/AppleNotification;->appId:Ljava/lang/String;

    return-object v0
.end method

.method public getAppName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/navdy/ancs/AppleNotification;->appName:Ljava/lang/String;

    return-object v0
.end method

.method public getCategoryCount()I
    .locals 1

    .prologue
    .line 116
    iget v0, p0, Lcom/navdy/ancs/AppleNotification;->categoryCount:I

    return v0
.end method

.method public getCategoryId()I
    .locals 1

    .prologue
    .line 112
    iget v0, p0, Lcom/navdy/ancs/AppleNotification;->categoryId:I

    return v0
.end method

.method public getDate()Ljava/util/Date;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/navdy/ancs/AppleNotification;->date:Ljava/util/Date;

    return-object v0
.end method

.method public getEventFlags()I
    .locals 1

    .prologue
    .line 120
    iget v0, p0, Lcom/navdy/ancs/AppleNotification;->eventFlags:I

    return v0
.end method

.method public getEventId()I
    .locals 1

    .prologue
    .line 108
    iget v0, p0, Lcom/navdy/ancs/AppleNotification;->eventId:I

    return v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/navdy/ancs/AppleNotification;->message:Ljava/lang/String;

    return-object v0
.end method

.method public getNegativeActionLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/navdy/ancs/AppleNotification;->negativeActionLabel:Ljava/lang/String;

    return-object v0
.end method

.method public getNotificationUid()I
    .locals 1

    .prologue
    .line 124
    iget v0, p0, Lcom/navdy/ancs/AppleNotification;->notificationUid:I

    return v0
.end method

.method public getPositiveActionLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/navdy/ancs/AppleNotification;->positiveActionLabel:Ljava/lang/String;

    return-object v0
.end method

.method public getSubTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/navdy/ancs/AppleNotification;->subTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/navdy/ancs/AppleNotification;->title:Ljava/lang/String;

    return-object v0
.end method

.method public hasNegativeAction()Z
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lcom/navdy/ancs/AppleNotification;->eventFlags:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPositiveAction()Z
    .locals 1

    .prologue
    .line 100
    iget v0, p0, Lcom/navdy/ancs/AppleNotification;->eventFlags:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isImportant()Z
    .locals 1

    .prologue
    .line 92
    iget v0, p0, Lcom/navdy/ancs/AppleNotification;->eventFlags:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPreExisting()Z
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lcom/navdy/ancs/AppleNotification;->eventFlags:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSilent()Z
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lcom/navdy/ancs/AppleNotification;->eventFlags:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAppId(Ljava/lang/String;)V
    .locals 0
    .param p1, "appId"    # Ljava/lang/String;

    .prologue
    .line 164
    iput-object p1, p0, Lcom/navdy/ancs/AppleNotification;->appId:Ljava/lang/String;

    .line 165
    return-void
.end method

.method public setAppName(Ljava/lang/String;)V
    .locals 0
    .param p1, "appName"    # Ljava/lang/String;

    .prologue
    .line 188
    iput-object p1, p0, Lcom/navdy/ancs/AppleNotification;->appName:Ljava/lang/String;

    .line 189
    return-void
.end method

.method public setAttribute(ILjava/lang/String;)V
    .locals 6
    .param p1, "attributeId"    # I
    .param p2, "attributeValue"    # Ljava/lang/String;

    .prologue
    .line 192
    packed-switch p1, :pswitch_data_0

    .line 222
    :pswitch_0
    sget-object v3, Lcom/navdy/ancs/AppleNotification;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unhandled attribute - id:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " val:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    :goto_0
    return-void

    .line 194
    :pswitch_1
    invoke-virtual {p0, p2}, Lcom/navdy/ancs/AppleNotification;->setTitle(Ljava/lang/String;)V

    goto :goto_0

    .line 197
    :pswitch_2
    invoke-virtual {p0, p2}, Lcom/navdy/ancs/AppleNotification;->setSubTitle(Ljava/lang/String;)V

    goto :goto_0

    .line 200
    :pswitch_3
    invoke-virtual {p0, p2}, Lcom/navdy/ancs/AppleNotification;->setMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 203
    :pswitch_4
    invoke-virtual {p0, p2}, Lcom/navdy/ancs/AppleNotification;->setAppId(Ljava/lang/String;)V

    goto :goto_0

    .line 207
    :pswitch_5
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyyMMdd\'T\'HHmmSS"

    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-direct {v1, v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 209
    .local v1, "df":Ljava/text/DateFormat;
    :try_start_0
    invoke-virtual {v1, p2}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 210
    .local v0, "date":Ljava/util/Date;
    invoke-virtual {p0, v0}, Lcom/navdy/ancs/AppleNotification;->setDate(Ljava/util/Date;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 211
    .end local v0    # "date":Ljava/util/Date;
    :catch_0
    move-exception v2

    .line 212
    .local v2, "e":Ljava/text/ParseException;
    sget-object v3, Lcom/navdy/ancs/AppleNotification;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unable to parse date - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 216
    .end local v1    # "df":Ljava/text/DateFormat;
    .end local v2    # "e":Ljava/text/ParseException;
    :pswitch_6
    invoke-virtual {p0, p2}, Lcom/navdy/ancs/AppleNotification;->setPositiveActionLabel(Ljava/lang/String;)V

    goto :goto_0

    .line 219
    :pswitch_7
    invoke-virtual {p0, p2}, Lcom/navdy/ancs/AppleNotification;->setNegativeActionLabel(Ljava/lang/String;)V

    goto :goto_0

    .line 192
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public setDate(Ljava/util/Date;)V
    .locals 0
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 156
    iput-object p1, p0, Lcom/navdy/ancs/AppleNotification;->date:Ljava/util/Date;

    .line 157
    return-void
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/navdy/ancs/AppleNotification;->message:Ljava/lang/String;

    .line 149
    return-void
.end method

.method public setNegativeActionLabel(Ljava/lang/String;)V
    .locals 0
    .param p1, "negativeActionLabel"    # Ljava/lang/String;

    .prologue
    .line 180
    iput-object p1, p0, Lcom/navdy/ancs/AppleNotification;->negativeActionLabel:Ljava/lang/String;

    .line 181
    return-void
.end method

.method public setPositiveActionLabel(Ljava/lang/String;)V
    .locals 0
    .param p1, "positiveActionLabel"    # Ljava/lang/String;

    .prologue
    .line 172
    iput-object p1, p0, Lcom/navdy/ancs/AppleNotification;->positiveActionLabel:Ljava/lang/String;

    .line 173
    return-void
.end method

.method public setSubTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "subTitle"    # Ljava/lang/String;

    .prologue
    .line 140
    iput-object p1, p0, Lcom/navdy/ancs/AppleNotification;->subTitle:Ljava/lang/String;

    .line 141
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 132
    iput-object p1, p0, Lcom/navdy/ancs/AppleNotification;->title:Ljava/lang/String;

    .line 133
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0x27

    .line 229
    new-instance v0, Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Notification{eventId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/navdy/ancs/AppleNotification;->eventId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", categoryId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/navdy/ancs/AppleNotification;->categoryId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", categoryCount="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/navdy/ancs/AppleNotification;->categoryCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", eventFlags="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/navdy/ancs/AppleNotification;->eventFlags:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", notificationUid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/navdy/ancs/AppleNotification;->notificationUid:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 237
    .local v0, "builder":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lcom/navdy/ancs/AppleNotification;->appId:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 238
    const-string v1, ", appId=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 239
    iget-object v1, p0, Lcom/navdy/ancs/AppleNotification;->appId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 240
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 242
    :cond_0
    iget-object v1, p0, Lcom/navdy/ancs/AppleNotification;->appName:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 243
    const-string v1, ", appName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 244
    iget-object v1, p0, Lcom/navdy/ancs/AppleNotification;->appName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 245
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 247
    :cond_1
    iget-object v1, p0, Lcom/navdy/ancs/AppleNotification;->title:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 248
    const-string v1, ", title=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 249
    iget-object v1, p0, Lcom/navdy/ancs/AppleNotification;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 250
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 252
    :cond_2
    iget-object v1, p0, Lcom/navdy/ancs/AppleNotification;->subTitle:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 253
    const-string v1, ", subTitle=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 254
    iget-object v1, p0, Lcom/navdy/ancs/AppleNotification;->subTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 255
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 257
    :cond_3
    iget-object v1, p0, Lcom/navdy/ancs/AppleNotification;->message:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 258
    const-string v1, ", message=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 259
    iget-object v1, p0, Lcom/navdy/ancs/AppleNotification;->message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 260
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 262
    :cond_4
    iget-object v1, p0, Lcom/navdy/ancs/AppleNotification;->positiveActionLabel:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 263
    const-string v1, ", positiveAction=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 264
    iget-object v1, p0, Lcom/navdy/ancs/AppleNotification;->positiveActionLabel:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 265
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 267
    :cond_5
    iget-object v1, p0, Lcom/navdy/ancs/AppleNotification;->negativeActionLabel:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 268
    const-string v1, ", negativeAction=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 269
    iget-object v1, p0, Lcom/navdy/ancs/AppleNotification;->negativeActionLabel:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 272
    :cond_6
    iget-object v1, p0, Lcom/navdy/ancs/AppleNotification;->date:Ljava/util/Date;

    if-eqz v1, :cond_7

    .line 273
    const-string v1, ", date=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 274
    iget-object v1, p0, Lcom/navdy/ancs/AppleNotification;->date:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 275
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 277
    :cond_7
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 278
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 325
    iget v0, p0, Lcom/navdy/ancs/AppleNotification;->eventId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 326
    iget v0, p0, Lcom/navdy/ancs/AppleNotification;->eventFlags:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 327
    iget v0, p0, Lcom/navdy/ancs/AppleNotification;->categoryId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 328
    iget v0, p0, Lcom/navdy/ancs/AppleNotification;->categoryCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 329
    iget v0, p0, Lcom/navdy/ancs/AppleNotification;->notificationUid:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 331
    iget-object v0, p0, Lcom/navdy/ancs/AppleNotification;->title:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 332
    iget-object v0, p0, Lcom/navdy/ancs/AppleNotification;->subTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 333
    iget-object v0, p0, Lcom/navdy/ancs/AppleNotification;->message:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 334
    iget-object v0, p0, Lcom/navdy/ancs/AppleNotification;->date:Ljava/util/Date;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/ancs/AppleNotification;->date:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    :goto_0
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 335
    iget-object v0, p0, Lcom/navdy/ancs/AppleNotification;->appId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 336
    iget-object v0, p0, Lcom/navdy/ancs/AppleNotification;->positiveActionLabel:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 337
    iget-object v0, p0, Lcom/navdy/ancs/AppleNotification;->negativeActionLabel:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 338
    iget-object v0, p0, Lcom/navdy/ancs/AppleNotification;->appName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 339
    return-void

    .line 334
    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method
