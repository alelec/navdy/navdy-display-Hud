#!/bin/bash
# Set version 
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
pushd "$DIR" > /dev/null

export VNUM=${1}
export VERS=1.3.${1}-$(git rev-parse --short HEAD)
echo $VERS

sed -r -i "s#versionCode: .*#versionCode: \x27${VNUM}\x27#" apktool.yml
sed -r -i "s#versionName: .*#versionName: ${VERS}#" apktool.yml
dos2unix apktool.yml

sed -r -i "s#VERSION_NAME:Ljava/lang/String; = .*#VERSION_NAME:Ljava/lang/String; = \"${VERS}\"#" smali_classes30/com/navdy/hud/app/BuildConfig.smali
dos2unix smali_classes30/com/navdy/hud/app/BuildConfig.smali

perl -0777 -i -pe "s#(  \.line 404\r?\n +const-string v1, )\".*#\1\"${VERS}\"#" smali_classes30/com/navdy/hud/app/service/ConnectionHandler.smali
perl -0777 -i -pe "s#(const-string v3, )\"\S+\"(\r?\n\s*\r?\n\s*.line 368)#\1\"${VERS}\"\2#" smali_classes30/com/navdy/hud/app/service/ConnectionHandler.smali
dos2unix smali_classes30/com/navdy/hud/app/service/ConnectionHandler.smali

perl -0777 -i -pe "s#(  \.line 459\r?\n +const-string v5, )\".*#\1\"${VERS}\"#" smali_classes30/com/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu.smali
dos2unix smali_classes30/com/navdy/hud/app/ui/component/mainmenu/SystemInfoMenu.smali

perl -0777 -i -pe "s#(  \.line 561\r?\n +const-string v0, )\".*#\1\"${VERS}\"#" smali_classes30/com/navdy/hud/app/util/OTAUpdateService.smali
dos2unix smali_classes30/com/navdy/hud/app/util/OTAUpdateService.smali

perl -0777 -i -pe "s#(  \.line 870\r?\n +const-string v47, \"HUD app version:).*#\1 ${VERS}\\\\n\"#" smali_classes30/com/navdy/hud/app/util/ReportIssueService.smali
dos2unix smali_classes30/com/navdy/hud/app/util/ReportIssueService.smali
