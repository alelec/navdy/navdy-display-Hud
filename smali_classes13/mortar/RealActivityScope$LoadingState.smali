.class final enum Lmortar/RealActivityScope$LoadingState;
.super Ljava/lang/Enum;
.source "RealActivityScope.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmortar/RealActivityScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "LoadingState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lmortar/RealActivityScope$LoadingState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lmortar/RealActivityScope$LoadingState;

.field public static final enum IDLE:Lmortar/RealActivityScope$LoadingState;

.field public static final enum LOADING:Lmortar/RealActivityScope$LoadingState;

.field public static final enum SAVING:Lmortar/RealActivityScope$LoadingState;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 30
    new-instance v0, Lmortar/RealActivityScope$LoadingState;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v2}, Lmortar/RealActivityScope$LoadingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmortar/RealActivityScope$LoadingState;->IDLE:Lmortar/RealActivityScope$LoadingState;

    new-instance v0, Lmortar/RealActivityScope$LoadingState;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v3}, Lmortar/RealActivityScope$LoadingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmortar/RealActivityScope$LoadingState;->LOADING:Lmortar/RealActivityScope$LoadingState;

    new-instance v0, Lmortar/RealActivityScope$LoadingState;

    const-string v1, "SAVING"

    invoke-direct {v0, v1, v4}, Lmortar/RealActivityScope$LoadingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmortar/RealActivityScope$LoadingState;->SAVING:Lmortar/RealActivityScope$LoadingState;

    .line 29
    const/4 v0, 0x3

    new-array v0, v0, [Lmortar/RealActivityScope$LoadingState;

    sget-object v1, Lmortar/RealActivityScope$LoadingState;->IDLE:Lmortar/RealActivityScope$LoadingState;

    aput-object v1, v0, v2

    sget-object v1, Lmortar/RealActivityScope$LoadingState;->LOADING:Lmortar/RealActivityScope$LoadingState;

    aput-object v1, v0, v3

    sget-object v1, Lmortar/RealActivityScope$LoadingState;->SAVING:Lmortar/RealActivityScope$LoadingState;

    aput-object v1, v0, v4

    sput-object v0, Lmortar/RealActivityScope$LoadingState;->$VALUES:[Lmortar/RealActivityScope$LoadingState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmortar/RealActivityScope$LoadingState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 29
    const-class v0, Lmortar/RealActivityScope$LoadingState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmortar/RealActivityScope$LoadingState;

    return-object v0
.end method

.method public static values()[Lmortar/RealActivityScope$LoadingState;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lmortar/RealActivityScope$LoadingState;->$VALUES:[Lmortar/RealActivityScope$LoadingState;

    invoke-virtual {v0}, [Lmortar/RealActivityScope$LoadingState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmortar/RealActivityScope$LoadingState;

    return-object v0
.end method
