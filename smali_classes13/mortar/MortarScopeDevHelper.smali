.class public Lmortar/MortarScopeDevHelper;
.super Ljava/lang/Object;
.source "MortarScopeDevHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmortar/MortarScopeDevHelper$1;,
        Lmortar/MortarScopeDevHelper$NodeSorter;,
        Lmortar/MortarScopeDevHelper$InjectNode;,
        Lmortar/MortarScopeDevHelper$ModuleNode;,
        Lmortar/MortarScopeDevHelper$MortarScopeNode;,
        Lmortar/MortarScopeDevHelper$Node;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 222
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 223
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This is a helper class"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static appendLinePrefix(Ljava/lang/StringBuilder;IJ)V
    .locals 10
    .param p0, "result"    # Ljava/lang/StringBuilder;
    .param p1, "depth"    # I
    .param p2, "lastChildMask"    # J

    .prologue
    const/16 v8, 0x20

    const/4 v3, 0x1

    .line 195
    add-int/lit8 v1, p1, -0x1

    .line 197
    .local v1, "lastDepth":I
    const/16 v4, 0xa0

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 198
    const/4 v2, 0x0

    .local v2, "parentDepth":I
    :goto_0
    if-gt v2, v1, :cond_5

    .line 199
    if-lez v2, :cond_0

    .line 200
    invoke-virtual {p0, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 202
    :cond_0
    shl-int v4, v3, v2

    int-to-long v4, v4

    and-long/2addr v4, p2

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_1

    move v0, v3

    .line 203
    .local v0, "lastChild":Z
    :goto_1
    if-eqz v0, :cond_3

    .line 204
    if-ne v2, v1, :cond_2

    .line 205
    const/16 v4, 0x60

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 198
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 202
    .end local v0    # "lastChild":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 207
    .restart local v0    # "lastChild":Z
    :cond_2
    invoke-virtual {p0, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 210
    :cond_3
    if-ne v2, v1, :cond_4

    .line 211
    const/16 v4, 0x2b

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 213
    :cond_4
    const/16 v4, 0x7c

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 217
    .end local v0    # "lastChild":Z
    :cond_5
    if-lez p1, :cond_6

    .line 218
    const-string v3, "-"

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 220
    :cond_6
    return-void
.end method

.method private static getMortarScope(Lmortar/MortarScope;)Lmortar/MortarScope;
    .locals 2
    .param p0, "mortarScope"    # Lmortar/MortarScope;

    .prologue
    .line 165
    instance-of v1, p0, Lmortar/RealScope;

    if-nez v1, :cond_0

    .line 172
    .end local p0    # "mortarScope":Lmortar/MortarScope;
    :goto_0
    return-object p0

    .restart local p0    # "mortarScope":Lmortar/MortarScope;
    :cond_0
    move-object v0, p0

    .line 168
    check-cast v0, Lmortar/RealScope;

    .line 169
    .local v0, "scope":Lmortar/RealScope;
    :goto_1
    invoke-virtual {v0}, Lmortar/RealScope;->getParent()Lmortar/RealScope;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 170
    invoke-virtual {v0}, Lmortar/RealScope;->getParent()Lmortar/RealScope;

    move-result-object v0

    goto :goto_1

    :cond_1
    move-object p0, v0

    .line 172
    goto :goto_0
.end method

.method private static nodeHierarchyToString(Ljava/lang/StringBuilder;IJLmortar/MortarScopeDevHelper$Node;)V
    .locals 8
    .param p0, "result"    # Ljava/lang/StringBuilder;
    .param p1, "depth"    # I
    .param p2, "lastChildMask"    # J
    .param p4, "node"    # Lmortar/MortarScopeDevHelper$Node;

    .prologue
    .line 177
    invoke-static {p0, p1, p2, p3}, Lmortar/MortarScopeDevHelper;->appendLinePrefix(Ljava/lang/StringBuilder;IJ)V

    .line 178
    invoke-interface {p4}, Lmortar/MortarScopeDevHelper$Node;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v6, 0xa

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 180
    invoke-interface {p4}, Lmortar/MortarScopeDevHelper$Node;->getChildNodes()Ljava/util/List;

    move-result-object v1

    .line 181
    .local v1, "childNodes":Ljava/util/List;, "Ljava/util/List<Lmortar/MortarScopeDevHelper$Node;>;"
    new-instance v5, Lmortar/MortarScopeDevHelper$NodeSorter;

    const/4 v6, 0x0

    invoke-direct {v5, v6}, Lmortar/MortarScopeDevHelper$NodeSorter;-><init>(Lmortar/MortarScopeDevHelper$1;)V

    invoke-static {v1, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 183
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v4, v5, -0x1

    .line 184
    .local v4, "lastIndex":I
    const/4 v3, 0x0

    .line 185
    .local v3, "index":I
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmortar/MortarScopeDevHelper$Node;

    .line 186
    .local v0, "childNode":Lmortar/MortarScopeDevHelper$Node;
    if-ne v3, v4, :cond_0

    .line 187
    const/4 v5, 0x1

    shl-int/2addr v5, p1

    int-to-long v6, v5

    or-long/2addr p2, v6

    .line 189
    :cond_0
    add-int/lit8 v5, p1, 0x1

    invoke-static {p0, v5, p2, p3, v0}, Lmortar/MortarScopeDevHelper;->nodeHierarchyToString(Ljava/lang/StringBuilder;IJLmortar/MortarScopeDevHelper$Node;)V

    .line 190
    add-int/lit8 v3, v3, 0x1

    .line 191
    goto :goto_0

    .line 192
    .end local v0    # "childNode":Lmortar/MortarScopeDevHelper$Node;
    :cond_1
    return-void
.end method

.method public static scopeHierarchyToString(Lmortar/MortarScope;)Ljava/lang/String;
    .locals 6
    .param p0, "mortarScope"    # Lmortar/MortarScope;

    .prologue
    .line 22
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Mortar Hierarchy:\n"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 23
    .local v1, "result":Ljava/lang/StringBuilder;
    invoke-static {p0}, Lmortar/MortarScopeDevHelper;->getMortarScope(Lmortar/MortarScope;)Lmortar/MortarScope;

    move-result-object v0

    .line 24
    .local v0, "MortarScope":Lmortar/MortarScope;
    new-instance v2, Lmortar/MortarScopeDevHelper$MortarScopeNode;

    invoke-direct {v2, v0}, Lmortar/MortarScopeDevHelper$MortarScopeNode;-><init>(Lmortar/MortarScope;)V

    .line 25
    .local v2, "rootNode":Lmortar/MortarScopeDevHelper$Node;
    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    invoke-static {v1, v3, v4, v5, v2}, Lmortar/MortarScopeDevHelper;->nodeHierarchyToString(Ljava/lang/StringBuilder;IJLmortar/MortarScopeDevHelper$Node;)V

    .line 26
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method
