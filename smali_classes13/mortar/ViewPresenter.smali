.class public Lmortar/ViewPresenter;
.super Lmortar/Presenter;
.source "ViewPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ">",
        "Lmortar/Presenter",
        "<TV;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    .local p0, "this":Lmortar/ViewPresenter;, "Lmortar/ViewPresenter<TV;>;"
    invoke-direct {p0}, Lmortar/Presenter;-><init>()V

    return-void
.end method


# virtual methods
.method protected final extractScope(Landroid/view/View;)Lmortar/MortarScope;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)",
            "Lmortar/MortarScope;"
        }
    .end annotation

    .prologue
    .line 22
    .local p0, "this":Lmortar/ViewPresenter;, "Lmortar/ViewPresenter<TV;>;"
    .local p1, "view":Landroid/view/View;, "TV;"
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lmortar/Mortar;->getScope(Landroid/content/Context;)Lmortar/MortarScope;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic extractScope(Ljava/lang/Object;)Lmortar/MortarScope;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 20
    .local p0, "this":Lmortar/ViewPresenter;, "Lmortar/ViewPresenter<TV;>;"
    check-cast p1, Landroid/view/View;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lmortar/ViewPresenter;->extractScope(Landroid/view/View;)Lmortar/MortarScope;

    move-result-object v0

    return-object v0
.end method
