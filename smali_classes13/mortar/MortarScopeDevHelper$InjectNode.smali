.class Lmortar/MortarScopeDevHelper$InjectNode;
.super Ljava/lang/Object;
.source "MortarScopeDevHelper.java"

# interfaces
.implements Lmortar/MortarScopeDevHelper$Node;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmortar/MortarScopeDevHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "InjectNode"
.end annotation


# static fields
.field private static final MEMBER_PREFIX:I


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 146
    const-string v0, "members/"

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    sput v0, Lmortar/MortarScopeDevHelper$InjectNode;->MEMBER_PREFIX:I

    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "moduleMember"    # Ljava/lang/String;

    .prologue
    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "INJECT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lmortar/MortarScopeDevHelper$InjectNode;->MEMBER_PREFIX:I

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmortar/MortarScopeDevHelper$InjectNode;->name:Ljava/lang/String;

    .line 153
    return-void
.end method


# virtual methods
.method public getChildNodes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lmortar/MortarScopeDevHelper$Node;",
            ">;"
        }
    .end annotation

    .prologue
    .line 160
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lmortar/MortarScopeDevHelper$InjectNode;->name:Ljava/lang/String;

    return-object v0
.end method
