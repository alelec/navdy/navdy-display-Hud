.class Lmortar/MortarContextWrapper;
.super Landroid/content/ContextWrapper;
.source "MortarContextWrapper.java"


# instance fields
.field private inflater:Landroid/view/LayoutInflater;

.field private final scope:Lmortar/MortarScope;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lmortar/MortarScope;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "scope"    # Lmortar/MortarScope;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    .line 29
    iput-object p2, p0, Lmortar/MortarContextWrapper;->scope:Lmortar/MortarScope;

    .line 30
    return-void
.end method


# virtual methods
.method public getSystemService(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-static {p1}, Lmortar/Mortar;->isScopeSystemService(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p0, Lmortar/MortarContextWrapper;->scope:Lmortar/MortarScope;

    .line 42
    :goto_0
    return-object v0

    .line 36
    :cond_0
    const-string v0, "layout_inflater"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 37
    iget-object v0, p0, Lmortar/MortarContextWrapper;->inflater:Landroid/view/LayoutInflater;

    if-nez v0, :cond_1

    .line 38
    invoke-virtual {p0}, Lmortar/MortarContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lmortar/MortarContextWrapper;->inflater:Landroid/view/LayoutInflater;

    .line 40
    :cond_1
    iget-object v0, p0, Lmortar/MortarContextWrapper;->inflater:Landroid/view/LayoutInflater;

    goto :goto_0

    .line 42
    :cond_2
    invoke-super {p0, p1}, Landroid/content/ContextWrapper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method
