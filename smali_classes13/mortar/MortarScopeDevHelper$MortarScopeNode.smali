.class Lmortar/MortarScopeDevHelper$MortarScopeNode;
.super Ljava/lang/Object;
.source "MortarScopeDevHelper.java"

# interfaces
.implements Lmortar/MortarScopeDevHelper$Node;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmortar/MortarScopeDevHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MortarScopeNode"
.end annotation


# instance fields
.field private final mortarScope:Lmortar/MortarScope;


# direct methods
.method constructor <init>(Lmortar/MortarScope;)V
    .locals 0
    .param p1, "mortarScope"    # Lmortar/MortarScope;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lmortar/MortarScopeDevHelper$MortarScopeNode;->mortarScope:Lmortar/MortarScope;

    .line 41
    return-void
.end method

.method private addScopeChildren(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lmortar/MortarScopeDevHelper$Node;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 55
    .local p1, "childNodes":Ljava/util/List;, "Ljava/util/List<Lmortar/MortarScopeDevHelper$Node;>;"
    iget-object v3, p0, Lmortar/MortarScopeDevHelper$MortarScopeNode;->mortarScope:Lmortar/MortarScope;

    instance-of v3, v3, Lmortar/RealScope;

    if-nez v3, :cond_1

    .line 62
    :cond_0
    return-void

    .line 58
    :cond_1
    iget-object v2, p0, Lmortar/MortarScopeDevHelper$MortarScopeNode;->mortarScope:Lmortar/MortarScope;

    check-cast v2, Lmortar/RealScope;

    .line 59
    .local v2, "realScope":Lmortar/RealScope;
    iget-object v3, v2, Lmortar/RealScope;->children:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmortar/MortarScope;

    .line 60
    .local v0, "childScope":Lmortar/MortarScope;
    new-instance v3, Lmortar/MortarScopeDevHelper$MortarScopeNode;

    invoke-direct {v3, v0}, Lmortar/MortarScopeDevHelper$MortarScopeNode;-><init>(Lmortar/MortarScope;)V

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public getChildNodes()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lmortar/MortarScopeDevHelper$Node;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 49
    .local v0, "childNodes":Ljava/util/List;, "Ljava/util/List<Lmortar/MortarScopeDevHelper$Node;>;"
    iget-object v1, p0, Lmortar/MortarScopeDevHelper$MortarScopeNode;->mortarScope:Lmortar/MortarScope;

    invoke-static {v1, v0}, Lmortar/MortarScopeDevHelper$ModuleNode;->addModuleChildren(Lmortar/MortarScope;Ljava/util/List;)V

    .line 50
    invoke-direct {p0, v0}, Lmortar/MortarScopeDevHelper$MortarScopeNode;->addScopeChildren(Ljava/util/List;)V

    .line 51
    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 44
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SCOPE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmortar/MortarScopeDevHelper$MortarScopeNode;->mortarScope:Lmortar/MortarScope;

    invoke-interface {v1}, Lmortar/MortarScope;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
