.class Lmortar/RealScope;
.super Ljava/lang/Object;
.source "RealScope.java"

# interfaces
.implements Lmortar/MortarScope;


# instance fields
.field protected final children:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lmortar/RealScope;",
            ">;"
        }
    .end annotation
.end field

.field protected dead:Z

.field private final graph:Ldagger/ObjectGraph;

.field private final name:Ljava/lang/String;

.field private final parent:Lmortar/RealScope;

.field private final tearDowns:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lmortar/Scoped;",
            ">;"
        }
    .end annotation
.end field

.field protected final validate:Z


# direct methods
.method constructor <init>(Ljava/lang/String;Lmortar/RealScope;ZLdagger/ObjectGraph;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "parent"    # Lmortar/RealScope;
    .param p3, "validate"    # Z
    .param p4, "graph"    # Ldagger/ObjectGraph;

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lmortar/RealScope;->children:Ljava/util/Map;

    .line 38
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmortar/RealScope;->tearDowns:Ljava/util/Set;

    .line 48
    iput-object p4, p0, Lmortar/RealScope;->graph:Ldagger/ObjectGraph;

    .line 49
    iput-object p2, p0, Lmortar/RealScope;->parent:Lmortar/RealScope;

    .line 50
    iput-object p1, p0, Lmortar/RealScope;->name:Ljava/lang/String;

    .line 51
    iput-boolean p3, p0, Lmortar/RealScope;->validate:Z

    .line 53
    if-eqz p3, :cond_0

    invoke-virtual {p4}, Ldagger/ObjectGraph;->validate()V

    .line 54
    :cond_0
    return-void
.end method

.method constructor <init>(ZLdagger/ObjectGraph;)V
    .locals 2
    .param p1, "validate"    # Z
    .param p2, "objectGraph"    # Ldagger/ObjectGraph;

    .prologue
    .line 44
    const-string v0, "Root"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p1, p2}, Lmortar/RealScope;-><init>(Ljava/lang/String;Lmortar/RealScope;ZLdagger/ObjectGraph;)V

    .line 45
    return-void
.end method


# virtual methods
.method assertNotDead()V
    .locals 3

    .prologue
    .line 170
    invoke-virtual {p0}, Lmortar/RealScope;->isDead()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Scope "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lmortar/RealScope;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was destroyed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 171
    :cond_0
    return-void
.end method

.method public createContext(Landroid/content/Context;)Landroid/content/Context;
    .locals 1
    .param p1, "parentContext"    # Landroid/content/Context;

    .prologue
    .line 120
    new-instance v0, Lmortar/MortarContextWrapper;

    invoke-direct {v0, p1, p0}, Lmortar/MortarContextWrapper;-><init>(Landroid/content/Context;Lmortar/MortarScope;)V

    return-object v0
.end method

.method public destroyChild(Lmortar/MortarScope;)V
    .locals 6
    .param p1, "child"    # Lmortar/MortarScope;

    .prologue
    .line 124
    move-object v0, p1

    check-cast v0, Lmortar/RealScope;

    .line 125
    .local v0, "realChild":Lmortar/RealScope;
    iget-object v1, v0, Lmortar/RealScope;->parent:Lmortar/RealScope;

    if-eq v1, p0, :cond_0

    .line 126
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "%s was created by another scope"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lmortar/RealScope;->name:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 129
    :cond_0
    invoke-virtual {v0}, Lmortar/RealScope;->doDestroy()V

    .line 130
    return-void
.end method

.method doDestroy()V
    .locals 5

    .prologue
    .line 148
    iget-boolean v4, p0, Lmortar/RealScope;->dead:Z

    if-eqz v4, :cond_1

    .line 157
    :cond_0
    return-void

    .line 149
    :cond_1
    const/4 v4, 0x1

    iput-boolean v4, p0, Lmortar/RealScope;->dead:Z

    .line 151
    iget-object v4, p0, Lmortar/RealScope;->tearDowns:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmortar/Scoped;

    .local v2, "s":Lmortar/Scoped;
    invoke-interface {v2}, Lmortar/Scoped;->onExitScope()V

    goto :goto_0

    .line 152
    .end local v2    # "s":Lmortar/Scoped;
    :cond_2
    iget-object v4, p0, Lmortar/RealScope;->tearDowns:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->clear()V

    .line 153
    iget-object v4, p0, Lmortar/RealScope;->parent:Lmortar/RealScope;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lmortar/RealScope;->parent:Lmortar/RealScope;

    invoke-virtual {v4, p0}, Lmortar/RealScope;->onChildDestroyed(Lmortar/RealScope;)V

    .line 155
    :cond_3
    new-instance v3, Ljava/util/ArrayList;

    iget-object v4, p0, Lmortar/RealScope;->children:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 156
    .local v3, "snapshot":Ljava/util/List;, "Ljava/util/List<Lmortar/RealScope;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmortar/RealScope;

    .local v0, "child":Lmortar/RealScope;
    invoke-virtual {v0}, Lmortar/RealScope;->doDestroy()V

    goto :goto_1
.end method

.method doRegister(Lmortar/Scoped;)V
    .locals 1
    .param p1, "scoped"    # Lmortar/Scoped;

    .prologue
    .line 77
    invoke-virtual {p0}, Lmortar/RealScope;->assertNotDead()V

    .line 78
    iget-object v0, p0, Lmortar/RealScope;->tearDowns:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1, p0}, Lmortar/Scoped;->onEnterScope(Lmortar/MortarScope;)V

    .line 79
    :cond_0
    return-void
.end method

.method public bridge synthetic findChild(Ljava/lang/String;)Lmortar/MortarScope;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-virtual {p0, p1}, Lmortar/RealScope;->findChild(Ljava/lang/String;)Lmortar/RealScope;

    move-result-object v0

    return-object v0
.end method

.method public findChild(Ljava/lang/String;)Lmortar/RealScope;
    .locals 1
    .param p1, "childName"    # Ljava/lang/String;

    .prologue
    .line 90
    invoke-virtual {p0}, Lmortar/RealScope;->assertNotDead()V

    .line 91
    iget-object v0, p0, Lmortar/RealScope;->children:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmortar/RealScope;

    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lmortar/RealScope;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final getObjectGraph()Ldagger/ObjectGraph;
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0}, Lmortar/RealScope;->assertNotDead()V

    .line 62
    iget-object v0, p0, Lmortar/RealScope;->graph:Ldagger/ObjectGraph;

    return-object v0
.end method

.method getParent()Lmortar/RealScope;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lmortar/RealScope;->parent:Lmortar/RealScope;

    return-object v0
.end method

.method isDead()Z
    .locals 1

    .prologue
    .line 166
    iget-boolean v0, p0, Lmortar/RealScope;->dead:Z

    return v0
.end method

.method public isDestroyed()Z
    .locals 1

    .prologue
    .line 133
    iget-boolean v0, p0, Lmortar/RealScope;->dead:Z

    return v0
.end method

.method isRoot()Z
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lmortar/RealScope;->parent:Lmortar/RealScope;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method onChildDestroyed(Lmortar/RealScope;)V
    .locals 2
    .param p1, "child"    # Lmortar/RealScope;

    .prologue
    .line 144
    iget-object v0, p0, Lmortar/RealScope;->children:Ljava/util/Map;

    invoke-virtual {p1}, Lmortar/RealScope;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    return-void
.end method

.method public register(Lmortar/Scoped;)V
    .locals 5
    .param p1, "scoped"    # Lmortar/Scoped;

    .prologue
    .line 66
    instance-of v0, p1, Lmortar/Bundler;

    if-eqz v0, :cond_0

    .line 67
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Scope %s cannot register %s instance %s. Only %ss and their children can provide bundle services"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lmortar/RealScope;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-class v4, Lmortar/Bundler;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    check-cast p1, Lmortar/Bundler;

    .end local p1    # "scoped":Lmortar/Scoped;
    invoke-interface {p1}, Lmortar/Bundler;->getMortarBundleKey()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-class v4, Lmortar/MortarActivityScope;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 73
    .restart local p1    # "scoped":Lmortar/Scoped;
    :cond_0
    invoke-virtual {p0, p1}, Lmortar/RealScope;->doRegister(Lmortar/Scoped;)V

    .line 74
    return-void
.end method

.method replaceChild(Ljava/lang/String;Lmortar/RealScope;)V
    .locals 2
    .param p1, "childName"    # Ljava/lang/String;
    .param p2, "scope"    # Lmortar/RealScope;

    .prologue
    .line 137
    invoke-virtual {p2}, Lmortar/RealScope;->getParent()Lmortar/RealScope;

    move-result-object v0

    if-eq v0, p0, :cond_0

    .line 138
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Replacement scope must have receiver as parent"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 140
    :cond_0
    iget-object v0, p0, Lmortar/RealScope;->children:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    return-void
.end method

.method public requireChild(Lmortar/Blueprint;)Lmortar/MortarScope;
    .locals 8
    .param p1, "blueprint"    # Lmortar/Blueprint;

    .prologue
    const/4 v7, 0x0

    .line 96
    invoke-virtual {p0}, Lmortar/RealScope;->assertNotDead()V

    .line 98
    invoke-interface {p1}, Lmortar/Blueprint;->getMortarScopeName()Ljava/lang/String;

    move-result-object v2

    .line 99
    .local v2, "childName":Ljava/lang/String;
    invoke-virtual {p0, v2}, Lmortar/RealScope;->findChild(Ljava/lang/String;)Lmortar/RealScope;

    move-result-object v1

    .line 101
    .local v1, "child":Lmortar/RealScope;
    if-nez v1, :cond_0

    .line 102
    invoke-interface {p1}, Lmortar/Blueprint;->getDaggerModule()Ljava/lang/Object;

    move-result-object v3

    .line 104
    .local v3, "daggerModule":Ljava/lang/Object;
    if-nez v3, :cond_1

    .line 105
    iget-object v5, p0, Lmortar/RealScope;->graph:Ldagger/ObjectGraph;

    new-array v6, v7, [Ljava/lang/Object;

    invoke-virtual {v5, v6}, Ldagger/ObjectGraph;->plus([Ljava/lang/Object;)Ldagger/ObjectGraph;

    move-result-object v4

    .line 112
    .local v4, "newGraph":Ldagger/ObjectGraph;
    :goto_0
    new-instance v1, Lmortar/RealScope;

    .end local v1    # "child":Lmortar/RealScope;
    iget-boolean v5, p0, Lmortar/RealScope;->validate:Z

    invoke-direct {v1, v2, p0, v5, v4}, Lmortar/RealScope;-><init>(Ljava/lang/String;Lmortar/RealScope;ZLdagger/ObjectGraph;)V

    .line 113
    .restart local v1    # "child":Lmortar/RealScope;
    iget-object v5, p0, Lmortar/RealScope;->children:Ljava/util/Map;

    invoke-interface {v5, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    .end local v3    # "daggerModule":Ljava/lang/Object;
    .end local v4    # "newGraph":Ldagger/ObjectGraph;
    :cond_0
    return-object v1

    .line 106
    .restart local v3    # "daggerModule":Ljava/lang/Object;
    :cond_1
    instance-of v5, v3, Ljava/util/Collection;

    if-eqz v5, :cond_2

    move-object v0, v3

    .line 107
    check-cast v0, Ljava/util/Collection;

    .line 108
    .local v0, "c":Ljava/util/Collection;
    iget-object v5, p0, Lmortar/RealScope;->graph:Ldagger/ObjectGraph;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v6

    new-array v6, v6, [Ljava/lang/Object;

    invoke-interface {v0, v6}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ldagger/ObjectGraph;->plus([Ljava/lang/Object;)Ldagger/ObjectGraph;

    move-result-object v4

    .line 109
    .restart local v4    # "newGraph":Ldagger/ObjectGraph;
    goto :goto_0

    .line 110
    .end local v0    # "c":Ljava/util/Collection;
    .end local v4    # "newGraph":Ldagger/ObjectGraph;
    :cond_2
    iget-object v5, p0, Lmortar/RealScope;->graph:Ldagger/ObjectGraph;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v3, v6, v7

    invoke-virtual {v5, v6}, Ldagger/ObjectGraph;->plus([Ljava/lang/Object;)Ldagger/ObjectGraph;

    move-result-object v4

    .restart local v4    # "newGraph":Ldagger/ObjectGraph;
    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 160
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RealScope@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "name=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lmortar/RealScope;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
