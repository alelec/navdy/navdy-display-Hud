.class Lmortar/MortarScopeDevHelper$NodeSorter;
.super Ljava/lang/Object;
.source "MortarScopeDevHelper.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmortar/MortarScopeDevHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "NodeSorter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lmortar/MortarScopeDevHelper$Node;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 226
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lmortar/MortarScopeDevHelper$1;)V
    .locals 0
    .param p1, "x0"    # Lmortar/MortarScopeDevHelper$1;

    .prologue
    .line 226
    invoke-direct {p0}, Lmortar/MortarScopeDevHelper$NodeSorter;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 226
    check-cast p1, Lmortar/MortarScopeDevHelper$Node;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lmortar/MortarScopeDevHelper$Node;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lmortar/MortarScopeDevHelper$NodeSorter;->compare(Lmortar/MortarScopeDevHelper$Node;Lmortar/MortarScopeDevHelper$Node;)I

    move-result v0

    return v0
.end method

.method public compare(Lmortar/MortarScopeDevHelper$Node;Lmortar/MortarScopeDevHelper$Node;)I
    .locals 2
    .param p1, "lhs"    # Lmortar/MortarScopeDevHelper$Node;
    .param p2, "rhs"    # Lmortar/MortarScopeDevHelper$Node;

    .prologue
    .line 228
    invoke-interface {p1}, Lmortar/MortarScopeDevHelper$Node;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2}, Lmortar/MortarScopeDevHelper$Node;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method
