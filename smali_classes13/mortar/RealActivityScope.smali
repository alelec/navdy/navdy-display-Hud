.class Lmortar/RealActivityScope;
.super Lmortar/RealScope;
.source "RealActivityScope.java"

# interfaces
.implements Lmortar/MortarActivityScope;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmortar/RealActivityScope$1;,
        Lmortar/RealActivityScope$LoadingState;
    }
.end annotation


# instance fields
.field private bundlers:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lmortar/Bundler;",
            ">;"
        }
    .end annotation
.end field

.field private latestSavedInstanceState:Landroid/os/Bundle;

.field private myLoadingState:Lmortar/RealActivityScope$LoadingState;

.field private toloadThisTime:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lmortar/Bundler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lmortar/RealScope;)V
    .locals 4
    .param p1, "original"    # Lmortar/RealScope;

    .prologue
    .line 39
    invoke-virtual {p1}, Lmortar/RealScope;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lmortar/RealScope;->getParent()Lmortar/RealScope;

    move-result-object v1

    iget-boolean v2, p1, Lmortar/RealScope;->validate:Z

    invoke-virtual {p1}, Lmortar/RealScope;->getObjectGraph()Ldagger/ObjectGraph;

    move-result-object v3

    invoke-direct {p0, v0, v1, v2, v3}, Lmortar/RealScope;-><init>(Ljava/lang/String;Lmortar/RealScope;ZLdagger/ObjectGraph;)V

    .line 33
    sget-object v0, Lmortar/RealActivityScope$LoadingState;->IDLE:Lmortar/RealActivityScope$LoadingState;

    iput-object v0, p0, Lmortar/RealActivityScope;->myLoadingState:Lmortar/RealActivityScope$LoadingState;

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmortar/RealActivityScope;->toloadThisTime:Ljava/util/List;

    .line 36
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmortar/RealActivityScope;->bundlers:Ljava/util/Set;

    .line 40
    return-void
.end method

.method private doLoading()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 139
    iget-object v4, p0, Lmortar/RealActivityScope;->myLoadingState:Lmortar/RealActivityScope$LoadingState;

    sget-object v5, Lmortar/RealActivityScope$LoadingState;->IDLE:Lmortar/RealActivityScope$LoadingState;

    if-eq v4, v5, :cond_0

    iget-object v4, p0, Lmortar/RealActivityScope;->myLoadingState:Lmortar/RealActivityScope$LoadingState;

    sget-object v5, Lmortar/RealActivityScope$LoadingState;->LOADING:Lmortar/RealActivityScope$LoadingState;

    if-eq v4, v5, :cond_0

    .line 140
    new-instance v4, Ljava/lang/IllegalStateException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Cannot load while "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lmortar/RealActivityScope;->myLoadingState:Lmortar/RealActivityScope$LoadingState;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 146
    :cond_0
    iget-object v2, p0, Lmortar/RealActivityScope;->myLoadingState:Lmortar/RealActivityScope$LoadingState;

    .line 147
    .local v2, "initialState":Lmortar/RealActivityScope$LoadingState;
    sget-object v4, Lmortar/RealActivityScope$LoadingState;->LOADING:Lmortar/RealActivityScope$LoadingState;

    iput-object v4, p0, Lmortar/RealActivityScope;->myLoadingState:Lmortar/RealActivityScope$LoadingState;

    .line 148
    :goto_0
    iget-object v4, p0, Lmortar/RealActivityScope;->toloadThisTime:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    .line 149
    invoke-virtual {p0}, Lmortar/RealActivityScope;->isDead()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 160
    :cond_1
    return-void

    .line 151
    :cond_2
    iget-object v4, p0, Lmortar/RealActivityScope;->toloadThisTime:Ljava/util/List;

    invoke-interface {v4, v6}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmortar/Bundler;

    .line 152
    .local v3, "next":Lmortar/Bundler;
    iget-object v4, p0, Lmortar/RealActivityScope;->bundlers:Ljava/util/Set;

    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 153
    iget-object v4, p0, Lmortar/RealActivityScope;->latestSavedInstanceState:Landroid/os/Bundle;

    invoke-direct {p0, v3, v4, v6}, Lmortar/RealActivityScope;->getNestedBundle(Lmortar/Bundler;Landroid/os/Bundle;Z)Landroid/os/Bundle;

    move-result-object v4

    invoke-interface {v3, v4}, Lmortar/Bundler;->onLoad(Landroid/os/Bundle;)V

    goto :goto_0

    .line 155
    .end local v3    # "next":Lmortar/Bundler;
    :cond_3
    iput-object v2, p0, Lmortar/RealActivityScope;->myLoadingState:Lmortar/RealActivityScope$LoadingState;

    .line 157
    iget-object v4, p0, Lmortar/RealActivityScope;->children:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmortar/RealScope;

    .line 158
    .local v0, "child":Lmortar/RealScope;
    instance-of v4, v0, Lmortar/RealActivityScope;

    if-eqz v4, :cond_4

    check-cast v0, Lmortar/RealActivityScope;

    .end local v0    # "child":Lmortar/RealScope;
    invoke-direct {v0}, Lmortar/RealActivityScope;->doLoading()V

    goto :goto_1
.end method

.method private getNamedBundle(Ljava/lang/String;Landroid/os/Bundle;Z)Landroid/os/Bundle;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "bundle"    # Landroid/os/Bundle;
    .param p3, "eager"    # Z

    .prologue
    .line 171
    if-nez p2, :cond_1

    const/4 v0, 0x0

    .line 178
    :cond_0
    :goto_0
    return-object v0

    .line 173
    :cond_1
    invoke-virtual {p2, p1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 174
    .local v0, "child":Landroid/os/Bundle;
    if-eqz p3, :cond_0

    if-nez v0, :cond_0

    .line 175
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "child":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 176
    .restart local v0    # "child":Landroid/os/Bundle;
    invoke-virtual {p2, p1, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method private getNestedBundle(Lmortar/Bundler;Landroid/os/Bundle;Z)Landroid/os/Bundle;
    .locals 1
    .param p1, "bundler"    # Lmortar/Bundler;
    .param p2, "bundle"    # Landroid/os/Bundle;
    .param p3, "eager"    # Z

    .prologue
    .line 163
    invoke-interface {p1}, Lmortar/Bundler;->getMortarBundleKey()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lmortar/RealActivityScope;->getNamedBundle(Ljava/lang/String;Landroid/os/Bundle;Z)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method private getNestedBundle(Lmortar/MortarScope;Landroid/os/Bundle;Z)Landroid/os/Bundle;
    .locals 1
    .param p1, "scope"    # Lmortar/MortarScope;
    .param p2, "bundle"    # Landroid/os/Bundle;
    .param p3, "eager"    # Z

    .prologue
    .line 167
    invoke-interface {p1}, Lmortar/MortarScope;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lmortar/RealActivityScope;->getNamedBundle(Ljava/lang/String;Landroid/os/Bundle;Z)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method private unionLoadingState(Lmortar/RealActivityScope;)Lmortar/RealActivityScope$LoadingState;
    .locals 3
    .param p1, "realActivityScope"    # Lmortar/RealActivityScope;

    .prologue
    .line 182
    iget-object v1, p1, Lmortar/RealActivityScope;->myLoadingState:Lmortar/RealActivityScope$LoadingState;

    .line 183
    .local v1, "s":Lmortar/RealActivityScope$LoadingState;
    sget-object v2, Lmortar/RealActivityScope$LoadingState;->IDLE:Lmortar/RealActivityScope$LoadingState;

    if-eq v1, v2, :cond_1

    .line 190
    .end local v1    # "s":Lmortar/RealActivityScope$LoadingState;
    :cond_0
    :goto_0
    return-object v1

    .line 187
    .restart local v1    # "s":Lmortar/RealActivityScope$LoadingState;
    :cond_1
    invoke-virtual {p1}, Lmortar/RealActivityScope;->getParent()Lmortar/RealScope;

    move-result-object v0

    .line 188
    .local v0, "parent":Lmortar/RealScope;
    instance-of v2, v0, Lmortar/RealActivityScope;

    if-eqz v2, :cond_0

    .line 190
    check-cast v0, Lmortar/RealActivityScope;

    .end local v0    # "parent":Lmortar/RealScope;
    invoke-direct {p0, v0}, Lmortar/RealActivityScope;->unionLoadingState(Lmortar/RealActivityScope;)Lmortar/RealActivityScope$LoadingState;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method onChildDestroyed(Lmortar/RealScope;)V
    .locals 3
    .param p1, "child"    # Lmortar/RealScope;

    .prologue
    .line 131
    iget-object v1, p0, Lmortar/RealActivityScope;->latestSavedInstanceState:Landroid/os/Bundle;

    if-eqz v1, :cond_0

    .line 132
    invoke-virtual {p1}, Lmortar/RealScope;->getName()Ljava/lang/String;

    move-result-object v0

    .line 133
    .local v0, "name":Ljava/lang/String;
    iget-object v1, p0, Lmortar/RealActivityScope;->latestSavedInstanceState:Landroid/os/Bundle;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 135
    .end local v0    # "name":Ljava/lang/String;
    :cond_0
    invoke-super {p0, p1}, Lmortar/RealScope;->onChildDestroyed(Lmortar/RealScope;)V

    .line 136
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 80
    invoke-virtual {p0}, Lmortar/RealActivityScope;->assertNotDead()V

    .line 83
    iput-object p1, p0, Lmortar/RealActivityScope;->latestSavedInstanceState:Landroid/os/Bundle;

    .line 85
    iget-object v2, p0, Lmortar/RealActivityScope;->toloadThisTime:Ljava/util/List;

    iget-object v3, p0, Lmortar/RealActivityScope;->bundlers:Ljava/util/Set;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 86
    invoke-direct {p0, p0}, Lmortar/RealActivityScope;->unionLoadingState(Lmortar/RealActivityScope;)Lmortar/RealActivityScope$LoadingState;

    move-result-object v2

    sget-object v3, Lmortar/RealActivityScope$LoadingState;->IDLE:Lmortar/RealActivityScope$LoadingState;

    if-ne v2, v3, :cond_0

    invoke-direct {p0}, Lmortar/RealActivityScope;->doLoading()V

    .line 88
    :cond_0
    iget-object v2, p0, Lmortar/RealActivityScope;->children:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmortar/RealScope;

    .line 89
    .local v0, "child":Lmortar/RealScope;
    instance-of v2, v0, Lmortar/RealActivityScope;

    if-eqz v2, :cond_1

    move-object v2, v0

    .line 90
    check-cast v2, Lmortar/RealActivityScope;

    const/4 v3, 0x0

    invoke-direct {p0, v0, p1, v3}, Lmortar/RealActivityScope;->getNestedBundle(Lmortar/MortarScope;Landroid/os/Bundle;Z)Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v2, v3}, Lmortar/RealActivityScope;->onCreate(Landroid/os/Bundle;)V

    goto :goto_0

    .line 92
    .end local v0    # "child":Lmortar/RealScope;
    :cond_2
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x1

    .line 95
    invoke-virtual {p0}, Lmortar/RealActivityScope;->assertNotDead()V

    .line 96
    iget-object v3, p0, Lmortar/RealActivityScope;->myLoadingState:Lmortar/RealActivityScope$LoadingState;

    sget-object v4, Lmortar/RealActivityScope$LoadingState;->IDLE:Lmortar/RealActivityScope$LoadingState;

    if-eq v3, v4, :cond_0

    .line 97
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cannot handle onSaveInstanceState while "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lmortar/RealActivityScope;->myLoadingState:Lmortar/RealActivityScope$LoadingState;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 100
    :cond_0
    iput-object p1, p0, Lmortar/RealActivityScope;->latestSavedInstanceState:Landroid/os/Bundle;

    .line 102
    sget-object v3, Lmortar/RealActivityScope$LoadingState;->SAVING:Lmortar/RealActivityScope$LoadingState;

    iput-object v3, p0, Lmortar/RealActivityScope;->myLoadingState:Lmortar/RealActivityScope$LoadingState;

    .line 103
    new-instance v3, Ljava/util/ArrayList;

    iget-object v4, p0, Lmortar/RealActivityScope;->bundlers:Ljava/util/Set;

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmortar/Bundler;

    .line 105
    .local v0, "b":Lmortar/Bundler;
    invoke-virtual {p0}, Lmortar/RealActivityScope;->isDead()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 117
    .end local v0    # "b":Lmortar/Bundler;
    :cond_1
    :goto_1
    return-void

    .line 107
    .restart local v0    # "b":Lmortar/Bundler;
    :cond_2
    iget-object v3, p0, Lmortar/RealActivityScope;->latestSavedInstanceState:Landroid/os/Bundle;

    invoke-direct {p0, v0, v3, v5}, Lmortar/RealActivityScope;->getNestedBundle(Lmortar/Bundler;Landroid/os/Bundle;Z)Landroid/os/Bundle;

    move-result-object v3

    invoke-interface {v0, v3}, Lmortar/Bundler;->onSave(Landroid/os/Bundle;)V

    goto :goto_0

    .line 110
    .end local v0    # "b":Lmortar/Bundler;
    :cond_3
    iget-object v3, p0, Lmortar/RealActivityScope;->children:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmortar/RealScope;

    .line 111
    .local v1, "child":Lmortar/RealScope;
    instance-of v3, v1, Lmortar/RealActivityScope;

    if-eqz v3, :cond_1

    move-object v3, v1

    .line 112
    check-cast v3, Lmortar/RealActivityScope;

    iget-object v4, p0, Lmortar/RealActivityScope;->latestSavedInstanceState:Landroid/os/Bundle;

    invoke-direct {p0, v1, v4, v5}, Lmortar/RealActivityScope;->getNestedBundle(Lmortar/MortarScope;Landroid/os/Bundle;Z)Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v3, v4}, Lmortar/RealActivityScope;->onSaveInstanceState(Landroid/os/Bundle;)V

    goto :goto_2

    .line 116
    .end local v1    # "child":Lmortar/RealScope;
    :cond_4
    sget-object v3, Lmortar/RealActivityScope$LoadingState;->IDLE:Lmortar/RealActivityScope$LoadingState;

    iput-object v3, p0, Lmortar/RealActivityScope;->myLoadingState:Lmortar/RealActivityScope$LoadingState;

    goto :goto_1
.end method

.method public register(Lmortar/Scoped;)V
    .locals 7
    .param p1, "scoped"    # Lmortar/Scoped;

    .prologue
    .line 43
    if-nez p1, :cond_0

    new-instance v3, Ljava/lang/NullPointerException;

    const-string v4, "Cannot register null scoped."

    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 45
    :cond_0
    iget-object v3, p0, Lmortar/RealActivityScope;->myLoadingState:Lmortar/RealActivityScope$LoadingState;

    sget-object v4, Lmortar/RealActivityScope$LoadingState;->SAVING:Lmortar/RealActivityScope$LoadingState;

    if-ne v3, v4, :cond_1

    .line 46
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Cannot register during onSave"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 49
    :cond_1
    invoke-virtual {p0, p1}, Lmortar/RealActivityScope;->doRegister(Lmortar/Scoped;)V

    .line 50
    instance-of v3, p1, Lmortar/Bundler;

    if-nez v3, :cond_3

    .line 77
    :cond_2
    :goto_0
    return-void

    :cond_3
    move-object v1, p1

    .line 52
    check-cast v1, Lmortar/Bundler;

    .line 53
    .local v1, "b":Lmortar/Bundler;
    invoke-interface {v1}, Lmortar/Bundler;->getMortarBundleKey()Ljava/lang/String;

    move-result-object v2

    .line 54
    .local v2, "mortarBundleKey":Ljava/lang/String;
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 55
    :cond_4
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "%s has null or empty bundle key"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 59
    :cond_5
    invoke-virtual {p0}, Lmortar/RealActivityScope;->getParent()Lmortar/RealScope;

    move-result-object v3

    instance-of v3, v3, Lmortar/RealActivityScope;

    if-eqz v3, :cond_6

    .line 60
    invoke-virtual {p0}, Lmortar/RealActivityScope;->getParent()Lmortar/RealScope;

    move-result-object v3

    check-cast v3, Lmortar/RealActivityScope;

    invoke-direct {p0, v3}, Lmortar/RealActivityScope;->unionLoadingState(Lmortar/RealActivityScope;)Lmortar/RealActivityScope$LoadingState;

    move-result-object v0

    .line 65
    .local v0, "ancestorLoadingState":Lmortar/RealActivityScope$LoadingState;
    :goto_1
    sget-object v3, Lmortar/RealActivityScope$1;->$SwitchMap$mortar$RealActivityScope$LoadingState:[I

    invoke-virtual {v0}, Lmortar/RealActivityScope$LoadingState;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 75
    new-instance v3, Ljava/lang/AssertionError;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown state "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 62
    .end local v0    # "ancestorLoadingState":Lmortar/RealActivityScope$LoadingState;
    :cond_6
    sget-object v0, Lmortar/RealActivityScope$LoadingState;->IDLE:Lmortar/RealActivityScope$LoadingState;

    .restart local v0    # "ancestorLoadingState":Lmortar/RealActivityScope$LoadingState;
    goto :goto_1

    .line 67
    :pswitch_0
    iget-object v3, p0, Lmortar/RealActivityScope;->toloadThisTime:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    invoke-direct {p0}, Lmortar/RealActivityScope;->doLoading()V

    goto :goto_0

    .line 71
    :pswitch_1
    iget-object v3, p0, Lmortar/RealActivityScope;->toloadThisTime:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lmortar/RealActivityScope;->toloadThisTime:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 65
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public requireChild(Lmortar/Blueprint;)Lmortar/MortarScope;
    .locals 4
    .param p1, "blueprint"    # Lmortar/Blueprint;

    .prologue
    .line 120
    invoke-super {p0, p1}, Lmortar/RealScope;->requireChild(Lmortar/Blueprint;)Lmortar/MortarScope;

    move-result-object v1

    .line 121
    .local v1, "unwrapped":Lmortar/MortarScope;
    instance-of v2, v1, Lmortar/RealActivityScope;

    if-eqz v2, :cond_0

    .line 127
    .end local v1    # "unwrapped":Lmortar/MortarScope;
    :goto_0
    return-object v1

    .line 123
    .restart local v1    # "unwrapped":Lmortar/MortarScope;
    :cond_0
    new-instance v0, Lmortar/RealActivityScope;

    check-cast v1, Lmortar/RealScope;

    .end local v1    # "unwrapped":Lmortar/MortarScope;
    invoke-direct {v0, v1}, Lmortar/RealActivityScope;-><init>(Lmortar/RealScope;)V

    .line 124
    .local v0, "childScope":Lmortar/RealActivityScope;
    invoke-interface {p1}, Lmortar/Blueprint;->getMortarScopeName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, v0}, Lmortar/RealActivityScope;->replaceChild(Ljava/lang/String;Lmortar/RealScope;)V

    .line 125
    iget-object v2, p0, Lmortar/RealActivityScope;->latestSavedInstanceState:Landroid/os/Bundle;

    const/4 v3, 0x0

    invoke-direct {p0, v0, v2, v3}, Lmortar/RealActivityScope;->getNestedBundle(Lmortar/MortarScope;Landroid/os/Bundle;Z)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v2}, Lmortar/RealActivityScope;->onCreate(Landroid/os/Bundle;)V

    move-object v1, v0

    .line 127
    goto :goto_0
.end method
