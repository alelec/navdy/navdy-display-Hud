.class Lmortar/MortarScopeDevHelper$ModuleNode;
.super Ljava/lang/Object;
.source "MortarScopeDevHelper.java"

# interfaces
.implements Lmortar/MortarScopeDevHelper$Node;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmortar/MortarScopeDevHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ModuleNode"
.end annotation


# static fields
.field private static COULD_NOT_LOAD:Z

.field private static INJECTABLE_TYPES_FIELD:Ljava/lang/reflect/Field;

.field private static OBJECT_GRAPH_CLASS:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final injects:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lmortar/MortarScopeDevHelper$Node;",
            ">;"
        }
    .end annotation
.end field

.field private final moduleClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 73
    :try_start_0
    const-string v1, "dagger.ObjectGraph$DaggerObjectGraph"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sput-object v1, Lmortar/MortarScopeDevHelper$ModuleNode;->OBJECT_GRAPH_CLASS:Ljava/lang/Class;

    .line 74
    sget-object v1, Lmortar/MortarScopeDevHelper$ModuleNode;->OBJECT_GRAPH_CLASS:Ljava/lang/Class;

    const-string v2, "injectableTypes"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    sput-object v1, Lmortar/MortarScopeDevHelper$ModuleNode;->INJECTABLE_TYPES_FIELD:Ljava/lang/reflect/Field;

    .line 75
    sget-object v1, Lmortar/MortarScopeDevHelper$ModuleNode;->INJECTABLE_TYPES_FIELD:Ljava/lang/reflect/Field;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    .local v0, "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 76
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_0
    move-exception v0

    .line 77
    .restart local v0    # "e":Ljava/lang/Exception;
    sput-boolean v3, Lmortar/MortarScopeDevHelper$ModuleNode;->COULD_NOT_LOAD:Z

    goto :goto_0
.end method

.method constructor <init>(Ljava/lang/Class;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/util/List",
            "<",
            "Lmortar/MortarScopeDevHelper$Node;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 131
    .local p1, "moduleClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .local p2, "injects":Ljava/util/List;, "Ljava/util/List<Lmortar/MortarScopeDevHelper$Node;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132
    iput-object p1, p0, Lmortar/MortarScopeDevHelper$ModuleNode;->moduleClass:Ljava/lang/Class;

    .line 133
    iput-object p2, p0, Lmortar/MortarScopeDevHelper$ModuleNode;->injects:Ljava/util/List;

    .line 134
    return-void
.end method

.method static addModuleChildren(Lmortar/MortarScope;Ljava/util/List;)V
    .locals 13
    .param p0, "mortarScope"    # Lmortar/MortarScope;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmortar/MortarScope;",
            "Ljava/util/List",
            "<",
            "Lmortar/MortarScopeDevHelper$Node;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 82
    .local p1, "childNodes":Ljava/util/List;, "Ljava/util/List<Lmortar/MortarScopeDevHelper$Node;>;"
    sget-boolean v10, Lmortar/MortarScopeDevHelper$ModuleNode;->COULD_NOT_LOAD:Z

    if-eqz v10, :cond_1

    .line 83
    new-instance v10, Lmortar/MortarScopeDevHelper$ModuleNode$1;

    invoke-direct {v10}, Lmortar/MortarScopeDevHelper$ModuleNode$1;-><init>()V

    invoke-interface {p1, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 126
    :cond_0
    return-void

    .line 95
    :cond_1
    invoke-interface {p0}, Lmortar/MortarScope;->getObjectGraph()Ldagger/ObjectGraph;

    move-result-object v9

    .line 97
    .local v9, "objectGraph":Ldagger/ObjectGraph;
    sget-object v10, Lmortar/MortarScopeDevHelper$ModuleNode;->OBJECT_GRAPH_CLASS:Ljava/lang/Class;

    invoke-virtual {v10, v9}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 98
    new-instance v10, Ljava/lang/IllegalArgumentException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " is not an instance of "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Lmortar/MortarScopeDevHelper$ModuleNode;->OBJECT_GRAPH_CLASS:Ljava/lang/Class;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 106
    :cond_2
    :try_start_0
    sget-object v10, Lmortar/MortarScopeDevHelper$ModuleNode;->INJECTABLE_TYPES_FIELD:Ljava/lang/reflect/Field;

    invoke-virtual {v10, v9}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 111
    .local v3, "injectableTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Class<*>;>;"
    new-instance v4, Ljava/util/LinkedHashMap;

    invoke-direct {v4}, Ljava/util/LinkedHashMap;-><init>()V

    .line 112
    .local v4, "injectsByModule":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Class<*>;Ljava/util/List<Lmortar/MortarScopeDevHelper$Node;>;>;"
    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 113
    .local v2, "injectableType":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Class<*>;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Class;

    .line 114
    .local v7, "moduleClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-interface {v4, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/List;

    .line 115
    .local v8, "moduleInjects":Ljava/util/List;, "Ljava/util/List<Lmortar/MortarScopeDevHelper$Node;>;"
    if-nez v8, :cond_3

    .line 116
    new-instance v8, Ljava/util/ArrayList;

    .end local v8    # "moduleInjects":Ljava/util/List;, "Ljava/util/List<Lmortar/MortarScopeDevHelper$Node;>;"
    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 117
    .restart local v8    # "moduleInjects":Ljava/util/List;, "Ljava/util/List<Lmortar/MortarScopeDevHelper$Node;>;"
    invoke-interface {v4, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    :cond_3
    new-instance v11, Lmortar/MortarScopeDevHelper$InjectNode;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-direct {v11, v10}, Lmortar/MortarScopeDevHelper$InjectNode;-><init>(Ljava/lang/String;)V

    invoke-interface {v8, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 107
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "injectableType":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Class<*>;>;"
    .end local v3    # "injectableTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Class<*>;>;"
    .end local v4    # "injectsByModule":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Class<*>;Ljava/util/List<Lmortar/MortarScopeDevHelper$Node;>;>;"
    .end local v7    # "moduleClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v8    # "moduleInjects":Ljava/util/List;, "Ljava/util/List<Lmortar/MortarScopeDevHelper$Node;>;"
    :catch_0
    move-exception v0

    .line 108
    .local v0, "e":Ljava/lang/Exception;
    new-instance v10, Ljava/lang/RuntimeException;

    invoke-direct {v10, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v10

    .line 122
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v3    # "injectableTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Class<*>;>;"
    .restart local v4    # "injectsByModule":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Class<*>;Ljava/util/List<Lmortar/MortarScopeDevHelper$Node;>;>;"
    :cond_4
    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    .line 123
    .local v5, "injectsByModuleSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/Class<*>;Ljava/util/List<Lmortar/MortarScopeDevHelper$Node;>;>;>;"
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    .line 124
    .local v6, "moduleAndInjects":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Class<*>;Ljava/util/List<Lmortar/MortarScopeDevHelper$Node;>;>;"
    new-instance v12, Lmortar/MortarScopeDevHelper$ModuleNode;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Class;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/List;

    invoke-direct {v12, v10, v11}, Lmortar/MortarScopeDevHelper$ModuleNode;-><init>(Ljava/lang/Class;Ljava/util/List;)V

    invoke-interface {p1, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method


# virtual methods
.method public getChildNodes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lmortar/MortarScopeDevHelper$Node;",
            ">;"
        }
    .end annotation

    .prologue
    .line 141
    iget-object v0, p0, Lmortar/MortarScopeDevHelper$ModuleNode;->injects:Ljava/util/List;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 137
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MODULE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmortar/MortarScopeDevHelper$ModuleNode;->moduleClass:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
