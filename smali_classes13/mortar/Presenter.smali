.class public abstract Lmortar/Presenter;
.super Ljava/lang/Object;
.source "Presenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private loaded:Z

.field private registration:Lmortar/Bundler;

.field private view:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    .local p0, "this":Lmortar/Presenter;, "Lmortar/Presenter<TV;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lmortar/Presenter;->view:Ljava/lang/Object;

    .line 26
    new-instance v0, Lmortar/Presenter$1;

    invoke-direct {v0, p0}, Lmortar/Presenter$1;-><init>(Lmortar/Presenter;)V

    iput-object v0, p0, Lmortar/Presenter;->registration:Lmortar/Bundler;

    return-void
.end method

.method static synthetic access$000(Lmortar/Presenter;)Z
    .locals 1
    .param p0, "x0"    # Lmortar/Presenter;

    .prologue
    .line 20
    iget-boolean v0, p0, Lmortar/Presenter;->loaded:Z

    return v0
.end method

.method static synthetic access$002(Lmortar/Presenter;Z)Z
    .locals 0
    .param p0, "x0"    # Lmortar/Presenter;
    .param p1, "x1"    # Z

    .prologue
    .line 20
    iput-boolean p1, p0, Lmortar/Presenter;->loaded:Z

    return p1
.end method


# virtual methods
.method public dropView(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 91
    .local p0, "this":Lmortar/Presenter;, "Lmortar/Presenter<TV;>;"
    .local p1, "view":Ljava/lang/Object;, "TV;"
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "dropped view must not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 92
    :cond_0
    iget-object v0, p0, Lmortar/Presenter;->view:Ljava/lang/Object;

    if-ne p1, v0, :cond_1

    .line 93
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmortar/Presenter;->loaded:Z

    .line 94
    const/4 v0, 0x0

    iput-object v0, p0, Lmortar/Presenter;->view:Ljava/lang/Object;

    .line 96
    :cond_1
    return-void
.end method

.method protected abstract extractScope(Ljava/lang/Object;)Lmortar/MortarScope;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)",
            "Lmortar/MortarScope;"
        }
    .end annotation
.end method

.method protected getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    .local p0, "this":Lmortar/Presenter;, "Lmortar/Presenter<TV;>;"
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final getView()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 110
    .local p0, "this":Lmortar/Presenter;, "Lmortar/Presenter<TV;>;"
    iget-object v0, p0, Lmortar/Presenter;->view:Ljava/lang/Object;

    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 0
    .param p1, "scope"    # Lmortar/MortarScope;

    .prologue
    .line 115
    .local p0, "this":Lmortar/Presenter;, "Lmortar/Presenter<TV;>;"
    return-void
.end method

.method protected onExitScope()V
    .locals 0

    .prologue
    .line 137
    .local p0, "this":Lmortar/Presenter;, "Lmortar/Presenter<TV;>;"
    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 125
    .local p0, "this":Lmortar/Presenter;, "Lmortar/Presenter<TV;>;"
    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 129
    .local p0, "this":Lmortar/Presenter;, "Lmortar/Presenter<TV;>;"
    return-void
.end method

.method public final takeView(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 67
    .local p0, "this":Lmortar/Presenter;, "Lmortar/Presenter<TV;>;"
    .local p1, "view":Ljava/lang/Object;, "TV;"
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "new view must not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :cond_0
    iget-object v0, p0, Lmortar/Presenter;->view:Ljava/lang/Object;

    if-eq v0, p1, :cond_2

    .line 70
    iget-object v0, p0, Lmortar/Presenter;->view:Ljava/lang/Object;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmortar/Presenter;->view:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lmortar/Presenter;->dropView(Ljava/lang/Object;)V

    .line 72
    :cond_1
    iput-object p1, p0, Lmortar/Presenter;->view:Ljava/lang/Object;

    .line 73
    invoke-virtual {p0, p1}, Lmortar/Presenter;->extractScope(Ljava/lang/Object;)Lmortar/MortarScope;

    move-result-object v0

    iget-object v1, p0, Lmortar/Presenter;->registration:Lmortar/Bundler;

    invoke-interface {v0, v1}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 75
    :cond_2
    return-void
.end method
