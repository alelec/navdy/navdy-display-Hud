.class public abstract Lmortar/PopupPresenter;
.super Lmortar/Presenter;
.source "PopupPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<D::",
        "Landroid/os/Parcelable;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Lmortar/Presenter",
        "<",
        "Lmortar/Popup",
        "<TD;TR;>;>;"
    }
.end annotation


# static fields
.field private static final WITH_FLOURISH:Z = true


# instance fields
.field private whatToShow:Landroid/os/Parcelable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TD;"
        }
    .end annotation
.end field

.field private final whatToShowKey:Ljava/lang/String;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 45
    .local p0, "this":Lmortar/PopupPresenter;, "Lmortar/PopupPresenter<TD;TR;>;"
    const-string v0, ""

    invoke-direct {p0, v0}, Lmortar/PopupPresenter;-><init>(Ljava/lang/String;)V

    .line 46
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "customStateKey"    # Ljava/lang/String;

    .prologue
    .line 40
    .local p0, "this":Lmortar/PopupPresenter;, "Lmortar/PopupPresenter<TD;TR;>;"
    invoke-direct {p0}, Lmortar/Presenter;-><init>()V

    .line 41
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmortar/PopupPresenter;->whatToShowKey:Ljava/lang/String;

    .line 42
    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 2

    .prologue
    .line 66
    .local p0, "this":Lmortar/PopupPresenter;, "Lmortar/PopupPresenter<TD;TR;>;"
    iget-object v1, p0, Lmortar/PopupPresenter;->whatToShow:Landroid/os/Parcelable;

    if-eqz v1, :cond_0

    .line 67
    const/4 v1, 0x0

    iput-object v1, p0, Lmortar/PopupPresenter;->whatToShow:Landroid/os/Parcelable;

    .line 69
    invoke-virtual {p0}, Lmortar/PopupPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmortar/Popup;

    .line 70
    .local v0, "popUp":Lmortar/Popup;, "Lmortar/Popup<TD;TR;>;"
    if-nez v0, :cond_1

    .line 74
    .end local v0    # "popUp":Lmortar/Popup;, "Lmortar/Popup<TD;TR;>;"
    :cond_0
    :goto_0
    return-void

    .line 72
    .restart local v0    # "popUp":Lmortar/Popup;, "Lmortar/Popup<TD;TR;>;"
    :cond_1
    invoke-interface {v0}, Lmortar/Popup;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lmortar/Popup;->dismiss(Z)V

    goto :goto_0
.end method

.method public bridge synthetic dropView(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 29
    .local p0, "this":Lmortar/PopupPresenter;, "Lmortar/PopupPresenter<TD;TR;>;"
    check-cast p1, Lmortar/Popup;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lmortar/PopupPresenter;->dropView(Lmortar/Popup;)V

    return-void
.end method

.method public dropView(Lmortar/Popup;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmortar/Popup",
            "<TD;TR;>;)V"
        }
    .end annotation

    .prologue
    .line 88
    .local p0, "this":Lmortar/PopupPresenter;, "Lmortar/PopupPresenter<TD;TR;>;"
    .local p1, "view":Lmortar/Popup;, "Lmortar/Popup<TD;TR;>;"
    invoke-virtual {p0}, Lmortar/PopupPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmortar/Popup;

    .line 89
    .local v0, "oldView":Lmortar/Popup;, "Lmortar/Popup<TD;TR;>;"
    if-ne v0, p1, :cond_0

    invoke-interface {v0}, Lmortar/Popup;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lmortar/Popup;->dismiss(Z)V

    .line 90
    :cond_0
    invoke-super {p0, p1}, Lmortar/Presenter;->dropView(Ljava/lang/Object;)V

    .line 91
    return-void
.end method

.method protected bridge synthetic extractScope(Ljava/lang/Object;)Lmortar/MortarScope;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 29
    .local p0, "this":Lmortar/PopupPresenter;, "Lmortar/PopupPresenter<TD;TR;>;"
    check-cast p1, Lmortar/Popup;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lmortar/PopupPresenter;->extractScope(Lmortar/Popup;)Lmortar/MortarScope;

    move-result-object v0

    return-object v0
.end method

.method protected extractScope(Lmortar/Popup;)Lmortar/MortarScope;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmortar/Popup",
            "<TD;TR;>;)",
            "Lmortar/MortarScope;"
        }
    .end annotation

    .prologue
    .line 84
    .local p0, "this":Lmortar/PopupPresenter;, "Lmortar/PopupPresenter<TD;TR;>;"
    .local p1, "view":Lmortar/Popup;, "Lmortar/Popup<TD;TR;>;"
    invoke-interface {p1}, Lmortar/Popup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lmortar/Mortar;->getScope(Landroid/content/Context;)Lmortar/MortarScope;

    move-result-object v0

    return-object v0
.end method

.method public final onDismissed(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation

    .prologue
    .line 77
    .local p0, "this":Lmortar/PopupPresenter;, "Lmortar/PopupPresenter<TD;TR;>;"
    .local p1, "result":Ljava/lang/Object;, "TR;"
    const/4 v0, 0x0

    iput-object v0, p0, Lmortar/PopupPresenter;->whatToShow:Landroid/os/Parcelable;

    .line 78
    invoke-virtual {p0, p1}, Lmortar/PopupPresenter;->onPopupResult(Ljava/lang/Object;)V

    .line 79
    return-void
.end method

.method public onExitScope()V
    .locals 2

    .prologue
    .line 115
    .local p0, "this":Lmortar/PopupPresenter;, "Lmortar/PopupPresenter<TD;TR;>;"
    invoke-virtual {p0}, Lmortar/PopupPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmortar/Popup;

    .line 116
    .local v0, "popUp":Lmortar/Popup;, "Lmortar/Popup<TD;TR;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lmortar/Popup;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lmortar/Popup;->dismiss(Z)V

    .line 117
    :cond_0
    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 94
    .local p0, "this":Lmortar/PopupPresenter;, "Lmortar/PopupPresenter<TD;TR;>;"
    iget-object v1, p0, Lmortar/PopupPresenter;->whatToShow:Landroid/os/Parcelable;

    if-nez v1, :cond_0

    if-eqz p1, :cond_0

    .line 95
    iget-object v1, p0, Lmortar/PopupPresenter;->whatToShowKey:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    iput-object v1, p0, Lmortar/PopupPresenter;->whatToShow:Landroid/os/Parcelable;

    .line 98
    :cond_0
    iget-object v1, p0, Lmortar/PopupPresenter;->whatToShow:Landroid/os/Parcelable;

    if-nez v1, :cond_2

    .line 106
    :cond_1
    :goto_0
    return-void

    .line 100
    :cond_2
    invoke-virtual {p0}, Lmortar/PopupPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmortar/Popup;

    .line 101
    .local v0, "view":Lmortar/Popup;, "Lmortar/Popup<TD;TR;>;"
    if-eqz v0, :cond_1

    .line 103
    invoke-interface {v0}, Lmortar/Popup;->isShowing()Z

    move-result v1

    if-nez v1, :cond_1

    .line 104
    iget-object v1, p0, Lmortar/PopupPresenter;->whatToShow:Landroid/os/Parcelable;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2, p0}, Lmortar/Popup;->show(Landroid/os/Parcelable;ZLmortar/PopupPresenter;)V

    goto :goto_0
.end method

.method protected abstract onPopupResult(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 109
    .local p0, "this":Lmortar/PopupPresenter;, "Lmortar/PopupPresenter<TD;TR;>;"
    iget-object v0, p0, Lmortar/PopupPresenter;->whatToShow:Landroid/os/Parcelable;

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lmortar/PopupPresenter;->whatToShowKey:Ljava/lang/String;

    iget-object v1, p0, Lmortar/PopupPresenter;->whatToShow:Landroid/os/Parcelable;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 112
    :cond_0
    return-void
.end method

.method public show(Landroid/os/Parcelable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TD;)V"
        }
    .end annotation

    .prologue
    .line 53
    .local p0, "this":Lmortar/PopupPresenter;, "Lmortar/PopupPresenter<TD;TR;>;"
    .local p1, "info":Landroid/os/Parcelable;, "TD;"
    iget-object v1, p0, Lmortar/PopupPresenter;->whatToShow:Landroid/os/Parcelable;

    if-eq v1, p1, :cond_0

    iget-object v1, p0, Lmortar/PopupPresenter;->whatToShow:Landroid/os/Parcelable;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmortar/PopupPresenter;->whatToShow:Landroid/os/Parcelable;

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 63
    :cond_0
    :goto_0
    return-void

    .line 59
    :cond_1
    iput-object p1, p0, Lmortar/PopupPresenter;->whatToShow:Landroid/os/Parcelable;

    .line 60
    invoke-virtual {p0}, Lmortar/PopupPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmortar/Popup;

    .line 61
    .local v0, "view":Lmortar/Popup;, "Lmortar/Popup<TD;TR;>;"
    if-eqz v0, :cond_0

    .line 62
    iget-object v1, p0, Lmortar/PopupPresenter;->whatToShow:Landroid/os/Parcelable;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2, p0}, Lmortar/Popup;->show(Landroid/os/Parcelable;ZLmortar/PopupPresenter;)V

    goto :goto_0
.end method

.method public showing()Landroid/os/Parcelable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TD;"
        }
    .end annotation

    .prologue
    .line 49
    .local p0, "this":Lmortar/PopupPresenter;, "Lmortar/PopupPresenter<TD;TR;>;"
    iget-object v0, p0, Lmortar/PopupPresenter;->whatToShow:Landroid/os/Parcelable;

    return-object v0
.end method
