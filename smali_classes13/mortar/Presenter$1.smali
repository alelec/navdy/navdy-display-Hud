.class Lmortar/Presenter$1;
.super Ljava/lang/Object;
.source "Presenter.java"

# interfaces
.implements Lmortar/Bundler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmortar/Presenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmortar/Presenter;


# direct methods
.method constructor <init>(Lmortar/Presenter;)V
    .locals 0

    .prologue
    .line 26
    .local p0, "this":Lmortar/Presenter$1;, "Lmortar/Presenter.1;"
    iput-object p1, p0, Lmortar/Presenter$1;->this$0:Lmortar/Presenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    .local p0, "this":Lmortar/Presenter$1;, "Lmortar/Presenter.1;"
    iget-object v0, p0, Lmortar/Presenter$1;->this$0:Lmortar/Presenter;

    invoke-virtual {v0}, Lmortar/Presenter;->getMortarBundleKey()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1
    .param p1, "scope"    # Lmortar/MortarScope;

    .prologue
    .line 43
    .local p0, "this":Lmortar/Presenter$1;, "Lmortar/Presenter.1;"
    iget-object v0, p0, Lmortar/Presenter$1;->this$0:Lmortar/Presenter;

    invoke-virtual {v0, p1}, Lmortar/Presenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 44
    return-void
.end method

.method public onExitScope()V
    .locals 1

    .prologue
    .line 47
    .local p0, "this":Lmortar/Presenter$1;, "Lmortar/Presenter.1;"
    iget-object v0, p0, Lmortar/Presenter$1;->this$0:Lmortar/Presenter;

    invoke-virtual {v0}, Lmortar/Presenter;->onExitScope()V

    .line 48
    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 32
    .local p0, "this":Lmortar/Presenter$1;, "Lmortar/Presenter.1;"
    iget-object v0, p0, Lmortar/Presenter$1;->this$0:Lmortar/Presenter;

    invoke-virtual {v0}, Lmortar/Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmortar/Presenter$1;->this$0:Lmortar/Presenter;

    # getter for: Lmortar/Presenter;->loaded:Z
    invoke-static {v0}, Lmortar/Presenter;->access$000(Lmortar/Presenter;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 33
    iget-object v0, p0, Lmortar/Presenter$1;->this$0:Lmortar/Presenter;

    const/4 v1, 0x1

    # setter for: Lmortar/Presenter;->loaded:Z
    invoke-static {v0, v1}, Lmortar/Presenter;->access$002(Lmortar/Presenter;Z)Z

    .line 34
    iget-object v0, p0, Lmortar/Presenter$1;->this$0:Lmortar/Presenter;

    invoke-virtual {v0, p1}, Lmortar/Presenter;->onLoad(Landroid/os/Bundle;)V

    .line 36
    :cond_0
    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 39
    .local p0, "this":Lmortar/Presenter$1;, "Lmortar/Presenter.1;"
    iget-object v0, p0, Lmortar/Presenter$1;->this$0:Lmortar/Presenter;

    invoke-virtual {v0, p1}, Lmortar/Presenter;->onSave(Landroid/os/Bundle;)V

    .line 40
    return-void
.end method
