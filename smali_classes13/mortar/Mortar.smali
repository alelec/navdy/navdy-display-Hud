.class public Lmortar/Mortar;
.super Ljava/lang/Object;
.source "Mortar.java"


# static fields
.field static final MORTAR_SCOPE_SERVICE:Ljava/lang/String; = "mortar_scope"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    return-void
.end method

.method public static createRootScope(Z)Lmortar/MortarScope;
    .locals 2
    .param p0, "validate"    # Z

    .prologue
    .line 37
    new-instance v0, Lmortar/RealScope;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v1}, Ldagger/ObjectGraph;->create([Ljava/lang/Object;)Ldagger/ObjectGraph;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lmortar/RealScope;-><init>(ZLdagger/ObjectGraph;)V

    return-object v0
.end method

.method public static createRootScope(ZLdagger/ObjectGraph;)Lmortar/MortarScope;
    .locals 1
    .param p0, "validate"    # Z
    .param p1, "objectGraph"    # Ldagger/ObjectGraph;

    .prologue
    .line 45
    new-instance v0, Lmortar/RealScope;

    invoke-direct {v0, p0, p1}, Lmortar/RealScope;-><init>(ZLdagger/ObjectGraph;)V

    return-object v0
.end method

.method public static destroyRootScope(Lmortar/MortarScope;)V
    .locals 6
    .param p0, "rootScope"    # Lmortar/MortarScope;

    .prologue
    .line 80
    move-object v0, p0

    check-cast v0, Lmortar/RealScope;

    .line 81
    .local v0, "realScope":Lmortar/RealScope;
    invoke-virtual {v0}, Lmortar/RealScope;->isRoot()Z

    move-result v1

    if-nez v1, :cond_0

    .line 82
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "%s is not a root"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Lmortar/RealScope;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 84
    :cond_0
    invoke-virtual {v0}, Lmortar/RealScope;->doDestroy()V

    .line 85
    return-void
.end method

.method public static getScope(Landroid/content/Context;)Lmortar/MortarScope;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 102
    const-string v1, "mortar_scope"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmortar/MortarScope;

    .line 103
    .local v0, "scope":Lmortar/MortarScope;
    if-nez v0, :cond_0

    .line 104
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Cannot find scope in %s. Make sure your Activity overrides getSystemService()  to return its scope if isScopeSystemService() is true"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 110
    :cond_0
    return-object v0
.end method

.method public static inject(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 93
    invoke-static {p0}, Lmortar/Mortar;->getScope(Landroid/content/Context;)Lmortar/MortarScope;

    move-result-object v0

    invoke-interface {v0}, Lmortar/MortarScope;->getObjectGraph()Ldagger/ObjectGraph;

    move-result-object v0

    invoke-virtual {v0, p1}, Ldagger/ObjectGraph;->inject(Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    return-void
.end method

.method public static isScopeSystemService(Ljava/lang/String;)Z
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 118
    const-string v0, "mortar_scope"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static requireActivityScope(Lmortar/MortarScope;Lmortar/Blueprint;)Lmortar/MortarActivityScope;
    .locals 5
    .param p0, "parentScope"    # Lmortar/MortarScope;
    .param p1, "blueprint"    # Lmortar/Blueprint;

    .prologue
    .line 60
    invoke-interface {p1}, Lmortar/Blueprint;->getMortarScopeName()Ljava/lang/String;

    move-result-object v1

    .line 61
    .local v1, "name":Ljava/lang/String;
    invoke-interface {p0, p1}, Lmortar/MortarScope;->requireChild(Lmortar/Blueprint;)Lmortar/MortarScope;

    move-result-object v3

    check-cast v3, Lmortar/RealScope;

    .line 64
    .local v3, "unwrapped":Lmortar/RealScope;
    instance-of v4, v3, Lmortar/MortarActivityScope;

    if-eqz v4, :cond_0

    move-object v0, v3

    .line 65
    check-cast v0, Lmortar/RealActivityScope;

    .line 72
    .local v0, "activityScope":Lmortar/RealActivityScope;
    :goto_0
    return-object v0

    .end local v0    # "activityScope":Lmortar/RealActivityScope;
    :cond_0
    move-object v2, p0

    .line 67
    check-cast v2, Lmortar/RealScope;

    .line 68
    .local v2, "realParentScope":Lmortar/RealScope;
    new-instance v0, Lmortar/RealActivityScope;

    invoke-direct {v0, v3}, Lmortar/RealActivityScope;-><init>(Lmortar/RealScope;)V

    .line 69
    .restart local v0    # "activityScope":Lmortar/RealActivityScope;
    invoke-virtual {v2, v1, v0}, Lmortar/RealScope;->replaceChild(Ljava/lang/String;Lmortar/RealScope;)V

    goto :goto_0
.end method
