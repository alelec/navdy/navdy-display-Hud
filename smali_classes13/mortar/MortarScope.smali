.class public interface abstract Lmortar/MortarScope;
.super Ljava/lang/Object;
.source "MortarScope.java"


# static fields
.field public static final ROOT_NAME:Ljava/lang/String; = "Root"


# virtual methods
.method public abstract createContext(Landroid/content/Context;)Landroid/content/Context;
.end method

.method public abstract destroyChild(Lmortar/MortarScope;)V
.end method

.method public abstract findChild(Ljava/lang/String;)Lmortar/MortarScope;
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getObjectGraph()Ldagger/ObjectGraph;
.end method

.method public abstract isDestroyed()Z
.end method

.method public abstract register(Lmortar/Scoped;)V
.end method

.method public abstract requireChild(Lmortar/Blueprint;)Lmortar/MortarScope;
.end method
