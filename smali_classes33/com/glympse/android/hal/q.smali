.class Lcom/glympse/android/hal/q;
.super Ljava/lang/Object;
.source "LocalContactsProvider.java"

# interfaces
.implements Lcom/glympse/android/hal/GContactsProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/hal/q$c;,
        Lcom/glympse/android/hal/q$b;,
        Lcom/glympse/android/hal/q$a;
    }
.end annotation


# instance fields
.field private R:Ljava/util/concurrent/Future;

.field private _handler:Lcom/glympse/android/core/GHandler;

.field private bX:Lcom/glympse/android/hal/GContactsListener;

.field private bY:Lcom/glympse/android/hal/q$c;

.field private bZ:Lcom/glympse/android/hal/GVector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GVector",
            "<",
            "Lcom/glympse/android/lib/GPerson;",
            ">;"
        }
    .end annotation
.end field

.field private ca:Z

.field private cb:Lcom/glympse/android/hal/q$a;

.field protected e:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/glympse/android/hal/q;->ca:Z

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/hal/q;->cb:Lcom/glympse/android/hal/q$a;

    .line 54
    iput-object p1, p0, Lcom/glympse/android/hal/q;->e:Landroid/content/Context;

    .line 55
    new-instance v0, Lcom/glympse/android/hal/GVector;

    invoke-direct {v0}, Lcom/glympse/android/hal/GVector;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/hal/q;->bZ:Lcom/glympse/android/hal/GVector;

    .line 56
    return-void
.end method

.method static synthetic a(Lcom/glympse/android/hal/q;)Lcom/glympse/android/core/GHandler;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/glympse/android/hal/q;->_handler:Lcom/glympse/android/core/GHandler;

    return-object v0
.end method

.method static synthetic a(Lcom/glympse/android/hal/q;Z)Z
    .locals 0

    .prologue
    .line 28
    iput-boolean p1, p0, Lcom/glympse/android/hal/q;->ca:Z

    return p1
.end method


# virtual methods
.method protected complete(Lcom/glympse/android/hal/GVector;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/glympse/android/hal/GVector",
            "<",
            "Lcom/glympse/android/lib/GPerson;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 215
    iput-object p1, p0, Lcom/glympse/android/hal/q;->bZ:Lcom/glympse/android/hal/GVector;

    .line 216
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/hal/q;->bY:Lcom/glympse/android/hal/q$c;

    .line 217
    iget-object v0, p0, Lcom/glympse/android/hal/q;->bX:Lcom/glympse/android/hal/GContactsListener;

    if-eqz v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/glympse/android/hal/q;->bX:Lcom/glympse/android/hal/GContactsListener;

    invoke-interface {v0, p0}, Lcom/glympse/android/hal/GContactsListener;->contactsProviderUpdateComplete(Lcom/glympse/android/hal/GContactsProvider;)V

    .line 221
    :cond_0
    return-void
.end method

.method public getPeople()Lcom/glympse/android/hal/GVector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/glympse/android/hal/GVector",
            "<",
            "Lcom/glympse/android/lib/GPerson;",
            ">;"
        }
    .end annotation

    .prologue
    .line 179
    iget-object v0, p0, Lcom/glympse/android/hal/q;->bZ:Lcom/glympse/android/hal/GVector;

    return-object v0
.end method

.method public loadAvatar(J)Lcom/glympse/android/core/GDrawable;
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 185
    iget-object v1, p0, Lcom/glympse/android/hal/q;->e:Landroid/content/Context;

    const-string v2, "android.permission.READ_CONTACTS"

    invoke-static {v1, v2}, Lcom/glympse/android/hal/h;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 210
    :cond_0
    :goto_0
    return-object v0

    .line 191
    :cond_1
    invoke-static {}, Lcom/glympse/android/hal/Reflection$_Contacts;->CONTENT_URI()Landroid/net/Uri;

    move-result-object v1

    .line 192
    if-eqz v1, :cond_0

    .line 194
    invoke-static {v1, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 195
    iget-object v2, p0, Lcom/glympse/android/hal/q;->e:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v2, v1, v3}, Lcom/glympse/android/hal/Reflection$_Contacts;->openContactPhotoInputStream(Landroid/content/ContentResolver;Landroid/net/Uri;Z)Ljava/io/InputStream;

    move-result-object v1

    .line 196
    if-eqz v1, :cond_0

    .line 198
    invoke-static {v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 201
    :try_start_0
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 207
    :goto_1
    if-eqz v2, :cond_0

    new-instance v0, Lcom/glympse/android/hal/j;

    invoke-direct {v0, v2}, Lcom/glympse/android/hal/j;-><init>(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 203
    :catch_0
    move-exception v1

    .line 205
    invoke-static {v1, v3}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    goto :goto_1
.end method

.method public refresh()V
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lcom/glympse/android/hal/q;->bY:Lcom/glympse/android/hal/q$c;

    if-eqz v0, :cond_0

    .line 129
    :goto_0
    return-void

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/hal/q;->e:Landroid/content/Context;

    const-string v1, "android.permission.READ_CONTACTS"

    invoke-static {v0, v1}, Lcom/glympse/android/hal/h;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 122
    new-instance v0, Lcom/glympse/android/hal/GVector;

    invoke-direct {v0}, Lcom/glympse/android/hal/GVector;-><init>()V

    invoke-virtual {p0, v0}, Lcom/glympse/android/hal/q;->complete(Lcom/glympse/android/hal/GVector;)V

    goto :goto_0

    .line 127
    :cond_1
    new-instance v0, Lcom/glympse/android/hal/q$c;

    invoke-direct {v0, p0}, Lcom/glympse/android/hal/q$c;-><init>(Lcom/glympse/android/hal/q;)V

    iput-object v0, p0, Lcom/glympse/android/hal/q;->bY:Lcom/glympse/android/hal/q$c;

    .line 128
    invoke-static {}, Lcom/glympse/android/hal/GlympseThreadPool;->instance()Lcom/glympse/android/hal/GlympseThreadPool;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/hal/q;->bY:Lcom/glympse/android/hal/q$c;

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GlympseThreadPool;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/hal/q;->R:Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public setActive(Z)V
    .locals 1

    .prologue
    .line 89
    if-eqz p1, :cond_1

    .line 92
    invoke-virtual {p0}, Lcom/glympse/android/hal/q;->y()V

    .line 95
    iget-boolean v0, p0, Lcom/glympse/android/hal/q;->ca:Z

    if-eqz v0, :cond_0

    .line 98
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/glympse/android/hal/q;->ca:Z

    .line 101
    invoke-virtual {p0}, Lcom/glympse/android/hal/q;->refresh()V

    .line 109
    :cond_0
    :goto_0
    return-void

    .line 107
    :cond_1
    invoke-virtual {p0}, Lcom/glympse/android/hal/q;->x()V

    goto :goto_0
.end method

.method public start(Lcom/glympse/android/hal/GContactsListener;Lcom/glympse/android/core/GHandler;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/glympse/android/hal/q;->bX:Lcom/glympse/android/hal/GContactsListener;

    .line 61
    iput-object p2, p0, Lcom/glympse/android/hal/q;->_handler:Lcom/glympse/android/core/GHandler;

    .line 64
    invoke-static {}, Lcom/glympse/android/hal/g;->l()V

    .line 67
    invoke-virtual {p0}, Lcom/glympse/android/hal/q;->refresh()V

    .line 68
    return-void
.end method

.method public stop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 72
    iget-object v0, p0, Lcom/glympse/android/hal/q;->bY:Lcom/glympse/android/hal/q$c;

    if-eqz v0, :cond_0

    .line 76
    :try_start_0
    iget-object v0, p0, Lcom/glympse/android/hal/q;->R:Ljava/util/concurrent/Future;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 81
    :goto_0
    iput-object v2, p0, Lcom/glympse/android/hal/q;->R:Ljava/util/concurrent/Future;

    .line 82
    iput-object v2, p0, Lcom/glympse/android/hal/q;->bY:Lcom/glympse/android/hal/q$c;

    .line 84
    :cond_0
    iput-object v2, p0, Lcom/glympse/android/hal/q;->bX:Lcom/glympse/android/hal/GContactsListener;

    .line 85
    return-void

    .line 78
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public x()V
    .locals 4

    .prologue
    .line 133
    iget-object v0, p0, Lcom/glympse/android/hal/q;->cb:Lcom/glympse/android/hal/q$a;

    if-nez v0, :cond_0

    .line 135
    invoke-static {}, Lcom/glympse/android/hal/Reflection$_Contacts;->CONTENT_URI()Landroid/net/Uri;

    move-result-object v0

    .line 136
    if-eqz v0, :cond_0

    .line 138
    new-instance v1, Lcom/glympse/android/hal/q$a;

    invoke-direct {v1, p0}, Lcom/glympse/android/hal/q$a;-><init>(Lcom/glympse/android/hal/q;)V

    iput-object v1, p0, Lcom/glympse/android/hal/q;->cb:Lcom/glympse/android/hal/q$a;

    .line 139
    iget-object v1, p0, Lcom/glympse/android/hal/q;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/glympse/android/hal/q;->cb:Lcom/glympse/android/hal/q$a;

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 142
    :cond_0
    return-void
.end method

.method public y()V
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lcom/glympse/android/hal/q;->cb:Lcom/glympse/android/hal/q$a;

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/glympse/android/hal/q;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/hal/q;->cb:Lcom/glympse/android/hal/q$a;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 149
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/hal/q;->cb:Lcom/glympse/android/hal/q$a;

    .line 151
    :cond_0
    return-void
.end method
