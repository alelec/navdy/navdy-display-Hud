.class Lcom/glympse/android/hal/v;
.super Ljava/lang/Object;
.source "ProximityProviderGms.java"

# interfaces
.implements Lcom/glympse/android/core/GProximityProvider;
.implements Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
.implements Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;
.implements Lcom/glympse/android/hal/gms/location/LocationClient$OnAddGeofencesResultListener;
.implements Lcom/glympse/android/hal/gms/location/LocationClient$OnRemoveGeofencesResultListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/hal/v$a;
    }
.end annotation


# instance fields
.field private D:Landroid/app/PendingIntent;

.field private cE:Lcom/glympse/android/hal/gms/location/LocationClient;

.field private cV:Lcom/glympse/android/core/GProximityListener;

.field private cW:Lcom/glympse/android/hal/GHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GHashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/glympse/android/core/GRegion;",
            ">;"
        }
    .end annotation
.end field

.field private dc:Lcom/glympse/android/hal/v$a;

.field private e:Landroid/content/Context;

.field private v:Z

.field private w:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/glympse/android/hal/v;->e:Landroid/content/Context;

    .line 57
    new-instance v0, Lcom/glympse/android/hal/GHashtable;

    invoke-direct {v0}, Lcom/glympse/android/hal/GHashtable;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/hal/v;->cW:Lcom/glympse/android/hal/GHashtable;

    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/glympse/android/hal/v;->v:Z

    .line 64
    invoke-direct {p0, p1}, Lcom/glympse/android/hal/v;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/hal/v;->w:Ljava/lang/String;

    .line 65
    iget-object v0, p0, Lcom/glympse/android/hal/v;->w:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/glympse/android/hal/v;->e(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/hal/v;->D:Landroid/app/PendingIntent;

    .line 66
    return-void
.end method

.method private N()V
    .locals 3

    .prologue
    .line 286
    iget-object v0, p0, Lcom/glympse/android/hal/v;->cW:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GHashtable;->size()I

    move-result v0

    .line 287
    if-nez v0, :cond_0

    .line 302
    :goto_0
    return-void

    .line 293
    :cond_0
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1, v0}, Ljava/util/Vector;-><init>(I)V

    .line 294
    iget-object v0, p0, Lcom/glympse/android/hal/v;->cW:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GHashtable;->values()Ljava/util/Collection;

    move-result-object v0

    .line 295
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GRegion;

    .line 297
    invoke-direct {p0, v0}, Lcom/glympse/android/hal/v;->b(Lcom/glympse/android/core/GRegion;)Lcom/glympse/android/hal/gms/location/Geofence;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_1

    .line 301
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/hal/v;->cE:Lcom/glympse/android/hal/gms/location/LocationClient;

    iget-object v2, p0, Lcom/glympse/android/hal/v;->D:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1, v2, p0}, Lcom/glympse/android/hal/gms/location/LocationClient;->addGeofences(Ljava/util/List;Landroid/app/PendingIntent;Lcom/glympse/android/hal/gms/location/LocationClient$OnAddGeofencesResultListener;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/glympse/android/hal/v;)Lcom/glympse/android/core/GProximityListener;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/glympse/android/hal/v;->cV:Lcom/glympse/android/core/GProximityListener;

    return-object v0
.end method

.method static synthetic b(Lcom/glympse/android/hal/v;)Lcom/glympse/android/hal/GHashtable;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/glympse/android/hal/v;->cW:Lcom/glympse/android/hal/GHashtable;

    return-object v0
.end method

.method private b(Lcom/glympse/android/core/GRegion;)Lcom/glympse/android/hal/gms/location/Geofence;
    .locals 8

    .prologue
    .line 273
    new-instance v1, Lcom/glympse/android/hal/gms/location/Geofence$Builder;

    invoke-direct {v1}, Lcom/glympse/android/hal/gms/location/Geofence$Builder;-><init>()V

    .line 274
    invoke-interface {p1}, Lcom/glympse/android/core/GRegion;->getLatitude()D

    move-result-wide v2

    invoke-interface {p1}, Lcom/glympse/android/core/GRegion;->getLongitude()D

    move-result-wide v4

    invoke-interface {p1}, Lcom/glympse/android/core/GRegion;->getRadius()D

    move-result-wide v6

    double-to-float v6, v6

    invoke-virtual/range {v1 .. v6}, Lcom/glympse/android/hal/gms/location/Geofence$Builder;->setCircularRegion(DDF)Lcom/glympse/android/hal/gms/location/Geofence$Builder;

    .line 275
    invoke-interface {p1}, Lcom/glympse/android/core/GRegion;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/glympse/android/hal/gms/location/Geofence$Builder;->setRequestId(Ljava/lang/String;)Lcom/glympse/android/hal/gms/location/Geofence$Builder;

    .line 276
    const-wide/16 v2, -0x1

    invoke-virtual {v1, v2, v3}, Lcom/glympse/android/hal/gms/location/Geofence$Builder;->setExpirationDuration(J)Lcom/glympse/android/hal/gms/location/Geofence$Builder;

    .line 277
    const/4 v0, 0x3

    invoke-virtual {v1, v0}, Lcom/glympse/android/hal/gms/location/Geofence$Builder;->setTransitionTypes(I)Lcom/glympse/android/hal/gms/location/Geofence$Builder;

    .line 278
    invoke-virtual {v1}, Lcom/glympse/android/hal/gms/location/Geofence$Builder;->build()Lcom/glympse/android/hal/gms/location/Geofence;

    move-result-object v0

    return-object v0
.end method

.method private connect()V
    .locals 4

    .prologue
    .line 226
    iget-object v0, p0, Lcom/glympse/android/hal/v;->cE:Lcom/glympse/android/hal/gms/location/LocationClient;

    if-nez v0, :cond_0

    .line 228
    iget-object v0, p0, Lcom/glympse/android/hal/v;->e:Landroid/content/Context;

    invoke-static {v0, p0, p0}, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->createLocationClient(Landroid/content/Context;Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)Lcom/glympse/android/hal/gms/location/LocationClient;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/hal/v;->cE:Lcom/glympse/android/hal/gms/location/LocationClient;

    .line 229
    iget-object v0, p0, Lcom/glympse/android/hal/v;->cE:Lcom/glympse/android/hal/gms/location/LocationClient;

    invoke-virtual {v0}, Lcom/glympse/android/hal/gms/location/LocationClient;->connect()V

    .line 233
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/hal/v;->dc:Lcom/glympse/android/hal/v$a;

    if-nez v0, :cond_1

    .line 235
    new-instance v0, Lcom/glympse/android/hal/v$a;

    invoke-direct {v0, p0}, Lcom/glympse/android/hal/v$a;-><init>(Lcom/glympse/android/hal/v;)V

    iput-object v0, p0, Lcom/glympse/android/hal/v;->dc:Lcom/glympse/android/hal/v$a;

    .line 236
    iget-object v0, p0, Lcom/glympse/android/hal/v;->e:Landroid/content/Context;

    iget-object v1, p0, Lcom/glympse/android/hal/v;->dc:Lcom/glympse/android/hal/v$a;

    new-instance v2, Landroid/content/IntentFilter;

    iget-object v3, p0, Lcom/glympse/android/hal/v;->w:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 238
    :cond_1
    return-void
.end method

.method private disconnect()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 243
    iget-object v0, p0, Lcom/glympse/android/hal/v;->dc:Lcom/glympse/android/hal/v$a;

    if-eqz v0, :cond_0

    .line 245
    iget-object v0, p0, Lcom/glympse/android/hal/v;->e:Landroid/content/Context;

    iget-object v1, p0, Lcom/glympse/android/hal/v;->dc:Lcom/glympse/android/hal/v$a;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 246
    iput-object v2, p0, Lcom/glympse/android/hal/v;->dc:Lcom/glympse/android/hal/v$a;

    .line 250
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/hal/v;->cE:Lcom/glympse/android/hal/gms/location/LocationClient;

    if-eqz v0, :cond_1

    .line 252
    iget-object v0, p0, Lcom/glympse/android/hal/v;->cE:Lcom/glympse/android/hal/gms/location/LocationClient;

    invoke-virtual {v0}, Lcom/glympse/android/hal/gms/location/LocationClient;->disconnect()V

    .line 253
    iput-object v2, p0, Lcom/glympse/android/hal/v;->cE:Lcom/glympse/android/hal/gms/location/LocationClient;

    .line 254
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/glympse/android/hal/v;->v:Z

    .line 256
    :cond_1
    return-void
.end method

.method private e(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 265
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 266
    iget-object v1, p0, Lcom/glympse/android/hal/v;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 267
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 268
    iget-object v1, p0, Lcom/glympse/android/hal/v;->e:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private g(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 260
    const-string v0, "com.glympse.android.hal.proximity.REGION"

    return-object v0
.end method

.method public static isSupported(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 48
    .line 49
    invoke-static {p0}, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->isSupported(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    invoke-static {p0}, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->isGeofencingSupported(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "android.permission.ACCESS_FINE_LOCATION"

    .line 51
    invoke-static {p0, v0}, Lcom/glympse/android/hal/h;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 48
    :goto_0
    return v0

    .line 51
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public detachRegions()Lcom/glympse/android/core/GArray;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/glympse/android/core/GArray",
            "<",
            "Lcom/glympse/android/core/GRegion;",
            ">;"
        }
    .end annotation

    .prologue
    .line 140
    new-instance v1, Lcom/glympse/android/hal/GVector;

    iget-object v0, p0, Lcom/glympse/android/hal/v;->cW:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GHashtable;->size()I

    move-result v0

    invoke-direct {v1, v0}, Lcom/glympse/android/hal/GVector;-><init>(I)V

    .line 141
    iget-object v0, p0, Lcom/glympse/android/hal/v;->cW:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GHashtable;->values()Ljava/util/Collection;

    move-result-object v0

    .line 142
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GRegion;

    .line 144
    invoke-virtual {v1, v0}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    goto :goto_0

    .line 148
    :cond_0
    invoke-virtual {v1}, Lcom/glympse/android/hal/GVector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GRegion;

    .line 150
    invoke-virtual {p0, v0}, Lcom/glympse/android/hal/v;->stopMonitoring(Lcom/glympse/android/core/GRegion;)V

    goto :goto_1

    .line 152
    :cond_1
    return-object v1
.end method

.method public locationChanged(Lcom/glympse/android/core/GLocation;)V
    .locals 0

    .prologue
    .line 135
    return-void
.end method

.method public onAddGeofencesResult(I)V
    .locals 0

    .prologue
    .line 310
    sparse-switch p1, :sswitch_data_0

    .line 332
    :sswitch_0
    return-void

    .line 310
    :sswitch_data_0
    .sparse-switch
        0xd -> :sswitch_0
        0x3e8 -> :sswitch_0
        0x3e9 -> :sswitch_0
        0x3ea -> :sswitch_0
    .end sparse-switch
.end method

.method public onConnected(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 354
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/glympse/android/hal/v;->v:Z

    .line 357
    invoke-direct {p0}, Lcom/glympse/android/hal/v;->N()V

    .line 358
    return-void
.end method

.method public onConnectionFailed(Lcom/glympse/android/hal/gms/common/ConnectionResult;)V
    .locals 0

    .prologue
    .line 377
    return-void
.end method

.method public onDisconnected()V
    .locals 1

    .prologue
    .line 362
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/glympse/android/hal/v;->v:Z

    .line 363
    return-void
.end method

.method public onNetworkLost()V
    .locals 1

    .prologue
    .line 367
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/glympse/android/hal/v;->v:Z

    .line 368
    return-void
.end method

.method public onRemoveGeofencesByPendingIntentResult(ILandroid/app/PendingIntent;)V
    .locals 0

    .prologue
    .line 341
    return-void
.end method

.method public onRemoveGeofencesByRequestIdsResult(I)V
    .locals 0

    .prologue
    .line 346
    return-void
.end method

.method public setProximityListener(Lcom/glympse/android/core/GProximityListener;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/glympse/android/hal/v;->cV:Lcom/glympse/android/core/GProximityListener;

    .line 75
    return-void
.end method

.method public startMonitoring(Lcom/glympse/android/core/GArray;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/glympse/android/core/GArray",
            "<",
            "Lcom/glympse/android/core/GRegion;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 104
    invoke-interface {p1}, Lcom/glympse/android/core/GArray;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GRegion;

    .line 106
    invoke-virtual {p0, v0}, Lcom/glympse/android/hal/v;->startMonitoring(Lcom/glympse/android/core/GRegion;)V

    goto :goto_0

    .line 108
    :cond_0
    return-void
.end method

.method public startMonitoring(Lcom/glympse/android/core/GRegion;)V
    .locals 3

    .prologue
    .line 79
    iget-object v0, p0, Lcom/glympse/android/hal/v;->cW:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 100
    :cond_0
    :goto_0
    return-void

    .line 86
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/hal/v;->cW:Lcom/glympse/android/hal/GHashtable;

    invoke-interface {p1}, Lcom/glympse/android/core/GRegion;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    invoke-direct {p0}, Lcom/glympse/android/hal/v;->connect()V

    .line 92
    iget-boolean v0, p0, Lcom/glympse/android/hal/v;->v:Z

    if-eqz v0, :cond_0

    .line 95
    new-instance v0, Ljava/util/Vector;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/Vector;-><init>(I)V

    .line 96
    invoke-direct {p0, p1}, Lcom/glympse/android/hal/v;->b(Lcom/glympse/android/core/GRegion;)Lcom/glympse/android/hal/gms/location/Geofence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 98
    iget-object v1, p0, Lcom/glympse/android/hal/v;->cE:Lcom/glympse/android/hal/gms/location/LocationClient;

    iget-object v2, p0, Lcom/glympse/android/hal/v;->D:Landroid/app/PendingIntent;

    invoke-virtual {v1, v0, v2, p0}, Lcom/glympse/android/hal/gms/location/LocationClient;->addGeofences(Ljava/util/List;Landroid/app/PendingIntent;Lcom/glympse/android/hal/gms/location/LocationClient$OnAddGeofencesResultListener;)V

    goto :goto_0
.end method

.method public stopMonitoring(Lcom/glympse/android/core/GRegion;)V
    .locals 2

    .prologue
    .line 112
    iget-boolean v0, p0, Lcom/glympse/android/hal/v;->v:Z

    if-eqz v0, :cond_0

    .line 114
    new-instance v0, Ljava/util/Vector;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/Vector;-><init>(I)V

    .line 115
    invoke-interface {p1}, Lcom/glympse/android/core/GRegion;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 118
    iget-object v1, p0, Lcom/glympse/android/hal/v;->cE:Lcom/glympse/android/hal/gms/location/LocationClient;

    invoke-virtual {v1, v0, p0}, Lcom/glympse/android/hal/gms/location/LocationClient;->removeGeofences(Ljava/util/List;Lcom/glympse/android/hal/gms/location/LocationClient$OnRemoveGeofencesResultListener;)V

    .line 122
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/hal/v;->cW:Lcom/glympse/android/hal/GHashtable;

    invoke-interface {p1}, Lcom/glympse/android/core/GRegion;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GHashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    iget-object v0, p0, Lcom/glympse/android/hal/v;->cW:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GHashtable;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 127
    invoke-direct {p0}, Lcom/glympse/android/hal/v;->disconnect()V

    .line 129
    :cond_1
    return-void
.end method
