.class Lcom/glympse/android/hal/b;
.super Lcom/glympse/android/hal/t;
.source "AccountNotifier.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/hal/b$a;
    }
.end annotation


# static fields
.field public static final INTENT_EXTRA_FLAGS:Ljava/lang/String; = "flags"

.field public static final n:Ljava/lang/String; = "com.glympse.android.hal.acc.CHANGED"

.field public static final o:Ljava/lang/String; = "account"

.field public static final p:Ljava/lang/String; = "package"


# instance fields
.field private e:Landroid/content/Context;

.field private q:Lcom/glympse/android/lib/GGlympsePrivate;

.field private r:Lcom/glympse/android/hal/b$a;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/glympse/android/hal/t;-><init>()V

    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 67
    :try_start_0
    iget-object v0, p0, Lcom/glympse/android/hal/b;->cS:Lcom/glympse/android/api/GGlympse;

    if-nez v0, :cond_1

    .line 99
    :cond_0
    :goto_0
    return-void

    .line 73
    :cond_1
    const-string v0, "package"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 74
    iget-object v1, p0, Lcom/glympse/android/hal/b;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 80
    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 81
    invoke-direct {p0}, Lcom/glympse/android/hal/b;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    const-string v0, "flags"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 88
    if-eqz v0, :cond_0

    .line 94
    iget-object v1, p0, Lcom/glympse/android/hal/b;->q:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->getNotificationCenter()Lcom/glympse/android/lib/GNotificationCenter;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GNotificationCenter;->sync(I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 96
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/glympse/android/hal/b;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/glympse/android/hal/b;->a(Landroid/content/Intent;)V

    return-void
.end method

.method private c()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 121
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/glympse/android/hal/b;->cT:Lcom/glympse/android/api/GUser;

    invoke-interface {v1}, Lcom/glympse/android/api/GUser;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/hal/b;->q:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 122
    const-string v1, "UTF-8"

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/glympse/android/hal/d;->a([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 40
    iget-object v0, p0, Lcom/glympse/android/hal/b;->cS:Lcom/glympse/android/api/GGlympse;

    check-cast v0, Lcom/glympse/android/lib/GGlympsePrivate;

    iput-object v0, p0, Lcom/glympse/android/hal/b;->q:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 41
    iget-object v0, p0, Lcom/glympse/android/hal/b;->q:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getContextHolder()Lcom/glympse/android/hal/GContextHolder;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/hal/GContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/hal/b;->e:Landroid/content/Context;

    .line 44
    new-instance v0, Lcom/glympse/android/hal/b$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/glympse/android/hal/b$a;-><init>(Lcom/glympse/android/hal/b;Lcom/glympse/android/hal/b$1;)V

    iput-object v0, p0, Lcom/glympse/android/hal/b;->r:Lcom/glympse/android/hal/b$a;

    .line 45
    iget-object v0, p0, Lcom/glympse/android/hal/b;->e:Landroid/content/Context;

    iget-object v1, p0, Lcom/glympse/android/hal/b;->r:Lcom/glympse/android/hal/b$a;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.glympse.android.hal.acc.CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 46
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 51
    iget-object v0, p0, Lcom/glympse/android/hal/b;->e:Landroid/content/Context;

    iget-object v1, p0, Lcom/glympse/android/hal/b;->r:Lcom/glympse/android/hal/b$a;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 52
    iput-object v2, p0, Lcom/glympse/android/hal/b;->r:Lcom/glympse/android/hal/b$a;

    .line 55
    iput-object v2, p0, Lcom/glympse/android/hal/b;->e:Landroid/content/Context;

    .line 56
    iput-object v2, p0, Lcom/glympse/android/hal/b;->q:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 57
    return-void
.end method

.method public send(I)V
    .locals 3

    .prologue
    .line 105
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.glympse.android.hal.acc.CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 106
    const-string v1, "flags"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 107
    const-string v1, "account"

    invoke-direct {p0}, Lcom/glympse/android/hal/b;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 108
    const-string v1, "package"

    iget-object v2, p0, Lcom/glympse/android/hal/b;->e:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 109
    iget-object v1, p0, Lcom/glympse/android/hal/b;->e:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 114
    :goto_0
    return-void

    .line 111
    :catch_0
    move-exception v0

    goto :goto_0
.end method
