.class Lcom/glympse/android/hal/aa$b;
.super Landroid/content/BroadcastReceiver;
.source "SmsProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/hal/aa;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/hal/aa$b$a;
    }
.end annotation


# static fields
.field private static final dt:Ljava/lang/String; = "com.glympse.android.kit.send.SMS_SENT"


# instance fields
.field private aN:Z

.field private dp:Ljava/lang/String;

.field private dq:Ljava/lang/String;

.field private dr:Lcom/glympse/android/hal/GSmsListener;

.field final synthetic ds:Lcom/glympse/android/hal/aa;

.field private du:Ljava/lang/String;

.field private dv:I

.field private dw:Lcom/glympse/android/hal/aa$b$a;


# direct methods
.method public constructor <init>(Lcom/glympse/android/hal/aa;Lcom/glympse/android/hal/GSmsListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 249
    iput-object p1, p0, Lcom/glympse/android/hal/aa$b;->ds:Lcom/glympse/android/hal/aa;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 246
    iput-object v3, p0, Lcom/glympse/android/hal/aa$b;->dw:Lcom/glympse/android/hal/aa$b$a;

    .line 250
    iput-object p2, p0, Lcom/glympse/android/hal/aa$b;->dr:Lcom/glympse/android/hal/GSmsListener;

    .line 251
    iput-object p3, p0, Lcom/glympse/android/hal/aa$b;->du:Ljava/lang/String;

    .line 252
    iput-object p4, p0, Lcom/glympse/android/hal/aa$b;->dp:Ljava/lang/String;

    .line 253
    iput-object p5, p0, Lcom/glympse/android/hal/aa$b;->dq:Ljava/lang/String;

    .line 254
    iput p6, p0, Lcom/glympse/android/hal/aa$b;->dv:I

    .line 256
    const/4 v0, 0x1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SmsBroadcastReceiver.SmsBroadcastReceiver() - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/glympse/android/hal/aa$b;->du:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 258
    new-instance v0, Lcom/glympse/android/hal/aa$b$a;

    invoke-direct {v0, p0, v3}, Lcom/glympse/android/hal/aa$b$a;-><init>(Lcom/glympse/android/hal/aa$b;Lcom/glympse/android/hal/aa$1;)V

    iput-object v0, p0, Lcom/glympse/android/hal/aa$b;->dw:Lcom/glympse/android/hal/aa$b$a;

    .line 259
    invoke-static {p1}, Lcom/glympse/android/hal/aa;->a(Lcom/glympse/android/hal/aa;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/hal/aa$b;->dw:Lcom/glympse/android/hal/aa$b$a;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 261
    invoke-static {p1}, Lcom/glympse/android/hal/aa;->b(Lcom/glympse/android/hal/aa;)Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 263
    invoke-static {p1}, Lcom/glympse/android/hal/aa;->b(Lcom/glympse/android/hal/aa;)Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1, p3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 265
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/glympse/android/hal/aa$b;)Lcom/glympse/android/hal/GSmsListener;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/glympse/android/hal/aa$b;->dr:Lcom/glympse/android/hal/GSmsListener;

    return-object v0
.end method

.method static synthetic a(Lcom/glympse/android/hal/aa$b;Lcom/glympse/android/hal/GSmsListener;)Lcom/glympse/android/hal/GSmsListener;
    .locals 0

    .prologue
    .line 235
    iput-object p1, p0, Lcom/glympse/android/hal/aa$b;->dr:Lcom/glympse/android/hal/GSmsListener;

    return-object p1
.end method

.method static synthetic b(Lcom/glympse/android/hal/aa$b;)V
    .locals 0

    .prologue
    .line 235
    invoke-direct {p0}, Lcom/glympse/android/hal/aa$b;->stop()V

    return-void
.end method

.method private c(Landroid/content/Intent;)V
    .locals 10

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 323
    .line 335
    invoke-virtual {p0}, Lcom/glympse/android/hal/aa$b;->getResultCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 374
    iput-boolean v2, p0, Lcom/glympse/android/hal/aa$b;->aN:Z

    .line 375
    const-string v4, "Unknown error"

    .line 376
    invoke-virtual {p0}, Lcom/glympse/android/hal/aa$b;->getResultCode()I

    move-result v1

    .line 377
    if-lez v1, :cond_5

    move v0, v1

    :goto_0
    move v5, v0

    move v6, v1

    move-object v0, v4

    move v4, v3

    .line 382
    :goto_1
    if-eqz v0, :cond_8

    if-eqz v6, :cond_8

    .line 384
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 391
    :goto_2
    if-eqz v1, :cond_6

    move v0, v2

    .line 404
    :goto_3
    if-eqz v3, :cond_0

    iget v4, p0, Lcom/glympse/android/hal/aa$b;->dv:I

    if-nez v4, :cond_0

    .line 406
    iget-object v4, p0, Lcom/glympse/android/hal/aa$b;->ds:Lcom/glympse/android/hal/aa;

    iget-object v8, p0, Lcom/glympse/android/hal/aa$b;->dp:Ljava/lang/String;

    iget-object v9, p0, Lcom/glympse/android/hal/aa$b;->dq:Ljava/lang/String;

    invoke-virtual {v4, v8, v9}, Lcom/glympse/android/hal/aa;->c(Ljava/lang/String;Ljava/lang/String;)Z

    .line 409
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SmsBroadcastReceiver.handleIntent() - "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v8, p0, Lcom/glympse/android/hal/aa$b;->du:Ljava/lang/String;

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, ", Success: "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, ", Failed: "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, ", Error: "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, ", Code: "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", Result: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 410
    iget-boolean v2, p0, Lcom/glympse/android/hal/aa$b;->aN:Z

    if-eqz v2, :cond_1

    .line 412
    invoke-static {p1}, Lcom/glympse/android/hal/DebugBase;->dumpIntent(Landroid/content/Intent;)V

    .line 416
    :cond_1
    if-nez v3, :cond_2

    if-eqz v0, :cond_4

    .line 425
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/hal/aa$b;->dr:Lcom/glympse/android/hal/GSmsListener;

    if-eqz v0, :cond_3

    .line 427
    iget-object v0, p0, Lcom/glympse/android/hal/aa$b;->dr:Lcom/glympse/android/hal/GSmsListener;

    invoke-interface {v0, v3, v5, v1, v7}, Lcom/glympse/android/hal/GSmsListener;->complete(ZILjava/lang/String;Lcom/glympse/android/core/GCommon;)V

    .line 428
    iput-object v7, p0, Lcom/glympse/android/hal/aa$b;->dr:Lcom/glympse/android/hal/GSmsListener;

    .line 433
    :cond_3
    iget-boolean v0, p0, Lcom/glympse/android/hal/aa$b;->aN:Z

    if-nez v0, :cond_4

    .line 435
    invoke-direct {p0}, Lcom/glympse/android/hal/aa$b;->stop()V

    .line 438
    :cond_4
    return-void

    :sswitch_0
    move v4, v3

    move v5, v3

    move v6, v3

    move-object v0, v7

    .line 338
    goto/16 :goto_1

    .line 341
    :sswitch_1
    const-string v0, "General failure. The phone number may be invalid."

    .line 343
    const-string v1, "errorCode"

    invoke-virtual {p1, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    move v4, v3

    move v5, v2

    move v6, v1

    .line 344
    goto/16 :goto_1

    .line 347
    :sswitch_2
    const-string v1, "No SMS service"

    .line 348
    const/4 v0, 0x4

    move v4, v3

    move v5, v0

    move v6, v3

    move-object v0, v1

    .line 349
    goto/16 :goto_1

    .line 352
    :sswitch_3
    const-string v1, "Invalid SMS message format"

    .line 353
    const/4 v0, 0x3

    move v4, v3

    move v5, v0

    move v6, v3

    move-object v0, v1

    .line 354
    goto/16 :goto_1

    .line 357
    :sswitch_4
    const-string v1, "Radio is off. Are you in airplane mode?"

    .line 358
    const/4 v0, 0x2

    move v4, v3

    move v5, v0

    move v6, v3

    move-object v0, v1

    .line 359
    goto/16 :goto_1

    .line 370
    :sswitch_5
    iput-boolean v2, p0, Lcom/glympse/android/hal/aa$b;->aN:Z

    move v4, v2

    move v5, v3

    move v6, v3

    move-object v0, v7

    .line 371
    goto/16 :goto_1

    .line 377
    :cond_5
    const/16 v0, 0x22b

    goto/16 :goto_0

    .line 397
    :cond_6
    if-nez v4, :cond_7

    move v0, v3

    move v3, v2

    .line 399
    goto/16 :goto_3

    :cond_7
    move v0, v3

    goto/16 :goto_3

    :cond_8
    move-object v1, v0

    goto/16 :goto_2

    .line 335
    nop

    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_4
        0x3 -> :sswitch_3
        0x4 -> :sswitch_2
        0x7cf -> :sswitch_5
    .end sparse-switch
.end method

.method private stop()V
    .locals 2

    .prologue
    .line 269
    iget-object v0, p0, Lcom/glympse/android/hal/aa$b;->ds:Lcom/glympse/android/hal/aa;

    invoke-static {v0}, Lcom/glympse/android/hal/aa;->b(Lcom/glympse/android/hal/aa;)Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 275
    :try_start_0
    iget-object v0, p0, Lcom/glympse/android/hal/aa$b;->ds:Lcom/glympse/android/hal/aa;

    invoke-static {v0}, Lcom/glympse/android/hal/aa;->b(Lcom/glympse/android/hal/aa;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 282
    :cond_0
    :goto_0
    return-void

    .line 277
    :catch_0
    move-exception v0

    .line 279
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 288
    :try_start_0
    iget-object v0, p0, Lcom/glympse/android/hal/aa$b;->ds:Lcom/glympse/android/hal/aa;

    invoke-static {v0}, Lcom/glympse/android/hal/aa;->a(Lcom/glympse/android/hal/aa;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/hal/aa$b;->dw:Lcom/glympse/android/hal/aa$b$a;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 290
    iget-object v0, p0, Lcom/glympse/android/hal/aa$b;->ds:Lcom/glympse/android/hal/aa;

    invoke-static {v0}, Lcom/glympse/android/hal/aa;->c(Lcom/glympse/android/hal/aa;)I

    .line 291
    iget-object v0, p0, Lcom/glympse/android/hal/aa$b;->ds:Lcom/glympse/android/hal/aa;

    invoke-static {v0}, Lcom/glympse/android/hal/aa;->d(Lcom/glympse/android/hal/aa;)I

    move-result v0

    if-nez v0, :cond_0

    .line 293
    iget-object v0, p0, Lcom/glympse/android/hal/aa$b;->ds:Lcom/glympse/android/hal/aa;

    invoke-virtual {v0}, Lcom/glympse/android/hal/aa;->W()V

    .line 295
    :cond_0
    invoke-direct {p0, p2}, Lcom/glympse/android/hal/aa$b;->c(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 301
    :goto_0
    return-void

    .line 297
    :catch_0
    move-exception v0

    .line 299
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    goto :goto_0
.end method
