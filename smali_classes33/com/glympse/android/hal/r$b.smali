.class Lcom/glympse/android/hal/r$b;
.super Ljava/lang/Object;
.source "LocationProvider.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/hal/r;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic cB:Lcom/glympse/android/hal/r;

.field private cC:Lcom/glympse/android/core/GLocationProvider;


# direct methods
.method public constructor <init>(Lcom/glympse/android/hal/r;Lcom/glympse/android/core/GLocationProvider;)V
    .locals 0

    .prologue
    .line 821
    iput-object p1, p0, Lcom/glympse/android/hal/r$b;->cB:Lcom/glympse/android/hal/r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 822
    iput-object p2, p0, Lcom/glympse/android/hal/r$b;->cC:Lcom/glympse/android/core/GLocationProvider;

    .line 823
    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 827
    const/4 v0, 0x1

    const-string v1, "[PermissionTimer] Permission timer fired"

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 830
    iget-object v0, p0, Lcom/glympse/android/hal/r$b;->cB:Lcom/glympse/android/hal/r;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/glympse/android/hal/r;->a(Lcom/glympse/android/hal/r;Lcom/glympse/android/hal/r$b;)Lcom/glympse/android/hal/r$b;

    .line 832
    iget-object v0, p0, Lcom/glympse/android/hal/r$b;->cB:Lcom/glympse/android/hal/r;

    invoke-static {v0}, Lcom/glympse/android/hal/r;->a(Lcom/glympse/android/hal/r;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/glympse/android/hal/r;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 835
    iget-object v0, p0, Lcom/glympse/android/hal/r$b;->cB:Lcom/glympse/android/hal/r;

    iget-object v1, p0, Lcom/glympse/android/hal/r$b;->cC:Lcom/glympse/android/core/GLocationProvider;

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/r;->a(Lcom/glympse/android/core/GLocationProvider;)V

    .line 842
    :goto_0
    return-void

    .line 840
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/hal/r$b;->cC:Lcom/glympse/android/core/GLocationProvider;

    invoke-interface {v0}, Lcom/glympse/android/core/GLocationProvider;->start()V

    goto :goto_0
.end method
