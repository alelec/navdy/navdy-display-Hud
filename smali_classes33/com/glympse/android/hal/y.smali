.class Lcom/glympse/android/hal/y;
.super Ljava/lang/Object;
.source "ServiceWrapper.java"

# interfaces
.implements Lcom/glympse/android/hal/GServiceWrapper;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/hal/y$a;
    }
.end annotation


# instance fields
.field private dh:Lcom/glympse/android/hal/b;

.field private di:Lcom/glympse/android/hal/ConnectivityListener;

.field private dj:Lcom/glympse/android/hal/y$a;

.field private e:Landroid/content/Context;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private R()V
    .locals 3

    .prologue
    .line 134
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/glympse/android/hal/y;->e:Landroid/content/Context;

    const-class v2, Lcom/glympse/android/hal/GlympseService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 135
    iget-object v1, p0, Lcom/glympse/android/hal/y;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 136
    iget-object v1, p0, Lcom/glympse/android/hal/y;->e:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 137
    return-void
.end method

.method private S()V
    .locals 3

    .prologue
    .line 141
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/glympse/android/hal/y;->e:Landroid/content/Context;

    const-class v2, Lcom/glympse/android/hal/GlympseService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 142
    iget-object v1, p0, Lcom/glympse/android/hal/y;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 143
    iget-object v1, p0, Lcom/glympse/android/hal/y;->e:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 144
    return-void
.end method


# virtual methods
.method public setActive(Z)V
    .locals 2

    .prologue
    .line 117
    if-eqz p1, :cond_0

    .line 123
    const/4 v0, 0x1

    const-string v1, "ServiceWrapper.setActive() - calling local startService()"

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 124
    invoke-direct {p0}, Lcom/glympse/android/hal/y;->R()V

    .line 126
    :cond_0
    return-void
.end method

.method public start(Lcom/glympse/android/api/GGlympse;)V
    .locals 2

    .prologue
    .line 35
    check-cast p1, Lcom/glympse/android/lib/GGlympsePrivate;

    .line 36
    invoke-interface {p1}, Lcom/glympse/android/lib/GGlympsePrivate;->getContextHolder()Lcom/glympse/android/hal/GContextHolder;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/hal/GContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/hal/y;->e:Landroid/content/Context;

    .line 39
    sput-object p1, Lcom/glympse/android/hal/GlympseService;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 42
    const/4 v0, 0x1

    const-string v1, "ServiceWrapper.start() - calling local startService()"

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 43
    invoke-direct {p0}, Lcom/glympse/android/hal/y;->R()V

    .line 46
    invoke-static {}, Lcom/glympse/android/hal/GlympseService;->initializeNotifications()V

    .line 49
    invoke-interface {p1}, Lcom/glympse/android/lib/GGlympsePrivate;->isAccountSharingEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    new-instance v0, Lcom/glympse/android/hal/b;

    invoke-direct {v0}, Lcom/glympse/android/hal/b;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/hal/y;->dh:Lcom/glympse/android/hal/b;

    .line 52
    iget-object v0, p0, Lcom/glympse/android/hal/y;->dh:Lcom/glympse/android/hal/b;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/b;->start(Lcom/glympse/android/api/GGlympse;)V

    .line 56
    :cond_0
    new-instance v0, Lcom/glympse/android/hal/ConnectivityListener;

    invoke-direct {v0}, Lcom/glympse/android/hal/ConnectivityListener;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/hal/y;->di:Lcom/glympse/android/hal/ConnectivityListener;

    .line 57
    iget-object v0, p0, Lcom/glympse/android/hal/y;->di:Lcom/glympse/android/hal/ConnectivityListener;

    iget-object v1, p0, Lcom/glympse/android/hal/y;->e:Landroid/content/Context;

    invoke-virtual {v0, v1, p1}, Lcom/glympse/android/hal/ConnectivityListener;->start(Landroid/content/Context;Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 60
    new-instance v0, Lcom/glympse/android/hal/y$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/glympse/android/hal/y$a;-><init>(Lcom/glympse/android/hal/y;Lcom/glympse/android/hal/y$1;)V

    iput-object v0, p0, Lcom/glympse/android/hal/y;->dj:Lcom/glympse/android/hal/y$a;

    .line 61
    iget-object v0, p0, Lcom/glympse/android/hal/y;->dj:Lcom/glympse/android/hal/y$a;

    iget-object v1, p0, Lcom/glympse/android/hal/y;->e:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/y$a;->h(Landroid/content/Context;)V

    .line 64
    invoke-static {}, Lcom/glympse/android/hal/Platform;->getPushType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "google"

    invoke-static {v0, v1}, Lcom/glympse/android/hal/Helpers;->safeEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 66
    invoke-static {}, Lcom/glympse/android/hal/GCMReceiver;->register()V

    .line 82
    :goto_0
    return-void

    .line 72
    :cond_1
    :try_start_0
    new-instance v0, Lcom/glympse/android/hal/ADMWrapper;

    invoke-direct {v0}, Lcom/glympse/android/hal/ADMWrapper;-><init>()V

    .line 73
    iget-object v1, p0, Lcom/glympse/android/hal/y;->e:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/ADMWrapper;->register(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 75
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public stop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 87
    iget-object v0, p0, Lcom/glympse/android/hal/y;->dj:Lcom/glympse/android/hal/y$a;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/glympse/android/hal/y;->dj:Lcom/glympse/android/hal/y$a;

    invoke-virtual {v0}, Lcom/glympse/android/hal/y$a;->stop()V

    .line 90
    iput-object v1, p0, Lcom/glympse/android/hal/y;->dj:Lcom/glympse/android/hal/y$a;

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/hal/y;->di:Lcom/glympse/android/hal/ConnectivityListener;

    if-eqz v0, :cond_1

    .line 96
    iget-object v0, p0, Lcom/glympse/android/hal/y;->di:Lcom/glympse/android/hal/ConnectivityListener;

    invoke-virtual {v0}, Lcom/glympse/android/hal/ConnectivityListener;->stop()V

    .line 97
    iput-object v1, p0, Lcom/glympse/android/hal/y;->di:Lcom/glympse/android/hal/ConnectivityListener;

    .line 101
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/hal/y;->dh:Lcom/glympse/android/hal/b;

    if-eqz v0, :cond_2

    .line 103
    iget-object v0, p0, Lcom/glympse/android/hal/y;->dh:Lcom/glympse/android/hal/b;

    invoke-virtual {v0}, Lcom/glympse/android/hal/b;->stop()V

    .line 104
    iput-object v1, p0, Lcom/glympse/android/hal/y;->dh:Lcom/glympse/android/hal/b;

    .line 108
    :cond_2
    sput-object v1, Lcom/glympse/android/hal/GlympseService;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 111
    const/4 v0, 0x1

    const-string v1, "ServiceWrapper.stopService() - calling local stopService()"

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 112
    invoke-direct {p0}, Lcom/glympse/android/hal/y;->S()V

    .line 113
    return-void
.end method
