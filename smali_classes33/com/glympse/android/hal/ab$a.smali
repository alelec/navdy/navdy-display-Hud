.class Lcom/glympse/android/hal/ab$a;
.super Landroid/content/BroadcastReceiver;
.source "SmsReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/hal/ab;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# static fields
.field private static final dA:Ljava/lang/String; = "android.provider.Telephony.SMS_RECEIVED"


# instance fields
.field final synthetic dB:Lcom/glympse/android/hal/ab;


# direct methods
.method private constructor <init>(Lcom/glympse/android/hal/ab;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lcom/glympse/android/hal/ab$a;->dB:Lcom/glympse/android/hal/ab;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/glympse/android/hal/ab;Lcom/glympse/android/hal/ab$1;)V
    .locals 0

    .prologue
    .line 102
    invoke-direct {p0, p1}, Lcom/glympse/android/hal/ab$a;-><init>(Lcom/glympse/android/hal/ab;)V

    return-void
.end method

.method private d(Landroid/content/Intent;)[Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 195
    const/4 v1, 0x0

    .line 196
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 197
    if-eqz v0, :cond_1

    .line 201
    :try_start_0
    const-string v2, "pdus"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 202
    array-length v2, v0

    new-array v2, v2, [Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move v3, v4

    .line 203
    :goto_0
    :try_start_1
    array-length v1, v0

    if-ge v3, v1, :cond_0

    .line 205
    aget-object v1, v0, v3

    check-cast v1, [B

    check-cast v1, [B

    invoke-static {v1}, Lcom/glympse/android/hal/Reflection$_SmsMessage;->createFromPdu([B)Ljava/lang/Object;

    move-result-object v1

    aput-object v1, v2, v3
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 203
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    :cond_0
    move-object v0, v2

    .line 213
    :goto_1
    return-object v0

    .line 208
    :catch_0
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    .line 210
    :goto_2
    invoke-static {v1, v4}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    goto :goto_1

    .line 208
    :catch_1
    move-exception v0

    move-object v1, v0

    move-object v0, v2

    goto :goto_2

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11

    .prologue
    .line 108
    const/4 v0, 0x1

    const-string v1, "[SMSReceiver::onReceive]"

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 111
    iget-object v0, p0, Lcom/glympse/android/hal/ab$a;->dB:Lcom/glympse/android/hal/ab;

    invoke-static {v0}, Lcom/glympse/android/hal/ab;->a(Lcom/glympse/android/hal/ab;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "android.permission.READ_SMS"

    invoke-static {v0, v1}, Lcom/glympse/android/hal/h;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 191
    :cond_0
    :goto_0
    return-void

    .line 121
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x7d0

    add-long/2addr v2, v0

    .line 126
    if-eqz p2, :cond_0

    :try_start_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.provider.Telephony.SMS_RECEIVED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    invoke-direct {p0, p2}, Lcom/glympse/android/hal/ab$a;->d(Landroid/content/Intent;)[Ljava/lang/Object;

    move-result-object v9

    .line 130
    if-eqz v9, :cond_0

    .line 132
    const-wide/16 v4, 0x0

    .line 135
    array-length v10, v9

    const/4 v0, 0x0

    move v8, v0

    move-wide v0, v4

    :goto_1
    if-ge v8, v10, :cond_4

    aget-object v5, v9, v8
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 137
    if-eqz v5, :cond_3

    .line 155
    cmp-long v4, v2, v0

    if-lez v4, :cond_5

    move-wide v6, v2

    .line 161
    :goto_2
    :try_start_1
    invoke-static {v5}, Lcom/glympse/android/hal/Reflection$_SmsMessage;->getDisplayOriginatingAddress(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 163
    invoke-static {v5}, Lcom/glympse/android/hal/Reflection$_SmsMessage;->getDisplayMessageBody(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 167
    iget-object v0, p0, Lcom/glympse/android/hal/ab$a;->dB:Lcom/glympse/android/hal/ab;

    invoke-static {v0}, Lcom/glympse/android/hal/ab;->b(Lcom/glympse/android/hal/ab;)Lcom/glympse/android/hal/GMessageListener;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 169
    iget-object v0, p0, Lcom/glympse/android/hal/ab$a;->dB:Lcom/glympse/android/hal/ab;

    invoke-static {v0}, Lcom/glympse/android/hal/ab;->b(Lcom/glympse/android/hal/ab;)Lcom/glympse/android/hal/GMessageListener;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface/range {v0 .. v5}, Lcom/glympse/android/hal/GMessageListener;->messageReceived(ZJLjava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    :cond_2
    move-wide v0, v6

    .line 135
    :cond_3
    :goto_3
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    goto :goto_1

    .line 172
    :catch_0
    move-exception v0

    .line 174
    const/4 v1, 0x0

    :try_start_2
    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    move-wide v0, v6

    goto :goto_3

    .line 180
    :cond_4
    iget-object v2, p0, Lcom/glympse/android/hal/ab$a;->dB:Lcom/glympse/android/hal/ab;

    invoke-static {v2}, Lcom/glympse/android/hal/ab;->b(Lcom/glympse/android/hal/ab;)Lcom/glympse/android/hal/GMessageListener;

    move-result-object v2

    if-eqz v2, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v2, v2, v0

    if-eqz v2, :cond_0

    .line 182
    iget-object v2, p0, Lcom/glympse/android/hal/ab$a;->dB:Lcom/glympse/android/hal/ab;

    invoke-static {v2}, Lcom/glympse/android/hal/ab;->b(Lcom/glympse/android/hal/ab;)Lcom/glympse/android/hal/GMessageListener;

    move-result-object v2

    iget-object v3, p0, Lcom/glympse/android/hal/ab$a;->dB:Lcom/glympse/android/hal/ab;

    invoke-interface {v2, v3, v0, v1}, Lcom/glympse/android/hal/GMessageListener;->lastMessageFound(Lcom/glympse/android/core/GCommon;J)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 187
    :catch_1
    move-exception v0

    .line 189
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    goto :goto_0

    :cond_5
    move-wide v6, v0

    goto :goto_2
.end method
