.class Lcom/glympse/android/hal/u$b;
.super Landroid/content/BroadcastReceiver;
.source "ProximityProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/hal/u;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field private D:Landroid/app/PendingIntent;

.field final synthetic da:Lcom/glympse/android/hal/u;

.field private db:Lcom/glympse/android/core/GRegion;


# direct methods
.method public constructor <init>(Lcom/glympse/android/hal/u;Landroid/app/PendingIntent;Lcom/glympse/android/core/GRegion;)V
    .locals 0

    .prologue
    .line 241
    iput-object p1, p0, Lcom/glympse/android/hal/u$b;->da:Lcom/glympse/android/hal/u;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 242
    iput-object p2, p0, Lcom/glympse/android/hal/u$b;->D:Landroid/app/PendingIntent;

    .line 243
    iput-object p3, p0, Lcom/glympse/android/hal/u$b;->db:Lcom/glympse/android/core/GRegion;

    .line 244
    return-void
.end method


# virtual methods
.method public M()Landroid/app/PendingIntent;
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/glympse/android/hal/u$b;->D:Landroid/app/PendingIntent;

    return-object v0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 249
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.glympse.android.hal.proximity.REGION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252
    iget-object v0, p0, Lcom/glympse/android/hal/u$b;->da:Lcom/glympse/android/hal/u;

    invoke-static {v0}, Lcom/glympse/android/hal/u;->c(Lcom/glympse/android/hal/u;)Lcom/glympse/android/core/GProximityListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 254
    const-string v0, "entering"

    const/4 v1, 0x1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 255
    if-eqz v0, :cond_1

    .line 257
    iget-object v0, p0, Lcom/glympse/android/hal/u$b;->da:Lcom/glympse/android/hal/u;

    invoke-static {v0}, Lcom/glympse/android/hal/u;->c(Lcom/glympse/android/hal/u;)Lcom/glympse/android/core/GProximityListener;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/hal/u$b;->db:Lcom/glympse/android/core/GRegion;

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GProximityListener;->regionEntered(Lcom/glympse/android/core/GRegion;)V

    .line 265
    :cond_0
    :goto_0
    return-void

    .line 261
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/hal/u$b;->da:Lcom/glympse/android/hal/u;

    invoke-static {v0}, Lcom/glympse/android/hal/u;->c(Lcom/glympse/android/hal/u;)Lcom/glympse/android/core/GProximityListener;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/hal/u$b;->db:Lcom/glympse/android/core/GRegion;

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GProximityListener;->regionLeft(Lcom/glympse/android/core/GRegion;)V

    goto :goto_0
.end method
