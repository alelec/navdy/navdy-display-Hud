.class public abstract Lcom/glympse/android/hal/gms/location/LocationClient;
.super Ljava/lang/Object;
.source "LocationClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/hal/gms/location/LocationClient$GeofencingEvent;,
        Lcom/glympse/android/hal/gms/location/LocationClient$OnRemoveGeofencesResultListener;,
        Lcom/glympse/android/hal/gms/location/LocationClient$OnAddGeofencesResultListener;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract addGeofences(Ljava/util/List;Landroid/app/PendingIntent;Lcom/glympse/android/hal/gms/location/LocationClient$OnAddGeofencesResultListener;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/glympse/android/hal/gms/location/Geofence;",
            ">;",
            "Landroid/app/PendingIntent;",
            "Lcom/glympse/android/hal/gms/location/LocationClient$OnAddGeofencesResultListener;",
            ")V"
        }
    .end annotation
.end method

.method public abstract connect()V
.end method

.method public abstract disconnect()V
.end method

.method public abstract getLastLocation()Landroid/location/Location;
.end method

.method public abstract removeGeofences(Landroid/app/PendingIntent;Lcom/glympse/android/hal/gms/location/LocationClient$OnRemoveGeofencesResultListener;)V
.end method

.method public abstract removeGeofences(Ljava/util/List;Lcom/glympse/android/hal/gms/location/LocationClient$OnRemoveGeofencesResultListener;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/glympse/android/hal/gms/location/LocationClient$OnRemoveGeofencesResultListener;",
            ")V"
        }
    .end annotation
.end method

.method public abstract removeLocationUpdates(Lcom/glympse/android/hal/gms/location/LocationListener;)V
.end method

.method public abstract requestLocationUpdates(Lcom/glympse/android/hal/gms/location/LocationRequest;Lcom/glympse/android/hal/gms/location/LocationListener;)V
.end method
