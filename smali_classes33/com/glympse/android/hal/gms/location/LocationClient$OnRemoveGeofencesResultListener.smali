.class public interface abstract Lcom/glympse/android/hal/gms/location/LocationClient$OnRemoveGeofencesResultListener;
.super Ljava/lang/Object;
.source "LocationClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/hal/gms/location/LocationClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnRemoveGeofencesResultListener"
.end annotation


# virtual methods
.method public abstract onRemoveGeofencesByPendingIntentResult(ILandroid/app/PendingIntent;)V
.end method

.method public abstract onRemoveGeofencesByRequestIdsResult(I)V
.end method
