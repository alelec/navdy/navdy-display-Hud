.class public Lcom/glympse/android/hal/gms/location/Geofence$Builder;
.super Ljava/lang/Object;
.source "Geofence.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/hal/gms/location/Geofence;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# static fields
.field private static _class:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static eK:Ljava/lang/reflect/Constructor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/reflect/Constructor",
            "<*>;"
        }
    .end annotation
.end field

.field private static eO:Ljava/lang/reflect/Method;

.field private static fn:Ljava/lang/reflect/Method;

.field private static fo:Ljava/lang/reflect/Method;

.field private static fp:Ljava/lang/reflect/Method;

.field private static fq:Ljava/lang/reflect/Method;


# instance fields
.field private eB:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 42
    sput-object v0, Lcom/glympse/android/hal/gms/location/Geofence$Builder;->eK:Ljava/lang/reflect/Constructor;

    .line 43
    sput-object v0, Lcom/glympse/android/hal/gms/location/Geofence$Builder;->eO:Ljava/lang/reflect/Method;

    .line 44
    sput-object v0, Lcom/glympse/android/hal/gms/location/Geofence$Builder;->fn:Ljava/lang/reflect/Method;

    .line 45
    sput-object v0, Lcom/glympse/android/hal/gms/location/Geofence$Builder;->fo:Ljava/lang/reflect/Method;

    .line 46
    sput-object v0, Lcom/glympse/android/hal/gms/location/Geofence$Builder;->fp:Ljava/lang/reflect/Method;

    .line 47
    sput-object v0, Lcom/glympse/android/hal/gms/location/Geofence$Builder;->fq:Ljava/lang/reflect/Method;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    :try_start_0
    sget-object v1, Lcom/glympse/android/hal/gms/location/Geofence$Builder;->eK:Ljava/lang/reflect/Constructor;

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v0}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/hal/gms/location/Geofence$Builder;->eB:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 60
    :goto_0
    return-void

    .line 57
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic a(Ljava/lang/reflect/Method;)Ljava/lang/reflect/Method;
    .locals 0

    .prologue
    .line 39
    sput-object p0, Lcom/glympse/android/hal/gms/location/Geofence$Builder;->fp:Ljava/lang/reflect/Method;

    return-object p0
.end method

.method static synthetic access$000()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/glympse/android/hal/gms/location/Geofence$Builder;->_class:Ljava/lang/Class;

    return-object v0
.end method

.method static synthetic b(Ljava/lang/reflect/Constructor;)Ljava/lang/reflect/Constructor;
    .locals 0

    .prologue
    .line 39
    sput-object p0, Lcom/glympse/android/hal/gms/location/Geofence$Builder;->eK:Ljava/lang/reflect/Constructor;

    return-object p0
.end method

.method static synthetic b(Ljava/lang/reflect/Method;)Ljava/lang/reflect/Method;
    .locals 0

    .prologue
    .line 39
    sput-object p0, Lcom/glympse/android/hal/gms/location/Geofence$Builder;->fq:Ljava/lang/reflect/Method;

    return-object p0
.end method

.method static synthetic c(Ljava/lang/Class;)Ljava/lang/Class;
    .locals 0

    .prologue
    .line 39
    sput-object p0, Lcom/glympse/android/hal/gms/location/Geofence$Builder;->_class:Ljava/lang/Class;

    return-object p0
.end method

.method static synthetic e(Ljava/lang/reflect/Method;)Ljava/lang/reflect/Method;
    .locals 0

    .prologue
    .line 39
    sput-object p0, Lcom/glympse/android/hal/gms/location/Geofence$Builder;->eO:Ljava/lang/reflect/Method;

    return-object p0
.end method

.method static synthetic f(Ljava/lang/reflect/Method;)Ljava/lang/reflect/Method;
    .locals 0

    .prologue
    .line 39
    sput-object p0, Lcom/glympse/android/hal/gms/location/Geofence$Builder;->fn:Ljava/lang/reflect/Method;

    return-object p0
.end method

.method static synthetic g(Ljava/lang/reflect/Method;)Ljava/lang/reflect/Method;
    .locals 0

    .prologue
    .line 39
    sput-object p0, Lcom/glympse/android/hal/gms/location/Geofence$Builder;->fo:Ljava/lang/reflect/Method;

    return-object p0
.end method


# virtual methods
.method public build()Lcom/glympse/android/hal/gms/location/Geofence;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 66
    :try_start_0
    new-instance v1, Lcom/glympse/android/hal/gms/location/Geofence;

    sget-object v3, Lcom/glympse/android/hal/gms/location/Geofence$Builder;->eO:Ljava/lang/reflect/Method;

    iget-object v4, p0, Lcom/glympse/android/hal/gms/location/Geofence$Builder;->eB:Ljava/lang/Object;

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/glympse/android/hal/gms/location/Geofence;-><init>(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 71
    :goto_0
    return-object v0

    .line 68
    :catch_0
    move-exception v0

    move-object v0, v2

    .line 71
    goto :goto_0
.end method

.method public setCircularRegion(DDF)Lcom/glympse/android/hal/gms/location/Geofence$Builder;
    .locals 5

    .prologue
    .line 78
    :try_start_0
    sget-object v0, Lcom/glympse/android/hal/gms/location/Geofence$Builder;->fn:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/glympse/android/hal/gms/location/Geofence$Builder;->eB:Ljava/lang/Object;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 79
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p3, p4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {p5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    .line 78
    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    :goto_0
    return-object p0

    .line 81
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setExpirationDuration(J)Lcom/glympse/android/hal/gms/location/Geofence$Builder;
    .locals 5

    .prologue
    .line 91
    :try_start_0
    sget-object v0, Lcom/glympse/android/hal/gms/location/Geofence$Builder;->fo:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/glympse/android/hal/gms/location/Geofence$Builder;->eB:Ljava/lang/Object;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    :goto_0
    return-object p0

    .line 93
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setRequestId(Ljava/lang/String;)Lcom/glympse/android/hal/gms/location/Geofence$Builder;
    .locals 4

    .prologue
    .line 103
    :try_start_0
    sget-object v0, Lcom/glympse/android/hal/gms/location/Geofence$Builder;->fp:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/glympse/android/hal/gms/location/Geofence$Builder;->eB:Ljava/lang/Object;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    :goto_0
    return-object p0

    .line 105
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setTransitionTypes(I)Lcom/glympse/android/hal/gms/location/Geofence$Builder;
    .locals 5

    .prologue
    .line 115
    :try_start_0
    sget-object v0, Lcom/glympse/android/hal/gms/location/Geofence$Builder;->fq:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/glympse/android/hal/gms/location/Geofence$Builder;->eB:Ljava/lang/Object;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    :goto_0
    return-object p0

    .line 117
    :catch_0
    move-exception v0

    goto :goto_0
.end method
