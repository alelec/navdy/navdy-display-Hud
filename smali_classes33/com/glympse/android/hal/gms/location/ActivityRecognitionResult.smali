.class public Lcom/glympse/android/hal/gms/location/ActivityRecognitionResult;
.super Ljava/lang/Object;
.source "ActivityRecognitionResult.java"


# static fields
.field private static fd:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static fe:Ljava/lang/reflect/Method;

.field private static ff:Ljava/lang/reflect/Method;

.field private static fg:Ljava/lang/reflect/Method;


# instance fields
.field private fh:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 16
    sput-object v0, Lcom/glympse/android/hal/gms/location/ActivityRecognitionResult;->fe:Ljava/lang/reflect/Method;

    .line 17
    sput-object v0, Lcom/glympse/android/hal/gms/location/ActivityRecognitionResult;->ff:Ljava/lang/reflect/Method;

    .line 18
    sput-object v0, Lcom/glympse/android/hal/gms/location/ActivityRecognitionResult;->fg:Ljava/lang/reflect/Method;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/hal/gms/location/ActivityRecognitionResult;->fh:Ljava/lang/Object;

    return-void
.end method

.method public static extractResult(Landroid/content/Intent;)Lcom/glympse/android/hal/gms/location/ActivityRecognitionResult;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 38
    :try_start_0
    new-instance v0, Lcom/glympse/android/hal/gms/location/ActivityRecognitionResult;

    invoke-direct {v0}, Lcom/glympse/android/hal/gms/location/ActivityRecognitionResult;-><init>()V

    .line 39
    sget-object v2, Lcom/glympse/android/hal/gms/location/ActivityRecognitionResult;->ff:Ljava/lang/reflect/Method;

    const/4 v3, 0x0

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    iput-object v2, v0, Lcom/glympse/android/hal/gms/location/ActivityRecognitionResult;->fh:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 46
    :goto_0
    return-object v0

    .line 43
    :catch_0
    move-exception v0

    move-object v0, v1

    .line 46
    goto :goto_0
.end method

.method public static hasResult(Landroid/content/Intent;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 26
    :try_start_0
    sget-object v0, Lcom/glympse/android/hal/gms/location/ActivityRecognitionResult;->fe:Ljava/lang/reflect/Method;

    const/4 v2, 0x0

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 31
    :goto_0
    return v0

    .line 28
    :catch_0
    move-exception v0

    move v0, v1

    .line 31
    goto :goto_0
.end method

.method public static init()V
    .locals 5

    .prologue
    .line 64
    sget-object v0, Lcom/glympse/android/hal/gms/location/ActivityRecognitionResult;->fd:Ljava/lang/Class;

    if-eqz v0, :cond_0

    .line 78
    :goto_0
    return-void

    .line 70
    :cond_0
    :try_start_0
    const-string v0, "com.google.android.gms.location.ActivityRecognitionResult"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/location/ActivityRecognitionResult;->fd:Ljava/lang/Class;

    .line 71
    sget-object v0, Lcom/glympse/android/hal/gms/location/ActivityRecognitionResult;->fd:Ljava/lang/Class;

    const-string v1, "hasResult"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Landroid/content/Intent;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/location/ActivityRecognitionResult;->fe:Ljava/lang/reflect/Method;

    .line 72
    sget-object v0, Lcom/glympse/android/hal/gms/location/ActivityRecognitionResult;->fd:Ljava/lang/Class;

    const-string v1, "extractResult"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Landroid/content/Intent;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/location/ActivityRecognitionResult;->ff:Ljava/lang/reflect/Method;

    .line 73
    sget-object v1, Lcom/glympse/android/hal/gms/location/ActivityRecognitionResult;->fd:Ljava/lang/Class;

    const-string v2, "getMostProbableActivity"

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/location/ActivityRecognitionResult;->fg:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 75
    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public getMostProbableActivity()Lcom/glympse/android/hal/gms/location/DetectedActivity;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 53
    :try_start_0
    new-instance v1, Lcom/glympse/android/hal/gms/location/DetectedActivity;

    sget-object v3, Lcom/glympse/android/hal/gms/location/ActivityRecognitionResult;->fg:Ljava/lang/reflect/Method;

    iget-object v4, p0, Lcom/glympse/android/hal/gms/location/ActivityRecognitionResult;->fh:Ljava/lang/Object;

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/glympse/android/hal/gms/location/DetectedActivity;-><init>(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 59
    :goto_0
    return-object v0

    .line 56
    :catch_0
    move-exception v0

    move-object v0, v2

    .line 59
    goto :goto_0
.end method
