.class public Lcom/glympse/android/hal/gms/location/LocationRequest;
.super Ljava/lang/Object;
.source "LocationRequest.java"


# static fields
.field public static final PRIORITY_BALANCED_POWER_ACCURACY:I = 0x66

.field public static final PRIORITY_HIGH_ACCURACY:I = 0x64

.field public static final PRIORITY_NO_POWER:I = 0x69

.field public static _LocationRequest:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static fr:Ljava/lang/reflect/Method;

.field private static fs:Ljava/lang/reflect/Method;

.field private static ft:Ljava/lang/reflect/Method;

.field private static fu:Ljava/lang/reflect/Method;

.field private static fv:Ljava/lang/reflect/Method;


# instance fields
.field private fw:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 17
    sput-object v0, Lcom/glympse/android/hal/gms/location/LocationRequest;->_LocationRequest:Ljava/lang/Class;

    .line 18
    sput-object v0, Lcom/glympse/android/hal/gms/location/LocationRequest;->fr:Ljava/lang/reflect/Method;

    .line 19
    sput-object v0, Lcom/glympse/android/hal/gms/location/LocationRequest;->fs:Ljava/lang/reflect/Method;

    .line 20
    sput-object v0, Lcom/glympse/android/hal/gms/location/LocationRequest;->ft:Ljava/lang/reflect/Method;

    .line 21
    sput-object v0, Lcom/glympse/android/hal/gms/location/LocationRequest;->fu:Ljava/lang/reflect/Method;

    .line 22
    sput-object v0, Lcom/glympse/android/hal/gms/location/LocationRequest;->fv:Ljava/lang/reflect/Method;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/hal/gms/location/LocationRequest;->fw:Ljava/lang/Object;

    .line 42
    return-void
.end method

.method public static create()Lcom/glympse/android/hal/gms/location/LocationRequest;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 30
    :try_start_0
    new-instance v1, Lcom/glympse/android/hal/gms/location/LocationRequest;

    invoke-direct {v1}, Lcom/glympse/android/hal/gms/location/LocationRequest;-><init>()V

    .line 31
    sget-object v3, Lcom/glympse/android/hal/gms/location/LocationRequest;->fr:Ljava/lang/reflect/Method;

    const/4 v4, 0x0

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, v1, Lcom/glympse/android/hal/gms/location/LocationRequest;->fw:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 37
    :goto_0
    return-object v0

    .line 34
    :catch_0
    move-exception v0

    move-object v0, v2

    .line 37
    goto :goto_0
.end method

.method public static init()V
    .locals 5

    .prologue
    .line 95
    sget-object v0, Lcom/glympse/android/hal/gms/location/LocationRequest;->_LocationRequest:Ljava/lang/Class;

    if-eqz v0, :cond_0

    .line 111
    :goto_0
    return-void

    .line 101
    :cond_0
    :try_start_0
    const-string v0, "com.google.android.gms.location.LocationRequest"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/location/LocationRequest;->_LocationRequest:Ljava/lang/Class;

    .line 102
    sget-object v1, Lcom/glympse/android/hal/gms/location/LocationRequest;->_LocationRequest:Ljava/lang/Class;

    const-string v2, "create"

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/location/LocationRequest;->fr:Ljava/lang/reflect/Method;

    .line 103
    sget-object v0, Lcom/glympse/android/hal/gms/location/LocationRequest;->_LocationRequest:Ljava/lang/Class;

    const-string v1, "setPriority"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/location/LocationRequest;->fs:Ljava/lang/reflect/Method;

    .line 104
    sget-object v0, Lcom/glympse/android/hal/gms/location/LocationRequest;->_LocationRequest:Ljava/lang/Class;

    const-string v1, "setInterval"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/location/LocationRequest;->ft:Ljava/lang/reflect/Method;

    .line 105
    sget-object v0, Lcom/glympse/android/hal/gms/location/LocationRequest;->_LocationRequest:Ljava/lang/Class;

    const-string v1, "setFastestInterval"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/location/LocationRequest;->fu:Ljava/lang/reflect/Method;

    .line 106
    sget-object v0, Lcom/glympse/android/hal/gms/location/LocationRequest;->_LocationRequest:Ljava/lang/Class;

    const-string v1, "setSmallestDisplacement"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/location/LocationRequest;->fv:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 108
    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public getRequest()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/glympse/android/hal/gms/location/LocationRequest;->fw:Ljava/lang/Object;

    return-object v0
.end method

.method public setFastestInterval(J)V
    .locals 5

    .prologue
    .line 75
    :try_start_0
    sget-object v0, Lcom/glympse/android/hal/gms/location/LocationRequest;->fu:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/glympse/android/hal/gms/location/LocationRequest;->fw:Ljava/lang/Object;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 80
    :goto_0
    return-void

    .line 77
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setInterval(J)V
    .locals 5

    .prologue
    .line 64
    :try_start_0
    sget-object v0, Lcom/glympse/android/hal/gms/location/LocationRequest;->ft:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/glympse/android/hal/gms/location/LocationRequest;->fw:Ljava/lang/Object;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 69
    :goto_0
    return-void

    .line 66
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setPriority(I)V
    .locals 5

    .prologue
    .line 53
    :try_start_0
    sget-object v0, Lcom/glympse/android/hal/gms/location/LocationRequest;->fs:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/glympse/android/hal/gms/location/LocationRequest;->fw:Ljava/lang/Object;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    :goto_0
    return-void

    .line 55
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setSmallestDisplacement(F)V
    .locals 5

    .prologue
    .line 86
    :try_start_0
    sget-object v0, Lcom/glympse/android/hal/gms/location/LocationRequest;->fv:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/glympse/android/hal/gms/location/LocationRequest;->fw:Ljava/lang/Object;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 91
    :goto_0
    return-void

    .line 88
    :catch_0
    move-exception v0

    goto :goto_0
.end method
