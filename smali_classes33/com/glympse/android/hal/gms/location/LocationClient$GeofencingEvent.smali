.class public interface abstract Lcom/glympse/android/hal/gms/location/LocationClient$GeofencingEvent;
.super Ljava/lang/Object;
.source "LocationClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/hal/gms/location/LocationClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "GeofencingEvent"
.end annotation


# virtual methods
.method public abstract getGeofenceTransition()I
.end method

.method public abstract getTriggeringGeofences()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/glympse/android/hal/gms/location/Geofence;",
            ">;"
        }
    .end annotation
.end method

.method public abstract hasError()Z
.end method
