.class public abstract Lcom/glympse/android/hal/gms/location/ActivityRecognitionClient;
.super Ljava/lang/Object;
.source "ActivityRecognitionClient.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract connect()V
.end method

.method public abstract disconnect()V
.end method

.method public abstract removeActivityUpdates(Landroid/app/PendingIntent;)V
.end method

.method public abstract requestActivityUpdates(JLandroid/app/PendingIntent;)V
.end method
