.class public Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;
.super Ljava/lang/Object;
.source "GooglePlayServicesUtil.java"


# static fields
.field public static GOOGLE_PLAY_SERVICES_VERSION_CODE:I

.field private static dK:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static dL:Ljava/lang/reflect/Method;

.field private static dM:Z

.field private static dN:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 26
    sput-object v1, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->dK:Ljava/lang/Class;

    .line 27
    sput-object v1, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->dL:Ljava/lang/reflect/Method;

    .line 29
    sput v0, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->GOOGLE_PLAY_SERVICES_VERSION_CODE:I

    .line 31
    sput-boolean v0, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->dM:Z

    .line 32
    sput-boolean v0, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->dN:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createActivityRecognitionClient(Landroid/content/Context;Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)Lcom/glympse/android/hal/gms/location/ActivityRecognitionClient;
    .locals 1

    .prologue
    .line 166
    sget-boolean v0, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->dM:Z

    if-eqz v0, :cond_0

    .line 168
    new-instance v0, Lcom/glympse/android/hal/gms/gms7/location/ActivityRecognitionClient;

    invoke-direct {v0, p0, p1, p2}, Lcom/glympse/android/hal/gms/gms7/location/ActivityRecognitionClient;-><init>(Landroid/content/Context;Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V

    .line 176
    :goto_0
    return-object v0

    .line 171
    :cond_0
    sget-boolean v0, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->dN:Z

    if-eqz v0, :cond_1

    .line 173
    new-instance v0, Lcom/glympse/android/hal/gms/gms6/location/ActivityRecognitionClient;

    invoke-direct {v0, p0, p1, p2}, Lcom/glympse/android/hal/gms/gms6/location/ActivityRecognitionClient;-><init>(Landroid/content/Context;Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V

    goto :goto_0

    .line 176
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static createLocationClient(Landroid/content/Context;Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)Lcom/glympse/android/hal/gms/location/LocationClient;
    .locals 1

    .prologue
    .line 182
    sget-boolean v0, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->dM:Z

    if-eqz v0, :cond_0

    .line 184
    new-instance v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;

    invoke-direct {v0, p0, p1, p2}, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;-><init>(Landroid/content/Context;Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V

    .line 190
    :goto_0
    return-object v0

    .line 186
    :cond_0
    sget-boolean v0, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->dN:Z

    if-eqz v0, :cond_1

    .line 188
    new-instance v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;

    invoke-direct {v0, p0, p1, p2}, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;-><init>(Landroid/content/Context;Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V

    goto :goto_0

    .line 190
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getGeofencingEventFromIntent(Landroid/content/Context;Landroid/content/Intent;)Lcom/glympse/android/hal/gms/location/LocationClient$GeofencingEvent;
    .locals 1

    .prologue
    .line 195
    sget-boolean v0, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->dM:Z

    if-eqz v0, :cond_0

    .line 197
    new-instance v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient$GeofencingEvent;

    invoke-direct {v0, p1}, Lcom/glympse/android/hal/gms/gms7/location/LocationClient$GeofencingEvent;-><init>(Landroid/content/Intent;)V

    .line 203
    :goto_0
    return-object v0

    .line 199
    :cond_0
    sget-boolean v0, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->dN:Z

    if-eqz v0, :cond_1

    .line 201
    new-instance v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient$GeofencingEvent;

    invoke-direct {v0, p1}, Lcom/glympse/android/hal/gms/gms6/location/LocationClient$GeofencingEvent;-><init>(Landroid/content/Intent;)V

    goto :goto_0

    .line 203
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static init()Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 129
    :try_start_0
    sget-object v2, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->dK:Ljava/lang/Class;

    if-eqz v2, :cond_0

    .line 159
    :goto_0
    return v1

    .line 134
    :cond_0
    const-string v2, "com.google.android.gms.common.GooglePlayServicesUtil"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    sput-object v2, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->dK:Ljava/lang/Class;

    .line 135
    sget-object v2, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->dK:Ljava/lang/Class;

    const-string v3, "isGooglePlayServicesAvailable"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Landroid/content/Context;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    sput-object v2, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->dL:Ljava/lang/reflect/Method;

    .line 138
    sget-object v2, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->dK:Ljava/lang/Class;

    if-eqz v2, :cond_1

    .line 140
    invoke-static {}, Lcom/glympse/android/hal/gms/location/DetectedActivity;->init()V

    .line 141
    invoke-static {}, Lcom/glympse/android/hal/gms/location/ActivityRecognitionResult;->init()V

    .line 142
    invoke-static {}, Lcom/glympse/android/hal/gms/location/LocationRequest;->init()V

    .line 143
    invoke-static {}, Lcom/glympse/android/hal/gms/location/Geofence;->init()V

    .line 145
    invoke-static {}, Lcom/glympse/android/hal/gms/gms7/common/GooglePlayServicesClient;->init()Z

    move-result v2

    sput-boolean v2, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->dM:Z

    .line 146
    sget-boolean v2, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->dM:Z

    if-nez v2, :cond_1

    .line 148
    invoke-static {}, Lcom/glympse/android/hal/gms/gms6/common/GooglePlayServicesClient;->init()Z

    move-result v2

    sput-boolean v2, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->dN:Z

    .line 153
    :cond_1
    sget-object v2, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->dK:Ljava/lang/Class;

    const-string v3, "GOOGLE_PLAY_SERVICES_VERSION_CODE"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 154
    sget-object v3, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->dK:Ljava/lang/Class;

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v2

    sput v2, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->GOOGLE_PLAY_SERVICES_VERSION_CODE:I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 159
    :goto_1
    sget-boolean v2, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->dM:Z

    if-nez v2, :cond_2

    sget-boolean v2, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->dN:Z

    if-eqz v2, :cond_3

    :cond_2
    move v0, v1

    :cond_3
    move v1, v0

    goto :goto_0

    .line 156
    :catch_0
    move-exception v2

    goto :goto_1
.end method

.method public static isActivityRecognitionSupported(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 41
    sget-boolean v0, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->dM:Z

    if-eqz v0, :cond_0

    .line 43
    invoke-static {p0}, Lcom/glympse/android/hal/gms/gms7/location/ActivityRecognitionClient;->isSupported(Landroid/content/Context;)Z

    move-result v0

    .line 49
    :goto_0
    return v0

    .line 45
    :cond_0
    sget-boolean v0, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->dN:Z

    if-eqz v0, :cond_1

    .line 47
    invoke-static {p0}, Lcom/glympse/android/hal/gms/gms6/location/ActivityRecognitionClient;->isSupported(Landroid/content/Context;)Z

    move-result v0

    goto :goto_0

    .line 49
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isGeofencingSupported(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 54
    sget-boolean v0, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->dM:Z

    if-eqz v0, :cond_0

    .line 56
    invoke-static {p0}, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->isGeofencingSupported(Landroid/content/Context;)Z

    move-result v0

    .line 62
    :goto_0
    return v0

    .line 58
    :cond_0
    sget-boolean v0, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->dN:Z

    if-eqz v0, :cond_1

    .line 60
    invoke-static {p0}, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->isGeofencingSupported(Landroid/content/Context;)Z

    move-result v0

    goto :goto_0

    .line 62
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isGooglePlayServicesAvailable(Landroid/content/Context;)I
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 114
    :try_start_0
    sget-object v0, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->dK:Ljava/lang/Class;

    if-eqz v0, :cond_0

    .line 116
    sget-object v0, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->dL:Ljava/lang/reflect/Method;

    const/4 v2, 0x0

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 122
    :goto_0
    return v0

    .line 119
    :catch_0
    move-exception v0

    :cond_0
    move v0, v1

    .line 122
    goto :goto_0
.end method

.method public static isLocationSupported(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 67
    sget-boolean v0, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->dM:Z

    if-eqz v0, :cond_0

    .line 69
    invoke-static {p0}, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->isLocationSupported(Landroid/content/Context;)Z

    move-result v0

    .line 75
    :goto_0
    return v0

    .line 71
    :cond_0
    sget-boolean v0, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->dN:Z

    if-eqz v0, :cond_1

    .line 73
    invoke-static {p0}, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->isLocationSupported(Landroid/content/Context;)Z

    move-result v0

    goto :goto_0

    .line 75
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSupported(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 36
    invoke-static {p0}, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isVersionSupported(Landroid/content/Context;I)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 83
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 84
    const-string v2, "com.google.android.gms"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 85
    if-nez v1, :cond_1

    .line 106
    :cond_0
    :goto_0
    return v0

    .line 89
    :cond_1
    iget v1, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    .line 90
    if-lt v1, p1, :cond_0

    .line 96
    sget v1, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->GOOGLE_PLAY_SERVICES_VERSION_CODE:I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 97
    if-lt v1, p1, :cond_0

    .line 102
    const/4 v0, 0x1

    goto :goto_0

    .line 104
    :catch_0
    move-exception v1

    goto :goto_0
.end method
