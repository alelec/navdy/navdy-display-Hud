.class Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$a;
.super Ljava/lang/Object;
.source "GoogleApiClient.java"

# interfaces
.implements Ljava/lang/reflect/InvocationHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private eP:Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$OnStatusCallbackListener;


# direct methods
.method private constructor <init>(Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$OnStatusCallbackListener;)V
    .locals 0

    .prologue
    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 164
    iput-object p1, p0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$a;->eP:Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$OnStatusCallbackListener;

    .line 165
    return-void
.end method

.method public static a(Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$OnStatusCallbackListener;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 157
    # getter for: Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->eC:Ljava/lang/Class;
    invoke-static {}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->access$000()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    .line 158
    # getter for: Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->eC:Ljava/lang/Class;
    invoke-static {}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->access$000()Ljava/lang/Class;

    move-result-object v3

    aput-object v3, v1, v2

    new-instance v2, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$a;

    invoke-direct {v2, p0}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$a;-><init>(Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$OnStatusCallbackListener;)V

    .line 156
    invoke-static {v0, v1, v2}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 171
    :try_start_0
    # getter for: Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->eJ:Ljava/lang/reflect/Method;
    invoke-static {}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->access$100()Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/reflect/Method;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    iget-object v1, p0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$a;->eP:Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$OnStatusCallbackListener;

    const/4 v0, 0x0

    aget-object v0, p3, v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v0}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$OnStatusCallbackListener;->onResult(I)V

    .line 200
    :goto_0
    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 175
    :cond_0
    # getter for: Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->er:Ljava/lang/reflect/Method;
    invoke-static {}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->access$200()Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/reflect/Method;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 177
    iget-object v0, p0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$a;->eP:Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$OnStatusCallbackListener;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    .line 179
    :cond_1
    invoke-static {}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->ah()Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/reflect/Method;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    if-eqz v0, :cond_2

    .line 183
    const/4 v0, 0x0

    :try_start_1
    aget-object v0, p3, v0

    .line 184
    invoke-static {v0}, Ljava/lang/reflect/Proxy;->getInvocationHandler(Ljava/lang/Object;)Ljava/lang/reflect/InvocationHandler;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$a;

    .line 185
    iget-object v1, p0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$a;->eP:Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$OnStatusCallbackListener;

    iget-object v0, v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$a;->eP:Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$OnStatusCallbackListener;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_1

    .line 187
    :catch_0
    move-exception v0

    .line 189
    const/4 v0, 0x0

    :try_start_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1

    .line 194
    :cond_2
    # getter for: Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->eC:Ljava/lang/Class;
    invoke-static {}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->access$000()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p2, v0, p3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v0

    goto :goto_1

    .line 197
    :catch_1
    move-exception v0

    goto :goto_0
.end method
