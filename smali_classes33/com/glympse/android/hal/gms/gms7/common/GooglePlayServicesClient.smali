.class public Lcom/glympse/android/hal/gms/gms7/common/GooglePlayServicesClient;
.super Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient;
.source "GooglePlayServicesClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/hal/gms/gms7/common/GooglePlayServicesClient$OnConnectionFailedListenerProxy;,
        Lcom/glympse/android/hal/gms/gms7/common/GooglePlayServicesClient$ConnectionCallbacksProxy;
    }
.end annotation


# static fields
.field static _ConnectionCallbacks:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field static _OnConnectionFailedListener:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static dQ:Ljava/lang/reflect/Method;

.field private static dS:Ljava/lang/reflect/Method;

.field private static eQ:Ljava/lang/reflect/Method;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 17
    sput-object v0, Lcom/glympse/android/hal/gms/gms7/common/GooglePlayServicesClient;->_ConnectionCallbacks:Ljava/lang/Class;

    .line 18
    sput-object v0, Lcom/glympse/android/hal/gms/gms7/common/GooglePlayServicesClient;->dQ:Ljava/lang/reflect/Method;

    .line 19
    sput-object v0, Lcom/glympse/android/hal/gms/gms7/common/GooglePlayServicesClient;->eQ:Ljava/lang/reflect/Method;

    .line 71
    sput-object v0, Lcom/glympse/android/hal/gms/gms7/common/GooglePlayServicesClient;->_OnConnectionFailedListener:Ljava/lang/Class;

    .line 72
    sput-object v0, Lcom/glympse/android/hal/gms/gms7/common/GooglePlayServicesClient;->dS:Ljava/lang/reflect/Method;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient;-><init>()V

    return-void
.end method

.method static synthetic X()Ljava/lang/reflect/Method;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/common/GooglePlayServicesClient;->dQ:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/reflect/Method;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/common/GooglePlayServicesClient;->eQ:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/reflect/Method;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/common/GooglePlayServicesClient;->dS:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method public static init()Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 115
    :try_start_0
    invoke-static {}, Lcom/glympse/android/hal/gms/gms7/common/ConnectionResult;->init()Z

    .line 116
    const-string v2, "com.google.android.gms.common.api.GoogleApiClient$ConnectionCallbacks"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    sput-object v2, Lcom/glympse/android/hal/gms/gms7/common/GooglePlayServicesClient;->_ConnectionCallbacks:Ljava/lang/Class;

    .line 117
    sget-object v2, Lcom/glympse/android/hal/gms/gms7/common/GooglePlayServicesClient;->_ConnectionCallbacks:Ljava/lang/Class;

    const-string v3, "onConnected"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Landroid/os/Bundle;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    sput-object v2, Lcom/glympse/android/hal/gms/gms7/common/GooglePlayServicesClient;->dQ:Ljava/lang/reflect/Method;

    .line 118
    sget-object v2, Lcom/glympse/android/hal/gms/gms7/common/GooglePlayServicesClient;->_ConnectionCallbacks:Ljava/lang/Class;

    const-string v3, "onConnectionSuspended"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    sput-object v2, Lcom/glympse/android/hal/gms/gms7/common/GooglePlayServicesClient;->eQ:Ljava/lang/reflect/Method;

    .line 120
    const-string v2, "com.google.android.gms.common.api.GoogleApiClient$OnConnectionFailedListener"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    sput-object v2, Lcom/glympse/android/hal/gms/gms7/common/GooglePlayServicesClient;->_OnConnectionFailedListener:Ljava/lang/Class;

    .line 121
    sget-object v2, Lcom/glympse/android/hal/gms/gms7/common/GooglePlayServicesClient;->_OnConnectionFailedListener:Ljava/lang/Class;

    const-string v3, "onConnectionFailed"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Lcom/glympse/android/hal/gms/gms7/common/ConnectionResult;->_ConnectionResult:Ljava/lang/Class;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    sput-object v2, Lcom/glympse/android/hal/gms/gms7/common/GooglePlayServicesClient;->dS:Ljava/lang/reflect/Method;

    .line 123
    invoke-static {}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->init()V

    .line 124
    invoke-static {}, Lcom/glympse/android/hal/gms/gms7/location/ActivityRecognitionClient;->init()Z

    .line 125
    invoke-static {}, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->init()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 131
    :goto_0
    return v0

    .line 128
    :catch_0
    move-exception v0

    move v0, v1

    .line 131
    goto :goto_0
.end method
