.class public Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;
.super Ljava/lang/Object;
.source "GoogleApiClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$a;,
        Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;,
        Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$OnStatusCallbackListener;
    }
.end annotation


# static fields
.field public static _GoogleApiClient:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static dX:Ljava/lang/reflect/Method;

.field private static dY:Ljava/lang/reflect/Method;

.field private static eC:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static eD:Ljava/lang/reflect/Method;

.field private static eE:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static eF:Ljava/lang/reflect/Method;

.field private static eG:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static eH:Ljava/lang/reflect/Method;

.field private static eI:Ljava/lang/reflect/Method;

.field private static eJ:Ljava/lang/reflect/Method;

.field private static er:Ljava/lang/reflect/Method;

.field private static es:Ljava/lang/reflect/Method;


# instance fields
.field private eB:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 15
    sput-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->dX:Ljava/lang/reflect/Method;

    .line 16
    sput-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->dY:Ljava/lang/reflect/Method;

    .line 146
    sput-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->eJ:Ljava/lang/reflect/Method;

    .line 147
    sput-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->es:Ljava/lang/reflect/Method;

    .line 148
    sput-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->er:Ljava/lang/reflect/Method;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->eB:Ljava/lang/Object;

    .line 31
    return-void
.end method

.method static synthetic access$000()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->eC:Ljava/lang/Class;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/reflect/Method;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->eJ:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/reflect/Method;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->er:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method static synthetic ah()Ljava/lang/reflect/Method;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->es:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method public static awaitPendingResultStatus(Ljava/lang/Object;)I
    .locals 3

    .prologue
    .line 208
    :try_start_0
    sget-object v1, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->eD:Ljava/lang/reflect/Method;

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, p0, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 209
    sget-object v2, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->eH:Ljava/lang/reflect/Method;

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v2, v1, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 215
    :goto_0
    return v0

    .line 212
    :catch_0
    move-exception v0

    .line 215
    const/16 v0, 0xd

    goto :goto_0
.end method

.method public static create(Landroid/content/Context;Ljava/lang/Object;Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;
    .locals 1

    .prologue
    .line 293
    :try_start_0
    new-instance v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;

    invoke-direct {v0, p0}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;-><init>(Landroid/content/Context;)V

    .line 294
    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;->addApi(Ljava/lang/Object;)Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;

    move-result-object v0

    .line 295
    invoke-virtual {v0, p2}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;->addConnectionCallbacks(Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;

    move-result-object v0

    .line 296
    invoke-virtual {v0, p3}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;->addOnConnectionFailedListener(Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;

    move-result-object v0

    .line 297
    invoke-virtual {v0}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;->build()Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 302
    :goto_0
    return-object v0

    .line 299
    :catch_0
    move-exception v0

    .line 302
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static init()V
    .locals 5

    .prologue
    .line 238
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->_GoogleApiClient:Ljava/lang/Class;

    if-eqz v0, :cond_0

    .line 285
    :goto_0
    return-void

    .line 245
    :cond_0
    :try_start_0
    const-string v0, "com.google.android.gms.common.api.GoogleApiClient"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->_GoogleApiClient:Ljava/lang/Class;

    .line 246
    sget-object v1, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->_GoogleApiClient:Ljava/lang/Class;

    const-string v2, "connect"

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->dX:Ljava/lang/reflect/Method;

    .line 247
    sget-object v1, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->_GoogleApiClient:Ljava/lang/Class;

    const-string v2, "disconnect"

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->dY:Ljava/lang/reflect/Method;

    .line 249
    const-string v0, "com.google.android.gms.common.api.PendingResult"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->eE:Ljava/lang/Class;

    .line 250
    const-string v0, "com.google.android.gms.common.api.ResultCallback"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->eC:Ljava/lang/Class;

    .line 251
    const-string v0, "com.google.android.gms.common.api.Status"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->eG:Ljava/lang/Class;

    .line 254
    sget-object v1, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->eE:Ljava/lang/Class;

    const-string v2, "await"

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->eD:Ljava/lang/reflect/Method;

    .line 255
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->eE:Ljava/lang/Class;

    const-string v1, "setResultCallback"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->eC:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->eF:Ljava/lang/reflect/Method;

    .line 259
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->eG:Ljava/lang/Class;

    const-string v1, "getStatusCode"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->eH:Ljava/lang/reflect/Method;

    .line 260
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->eG:Ljava/lang/Class;

    const-string v1, "isSuccess"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->eI:Ljava/lang/reflect/Method;

    .line 263
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->eC:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getMethods()[Ljava/lang/reflect/Method;

    .line 264
    const-string v0, "com.google.android.gms.common.api.Result"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 266
    sget-object v1, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->eC:Ljava/lang/Class;

    const-string v2, "onResult"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->eJ:Ljava/lang/reflect/Method;

    .line 269
    const-class v1, Ljava/lang/Object;

    const-string v2, "hashCode"

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->er:Ljava/lang/reflect/Method;

    .line 270
    const-class v0, Ljava/lang/Object;

    const-string v1, "equals"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Ljava/lang/Object;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->es:Ljava/lang/reflect/Method;

    .line 272
    const-string v0, "com.google.android.gms.common.api.GoogleApiClient$Builder"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;->_class:Ljava/lang/Class;

    .line 273
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;->_class:Ljava/lang/Class;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    const-class v3, Landroid/content/Context;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    invoke-static {v0}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;->a(Ljava/lang/reflect/Constructor;)Ljava/lang/reflect/Constructor;

    .line 274
    const-string v0, "com.google.android.gms.common.api.Api"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;->_Api:Ljava/lang/Class;

    .line 275
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;->_class:Ljava/lang/Class;

    const-string v1, "addApi"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;->_Api:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;->a(Ljava/lang/reflect/Method;)Ljava/lang/reflect/Method;

    .line 276
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;->_class:Ljava/lang/Class;

    const-string v1, "addConnectionCallbacks"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Lcom/glympse/android/hal/gms/gms7/common/GooglePlayServicesClient;->_ConnectionCallbacks:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;->b(Ljava/lang/reflect/Method;)Ljava/lang/reflect/Method;

    .line 278
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;->_class:Ljava/lang/Class;

    const-string v1, "addOnConnectionFailedListener"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Lcom/glympse/android/hal/gms/gms7/common/GooglePlayServicesClient;->_OnConnectionFailedListener:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;->c(Ljava/lang/reflect/Method;)Ljava/lang/reflect/Method;

    .line 280
    sget-object v1, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;->_class:Ljava/lang/Class;

    const-string v2, "build"

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;->d(Ljava/lang/reflect/Method;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 282
    :catch_0
    move-exception v0

    goto/16 :goto_0
.end method

.method public static isSupported()Z
    .locals 1

    .prologue
    .line 233
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->_GoogleApiClient:Ljava/lang/Class;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static setCallbackPendingResultStatus(Ljava/lang/Object;Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$OnStatusCallbackListener;)V
    .locals 4

    .prologue
    .line 223
    :try_start_0
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->eF:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 224
    invoke-static {p1}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$a;->a(Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$OnStatusCallbackListener;)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v1, v2

    .line 223
    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 229
    :goto_0
    return-void

    .line 226
    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public connect()V
    .locals 3

    .prologue
    .line 128
    :try_start_0
    sget-object v1, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->dX:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->eB:Ljava/lang/Object;

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 133
    :goto_0
    return-void

    .line 130
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public disconnect()V
    .locals 3

    .prologue
    .line 139
    :try_start_0
    sget-object v1, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->dY:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->eB:Ljava/lang/Object;

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 144
    :goto_0
    return-void

    .line 141
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public getSelf()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->eB:Ljava/lang/Object;

    return-object v0
.end method
