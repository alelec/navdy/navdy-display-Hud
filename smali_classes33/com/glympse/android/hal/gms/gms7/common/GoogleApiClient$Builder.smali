.class public Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;
.super Ljava/lang/Object;
.source "GoogleApiClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# static fields
.field public static _Api:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static _class:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static eK:Ljava/lang/reflect/Constructor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/reflect/Constructor",
            "<*>;"
        }
    .end annotation
.end field

.field private static eL:Ljava/lang/reflect/Method;

.field private static eM:Ljava/lang/reflect/Method;

.field private static eN:Ljava/lang/reflect/Method;

.field private static eO:Ljava/lang/reflect/Method;


# instance fields
.field private eB:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 49
    sput-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;->eK:Ljava/lang/reflect/Constructor;

    .line 50
    sput-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;->eL:Ljava/lang/reflect/Method;

    .line 51
    sput-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;->eM:Ljava/lang/reflect/Method;

    .line 52
    sput-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;->eN:Ljava/lang/reflect/Method;

    .line 53
    sput-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;->eO:Ljava/lang/reflect/Method;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    :try_start_0
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;->eK:Ljava/lang/reflect/Constructor;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;->eB:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    :goto_0
    return-void

    .line 64
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic a(Ljava/lang/reflect/Constructor;)Ljava/lang/reflect/Constructor;
    .locals 0

    .prologue
    .line 46
    sput-object p0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;->eK:Ljava/lang/reflect/Constructor;

    return-object p0
.end method

.method static synthetic a(Ljava/lang/reflect/Method;)Ljava/lang/reflect/Method;
    .locals 0

    .prologue
    .line 46
    sput-object p0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;->eL:Ljava/lang/reflect/Method;

    return-object p0
.end method

.method static synthetic b(Ljava/lang/reflect/Method;)Ljava/lang/reflect/Method;
    .locals 0

    .prologue
    .line 46
    sput-object p0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;->eM:Ljava/lang/reflect/Method;

    return-object p0
.end method

.method static synthetic c(Ljava/lang/reflect/Method;)Ljava/lang/reflect/Method;
    .locals 0

    .prologue
    .line 46
    sput-object p0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;->eN:Ljava/lang/reflect/Method;

    return-object p0
.end method

.method static synthetic d(Ljava/lang/reflect/Method;)Ljava/lang/reflect/Method;
    .locals 0

    .prologue
    .line 46
    sput-object p0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;->eO:Ljava/lang/reflect/Method;

    return-object p0
.end method


# virtual methods
.method public addApi(Ljava/lang/Object;)Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;
    .locals 4

    .prologue
    .line 85
    :try_start_0
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;->eL:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;->eB:Ljava/lang/Object;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 91
    :goto_0
    return-object p0

    .line 88
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public addConnectionCallbacks(Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;
    .locals 5

    .prologue
    .line 99
    :try_start_0
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;->eM:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;->eB:Ljava/lang/Object;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 100
    invoke-static {p1}, Lcom/glympse/android/hal/gms/gms7/common/GooglePlayServicesClient$ConnectionCallbacksProxy;->create(Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v3

    .line 99
    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 106
    :goto_0
    return-object p0

    .line 103
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public addOnConnectionFailedListener(Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;
    .locals 5

    .prologue
    .line 114
    :try_start_0
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;->eN:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;->eB:Ljava/lang/Object;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 115
    invoke-static {p1}, Lcom/glympse/android/hal/gms/gms7/common/GooglePlayServicesClient$OnConnectionFailedListenerProxy;->create(Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v3

    .line 114
    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    :goto_0
    return-object p0

    .line 117
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public build()Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 73
    :try_start_0
    new-instance v1, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;

    sget-object v3, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;->eO:Ljava/lang/reflect/Method;

    iget-object v4, p0, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$Builder;->eB:Ljava/lang/Object;

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;-><init>(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 78
    :goto_0
    return-object v0

    .line 75
    :catch_0
    move-exception v0

    move-object v0, v2

    .line 78
    goto :goto_0
.end method
