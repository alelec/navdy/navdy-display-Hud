.class Lcom/glympse/android/hal/gms/gms7/location/LocationClient$3;
.super Ljava/lang/Object;
.source "LocationClient.java"

# interfaces
.implements Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$OnStatusCallbackListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->removeGeofences(Landroid/app/PendingIntent;Lcom/glympse/android/hal/gms/location/LocationClient$OnRemoveGeofencesResultListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic eZ:Lcom/glympse/android/hal/gms/gms7/location/LocationClient;

.field final synthetic fa:Lcom/glympse/android/hal/gms/location/LocationClient$OnRemoveGeofencesResultListener;

.field final synthetic fb:Landroid/app/PendingIntent;


# direct methods
.method constructor <init>(Lcom/glympse/android/hal/gms/gms7/location/LocationClient;Lcom/glympse/android/hal/gms/location/LocationClient$OnRemoveGeofencesResultListener;Landroid/app/PendingIntent;)V
    .locals 0

    .prologue
    .line 173
    iput-object p1, p0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient$3;->eZ:Lcom/glympse/android/hal/gms/gms7/location/LocationClient;

    iput-object p2, p0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient$3;->fa:Lcom/glympse/android/hal/gms/location/LocationClient$OnRemoveGeofencesResultListener;

    iput-object p3, p0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient$3;->fb:Landroid/app/PendingIntent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResult(I)V
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient$3;->fa:Lcom/glympse/android/hal/gms/location/LocationClient$OnRemoveGeofencesResultListener;

    iget-object v1, p0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient$3;->fb:Landroid/app/PendingIntent;

    invoke-interface {v0, p1, v1}, Lcom/glympse/android/hal/gms/location/LocationClient$OnRemoveGeofencesResultListener;->onRemoveGeofencesByPendingIntentResult(ILandroid/app/PendingIntent;)V

    .line 178
    return-void
.end method
