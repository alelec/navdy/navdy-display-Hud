.class public Lcom/glympse/android/hal/gms/gms7/location/LocationClient$GeofencingEvent;
.super Ljava/lang/Object;
.source "LocationClient.java"

# interfaces
.implements Lcom/glympse/android/hal/gms/location/LocationClient$GeofencingEvent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/hal/gms/gms7/location/LocationClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GeofencingEvent"
.end annotation


# instance fields
.field private fc:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 188
    iput-object v0, p0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient$GeofencingEvent;->fc:Ljava/lang/Object;

    .line 194
    :try_start_0
    invoke-static {}, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->X()Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient$GeofencingEvent;->fc:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 200
    :goto_0
    return-void

    .line 196
    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public getGeofenceTransition()I
    .locals 3

    .prologue
    .line 218
    :try_start_0
    # getter for: Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->em:Ljava/lang/reflect/Method;
    invoke-static {}, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->access$200()Ljava/lang/reflect/Method;

    move-result-object v1

    iget-object v2, p0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient$GeofencingEvent;->fc:Ljava/lang/Object;

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 223
    :goto_0
    return v0

    .line 220
    :catch_0
    move-exception v0

    .line 223
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTriggeringGeofences()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/glympse/android/hal/gms/location/Geofence;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 231
    :try_start_0
    invoke-static {}, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->ah()Ljava/lang/reflect/Method;

    move-result-object v1

    iget-object v3, p0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient$GeofencingEvent;->fc:Ljava/lang/Object;

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v3, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 232
    new-instance v1, Ljava/util/Vector;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/Vector;-><init>(I)V

    .line 233
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 235
    new-instance v4, Lcom/glympse/android/hal/gms/location/Geofence;

    invoke-direct {v4, v3}, Lcom/glympse/android/hal/gms/location/Geofence;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v4}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 239
    :catch_0
    move-exception v0

    move-object v0, v2

    .line 242
    :goto_1
    return-object v0

    :cond_0
    move-object v0, v1

    .line 237
    goto :goto_1
.end method

.method public hasError()Z
    .locals 3

    .prologue
    .line 206
    :try_start_0
    # getter for: Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->el:Ljava/lang/reflect/Method;
    invoke-static {}, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->access$100()Ljava/lang/reflect/Method;

    move-result-object v1

    iget-object v2, p0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient$GeofencingEvent;->fc:Ljava/lang/Object;

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 211
    :goto_0
    return v0

    .line 208
    :catch_0
    move-exception v0

    .line 211
    const/4 v0, 0x1

    goto :goto_0
.end method
