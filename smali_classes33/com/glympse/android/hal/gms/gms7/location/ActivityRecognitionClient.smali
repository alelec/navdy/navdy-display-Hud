.class public Lcom/glympse/android/hal/gms/gms7/location/ActivityRecognitionClient;
.super Lcom/glympse/android/hal/gms/location/ActivityRecognitionClient;
.source "ActivityRecognitionClient.java"


# static fields
.field private static dZ:Ljava/lang/reflect/Method;

.field private static eR:Ljava/lang/Object;

.field private static ea:Ljava/lang/reflect/Method;

.field private static eb:Ljava/lang/Object;


# instance fields
.field private eS:Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 21
    sput-object v0, Lcom/glympse/android/hal/gms/gms7/location/ActivityRecognitionClient;->dZ:Ljava/lang/reflect/Method;

    .line 22
    sput-object v0, Lcom/glympse/android/hal/gms/gms7/location/ActivityRecognitionClient;->ea:Ljava/lang/reflect/Method;

    .line 23
    sput-object v0, Lcom/glympse/android/hal/gms/gms7/location/ActivityRecognitionClient;->eb:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/glympse/android/hal/gms/location/ActivityRecognitionClient;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/hal/gms/gms7/location/ActivityRecognitionClient;->eS:Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;

    .line 32
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/location/ActivityRecognitionClient;->eb:Ljava/lang/Object;

    invoke-static {p1, v0, p2, p3}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->create(Landroid/content/Context;Ljava/lang/Object;Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/hal/gms/gms7/location/ActivityRecognitionClient;->eS:Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;

    .line 34
    return-void
.end method

.method public static init()Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 80
    :try_start_0
    invoke-static {}, Lcom/glympse/android/hal/gms/location/DetectedActivity;->init()V

    .line 81
    invoke-static {}, Lcom/glympse/android/hal/gms/location/ActivityRecognitionResult;->init()V

    .line 82
    invoke-static {}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->init()V

    .line 84
    const-string v2, "com.google.android.gms.location.ActivityRecognition"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 85
    const-string v3, "API"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    sput-object v3, Lcom/glympse/android/hal/gms/gms7/location/ActivityRecognitionClient;->eb:Ljava/lang/Object;

    .line 86
    const-string v3, "ActivityRecognitionApi"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    sput-object v2, Lcom/glympse/android/hal/gms/gms7/location/ActivityRecognitionClient;->eR:Ljava/lang/Object;

    .line 87
    sget-object v2, Lcom/glympse/android/hal/gms/gms7/location/ActivityRecognitionClient;->eR:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 88
    const-string v3, "requestActivityUpdates"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->_GoogleApiClient:Ljava/lang/Class;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    sget-object v6, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-class v6, Landroid/app/PendingIntent;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    sput-object v3, Lcom/glympse/android/hal/gms/gms7/location/ActivityRecognitionClient;->dZ:Ljava/lang/reflect/Method;

    .line 92
    const-string v3, "removeActivityUpdates"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->_GoogleApiClient:Ljava/lang/Class;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-class v6, Landroid/app/PendingIntent;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    sput-object v2, Lcom/glympse/android/hal/gms/gms7/location/ActivityRecognitionClient;->ea:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    :goto_0
    return v0

    .line 98
    :catch_0
    move-exception v0

    move v0, v1

    .line 101
    goto :goto_0
.end method

.method public static isSupported(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 73
    invoke-static {}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->isSupported()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public connect()V
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/glympse/android/hal/gms/gms7/location/ActivityRecognitionClient;->eS:Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;

    invoke-virtual {v0}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->connect()V

    .line 39
    return-void
.end method

.method public disconnect()V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/glympse/android/hal/gms/gms7/location/ActivityRecognitionClient;->eS:Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;

    invoke-virtual {v0}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->disconnect()V

    .line 44
    return-void
.end method

.method public removeActivityUpdates(Landroid/app/PendingIntent;)V
    .locals 5

    .prologue
    .line 63
    :try_start_0
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/location/ActivityRecognitionClient;->ea:Ljava/lang/reflect/Method;

    sget-object v1, Lcom/glympse/android/hal/gms/gms7/location/ActivityRecognitionClient;->eR:Ljava/lang/Object;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/glympse/android/hal/gms/gms7/location/ActivityRecognitionClient;->eS:Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;

    .line 64
    invoke-virtual {v4}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->getSelf()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    .line 63
    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 69
    :goto_0
    return-void

    .line 66
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public requestActivityUpdates(JLandroid/app/PendingIntent;)V
    .locals 5

    .prologue
    .line 50
    :try_start_0
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/location/ActivityRecognitionClient;->dZ:Ljava/lang/reflect/Method;

    sget-object v1, Lcom/glympse/android/hal/gms/gms7/location/ActivityRecognitionClient;->eR:Ljava/lang/Object;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/glympse/android/hal/gms/gms7/location/ActivityRecognitionClient;->eS:Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;

    .line 51
    invoke-virtual {v4}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->getSelf()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object p3, v2, v3

    .line 50
    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    :goto_0
    return-void

    .line 54
    :catch_0
    move-exception v0

    goto :goto_0
.end method
