.class public Lcom/glympse/android/hal/gms/gms7/location/LocationClient;
.super Lcom/glympse/android/hal/gms/location/LocationClient;
.source "LocationClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/hal/gms/gms7/location/LocationClient$LocationListenerHandler;,
        Lcom/glympse/android/hal/gms/gms7/location/LocationClient$GeofencingEvent;
    }
.end annotation


# static fields
.field private static eU:Ljava/lang/Object;

.field private static eV:Ljava/lang/Object;

.field private static eW:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static eX:Ljava/lang/reflect/Method;

.field private static eb:Ljava/lang/Object;

.field private static ef:Ljava/lang/reflect/Method;

.field private static eg:Ljava/lang/reflect/Method;

.field private static eh:Ljava/lang/reflect/Method;

.field private static ei:Ljava/lang/reflect/Method;

.field private static ej:Ljava/lang/reflect/Method;

.field private static ek:Ljava/lang/reflect/Method;

.field private static el:Ljava/lang/reflect/Method;

.field private static em:Ljava/lang/reflect/Method;

.field private static en:Ljava/lang/reflect/Method;

.field private static ep:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static eq:Ljava/lang/reflect/Method;

.field private static er:Ljava/lang/reflect/Method;

.field private static es:Ljava/lang/reflect/Method;


# instance fields
.field eT:Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29
    sput-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eb:Ljava/lang/Object;

    .line 30
    sput-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eU:Ljava/lang/Object;

    .line 32
    sput-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->ef:Ljava/lang/reflect/Method;

    .line 33
    sput-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eg:Ljava/lang/reflect/Method;

    .line 34
    sput-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eh:Ljava/lang/reflect/Method;

    .line 36
    sput-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eV:Ljava/lang/Object;

    .line 37
    sput-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->ei:Ljava/lang/reflect/Method;

    .line 38
    sput-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->ej:Ljava/lang/reflect/Method;

    .line 39
    sput-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->ek:Ljava/lang/reflect/Method;

    .line 42
    sput-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eW:Ljava/lang/Class;

    .line 43
    sput-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->el:Ljava/lang/reflect/Method;

    .line 44
    sput-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->em:Ljava/lang/reflect/Method;

    .line 45
    sput-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->en:Ljava/lang/reflect/Method;

    .line 46
    sput-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eX:Ljava/lang/reflect/Method;

    .line 249
    sput-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->ep:Ljava/lang/Class;

    .line 250
    sput-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eq:Ljava/lang/reflect/Method;

    .line 251
    sput-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->er:Ljava/lang/reflect/Method;

    .line 252
    sput-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->es:Ljava/lang/reflect/Method;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/glympse/android/hal/gms/location/LocationClient;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eT:Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;

    .line 52
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eb:Ljava/lang/Object;

    invoke-static {p1, v0, p2, p3}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->create(Landroid/content/Context;Ljava/lang/Object;Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eT:Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;

    .line 53
    return-void
.end method

.method static synthetic X()Ljava/lang/reflect/Method;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eX:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method static synthetic aa()Ljava/lang/reflect/Method;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eq:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method static synthetic ab()Ljava/lang/reflect/Method;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->er:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/reflect/Method;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->el:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/reflect/Method;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->em:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method static synthetic ah()Ljava/lang/reflect/Method;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->en:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method static synthetic ai()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->ep:Ljava/lang/Class;

    return-object v0
.end method

.method static synthetic aj()Ljava/lang/reflect/Method;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->es:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method public static init()Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 325
    :try_start_0
    const-string v0, "com.google.android.gms.location.LocationListener"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->ep:Ljava/lang/Class;

    .line 326
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->ep:Ljava/lang/Class;

    const-string v3, "onLocationChanged"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Landroid/location/Location;

    aput-object v6, v4, v5

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eq:Ljava/lang/reflect/Method;

    .line 327
    const-class v3, Ljava/lang/Object;

    const-string v4, "hashCode"

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Class;

    invoke-virtual {v3, v4, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->er:Ljava/lang/reflect/Method;

    .line 328
    const-class v0, Ljava/lang/Object;

    const-string v3, "equals"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Ljava/lang/Object;

    aput-object v6, v4, v5

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->es:Ljava/lang/reflect/Method;

    .line 330
    const-string v0, "com.google.android.gms.location.LocationServices"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 331
    const-string v3, "API"

    invoke-virtual {v0, v3}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    sput-object v3, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eb:Ljava/lang/Object;

    .line 332
    const-string v3, "FusedLocationApi"

    invoke-virtual {v0, v3}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    sput-object v3, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eU:Ljava/lang/Object;

    .line 334
    sget-object v3, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eU:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "requestLocationUpdates"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    sget-object v7, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->_GoogleApiClient:Ljava/lang/Class;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    sget-object v7, Lcom/glympse/android/hal/gms/location/LocationRequest;->_LocationRequest:Ljava/lang/Class;

    aput-object v7, v5, v6

    const/4 v6, 0x2

    sget-object v7, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->ep:Ljava/lang/Class;

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    sput-object v3, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->ef:Ljava/lang/reflect/Method;

    .line 338
    sget-object v3, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eU:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "removeLocationUpdates"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    sget-object v7, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->_GoogleApiClient:Ljava/lang/Class;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    sget-object v7, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->ep:Ljava/lang/Class;

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    sput-object v3, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eg:Ljava/lang/reflect/Method;

    .line 341
    sget-object v3, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eU:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "getLastLocation"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    sget-object v7, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->_GoogleApiClient:Ljava/lang/Class;

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    sput-object v3, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eh:Ljava/lang/reflect/Method;

    .line 344
    const-string v3, "GeofencingApi"

    invoke-virtual {v0, v3}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eV:Ljava/lang/Object;

    .line 345
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eV:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v3, "addGeofences"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->_GoogleApiClient:Ljava/lang/Class;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-class v6, Ljava/util/List;

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-class v6, Landroid/app/PendingIntent;

    aput-object v6, v4, v5

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->ei:Ljava/lang/reflect/Method;

    .line 347
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eV:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v3, "removeGeofences"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->_GoogleApiClient:Ljava/lang/Class;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-class v6, Ljava/util/List;

    aput-object v6, v4, v5

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->ej:Ljava/lang/reflect/Method;

    .line 349
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eV:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v3, "removeGeofences"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->_GoogleApiClient:Ljava/lang/Class;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-class v6, Landroid/app/PendingIntent;

    aput-object v6, v4, v5

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->ek:Ljava/lang/reflect/Method;

    .line 352
    const-string v0, "com.google.android.gms.location.GeofencingEvent"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eW:Ljava/lang/Class;

    .line 353
    sget-object v3, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eW:Ljava/lang/Class;

    const-string v4, "hasError"

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Class;

    invoke-virtual {v3, v4, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->el:Ljava/lang/reflect/Method;

    .line 354
    sget-object v3, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eW:Ljava/lang/Class;

    const-string v4, "getGeofenceTransition"

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Class;

    invoke-virtual {v3, v4, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->em:Ljava/lang/reflect/Method;

    .line 355
    sget-object v3, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eW:Ljava/lang/Class;

    const-string v4, "getTriggeringGeofences"

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Class;

    invoke-virtual {v3, v4, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->en:Ljava/lang/reflect/Method;

    .line 356
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eW:Ljava/lang/Class;

    const-string v3, "fromIntent"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Landroid/content/Intent;

    aput-object v6, v4, v5

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eX:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 362
    :goto_0
    return v0

    .line 359
    :catch_0
    move-exception v0

    move v0, v2

    .line 362
    goto :goto_0
.end method

.method public static isGeofencingSupported(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 313
    invoke-static {}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->isSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->ei:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isLocationSupported(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 318
    invoke-static {}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->isSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->ef:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSupported(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 57
    invoke-static {}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->isSupported()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public addGeofences(Ljava/util/List;Landroid/app/PendingIntent;Lcom/glympse/android/hal/gms/location/LocationClient$OnAddGeofencesResultListener;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/glympse/android/hal/gms/location/Geofence;",
            ">;",
            "Landroid/app/PendingIntent;",
            "Lcom/glympse/android/hal/gms/location/LocationClient$OnAddGeofencesResultListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 118
    :try_start_0
    new-instance v1, Ljava/util/Vector;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/Vector;-><init>(I)V

    .line 119
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/hal/gms/location/Geofence;

    .line 121
    invoke-virtual {v0}, Lcom/glympse/android/hal/gms/location/Geofence;->self()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0

    .line 137
    :catch_0
    move-exception v0

    .line 140
    :goto_1
    return-void

    .line 125
    :cond_0
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->ei:Ljava/lang/reflect/Method;

    sget-object v2, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eV:Ljava/lang/Object;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eT:Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;

    .line 126
    invoke-virtual {v5}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->getSelf()Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v1, v3, v4

    const/4 v1, 0x2

    aput-object p2, v3, v1

    .line 125
    invoke-virtual {v0, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 127
    new-instance v1, Lcom/glympse/android/hal/gms/gms7/location/LocationClient$1;

    invoke-direct {v1, p0, p3}, Lcom/glympse/android/hal/gms/gms7/location/LocationClient$1;-><init>(Lcom/glympse/android/hal/gms/gms7/location/LocationClient;Lcom/glympse/android/hal/gms/location/LocationClient$OnAddGeofencesResultListener;)V

    invoke-static {v0, v1}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->setCallbackPendingResultStatus(Ljava/lang/Object;Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$OnStatusCallbackListener;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1
.end method

.method public connect()V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eT:Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;

    invoke-virtual {v0}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->connect()V

    .line 63
    return-void
.end method

.method public disconnect()V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eT:Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;

    invoke-virtual {v0}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->disconnect()V

    .line 68
    return-void
.end method

.method public getLastLocation()Landroid/location/Location;
    .locals 5

    .prologue
    .line 103
    :try_start_0
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eh:Ljava/lang/reflect/Method;

    sget-object v1, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eU:Ljava/lang/Object;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eT:Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;

    .line 104
    invoke-virtual {v4}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->getSelf()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v3

    .line 103
    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 109
    :goto_0
    return-object v0

    .line 106
    :catch_0
    move-exception v0

    .line 109
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeGeofences(Landroid/app/PendingIntent;Lcom/glympse/android/hal/gms/location/LocationClient$OnRemoveGeofencesResultListener;)V
    .locals 5

    .prologue
    .line 169
    :try_start_0
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->ek:Ljava/lang/reflect/Method;

    sget-object v1, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eV:Ljava/lang/Object;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eT:Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;

    .line 170
    invoke-virtual {v4}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->getSelf()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    .line 169
    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 171
    new-instance v1, Lcom/glympse/android/hal/gms/gms7/location/LocationClient$3;

    invoke-direct {v1, p0, p2, p1}, Lcom/glympse/android/hal/gms/gms7/location/LocationClient$3;-><init>(Lcom/glympse/android/hal/gms/gms7/location/LocationClient;Lcom/glympse/android/hal/gms/location/LocationClient$OnRemoveGeofencesResultListener;Landroid/app/PendingIntent;)V

    invoke-static {v0, v1}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->setCallbackPendingResultStatus(Ljava/lang/Object;Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$OnStatusCallbackListener;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 184
    :goto_0
    return-void

    .line 181
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public removeGeofences(Ljava/util/List;Lcom/glympse/android/hal/gms/location/LocationClient$OnRemoveGeofencesResultListener;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/glympse/android/hal/gms/location/LocationClient$OnRemoveGeofencesResultListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 147
    :try_start_0
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->ej:Ljava/lang/reflect/Method;

    sget-object v1, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eV:Ljava/lang/Object;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eT:Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;

    .line 148
    invoke-virtual {v4}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->getSelf()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    .line 147
    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 149
    new-instance v1, Lcom/glympse/android/hal/gms/gms7/location/LocationClient$2;

    invoke-direct {v1, p0, p2}, Lcom/glympse/android/hal/gms/gms7/location/LocationClient$2;-><init>(Lcom/glympse/android/hal/gms/gms7/location/LocationClient;Lcom/glympse/android/hal/gms/location/LocationClient$OnRemoveGeofencesResultListener;)V

    invoke-static {v0, v1}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->setCallbackPendingResultStatus(Ljava/lang/Object;Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient$OnStatusCallbackListener;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    :goto_0
    return-void

    .line 159
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public removeLocationUpdates(Lcom/glympse/android/hal/gms/location/LocationListener;)V
    .locals 5

    .prologue
    .line 89
    :try_start_0
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eg:Ljava/lang/reflect/Method;

    sget-object v1, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eU:Ljava/lang/Object;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eT:Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;

    .line 91
    invoke-virtual {v4}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->getSelf()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 92
    invoke-static {p1}, Lcom/glympse/android/hal/gms/gms7/location/LocationClient$LocationListenerHandler;->create(Lcom/glympse/android/hal/gms/location/LocationListener;)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v3

    .line 89
    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 97
    :goto_0
    return-void

    .line 94
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public requestLocationUpdates(Lcom/glympse/android/hal/gms/location/LocationRequest;Lcom/glympse/android/hal/gms/location/LocationListener;)V
    .locals 5

    .prologue
    .line 74
    :try_start_0
    sget-object v0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->ef:Ljava/lang/reflect/Method;

    sget-object v1, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eU:Ljava/lang/Object;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/glympse/android/hal/gms/gms7/location/LocationClient;->eT:Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;

    .line 76
    invoke-virtual {v4}, Lcom/glympse/android/hal/gms/gms7/common/GoogleApiClient;->getSelf()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 77
    invoke-virtual {p1}, Lcom/glympse/android/hal/gms/location/LocationRequest;->getRequest()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    .line 78
    invoke-static {p2}, Lcom/glympse/android/hal/gms/gms7/location/LocationClient$LocationListenerHandler;->create(Lcom/glympse/android/hal/gms/location/LocationListener;)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v3

    .line 74
    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    :goto_0
    return-void

    .line 80
    :catch_0
    move-exception v0

    goto :goto_0
.end method
