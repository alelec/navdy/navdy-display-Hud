.class public Lcom/glympse/android/hal/gms/gms6/common/GooglePlayServicesClient$OnConnectionFailedListenerProxy;
.super Ljava/lang/Object;
.source "GooglePlayServicesClient.java"

# interfaces
.implements Ljava/lang/reflect/InvocationHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/hal/gms/gms6/common/GooglePlayServicesClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "OnConnectionFailedListenerProxy"
.end annotation


# instance fields
.field private dU:Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;


# direct methods
.method private constructor <init>(Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput-object p1, p0, Lcom/glympse/android/hal/gms/gms6/common/GooglePlayServicesClient$OnConnectionFailedListenerProxy;->dU:Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;

    .line 80
    return-void
.end method

.method public static create(Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 71
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/common/GooglePlayServicesClient;->_OnConnectionFailedListener:Ljava/lang/Class;

    .line 72
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    sget-object v3, Lcom/glympse/android/hal/gms/gms6/common/GooglePlayServicesClient;->_OnConnectionFailedListener:Ljava/lang/Class;

    aput-object v3, v1, v2

    new-instance v2, Lcom/glympse/android/hal/gms/gms6/common/GooglePlayServicesClient$OnConnectionFailedListenerProxy;

    invoke-direct {v2, p0}, Lcom/glympse/android/hal/gms/gms6/common/GooglePlayServicesClient$OnConnectionFailedListenerProxy;-><init>(Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V

    .line 71
    invoke-static {v0, v1, v2}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 86
    :try_start_0
    # getter for: Lcom/glympse/android/hal/gms/gms6/common/GooglePlayServicesClient;->dS:Ljava/lang/reflect/Method;
    invoke-static {}, Lcom/glympse/android/hal/gms/gms6/common/GooglePlayServicesClient;->access$200()Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/reflect/Method;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/glympse/android/hal/gms/gms6/common/GooglePlayServicesClient$OnConnectionFailedListenerProxy;->dU:Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;

    new-instance v1, Lcom/glympse/android/hal/gms/gms6/common/ConnectionResult;

    const/4 v2, 0x0

    aget-object v2, p3, v2

    invoke-direct {v1, v2}, Lcom/glympse/android/hal/gms/gms6/common/ConnectionResult;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;->onConnectionFailed(Lcom/glympse/android/hal/gms/common/ConnectionResult;)V

    .line 98
    :goto_0
    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/hal/gms/gms6/common/GooglePlayServicesClient$OnConnectionFailedListenerProxy;->dU:Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;

    invoke-virtual {p2, v0, p3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_1

    .line 95
    :catch_0
    move-exception v0

    goto :goto_0
.end method
