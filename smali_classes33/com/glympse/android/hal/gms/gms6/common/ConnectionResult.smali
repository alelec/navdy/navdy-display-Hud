.class public Lcom/glympse/android/hal/gms/gms6/common/ConnectionResult;
.super Lcom/glympse/android/hal/gms/common/ConnectionResult;
.source "ConnectionResult.java"


# static fields
.field public static _ConnectionResult:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static dO:Ljava/lang/reflect/Method;


# instance fields
.field private dP:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const/4 v0, 0x0

    sput-object v0, Lcom/glympse/android/hal/gms/gms6/common/ConnectionResult;->dO:Ljava/lang/reflect/Method;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/glympse/android/hal/gms/common/ConnectionResult;-><init>()V

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/hal/gms/gms6/common/ConnectionResult;->dP:Ljava/lang/Object;

    .line 20
    iput-object p1, p0, Lcom/glympse/android/hal/gms/gms6/common/ConnectionResult;->dP:Ljava/lang/Object;

    .line 21
    return-void
.end method

.method public static init()Z
    .locals 3

    .prologue
    .line 44
    :try_start_0
    const-string v0, "com.google.android.gms.location.ActivityRecognitionResult"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms6/common/ConnectionResult;->_ConnectionResult:Ljava/lang/Class;

    .line 45
    sget-object v1, Lcom/glympse/android/hal/gms/gms6/common/ConnectionResult;->_ConnectionResult:Ljava/lang/Class;

    const-string v2, "getErrorCode"

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms6/common/ConnectionResult;->dO:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 46
    const/4 v0, 0x1

    .line 51
    :goto_0
    return v0

    .line 48
    :catch_0
    move-exception v0

    .line 51
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSupported()Z
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/common/ConnectionResult;->_ConnectionResult:Ljava/lang/Class;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getErrorCode()I
    .locals 3

    .prologue
    .line 32
    :try_start_0
    sget-object v1, Lcom/glympse/android/hal/gms/gms6/common/ConnectionResult;->dO:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lcom/glympse/android/hal/gms/gms6/common/ConnectionResult;->dP:Ljava/lang/Object;

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 37
    :goto_0
    return v0

    .line 34
    :catch_0
    move-exception v0

    .line 37
    const/16 v0, 0x8

    goto :goto_0
.end method
