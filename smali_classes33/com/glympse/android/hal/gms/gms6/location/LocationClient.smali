.class public Lcom/glympse/android/hal/gms/gms6/location/LocationClient;
.super Lcom/glympse/android/hal/gms/location/LocationClient;
.source "LocationClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/hal/gms/gms6/location/LocationClient$OnRemoveGeofencesResultListenerProxy;,
        Lcom/glympse/android/hal/gms/gms6/location/LocationClient$OnAddGeofencesResultListenerProxy;,
        Lcom/glympse/android/hal/gms/gms6/location/LocationClient$LocationListenerHandler;,
        Lcom/glympse/android/hal/gms/gms6/location/LocationClient$GeofencingEvent;
    }
.end annotation


# static fields
.field private static dX:Ljava/lang/reflect/Method;

.field private static dY:Ljava/lang/reflect/Method;

.field private static ed:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static ee:Ljava/lang/reflect/Constructor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/reflect/Constructor",
            "<*>;"
        }
    .end annotation
.end field

.field private static ef:Ljava/lang/reflect/Method;

.field private static eg:Ljava/lang/reflect/Method;

.field private static eh:Ljava/lang/reflect/Method;

.field private static ei:Ljava/lang/reflect/Method;

.field private static ej:Ljava/lang/reflect/Method;

.field private static ek:Ljava/lang/reflect/Method;

.field private static el:Ljava/lang/reflect/Method;

.field private static em:Ljava/lang/reflect/Method;

.field private static en:Ljava/lang/reflect/Method;

.field private static ep:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static eq:Ljava/lang/reflect/Method;

.field private static er:Ljava/lang/reflect/Method;

.field private static es:Ljava/lang/reflect/Method;

.field private static et:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static eu:Ljava/lang/reflect/Method;

.field private static ev:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static ew:Ljava/lang/reflect/Method;

.field private static ex:Ljava/lang/reflect/Method;


# instance fields
.field private eo:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29
    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ed:Ljava/lang/Class;

    .line 30
    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ee:Ljava/lang/reflect/Constructor;

    .line 31
    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->dX:Ljava/lang/reflect/Method;

    .line 32
    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->dY:Ljava/lang/reflect/Method;

    .line 33
    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ef:Ljava/lang/reflect/Method;

    .line 34
    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->eg:Ljava/lang/reflect/Method;

    .line 35
    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->eh:Ljava/lang/reflect/Method;

    .line 36
    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ei:Ljava/lang/reflect/Method;

    .line 37
    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ej:Ljava/lang/reflect/Method;

    .line 38
    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ek:Ljava/lang/reflect/Method;

    .line 39
    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->el:Ljava/lang/reflect/Method;

    .line 40
    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->em:Ljava/lang/reflect/Method;

    .line 41
    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->en:Ljava/lang/reflect/Method;

    .line 220
    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ep:Ljava/lang/Class;

    .line 221
    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->eq:Ljava/lang/reflect/Method;

    .line 222
    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->er:Ljava/lang/reflect/Method;

    .line 223
    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->es:Ljava/lang/reflect/Method;

    .line 282
    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->et:Ljava/lang/Class;

    .line 283
    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->eu:Ljava/lang/reflect/Method;

    .line 331
    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ev:Ljava/lang/Class;

    .line 332
    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ew:Ljava/lang/reflect/Method;

    .line 333
    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ex:Ljava/lang/reflect/Method;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V
    .locals 4

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/glympse/android/hal/gms/location/LocationClient;-><init>()V

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->eo:Ljava/lang/Object;

    .line 51
    :try_start_0
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ee:Ljava/lang/reflect/Constructor;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    .line 53
    invoke-static {p2}, Lcom/glympse/android/hal/gms/gms6/common/GooglePlayServicesClient$ConnectionCallbacksProxy;->create(Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    .line 54
    invoke-static {p3}, Lcom/glympse/android/hal/gms/gms6/common/GooglePlayServicesClient$OnConnectionFailedListenerProxy;->create(Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v1, v2

    .line 51
    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->eo:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    :goto_0
    return-void

    .line 56
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic X()Ljava/lang/reflect/Method;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->el:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method static synthetic Y()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ep:Ljava/lang/Class;

    return-object v0
.end method

.method static synthetic Z()Ljava/lang/reflect/Method;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->eq:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method static synthetic aa()Ljava/lang/reflect/Method;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->er:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method static synthetic ab()Ljava/lang/reflect/Method;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->es:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method static synthetic ac()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->et:Ljava/lang/Class;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/reflect/Method;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->em:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/reflect/Method;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->en:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method static synthetic ad()Ljava/lang/reflect/Method;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->eu:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method static synthetic ae()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ev:Ljava/lang/Class;

    return-object v0
.end method

.method static synthetic af()Ljava/lang/reflect/Method;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ew:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method static synthetic ag()Ljava/lang/reflect/Method;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ex:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method public static init()Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 400
    :try_start_0
    const-string v0, "com.google.android.gms.location.LocationListener"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ep:Ljava/lang/Class;

    .line 401
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ep:Ljava/lang/Class;

    const-string v3, "onLocationChanged"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Landroid/location/Location;

    aput-object v6, v4, v5

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->eq:Ljava/lang/reflect/Method;

    .line 402
    const-class v3, Ljava/lang/Object;

    const-string v4, "hashCode"

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Class;

    invoke-virtual {v3, v4, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->er:Ljava/lang/reflect/Method;

    .line 403
    const-class v0, Ljava/lang/Object;

    const-string v3, "equals"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Ljava/lang/Object;

    aput-object v6, v4, v5

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->es:Ljava/lang/reflect/Method;

    .line 405
    const-string v0, "com.google.android.gms.location.LocationClient$OnAddGeofencesResultListener"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->et:Ljava/lang/Class;

    .line 406
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->et:Ljava/lang/Class;

    const-string v3, "onAddGeofencesResult"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-class v6, [Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->eu:Ljava/lang/reflect/Method;

    .line 408
    const-string v0, "com.google.android.gms.location.LocationClient$OnRemoveGeofencesResultListener"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ev:Ljava/lang/Class;

    .line 409
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ev:Ljava/lang/Class;

    const-string v3, "onRemoveGeofencesByPendingIntentResult"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-class v6, Landroid/app/PendingIntent;

    aput-object v6, v4, v5

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ew:Ljava/lang/reflect/Method;

    .line 410
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ev:Ljava/lang/Class;

    const-string v3, "onRemoveGeofencesByRequestIdsResult"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-class v6, [Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ex:Ljava/lang/reflect/Method;

    .line 412
    const-string v0, "com.google.android.gms.location.LocationClient"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ed:Ljava/lang/Class;

    .line 413
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ed:Ljava/lang/Class;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Landroid/content/Context;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    sget-object v5, Lcom/glympse/android/hal/gms/gms6/common/GooglePlayServicesClient;->_ConnectionCallbacks:Ljava/lang/Class;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    sget-object v5, Lcom/glympse/android/hal/gms/gms6/common/GooglePlayServicesClient;->_OnConnectionFailedListener:Ljava/lang/Class;

    aput-object v5, v3, v4

    invoke-virtual {v0, v3}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ee:Ljava/lang/reflect/Constructor;

    .line 416
    sget-object v3, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ed:Ljava/lang/Class;

    const-string v4, "connect"

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Class;

    invoke-virtual {v3, v4, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->dX:Ljava/lang/reflect/Method;

    .line 417
    sget-object v3, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ed:Ljava/lang/Class;

    const-string v4, "disconnect"

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Class;

    invoke-virtual {v3, v4, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->dY:Ljava/lang/reflect/Method;

    .line 418
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ed:Ljava/lang/Class;

    const-string v3, "requestLocationUpdates"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Lcom/glympse/android/hal/gms/location/LocationRequest;->_LocationRequest:Ljava/lang/Class;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    sget-object v6, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ep:Ljava/lang/Class;

    aput-object v6, v4, v5

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ef:Ljava/lang/reflect/Method;

    .line 420
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ed:Ljava/lang/Class;

    const-string v3, "removeLocationUpdates"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ep:Ljava/lang/Class;

    aput-object v6, v4, v5

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->eg:Ljava/lang/reflect/Method;

    .line 422
    sget-object v3, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ed:Ljava/lang/Class;

    const-string v4, "getLastLocation"

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Class;

    invoke-virtual {v3, v4, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->eh:Ljava/lang/reflect/Method;

    .line 423
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ed:Ljava/lang/Class;

    const-string v3, "addGeofences"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Ljava/util/List;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-class v6, Landroid/app/PendingIntent;

    aput-object v6, v4, v5

    const/4 v5, 0x2

    sget-object v6, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->et:Ljava/lang/Class;

    aput-object v6, v4, v5

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ei:Ljava/lang/reflect/Method;

    .line 425
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ed:Ljava/lang/Class;

    const-string v3, "removeGeofences"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Ljava/util/List;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    sget-object v6, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ev:Ljava/lang/Class;

    aput-object v6, v4, v5

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ej:Ljava/lang/reflect/Method;

    .line 427
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ed:Ljava/lang/Class;

    const-string v3, "removeGeofences"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Landroid/app/PendingIntent;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    sget-object v6, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ev:Ljava/lang/Class;

    aput-object v6, v4, v5

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ek:Ljava/lang/reflect/Method;

    .line 429
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ed:Ljava/lang/Class;

    const-string v3, "hasError"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Landroid/content/Intent;

    aput-object v6, v4, v5

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->el:Ljava/lang/reflect/Method;

    .line 430
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ed:Ljava/lang/Class;

    const-string v3, "getGeofenceTransition"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Landroid/content/Intent;

    aput-object v6, v4, v5

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->em:Ljava/lang/reflect/Method;

    .line 431
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ed:Ljava/lang/Class;

    const-string v3, "getTriggeringGeofences"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Landroid/content/Intent;

    aput-object v6, v4, v5

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->en:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 436
    :goto_0
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ed:Ljava/lang/Class;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_1
    return v0

    :cond_0
    move v0, v2

    goto :goto_1

    .line 433
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static isGeofencingSupported(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 382
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ed:Ljava/lang/Class;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ei:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isLocationSupported(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 387
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ed:Ljava/lang/Class;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ef:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSupported(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 392
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ed:Ljava/lang/Class;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addGeofences(Ljava/util/List;Landroid/app/PendingIntent;Lcom/glympse/android/hal/gms/location/LocationClient$OnAddGeofencesResultListener;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/glympse/android/hal/gms/location/Geofence;",
            ">;",
            "Landroid/app/PendingIntent;",
            "Lcom/glympse/android/hal/gms/location/LocationClient$OnAddGeofencesResultListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 125
    :try_start_0
    new-instance v1, Ljava/util/Vector;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/Vector;-><init>(I)V

    .line 126
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/hal/gms/location/Geofence;

    .line 128
    invoke-virtual {v0}, Lcom/glympse/android/hal/gms/location/Geofence;->self()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0

    .line 134
    :catch_0
    move-exception v0

    .line 137
    :goto_1
    return-void

    .line 131
    :cond_0
    invoke-static {p3}, Lcom/glympse/android/hal/gms/gms6/location/LocationClient$OnAddGeofencesResultListenerProxy;->create(Lcom/glympse/android/hal/gms/location/LocationClient$OnAddGeofencesResultListener;)Ljava/lang/Object;

    move-result-object v0

    .line 132
    sget-object v2, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ei:Ljava/lang/reflect/Method;

    iget-object v3, p0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->eo:Ljava/lang/Object;

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    const/4 v1, 0x1

    aput-object p2, v4, v1

    const/4 v1, 0x2

    aput-object v0, v4, v1

    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1
.end method

.method public connect()V
    .locals 3

    .prologue
    .line 65
    :try_start_0
    sget-object v1, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->dX:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->eo:Ljava/lang/Object;

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    :goto_0
    return-void

    .line 67
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public disconnect()V
    .locals 3

    .prologue
    .line 76
    :try_start_0
    sget-object v1, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->dY:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->eo:Ljava/lang/Object;

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 81
    :goto_0
    return-void

    .line 78
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public getLastLocation()Landroid/location/Location;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 112
    :try_start_0
    sget-object v2, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->eh:Ljava/lang/reflect/Method;

    iget-object v3, p0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->eo:Ljava/lang/Object;

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 117
    :goto_0
    return-object v0

    .line 114
    :catch_0
    move-exception v0

    move-object v0, v1

    .line 117
    goto :goto_0
.end method

.method public removeGeofences(Landroid/app/PendingIntent;Lcom/glympse/android/hal/gms/location/LocationClient$OnRemoveGeofencesResultListener;)V
    .locals 5

    .prologue
    .line 155
    :try_start_0
    invoke-static {p2}, Lcom/glympse/android/hal/gms/gms6/location/LocationClient$OnRemoveGeofencesResultListenerProxy;->create(Lcom/glympse/android/hal/gms/location/LocationClient$OnRemoveGeofencesResultListener;)Ljava/lang/Object;

    move-result-object v0

    .line 156
    sget-object v1, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ek:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->eo:Ljava/lang/Object;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 161
    :goto_0
    return-void

    .line 158
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public removeGeofences(Ljava/util/List;Lcom/glympse/android/hal/gms/location/LocationClient$OnRemoveGeofencesResultListener;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/glympse/android/hal/gms/location/LocationClient$OnRemoveGeofencesResultListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 143
    :try_start_0
    invoke-static {p2}, Lcom/glympse/android/hal/gms/gms6/location/LocationClient$OnRemoveGeofencesResultListenerProxy;->create(Lcom/glympse/android/hal/gms/location/LocationClient$OnRemoveGeofencesResultListener;)Ljava/lang/Object;

    move-result-object v0

    .line 144
    sget-object v1, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ej:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->eo:Ljava/lang/Object;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 149
    :goto_0
    return-void

    .line 146
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public removeLocationUpdates(Lcom/glympse/android/hal/gms/location/LocationListener;)V
    .locals 5

    .prologue
    .line 100
    :try_start_0
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->eg:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->eo:Ljava/lang/Object;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 101
    invoke-static {p1}, Lcom/glympse/android/hal/gms/gms6/location/LocationClient$LocationListenerHandler;->create(Lcom/glympse/android/hal/gms/location/LocationListener;)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v3

    .line 100
    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 106
    :goto_0
    return-void

    .line 103
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public requestLocationUpdates(Lcom/glympse/android/hal/gms/location/LocationRequest;Lcom/glympse/android/hal/gms/location/LocationListener;)V
    .locals 5

    .prologue
    .line 87
    :try_start_0
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ef:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->eo:Ljava/lang/Object;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 88
    invoke-virtual {p1}, Lcom/glympse/android/hal/gms/location/LocationRequest;->getRequest()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 89
    invoke-static {p2}, Lcom/glympse/android/hal/gms/gms6/location/LocationClient$LocationListenerHandler;->create(Lcom/glympse/android/hal/gms/location/LocationListener;)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v3

    .line 87
    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    :goto_0
    return-void

    .line 91
    :catch_0
    move-exception v0

    goto :goto_0
.end method
