.class public Lcom/glympse/android/hal/gms/gms6/location/LocationClient$GeofencingEvent;
.super Ljava/lang/Object;
.source "LocationClient.java"

# interfaces
.implements Lcom/glympse/android/hal/gms/location/LocationClient$GeofencingEvent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/hal/gms/gms6/location/LocationClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GeofencingEvent"
.end annotation


# instance fields
.field private x:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 169
    iput-object p1, p0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient$GeofencingEvent;->x:Landroid/content/Intent;

    .line 170
    return-void
.end method


# virtual methods
.method public getGeofenceTransition()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 188
    :try_start_0
    # getter for: Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->em:Ljava/lang/reflect/Method;
    invoke-static {}, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->access$100()Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient$GeofencingEvent;->x:Landroid/content/Intent;

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 193
    :goto_0
    return v0

    .line 190
    :catch_0
    move-exception v0

    move v0, v1

    .line 193
    goto :goto_0
.end method

.method public getTriggeringGeofences()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/glympse/android/hal/gms/location/Geofence;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 201
    :try_start_0
    # getter for: Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->en:Ljava/lang/reflect/Method;
    invoke-static {}, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->access$200()Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient$GeofencingEvent;->x:Landroid/content/Intent;

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 202
    new-instance v1, Ljava/util/Vector;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/Vector;-><init>(I)V

    .line 203
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 205
    new-instance v4, Lcom/glympse/android/hal/gms/location/Geofence;

    invoke-direct {v4, v3}, Lcom/glympse/android/hal/gms/location/Geofence;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v4}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 209
    :catch_0
    move-exception v0

    move-object v0, v2

    .line 212
    :goto_1
    return-object v0

    :cond_0
    move-object v0, v1

    .line 207
    goto :goto_1
.end method

.method public hasError()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 176
    :try_start_0
    invoke-static {}, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->X()Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient$GeofencingEvent;->x:Landroid/content/Intent;

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 181
    :goto_0
    return v0

    .line 178
    :catch_0
    move-exception v0

    move v0, v1

    .line 181
    goto :goto_0
.end method
