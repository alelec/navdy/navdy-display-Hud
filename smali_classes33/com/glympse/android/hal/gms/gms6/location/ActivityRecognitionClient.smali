.class public Lcom/glympse/android/hal/gms/gms6/location/ActivityRecognitionClient;
.super Lcom/glympse/android/hal/gms/location/ActivityRecognitionClient;
.source "ActivityRecognitionClient.java"


# static fields
.field private static dV:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static dW:Ljava/lang/reflect/Constructor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/reflect/Constructor",
            "<*>;"
        }
    .end annotation
.end field

.field private static dX:Ljava/lang/reflect/Method;

.field private static dY:Ljava/lang/reflect/Method;

.field private static dZ:Ljava/lang/reflect/Method;

.field private static ea:Ljava/lang/reflect/Method;

.field private static eb:Ljava/lang/Object;


# instance fields
.field private ec:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 21
    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/ActivityRecognitionClient;->dV:Ljava/lang/Class;

    .line 22
    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/ActivityRecognitionClient;->dW:Ljava/lang/reflect/Constructor;

    .line 23
    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/ActivityRecognitionClient;->dX:Ljava/lang/reflect/Method;

    .line 24
    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/ActivityRecognitionClient;->dY:Ljava/lang/reflect/Method;

    .line 25
    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/ActivityRecognitionClient;->dZ:Ljava/lang/reflect/Method;

    .line 26
    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/ActivityRecognitionClient;->ea:Ljava/lang/reflect/Method;

    .line 27
    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/ActivityRecognitionClient;->eb:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V
    .locals 4

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/glympse/android/hal/gms/location/ActivityRecognitionClient;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/hal/gms/gms6/location/ActivityRecognitionClient;->ec:Ljava/lang/Object;

    .line 38
    :try_start_0
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/ActivityRecognitionClient;->dW:Ljava/lang/reflect/Constructor;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    .line 39
    invoke-static {p2}, Lcom/glympse/android/hal/gms/gms6/common/GooglePlayServicesClient$ConnectionCallbacksProxy;->create(Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    .line 40
    invoke-static {p3}, Lcom/glympse/android/hal/gms/gms6/common/GooglePlayServicesClient$OnConnectionFailedListenerProxy;->create(Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v1, v2

    .line 38
    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/hal/gms/gms6/location/ActivityRecognitionClient;->ec:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 45
    :goto_0
    return-void

    .line 42
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static init()Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 102
    :try_start_0
    invoke-static {}, Lcom/glympse/android/hal/gms/location/DetectedActivity;->init()V

    .line 103
    invoke-static {}, Lcom/glympse/android/hal/gms/location/ActivityRecognitionResult;->init()V

    .line 105
    const-string v0, "com.google.android.gms.location.ActivityRecognitionClient"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/ActivityRecognitionClient;->dV:Ljava/lang/Class;

    .line 106
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/ActivityRecognitionClient;->dV:Ljava/lang/Class;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Landroid/content/Context;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    sget-object v5, Lcom/glympse/android/hal/gms/gms6/common/GooglePlayServicesClient;->_ConnectionCallbacks:Ljava/lang/Class;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    sget-object v5, Lcom/glympse/android/hal/gms/gms6/common/GooglePlayServicesClient;->_OnConnectionFailedListener:Ljava/lang/Class;

    aput-object v5, v3, v4

    invoke-virtual {v0, v3}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/ActivityRecognitionClient;->dW:Ljava/lang/reflect/Constructor;

    .line 109
    sget-object v3, Lcom/glympse/android/hal/gms/gms6/location/ActivityRecognitionClient;->dV:Ljava/lang/Class;

    const-string v4, "connect"

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Class;

    invoke-virtual {v3, v4, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/ActivityRecognitionClient;->dX:Ljava/lang/reflect/Method;

    .line 110
    sget-object v3, Lcom/glympse/android/hal/gms/gms6/location/ActivityRecognitionClient;->dV:Ljava/lang/Class;

    const-string v4, "disconnect"

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Class;

    invoke-virtual {v3, v4, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/ActivityRecognitionClient;->dY:Ljava/lang/reflect/Method;

    .line 111
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/ActivityRecognitionClient;->dV:Ljava/lang/Class;

    const-string v3, "requestActivityUpdates"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-class v6, Landroid/app/PendingIntent;

    aput-object v6, v4, v5

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/ActivityRecognitionClient;->dZ:Ljava/lang/reflect/Method;

    .line 113
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/ActivityRecognitionClient;->dV:Ljava/lang/Class;

    const-string v3, "removeActivityUpdates"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Landroid/app/PendingIntent;

    aput-object v6, v4, v5

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/hal/gms/gms6/location/ActivityRecognitionClient;->ea:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 120
    :goto_0
    return v0

    .line 117
    :catch_0
    move-exception v0

    move v0, v2

    .line 120
    goto :goto_0
.end method

.method public static isSupported(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 94
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/ActivityRecognitionClient;->dV:Ljava/lang/Class;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public connect()V
    .locals 3

    .prologue
    .line 51
    :try_start_0
    sget-object v1, Lcom/glympse/android/hal/gms/gms6/location/ActivityRecognitionClient;->dX:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lcom/glympse/android/hal/gms/gms6/location/ActivityRecognitionClient;->ec:Ljava/lang/Object;

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 56
    :goto_0
    return-void

    .line 53
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public disconnect()V
    .locals 3

    .prologue
    .line 62
    :try_start_0
    sget-object v1, Lcom/glympse/android/hal/gms/gms6/location/ActivityRecognitionClient;->dY:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lcom/glympse/android/hal/gms/gms6/location/ActivityRecognitionClient;->ec:Ljava/lang/Object;

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    :goto_0
    return-void

    .line 64
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public removeActivityUpdates(Landroid/app/PendingIntent;)V
    .locals 4

    .prologue
    .line 85
    :try_start_0
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/ActivityRecognitionClient;->ea:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/glympse/android/hal/gms/gms6/location/ActivityRecognitionClient;->ec:Ljava/lang/Object;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    :goto_0
    return-void

    .line 87
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public requestActivityUpdates(JLandroid/app/PendingIntent;)V
    .locals 5

    .prologue
    .line 73
    :try_start_0
    sget-object v0, Lcom/glympse/android/hal/gms/gms6/location/ActivityRecognitionClient;->dZ:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/glympse/android/hal/gms/gms6/location/ActivityRecognitionClient;->ec:Ljava/lang/Object;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 74
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p3, v2, v3

    .line 73
    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    :goto_0
    return-void

    .line 76
    :catch_0
    move-exception v0

    goto :goto_0
.end method
