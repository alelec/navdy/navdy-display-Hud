.class public Lcom/glympse/android/hal/gms/gms6/location/LocationClient$OnAddGeofencesResultListenerProxy;
.super Ljava/lang/Object;
.source "LocationClient.java"

# interfaces
.implements Ljava/lang/reflect/InvocationHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/hal/gms/gms6/location/LocationClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "OnAddGeofencesResultListenerProxy"
.end annotation


# instance fields
.field private ez:Lcom/glympse/android/hal/gms/location/LocationClient$OnAddGeofencesResultListener;


# direct methods
.method private constructor <init>(Lcom/glympse/android/hal/gms/location/LocationClient$OnAddGeofencesResultListener;)V
    .locals 0

    .prologue
    .line 298
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 299
    iput-object p1, p0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient$OnAddGeofencesResultListenerProxy;->ez:Lcom/glympse/android/hal/gms/location/LocationClient$OnAddGeofencesResultListener;

    .line 300
    return-void
.end method

.method public static create(Lcom/glympse/android/hal/gms/location/LocationClient$OnAddGeofencesResultListener;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 292
    invoke-static {}, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ac()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    .line 293
    invoke-static {}, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ac()Ljava/lang/Class;

    move-result-object v3

    aput-object v3, v1, v2

    new-instance v2, Lcom/glympse/android/hal/gms/gms6/location/LocationClient$OnAddGeofencesResultListenerProxy;

    invoke-direct {v2, p0}, Lcom/glympse/android/hal/gms/gms6/location/LocationClient$OnAddGeofencesResultListenerProxy;-><init>(Lcom/glympse/android/hal/gms/location/LocationClient$OnAddGeofencesResultListener;)V

    .line 291
    invoke-static {v0, v1, v2}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 306
    :try_start_0
    invoke-static {}, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->ad()Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/reflect/Method;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 308
    const/4 v0, 0x0

    aget-object v0, p3, v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 309
    const/4 v1, 0x1

    if-ne v1, v0, :cond_0

    .line 311
    const/16 v0, 0xd

    .line 313
    :cond_0
    iget-object v1, p0, Lcom/glympse/android/hal/gms/gms6/location/LocationClient$OnAddGeofencesResultListenerProxy;->ez:Lcom/glympse/android/hal/gms/location/LocationClient$OnAddGeofencesResultListener;

    invoke-interface {v1, v0}, Lcom/glympse/android/hal/gms/location/LocationClient$OnAddGeofencesResultListener;->onAddGeofencesResult(I)V

    .line 323
    :goto_0
    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 317
    :cond_1
    invoke-static {}, Lcom/glympse/android/hal/gms/gms6/location/LocationClient;->Y()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p2, v0, p3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_1

    .line 320
    :catch_0
    move-exception v0

    goto :goto_0
.end method
