.class public Lcom/glympse/android/hal/HalFactory;
.super Ljava/lang/Object;
.source "HalFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createAccountImporter(Landroid/content/Context;Lcom/glympse/android/core/GHandler;Ljava/lang/String;)Lcom/glympse/android/lib/GAccountProvider;
    .locals 1

    .prologue
    .line 206
    new-instance v0, Lcom/glympse/android/hal/a;

    invoke-direct {v0, p0, p1, p2}, Lcom/glympse/android/hal/a;-><init>(Landroid/content/Context;Lcom/glympse/android/core/GHandler;Ljava/lang/String;)V

    return-object v0
.end method

.method public static createActivityProvider(Landroid/content/Context;)Lcom/glympse/android/hal/GActivityProvider;
    .locals 1

    .prologue
    .line 161
    new-instance v0, Lcom/glympse/android/hal/c;

    invoke-direct {v0, p0}, Lcom/glympse/android/hal/c;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public static createAndroidLocationProvider(Landroid/content/Context;)Lcom/glympse/android/core/GLocationProvider;
    .locals 1

    .prologue
    .line 132
    new-instance v0, Lcom/glympse/android/hal/r;

    invoke-direct {v0, p0}, Lcom/glympse/android/hal/r;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public static createBatteryProvider(Landroid/content/Context;)Lcom/glympse/android/hal/GBatteryProvider;
    .locals 1

    .prologue
    .line 166
    new-instance v0, Lcom/glympse/android/hal/e;

    invoke-direct {v0, p0}, Lcom/glympse/android/hal/e;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public static createCalendarProvider(Landroid/content/Context;)Lcom/glympse/android/hal/GCalendarProvider;
    .locals 1

    .prologue
    .line 191
    new-instance v0, Lcom/glympse/android/hal/f;

    invoke-direct {v0, p0}, Lcom/glympse/android/hal/f;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public static createClipboard(Landroid/content/Context;)Lcom/glympse/android/hal/GClipboard;
    .locals 1

    .prologue
    .line 62
    new-instance v0, Lcom/glympse/android/hal/Clipboard;

    invoke-direct {v0, p0}, Lcom/glympse/android/hal/Clipboard;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public static createContextHolder(Landroid/content/Context;)Lcom/glympse/android/hal/GContextHolder;
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/glympse/android/hal/h;

    invoke-direct {v0, p0}, Lcom/glympse/android/hal/h;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public static createDirectory(Landroid/content/Context;Ljava/lang/String;Z)Lcom/glympse/android/hal/GDirectory;
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x1

    invoke-static {p0, p1, p2, v0}, Lcom/glympse/android/hal/i;->b(Landroid/content/Context;Ljava/lang/String;ZZ)Lcom/glympse/android/hal/GDirectory;

    move-result-object v0

    return-object v0
.end method

.method public static createDrawable(Landroid/graphics/Bitmap;)Lcom/glympse/android/core/GDrawable;
    .locals 1

    .prologue
    .line 92
    new-instance v0, Lcom/glympse/android/hal/j;

    invoke-direct {v0, p0}, Lcom/glympse/android/hal/j;-><init>(Landroid/graphics/Bitmap;)V

    return-object v0
.end method

.method public static createDrawable(Landroid/graphics/drawable/BitmapDrawable;)Lcom/glympse/android/core/GDrawable;
    .locals 1

    .prologue
    .line 87
    new-instance v0, Lcom/glympse/android/hal/j;

    invoke-direct {v0, p0}, Lcom/glympse/android/hal/j;-><init>(Landroid/graphics/drawable/BitmapDrawable;)V

    return-object v0
.end method

.method public static createDrawable(Ljava/lang/String;I)Lcom/glympse/android/core/GDrawable;
    .locals 1

    .prologue
    .line 97
    invoke-static {p0, p1}, Lcom/glympse/android/hal/j;->a(Ljava/lang/String;I)Lcom/glympse/android/core/GDrawable;

    move-result-object v0

    return-object v0
.end method

.method public static createEventThread()Lcom/glympse/android/hal/GEventThread;
    .locals 1

    .prologue
    .line 52
    new-instance v0, Lcom/glympse/android/hal/EventThread;

    invoke-direct {v0}, Lcom/glympse/android/hal/EventThread;-><init>()V

    return-object v0
.end method

.method public static createFuseLocationProvider(Landroid/content/Context;)Lcom/glympse/android/core/GLocationProvider;
    .locals 1

    .prologue
    .line 137
    new-instance v0, Lcom/glympse/android/hal/s;

    invoke-direct {v0, p0}, Lcom/glympse/android/hal/s;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public static createHandler()Lcom/glympse/android/core/GHandler;
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lcom/glympse/android/hal/k;

    invoke-direct {v0}, Lcom/glympse/android/hal/k;-><init>()V

    return-object v0
.end method

.method public static createHttpConnection()Lcom/glympse/android/hal/GHttpConnection;
    .locals 1

    .prologue
    .line 102
    new-instance v0, Lcom/glympse/android/hal/l;

    invoke-direct {v0}, Lcom/glympse/android/hal/l;-><init>()V

    return-object v0
.end method

.method public static createIntent()Lcom/glympse/android/hal/GIntent;
    .locals 1

    .prologue
    .line 231
    new-instance v0, Lcom/glympse/android/hal/GlympseIntent;

    invoke-direct {v0}, Lcom/glympse/android/hal/GlympseIntent;-><init>()V

    return-object v0
.end method

.method public static createInvocationAgent(Landroid/content/Context;)Lcom/glympse/android/hal/GInvocationAgent;
    .locals 1

    .prologue
    .line 226
    new-instance v0, Lcom/glympse/android/hal/InvocationAgent;

    invoke-direct {v0, p0}, Lcom/glympse/android/hal/InvocationAgent;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public static createLocalContactsProvider(Landroid/content/Context;)Lcom/glympse/android/hal/GContactsProvider;
    .locals 1

    .prologue
    .line 186
    new-instance v0, Lcom/glympse/android/hal/q;

    invoke-direct {v0, p0}, Lcom/glympse/android/hal/q;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public static createLocationProfile(I)Lcom/glympse/android/lib/GLocationProfilePrivate;
    .locals 1

    .prologue
    .line 156
    invoke-static {p0}, Lcom/glympse/android/hal/r;->b(I)Lcom/glympse/android/core/GLocationProfile;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GLocationProfilePrivate;

    return-object v0
.end method

.method public static createLocationProvider(Landroid/content/Context;)Lcom/glympse/android/core/GLocationProvider;
    .locals 1

    .prologue
    .line 125
    invoke-static {p0}, Lcom/glympse/android/hal/s;->isSupported(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/glympse/android/hal/s;

    invoke-direct {v0, p0}, Lcom/glympse/android/hal/s;-><init>(Landroid/content/Context;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/glympse/android/hal/r;

    invoke-direct {v0, p0}, Lcom/glympse/android/hal/r;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public static createMutex()Lcom/glympse/android/hal/GMutex;
    .locals 1

    .prologue
    .line 35
    new-instance v0, Lcom/glympse/android/hal/m;

    invoke-direct {v0}, Lcom/glympse/android/hal/m;-><init>()V

    return-object v0
.end method

.method public static createPhoneFavoriteProvider(Landroid/content/Context;)Lcom/glympse/android/hal/GPhoneFavoriteProvider;
    .locals 1

    .prologue
    .line 196
    new-instance v0, Lcom/glympse/android/hal/PhoneFavoriteProvider;

    invoke-direct {v0, p0}, Lcom/glympse/android/hal/PhoneFavoriteProvider;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public static createPhoneHistoryProvider(Landroid/content/Context;)Lcom/glympse/android/hal/GPhoneHistoryProvider;
    .locals 1

    .prologue
    .line 201
    new-instance v0, Lcom/glympse/android/hal/PhoneHistoryProvider;

    invoke-direct {v0, p0}, Lcom/glympse/android/hal/PhoneHistoryProvider;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public static createProximityProvider(Landroid/content/Context;)Lcom/glympse/android/core/GProximityProvider;
    .locals 1

    .prologue
    .line 142
    invoke-static {p0}, Lcom/glympse/android/hal/v;->isSupported(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/glympse/android/hal/v;

    invoke-direct {v0, p0}, Lcom/glympse/android/hal/v;-><init>(Landroid/content/Context;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/glympse/android/hal/u;

    invoke-direct {v0, p0}, Lcom/glympse/android/hal/u;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public static createSemaphore()Lcom/glympse/android/hal/GSemaphore;
    .locals 1

    .prologue
    .line 40
    new-instance v0, Lcom/glympse/android/hal/n;

    invoke-direct {v0}, Lcom/glympse/android/hal/n;-><init>()V

    return-object v0
.end method

.method public static createServiceWrapper()Lcom/glympse/android/hal/GServiceWrapper;
    .locals 1

    .prologue
    .line 181
    new-instance v0, Lcom/glympse/android/hal/y;

    invoke-direct {v0}, Lcom/glympse/android/hal/y;-><init>()V

    return-object v0
.end method

.method public static createSmsProvider(Landroid/content/Context;)Lcom/glympse/android/hal/GSmsProvider;
    .locals 1

    .prologue
    .line 171
    new-instance v0, Lcom/glympse/android/hal/aa;

    invoke-direct {v0, p0}, Lcom/glympse/android/hal/aa;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public static createSmsReceiver(Landroid/content/Context;)Lcom/glympse/android/hal/GSmsReceiver;
    .locals 1

    .prologue
    .line 176
    new-instance v0, Lcom/glympse/android/hal/ab;

    invoke-direct {v0, p0}, Lcom/glympse/android/hal/ab;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public static createSocket()Lcom/glympse/android/hal/GSocket;
    .locals 1

    .prologue
    .line 211
    new-instance v0, Lcom/glympse/android/hal/o;

    invoke-direct {v0}, Lcom/glympse/android/hal/o;-><init>()V

    return-object v0
.end method

.method public static createThread(Ljava/lang/Runnable;)Ljava/lang/Thread;
    .locals 2

    .prologue
    .line 45
    new-instance v0, Ljava/lang/Thread;

    const-string v1, "glympse.worker"

    invoke-direct {v0, p0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 46
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 47
    return-object v0
.end method

.method public static createTimer(Ljava/lang/Runnable;JLcom/glympse/android/core/GHandler;)Lcom/glympse/android/hal/GTimer;
    .locals 1

    .prologue
    .line 57
    new-instance v0, Lcom/glympse/android/hal/ac;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/glympse/android/hal/ac;-><init>(Ljava/lang/Runnable;JLcom/glympse/android/core/GHandler;)V

    return-object v0
.end method

.method public static createUserProfile(Landroid/content/Context;)Lcom/glympse/android/hal/GUserProfile;
    .locals 1

    .prologue
    .line 221
    new-instance v0, Lcom/glympse/android/hal/ad;

    invoke-direct {v0, p0}, Lcom/glympse/android/hal/ad;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public static createWifiProvider(Landroid/content/Context;Lcom/glympse/android/core/GHandler;)Lcom/glympse/android/hal/GWifiProvider;
    .locals 1

    .prologue
    .line 216
    new-instance v0, Lcom/glympse/android/hal/af;

    invoke-direct {v0, p0}, Lcom/glympse/android/hal/af;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public static isFuseProviderSupported(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 107
    invoke-static {p0}, Lcom/glympse/android/hal/s;->isSupported(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public static isProximityReliable(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 151
    invoke-static {p0}, Lcom/glympse/android/hal/v;->isSupported(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public static openDirectory(Landroid/content/Context;Ljava/lang/String;Z)Lcom/glympse/android/hal/GDirectory;
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/glympse/android/hal/i;->a(Landroid/content/Context;Ljava/lang/String;ZZ)Lcom/glympse/android/hal/GDirectory;

    move-result-object v0

    return-object v0
.end method

.method public static openKeychain(Landroid/content/Context;Ljava/lang/String;)Lcom/glympse/android/hal/GKeychain;
    .locals 1

    .prologue
    .line 67
    new-instance v0, Lcom/glympse/android/hal/p;

    invoke-direct {v0, p0}, Lcom/glympse/android/hal/p;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public static openSharedPreferences(Landroid/content/Context;Ljava/lang/String;)Lcom/glympse/android/hal/GSharedPreferences;
    .locals 1

    .prologue
    .line 72
    new-instance v0, Lcom/glympse/android/hal/z;

    invoke-direct {v0, p0}, Lcom/glympse/android/hal/z;-><init>(Landroid/content/Context;)V

    return-object v0
.end method
