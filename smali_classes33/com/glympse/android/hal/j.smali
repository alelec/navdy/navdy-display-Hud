.class Lcom/glympse/android/hal/j;
.super Ljava/lang/Object;
.source "GlympseDrawable.java"

# interfaces
.implements Lcom/glympse/android/hal/GDrawablePrivate;
.implements Lcom/glympse/android/ui/GDrawableExt;


# instance fields
.field private ba:[B

.field private bb:Landroid/graphics/Bitmap;

.field private bc:Z

.field private bd:I

.field private be:I

.field private bf:I

.field private bg:I

.field private bh:I

.field private bi:I


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/hal/j;->ba:[B

    .line 34
    iput-boolean v1, p0, Lcom/glympse/android/hal/j;->bc:Z

    .line 35
    iput v1, p0, Lcom/glympse/android/hal/j;->bd:I

    .line 36
    iput v1, p0, Lcom/glympse/android/hal/j;->be:I

    .line 37
    iput v1, p0, Lcom/glympse/android/hal/j;->bf:I

    .line 39
    iput v1, p0, Lcom/glympse/android/hal/j;->bg:I

    .line 40
    const/16 v0, 0x64

    iput v0, p0, Lcom/glympse/android/hal/j;->bh:I

    .line 42
    iput v1, p0, Lcom/glympse/android/hal/j;->bi:I

    .line 51
    iput-object p1, p0, Lcom/glympse/android/hal/j;->bb:Landroid/graphics/Bitmap;

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/graphics/drawable/BitmapDrawable;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/hal/j;->ba:[B

    .line 34
    iput-boolean v1, p0, Lcom/glympse/android/hal/j;->bc:Z

    .line 35
    iput v1, p0, Lcom/glympse/android/hal/j;->bd:I

    .line 36
    iput v1, p0, Lcom/glympse/android/hal/j;->be:I

    .line 37
    iput v1, p0, Lcom/glympse/android/hal/j;->bf:I

    .line 39
    iput v1, p0, Lcom/glympse/android/hal/j;->bg:I

    .line 40
    const/16 v0, 0x64

    iput v0, p0, Lcom/glympse/android/hal/j;->bh:I

    .line 42
    iput v1, p0, Lcom/glympse/android/hal/j;->bi:I

    .line 46
    invoke-virtual {p1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/hal/j;->bb:Landroid/graphics/Bitmap;

    .line 47
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/hal/j;->ba:[B

    .line 34
    iput-boolean v1, p0, Lcom/glympse/android/hal/j;->bc:Z

    .line 35
    iput v1, p0, Lcom/glympse/android/hal/j;->bd:I

    .line 36
    iput v1, p0, Lcom/glympse/android/hal/j;->be:I

    .line 37
    iput v1, p0, Lcom/glympse/android/hal/j;->bf:I

    .line 39
    iput v1, p0, Lcom/glympse/android/hal/j;->bg:I

    .line 40
    const/16 v0, 0x64

    iput v0, p0, Lcom/glympse/android/hal/j;->bh:I

    .line 42
    iput v1, p0, Lcom/glympse/android/hal/j;->bi:I

    .line 56
    iput p2, p0, Lcom/glympse/android/hal/j;->bi:I

    .line 57
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/glympse/android/hal/j;->a(Ljava/lang/String;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/hal/j;->bb:Landroid/graphics/Bitmap;

    .line 58
    return-void
.end method

.method private static a(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 504
    if-nez p1, :cond_0

    .line 512
    :goto_0
    return-object p0

    .line 510
    :cond_0
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 511
    int-to-float v0, p1

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 512
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p0

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object p0

    goto :goto_0
.end method

.method private static a(Landroid/graphics/Bitmap;III)Landroid/graphics/Bitmap;
    .locals 11

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x3

    const/4 v10, 0x1

    .line 408
    if-eqz p0, :cond_0

    if-lez p1, :cond_0

    if-gtz p2, :cond_2

    :cond_0
    move-object p0, v0

    .line 498
    :cond_1
    :goto_0
    return-object p0

    .line 413
    :cond_2
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 414
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 419
    if-eqz p3, :cond_3

    if-ne v2, p1, :cond_5

    if-ne v1, p2, :cond_5

    .line 493
    :cond_3
    :goto_1
    if-ne v2, p1, :cond_4

    if-eq v1, p2, :cond_1

    .line 495
    :cond_4
    invoke-static {p0, p1, p2, v10}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object p0

    goto :goto_0

    .line 427
    :cond_5
    if-ne v10, p3, :cond_6

    if-gt v2, p1, :cond_6

    if-gt v1, p2, :cond_6

    move p2, v1

    move p1, v2

    .line 431
    goto :goto_1

    .line 437
    :cond_6
    if-ne v4, p3, :cond_8

    if-le v2, p1, :cond_7

    if-gt v1, p2, :cond_8

    :cond_7
    move p2, v1

    move p1, v2

    .line 441
    goto :goto_1

    .line 448
    :cond_8
    const/4 v3, 0x2

    if-eq v3, p3, :cond_9

    if-ne v10, p3, :cond_b

    .line 450
    :cond_9
    int-to-double v4, p1

    int-to-double v6, v2

    div-double/2addr v4, v6

    .line 451
    int-to-double v6, p2

    int-to-double v8, v1

    div-double/2addr v6, v8

    .line 455
    cmpg-double v0, v4, v6

    if-gtz v0, :cond_a

    .line 457
    int-to-double v6, v1

    mul-double/2addr v4, v6

    double-to-int p2, v4

    goto :goto_1

    .line 461
    :cond_a
    int-to-double v4, v2

    mul-double/2addr v4, v6

    double-to-int p1, v4

    goto :goto_1

    .line 469
    :cond_b
    const/4 v3, 0x4

    if-eq v3, p3, :cond_c

    if-ne v4, p3, :cond_e

    .line 471
    :cond_c
    int-to-double v4, p1

    int-to-double v6, v2

    div-double/2addr v4, v6

    .line 472
    int-to-double v6, p2

    int-to-double v8, v1

    div-double/2addr v6, v8

    .line 476
    cmpl-double v0, v4, v6

    if-ltz v0, :cond_d

    .line 478
    int-to-double v6, v1

    mul-double/2addr v4, v6

    double-to-int p2, v4

    goto :goto_1

    .line 482
    :cond_d
    int-to-double v4, v2

    mul-double/2addr v4, v6

    double-to-int p1, v4

    goto :goto_1

    :cond_e
    move-object p0, v0

    .line 489
    goto :goto_0
.end method

.method private a(Ljava/lang/String;Z)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 172
    invoke-static {p1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 174
    const/4 v0, 0x0

    .line 178
    :goto_0
    return-object v0

    .line 177
    :cond_0
    const-string v0, "://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    .line 178
    if-eqz v0, :cond_1

    .line 179
    invoke-direct {p0, p1, p2}, Lcom/glympse/android/hal/j;->b(Ljava/lang/String;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 180
    :cond_1
    invoke-static {p1, p2}, Lcom/glympse/android/hal/j;->c(Ljava/lang/String;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method private static a([BZ)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 357
    :try_start_0
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 358
    if-eqz p1, :cond_0

    .line 360
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 361
    const/4 v1, 0x0

    array-length v2, p0

    invoke-static {p0, v1, v2, v0}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 362
    invoke-static {v0}, Lcom/glympse/android/hal/j;->a(Landroid/graphics/BitmapFactory$Options;)V

    .line 366
    :cond_0
    const/4 v1, 0x0

    array-length v2, p0

    invoke-static {p0, v1, v2, v0}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 371
    :goto_0
    return-object v0

    .line 368
    :catch_0
    move-exception v0

    .line 370
    invoke-static {v0, v3}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    .line 371
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;I)Lcom/glympse/android/core/GDrawable;
    .locals 2

    .prologue
    .line 62
    new-instance v0, Lcom/glympse/android/hal/j;

    invoke-direct {v0, p0, p1}, Lcom/glympse/android/hal/j;-><init>(Ljava/lang/String;I)V

    .line 63
    invoke-virtual {v0}, Lcom/glympse/android/hal/j;->getImage()Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method private static a(Landroid/graphics/BitmapFactory$Options;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 379
    iget v0, p0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v1, p0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 380
    int-to-double v0, v0

    const-wide/high16 v2, 0x4074000000000000L    # 320.0

    div-double/2addr v0, v2

    .line 383
    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v1, v0

    .line 384
    const/high16 v0, 0x40000000    # 2.0f

    .line 385
    :goto_0
    if-le v0, v5, :cond_0

    and-int v2, v1, v0

    if-nez v2, :cond_0

    .line 387
    ushr-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 391
    :cond_0
    iput-boolean v4, p0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 392
    iput v0, p0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 393
    iput v4, p0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 394
    iput v4, p0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 397
    :try_start_0
    const-class v0, Landroid/graphics/BitmapFactory$Options;

    const-string v1, "inPurgeable"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 398
    :goto_1
    return-void

    .line 397
    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method private static a(Landroid/graphics/Bitmap;IIIIII)[B
    .locals 1

    .prologue
    .line 402
    invoke-static {p0, p1, p2, p3}, Lcom/glympse/android/hal/j;->a(Landroid/graphics/Bitmap;III)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 403
    invoke-static {v0, p5, p4, p6}, Lcom/glympse/android/hal/j;->b(Landroid/graphics/Bitmap;III)[B

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/String;Z)Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 188
    :try_start_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 189
    if-nez v5, :cond_1

    .line 325
    :cond_0
    :goto_0
    return-object v0

    .line 195
    :cond_1
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/glympse/android/hal/h;->a(Landroid/content/Context;)Landroid/content/Context;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v6

    .line 196
    if-eqz v6, :cond_0

    .line 235
    :try_start_1
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v5, v2}, Landroid/provider/MediaStore$Images$Media;->query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 236
    if-eqz v3, :cond_8

    .line 239
    :try_start_2
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 242
    const-string v1, "_data"

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 243
    if-ltz v1, :cond_7

    .line 246
    invoke-interface {v3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 247
    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 250
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->canRead()Z
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v1

    if-eqz v1, :cond_7

    .line 259
    :goto_1
    :try_start_3
    const-string v1, "orientation"

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 260
    if-ltz v1, :cond_6

    .line 262
    invoke-interface {v3, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v1

    .line 274
    :goto_2
    if-eqz v3, :cond_2

    .line 276
    :try_start_4
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 282
    :cond_2
    :goto_3
    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 285
    iput v1, p0, Lcom/glympse/android/hal/j;->bi:I

    .line 286
    const/4 v1, 0x1

    invoke-static {v2, v1}, Lcom/glympse/android/hal/j;->c(Ljava/lang/String;Z)Landroid/graphics/Bitmap;
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1

    move-result-object v0

    goto :goto_0

    .line 267
    :catch_0
    move-exception v1

    move-object v2, v0

    move-object v3, v0

    .line 269
    :goto_4
    const/4 v7, 0x0

    :try_start_5
    invoke-static {v1, v7}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 274
    if-eqz v3, :cond_5

    .line 276
    :try_start_6
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    move v1, v4

    .line 277
    goto :goto_3

    .line 274
    :catchall_0
    move-exception v1

    move-object v3, v0

    :goto_5
    if-eqz v3, :cond_3

    .line 276
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 277
    :cond_3
    throw v1
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1

    .line 322
    :catch_1
    move-exception v1

    .line 324
    invoke-static {v1, v4}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    goto :goto_0

    .line 308
    :cond_4
    :try_start_7
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 309
    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 310
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v2

    .line 311
    const/4 v3, 0x0

    invoke-static {v2, v3, v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 312
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 315
    invoke-static {v1}, Lcom/glympse/android/hal/j;->a(Landroid/graphics/BitmapFactory$Options;)V

    .line 316
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v2

    .line 317
    const/4 v3, 0x0

    invoke-static {v2, v3, v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 318
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1

    move-object v0, v1

    .line 320
    goto/16 :goto_0

    .line 274
    :catchall_1
    move-exception v1

    goto :goto_5

    .line 267
    :catch_2
    move-exception v1

    move-object v2, v0

    goto :goto_4

    :catch_3
    move-exception v1

    goto :goto_4

    :cond_5
    move v1, v4

    goto :goto_3

    :cond_6
    move v1, v4

    goto :goto_2

    :cond_7
    move-object v2, v0

    goto :goto_1

    :cond_8
    move v1, v4

    move-object v2, v0

    goto :goto_2
.end method

.method private static b(Landroid/graphics/Bitmap;III)[B
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 517
    .line 519
    if-nez p0, :cond_1

    .line 557
    :cond_0
    :goto_0
    return-object v0

    .line 524
    :cond_1
    invoke-static {p0, p2}, Lcom/glympse/android/hal/j;->a(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 527
    if-nez p1, :cond_2

    .line 529
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    .line 543
    :goto_1
    :try_start_0
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 544
    invoke-virtual {v2, v1, p3, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 547
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 550
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 552
    :catch_0
    move-exception v1

    .line 554
    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    goto :goto_0

    .line 531
    :cond_2
    const/4 v1, 0x1

    if-ne v1, p1, :cond_0

    .line 533
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    goto :goto_1
.end method

.method private static c(Ljava/lang/String;Z)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 334
    :try_start_0
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 335
    if-eqz p1, :cond_0

    .line 337
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 338
    invoke-static {p0, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 339
    invoke-static {v0}, Lcom/glympse/android/hal/j;->a(Landroid/graphics/BitmapFactory$Options;)V

    .line 343
    :cond_0
    invoke-static {p0, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 348
    :goto_0
    return-object v0

    .line 345
    :catch_0
    move-exception v0

    .line 347
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    .line 348
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public clearBuffer()V
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/hal/j;->ba:[B

    .line 111
    return-void
.end method

.method public compress()Z
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 126
    iget-object v0, p0, Lcom/glympse/android/hal/j;->bb:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    move v0, v7

    .line 146
    :goto_0
    return v0

    .line 132
    :cond_0
    invoke-virtual {p0}, Lcom/glympse/android/hal/j;->clearBuffer()V

    .line 136
    :try_start_0
    iget-boolean v0, p0, Lcom/glympse/android/hal/j;->bc:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/glympse/android/hal/j;->bb:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/glympse/android/hal/j;->be:I

    iget v2, p0, Lcom/glympse/android/hal/j;->bf:I

    iget v3, p0, Lcom/glympse/android/hal/j;->bd:I

    iget v4, p0, Lcom/glympse/android/hal/j;->bi:I

    iget v5, p0, Lcom/glympse/android/hal/j;->bg:I

    iget v6, p0, Lcom/glympse/android/hal/j;->bh:I

    .line 137
    invoke-static/range {v0 .. v6}, Lcom/glympse/android/hal/j;->a(Landroid/graphics/Bitmap;IIIIII)[B

    move-result-object v0

    .line 138
    :goto_1
    iput-object v0, p0, Lcom/glympse/android/hal/j;->ba:[B

    .line 146
    const/4 v0, 0x1

    goto :goto_0

    .line 137
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/hal/j;->bb:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/glympse/android/hal/j;->bg:I

    iget v2, p0, Lcom/glympse/android/hal/j;->bi:I

    iget v3, p0, Lcom/glympse/android/hal/j;->bh:I

    .line 138
    invoke-static {v0, v1, v2, v3}, Lcom/glympse/android/hal/j;->b(Landroid/graphics/Bitmap;III)[B
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_1

    .line 140
    :catch_0
    move-exception v0

    .line 142
    invoke-static {v0, v7}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    move v0, v7

    .line 143
    goto :goto_0
.end method

.method public decompress(Z)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 151
    iget-object v1, p0, Lcom/glympse/android/hal/j;->ba:[B

    if-nez v1, :cond_1

    .line 163
    :cond_0
    :goto_0
    return v0

    .line 156
    :cond_1
    iget-object v1, p0, Lcom/glympse/android/hal/j;->ba:[B

    iget-boolean v2, p0, Lcom/glympse/android/hal/j;->bc:Z

    invoke-static {v1, v2}, Lcom/glympse/android/hal/j;->a([BZ)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/glympse/android/hal/j;->bb:Landroid/graphics/Bitmap;

    .line 158
    if-eqz p1, :cond_2

    .line 160
    invoke-virtual {p0}, Lcom/glympse/android/hal/j;->clearBuffer()V

    .line 163
    :cond_2
    iget-object v1, p0, Lcom/glympse/android/hal/j;->bb:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getBuffer()[B
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/glympse/android/hal/j;->ba:[B

    return-object v0
.end method

.method public getImage()Landroid/graphics/drawable/BitmapDrawable;
    .locals 3

    .prologue
    .line 72
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-static {}, Lcom/glympse/android/hal/h;->m()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/glympse/android/hal/j;->bb:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    return-object v0
.end method

.method public getLength()I
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/glympse/android/hal/j;->ba:[B

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/glympse/android/hal/j;->ba:[B

    array-length v0, v0

    goto :goto_0
.end method

.method public setBuffer([BIZ)V
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lcom/glympse/android/hal/j;->ba:[B

    .line 106
    return-void
.end method

.method public setCompression(II)V
    .locals 0

    .prologue
    .line 93
    iput p1, p0, Lcom/glympse/android/hal/j;->bg:I

    .line 94
    iput p2, p0, Lcom/glympse/android/hal/j;->bh:I

    .line 95
    return-void
.end method

.method public setRotation(I)V
    .locals 0

    .prologue
    .line 99
    iput p1, p0, Lcom/glympse/android/hal/j;->bi:I

    .line 100
    return-void
.end method

.method public setScale(ZIII)V
    .locals 0

    .prologue
    .line 85
    iput-boolean p1, p0, Lcom/glympse/android/hal/j;->bc:Z

    .line 86
    iput p2, p0, Lcom/glympse/android/hal/j;->bd:I

    .line 87
    iput p3, p0, Lcom/glympse/android/hal/j;->be:I

    .line 88
    iput p4, p0, Lcom/glympse/android/hal/j;->bf:I

    .line 89
    return-void
.end method
