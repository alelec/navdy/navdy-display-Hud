.class Lcom/glympse/android/hal/s$a;
.super Ljava/lang/Object;
.source "LocationProviderFuse.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/hal/s;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field private cC:Lcom/glympse/android/core/GLocationProvider;

.field final synthetic cH:Lcom/glympse/android/hal/s;


# direct methods
.method public constructor <init>(Lcom/glympse/android/hal/s;Lcom/glympse/android/core/GLocationProvider;)V
    .locals 0

    .prologue
    .line 250
    iput-object p1, p0, Lcom/glympse/android/hal/s$a;->cH:Lcom/glympse/android/hal/s;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 251
    iput-object p2, p0, Lcom/glympse/android/hal/s$a;->cC:Lcom/glympse/android/core/GLocationProvider;

    .line 252
    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 256
    const/4 v0, 0x1

    const-string v1, "[PermissionTimer] Permission timer fired"

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 259
    iget-object v0, p0, Lcom/glympse/android/hal/s$a;->cH:Lcom/glympse/android/hal/s;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/glympse/android/hal/s;->a(Lcom/glympse/android/hal/s;Lcom/glympse/android/hal/s$a;)Lcom/glympse/android/hal/s$a;

    .line 261
    iget-object v0, p0, Lcom/glympse/android/hal/s$a;->cH:Lcom/glympse/android/hal/s;

    invoke-static {v0}, Lcom/glympse/android/hal/s;->a(Lcom/glympse/android/hal/s;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/glympse/android/hal/r;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 264
    iget-object v0, p0, Lcom/glympse/android/hal/s$a;->cH:Lcom/glympse/android/hal/s;

    iget-object v1, p0, Lcom/glympse/android/hal/s$a;->cC:Lcom/glympse/android/core/GLocationProvider;

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/s;->a(Lcom/glympse/android/core/GLocationProvider;)V

    .line 279
    :cond_0
    :goto_0
    return-void

    .line 268
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/hal/s$a;->cH:Lcom/glympse/android/hal/s;

    invoke-static {v0}, Lcom/glympse/android/hal/s;->b(Lcom/glympse/android/hal/s;)Lcom/glympse/android/hal/gms/location/LocationClient;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 271
    iget-object v0, p0, Lcom/glympse/android/hal/s$a;->cH:Lcom/glympse/android/hal/s;

    invoke-static {v0}, Lcom/glympse/android/hal/s;->b(Lcom/glympse/android/hal/s;)Lcom/glympse/android/hal/gms/location/LocationClient;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/hal/s$a;->cH:Lcom/glympse/android/hal/s;

    invoke-static {v1}, Lcom/glympse/android/hal/s;->c(Lcom/glympse/android/hal/s;)Lcom/glympse/android/hal/gms/location/LocationRequest;

    move-result-object v1

    iget-object v2, p0, Lcom/glympse/android/hal/s$a;->cH:Lcom/glympse/android/hal/s;

    invoke-virtual {v0, v1, v2}, Lcom/glympse/android/hal/gms/location/LocationClient;->requestLocationUpdates(Lcom/glympse/android/hal/gms/location/LocationRequest;Lcom/glympse/android/hal/gms/location/LocationListener;)V

    .line 274
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/hal/s$a;->cH:Lcom/glympse/android/hal/s;

    invoke-static {v0}, Lcom/glympse/android/hal/s;->d(Lcom/glympse/android/hal/s;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 276
    iget-object v0, p0, Lcom/glympse/android/hal/s$a;->cH:Lcom/glympse/android/hal/s;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/s;->c(I)V

    goto :goto_0
.end method
