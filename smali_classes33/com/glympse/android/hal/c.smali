.class Lcom/glympse/android/hal/c;
.super Ljava/lang/Object;
.source "ActivityProvider.java"

# interfaces
.implements Lcom/glympse/android/hal/GActivityProvider;
.implements Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
.implements Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/hal/c$a;,
        Lcom/glympse/android/hal/c$b;
    }
.end annotation


# static fields
.field private static final A:Ljava/lang/String; = "com.glympse.android.hal.activity.CHANGED"


# instance fields
.field private e:Landroid/content/Context;

.field private t:Z

.field private u:Lcom/glympse/android/hal/gms/location/ActivityRecognitionClient;

.field private v:Z

.field private w:Ljava/lang/String;

.field private x:Landroid/content/Intent;

.field private y:Lcom/glympse/android/hal/c$a;

.field private z:Lcom/glympse/android/hal/GHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GHashtable",
            "<",
            "Lcom/glympse/android/hal/GActivityListener;",
            "Lcom/glympse/android/hal/c$b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    iput-object p1, p0, Lcom/glympse/android/hal/c;->e:Landroid/content/Context;

    .line 95
    invoke-static {p1}, Lcom/glympse/android/hal/c;->isSupported(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/glympse/android/hal/c;->t:Z

    .line 96
    iget-boolean v0, p0, Lcom/glympse/android/hal/c;->t:Z

    if-eqz v0, :cond_0

    .line 98
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/glympse/android/hal/c;->v:Z

    .line 99
    invoke-static {p1, p0, p0}, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->createActivityRecognitionClient(Landroid/content/Context;Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)Lcom/glympse/android/hal/gms/location/ActivityRecognitionClient;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/hal/c;->u:Lcom/glympse/android/hal/gms/location/ActivityRecognitionClient;

    .line 100
    new-instance v0, Lcom/glympse/android/hal/GHashtable;

    invoke-direct {v0}, Lcom/glympse/android/hal/GHashtable;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/hal/c;->z:Lcom/glympse/android/hal/GHashtable;

    .line 103
    invoke-direct {p0}, Lcom/glympse/android/hal/c;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/hal/c;->w:Ljava/lang/String;

    .line 104
    invoke-direct {p0}, Lcom/glympse/android/hal/c;->createIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/hal/c;->x:Landroid/content/Intent;

    .line 106
    :cond_0
    return-void
.end method

.method private a(I)I
    .locals 1

    .prologue
    .line 179
    packed-switch p1, :pswitch_data_0

    .line 192
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 182
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 184
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 186
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 188
    :pswitch_4
    const/4 v0, 0x4

    goto :goto_0

    .line 190
    :pswitch_5
    const/4 v0, 0x5

    goto :goto_0

    .line 179
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method static synthetic a(Lcom/glympse/android/hal/c;I)I
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/glympse/android/hal/c;->a(I)I

    move-result v0

    return v0
.end method

.method private a(J)Landroid/app/PendingIntent;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 231
    iget-object v0, p0, Lcom/glympse/android/hal/c;->e:Landroid/content/Context;

    iget-object v1, p0, Lcom/glympse/android/hal/c;->x:Landroid/content/Intent;

    invoke-static {v0, v2, v1, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 232
    iget-object v1, p0, Lcom/glympse/android/hal/c;->u:Lcom/glympse/android/hal/gms/location/ActivityRecognitionClient;

    invoke-virtual {v1, p1, p2, v0}, Lcom/glympse/android/hal/gms/location/ActivityRecognitionClient;->requestActivityUpdates(JLandroid/app/PendingIntent;)V

    .line 233
    return-object v0
.end method

.method static synthetic a(Lcom/glympse/android/hal/c;)Lcom/glympse/android/hal/GHashtable;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/glympse/android/hal/c;->z:Lcom/glympse/android/hal/GHashtable;

    return-object v0
.end method

.method private createIntent()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 243
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/glympse/android/hal/c;->w:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 244
    iget-object v1, p0, Lcom/glympse/android/hal/c;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 245
    return-object v0
.end method

.method private d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 238
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "com.glympse.android.hal.activity.CHANGED_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/hal/c;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static isSupported(Landroid/content/Context;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 58
    .line 61
    :try_start_0
    invoke-static {p0}, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->isSupported(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 88
    :cond_0
    :goto_0
    return v0

    .line 67
    :cond_1
    const v1, 0x3d0900

    invoke-static {p0, v1}, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->isVersionSupported(Landroid/content/Context;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 73
    invoke-static {p0}, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->isActivityRecognitionSupported(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 79
    sget-object v1, Lcom/glympse/android/hal/gms/common/Permission;->ACTIVITY_RECOGNITION:Ljava/lang/String;

    invoke-static {p0, v1}, Lcom/glympse/android/hal/h;->b(Landroid/content/Context;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 84
    const/4 v0, 0x1

    goto :goto_0

    .line 86
    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public isSupported()Z
    .locals 1

    .prologue
    .line 114
    iget-boolean v0, p0, Lcom/glympse/android/hal/c;->t:Z

    return v0
.end method

.method public onConnected(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 256
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/glympse/android/hal/c;->v:Z

    .line 259
    iget-object v0, p0, Lcom/glympse/android/hal/c;->z:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GHashtable;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 261
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/hal/c$b;

    .line 264
    iget-object v2, v0, Lcom/glympse/android/hal/c$b;->D:Landroid/app/PendingIntent;

    if-nez v2, :cond_0

    .line 266
    iget-wide v2, v0, Lcom/glympse/android/hal/c$b;->C:J

    invoke-direct {p0, v2, v3}, Lcom/glympse/android/hal/c;->a(J)Landroid/app/PendingIntent;

    move-result-object v2

    iput-object v2, v0, Lcom/glympse/android/hal/c$b;->D:Landroid/app/PendingIntent;

    goto :goto_0

    .line 269
    :cond_1
    return-void
.end method

.method public onConnectionFailed(Lcom/glympse/android/hal/gms/common/ConnectionResult;)V
    .locals 0

    .prologue
    .line 297
    return-void
.end method

.method public onDisconnected()V
    .locals 3

    .prologue
    .line 273
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/glympse/android/hal/c;->v:Z

    .line 277
    iget-object v0, p0, Lcom/glympse/android/hal/c;->z:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GHashtable;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 279
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/hal/c$b;

    .line 280
    const/4 v2, 0x0

    iput-object v2, v0, Lcom/glympse/android/hal/c$b;->D:Landroid/app/PendingIntent;

    goto :goto_0

    .line 282
    :cond_0
    return-void
.end method

.method public onNetworkLost()V
    .locals 0

    .prologue
    .line 287
    invoke-virtual {p0}, Lcom/glympse/android/hal/c;->onDisconnected()V

    .line 288
    return-void
.end method

.method public registerUpdates(Lcom/glympse/android/hal/GActivityListener;J)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 119
    iget-boolean v0, p0, Lcom/glympse/android/hal/c;->t:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/hal/c;->z:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 141
    :cond_0
    :goto_0
    return-void

    .line 124
    :cond_1
    new-instance v0, Lcom/glympse/android/hal/c$b;

    invoke-direct {v0, p0, v3}, Lcom/glympse/android/hal/c$b;-><init>(Lcom/glympse/android/hal/c;Lcom/glympse/android/hal/c$1;)V

    .line 125
    iput-wide p2, v0, Lcom/glympse/android/hal/c$b;->C:J

    .line 126
    iget-object v1, p0, Lcom/glympse/android/hal/c;->z:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v1, p1, v0}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/glympse/android/hal/c;->z:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v2}, Lcom/glympse/android/hal/GHashtable;->size()I

    move-result v2

    if-ne v1, v2, :cond_2

    .line 131
    iget-object v1, p0, Lcom/glympse/android/hal/c;->u:Lcom/glympse/android/hal/gms/location/ActivityRecognitionClient;

    invoke-virtual {v1}, Lcom/glympse/android/hal/gms/location/ActivityRecognitionClient;->connect()V

    .line 133
    new-instance v1, Lcom/glympse/android/hal/c$a;

    invoke-direct {v1, p0, v3}, Lcom/glympse/android/hal/c$a;-><init>(Lcom/glympse/android/hal/c;Lcom/glympse/android/hal/c$1;)V

    iput-object v1, p0, Lcom/glympse/android/hal/c;->y:Lcom/glympse/android/hal/c$a;

    .line 134
    iget-object v1, p0, Lcom/glympse/android/hal/c;->e:Landroid/content/Context;

    iget-object v2, p0, Lcom/glympse/android/hal/c;->y:Lcom/glympse/android/hal/c$a;

    new-instance v3, Landroid/content/IntentFilter;

    iget-object v4, p0, Lcom/glympse/android/hal/c;->w:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 137
    :cond_2
    iget-boolean v1, p0, Lcom/glympse/android/hal/c;->v:Z

    if-eqz v1, :cond_0

    .line 139
    invoke-direct {p0, p2, p3}, Lcom/glympse/android/hal/c;->a(J)Landroid/app/PendingIntent;

    move-result-object v1

    iput-object v1, v0, Lcom/glympse/android/hal/c$b;->D:Landroid/app/PendingIntent;

    goto :goto_0
.end method

.method public removeUpdates(Lcom/glympse/android/hal/GActivityListener;)V
    .locals 2

    .prologue
    .line 145
    iget-boolean v0, p0, Lcom/glympse/android/hal/c;->t:Z

    if-nez v0, :cond_1

    .line 171
    :cond_0
    :goto_0
    return-void

    .line 150
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/hal/c;->z:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/hal/c$b;

    .line 151
    if-eqz v0, :cond_0

    .line 155
    iget-object v1, p0, Lcom/glympse/android/hal/c;->z:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v1, p1}, Lcom/glympse/android/hal/GHashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    iget-boolean v1, p0, Lcom/glympse/android/hal/c;->v:Z

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/glympse/android/hal/c$b;->D:Landroid/app/PendingIntent;

    if-eqz v1, :cond_2

    .line 159
    iget-object v1, p0, Lcom/glympse/android/hal/c;->u:Lcom/glympse/android/hal/gms/location/ActivityRecognitionClient;

    iget-object v0, v0, Lcom/glympse/android/hal/c$b;->D:Landroid/app/PendingIntent;

    invoke-virtual {v1, v0}, Lcom/glympse/android/hal/gms/location/ActivityRecognitionClient;->removeActivityUpdates(Landroid/app/PendingIntent;)V

    .line 163
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/hal/c;->z:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GHashtable;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/glympse/android/hal/c;->u:Lcom/glympse/android/hal/gms/location/ActivityRecognitionClient;

    invoke-virtual {v0}, Lcom/glympse/android/hal/gms/location/ActivityRecognitionClient;->disconnect()V

    .line 166
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/glympse/android/hal/c;->v:Z

    .line 168
    iget-object v0, p0, Lcom/glympse/android/hal/c;->e:Landroid/content/Context;

    iget-object v1, p0, Lcom/glympse/android/hal/c;->y:Lcom/glympse/android/hal/c$a;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 169
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/hal/c;->y:Lcom/glympse/android/hal/c$a;

    goto :goto_0
.end method
