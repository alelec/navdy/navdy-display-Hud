.class Lcom/glympse/android/hal/r;
.super Ljava/lang/Object;
.source "LocationProvider.java"

# interfaces
.implements Lcom/glympse/android/core/GLocationProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/hal/r$e;,
        Lcom/glympse/android/hal/r$b;,
        Lcom/glympse/android/hal/r$a;,
        Lcom/glympse/android/hal/r$c;,
        Lcom/glympse/android/hal/r$d;
    }
.end annotation


# static fields
.field private static final cA:I = 0x1d4c0

.field public static final cu:I

.field public static final cv:I

.field public static final cw:I

.field public static final cx:I

.field public static final cy:I

.field public static final cz:I


# instance fields
.field private aU:Landroid/os/Handler;

.field private ce:Lcom/glympse/android/core/GLocationListener;

.field private cf:Landroid/location/LocationManager;

.field private cg:I

.field private ch:I

.field private ci:I

.field private cj:Lcom/glympse/android/core/GLocationProfile;

.field private ck:Z

.field private cl:Z

.field private cm:Z

.field private cn:Lcom/glympse/android/hal/r$d;

.field private co:Lcom/glympse/android/hal/r$c;

.field private cp:Lcom/glympse/android/hal/r$e;

.field private cq:Lcom/glympse/android/hal/r$a;

.field private cr:Lcom/glympse/android/hal/r$b;

.field private cs:Landroid/location/Location;

.field private ct:Z

.field private e:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p1, p0, Lcom/glympse/android/hal/r;->e:Landroid/content/Context;

    .line 64
    iput v0, p0, Lcom/glympse/android/hal/r;->cg:I

    .line 65
    iput v0, p0, Lcom/glympse/android/hal/r;->ch:I

    .line 66
    iput v0, p0, Lcom/glympse/android/hal/r;->ci:I

    .line 67
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/hal/r;->aU:Landroid/os/Handler;

    .line 68
    iput-object v1, p0, Lcom/glympse/android/hal/r;->cs:Landroid/location/Location;

    .line 69
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/glympse/android/hal/r;->ct:Z

    .line 72
    invoke-virtual {p0, v1}, Lcom/glympse/android/hal/r;->applyProfile(Lcom/glympse/android/core/GLocationProfile;)V

    .line 73
    return-void
.end method

.method private A()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x1

    .line 316
    iget v0, p0, Lcom/glympse/android/hal/r;->ch:I

    iget v1, p0, Lcom/glympse/android/hal/r;->ci:I

    if-ne v0, v1, :cond_0

    .line 318
    iget v0, p0, Lcom/glympse/android/hal/r;->ci:I

    invoke-direct {p0, v0}, Lcom/glympse/android/hal/r;->c(I)V

    .line 340
    :goto_0
    return-void

    .line 323
    :cond_0
    iget v0, p0, Lcom/glympse/android/hal/r;->ch:I

    if-eq v3, v0, :cond_1

    iget v0, p0, Lcom/glympse/android/hal/r;->ci:I

    if-ne v3, v0, :cond_2

    .line 325
    :cond_1
    invoke-direct {p0, v3}, Lcom/glympse/android/hal/r;->c(I)V

    goto :goto_0

    .line 329
    :cond_2
    iget v0, p0, Lcom/glympse/android/hal/r;->ch:I

    if-eq v2, v0, :cond_3

    iget v0, p0, Lcom/glympse/android/hal/r;->ci:I

    if-ne v2, v0, :cond_4

    .line 331
    :cond_3
    invoke-direct {p0, v2}, Lcom/glympse/android/hal/r;->c(I)V

    goto :goto_0

    .line 337
    :cond_4
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/glympse/android/hal/r;->c(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/glympse/android/hal/r;I)I
    .locals 0

    .prologue
    .line 31
    iput p1, p0, Lcom/glympse/android/hal/r;->ch:I

    return p1
.end method

.method static synthetic a(Lcom/glympse/android/hal/r;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/glympse/android/hal/r;->e:Landroid/content/Context;

    return-object v0
.end method

.method public static a(Landroid/location/Location;)Lcom/glympse/android/core/GLocation;
    .locals 13

    .prologue
    const/high16 v12, 0x7fc00000    # NaNf

    .line 290
    new-instance v1, Lcom/glympse/android/lib/Location;

    .line 291
    invoke-virtual {p0}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    .line 292
    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    .line 293
    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    .line 294
    invoke-virtual {p0}, Landroid/location/Location;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/location/Location;->getSpeed()F

    move-result v8

    .line 295
    :goto_0
    invoke-virtual {p0}, Landroid/location/Location;->hasBearing()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/location/Location;->getBearing()F

    move-result v9

    .line 296
    :goto_1
    invoke-virtual {p0}, Landroid/location/Location;->hasAltitude()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Landroid/location/Location;->getAltitude()D

    move-result-wide v10

    double-to-float v10, v10

    .line 297
    :goto_2
    invoke-virtual {p0}, Landroid/location/Location;->hasAccuracy()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Landroid/location/Location;->getAccuracy()F

    move-result v11

    :goto_3
    invoke-direct/range {v1 .. v12}, Lcom/glympse/android/lib/Location;-><init>(JDDFFFFF)V

    .line 290
    return-object v1

    :cond_0
    move v8, v12

    .line 294
    goto :goto_0

    :cond_1
    move v9, v12

    .line 295
    goto :goto_1

    :cond_2
    move v10, v12

    .line 296
    goto :goto_2

    :cond_3
    move v11, v12

    .line 297
    goto :goto_3
.end method

.method static synthetic a(Lcom/glympse/android/hal/r;Lcom/glympse/android/hal/r$a;)Lcom/glympse/android/hal/r$a;
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/glympse/android/hal/r;->cq:Lcom/glympse/android/hal/r$a;

    return-object p1
.end method

.method static synthetic a(Lcom/glympse/android/hal/r;Lcom/glympse/android/hal/r$b;)Lcom/glympse/android/hal/r$b;
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/glympse/android/hal/r;->cr:Lcom/glympse/android/hal/r$b;

    return-object p1
.end method

.method static synthetic a(Lcom/glympse/android/hal/r;Z)Z
    .locals 0

    .prologue
    .line 31
    iput-boolean p1, p0, Lcom/glympse/android/hal/r;->ct:Z

    return p1
.end method

.method static synthetic b(Lcom/glympse/android/hal/r;I)I
    .locals 0

    .prologue
    .line 31
    iput p1, p0, Lcom/glympse/android/hal/r;->ci:I

    return p1
.end method

.method public static b(I)Lcom/glympse/android/core/GLocationProfile;
    .locals 7

    .prologue
    const/16 v6, 0x64

    const/4 v3, 0x3

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 209
    packed-switch p0, :pswitch_data_0

    .line 256
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 213
    :pswitch_0
    new-instance v0, Lcom/glympse/android/lib/LocationProfile;

    invoke-direct {v0}, Lcom/glympse/android/lib/LocationProfile;-><init>()V

    .line 214
    invoke-virtual {v0, p0}, Lcom/glympse/android/lib/LocationProfile;->setProfile(I)V

    .line 215
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/glympse/android/lib/LocationProfile;->setMode(I)V

    .line 216
    invoke-virtual {v0, v3}, Lcom/glympse/android/lib/LocationProfile;->setSource(I)V

    .line 217
    invoke-virtual {v0, v6}, Lcom/glympse/android/lib/LocationProfile;->setPriority(I)V

    .line 218
    invoke-virtual {v0, v4, v5}, Lcom/glympse/android/lib/LocationProfile;->setDistance(D)V

    .line 219
    invoke-virtual {v0, v2}, Lcom/glympse/android/lib/LocationProfile;->setFrequency(I)V

    goto :goto_0

    .line 224
    :pswitch_1
    new-instance v0, Lcom/glympse/android/lib/LocationProfile;

    invoke-direct {v0}, Lcom/glympse/android/lib/LocationProfile;-><init>()V

    .line 225
    invoke-virtual {v0, p0}, Lcom/glympse/android/lib/LocationProfile;->setProfile(I)V

    .line 226
    invoke-virtual {v0, v2}, Lcom/glympse/android/lib/LocationProfile;->setMode(I)V

    .line 227
    invoke-virtual {v0, v3}, Lcom/glympse/android/lib/LocationProfile;->setSource(I)V

    .line 228
    invoke-virtual {v0, v6}, Lcom/glympse/android/lib/LocationProfile;->setPriority(I)V

    .line 229
    invoke-virtual {v0, v4, v5}, Lcom/glympse/android/lib/LocationProfile;->setDistance(D)V

    .line 230
    invoke-virtual {v0, v2}, Lcom/glympse/android/lib/LocationProfile;->setFrequency(I)V

    goto :goto_0

    .line 235
    :pswitch_2
    new-instance v0, Lcom/glympse/android/lib/LocationProfile;

    invoke-direct {v0}, Lcom/glympse/android/lib/LocationProfile;-><init>()V

    .line 236
    invoke-virtual {v0, p0}, Lcom/glympse/android/lib/LocationProfile;->setProfile(I)V

    .line 237
    invoke-virtual {v0, v2}, Lcom/glympse/android/lib/LocationProfile;->setMode(I)V

    .line 238
    invoke-virtual {v0, v3}, Lcom/glympse/android/lib/LocationProfile;->setSource(I)V

    .line 239
    invoke-virtual {v0, v6}, Lcom/glympse/android/lib/LocationProfile;->setPriority(I)V

    .line 240
    invoke-virtual {v0, v4, v5}, Lcom/glympse/android/lib/LocationProfile;->setDistance(D)V

    .line 241
    invoke-virtual {v0, v2}, Lcom/glympse/android/lib/LocationProfile;->setFrequency(I)V

    goto :goto_0

    .line 246
    :pswitch_3
    new-instance v0, Lcom/glympse/android/lib/LocationProfile;

    invoke-direct {v0}, Lcom/glympse/android/lib/LocationProfile;-><init>()V

    .line 247
    invoke-virtual {v0, p0}, Lcom/glympse/android/lib/LocationProfile;->setProfile(I)V

    .line 248
    invoke-virtual {v0, v2}, Lcom/glympse/android/lib/LocationProfile;->setMode(I)V

    .line 249
    invoke-virtual {v0, v3}, Lcom/glympse/android/lib/LocationProfile;->setSource(I)V

    .line 250
    invoke-virtual {v0, v6}, Lcom/glympse/android/lib/LocationProfile;->setPriority(I)V

    .line 251
    invoke-virtual {v0, v4, v5}, Lcom/glympse/android/lib/LocationProfile;->setDistance(D)V

    .line 252
    invoke-virtual {v0, v2}, Lcom/glympse/android/lib/LocationProfile;->setFrequency(I)V

    goto :goto_0

    .line 209
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1018
    if-nez p1, :cond_1

    .line 1020
    if-nez p2, :cond_0

    const/4 v0, 0x1

    .line 1022
    :goto_0
    return v0

    .line 1020
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1022
    :cond_1
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method private c(I)V
    .locals 2

    .prologue
    .line 303
    iget v0, p0, Lcom/glympse/android/hal/r;->cg:I

    if-eq p1, v0, :cond_0

    .line 305
    iput p1, p0, Lcom/glympse/android/hal/r;->cg:I

    .line 306
    iget-object v0, p0, Lcom/glympse/android/hal/r;->ce:Lcom/glympse/android/core/GLocationListener;

    if-eqz v0, :cond_0

    .line 308
    iget-object v0, p0, Lcom/glympse/android/hal/r;->ce:Lcom/glympse/android/core/GLocationListener;

    iget v1, p0, Lcom/glympse/android/hal/r;->cg:I

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GLocationListener;->stateChanged(I)V

    .line 311
    :cond_0
    return-void
.end method

.method public static d(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 375
    const-string v0, "android.permission.ACCESS_FINE_LOCATION"

    invoke-static {p0, v0}, Lcom/glympse/android/hal/h;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "android.permission.ACCESS_COARSE_LOCATION"

    .line 376
    invoke-static {p0, v0}, Lcom/glympse/android/hal/h;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 378
    const/4 v0, 0x0

    .line 380
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected B()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 389
    iget-boolean v0, p0, Lcom/glympse/android/hal/r;->ct:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/hal/r;->cn:Lcom/glympse/android/hal/r$d;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/glympse/android/hal/r;->ck:Z

    if-eqz v0, :cond_0

    .line 391
    new-instance v0, Lcom/glympse/android/hal/r$d;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/glympse/android/hal/r$d;-><init>(Lcom/glympse/android/hal/r;Lcom/glympse/android/hal/r$1;)V

    iput-object v0, p0, Lcom/glympse/android/hal/r;->cn:Lcom/glympse/android/hal/r$d;

    .line 392
    iget-object v0, p0, Lcom/glympse/android/hal/r;->cn:Lcom/glympse/android/hal/r$d;

    iget-object v1, p0, Lcom/glympse/android/hal/r;->cf:Landroid/location/LocationManager;

    invoke-virtual {v0, v1, v2, v2}, Lcom/glympse/android/hal/r$d;->a(Landroid/location/LocationManager;II)Z

    .line 394
    :cond_0
    return-void
.end method

.method protected C()V
    .locals 2

    .prologue
    .line 398
    iget-object v0, p0, Lcom/glympse/android/hal/r;->cn:Lcom/glympse/android/hal/r$d;

    if-eqz v0, :cond_0

    .line 400
    iget-object v0, p0, Lcom/glympse/android/hal/r;->cn:Lcom/glympse/android/hal/r$d;

    iget-object v1, p0, Lcom/glympse/android/hal/r;->cf:Landroid/location/LocationManager;

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/r$d;->a(Landroid/location/LocationManager;)V

    .line 401
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/hal/r;->cn:Lcom/glympse/android/hal/r$d;

    .line 403
    :cond_0
    return-void
.end method

.method protected D()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 407
    iget-object v0, p0, Lcom/glympse/android/hal/r;->cp:Lcom/glympse/android/hal/r$e;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/glympse/android/hal/r;->cm:Z

    if-eqz v0, :cond_0

    .line 409
    new-instance v0, Lcom/glympse/android/hal/r$e;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/glympse/android/hal/r$e;-><init>(Lcom/glympse/android/hal/r;Lcom/glympse/android/hal/r$1;)V

    iput-object v0, p0, Lcom/glympse/android/hal/r;->cp:Lcom/glympse/android/hal/r$e;

    .line 410
    iget-object v0, p0, Lcom/glympse/android/hal/r;->cp:Lcom/glympse/android/hal/r$e;

    iget-object v1, p0, Lcom/glympse/android/hal/r;->cf:Landroid/location/LocationManager;

    invoke-virtual {v0, v1, v2, v2}, Lcom/glympse/android/hal/r$e;->a(Landroid/location/LocationManager;II)Z

    .line 412
    :cond_0
    return-void
.end method

.method protected E()V
    .locals 2

    .prologue
    .line 416
    iget-object v0, p0, Lcom/glympse/android/hal/r;->cp:Lcom/glympse/android/hal/r$e;

    if-eqz v0, :cond_0

    .line 418
    iget-object v0, p0, Lcom/glympse/android/hal/r;->cp:Lcom/glympse/android/hal/r$e;

    iget-object v1, p0, Lcom/glympse/android/hal/r;->cf:Landroid/location/LocationManager;

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/r$e;->a(Landroid/location/LocationManager;)V

    .line 419
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/hal/r;->cp:Lcom/glympse/android/hal/r$e;

    .line 421
    :cond_0
    return-void
.end method

.method protected F()V
    .locals 5

    .prologue
    .line 425
    iget-object v0, p0, Lcom/glympse/android/hal/r;->co:Lcom/glympse/android/hal/r$c;

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/glympse/android/hal/r;->cl:Z

    if-eqz v0, :cond_1

    .line 427
    const-wide/16 v2, 0x0

    .line 428
    const/4 v0, 0x0

    .line 431
    iget-object v1, p0, Lcom/glympse/android/hal/r;->cj:Lcom/glympse/android/core/GLocationProfile;

    if-eqz v1, :cond_0

    .line 433
    iget-object v0, p0, Lcom/glympse/android/hal/r;->cj:Lcom/glympse/android/core/GLocationProfile;

    invoke-interface {v0}, Lcom/glympse/android/core/GLocationProfile;->getFrequency()I

    move-result v0

    int-to-long v2, v0

    .line 434
    iget-object v0, p0, Lcom/glympse/android/hal/r;->cj:Lcom/glympse/android/core/GLocationProfile;

    invoke-interface {v0}, Lcom/glympse/android/core/GLocationProfile;->getDistance()D

    move-result-wide v0

    double-to-float v0, v0

    .line 438
    :cond_0
    new-instance v1, Lcom/glympse/android/hal/r$c;

    const/4 v4, 0x0

    invoke-direct {v1, p0, v4}, Lcom/glympse/android/hal/r$c;-><init>(Lcom/glympse/android/hal/r;Lcom/glympse/android/hal/r$1;)V

    iput-object v1, p0, Lcom/glympse/android/hal/r;->co:Lcom/glympse/android/hal/r$c;

    .line 439
    iget-object v1, p0, Lcom/glympse/android/hal/r;->co:Lcom/glympse/android/hal/r$c;

    iget-object v4, p0, Lcom/glympse/android/hal/r;->cf:Landroid/location/LocationManager;

    invoke-virtual {v1, v4, v2, v3, v0}, Lcom/glympse/android/hal/r$c;->a(Landroid/location/LocationManager;JF)Z

    .line 441
    :cond_1
    return-void
.end method

.method protected G()V
    .locals 2

    .prologue
    .line 445
    iget-object v0, p0, Lcom/glympse/android/hal/r;->co:Lcom/glympse/android/hal/r$c;

    if-eqz v0, :cond_0

    .line 447
    iget-object v0, p0, Lcom/glympse/android/hal/r;->co:Lcom/glympse/android/hal/r$c;

    iget-object v1, p0, Lcom/glympse/android/hal/r;->cf:Landroid/location/LocationManager;

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/r$c;->a(Landroid/location/LocationManager;)V

    .line 448
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/hal/r;->co:Lcom/glympse/android/hal/r$c;

    .line 450
    :cond_0
    return-void
.end method

.method protected H()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 455
    iget-object v0, p0, Lcom/glympse/android/hal/r;->cq:Lcom/glympse/android/hal/r$a;

    if-nez v0, :cond_0

    .line 457
    const/4 v0, 0x1

    const-string v1, "[LocationProvider] Starting location timer"

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 460
    new-instance v0, Lcom/glympse/android/hal/r$a;

    invoke-direct {v0, p0, v4}, Lcom/glympse/android/hal/r$a;-><init>(Lcom/glympse/android/hal/r;Lcom/glympse/android/hal/r$1;)V

    iput-object v0, p0, Lcom/glympse/android/hal/r;->cq:Lcom/glympse/android/hal/r$a;

    .line 463
    iget-object v0, p0, Lcom/glympse/android/hal/r;->aU:Landroid/os/Handler;

    iget-object v1, p0, Lcom/glympse/android/hal/r;->cq:Lcom/glympse/android/hal/r$a;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 465
    iput-object v4, p0, Lcom/glympse/android/hal/r;->cq:Lcom/glympse/android/hal/r$a;

    .line 468
    :cond_0
    return-void
.end method

.method protected I()V
    .locals 2

    .prologue
    .line 473
    iget-object v0, p0, Lcom/glympse/android/hal/r;->cq:Lcom/glympse/android/hal/r$a;

    if-eqz v0, :cond_0

    .line 475
    const/4 v0, 0x1

    const-string v1, "[LocationProvider] Stopping location timer"

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 478
    iget-object v0, p0, Lcom/glympse/android/hal/r;->aU:Landroid/os/Handler;

    iget-object v1, p0, Lcom/glympse/android/hal/r;->cq:Lcom/glympse/android/hal/r$a;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 481
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/hal/r;->cq:Lcom/glympse/android/hal/r$a;

    .line 483
    :cond_0
    return-void
.end method

.method protected J()V
    .locals 2

    .prologue
    .line 506
    iget-object v0, p0, Lcom/glympse/android/hal/r;->cr:Lcom/glympse/android/hal/r$b;

    if-eqz v0, :cond_0

    .line 508
    const/4 v0, 0x1

    const-string v1, "[LocationProvider] Stopping permission timer"

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 511
    iget-object v0, p0, Lcom/glympse/android/hal/r;->aU:Landroid/os/Handler;

    iget-object v1, p0, Lcom/glympse/android/hal/r;->cr:Lcom/glympse/android/hal/r$b;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 514
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/hal/r;->cr:Lcom/glympse/android/hal/r$b;

    .line 516
    :cond_0
    return-void
.end method

.method protected K()V
    .locals 1

    .prologue
    .line 520
    invoke-virtual {p0}, Lcom/glympse/android/hal/r;->G()V

    .line 521
    invoke-virtual {p0}, Lcom/glympse/android/hal/r;->C()V

    .line 522
    invoke-virtual {p0}, Lcom/glympse/android/hal/r;->E()V

    .line 525
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/hal/r;->cs:Landroid/location/Location;

    .line 529
    iget-object v0, p0, Lcom/glympse/android/hal/r;->cf:Landroid/location/LocationManager;

    if-eqz v0, :cond_0

    .line 531
    invoke-virtual {p0}, Lcom/glympse/android/hal/r;->F()V

    .line 532
    invoke-virtual {p0}, Lcom/glympse/android/hal/r;->B()V

    .line 533
    invoke-virtual {p0}, Lcom/glympse/android/hal/r;->D()V

    .line 535
    :cond_0
    return-void
.end method

.method protected a(Lcom/glympse/android/core/GLocationProvider;)V
    .locals 4

    .prologue
    .line 488
    iget-object v0, p0, Lcom/glympse/android/hal/r;->cr:Lcom/glympse/android/hal/r$b;

    if-nez v0, :cond_0

    .line 490
    const/4 v0, 0x1

    const-string v1, "[LocationProvider] Starting permission timer"

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 493
    new-instance v0, Lcom/glympse/android/hal/r$b;

    invoke-direct {v0, p0, p1}, Lcom/glympse/android/hal/r$b;-><init>(Lcom/glympse/android/hal/r;Lcom/glympse/android/core/GLocationProvider;)V

    iput-object v0, p0, Lcom/glympse/android/hal/r;->cr:Lcom/glympse/android/hal/r$b;

    .line 496
    iget-object v0, p0, Lcom/glympse/android/hal/r;->aU:Landroid/os/Handler;

    iget-object v1, p0, Lcom/glympse/android/hal/r;->cr:Lcom/glympse/android/hal/r$b;

    const-wide/16 v2, 0x3a98

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 498
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/hal/r;->cr:Lcom/glympse/android/hal/r$b;

    .line 501
    :cond_0
    return-void
.end method

.method protected a(Landroid/location/Location;Landroid/location/Location;)Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 964
    if-nez p2, :cond_1

    .line 1010
    :cond_0
    :goto_0
    return v1

    .line 971
    :cond_1
    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    invoke-virtual {p2}, Landroid/location/Location;->getTime()J

    move-result-wide v6

    sub-long/2addr v4, v6

    .line 972
    const-wide/32 v6, 0x1d4c0

    cmp-long v0, v4, v6

    if-lez v0, :cond_2

    move v3, v1

    .line 973
    :goto_1
    const-wide/32 v6, -0x1d4c0

    cmp-long v0, v4, v6

    if-gez v0, :cond_3

    move v0, v1

    .line 974
    :goto_2
    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_4

    move v5, v1

    .line 978
    :goto_3
    if-nez v3, :cond_0

    .line 983
    if-eqz v0, :cond_5

    move v1, v2

    .line 985
    goto :goto_0

    :cond_2
    move v3, v2

    .line 972
    goto :goto_1

    :cond_3
    move v0, v2

    .line 973
    goto :goto_2

    :cond_4
    move v5, v2

    .line 974
    goto :goto_3

    .line 989
    :cond_5
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    invoke-virtual {p2}, Landroid/location/Location;->getAccuracy()F

    move-result v3

    sub-float/2addr v0, v3

    float-to-int v0, v0

    .line 990
    if-lez v0, :cond_8

    move v4, v1

    .line 991
    :goto_4
    if-gez v0, :cond_9

    move v3, v1

    .line 992
    :goto_5
    const/16 v6, 0xc8

    if-le v0, v6, :cond_a

    move v0, v1

    .line 995
    :goto_6
    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v6, v7}, Lcom/glympse/android/hal/r;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    .line 998
    if-nez v3, :cond_0

    .line 1002
    if-eqz v5, :cond_6

    if-eqz v4, :cond_0

    .line 1006
    :cond_6
    if-eqz v5, :cond_7

    if-nez v0, :cond_7

    if-nez v6, :cond_0

    :cond_7
    move v1, v2

    .line 1010
    goto :goto_0

    :cond_8
    move v4, v2

    .line 990
    goto :goto_4

    :cond_9
    move v3, v2

    .line 991
    goto :goto_5

    :cond_a
    move v0, v2

    .line 992
    goto :goto_6
.end method

.method protected a(Ljava/lang/String;Landroid/location/Location;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 267
    iget-object v1, p0, Lcom/glympse/android/hal/r;->ce:Lcom/glympse/android/core/GLocationListener;

    if-nez v1, :cond_1

    .line 285
    :cond_0
    :goto_0
    return v0

    .line 273
    :cond_1
    iget-object v1, p0, Lcom/glympse/android/hal/r;->cs:Landroid/location/Location;

    invoke-virtual {p0, p2, v1}, Lcom/glympse/android/hal/r;->a(Landroid/location/Location;Landroid/location/Location;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 277
    iput-object p2, p0, Lcom/glympse/android/hal/r;->cs:Landroid/location/Location;

    .line 280
    invoke-static {p2}, Lcom/glympse/android/hal/r;->a(Landroid/location/Location;)Lcom/glympse/android/core/GLocation;

    move-result-object v0

    .line 283
    iget-object v1, p0, Lcom/glympse/android/hal/r;->ce:Lcom/glympse/android/core/GLocationListener;

    invoke-interface {v1, v0}, Lcom/glympse/android/core/GLocationListener;->locationChanged(Lcom/glympse/android/core/GLocation;)V

    .line 285
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public applyProfile(Lcom/glympse/android/core/GLocationProfile;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 176
    iput-object p1, p0, Lcom/glympse/android/hal/r;->cj:Lcom/glympse/android/core/GLocationProfile;

    .line 179
    iget-object v0, p0, Lcom/glympse/android/hal/r;->cj:Lcom/glympse/android/core/GLocationProfile;

    if-eqz v0, :cond_3

    .line 181
    iget-object v0, p0, Lcom/glympse/android/hal/r;->cj:Lcom/glympse/android/core/GLocationProfile;

    invoke-interface {v0}, Lcom/glympse/android/core/GLocationProfile;->getSource()I

    move-result v3

    .line 182
    and-int/lit8 v0, v3, 0x1

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/glympse/android/hal/r;->ck:Z

    .line 183
    and-int/lit8 v0, v3, 0x2

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/glympse/android/hal/r;->cl:Z

    .line 184
    and-int/lit8 v0, v3, 0x4

    if-eqz v0, :cond_2

    :goto_2
    iput-boolean v1, p0, Lcom/glympse/android/hal/r;->cm:Z

    .line 195
    :goto_3
    invoke-virtual {p0}, Lcom/glympse/android/hal/r;->K()V

    .line 196
    return-void

    :cond_0
    move v0, v2

    .line 182
    goto :goto_0

    :cond_1
    move v0, v2

    .line 183
    goto :goto_1

    :cond_2
    move v1, v2

    .line 184
    goto :goto_2

    .line 188
    :cond_3
    iput-boolean v1, p0, Lcom/glympse/android/hal/r;->ck:Z

    .line 189
    iput-boolean v1, p0, Lcom/glympse/android/hal/r;->cl:Z

    .line 190
    iput-boolean v2, p0, Lcom/glympse/android/hal/r;->cm:Z

    goto :goto_3
.end method

.method protected d(I)V
    .locals 2

    .prologue
    .line 344
    iget v0, p0, Lcom/glympse/android/hal/r;->ch:I

    if-eq p1, v0, :cond_0

    .line 347
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    iget v0, p0, Lcom/glympse/android/hal/r;->ch:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 355
    :cond_0
    :goto_0
    return-void

    .line 352
    :cond_1
    iput p1, p0, Lcom/glympse/android/hal/r;->ch:I

    .line 353
    invoke-direct {p0}, Lcom/glympse/android/hal/r;->A()V

    goto :goto_0
.end method

.method protected e(I)V
    .locals 2

    .prologue
    .line 359
    iget v0, p0, Lcom/glympse/android/hal/r;->ci:I

    if-eq p1, v0, :cond_0

    .line 362
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    iget v0, p0, Lcom/glympse/android/hal/r;->ci:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 370
    :cond_0
    :goto_0
    return-void

    .line 367
    :cond_1
    iput p1, p0, Lcom/glympse/android/hal/r;->ci:I

    .line 368
    invoke-direct {p0}, Lcom/glympse/android/hal/r;->A()V

    goto :goto_0
.end method

.method public getLastKnownLocation()Lcom/glympse/android/core/GLocation;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 136
    .line 138
    iget-object v0, p0, Lcom/glympse/android/hal/r;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/glympse/android/hal/r;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 165
    :goto_0
    return-object v3

    .line 146
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/glympse/android/hal/r;->e:Landroid/content/Context;

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 149
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getProviders(Z)Ljava/util/List;

    move-result-object v1

    .line 150
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    move-object v2, v3

    :goto_1
    :try_start_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 153
    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v1

    .line 154
    if-eqz v1, :cond_3

    invoke-virtual {p0, v1, v2}, Lcom/glympse/android/hal/r;->a(Landroid/location/Location;Landroid/location/Location;)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    move-result v5

    if-eqz v5, :cond_3

    :goto_2
    move-object v2, v1

    .line 158
    goto :goto_1

    .line 160
    :catch_0
    move-exception v0

    move-object v2, v3

    .line 165
    :cond_1
    :goto_3
    if-nez v2, :cond_2

    move-object v0, v3

    :goto_4
    move-object v3, v0

    goto :goto_0

    :cond_2
    invoke-static {v2}, Lcom/glympse/android/hal/r;->a(Landroid/location/Location;)Lcom/glympse/android/core/GLocation;

    move-result-object v0

    goto :goto_4

    .line 160
    :catch_1
    move-exception v0

    goto :goto_3

    :cond_3
    move-object v1, v2

    goto :goto_2
.end method

.method public isStarted()Z
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/glympse/android/hal/r;->cf:Landroid/location/LocationManager;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setLocationListener(Lcom/glympse/android/core/GLocationListener;)V
    .locals 0

    .prologue
    .line 170
    iput-object p1, p0, Lcom/glympse/android/hal/r;->ce:Lcom/glympse/android/core/GLocationListener;

    .line 171
    return-void
.end method

.method public start()V
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lcom/glympse/android/hal/r;->cf:Landroid/location/LocationManager;

    if-nez v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/glympse/android/hal/r;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/glympse/android/hal/r;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 86
    invoke-virtual {p0, p0}, Lcom/glympse/android/hal/r;->a(Lcom/glympse/android/core/GLocationProvider;)V

    .line 106
    :cond_0
    :goto_0
    return-void

    .line 93
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/glympse/android/hal/r;->e:Landroid/content/Context;

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/glympse/android/hal/r;->cf:Landroid/location/LocationManager;

    .line 96
    invoke-virtual {p0}, Lcom/glympse/android/hal/r;->F()V

    .line 97
    invoke-virtual {p0}, Lcom/glympse/android/hal/r;->B()V

    .line 98
    invoke-virtual {p0}, Lcom/glympse/android/hal/r;->D()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 100
    :catch_0
    move-exception v0

    .line 102
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    .line 103
    invoke-virtual {p0}, Lcom/glympse/android/hal/r;->stop()V

    goto :goto_0
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 110
    invoke-virtual {p0}, Lcom/glympse/android/hal/r;->J()V

    .line 112
    iget-object v0, p0, Lcom/glympse/android/hal/r;->cf:Landroid/location/LocationManager;

    if-eqz v0, :cond_0

    .line 116
    :try_start_0
    invoke-virtual {p0}, Lcom/glympse/android/hal/r;->C()V

    .line 117
    invoke-virtual {p0}, Lcom/glympse/android/hal/r;->G()V

    .line 118
    invoke-virtual {p0}, Lcom/glympse/android/hal/r;->E()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 124
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/hal/r;->cf:Landroid/location/LocationManager;

    .line 126
    :cond_0
    return-void

    .line 120
    :catch_0
    move-exception v0

    .line 122
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1031
    const-string v0, "com.glympse.android.hal.LocationProvider"

    return-object v0
.end method
