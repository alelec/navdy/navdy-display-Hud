.class public Lcom/glympse/android/hal/PhoneFavoriteProvider;
.super Ljava/lang/Object;
.source "PhoneFavoriteProvider.java"

# interfaces
.implements Lcom/glympse/android/hal/GPhoneFavoriteProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/hal/PhoneFavoriteProvider$b;,
        Lcom/glympse/android/hal/PhoneFavoriteProvider$a;
    }
.end annotation


# static fields
.field private static final cM:Ljava/lang/String; = "data1"


# instance fields
.field private R:Ljava/util/concurrent/Future;

.field protected _handler:Lcom/glympse/android/core/GHandler;

.field protected cJ:Lcom/glympse/android/hal/GPhoneFavoriteListener;

.field private cK:Lcom/glympse/android/hal/PhoneFavoriteProvider$b;

.field private cL:Lcom/glympse/android/core/GArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/core/GArray",
            "<",
            "Lcom/glympse/android/lib/GPhoneFavorite;",
            ">;"
        }
    .end annotation
.end field

.field protected e:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/glympse/android/hal/PhoneFavoriteProvider;->e:Landroid/content/Context;

    .line 31
    return-void
.end method

.method private static e(Landroid/content/Context;)Lcom/glympse/android/hal/GVector;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lcom/glympse/android/hal/GVector",
            "<",
            "Lcom/glympse/android/lib/GPhoneFavorite;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v13, 0x2

    const/4 v5, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 156
    .line 157
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 159
    const/4 v1, 0x4

    new-array v2, v1, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v2, v9

    const-string v1, "display_name"

    aput-object v1, v2, v8

    const-string v1, "starred"

    aput-object v1, v2, v13

    const/4 v1, 0x3

    const-string v3, "has_phone_number"

    aput-object v3, v2, v1

    .line 167
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v1

    if-nez v1, :cond_d

    .line 169
    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "starred=?"

    new-array v4, v8, [Ljava/lang/String;

    const-string v6, "1"

    aput-object v6, v4, v9

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    move-object v10, v1

    .line 173
    :goto_0
    if-eqz v10, :cond_5

    .line 176
    new-instance v6, Lcom/glympse/android/hal/GVector;

    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-direct {v6, v1}, Lcom/glympse/android/hal/GVector;-><init>(I)V

    .line 179
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    .line 180
    :goto_1
    if-eqz v1, :cond_4

    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v1

    if-nez v1, :cond_4

    .line 183
    const-string v1, "_id"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 185
    const-string v1, "display_name"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 187
    const-string v1, "has_phone_number"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-ne v1, v8, :cond_3

    move v1, v8

    .line 192
    :goto_2
    if-eqz v1, :cond_c

    .line 195
    new-array v2, v13, [Ljava/lang/String;

    const-string v1, "contact_id"

    aput-object v1, v2, v9

    const-string v1, "data1"

    aput-object v1, v2, v8

    .line 201
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v1

    if-nez v1, :cond_b

    .line 203
    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "contact_id=?"

    new-array v4, v8, [Ljava/lang/String;

    aput-object v11, v4, v9

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 209
    :goto_3
    if-eqz v1, :cond_a

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 211
    const-string v2, "data1"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 214
    :goto_4
    if-eqz v1, :cond_0

    .line 216
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 220
    :cond_0
    :goto_5
    if-nez v7, :cond_9

    .line 222
    new-array v2, v13, [Ljava/lang/String;

    const-string v1, "contact_id"

    aput-object v1, v2, v9

    const-string v1, "data1"

    aput-object v1, v2, v8

    .line 228
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v1

    if-nez v1, :cond_8

    .line 230
    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$Email;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "contact_id=?"

    new-array v4, v8, [Ljava/lang/String;

    aput-object v11, v4, v9

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    move-object v2, v1

    .line 236
    :goto_6
    if-eqz v2, :cond_7

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 238
    const-string v1, "data1"

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object v1, v7

    .line 241
    :goto_7
    if-eqz v2, :cond_1

    .line 243
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 247
    :cond_1
    :goto_8
    if-eqz v1, :cond_2

    .line 250
    invoke-static {v12, v1, v9}, Lcom/glympse/android/lib/LibFactory;->createPhoneFavorite(Ljava/lang/String;Ljava/lang/String;I)Lcom/glympse/android/lib/GPhoneFavorite;

    move-result-object v1

    .line 251
    invoke-virtual {v6, v1}, Lcom/glympse/android/hal/GVector;->add(Ljava/lang/Object;)Z

    .line 255
    :cond_2
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    goto/16 :goto_1

    :cond_3
    move v1, v9

    .line 187
    goto :goto_2

    :cond_4
    move-object v5, v6

    .line 259
    :cond_5
    if-eqz v10, :cond_6

    .line 261
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 263
    :cond_6
    return-object v5

    :cond_7
    move-object v1, v7

    goto :goto_7

    :cond_8
    move-object v2, v5

    goto :goto_6

    :cond_9
    move-object v1, v7

    goto :goto_8

    :cond_a
    move-object v7, v5

    goto :goto_4

    :cond_b
    move-object v1, v5

    goto :goto_3

    :cond_c
    move-object v7, v5

    goto :goto_5

    :cond_d
    move-object v10, v5

    goto/16 :goto_0
.end method

.method static synthetic f(Landroid/content/Context;)Lcom/glympse/android/hal/GVector;
    .locals 1

    .prologue
    .line 17
    invoke-static {p0}, Lcom/glympse/android/hal/PhoneFavoriteProvider;->e(Landroid/content/Context;)Lcom/glympse/android/hal/GVector;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public complete(Lcom/glympse/android/hal/GVector;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/glympse/android/hal/GVector",
            "<",
            "Lcom/glympse/android/lib/GPhoneFavorite;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 98
    iput-object p1, p0, Lcom/glympse/android/hal/PhoneFavoriteProvider;->cL:Lcom/glympse/android/core/GArray;

    .line 99
    iput-object v0, p0, Lcom/glympse/android/hal/PhoneFavoriteProvider;->cK:Lcom/glympse/android/hal/PhoneFavoriteProvider$b;

    .line 100
    iput-object v0, p0, Lcom/glympse/android/hal/PhoneFavoriteProvider;->R:Ljava/util/concurrent/Future;

    .line 101
    iget-object v0, p0, Lcom/glympse/android/hal/PhoneFavoriteProvider;->cJ:Lcom/glympse/android/hal/GPhoneFavoriteListener;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/glympse/android/hal/PhoneFavoriteProvider;->cJ:Lcom/glympse/android/hal/GPhoneFavoriteListener;

    invoke-interface {v0, p0}, Lcom/glympse/android/hal/GPhoneFavoriteListener;->phoneFavoritesReady(Lcom/glympse/android/hal/GPhoneFavoriteProvider;)V

    .line 105
    :cond_0
    return-void
.end method

.method public getPhoneFavorites()Lcom/glympse/android/core/GArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/glympse/android/core/GArray",
            "<",
            "Lcom/glympse/android/lib/GPhoneFavorite;",
            ">;"
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lcom/glympse/android/hal/PhoneFavoriteProvider;->cL:Lcom/glympse/android/core/GArray;

    return-object v0
.end method

.method public refresh(Z)V
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/glympse/android/hal/PhoneFavoriteProvider;->e:Landroid/content/Context;

    const-string v1, "android.permission.READ_CONTACTS"

    invoke-static {v0, v1}, Lcom/glympse/android/hal/h;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 69
    new-instance v0, Lcom/glympse/android/hal/GVector;

    invoke-direct {v0}, Lcom/glympse/android/hal/GVector;-><init>()V

    invoke-virtual {p0, v0}, Lcom/glympse/android/hal/PhoneFavoriteProvider;->complete(Lcom/glympse/android/hal/GVector;)V

    .line 88
    :cond_0
    :goto_0
    return-void

    .line 73
    :cond_1
    if-eqz p1, :cond_2

    .line 75
    iget-object v0, p0, Lcom/glympse/android/hal/PhoneFavoriteProvider;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/glympse/android/hal/PhoneFavoriteProvider;->e(Landroid/content/Context;)Lcom/glympse/android/hal/GVector;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/hal/PhoneFavoriteProvider;->cL:Lcom/glympse/android/core/GArray;

    goto :goto_0

    .line 80
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/hal/PhoneFavoriteProvider;->cK:Lcom/glympse/android/hal/PhoneFavoriteProvider$b;

    if-nez v0, :cond_0

    .line 85
    new-instance v0, Lcom/glympse/android/hal/PhoneFavoriteProvider$b;

    iget-object v1, p0, Lcom/glympse/android/hal/PhoneFavoriteProvider;->e:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/glympse/android/hal/PhoneFavoriteProvider$b;-><init>(Lcom/glympse/android/hal/PhoneFavoriteProvider;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/glympse/android/hal/PhoneFavoriteProvider;->cK:Lcom/glympse/android/hal/PhoneFavoriteProvider$b;

    .line 86
    invoke-static {}, Lcom/glympse/android/hal/GlympseThreadPool;->instance()Lcom/glympse/android/hal/GlympseThreadPool;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/hal/PhoneFavoriteProvider;->cK:Lcom/glympse/android/hal/PhoneFavoriteProvider$b;

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GlympseThreadPool;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/hal/PhoneFavoriteProvider;->R:Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public start(Lcom/glympse/android/hal/GPhoneFavoriteListener;Lcom/glympse/android/core/GHandler;)V
    .locals 1

    .prologue
    .line 38
    iput-object p1, p0, Lcom/glympse/android/hal/PhoneFavoriteProvider;->cJ:Lcom/glympse/android/hal/GPhoneFavoriteListener;

    .line 39
    iput-object p2, p0, Lcom/glympse/android/hal/PhoneFavoriteProvider;->_handler:Lcom/glympse/android/core/GHandler;

    .line 41
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/glympse/android/hal/PhoneFavoriteProvider;->refresh(Z)V

    .line 42
    return-void
.end method

.method public stop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 47
    iget-object v0, p0, Lcom/glympse/android/hal/PhoneFavoriteProvider;->cK:Lcom/glympse/android/hal/PhoneFavoriteProvider$b;

    if-eqz v0, :cond_0

    .line 51
    :try_start_0
    iget-object v0, p0, Lcom/glympse/android/hal/PhoneFavoriteProvider;->R:Ljava/util/concurrent/Future;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 56
    :goto_0
    iput-object v2, p0, Lcom/glympse/android/hal/PhoneFavoriteProvider;->cK:Lcom/glympse/android/hal/PhoneFavoriteProvider$b;

    .line 57
    iput-object v2, p0, Lcom/glympse/android/hal/PhoneFavoriteProvider;->R:Ljava/util/concurrent/Future;

    .line 59
    :cond_0
    iput-object v2, p0, Lcom/glympse/android/hal/PhoneFavoriteProvider;->cJ:Lcom/glympse/android/hal/GPhoneFavoriteListener;

    .line 60
    iput-object v2, p0, Lcom/glympse/android/hal/PhoneFavoriteProvider;->_handler:Lcom/glympse/android/core/GHandler;

    .line 61
    return-void

    .line 53
    :catch_0
    move-exception v0

    goto :goto_0
.end method
