.class Lcom/glympse/android/hal/s;
.super Ljava/lang/Object;
.source "LocationProviderFuse.java"

# interfaces
.implements Lcom/glympse/android/core/GLocationProvider;
.implements Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
.implements Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;
.implements Lcom/glympse/android/hal/gms/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/hal/s$a;
    }
.end annotation


# instance fields
.field private aU:Landroid/os/Handler;

.field private cE:Lcom/glympse/android/hal/gms/location/LocationClient;

.field private cF:Lcom/glympse/android/hal/gms/location/LocationRequest;

.field private cG:Lcom/glympse/android/hal/s$a;

.field private ce:Lcom/glympse/android/core/GLocationListener;

.field private cg:I

.field private e:Landroid/content/Context;

.field private v:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    iput-object p1, p0, Lcom/glympse/android/hal/s;->e:Landroid/content/Context;

    .line 92
    const/4 v0, 0x1

    iput v0, p0, Lcom/glympse/android/hal/s;->cg:I

    .line 93
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/glympse/android/hal/s;->v:Z

    .line 96
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/glympse/android/hal/r;->b(I)Lcom/glympse/android/core/GLocationProfile;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/glympse/android/hal/s;->applyProfile(Lcom/glympse/android/core/GLocationProfile;)V

    .line 97
    return-void
.end method

.method private L()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 210
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-lt v2, v3, :cond_2

    .line 220
    :try_start_0
    iget-object v2, p0, Lcom/glympse/android/hal/s;->e:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "location_mode"

    invoke-static {v2, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 221
    if-nez v2, :cond_1

    .line 239
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 225
    goto :goto_0

    .line 227
    :catch_0
    move-exception v2

    .line 234
    :cond_2
    iget-object v2, p0, Lcom/glympse/android/hal/s;->e:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "location_providers_allowed"

    invoke-static {v2, v3}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 235
    const-string v3, "gps"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 239
    goto :goto_0
.end method

.method static synthetic a(Lcom/glympse/android/hal/s;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/glympse/android/hal/s;->e:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(Lcom/glympse/android/hal/s;Lcom/glympse/android/hal/s$a;)Lcom/glympse/android/hal/s$a;
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lcom/glympse/android/hal/s;->cG:Lcom/glympse/android/hal/s$a;

    return-object p1
.end method

.method static synthetic b(Lcom/glympse/android/hal/s;)Lcom/glympse/android/hal/gms/location/LocationClient;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/glympse/android/hal/s;->cE:Lcom/glympse/android/hal/gms/location/LocationClient;

    return-object v0
.end method

.method static synthetic c(Lcom/glympse/android/hal/s;)Lcom/glympse/android/hal/gms/location/LocationRequest;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/glympse/android/hal/s;->cF:Lcom/glympse/android/hal/gms/location/LocationRequest;

    return-object v0
.end method

.method static synthetic d(Lcom/glympse/android/hal/s;)Z
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/glympse/android/hal/s;->L()Z

    move-result v0

    return v0
.end method

.method public static isSupported(Landroid/content/Context;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 61
    .line 64
    :try_start_0
    invoke-static {p0}, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->isSupported(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 85
    :cond_0
    :goto_0
    return v0

    .line 70
    :cond_1
    const v1, 0x3d0900

    invoke-static {p0, v1}, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->isVersionSupported(Landroid/content/Context;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 76
    invoke-static {p0}, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->isLocationSupported(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 81
    const/4 v0, 0x1

    goto :goto_0

    .line 83
    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method protected J()V
    .locals 2

    .prologue
    .line 308
    iget-object v0, p0, Lcom/glympse/android/hal/s;->cG:Lcom/glympse/android/hal/s$a;

    if-eqz v0, :cond_0

    .line 310
    const/4 v0, 0x1

    const-string v1, "[LocationProvider] Stopping permission timer"

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 313
    iget-object v0, p0, Lcom/glympse/android/hal/s;->aU:Landroid/os/Handler;

    iget-object v1, p0, Lcom/glympse/android/hal/s;->cG:Lcom/glympse/android/hal/s$a;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 316
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/hal/s;->cG:Lcom/glympse/android/hal/s$a;

    .line 318
    :cond_0
    return-void
.end method

.method protected K()V
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Lcom/glympse/android/hal/s;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/glympse/android/hal/r;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 182
    :cond_0
    :goto_0
    return-void

    .line 176
    :cond_1
    iget-boolean v0, p0, Lcom/glympse/android/hal/s;->v:Z

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/glympse/android/hal/s;->cE:Lcom/glympse/android/hal/gms/location/LocationClient;

    iget-object v1, p0, Lcom/glympse/android/hal/s;->cF:Lcom/glympse/android/hal/gms/location/LocationRequest;

    invoke-virtual {v0, v1, p0}, Lcom/glympse/android/hal/gms/location/LocationClient;->requestLocationUpdates(Lcom/glympse/android/hal/gms/location/LocationRequest;Lcom/glympse/android/hal/gms/location/LocationListener;)V

    goto :goto_0
.end method

.method protected a(Lcom/glympse/android/core/GLocationProfile;)Lcom/glympse/android/hal/gms/location/LocationRequest;
    .locals 4

    .prologue
    .line 187
    invoke-static {}, Lcom/glympse/android/hal/gms/location/LocationRequest;->create()Lcom/glympse/android/hal/gms/location/LocationRequest;

    move-result-object v0

    .line 188
    invoke-interface {p1}, Lcom/glympse/android/core/GLocationProfile;->getPriority()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/gms/location/LocationRequest;->setPriority(I)V

    .line 189
    invoke-interface {p1}, Lcom/glympse/android/core/GLocationProfile;->getFrequency()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/glympse/android/hal/gms/location/LocationRequest;->setInterval(J)V

    .line 193
    return-object v0
.end method

.method protected a(Lcom/glympse/android/core/GLocationProvider;)V
    .locals 4

    .prologue
    .line 285
    iget-object v0, p0, Lcom/glympse/android/hal/s;->cG:Lcom/glympse/android/hal/s$a;

    if-nez v0, :cond_1

    .line 287
    const/4 v0, 0x1

    const-string v1, "[LocationProvider] Starting permission timer"

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 290
    new-instance v0, Lcom/glympse/android/hal/s$a;

    invoke-direct {v0, p0, p1}, Lcom/glympse/android/hal/s$a;-><init>(Lcom/glympse/android/hal/s;Lcom/glympse/android/core/GLocationProvider;)V

    iput-object v0, p0, Lcom/glympse/android/hal/s;->cG:Lcom/glympse/android/hal/s$a;

    .line 292
    iget-object v0, p0, Lcom/glympse/android/hal/s;->aU:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 294
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/hal/s;->aU:Landroid/os/Handler;

    .line 298
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/hal/s;->aU:Landroid/os/Handler;

    iget-object v1, p0, Lcom/glympse/android/hal/s;->cG:Lcom/glympse/android/hal/s$a;

    const-wide/16 v2, 0x3a98

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    move-result v0

    if-nez v0, :cond_1

    .line 300
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/hal/s;->cG:Lcom/glympse/android/hal/s$a;

    .line 303
    :cond_1
    return-void
.end method

.method public applyProfile(Lcom/glympse/android/core/GLocationProfile;)V
    .locals 1

    .prologue
    .line 153
    if-nez p1, :cond_0

    .line 155
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/glympse/android/hal/r;->b(I)Lcom/glympse/android/core/GLocationProfile;

    move-result-object p1

    .line 159
    :cond_0
    invoke-virtual {p0, p1}, Lcom/glympse/android/hal/s;->a(Lcom/glympse/android/core/GLocationProfile;)Lcom/glympse/android/hal/gms/location/LocationRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/hal/s;->cF:Lcom/glympse/android/hal/gms/location/LocationRequest;

    .line 162
    invoke-virtual {p0}, Lcom/glympse/android/hal/s;->K()V

    .line 163
    return-void
.end method

.method protected c(I)V
    .locals 2

    .prologue
    .line 198
    iget v0, p0, Lcom/glympse/android/hal/s;->cg:I

    if-eq p1, v0, :cond_0

    .line 200
    iput p1, p0, Lcom/glympse/android/hal/s;->cg:I

    .line 201
    iget-object v0, p0, Lcom/glympse/android/hal/s;->ce:Lcom/glympse/android/core/GLocationListener;

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/glympse/android/hal/s;->ce:Lcom/glympse/android/core/GLocationListener;

    iget v1, p0, Lcom/glympse/android/hal/s;->cg:I

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GLocationListener;->stateChanged(I)V

    .line 206
    :cond_0
    return-void
.end method

.method public getLastKnownLocation()Lcom/glympse/android/core/GLocation;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 132
    iget-object v1, p0, Lcom/glympse/android/hal/s;->cE:Lcom/glympse/android/hal/gms/location/LocationClient;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/glympse/android/hal/s;->v:Z

    if-nez v1, :cond_1

    .line 143
    :cond_0
    :goto_0
    return-object v0

    .line 137
    :cond_1
    iget-object v1, p0, Lcom/glympse/android/hal/s;->e:Landroid/content/Context;

    invoke-static {v1}, Lcom/glympse/android/hal/r;->d(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 142
    iget-object v1, p0, Lcom/glympse/android/hal/s;->cE:Lcom/glympse/android/hal/gms/location/LocationClient;

    invoke-virtual {v1}, Lcom/glympse/android/hal/gms/location/LocationClient;->getLastLocation()Landroid/location/Location;

    move-result-object v1

    .line 143
    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/glympse/android/hal/r;->a(Landroid/location/Location;)Lcom/glympse/android/core/GLocation;

    move-result-object v0

    goto :goto_0
.end method

.method public isStarted()Z
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/glympse/android/hal/s;->cE:Lcom/glympse/android/hal/gms/location/LocationClient;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onConnected(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 348
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/glympse/android/hal/s;->v:Z

    .line 350
    iget-object v0, p0, Lcom/glympse/android/hal/s;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/glympse/android/hal/r;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 352
    invoke-virtual {p0, p0}, Lcom/glympse/android/hal/s;->a(Lcom/glympse/android/core/GLocationProvider;)V

    .line 363
    :cond_0
    :goto_0
    return-void

    .line 357
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/hal/s;->cE:Lcom/glympse/android/hal/gms/location/LocationClient;

    iget-object v1, p0, Lcom/glympse/android/hal/s;->cF:Lcom/glympse/android/hal/gms/location/LocationRequest;

    invoke-virtual {v0, v1, p0}, Lcom/glympse/android/hal/gms/location/LocationClient;->requestLocationUpdates(Lcom/glympse/android/hal/gms/location/LocationRequest;Lcom/glympse/android/hal/gms/location/LocationListener;)V

    .line 359
    invoke-direct {p0}, Lcom/glympse/android/hal/s;->L()Z

    move-result v0

    if-nez v0, :cond_0

    .line 361
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/glympse/android/hal/s;->c(I)V

    goto :goto_0
.end method

.method public onConnectionFailed(Lcom/glympse/android/hal/gms/common/ConnectionResult;)V
    .locals 0

    .prologue
    .line 385
    return-void
.end method

.method public onDisconnected()V
    .locals 1

    .prologue
    .line 368
    invoke-virtual {p0}, Lcom/glympse/android/hal/s;->J()V

    .line 370
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/glympse/android/hal/s;->v:Z

    .line 371
    return-void
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 2

    .prologue
    .line 327
    iget-object v0, p0, Lcom/glympse/android/hal/s;->ce:Lcom/glympse/android/core/GLocationListener;

    if-nez v0, :cond_0

    .line 340
    :goto_0
    return-void

    .line 333
    :cond_0
    invoke-static {p1}, Lcom/glympse/android/hal/r;->a(Landroid/location/Location;)Lcom/glympse/android/core/GLocation;

    move-result-object v0

    .line 336
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/glympse/android/hal/s;->c(I)V

    .line 339
    iget-object v1, p0, Lcom/glympse/android/hal/s;->ce:Lcom/glympse/android/core/GLocationListener;

    invoke-interface {v1, v0}, Lcom/glympse/android/core/GLocationListener;->locationChanged(Lcom/glympse/android/core/GLocation;)V

    goto :goto_0
.end method

.method public onNetworkLost()V
    .locals 1

    .prologue
    .line 375
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/glympse/android/hal/s;->v:Z

    .line 376
    return-void
.end method

.method public setLocationListener(Lcom/glympse/android/core/GLocationListener;)V
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lcom/glympse/android/hal/s;->ce:Lcom/glympse/android/core/GLocationListener;

    .line 149
    return-void
.end method

.method public start()V
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/glympse/android/hal/s;->cE:Lcom/glympse/android/hal/gms/location/LocationClient;

    if-nez v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/glympse/android/hal/s;->e:Landroid/content/Context;

    invoke-static {v0, p0, p0}, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->createLocationClient(Landroid/content/Context;Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/glympse/android/hal/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)Lcom/glympse/android/hal/gms/location/LocationClient;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/hal/s;->cE:Lcom/glympse/android/hal/gms/location/LocationClient;

    .line 109
    iget-object v0, p0, Lcom/glympse/android/hal/s;->cE:Lcom/glympse/android/hal/gms/location/LocationClient;

    invoke-virtual {v0}, Lcom/glympse/android/hal/gms/location/LocationClient;->connect()V

    .line 111
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/glympse/android/hal/s;->cE:Lcom/glympse/android/hal/gms/location/LocationClient;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/glympse/android/hal/s;->cE:Lcom/glympse/android/hal/gms/location/LocationClient;

    invoke-virtual {v0}, Lcom/glympse/android/hal/gms/location/LocationClient;->disconnect()V

    .line 120
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/hal/s;->cE:Lcom/glympse/android/hal/gms/location/LocationClient;

    .line 121
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/glympse/android/hal/s;->v:Z

    .line 123
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 393
    const-string v0, "com.glympse.android.hal.LocationProviderFuse"

    return-object v0
.end method
