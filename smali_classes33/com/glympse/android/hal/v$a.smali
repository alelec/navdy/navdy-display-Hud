.class Lcom/glympse/android/hal/v$a;
.super Landroid/content/BroadcastReceiver;
.source "ProximityProviderGms.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/hal/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic dd:Lcom/glympse/android/hal/v;


# direct methods
.method public constructor <init>(Lcom/glympse/android/hal/v;)V
    .locals 0

    .prologue
    .line 161
    iput-object p1, p0, Lcom/glympse/android/hal/v$a;->dd:Lcom/glympse/android/hal/v;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 162
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    .line 169
    :try_start_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.glympse.android.hal.proximity.REGION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/glympse/android/hal/v$a;->dd:Lcom/glympse/android/hal/v;

    invoke-static {v0}, Lcom/glympse/android/hal/v;->a(Lcom/glympse/android/hal/v;)Lcom/glympse/android/core/GProximityListener;

    move-result-object v0

    if-nez v0, :cond_1

    .line 216
    :cond_0
    :goto_0
    return-void

    .line 176
    :cond_1
    invoke-static {p1, p2}, Lcom/glympse/android/hal/gms/common/GooglePlayServicesUtil;->getGeofencingEventFromIntent(Landroid/content/Context;Landroid/content/Intent;)Lcom/glympse/android/hal/gms/location/LocationClient$GeofencingEvent;

    move-result-object v0

    .line 177
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/glympse/android/hal/gms/location/LocationClient$GeofencingEvent;->hasError()Z

    move-result v1

    if-nez v1, :cond_0

    .line 183
    invoke-interface {v0}, Lcom/glympse/android/hal/gms/location/LocationClient$GeofencingEvent;->getGeofenceTransition()I

    move-result v2

    .line 185
    and-int/lit8 v1, v2, 0x3

    if-eqz v1, :cond_0

    .line 190
    invoke-interface {v0}, Lcom/glympse/android/hal/gms/location/LocationClient$GeofencingEvent;->getTriggeringGeofences()Ljava/util/List;

    move-result-object v3

    .line 191
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 193
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/hal/gms/location/Geofence;

    invoke-virtual {v0}, Lcom/glympse/android/hal/gms/location/Geofence;->getRequestId()Ljava/lang/String;

    move-result-object v0

    .line 194
    iget-object v4, p0, Lcom/glympse/android/hal/v$a;->dd:Lcom/glympse/android/hal/v;

    invoke-static {v4}, Lcom/glympse/android/hal/v;->b(Lcom/glympse/android/hal/v;)Lcom/glympse/android/hal/GHashtable;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GRegion;

    .line 195
    if-eqz v0, :cond_2

    .line 201
    const/4 v4, 0x1

    if-ne v4, v2, :cond_3

    .line 203
    iget-object v4, p0, Lcom/glympse/android/hal/v$a;->dd:Lcom/glympse/android/hal/v;

    invoke-static {v4}, Lcom/glympse/android/hal/v;->a(Lcom/glympse/android/hal/v;)Lcom/glympse/android/core/GProximityListener;

    move-result-object v4

    invoke-interface {v4, v0}, Lcom/glympse/android/core/GProximityListener;->regionEntered(Lcom/glympse/android/core/GRegion;)V

    .line 191
    :cond_2
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 205
    :cond_3
    const/4 v4, 0x2

    if-ne v4, v2, :cond_2

    .line 207
    iget-object v4, p0, Lcom/glympse/android/hal/v$a;->dd:Lcom/glympse/android/hal/v;

    invoke-static {v4}, Lcom/glympse/android/hal/v;->a(Lcom/glympse/android/hal/v;)Lcom/glympse/android/core/GProximityListener;

    move-result-object v4

    invoke-interface {v4, v0}, Lcom/glympse/android/core/GProximityListener;->regionLeft(Lcom/glympse/android/core/GRegion;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 213
    :catch_0
    move-exception v0

    goto :goto_0
.end method
