.class Lcom/glympse/android/hal/u;
.super Ljava/lang/Object;
.source "ProximityProvider.java"

# interfaces
.implements Lcom/glympse/android/core/GProximityProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/hal/u$b;,
        Lcom/glympse/android/hal/u$a;
    }
.end annotation


# static fields
.field private static final cU:Ljava/lang/String; = "com.glympse.android.hal.proximity.REGION"


# instance fields
.field private aU:Landroid/os/Handler;

.field private cV:Lcom/glympse/android/core/GProximityListener;

.field private cW:Lcom/glympse/android/hal/GHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GHashtable",
            "<",
            "Lcom/glympse/android/core/GRegion;",
            "Lcom/glympse/android/hal/u$b;",
            ">;"
        }
    .end annotation
.end field

.field private cX:Lcom/glympse/android/hal/u$a;

.field private cY:Lcom/glympse/android/hal/GVector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GVector",
            "<",
            "Lcom/glympse/android/core/GRegion;",
            ">;"
        }
    .end annotation
.end field

.field private cf:Landroid/location/LocationManager;

.field private e:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/glympse/android/hal/u;->e:Landroid/content/Context;

    .line 42
    iget-object v0, p0, Lcom/glympse/android/hal/u;->e:Landroid/content/Context;

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/glympse/android/hal/u;->cf:Landroid/location/LocationManager;

    .line 43
    new-instance v0, Lcom/glympse/android/hal/GHashtable;

    invoke-direct {v0}, Lcom/glympse/android/hal/GHashtable;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/hal/u;->cW:Lcom/glympse/android/hal/GHashtable;

    .line 44
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/hal/u;->aU:Landroid/os/Handler;

    .line 45
    return-void
.end method

.method static synthetic a(Lcom/glympse/android/hal/u;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/glympse/android/hal/u;->e:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(Lcom/glympse/android/hal/u;Lcom/glympse/android/hal/u$a;)Lcom/glympse/android/hal/u$a;
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/glympse/android/hal/u;->cX:Lcom/glympse/android/hal/u$a;

    return-object p1
.end method

.method static synthetic b(Lcom/glympse/android/hal/u;)Lcom/glympse/android/hal/GVector;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/glympse/android/hal/u;->cY:Lcom/glympse/android/hal/GVector;

    return-object v0
.end method

.method static synthetic c(Lcom/glympse/android/hal/u;)Lcom/glympse/android/core/GProximityListener;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/glympse/android/hal/u;->cV:Lcom/glympse/android/core/GProximityListener;

    return-object v0
.end method


# virtual methods
.method protected a(Lcom/glympse/android/core/GRegion;)V
    .locals 2

    .prologue
    .line 183
    iget-object v0, p0, Lcom/glympse/android/hal/u;->cY:Lcom/glympse/android/hal/GVector;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 185
    iget-object v0, p0, Lcom/glympse/android/hal/u;->cY:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GVector;->remove(Ljava/lang/Object;)Z

    .line 189
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/hal/u;->cY:Lcom/glympse/android/hal/GVector;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/glympse/android/hal/u;->cY:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 192
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/hal/u;->cX:Lcom/glympse/android/hal/u$a;

    if-eqz v0, :cond_2

    .line 195
    iget-object v0, p0, Lcom/glympse/android/hal/u;->aU:Landroid/os/Handler;

    iget-object v1, p0, Lcom/glympse/android/hal/u;->cX:Lcom/glympse/android/hal/u$a;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 198
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/hal/u;->cX:Lcom/glympse/android/hal/u$a;

    .line 201
    :cond_2
    return-void
.end method

.method protected a(Lcom/glympse/android/hal/u;Lcom/glympse/android/core/GRegion;)V
    .locals 4

    .prologue
    .line 154
    if-eqz p2, :cond_1

    .line 156
    iget-object v0, p0, Lcom/glympse/android/hal/u;->cY:Lcom/glympse/android/hal/GVector;

    if-nez v0, :cond_0

    .line 158
    new-instance v0, Lcom/glympse/android/hal/GVector;

    invoke-direct {v0}, Lcom/glympse/android/hal/GVector;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/hal/u;->cY:Lcom/glympse/android/hal/GVector;

    .line 160
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/hal/u;->cY:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, p2}, Lcom/glympse/android/hal/GVector;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 162
    iget-object v0, p0, Lcom/glympse/android/hal/u;->cY:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, p2}, Lcom/glympse/android/hal/GVector;->add(Ljava/lang/Object;)Z

    .line 167
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/hal/u;->cX:Lcom/glympse/android/hal/u$a;

    if-nez v0, :cond_2

    .line 170
    new-instance v0, Lcom/glympse/android/hal/u$a;

    invoke-direct {v0, p0, p1}, Lcom/glympse/android/hal/u$a;-><init>(Lcom/glympse/android/hal/u;Lcom/glympse/android/hal/u;)V

    iput-object v0, p0, Lcom/glympse/android/hal/u;->cX:Lcom/glympse/android/hal/u$a;

    .line 173
    iget-object v0, p0, Lcom/glympse/android/hal/u;->aU:Landroid/os/Handler;

    iget-object v1, p0, Lcom/glympse/android/hal/u;->cX:Lcom/glympse/android/hal/u$a;

    const-wide/16 v2, 0x3a98

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    move-result v0

    if-nez v0, :cond_2

    .line 175
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/hal/u;->cX:Lcom/glympse/android/hal/u$a;

    .line 178
    :cond_2
    return-void
.end method

.method public detachRegions()Lcom/glympse/android/core/GArray;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/glympse/android/core/GArray",
            "<",
            "Lcom/glympse/android/core/GRegion;",
            ">;"
        }
    .end annotation

    .prologue
    .line 131
    new-instance v1, Lcom/glympse/android/hal/GVector;

    iget-object v0, p0, Lcom/glympse/android/hal/u;->cW:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GHashtable;->size()I

    move-result v0

    invoke-direct {v1, v0}, Lcom/glympse/android/hal/GVector;-><init>(I)V

    .line 132
    iget-object v0, p0, Lcom/glympse/android/hal/u;->cW:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GHashtable;->keys()Ljava/util/Enumeration;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 134
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    goto :goto_0

    .line 138
    :cond_0
    invoke-virtual {v1}, Lcom/glympse/android/hal/GVector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GRegion;

    .line 140
    invoke-virtual {p0, v0}, Lcom/glympse/android/hal/u;->stopMonitoring(Lcom/glympse/android/core/GRegion;)V

    goto :goto_1

    .line 142
    :cond_1
    return-object v1
.end method

.method public locationChanged(Lcom/glympse/android/core/GLocation;)V
    .locals 0

    .prologue
    .line 126
    return-void
.end method

.method public setProximityListener(Lcom/glympse/android/core/GProximityListener;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/glympse/android/hal/u;->cV:Lcom/glympse/android/core/GProximityListener;

    .line 54
    return-void
.end method

.method public startMonitoring(Lcom/glympse/android/core/GArray;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/glympse/android/core/GArray",
            "<",
            "Lcom/glympse/android/core/GRegion;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 88
    invoke-interface {p1}, Lcom/glympse/android/core/GArray;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GRegion;

    .line 90
    invoke-virtual {p0, v0}, Lcom/glympse/android/hal/u;->startMonitoring(Lcom/glympse/android/core/GRegion;)V

    goto :goto_0

    .line 92
    :cond_0
    return-void
.end method

.method public startMonitoring(Lcom/glympse/android/core/GRegion;)V
    .locals 11

    .prologue
    const/4 v3, 0x0

    .line 58
    iget-object v0, p0, Lcom/glympse/android/hal/u;->cW:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    :goto_0
    return-void

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/hal/u;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/glympse/android/hal/r;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 66
    invoke-virtual {p0, p0, p1}, Lcom/glympse/android/hal/u;->a(Lcom/glympse/android/hal/u;Lcom/glympse/android/core/GRegion;)V

    goto :goto_0

    .line 70
    :cond_1
    const/4 v0, 0x1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ProximityProvider: startMonitoring: Region Count: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/glympse/android/hal/u;->cW:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v2}, Lcom/glympse/android/hal/GHashtable;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/glympse/android/hal/Helpers;->log(ILjava/lang/String;)V

    .line 73
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "com.glympse.android.hal.proximity.REGION_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 74
    iget-object v1, p0, Lcom/glympse/android/hal/u;->e:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v3, v2, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v9

    .line 75
    new-instance v10, Lcom/glympse/android/hal/u$b;

    invoke-direct {v10, p0, v9, p1}, Lcom/glympse/android/hal/u$b;-><init>(Lcom/glympse/android/hal/u;Landroid/app/PendingIntent;Lcom/glympse/android/core/GRegion;)V

    .line 76
    iget-object v1, p0, Lcom/glympse/android/hal/u;->e:Landroid/content/Context;

    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2, v0}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v10, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 80
    iget-object v1, p0, Lcom/glympse/android/hal/u;->cf:Landroid/location/LocationManager;

    invoke-interface {p1}, Lcom/glympse/android/core/GRegion;->getLatitude()D

    move-result-wide v2

    invoke-interface {p1}, Lcom/glympse/android/core/GRegion;->getLongitude()D

    move-result-wide v4

    invoke-interface {p1}, Lcom/glympse/android/core/GRegion;->getRadius()D

    move-result-wide v6

    double-to-float v6, v6

    const-wide/16 v7, -0x1

    invoke-virtual/range {v1 .. v9}, Landroid/location/LocationManager;->addProximityAlert(DDFJLandroid/app/PendingIntent;)V

    .line 83
    iget-object v0, p0, Lcom/glympse/android/hal/u;->cW:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1, v10}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public stopMonitoring(Lcom/glympse/android/core/GRegion;)V
    .locals 4

    .prologue
    .line 96
    invoke-virtual {p0, p1}, Lcom/glympse/android/hal/u;->a(Lcom/glympse/android/core/GRegion;)V

    .line 98
    iget-object v0, p0, Lcom/glympse/android/hal/u;->cW:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/hal/u$b;

    .line 99
    if-nez v0, :cond_1

    .line 120
    :cond_0
    :goto_0
    return-void

    .line 104
    :cond_1
    iget-object v1, p0, Lcom/glympse/android/hal/u;->e:Landroid/content/Context;

    invoke-static {v1}, Lcom/glympse/android/hal/r;->d(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 109
    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ProximityProvider: stopMonitoring: Region Count: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/glympse/android/hal/u;->cW:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v3}, Lcom/glympse/android/hal/GHashtable;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/glympse/android/hal/Helpers;->log(ILjava/lang/String;)V

    .line 113
    iget-object v1, p0, Lcom/glympse/android/hal/u;->cf:Landroid/location/LocationManager;

    invoke-virtual {v0}, Lcom/glympse/android/hal/u$b;->M()Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/location/LocationManager;->removeProximityAlert(Landroid/app/PendingIntent;)V

    .line 116
    iget-object v1, p0, Lcom/glympse/android/hal/u;->e:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 119
    iget-object v0, p0, Lcom/glympse/android/hal/u;->cW:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
