.class Lcom/glympse/android/hal/r$d;
.super Ljava/lang/Object;
.source "LocationProvider.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/hal/r;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "d"
.end annotation


# instance fields
.field final synthetic cB:Lcom/glympse/android/hal/r;

.field protected cD:Z


# direct methods
.method private constructor <init>(Lcom/glympse/android/hal/r;)V
    .locals 1

    .prologue
    .line 540
    iput-object p1, p0, Lcom/glympse/android/hal/r$d;->cB:Lcom/glympse/android/hal/r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 542
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/glympse/android/hal/r$d;->cD:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/glympse/android/hal/r;Lcom/glympse/android/hal/r$1;)V
    .locals 0

    .prologue
    .line 540
    invoke-direct {p0, p1}, Lcom/glympse/android/hal/r$d;-><init>(Lcom/glympse/android/hal/r;)V

    return-void
.end method


# virtual methods
.method protected a(Landroid/location/LocationManager;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 573
    iget-boolean v0, p0, Lcom/glympse/android/hal/r$d;->cD:Z

    if-eqz v0, :cond_0

    .line 578
    :try_start_0
    invoke-virtual {p1, p0}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 584
    :goto_0
    iput-boolean v1, p0, Lcom/glympse/android/hal/r$d;->cD:Z

    .line 585
    iget-object v0, p0, Lcom/glympse/android/hal/r$d;->cB:Lcom/glympse/android/hal/r;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/glympse/android/hal/r;->a(Lcom/glympse/android/hal/r;I)I

    .line 587
    :cond_0
    return-void

    .line 580
    :catch_0
    move-exception v0

    .line 582
    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    goto :goto_0
.end method

.method protected a(Landroid/location/LocationManager;II)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 546
    iget-object v0, p0, Lcom/glympse/android/hal/r$d;->cB:Lcom/glympse/android/hal/r;

    invoke-static {v0}, Lcom/glympse/android/hal/r;->a(Lcom/glympse/android/hal/r;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/glympse/android/hal/r;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v6

    .line 568
    :goto_0
    return v0

    .line 553
    :cond_0
    :try_start_0
    iget-boolean v0, p0, Lcom/glympse/android/hal/r$d;->cD:Z

    if-nez v0, :cond_1

    .line 556
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/glympse/android/hal/r$d;->cD:Z

    .line 560
    const-string v1, "network"

    int-to-long v2, p2

    int-to-float v4, p3

    move-object v0, p1

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 568
    :cond_1
    :goto_1
    iget-boolean v0, p0, Lcom/glympse/android/hal/r$d;->cD:Z

    goto :goto_0

    .line 563
    :catch_0
    move-exception v0

    .line 565
    iput-boolean v6, p0, Lcom/glympse/android/hal/r$d;->cD:Z

    .line 566
    invoke-static {v0, v6}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    goto :goto_1
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 2

    .prologue
    .line 591
    if-eqz p1, :cond_0

    .line 595
    :try_start_0
    iget-object v0, p0, Lcom/glympse/android/hal/r$d;->cB:Lcom/glympse/android/hal/r;

    const-string v1, "NETWORK"

    invoke-virtual {v0, v1, p1}, Lcom/glympse/android/hal/r;->a(Ljava/lang/String;Landroid/location/Location;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 598
    iget-object v0, p0, Lcom/glympse/android/hal/r$d;->cB:Lcom/glympse/android/hal/r;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/r;->d(I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 606
    :cond_0
    :goto_0
    return-void

    .line 601
    :catch_0
    move-exception v0

    .line 603
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    goto :goto_0
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 610
    const/4 v0, 0x3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ProviderNetwork::onProviderDisabled] - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 611
    iget-object v0, p0, Lcom/glympse/android/hal/r$d;->cB:Lcom/glympse/android/hal/r;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/r;->d(I)V

    .line 612
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 616
    const/4 v0, 0x3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ProviderNetwork::onProviderEnabled] - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 617
    iget-object v0, p0, Lcom/glympse/android/hal/r$d;->cB:Lcom/glympse/android/hal/r;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/r;->d(I)V

    .line 618
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x3

    .line 622
    if-nez p2, :cond_0

    .line 624
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ProviderNetwork::onStatusChanged] - OUT_OF_SERVICE, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 625
    iget-object v0, p0, Lcom/glympse/android/hal/r$d;->cB:Lcom/glympse/android/hal/r;

    invoke-virtual {v0, v4}, Lcom/glympse/android/hal/r;->d(I)V

    .line 641
    :goto_0
    return-void

    .line 627
    :cond_0
    if-ne v3, p2, :cond_1

    .line 629
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ProviderNetwork::onStatusChanged] - TEMPORARILY_UNAVAILABLE, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 630
    iget-object v0, p0, Lcom/glympse/android/hal/r$d;->cB:Lcom/glympse/android/hal/r;

    invoke-virtual {v0, v4}, Lcom/glympse/android/hal/r;->d(I)V

    goto :goto_0

    .line 632
    :cond_1
    const/4 v0, 0x2

    if-ne v0, p2, :cond_2

    .line 634
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ProviderNetwork::onStatusChanged] - AVAILABLE, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 635
    iget-object v0, p0, Lcom/glympse/android/hal/r$d;->cB:Lcom/glympse/android/hal/r;

    invoke-virtual {v0, v3}, Lcom/glympse/android/hal/r;->d(I)V

    goto :goto_0

    .line 639
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ProviderNetwork::onStatusChanged] - UNKNOWN("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "), "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    goto :goto_0
.end method
