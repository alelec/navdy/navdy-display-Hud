.class Lcom/glympse/android/hal/ad;
.super Ljava/lang/Object;
.source "UserProfile.java"

# interfaces
.implements Lcom/glympse/android/hal/GUserProfile;


# instance fields
.field private _name:Ljava/lang/String;

.field private dE:Lcom/glympse/android/core/GDrawable;

.field private e:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/glympse/android/hal/ad;->e:Landroid/content/Context;

    .line 34
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/database/Cursor;)Lcom/glympse/android/core/GDrawable;
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x0

    .line 116
    .line 122
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 126
    const-string v0, "photo_thumb_uri"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 127
    if-ltz v0, :cond_2

    .line 129
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 130
    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_9

    move-result v2

    if-nez v2, :cond_2

    .line 134
    :try_start_1
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v2

    .line 145
    :goto_0
    if-nez v2, :cond_0

    .line 147
    :try_start_2
    const-string v0, "photo_uri"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 148
    if-ltz v0, :cond_0

    .line 150
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 151
    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2

    move-result v4

    if-nez v4, :cond_0

    .line 155
    :try_start_3
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    move-result-object v2

    .line 167
    :cond_0
    :goto_1
    if-nez v2, :cond_5

    .line 169
    :try_start_4
    const-string v0, "contact_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 170
    if-ltz v0, :cond_5

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_5

    .line 172
    sget-object v4, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v6, v0

    invoke-static {v4, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2

    move-result-object v0

    .line 175
    :try_start_5
    invoke-static {v3, v0}, Landroid/provider/ContactsContract$Contacts;->openContactPhotoInputStream(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_3

    move-result-object v2

    .line 186
    :goto_2
    if-nez v2, :cond_3

    if-eqz v0, :cond_3

    .line 188
    :try_start_6
    const-string v4, "photo"

    invoke-static {v0, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_2

    move-result-object v0

    .line 191
    :try_start_7
    invoke-virtual {v3, v0}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_4

    move-result-object v2

    move-object v0, v2

    .line 201
    :goto_3
    if-nez v0, :cond_1

    .line 203
    :try_start_8
    const-string v2, "photo_file_id"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 204
    if-ltz v2, :cond_1

    invoke-interface {p1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_1

    .line 206
    sget-object v4, Lcom/glympse/android/hal/android/provider/ContactsContract$DisplayPhoto;->CONTENT_URI:Landroid/net/Uri;

    .line 208
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-long v6, v2

    .line 206
    invoke-static {v4, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_6

    move-result-object v2

    .line 211
    :try_start_9
    invoke-virtual {v3, v2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_5

    move-result-object v0

    :cond_1
    :goto_4
    move-object v2, v0

    .line 226
    :goto_5
    if-eqz v2, :cond_4

    .line 230
    new-instance v3, Ljava/io/BufferedInputStream;

    invoke-direct {v3, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 233
    :try_start_a
    new-instance v4, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-direct {v4, v0, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Ljava/io/InputStream;)V

    .line 234
    new-instance v0, Lcom/glympse/android/hal/j;

    invoke-direct {v0, v4}, Lcom/glympse/android/hal/j;-><init>(Landroid/graphics/drawable/BitmapDrawable;)V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_7

    .line 243
    :goto_6
    :try_start_b
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 244
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_8

    .line 254
    :goto_7
    return-object v0

    .line 136
    :catch_0
    move-exception v0

    .line 138
    const/4 v2, 0x0

    :try_start_c
    invoke-static {v0, v2}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_9

    :cond_2
    move-object v2, v1

    goto/16 :goto_0

    .line 157
    :catch_1
    move-exception v0

    .line 159
    const/4 v4, 0x0

    :try_start_d
    invoke-static {v0, v4}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_2

    goto :goto_1

    .line 220
    :catch_2
    move-exception v0

    .line 222
    :goto_8
    invoke-static {v0, v8}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    goto :goto_5

    .line 177
    :catch_3
    move-exception v4

    .line 179
    const/4 v5, 0x0

    :try_start_e
    invoke-static {v4, v5}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    goto :goto_2

    .line 193
    :catch_4
    move-exception v0

    .line 195
    const/4 v4, 0x1

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V
    :try_end_e
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_e} :catch_2

    :cond_3
    move-object v0, v2

    goto :goto_3

    .line 213
    :catch_5
    move-exception v2

    .line 215
    const/4 v3, 0x1

    :try_start_f
    invoke-virtual {v2}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V
    :try_end_f
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_6

    goto :goto_4

    .line 220
    :catch_6
    move-exception v2

    move-object v9, v2

    move-object v2, v0

    move-object v0, v9

    goto :goto_8

    .line 236
    :catch_7
    move-exception v0

    .line 238
    invoke-static {v0, v8}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    move-object v0, v1

    goto :goto_6

    .line 246
    :catch_8
    move-exception v1

    .line 248
    invoke-static {v1, v8}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    goto :goto_7

    :cond_4
    move-object v0, v1

    .line 254
    goto :goto_7

    .line 220
    :catch_9
    move-exception v0

    move-object v2, v1

    goto :goto_8

    :cond_5
    move-object v0, v1

    goto/16 :goto_2
.end method

.method public static a(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 259
    const-string v0, "display_name"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 260
    if-ltz v0, :cond_0

    .line 262
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 263
    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 269
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/net/Uri;)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 73
    :try_start_0
    iget-object v0, p0, Lcom/glympse/android/hal/ad;->e:Landroid/content/Context;

    const-string v1, "android.permission.READ_CONTACTS"

    invoke-static {v0, v1}, Lcom/glympse/android/hal/h;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v6

    .line 107
    :goto_0
    return v0

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/hal/ad;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 82
    if-nez v0, :cond_1

    move v0, v6

    .line 84
    goto :goto_0

    .line 88
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 93
    :try_start_1
    invoke-static {v0}, Lcom/glympse/android/hal/ad;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/glympse/android/hal/ad;->_name:Ljava/lang/String;

    .line 94
    iget-object v1, p0, Lcom/glympse/android/hal/ad;->e:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/glympse/android/hal/ad;->a(Landroid/content/Context;Landroid/database/Cursor;)Lcom/glympse/android/core/GDrawable;

    move-result-object v1

    iput-object v1, p0, Lcom/glympse/android/hal/ad;->dE:Lcom/glympse/android/core/GDrawable;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 101
    :goto_1
    :try_start_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 103
    const/4 v0, 0x1

    goto :goto_0

    .line 105
    :catch_0
    move-exception v0

    move v0, v6

    .line 107
    goto :goto_0

    .line 96
    :catch_1
    move-exception v1

    goto :goto_1
.end method


# virtual methods
.method public fetch()Z
    .locals 1

    .prologue
    .line 43
    invoke-static {}, Lcom/glympse/android/hal/android/provider/ContactsContract;->init()V

    .line 44
    sget-object v0, Lcom/glympse/android/hal/android/provider/ContactsContract$Profile;->CONTENT_URI:Landroid/net/Uri;

    .line 45
    if-nez v0, :cond_0

    .line 47
    const/4 v0, 0x0

    .line 51
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, v0}, Lcom/glympse/android/hal/ad;->a(Landroid/net/Uri;)Z

    move-result v0

    goto :goto_0
.end method

.method public getAvatar()Lcom/glympse/android/core/GDrawable;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/glympse/android/hal/ad;->dE:Lcom/glympse/android/core/GDrawable;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/glympse/android/hal/ad;->_name:Ljava/lang/String;

    return-object v0
.end method
