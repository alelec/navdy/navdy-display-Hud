.class Lcom/glympse/android/hal/r$c;
.super Ljava/lang/Object;
.source "LocationProvider.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/hal/r;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field final synthetic cB:Lcom/glympse/android/hal/r;

.field protected cD:Z


# direct methods
.method private constructor <init>(Lcom/glympse/android/hal/r;)V
    .locals 1

    .prologue
    .line 647
    iput-object p1, p0, Lcom/glympse/android/hal/r$c;->cB:Lcom/glympse/android/hal/r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 649
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/glympse/android/hal/r$c;->cD:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/glympse/android/hal/r;Lcom/glympse/android/hal/r$1;)V
    .locals 0

    .prologue
    .line 647
    invoke-direct {p0, p1}, Lcom/glympse/android/hal/r$c;-><init>(Lcom/glympse/android/hal/r;)V

    return-void
.end method


# virtual methods
.method protected a(Landroid/location/LocationManager;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 681
    iget-boolean v0, p0, Lcom/glympse/android/hal/r$c;->cD:Z

    if-eqz v0, :cond_0

    .line 686
    :try_start_0
    invoke-virtual {p1, p0}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 688
    const/4 v0, 0x3

    const-string v1, "[ProviderGps::stop] Updates removed"

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 694
    :goto_0
    iput-boolean v2, p0, Lcom/glympse/android/hal/r$c;->cD:Z

    .line 695
    iget-object v0, p0, Lcom/glympse/android/hal/r$c;->cB:Lcom/glympse/android/hal/r;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/glympse/android/hal/r;->b(Lcom/glympse/android/hal/r;I)I

    .line 699
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/hal/r$c;->cB:Lcom/glympse/android/hal/r;

    invoke-virtual {v0}, Lcom/glympse/android/hal/r;->I()V

    .line 700
    iget-object v0, p0, Lcom/glympse/android/hal/r$c;->cB:Lcom/glympse/android/hal/r;

    invoke-static {v0, v2}, Lcom/glympse/android/hal/r;->a(Lcom/glympse/android/hal/r;Z)Z

    .line 701
    return-void

    .line 690
    :catch_0
    move-exception v0

    .line 692
    invoke-static {v0, v2}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    goto :goto_0
.end method

.method protected a(Landroid/location/LocationManager;JF)Z
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 655
    :try_start_0
    iget-boolean v0, p0, Lcom/glympse/android/hal/r$c;->cD:Z

    if-nez v0, :cond_1

    .line 657
    iget-object v0, p0, Lcom/glympse/android/hal/r$c;->cB:Lcom/glympse/android/hal/r;

    invoke-static {v0}, Lcom/glympse/android/hal/r;->a(Lcom/glympse/android/hal/r;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/glympse/android/hal/r;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v6

    .line 676
    :goto_0
    return v0

    .line 663
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/glympse/android/hal/r$c;->cD:Z

    .line 667
    const-string v1, "gps"

    move-object v0, p1

    move-wide v2, p2

    move v4, p4

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 676
    :cond_1
    :goto_1
    iget-boolean v0, p0, Lcom/glympse/android/hal/r$c;->cD:Z

    goto :goto_0

    .line 670
    :catch_0
    move-exception v0

    .line 672
    iput-boolean v6, p0, Lcom/glympse/android/hal/r$c;->cD:Z

    .line 673
    invoke-static {v0, v6}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    goto :goto_1
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 2

    .prologue
    .line 705
    if-eqz p1, :cond_0

    .line 709
    :try_start_0
    iget-object v0, p0, Lcom/glympse/android/hal/r$c;->cB:Lcom/glympse/android/hal/r;

    const-string v1, "GPS"

    invoke-virtual {v0, v1, p1}, Lcom/glympse/android/hal/r;->a(Ljava/lang/String;Landroid/location/Location;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 712
    iget-object v0, p0, Lcom/glympse/android/hal/r$c;->cB:Lcom/glympse/android/hal/r;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/r;->e(I)V

    .line 715
    iget-object v0, p0, Lcom/glympse/android/hal/r$c;->cB:Lcom/glympse/android/hal/r;

    invoke-virtual {v0}, Lcom/glympse/android/hal/r;->I()V

    .line 718
    iget-object v0, p0, Lcom/glympse/android/hal/r$c;->cB:Lcom/glympse/android/hal/r;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/glympse/android/hal/r;->a(Lcom/glympse/android/hal/r;Z)Z

    .line 721
    iget-object v0, p0, Lcom/glympse/android/hal/r$c;->cB:Lcom/glympse/android/hal/r;

    invoke-virtual {v0}, Lcom/glympse/android/hal/r;->C()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 729
    :cond_0
    :goto_0
    return-void

    .line 724
    :catch_0
    move-exception v0

    .line 726
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    goto :goto_0
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 733
    const/4 v0, 0x3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ProviderGps::onProviderDisabled] - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 735
    iget-boolean v0, p0, Lcom/glympse/android/hal/r$c;->cD:Z

    if-eqz v0, :cond_0

    .line 737
    iget-object v0, p0, Lcom/glympse/android/hal/r$c;->cB:Lcom/glympse/android/hal/r;

    invoke-virtual {v0}, Lcom/glympse/android/hal/r;->H()V

    .line 740
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/hal/r$c;->cB:Lcom/glympse/android/hal/r;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/r;->e(I)V

    .line 741
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 745
    const/4 v0, 0x3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ProviderGps::onProviderEnabled] - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 746
    iget-object v0, p0, Lcom/glympse/android/hal/r$c;->cB:Lcom/glympse/android/hal/r;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/r;->e(I)V

    .line 747
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x3

    .line 752
    if-nez p2, :cond_1

    .line 754
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ProviderGps::onStatusChanged] - OUT_OF_SERVICE, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 755
    iget-object v0, p0, Lcom/glympse/android/hal/r$c;->cB:Lcom/glympse/android/hal/r;

    invoke-virtual {v0, v5}, Lcom/glympse/android/hal/r;->e(I)V

    .line 776
    :goto_0
    if-eq v4, p2, :cond_0

    .line 778
    const-string v0, "[ProviderGps::onStatusChanged] Not AVAILABLE"

    invoke-static {v2, v0}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 780
    iget-boolean v0, p0, Lcom/glympse/android/hal/r$c;->cD:Z

    if-eqz v0, :cond_0

    .line 788
    iget-object v0, p0, Lcom/glympse/android/hal/r$c;->cB:Lcom/glympse/android/hal/r;

    invoke-virtual {v0}, Lcom/glympse/android/hal/r;->H()V

    .line 791
    :cond_0
    return-void

    .line 759
    :cond_1
    if-ne v3, p2, :cond_2

    .line 761
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ProviderGps::onStatusChanged] - TEMPORARILY_UNAVAILABLE, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 762
    iget-object v0, p0, Lcom/glympse/android/hal/r$c;->cB:Lcom/glympse/android/hal/r;

    invoke-virtual {v0, v5}, Lcom/glympse/android/hal/r;->e(I)V

    goto :goto_0

    .line 766
    :cond_2
    if-ne v4, p2, :cond_3

    .line 768
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ProviderGps::onStatusChanged] - AVAILABLE, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 769
    iget-object v0, p0, Lcom/glympse/android/hal/r$c;->cB:Lcom/glympse/android/hal/r;

    invoke-virtual {v0, v3}, Lcom/glympse/android/hal/r;->e(I)V

    goto :goto_0

    .line 773
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[ProviderGps::onStatusChanged] - UNKNOWN("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "), "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    goto :goto_0
.end method
