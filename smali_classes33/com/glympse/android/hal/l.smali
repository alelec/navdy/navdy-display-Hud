.class Lcom/glympse/android/hal/l;
.super Ljava/lang/Object;
.source "GlympseHttpConnection.java"

# interfaces
.implements Lcom/glympse/android/hal/GHttpConnection;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/hal/l$a;
    }
.end annotation


# instance fields
.field private bk:Ljava/net/HttpURLConnection;

.field private bl:I

.field private bm:I

.field private bn:Ljava/lang/String;

.field private bo:[B

.field private bp:[B

.field private bq:Z

.field private br:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 50
    :try_start_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-ge v0, v1, :cond_0

    .line 52
    const-string v0, "http.keepAlive"

    const-string v1, "false"

    invoke-static {v0, v1}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    :cond_0
    :goto_0
    return-void

    .line 55
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object v1, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    .line 63
    iput-object v1, p0, Lcom/glympse/android/hal/l;->bn:Ljava/lang/String;

    .line 64
    const/4 v0, 0x1

    iput v0, p0, Lcom/glympse/android/hal/l;->bl:I

    .line 65
    iput-object v1, p0, Lcom/glympse/android/hal/l;->bp:[B

    .line 66
    iput-object v1, p0, Lcom/glympse/android/hal/l;->br:Ljava/lang/String;

    .line 67
    return-void
.end method

.method public static a(Ljava/net/URL;Ljava/net/HttpURLConnection;)V
    .locals 2

    .prologue
    .line 625
    invoke-virtual {p0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    const-string v1, "https"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 627
    check-cast p1, Ljavax/net/ssl/HttpsURLConnection;

    .line 628
    new-instance v0, Lorg/apache/http/conn/ssl/BrowserCompatHostnameVerifier;

    invoke-direct {v0}, Lorg/apache/http/conn/ssl/BrowserCompatHostnameVerifier;-><init>()V

    invoke-virtual {p1, v0}, Ljavax/net/ssl/HttpsURLConnection;->setHostnameVerifier(Ljavax/net/ssl/HostnameVerifier;)V

    .line 630
    :cond_0
    return-void
.end method

.method private p()Ljava/io/OutputStream;
    .locals 2

    .prologue
    .line 455
    iget-object v0, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    if-eqz v0, :cond_0

    .line 459
    :try_start_0
    iget-object v0, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 466
    :goto_0
    return-object v0

    .line 461
    :catch_0
    move-exception v0

    .line 463
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    .line 466
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private q()Ljava/io/InputStream;
    .locals 5

    .prologue
    const/4 v2, 0x4

    const/4 v4, 0x0

    .line 471
    const/4 v0, 0x0

    .line 473
    iget-object v1, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    if-eqz v1, :cond_1

    .line 481
    :try_start_0
    iget-object v1, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 492
    :goto_0
    if-nez v1, :cond_0

    .line 494
    const-string v0, "Failed to get input stream.  Attempting to get error stream."

    invoke-static {v2, v0}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 497
    :try_start_1
    iget-object v0, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getErrorStream()Ljava/io/InputStream;

    move-result-object v1

    .line 498
    const/4 v2, 0x4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error stream: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez v1, :cond_2

    const-string v0, "null"

    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 509
    :cond_0
    :goto_2
    :try_start_2
    iget-object v0, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    const-string v2, "Content-Encoding"

    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 510
    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "gzip"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 512
    new-instance v0, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v0, v1}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2

    .line 521
    :cond_1
    :goto_3
    return-object v0

    .line 483
    :catch_0
    move-exception v1

    .line 485
    invoke-static {v1, v4}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    move-object v1, v0

    goto :goto_0

    .line 498
    :cond_2
    :try_start_3
    const-string v0, "not null"
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 500
    :catch_1
    move-exception v0

    .line 502
    invoke-static {v0, v4}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    goto :goto_2

    .line 515
    :catch_2
    move-exception v0

    .line 517
    invoke-static {v0, v4}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    :cond_3
    move-object v0, v1

    goto :goto_3
.end method

.method private r()Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 526
    iget-object v2, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/glympse/android/hal/l;->bq:Z

    if-eqz v2, :cond_2

    :cond_0
    move v0, v1

    .line 618
    :cond_1
    :goto_0
    return v0

    .line 530
    :cond_2
    iput-boolean v0, p0, Lcom/glympse/android/hal/l;->bq:Z

    .line 532
    const/4 v2, 0x0

    .line 538
    :try_start_0
    iget-object v3, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v3

    .line 539
    const/4 v4, 0x1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Response content-length: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 542
    if-nez v3, :cond_3

    .line 604
    if-eqz v2, :cond_1

    .line 608
    :try_start_1
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 610
    :catch_0
    move-exception v2

    .line 612
    invoke-static {v2, v1}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    goto :goto_0

    .line 548
    :cond_3
    :try_start_2
    invoke-direct {p0}, Lcom/glympse/android/hal/l;->q()Ljava/io/InputStream;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    .line 549
    if-nez v2, :cond_5

    .line 604
    if-eqz v2, :cond_4

    .line 608
    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    :cond_4
    :goto_1
    move v0, v1

    .line 551
    goto :goto_0

    .line 610
    :catch_1
    move-exception v0

    .line 612
    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    goto :goto_1

    .line 566
    :cond_5
    :try_start_4
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 569
    const/16 v5, 0x200

    if-gt v3, v5, :cond_6

    const/16 v3, 0x400

    .line 572
    :goto_2
    new-array v3, v3, [B

    .line 578
    :goto_3
    const/4 v5, 0x0

    array-length v6, v3

    invoke-virtual {v2, v3, v5, v6}, Ljava/io/InputStream;->read([BII)I

    move-result v5

    .line 582
    if-gez v5, :cond_7

    .line 592
    const/4 v3, 0x1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Response bytes read: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 593
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    iput-object v3, p0, Lcom/glympse/android/hal/l;->bp:[B
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 604
    if-eqz v2, :cond_1

    .line 608
    :try_start_5
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    .line 610
    :catch_2
    move-exception v2

    .line 612
    invoke-static {v2, v1}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    goto :goto_0

    .line 569
    :cond_6
    const/16 v5, 0x2000

    mul-int/lit8 v3, v3, 0x2

    :try_start_6
    invoke-static {v5, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    goto :goto_2

    .line 588
    :cond_7
    const/4 v6, 0x0

    invoke-virtual {v4, v3, v6, v5}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_3

    .line 596
    :catch_3
    move-exception v0

    .line 598
    const/4 v3, 0x0

    :try_start_7
    invoke-static {v0, v3}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 604
    if-eqz v2, :cond_8

    .line 608
    :try_start_8
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_4

    :cond_8
    :goto_4
    move v0, v1

    .line 599
    goto/16 :goto_0

    .line 610
    :catch_4
    move-exception v0

    .line 612
    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    goto :goto_4

    .line 604
    :catchall_0
    move-exception v0

    if-eqz v2, :cond_9

    .line 608
    :try_start_9
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_5

    .line 614
    :cond_9
    :goto_5
    throw v0

    .line 610
    :catch_5
    move-exception v2

    .line 612
    invoke-static {v2, v1}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    goto :goto_5
.end method


# virtual methods
.method public close()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 416
    iget-object v0, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    if-eqz v0, :cond_0

    .line 420
    :try_start_0
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_1

    .line 422
    invoke-static {}, Lcom/glympse/android/hal/GlympseThreadPool;->instance()Lcom/glympse/android/hal/GlympseThreadPool;

    move-result-object v0

    new-instance v1, Lcom/glympse/android/hal/l$a;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/glympse/android/hal/l$a;-><init>(Lcom/glympse/android/hal/l;Lcom/glympse/android/hal/l$1;)V

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GlympseThreadPool;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 439
    :cond_0
    :goto_0
    return-void

    .line 426
    :catch_0
    move-exception v0

    .line 432
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 437
    :goto_1
    iput-object v3, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    goto :goto_0

    .line 434
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public establish()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 240
    iget-object v0, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    if-nez v0, :cond_1

    .line 294
    :cond_0
    :goto_0
    return-void

    .line 246
    :cond_1
    invoke-static {v1}, Lcom/glympse/android/hal/DebugBase;->logMemory(Landroid/content/Context;)V

    .line 249
    iget v0, p0, Lcom/glympse/android/hal/l;->bm:I

    if-lez v0, :cond_0

    .line 256
    :try_start_0
    iget-object v0, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    if-eqz v0, :cond_2

    .line 258
    iget-object v0, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    iget v2, p0, Lcom/glympse/android/hal/l;->bm:I

    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V

    .line 262
    :cond_2
    invoke-direct {p0}, Lcom/glympse/android/hal/l;->p()Ljava/io/OutputStream;

    move-result-object v1

    .line 263
    if-eqz v1, :cond_3

    .line 266
    iget-object v0, p0, Lcom/glympse/android/hal/l;->bo:[B

    const/4 v2, 0x0

    iget v3, p0, Lcom/glympse/android/hal/l;->bm:I

    invoke-virtual {v1, v0, v2, v3}, Ljava/io/OutputStream;->write([BII)V

    .line 269
    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 280
    :cond_3
    if-eqz v1, :cond_0

    .line 284
    :try_start_1
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 286
    :catch_0
    move-exception v0

    .line 288
    invoke-static {v0, v4}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    goto :goto_0

    .line 273
    :catch_1
    move-exception v0

    .line 275
    const/4 v2, 0x0

    :try_start_2
    invoke-static {v0, v2}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 280
    if-eqz v1, :cond_0

    .line 284
    :try_start_3
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 286
    :catch_2
    move-exception v0

    .line 288
    invoke-static {v0, v4}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    goto :goto_0

    .line 280
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_4

    .line 284
    :try_start_4
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_3

    .line 290
    :cond_4
    :goto_1
    throw v0

    .line 286
    :catch_3
    move-exception v1

    .line 288
    invoke-static {v1, v4}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    goto :goto_1
.end method

.method public getCookie(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 365
    invoke-static {}, Lcom/glympse/android/hal/Helpers;->logTODO()V

    .line 366
    const/4 v0, 0x0

    return-object v0
.end method

.method public getResponseCode()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 298
    const/4 v2, 0x0

    .line 300
    iget-object v0, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    if-eqz v0, :cond_1

    .line 307
    :try_start_0
    iget-object v0, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 345
    :cond_0
    :goto_0
    return v0

    .line 309
    :catch_0
    move-exception v0

    move-object v2, v0

    .line 318
    :try_start_1
    iget-object v0, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    const-string v3, "status"

    invoke-virtual {v0, v3}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 319
    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 321
    const/16 v3, 0x20

    invoke-static {v0, v3}, Lcom/glympse/android/hal/Helpers;->strSplit(Ljava/lang/String;C)Ljava/util/Vector;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    .line 322
    if-gtz v0, :cond_0

    .line 334
    :cond_1
    :goto_1
    if-eqz v2, :cond_3

    .line 336
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v3, Ljava/io/IOException;

    invoke-virtual {v0, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    const-string v3, "Received authentication challenge is null"

    invoke-static {v0, v3}, Lcom/glympse/android/hal/Helpers;->safeEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 338
    const/16 v0, 0x191

    goto :goto_0

    .line 328
    :catch_1
    move-exception v0

    .line 330
    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    goto :goto_1

    .line 342
    :cond_2
    const/4 v0, 0x4

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "GlympseHttpConnection.getResponseCode() threw "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    :cond_3
    move v0, v1

    .line 345
    goto :goto_0
.end method

.method public getResponseData()[B
    .locals 1

    .prologue
    .line 386
    iget-boolean v0, p0, Lcom/glympse/android/hal/l;->bq:Z

    if-nez v0, :cond_0

    .line 388
    invoke-direct {p0}, Lcom/glympse/android/hal/l;->r()Z

    .line 390
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/hal/l;->bp:[B

    return-object v0
.end method

.method public getResponseDataLength()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 397
    :try_start_0
    invoke-virtual {p0}, Lcom/glympse/android/hal/l;->getResponseData()[B

    move-result-object v1

    .line 398
    if-eqz v1, :cond_1

    .line 400
    array-length v0, v1

    .line 411
    :cond_0
    :goto_0
    return v0

    .line 402
    :cond_1
    iget-object v1, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    if-eqz v1, :cond_0

    .line 404
    iget-object v1, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getContentLength()I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 407
    :catch_0
    move-exception v1

    .line 409
    invoke-static {v1, v0}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    goto :goto_0
.end method

.method public getResponseDataString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 371
    iget-object v0, p0, Lcom/glympse/android/hal/l;->br:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 373
    iget-object v0, p0, Lcom/glympse/android/hal/l;->br:Ljava/lang/String;

    .line 381
    :goto_0
    return-object v0

    .line 375
    :cond_0
    invoke-virtual {p0}, Lcom/glympse/android/hal/l;->getResponseData()[B

    move-result-object v0

    .line 376
    if-eqz v0, :cond_1

    .line 378
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V

    iput-object v1, p0, Lcom/glympse/android/hal/l;->br:Ljava/lang/String;

    .line 379
    iget-object v0, p0, Lcom/glympse/android/hal/l;->br:Ljava/lang/String;

    goto :goto_0

    .line 381
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getResponseHeader(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 352
    :try_start_0
    iget-object v0, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    if-eqz v0, :cond_0

    .line 354
    iget-object v0, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, p1}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 360
    :goto_0
    return-object v0

    .line 357
    :catch_0
    move-exception v0

    .line 360
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/glympse/android/hal/l;->bn:Ljava/lang/String;

    return-object v0
.end method

.method public methodType()I
    .locals 1

    .prologue
    .line 234
    iget v0, p0, Lcom/glympse/android/hal/l;->bl:I

    return v0
.end method

.method public setConnectTimeout(I)V
    .locals 1

    .prologue
    .line 115
    :try_start_0
    iget-object v0, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, p1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 123
    :cond_0
    :goto_0
    return-void

    .line 120
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setCookie(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 204
    invoke-static {}, Lcom/glympse/android/hal/Helpers;->logTODO()V

    .line 205
    return-void
.end method

.method public setFollowRedirects(Z)V
    .locals 1

    .prologue
    .line 143
    :try_start_0
    iget-object v0, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, p1}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 151
    :cond_0
    :goto_0
    return-void

    .line 148
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setReadTimeout(I)V
    .locals 1

    .prologue
    .line 129
    :try_start_0
    iget-object v0, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, p1}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    :cond_0
    :goto_0
    return-void

    .line 134
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setRequestData(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 209
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/hal/l;->bo:[B

    .line 210
    iput v1, p0, Lcom/glympse/android/hal/l;->bm:I

    .line 212
    if-eqz p1, :cond_0

    .line 216
    :try_start_0
    const-string v0, "UTF-8"

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/hal/l;->bo:[B

    .line 217
    iget-object v0, p0, Lcom/glympse/android/hal/l;->bo:[B

    array-length v0, v0

    iput v0, p0, Lcom/glympse/android/hal/l;->bm:I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 224
    :cond_0
    :goto_0
    return-void

    .line 219
    :catch_0
    move-exception v0

    .line 221
    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    goto :goto_0
.end method

.method public setRequestData([BI)V
    .locals 0

    .prologue
    .line 228
    iput-object p1, p0, Lcom/glympse/android/hal/l;->bo:[B

    .line 229
    iput p2, p0, Lcom/glympse/android/hal/l;->bm:I

    .line 230
    return-void
.end method

.method public setRequestHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 192
    :try_start_0
    iget-object v0, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, p1, p2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 200
    :cond_0
    :goto_0
    return-void

    .line 197
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setRequestMethod(I)V
    .locals 2

    .prologue
    .line 157
    :try_start_0
    iget-object v0, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    if-eqz v0, :cond_0

    .line 159
    iput p1, p0, Lcom/glympse/android/hal/l;->bl:I

    .line 160
    iget-object v0, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 161
    const/4 v0, 0x2

    if-ne v0, p1, :cond_1

    .line 163
    iget-object v0, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 164
    iget-object v0, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    const-string v1, "POST"

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 186
    :cond_0
    :goto_0
    return-void

    .line 166
    :cond_1
    const/4 v0, 0x3

    if-ne v0, p1, :cond_2

    .line 168
    iget-object v0, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 169
    iget-object v0, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    const-string v1, "PUT"

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    goto :goto_0

    .line 183
    :catch_0
    move-exception v0

    goto :goto_0

    .line 171
    :cond_2
    const/4 v0, 0x4

    if-ne v0, p1, :cond_3

    .line 173
    iget-object v0, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 174
    iget-object v0, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    const-string v1, "DELETE"

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    goto :goto_0

    .line 178
    :cond_3
    iget-object v0, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 179
    iget-object v0, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    const-string v1, "GET"

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0
.end method

.method public setUrl(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 73
    const/4 v0, 0x1

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "URL: \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 76
    invoke-static {}, Lcom/glympse/android/hal/ConnectivityChecker;->isConnected()Z

    move-result v0

    if-nez v0, :cond_1

    .line 78
    const/4 v0, 0x1

    const-string v1, "[GlympseHttpConnection.setUrl] Not connected"

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 104
    :cond_0
    :goto_0
    return-void

    .line 83
    :cond_1
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 84
    invoke-virtual {v1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    iput-object v0, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    .line 86
    iget-object v0, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    if-eqz v0, :cond_2

    .line 89
    iget-object v0, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    invoke-static {v1, v0}, Lcom/glympse/android/hal/l;->a(Ljava/net/URL;Ljava/net/HttpURLConnection;)V

    .line 92
    :cond_2
    iput-object p1, p0, Lcom/glympse/android/hal/l;->bn:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    :goto_1
    iget-object v0, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    if-nez v0, :cond_0

    .line 102
    const/4 v0, 0x5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to open URL: \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    goto :goto_0

    .line 95
    :catch_0
    move-exception v0

    .line 97
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/hal/l;->bk:Ljava/net/HttpURLConnection;

    goto :goto_1
.end method
