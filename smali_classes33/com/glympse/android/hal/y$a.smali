.class Lcom/glympse/android/hal/y$a;
.super Landroid/content/BroadcastReceiver;
.source "ServiceWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/hal/y;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field private dk:J

.field private dl:J

.field final synthetic dm:Lcom/glympse/android/hal/y;

.field private e:Landroid/content/Context;


# direct methods
.method private constructor <init>(Lcom/glympse/android/hal/y;)V
    .locals 2

    .prologue
    const-wide/16 v0, -0x1

    .line 150
    iput-object p1, p0, Lcom/glympse/android/hal/y$a;->dm:Lcom/glympse/android/hal/y;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 154
    iput-wide v0, p0, Lcom/glympse/android/hal/y$a;->dk:J

    .line 155
    iput-wide v0, p0, Lcom/glympse/android/hal/y$a;->dl:J

    return-void
.end method

.method synthetic constructor <init>(Lcom/glympse/android/hal/y;Lcom/glympse/android/hal/y$1;)V
    .locals 0

    .prologue
    .line 150
    invoke-direct {p0, p1}, Lcom/glympse/android/hal/y$a;-><init>(Lcom/glympse/android/hal/y;)V

    return-void
.end method

.method private T()Z
    .locals 8

    .prologue
    .line 201
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 202
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 207
    iget-wide v0, p0, Lcom/glympse/android/hal/y$a;->dk:J

    sub-long v0, v2, v0

    .line 208
    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    iget-wide v6, p0, Lcom/glympse/android/hal/y$a;->dl:J

    sub-long v6, v4, v6

    .line 209
    invoke-static {v6, v7}, Ljava/lang/Math;->abs(J)J

    move-result-wide v6

    sub-long/2addr v0, v6

    .line 207
    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    .line 212
    const-wide/32 v6, 0x493e0

    cmp-long v0, v0, v6

    if-lez v0, :cond_0

    const/4 v0, 0x1

    .line 214
    :goto_0
    iput-wide v2, p0, Lcom/glympse/android/hal/y$a;->dk:J

    .line 215
    iput-wide v4, p0, Lcom/glympse/android/hal/y$a;->dl:J

    .line 217
    return v0

    .line 212
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public h(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 159
    iput-object p1, p0, Lcom/glympse/android/hal/y$a;->e:Landroid/content/Context;

    .line 161
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/glympse/android/hal/y$a;->dk:J

    .line 162
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/glympse/android/hal/y$a;->dl:J

    .line 164
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.TIME_SET"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 165
    iget-object v1, p0, Lcom/glympse/android/hal/y$a;->e:Landroid/content/Context;

    invoke-virtual {v1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 166
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 175
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 176
    const-string v1, "android.intent.action.TIME_SET"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 178
    invoke-static {}, Lcom/glympse/android/hal/GlympseService;->isTimeSynchronizationEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/glympse/android/hal/GlympseService;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-nez v0, :cond_1

    .line 197
    :cond_0
    :goto_0
    return-void

    .line 184
    :cond_1
    invoke-direct {p0}, Lcom/glympse/android/hal/y$a;->T()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    sget-object v0, Lcom/glympse/android/hal/GlympseService;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getCorrectedTime()Lcom/glympse/android/lib/GCorrectedTime;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/lib/GCorrectedTime;->flushBiasSetFlag()V

    .line 195
    sget-object v0, Lcom/glympse/android/hal/GlympseService;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getServerPost()Lcom/glympse/android/lib/GServerPost;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/lib/GServerPost;->doPost()V

    goto :goto_0
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/glympse/android/hal/y$a;->e:Landroid/content/Context;

    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 171
    return-void
.end method
