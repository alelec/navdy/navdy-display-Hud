.class Lcom/glympse/android/ui/NotificationProvider$a$1;
.super Landroid/os/CountDownTimer;
.source "NotificationProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/glympse/android/ui/NotificationProvider$a;->a(JJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic wO:Lcom/glympse/android/ui/NotificationProvider$a;


# direct methods
.method constructor <init>(Lcom/glympse/android/ui/NotificationProvider$a;JJ)V
    .locals 0

    .prologue
    .line 612
    iput-object p1, p0, Lcom/glympse/android/ui/NotificationProvider$a$1;->wO:Lcom/glympse/android/ui/NotificationProvider$a;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 1

    .prologue
    .line 626
    iget-object v0, p0, Lcom/glympse/android/ui/NotificationProvider$a$1;->wO:Lcom/glympse/android/ui/NotificationProvider$a;

    iget-object v0, v0, Lcom/glympse/android/ui/NotificationProvider$a;->wN:Lcom/glympse/android/ui/NotificationProvider;

    invoke-static {v0}, Lcom/glympse/android/ui/NotificationProvider;->e(Lcom/glympse/android/ui/NotificationProvider;)Lcom/glympse/android/ui/NotificationProvider$a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 628
    iget-object v0, p0, Lcom/glympse/android/ui/NotificationProvider$a$1;->wO:Lcom/glympse/android/ui/NotificationProvider$a;

    iget-object v0, v0, Lcom/glympse/android/ui/NotificationProvider$a;->wN:Lcom/glympse/android/ui/NotificationProvider;

    invoke-static {v0}, Lcom/glympse/android/ui/NotificationProvider;->e(Lcom/glympse/android/ui/NotificationProvider;)Lcom/glympse/android/ui/NotificationProvider$a;

    move-result-object v0

    invoke-static {v0}, Lcom/glympse/android/ui/NotificationProvider$a;->b(Lcom/glympse/android/ui/NotificationProvider$a;)V

    .line 630
    :cond_0
    return-void
.end method

.method public onTick(J)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 615
    iget-object v0, p0, Lcom/glympse/android/ui/NotificationProvider$a$1;->wO:Lcom/glympse/android/ui/NotificationProvider$a;

    iget-object v0, v0, Lcom/glympse/android/ui/NotificationProvider$a;->wN:Lcom/glympse/android/ui/NotificationProvider;

    invoke-static {v0}, Lcom/glympse/android/ui/NotificationProvider;->b(Lcom/glympse/android/ui/NotificationProvider;)Landroid/app/Service;

    move-result-object v0

    sget v1, Lcom/glympse/android/api/R$string;->remaining_title:I

    invoke-virtual {v0, v1}, Landroid/app/Service;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/glympse/android/ui/NotificationProvider$a$1;->wO:Lcom/glympse/android/ui/NotificationProvider$a;

    iget-object v3, v3, Lcom/glympse/android/ui/NotificationProvider$a;->wN:Lcom/glympse/android/ui/NotificationProvider;

    .line 616
    invoke-static {v3}, Lcom/glympse/android/ui/NotificationProvider;->b(Lcom/glympse/android/ui/NotificationProvider;)Landroid/app/Service;

    move-result-object v3

    invoke-static {v3, p1, p2, v4}, Lcom/glympse/android/hal/Helpers;->formatDuration(Landroid/content/Context;JZ)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 615
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 617
    iget-object v1, p0, Lcom/glympse/android/ui/NotificationProvider$a$1;->wO:Lcom/glympse/android/ui/NotificationProvider$a;

    invoke-virtual {v1, v0}, Lcom/glympse/android/ui/NotificationProvider$a;->setTitle(Ljava/lang/String;)V

    .line 618
    iget-object v0, p0, Lcom/glympse/android/ui/NotificationProvider$a$1;->wO:Lcom/glympse/android/ui/NotificationProvider$a;

    iget-object v0, v0, Lcom/glympse/android/ui/NotificationProvider$a;->wN:Lcom/glympse/android/ui/NotificationProvider;

    invoke-virtual {v0}, Lcom/glympse/android/ui/NotificationProvider;->getNotificationManager()Landroid/app/NotificationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/ui/NotificationProvider$a$1;->wO:Lcom/glympse/android/ui/NotificationProvider$a;

    iget v1, v1, Lcom/glympse/android/ui/NotificationProvider$a;->wP:I

    iget-object v2, p0, Lcom/glympse/android/ui/NotificationProvider$a$1;->wO:Lcom/glympse/android/ui/NotificationProvider$a;

    iget-object v2, v2, Lcom/glympse/android/ui/NotificationProvider$a;->wQ:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v2}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 619
    return-void
.end method
