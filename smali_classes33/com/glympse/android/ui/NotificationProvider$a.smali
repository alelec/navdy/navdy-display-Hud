.class Lcom/glympse/android/ui/NotificationProvider$a;
.super Lcom/glympse/android/ui/NotificationProvider$c;
.source "NotificationProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/ui/NotificationProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field private wJ:Z

.field private wK:Z

.field private wL:Ljava/lang/String;

.field private wM:Landroid/os/CountDownTimer;

.field final synthetic wN:Lcom/glympse/android/ui/NotificationProvider;


# direct methods
.method public constructor <init>(Lcom/glympse/android/ui/NotificationProvider;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 457
    iput-object p1, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wN:Lcom/glympse/android/ui/NotificationProvider;

    .line 458
    invoke-direct {p0, p1}, Lcom/glympse/android/ui/NotificationProvider$c;-><init>(Lcom/glympse/android/ui/NotificationProvider;)V

    .line 450
    iput-boolean v0, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wJ:Z

    .line 451
    iput-boolean v4, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wK:Z

    .line 462
    :try_start_0
    iget-object v0, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wQ:Landroid/support/v4/app/NotificationCompat$Builder;

    const/4 v1, 0x0

    .line 463
    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 464
    invoke-static {p1}, Lcom/glympse/android/ui/NotificationProvider;->c(Lcom/glympse/android/ui/NotificationProvider;)Lcom/glympse/android/hal/GResourceGateway;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/glympse/android/hal/GResourceGateway;->getDrawable(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    const-wide/16 v2, 0x0

    .line 465
    invoke-virtual {v0, v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setWhen(J)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 466
    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setOngoing(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 467
    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setOnlyAlertOnce(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 468
    invoke-static {p1}, Lcom/glympse/android/ui/NotificationProvider;->b(Lcom/glympse/android/ui/NotificationProvider;)Landroid/app/Service;

    move-result-object v1

    .line 469
    invoke-static {p1}, Lcom/glympse/android/ui/NotificationProvider;->b(Lcom/glympse/android/ui/NotificationProvider;)Landroid/app/Service;

    move-result-object v2

    invoke-static {v2}, Lcom/glympse/android/hal/GlympseService;->getServiceNotificationIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    const/4 v3, 0x0

    .line 468
    invoke-static {v1, v2, v3}, Lcom/glympse/android/ui/NotificationProvider;->createPendingServiceIntent(Landroid/content/Context;Landroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 476
    :goto_0
    return-void

    .line 472
    :catch_0
    move-exception v0

    .line 474
    invoke-static {v0, v4}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    goto :goto_0
.end method

.method private a(JJ)V
    .locals 7

    .prologue
    .line 610
    invoke-direct {p0}, Lcom/glympse/android/ui/NotificationProvider$a;->dN()V

    .line 611
    new-instance v0, Lcom/glympse/android/ui/NotificationProvider$a$1;

    const-wide/16 v4, 0x3e8

    move-object v1, p0

    move-wide v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/glympse/android/ui/NotificationProvider$a$1;-><init>(Lcom/glympse/android/ui/NotificationProvider$a;JJ)V

    iput-object v0, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wM:Landroid/os/CountDownTimer;

    .line 632
    iget-object v0, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wM:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    .line 633
    return-void
.end method

.method static synthetic a(Lcom/glympse/android/ui/NotificationProvider$a;)V
    .locals 0

    .prologue
    .line 448
    invoke-direct {p0}, Lcom/glympse/android/ui/NotificationProvider$a;->dN()V

    return-void
.end method

.method static synthetic b(Lcom/glympse/android/ui/NotificationProvider$a;)V
    .locals 0

    .prologue
    .line 448
    invoke-direct {p0}, Lcom/glympse/android/ui/NotificationProvider$a;->update()V

    return-void
.end method

.method private c(II)V
    .locals 4

    .prologue
    .line 645
    iget-object v0, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wN:Lcom/glympse/android/ui/NotificationProvider;

    invoke-static {v0}, Lcom/glympse/android/ui/NotificationProvider;->b(Lcom/glympse/android/ui/NotificationProvider;)Landroid/app/Service;

    move-result-object v0

    sget v1, Lcom/glympse/android/api/R$string;->remaining_details:I

    invoke-virtual {v0, v1}, Landroid/app/Service;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 646
    invoke-virtual {p0, v0}, Lcom/glympse/android/ui/NotificationProvider$a;->Q(Ljava/lang/String;)V

    .line 647
    iget-object v0, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wN:Lcom/glympse/android/ui/NotificationProvider;

    invoke-virtual {v0}, Lcom/glympse/android/ui/NotificationProvider;->getNotificationManager()Landroid/app/NotificationManager;

    move-result-object v0

    iget v1, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wP:I

    iget-object v2, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wQ:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v2}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 648
    return-void
.end method

.method private dL()Z
    .locals 23

    .prologue
    .line 523
    const-wide/16 v8, 0x0

    .line 524
    const-wide/16 v6, 0x0

    .line 525
    const/4 v5, 0x0

    .line 526
    const/4 v4, 0x0

    .line 527
    const/4 v3, 0x0

    .line 529
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/glympse/android/ui/NotificationProvider$a;->wN:Lcom/glympse/android/ui/NotificationProvider;

    invoke-static {v2}, Lcom/glympse/android/ui/NotificationProvider;->d(Lcom/glympse/android/ui/NotificationProvider;)Lcom/glympse/android/api/GGlympse;

    move-result-object v2

    if-eqz v2, :cond_8

    .line 533
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/glympse/android/ui/NotificationProvider$a;->wN:Lcom/glympse/android/ui/NotificationProvider;

    invoke-static {v2}, Lcom/glympse/android/ui/NotificationProvider;->d(Lcom/glympse/android/ui/NotificationProvider;)Lcom/glympse/android/api/GGlympse;

    move-result-object v2

    invoke-interface {v2}, Lcom/glympse/android/api/GGlympse;->isActive()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/glympse/android/hal/GlympseService;->areOngoingNotificationsEnabledInForeground()Z

    move-result v2

    if-nez v2, :cond_0

    .line 535
    const/4 v2, 0x0

    .line 597
    :goto_0
    return v2

    .line 538
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/glympse/android/ui/NotificationProvider$a;->wN:Lcom/glympse/android/ui/NotificationProvider;

    invoke-static {v2}, Lcom/glympse/android/ui/NotificationProvider;->d(Lcom/glympse/android/ui/NotificationProvider;)Lcom/glympse/android/api/GGlympse;

    move-result-object v2

    invoke-interface {v2}, Lcom/glympse/android/api/GGlympse;->getTime()J

    move-result-wide v16

    .line 539
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/glympse/android/ui/NotificationProvider$a;->wN:Lcom/glympse/android/ui/NotificationProvider;

    invoke-static {v2}, Lcom/glympse/android/ui/NotificationProvider;->d(Lcom/glympse/android/ui/NotificationProvider;)Lcom/glympse/android/api/GGlympse;

    move-result-object v2

    invoke-interface {v2}, Lcom/glympse/android/api/GGlympse;->getHistoryManager()Lcom/glympse/android/api/GHistoryManager;

    move-result-object v2

    check-cast v2, Lcom/glympse/android/lib/GHistoryManagerPrivate;

    .line 540
    if-eqz v2, :cond_8

    .line 543
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/glympse/android/ui/NotificationProvider$a;->wN:Lcom/glympse/android/ui/NotificationProvider;

    invoke-static {v10}, Lcom/glympse/android/ui/NotificationProvider;->d(Lcom/glympse/android/ui/NotificationProvider;)Lcom/glympse/android/api/GGlympse;

    move-result-object v10

    invoke-interface {v10}, Lcom/glympse/android/api/GGlympse;->getHistoryManager()Lcom/glympse/android/api/GHistoryManager;

    move-result-object v10

    invoke-interface {v10}, Lcom/glympse/android/api/GHistoryManager;->getTickets()Lcom/glympse/android/core/GArray;

    move-result-object v10

    invoke-static {v10}, Lcom/glympse/android/hal/Helpers;->emptyIfNull(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v18

    move-wide v10, v8

    move-wide v8, v6

    move v6, v4

    move v4, v5

    move v5, v3

    :goto_1
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/glympse/android/api/GTicket;

    .line 546
    invoke-interface {v3}, Lcom/glympse/android/api/GTicket;->isSibling()Z

    move-result v7

    if-nez v7, :cond_1

    move-wide/from16 v0, v16

    invoke-interface {v2, v3, v0, v1}, Lcom/glympse/android/lib/GHistoryManagerPrivate;->isActive(Lcom/glympse/android/api/GTicket;J)Z

    move-result v7

    if-nez v7, :cond_2

    .line 583
    :cond_1
    :goto_2
    const/4 v2, 0x1

    if-lt v4, v2, :cond_6

    .line 586
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/glympse/android/ui/NotificationProvider$a;->wQ:Landroid/support/v4/app/NotificationCompat$Builder;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/glympse/android/ui/NotificationProvider$a;->wN:Lcom/glympse/android/ui/NotificationProvider;

    invoke-virtual {v3, v6}, Lcom/glympse/android/ui/NotificationProvider;->K(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 587
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/glympse/android/ui/NotificationProvider$a;->wN:Lcom/glympse/android/ui/NotificationProvider;

    invoke-static {v2}, Lcom/glympse/android/ui/NotificationProvider;->b(Lcom/glympse/android/ui/NotificationProvider;)Landroid/app/Service;

    move-result-object v2

    sget v3, Lcom/glympse/android/api/R$string;->remaining_title:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/glympse/android/ui/NotificationProvider$a;->wN:Lcom/glympse/android/ui/NotificationProvider;

    invoke-static {v12}, Lcom/glympse/android/ui/NotificationProvider;->b(Lcom/glympse/android/ui/NotificationProvider;)Landroid/app/Service;

    move-result-object v12

    const/4 v13, 0x1

    invoke-static {v12, v10, v11, v13}, Lcom/glympse/android/hal/Helpers;->formatDuration(Landroid/content/Context;JZ)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v4, v7

    invoke-virtual {v2, v3, v4}, Landroid/app/Service;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/glympse/android/ui/NotificationProvider$a;->wL:Ljava/lang/String;

    .line 588
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v11, v8, v9}, Lcom/glympse/android/ui/NotificationProvider$a;->a(JJ)V

    .line 589
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v5}, Lcom/glympse/android/ui/NotificationProvider$a;->c(II)V

    .line 597
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 552
    :cond_2
    invoke-interface {v3}, Lcom/glympse/android/api/GTicket;->getExpireTime()J

    move-result-wide v12

    cmp-long v7, v12, v16

    if-lez v7, :cond_3

    invoke-interface {v3}, Lcom/glympse/android/api/GTicket;->getExpireTime()J

    move-result-wide v12

    sub-long v14, v12, v16

    .line 554
    :goto_3
    const/4 v12, 0x0

    .line 555
    const/4 v7, 0x0

    .line 558
    const-wide/16 v20, 0x0

    cmp-long v13, v14, v20

    if-lez v13, :cond_7

    .line 560
    add-int/lit8 v13, v4, 0x1

    .line 563
    invoke-interface {v3}, Lcom/glympse/android/api/GTicket;->getInvites()Lcom/glympse/android/core/GArray;

    move-result-object v4

    invoke-static {v4}, Lcom/glympse/android/hal/Helpers;->emptyIfNull(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :goto_4
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/glympse/android/api/GInvite;

    .line 565
    invoke-interface {v4}, Lcom/glympse/android/api/GInvite;->getViewing()I

    move-result v20

    add-int v12, v12, v20

    .line 566
    invoke-interface {v4}, Lcom/glympse/android/api/GInvite;->getViewers()I

    move-result v4

    add-int/2addr v4, v7

    move v7, v4

    .line 567
    goto :goto_4

    .line 552
    :cond_3
    const-wide/16 v14, 0x0

    goto :goto_3

    .line 570
    :cond_4
    add-int/2addr v6, v12

    .line 571
    add-int v4, v5, v7

    move v5, v6

    move v6, v13

    .line 574
    :goto_5
    cmp-long v7, v14, v10

    if-lez v7, :cond_5

    .line 577
    invoke-interface {v3}, Lcom/glympse/android/api/GTicket;->getDuration()I

    move-result v3

    int-to-long v8, v3

    move-wide v10, v14

    :cond_5
    move/from16 v22, v4

    move v4, v6

    move v6, v5

    move/from16 v5, v22

    .line 579
    goto/16 :goto_1

    .line 594
    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_7
    move/from16 v22, v5

    move v5, v6

    move v6, v4

    move/from16 v4, v22

    goto :goto_5

    :cond_8
    move-wide v10, v8

    move-wide v8, v6

    move v6, v4

    move v4, v5

    move v5, v3

    goto/16 :goto_2
.end method

.method private dM()Ljava/lang/String;
    .locals 1

    .prologue
    .line 603
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wJ:Z

    .line 605
    iget-object v0, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wL:Ljava/lang/String;

    return-object v0
.end method

.method private dN()V
    .locals 1

    .prologue
    .line 637
    iget-object v0, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wM:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    .line 639
    iget-object v0, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wM:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 641
    :cond_0
    return-void
.end method

.method private dP()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 671
    iget-object v1, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wN:Lcom/glympse/android/ui/NotificationProvider;

    invoke-static {v1}, Lcom/glympse/android/ui/NotificationProvider;->d(Lcom/glympse/android/ui/NotificationProvider;)Lcom/glympse/android/api/GGlympse;

    move-result-object v1

    invoke-interface {v1}, Lcom/glympse/android/api/GGlympse;->getNetworkManager()Lcom/glympse/android/api/GNetworkManager;

    move-result-object v1

    .line 672
    if-nez v1, :cond_0

    move v1, v0

    .line 673
    :goto_0
    if-nez v1, :cond_1

    :goto_1
    return v0

    .line 672
    :cond_0
    invoke-interface {v1}, Lcom/glympse/android/api/GNetworkManager;->isNetworkError()Z

    move-result v1

    goto :goto_0

    .line 673
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private isBatteryOk()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 652
    iget-object v2, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wN:Lcom/glympse/android/ui/NotificationProvider;

    invoke-static {v2}, Lcom/glympse/android/ui/NotificationProvider;->d(Lcom/glympse/android/ui/NotificationProvider;)Lcom/glympse/android/api/GGlympse;

    move-result-object v2

    invoke-interface {v2}, Lcom/glympse/android/api/GGlympse;->getBatteryManager()Lcom/glympse/android/api/GBatteryManager;

    move-result-object v2

    .line 653
    if-nez v2, :cond_0

    move v2, v1

    .line 654
    :goto_0
    if-nez v2, :cond_2

    :goto_1
    return v0

    .line 653
    :cond_0
    invoke-interface {v2}, Lcom/glympse/android/api/GBatteryManager;->isBatteryOk()Z

    move-result v2

    if-nez v2, :cond_1

    move v2, v0

    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 654
    goto :goto_1
.end method

.method private update()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 480
    iget-object v0, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wQ:Landroid/support/v4/app/NotificationCompat$Builder;

    const-wide/16 v2, 0x0

    .line 481
    invoke-virtual {v0, v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setWhen(J)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    iget-boolean v0, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wJ:Z

    if-eqz v0, :cond_1

    .line 482
    invoke-direct {p0}, Lcom/glympse/android/ui/NotificationProvider$a;->dM()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 484
    invoke-direct {p0}, Lcom/glympse/android/ui/NotificationProvider$a;->dL()Z

    move-result v0

    .line 486
    if-nez v0, :cond_2

    .line 488
    invoke-direct {p0}, Lcom/glympse/android/ui/NotificationProvider$a;->dN()V

    .line 489
    iget-object v0, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wN:Lcom/glympse/android/ui/NotificationProvider;

    invoke-virtual {v0}, Lcom/glympse/android/ui/NotificationProvider;->getNotificationManager()Landroid/app/NotificationManager;

    move-result-object v0

    iget v2, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wP:I

    invoke-virtual {v0, v2}, Landroid/app/NotificationManager;->cancel(I)V

    .line 490
    iput-boolean v5, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wK:Z

    .line 491
    iget-object v0, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wN:Lcom/glympse/android/ui/NotificationProvider;

    invoke-static {v0}, Lcom/glympse/android/ui/NotificationProvider;->b(Lcom/glympse/android/ui/NotificationProvider;)Landroid/app/Service;

    move-result-object v0

    invoke-static {v0, v4}, Lcom/glympse/android/hal/Reflection$_Service;->stopForeground(Landroid/app/Service;Z)V

    .line 492
    iput-boolean v4, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wJ:Z

    .line 493
    iget-object v0, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wQ:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 519
    :cond_0
    :goto_1
    return-void

    :cond_1
    move-object v0, v1

    .line 482
    goto :goto_0

    .line 499
    :cond_2
    :try_start_0
    iget-object v0, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wN:Lcom/glympse/android/ui/NotificationProvider;

    invoke-virtual {v0}, Lcom/glympse/android/ui/NotificationProvider;->getNotificationManager()Landroid/app/NotificationManager;

    move-result-object v0

    iget v1, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wP:I

    iget-object v2, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wQ:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v2}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 500
    iget-boolean v0, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wK:Z

    if-nez v0, :cond_3

    .line 502
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wK:Z

    .line 503
    iget-object v0, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wN:Lcom/glympse/android/ui/NotificationProvider;

    invoke-static {v0}, Lcom/glympse/android/ui/NotificationProvider;->b(Lcom/glympse/android/ui/NotificationProvider;)Landroid/app/Service;

    move-result-object v0

    iget v1, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wP:I

    iget-object v2, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wQ:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v2}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/glympse/android/hal/Reflection$_Service;->startForeground(Landroid/app/Service;ILandroid/app/Notification;)V

    .line 506
    :cond_3
    invoke-direct {p0}, Lcom/glympse/android/ui/NotificationProvider$a;->isBatteryOk()Z

    move-result v0

    if-nez v0, :cond_4

    .line 508
    invoke-virtual {p0}, Lcom/glympse/android/ui/NotificationProvider$a;->dO()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 515
    :catch_0
    move-exception v0

    .line 517
    invoke-static {v0, v5}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    goto :goto_1

    .line 510
    :cond_4
    :try_start_1
    invoke-direct {p0}, Lcom/glympse/android/ui/NotificationProvider$a;->dP()Z

    move-result v0

    if-nez v0, :cond_0

    .line 512
    invoke-virtual {p0}, Lcom/glympse/android/ui/NotificationProvider$a;->dQ()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method


# virtual methods
.method public dO()V
    .locals 3

    .prologue
    .line 659
    invoke-direct {p0}, Lcom/glympse/android/ui/NotificationProvider$a;->isBatteryOk()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wK:Z

    if-eqz v0, :cond_0

    .line 661
    iget-object v0, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wQ:Landroid/support/v4/app/NotificationCompat$Builder;

    iget-object v1, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wN:Lcom/glympse/android/ui/NotificationProvider;

    invoke-static {v1}, Lcom/glympse/android/ui/NotificationProvider;->c(Lcom/glympse/android/ui/NotificationProvider;)Lcom/glympse/android/hal/GResourceGateway;

    move-result-object v1

    const/16 v2, 0xd

    invoke-interface {v1, v2}, Lcom/glympse/android/hal/GResourceGateway;->getDrawable(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 662
    iget-object v0, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wN:Lcom/glympse/android/ui/NotificationProvider;

    invoke-static {v0}, Lcom/glympse/android/ui/NotificationProvider;->b(Lcom/glympse/android/ui/NotificationProvider;)Landroid/app/Service;

    move-result-object v0

    sget v1, Lcom/glympse/android/api/R$string;->low_battey_warning:I

    invoke-virtual {v0, v1}, Landroid/app/Service;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/glympse/android/ui/NotificationProvider$a;->Q(Ljava/lang/String;)V

    .line 663
    iget-object v0, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wN:Lcom/glympse/android/ui/NotificationProvider;

    invoke-virtual {v0}, Lcom/glympse/android/ui/NotificationProvider;->getNotificationManager()Landroid/app/NotificationManager;

    move-result-object v0

    iget v1, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wP:I

    iget-object v2, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wQ:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v2}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 667
    :goto_0
    return-void

    .line 666
    :cond_0
    invoke-direct {p0}, Lcom/glympse/android/ui/NotificationProvider$a;->update()V

    goto :goto_0
.end method

.method public dQ()V
    .locals 3

    .prologue
    .line 678
    invoke-direct {p0}, Lcom/glympse/android/ui/NotificationProvider$a;->dP()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wK:Z

    if-eqz v0, :cond_0

    .line 680
    iget-object v0, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wQ:Landroid/support/v4/app/NotificationCompat$Builder;

    iget-object v1, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wN:Lcom/glympse/android/ui/NotificationProvider;

    invoke-static {v1}, Lcom/glympse/android/ui/NotificationProvider;->c(Lcom/glympse/android/ui/NotificationProvider;)Lcom/glympse/android/hal/GResourceGateway;

    move-result-object v1

    const/16 v2, 0xd

    invoke-interface {v1, v2}, Lcom/glympse/android/hal/GResourceGateway;->getDrawable(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 681
    iget-object v0, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wN:Lcom/glympse/android/ui/NotificationProvider;

    invoke-static {v0}, Lcom/glympse/android/ui/NotificationProvider;->b(Lcom/glympse/android/ui/NotificationProvider;)Landroid/app/Service;

    move-result-object v0

    sget v1, Lcom/glympse/android/api/R$string;->no_network_connection:I

    invoke-virtual {v0, v1}, Landroid/app/Service;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/glympse/android/ui/NotificationProvider$a;->Q(Ljava/lang/String;)V

    .line 682
    iget-object v0, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wN:Lcom/glympse/android/ui/NotificationProvider;

    invoke-virtual {v0}, Lcom/glympse/android/ui/NotificationProvider;->getNotificationManager()Landroid/app/NotificationManager;

    move-result-object v0

    iget v1, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wP:I

    iget-object v2, p0, Lcom/glympse/android/ui/NotificationProvider$a;->wQ:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v2}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 686
    :goto_0
    return-void

    .line 685
    :cond_0
    invoke-direct {p0}, Lcom/glympse/android/ui/NotificationProvider$a;->update()V

    goto :goto_0
.end method
