.class Lcom/glympse/android/ui/NotificationProvider$c;
.super Landroid/support/v4/app/NotificationCompat;
.source "NotificationProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/ui/NotificationProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field final synthetic wN:Lcom/glympse/android/ui/NotificationProvider;

.field protected final wP:I

.field protected final wQ:Landroid/support/v4/app/NotificationCompat$Builder;


# direct methods
.method public constructor <init>(Lcom/glympse/android/ui/NotificationProvider;)V
    .locals 2

    .prologue
    .line 428
    iput-object p1, p0, Lcom/glympse/android/ui/NotificationProvider$c;->wN:Lcom/glympse/android/ui/NotificationProvider;

    invoke-direct {p0}, Landroid/support/v4/app/NotificationCompat;-><init>()V

    .line 424
    iget-object v0, p0, Lcom/glympse/android/ui/NotificationProvider$c;->wN:Lcom/glympse/android/ui/NotificationProvider;

    invoke-static {v0}, Lcom/glympse/android/ui/NotificationProvider;->a(Lcom/glympse/android/ui/NotificationProvider;)I

    move-result v0

    iput v0, p0, Lcom/glympse/android/ui/NotificationProvider$c;->wP:I

    .line 429
    new-instance v0, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-static {p1}, Lcom/glympse/android/ui/NotificationProvider;->b(Lcom/glympse/android/ui/NotificationProvider;)Landroid/app/Service;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/glympse/android/ui/NotificationProvider$c;->wQ:Landroid/support/v4/app/NotificationCompat$Builder;

    .line 430
    return-void
.end method


# virtual methods
.method protected Q(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 444
    iget-object v0, p0, Lcom/glympse/android/ui/NotificationProvider$c;->wQ:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 445
    return-void
.end method

.method public remove()V
    .locals 2

    .prologue
    .line 434
    iget-object v0, p0, Lcom/glympse/android/ui/NotificationProvider$c;->wN:Lcom/glympse/android/ui/NotificationProvider;

    invoke-virtual {v0}, Lcom/glympse/android/ui/NotificationProvider;->getNotificationManager()Landroid/app/NotificationManager;

    move-result-object v0

    iget v1, p0, Lcom/glympse/android/ui/NotificationProvider$c;->wP:I

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 435
    return-void
.end method

.method protected setTitle(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 439
    iget-object v0, p0, Lcom/glympse/android/ui/NotificationProvider$c;->wQ:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 440
    return-void
.end method
