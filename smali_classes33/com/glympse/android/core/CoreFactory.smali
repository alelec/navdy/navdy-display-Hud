.class public abstract Lcom/glympse/android/core/CoreFactory;
.super Ljava/lang/Object;
.source "CoreFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static areSignalHandlersEnabled(Z)Z
    .locals 1

    .prologue
    .line 260
    invoke-static {}, Lcom/glympse/android/lib/Debug;->areSignalHandlersEnabled()Z

    move-result v0

    return v0
.end method

.method public static createDrawable()Lcom/glympse/android/core/GDrawable;
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/glympse/android/hal/HalFactory;->createDrawable(Landroid/graphics/Bitmap;)Lcom/glympse/android/core/GDrawable;

    move-result-object v0

    return-object v0
.end method

.method public static createDrawable(Landroid/graphics/Bitmap;)Lcom/glympse/android/core/GDrawable;
    .locals 1

    .prologue
    .line 47
    invoke-static {p0}, Lcom/glympse/android/hal/HalFactory;->createDrawable(Landroid/graphics/Bitmap;)Lcom/glympse/android/core/GDrawable;

    move-result-object v0

    return-object v0
.end method

.method public static createDrawable(Landroid/graphics/drawable/BitmapDrawable;)Lcom/glympse/android/core/GDrawable;
    .locals 1

    .prologue
    .line 39
    invoke-static {p0}, Lcom/glympse/android/hal/HalFactory;->createDrawable(Landroid/graphics/drawable/BitmapDrawable;)Lcom/glympse/android/core/GDrawable;

    move-result-object v0

    return-object v0
.end method

.method public static createDrawable(Ljava/lang/String;I)Lcom/glympse/android/core/GDrawable;
    .locals 1

    .prologue
    .line 64
    invoke-static {p0, p1}, Lcom/glympse/android/hal/HalFactory;->createDrawable(Ljava/lang/String;I)Lcom/glympse/android/core/GDrawable;

    move-result-object v0

    return-object v0
.end method

.method public static createHashtable()Lcom/glympse/android/hal/GHashtable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/glympse/android/hal/GHashtable",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 234
    new-instance v0, Lcom/glympse/android/hal/GHashtable;

    invoke-direct {v0}, Lcom/glympse/android/hal/GHashtable;-><init>()V

    return-object v0
.end method

.method public static createLinkedList()Lcom/glympse/android/hal/GLinkedList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/glympse/android/hal/GLinkedList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 226
    new-instance v0, Lcom/glympse/android/hal/GLinkedList;

    invoke-direct {v0}, Lcom/glympse/android/hal/GLinkedList;-><init>()V

    return-object v0
.end method

.method public static createLocation(DD)Lcom/glympse/android/core/GLocation;
    .locals 14

    .prologue
    .line 159
    new-instance v1, Lcom/glympse/android/lib/Location;

    const-wide/16 v2, 0x0

    const/high16 v8, 0x7fc00000    # NaNf

    const/high16 v9, 0x7fc00000    # NaNf

    const/high16 v10, 0x7fc00000    # NaNf

    const/high16 v11, 0x7fc00000    # NaNf

    const/high16 v12, 0x7fc00000    # NaNf

    move-wide v4, p0

    move-wide/from16 v6, p2

    invoke-direct/range {v1 .. v12}, Lcom/glympse/android/lib/Location;-><init>(JDDFFFFF)V

    return-object v1
.end method

.method public static createLocation(JDDFFFFF)Lcom/glympse/android/core/GLocation;
    .locals 14

    .prologue
    .line 148
    new-instance v1, Lcom/glympse/android/lib/Location;

    move-wide v2, p0

    move-wide/from16 v4, p2

    move-wide/from16 v6, p4

    move/from16 v8, p6

    move/from16 v9, p7

    move/from16 v10, p8

    move/from16 v11, p9

    move/from16 v12, p10

    invoke-direct/range {v1 .. v12}, Lcom/glympse/android/lib/Location;-><init>(JDDFFFFF)V

    return-object v1
.end method

.method public static createLocationProfileBuilder(I)Lcom/glympse/android/core/GLocationProfileBuilder;
    .locals 1

    .prologue
    .line 191
    new-instance v0, Lcom/glympse/android/lib/LocationProfileBuilder;

    invoke-direct {v0, p0}, Lcom/glympse/android/lib/LocationProfileBuilder;-><init>(I)V

    return-object v0
.end method

.method public static createLocationProvider(Ljava/lang/Object;)Lcom/glympse/android/core/GLocationProvider;
    .locals 1

    .prologue
    .line 181
    check-cast p0, Landroid/content/Context;

    invoke-static {p0}, Lcom/glympse/android/hal/HalFactory;->createLocationProvider(Landroid/content/Context;)Lcom/glympse/android/core/GLocationProvider;

    move-result-object v0

    return-object v0
.end method

.method public static createPrimitive()Lcom/glympse/android/core/GPrimitive;
    .locals 1

    .prologue
    .line 127
    new-instance v0, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v0}, Lcom/glympse/android/lib/Primitive;-><init>()V

    return-object v0
.end method

.method public static createPrimitive(D)Lcom/glympse/android/core/GPrimitive;
    .locals 2

    .prologue
    .line 95
    new-instance v0, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v0, p0, p1}, Lcom/glympse/android/lib/Primitive;-><init>(D)V

    return-object v0
.end method

.method public static createPrimitive(I)Lcom/glympse/android/core/GPrimitive;
    .locals 1

    .prologue
    .line 87
    new-instance v0, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v0, p0}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    return-object v0
.end method

.method public static createPrimitive(J)Lcom/glympse/android/core/GPrimitive;
    .locals 2

    .prologue
    .line 103
    new-instance v0, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v0, p0, p1}, Lcom/glympse/android/lib/Primitive;-><init>(J)V

    return-object v0
.end method

.method public static createPrimitive(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;
    .locals 1

    .prologue
    .line 119
    new-instance v0, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v0, p0}, Lcom/glympse/android/lib/Primitive;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static createPrimitive(Z)Lcom/glympse/android/core/GPrimitive;
    .locals 1

    .prologue
    .line 111
    new-instance v0, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v0, p0}, Lcom/glympse/android/lib/Primitive;-><init>(Z)V

    return-object v0
.end method

.method public static createRegion(DDDLjava/lang/String;)Lcom/glympse/android/core/GRegion;
    .locals 2

    .prologue
    .line 173
    invoke-static/range {p0 .. p6}, Lcom/glympse/android/lib/LibFactory;->createRegion(DDDLjava/lang/String;)Lcom/glympse/android/core/GRegion;

    move-result-object v0

    return-object v0
.end method

.method public static createStorage(Ljava/lang/Object;Ljava/lang/String;)Lcom/glympse/android/core/GStorageUnit;
    .locals 1

    .prologue
    .line 75
    check-cast p0, Landroid/content/Context;

    invoke-static {p0, p1}, Lcom/glympse/android/lib/LibFactory;->createStorage(Landroid/content/Context;Ljava/lang/String;)Lcom/glympse/android/core/GStorageUnit;

    move-result-object v0

    return-object v0
.end method

.method public static createString(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 199
    return-object p0
.end method

.method public static createStringBuilder(I)Ljava/lang/StringBuilder;
    .locals 1

    .prologue
    .line 210
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p0}, Ljava/lang/StringBuilder;-><init>(I)V

    return-object v0
.end method

.method public static createVector()Lcom/glympse/android/hal/GVector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/glympse/android/hal/GVector",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 218
    new-instance v0, Lcom/glympse/android/hal/GVector;

    invoke-direct {v0}, Lcom/glympse/android/hal/GVector;-><init>()V

    return-object v0
.end method

.method public static enableSignalHandlers(Z)V
    .locals 0

    .prologue
    .line 252
    invoke-static {p0}, Lcom/glympse/android/lib/Debug;->enableSignalHandlers(Z)V

    .line 253
    return-void
.end method

.method public static getOsName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 242
    invoke-static {}, Lcom/glympse/android/hal/Platform;->getOsName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
