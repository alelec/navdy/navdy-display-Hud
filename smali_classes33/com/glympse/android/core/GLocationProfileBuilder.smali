.class public interface abstract Lcom/glympse/android/core/GLocationProfileBuilder;
.super Ljava/lang/Object;
.source "GLocationProfileBuilder.java"

# interfaces
.implements Lcom/glympse/android/core/GCommon;


# virtual methods
.method public abstract getLocationProfile()Lcom/glympse/android/core/GLocationProfile;
.end method

.method public abstract setAccuracy(D)V
.end method

.method public abstract setActivity(I)V
.end method

.method public abstract setAutoPauseEnabled(Z)V
.end method

.method public abstract setDistance(D)V
.end method

.method public abstract setFrequency(I)V
.end method

.method public abstract setMode(I)V
.end method

.method public abstract setPriority(I)V
.end method

.method public abstract setSignificantLocationChangeMonitoringEnabled(Z)V
.end method

.method public abstract setSource(I)V
.end method
