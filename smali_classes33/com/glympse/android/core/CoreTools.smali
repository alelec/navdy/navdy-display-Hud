.class public Lcom/glympse/android/core/CoreTools;
.super Ljava/lang/Object;
.source "CoreTools.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    return-void
.end method

.method public static httpMethodEnumToString(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    invoke-static {p0}, Lcom/glympse/android/lib/HttpJob;->httpMethodEnumToString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static httpMethodStringToEnum(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 51
    invoke-static {p0}, Lcom/glympse/android/lib/HttpJob;->httpMethodStringToEnum(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static primitiveToString(Lcom/glympse/android/core/GPrimitive;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    invoke-static {p0}, Lcom/glympse/android/lib/json/JsonSerializer;->toString(Lcom/glympse/android/core/GPrimitive;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static stringToPrimitive(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;
    .locals 1

    .prologue
    .line 35
    invoke-static {p0}, Lcom/glympse/android/lib/json/JsonSerializer;->toPrimitive(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    return-object v0
.end method

.method public static urlDecode(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->urlDecode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static urlEncode(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
