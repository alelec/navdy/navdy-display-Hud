.class public Lcom/glympse/android/rpc/MessageTrack;
.super Ljava/lang/Object;
.source "MessageTrack.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static decodeTrack(Lcom/glympse/android/core/GPrimitive;)Lcom/glympse/android/api/GTrack;
    .locals 20

    .prologue
    .line 130
    invoke-static {}, Lcom/glympse/android/api/GlympseFactory;->createTrackBuilder()Lcom/glympse/android/api/GTrackBuilder;

    move-result-object v14

    .line 132
    invoke-interface/range {p0 .. p0}, Lcom/glympse/android/core/GPrimitive;->getArray()Lcom/glympse/android/core/GArray;

    move-result-object v15

    .line 133
    invoke-interface {v15}, Lcom/glympse/android/core/GArray;->length()I

    move-result v16

    .line 135
    const/4 v2, 0x0

    move v13, v2

    :goto_0
    move/from16 v0, v16

    if-ge v13, v0, :cond_5

    .line 137
    invoke-interface {v15, v13}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v2}, Lcom/glympse/android/core/GPrimitive;->getArray()Lcom/glympse/android/core/GArray;

    move-result-object v17

    .line 139
    const/4 v2, 0x0

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v2}, Lcom/glympse/android/core/GPrimitive;->getLong()J

    move-result-wide v2

    .line 140
    const/4 v4, 0x1

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v4}, Lcom/glympse/android/core/GPrimitive;->getDouble()D

    move-result-wide v4

    const-wide v6, 0x412e848000000000L    # 1000000.0

    div-double/2addr v4, v6

    .line 141
    const/4 v6, 0x2

    move-object/from16 v0, v17

    invoke-interface {v0, v6}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v6}, Lcom/glympse/android/core/GPrimitive;->getDouble()D

    move-result-wide v6

    const-wide v8, 0x412e848000000000L    # 1000000.0

    div-double/2addr v6, v8

    .line 142
    const/4 v8, 0x3

    move-object/from16 v0, v17

    invoke-interface {v0, v8}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v8}, Lcom/glympse/android/core/GPrimitive;->isNull()Z

    move-result v8

    if-eqz v8, :cond_0

    const/high16 v8, 0x7fc00000    # NaNf

    .line 143
    :goto_1
    const/4 v9, 0x4

    move-object/from16 v0, v17

    invoke-interface {v0, v9}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v9}, Lcom/glympse/android/core/GPrimitive;->isNull()Z

    move-result v9

    if-eqz v9, :cond_1

    const/high16 v9, 0x7fc00000    # NaNf

    .line 144
    :goto_2
    const/4 v10, 0x5

    move-object/from16 v0, v17

    invoke-interface {v0, v10}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v10}, Lcom/glympse/android/core/GPrimitive;->isNull()Z

    move-result v10

    if-eqz v10, :cond_2

    const/high16 v10, 0x7fc00000    # NaNf

    .line 145
    :goto_3
    const/4 v11, 0x6

    move-object/from16 v0, v17

    invoke-interface {v0, v11}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v11}, Lcom/glympse/android/core/GPrimitive;->isNull()Z

    move-result v11

    if-eqz v11, :cond_3

    const/high16 v11, 0x7fc00000    # NaNf

    .line 146
    :goto_4
    const/4 v12, 0x7

    move-object/from16 v0, v17

    invoke-interface {v0, v12}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v12}, Lcom/glympse/android/core/GPrimitive;->isNull()Z

    move-result v12

    if-eqz v12, :cond_4

    const/high16 v12, 0x7fc00000    # NaNf

    .line 148
    :goto_5
    invoke-static/range {v2 .. v12}, Lcom/glympse/android/core/CoreFactory;->createLocation(JDDFFFFF)Lcom/glympse/android/core/GLocation;

    move-result-object v2

    check-cast v2, Lcom/glympse/android/lib/GLocationPrivate;

    .line 150
    invoke-interface {v14, v2}, Lcom/glympse/android/api/GTrackBuilder;->addLocation(Lcom/glympse/android/core/GLocation;)V

    .line 135
    add-int/lit8 v2, v13, 0x1

    move v13, v2

    goto/16 :goto_0

    .line 142
    :cond_0
    const/4 v8, 0x3

    move-object/from16 v0, v17

    invoke-interface {v0, v8}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v8}, Lcom/glympse/android/core/GPrimitive;->getDouble()D

    move-result-wide v8

    const-wide/high16 v10, 0x4059000000000000L    # 100.0

    div-double/2addr v8, v10

    double-to-float v8, v8

    goto :goto_1

    .line 143
    :cond_1
    const/4 v9, 0x4

    move-object/from16 v0, v17

    invoke-interface {v0, v9}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v9}, Lcom/glympse/android/core/GPrimitive;->getDouble()D

    move-result-wide v10

    double-to-float v9, v10

    goto :goto_2

    .line 144
    :cond_2
    const/4 v10, 0x5

    move-object/from16 v0, v17

    invoke-interface {v0, v10}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v10}, Lcom/glympse/android/core/GPrimitive;->getDouble()D

    move-result-wide v10

    double-to-float v10, v10

    goto :goto_3

    .line 145
    :cond_3
    const/4 v11, 0x6

    move-object/from16 v0, v17

    invoke-interface {v0, v11}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v11}, Lcom/glympse/android/core/GPrimitive;->getDouble()D

    move-result-wide v18

    move-wide/from16 v0, v18

    double-to-float v11, v0

    goto :goto_4

    .line 146
    :cond_4
    const/4 v12, 0x7

    move-object/from16 v0, v17

    invoke-interface {v0, v12}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v12}, Lcom/glympse/android/core/GPrimitive;->getDouble()D

    move-result-wide v18

    move-wide/from16 v0, v18

    double-to-float v12, v0

    goto :goto_5

    .line 153
    :cond_5
    invoke-interface {v14}, Lcom/glympse/android/api/GTrackBuilder;->getTrack()Lcom/glympse/android/api/GTrack;

    move-result-object v2

    return-object v2
.end method

.method public static encodeTrack(Lcom/glympse/android/api/GTicket;Lcom/glympse/android/core/GPrimitive;)V
    .locals 7

    .prologue
    const/16 v6, 0x40

    .line 25
    invoke-interface {p0}, Lcom/glympse/android/api/GTicket;->getTrack()Lcom/glympse/android/api/GTrack;

    move-result-object v1

    .line 26
    invoke-interface {v1}, Lcom/glympse/android/api/GTrack;->getNewLocations()Lcom/glympse/android/core/GList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Lcom/glympse/android/api/GTrack;->getNewLocations()Lcom/glympse/android/core/GList;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/core/GList;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 91
    :cond_0
    return-void

    .line 31
    :cond_1
    const/4 v0, 0x0

    .line 32
    invoke-interface {v1}, Lcom/glympse/android/api/GTrack;->getNewLocations()Lcom/glympse/android/core/GList;

    move-result-object v1

    invoke-interface {v1}, Lcom/glympse/android/core/GList;->elements()Ljava/util/Enumeration;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/glympse/android/core/CoreFactory;->createPrimitive(I)Lcom/glympse/android/core/GPrimitive;

    move-result-object v3

    .line 35
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GLocation;

    .line 38
    invoke-interface {v0}, Lcom/glympse/android/core/GLocation;->getTime()J

    move-result-wide v4

    invoke-interface {v3, v4, v5}, Lcom/glympse/android/core/GPrimitive;->put(J)V

    .line 40
    invoke-interface {v0}, Lcom/glympse/android/core/GLocation;->getLatitudeE6()I

    move-result v4

    int-to-long v4, v4

    invoke-interface {v3, v4, v5}, Lcom/glympse/android/core/GPrimitive;->put(J)V

    .line 42
    invoke-interface {v0}, Lcom/glympse/android/core/GLocation;->getLongitudeE6()I

    move-result v4

    int-to-long v4, v4

    invoke-interface {v3, v4, v5}, Lcom/glympse/android/core/GPrimitive;->put(J)V

    .line 44
    invoke-interface {v0}, Lcom/glympse/android/core/GLocation;->hasSpeed()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 46
    invoke-interface {v0}, Lcom/glympse/android/core/GLocation;->getSpeed()F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    float-to-double v4, v4

    invoke-interface {v3, v4, v5}, Lcom/glympse/android/core/GPrimitive;->put(D)V

    .line 53
    :goto_1
    invoke-interface {v0}, Lcom/glympse/android/core/GLocation;->hasBearing()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 55
    invoke-interface {v0}, Lcom/glympse/android/core/GLocation;->getBearing()F

    move-result v4

    float-to-double v4, v4

    invoke-interface {v3, v4, v5}, Lcom/glympse/android/core/GPrimitive;->put(D)V

    .line 62
    :goto_2
    invoke-interface {v0}, Lcom/glympse/android/core/GLocation;->hasAltitude()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 64
    invoke-interface {v0}, Lcom/glympse/android/core/GLocation;->getAltitude()F

    move-result v4

    float-to-double v4, v4

    invoke-interface {v3, v4, v5}, Lcom/glympse/android/core/GPrimitive;->put(D)V

    .line 71
    :goto_3
    invoke-interface {v0}, Lcom/glympse/android/core/GLocation;->hasHAccuracy()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 73
    invoke-interface {v0}, Lcom/glympse/android/core/GLocation;->getHAccuracy()F

    move-result v4

    float-to-double v4, v4

    invoke-interface {v3, v4, v5}, Lcom/glympse/android/core/GPrimitive;->put(D)V

    .line 80
    :goto_4
    invoke-interface {v0}, Lcom/glympse/android/core/GLocation;->hasVAccuracy()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 82
    invoke-interface {v0}, Lcom/glympse/android/core/GLocation;->getVAccuracy()F

    move-result v0

    float-to-double v4, v0

    invoke-interface {v3, v4, v5}, Lcom/glympse/android/core/GPrimitive;->put(D)V

    .line 89
    :goto_5
    invoke-interface {p1, v3}, Lcom/glympse/android/core/GPrimitive;->put(Lcom/glympse/android/core/GPrimitive;)V

    .line 32
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 50
    :cond_2
    invoke-static {v6}, Lcom/glympse/android/core/CoreFactory;->createPrimitive(I)Lcom/glympse/android/core/GPrimitive;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/glympse/android/core/GPrimitive;->put(Lcom/glympse/android/core/GPrimitive;)V

    goto :goto_1

    .line 59
    :cond_3
    invoke-static {v6}, Lcom/glympse/android/core/CoreFactory;->createPrimitive(I)Lcom/glympse/android/core/GPrimitive;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/glympse/android/core/GPrimitive;->put(Lcom/glympse/android/core/GPrimitive;)V

    goto :goto_2

    .line 68
    :cond_4
    invoke-static {v6}, Lcom/glympse/android/core/CoreFactory;->createPrimitive(I)Lcom/glympse/android/core/GPrimitive;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/glympse/android/core/GPrimitive;->put(Lcom/glympse/android/core/GPrimitive;)V

    goto :goto_3

    .line 77
    :cond_5
    invoke-static {v6}, Lcom/glympse/android/core/CoreFactory;->createPrimitive(I)Lcom/glympse/android/core/GPrimitive;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/glympse/android/core/GPrimitive;->put(Lcom/glympse/android/core/GPrimitive;)V

    goto :goto_4

    .line 86
    :cond_6
    invoke-static {v6}, Lcom/glympse/android/core/CoreFactory;->createPrimitive(I)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    invoke-interface {v3, v0}, Lcom/glympse/android/core/GPrimitive;->put(Lcom/glympse/android/core/GPrimitive;)V

    goto :goto_5
.end method

.method public static encodeTrackLegacy(Lcom/glympse/android/api/GTicket;Lcom/glympse/android/core/GPrimitive;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/16 v8, 0x2c

    .line 95
    invoke-interface {p0}, Lcom/glympse/android/api/GTicket;->getTrack()Lcom/glympse/android/api/GTrack;

    move-result-object v0

    .line 96
    invoke-interface {v0}, Lcom/glympse/android/api/GTrack;->getNewLocations()Lcom/glympse/android/core/GList;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Lcom/glympse/android/api/GTrack;->getNewLocations()Lcom/glympse/android/core/GList;

    move-result-object v1

    invoke-interface {v1}, Lcom/glympse/android/core/GList;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 122
    :cond_0
    return-void

    .line 102
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v1, 0x1e

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 103
    invoke-interface {v0}, Lcom/glympse/android/api/GTrack;->getNewLocations()Lcom/glympse/android/core/GList;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/core/GList;->elements()Ljava/util/Enumeration;

    move-result-object v4

    move v1, v2

    :goto_0
    invoke-interface {v4}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    invoke-interface {v4}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GLocation;

    .line 108
    invoke-interface {v0}, Lcom/glympse/android/core/GLocation;->getLatitude()D

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 109
    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 111
    invoke-interface {v0}, Lcom/glympse/android/core/GLocation;->getLongitude()D

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 112
    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 114
    invoke-interface {v0}, Lcom/glympse/android/core/GLocation;->getBearing()F

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 115
    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 117
    invoke-interface {v0}, Lcom/glympse/android/core/GLocation;->getTime()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 119
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;)V

    .line 120
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 103
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method
