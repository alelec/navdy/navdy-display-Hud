.class Lcom/glympse/android/lib/gf$b;
.super Ljava/lang/Object;
.source "UserManager.java"

# interfaces
.implements Lcom/glympse/android/api/GEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/lib/gf;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation


# instance fields
.field private uZ:Lcom/glympse/android/lib/GTrackPrivate;


# direct methods
.method public constructor <init>(Lcom/glympse/android/lib/GTrackPrivate;)V
    .locals 0

    .prologue
    .line 1119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120
    iput-object p1, p0, Lcom/glympse/android/lib/gf$b;->uZ:Lcom/glympse/android/lib/GTrackPrivate;

    .line 1121
    return-void
.end method


# virtual methods
.method public eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1125
    const v0, 0x10002

    if-ne v0, p2, :cond_0

    .line 1127
    and-int/lit16 v0, p3, 0x2000

    if-eqz v0, :cond_0

    .line 1130
    iget-object v0, p0, Lcom/glympse/android/lib/gf$b;->uZ:Lcom/glympse/android/lib/GTrackPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GTrackPrivate;->getLocationsRaw()Lcom/glympse/android/hal/GLinkedList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/glympse/android/hal/GLinkedList;->clear()V

    .line 1137
    :cond_0
    return-void
.end method
