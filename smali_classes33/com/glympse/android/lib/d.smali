.class Lcom/glympse/android/lib/d;
.super Ljava/lang/Object;
.source "AccountManager.java"

# interfaces
.implements Lcom/glympse/android/lib/GAccountImportListener;
.implements Lcom/glympse/android/lib/GAccountListener;
.implements Lcom/glympse/android/lib/GAccountManager;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/lib/d$d;,
        Lcom/glympse/android/lib/d$c;,
        Lcom/glympse/android/lib/d$f;,
        Lcom/glympse/android/lib/d$e;,
        Lcom/glympse/android/lib/d$b;,
        Lcom/glympse/android/lib/d$a;
    }
.end annotation


# instance fields
.field private _glympse:Lcom/glympse/android/lib/GGlympsePrivate;

.field private gb:Lcom/glympse/android/lib/GAccountListener;

.field private gp:Lcom/glympse/android/lib/GAccountProvider;

.field private gq:Lcom/glympse/android/lib/HttpJob;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/glympse/android/lib/GGlympsePrivate;Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)Lcom/glympse/android/api/GEventSink;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-static {p1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    if-nez p2, :cond_1

    .line 52
    :cond_0
    :goto_0
    return-object v0

    .line 44
    :cond_1
    const-string v1, "type"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 45
    invoke-static {p1, v1}, Lcom/glympse/android/hal/Helpers;->safeEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 51
    new-instance v0, Lcom/glympse/android/lib/d$c;

    invoke-direct {v0, p0, p1, p2}, Lcom/glympse/android/lib/d$c;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 52
    invoke-virtual {v0}, Lcom/glympse/android/lib/d$c;->ao()Lcom/glympse/android/api/GEventSink;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lcom/glympse/android/core/GPrimitive;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x4

    const/4 v4, 0x1

    .line 303
    const-string v0, "type"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 304
    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 306
    invoke-virtual {p0, v4, v5, v6}, Lcom/glympse/android/lib/d;->failedToCreate(ZILcom/glympse/android/api/GServerError;)V

    .line 351
    :goto_0
    return-void

    .line 313
    :cond_0
    const-string v1, "account"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 316
    const-string v0, "id"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 317
    const-string v1, "password"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 318
    const-string v2, "device_id"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 319
    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 321
    :cond_1
    invoke-virtual {p0, v4, v5, v6}, Lcom/glympse/android/lib/d;->failedToCreate(ZILcom/glympse/android/api/GServerError;)V

    goto :goto_0

    .line 326
    :cond_2
    iget-object v3, p0, Lcom/glympse/android/lib/d;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v3}, Lcom/glympse/android/lib/GGlympsePrivate;->getConfigPrivate()Lcom/glympse/android/lib/GConfigPrivate;

    move-result-object v3

    .line 327
    invoke-interface {v3, v2}, Lcom/glympse/android/lib/GConfigPrivate;->saveCurrentDeviceId(Ljava/lang/String;)V

    .line 330
    invoke-virtual {p0, v0, v1}, Lcom/glympse/android/lib/d;->accountCreated(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 335
    :cond_3
    new-instance v1, Lcom/glympse/android/hal/GVector;

    invoke-direct {v1}, Lcom/glympse/android/hal/GVector;-><init>()V

    .line 338
    invoke-static {}, Lcom/glympse/android/api/GC;->LINKED_ACCOUNT_TYPE_PAIRING()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 340
    const-string v0, "code"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 342
    new-instance v0, Lcom/glympse/android/lib/bq;

    iget-object v2, p0, Lcom/glympse/android/lib/d;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct {p0}, Lcom/glympse/android/lib/d;->am()Lcom/glympse/android/lib/GAccountListener;

    move-result-object v3

    invoke-direct {v0, v2, v3, p1}, Lcom/glympse/android/lib/bq;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/GAccountListener;Lcom/glympse/android/core/GPrimitive;)V

    invoke-virtual {v1, v0}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 347
    :cond_4
    new-instance v0, Lcom/glympse/android/lib/b;

    iget-object v2, p0, Lcom/glympse/android/lib/d;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct {p0}, Lcom/glympse/android/lib/d;->am()Lcom/glympse/android/lib/GAccountListener;

    move-result-object v3

    invoke-direct {v0, v2, v3, p1}, Lcom/glympse/android/lib/b;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/GAccountListener;Lcom/glympse/android/core/GPrimitive;)V

    invoke-virtual {v1, v0}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 350
    invoke-direct {p0, v1}, Lcom/glympse/android/lib/d;->a(Lcom/glympse/android/hal/GVector;)V

    goto :goto_0
.end method

.method private a(Lcom/glympse/android/hal/GVector;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/glympse/android/hal/GVector",
            "<",
            "Lcom/glympse/android/lib/GApiEndpoint;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 372
    new-instance v0, Lcom/glympse/android/lib/r;

    iget-object v1, p0, Lcom/glympse/android/lib/d;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    new-instance v2, Lcom/glympse/android/lib/d$b;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/glympse/android/lib/d$b;-><init>(Lcom/glympse/android/lib/d$1;)V

    invoke-direct {v0, v1, v2, p1}, Lcom/glympse/android/lib/r;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/bf;Lcom/glympse/android/hal/GVector;)V

    .line 374
    new-instance v1, Lcom/glympse/android/lib/d$a;

    iget-object v2, p0, Lcom/glympse/android/lib/d;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct {v1, v2, v0}, Lcom/glympse/android/lib/d$a;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/r;)V

    iput-object v1, p0, Lcom/glympse/android/lib/d;->gq:Lcom/glympse/android/lib/HttpJob;

    .line 375
    iget-object v0, p0, Lcom/glympse/android/lib/d;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getJobQueue()Lcom/glympse/android/lib/GJobQueue;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/lib/d;->gq:Lcom/glympse/android/lib/HttpJob;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GJobQueue;->addJob(Lcom/glympse/android/lib/GJob;)V

    .line 376
    return-void
.end method

.method private a(Lcom/glympse/android/lib/GApiEndpoint;)V
    .locals 1

    .prologue
    .line 365
    new-instance v0, Lcom/glympse/android/hal/GVector;

    invoke-direct {v0}, Lcom/glympse/android/hal/GVector;-><init>()V

    .line 366
    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 367
    invoke-direct {p0, v0}, Lcom/glympse/android/lib/d;->a(Lcom/glympse/android/hal/GVector;)V

    .line 368
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 355
    new-instance v0, Lcom/glympse/android/lib/b;

    iget-object v1, p0, Lcom/glympse/android/lib/d;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct {p0}, Lcom/glympse/android/lib/d;->am()Lcom/glympse/android/lib/GAccountListener;

    move-result-object v2

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/glympse/android/lib/b;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/GAccountListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/d;->a(Lcom/glympse/android/lib/GApiEndpoint;)V

    .line 356
    return-void
.end method

.method private ak()V
    .locals 3

    .prologue
    .line 297
    new-instance v0, Lcom/glympse/android/lib/b;

    iget-object v1, p0, Lcom/glympse/android/lib/d;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct {p0}, Lcom/glympse/android/lib/d;->am()Lcom/glympse/android/lib/GAccountListener;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/glympse/android/lib/b;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/GAccountListener;)V

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/d;->a(Lcom/glympse/android/lib/GApiEndpoint;)V

    .line 298
    return-void
.end method

.method private al()Lcom/glympse/android/lib/GAccountImportListener;
    .locals 1

    .prologue
    .line 579
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GAccountImportListener;

    return-object v0
.end method

.method private am()Lcom/glympse/android/lib/GAccountListener;
    .locals 1

    .prologue
    .line 584
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GAccountListener;

    return-object v0
.end method

.method private d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 360
    new-instance v0, Lcom/glympse/android/lib/c;

    iget-object v1, p0, Lcom/glympse/android/lib/d;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct {p0}, Lcom/glympse/android/lib/d;->am()Lcom/glympse/android/lib/GAccountListener;

    move-result-object v2

    invoke-direct {v0, v1, p1, p2, v2}, Lcom/glympse/android/lib/c;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Ljava/lang/String;Ljava/lang/String;Lcom/glympse/android/lib/GAccountListener;)V

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/d;->a(Lcom/glympse/android/lib/GApiEndpoint;)V

    .line 361
    return-void
.end method


# virtual methods
.method public accountCreated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 157
    iget-object v0, p0, Lcom/glympse/android/lib/d;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-nez v0, :cond_1

    .line 171
    :cond_0
    :goto_0
    return-void

    .line 163
    :cond_1
    iput-object v1, p0, Lcom/glympse/android/lib/d;->gp:Lcom/glympse/android/lib/GAccountProvider;

    .line 164
    iput-object v1, p0, Lcom/glympse/android/lib/d;->gq:Lcom/glympse/android/lib/HttpJob;

    .line 167
    iget-object v0, p0, Lcom/glympse/android/lib/d;->gb:Lcom/glympse/android/lib/GAccountListener;

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/glympse/android/lib/d;->gb:Lcom/glympse/android/lib/GAccountListener;

    invoke-interface {v0, p1, p2}, Lcom/glympse/android/lib/GAccountListener;->accountCreated(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public accountImported(Lcom/glympse/android/core/GPrimitive;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 257
    iget-object v0, p0, Lcom/glympse/android/lib/d;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-nez v0, :cond_0

    .line 268
    :goto_0
    return-void

    .line 263
    :cond_0
    iput-object v1, p0, Lcom/glympse/android/lib/d;->gp:Lcom/glympse/android/lib/GAccountProvider;

    .line 264
    iput-object v1, p0, Lcom/glympse/android/lib/d;->gq:Lcom/glympse/android/lib/HttpJob;

    .line 267
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/d;->a(Lcom/glympse/android/core/GPrimitive;)V

    goto :goto_0
.end method

.method public accountImported(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 251
    invoke-virtual {p0, p1, p2}, Lcom/glympse/android/lib/d;->accountCreated(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    return-void
.end method

.method public accountImported(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 273
    iget-object v0, p0, Lcom/glympse/android/lib/d;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-nez v0, :cond_0

    .line 284
    :goto_0
    return-void

    .line 279
    :cond_0
    iput-object v1, p0, Lcom/glympse/android/lib/d;->gp:Lcom/glympse/android/lib/GAccountProvider;

    .line 280
    iput-object v1, p0, Lcom/glympse/android/lib/d;->gq:Lcom/glympse/android/lib/HttpJob;

    .line 283
    invoke-direct {p0, p1, p2, p3}, Lcom/glympse/android/lib/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public cancel()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 140
    iget-object v0, p0, Lcom/glympse/android/lib/d;->gp:Lcom/glympse/android/lib/GAccountProvider;

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/glympse/android/lib/d;->gp:Lcom/glympse/android/lib/GAccountProvider;

    invoke-interface {v0}, Lcom/glympse/android/lib/GAccountProvider;->cancel()V

    .line 143
    iput-object v1, p0, Lcom/glympse/android/lib/d;->gp:Lcom/glympse/android/lib/GAccountProvider;

    .line 147
    :cond_0
    iput-object v1, p0, Lcom/glympse/android/lib/d;->gq:Lcom/glympse/android/lib/HttpJob;

    .line 148
    return-void
.end method

.method public create(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 81
    iget-object v0, p0, Lcom/glympse/android/lib/d;->gp:Lcom/glympse/android/lib/GAccountProvider;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/d;->gq:Lcom/glympse/android/lib/HttpJob;

    if-eqz v0, :cond_1

    .line 83
    :cond_0
    const/4 v0, 0x0

    .line 120
    :goto_0
    return v0

    .line 87
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/d;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getAccountProfile()Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 88
    if-eqz v0, :cond_3

    .line 91
    new-instance v1, Lcom/glympse/android/lib/d$f;

    iget-object v2, p0, Lcom/glympse/android/lib/d;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v2}, Lcom/glympse/android/lib/GGlympsePrivate;->getHandler()Lcom/glympse/android/core/GHandler;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/glympse/android/lib/d$f;-><init>(Lcom/glympse/android/core/GHandler;Lcom/glympse/android/core/GPrimitive;)V

    iput-object v1, p0, Lcom/glympse/android/lib/d;->gp:Lcom/glympse/android/lib/GAccountProvider;

    .line 106
    :goto_1
    iget-object v0, p0, Lcom/glympse/android/lib/d;->gp:Lcom/glympse/android/lib/GAccountProvider;

    invoke-direct {p0}, Lcom/glympse/android/lib/d;->al()Lcom/glympse/android/lib/GAccountImportListener;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GAccountProvider;->setAccountListener(Lcom/glympse/android/lib/GAccountImportListener;)V

    .line 110
    iget-object v0, p0, Lcom/glympse/android/lib/d;->gp:Lcom/glympse/android/lib/GAccountProvider;

    invoke-interface {v0, p1}, Lcom/glympse/android/lib/GAccountProvider;->create(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 113
    iput-object v3, p0, Lcom/glympse/android/lib/d;->gp:Lcom/glympse/android/lib/GAccountProvider;

    .line 116
    invoke-direct {p0}, Lcom/glympse/android/lib/d;->ak()V

    .line 120
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 93
    :cond_3
    iget-object v0, p0, Lcom/glympse/android/lib/d;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->isAccountSharingEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 96
    iget-object v0, p0, Lcom/glympse/android/lib/d;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 97
    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getContextHolder()Lcom/glympse/android/hal/GContextHolder;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/hal/GContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/lib/d;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->getHandler()Lcom/glympse/android/core/GHandler;

    move-result-object v1

    iget-object v2, p0, Lcom/glympse/android/lib/d;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v2}, Lcom/glympse/android/lib/GGlympsePrivate;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    .line 96
    invoke-static {v0, v1, v2}, Lcom/glympse/android/hal/HalFactory;->createAccountImporter(Landroid/content/Context;Lcom/glympse/android/core/GHandler;Ljava/lang/String;)Lcom/glympse/android/lib/GAccountProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/d;->gp:Lcom/glympse/android/lib/GAccountProvider;

    goto :goto_1

    .line 102
    :cond_4
    new-instance v0, Lcom/glympse/android/lib/d$d;

    invoke-direct {v0, v3}, Lcom/glympse/android/lib/d$d;-><init>(Lcom/glympse/android/lib/d$1;)V

    iput-object v0, p0, Lcom/glympse/android/lib/d;->gp:Lcom/glympse/android/lib/GAccountProvider;

    goto :goto_1
.end method

.method public failedToCreate(ZILcom/glympse/android/api/GServerError;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 181
    iget-object v0, p0, Lcom/glympse/android/lib/d;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-nez v0, :cond_1

    .line 207
    :cond_0
    :goto_0
    return-void

    .line 187
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/d;->gp:Lcom/glympse/android/lib/GAccountProvider;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 190
    :goto_1
    iput-object v1, p0, Lcom/glympse/android/lib/d;->gp:Lcom/glympse/android/lib/GAccountProvider;

    .line 191
    iput-object v1, p0, Lcom/glympse/android/lib/d;->gq:Lcom/glympse/android/lib/HttpJob;

    .line 194
    if-eqz v0, :cond_3

    if-nez p1, :cond_3

    .line 197
    invoke-direct {p0}, Lcom/glympse/android/lib/d;->ak()V

    goto :goto_0

    .line 187
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 202
    :cond_3
    iget-object v0, p0, Lcom/glympse/android/lib/d;->gb:Lcom/glympse/android/lib/GAccountListener;

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/glympse/android/lib/d;->gb:Lcom/glympse/android/lib/GAccountListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/glympse/android/lib/GAccountListener;->failedToCreate(ZILcom/glympse/android/api/GServerError;)V

    goto :goto_0
.end method

.method public failedToImport(ZI)V
    .locals 1

    .prologue
    .line 288
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/glympse/android/lib/d;->failedToCreate(ZILcom/glympse/android/api/GServerError;)V

    .line 289
    return-void
.end method

.method public failedToLogin(ILcom/glympse/android/api/GServerError;)V
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcom/glympse/android/lib/d;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-nez v0, :cond_1

    .line 243
    :cond_0
    :goto_0
    return-void

    .line 236
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/d;->gq:Lcom/glympse/android/lib/HttpJob;

    .line 239
    iget-object v0, p0, Lcom/glympse/android/lib/d;->gb:Lcom/glympse/android/lib/GAccountListener;

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/glympse/android/lib/d;->gb:Lcom/glympse/android/lib/GAccountListener;

    invoke-interface {v0, p1, p2}, Lcom/glympse/android/lib/GAccountListener;->failedToLogin(ILcom/glympse/android/api/GServerError;)V

    goto :goto_0
.end method

.method public loggedIn(Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 212
    iget-object v0, p0, Lcom/glympse/android/lib/d;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-nez v0, :cond_1

    .line 225
    :cond_0
    :goto_0
    return-void

    .line 218
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/d;->gq:Lcom/glympse/android/lib/HttpJob;

    .line 221
    iget-object v0, p0, Lcom/glympse/android/lib/d;->gb:Lcom/glympse/android/lib/GAccountListener;

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/glympse/android/lib/d;->gb:Lcom/glympse/android/lib/GAccountListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/glympse/android/lib/GAccountListener;->loggedIn(Ljava/lang/String;J)V

    goto :goto_0
.end method

.method public login(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/glympse/android/lib/d;->gp:Lcom/glympse/android/lib/GAccountProvider;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/d;->gq:Lcom/glympse/android/lib/HttpJob;

    if-eqz v0, :cond_1

    .line 128
    :cond_0
    const/4 v0, 0x0

    .line 134
    :goto_0
    return v0

    .line 132
    :cond_1
    invoke-direct {p0, p2, p3}, Lcom/glympse/android/lib/d;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public retryAccountCreate(Lcom/glympse/android/core/GPrimitive;)V
    .locals 0

    .prologue
    .line 175
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/d;->a(Lcom/glympse/android/core/GPrimitive;)V

    .line 176
    return-void
.end method

.method public setAccountListener(Lcom/glympse/android/lib/GAccountListener;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/glympse/android/lib/d;->gb:Lcom/glympse/android/lib/GAccountListener;

    .line 76
    return-void
.end method

.method public start(Lcom/glympse/android/lib/GGlympsePrivate;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/glympse/android/lib/d;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 62
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 67
    invoke-virtual {p0}, Lcom/glympse/android/lib/d;->cancel()V

    .line 69
    iput-object v0, p0, Lcom/glympse/android/lib/d;->gb:Lcom/glympse/android/lib/GAccountListener;

    .line 70
    iput-object v0, p0, Lcom/glympse/android/lib/d;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 71
    return-void
.end method
