.class Lcom/glympse/android/lib/ar;
.super Ljava/lang/Object;
.source "DirectionsManager.java"

# interfaces
.implements Lcom/glympse/android/api/GEventListener;
.implements Lcom/glympse/android/hal/GActivityListener;
.implements Lcom/glympse/android/lib/GDirectionsManagerPrivate;


# instance fields
.field private _glympse:Lcom/glympse/android/lib/GGlympsePrivate;

.field private kA:I

.field private kB:Lcom/glympse/android/hal/GActivityProvider;

.field private kC:Z

.field private kf:I

.field private kv:Lcom/glympse/android/api/GHistoryManager;

.field private kw:Z

.field private kx:Lcom/glympse/android/hal/GHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GHashtable",
            "<",
            "Lcom/glympse/android/lib/GTicketPrivate;",
            "Lcom/glympse/android/lib/bl;",
            ">;"
        }
    .end annotation
.end field

.field private ky:Z

.field private kz:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-boolean v0, p0, Lcom/glympse/android/lib/ar;->kw:Z

    .line 49
    iput v0, p0, Lcom/glympse/android/lib/ar;->kf:I

    .line 50
    iput-boolean v0, p0, Lcom/glympse/android/lib/ar;->ky:Z

    .line 51
    iput-boolean v0, p0, Lcom/glympse/android/lib/ar;->kz:Z

    .line 52
    iput v0, p0, Lcom/glympse/android/lib/ar;->kA:I

    .line 53
    iput-boolean v0, p0, Lcom/glympse/android/lib/ar;->kC:Z

    .line 54
    return-void
.end method

.method private a(Lcom/glympse/android/lib/GTicketPrivate;Lcom/glympse/android/lib/bl;)V
    .locals 1

    .prologue
    .line 522
    invoke-interface {p2, p1}, Lcom/glympse/android/lib/bl;->removeTicket(Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 525
    iget-object v0, p0, Lcom/glympse/android/lib/ar;->kx:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 528
    invoke-interface {p2}, Lcom/glympse/android/lib/bl;->bE()Z

    move-result v0

    if-nez v0, :cond_0

    .line 530
    invoke-interface {p2}, Lcom/glympse/android/lib/bl;->stop()V

    .line 532
    :cond_0
    return-void
.end method

.method private a(Lcom/glympse/android/core/GLatLng;Lcom/glympse/android/core/GLatLng;)Z
    .locals 8

    .prologue
    .line 568
    .line 569
    invoke-interface {p1}, Lcom/glympse/android/core/GLatLng;->getLatitude()D

    move-result-wide v0

    invoke-interface {p1}, Lcom/glympse/android/core/GLatLng;->getLongitude()D

    move-result-wide v2

    .line 570
    invoke-interface {p2}, Lcom/glympse/android/core/GLatLng;->getLatitude()D

    move-result-wide v4

    invoke-interface {p2}, Lcom/glympse/android/core/GLatLng;->getLongitude()D

    move-result-wide v6

    .line 568
    invoke-static/range {v0 .. v7}, Lcom/glympse/android/lib/Location;->distance(DDDD)D

    move-result-wide v0

    double-to-int v0, v0

    .line 571
    const/16 v1, 0x96

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lcom/glympse/android/lib/GTicketPrivate;)V
    .locals 3

    .prologue
    .line 465
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/ar;->d(Lcom/glympse/android/lib/GTicketPrivate;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 468
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/ar;->e(Lcom/glympse/android/lib/GTicketPrivate;)Lcom/glympse/android/lib/bl;

    move-result-object v0

    .line 469
    if-eqz v0, :cond_1

    .line 472
    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getDestination()Lcom/glympse/android/api/GPlace;

    move-result-object v1

    invoke-interface {v0}, Lcom/glympse/android/lib/bl;->getDestination()Lcom/glympse/android/core/GLatLng;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/glympse/android/lib/ar;->a(Lcom/glympse/android/core/GLatLng;Lcom/glympse/android/core/GLatLng;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 475
    invoke-direct {p0, p1, v0}, Lcom/glympse/android/lib/ar;->a(Lcom/glympse/android/lib/GTicketPrivate;Lcom/glympse/android/lib/bl;)V

    .line 478
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/ar;->c(Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 499
    :cond_0
    :goto_0
    return-void

    .line 484
    :cond_1
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/ar;->c(Lcom/glympse/android/lib/GTicketPrivate;)V

    goto :goto_0

    .line 492
    :cond_2
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/ar;->e(Lcom/glympse/android/lib/GTicketPrivate;)Lcom/glympse/android/lib/bl;

    move-result-object v0

    .line 493
    if-eqz v0, :cond_0

    .line 496
    invoke-direct {p0, p1, v0}, Lcom/glympse/android/lib/ar;->a(Lcom/glympse/android/lib/GTicketPrivate;Lcom/glympse/android/lib/bl;)V

    goto :goto_0
.end method

.method private b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 411
    check-cast p1, Lcom/glympse/android/lib/GTicketPrivate;

    .line 414
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/ar;->b(Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 415
    return-void
.end method

.method private bb()V
    .locals 6

    .prologue
    .line 424
    new-instance v2, Lcom/glympse/android/lib/fu;

    iget v0, p0, Lcom/glympse/android/lib/ar;->kf:I

    const/4 v1, 0x0

    invoke-direct {v2, v0, v1}, Lcom/glympse/android/lib/fu;-><init>(ILcom/glympse/android/core/GPrimitive;)V

    .line 427
    iget-object v0, p0, Lcom/glympse/android/lib/ar;->kv:Lcom/glympse/android/api/GHistoryManager;

    invoke-interface {v0}, Lcom/glympse/android/api/GHistoryManager;->getTickets()Lcom/glympse/android/core/GArray;

    move-result-object v3

    .line 428
    invoke-interface {v3}, Lcom/glympse/android/core/GArray;->length()I

    move-result v4

    .line 429
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    .line 431
    invoke-interface {v3, v1}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GTicketPrivate;

    .line 432
    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketPrivate;->isActive()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 434
    invoke-interface {v0, v2}, Lcom/glympse/android/lib/GTicketPrivate;->updateTravelMode(Lcom/glympse/android/api/GTravelMode;)V

    .line 429
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 441
    :cond_0
    return-void
.end method

.method private bc()V
    .locals 3

    .prologue
    .line 449
    iget-boolean v0, p0, Lcom/glympse/android/lib/ar;->kw:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/ar;->kx:Lcom/glympse/android/hal/GHashtable;

    if-nez v0, :cond_1

    .line 460
    :cond_0
    return-void

    .line 454
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/ar;->kx:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GHashtable;->keys()Ljava/util/Enumeration;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 456
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GTicketPrivate;

    .line 457
    iget-object v2, p0, Lcom/glympse/android/lib/ar;->kx:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v2, v0}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/bl;

    .line 458
    invoke-interface {v0}, Lcom/glympse/android/lib/bl;->bF()V

    goto :goto_0
.end method

.method private bd()Z
    .locals 2

    .prologue
    .line 580
    iget-object v0, p0, Lcom/glympse/android/lib/ar;->kB:Lcom/glympse/android/hal/GActivityProvider;

    if-nez v0, :cond_0

    .line 582
    iget-object v0, p0, Lcom/glympse/android/lib/ar;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getContextHolder()Lcom/glympse/android/hal/GContextHolder;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/hal/GContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/glympse/android/hal/HalFactory;->createActivityProvider(Landroid/content/Context;)Lcom/glympse/android/hal/GActivityProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/ar;->kB:Lcom/glympse/android/hal/GActivityProvider;

    .line 584
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/ar;->kB:Lcom/glympse/android/hal/GActivityProvider;

    invoke-interface {v0}, Lcom/glympse/android/hal/GActivityProvider;->isSupported()Z

    move-result v0

    .line 585
    if-nez v0, :cond_1

    .line 587
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/glympse/android/lib/ar;->kB:Lcom/glympse/android/hal/GActivityProvider;

    .line 589
    :cond_1
    return v0
.end method

.method private be()V
    .locals 1

    .prologue
    .line 594
    invoke-direct {p0}, Lcom/glympse/android/lib/ar;->bf()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 596
    invoke-direct {p0}, Lcom/glympse/android/lib/ar;->bg()V

    .line 602
    :goto_0
    return-void

    .line 600
    :cond_0
    invoke-direct {p0}, Lcom/glympse/android/lib/ar;->bh()V

    goto :goto_0
.end method

.method private bf()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 607
    iget-boolean v1, p0, Lcom/glympse/android/lib/ar;->ky:Z

    if-nez v1, :cond_1

    .line 624
    :cond_0
    :goto_0
    return v0

    .line 613
    :cond_1
    iget-object v1, p0, Lcom/glympse/android/lib/ar;->kB:Lcom/glympse/android/hal/GActivityProvider;

    if-eqz v1, :cond_0

    .line 619
    iget-object v1, p0, Lcom/glympse/android/lib/ar;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/glympse/android/lib/ar;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->isStarted()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/glympse/android/lib/ar;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->isSharing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 624
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private bg()V
    .locals 4

    .prologue
    .line 629
    iget-boolean v0, p0, Lcom/glympse/android/lib/ar;->kz:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/ar;->kB:Lcom/glympse/android/hal/GActivityProvider;

    if-nez v0, :cond_1

    .line 636
    :cond_0
    :goto_0
    return-void

    .line 634
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/ar;->kB:Lcom/glympse/android/hal/GActivityProvider;

    invoke-direct {p0}, Lcom/glympse/android/lib/ar;->bj()Lcom/glympse/android/hal/GActivityListener;

    move-result-object v1

    const-wide/16 v2, 0x7530

    invoke-interface {v0, v1, v2, v3}, Lcom/glympse/android/hal/GActivityProvider;->registerUpdates(Lcom/glympse/android/hal/GActivityListener;J)V

    .line 635
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/glympse/android/lib/ar;->kz:Z

    goto :goto_0
.end method

.method private bh()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 640
    iget-boolean v0, p0, Lcom/glympse/android/lib/ar;->kz:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/ar;->kB:Lcom/glympse/android/hal/GActivityProvider;

    if-nez v0, :cond_1

    .line 650
    :cond_0
    :goto_0
    return-void

    .line 645
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/ar;->kB:Lcom/glympse/android/hal/GActivityProvider;

    invoke-direct {p0}, Lcom/glympse/android/lib/ar;->bj()Lcom/glympse/android/hal/GActivityListener;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/hal/GActivityProvider;->removeUpdates(Lcom/glympse/android/hal/GActivityListener;)V

    .line 647
    iput v2, p0, Lcom/glympse/android/lib/ar;->kA:I

    .line 648
    iput v2, p0, Lcom/glympse/android/lib/ar;->kf:I

    .line 649
    iput-boolean v2, p0, Lcom/glympse/android/lib/ar;->kz:Z

    goto :goto_0
.end method

.method private bi()Lcom/glympse/android/api/GEventListener;
    .locals 1

    .prologue
    .line 681
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GEventListener;

    return-object v0
.end method

.method private bj()Lcom/glympse/android/hal/GActivityListener;
    .locals 1

    .prologue
    .line 686
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/hal/GActivityListener;

    return-object v0
.end method

.method private c(Lcom/glympse/android/lib/GTicketPrivate;)V
    .locals 2

    .prologue
    .line 504
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/ar;->f(Lcom/glympse/android/lib/GTicketPrivate;)Lcom/glympse/android/lib/bl;

    move-result-object v0

    .line 505
    if-nez v0, :cond_0

    .line 508
    new-instance v0, Lcom/glympse/android/lib/fi;

    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getDestination()Lcom/glympse/android/api/GPlace;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/fi;-><init>(Lcom/glympse/android/core/GLatLng;)V

    .line 509
    iget-object v1, p0, Lcom/glympse/android/lib/ar;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/bl;->start(Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 513
    :cond_0
    invoke-interface {v0, p1}, Lcom/glympse/android/lib/bl;->addTicket(Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 516
    iget-object v1, p0, Lcom/glympse/android/lib/ar;->kx:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v1, p1, v0}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 517
    return-void
.end method

.method private d(Lcom/glympse/android/lib/GTicketPrivate;)Z
    .locals 1

    .prologue
    .line 540
    .line 541
    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->isActive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 542
    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 543
    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getDestination()Lcom/glympse/android/api/GPlace;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 544
    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getGlympse()Lcom/glympse/android/lib/GGlympsePrivate;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 540
    :goto_0
    return v0

    .line 544
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e(Lcom/glympse/android/lib/GTicketPrivate;)Lcom/glympse/android/lib/bl;
    .locals 1

    .prologue
    .line 549
    iget-object v0, p0, Lcom/glympse/android/lib/ar;->kx:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/bl;

    return-object v0
.end method

.method private f(Lcom/glympse/android/lib/GTicketPrivate;)Lcom/glympse/android/lib/bl;
    .locals 4

    .prologue
    .line 554
    iget-object v0, p0, Lcom/glympse/android/lib/ar;->kx:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GHashtable;->keys()Ljava/util/Enumeration;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 556
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GTicketPrivate;

    .line 557
    iget-object v2, p0, Lcom/glympse/android/lib/ar;->kx:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v2, v0}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/bl;

    .line 558
    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getDestination()Lcom/glympse/android/api/GPlace;

    move-result-object v2

    invoke-interface {v0}, Lcom/glympse/android/lib/bl;->getDestination()Lcom/glympse/android/core/GLatLng;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/glympse/android/lib/ar;->a(Lcom/glympse/android/core/GLatLng;Lcom/glympse/android/core/GLatLng;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 563
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private n(I)V
    .locals 2

    .prologue
    .line 97
    iget v0, p0, Lcom/glympse/android/lib/ar;->kA:I

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/ar;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-nez v0, :cond_1

    .line 115
    :cond_0
    :goto_0
    return-void

    .line 103
    :cond_1
    iput p1, p0, Lcom/glympse/android/lib/ar;->kA:I

    .line 106
    iget-object v0, p0, Lcom/glympse/android/lib/ar;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getLocationManagerPrivate()Lcom/glympse/android/lib/GLocationManagerPrivate;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GLocationManagerPrivate;->updateProfile(Z)V

    .line 111
    const/4 v0, 0x4

    if-eq v0, p1, :cond_0

    .line 113
    iget-object v0, p0, Lcom/glympse/android/lib/ar;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getServerPost()Lcom/glympse/android/lib/GServerPost;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/lib/GServerPost;->resetPostTimer()V

    goto :goto_0
.end method

.method private o(I)V
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/glympse/android/lib/ar;->kf:I

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/ar;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-nez v0, :cond_1

    .line 167
    :cond_0
    :goto_0
    return-void

    .line 160
    :cond_1
    iput p1, p0, Lcom/glympse/android/lib/ar;->kf:I

    .line 163
    invoke-direct {p0}, Lcom/glympse/android/lib/ar;->bb()V

    .line 166
    invoke-direct {p0}, Lcom/glympse/android/lib/ar;->bc()V

    goto :goto_0
.end method

.method private p(I)I
    .locals 1

    .prologue
    .line 290
    packed-switch p1, :pswitch_data_0

    .line 305
    iget v0, p0, Lcom/glympse/android/lib/ar;->kf:I

    :goto_0
    return v0

    .line 293
    :pswitch_0
    iget v0, p0, Lcom/glympse/android/lib/ar;->kf:I

    goto :goto_0

    .line 295
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 297
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 299
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 301
    :pswitch_4
    iget v0, p0, Lcom/glympse/android/lib/ar;->kf:I

    goto :goto_0

    .line 303
    :pswitch_5
    iget v0, p0, Lcom/glympse/android/lib/ar;->kf:I

    goto :goto_0

    .line 290
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private static q(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 658
    packed-switch p0, :pswitch_data_0

    .line 671
    const-string v0, "unknown"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 661
    :pswitch_0
    const-string v0, "in_vehicle"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 663
    :pswitch_1
    const-string v0, "on_bicycle"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 665
    :pswitch_2
    const-string v0, "on_foot"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 667
    :pswitch_3
    const-string v0, "still"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 669
    :pswitch_4
    const-string v0, "tilting"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 658
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public a(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 331
    const/4 v0, 0x1

    if-ne v0, p2, :cond_3

    .line 333
    and-int/lit16 v0, p3, 0x80

    if-eqz v0, :cond_1

    .line 336
    invoke-direct {p0}, Lcom/glympse/android/lib/ar;->be()V

    .line 339
    iget-object v0, p0, Lcom/glympse/android/lib/ar;->kv:Lcom/glympse/android/api/GHistoryManager;

    invoke-direct {p0}, Lcom/glympse/android/lib/ar;->bi()Lcom/glympse/android/api/GEventListener;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/api/GHistoryManager;->simulateAddedEvents(Lcom/glympse/android/api/GEventListener;)V

    .line 386
    :cond_0
    :goto_0
    return-void

    .line 341
    :cond_1
    const/high16 v0, 0x20000

    and-int/2addr v0, p3

    if-eqz v0, :cond_2

    move-object v0, p4

    .line 343
    check-cast v0, Lcom/glympse/android/api/GTicket;

    .line 346
    invoke-interface {v0}, Lcom/glympse/android/api/GTicket;->getState()I

    move-result v1

    and-int/lit8 v1, v1, 0x12

    if-eqz v1, :cond_0

    .line 348
    invoke-direct {p0, p4}, Lcom/glympse/android/lib/ar;->b(Ljava/lang/Object;)V

    .line 351
    invoke-direct {p0}, Lcom/glympse/android/lib/ar;->bi()Lcom/glympse/android/api/GEventListener;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/api/GTicket;->addListener(Lcom/glympse/android/api/GEventListener;)Z

    goto :goto_0

    .line 354
    :cond_2
    const/high16 v0, 0x40000

    and-int/2addr v0, p3

    if-eqz v0, :cond_0

    .line 356
    invoke-direct {p0, p4}, Lcom/glympse/android/lib/ar;->b(Ljava/lang/Object;)V

    .line 359
    check-cast p4, Lcom/glympse/android/api/GTicket;

    .line 360
    invoke-direct {p0}, Lcom/glympse/android/lib/ar;->bi()Lcom/glympse/android/api/GEventListener;

    move-result-object v0

    invoke-interface {p4, v0}, Lcom/glympse/android/api/GTicket;->removeListener(Lcom/glympse/android/api/GEventListener;)Z

    goto :goto_0

    .line 363
    :cond_3
    const/4 v0, 0x4

    if-ne v0, p2, :cond_0

    .line 365
    and-int/lit8 v0, p3, 0x40

    if-eqz v0, :cond_4

    .line 367
    invoke-direct {p0, p4}, Lcom/glympse/android/lib/ar;->b(Ljava/lang/Object;)V

    .line 369
    :cond_4
    and-int/lit8 v0, p3, 0x8

    if-eqz v0, :cond_5

    .line 371
    invoke-direct {p0, p4}, Lcom/glympse/android/lib/ar;->b(Ljava/lang/Object;)V

    .line 373
    :cond_5
    const/high16 v0, 0x1000000

    and-int/2addr v0, p3

    if-eqz v0, :cond_6

    .line 375
    invoke-direct {p0, p4}, Lcom/glympse/android/lib/ar;->b(Ljava/lang/Object;)V

    .line 377
    :cond_6
    and-int/lit8 v0, p3, 0x2

    if-eqz v0, :cond_0

    .line 379
    invoke-direct {p0, p4}, Lcom/glympse/android/lib/ar;->b(Ljava/lang/Object;)V

    .line 382
    check-cast p4, Lcom/glympse/android/api/GTicket;

    .line 383
    invoke-direct {p0}, Lcom/glympse/android/lib/ar;->bi()Lcom/glympse/android/api/GEventListener;

    move-result-object v0

    invoke-interface {p4, v0}, Lcom/glympse/android/api/GTicket;->removeListener(Lcom/glympse/android/api/GEventListener;)Z

    goto :goto_0
.end method

.method public activityRecognized(II)V
    .locals 3

    .prologue
    .line 269
    const/4 v0, 0x3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[DirectionsManager.activityRecognized] Activity: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/glympse/android/lib/ar;->q(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 272
    packed-switch p1, :pswitch_data_0

    .line 281
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/ar;->n(I)V

    .line 284
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/ar;->p(I)I

    move-result v0

    .line 285
    invoke-direct {p0, v0}, Lcom/glympse/android/lib/ar;->o(I)V

    .line 286
    :pswitch_0
    return-void

    .line 272
    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
    .end packed-switch
.end method

.method public b(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 390
    const v0, 0x10002

    if-ne v0, p2, :cond_1

    .line 392
    and-int/lit16 v0, p3, 0x2000

    if-eqz v0, :cond_0

    .line 395
    invoke-direct {p0}, Lcom/glympse/android/lib/ar;->be()V

    .line 397
    :cond_0
    and-int/lit16 v0, p3, 0x4000

    if-eqz v0, :cond_1

    .line 400
    invoke-direct {p0}, Lcom/glympse/android/lib/ar;->be()V

    .line 403
    :cond_1
    return-void
.end method

.method public enableActivityRecognition(Z)V
    .locals 2

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/glympse/android/lib/ar;->ky:Z

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/ar;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-nez v0, :cond_1

    .line 86
    :cond_0
    :goto_0
    return-void

    .line 69
    :cond_1
    if-eqz p1, :cond_2

    invoke-direct {p0}, Lcom/glympse/android/lib/ar;->bd()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    :cond_2
    if-eqz p1, :cond_3

    iget-boolean v0, p0, Lcom/glympse/android/lib/ar;->kC:Z

    if-nez v0, :cond_3

    .line 77
    iget-object v0, p0, Lcom/glympse/android/lib/ar;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct {p0}, Lcom/glympse/android/lib/ar;->bi()Lcom/glympse/android/api/GEventListener;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GGlympsePrivate;->addListener(Lcom/glympse/android/api/GEventListener;)Z

    .line 78
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/glympse/android/lib/ar;->kC:Z

    .line 82
    :cond_3
    iput-boolean p1, p0, Lcom/glympse/android/lib/ar;->ky:Z

    .line 85
    invoke-direct {p0}, Lcom/glympse/android/lib/ar;->be()V

    goto :goto_0
.end method

.method public eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 317
    iget-boolean v0, p0, Lcom/glympse/android/lib/ar;->kw:Z

    if-eqz v0, :cond_0

    .line 319
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/glympse/android/lib/ar;->a(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 323
    :cond_0
    iget-boolean v0, p0, Lcom/glympse/android/lib/ar;->ky:Z

    if-eqz v0, :cond_1

    .line 325
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/glympse/android/lib/ar;->b(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 327
    :cond_1
    return-void
.end method

.method public getDeviceActivity()I
    .locals 1

    .prologue
    .line 119
    iget v0, p0, Lcom/glympse/android/lib/ar;->kA:I

    return v0
.end method

.method public getTravelMode()I
    .locals 1

    .prologue
    .line 171
    iget v0, p0, Lcom/glympse/android/lib/ar;->kf:I

    return v0
.end method

.method public isActivityRecognitionEnabled()Z
    .locals 1

    .prologue
    .line 90
    iget-boolean v0, p0, Lcom/glympse/android/lib/ar;->ky:Z

    return v0
.end method

.method public isDeviceStationary()Z
    .locals 2

    .prologue
    .line 124
    iget-boolean v0, p0, Lcom/glympse/android/lib/ar;->ky:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    iget v1, p0, Lcom/glympse/android/lib/ar;->kA:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public queryDirections(Lcom/glympse/android/core/GLatLng;Lcom/glympse/android/core/GLatLng;I)Lcom/glympse/android/api/GDirections;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 177
    iget-object v1, p0, Lcom/glympse/android/lib/ar;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    move-object v1, v0

    .line 200
    :goto_0
    return-object v1

    .line 182
    :cond_1
    new-instance v1, Lcom/glympse/android/lib/am;

    iget-object v2, p0, Lcom/glympse/android/lib/ar;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v2}, Lcom/glympse/android/lib/GGlympsePrivate;->getTime()J

    move-result-wide v2

    move-object v4, p1

    move-object v5, p2

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/glympse/android/lib/am;-><init>(JLcom/glympse/android/core/GLatLng;Lcom/glympse/android/core/GLatLng;I)V

    .line 184
    iget-object v2, p0, Lcom/glympse/android/lib/ar;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v2}, Lcom/glympse/android/lib/GGlympsePrivate;->getConfig()Lcom/glympse/android/api/GConfig;

    move-result-object v2

    invoke-interface {v2}, Lcom/glympse/android/api/GConfig;->getDirectionsProvider()Lcom/glympse/android/core/GPrimitive;

    move-result-object v2

    .line 185
    const/4 v3, 0x2

    invoke-static {v3}, Lcom/glympse/android/core/CoreFactory;->createPrimitive(I)Lcom/glympse/android/core/GPrimitive;

    move-result-object v3

    .line 186
    const-string v4, "src"

    invoke-static {v4}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    long-to-int v4, v4

    .line 187
    const/4 v5, 0x4

    if-ne v4, v5, :cond_3

    .line 189
    iget-object v4, p0, Lcom/glympse/android/lib/ar;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v4}, Lcom/glympse/android/lib/GGlympsePrivate;->getAccessToken()Ljava/lang/String;

    move-result-object v4

    .line 190
    invoke-static {v4}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    move-object v1, v0

    .line 192
    goto :goto_0

    .line 194
    :cond_2
    const-string v0, "base_url"

    invoke-static {v0}, Lcom/glympse/android/core/CoreFactory;->createString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v5, p0, Lcom/glympse/android/lib/ar;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v5}, Lcom/glympse/android/lib/GGlympsePrivate;->getBaseUrl()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v0, v5}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    const-string v0, "access_token"

    invoke-static {v0}, Lcom/glympse/android/core/CoreFactory;->createString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0, v4}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    :cond_3
    iget-object v0, p0, Lcom/glympse/android/lib/ar;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GDirectionsPrivate;->fetchingStarted(Lcom/glympse/android/api/GGlympse;)V

    .line 199
    iget-object v0, p0, Lcom/glympse/android/lib/ar;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getJobQueue()Lcom/glympse/android/lib/GJobQueue;

    move-result-object v0

    invoke-static {v0, v2, v3, v1}, Lcom/glympse/android/lib/as;->a(Lcom/glympse/android/lib/GJobQueue;Lcom/glympse/android/core/GPrimitive;Lcom/glympse/android/core/GPrimitive;Lcom/glympse/android/lib/GDirectionsPrivate;)V

    goto :goto_0
.end method

.method public setActive(Z)V
    .locals 0

    .prologue
    .line 256
    if-eqz p1, :cond_0

    .line 259
    invoke-direct {p0}, Lcom/glympse/android/lib/ar;->bc()V

    .line 261
    :cond_0
    return-void
.end method

.method public setTravelMode(I)V
    .locals 0

    .prologue
    .line 130
    packed-switch p1, :pswitch_data_0

    .line 148
    :goto_0
    :pswitch_0
    return-void

    .line 147
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/ar;->o(I)V

    goto :goto_0

    .line 130
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public start(Lcom/glympse/android/lib/GGlympsePrivate;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 209
    iput-object p1, p0, Lcom/glympse/android/lib/ar;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 210
    iget-object v0, p0, Lcom/glympse/android/lib/ar;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getHistoryManager()Lcom/glympse/android/api/GHistoryManager;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/ar;->kv:Lcom/glympse/android/api/GHistoryManager;

    .line 211
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/glympse/android/lib/ar;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v2}, Lcom/glympse/android/lib/GGlympsePrivate;->getEtaMode()I

    move-result v2

    if-ne v0, v2, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/glympse/android/lib/ar;->kw:Z

    .line 214
    iget-boolean v0, p0, Lcom/glympse/android/lib/ar;->kw:Z

    if-eqz v0, :cond_0

    .line 217
    new-instance v0, Lcom/glympse/android/hal/GHashtable;

    invoke-direct {v0}, Lcom/glympse/android/hal/GHashtable;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/ar;->kx:Lcom/glympse/android/hal/GHashtable;

    .line 220
    iget-object v0, p0, Lcom/glympse/android/lib/ar;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct {p0}, Lcom/glympse/android/lib/ar;->bi()Lcom/glympse/android/api/GEventListener;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/glympse/android/lib/GGlympsePrivate;->addListener(Lcom/glympse/android/api/GEventListener;)Z

    .line 221
    iput-boolean v1, p0, Lcom/glympse/android/lib/ar;->kC:Z

    .line 223
    :cond_0
    return-void

    .line 211
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public stop()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 228
    invoke-direct {p0}, Lcom/glympse/android/lib/ar;->bh()V

    .line 229
    iput-object v3, p0, Lcom/glympse/android/lib/ar;->kB:Lcom/glympse/android/hal/GActivityProvider;

    .line 231
    iget-boolean v0, p0, Lcom/glympse/android/lib/ar;->kw:Z

    if-eqz v0, :cond_1

    .line 234
    iget-object v0, p0, Lcom/glympse/android/lib/ar;->kx:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GHashtable;->keys()Ljava/util/Enumeration;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 236
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GTicketPrivate;

    .line 237
    iget-object v1, p0, Lcom/glympse/android/lib/ar;->kx:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v1, v0}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/glympse/android/lib/bl;

    .line 238
    invoke-interface {v1, v0}, Lcom/glympse/android/lib/bl;->removeTicket(Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 239
    invoke-interface {v1}, Lcom/glympse/android/lib/bl;->stop()V

    goto :goto_0

    .line 241
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/ar;->kx:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GHashtable;->clear()V

    .line 242
    iput-object v3, p0, Lcom/glympse/android/lib/ar;->kx:Lcom/glympse/android/hal/GHashtable;

    .line 245
    :cond_1
    iget-boolean v0, p0, Lcom/glympse/android/lib/ar;->kC:Z

    if-eqz v0, :cond_2

    .line 247
    iget-object v0, p0, Lcom/glympse/android/lib/ar;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct {p0}, Lcom/glympse/android/lib/ar;->bi()Lcom/glympse/android/api/GEventListener;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GGlympsePrivate;->removeListener(Lcom/glympse/android/api/GEventListener;)Z

    .line 250
    :cond_2
    iput-object v3, p0, Lcom/glympse/android/lib/ar;->kv:Lcom/glympse/android/api/GHistoryManager;

    .line 251
    iput-object v3, p0, Lcom/glympse/android/lib/ar;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 252
    return-void
.end method
