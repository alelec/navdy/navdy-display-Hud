.class public Lcom/glympse/android/lib/DebugFormatter;
.super Ljava/lang/Object;
.source "DebugFormatter.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/StringBuilder;Lcom/glympse/android/api/GEventSink;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 422
    const-string v1, "\n\nSUBSCRIBERS:\n"

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 423
    if-nez p1, :cond_0

    const/4 v1, 0x0

    move-object v3, v1

    .line 424
    :goto_0
    if-nez v3, :cond_1

    move v1, v0

    :goto_1
    move v2, v0

    .line 425
    :goto_2
    if-ge v2, v1, :cond_2

    .line 427
    invoke-interface {v3, v2}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GEventListener;

    .line 431
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v4

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 432
    const-string v4, ": "

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 433
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 434
    const-string v0, "\n"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 425
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 423
    :cond_0
    invoke-interface {p1}, Lcom/glympse/android/api/GEventSink;->getListeners()Lcom/glympse/android/core/GArray;

    move-result-object v1

    move-object v3, v1

    goto :goto_0

    .line 424
    :cond_1
    invoke-interface {v3}, Lcom/glympse/android/core/GArray;->length()I

    move-result v1

    goto :goto_1

    .line 436
    :cond_2
    return-void
.end method

.method private static a(Ljava/lang/StringBuilder;Lcom/glympse/android/api/GPlace;Lcom/glympse/android/lib/GGlympsePrivate;)V
    .locals 1

    .prologue
    .line 407
    if-eqz p1, :cond_0

    .line 409
    const-string v0, "\ndest:\t"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 410
    invoke-static {p0, p1}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Lcom/glympse/android/core/GLatLng;)V

    .line 411
    const-string v0, ", "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 412
    invoke-interface {p1}, Lcom/glympse/android/api/GPlace;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 418
    :goto_0
    return-void

    .line 416
    :cond_0
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/StringBuilder;Lcom/glympse/android/core/GLatLng;)V
    .locals 2

    .prologue
    .line 384
    if-eqz p1, :cond_0

    .line 386
    invoke-interface {p1}, Lcom/glympse/android/core/GLatLng;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 387
    const-string v0, ", "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 388
    invoke-interface {p1}, Lcom/glympse/android/core/GLatLng;->getLongitude()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 390
    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/StringBuilder;Lcom/glympse/android/core/GLocation;Lcom/glympse/android/lib/GGlympsePrivate;)V
    .locals 4

    .prologue
    .line 394
    if-eqz p1, :cond_0

    .line 396
    const-string v0, "\nloc:\t"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 397
    invoke-static {p0, p1}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Lcom/glympse/android/core/GLatLng;)V

    .line 398
    const-string v0, ", "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 399
    invoke-interface {p1}, Lcom/glympse/android/core/GLocation;->getTime()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 400
    const-string v0, " ago: "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 401
    invoke-interface {p2}, Lcom/glympse/android/lib/GGlympsePrivate;->getTime()J

    move-result-wide v0

    invoke-interface {p1}, Lcom/glympse/android/core/GLocation;->getTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    invoke-virtual {p0, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 403
    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/StringBuilder;Lcom/glympse/android/core/GPrimitive;Ljava/lang/StringBuilder;)V
    .locals 5

    .prologue
    const/16 v2, 0x64

    const/4 v0, 0x0

    .line 440
    invoke-interface {p1}, Lcom/glympse/android/core/GPrimitive;->type()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 512
    :goto_0
    return-void

    .line 444
    :sswitch_0
    invoke-interface {p1}, Lcom/glympse/android/core/GPrimitive;->getLong()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 449
    :sswitch_1
    invoke-interface {p1}, Lcom/glympse/android/core/GPrimitive;->getDouble()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 454
    :sswitch_2
    invoke-interface {p1}, Lcom/glympse/android/core/GPrimitive;->getBool()Z

    move-result v0

    invoke-static {p0, v0}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Z)V

    goto :goto_0

    .line 459
    :sswitch_3
    invoke-interface {p1}, Lcom/glympse/android/core/GPrimitive;->getString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 464
    :sswitch_4
    invoke-static {v2}, Lcom/glympse/android/core/CoreFactory;->createStringBuilder(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 465
    if-eqz p2, :cond_0

    .line 467
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 469
    :cond_0
    const-string v1, "  "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 470
    invoke-interface {p1}, Lcom/glympse/android/core/GPrimitive;->getArray()Lcom/glympse/android/core/GArray;

    move-result-object v4

    .line 471
    const-string v1, "[\n"

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 472
    if-nez v4, :cond_1

    move v1, v0

    :goto_1
    move v2, v0

    .line 473
    :goto_2
    if-ge v2, v1, :cond_2

    .line 475
    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 476
    invoke-interface {v4, v2}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GPrimitive;

    invoke-static {p0, v0, v3}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Lcom/glympse/android/core/GPrimitive;Ljava/lang/StringBuilder;)V

    .line 477
    const-string v0, ",\n"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 473
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 472
    :cond_1
    invoke-interface {v4}, Lcom/glympse/android/core/GArray;->length()I

    move-result v1

    goto :goto_1

    .line 479
    :cond_2
    if-eqz p2, :cond_3

    .line 481
    invoke-virtual {p0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 483
    :cond_3
    const-string v0, "]"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 488
    :sswitch_5
    invoke-static {v2}, Lcom/glympse/android/core/CoreFactory;->createStringBuilder(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 489
    if-eqz p2, :cond_4

    .line 491
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 493
    :cond_4
    const-string v0, "  "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 494
    const-string v0, "{\n"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 495
    invoke-interface {p1}, Lcom/glympse/android/core/GPrimitive;->getKeys()Ljava/util/Enumeration;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 497
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 498
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 499
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 500
    const-string v3, ": "

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 501
    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    invoke-static {p0, v0, v1}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Lcom/glympse/android/core/GPrimitive;Ljava/lang/StringBuilder;)V

    .line 502
    const-string v0, ",\n"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 504
    :cond_5
    if-eqz p2, :cond_6

    .line 506
    invoke-virtual {p0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 508
    :cond_6
    const-string v0, "}"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 440
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_4
        0x2 -> :sswitch_5
        0x4 -> :sswitch_1
        0x8 -> :sswitch_0
        0x10 -> :sswitch_2
        0x20 -> :sswitch_3
    .end sparse-switch
.end method

.method private static a(Ljava/lang/StringBuilder;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 358
    invoke-static {p1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "<empty>"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :cond_0
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 359
    return-void
.end method

.method private static a(Ljava/lang/StringBuilder;Ljava/lang/String;Lcom/glympse/android/core/GLocationProfile;Lcom/glympse/android/core/GLocationProfile;)V
    .locals 2

    .prologue
    .line 363
    const-string v0, "name :\t"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 364
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 365
    if-ne p2, p3, :cond_0

    .line 367
    const-string v0, " <<<< "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 369
    :cond_0
    const-string v0, "\nmode:\t"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 370
    invoke-interface {p2}, Lcom/glympse/android/core/GLocationProfile;->getMode()I

    move-result v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 371
    const-string v0, "\nsource:\t"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 372
    invoke-interface {p2}, Lcom/glympse/android/core/GLocationProfile;->getSource()I

    move-result v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 373
    const-string v0, "\npriority:\t"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 374
    invoke-interface {p2}, Lcom/glympse/android/core/GLocationProfile;->getPriority()I

    move-result v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 375
    const-string v0, "\ndistance:\t"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 376
    invoke-interface {p2}, Lcom/glympse/android/core/GLocationProfile;->getDistance()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 377
    const-string v0, "\nfreq:\t"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 378
    invoke-interface {p2}, Lcom/glympse/android/core/GLocationProfile;->getFrequency()I

    move-result v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 379
    const-string v0, "\n"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 380
    return-void
.end method

.method private static a(Ljava/lang/StringBuilder;Z)V
    .locals 1

    .prologue
    .line 353
    if-eqz p1, :cond_0

    const-string v0, "true"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 354
    return-void

    .line 353
    :cond_0
    const-string v0, "false"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static general(Lcom/glympse/android/lib/GGlympsePrivate;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 31
    const/16 v0, 0x3e8

    invoke-static {v0}, Lcom/glympse/android/core/CoreFactory;->createStringBuilder(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 33
    invoke-interface {p0}, Lcom/glympse/android/lib/GGlympsePrivate;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    .line 34
    const-string v0, "GLYMPSE"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    const-string v0, "\nPlatform version: "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    invoke-interface {p0}, Lcom/glympse/android/lib/GGlympsePrivate;->getApiVersionFull()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    const-string v0, "\nServer:\t"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    const-string v0, "\nKey:\t\t"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    invoke-interface {p0}, Lcom/glympse/android/lib/GGlympsePrivate;->getApiKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    const-string v0, "\nAccount Sharing:\t"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    invoke-interface {p0}, Lcom/glympse/android/lib/GGlympsePrivate;->isAccountSharingEnabled()Z

    move-result v0

    invoke-static {v4, v0}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Z)V

    .line 43
    const-string v0, "\nShow Siblings:\t"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    invoke-interface {p0}, Lcom/glympse/android/lib/GGlympsePrivate;->areSiblingTicketsAllowed()Z

    move-result v0

    invoke-static {v4, v0}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Z)V

    .line 45
    const-string v0, "\n"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    const-string v0, "\nCONFIG\n"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    invoke-interface {p0}, Lcom/glympse/android/lib/GGlympsePrivate;->getConfig()Lcom/glympse/android/api/GConfig;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GConfigPrivate;

    .line 49
    if-eqz v0, :cond_0

    .line 51
    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GConfigPrivate;->getAccount(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v1

    .line 53
    const-string v2, "Account:\t"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    invoke-static {v1}, Lcom/glympse/android/core/CoreTools;->primitiveToString(Lcom/glympse/android/core/GPrimitive;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 55
    const-string v1, "\nDevice ID:\t"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    invoke-interface {v0}, Lcom/glympse/android/lib/GConfigPrivate;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    const-string v1, "\n"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    :cond_0
    const-string v1, "\nSERVER POST\n"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    invoke-interface {p0}, Lcom/glympse/android/lib/GGlympsePrivate;->getServerPost()Lcom/glympse/android/lib/GServerPost;

    move-result-object v1

    .line 62
    if-eqz v1, :cond_1

    .line 64
    invoke-interface {v1}, Lcom/glympse/android/lib/GServerPost;->getAccessToken()Ljava/lang/String;

    move-result-object v2

    .line 65
    const-string v5, "Token:\t"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    invoke-static {v4, v2}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 67
    const-string v2, "\nisSomeoneWatching:\t"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    invoke-interface {p0}, Lcom/glympse/android/lib/GGlympsePrivate;->getHistoryManager()Lcom/glympse/android/api/GHistoryManager;

    move-result-object v2

    invoke-interface {v2}, Lcom/glympse/android/api/GHistoryManager;->isSomeoneWatching()Z

    move-result v2

    invoke-static {v4, v2}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Z)V

    .line 69
    const-string v2, "\nRate:\t"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    invoke-interface {v1}, Lcom/glympse/android/lib/GServerPost;->getPostRate()I

    move-result v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 71
    const-string v2, "\nAgent:\t"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    invoke-interface {v1}, Lcom/glympse/android/lib/GServerPost;->getUserAgent()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    const-string v1, "\n"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    :cond_1
    const-string v1, "\nDIRECTIONS\n"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    invoke-interface {p0}, Lcom/glympse/android/lib/GGlympsePrivate;->getDirectionsManager()Lcom/glympse/android/api/GDirectionsManager;

    move-result-object v1

    .line 78
    if-eqz v1, :cond_2

    .line 80
    const-string v2, "Activity:\t"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    invoke-interface {v1}, Lcom/glympse/android/api/GDirectionsManager;->getDeviceActivity()I

    move-result v2

    invoke-static {v2}, Lcom/glympse/android/lib/DebugFormatter;->i(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    const-string v2, "\nTravel:\t"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    invoke-interface {v1}, Lcom/glympse/android/api/GDirectionsManager;->getTravelMode()I

    move-result v1

    invoke-static {v1}, Lcom/glympse/android/lib/DebugFormatter;->j(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    const-string v1, "\n"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    :cond_2
    const-string v1, "\nLOCATION\n"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    invoke-interface {p0}, Lcom/glympse/android/lib/GGlympsePrivate;->getLocationManager()Lcom/glympse/android/api/GLocationManager;

    move-result-object v1

    check-cast v1, Lcom/glympse/android/lib/GLocationManagerPrivate;

    .line 89
    if-eqz v1, :cond_3

    .line 91
    const-string v2, "Location Provider:\t"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    invoke-interface {v1}, Lcom/glympse/android/lib/GLocationManagerPrivate;->getLocationProvider()Lcom/glympse/android/core/GLocationProvider;

    move-result-object v2

    .line 93
    if-nez v2, :cond_8

    move-object v2, v3

    .line 94
    :goto_0
    invoke-static {v4, v2}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 96
    const-string v2, "\nProximity Provider:\t"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    invoke-interface {v1}, Lcom/glympse/android/lib/GLocationManagerPrivate;->getProximityProvider()Lcom/glympse/android/core/GProximityProvider;

    move-result-object v2

    .line 98
    if-nez v2, :cond_9

    move-object v2, v3

    .line 99
    :goto_1
    invoke-static {v4, v2}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 101
    invoke-interface {v1}, Lcom/glympse/android/lib/GLocationManagerPrivate;->getLocation()Lcom/glympse/android/core/GLocation;

    move-result-object v2

    invoke-static {v4, v2, p0}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Lcom/glympse/android/core/GLocation;Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 102
    const-string v2, "\n\nPROFILES\n"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    invoke-interface {v1}, Lcom/glympse/android/lib/GLocationManagerPrivate;->getCurrentProfile()Lcom/glympse/android/core/GLocationProfile;

    move-result-object v2

    .line 104
    if-eqz v2, :cond_3

    .line 106
    const-string v5, "IS_WATCHED"

    invoke-static {v5}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x3

    invoke-interface {v1, v6}, Lcom/glympse/android/lib/GLocationManagerPrivate;->getProfile(I)Lcom/glympse/android/core/GLocationProfile;

    move-result-object v6

    invoke-static {v4, v5, v6, v2}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Lcom/glympse/android/core/GLocationProfile;Lcom/glympse/android/core/GLocationProfile;)V

    .line 107
    const-string v5, "NOT_WATCHED"

    invoke-static {v5}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    invoke-interface {v1, v6}, Lcom/glympse/android/lib/GLocationManagerPrivate;->getProfile(I)Lcom/glympse/android/core/GLocationProfile;

    move-result-object v6

    invoke-static {v4, v5, v6, v2}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Lcom/glympse/android/core/GLocationProfile;Lcom/glympse/android/core/GLocationProfile;)V

    .line 108
    const-string v5, "FOREGROUND"

    invoke-static {v5}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-interface {v1, v6}, Lcom/glympse/android/lib/GLocationManagerPrivate;->getProfile(I)Lcom/glympse/android/core/GLocationProfile;

    move-result-object v6

    invoke-static {v4, v5, v6, v2}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Lcom/glympse/android/core/GLocationProfile;Lcom/glympse/android/core/GLocationProfile;)V

    .line 109
    const-string v5, "BACKGROUND"

    invoke-static {v5}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v1, v6}, Lcom/glympse/android/lib/GLocationManagerPrivate;->getProfile(I)Lcom/glympse/android/core/GLocationProfile;

    move-result-object v1

    invoke-static {v4, v5, v1, v2}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Lcom/glympse/android/core/GLocationProfile;Lcom/glympse/android/core/GLocationProfile;)V

    .line 113
    :cond_3
    const-string v1, "\nNETWORK\n"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    invoke-interface {p0}, Lcom/glympse/android/lib/GGlympsePrivate;->getNetworkManager()Lcom/glympse/android/api/GNetworkManager;

    move-result-object v1

    .line 115
    if-eqz v1, :cond_4

    .line 117
    const-string v2, "init:\t"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    invoke-interface {v1}, Lcom/glympse/android/api/GNetworkManager;->isInitialDataReceived()Z

    move-result v2

    invoke-static {v4, v2}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Z)V

    .line 119
    const-string v2, "\nerror:\t"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    invoke-interface {v1}, Lcom/glympse/android/api/GNetworkManager;->isNetworkError()Z

    move-result v1

    invoke-static {v4, v1}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Z)V

    .line 121
    const-string v1, "\n"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    :cond_4
    const-string v1, "\nBATTERY\n"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    invoke-interface {p0}, Lcom/glympse/android/lib/GGlympsePrivate;->getBatteryManager()Lcom/glympse/android/api/GBatteryManager;

    move-result-object v1

    .line 126
    if-eqz v1, :cond_5

    .line 128
    const-string v2, "good:\t"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    invoke-interface {v1}, Lcom/glympse/android/api/GBatteryManager;->isBatteryLevelGood()Z

    move-result v2

    invoke-static {v4, v2}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Z)V

    .line 130
    const-string v2, "\nok:\t"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131
    invoke-interface {v1}, Lcom/glympse/android/api/GBatteryManager;->isBatteryOk()Z

    move-result v1

    invoke-static {v4, v1}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Z)V

    .line 132
    const-string v1, "\n"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    :cond_5
    const-string v1, "\nPUSH\n"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    if-eqz v0, :cond_6

    .line 138
    const-string v1, "provider:\t"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    invoke-static {}, Lcom/glympse/android/hal/Platform;->getPushType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    const-string v1, "\ntoken:\t"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    invoke-interface {v0}, Lcom/glympse/android/lib/GConfigPrivate;->getRegistrationToken()Ljava/lang/String;

    move-result-object v0

    .line 142
    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 144
    const-string v0, "Not Registered"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    :goto_2
    const-string v0, "\n"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    :cond_6
    invoke-static {}, Lcom/glympse/android/hal/Platform;->getPropertyMap()Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 154
    if-eqz v0, :cond_7

    .line 156
    const-string v1, "\nPLATFORM PROPERTIES\n"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 157
    invoke-static {v4, v0, v3}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Lcom/glympse/android/core/GPrimitive;Ljava/lang/StringBuilder;)V

    .line 159
    :cond_7
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 93
    :cond_8
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 98
    :cond_9
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 148
    :cond_a
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2
.end method

.method public static group(Lcom/glympse/android/lib/GGroupPrivate;Lcom/glympse/android/lib/GGlympsePrivate;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 181
    const/16 v1, 0x3e8

    invoke-static {v1}, Lcom/glympse/android/core/CoreFactory;->createStringBuilder(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 182
    const-string v1, "id:\t\t"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 183
    invoke-interface {p0}, Lcom/glympse/android/lib/GGroupPrivate;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 184
    const-string v1, "\nname:\t"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 185
    invoke-interface {p0}, Lcom/glympse/android/lib/GGroupPrivate;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 187
    invoke-interface {p0}, Lcom/glympse/android/lib/GGroupPrivate;->getMembers()Lcom/glympse/android/core/GArray;

    move-result-object v4

    .line 188
    if-nez v4, :cond_2

    move v1, v0

    :goto_0
    move v2, v0

    .line 189
    :goto_1
    if-ge v2, v1, :cond_3

    .line 191
    const-string v0, "\nMEMBER:"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    invoke-interface {v4, v2}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GGroupMember;

    .line 193
    invoke-interface {v0}, Lcom/glympse/android/api/GGroupMember;->getUser()Lcom/glympse/android/api/GUser;

    move-result-object v5

    .line 194
    if-eqz v5, :cond_0

    .line 196
    invoke-interface {v5}, Lcom/glympse/android/api/GUser;->getId()Ljava/lang/String;

    move-result-object v5

    .line 197
    const-string v6, "\nuserId:\t"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 198
    invoke-static {v3, v5}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 200
    :cond_0
    invoke-interface {v0}, Lcom/glympse/android/api/GGroupMember;->getTicket()Lcom/glympse/android/api/GTicket;

    move-result-object v0

    .line 201
    if-eqz v0, :cond_1

    .line 203
    invoke-interface {v0}, Lcom/glympse/android/api/GTicket;->getId()Ljava/lang/String;

    move-result-object v0

    .line 204
    const-string v5, "\nticketId:\t"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 205
    invoke-static {v3, v0}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 189
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 188
    :cond_2
    invoke-interface {v4}, Lcom/glympse/android/core/GArray;->length()I

    move-result v1

    goto :goto_0

    .line 208
    :cond_3
    const-string v0, "\n"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static i(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 286
    packed-switch p0, :pswitch_data_0

    .line 295
    const-string v0, ""

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 288
    :pswitch_0
    const-string v0, "UNKNOWN"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 289
    :pswitch_1
    const-string v0, "IN_VEHICLE"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 290
    :pswitch_2
    const-string v0, "ON_BICYCLE"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 291
    :pswitch_3
    const-string v0, "ON_FOOT"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 292
    :pswitch_4
    const-string v0, "STILL"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 293
    :pswitch_5
    const-string v0, "TILTING"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 286
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private static j(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 300
    packed-switch p0, :pswitch_data_0

    .line 308
    const-string v0, ""

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 302
    :pswitch_0
    const-string v0, "DEFAULT"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 303
    :pswitch_1
    const-string v0, "DRIVING"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 304
    :pswitch_2
    const-string v0, "BYCYCLING"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 305
    :pswitch_3
    const-string v0, "WALKING"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 306
    :pswitch_4
    const-string v0, "AIRLINE"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 300
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private static k(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 313
    sparse-switch p0, :sswitch_data_0

    .line 325
    const-string v0, ""

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 315
    :sswitch_0
    const-string v0, "NONE"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 316
    :sswitch_1
    const-string v0, "ADDING"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 317
    :sswitch_2
    const-string v0, "INVALID"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 318
    :sswitch_3
    const-string v0, "ACTIVE"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 319
    :sswitch_4
    const-string v0, "EXPIRING"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 320
    :sswitch_5
    const-string v0, "EXPIRED"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 321
    :sswitch_6
    const-string v0, "DELETING"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 322
    :sswitch_7
    const-string v0, "DELETED"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 323
    :sswitch_8
    const-string v0, "FAILED_TO_CREATE"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 313
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x4 -> :sswitch_2
        0x10 -> :sswitch_3
        0x20 -> :sswitch_4
        0x40 -> :sswitch_5
        0x80 -> :sswitch_6
        0x100 -> :sswitch_7
        0x200 -> :sswitch_8
    .end sparse-switch
.end method

.method private static l(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 330
    packed-switch p0, :pswitch_data_0

    .line 343
    const-string v0, ""

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 332
    :pswitch_0
    const-string v0, "NONE"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 333
    :pswitch_1
    const-string v0, "SERVERSENDING"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 334
    :pswitch_2
    const-string v0, "CLIENTSENDING"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 335
    :pswitch_3
    const-string v0, "NEEDTOSEND"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 336
    :pswitch_4
    const-string v0, "SUCCEEDED"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 337
    :pswitch_5
    const-string v0, "DELETING"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 338
    :pswitch_6
    const-string v0, "DELETED"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 339
    :pswitch_7
    const-string v0, "FAILED_TO_CREATE"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 340
    :pswitch_8
    const-string v0, "FAILED_TO_SEND"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 341
    :pswitch_9
    const-string v0, "FAILED_TO_DELETE"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 330
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method private static m(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 348
    invoke-static {p0}, Lcom/glympse/android/api/GlympseTools;->inviteTypeEnumToString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static ticket(Lcom/glympse/android/lib/GTicketPrivate;Lcom/glympse/android/lib/GGlympsePrivate;)Ljava/lang/String;
    .locals 10

    .prologue
    const-wide/16 v8, 0x3e8

    .line 214
    const/16 v0, 0x3e8

    invoke-static {v0}, Lcom/glympse/android/core/CoreFactory;->createStringBuilder(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 215
    const-string v0, "id:\t\t"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    invoke-interface {p0}, Lcom/glympse/android/lib/GTicketPrivate;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 217
    const-string v0, "\nmine:\t\t"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 218
    invoke-interface {p0}, Lcom/glympse/android/lib/GTicketPrivate;->isMine()Z

    move-result v0

    invoke-static {v2, v0}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Z)V

    .line 219
    const-string v0, "\nwatching:\t\t"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 220
    invoke-interface {p0}, Lcom/glympse/android/lib/GTicketPrivate;->isWatching()Z

    move-result v0

    invoke-static {v2, v0}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Z)V

    .line 221
    const-string v0, "\ncode:\t"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 222
    invoke-interface {p0}, Lcom/glympse/android/lib/GTicketPrivate;->getCode()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 223
    const-string v0, "\tstatus:\t"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    invoke-interface {p0}, Lcom/glympse/android/lib/GTicketPrivate;->getState()I

    move-result v0

    invoke-static {v0}, Lcom/glympse/android/lib/DebugFormatter;->k(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 225
    const-string v0, "\ncompleted:\t\t"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 226
    invoke-interface {p0}, Lcom/glympse/android/lib/GTicketPrivate;->isCompleted()Z

    move-result v0

    invoke-static {v2, v0}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Z)V

    .line 227
    const-string v0, "\nstart:\t"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 228
    invoke-interface {p0}, Lcom/glympse/android/lib/GTicketPrivate;->getStartTime()J

    move-result-wide v0

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 229
    const-string v0, "\nend:\t\t"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 230
    invoke-interface {p0}, Lcom/glympse/android/lib/GTicketPrivate;->getExpireTime()J

    move-result-wide v0

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 231
    const-string v0, "\trem:\t"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 232
    invoke-interface {p0}, Lcom/glympse/android/lib/GTicketPrivate;->getExpireTime()J

    move-result-wide v0

    invoke-interface {p1}, Lcom/glympse/android/lib/GGlympsePrivate;->getTime()J

    move-result-wide v4

    sub-long/2addr v0, v4

    div-long/2addr v0, v8

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 233
    const-string v0, "\nduration:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 234
    invoke-interface {p0}, Lcom/glympse/android/lib/GTicketPrivate;->getDuration()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 235
    const-string v0, "\nmessage:\t"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 236
    invoke-interface {p0}, Lcom/glympse/android/lib/GTicketPrivate;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 237
    const-string v0, "\ndest:\t\t"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 238
    invoke-interface {p0}, Lcom/glympse/android/lib/GTicketPrivate;->getDestination()Lcom/glympse/android/api/GPlace;

    move-result-object v0

    invoke-static {v2, v0, p1}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Lcom/glympse/android/api/GPlace;Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 239
    const-string v0, "\nrecip:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 240
    invoke-interface {p0}, Lcom/glympse/android/lib/GTicketPrivate;->getInvites()Lcom/glympse/android/core/GArray;

    move-result-object v3

    .line 241
    invoke-interface {v3}, Lcom/glympse/android/core/GArray;->length()I

    move-result v4

    .line 242
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    .line 244
    invoke-interface {v3, v1}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GInvite;

    .line 245
    const-string v5, "\ni: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 246
    invoke-interface {v0}, Lcom/glympse/android/api/GInvite;->getCode()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 247
    const-string v5, ", "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 248
    invoke-interface {v0}, Lcom/glympse/android/api/GInvite;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 249
    const-string v5, ", "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 250
    invoke-interface {v0}, Lcom/glympse/android/api/GInvite;->getType()I

    move-result v5

    invoke-static {v5}, Lcom/glympse/android/lib/DebugFormatter;->m(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    const-string v5, ", "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 252
    invoke-interface {v0}, Lcom/glympse/android/api/GInvite;->getSubtype()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 253
    const-string v5, ", "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 254
    invoke-interface {v0}, Lcom/glympse/android/api/GInvite;->getState()I

    move-result v5

    invoke-static {v5}, Lcom/glympse/android/lib/DebugFormatter;->l(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 255
    const-string v5, "\ncr: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 256
    invoke-interface {v0}, Lcom/glympse/android/api/GInvite;->getCreatedTime()J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 257
    const-string v5, ", lv: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 258
    invoke-interface {v0}, Lcom/glympse/android/api/GInvite;->getLastViewTime()J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 259
    const-string v5, "\nvc: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 260
    invoke-interface {v0}, Lcom/glympse/android/api/GInvite;->getViewers()I

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 261
    const-string v5, ", ving: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 262
    invoke-interface {v0}, Lcom/glympse/android/api/GInvite;->getViewing()I

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 263
    const-string v5, "\nbody: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 264
    invoke-interface {v0}, Lcom/glympse/android/api/GInvite;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 242
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 266
    :cond_0
    const-string v0, "\ntrack:\t"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267
    invoke-interface {p0}, Lcom/glympse/android/lib/GTicketPrivate;->getTrack()Lcom/glympse/android/api/GTrack;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/api/GTrack;->length()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 268
    const-string v0, "\tnext:\t\t"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 269
    invoke-interface {p0}, Lcom/glympse/android/lib/GTicketPrivate;->getNext()J

    move-result-wide v0

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 270
    const-string v0, "\neta: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 271
    invoke-interface {p0}, Lcom/glympse/android/lib/GTicketPrivate;->getEta()J

    move-result-wide v0

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 272
    const-string v0, ", eta time: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 273
    invoke-interface {p0}, Lcom/glympse/android/lib/GTicketPrivate;->getEtaTs()J

    move-result-wide v0

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 274
    const-string v0, ", since:\t"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 275
    invoke-interface {p1}, Lcom/glympse/android/lib/GGlympsePrivate;->getTime()J

    move-result-wide v0

    invoke-interface {p0}, Lcom/glympse/android/lib/GTicketPrivate;->getEtaTs()J

    move-result-wide v4

    sub-long/2addr v0, v4

    div-long/2addr v0, v8

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 276
    const-string v0, "\n"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 277
    invoke-static {v2, p0}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Lcom/glympse/android/api/GEventSink;)V

    .line 278
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static user(Lcom/glympse/android/lib/GUserPrivate;Lcom/glympse/android/lib/GGlympsePrivate;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 164
    const/16 v0, 0x3e8

    invoke-static {v0}, Lcom/glympse/android/core/CoreFactory;->createStringBuilder(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 166
    const-string v1, "name:\t"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 167
    invoke-interface {p0}, Lcom/glympse/android/lib/GUserPrivate;->getNickname()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 168
    const-string v1, "\nowner:\t"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    invoke-interface {p0}, Lcom/glympse/android/lib/GUserPrivate;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 170
    const-string v1, "\navatar:\t"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171
    invoke-interface {p0}, Lcom/glympse/android/lib/GUserPrivate;->getAvatar()Lcom/glympse/android/api/GImage;

    move-result-object v1

    invoke-interface {v1}, Lcom/glympse/android/api/GImage;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 173
    invoke-interface {p0}, Lcom/glympse/android/lib/GUserPrivate;->getLocation()Lcom/glympse/android/core/GLocation;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Lcom/glympse/android/core/GLocation;Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 174
    invoke-static {v0, p0}, Lcom/glympse/android/lib/DebugFormatter;->a(Ljava/lang/StringBuilder;Lcom/glympse/android/api/GEventSink;)V

    .line 175
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 176
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
