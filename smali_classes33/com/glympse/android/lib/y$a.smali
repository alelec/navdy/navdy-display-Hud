.class Lcom/glympse/android/lib/y$a;
.super Lcom/glympse/android/lib/f;
.source "ConfigEndpoint.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/lib/y;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field public iR:Ljava/lang/String;

.field public iS:J

.field public iT:J

.field public iU:J

.field public iV:Z

.field public iW:Z

.field public iX:Z

.field public iY:Z

.field public iZ:Z

.field public ja:Ljava/lang/String;

.field public jb:Ljava/lang/String;

.field public jc:Ljava/lang/String;

.field public jd:J

.field private je:Lcom/glympse/android/lib/json/JsonSerializer$a;

.field public jf:Lcom/glympse/android/core/GPrimitive;


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const-wide/16 v2, 0x0

    .line 131
    invoke-direct {p0}, Lcom/glympse/android/lib/f;-><init>()V

    .line 132
    iput-wide v2, p0, Lcom/glympse/android/lib/y$a;->iS:J

    .line 133
    iput-wide v2, p0, Lcom/glympse/android/lib/y$a;->iT:J

    .line 134
    iput-wide v2, p0, Lcom/glympse/android/lib/y$a;->iU:J

    .line 135
    invoke-static {}, Lcom/glympse/android/hal/Helpers;->emptyString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/y$a;->ja:Ljava/lang/String;

    .line 136
    iput-boolean v1, p0, Lcom/glympse/android/lib/y$a;->iV:Z

    .line 137
    iput-boolean v1, p0, Lcom/glympse/android/lib/y$a;->iW:Z

    .line 138
    iput-boolean v4, p0, Lcom/glympse/android/lib/y$a;->iX:Z

    .line 139
    iput-boolean v4, p0, Lcom/glympse/android/lib/y$a;->iY:Z

    .line 140
    iput-boolean v1, p0, Lcom/glympse/android/lib/y$a;->iZ:Z

    .line 141
    iput-wide v2, p0, Lcom/glympse/android/lib/y$a;->jd:J

    .line 142
    return-void
.end method


# virtual methods
.method public endObject(I)Z
    .locals 8

    .prologue
    .line 248
    packed-switch p1, :pswitch_data_0

    .line 272
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 252
    :pswitch_0
    iget-object v0, p0, Lcom/glympse/android/lib/y$a;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    invoke-interface {v0}, Lcom/glympse/android/lib/json/GJsonHandlerStack;->popHandler()V

    goto :goto_0

    .line 257
    :pswitch_1
    iget-object v0, p0, Lcom/glympse/android/lib/y$a;->je:Lcom/glympse/android/lib/json/JsonSerializer$a;

    if-eqz v0, :cond_0

    .line 260
    new-instance v0, Lcom/glympse/android/lib/Primitive;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    iput-object v0, p0, Lcom/glympse/android/lib/y$a;->jf:Lcom/glympse/android/core/GPrimitive;

    .line 261
    iget-object v0, p0, Lcom/glympse/android/lib/y$a;->je:Lcom/glympse/android/lib/json/JsonSerializer$a;

    invoke-virtual {v0}, Lcom/glympse/android/lib/json/JsonSerializer$a;->dt()Lcom/glympse/android/core/GPrimitive;

    move-result-object v1

    .line 262
    invoke-interface {v1}, Lcom/glympse/android/core/GPrimitive;->size()I

    move-result v2

    .line 263
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_0

    .line 265
    invoke-interface {v1, v0}, Lcom/glympse/android/core/GPrimitive;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 266
    iget-object v4, p0, Lcom/glympse/android/lib/y$a;->jf:Lcom/glympse/android/core/GPrimitive;

    const-wide/16 v6, 0x0

    invoke-interface {v4, v3, v6, v7}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 263
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 248
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public primitive(ILcom/glympse/android/lib/json/GJsonPrimitive;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 160
    packed-switch p1, :pswitch_data_0

    .line 243
    :cond_0
    :goto_0
    return v2

    .line 164
    :pswitch_0
    iget-object v0, p0, Lcom/glympse/android/lib/y$a;->gE:Ljava/lang/String;

    const-string v1, "result"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    invoke-virtual {p2, v2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/y$a;->gF:Ljava/lang/String;

    goto :goto_0

    .line 172
    :pswitch_1
    iget-object v0, p0, Lcom/glympse/android/lib/y$a;->gE:Ljava/lang/String;

    const-string v1, "config"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 174
    invoke-virtual {p2, v2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/y$a;->iR:Ljava/lang/String;

    goto :goto_0

    .line 176
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/y$a;->gE:Ljava/lang/String;

    const-string v1, "max_ticket_duration"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 178
    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/glympse/android/lib/y$a;->iS:J

    goto :goto_0

    .line 180
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/y$a;->gE:Ljava/lang/String;

    const-string v1, "post_rate_period"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 182
    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/glympse/android/lib/y$a;->iT:J

    goto :goto_0

    .line 184
    :cond_3
    iget-object v0, p0, Lcom/glympse/android/lib/y$a;->gE:Ljava/lang/String;

    const-string v1, "max_name_length"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 186
    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/glympse/android/lib/y$a;->iU:J

    goto :goto_0

    .line 188
    :cond_4
    iget-object v0, p0, Lcom/glympse/android/lib/y$a;->gE:Ljava/lang/String;

    const-string v1, "post_rate_plugged_high"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 190
    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/glympse/android/lib/y$a;->iV:Z

    goto :goto_0

    .line 192
    :cond_5
    iget-object v0, p0, Lcom/glympse/android/lib/y$a;->gE:Ljava/lang/String;

    const-string v1, "accuracy_plugged_high"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 194
    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/glympse/android/lib/y$a;->iW:Z

    goto :goto_0

    .line 196
    :cond_6
    iget-object v0, p0, Lcom/glympse/android/lib/y$a;->gE:Ljava/lang/String;

    const-string v1, "post_rate_stationary_low"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 198
    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/glympse/android/lib/y$a;->iX:Z

    goto/16 :goto_0

    .line 200
    :cond_7
    iget-object v0, p0, Lcom/glympse/android/lib/y$a;->gE:Ljava/lang/String;

    const-string v1, "accuracy_stationary_low"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 202
    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/glympse/android/lib/y$a;->iY:Z

    goto/16 :goto_0

    .line 204
    :cond_8
    iget-object v0, p0, Lcom/glympse/android/lib/y$a;->gE:Ljava/lang/String;

    const-string v1, "invite_poll_push_enabled"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 206
    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/glympse/android/lib/y$a;->iZ:Z

    goto/16 :goto_0

    .line 208
    :cond_9
    iget-object v0, p0, Lcom/glympse/android/lib/y$a;->gE:Ljava/lang/String;

    const-string v1, "time"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 210
    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/glympse/android/lib/y$a;->_time:J

    goto/16 :goto_0

    .line 212
    :cond_a
    iget-object v0, p0, Lcom/glympse/android/lib/y$a;->gE:Ljava/lang/String;

    const-string v1, "error"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 214
    invoke-virtual {p2, v2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/y$a;->gG:Ljava/lang/String;

    goto/16 :goto_0

    .line 216
    :cond_b
    iget-object v0, p0, Lcom/glympse/android/lib/y$a;->gE:Ljava/lang/String;

    const-string v1, "error_detail"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218
    invoke-virtual {p2, v2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/y$a;->gH:Ljava/lang/String;

    goto/16 :goto_0

    .line 224
    :pswitch_2
    iget-object v0, p0, Lcom/glympse/android/lib/y$a;->gE:Ljava/lang/String;

    const-string v1, "url"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 226
    invoke-virtual {p2, v2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/y$a;->ja:Ljava/lang/String;

    goto/16 :goto_0

    .line 228
    :cond_c
    iget-object v0, p0, Lcom/glympse/android/lib/y$a;->gE:Ljava/lang/String;

    const-string v1, "file"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 230
    invoke-virtual {p2, v2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/y$a;->jb:Ljava/lang/String;

    goto/16 :goto_0

    .line 232
    :cond_d
    iget-object v0, p0, Lcom/glympse/android/lib/y$a;->gE:Ljava/lang/String;

    const-string v1, "debug"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 234
    invoke-virtual {p2, v2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/y$a;->jc:Ljava/lang/String;

    goto/16 :goto_0

    .line 236
    :cond_e
    iget-object v0, p0, Lcom/glympse/android/lib/y$a;->gE:Ljava/lang/String;

    const-string v1, "upload_frequency"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 238
    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/glympse/android/lib/y$a;->jd:J

    goto/16 :goto_0

    .line 160
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public startPair(ILjava/lang/String;)Z
    .locals 2

    .prologue
    .line 146
    iput-object p2, p0, Lcom/glympse/android/lib/y$a;->gE:Ljava/lang/String;

    .line 147
    const/4 v0, 0x2

    if-ne v0, p1, :cond_0

    .line 149
    iget-object v0, p0, Lcom/glympse/android/lib/y$a;->gE:Ljava/lang/String;

    const-string v1, "invite_urls"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    new-instance v0, Lcom/glympse/android/lib/json/JsonSerializer$a;

    iget-object v1, p0, Lcom/glympse/android/lib/y$a;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    invoke-direct {v0, v1, p1}, Lcom/glympse/android/lib/json/JsonSerializer$a;-><init>(Lcom/glympse/android/lib/json/GJsonHandlerStack;I)V

    iput-object v0, p0, Lcom/glympse/android/lib/y$a;->je:Lcom/glympse/android/lib/json/JsonSerializer$a;

    .line 152
    iget-object v0, p0, Lcom/glympse/android/lib/y$a;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    iget-object v1, p0, Lcom/glympse/android/lib/y$a;->je:Lcom/glympse/android/lib/json/JsonSerializer$a;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/json/GJsonHandlerStack;->pushHandler(Lcom/glympse/android/lib/json/GJsonHandler;)V

    .line 155
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
