.class Lcom/glympse/android/lib/af;
.super Ljava/lang/Object;
.source "CorrectedTime.java"

# interfaces
.implements Lcom/glympse/android/lib/GCorrectedTime;


# instance fields
.field private js:Z

.field private jt:J

.field private ju:J

.field private jv:J

.field private jw:J

.field private jx:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const-wide/16 v0, 0x0

    const/4 v2, 0x0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-boolean v2, p0, Lcom/glympse/android/lib/af;->js:Z

    .line 33
    iput-wide v0, p0, Lcom/glympse/android/lib/af;->jt:J

    .line 34
    iput-wide v0, p0, Lcom/glympse/android/lib/af;->ju:J

    .line 35
    invoke-static {}, Lcom/glympse/android/hal/Concurrent;->getTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/glympse/android/lib/af;->jv:J

    .line 36
    invoke-static {}, Lcom/glympse/android/hal/Concurrent;->getTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/glympse/android/lib/af;->jw:J

    .line 37
    iput v2, p0, Lcom/glympse/android/lib/af;->jx:I

    .line 38
    return-void
.end method


# virtual methods
.method public flushBiasSetFlag()V
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/glympse/android/lib/af;->js:Z

    .line 95
    return-void
.end method

.method public getLastServerTime()J
    .locals 2

    .prologue
    .line 74
    iget-wide v0, p0, Lcom/glympse/android/lib/af;->ju:J

    return-wide v0
.end method

.method public getLastStateChangeTime()J
    .locals 2

    .prologue
    .line 110
    iget-wide v0, p0, Lcom/glympse/android/lib/af;->jw:J

    return-wide v0
.end method

.method public getPlatformStartTime()J
    .locals 2

    .prologue
    .line 105
    iget-wide v0, p0, Lcom/glympse/android/lib/af;->jv:J

    return-wide v0
.end method

.method public getStateChangesCount()I
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lcom/glympse/android/lib/af;->jx:I

    return v0
.end method

.method public getTime()J
    .locals 4

    .prologue
    .line 79
    invoke-static {}, Lcom/glympse/android/hal/Concurrent;->getTime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/glympse/android/lib/af;->jt:J

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public getTime(J)J
    .locals 3

    .prologue
    .line 84
    iget-wide v0, p0, Lcom/glympse/android/lib/af;->jt:J

    add-long/2addr v0, p1

    return-wide v0
.end method

.method public isBiasSet()Z
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/glympse/android/lib/af;->js:Z

    return v0
.end method

.method public setServerTime(J)V
    .locals 7

    .prologue
    .line 43
    iput-wide p1, p0, Lcom/glympse/android/lib/af;->ju:J

    .line 46
    invoke-static {}, Lcom/glympse/android/hal/Concurrent;->getTime()J

    move-result-wide v0

    .line 49
    sub-long v0, p1, v0

    .line 63
    iget-boolean v2, p0, Lcom/glympse/android/lib/af;->js:Z

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lcom/glympse/android/lib/af;->jt:J

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    iget-wide v2, p0, Lcom/glympse/android/lib/af;->jt:J

    sub-long/2addr v2, v0

    const-wide/16 v4, 0x3a98

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    .line 67
    :cond_0
    iput-wide v0, p0, Lcom/glympse/android/lib/af;->jt:J

    .line 68
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/glympse/android/lib/af;->js:Z

    .line 70
    :cond_1
    return-void
.end method

.method public stateChanged()V
    .locals 2

    .prologue
    .line 99
    invoke-static {}, Lcom/glympse/android/hal/Concurrent;->getTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/glympse/android/lib/af;->jw:J

    .line 100
    iget v0, p0, Lcom/glympse/android/lib/af;->jx:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/glympse/android/lib/af;->jx:I

    .line 101
    return-void
.end method
