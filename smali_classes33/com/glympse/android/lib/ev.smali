.class Lcom/glympse/android/lib/ev;
.super Lcom/glympse/android/lib/cu;
.source "RequestInviteCreate.java"


# direct methods
.method public constructor <init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/GTicketPrivate;Lcom/glympse/android/lib/GInvitePrivate;)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/glympse/android/lib/cu;-><init>()V

    .line 21
    iput-object p2, p0, Lcom/glympse/android/lib/ev;->gr:Lcom/glympse/android/api/GEventSink;

    .line 22
    const/4 v0, 0x4

    iput v0, p0, Lcom/glympse/android/lib/ev;->oC:I

    .line 23
    const/high16 v0, 0x20000

    iput v0, p0, Lcom/glympse/android/lib/ev;->oD:I

    .line 24
    const/high16 v0, 0x40000

    iput v0, p0, Lcom/glympse/android/lib/ev;->oE:I

    .line 25
    const/high16 v0, 0x80000

    iput v0, p0, Lcom/glympse/android/lib/ev;->oF:I

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/ev;->oG:Lcom/glympse/android/lib/GTicketPrivate;

    .line 28
    iput-object p3, p0, Lcom/glympse/android/lib/ev;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    .line 29
    iput-object p1, p0, Lcom/glympse/android/lib/ev;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 30
    invoke-interface {p3}, Lcom/glympse/android/lib/GInvitePrivate;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/ev;->oy:Ljava/lang/String;

    .line 33
    invoke-virtual {p0}, Lcom/glympse/android/lib/ev;->cl()V

    .line 34
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/StringBuilder;)V
    .locals 1

    .prologue
    .line 42
    const-string v0, "users/self/create_request"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    return-void
.end method
