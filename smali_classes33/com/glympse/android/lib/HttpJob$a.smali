.class Lcom/glympse/android/lib/HttpJob$a;
.super Ljava/lang/Object;
.source "HttpJob.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/lib/HttpJob;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private nK:Lcom/glympse/android/lib/HttpJob;

.field private nL:Z


# direct methods
.method public constructor <init>(Lcom/glympse/android/lib/HttpJob;)V
    .locals 1

    .prologue
    .line 375
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 376
    iput-object p1, p0, Lcom/glympse/android/lib/HttpJob$a;->nK:Lcom/glympse/android/lib/HttpJob;

    .line 377
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/glympse/android/lib/HttpJob$a;->nL:Z

    .line 378
    return-void
.end method


# virtual methods
.method public abort()V
    .locals 1

    .prologue
    .line 382
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/glympse/android/lib/HttpJob$a;->nL:Z

    .line 383
    return-void
.end method

.method public run()V
    .locals 2

    .prologue
    .line 387
    iget-boolean v0, p0, Lcom/glympse/android/lib/HttpJob$a;->nL:Z

    if-eqz v0, :cond_0

    .line 396
    :goto_0
    return-void

    .line 392
    :cond_0
    const/4 v0, 0x3

    const-string v1, "HttpJob.TimeoutTimer.run"

    invoke-static {v0, v1}, Lcom/glympse/android/hal/Helpers;->log(ILjava/lang/String;)V

    .line 395
    iget-object v0, p0, Lcom/glympse/android/lib/HttpJob$a;->nK:Lcom/glympse/android/lib/HttpJob;

    invoke-virtual {v0}, Lcom/glympse/android/lib/HttpJob;->cancel()V

    goto :goto_0
.end method
