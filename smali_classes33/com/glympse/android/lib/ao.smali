.class Lcom/glympse/android/lib/ao;
.super Lcom/glympse/android/lib/HttpJob;
.source "DirectionsGlympseJob.java"


# instance fields
.field private f:Ljava/lang/String;

.field private gj:Ljava/lang/String;

.field private kh:J

.field private kj:Ljava/lang/String;

.field private kk:Lcom/glympse/android/lib/GDirectionsPrivate;

.field private kl:Lcom/glympse/android/api/GTrack;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;Ljava/lang/String;Lcom/glympse/android/lib/GDirectionsPrivate;)V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/glympse/android/lib/HttpJob;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/glympse/android/lib/ao;->f:Ljava/lang/String;

    .line 35
    iput-object p3, p0, Lcom/glympse/android/lib/ao;->gj:Ljava/lang/String;

    .line 36
    iput-object p4, p0, Lcom/glympse/android/lib/ao;->kk:Lcom/glympse/android/lib/GDirectionsPrivate;

    .line 37
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/glympse/android/lib/ao;->kh:J

    .line 40
    const-string v0, "data"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 41
    if-eqz v0, :cond_0

    .line 43
    const-string v1, "provider"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/ao;->kj:Ljava/lang/String;

    .line 46
    :cond_0
    return-void
.end method

.method private e(Lcom/glympse/android/core/GPrimitive;)V
    .locals 6

    .prologue
    .line 150
    if-nez p1, :cond_1

    .line 184
    :cond_0
    :goto_0
    return-void

    .line 155
    :cond_1
    const-string v0, "response"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v2

    .line 156
    if-eqz v2, :cond_0

    .line 162
    const-string v0, "summary"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 163
    if-eqz v0, :cond_0

    .line 167
    const-string v1, "distance"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    long-to-int v3, v4

    .line 168
    const-string v1, "traffic_time"

    .line 169
    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "traffic_time"

    .line 170
    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 171
    :goto_1
    iput-wide v0, p0, Lcom/glympse/android/lib/ao;->kh:J

    .line 174
    const-string v0, "line"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 175
    if-eqz v0, :cond_0

    .line 177
    const-string v1, "points"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 178
    invoke-static {v0}, Lcom/glympse/android/lib/an;->k(Ljava/lang/String;)Lcom/glympse/android/api/GTrack;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/ao;->kl:Lcom/glympse/android/api/GTrack;

    .line 180
    iget-object v0, p0, Lcom/glympse/android/lib/ao;->kl:Lcom/glympse/android/api/GTrack;

    check-cast v0, Lcom/glympse/android/lib/GTrackPrivate;

    .line 181
    const/4 v1, 0x4

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GTrackPrivate;->setSource(I)V

    .line 182
    invoke-interface {v0, v3}, Lcom/glympse/android/lib/GTrackPrivate;->setDistance(I)V

    goto :goto_0

    .line 170
    :cond_2
    const-string v1, "travel_time"

    .line 171
    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_1
.end method


# virtual methods
.method public checkResponse(II)Z
    .locals 1

    .prologue
    .line 124
    const/16 v0, 0xc8

    if-ne v0, p1, :cond_0

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRetryInterval(I)I
    .locals 1

    .prologue
    .line 129
    const/4 v0, -0x1

    return v0
.end method

.method public onAbort()V
    .locals 1

    .prologue
    .line 140
    invoke-super {p0}, Lcom/glympse/android/lib/HttpJob;->onAbort()V

    .line 141
    iget-object v0, p0, Lcom/glympse/android/lib/ao;->kk:Lcom/glympse/android/lib/GDirectionsPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GDirectionsPrivate;->failed()V

    .line 142
    return-void
.end method

.method public onComplete()V
    .locals 7

    .prologue
    .line 134
    invoke-super {p0}, Lcom/glympse/android/lib/HttpJob;->onComplete()V

    .line 135
    iget-object v1, p0, Lcom/glympse/android/lib/ao;->kk:Lcom/glympse/android/lib/GDirectionsPrivate;

    iget-object v0, p0, Lcom/glympse/android/lib/ao;->kk:Lcom/glympse/android/lib/GDirectionsPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GDirectionsPrivate;->getRequestTime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/glympse/android/lib/ao;->kh:J

    iget-object v6, p0, Lcom/glympse/android/lib/ao;->kl:Lcom/glympse/android/api/GTrack;

    invoke-interface/range {v1 .. v6}, Lcom/glympse/android/lib/GDirectionsPrivate;->set(JJLcom/glympse/android/api/GTrack;)V

    .line 136
    return-void
.end method

.method public onPreProcess()V
    .locals 5

    .prologue
    const/16 v4, 0x2c

    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 52
    const-string v1, "https://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 53
    iget-object v1, p0, Lcom/glympse/android/lib/ao;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    const-string v1, "/v2/maps/route"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    iget-object v1, p0, Lcom/glympse/android/lib/ao;->kk:Lcom/glympse/android/lib/GDirectionsPrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GDirectionsPrivate;->getOrigin()Lcom/glympse/android/core/GLatLng;

    move-result-object v1

    .line 58
    const-string v2, "?start="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    invoke-interface {v1}, Lcom/glympse/android/core/GLatLng;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 60
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 61
    invoke-interface {v1}, Lcom/glympse/android/core/GLatLng;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 64
    iget-object v1, p0, Lcom/glympse/android/lib/ao;->kk:Lcom/glympse/android/lib/GDirectionsPrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GDirectionsPrivate;->getDestination()Lcom/glympse/android/core/GLatLng;

    move-result-object v1

    .line 65
    const-string v2, "&end="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    invoke-interface {v1}, Lcom/glympse/android/core/GLatLng;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 67
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 68
    invoke-interface {v1}, Lcom/glympse/android/core/GLatLng;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 71
    iget-object v1, p0, Lcom/glympse/android/lib/ao;->kj:Ljava/lang/String;

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 73
    const-string v1, "&provider="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    iget-object v1, p0, Lcom/glympse/android/lib/ao;->kj:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    :cond_0
    const-string v1, "&travel_mode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    iget-object v1, p0, Lcom/glympse/android/lib/ao;->kk:Lcom/glympse/android/lib/GDirectionsPrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GDirectionsPrivate;->getTravelMode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 92
    :pswitch_0
    const-string v1, "drive"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 98
    iget-object v1, p0, Lcom/glympse/android/lib/ao;->_httpConnection:Lcom/glympse/android/hal/GHttpConnection;

    invoke-interface {v1, v0}, Lcom/glympse/android/hal/GHttpConnection;->setUrl(Ljava/lang/String;)V

    .line 100
    iget-object v0, p0, Lcom/glympse/android/lib/ao;->_httpConnection:Lcom/glympse/android/hal/GHttpConnection;

    iget-object v1, p0, Lcom/glympse/android/lib/ao;->gj:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/glympse/android/lib/ao;->setAuthorization(Lcom/glympse/android/hal/GHttpConnection;Ljava/lang/String;)V

    .line 101
    return-void

    .line 82
    :pswitch_1
    const-string v1, "transit"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 86
    :pswitch_2
    const-string v1, "drive"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 89
    :pswitch_3
    const-string v1, "walk"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 79
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onProcessResponse()V
    .locals 2

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/glympse/android/lib/ao;->isSucceeded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 120
    :cond_0
    :goto_0
    return-void

    .line 112
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/ao;->_httpConnection:Lcom/glympse/android/hal/GHttpConnection;

    invoke-interface {v0}, Lcom/glympse/android/hal/GHttpConnection;->getResponseDataString()Ljava/lang/String;

    move-result-object v0

    .line 113
    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 119
    invoke-static {v0}, Lcom/glympse/android/lib/json/JsonSerializer;->toPrimitive(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/ao;->e(Lcom/glympse/android/core/GPrimitive;)V

    goto :goto_0
.end method
