.class Lcom/glympse/android/lib/fi;
.super Ljava/lang/Object;
.source "TicketDirectionsProvider.java"

# interfaces
.implements Lcom/glympse/android/lib/bl;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/lib/fi$a;,
        Lcom/glympse/android/lib/fi$b;,
        Lcom/glympse/android/lib/fi$c;
    }
.end annotation


# instance fields
.field private _glympse:Lcom/glympse/android/lib/GGlympsePrivate;

.field private ke:Lcom/glympse/android/core/GLatLng;

.field private mo:Ljava/lang/Runnable;

.field private sc:Lcom/glympse/android/hal/GHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GHashtable",
            "<",
            "Lcom/glympse/android/lib/GTicketPrivate;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private tD:Z

.field private tE:Lcom/glympse/android/core/GLocation;

.field private tF:J

.field private tG:Lcom/glympse/android/api/GEventListener;


# direct methods
.method public constructor <init>(Lcom/glympse/android/core/GLatLng;)V
    .locals 2

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/glympse/android/lib/fi;->ke:Lcom/glympse/android/core/GLatLng;

    .line 49
    new-instance v0, Lcom/glympse/android/hal/GHashtable;

    invoke-direct {v0}, Lcom/glympse/android/hal/GHashtable;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/fi;->sc:Lcom/glympse/android/hal/GHashtable;

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/glympse/android/lib/fi;->tD:Z

    .line 51
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/glympse/android/lib/fi;->tF:J

    .line 52
    return-void
.end method

.method private a(JJLcom/glympse/android/api/GTrack;)V
    .locals 9

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x1

    .line 121
    iget-object v0, p0, Lcom/glympse/android/lib/fi;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-nez v0, :cond_0

    .line 152
    :goto_0
    return-void

    .line 127
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/glympse/android/lib/fi;->tD:Z

    .line 129
    cmp-long v0, p1, v2

    if-eqz v0, :cond_4

    cmp-long v0, p3, v2

    if-eqz v0, :cond_4

    .line 132
    iput-wide p3, p0, Lcom/glympse/android/lib/fi;->tF:J

    .line 135
    iget-object v0, p0, Lcom/glympse/android/lib/fi;->sc:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GHashtable;->keys()Ljava/util/Enumeration;

    move-result-object v8

    :cond_1
    :goto_1
    invoke-interface {v8}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 137
    invoke-interface {v8}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GTicketPrivate;

    .line 140
    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketPrivate;->isActive()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 142
    if-eqz p5, :cond_2

    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketPrivate;->isVisible()Z

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    const/4 v6, -0x1

    :goto_2
    move-wide v2, p1

    move-wide v4, p3

    move-object v7, p5

    .line 145
    invoke-interface/range {v0 .. v7}, Lcom/glympse/android/lib/GTicketPrivate;->updateEta(IJJILcom/glympse/android/api/GTrack;)V

    goto :goto_1

    :cond_3
    move v6, v1

    .line 142
    goto :goto_2

    .line 151
    :cond_4
    invoke-direct {p0}, Lcom/glympse/android/lib/fi;->as()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/glympse/android/lib/fi;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/glympse/android/lib/fi;->cY()V

    return-void
.end method

.method static synthetic a(Lcom/glympse/android/lib/fi;JJLcom/glympse/android/api/GTrack;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct/range {p0 .. p5}, Lcom/glympse/android/lib/fi;->a(JJLcom/glympse/android/api/GTrack;)V

    return-void
.end method

.method private as()V
    .locals 4

    .prologue
    .line 156
    invoke-direct {p0}, Lcom/glympse/android/lib/fi;->cV()V

    .line 157
    new-instance v1, Lcom/glympse/android/lib/fi$b;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/fi;

    invoke-direct {v1, v0}, Lcom/glympse/android/lib/fi$b;-><init>(Lcom/glympse/android/lib/fi;)V

    iput-object v1, p0, Lcom/glympse/android/lib/fi;->mo:Ljava/lang/Runnable;

    .line 158
    iget-object v0, p0, Lcom/glympse/android/lib/fi;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getHandler()Lcom/glympse/android/core/GHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/lib/fi;->mo:Ljava/lang/Runnable;

    const-wide/32 v2, 0xea60

    invoke-interface {v0, v1, v2, v3}, Lcom/glympse/android/core/GHandler;->postDelayed(Ljava/lang/Runnable;J)V

    .line 159
    return-void
.end method

.method static synthetic b(Lcom/glympse/android/lib/fi;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/glympse/android/lib/fi;->cZ()V

    return-void
.end method

.method private cV()V
    .locals 2

    .prologue
    .line 163
    iget-object v0, p0, Lcom/glympse/android/lib/fi;->mo:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/glympse/android/lib/fi;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getHandler()Lcom/glympse/android/core/GHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/lib/fi;->mo:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GHandler;->cancel(Ljava/lang/Runnable;)V

    .line 166
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/fi;->mo:Ljava/lang/Runnable;

    .line 168
    :cond_0
    return-void
.end method

.method private cW()V
    .locals 2

    .prologue
    .line 172
    iget-object v0, p0, Lcom/glympse/android/lib/fi;->tG:Lcom/glympse/android/api/GEventListener;

    if-eqz v0, :cond_0

    .line 179
    :goto_0
    return-void

    .line 177
    :cond_0
    new-instance v1, Lcom/glympse/android/lib/fi$c;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/fi;

    invoke-direct {v1, v0}, Lcom/glympse/android/lib/fi$c;-><init>(Lcom/glympse/android/lib/fi;)V

    iput-object v1, p0, Lcom/glympse/android/lib/fi;->tG:Lcom/glympse/android/api/GEventListener;

    .line 178
    iget-object v0, p0, Lcom/glympse/android/lib/fi;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/fi;->tG:Lcom/glympse/android/api/GEventListener;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GGlympsePrivate;->addListener(Lcom/glympse/android/api/GEventListener;)Z

    goto :goto_0
.end method

.method private cX()V
    .locals 2

    .prologue
    .line 183
    iget-object v0, p0, Lcom/glympse/android/lib/fi;->tG:Lcom/glympse/android/api/GEventListener;

    if-nez v0, :cond_0

    .line 190
    :goto_0
    return-void

    .line 188
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/fi;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/fi;->tG:Lcom/glympse/android/api/GEventListener;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GGlympsePrivate;->removeListener(Lcom/glympse/android/api/GEventListener;)Z

    .line 189
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/fi;->tG:Lcom/glympse/android/api/GEventListener;

    goto :goto_0
.end method

.method private cY()V
    .locals 1

    .prologue
    .line 253
    invoke-direct {p0}, Lcom/glympse/android/lib/fi;->cX()V

    .line 256
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/fi;->l(Z)V

    .line 257
    return-void
.end method

.method private cZ()V
    .locals 1

    .prologue
    .line 261
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/fi;->mo:Ljava/lang/Runnable;

    .line 262
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/fi;->l(Z)V

    .line 263
    return-void
.end method

.method private l(Z)V
    .locals 7

    .prologue
    const-wide/16 v2, 0x0

    const/4 v6, 0x0

    .line 195
    iget-object v0, p0, Lcom/glympse/android/lib/fi;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/glympse/android/lib/fi;->tD:Z

    if-eqz v0, :cond_1

    .line 248
    :cond_0
    :goto_0
    return-void

    .line 201
    :cond_1
    invoke-direct {p0}, Lcom/glympse/android/lib/fi;->cV()V

    .line 204
    iget-object v0, p0, Lcom/glympse/android/lib/fi;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getLocationManager()Lcom/glympse/android/api/GLocationManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/api/GLocationManager;->getLocation()Lcom/glympse/android/core/GLocation;

    move-result-object v0

    .line 205
    if-nez v0, :cond_2

    .line 208
    invoke-direct {p0}, Lcom/glympse/android/lib/fi;->cW()V

    goto :goto_0

    .line 214
    :cond_2
    if-nez p1, :cond_3

    .line 217
    iget-object v1, p0, Lcom/glympse/android/lib/fi;->tE:Lcom/glympse/android/core/GLocation;

    if-eqz v1, :cond_3

    .line 219
    iget-object v1, p0, Lcom/glympse/android/lib/fi;->tE:Lcom/glympse/android/core/GLocation;

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GLocation;->distanceTo(Lcom/glympse/android/core/GLatLng;)F

    move-result v1

    .line 220
    const/high16 v4, 0x43160000    # 150.0f

    cmpg-float v1, v1, v4

    if-gez v1, :cond_3

    .line 224
    iget-object v0, p0, Lcom/glympse/android/lib/fi;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getTime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/glympse/android/lib/fi;->tF:J

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/glympse/android/lib/fi;->a(JJLcom/glympse/android/api/GTrack;)V

    goto :goto_0

    .line 232
    :cond_3
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/glympse/android/lib/fi;->tD:Z

    .line 235
    iput-object v0, p0, Lcom/glympse/android/lib/fi;->tE:Lcom/glympse/android/core/GLocation;

    .line 237
    iget-object v1, p0, Lcom/glympse/android/lib/fi;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->getDirectionsManager()Lcom/glympse/android/api/GDirectionsManager;

    move-result-object v1

    .line 238
    iget-object v4, p0, Lcom/glympse/android/lib/fi;->ke:Lcom/glympse/android/core/GLatLng;

    .line 239
    invoke-interface {v1}, Lcom/glympse/android/api/GDirectionsManager;->getTravelMode()I

    move-result v5

    .line 238
    invoke-interface {v1, v0, v4, v5}, Lcom/glympse/android/api/GDirectionsManager;->queryDirections(Lcom/glympse/android/core/GLatLng;Lcom/glympse/android/core/GLatLng;I)Lcom/glympse/android/api/GDirections;

    move-result-object v1

    .line 240
    if-eqz v1, :cond_4

    .line 242
    new-instance v2, Lcom/glympse/android/lib/fi$a;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/fi;

    invoke-direct {v2, v0}, Lcom/glympse/android/lib/fi$a;-><init>(Lcom/glympse/android/lib/fi;)V

    invoke-interface {v1, v2}, Lcom/glympse/android/api/GDirections;->addListener(Lcom/glympse/android/api/GEventListener;)Z

    goto :goto_0

    :cond_4
    move-object v1, p0

    move-wide v4, v2

    .line 246
    invoke-direct/range {v1 .. v6}, Lcom/glympse/android/lib/fi;->a(JJLcom/glympse/android/api/GTrack;)V

    goto :goto_0
.end method


# virtual methods
.method public addTicket(Lcom/glympse/android/lib/GTicketPrivate;)V
    .locals 4

    .prologue
    .line 87
    iget-object v0, p0, Lcom/glympse/android/lib/fi;->sc:Lcom/glympse/android/hal/GHashtable;

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/fi;->l(Z)V

    .line 91
    return-void
.end method

.method public bE()Z
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/glympse/android/lib/fi;->sc:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GHashtable;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bF()V
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/fi;->l(Z)V

    .line 113
    return-void
.end method

.method public getDestination()Lcom/glympse/android/core/GLatLng;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/glympse/android/lib/fi;->ke:Lcom/glympse/android/core/GLatLng;

    return-object v0
.end method

.method public removeTicket(Lcom/glympse/android/lib/GTicketPrivate;)V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/glympse/android/lib/fi;->sc:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    iget-object v0, p0, Lcom/glympse/android/lib/fi;->sc:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GHashtable;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 101
    invoke-direct {p0}, Lcom/glympse/android/lib/fi;->cV()V

    .line 103
    :cond_0
    return-void
.end method

.method public start(Lcom/glympse/android/lib/GGlympsePrivate;)V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/glympse/android/lib/fi;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_0

    .line 64
    :goto_0
    return-void

    .line 63
    :cond_0
    iput-object p1, p0, Lcom/glympse/android/lib/fi;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    goto :goto_0
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/glympse/android/lib/fi;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-nez v0, :cond_0

    .line 77
    :goto_0
    return-void

    .line 73
    :cond_0
    invoke-direct {p0}, Lcom/glympse/android/lib/fi;->cV()V

    .line 74
    invoke-direct {p0}, Lcom/glympse/android/lib/fi;->cX()V

    .line 76
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/fi;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    goto :goto_0
.end method
