.class Lcom/glympse/android/lib/d$f;
.super Ljava/lang/Object;
.source "AccountManager.java"

# interfaces
.implements Lcom/glympse/android/lib/GAccountProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/lib/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "f"
.end annotation


# instance fields
.field private _handler:Lcom/glympse/android/core/GHandler;

.field private fY:Lcom/glympse/android/core/GPrimitive;

.field private g:Lcom/glympse/android/lib/GAccountImportListener;


# direct methods
.method public constructor <init>(Lcom/glympse/android/core/GHandler;Lcom/glympse/android/core/GPrimitive;)V
    .locals 0

    .prologue
    .line 455
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 456
    iput-object p1, p0, Lcom/glympse/android/lib/d$f;->_handler:Lcom/glympse/android/core/GHandler;

    .line 457
    iput-object p2, p0, Lcom/glympse/android/lib/d$f;->fY:Lcom/glympse/android/core/GPrimitive;

    .line 458
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 0

    .prologue
    .line 479
    return-void
.end method

.method public create(Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 467
    iget-object v0, p0, Lcom/glympse/android/lib/d$f;->g:Lcom/glympse/android/lib/GAccountImportListener;

    if-nez v0, :cond_0

    .line 469
    const/4 v0, 0x0

    .line 474
    :goto_0
    return v0

    .line 472
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/d$f;->_handler:Lcom/glympse/android/core/GHandler;

    new-instance v1, Lcom/glympse/android/lib/d$e;

    iget-object v2, p0, Lcom/glympse/android/lib/d$f;->g:Lcom/glympse/android/lib/GAccountImportListener;

    iget-object v3, p0, Lcom/glympse/android/lib/d$f;->fY:Lcom/glympse/android/core/GPrimitive;

    invoke-direct {v1, v2, v3}, Lcom/glympse/android/lib/d$e;-><init>(Lcom/glympse/android/lib/GAccountImportListener;Lcom/glympse/android/core/GPrimitive;)V

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GHandler;->post(Ljava/lang/Runnable;)V

    .line 474
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setAccountListener(Lcom/glympse/android/lib/GAccountImportListener;)V
    .locals 0

    .prologue
    .line 462
    iput-object p1, p0, Lcom/glympse/android/lib/d$f;->g:Lcom/glympse/android/lib/GAccountImportListener;

    .line 463
    return-void
.end method
