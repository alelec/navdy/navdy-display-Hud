.class Lcom/glympse/android/lib/fo$a;
.super Lcom/glympse/android/lib/f;
.source "TicketParsers.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/lib/fo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private mD:Lcom/glympse/android/hal/GVector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GVector",
            "<",
            "Lcom/glympse/android/lib/GInvitePrivate;",
            ">;"
        }
    .end annotation
.end field

.field private oH:Lcom/glympse/android/lib/GInvitePrivate;

.field private tL:Lcom/glympse/android/lib/GTicketPrivate;


# direct methods
.method public constructor <init>(Lcom/glympse/android/lib/json/GJsonHandlerStack;Lcom/glympse/android/hal/GVector;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/glympse/android/lib/json/GJsonHandlerStack;",
            "Lcom/glympse/android/hal/GVector",
            "<",
            "Lcom/glympse/android/lib/GInvitePrivate;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 194
    invoke-direct {p0}, Lcom/glympse/android/lib/f;-><init>()V

    .line 195
    iput-object p1, p0, Lcom/glympse/android/lib/fo$a;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    .line 196
    iput-object p2, p0, Lcom/glympse/android/lib/fo$a;->mD:Lcom/glympse/android/hal/GVector;

    .line 197
    return-void
.end method


# virtual methods
.method public endObject(I)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 215
    packed-switch p1, :pswitch_data_0

    .line 234
    :goto_0
    :pswitch_0
    const/4 v0, 0x1

    return v0

    .line 219
    :pswitch_1
    iget-object v0, p0, Lcom/glympse/android/lib/fo$a;->tL:Lcom/glympse/android/lib/GTicketPrivate;

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, p0, Lcom/glympse/android/lib/fo$a;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/fo$a;->tL:Lcom/glympse/android/lib/GTicketPrivate;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GInvitePrivate;->setRequestTicket(Lcom/glympse/android/api/GTicket;)V

    .line 222
    iput-object v2, p0, Lcom/glympse/android/lib/fo$a;->tL:Lcom/glympse/android/lib/GTicketPrivate;

    .line 224
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/fo$a;->mD:Lcom/glympse/android/hal/GVector;

    iget-object v1, p0, Lcom/glympse/android/lib/fo$a;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 225
    iput-object v2, p0, Lcom/glympse/android/lib/fo$a;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    goto :goto_0

    .line 230
    :pswitch_2
    iget-object v0, p0, Lcom/glympse/android/lib/fo$a;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    invoke-interface {v0}, Lcom/glympse/android/lib/json/GJsonHandlerStack;->popHandler()V

    goto :goto_0

    .line 215
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public primitive(ILcom/glympse/android/lib/json/GJsonPrimitive;)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 239
    packed-switch p1, :pswitch_data_0

    .line 313
    :cond_0
    :goto_0
    return v4

    .line 243
    :pswitch_0
    iget-object v0, p0, Lcom/glympse/android/lib/fo$a;->gE:Ljava/lang/String;

    const-string v1, "id"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 245
    invoke-virtual {p2, v4}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v0

    .line 246
    iget-object v1, p0, Lcom/glympse/android/lib/fo$a;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-static {v0}, Lcom/glympse/android/lib/TicketCode;->cleanupInviteCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GInvitePrivate;->setCode(Ljava/lang/String;)V

    goto :goto_0

    .line 248
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/fo$a;->gE:Ljava/lang/String;

    const-string v1, "url"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 250
    iget-object v0, p0, Lcom/glympse/android/lib/fo$a;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-virtual {p2, v4}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GInvitePrivate;->setUrl(Ljava/lang/String;)V

    goto :goto_0

    .line 252
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/fo$a;->gE:Ljava/lang/String;

    const-string v1, "text"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 254
    iget-object v0, p0, Lcom/glympse/android/lib/fo$a;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-virtual {p2, v2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GInvitePrivate;->setText(Ljava/lang/String;)V

    goto :goto_0

    .line 256
    :cond_3
    iget-object v0, p0, Lcom/glympse/android/lib/fo$a;->gE:Ljava/lang/String;

    const-string v1, "status"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 258
    invoke-virtual {p2, v4}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getString(Z)Ljava/lang/String;

    move-result-object v0

    .line 259
    iget-object v1, p0, Lcom/glympse/android/lib/fo$a;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-static {v0}, Lcom/glympse/android/lib/ct;->q(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GInvitePrivate;->setState(I)V

    goto :goto_0

    .line 261
    :cond_4
    iget-object v0, p0, Lcom/glympse/android/lib/fo$a;->gE:Ljava/lang/String;

    const-string v1, "created"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 263
    iget-object v0, p0, Lcom/glympse/android/lib/fo$a;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getLong()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/glympse/android/lib/GInvitePrivate;->setCreatedTime(J)V

    goto :goto_0

    .line 265
    :cond_5
    iget-object v0, p0, Lcom/glympse/android/lib/fo$a;->gE:Ljava/lang/String;

    const-string v1, "last_view"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 267
    iget-object v0, p0, Lcom/glympse/android/lib/fo$a;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getLong()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/glympse/android/lib/GInvitePrivate;->setLastViewTime(J)V

    goto :goto_0

    .line 269
    :cond_6
    iget-object v0, p0, Lcom/glympse/android/lib/fo$a;->gE:Ljava/lang/String;

    const-string v1, "viewers"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 271
    iget-object v0, p0, Lcom/glympse/android/lib/fo$a;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getLong()J

    move-result-wide v2

    long-to-int v1, v2

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GInvitePrivate;->setViewers(I)V

    goto/16 :goto_0

    .line 273
    :cond_7
    iget-object v0, p0, Lcom/glympse/android/lib/fo$a;->gE:Ljava/lang/String;

    const-string v1, "viewing"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 275
    iget-object v0, p0, Lcom/glympse/android/lib/fo$a;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getLong()J

    move-result-wide v2

    long-to-int v1, v2

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GInvitePrivate;->setViewing(I)V

    goto/16 :goto_0

    .line 277
    :cond_8
    iget-object v0, p0, Lcom/glympse/android/lib/fo$a;->gE:Ljava/lang/String;

    const-string v1, "visible"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 279
    invoke-virtual {p2, v4}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getString(Z)Ljava/lang/String;

    move-result-object v0

    .line 280
    const-string v1, "all"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 282
    iget-object v0, p0, Lcom/glympse/android/lib/fo$a;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-interface {v0, v4}, Lcom/glympse/android/lib/GInvitePrivate;->setVisible(Z)V

    goto/16 :goto_0

    .line 285
    :cond_9
    iget-object v0, p0, Lcom/glympse/android/lib/fo$a;->gE:Ljava/lang/String;

    const-string v1, "reference"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 287
    iget-object v0, p0, Lcom/glympse/android/lib/fo$a;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-virtual {p2, v4}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GInvitePrivate;->setReference(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 293
    :pswitch_1
    iget-object v0, p0, Lcom/glympse/android/lib/fo$a;->gE:Ljava/lang/String;

    const-string v1, "type"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 295
    invoke-virtual {p2, v4}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getString(Z)Ljava/lang/String;

    move-result-object v0

    .line 296
    iget-object v1, p0, Lcom/glympse/android/lib/fo$a;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-static {v0}, Lcom/glympse/android/lib/ct;->p(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GInvitePrivate;->setType(I)V

    goto/16 :goto_0

    .line 298
    :cond_a
    iget-object v0, p0, Lcom/glympse/android/lib/fo$a;->gE:Ljava/lang/String;

    const-string v1, "subtype"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 300
    iget-object v0, p0, Lcom/glympse/android/lib/fo$a;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-virtual {p2, v2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GInvitePrivate;->setSubtype(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 302
    :cond_b
    iget-object v0, p0, Lcom/glympse/android/lib/fo$a;->gE:Ljava/lang/String;

    const-string v1, "name"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 304
    iget-object v0, p0, Lcom/glympse/android/lib/fo$a;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-virtual {p2, v2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GInvitePrivate;->setName(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 306
    :cond_c
    iget-object v0, p0, Lcom/glympse/android/lib/fo$a;->gE:Ljava/lang/String;

    const-string v1, "address"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 308
    iget-object v0, p0, Lcom/glympse/android/lib/fo$a;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-virtual {p2, v2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GInvitePrivate;->setAddress(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 239
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public startObject(I)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 201
    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 203
    new-instance v0, Lcom/glympse/android/lib/ct;

    invoke-direct {v0}, Lcom/glympse/android/lib/ct;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/fo$a;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    .line 210
    :cond_0
    :goto_0
    return v4

    .line 205
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/fo$a;->gE:Ljava/lang/String;

    const-string v1, "reply"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 207
    new-instance v0, Lcom/glympse/android/lib/fe;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/fe;-><init>(Z)V

    iput-object v0, p0, Lcom/glympse/android/lib/fo$a;->tL:Lcom/glympse/android/lib/GTicketPrivate;

    .line 208
    iget-object v0, p0, Lcom/glympse/android/lib/fo$a;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    new-instance v1, Lcom/glympse/android/lib/eu$c;

    iget-object v2, p0, Lcom/glympse/android/lib/fo$a;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    iget-object v3, p0, Lcom/glympse/android/lib/fo$a;->tL:Lcom/glympse/android/lib/GTicketPrivate;

    invoke-direct {v1, v2, v3}, Lcom/glympse/android/lib/eu$c;-><init>(Lcom/glympse/android/lib/json/GJsonHandlerStack;Lcom/glympse/android/lib/GTicketPrivate;)V

    invoke-interface {v0, v1, v4}, Lcom/glympse/android/lib/json/GJsonHandlerStack;->pushHandler(Lcom/glympse/android/lib/json/GJsonHandler;I)V

    goto :goto_0
.end method
