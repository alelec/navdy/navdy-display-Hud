.class Lcom/glympse/android/lib/eg;
.super Ljava/lang/Object;
.source "Place.java"

# interfaces
.implements Lcom/glympse/android/lib/GPlacePrivate;


# instance fields
.field public _latitude:D

.field public _longitude:D

.field public _name:Ljava/lang/String;

.field public kE:Lcom/glympse/android/lib/GImagePrivate;

.field public rL:Ljava/lang/String;

.field public rM:Ljava/lang/String;

.field public rN:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-wide v0, p0, Lcom/glympse/android/lib/eg;->_latitude:D

    .line 35
    iput-wide v0, p0, Lcom/glympse/android/lib/eg;->_longitude:D

    .line 36
    return-void
.end method

.method public constructor <init>(DDLjava/lang/String;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-wide p1, p0, Lcom/glympse/android/lib/eg;->_latitude:D

    .line 41
    iput-wide p3, p0, Lcom/glympse/android/lib/eg;->_longitude:D

    .line 42
    iput-object p5, p0, Lcom/glympse/android/lib/eg;->_name:Ljava/lang/String;

    .line 43
    return-void
.end method


# virtual methods
.method public clone()Lcom/glympse/android/api/GPlace;
    .locals 4

    .prologue
    .line 85
    new-instance v0, Lcom/glympse/android/lib/eg;

    invoke-direct {v0}, Lcom/glympse/android/lib/eg;-><init>()V

    .line 87
    iget-wide v2, p0, Lcom/glympse/android/lib/eg;->_latitude:D

    iput-wide v2, v0, Lcom/glympse/android/lib/eg;->_latitude:D

    .line 88
    iget-wide v2, p0, Lcom/glympse/android/lib/eg;->_longitude:D

    iput-wide v2, v0, Lcom/glympse/android/lib/eg;->_longitude:D

    .line 89
    iget-object v1, p0, Lcom/glympse/android/lib/eg;->_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/glympse/android/lib/eg;->_name:Ljava/lang/String;

    .line 90
    iget-object v1, p0, Lcom/glympse/android/lib/eg;->rL:Ljava/lang/String;

    iput-object v1, v0, Lcom/glympse/android/lib/eg;->rL:Ljava/lang/String;

    .line 91
    iget-object v1, p0, Lcom/glympse/android/lib/eg;->rM:Ljava/lang/String;

    iput-object v1, v0, Lcom/glympse/android/lib/eg;->rM:Ljava/lang/String;

    .line 92
    iget-object v1, p0, Lcom/glympse/android/lib/eg;->rN:Ljava/lang/String;

    iput-object v1, v0, Lcom/glympse/android/lib/eg;->rN:Ljava/lang/String;

    .line 94
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 16
    invoke-virtual {p0}, Lcom/glympse/android/lib/eg;->clone()Lcom/glympse/android/api/GPlace;

    move-result-object v0

    return-object v0
.end method

.method public decode(Lcom/glympse/android/core/GPrimitive;)V
    .locals 3

    .prologue
    .line 230
    const-string v0, "lt"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/glympse/android/lib/eg;->_latitude:D

    .line 231
    const-string v0, "ln"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/glympse/android/lib/eg;->_longitude:D

    .line 232
    const-string v0, "nm"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/eg;->_name:Ljava/lang/String;

    .line 233
    const-string v0, "al1"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/eg;->rL:Ljava/lang/String;

    .line 234
    const-string v0, "al2"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/eg;->rM:Ljava/lang/String;

    .line 235
    const-string v0, "phn"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/eg;->rN:Ljava/lang/String;

    .line 236
    const-string v0, "imgurl"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 237
    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 239
    new-instance v1, Lcom/glympse/android/lib/cq;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/glympse/android/lib/cq;-><init>(Ljava/lang/String;Lcom/glympse/android/core/GDrawable;)V

    iput-object v1, p0, Lcom/glympse/android/lib/eg;->kE:Lcom/glympse/android/lib/GImagePrivate;

    .line 241
    :cond_0
    return-void
.end method

.method public encode(Lcom/glympse/android/core/GPrimitive;I)V
    .locals 4

    .prologue
    .line 204
    const-string v0, "lt"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lcom/glympse/android/lib/eg;->_latitude:D

    invoke-interface {p1, v0, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;D)V

    .line 205
    const-string v0, "ln"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lcom/glympse/android/lib/eg;->_longitude:D

    invoke-interface {p1, v0, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;D)V

    .line 206
    iget-object v0, p0, Lcom/glympse/android/lib/eg;->_name:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 208
    const-string v0, "nm"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/lib/eg;->_name:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/eg;->rL:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 212
    const-string v0, "al1"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/lib/eg;->rL:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/eg;->rM:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 216
    const-string v0, "al2"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/lib/eg;->rM:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/eg;->rN:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 220
    const-string v0, "phn"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/lib/eg;->rN:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    :cond_3
    iget-object v0, p0, Lcom/glympse/android/lib/eg;->kE:Lcom/glympse/android/lib/GImagePrivate;

    if-eqz v0, :cond_4

    .line 224
    const-string v0, "imgurl"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/lib/eg;->kE:Lcom/glympse/android/lib/GImagePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GImagePrivate;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    :cond_4
    return-void
.end method

.method public getAddressLine1()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/glympse/android/lib/eg;->rL:Ljava/lang/String;

    return-object v0
.end method

.method public getAddressLine2()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/glympse/android/lib/eg;->rM:Ljava/lang/String;

    return-object v0
.end method

.method public getImage()Lcom/glympse/android/api/GImage;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/glympse/android/lib/eg;->kE:Lcom/glympse/android/lib/GImagePrivate;

    return-object v0
.end method

.method public getLatitude()D
    .locals 2

    .prologue
    .line 56
    iget-wide v0, p0, Lcom/glympse/android/lib/eg;->_latitude:D

    return-wide v0
.end method

.method public getLatitudeE6()I
    .locals 4

    .prologue
    .line 66
    iget-wide v0, p0, Lcom/glympse/android/lib/eg;->_latitude:D

    const-wide v2, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method public getLongitude()D
    .locals 2

    .prologue
    .line 61
    iget-wide v0, p0, Lcom/glympse/android/lib/eg;->_longitude:D

    return-wide v0
.end method

.method public getLongitudeE6()I
    .locals 4

    .prologue
    .line 71
    iget-wide v0, p0, Lcom/glympse/android/lib/eg;->_longitude:D

    const-wide v2, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/glympse/android/lib/eg;->_name:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/glympse/android/lib/eg;->rN:Ljava/lang/String;

    return-object v0
.end method

.method public hasLocation()Z
    .locals 4

    .prologue
    .line 51
    iget-wide v0, p0, Lcom/glympse/android/lib/eg;->_latitude:D

    iget-wide v2, p0, Lcom/glympse/android/lib/eg;->_longitude:D

    invoke-static {v0, v1, v2, v3}, Lcom/glympse/android/lib/Location;->isValid(DD)Z

    move-result v0

    return v0
.end method

.method public isEqual(Lcom/glympse/android/core/GCommon;)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    const-wide v6, 0x408f400000000000L    # 1000.0

    .line 178
    check-cast p1, Lcom/glympse/android/lib/eg;

    .line 179
    if-nez p1, :cond_1

    .line 195
    :cond_0
    :goto_0
    return v0

    .line 185
    :cond_1
    iget-object v1, p0, Lcom/glympse/android/lib/eg;->_name:Ljava/lang/String;

    iget-object v2, p1, Lcom/glympse/android/lib/eg;->_name:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/glympse/android/hal/Helpers;->safeEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 191
    iget-wide v2, p0, Lcom/glympse/android/lib/eg;->_latitude:D

    mul-double/2addr v2, v6

    double-to-int v1, v2

    .line 192
    iget-wide v2, p0, Lcom/glympse/android/lib/eg;->_longitude:D

    mul-double/2addr v2, v6

    double-to-int v2, v2

    .line 193
    iget-wide v4, p1, Lcom/glympse/android/lib/eg;->_latitude:D

    mul-double/2addr v4, v6

    double-to-int v3, v4

    .line 194
    iget-wide v4, p1, Lcom/glympse/android/lib/eg;->_longitude:D

    mul-double/2addr v4, v6

    double-to-int v4, v4

    .line 195
    if-ne v1, v3, :cond_0

    if-ne v2, v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setAddressLine1(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 134
    iput-object p1, p0, Lcom/glympse/android/lib/eg;->rL:Ljava/lang/String;

    .line 135
    return-void
.end method

.method public setAddressLine2(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/glympse/android/lib/eg;->rM:Ljava/lang/String;

    .line 140
    return-void
.end method

.method public setImage(Lcom/glympse/android/core/GDrawable;)V
    .locals 5

    .prologue
    const/16 v4, 0x5f

    .line 149
    if-nez p1, :cond_0

    .line 170
    :goto_0
    return-void

    .line 156
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 157
    const-string v1, "glympse-place:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    iget-wide v2, p0, Lcom/glympse/android/lib/eg;->_latitude:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 159
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 160
    iget-wide v2, p0, Lcom/glympse/android/lib/eg;->_longitude:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 161
    iget-object v1, p0, Lcom/glympse/android/lib/eg;->_name:Ljava/lang/String;

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 163
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 164
    iget-object v1, p0, Lcom/glympse/android/lib/eg;->_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    :cond_1
    const-string v1, ".jpg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    new-instance v1, Lcom/glympse/android/lib/cq;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0, p1}, Lcom/glympse/android/lib/cq;-><init>(Ljava/lang/String;Lcom/glympse/android/core/GDrawable;)V

    iput-object v1, p0, Lcom/glympse/android/lib/eg;->kE:Lcom/glympse/android/lib/GImagePrivate;

    goto :goto_0
.end method

.method public setLocation(DD)V
    .locals 1

    .prologue
    .line 128
    iput-wide p1, p0, Lcom/glympse/android/lib/eg;->_latitude:D

    .line 129
    iput-wide p3, p0, Lcom/glympse/android/lib/eg;->_longitude:D

    .line 130
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lcom/glympse/android/lib/eg;->_name:Ljava/lang/String;

    .line 124
    return-void
.end method

.method public setPhoneNumber(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, Lcom/glympse/android/lib/eg;->rN:Ljava/lang/String;

    .line 145
    return-void
.end method
