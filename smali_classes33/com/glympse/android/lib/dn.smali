.class Lcom/glympse/android/lib/dn;
.super Lcom/glympse/android/lib/e;
.source "LocationAppend.java"


# instance fields
.field private _glympse:Lcom/glympse/android/lib/GGlympsePrivate;

.field private fZ:Ljava/lang/String;

.field private ib:J

.field private jC:Ljava/lang/String;

.field private lC:Z

.field private qk:Lcom/glympse/android/hal/GLinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GLinkedList",
            "<",
            "Lcom/glympse/android/core/GLocation;",
            ">;"
        }
    .end annotation
.end field

.field private ql:Z

.field private qm:Lcom/glympse/android/lib/ge$a;


# direct methods
.method public constructor <init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/GTicketPrivate;Lcom/glympse/android/hal/GLinkedList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/glympse/android/lib/GGlympsePrivate;",
            "Lcom/glympse/android/lib/GTicketPrivate;",
            "Lcom/glympse/android/hal/GLinkedList",
            "<",
            "Lcom/glympse/android/core/GLocation;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 29
    invoke-direct {p0}, Lcom/glympse/android/lib/e;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/glympse/android/lib/dn;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 31
    if-nez p2, :cond_0

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lcom/glympse/android/lib/dn;->jC:Ljava/lang/String;

    .line 32
    iput-object p3, p0, Lcom/glympse/android/lib/dn;->qk:Lcom/glympse/android/hal/GLinkedList;

    .line 33
    iget-object v0, p0, Lcom/glympse/android/lib/dn;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->areSiblingTicketsAllowed()Z

    move-result v0

    iput-boolean v0, p0, Lcom/glympse/android/lib/dn;->lC:Z

    .line 34
    iget-object v0, p0, Lcom/glympse/android/lib/dn;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getConfig()Lcom/glympse/android/api/GConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/api/GConfig;->isSharingSpeed()Z

    move-result v0

    iput-boolean v0, p0, Lcom/glympse/android/lib/dn;->ql:Z

    .line 35
    iput-object v1, p0, Lcom/glympse/android/lib/dn;->fZ:Ljava/lang/String;

    .line 37
    new-instance v0, Lcom/glympse/android/lib/ge$a;

    invoke-direct {v0}, Lcom/glympse/android/lib/ge$a;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/dn;->qm:Lcom/glympse/android/lib/ge$a;

    .line 38
    iget-object v0, p0, Lcom/glympse/android/lib/dn;->qm:Lcom/glympse/android/lib/ge$a;

    iput-object v0, p0, Lcom/glympse/android/lib/dn;->gB:Lcom/glympse/android/lib/f;

    .line 40
    iget-object v0, p0, Lcom/glympse/android/lib/dn;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getTicketProtocol()Lcom/glympse/android/lib/GTicketProtocol;

    move-result-object v0

    .line 41
    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketProtocol;->getLastInviteRefreshTime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/glympse/android/lib/dn;->ib:J

    .line 42
    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketProtocol;->startedRefreshingInvites()V

    .line 43
    return-void

    .line 31
    :cond_0
    invoke-interface {p2}, Lcom/glympse/android/lib/GTicketPrivate;->getId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 229
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/dn;->fZ:Ljava/lang/String;

    .line 230
    new-instance v0, Lcom/glympse/android/lib/ge$a;

    invoke-direct {v0}, Lcom/glympse/android/lib/ge$a;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/dn;->qm:Lcom/glympse/android/lib/ge$a;

    .line 231
    iget-object v0, p0, Lcom/glympse/android/lib/dn;->qm:Lcom/glympse/android/lib/ge$a;

    iput-object v0, p0, Lcom/glympse/android/lib/dn;->gB:Lcom/glympse/android/lib/f;

    .line 232
    return-void
.end method

.method public methodType()I
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x2

    return v0
.end method

.method public post()Ljava/lang/String;
    .locals 14

    .prologue
    .line 87
    iget-object v0, p0, Lcom/glympse/android/lib/dn;->fZ:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/glympse/android/lib/dn;->fZ:Ljava/lang/String;

    .line 202
    :goto_0
    return-object v0

    .line 92
    :cond_0
    const/16 v0, 0x8

    new-array v4, v0, [J

    .line 93
    const/16 v0, 0x8

    new-array v5, v0, [J

    .line 94
    const/4 v0, 0x0

    :goto_1
    const/16 v1, 0x8

    if-ge v0, v1, :cond_1

    .line 96
    const-wide/16 v2, 0x0

    aput-wide v2, v4, v0

    .line 94
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 99
    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/glympse/android/lib/dn;->qk:Lcom/glympse/android/hal/GLinkedList;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GLinkedList;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x64

    invoke-direct {v6, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 100
    const/16 v0, 0x5b

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 101
    const/4 v0, 0x0

    .line 102
    iget-object v1, p0, Lcom/glympse/android/lib/dn;->qk:Lcom/glympse/android/hal/GLinkedList;

    invoke-virtual {v1}, Lcom/glympse/android/hal/GLinkedList;->size()I

    move-result v7

    .line 103
    iget-object v1, p0, Lcom/glympse/android/lib/dn;->qk:Lcom/glympse/android/hal/GLinkedList;

    invoke-virtual {v1}, Lcom/glympse/android/hal/GLinkedList;->elements()Ljava/util/Enumeration;

    move-result-object v8

    move v1, v0

    :goto_2
    invoke-interface {v8}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 105
    invoke-interface {v8}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GLocation;

    .line 108
    const/4 v2, 0x0

    invoke-interface {v0}, Lcom/glympse/android/core/GLocation;->getTime()J

    move-result-wide v10

    aput-wide v10, v5, v2

    .line 109
    const/4 v2, 0x1

    invoke-interface {v0}, Lcom/glympse/android/core/GLocation;->getLatitude()D

    move-result-wide v10

    const-wide v12, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v10, v12

    double-to-long v10, v10

    aput-wide v10, v5, v2

    .line 110
    const/4 v2, 0x2

    invoke-interface {v0}, Lcom/glympse/android/core/GLocation;->getLongitude()D

    move-result-wide v10

    const-wide v12, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v10, v12

    double-to-long v10, v10

    aput-wide v10, v5, v2

    .line 111
    const/4 v9, 0x3

    iget-boolean v2, p0, Lcom/glympse/android/lib/dn;->ql:Z

    if-eqz v2, :cond_5

    invoke-interface {v0}, Lcom/glympse/android/core/GLocation;->hasSpeed()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 112
    invoke-interface {v0}, Lcom/glympse/android/core/GLocation;->getSpeed()F

    move-result v2

    const/high16 v3, 0x42c80000    # 100.0f

    mul-float/2addr v2, v3

    float-to-long v2, v2

    :goto_3
    aput-wide v2, v5, v9

    .line 114
    const/4 v9, 0x4

    invoke-interface {v0}, Lcom/glympse/android/core/GLocation;->hasBearing()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 115
    invoke-interface {v0}, Lcom/glympse/android/core/GLocation;->getBearing()F

    move-result v2

    float-to-long v2, v2

    :goto_4
    aput-wide v2, v5, v9

    .line 117
    const/4 v9, 0x5

    invoke-interface {v0}, Lcom/glympse/android/core/GLocation;->hasAltitude()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 118
    invoke-interface {v0}, Lcom/glympse/android/core/GLocation;->getAltitude()F

    move-result v2

    float-to-long v2, v2

    :goto_5
    aput-wide v2, v5, v9

    .line 120
    const/4 v9, 0x6

    invoke-interface {v0}, Lcom/glympse/android/core/GLocation;->hasHAccuracy()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 121
    invoke-interface {v0}, Lcom/glympse/android/core/GLocation;->getHAccuracy()F

    move-result v2

    float-to-long v2, v2

    :goto_6
    aput-wide v2, v5, v9

    .line 123
    const/4 v9, 0x7

    invoke-interface {v0}, Lcom/glympse/android/core/GLocation;->hasVAccuracy()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 124
    invoke-interface {v0}, Lcom/glympse/android/core/GLocation;->getVAccuracy()F

    move-result v0

    float-to-long v2, v0

    :goto_7
    aput-wide v2, v5, v9

    .line 127
    const/4 v0, 0x1

    .line 132
    if-eqz v1, :cond_2

    add-int/lit8 v2, v7, -0x1

    if-eq v2, v1, :cond_2

    .line 135
    const/4 v0, 0x0

    .line 138
    const/4 v2, 0x1

    :goto_8
    const/16 v3, 0x8

    if-ge v2, v3, :cond_2

    .line 140
    aget-wide v10, v4, v2

    aget-wide v12, v5, v2

    cmp-long v3, v10, v12

    if-eqz v3, :cond_a

    .line 142
    const/4 v0, 0x1

    .line 148
    :cond_2
    if-eqz v0, :cond_10

    .line 151
    const/4 v0, 0x7

    move v2, v0

    .line 152
    :goto_9
    if-ltz v2, :cond_3

    .line 154
    const-wide/32 v10, 0xfffffff

    aget-wide v12, v5, v2

    cmp-long v0, v10, v12

    if-eqz v0, :cond_b

    .line 161
    :cond_3
    const/16 v0, 0x5b

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 162
    const/4 v0, 0x0

    :goto_a
    if-gt v0, v2, :cond_e

    .line 164
    const-wide/32 v10, 0xfffffff

    aget-wide v12, v5, v0

    cmp-long v3, v10, v12

    if-eqz v3, :cond_d

    .line 166
    const-wide/32 v10, 0xfffffff

    aget-wide v12, v4, v0

    cmp-long v3, v10, v12

    if-eqz v3, :cond_c

    .line 168
    aget-wide v10, v5, v0

    aget-wide v12, v4, v0

    sub-long/2addr v10, v12

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 179
    :goto_b
    if-eq v2, v0, :cond_4

    .line 181
    const/16 v3, 0x2c

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 162
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 112
    :cond_5
    const-wide/32 v2, 0xfffffff

    goto/16 :goto_3

    .line 115
    :cond_6
    const-wide/32 v2, 0xfffffff

    goto/16 :goto_4

    .line 118
    :cond_7
    const-wide/32 v2, 0xfffffff

    goto :goto_5

    .line 121
    :cond_8
    const-wide/32 v2, 0xfffffff

    goto :goto_6

    .line 124
    :cond_9
    const-wide/32 v2, 0xfffffff

    goto :goto_7

    .line 138
    :cond_a
    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    .line 152
    :cond_b
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_9

    .line 172
    :cond_c
    aget-wide v10, v5, v0

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    goto :goto_b

    .line 177
    :cond_d
    const-string v3, "null"

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_b

    .line 184
    :cond_e
    const/16 v0, 0x5d

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 187
    add-int/lit8 v0, v7, -0x1

    if-eq v1, v0, :cond_f

    .line 189
    const/16 v0, 0x2c

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 193
    :cond_f
    const/4 v0, 0x0

    :goto_c
    const/16 v2, 0x8

    if-ge v0, v2, :cond_10

    .line 195
    aget-wide v2, v5, v0

    aput-wide v2, v4, v0

    .line 193
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 103
    :cond_10
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_2

    .line 199
    :cond_11
    const/16 v0, 0x5d

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 201
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/dn;->fZ:Ljava/lang/String;

    .line 202
    iget-object v0, p0, Lcom/glympse/android/lib/dn;->fZ:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public process()Z
    .locals 2

    .prologue
    .line 208
    iget-object v0, p0, Lcom/glympse/android/lib/dn;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getTicketProtocol()Lcom/glympse/android/lib/GTicketProtocol;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketProtocol;->doneRefreshingInvites()V

    .line 211
    iget-object v0, p0, Lcom/glympse/android/lib/dn;->qm:Lcom/glympse/android/lib/ge$a;

    iget-object v0, v0, Lcom/glympse/android/lib/ge$a;->gF:Ljava/lang/String;

    const-string v1, "ok"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 214
    iget-object v0, p0, Lcom/glympse/android/lib/dn;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/dn;->qm:Lcom/glympse/android/lib/ge$a;

    invoke-static {v0, v1}, Lcom/glympse/android/lib/ge;->a(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/ge$a;)V

    .line 217
    iget-object v0, p0, Lcom/glympse/android/lib/dn;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getServerPost()Lcom/glympse/android/lib/GServerPost;

    move-result-object v0

    const/16 v1, 0x800

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GServerPost;->rememberEvents(I)V

    .line 224
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 222
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public shouldAdd(Lcom/glympse/android/lib/GApiEndpoint;)I
    .locals 1

    .prologue
    .line 47
    instance-of v0, p1, Lcom/glympse/android/lib/ge;

    if-eqz v0, :cond_0

    .line 49
    const/4 v0, 0x4

    .line 52
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public url(Ljava/lang/StringBuilder;)Z
    .locals 4

    .prologue
    .line 57
    const-string v0, "tickets/append_location?since="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 58
    iget-wide v0, p0, Lcom/glympse/android/lib/dn;->ib:J

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 62
    const-wide/16 v0, 0x0

    iget-wide v2, p0, Lcom/glympse/android/lib/dn;->ib:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 64
    const-string v0, "&expired=true"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/dn;->jC:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 69
    const-string v0, "&ids="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    iget-object v0, p0, Lcom/glympse/android/lib/dn;->jC:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    :cond_1
    iget-boolean v0, p0, Lcom/glympse/android/lib/dn;->lC:Z

    if-eqz v0, :cond_2

    .line 75
    const-string v0, "&siblings=true"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    :cond_2
    const/4 v0, 0x1

    return v0
.end method
