.class Lcom/glympse/android/lib/de;
.super Ljava/lang/Object;
.source "Job.java"

# interfaces
.implements Lcom/glympse/android/lib/GJob;


# instance fields
.field protected _aborted:Z

.field protected _handler:Lcom/glympse/android/core/GHandler;

.field protected _jobQueue:Lcom/glympse/android/lib/GJobQueue;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/de;->_jobQueue:Lcom/glympse/android/lib/GJobQueue;

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/glympse/android/lib/de;->_aborted:Z

    .line 29
    return-void
.end method


# virtual methods
.method public abort()V
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/glympse/android/lib/de;->_aborted:Z

    .line 77
    return-void
.end method

.method public isAborted()Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/glympse/android/lib/de;->_aborted:Z

    return v0
.end method

.method public onAbort()V
    .locals 0

    .prologue
    .line 54
    return-void
.end method

.method public onComplete()V
    .locals 0

    .prologue
    .line 58
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 40
    iput-object v0, p0, Lcom/glympse/android/lib/de;->_jobQueue:Lcom/glympse/android/lib/GJobQueue;

    .line 41
    iput-object v0, p0, Lcom/glympse/android/lib/de;->_handler:Lcom/glympse/android/core/GHandler;

    .line 42
    return-void
.end method

.method public onProcess()V
    .locals 0

    .prologue
    .line 50
    return-void
.end method

.method public onRetry()V
    .locals 0

    .prologue
    .line 46
    return-void
.end method

.method public onSchedule(Lcom/glympse/android/lib/GJobQueue;Lcom/glympse/android/core/GHandler;)V
    .locals 1

    .prologue
    .line 33
    iput-object p1, p0, Lcom/glympse/android/lib/de;->_jobQueue:Lcom/glympse/android/lib/GJobQueue;

    .line 34
    iput-object p2, p0, Lcom/glympse/android/lib/de;->_handler:Lcom/glympse/android/core/GHandler;

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/glympse/android/lib/de;->_aborted:Z

    .line 36
    return-void
.end method

.method public reset()V
    .locals 0

    .prologue
    .line 67
    return-void
.end method

.method public useHandler()Z
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    return v0
.end method
