.class Lcom/glympse/android/lib/d$a;
.super Lcom/glympse/android/lib/ew;
.source "AccountManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/lib/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# direct methods
.method public constructor <init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/r;)V
    .locals 4

    .prologue
    .line 389
    invoke-direct {p0, p1, p2}, Lcom/glympse/android/lib/ew;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/r;)V

    .line 392
    invoke-interface {p1}, Lcom/glympse/android/lib/GGlympsePrivate;->getBaseUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/glympse/android/lib/UrlParser;->prepareAuthUrlServer(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/d$a;->f:Ljava/lang/String;

    .line 395
    iget-object v0, p0, Lcom/glympse/android/lib/d$a;->_backOffPolicy:Lcom/glympse/android/lib/GBackOffPolicy;

    invoke-static {}, Lcom/glympse/android/lib/d$a;->an()D

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/glympse/android/lib/GBackOffPolicy;->setMultiplier(D)V

    .line 396
    return-void
.end method

.method public static an()D
    .locals 2

    .prologue
    .line 384
    const-wide/high16 v0, 0x4004000000000000L    # 2.5

    return-wide v0
.end method
