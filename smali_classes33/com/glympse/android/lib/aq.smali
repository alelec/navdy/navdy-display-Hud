.class Lcom/glympse/android/lib/aq;
.super Lcom/glympse/android/lib/HttpJob;
.source "DirectionsHereJob.java"


# instance fields
.field private kh:J

.field private kk:Lcom/glympse/android/lib/GDirectionsPrivate;

.field private ko:Lcom/glympse/android/lib/GTrackPrivate;

.field private ks:Z

.field private kt:Ljava/lang/String;

.field private ku:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/glympse/android/core/GPrimitive;Lcom/glympse/android/lib/GDirectionsPrivate;)V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/glympse/android/lib/HttpJob;-><init>()V

    .line 33
    iput-object p2, p0, Lcom/glympse/android/lib/aq;->kk:Lcom/glympse/android/lib/GDirectionsPrivate;

    .line 34
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/glympse/android/lib/aq;->kh:J

    .line 37
    const-string v0, "data"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 38
    const-string v1, "enterprise"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getBool(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/glympse/android/lib/aq;->ks:Z

    .line 39
    const-string v1, "app_id"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/glympse/android/lib/aq;->kt:Ljava/lang/String;

    .line 40
    const-string v1, "app_code"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/aq;->ku:Ljava/lang/String;

    .line 41
    return-void
.end method

.method private e(Lcom/glympse/android/core/GPrimitive;)V
    .locals 19

    .prologue
    .line 145
    if-nez p1, :cond_1

    .line 211
    :cond_0
    :goto_0
    return-void

    .line 150
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/glympse/android/lib/aq;->ks:Z

    if-eqz v2, :cond_3

    const-string v2, "Response"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 151
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/glympse/android/lib/aq;->ks:Z

    if-eqz v3, :cond_4

    const-string v3, "Route"

    invoke-static {v3}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 152
    :goto_2
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/glympse/android/lib/aq;->ks:Z

    if-eqz v4, :cond_5

    const-string v4, "Summary"

    invoke-static {v4}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 153
    :goto_3
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/glympse/android/lib/aq;->ks:Z

    if-eqz v5, :cond_6

    const-string v5, "Distance"

    invoke-static {v5}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 154
    :goto_4
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/glympse/android/lib/aq;->ks:Z

    if-eqz v6, :cond_7

    const-string v6, "BaseTime"

    invoke-static {v6}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 155
    :goto_5
    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/glympse/android/lib/aq;->ks:Z

    if-eqz v7, :cond_8

    const-string v7, "TrafficTime"

    invoke-static {v7}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 156
    :goto_6
    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/glympse/android/lib/aq;->ks:Z

    if-eqz v8, :cond_9

    const-string v8, "Shape"

    invoke-static {v8}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 158
    :goto_7
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v2

    .line 159
    if-eqz v2, :cond_0

    .line 163
    invoke-interface {v2, v3}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v2

    .line 164
    if-eqz v2, :cond_0

    .line 168
    invoke-interface {v2}, Lcom/glympse/android/core/GPrimitive;->size()I

    move-result v3

    if-eqz v3, :cond_0

    .line 172
    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/glympse/android/core/GPrimitive;->get(I)Lcom/glympse/android/core/GPrimitive;

    move-result-object v9

    .line 175
    invoke-interface {v9, v4}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v2

    .line 176
    if-eqz v2, :cond_0

    .line 180
    invoke-interface {v2, v5}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    long-to-int v0, v4

    move/from16 v16, v0

    .line 183
    invoke-interface {v2, v7}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 184
    invoke-interface {v2, v7}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 186
    :goto_8
    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/glympse/android/lib/aq;->kh:J

    .line 189
    invoke-interface {v9, v8}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v17

    .line 190
    if-eqz v17, :cond_0

    .line 192
    new-instance v2, Lcom/glympse/android/lib/fs;

    const/4 v3, 0x3

    invoke-direct {v2, v3}, Lcom/glympse/android/lib/fs;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/glympse/android/lib/aq;->ko:Lcom/glympse/android/lib/GTrackPrivate;

    .line 194
    invoke-interface/range {v17 .. v17}, Lcom/glympse/android/core/GPrimitive;->size()I

    move-result v18

    .line 195
    const/4 v2, 0x0

    move v15, v2

    :goto_9
    move/from16 v0, v18

    if-ge v15, v0, :cond_c

    .line 197
    move-object/from16 v0, v17

    invoke-interface {v0, v15}, Lcom/glympse/android/core/GPrimitive;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 198
    const-string v3, ","

    invoke-static {v2, v3}, Lcom/glympse/android/hal/Helpers;->split(Ljava/lang/String;Ljava/lang/String;)Lcom/glympse/android/hal/GVector;

    move-result-object v3

    .line 199
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v2

    const/4 v4, 0x2

    if-ge v2, v4, :cond_b

    .line 195
    :cond_2
    :goto_a
    add-int/lit8 v2, v15, 0x1

    move v15, v2

    goto :goto_9

    .line 150
    :cond_3
    const-string v2, "response"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 151
    :cond_4
    const-string v3, "route"

    invoke-static {v3}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_2

    .line 152
    :cond_5
    const-string v4, "summary"

    invoke-static {v4}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_3

    .line 153
    :cond_6
    const-string v5, "distance"

    invoke-static {v5}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_4

    .line 154
    :cond_7
    const-string v6, "baseTime"

    invoke-static {v6}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_5

    .line 155
    :cond_8
    const-string v7, "trafficTime"

    invoke-static {v7}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_6

    .line 156
    :cond_9
    const-string v8, "shape"

    invoke-static {v8}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_7

    .line 185
    :cond_a
    invoke-interface {v2, v6}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    goto :goto_8

    .line 203
    :cond_b
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->toDouble(Ljava/lang/String;)D

    move-result-wide v6

    .line 204
    const/4 v2, 0x1

    invoke-virtual {v3, v2}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->toDouble(Ljava/lang/String;)D

    move-result-wide v8

    .line 205
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/glympse/android/lib/aq;->ko:Lcom/glympse/android/lib/GTrackPrivate;

    new-instance v3, Lcom/glympse/android/lib/Location;

    const-wide/16 v4, 0x0

    const/high16 v10, 0x7fc00000    # NaNf

    const/high16 v11, 0x7fc00000    # NaNf

    const/high16 v12, 0x7fc00000    # NaNf

    const/high16 v13, 0x7fc00000    # NaNf

    const/high16 v14, 0x7fc00000    # NaNf

    invoke-direct/range {v3 .. v14}, Lcom/glympse/android/lib/Location;-><init>(JDDFFFFF)V

    invoke-interface {v2, v3}, Lcom/glympse/android/lib/GTrackPrivate;->addCore(Lcom/glympse/android/core/GLocation;)V

    goto :goto_a

    .line 209
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/glympse/android/lib/aq;->ko:Lcom/glympse/android/lib/GTrackPrivate;

    move/from16 v0, v16

    invoke-interface {v2, v0}, Lcom/glympse/android/lib/GTrackPrivate;->setDistance(I)V

    goto/16 :goto_0
.end method


# virtual methods
.method public checkResponse(II)Z
    .locals 1

    .prologue
    .line 117
    const/16 v0, 0xc8

    if-ne v0, p1, :cond_0

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRetryInterval(I)I
    .locals 1

    .prologue
    .line 122
    const/4 v0, -0x1

    return v0
.end method

.method public onAbort()V
    .locals 1

    .prologue
    .line 134
    invoke-super {p0}, Lcom/glympse/android/lib/HttpJob;->onAbort()V

    .line 136
    iget-object v0, p0, Lcom/glympse/android/lib/aq;->kk:Lcom/glympse/android/lib/GDirectionsPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GDirectionsPrivate;->failed()V

    .line 137
    return-void
.end method

.method public onComplete()V
    .locals 7

    .prologue
    .line 127
    invoke-super {p0}, Lcom/glympse/android/lib/HttpJob;->onComplete()V

    .line 129
    iget-object v1, p0, Lcom/glympse/android/lib/aq;->kk:Lcom/glympse/android/lib/GDirectionsPrivate;

    iget-object v0, p0, Lcom/glympse/android/lib/aq;->kk:Lcom/glympse/android/lib/GDirectionsPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GDirectionsPrivate;->getRequestTime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/glympse/android/lib/aq;->kh:J

    iget-object v6, p0, Lcom/glympse/android/lib/aq;->ko:Lcom/glympse/android/lib/GTrackPrivate;

    invoke-interface/range {v1 .. v6}, Lcom/glympse/android/lib/GDirectionsPrivate;->set(JJLcom/glympse/android/api/GTrack;)V

    .line 130
    return-void
.end method

.method public onPreProcess()V
    .locals 5

    .prologue
    const/16 v4, 0x2c

    .line 46
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v0, 0x100

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 47
    iget-boolean v0, p0, Lcom/glympse/android/lib/aq;->ks:Z

    if-eqz v0, :cond_0

    const-string v0, "http://route.nlp.nokia.com/routing/6.2/calculateroute.json"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    const-string v0, "?app_id="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 53
    iget-object v0, p0, Lcom/glympse/android/lib/aq;->kt:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    const-string v0, "&app_code="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55
    iget-object v0, p0, Lcom/glympse/android/lib/aq;->ku:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 58
    const-string v0, "&routeattributes=sh"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    iget-object v0, p0, Lcom/glympse/android/lib/aq;->kk:Lcom/glympse/android/lib/GDirectionsPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GDirectionsPrivate;->getOrigin()Lcom/glympse/android/core/GLatLng;

    move-result-object v0

    .line 62
    const-string v2, "&waypoint0=geo!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    invoke-interface {v0}, Lcom/glympse/android/core/GLatLng;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 64
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 65
    invoke-interface {v0}, Lcom/glympse/android/core/GLatLng;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 68
    iget-object v0, p0, Lcom/glympse/android/lib/aq;->kk:Lcom/glympse/android/lib/GDirectionsPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GDirectionsPrivate;->getDestination()Lcom/glympse/android/core/GLatLng;

    move-result-object v0

    .line 69
    const-string v2, "&waypoint1=geo!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    invoke-interface {v0}, Lcom/glympse/android/core/GLatLng;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 71
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 72
    invoke-interface {v0}, Lcom/glympse/android/core/GLatLng;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 75
    iget-boolean v0, p0, Lcom/glympse/android/lib/aq;->ks:Z

    if-eqz v0, :cond_1

    const-string v0, "&mode=fastestNow;"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    iget-object v0, p0, Lcom/glympse/android/lib/aq;->kk:Lcom/glympse/android/lib/GDirectionsPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GDirectionsPrivate;->getTravelMode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 87
    :pswitch_0
    const-string v0, "car"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    :goto_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 93
    iget-object v1, p0, Lcom/glympse/android/lib/aq;->_httpConnection:Lcom/glympse/android/hal/GHttpConnection;

    invoke-interface {v1, v0}, Lcom/glympse/android/hal/GHttpConnection;->setUrl(Ljava/lang/String;)V

    .line 94
    return-void

    .line 47
    :cond_0
    const-string v0, "http://route.api.here.com/routing/7.2/calculateroute.json"

    goto :goto_0

    .line 75
    :cond_1
    const-string v0, "&mode=fastest;"

    goto :goto_1

    .line 81
    :pswitch_1
    const-string v0, "car"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 84
    :pswitch_2
    const-string v0, "pedestrian"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 76
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onProcessResponse()V
    .locals 2

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/glympse/android/lib/aq;->isSucceeded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 113
    :cond_0
    :goto_0
    return-void

    .line 105
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/aq;->_httpConnection:Lcom/glympse/android/hal/GHttpConnection;

    invoke-interface {v0}, Lcom/glympse/android/hal/GHttpConnection;->getResponseDataString()Ljava/lang/String;

    move-result-object v0

    .line 106
    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 112
    invoke-static {v0}, Lcom/glympse/android/lib/json/JsonSerializer;->toPrimitive(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/aq;->e(Lcom/glympse/android/core/GPrimitive;)V

    goto :goto_0
.end method
