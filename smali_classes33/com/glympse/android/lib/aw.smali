.class Lcom/glympse/android/lib/aw;
.super Ljava/lang/Object;
.source "EtaTrigger.java"

# interfaces
.implements Lcom/glympse/android/lib/GEtaTriggerPrivate;


# instance fields
.field private ic:Lcom/glympse/android/lib/fv;

.field private kN:J

.field private kO:I

.field private kP:Lcom/glympse/android/api/GTicket;

.field private kQ:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Lcom/glympse/android/lib/fv;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/fv;-><init>(I)V

    iput-object v0, p0, Lcom/glympse/android/lib/aw;->ic:Lcom/glympse/android/lib/fv;

    .line 28
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZLcom/glympse/android/api/GTicket;JILcom/glympse/android/api/GTicket;)V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Lcom/glympse/android/lib/fv;

    const/4 v1, 0x3

    invoke-direct {v0, v1, p1, p2, p3}, Lcom/glympse/android/lib/fv;-><init>(ILjava/lang/String;ZLcom/glympse/android/api/GTicket;)V

    iput-object v0, p0, Lcom/glympse/android/lib/aw;->ic:Lcom/glympse/android/lib/fv;

    .line 33
    iput-wide p4, p0, Lcom/glympse/android/lib/aw;->kN:J

    .line 34
    iput p6, p0, Lcom/glympse/android/lib/aw;->kO:I

    .line 35
    iput-object p7, p0, Lcom/glympse/android/lib/aw;->kP:Lcom/glympse/android/api/GTicket;

    .line 36
    invoke-interface {p7}, Lcom/glympse/android/api/GTicket;->getId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/aw;->kQ:Ljava/lang/String;

    .line 37
    return-void
.end method


# virtual methods
.method public autoSend()Z
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/glympse/android/lib/aw;->ic:Lcom/glympse/android/lib/fv;

    invoke-virtual {v0}, Lcom/glympse/android/lib/fv;->autoSend()Z

    move-result v0

    return v0
.end method

.method public decode(Lcom/glympse/android/core/GPrimitive;)V
    .locals 2

    .prologue
    .line 125
    iget-object v0, p0, Lcom/glympse/android/lib/aw;->ic:Lcom/glympse/android/lib/fv;

    invoke-virtual {v0, p1}, Lcom/glympse/android/lib/fv;->decode(Lcom/glympse/android/core/GPrimitive;)V

    .line 127
    const-string v0, "thrshld"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/glympse/android/lib/aw;->kN:J

    .line 128
    const-string v0, "trns"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/glympse/android/lib/aw;->kO:I

    .line 130
    const-string v0, "etatckt"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/aw;->kQ:Ljava/lang/String;

    .line 131
    return-void
.end method

.method public encode(Lcom/glympse/android/core/GPrimitive;I)V
    .locals 4

    .prologue
    .line 112
    iget-object v0, p0, Lcom/glympse/android/lib/aw;->ic:Lcom/glympse/android/lib/fv;

    invoke-virtual {v0, p1, p2}, Lcom/glympse/android/lib/fv;->encode(Lcom/glympse/android/core/GPrimitive;I)V

    .line 114
    const-string v0, "thrshld"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lcom/glympse/android/lib/aw;->kN:J

    invoke-interface {p1, v0, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 115
    const-string v0, "trns"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/glympse/android/lib/aw;->kO:I

    int-to-long v2, v1

    invoke-interface {p1, v0, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 117
    iget-object v0, p0, Lcom/glympse/android/lib/aw;->kP:Lcom/glympse/android/api/GTicket;

    if-eqz v0, :cond_0

    .line 119
    const-string v0, "etatckt"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/lib/aw;->kP:Lcom/glympse/android/api/GTicket;

    invoke-interface {v1}, Lcom/glympse/android/api/GTicket;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    :cond_0
    return-void
.end method

.method public getEtaTicket()Lcom/glympse/android/api/GTicket;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/glympse/android/lib/aw;->kP:Lcom/glympse/android/api/GTicket;

    return-object v0
.end method

.method public getEtaTicketId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/glympse/android/lib/aw;->kQ:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/glympse/android/lib/aw;->ic:Lcom/glympse/android/lib/fv;

    invoke-virtual {v0}, Lcom/glympse/android/lib/fv;->getId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/glympse/android/lib/aw;->ic:Lcom/glympse/android/lib/fv;

    invoke-virtual {v0}, Lcom/glympse/android/lib/fv;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getThreshold()J
    .locals 2

    .prologue
    .line 84
    iget-wide v0, p0, Lcom/glympse/android/lib/aw;->kN:J

    return-wide v0
.end method

.method public getTicket()Lcom/glympse/android/api/GTicket;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/glympse/android/lib/aw;->ic:Lcom/glympse/android/lib/fv;

    invoke-virtual {v0}, Lcom/glympse/android/lib/fv;->getTicket()Lcom/glympse/android/api/GTicket;

    move-result-object v0

    return-object v0
.end method

.method public getTimeConstraint()Lcom/glympse/android/api/GTimeConstraint;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/glympse/android/lib/aw;->ic:Lcom/glympse/android/lib/fv;

    invoke-virtual {v0}, Lcom/glympse/android/lib/fv;->getTimeConstraint()Lcom/glympse/android/api/GTimeConstraint;

    move-result-object v0

    return-object v0
.end method

.method public getTransition()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/glympse/android/lib/aw;->kO:I

    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/glympse/android/lib/aw;->ic:Lcom/glympse/android/lib/fv;

    invoke-virtual {v0}, Lcom/glympse/android/lib/fv;->getType()I

    move-result v0

    return v0
.end method

.method public setEtaTicket(Lcom/glympse/android/api/GTicket;)V
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lcom/glympse/android/lib/aw;->kP:Lcom/glympse/android/api/GTicket;

    .line 104
    return-void
.end method
