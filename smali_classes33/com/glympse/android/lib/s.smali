.class Lcom/glympse/android/lib/s;
.super Ljava/lang/Object;
.source "BatteryManager.java"

# interfaces
.implements Lcom/glympse/android/hal/GBatteryListener;
.implements Lcom/glympse/android/lib/GBatteryManagerPrivate;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/lib/s$a;
    }
.end annotation


# instance fields
.field private I:Z

.field private _glympse:Lcom/glympse/android/lib/GGlympsePrivate;

.field private hK:I

.field private hL:I

.field private hM:I

.field private hN:Lcom/glympse/android/hal/GBatteryProvider;

.field private hO:Z

.field private hP:Z

.field private hQ:Z

.field private hR:Lcom/glympse/android/hal/GTimer;

.field private he:Lcom/glympse/android/lib/CommonSink;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput v2, p0, Lcom/glympse/android/lib/s;->hK:I

    .line 50
    const/16 v0, 0x64

    iput v0, p0, Lcom/glympse/android/lib/s;->hL:I

    .line 51
    const/4 v0, -0x1

    iput v0, p0, Lcom/glympse/android/lib/s;->hM:I

    .line 52
    new-instance v0, Lcom/glympse/android/lib/CommonSink;

    const-string v1, "BatteryManager"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/CommonSink;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/glympse/android/lib/s;->he:Lcom/glympse/android/lib/CommonSink;

    .line 53
    iput-boolean v3, p0, Lcom/glympse/android/lib/s;->hO:Z

    .line 54
    iput-boolean v2, p0, Lcom/glympse/android/lib/s;->hP:Z

    .line 55
    iput-boolean v2, p0, Lcom/glympse/android/lib/s;->I:Z

    .line 56
    iput-boolean v3, p0, Lcom/glympse/android/lib/s;->hQ:Z

    .line 57
    return-void
.end method

.method private aE()Z
    .locals 2

    .prologue
    .line 169
    iget-object v0, p0, Lcom/glympse/android/lib/s;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getServerPost()Lcom/glympse/android/lib/GServerPost;

    move-result-object v0

    .line 175
    invoke-virtual {p0}, Lcom/glympse/android/lib/s;->isBatteryOk()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/glympse/android/lib/s;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 176
    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->isSharing()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, Lcom/glympse/android/lib/GServerPost;->haveLocationsToPost()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 174
    :goto_0
    return v0

    .line 176
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aF()V
    .locals 4

    .prologue
    .line 412
    iget-object v0, p0, Lcom/glympse/android/lib/s;->hR:Lcom/glympse/android/hal/GTimer;

    if-nez v0, :cond_0

    .line 414
    const/4 v0, 0x1

    const-string v1, "[BatteryManager.startWatchdogTimer]"

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 416
    new-instance v0, Lcom/glympse/android/lib/s$a;

    iget-object v1, p0, Lcom/glympse/android/lib/s;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/s$a;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 417
    const-wide/16 v2, 0x7530

    iget-object v1, p0, Lcom/glympse/android/lib/s;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->getHandler()Lcom/glympse/android/core/GHandler;

    move-result-object v1

    invoke-static {v0, v2, v3, v1}, Lcom/glympse/android/hal/HalFactory;->createTimer(Ljava/lang/Runnable;JLcom/glympse/android/core/GHandler;)Lcom/glympse/android/hal/GTimer;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/s;->hR:Lcom/glympse/android/hal/GTimer;

    .line 418
    iget-object v0, p0, Lcom/glympse/android/lib/s;->hR:Lcom/glympse/android/hal/GTimer;

    invoke-interface {v0}, Lcom/glympse/android/hal/GTimer;->start()V

    .line 420
    :cond_0
    return-void
.end method

.method private aG()V
    .locals 2

    .prologue
    .line 424
    iget-object v0, p0, Lcom/glympse/android/lib/s;->hR:Lcom/glympse/android/hal/GTimer;

    if-eqz v0, :cond_0

    .line 426
    const/4 v0, 0x1

    const-string v1, "[BatteryManager.stopWatchdogTimer]"

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 428
    iget-object v0, p0, Lcom/glympse/android/lib/s;->hR:Lcom/glympse/android/hal/GTimer;

    invoke-interface {v0}, Lcom/glympse/android/hal/GTimer;->stop()V

    .line 429
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/s;->hR:Lcom/glympse/android/hal/GTimer;

    .line 431
    :cond_0
    return-void
.end method


# virtual methods
.method public addListener(Lcom/glympse/android/api/GEventListener;)Z
    .locals 1

    .prologue
    .line 358
    iget-object v0, p0, Lcom/glympse/android/lib/s;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1}, Lcom/glympse/android/lib/CommonSink;->addListener(Lcom/glympse/android/api/GEventListener;)Z

    move-result v0

    return v0
.end method

.method public associateContext(JLjava/lang/Object;)V
    .locals 1

    .prologue
    .line 383
    iget-object v0, p0, Lcom/glympse/android/lib/s;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1, p2, p3}, Lcom/glympse/android/lib/CommonSink;->associateContext(JLjava/lang/Object;)V

    .line 384
    return-void
.end method

.method public clearContext(J)V
    .locals 1

    .prologue
    .line 393
    iget-object v0, p0, Lcom/glympse/android/lib/s;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1, p2}, Lcom/glympse/android/lib/CommonSink;->clearContext(J)V

    .line 394
    return-void
.end method

.method public deriveContext(Lcom/glympse/android/api/GEventSink;)V
    .locals 1

    .prologue
    .line 403
    iget-object v0, p0, Lcom/glympse/android/lib/s;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1}, Lcom/glympse/android/lib/CommonSink;->deriveContext(Lcom/glympse/android/api/GEventSink;)V

    .line 404
    return-void
.end method

.method public enableWakeLock(Z)V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/glympse/android/lib/s;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-nez v0, :cond_1

    .line 143
    :cond_0
    :goto_0
    return-void

    .line 135
    :cond_1
    iget-boolean v0, p0, Lcom/glympse/android/lib/s;->hQ:Z

    if-eq p1, v0, :cond_0

    .line 139
    iput-boolean p1, p0, Lcom/glympse/android/lib/s;->hQ:Z

    .line 142
    invoke-virtual {p0}, Lcom/glympse/android/lib/s;->setKeepAwake()V

    goto :goto_0
.end method

.method public eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V
    .locals 6

    .prologue
    .line 373
    iget-object v0, p0, Lcom/glympse/android/lib/s;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/glympse/android/api/GEventSink;

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/glympse/android/lib/CommonSink;->eventsOccurred(Lcom/glympse/android/api/GEventSink;Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 374
    return-void
.end method

.method public getContext(J)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 388
    iget-object v0, p0, Lcom/glympse/android/lib/s;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1, p2}, Lcom/glympse/android/lib/CommonSink;->getContext(J)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getContextKeys()Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Enumeration",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 398
    iget-object v0, p0, Lcom/glympse/android/lib/s;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0}, Lcom/glympse/android/lib/CommonSink;->getContextKeys()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public getListener()Lcom/glympse/android/hal/GBatteryListener;
    .locals 1

    .prologue
    .line 321
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/hal/GBatteryListener;

    return-object v0
.end method

.method public getListeners()Lcom/glympse/android/core/GArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/glympse/android/core/GArray",
            "<",
            "Lcom/glympse/android/api/GEventListener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 368
    iget-object v0, p0, Lcom/glympse/android/lib/s;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0}, Lcom/glympse/android/lib/CommonSink;->getListeners()Lcom/glympse/android/core/GArray;

    move-result-object v0

    return-object v0
.end method

.method public getProvider()Lcom/glympse/android/hal/GBatteryProvider;
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lcom/glympse/android/lib/s;->hN:Lcom/glympse/android/hal/GBatteryProvider;

    return-object v0
.end method

.method public hasContext(J)Z
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lcom/glympse/android/lib/s;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1, p2}, Lcom/glympse/android/lib/CommonSink;->hasContext(J)Z

    move-result v0

    return v0
.end method

.method public isBatteryForce()Z
    .locals 1

    .prologue
    .line 98
    iget-boolean v0, p0, Lcom/glympse/android/lib/s;->hP:Z

    return v0
.end method

.method public isBatteryLevelGood()Z
    .locals 1

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/glympse/android/lib/s;->hO:Z

    return v0
.end method

.method public isBatteryOk()Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/glympse/android/lib/s;->hO:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/glympse/android/lib/s;->hP:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isWakeLockEnabled()Z
    .locals 1

    .prologue
    .line 125
    iget-boolean v0, p0, Lcom/glympse/android/lib/s;->hQ:Z

    return v0
.end method

.method public logBatteryEvent(Lcom/glympse/android/api/GTicket;)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 341
    iget-object v0, p0, Lcom/glympse/android/lib/s;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->isActive()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    shl-int/lit8 v4, v0, 0x0

    .line 342
    iget-boolean v0, p0, Lcom/glympse/android/lib/s;->hO:Z

    if-eqz v0, :cond_1

    move v0, v2

    :goto_1
    shl-int/lit8 v0, v0, 0x1

    or-int/2addr v0, v4

    .line 343
    iget-object v4, p0, Lcom/glympse/android/lib/s;->hN:Lcom/glympse/android/hal/GBatteryProvider;

    invoke-interface {v4}, Lcom/glympse/android/hal/GBatteryProvider;->isPlugged()Z

    move-result v4

    if-eqz v4, :cond_3

    :goto_2
    shl-int/lit8 v1, v1, 0x3

    or-int/2addr v0, v1

    .line 344
    iget-object v1, p0, Lcom/glympse/android/lib/s;->hN:Lcom/glympse/android/hal/GBatteryProvider;

    invoke-interface {v1}, Lcom/glympse/android/hal/GBatteryProvider;->getLevel()I

    move-result v1

    shl-int/lit8 v1, v1, 0x4

    or-int/2addr v0, v1

    .line 346
    new-instance v1, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v1, v3}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 347
    const-string v2, "state"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    int-to-long v4, v0

    invoke-interface {v1, v2, v4, v5}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 349
    iget-object v0, p0, Lcom/glympse/android/lib/s;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getDiagnosticsManager()Lcom/glympse/android/lib/GDiagnosticsManager;

    move-result-object v0

    const-string v2, "battery"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, p1, v2, v1}, Lcom/glympse/android/lib/GDiagnosticsManager;->logEvent(Lcom/glympse/android/api/GTicket;Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 350
    return-void

    :cond_0
    move v0, v2

    .line 341
    goto :goto_0

    .line 342
    :cond_1
    iget-boolean v0, p0, Lcom/glympse/android/lib/s;->hP:Z

    if-eqz v0, :cond_2

    move v0, v1

    goto :goto_1

    :cond_2
    move v0, v3

    goto :goto_1

    :cond_3
    move v1, v2

    .line 343
    goto :goto_2
.end method

.method public memoryWarningReceived()V
    .locals 3

    .prologue
    .line 265
    iget-object v0, p0, Lcom/glympse/android/lib/s;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-nez v0, :cond_0

    .line 275
    :goto_0
    return-void

    .line 270
    :cond_0
    new-instance v1, Lcom/glympse/android/lib/Primitive;

    const/4 v0, 0x2

    invoke-direct {v1, v0}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 271
    const-string v0, "state"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "low"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    const-string v0, "platform"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/glympse/android/lib/s;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->isActive()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "active"

    :goto_1
    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    iget-object v0, p0, Lcom/glympse/android/lib/s;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getDiagnosticsManager()Lcom/glympse/android/lib/GDiagnosticsManager;

    move-result-object v0

    const-string v2, "memory"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v1}, Lcom/glympse/android/lib/GDiagnosticsManager;->logEvent(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    goto :goto_0

    .line 272
    :cond_1
    const-string v0, "inactive"

    goto :goto_1
.end method

.method public removeListener(Lcom/glympse/android/api/GEventListener;)Z
    .locals 1

    .prologue
    .line 363
    iget-object v0, p0, Lcom/glympse/android/lib/s;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1}, Lcom/glympse/android/lib/CommonSink;->removeListener(Lcom/glympse/android/api/GEventListener;)Z

    move-result v0

    return v0
.end method

.method public setBatteryForce()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 103
    iget-object v0, p0, Lcom/glympse/android/lib/s;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/glympse/android/lib/s;->hO:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/glympse/android/lib/s;->hP:Z

    if-eqz v0, :cond_1

    .line 121
    :cond_0
    :goto_0
    return-void

    .line 109
    :cond_1
    iput-boolean v2, p0, Lcom/glympse/android/lib/s;->hP:Z

    .line 112
    iget-object v0, p0, Lcom/glympse/android/lib/s;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->okToPost()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 114
    iget-object v0, p0, Lcom/glympse/android/lib/s;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getServerPost()Lcom/glympse/android/lib/GServerPost;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/lib/GServerPost;->doPost()V

    .line 118
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/s;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/glympse/android/lib/s;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 120
    invoke-virtual {p0, v3}, Lcom/glympse/android/lib/s;->logBatteryEvent(Lcom/glympse/android/api/GTicket;)V

    goto :goto_0
.end method

.method public setBatteryLevels(II)Z
    .locals 3

    .prologue
    const/16 v0, 0x64

    .line 66
    if-ltz p1, :cond_0

    if-gt p1, v0, :cond_0

    if-ltz p2, :cond_0

    if-gt p2, v0, :cond_0

    if-le p1, p2, :cond_1

    .line 69
    :cond_0
    const/4 v0, 0x0

    .line 82
    :goto_0
    return v0

    .line 73
    :cond_1
    iput p1, p0, Lcom/glympse/android/lib/s;->hK:I

    .line 74
    iput p2, p0, Lcom/glympse/android/lib/s;->hL:I

    .line 77
    iget-object v0, p0, Lcom/glympse/android/lib/s;->hN:Lcom/glympse/android/hal/GBatteryProvider;

    if-eqz v0, :cond_2

    .line 79
    iget-object v0, p0, Lcom/glympse/android/lib/s;->hN:Lcom/glympse/android/hal/GBatteryProvider;

    invoke-interface {v0}, Lcom/glympse/android/hal/GBatteryProvider;->getLevel()I

    move-result v0

    iget-object v1, p0, Lcom/glympse/android/lib/s;->hN:Lcom/glympse/android/hal/GBatteryProvider;

    invoke-interface {v1}, Lcom/glympse/android/hal/GBatteryProvider;->isPlugged()Z

    move-result v1

    iget-object v2, p0, Lcom/glympse/android/lib/s;->hN:Lcom/glympse/android/hal/GBatteryProvider;

    invoke-interface {v2}, Lcom/glympse/android/hal/GBatteryProvider;->isPresent()Z

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/glympse/android/lib/s;->updateStatus(IZZ)V

    .line 82
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setKeepAwake()V
    .locals 2

    .prologue
    .line 147
    invoke-direct {p0}, Lcom/glympse/android/lib/s;->aE()Z

    move-result v0

    .line 150
    if-eqz v0, :cond_1

    .line 152
    iget-boolean v1, p0, Lcom/glympse/android/lib/s;->hQ:Z

    if-eqz v1, :cond_0

    .line 154
    iget-object v1, p0, Lcom/glympse/android/lib/s;->hN:Lcom/glympse/android/hal/GBatteryProvider;

    invoke-interface {v1}, Lcom/glympse/android/hal/GBatteryProvider;->acquireWakeLock()V

    .line 156
    :cond_0
    invoke-direct {p0}, Lcom/glympse/android/lib/s;->aF()V

    .line 160
    :cond_1
    if-nez v0, :cond_2

    .line 162
    iget-object v0, p0, Lcom/glympse/android/lib/s;->hN:Lcom/glympse/android/hal/GBatteryProvider;

    invoke-interface {v0}, Lcom/glympse/android/hal/GBatteryProvider;->releaseWakeLock()V

    .line 163
    invoke-direct {p0}, Lcom/glympse/android/lib/s;->aG()V

    .line 165
    :cond_2
    return-void
.end method

.method public start(Lcom/glympse/android/lib/GGlympsePrivate;)V
    .locals 3

    .prologue
    .line 283
    iput-object p1, p0, Lcom/glympse/android/lib/s;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 284
    iget-object v0, p0, Lcom/glympse/android/lib/s;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getContextHolder()Lcom/glympse/android/hal/GContextHolder;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/hal/GContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/glympse/android/hal/HalFactory;->createBatteryProvider(Landroid/content/Context;)Lcom/glympse/android/hal/GBatteryProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/s;->hN:Lcom/glympse/android/hal/GBatteryProvider;

    .line 285
    iget-object v1, p0, Lcom/glympse/android/lib/s;->hN:Lcom/glympse/android/hal/GBatteryProvider;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/hal/GBatteryListener;

    invoke-interface {v1, v0}, Lcom/glympse/android/hal/GBatteryProvider;->setBatteryListener(Lcom/glympse/android/hal/GBatteryListener;)V

    .line 286
    iget-object v0, p0, Lcom/glympse/android/lib/s;->hN:Lcom/glympse/android/hal/GBatteryProvider;

    invoke-interface {v0}, Lcom/glympse/android/hal/GBatteryProvider;->start()V

    .line 289
    iget-object v0, p0, Lcom/glympse/android/lib/s;->hN:Lcom/glympse/android/hal/GBatteryProvider;

    invoke-interface {v0}, Lcom/glympse/android/hal/GBatteryProvider;->getLevel()I

    move-result v0

    iget-object v1, p0, Lcom/glympse/android/lib/s;->hN:Lcom/glympse/android/hal/GBatteryProvider;

    invoke-interface {v1}, Lcom/glympse/android/hal/GBatteryProvider;->isPlugged()Z

    move-result v1

    iget-object v2, p0, Lcom/glympse/android/lib/s;->hN:Lcom/glympse/android/hal/GBatteryProvider;

    invoke-interface {v2}, Lcom/glympse/android/hal/GBatteryProvider;->isPresent()Z

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/glympse/android/lib/s;->updateStatus(IZZ)V

    .line 290
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 295
    invoke-direct {p0}, Lcom/glympse/android/lib/s;->aG()V

    .line 298
    iget-object v0, p0, Lcom/glympse/android/lib/s;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0}, Lcom/glympse/android/lib/CommonSink;->removeAllListeners()Z

    .line 300
    iget-object v0, p0, Lcom/glympse/android/lib/s;->hN:Lcom/glympse/android/hal/GBatteryProvider;

    invoke-interface {v0}, Lcom/glympse/android/hal/GBatteryProvider;->stop()V

    .line 301
    iget-object v0, p0, Lcom/glympse/android/lib/s;->hN:Lcom/glympse/android/hal/GBatteryProvider;

    invoke-interface {v0, v1}, Lcom/glympse/android/hal/GBatteryProvider;->setBatteryListener(Lcom/glympse/android/hal/GBatteryListener;)V

    .line 302
    iput-object v1, p0, Lcom/glympse/android/lib/s;->hN:Lcom/glympse/android/hal/GBatteryProvider;

    .line 303
    iput-object v1, p0, Lcom/glympse/android/lib/s;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 304
    return-void
.end method

.method public updateStatus()V
    .locals 3

    .prologue
    .line 308
    iget-object v0, p0, Lcom/glympse/android/lib/s;->hN:Lcom/glympse/android/hal/GBatteryProvider;

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/glympse/android/lib/s;->hN:Lcom/glympse/android/hal/GBatteryProvider;

    invoke-interface {v0}, Lcom/glympse/android/hal/GBatteryProvider;->getLevel()I

    move-result v0

    iget-object v1, p0, Lcom/glympse/android/lib/s;->hN:Lcom/glympse/android/hal/GBatteryProvider;

    invoke-interface {v1}, Lcom/glympse/android/hal/GBatteryProvider;->isPlugged()Z

    move-result v1

    iget-object v2, p0, Lcom/glympse/android/lib/s;->hN:Lcom/glympse/android/hal/GBatteryProvider;

    invoke-interface {v2}, Lcom/glympse/android/hal/GBatteryProvider;->isPresent()Z

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/glympse/android/lib/s;->updateStatus(IZZ)V

    .line 312
    :cond_0
    return-void
.end method

.method public updateStatus(IZZ)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 185
    iget-object v0, p0, Lcom/glympse/android/lib/s;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_0

    if-gez p1, :cond_1

    .line 261
    :cond_0
    :goto_0
    return-void

    .line 191
    :cond_1
    iget v0, p0, Lcom/glympse/android/lib/s;->hK:I

    if-gt p1, v0, :cond_6

    move v0, v1

    .line 192
    :goto_1
    iget v3, p0, Lcom/glympse/android/lib/s;->hL:I

    if-lt p1, v3, :cond_7

    move v3, v1

    .line 195
    :goto_2
    iget-boolean v4, p0, Lcom/glympse/android/lib/s;->hO:Z

    .line 200
    if-nez v3, :cond_2

    if-nez p2, :cond_2

    if-eqz p3, :cond_2

    iget v3, p0, Lcom/glympse/android/lib/s;->hK:I

    if-nez v3, :cond_b

    :cond_2
    move v3, v1

    .line 206
    :goto_3
    if-eqz v0, :cond_a

    if-nez p2, :cond_a

    if-eqz p3, :cond_a

    iget v0, p0, Lcom/glympse/android/lib/s;->hK:I

    if-lez v0, :cond_a

    move v0, v2

    .line 212
    :goto_4
    iget-boolean v3, p0, Lcom/glympse/android/lib/s;->hO:Z

    if-eq v0, v3, :cond_8

    .line 215
    iput-boolean v0, p0, Lcom/glympse/android/lib/s;->hO:Z

    .line 220
    iget-boolean v0, p0, Lcom/glympse/android/lib/s;->hO:Z

    if-eqz v0, :cond_3

    .line 222
    iput-boolean v2, p0, Lcom/glympse/android/lib/s;->hP:Z

    .line 225
    iget-object v0, p0, Lcom/glympse/android/lib/s;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getServerPost()Lcom/glympse/android/lib/GServerPost;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/lib/GServerPost;->doPost()V

    .line 229
    :cond_3
    iget-object v0, p0, Lcom/glympse/android/lib/s;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->startStopLocation()V

    .line 232
    const/4 v0, 0x3

    const-string v3, "[BatteryManager.updateStatus] Changed"

    invoke-static {v0, v3}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 235
    iget-object v0, p0, Lcom/glympse/android/lib/s;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/4 v3, 0x6

    invoke-virtual {p0, v0, v3, v1, v5}, Lcom/glympse/android/lib/s;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 237
    invoke-virtual {p0, v5}, Lcom/glympse/android/lib/s;->logBatteryEvent(Lcom/glympse/android/api/GTicket;)V

    .line 249
    :cond_4
    :goto_5
    iget-boolean v0, p0, Lcom/glympse/android/lib/s;->I:Z

    if-eq v0, p2, :cond_5

    .line 252
    iget-object v0, p0, Lcom/glympse/android/lib/s;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->startStopLocation()V

    .line 254
    :cond_5
    iput-boolean p2, p0, Lcom/glympse/android/lib/s;->I:Z

    .line 257
    iget-object v0, p0, Lcom/glympse/android/lib/s;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getConfigPrivate()Lcom/glympse/android/lib/GConfigPrivate;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/lib/GConfigPrivate;->shouldForceRefresh()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/glympse/android/lib/s;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getJobQueue()Lcom/glympse/android/lib/GJobQueue;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/glympse/android/lib/GJobQueue;->retryAll(Z)V

    goto :goto_0

    :cond_6
    move v0, v2

    .line 191
    goto :goto_1

    :cond_7
    move v3, v2

    .line 192
    goto :goto_2

    .line 241
    :cond_8
    iget v0, p0, Lcom/glympse/android/lib/s;->hM:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_9

    iget v0, p0, Lcom/glympse/android/lib/s;->hM:I

    sub-int v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_4

    .line 243
    :cond_9
    iput p1, p0, Lcom/glympse/android/lib/s;->hM:I

    .line 245
    invoke-virtual {p0, v5}, Lcom/glympse/android/lib/s;->logBatteryEvent(Lcom/glympse/android/api/GTicket;)V

    goto :goto_5

    :cond_a
    move v0, v3

    goto :goto_4

    :cond_b
    move v3, v4

    goto :goto_3
.end method
