.class Lcom/glympse/android/lib/do;
.super Ljava/lang/Object;
.source "LocationManager.java"

# interfaces
.implements Lcom/glympse/android/lib/GLocationManagerPrivate;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/lib/do$a;,
        Lcom/glympse/android/lib/do$b;
    }
.end annotation


# static fields
.field private static final qt:J = 0x1b7740L

.field private static final qu:I = 0xc8

.field private static final qv:I = 0x2

.field private static final qw:I = 0x64


# instance fields
.field private F:Z

.field private _glympse:Lcom/glympse/android/lib/GGlympsePrivate;

.field private cC:Lcom/glympse/android/core/GLocationProvider;

.field private cg:I

.field private cj:Lcom/glympse/android/core/GLocationProfile;

.field private he:Lcom/glympse/android/lib/CommonSink;

.field private hl:Lcom/glympse/android/core/GProximityProvider;

.field private hp:Lcom/glympse/android/core/GLocation;

.field private lN:Lcom/glympse/android/lib/GBatteryManagerPrivate;

.field private lX:Lcom/glympse/android/lib/GCorrectedTime;

.field private qn:I

.field private qo:Z

.field private qp:Z

.field private qq:Lcom/glympse/android/lib/do$a;

.field private qr:Lcom/glympse/android/hal/GVector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GVector",
            "<",
            "Lcom/glympse/android/core/GLocationProfile;",
            ">;"
        }
    .end annotation
.end field

.field private qs:I

.field private qx:D


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    iput v0, p0, Lcom/glympse/android/lib/do;->qn:I

    .line 115
    iput-boolean v1, p0, Lcom/glympse/android/lib/do;->qo:Z

    .line 116
    iput-boolean v0, p0, Lcom/glympse/android/lib/do;->qp:Z

    .line 117
    iput v1, p0, Lcom/glympse/android/lib/do;->cg:I

    .line 118
    iput-boolean v0, p0, Lcom/glympse/android/lib/do;->F:Z

    .line 119
    new-instance v0, Lcom/glympse/android/lib/CommonSink;

    const-string v1, "LocationManager"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/CommonSink;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/glympse/android/lib/do;->he:Lcom/glympse/android/lib/CommonSink;

    .line 120
    invoke-direct {p0}, Lcom/glympse/android/lib/do;->cu()V

    .line 121
    invoke-direct {p0}, Lcom/glympse/android/lib/do;->cx()V

    .line 122
    return-void
.end method

.method private a(Lcom/glympse/android/lib/GLocationPrivate;)Z
    .locals 13

    .prologue
    const/4 v1, 0x1

    const/4 v12, 0x3

    const/4 v2, 0x0

    .line 975
    iget-object v0, p0, Lcom/glympse/android/lib/do;->lX:Lcom/glympse/android/lib/GCorrectedTime;

    invoke-interface {v0}, Lcom/glympse/android/lib/GCorrectedTime;->getTime()J

    move-result-wide v4

    .line 978
    iget-boolean v0, p0, Lcom/glympse/android/lib/do;->qp:Z

    if-eqz v0, :cond_2

    .line 983
    iget-object v0, p0, Lcom/glympse/android/lib/do;->hp:Lcom/glympse/android/core/GLocation;

    if-eqz v0, :cond_3

    .line 987
    invoke-interface {p1}, Lcom/glympse/android/lib/GLocationPrivate;->getHAccuracy()F

    move-result v0

    float-to-int v0, v0

    int-to-double v6, v0

    .line 988
    iget-object v0, p0, Lcom/glympse/android/lib/do;->hp:Lcom/glympse/android/core/GLocation;

    invoke-interface {p1, v0}, Lcom/glympse/android/lib/GLocationPrivate;->distanceTo(Lcom/glympse/android/core/GLatLng;)F

    move-result v0

    float-to-double v8, v0

    .line 989
    iget-object v0, p0, Lcom/glympse/android/lib/do;->hp:Lcom/glympse/android/core/GLocation;

    invoke-interface {v0}, Lcom/glympse/android/core/GLocation;->getHAccuracy()F

    move-result v0

    float-to-int v0, v0

    int-to-double v10, v0

    .line 990
    add-double/2addr v10, v6

    cmpg-double v0, v8, v10

    if-gez v0, :cond_0

    move v0, v1

    .line 991
    :goto_0
    iget-wide v10, p0, Lcom/glympse/android/lib/do;->qx:D

    cmpl-double v3, v6, v10

    if-lez v3, :cond_1

    if-eqz v0, :cond_1

    .line 993
    const-string v0, "[LocationManager] Bad location (hacc)"

    invoke-static {v12, v0}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 1035
    :goto_1
    return v2

    :cond_0
    move v0, v2

    .line 990
    goto :goto_0

    .line 999
    :cond_1
    const-wide v6, 0x408f400000000000L    # 1000.0

    mul-double/2addr v6, v8

    iget-object v0, p0, Lcom/glympse/android/lib/do;->hp:Lcom/glympse/android/core/GLocation;

    invoke-interface {v0}, Lcom/glympse/android/core/GLocation;->getTime()J

    move-result-wide v8

    sub-long v8, v4, v8

    long-to-double v8, v8

    div-double/2addr v6, v8

    .line 1000
    const-wide v8, 0x40765a1ca0000000L    # 357.6319885253906

    cmpl-double v0, v6, v8

    if-lez v0, :cond_3

    .line 1002
    const-string v0, "[LocationManager] Bad location (cspeed)"

    invoke-static {v12, v0}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    goto :goto_1

    .line 1012
    :cond_2
    invoke-interface {p1}, Lcom/glympse/android/lib/GLocationPrivate;->getTime()J

    move-result-wide v6

    sub-long v6, v4, v6

    const-wide/32 v8, 0x1b7740

    cmp-long v0, v6, v8

    if-lez v0, :cond_3

    .line 1014
    const-string v0, "[LocationManager] Old location"

    invoke-static {v12, v0}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    goto :goto_1

    .line 1025
    :cond_3
    invoke-interface {p1, v4, v5}, Lcom/glympse/android/lib/GLocationPrivate;->setTime(J)V

    .line 1028
    invoke-interface {p1}, Lcom/glympse/android/lib/GLocationPrivate;->getSpeed()F

    move-result v0

    .line 1029
    const/4 v2, 0x0

    cmpg-float v2, v0, v2

    if-ltz v2, :cond_4

    const v2, 0x43b2d0e5

    cmpl-float v0, v0, v2

    if-lez v0, :cond_5

    .line 1031
    :cond_4
    const/high16 v0, 0x7fc00000    # NaNf

    invoke-interface {p1, v0}, Lcom/glympse/android/lib/GLocationPrivate;->setSpeed(F)V

    .line 1032
    const-string v0, "[LocationManager] Invalid speed"

    invoke-static {v12, v0}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    :cond_5
    move v2, v1

    .line 1035
    goto :goto_1
.end method

.method private b(Lcom/glympse/android/core/GLocation;)V
    .locals 2

    .prologue
    .line 830
    invoke-static {p1}, Lcom/glympse/android/lib/Debug;->dumpLocation(Lcom/glympse/android/core/GLocation;)V

    .line 833
    iput-object p1, p0, Lcom/glympse/android/lib/do;->hp:Lcom/glympse/android/core/GLocation;

    .line 836
    iget-object v0, p0, Lcom/glympse/android/lib/do;->hl:Lcom/glympse/android/core/GProximityProvider;

    iget-object v1, p0, Lcom/glympse/android/lib/do;->hp:Lcom/glympse/android/core/GLocation;

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GProximityProvider;->locationChanged(Lcom/glympse/android/core/GLocation;)V

    .line 839
    iget-object v0, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0, p1}, Lcom/glympse/android/lib/GGlympsePrivate;->addLocation(Lcom/glympse/android/core/GLocation;)V

    .line 840
    return-void
.end method

.method private b(Lcom/glympse/android/core/GLocationProfile;)V
    .locals 5

    .prologue
    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    .line 925
    .line 926
    if-eqz p1, :cond_2

    .line 928
    invoke-interface {p1}, Lcom/glympse/android/core/GLocationProfile;->getAccuracy()D

    move-result-wide v0

    .line 929
    cmpl-double v4, v0, v2

    if-lez v4, :cond_1

    .line 944
    :cond_0
    :goto_0
    cmpg-double v2, v0, v2

    if-gtz v2, :cond_3

    const-wide/high16 v0, 0x4069000000000000L    # 200.0

    :goto_1
    iput-wide v0, p0, Lcom/glympse/android/lib/do;->qx:D

    .line 947
    return-void

    .line 935
    :cond_1
    invoke-interface {p1}, Lcom/glympse/android/core/GLocationProfile;->getDistance()D

    move-result-wide v0

    .line 936
    cmpl-double v4, v0, v2

    if-gtz v4, :cond_0

    :cond_2
    move-wide v0, v2

    goto :goto_0

    .line 944
    :cond_3
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    mul-double/2addr v0, v2

    goto :goto_1
.end method

.method private c(Lcom/glympse/android/core/GLocation;)V
    .locals 1

    .prologue
    .line 845
    iget-object v0, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0, p1}, Lcom/glympse/android/lib/GGlympsePrivate;->declineLocation(Lcom/glympse/android/core/GLocation;)V

    .line 846
    return-void
.end method

.method private ct()V
    .locals 2

    .prologue
    .line 437
    iget-boolean v0, p0, Lcom/glympse/android/lib/do;->F:Z

    if-eqz v0, :cond_0

    .line 450
    :goto_0
    return-void

    .line 441
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/glympse/android/lib/do;->F:Z

    .line 443
    const/4 v0, 0x3

    const-string v1, "[LocationManager] Started"

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 446
    iget-object v0, p0, Lcom/glympse/android/lib/do;->cC:Lcom/glympse/android/core/GLocationProvider;

    invoke-interface {v0}, Lcom/glympse/android/core/GLocationProvider;->start()V

    .line 449
    iget-object v0, p0, Lcom/glympse/android/lib/do;->hl:Lcom/glympse/android/core/GProximityProvider;

    iget-object v1, p0, Lcom/glympse/android/lib/do;->hp:Lcom/glympse/android/core/GLocation;

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GProximityProvider;->locationChanged(Lcom/glympse/android/core/GLocation;)V

    goto :goto_0
.end method

.method private cu()V
    .locals 1

    .prologue
    .line 504
    new-instance v0, Lcom/glympse/android/hal/GVector;

    invoke-direct {v0}, Lcom/glympse/android/hal/GVector;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/do;->qr:Lcom/glympse/android/hal/GVector;

    .line 505
    const/4 v0, -0x1

    iput v0, p0, Lcom/glympse/android/lib/do;->qs:I

    .line 506
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/do;->cj:Lcom/glympse/android/core/GLocationProfile;

    .line 507
    return-void
.end method

.method private cv()I
    .locals 4

    .prologue
    const/4 v1, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x3

    .line 652
    iget-object v0, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getHistoryManager()Lcom/glympse/android/api/GHistoryManager;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GHistoryManagerPrivate;

    .line 653
    invoke-interface {v0, v3}, Lcom/glympse/android/lib/GHistoryManagerPrivate;->anyActive(Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 656
    iget-object v0, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getServerPost()Lcom/glympse/android/lib/GServerPost;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/lib/GServerPost;->isServerPostRateHigh()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 662
    iget-object v0, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getConfigPrivate()Lcom/glympse/android/lib/GConfigPrivate;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/lib/GConfigPrivate;->isAccuracyStationaryLow()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 664
    iget-object v0, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getDirectionsManager()Lcom/glympse/android/api/GDirectionsManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/api/GDirectionsManager;->isDeviceStationary()Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    .line 705
    :goto_0
    return v0

    .line 680
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getConfigPrivate()Lcom/glympse/android/lib/GConfigPrivate;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/lib/GConfigPrivate;->isAccuracyPluggedHigh()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 682
    iget-object v0, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getBatteryManagerPrivate()Lcom/glympse/android/lib/GBatteryManagerPrivate;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/lib/GBatteryManagerPrivate;->getProvider()Lcom/glympse/android/hal/GBatteryProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/hal/GBatteryProvider;->isPlugged()Z

    move-result v0

    if-eqz v0, :cond_1

    move v1, v2

    .line 688
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->isActive()Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/glympse/android/lib/do;->qn:I

    if-lez v0, :cond_2

    move v1, v2

    :cond_2
    move v0, v1

    .line 693
    goto :goto_0

    .line 699
    :cond_3
    iget-object v0, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->isActive()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 701
    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v3

    .line 705
    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_0
.end method

.method private cw()V
    .locals 2

    .prologue
    .line 713
    iget-object v0, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->isSharing()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/do;->j(Z)V

    .line 716
    iget-object v0, p0, Lcom/glympse/android/lib/do;->cC:Lcom/glympse/android/core/GLocationProvider;

    iget-object v1, p0, Lcom/glympse/android/lib/do;->cj:Lcom/glympse/android/core/GLocationProfile;

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GLocationProvider;->applyProfile(Lcom/glympse/android/core/GLocationProfile;)V

    .line 719
    iget-object v0, p0, Lcom/glympse/android/lib/do;->cj:Lcom/glympse/android/core/GLocationProfile;

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/do;->b(Lcom/glympse/android/core/GLocationProfile;)V

    .line 720
    return-void
.end method

.method private cx()V
    .locals 2

    .prologue
    .line 916
    const-wide/high16 v0, 0x4069000000000000L    # 200.0

    iput-wide v0, p0, Lcom/glympse/android/lib/do;->qx:D

    .line 917
    return-void
.end method

.method private d(Lcom/glympse/android/core/GLocation;)Z
    .locals 1

    .prologue
    .line 955
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/glympse/android/core/GLocation;->hasLocation()Z

    move-result v0

    if-nez v0, :cond_1

    .line 957
    :cond_0
    const/4 v0, 0x0

    .line 967
    :goto_0
    return v0

    .line 961
    :cond_1
    iget-boolean v0, p0, Lcom/glympse/android/lib/do;->qo:Z

    if-nez v0, :cond_2

    .line 964
    const/4 v0, 0x1

    goto :goto_0

    .line 967
    :cond_2
    check-cast p1, Lcom/glympse/android/lib/GLocationPrivate;

    invoke-direct {p0, p1}, Lcom/glympse/android/lib/do;->a(Lcom/glympse/android/lib/GLocationPrivate;)Z

    move-result v0

    goto :goto_0
.end method

.method private j(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 410
    .line 411
    iget-object v0, p0, Lcom/glympse/android/lib/do;->cj:Lcom/glympse/android/core/GLocationProfile;

    if-eqz v0, :cond_0

    .line 413
    iget-object v0, p0, Lcom/glympse/android/lib/do;->cj:Lcom/glympse/android/core/GLocationProfile;

    invoke-interface {v0}, Lcom/glympse/android/core/GLocationProfile;->getMode()I

    move-result v0

    .line 421
    :goto_0
    invoke-virtual {p0, v0, p1}, Lcom/glympse/android/lib/do;->a(IZ)Z

    move-result v0

    .line 424
    if-eqz v0, :cond_2

    .line 426
    invoke-direct {p0}, Lcom/glympse/android/lib/do;->ct()V

    .line 433
    :goto_1
    return-void

    .line 417
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->isActive()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 431
    :cond_2
    invoke-direct {p0, v1}, Lcom/glympse/android/lib/do;->k(Z)V

    goto :goto_1
.end method

.method private k(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 458
    iget-boolean v0, p0, Lcom/glympse/android/lib/do;->F:Z

    if-nez v0, :cond_1

    .line 478
    :cond_0
    :goto_0
    return-void

    .line 462
    :cond_1
    iput-boolean v2, p0, Lcom/glympse/android/lib/do;->F:Z

    .line 464
    const/4 v0, 0x3

    const-string v1, "[LocationManager] Stopped"

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 467
    iget-object v0, p0, Lcom/glympse/android/lib/do;->cC:Lcom/glympse/android/core/GLocationProvider;

    invoke-interface {v0}, Lcom/glympse/android/core/GLocationProvider;->stop()V

    .line 471
    iput-object v3, p0, Lcom/glympse/android/lib/do;->hp:Lcom/glympse/android/core/GLocation;

    .line 472
    iput-boolean v2, p0, Lcom/glympse/android/lib/do;->qp:Z

    .line 474
    if-eqz p1, :cond_0

    .line 476
    iget-object v0, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/16 v1, 0x8

    const/16 v2, 0x200

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/glympse/android/lib/do;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method protected a(Lcom/glympse/android/core/GLocation;Z)V
    .locals 3

    .prologue
    .line 797
    iget-object v0, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-nez v0, :cond_0

    .line 825
    :goto_0
    return-void

    .line 803
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getHandler()Lcom/glympse/android/core/GHandler;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/core/GHandler;->isMainThread()Z

    move-result v0

    if-nez v0, :cond_1

    .line 805
    const/4 v0, 0x3

    const-string v1, "[LocationManager] Worker thread location"

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 807
    iget-object v0, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getHandler()Lcom/glympse/android/core/GHandler;

    move-result-object v1

    new-instance v2, Lcom/glympse/android/lib/do$b;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/do;

    invoke-direct {v2, v0, p1, p2}, Lcom/glympse/android/lib/do$b;-><init>(Lcom/glympse/android/lib/do;Lcom/glympse/android/core/GLocation;Z)V

    invoke-interface {v1, v2}, Lcom/glympse/android/core/GHandler;->post(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 814
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getJobQueue()Lcom/glympse/android/lib/GJobQueue;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GJobQueue;->retryAll(Z)V

    .line 817
    if-eqz p2, :cond_2

    .line 819
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/do;->b(Lcom/glympse/android/core/GLocation;)V

    goto :goto_0

    .line 823
    :cond_2
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/do;->c(Lcom/glympse/android/core/GLocation;)V

    goto :goto_0
.end method

.method public a(IZ)Z
    .locals 1

    .prologue
    .line 404
    iget-object v0, p0, Lcom/glympse/android/lib/do;->lN:Lcom/glympse/android/lib/GBatteryManagerPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GBatteryManagerPrivate;->isBatteryOk()Z

    move-result v0

    if-eqz v0, :cond_2

    if-nez p1, :cond_0

    iget v0, p0, Lcom/glympse/android/lib/do;->qn:I

    if-gtz v0, :cond_1

    :cond_0
    if-eqz p2, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public addListener(Lcom/glympse/android/api/GEventListener;)Z
    .locals 1

    .prologue
    .line 1044
    iget-object v0, p0, Lcom/glympse/android/lib/do;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1}, Lcom/glympse/android/lib/CommonSink;->addListener(Lcom/glympse/android/api/GEventListener;)Z

    move-result v0

    return v0
.end method

.method public areProfilesEnabled()Z
    .locals 1

    .prologue
    .line 547
    iget-object v0, p0, Lcom/glympse/android/lib/do;->qr:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public associateContext(JLjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1069
    iget-object v0, p0, Lcom/glympse/android/lib/do;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1, p2, p3}, Lcom/glympse/android/lib/CommonSink;->associateContext(JLjava/lang/Object;)V

    .line 1070
    return-void
.end method

.method public clearContext(J)V
    .locals 1

    .prologue
    .line 1079
    iget-object v0, p0, Lcom/glympse/android/lib/do;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1, p2}, Lcom/glympse/android/lib/CommonSink;->clearContext(J)V

    .line 1080
    return-void
.end method

.method public deriveContext(Lcom/glympse/android/api/GEventSink;)V
    .locals 1

    .prologue
    .line 1089
    iget-object v0, p0, Lcom/glympse/android/lib/do;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1}, Lcom/glympse/android/lib/CommonSink;->deriveContext(Lcom/glympse/android/api/GEventSink;)V

    .line 1090
    return-void
.end method

.method public enableFiltering(Z)V
    .locals 0

    .prologue
    .line 263
    iput-boolean p1, p0, Lcom/glympse/android/lib/do;->qo:Z

    .line 264
    return-void
.end method

.method public enableProfiles(Z)V
    .locals 3

    .prologue
    .line 511
    if-eqz p1, :cond_3

    .line 513
    iget-object v0, p0, Lcom/glympse/android/lib/do;->qr:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v0

    if-eqz v0, :cond_1

    .line 543
    :cond_0
    :goto_0
    return-void

    .line 521
    :cond_1
    const/4 v0, 0x0

    :goto_1
    const/4 v1, 0x4

    if-ge v0, v1, :cond_2

    .line 523
    iget-object v1, p0, Lcom/glympse/android/lib/do;->qr:Lcom/glympse/android/hal/GVector;

    invoke-static {v0}, Lcom/glympse/android/hal/HalFactory;->createLocationProfile(I)Lcom/glympse/android/lib/GLocationProfilePrivate;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 521
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 527
    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/do;->updateProfile(Z)V

    goto :goto_0

    .line 531
    :cond_3
    iget-object v0, p0, Lcom/glympse/android/lib/do;->qr:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 538
    invoke-direct {p0}, Lcom/glympse/android/lib/do;->cu()V

    .line 541
    invoke-direct {p0}, Lcom/glympse/android/lib/do;->cw()V

    goto :goto_0
.end method

.method public eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V
    .locals 6

    .prologue
    .line 1059
    iget-object v0, p0, Lcom/glympse/android/lib/do;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/glympse/android/api/GEventSink;

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/glympse/android/lib/CommonSink;->eventsOccurred(Lcom/glympse/android/api/GEventSink;Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 1060
    return-void
.end method

.method public getContext(J)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1074
    iget-object v0, p0, Lcom/glympse/android/lib/do;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1, p2}, Lcom/glympse/android/lib/CommonSink;->getContext(J)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getContextKeys()Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Enumeration",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1084
    iget-object v0, p0, Lcom/glympse/android/lib/do;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0}, Lcom/glympse/android/lib/CommonSink;->getContextKeys()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentProfile()Lcom/glympse/android/core/GLocationProfile;
    .locals 1

    .prologue
    .line 586
    iget-object v0, p0, Lcom/glympse/android/lib/do;->cj:Lcom/glympse/android/core/GLocationProfile;

    return-object v0
.end method

.method public getListeners()Lcom/glympse/android/core/GArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/glympse/android/core/GArray",
            "<",
            "Lcom/glympse/android/api/GEventListener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1054
    iget-object v0, p0, Lcom/glympse/android/lib/do;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0}, Lcom/glympse/android/lib/CommonSink;->getListeners()Lcom/glympse/android/core/GArray;

    move-result-object v0

    return-object v0
.end method

.method public getLocation()Lcom/glympse/android/core/GLocation;
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/glympse/android/lib/do;->hp:Lcom/glympse/android/core/GLocation;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-nez v0, :cond_2

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/do;->hp:Lcom/glympse/android/core/GLocation;

    .line 140
    :cond_1
    :goto_0
    return-object v0

    .line 137
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/do;->cC:Lcom/glympse/android/core/GLocationProvider;

    invoke-interface {v0}, Lcom/glympse/android/core/GLocationProvider;->getLastKnownLocation()Lcom/glympse/android/core/GLocation;

    move-result-object v0

    .line 140
    invoke-direct {p0, v0}, Lcom/glympse/android/lib/do;->d(Lcom/glympse/android/core/GLocation;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLocationProvider()Lcom/glympse/android/core/GLocationProvider;
    .locals 1

    .prologue
    .line 390
    iget-object v0, p0, Lcom/glympse/android/lib/do;->cC:Lcom/glympse/android/core/GLocationProvider;

    return-object v0
.end method

.method public getLocationState()I
    .locals 1

    .prologue
    .line 258
    iget v0, p0, Lcom/glympse/android/lib/do;->cg:I

    return v0
.end method

.method public getProfile(I)Lcom/glympse/android/core/GLocationProfile;
    .locals 1

    .prologue
    .line 592
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/do;->qr:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 594
    :cond_0
    const/4 v0, 0x0

    .line 598
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/do;->qr:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GLocationProfile;

    goto :goto_0
.end method

.method public getProximityProvider()Lcom/glympse/android/core/GProximityProvider;
    .locals 1

    .prologue
    .line 395
    iget-object v0, p0, Lcom/glympse/android/lib/do;->hl:Lcom/glympse/android/core/GProximityProvider;

    return-object v0
.end method

.method public handleGeofence(Ljava/lang/String;I)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    .line 274
    iget-object v2, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v2, :cond_0

    invoke-static {p1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    if-eq v7, p2, :cond_1

    const/4 v2, 0x2

    if-eq v2, p2, :cond_1

    .line 293
    :cond_0
    :goto_0
    return-void

    .line 281
    :cond_1
    iget-object v2, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v2}, Lcom/glympse/android/lib/GGlympsePrivate;->getTriggersManager()Lcom/glympse/android/api/GTriggersManager;

    move-wide v2, v0

    move-wide v4, v0

    move-object v6, p1

    .line 286
    invoke-static/range {v0 .. v6}, Lcom/glympse/android/core/CoreFactory;->createRegion(DDDLjava/lang/String;)Lcom/glympse/android/core/GRegion;

    move-result-object v1

    .line 289
    if-ne v7, p2, :cond_2

    const/16 v0, 0x10

    .line 292
    :goto_1
    iget-object v2, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/16 v3, 0x8

    invoke-virtual {p0, v2, v3, v0, v1}, Lcom/glympse/android/lib/do;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    goto :goto_0

    .line 289
    :cond_2
    const/16 v0, 0x20

    goto :goto_1
.end method

.method public hasContext(J)Z
    .locals 1

    .prologue
    .line 1064
    iget-object v0, p0, Lcom/glympse/android/lib/do;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1, p2}, Lcom/glympse/android/lib/CommonSink;->hasContext(J)Z

    move-result v0

    return v0
.end method

.method public isFilteringEnabled()Z
    .locals 1

    .prologue
    .line 268
    iget-boolean v0, p0, Lcom/glympse/android/lib/do;->qo:Z

    return v0
.end method

.method public locationChanged(Lcom/glympse/android/core/GLocation;)V
    .locals 1

    .prologue
    .line 731
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/glympse/android/lib/do;->qp:Z

    .line 734
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/do;->d(Lcom/glympse/android/core/GLocation;)Z

    move-result v0

    .line 737
    invoke-virtual {p0, p1, v0}, Lcom/glympse/android/lib/do;->a(Lcom/glympse/android/core/GLocation;Z)V

    .line 738
    return-void
.end method

.method public regionEntered(Lcom/glympse/android/core/GRegion;)V
    .locals 3

    .prologue
    .line 775
    iget-object v0, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_0

    .line 778
    iget-object v0, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/16 v1, 0x8

    const/16 v2, 0x10

    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/glympse/android/lib/do;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 780
    :cond_0
    return-void
.end method

.method public regionLeft(Lcom/glympse/android/core/GRegion;)V
    .locals 3

    .prologue
    .line 784
    iget-object v0, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_0

    .line 787
    iget-object v0, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/16 v1, 0x8

    const/16 v2, 0x20

    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/glympse/android/lib/do;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 789
    :cond_0
    return-void
.end method

.method public removeListener(Lcom/glympse/android/api/GEventListener;)Z
    .locals 1

    .prologue
    .line 1049
    iget-object v0, p0, Lcom/glympse/android/lib/do;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1}, Lcom/glympse/android/lib/CommonSink;->removeListener(Lcom/glympse/android/api/GEventListener;)Z

    move-result v0

    return v0
.end method

.method public restartProvider()V
    .locals 2

    .prologue
    .line 350
    invoke-static {}, Lcom/glympse/android/hal/Platform;->getOsName()Ljava/lang/String;

    move-result-object v0

    .line 351
    const-string v1, "ios"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->isActive()Z

    move-result v0

    if-nez v0, :cond_1

    .line 361
    :cond_0
    :goto_0
    return-void

    .line 356
    :cond_1
    iget-boolean v0, p0, Lcom/glympse/android/lib/do;->F:Z

    if-eqz v0, :cond_0

    .line 358
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/do;->k(Z)V

    .line 359
    invoke-direct {p0}, Lcom/glympse/android/lib/do;->ct()V

    goto :goto_0
.end method

.method public setActive(Z)V
    .locals 1

    .prologue
    .line 344
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/do;->updateProfile(Z)V

    .line 345
    return-void
.end method

.method public setLocationProvider(Lcom/glympse/android/core/GLocationProvider;)V
    .locals 3

    .prologue
    .line 189
    if-nez p1, :cond_1

    .line 217
    :cond_0
    :goto_0
    return-void

    .line 194
    :cond_1
    iget-boolean v1, p0, Lcom/glympse/android/lib/do;->F:Z

    .line 195
    iget-object v0, p0, Lcom/glympse/android/lib/do;->cC:Lcom/glympse/android/core/GLocationProvider;

    if-eqz v0, :cond_2

    .line 198
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/do;->k(Z)V

    .line 200
    iget-object v0, p0, Lcom/glympse/android/lib/do;->cC:Lcom/glympse/android/core/GLocationProvider;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Lcom/glympse/android/core/GLocationProvider;->setLocationListener(Lcom/glympse/android/core/GLocationListener;)V

    .line 204
    :cond_2
    iput-object p1, p0, Lcom/glympse/android/lib/do;->cC:Lcom/glympse/android/core/GLocationProvider;

    .line 207
    iget-object v0, p0, Lcom/glympse/android/lib/do;->cC:Lcom/glympse/android/core/GLocationProvider;

    invoke-interface {v0}, Lcom/glympse/android/core/GLocationProvider;->stop()V

    .line 209
    iget-object v2, p0, Lcom/glympse/android/lib/do;->cC:Lcom/glympse/android/core/GLocationProvider;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GLocationListener;

    invoke-interface {v2, v0}, Lcom/glympse/android/core/GLocationProvider;->setLocationListener(Lcom/glympse/android/core/GLocationListener;)V

    .line 211
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/do;->updateProfile(Z)V

    .line 213
    if-eqz v1, :cond_0

    .line 215
    invoke-direct {p0}, Lcom/glympse/android/lib/do;->ct()V

    goto :goto_0
.end method

.method public setProximityProvider(Lcom/glympse/android/core/GProximityProvider;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 222
    if-nez p1, :cond_1

    .line 254
    :cond_0
    :goto_0
    return-void

    .line 228
    :cond_1
    iget-object v1, p0, Lcom/glympse/android/lib/do;->hl:Lcom/glympse/android/core/GProximityProvider;

    if-eqz v1, :cond_3

    .line 231
    iget-object v1, p0, Lcom/glympse/android/lib/do;->hl:Lcom/glympse/android/core/GProximityProvider;

    invoke-interface {v1, v0}, Lcom/glympse/android/core/GProximityProvider;->setProximityListener(Lcom/glympse/android/core/GProximityListener;)V

    .line 234
    iget-object v0, p0, Lcom/glympse/android/lib/do;->hl:Lcom/glympse/android/core/GProximityProvider;

    invoke-interface {v0}, Lcom/glympse/android/core/GProximityProvider;->detachRegions()Lcom/glympse/android/core/GArray;

    move-result-object v0

    move-object v1, v0

    .line 238
    :goto_1
    iput-object p1, p0, Lcom/glympse/android/lib/do;->hl:Lcom/glympse/android/core/GProximityProvider;

    .line 241
    iget-object v2, p0, Lcom/glympse/android/lib/do;->hl:Lcom/glympse/android/core/GProximityProvider;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GProximityListener;

    invoke-interface {v2, v0}, Lcom/glympse/android/core/GProximityProvider;->setProximityListener(Lcom/glympse/android/core/GProximityListener;)V

    .line 243
    if-eqz v1, :cond_2

    .line 246
    iget-object v0, p0, Lcom/glympse/android/lib/do;->hl:Lcom/glympse/android/core/GProximityProvider;

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GProximityProvider;->startMonitoring(Lcom/glympse/android/core/GArray;)V

    .line 250
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/do;->hp:Lcom/glympse/android/core/GLocation;

    if-eqz v0, :cond_0

    .line 252
    iget-object v0, p0, Lcom/glympse/android/lib/do;->hl:Lcom/glympse/android/core/GProximityProvider;

    iget-object v1, p0, Lcom/glympse/android/lib/do;->hp:Lcom/glympse/android/core/GLocation;

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GProximityProvider;->locationChanged(Lcom/glympse/android/core/GLocation;)V

    goto :goto_0

    :cond_3
    move-object v1, v0

    goto :goto_1
.end method

.method public start(Lcom/glympse/android/lib/GGlympsePrivate;)V
    .locals 3

    .prologue
    .line 302
    iput-object p1, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 303
    iget-object v0, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getCorrectedTime()Lcom/glympse/android/lib/GCorrectedTime;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/do;->lX:Lcom/glympse/android/lib/GCorrectedTime;

    .line 304
    iget-object v0, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getBatteryManager()Lcom/glympse/android/api/GBatteryManager;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GBatteryManagerPrivate;

    iput-object v0, p0, Lcom/glympse/android/lib/do;->lN:Lcom/glympse/android/lib/GBatteryManagerPrivate;

    .line 307
    new-instance v0, Lcom/glympse/android/lib/do$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/do$a;-><init>(Lcom/glympse/android/lib/do$1;)V

    iput-object v0, p0, Lcom/glympse/android/lib/do;->qq:Lcom/glympse/android/lib/do$a;

    .line 308
    iget-object v0, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getConfig()Lcom/glympse/android/api/GConfig;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GConfigPrivate;

    .line 309
    iget-object v1, p0, Lcom/glympse/android/lib/do;->qq:Lcom/glympse/android/lib/do$a;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GConfigPrivate;->addListener(Lcom/glympse/android/api/GEventListener;)Z

    .line 312
    iget-object v1, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->getContextHolder()Lcom/glympse/android/hal/GContextHolder;

    move-result-object v1

    .line 313
    invoke-interface {v1}, Lcom/glympse/android/hal/GContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/glympse/android/hal/HalFactory;->createLocationProvider(Landroid/content/Context;)Lcom/glympse/android/core/GLocationProvider;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/glympse/android/lib/do;->setLocationProvider(Lcom/glympse/android/core/GLocationProvider;)V

    .line 316
    invoke-interface {v0}, Lcom/glympse/android/lib/GConfigPrivate;->useGlympseProximity()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/glympse/android/lib/o;

    iget-object v1, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 317
    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->getHandler()Lcom/glympse/android/core/GHandler;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/o;-><init>(Lcom/glympse/android/core/GHandler;)V

    .line 316
    :goto_0
    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/do;->setProximityProvider(Lcom/glympse/android/core/GProximityProvider;)V

    .line 319
    return-void

    .line 318
    :cond_0
    invoke-interface {v1}, Lcom/glympse/android/hal/GContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/glympse/android/hal/HalFactory;->createProximityProvider(Landroid/content/Context;)Lcom/glympse/android/core/GProximityProvider;

    move-result-object v0

    goto :goto_0
.end method

.method public startLocation()I
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 145
    iget v0, p0, Lcom/glympse/android/lib/do;->qn:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/glympse/android/lib/do;->qn:I

    .line 146
    iget v0, p0, Lcom/glympse/android/lib/do;->qn:I

    if-ne v4, v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->startStopLocation()V

    .line 154
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[LocationManager.startLocation] Counter: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/glympse/android/lib/do;->qn:I

    int-to-long v2, v1

    invoke-static {v2, v3}, Lcom/glympse/android/hal/Helpers;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 156
    iget v0, p0, Lcom/glympse/android/lib/do;->qn:I

    return v0
.end method

.method public startMonitoring(Lcom/glympse/android/core/GRegion;)V
    .locals 1

    .prologue
    .line 365
    iget-object v0, p0, Lcom/glympse/android/lib/do;->hl:Lcom/glympse/android/core/GProximityProvider;

    invoke-interface {v0, p1}, Lcom/glympse/android/core/GProximityProvider;->startMonitoring(Lcom/glympse/android/core/GRegion;)V

    .line 366
    return-void
.end method

.method public startStopLocation(Z)V
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-nez v0, :cond_0

    .line 386
    :goto_0
    return-void

    .line 382
    :cond_0
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/do;->j(Z)V

    .line 385
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/do;->updateProfile(Z)V

    goto :goto_0
.end method

.method public stateChanged(I)V
    .locals 4

    .prologue
    .line 742
    iget-object v0, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/glympse/android/lib/do;->cg:I

    if-eq p1, v0, :cond_0

    .line 746
    iput p1, p0, Lcom/glympse/android/lib/do;->cg:I

    .line 749
    iget-object v0, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/16 v1, 0x8

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/glympse/android/lib/do;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 752
    iget v0, p0, Lcom/glympse/android/lib/do;->cg:I

    packed-switch v0, :pswitch_data_0

    .line 764
    const-string v0, "undetermined"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 768
    :goto_0
    iget-object v1, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->getDiagnosticsManager()Lcom/glympse/android/lib/GDiagnosticsManager;

    move-result-object v1

    const-string v2, "location"

    .line 769
    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "state"

    invoke-static {v3}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 768
    invoke-interface {v1, v2, v3, v0}, Lcom/glympse/android/lib/GDiagnosticsManager;->logEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 771
    :cond_0
    return-void

    .line 755
    :pswitch_0
    const-string v0, "acquired"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 758
    :pswitch_1
    const-string v0, "denied"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 761
    :pswitch_2
    const-string v0, "error"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 752
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public stop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 324
    iget-object v0, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getConfig()Lcom/glympse/android/api/GConfig;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GConfigPrivate;

    .line 325
    iget-object v1, p0, Lcom/glympse/android/lib/do;->qq:Lcom/glympse/android/lib/do$a;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GConfigPrivate;->removeListener(Lcom/glympse/android/api/GEventListener;)Z

    .line 326
    iput-object v2, p0, Lcom/glympse/android/lib/do;->qq:Lcom/glympse/android/lib/do$a;

    .line 329
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/do;->k(Z)V

    .line 330
    invoke-direct {p0}, Lcom/glympse/android/lib/do;->cu()V

    .line 333
    iget-object v0, p0, Lcom/glympse/android/lib/do;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0}, Lcom/glympse/android/lib/CommonSink;->removeAllListeners()Z

    .line 336
    iput-object v2, p0, Lcom/glympse/android/lib/do;->lN:Lcom/glympse/android/lib/GBatteryManagerPrivate;

    .line 337
    iput-object v2, p0, Lcom/glympse/android/lib/do;->lX:Lcom/glympse/android/lib/GCorrectedTime;

    .line 338
    iput-object v2, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 339
    return-void
.end method

.method public stopLocation(Z)I
    .locals 4

    .prologue
    .line 161
    iget v0, p0, Lcom/glympse/android/lib/do;->qn:I

    if-nez v0, :cond_0

    .line 163
    const/4 v0, -0x1

    .line 183
    :goto_0
    return v0

    .line 165
    :cond_0
    if-eqz p1, :cond_2

    .line 167
    const/4 v0, 0x0

    iput v0, p0, Lcom/glympse/android/lib/do;->qn:I

    .line 173
    :goto_1
    iget v0, p0, Lcom/glympse/android/lib/do;->qn:I

    if-nez v0, :cond_1

    .line 175
    iget-object v0, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_1

    .line 177
    iget-object v0, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->startStopLocation()V

    .line 181
    :cond_1
    const/4 v0, 0x1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[LocationManager.stopLocation] Counter: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/glympse/android/lib/do;->qn:I

    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/glympse/android/hal/Helpers;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 183
    iget v0, p0, Lcom/glympse/android/lib/do;->qn:I

    goto :goto_0

    .line 171
    :cond_2
    iget v0, p0, Lcom/glympse/android/lib/do;->qn:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/glympse/android/lib/do;->qn:I

    goto :goto_1
.end method

.method public stopMonitoring(Lcom/glympse/android/core/GRegion;)V
    .locals 1

    .prologue
    .line 370
    iget-object v0, p0, Lcom/glympse/android/lib/do;->hl:Lcom/glympse/android/core/GProximityProvider;

    invoke-interface {v0, p1}, Lcom/glympse/android/core/GProximityProvider;->stopMonitoring(Lcom/glympse/android/core/GRegion;)V

    .line 371
    return-void
.end method

.method public updateProfile(Z)V
    .locals 6

    .prologue
    .line 615
    iget-object v0, p0, Lcom/glympse/android/lib/do;->qr:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 647
    :cond_0
    :goto_0
    return-void

    .line 621
    :cond_1
    invoke-direct {p0}, Lcom/glympse/android/lib/do;->cv()I

    move-result v1

    .line 624
    iget v0, p0, Lcom/glympse/android/lib/do;->qs:I

    if-ne v1, v0, :cond_2

    if-eqz p1, :cond_0

    .line 626
    :cond_2
    const/4 v0, 0x3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[LocationManager.updateProfile] New profile: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    int-to-long v4, v1

    invoke-static {v4, v5}, Lcom/glympse/android/hal/Helpers;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 629
    iput v1, p0, Lcom/glympse/android/lib/do;->qs:I

    .line 630
    iget-object v0, p0, Lcom/glympse/android/lib/do;->qr:Lcom/glympse/android/hal/GVector;

    iget v2, p0, Lcom/glympse/android/lib/do;->qs:I

    invoke-virtual {v0, v2}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GLocationProfile;

    iput-object v0, p0, Lcom/glympse/android/lib/do;->cj:Lcom/glympse/android/core/GLocationProfile;

    .line 633
    invoke-direct {p0}, Lcom/glympse/android/lib/do;->cw()V

    .line 636
    iget-object v0, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getDiagnosticsManager()Lcom/glympse/android/lib/GDiagnosticsManager;

    move-result-object v0

    const-string v2, "location"

    .line 637
    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "profile"

    invoke-static {v3}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    int-to-long v4, v1

    invoke-static {v4, v5}, Lcom/glympse/android/hal/Helpers;->toString(J)Ljava/lang/String;

    move-result-object v1

    .line 636
    invoke-interface {v0, v2, v3, v1}, Lcom/glympse/android/lib/GDiagnosticsManager;->logEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 641
    iget-object v0, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getBatteryManager()Lcom/glympse/android/api/GBatteryManager;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GBatteryManagerPrivate;

    .line 642
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GBatteryManagerPrivate;->logBatteryEvent(Lcom/glympse/android/api/GTicket;)V

    .line 645
    iget-object v0, p0, Lcom/glympse/android/lib/do;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/16 v1, 0x8

    const/16 v2, 0x100

    iget-object v3, p0, Lcom/glympse/android/lib/do;->cj:Lcom/glympse/android/core/GLocationProfile;

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/glympse/android/lib/do;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    goto :goto_0
.end method

.method public updateProfile(Lcom/glympse/android/core/GLocationProfile;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 554
    invoke-interface {p1}, Lcom/glympse/android/core/GLocationProfile;->getProfile()I

    move-result v1

    .line 555
    if-ltz v1, :cond_0

    iget-object v2, p0, Lcom/glympse/android/lib/do;->qr:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v2}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v2

    if-lt v1, v2, :cond_2

    .line 557
    :cond_0
    const/4 v0, 0x0

    .line 581
    :cond_1
    :goto_0
    return v0

    .line 561
    :cond_2
    iget-object v2, p0, Lcom/glympse/android/lib/do;->qr:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v2, p1, v1}, Lcom/glympse/android/hal/GVector;->setElementAt(Ljava/lang/Object;I)V

    .line 564
    iget v2, p0, Lcom/glympse/android/lib/do;->qs:I

    if-ne v1, v2, :cond_1

    .line 569
    iget-object v1, p0, Lcom/glympse/android/lib/do;->cj:Lcom/glympse/android/core/GLocationProfile;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/glympse/android/lib/do;->cj:Lcom/glympse/android/core/GLocationProfile;

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 575
    :cond_3
    iput-object p1, p0, Lcom/glympse/android/lib/do;->cj:Lcom/glympse/android/core/GLocationProfile;

    .line 578
    invoke-direct {p0}, Lcom/glympse/android/lib/do;->cw()V

    goto :goto_0
.end method
