.class Lcom/glympse/android/lib/b;
.super Lcom/glympse/android/lib/e;
.source "AccountCreate.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/lib/b$a;
    }
.end annotation


# instance fields
.field protected fW:Ljava/lang/String;

.field protected fY:Lcom/glympse/android/core/GPrimitive;

.field private fZ:Ljava/lang/String;

.field protected gb:Lcom/glympse/android/lib/GAccountListener;

.field protected gc:Lcom/glympse/android/lib/b$a;

.field private h:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/GAccountListener;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/glympse/android/lib/e;-><init>()V

    .line 31
    invoke-direct {p0, p1, p2}, Lcom/glympse/android/lib/b;->a(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/GAccountListener;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/GAccountListener;Lcom/glympse/android/core/GPrimitive;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/glympse/android/lib/e;-><init>()V

    .line 36
    invoke-direct {p0, p1, p2}, Lcom/glympse/android/lib/b;->a(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/GAccountListener;)V

    .line 38
    iput-object p3, p0, Lcom/glympse/android/lib/b;->fY:Lcom/glympse/android/core/GPrimitive;

    .line 39
    return-void
.end method

.method public constructor <init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/GAccountListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/glympse/android/lib/e;-><init>()V

    .line 43
    invoke-direct {p0, p1, p2}, Lcom/glympse/android/lib/b;->a(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/GAccountListener;)V

    .line 45
    iput-object p3, p0, Lcom/glympse/android/lib/b;->k:Ljava/lang/String;

    .line 46
    iput-object p4, p0, Lcom/glympse/android/lib/b;->l:Ljava/lang/String;

    .line 47
    iput-object p5, p0, Lcom/glympse/android/lib/b;->h:Ljava/lang/String;

    .line 48
    return-void
.end method

.method private a(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/GAccountListener;)V
    .locals 1

    .prologue
    .line 165
    iput-object p2, p0, Lcom/glympse/android/lib/b;->gb:Lcom/glympse/android/lib/GAccountListener;

    .line 166
    invoke-interface {p1}, Lcom/glympse/android/lib/GGlympsePrivate;->getApiKey()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/b;->fW:Ljava/lang/String;

    .line 167
    new-instance v0, Lcom/glympse/android/lib/b$a;

    invoke-direct {v0}, Lcom/glympse/android/lib/b$a;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/b;->gc:Lcom/glympse/android/lib/b$a;

    .line 168
    iget-object v0, p0, Lcom/glympse/android/lib/b;->gc:Lcom/glympse/android/lib/b$a;

    iput-object v0, p0, Lcom/glympse/android/lib/b;->gB:Lcom/glympse/android/lib/f;

    .line 169
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 155
    new-instance v0, Lcom/glympse/android/lib/b$a;

    invoke-direct {v0}, Lcom/glympse/android/lib/b$a;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/b;->gc:Lcom/glympse/android/lib/b$a;

    .line 156
    iget-object v0, p0, Lcom/glympse/android/lib/b;->gc:Lcom/glympse/android/lib/b$a;

    iput-object v0, p0, Lcom/glympse/android/lib/b;->gB:Lcom/glympse/android/lib/f;

    .line 157
    return-void
.end method

.method public methodType()I
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x2

    return v0
.end method

.method public post()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/glympse/android/lib/b;->fZ:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/b;->fY:Lcom/glympse/android/core/GPrimitive;

    if-nez v0, :cond_1

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/b;->fZ:Ljava/lang/String;

    .line 104
    :goto_0
    return-object v0

    .line 102
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/b;->fY:Lcom/glympse/android/core/GPrimitive;

    invoke-static {v0}, Lcom/glympse/android/lib/json/JsonSerializer;->toString(Lcom/glympse/android/core/GPrimitive;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/b;->fZ:Ljava/lang/String;

    .line 104
    iget-object v0, p0, Lcom/glympse/android/lib/b;->fZ:Ljava/lang/String;

    goto :goto_0
.end method

.method public process()Z
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 109
    iget-object v2, p0, Lcom/glympse/android/lib/b;->gc:Lcom/glympse/android/lib/b$a;

    iget-object v2, v2, Lcom/glympse/android/lib/b$a;->gF:Ljava/lang/String;

    const-string v3, "ok"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/glympse/android/lib/b;->gc:Lcom/glympse/android/lib/b$a;

    iget-object v2, v2, Lcom/glympse/android/lib/b$a;->gd:Ljava/lang/String;

    .line 110
    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/glympse/android/lib/b;->gc:Lcom/glympse/android/lib/b$a;

    iget-object v2, v2, Lcom/glympse/android/lib/b$a;->ge:Ljava/lang/String;

    .line 111
    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 113
    iget-object v1, p0, Lcom/glympse/android/lib/b;->gb:Lcom/glympse/android/lib/GAccountListener;

    iget-object v2, p0, Lcom/glympse/android/lib/b;->gc:Lcom/glympse/android/lib/b$a;

    iget-object v2, v2, Lcom/glympse/android/lib/b$a;->gd:Ljava/lang/String;

    iget-object v3, p0, Lcom/glympse/android/lib/b;->gc:Lcom/glympse/android/lib/b$a;

    iget-object v3, v3, Lcom/glympse/android/lib/b$a;->ge:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lcom/glympse/android/lib/GAccountListener;->accountCreated(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    :goto_0
    return v0

    .line 116
    :cond_0
    iget-object v2, p0, Lcom/glympse/android/lib/b;->gc:Lcom/glympse/android/lib/b$a;

    iget-object v2, v2, Lcom/glympse/android/lib/b$a;->gF:Ljava/lang/String;

    const-string v3, "retry"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 118
    iget-object v0, p0, Lcom/glympse/android/lib/b;->gb:Lcom/glympse/android/lib/GAccountListener;

    iget-object v2, p0, Lcom/glympse/android/lib/b;->fY:Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v0, v2}, Lcom/glympse/android/lib/GAccountListener;->retryAccountCreate(Lcom/glympse/android/core/GPrimitive;)V

    move v0, v1

    .line 119
    goto :goto_0

    .line 121
    :cond_1
    iget-object v2, p0, Lcom/glympse/android/lib/b;->gc:Lcom/glympse/android/lib/b$a;

    iget-object v2, v2, Lcom/glympse/android/lib/b$a;->gG:Ljava/lang/String;

    const-string v3, "access_denied"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 123
    iget-object v2, p0, Lcom/glympse/android/lib/b;->gb:Lcom/glympse/android/lib/GAccountListener;

    new-instance v3, Lcom/glympse/android/lib/ex;

    const/4 v4, 0x3

    iget-object v5, p0, Lcom/glympse/android/lib/b;->gc:Lcom/glympse/android/lib/b$a;

    iget-object v5, v5, Lcom/glympse/android/lib/b$a;->gG:Ljava/lang/String;

    iget-object v6, p0, Lcom/glympse/android/lib/b;->gc:Lcom/glympse/android/lib/b$a;

    iget-object v6, v6, Lcom/glympse/android/lib/b$a;->gH:Ljava/lang/String;

    invoke-direct {v3, v4, v5, v6}, Lcom/glympse/android/lib/ex;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v0, v7, v3}, Lcom/glympse/android/lib/GAccountListener;->failedToCreate(ZILcom/glympse/android/api/GServerError;)V

    move v0, v1

    .line 125
    goto :goto_0

    .line 127
    :cond_2
    iget-object v2, p0, Lcom/glympse/android/lib/b;->gc:Lcom/glympse/android/lib/b$a;

    iget-object v2, v2, Lcom/glympse/android/lib/b$a;->gG:Ljava/lang/String;

    const-string v3, "invalid_argument"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/glympse/android/lib/b;->gc:Lcom/glympse/android/lib/b$a;

    iget-object v2, v2, Lcom/glympse/android/lib/b$a;->gG:Ljava/lang/String;

    const-string v3, "serialization_error"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 129
    :cond_3
    iget-object v2, p0, Lcom/glympse/android/lib/b;->gb:Lcom/glympse/android/lib/GAccountListener;

    new-instance v3, Lcom/glympse/android/lib/ex;

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/glympse/android/lib/b;->gc:Lcom/glympse/android/lib/b$a;

    iget-object v5, v5, Lcom/glympse/android/lib/b$a;->gG:Ljava/lang/String;

    iget-object v6, p0, Lcom/glympse/android/lib/b;->gc:Lcom/glympse/android/lib/b$a;

    iget-object v6, v6, Lcom/glympse/android/lib/b$a;->gH:Ljava/lang/String;

    invoke-direct {v3, v4, v5, v6}, Lcom/glympse/android/lib/ex;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v0, v7, v3}, Lcom/glympse/android/lib/GAccountListener;->failedToCreate(ZILcom/glympse/android/api/GServerError;)V

    move v0, v1

    .line 131
    goto :goto_0

    .line 133
    :cond_4
    iget-object v2, p0, Lcom/glympse/android/lib/b;->gc:Lcom/glympse/android/lib/b$a;

    iget-object v2, v2, Lcom/glympse/android/lib/b$a;->gG:Ljava/lang/String;

    const-string v3, "link_failed"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/glympse/android/lib/b;->gc:Lcom/glympse/android/lib/b$a;

    iget-object v2, v2, Lcom/glympse/android/lib/b$a;->gG:Ljava/lang/String;

    const-string v3, "required_argument"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 135
    :cond_5
    iget-object v2, p0, Lcom/glympse/android/lib/b;->gb:Lcom/glympse/android/lib/GAccountListener;

    new-instance v3, Lcom/glympse/android/lib/ex;

    iget-object v4, p0, Lcom/glympse/android/lib/b;->gc:Lcom/glympse/android/lib/b$a;

    iget-object v4, v4, Lcom/glympse/android/lib/b$a;->gG:Ljava/lang/String;

    iget-object v5, p0, Lcom/glympse/android/lib/b;->gc:Lcom/glympse/android/lib/b$a;

    iget-object v5, v5, Lcom/glympse/android/lib/b$a;->gH:Ljava/lang/String;

    invoke-direct {v3, v7, v4, v5}, Lcom/glympse/android/lib/ex;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v0, v7, v3}, Lcom/glympse/android/lib/GAccountListener;->failedToCreate(ZILcom/glympse/android/api/GServerError;)V

    move v0, v1

    .line 137
    goto/16 :goto_0

    .line 139
    :cond_6
    iget-object v2, p0, Lcom/glympse/android/lib/b;->gc:Lcom/glympse/android/lib/b$a;

    iget-object v2, v2, Lcom/glympse/android/lib/b$a;->gG:Ljava/lang/String;

    const-string v3, "disabled"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 141
    iget-object v2, p0, Lcom/glympse/android/lib/b;->gb:Lcom/glympse/android/lib/GAccountListener;

    new-instance v3, Lcom/glympse/android/lib/ex;

    const/16 v4, 0x9

    iget-object v5, p0, Lcom/glympse/android/lib/b;->gc:Lcom/glympse/android/lib/b$a;

    iget-object v5, v5, Lcom/glympse/android/lib/b$a;->gG:Ljava/lang/String;

    iget-object v6, p0, Lcom/glympse/android/lib/b;->gc:Lcom/glympse/android/lib/b$a;

    iget-object v6, v6, Lcom/glympse/android/lib/b$a;->gH:Ljava/lang/String;

    invoke-direct {v3, v4, v5, v6}, Lcom/glympse/android/lib/ex;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v0, v7, v3}, Lcom/glympse/android/lib/GAccountListener;->failedToCreate(ZILcom/glympse/android/api/GServerError;)V

    move v0, v1

    .line 143
    goto/16 :goto_0

    .line 147
    :cond_7
    iget-object v2, p0, Lcom/glympse/android/lib/b;->gb:Lcom/glympse/android/lib/GAccountListener;

    new-instance v3, Lcom/glympse/android/lib/ex;

    iget-object v4, p0, Lcom/glympse/android/lib/b;->gc:Lcom/glympse/android/lib/b$a;

    iget-object v4, v4, Lcom/glympse/android/lib/b$a;->gG:Ljava/lang/String;

    iget-object v5, p0, Lcom/glympse/android/lib/b;->gc:Lcom/glympse/android/lib/b$a;

    iget-object v5, v5, Lcom/glympse/android/lib/b$a;->gH:Ljava/lang/String;

    invoke-direct {v3, v0, v4, v5}, Lcom/glympse/android/lib/ex;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v0, v7, v3}, Lcom/glympse/android/lib/GAccountListener;->failedToCreate(ZILcom/glympse/android/api/GServerError;)V

    move v0, v1

    .line 149
    goto/16 :goto_0
.end method

.method public url(Ljava/lang/StringBuilder;)Z
    .locals 2

    .prologue
    .line 56
    const-string v0, "account/create?api_key="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    iget-object v0, p0, Lcom/glympse/android/lib/b;->fW:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 58
    iget-object v0, p0, Lcom/glympse/android/lib/b;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 60
    const-string v0, "&key_id="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    iget-object v0, p0, Lcom/glympse/android/lib/b;->k:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/b;->l:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 65
    const-string v0, "&data="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    iget-object v0, p0, Lcom/glympse/android/lib/b;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/b;->h:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 70
    const-string v0, "&secret="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    iget-object v0, p0, Lcom/glympse/android/lib/b;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/b;->fY:Lcom/glympse/android/core/GPrimitive;

    if-eqz v0, :cond_3

    .line 75
    iget-object v0, p0, Lcom/glympse/android/lib/b;->fY:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "type"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 76
    if-eqz v0, :cond_3

    .line 78
    const-string v1, "&type="

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    :cond_3
    const/4 v0, 0x1

    return v0
.end method

.method public userAgent()Z
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x1

    return v0
.end method
