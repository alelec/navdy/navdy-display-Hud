.class public Lcom/glympse/android/lib/LibFactory;
.super Ljava/lang/Object;
.source "LibFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createApiStatus(Ljava/lang/String;)Lcom/glympse/android/api/GApiStatus;
    .locals 1

    .prologue
    .line 35
    new-instance v0, Lcom/glympse/android/lib/h;

    invoke-direct {v0, p0}, Lcom/glympse/android/lib/h;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static createCalendarEvent(Ljava/lang/String;ZJLjava/lang/String;Lcom/glympse/android/api/GInvite;Lcom/glympse/android/core/GArray;)Lcom/glympse/android/lib/GCalendarEvent;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "ZJ",
            "Ljava/lang/String;",
            "Lcom/glympse/android/api/GInvite;",
            "Lcom/glympse/android/core/GArray",
            "<",
            "Lcom/glympse/android/api/GInvite;",
            ">;)",
            "Lcom/glympse/android/lib/GCalendarEvent;"
        }
    .end annotation

    .prologue
    .line 132
    new-instance v1, Lcom/glympse/android/lib/t;

    move-object v2, p0

    move v3, p1

    move-wide v4, p2

    move-object v6, p4

    move-object v7, p5

    move-object/from16 v8, p6

    invoke-direct/range {v1 .. v8}, Lcom/glympse/android/lib/t;-><init>(Ljava/lang/String;ZJLjava/lang/String;Lcom/glympse/android/api/GInvite;Lcom/glympse/android/core/GArray;)V

    return-object v1
.end method

.method public static createCalendarManager()Lcom/glympse/android/lib/GCalendarManager;
    .locals 1

    .prologue
    .line 126
    new-instance v0, Lcom/glympse/android/lib/u;

    invoke-direct {v0}, Lcom/glympse/android/lib/u;-><init>()V

    return-object v0
.end method

.method public static createChronoTrigger(Ljava/lang/String;ZLcom/glympse/android/api/GTicket;J)Lcom/glympse/android/api/GChronoTrigger;
    .locals 7

    .prologue
    .line 265
    new-instance v0, Lcom/glympse/android/lib/v;

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/glympse/android/lib/v;-><init>(Ljava/lang/String;ZLcom/glympse/android/api/GTicket;J)V

    return-object v0
.end method

.method public static createConfig()Lcom/glympse/android/lib/GConfigPrivate;
    .locals 1

    .prologue
    .line 171
    new-instance v0, Lcom/glympse/android/lib/x;

    invoke-direct {v0}, Lcom/glympse/android/lib/x;-><init>()V

    return-object v0
.end method

.method public static createContactsManager()Lcom/glympse/android/lib/GContactsManager;
    .locals 1

    .prologue
    .line 111
    new-instance v0, Lcom/glympse/android/lib/ac;

    invoke-direct {v0}, Lcom/glympse/android/lib/ac;-><init>()V

    return-object v0
.end method

.method public static createDataRow(JJLjava/lang/String;Lcom/glympse/android/core/GPrimitive;)Lcom/glympse/android/api/GDataRow;
    .locals 8

    .prologue
    .line 290
    new-instance v1, Lcom/glympse/android/lib/aj;

    move-wide v2, p0

    move-wide v4, p2

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/glympse/android/lib/aj;-><init>(JJLjava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    return-object v1
.end method

.method public static createEtaTrigger(Ljava/lang/String;ZLcom/glympse/android/api/GTicket;JILcom/glympse/android/api/GTicket;)Lcom/glympse/android/api/GEtaTrigger;
    .locals 9

    .prologue
    .line 270
    new-instance v0, Lcom/glympse/android/lib/aw;

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-wide v4, p3

    move v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/glympse/android/lib/aw;-><init>(Ljava/lang/String;ZLcom/glympse/android/api/GTicket;JILcom/glympse/android/api/GTicket;)V

    return-object v0
.end method

.method public static createEventSink(Ljava/lang/String;)Lcom/glympse/android/api/GEventSink;
    .locals 1

    .prologue
    .line 186
    new-instance v0, Lcom/glympse/android/lib/ay;

    invoke-direct {v0, p0}, Lcom/glympse/android/lib/ay;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static createExponentialBackOff()Lcom/glympse/android/lib/GBackOffPolicy;
    .locals 1

    .prologue
    .line 280
    new-instance v0, Lcom/glympse/android/lib/az;

    invoke-direct {v0}, Lcom/glympse/android/lib/az;-><init>()V

    return-object v0
.end method

.method public static createExtensionManager()Lcom/glympse/android/lib/GExtensionManager;
    .locals 1

    .prologue
    .line 161
    new-instance v0, Lcom/glympse/android/lib/ba;

    invoke-direct {v0}, Lcom/glympse/android/lib/ba;-><init>()V

    return-object v0
.end method

.method public static createGeoTrigger(Ljava/lang/String;ZLcom/glympse/android/api/GTicket;Lcom/glympse/android/core/GLatLng;DI)Lcom/glympse/android/api/GGeoTrigger;
    .locals 10

    .prologue
    .line 260
    new-instance v1, Lcom/glympse/android/lib/bo;

    move-object v2, p0

    move v3, p1

    move-object v4, p2

    move-object v5, p3

    move-wide v6, p4

    move/from16 v8, p6

    invoke-direct/range {v1 .. v8}, Lcom/glympse/android/lib/bo;-><init>(Ljava/lang/String;ZLcom/glympse/android/api/GTicket;Lcom/glympse/android/core/GLatLng;DI)V

    return-object v1
.end method

.method public static createGlympse(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/glympse/android/api/GGlympse;
    .locals 2

    .prologue
    .line 45
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/glympse/android/hal/HalFactory;->createContextHolder(Landroid/content/Context;)Lcom/glympse/android/hal/GContextHolder;

    move-result-object v0

    .line 49
    new-instance v1, Lcom/glympse/android/lib/br;

    invoke-direct {v1, v0, p1, p2, p3}, Lcom/glympse/android/lib/br;-><init>(Lcom/glympse/android/hal/GContextHolder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    return-object v1
.end method

.method public static createGlympse(Lcom/glympse/android/hal/GContextHolder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/glympse/android/api/GGlympse;
    .locals 1

    .prologue
    .line 56
    new-instance v0, Lcom/glympse/android/lib/br;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/glympse/android/lib/br;-><init>(Lcom/glympse/android/hal/GContextHolder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static createGroup(Z)Lcom/glympse/android/lib/GGroupPrivate;
    .locals 1

    .prologue
    .line 86
    new-instance v0, Lcom/glympse/android/lib/bw;

    invoke-direct {v0, p0}, Lcom/glympse/android/lib/bw;-><init>(Z)V

    return-object v0
.end method

.method public static createGroupMember(Lcom/glympse/android/api/GUser;Lcom/glympse/android/api/GTicket;)Lcom/glympse/android/lib/GGroupMemberPrivate;
    .locals 1

    .prologue
    .line 106
    new-instance v0, Lcom/glympse/android/lib/cf;

    invoke-direct {v0, p0, p1}, Lcom/glympse/android/lib/cf;-><init>(Lcom/glympse/android/api/GUser;Lcom/glympse/android/api/GTicket;)V

    return-object v0
.end method

.method public static createHandler()Lcom/glympse/android/core/GHandler;
    .locals 1

    .prologue
    .line 285
    invoke-static {}, Lcom/glympse/android/hal/HalFactory;->createHandler()Lcom/glympse/android/core/GHandler;

    move-result-object v0

    return-object v0
.end method

.method public static createHttpConnection()Lcom/glympse/android/hal/GHttpConnection;
    .locals 1

    .prologue
    .line 275
    invoke-static {}, Lcom/glympse/android/hal/HalFactory;->createHttpConnection()Lcom/glympse/android/hal/GHttpConnection;

    move-result-object v0

    return-object v0
.end method

.method public static createImage(Lcom/glympse/android/core/GDrawable;)Lcom/glympse/android/lib/GImagePrivate;
    .locals 2

    .prologue
    .line 176
    new-instance v0, Lcom/glympse/android/lib/cq;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0}, Lcom/glympse/android/lib/cq;-><init>(Ljava/lang/String;Lcom/glympse/android/core/GDrawable;)V

    return-object v0
.end method

.method public static createImage(Ljava/lang/String;Lcom/glympse/android/core/GDrawable;)Lcom/glympse/android/lib/GImagePrivate;
    .locals 1

    .prologue
    .line 181
    new-instance v0, Lcom/glympse/android/lib/cq;

    invoke-direct {v0, p0, p1}, Lcom/glympse/android/lib/cq;-><init>(Ljava/lang/String;Lcom/glympse/android/core/GDrawable;)V

    return-object v0
.end method

.method public static createInvite(ILjava/lang/String;Ljava/lang/String;)Lcom/glympse/android/api/GInvite;
    .locals 1

    .prologue
    .line 91
    invoke-static {p0, p1, p2}, Lcom/glympse/android/lib/ct;->createInvite(ILjava/lang/String;Ljava/lang/String;)Lcom/glympse/android/api/GInvite;

    move-result-object v0

    return-object v0
.end method

.method public static createInvite(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/glympse/android/api/GInvite;
    .locals 1

    .prologue
    .line 96
    invoke-static {p0, p1, p2, p3}, Lcom/glympse/android/lib/ct;->createInvite(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/glympse/android/api/GInvite;

    move-result-object v0

    return-object v0
.end method

.method public static createJobQueue(Lcom/glympse/android/core/GHandler;)Lcom/glympse/android/lib/GJobQueue;
    .locals 1

    .prologue
    .line 220
    new-instance v0, Lcom/glympse/android/lib/df;

    invoke-direct {v0, p0}, Lcom/glympse/android/lib/df;-><init>(Lcom/glympse/android/core/GHandler;)V

    return-object v0
.end method

.method public static createLocalContactsProvider(Lcom/glympse/android/api/GGlympse;Landroid/content/Context;)Lcom/glympse/android/hal/GContactsProvider;
    .locals 3

    .prologue
    .line 116
    invoke-static {p1}, Lcom/glympse/android/hal/HalFactory;->createLocalContactsProvider(Landroid/content/Context;)Lcom/glympse/android/hal/GContactsProvider;

    move-result-object v0

    .line 119
    check-cast p0, Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {p0}, Lcom/glympse/android/lib/GGlympsePrivate;->getContentResolver()Lcom/glympse/android/lib/GContentResolver;

    move-result-object v1

    new-instance v2, Lcom/glympse/android/lib/ea;

    invoke-direct {v2, v0}, Lcom/glympse/android/lib/ea;-><init>(Lcom/glympse/android/hal/GContactsProvider;)V

    invoke-interface {v1, v2}, Lcom/glympse/android/lib/GContentResolver;->registerProvider(Lcom/glympse/android/lib/GContentProvider;)V

    .line 121
    return-object v0
.end method

.method public static createLocation(JDD)Lcom/glympse/android/lib/GLocationPrivate;
    .locals 14

    .prologue
    .line 61
    new-instance v1, Lcom/glympse/android/lib/Location;

    const/high16 v8, 0x7fc00000    # NaNf

    const/high16 v9, 0x7fc00000    # NaNf

    const/high16 v10, 0x7fc00000    # NaNf

    const/high16 v11, 0x7fc00000    # NaNf

    const/high16 v12, 0x7fc00000    # NaNf

    move-wide v2, p0

    move-wide/from16 v4, p2

    move-wide/from16 v6, p4

    invoke-direct/range {v1 .. v12}, Lcom/glympse/android/lib/Location;-><init>(JDDFFFFF)V

    return-object v1
.end method

.method public static createMemoryCache(II)Lcom/glympse/android/lib/GMemoryCache;
    .locals 1

    .prologue
    .line 142
    new-instance v0, Lcom/glympse/android/lib/dp;

    invoke-direct {v0, p0, p1}, Lcom/glympse/android/lib/dp;-><init>(II)V

    return-object v0
.end method

.method public static createNotificationCenter(Landroid/content/Context;Ljava/lang/String;)Lcom/glympse/android/lib/GNotificationCenter;
    .locals 2

    .prologue
    .line 148
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/glympse/android/hal/HalFactory;->createContextHolder(Landroid/content/Context;)Lcom/glympse/android/hal/GContextHolder;

    move-result-object v0

    .line 151
    new-instance v1, Lcom/glympse/android/lib/dx;

    invoke-direct {v1, v0, p1}, Lcom/glympse/android/lib/dx;-><init>(Lcom/glympse/android/hal/GContextHolder;Ljava/lang/String;)V

    return-object v1
.end method

.method public static createPersistentChannel(Lcom/glympse/android/core/GHandler;)Lcom/glympse/android/lib/PersistentChannel;
    .locals 1

    .prologue
    .line 156
    new-instance v0, Lcom/glympse/android/lib/PersistentChannel;

    invoke-direct {v0, p0}, Lcom/glympse/android/lib/PersistentChannel;-><init>(Lcom/glympse/android/core/GHandler;)V

    return-object v0
.end method

.method public static createPerson(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/glympse/android/lib/GPerson;
    .locals 8

    .prologue
    .line 166
    new-instance v1, Lcom/glympse/android/lib/PersonLocal;

    move-wide v2, p0

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/glympse/android/lib/PersonLocal;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method public static createPhoneFavorite(Ljava/lang/String;Ljava/lang/String;I)Lcom/glympse/android/lib/GPhoneFavorite;
    .locals 1

    .prologue
    .line 137
    new-instance v0, Lcom/glympse/android/lib/ef;

    invoke-direct {v0, p0, p1, p2}, Lcom/glympse/android/lib/ef;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public static createPlace(DDLjava/lang/String;)Lcom/glympse/android/api/GPlace;
    .locals 8

    .prologue
    .line 101
    new-instance v1, Lcom/glympse/android/lib/eg;

    move-wide v2, p0

    move-wide v4, p2

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/glympse/android/lib/eg;-><init>(DDLjava/lang/String;)V

    return-object v1
.end method

.method public static createRegion(DDDLjava/lang/String;)Lcom/glympse/android/core/GRegion;
    .locals 12

    .prologue
    .line 66
    new-instance v1, Lcom/glympse/android/lib/es;

    const-wide/16 v8, 0x0

    move-wide v2, p0

    move-wide v4, p2

    move-wide/from16 v6, p4

    move-object/from16 v10, p6

    invoke-direct/range {v1 .. v10}, Lcom/glympse/android/lib/es;-><init>(DDDDLjava/lang/String;)V

    return-object v1
.end method

.method public static createStorage(Landroid/content/Context;Ljava/lang/String;)Lcom/glympse/android/core/GStorageUnit;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 192
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/glympse/android/hal/HalFactory;->createContextHolder(Landroid/content/Context;)Lcom/glympse/android/hal/GContextHolder;

    move-result-object v0

    .line 194
    new-instance v1, Lcom/glympse/android/lib/fb;

    invoke-direct {v1}, Lcom/glympse/android/lib/fb;-><init>()V

    .line 195
    invoke-virtual {v1, v0, v2, v2, p1}, Lcom/glympse/android/lib/fb;->a(Lcom/glympse/android/hal/GContextHolder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    return-object v1
.end method

.method public static createTicket(Z)Lcom/glympse/android/lib/GTicketPrivate;
    .locals 1

    .prologue
    .line 81
    new-instance v0, Lcom/glympse/android/lib/fe;

    invoke-direct {v0, p0}, Lcom/glympse/android/lib/fe;-><init>(Z)V

    return-object v0
.end method

.method public static createTrack()Lcom/glympse/android/lib/GTrackPrivate;
    .locals 1

    .prologue
    .line 71
    new-instance v0, Lcom/glympse/android/lib/fs;

    invoke-direct {v0}, Lcom/glympse/android/lib/fs;-><init>()V

    return-object v0
.end method

.method public static createTrackBuilder()Lcom/glympse/android/api/GTrackBuilder;
    .locals 1

    .prologue
    .line 215
    new-instance v0, Lcom/glympse/android/lib/ft;

    invoke-direct {v0}, Lcom/glympse/android/lib/ft;-><init>()V

    return-object v0
.end method

.method public static createUri(Ljava/lang/String;)Lcom/glympse/android/lib/GUri;
    .locals 1

    .prologue
    .line 210
    invoke-static {p0}, Lcom/glympse/android/lib/ga;->F(Ljava/lang/String;)Lcom/glympse/android/lib/GUri;

    move-result-object v0

    return-object v0
.end method

.method public static createUser()Lcom/glympse/android/lib/GUserPrivate;
    .locals 1

    .prologue
    .line 76
    new-instance v0, Lcom/glympse/android/lib/gb;

    invoke-direct {v0}, Lcom/glympse/android/lib/gb;-><init>()V

    return-object v0
.end method

.method public static createUserMessage(Lcom/glympse/android/api/GUser;Lcom/glympse/android/api/GDataRow;)Lcom/glympse/android/lib/GUserMessage;
    .locals 1

    .prologue
    .line 295
    new-instance v0, Lcom/glympse/android/lib/gg;

    invoke-direct {v0, p0, p1}, Lcom/glympse/android/lib/gg;-><init>(Lcom/glympse/android/api/GUser;Lcom/glympse/android/api/GDataRow;)V

    return-object v0
.end method

.method public static generateReturnUri(Lcom/glympse/android/api/GGlympse;Ljava/lang/String;Lcom/glympse/android/api/GTicket;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 255
    invoke-static {p0, p1, p2, p3, p4}, Lcom/glympse/android/lib/UrlGenerator;->generateReturnUri(Lcom/glympse/android/api/GGlympse;Ljava/lang/String;Lcom/glympse/android/api/GTicket;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static guessInviteType(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 235
    invoke-static {p0}, Lcom/glympse/android/lib/ct;->r(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static inviteTypeEnumToString(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 225
    invoke-static {p0}, Lcom/glympse/android/lib/ct;->v(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static inviteTypeStringToEnum(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 230
    invoke-static {p0}, Lcom/glympse/android/lib/ct;->p(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static isAddressRequired(I)Z
    .locals 1

    .prologue
    .line 240
    invoke-static {p0}, Lcom/glympse/android/lib/ct;->isAddressRequired(I)Z

    move-result v0

    return v0
.end method

.method public static ticketStateEnumToString(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 245
    invoke-static {p0}, Lcom/glympse/android/lib/fe;->A(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static ticketStateStringToEnum(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 250
    invoke-static {p0}, Lcom/glympse/android/lib/fe;->q(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static wipeUsers(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 201
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/glympse/android/hal/HalFactory;->openDirectory(Landroid/content/Context;Ljava/lang/String;Z)Lcom/glympse/android/hal/GDirectory;

    move-result-object v0

    .line 202
    if-eqz v0, :cond_0

    .line 204
    invoke-static {v0, p1}, Lcom/glympse/android/lib/gf;->b(Lcom/glympse/android/hal/GDirectory;Ljava/lang/String;)V

    .line 206
    :cond_0
    return-void
.end method
