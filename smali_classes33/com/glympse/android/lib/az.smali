.class Lcom/glympse/android/lib/az;
.super Ljava/lang/Object;
.source "ExponentialBackOff.java"

# interfaces
.implements Lcom/glympse/android/lib/GBackOffPolicy;


# static fields
.field public static final kU:I = 0x3e8

.field public static final kV:I = 0xea60


# instance fields
.field protected kW:I

.field protected kX:D

.field protected kY:I

.field protected kZ:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/glympse/android/lib/az;->kW:I

    .line 58
    invoke-static {}, Lcom/glympse/android/lib/az;->an()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/glympse/android/lib/az;->kX:D

    .line 59
    invoke-virtual {p0}, Lcom/glympse/android/lib/az;->reset()V

    .line 60
    return-void
.end method

.method private static a(DI)I
    .locals 6

    .prologue
    .line 166
    invoke-static {}, Lcom/glympse/android/lib/az;->bk()D

    move-result-wide v0

    int-to-double v2, p2

    mul-double/2addr v0, v2

    .line 167
    int-to-double v2, p2

    sub-double/2addr v2, v0

    .line 168
    int-to-double v4, p2

    add-double/2addr v0, v4

    .line 172
    sub-double/2addr v0, v2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    add-double/2addr v0, v4

    mul-double/2addr v0, p0

    add-double/2addr v0, v2

    double-to-int v0, v0

    .line 173
    return v0
.end method

.method public static an()D
    .locals 2

    .prologue
    .line 38
    const-wide/high16 v0, 0x3ff8000000000000L    # 1.5

    return-wide v0
.end method

.method public static bk()D
    .locals 2

    .prologue
    .line 44
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    return-wide v0
.end method

.method private bl()V
    .locals 6

    .prologue
    .line 182
    iget v0, p0, Lcom/glympse/android/lib/az;->kY:I

    int-to-double v0, v0

    const-wide v2, 0x40ed4c0000000000L    # 60000.0

    iget-wide v4, p0, Lcom/glympse/android/lib/az;->kX:D

    div-double/2addr v2, v4

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_0

    .line 184
    const v0, 0xea60

    iput v0, p0, Lcom/glympse/android/lib/az;->kY:I

    .line 191
    :goto_0
    return-void

    .line 188
    :cond_0
    iget v0, p0, Lcom/glympse/android/lib/az;->kY:I

    int-to-double v0, v0

    iget-wide v2, p0, Lcom/glympse/android/lib/az;->kX:D

    mul-double/2addr v0, v2

    .line 189
    double-to-int v0, v0

    iput v0, p0, Lcom/glympse/android/lib/az;->kY:I

    goto :goto_0
.end method


# virtual methods
.method public getNextBackOffMillis()I
    .locals 3

    .prologue
    .line 100
    invoke-static {}, Lcom/glympse/android/hal/Platform;->random()D

    move-result-wide v0

    iget v2, p0, Lcom/glympse/android/lib/az;->kY:I

    invoke-static {v0, v1, v2}, Lcom/glympse/android/lib/az;->a(DI)I

    move-result v0

    .line 101
    invoke-direct {p0}, Lcom/glympse/android/lib/az;->bl()V

    .line 102
    return v0
.end method

.method public getOsConnectTimeout()I
    .locals 2

    .prologue
    .line 134
    iget v0, p0, Lcom/glympse/android/lib/az;->kY:I

    add-int/lit16 v0, v0, 0x7d0

    const/16 v1, 0x6d60

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/glympse/android/lib/az;->kZ:I

    .line 137
    iget v0, p0, Lcom/glympse/android/lib/az;->kZ:I

    return v0
.end method

.method public getOsReadTimeout()I
    .locals 1

    .prologue
    .line 145
    const/16 v0, 0x6d60

    return v0
.end method

.method public getPlatformTimeout()J
    .locals 2

    .prologue
    .line 154
    iget v0, p0, Lcom/glympse/android/lib/az;->kZ:I

    add-int/lit16 v0, v0, 0x6d60

    add-int/lit16 v0, v0, 0x7d0

    int-to-long v0, v0

    return-wide v0
.end method

.method public maxOutBackOffInterval()V
    .locals 1

    .prologue
    .line 110
    const v0, 0xea60

    iput v0, p0, Lcom/glympse/android/lib/az;->kY:I

    .line 111
    return-void
.end method

.method public r(I)V
    .locals 0

    .prologue
    .line 72
    iput p1, p0, Lcom/glympse/android/lib/az;->kW:I

    .line 73
    invoke-virtual {p0}, Lcom/glympse/android/lib/az;->reset()V

    .line 74
    return-void
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lcom/glympse/android/lib/az;->kW:I

    iput v0, p0, Lcom/glympse/android/lib/az;->kY:I

    .line 86
    return-void
.end method

.method public setMultiplier(D)V
    .locals 1

    .prologue
    .line 118
    iput-wide p1, p0, Lcom/glympse/android/lib/az;->kX:D

    .line 119
    return-void
.end method
