.class Lcom/glympse/android/lib/al$a;
.super Lcom/glympse/android/lib/co;
.source "DiagnosticsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/lib/al;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# direct methods
.method public constructor <init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/r;)V
    .locals 0

    .prologue
    .line 509
    invoke-direct {p0, p1, p2}, Lcom/glympse/android/lib/co;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/r;)V

    .line 510
    return-void
.end method


# virtual methods
.method public onComplete()V
    .locals 2

    .prologue
    .line 514
    invoke-super {p0}, Lcom/glympse/android/lib/co;->onComplete()V

    .line 516
    invoke-virtual {p0}, Lcom/glympse/android/lib/al$a;->isSucceeded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 520
    iget v0, p0, Lcom/glympse/android/lib/al$a;->_failures:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 522
    invoke-virtual {p0}, Lcom/glympse/android/lib/al$a;->abort()V

    .line 528
    iget-object v0, p0, Lcom/glympse/android/lib/al$a;->nI:Lcom/glympse/android/lib/r;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/glympse/android/lib/r;->a(Z)V

    .line 533
    :cond_0
    return-void
.end method
