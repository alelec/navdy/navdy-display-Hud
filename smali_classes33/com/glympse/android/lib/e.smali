.class abstract Lcom/glympse/android/lib/e;
.super Ljava/lang/Object;
.source "ApiEndpointBasic.java"

# interfaces
.implements Lcom/glympse/android/lib/GApiEndpoint;


# static fields
.field public static final gA:I = 0xfffffff

.field public static final gs:I = 0x0

.field public static final gt:I = 0x1

.field public static final gu:I = 0x2

.field public static final gv:I = 0x3

.field public static final gw:I = 0x4

.field public static final gx:I = 0xf4240

.field public static final gy:I = 0x64

.field public static final gz:I = 0x8


# instance fields
.field public gB:Lcom/glympse/android/lib/f;

.field public gC:J


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(ZLjava/lang/String;Lcom/glympse/android/lib/GApiEndpoint;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 43
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 44
    invoke-static {v0, p0, p1, p2}, Lcom/glympse/android/lib/e;->a(Ljava/lang/StringBuilder;ZLjava/lang/String;Lcom/glympse/android/lib/GApiEndpoint;)Z

    .line 45
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/StringBuilder;ZLjava/lang/String;Lcom/glympse/android/lib/GApiEndpoint;)Z
    .locals 3

    .prologue
    .line 51
    if-eqz p2, :cond_0

    .line 53
    if-eqz p1, :cond_2

    invoke-static {}, Lcom/glympse/android/lib/StaticConfig;->HTTPS()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    invoke-virtual {p0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 58
    :cond_0
    invoke-interface {p3, p0}, Lcom/glympse/android/lib/GApiEndpoint;->url(Ljava/lang/StringBuilder;)Z

    move-result v0

    .line 61
    invoke-static {}, Lcom/glympse/android/lib/Debug;->getLevel()I

    move-result v1

    const/4 v2, 0x2

    if-gt v1, v2, :cond_1

    .line 63
    if-eqz v0, :cond_3

    const/16 v0, 0x26

    :goto_1
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 64
    const-string v0, "warnings=true"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    const/4 v0, 0x1

    .line 68
    :cond_1
    return v0

    .line 53
    :cond_2
    invoke-static {}, Lcom/glympse/android/lib/StaticConfig;->HTTP()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 63
    :cond_3
    const/16 v0, 0x3f

    goto :goto_1
.end method


# virtual methods
.method public getCode()J
    .locals 2

    .prologue
    .line 128
    iget-wide v0, p0, Lcom/glympse/android/lib/e;->gC:J

    return-wide v0
.end method

.method public getError()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/glympse/android/lib/e;->gB:Lcom/glympse/android/lib/f;

    iget-object v0, v0, Lcom/glympse/android/lib/f;->gG:Ljava/lang/String;

    return-object v0
.end method

.method public getErrorDetail()Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/glympse/android/lib/e;->gB:Lcom/glympse/android/lib/f;

    iget-object v0, v0, Lcom/glympse/android/lib/f;->gH:Ljava/lang/String;

    return-object v0
.end method

.method public getHandler(Lcom/glympse/android/lib/json/GJsonHandlerStack;)Lcom/glympse/android/lib/json/GJsonHandler;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/glympse/android/lib/e;->gB:Lcom/glympse/android/lib/f;

    iput-object p1, v0, Lcom/glympse/android/lib/f;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    .line 96
    iget-object v0, p0, Lcom/glympse/android/lib/e;->gB:Lcom/glympse/android/lib/f;

    return-object v0
.end method

.method public getResult()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/glympse/android/lib/e;->gB:Lcom/glympse/android/lib/f;

    iget-object v0, v0, Lcom/glympse/android/lib/f;->gF:Ljava/lang/String;

    return-object v0
.end method

.method public getTime()J
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lcom/glympse/android/lib/e;->gB:Lcom/glympse/android/lib/f;

    iget-wide v0, v0, Lcom/glympse/android/lib/f;->_time:J

    return-wide v0
.end method

.method public methodType()I
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x0

    return v0
.end method

.method public post()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x0

    return-object v0
.end method

.method public setCode(J)V
    .locals 1

    .prologue
    .line 123
    iput-wide p1, p0, Lcom/glympse/android/lib/e;->gC:J

    .line 124
    return-void
.end method

.method public shouldAdd(Lcom/glympse/android/lib/GApiEndpoint;)I
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x0

    return v0
.end method

.method public shouldRetry(ZI)Z
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x1

    return v0
.end method

.method public userAgent()Z
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    return v0
.end method
