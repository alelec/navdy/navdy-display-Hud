.class Lcom/glympse/android/lib/bb;
.super Ljava/lang/Object;
.source "FavoritesManager.java"

# interfaces
.implements Lcom/glympse/android/lib/GFavoritesManagerPrivate;


# instance fields
.field private _glympse:Lcom/glympse/android/lib/GGlympsePrivate;

.field private he:Lcom/glympse/android/lib/CommonSink;

.field private iu:Lcom/glympse/android/hal/GContextHolder;

.field private iv:Ljava/lang/String;

.field private jZ:Lcom/glympse/android/lib/fb;

.field private le:Lcom/glympse/android/lib/fb;

.field private lf:Lcom/glympse/android/api/GTicket;

.field private lg:Lcom/glympse/android/hal/GVector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GVector",
            "<",
            "Lcom/glympse/android/api/GTicket;",
            ">;"
        }
    .end annotation
.end field

.field private lh:Lcom/glympse/android/core/GPrimitive;

.field private li:Ljava/lang/String;

.field private lj:Lcom/glympse/android/core/GPrimitive;

.field private lk:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    new-instance v0, Lcom/glympse/android/lib/fb;

    invoke-direct {v0}, Lcom/glympse/android/lib/fb;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/bb;->jZ:Lcom/glympse/android/lib/fb;

    .line 76
    new-instance v0, Lcom/glympse/android/lib/fb;

    invoke-direct {v0}, Lcom/glympse/android/lib/fb;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/bb;->le:Lcom/glympse/android/lib/fb;

    .line 77
    new-instance v0, Lcom/glympse/android/lib/CommonSink;

    const-string v1, "FavoritesManager"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/CommonSink;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/glympse/android/lib/bb;->he:Lcom/glympse/android/lib/CommonSink;

    .line 78
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/glympse/android/lib/bb;->lk:J

    .line 79
    return-void
.end method

.method private a(JI)Lcom/glympse/android/core/GPrimitive;
    .locals 7

    .prologue
    .line 748
    new-instance v0, Lcom/glympse/android/lib/Primitive;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 749
    const-string v1, "action"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "move"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 750
    const-string v1, "tag"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1, p2}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 751
    const-string v1, "location"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    int-to-long v2, p3

    invoke-interface {v0, v1, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 752
    const-string v1, "op_index"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-wide v2, p0, Lcom/glympse/android/lib/bb;->lk:J

    const-wide/16 v4, 0x1

    add-long/2addr v4, v2

    iput-wide v4, p0, Lcom/glympse/android/lib/bb;->lk:J

    invoke-interface {v0, v1, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 753
    return-object v0
.end method

.method private a(JLcom/glympse/android/core/GPrimitive;)Lcom/glympse/android/core/GPrimitive;
    .locals 7

    .prologue
    .line 728
    new-instance v0, Lcom/glympse/android/lib/Primitive;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 729
    const-string v1, "action"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "add"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 730
    const-string v1, "tag"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1, p2}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 731
    const-string v1, "ticket"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 732
    const-string v1, "op_index"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-wide v2, p0, Lcom/glympse/android/lib/bb;->lk:J

    const-wide/16 v4, 0x1

    add-long/2addr v4, v2

    iput-wide v4, p0, Lcom/glympse/android/lib/bb;->lk:J

    invoke-interface {v0, v1, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 733
    return-object v0
.end method

.method private static a(Lcom/glympse/android/core/GPrimitive;Lcom/glympse/android/core/GPrimitive;)V
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 611
    const-string v1, "action"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 612
    const-string v2, "favorites"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p0, v2}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v2

    .line 613
    const-string v3, "add"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 615
    const-string v1, "ticket"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 616
    const-string v3, "tag"

    invoke-static {v3}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 617
    invoke-interface {p1, v1}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v1

    .line 618
    invoke-interface {p1, v3}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-interface {v1, v3, v4, v5}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 619
    invoke-interface {v2, v0, v1}, Lcom/glympse/android/core/GPrimitive;->insert(ILcom/glympse/android/core/GPrimitive;)V

    .line 652
    :cond_0
    :goto_0
    return-void

    .line 621
    :cond_1
    const-string v3, "remove"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 623
    const-string v1, "tag"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 624
    invoke-interface {p1, v1}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 625
    invoke-interface {v2}, Lcom/glympse/android/core/GPrimitive;->size()I

    move-result v3

    .line 626
    :goto_1
    if-ge v0, v3, :cond_0

    .line 628
    invoke-interface {v2, v0}, Lcom/glympse/android/core/GPrimitive;->get(I)Lcom/glympse/android/core/GPrimitive;

    move-result-object v6

    invoke-interface {v6, v1}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    cmp-long v6, v4, v6

    if-nez v6, :cond_2

    .line 630
    invoke-interface {v2, v0}, Lcom/glympse/android/core/GPrimitive;->remove(I)V

    goto :goto_0

    .line 626
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 635
    :cond_3
    const-string v3, "move"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 637
    const-string v1, "tag"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 638
    invoke-interface {p1, v1}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 639
    const-string v3, "location"

    invoke-static {v3}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 640
    invoke-interface {v2}, Lcom/glympse/android/core/GPrimitive;->size()I

    move-result v6

    .line 641
    :goto_2
    if-ge v0, v6, :cond_0

    .line 643
    invoke-interface {v2, v0}, Lcom/glympse/android/core/GPrimitive;->get(I)Lcom/glympse/android/core/GPrimitive;

    move-result-object v7

    .line 644
    invoke-interface {v7, v1}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    cmp-long v8, v4, v8

    if-nez v8, :cond_4

    .line 646
    invoke-interface {v2, v0}, Lcom/glympse/android/core/GPrimitive;->remove(I)V

    .line 647
    invoke-interface {p1, v3}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    long-to-int v0, v0

    invoke-interface {v2, v0, v7}, Lcom/glympse/android/core/GPrimitive;->insert(ILcom/glympse/android/core/GPrimitive;)V

    goto :goto_0

    .line 641
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method private a(Lcom/glympse/android/core/GPrimitive;Ljava/lang/String;J)V
    .locals 21

    .prologue
    .line 656
    const-string v2, "tag"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 657
    const-string v2, "next_tag"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 658
    const-string v2, "op_index"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 659
    const-string v2, "action"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 662
    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 668
    const-wide/16 v4, -0x1

    .line 670
    new-instance v15, Lcom/glympse/android/lib/Primitive;

    const/4 v2, 0x1

    invoke-direct {v15, v2}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 671
    const/4 v2, 0x0

    move-wide v6, v8

    :goto_0
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/glympse/android/lib/bb;->lj:Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v10}, Lcom/glympse/android/core/GPrimitive;->size()I

    move-result v10

    if-ge v2, v10, :cond_4

    .line 673
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/glympse/android/lib/bb;->lj:Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v10, v2}, Lcom/glympse/android/core/GPrimitive;->get(I)Lcom/glympse/android/core/GPrimitive;

    move-result-object v16

    .line 674
    move-object/from16 v0, v16

    invoke-interface {v0, v13}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    .line 676
    cmp-long v10, v10, p3

    if-lez v10, :cond_3

    .line 678
    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    .line 679
    const-wide/16 v18, 0x0

    cmp-long v17, v4, v18

    if-gez v17, :cond_0

    .line 683
    move-object/from16 v0, v16

    invoke-interface {v0, v14}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 684
    const-string v18, "add"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_0

    move-wide v4, v10

    .line 690
    :cond_0
    const-wide/16 v18, 0x0

    cmp-long v17, v4, v18

    if-ltz v17, :cond_2

    cmp-long v17, v10, v4

    if-ltz v17, :cond_2

    .line 692
    sub-long/2addr v10, v4

    add-long/2addr v10, v8

    .line 693
    cmp-long v17, v10, v6

    if-ltz v17, :cond_1

    .line 695
    const-wide/16 v6, 0x1

    add-long/2addr v6, v10

    .line 699
    :cond_1
    move-object/from16 v0, v16

    invoke-interface {v0, v3, v10, v11}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 701
    :cond_2
    invoke-interface/range {v15 .. v16}, Lcom/glympse/android/core/GPrimitive;->put(Lcom/glympse/android/core/GPrimitive;)V

    .line 704
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/glympse/android/lib/bb;->a(Lcom/glympse/android/core/GPrimitive;Lcom/glympse/android/core/GPrimitive;)V

    .line 671
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 707
    :cond_4
    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/glympse/android/lib/bb;->lj:Lcom/glympse/android/core/GPrimitive;

    .line 709
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/glympse/android/lib/bb;->lh:Lcom/glympse/android/core/GPrimitive;

    .line 710
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/glympse/android/lib/bb;->lh:Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v2, v12, v6, v7}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 711
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/glympse/android/lib/bb;->li:Ljava/lang/String;

    .line 712
    return-void
.end method

.method public static b(Lcom/glympse/android/hal/GDirectory;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 83
    const-string v0, "favorites_synced_v2"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/glympse/android/lib/fb;->e(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/glympse/android/hal/GDirectory;->deleteFile(Ljava/lang/String;)Z

    .line 84
    return-void
.end method

.method private bn()V
    .locals 4

    .prologue
    .line 354
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->jZ:Lcom/glympse/android/lib/fb;

    invoke-virtual {v0}, Lcom/glympse/android/lib/fb;->cP()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 356
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->le:Lcom/glympse/android/lib/fb;

    invoke-virtual {v0}, Lcom/glympse/android/lib/fb;->cP()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 358
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->le:Lcom/glympse/android/lib/fb;

    invoke-virtual {v0}, Lcom/glympse/android/lib/fb;->remove()V

    .line 362
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->jZ:Lcom/glympse/android/lib/fb;

    invoke-virtual {v0}, Lcom/glympse/android/lib/fb;->load()Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 363
    const-string v1, "blob"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v1

    iput-object v1, p0, Lcom/glympse/android/lib/bb;->lh:Lcom/glympse/android/core/GPrimitive;

    .line 364
    const-string v1, "blob_version"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/glympse/android/lib/bb;->li:Ljava/lang/String;

    .line 365
    const-string v1, "operations"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v1

    iput-object v1, p0, Lcom/glympse/android/lib/bb;->lj:Lcom/glympse/android/core/GPrimitive;

    .line 366
    const-string v1, "next_operation"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/glympse/android/lib/bb;->lk:J

    .line 367
    const-string v1, "draft"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 368
    if-eqz v0, :cond_1

    .line 370
    invoke-static {v0}, Lcom/glympse/android/lib/bb;->g(Lcom/glympse/android/core/GPrimitive;)Lcom/glympse/android/lib/GTicketPrivate;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/bb;->lf:Lcom/glympse/android/api/GTicket;

    .line 382
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/glympse/android/lib/bb;->br()V

    .line 383
    return-void

    .line 375
    :cond_2
    new-instance v0, Lcom/glympse/android/lib/Primitive;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    iput-object v0, p0, Lcom/glympse/android/lib/bb;->lj:Lcom/glympse/android/core/GPrimitive;

    .line 376
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->le:Lcom/glympse/android/lib/fb;

    invoke-virtual {v0}, Lcom/glympse/android/lib/fb;->cP()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 378
    invoke-direct {p0}, Lcom/glympse/android/lib/bb;->bo()V

    .line 379
    invoke-direct {p0}, Lcom/glympse/android/lib/bb;->save()V

    goto :goto_0
.end method

.method private bo()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 395
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->le:Lcom/glympse/android/lib/fb;

    invoke-virtual {v0}, Lcom/glympse/android/lib/fb;->load()Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 396
    if-nez v0, :cond_1

    .line 434
    :cond_0
    :goto_0
    return-void

    .line 403
    :cond_1
    const-string v1, "dr"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v1

    .line 404
    if-eqz v1, :cond_2

    .line 406
    new-instance v2, Lcom/glympse/android/lib/fe;

    invoke-direct {v2, v6}, Lcom/glympse/android/lib/fe;-><init>(Z)V

    .line 407
    invoke-interface {v2, v1}, Lcom/glympse/android/lib/GTicketPrivate;->decode(Lcom/glympse/android/core/GPrimitive;)V

    .line 408
    iput-object v2, p0, Lcom/glympse/android/lib/bb;->lf:Lcom/glympse/android/api/GTicket;

    .line 412
    :cond_2
    const-string v1, "favs"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v1

    .line 413
    if-eqz v1, :cond_0

    .line 415
    invoke-direct {p0}, Lcom/glympse/android/lib/bb;->bp()V

    .line 416
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->lh:Lcom/glympse/android/core/GPrimitive;

    const-string v2, "next_tag"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 417
    invoke-interface {v1}, Lcom/glympse/android/core/GPrimitive;->size()I

    move-result v0

    .line 419
    add-int/lit8 v0, v0, -0x1

    :goto_1
    if-ltz v0, :cond_3

    .line 421
    invoke-interface {v1, v0}, Lcom/glympse/android/core/GPrimitive;->get(I)Lcom/glympse/android/core/GPrimitive;

    move-result-object v4

    .line 422
    new-instance v5, Lcom/glympse/android/lib/fe;

    invoke-direct {v5, v6}, Lcom/glympse/android/lib/fe;-><init>(Z)V

    .line 423
    invoke-interface {v5, v4}, Lcom/glympse/android/lib/GTicketPrivate;->decode(Lcom/glympse/android/core/GPrimitive;)V

    .line 425
    invoke-static {v5}, Lcom/glympse/android/lib/bb;->g(Lcom/glympse/android/lib/GTicketPrivate;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v4

    .line 426
    invoke-direct {p0, v2, v3, v4}, Lcom/glympse/android/lib/bb;->a(JLcom/glympse/android/core/GPrimitive;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v4

    .line 427
    iget-object v5, p0, Lcom/glympse/android/lib/bb;->lj:Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v5, v4}, Lcom/glympse/android/core/GPrimitive;->put(Lcom/glympse/android/core/GPrimitive;)V

    .line 428
    iget-object v5, p0, Lcom/glympse/android/lib/bb;->lh:Lcom/glympse/android/core/GPrimitive;

    invoke-static {v5, v4}, Lcom/glympse/android/lib/bb;->a(Lcom/glympse/android/core/GPrimitive;Lcom/glympse/android/core/GPrimitive;)V

    .line 429
    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    .line 419
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 431
    :cond_3
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->lh:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "next_tag"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 432
    invoke-direct {p0}, Lcom/glympse/android/lib/bb;->br()V

    goto :goto_0
.end method

.method private bp()V
    .locals 4

    .prologue
    .line 561
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->lh:Lcom/glympse/android/core/GPrimitive;

    if-nez v0, :cond_0

    .line 563
    new-instance v0, Lcom/glympse/android/lib/Primitive;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    iput-object v0, p0, Lcom/glympse/android/lib/bb;->lh:Lcom/glympse/android/core/GPrimitive;

    .line 565
    :cond_0
    const-string v0, "favorites"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 566
    iget-object v1, p0, Lcom/glympse/android/lib/bb;->lh:Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v1, v0}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 568
    new-instance v1, Lcom/glympse/android/lib/Primitive;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 569
    iget-object v2, p0, Lcom/glympse/android/lib/bb;->lh:Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v2, v0, v1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 571
    :cond_1
    const-string v0, "next_tag"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 572
    iget-object v1, p0, Lcom/glympse/android/lib/bb;->lh:Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v1, v0}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 574
    iget-object v1, p0, Lcom/glympse/android/lib/bb;->lh:Lcom/glympse/android/core/GPrimitive;

    const-wide/16 v2, 0x1

    invoke-interface {v1, v0, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 576
    :cond_2
    return-void
.end method

.method private bq()V
    .locals 4

    .prologue
    .line 580
    new-instance v0, Lcom/glympse/android/hal/GVector;

    invoke-direct {v0}, Lcom/glympse/android/hal/GVector;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/bb;->lg:Lcom/glympse/android/hal/GVector;

    .line 581
    invoke-direct {p0}, Lcom/glympse/android/lib/bb;->bp()V

    .line 583
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->lh:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "favorites"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v1

    .line 584
    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1}, Lcom/glympse/android/core/GPrimitive;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 586
    invoke-interface {v1, v0}, Lcom/glympse/android/core/GPrimitive;->get(I)Lcom/glympse/android/core/GPrimitive;

    move-result-object v2

    .line 587
    invoke-static {v2}, Lcom/glympse/android/lib/bb;->g(Lcom/glympse/android/core/GPrimitive;)Lcom/glympse/android/lib/GTicketPrivate;

    move-result-object v2

    .line 588
    iget-object v3, p0, Lcom/glympse/android/lib/bb;->lg:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v3, v2}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 584
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 590
    :cond_0
    return-void
.end method

.method private br()V
    .locals 7

    .prologue
    .line 598
    invoke-direct {p0}, Lcom/glympse/android/lib/bb;->bq()V

    .line 600
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->lj:Lcom/glympse/android/core/GPrimitive;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/bb;->lj:Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v0}, Lcom/glympse/android/core/GPrimitive;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 607
    :cond_0
    :goto_0
    return-void

    .line 605
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->lj:Lcom/glympse/android/core/GPrimitive;

    iget-object v1, p0, Lcom/glympse/android/lib/bb;->lj:Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v1}, Lcom/glympse/android/core/GPrimitive;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->get(I)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    const-string v1, "op_index"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 606
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getServerPost()Lcom/glympse/android/lib/GServerPost;

    move-result-object v6

    new-instance v0, Lcom/glympse/android/lib/fy;

    iget-object v1, p0, Lcom/glympse/android/lib/bb;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget-object v2, p0, Lcom/glympse/android/lib/bb;->lh:Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v2}, Lcom/glympse/android/core/GPrimitive;->clone()Lcom/glympse/android/core/GPrimitive;

    move-result-object v2

    iget-object v3, p0, Lcom/glympse/android/lib/bb;->li:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/glympse/android/lib/fy;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/core/GPrimitive;Ljava/lang/String;J)V

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-interface {v6, v0, v1, v2}, Lcom/glympse/android/lib/GServerPost;->invokeEndpoint(Lcom/glympse/android/lib/GApiEndpoint;ZZ)V

    goto :goto_0
.end method

.method private bs()V
    .locals 5

    .prologue
    .line 759
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_0

    .line 761
    iget-object v1, p0, Lcom/glympse/android/lib/bb;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GEventListener;

    const/16 v2, 0x12

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {v1, v0, v2, v3, v4}, Lcom/glympse/android/lib/ax;->a(Lcom/glympse/android/api/GGlympse;Lcom/glympse/android/api/GEventListener;IILcom/glympse/android/core/GCommon;)V

    .line 763
    :cond_0
    return-void
.end method

.method private f(J)Lcom/glympse/android/core/GPrimitive;
    .locals 7

    .prologue
    .line 738
    new-instance v0, Lcom/glympse/android/lib/Primitive;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 739
    const-string v1, "action"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "remove"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 740
    const-string v1, "tag"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1, p2}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 741
    const-string v1, "op_index"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-wide v2, p0, Lcom/glympse/android/lib/bb;->lk:J

    const-wide/16 v4, 0x1

    add-long/2addr v4, v2

    iput-wide v4, p0, Lcom/glympse/android/lib/bb;->lk:J

    invoke-interface {v0, v1, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 742
    return-object v0
.end method

.method private f(Lcom/glympse/android/core/GPrimitive;)V
    .locals 1

    .prologue
    .line 387
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->lj:Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v0, p1}, Lcom/glympse/android/core/GPrimitive;->put(Lcom/glympse/android/core/GPrimitive;)V

    .line 388
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->lh:Lcom/glympse/android/core/GPrimitive;

    invoke-static {v0, p1}, Lcom/glympse/android/lib/bb;->a(Lcom/glympse/android/core/GPrimitive;Lcom/glympse/android/core/GPrimitive;)V

    .line 389
    invoke-direct {p0}, Lcom/glympse/android/lib/bb;->br()V

    .line 390
    invoke-direct {p0}, Lcom/glympse/android/lib/bb;->save()V

    .line 391
    return-void
.end method

.method private static g(Lcom/glympse/android/lib/GTicketPrivate;)Lcom/glympse/android/core/GPrimitive;
    .locals 15

    .prologue
    .line 438
    const-string v0, "name"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 439
    new-instance v3, Lcom/glympse/android/lib/Primitive;

    const/4 v0, 0x2

    invoke-direct {v3, v0}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 440
    const-string v0, "duration"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0}, Lcom/glympse/android/lib/GTicketPrivate;->getDuration()I

    move-result v1

    int-to-long v4, v1

    invoke-interface {v3, v0, v4, v5}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 441
    invoke-interface {p0}, Lcom/glympse/android/lib/GTicketPrivate;->getMessage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 443
    const-string v0, "message"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0}, Lcom/glympse/android/lib/GTicketPrivate;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v0, v1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    :cond_0
    invoke-interface {p0}, Lcom/glympse/android/lib/GTicketPrivate;->getName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 447
    invoke-interface {p0}, Lcom/glympse/android/lib/GTicketPrivate;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v2, v0}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 450
    :cond_1
    invoke-interface {p0}, Lcom/glympse/android/lib/GTicketPrivate;->getDestination()Lcom/glympse/android/api/GPlace;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GPlacePrivate;

    .line 451
    if-eqz v0, :cond_3

    .line 453
    new-instance v1, Lcom/glympse/android/lib/Primitive;

    const/4 v4, 0x2

    invoke-direct {v1, v4}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 454
    const-string v4, "lat"

    invoke-static {v4}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0}, Lcom/glympse/android/lib/GPlacePrivate;->getLatitude()D

    move-result-wide v6

    invoke-interface {v1, v4, v6, v7}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;D)V

    .line 455
    const-string v4, "lng"

    invoke-static {v4}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0}, Lcom/glympse/android/lib/GPlacePrivate;->getLongitude()D

    move-result-wide v6

    invoke-interface {v1, v4, v6, v7}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;D)V

    .line 456
    invoke-interface {v0}, Lcom/glympse/android/lib/GPlacePrivate;->getName()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 458
    invoke-interface {v0}, Lcom/glympse/android/lib/GPlacePrivate;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    :cond_2
    const-string v0, "destination"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0, v1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 462
    :cond_3
    invoke-interface {p0}, Lcom/glympse/android/lib/GTicketPrivate;->getInvites()Lcom/glympse/android/core/GArray;

    move-result-object v4

    .line 463
    invoke-interface {v4}, Lcom/glympse/android/core/GArray;->length()I

    move-result v5

    .line 464
    if-lez v5, :cond_a

    .line 466
    const-string v0, "type"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 467
    const-string v0, "subtype"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 468
    const-string v0, "address"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 469
    const-string v0, "visibility"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 470
    const-string v0, "reference"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 471
    const-string v0, "request"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 473
    new-instance v12, Lcom/glympse/android/lib/Primitive;

    const/4 v0, 0x1

    invoke-direct {v12, v0}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 474
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_9

    .line 476
    invoke-interface {v4, v1}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GInvitePrivate;

    .line 477
    new-instance v13, Lcom/glympse/android/lib/Primitive;

    const/4 v14, 0x2

    invoke-direct {v13, v14}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 478
    invoke-interface {v0}, Lcom/glympse/android/lib/GInvitePrivate;->getType()I

    move-result v14

    invoke-static {v14}, Lcom/glympse/android/lib/ct;->v(I)Ljava/lang/String;

    move-result-object v14

    invoke-interface {v13, v6, v14}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    invoke-interface {v0}, Lcom/glympse/android/lib/GInvitePrivate;->getSubtype()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_4

    .line 481
    invoke-interface {v0}, Lcom/glympse/android/lib/GInvitePrivate;->getSubtype()Ljava/lang/String;

    move-result-object v14

    invoke-interface {v13, v7, v14}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    :cond_4
    invoke-interface {v0}, Lcom/glympse/android/lib/GInvitePrivate;->getName()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_5

    .line 485
    invoke-interface {v0}, Lcom/glympse/android/lib/GInvitePrivate;->getName()Ljava/lang/String;

    move-result-object v14

    invoke-interface {v13, v2, v14}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 487
    :cond_5
    invoke-interface {v0}, Lcom/glympse/android/lib/GInvitePrivate;->getAddress()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_6

    .line 489
    invoke-interface {v0}, Lcom/glympse/android/lib/GInvitePrivate;->getAddress()Ljava/lang/String;

    move-result-object v14

    invoke-interface {v13, v8, v14}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    :cond_6
    invoke-interface {v0}, Lcom/glympse/android/lib/GInvitePrivate;->isVisible()Z

    move-result v14

    invoke-interface {v13, v9, v14}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Z)V

    .line 492
    invoke-interface {v0}, Lcom/glympse/android/lib/GInvitePrivate;->getReference()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_7

    .line 494
    invoke-interface {v0}, Lcom/glympse/android/lib/GInvitePrivate;->getReference()Ljava/lang/String;

    move-result-object v14

    invoke-interface {v13, v10, v14}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 496
    :cond_7
    invoke-interface {v0}, Lcom/glympse/android/lib/GInvitePrivate;->getRequestTicket()Lcom/glympse/android/api/GTicket;

    move-result-object v14

    if-eqz v14, :cond_8

    .line 498
    invoke-interface {v0}, Lcom/glympse/android/lib/GInvitePrivate;->getRequestTicket()Lcom/glympse/android/api/GTicket;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GTicketPrivate;

    invoke-static {v0}, Lcom/glympse/android/lib/bb;->g(Lcom/glympse/android/lib/GTicketPrivate;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 499
    invoke-interface {v13, v11, v0}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 501
    :cond_8
    invoke-interface {v12, v13}, Lcom/glympse/android/core/GPrimitive;->put(Lcom/glympse/android/core/GPrimitive;)V

    .line 474
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 503
    :cond_9
    const-string v0, "invites"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0, v12}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 505
    :cond_a
    return-object v3
.end method

.method private static g(Lcom/glympse/android/core/GPrimitive;)Lcom/glympse/android/lib/GTicketPrivate;
    .locals 14

    .prologue
    const/4 v0, 0x0

    .line 510
    const-string v1, "name"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 512
    new-instance v8, Lcom/glympse/android/lib/fe;

    invoke-direct {v8, v0}, Lcom/glympse/android/lib/fe;-><init>(Z)V

    .line 513
    const-string v1, "duration"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v1}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    long-to-int v1, v2

    invoke-interface {v8, v1}, Lcom/glympse/android/lib/GTicketPrivate;->setDuration(I)V

    .line 514
    const-string v1, "message"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v1}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v8, v1}, Lcom/glympse/android/lib/GTicketPrivate;->setMessage(Ljava/lang/String;)V

    .line 515
    invoke-interface {p0, v7}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v8, v1}, Lcom/glympse/android/lib/GTicketPrivate;->setName(Ljava/lang/String;)V

    .line 516
    const-string v1, "destination"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v1}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v6

    .line 517
    if-eqz v6, :cond_0

    .line 519
    new-instance v1, Lcom/glympse/android/lib/eg;

    const-string v2, "lat"

    .line 520
    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v6, v2}, Lcom/glympse/android/core/GPrimitive;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    const-string v4, "lng"

    .line 521
    invoke-static {v4}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v6, v4}, Lcom/glympse/android/core/GPrimitive;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 522
    invoke-interface {v6, v7}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Lcom/glympse/android/lib/eg;-><init>(DDLjava/lang/String;)V

    .line 523
    invoke-interface {v8, v1}, Lcom/glympse/android/lib/GTicketPrivate;->setDestination(Lcom/glympse/android/api/GPlace;)V

    .line 526
    :cond_0
    const-string v1, "invites"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v1}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v1

    .line 527
    if-eqz v1, :cond_2

    .line 529
    const-string v2, "type"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 530
    const-string v3, "subtype"

    invoke-static {v3}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 531
    const-string v4, "address"

    invoke-static {v4}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 532
    const-string v5, "visibility"

    invoke-static {v5}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 533
    const-string v6, "reference"

    invoke-static {v6}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 534
    const-string v9, "request"

    invoke-static {v9}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 536
    invoke-interface {v1}, Lcom/glympse/android/core/GPrimitive;->size()I

    move-result v10

    .line 537
    :goto_0
    if-ge v0, v10, :cond_2

    .line 539
    invoke-interface {v1, v0}, Lcom/glympse/android/core/GPrimitive;->get(I)Lcom/glympse/android/core/GPrimitive;

    move-result-object v11

    .line 540
    new-instance v12, Lcom/glympse/android/lib/ct;

    invoke-direct {v12}, Lcom/glympse/android/lib/ct;-><init>()V

    .line 541
    invoke-interface {v11, v2}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/glympse/android/lib/ct;->p(Ljava/lang/String;)I

    move-result v13

    invoke-interface {v12, v13}, Lcom/glympse/android/lib/GInvitePrivate;->setType(I)V

    .line 542
    invoke-interface {v11, v3}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13}, Lcom/glympse/android/lib/GInvitePrivate;->setSubtype(Ljava/lang/String;)V

    .line 543
    invoke-interface {v11, v7}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13}, Lcom/glympse/android/lib/GInvitePrivate;->setName(Ljava/lang/String;)V

    .line 544
    invoke-interface {v11, v4}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13}, Lcom/glympse/android/lib/GInvitePrivate;->setAddress(Ljava/lang/String;)V

    .line 545
    invoke-interface {v11, v5}, Lcom/glympse/android/core/GPrimitive;->getBool(Ljava/lang/String;)Z

    move-result v13

    invoke-interface {v12, v13}, Lcom/glympse/android/lib/GInvitePrivate;->setVisible(Z)V

    .line 546
    invoke-interface {v11, v6}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13}, Lcom/glympse/android/lib/GInvitePrivate;->setReference(Ljava/lang/String;)V

    .line 548
    invoke-interface {v11, v9}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v11

    .line 549
    if-eqz v11, :cond_1

    .line 551
    invoke-static {v11}, Lcom/glympse/android/lib/bb;->g(Lcom/glympse/android/core/GPrimitive;)Lcom/glympse/android/lib/GTicketPrivate;

    move-result-object v11

    invoke-interface {v12, v11}, Lcom/glympse/android/lib/GInvitePrivate;->setRequestTicket(Lcom/glympse/android/api/GTicket;)V

    .line 553
    :cond_1
    invoke-interface {v8, v12}, Lcom/glympse/android/lib/GTicketPrivate;->addInviteCore(Lcom/glympse/android/api/GInvite;)V

    .line 537
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 556
    :cond_2
    return-object v8
.end method

.method private s(I)Z
    .locals 4

    .prologue
    .line 318
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/bb;->t(I)J

    move-result-wide v0

    .line 319
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 321
    const/4 v0, 0x0

    .line 324
    :goto_0
    return v0

    .line 323
    :cond_0
    invoke-direct {p0, v0, v1}, Lcom/glympse/android/lib/bb;->f(J)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/bb;->f(Lcom/glympse/android/core/GPrimitive;)V

    .line 324
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private save()V
    .locals 4

    .prologue
    .line 329
    new-instance v1, Lcom/glympse/android/lib/Primitive;

    const/4 v0, 0x2

    invoke-direct {v1, v0}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 332
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->lh:Lcom/glympse/android/core/GPrimitive;

    if-eqz v0, :cond_0

    .line 334
    const-string v0, "blob"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/glympse/android/lib/bb;->lh:Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v1, v0, v2}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 335
    const-string v0, "blob_version"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/glympse/android/lib/bb;->li:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->lj:Lcom/glympse/android/core/GPrimitive;

    if-eqz v0, :cond_1

    .line 339
    const-string v0, "operations"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/glympse/android/lib/bb;->lj:Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v1, v0, v2}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 340
    const-string v0, "next_operation"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lcom/glympse/android/lib/bb;->lk:J

    invoke-interface {v1, v0, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 344
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->lf:Lcom/glympse/android/api/GTicket;

    if-eqz v0, :cond_2

    .line 346
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->lf:Lcom/glympse/android/api/GTicket;

    check-cast v0, Lcom/glympse/android/lib/GTicketPrivate;

    invoke-static {v0}, Lcom/glympse/android/lib/bb;->g(Lcom/glympse/android/lib/GTicketPrivate;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 347
    const-string v2, "draft"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 349
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->jZ:Lcom/glympse/android/lib/fb;

    invoke-virtual {v0, v1}, Lcom/glympse/android/lib/fb;->save(Lcom/glympse/android/core/GPrimitive;)V

    .line 350
    return-void
.end method

.method private t(I)J
    .locals 3

    .prologue
    .line 716
    const-string v0, "tag"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 717
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->lh:Lcom/glympse/android/core/GPrimitive;

    const-string v2, "favorites"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 718
    if-eqz v0, :cond_1

    invoke-interface {v0, p1}, Lcom/glympse/android/core/GPrimitive;->get(I)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 719
    :goto_0
    if-eqz v0, :cond_0

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 721
    :cond_0
    const-wide/16 v0, -0x1

    .line 723
    :goto_1
    return-wide v0

    .line 718
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 723
    :cond_2
    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_1
.end method


# virtual methods
.method public addFavorite(Lcom/glympse/android/api/GTicket;)V
    .locals 6

    .prologue
    .line 120
    if-nez p1, :cond_0

    .line 138
    :goto_0
    return-void

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->lg:Lcom/glympse/android/hal/GVector;

    if-nez v0, :cond_1

    .line 126
    invoke-direct {p0}, Lcom/glympse/android/lib/bb;->bn()V

    .line 128
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->lh:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "next_tag"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 129
    iget-object v2, p0, Lcom/glympse/android/lib/bb;->lh:Lcom/glympse/android/core/GPrimitive;

    const-string v3, "next_tag"

    invoke-static {v3}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-wide/16 v4, 0x1

    add-long/2addr v4, v0

    invoke-interface {v2, v3, v4, v5}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 131
    check-cast p1, Lcom/glympse/android/lib/GTicketPrivate;

    .line 132
    invoke-static {p1}, Lcom/glympse/android/lib/bb;->g(Lcom/glympse/android/lib/GTicketPrivate;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v2

    .line 134
    invoke-direct {p0, v0, v1, v2}, Lcom/glympse/android/lib/bb;->a(JLcom/glympse/android/core/GPrimitive;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/bb;->f(Lcom/glympse/android/core/GPrimitive;)V

    .line 137
    invoke-direct {p0}, Lcom/glympse/android/lib/bb;->bs()V

    goto :goto_0
.end method

.method public addListener(Lcom/glympse/android/api/GEventListener;)Z
    .locals 1

    .prologue
    .line 771
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1}, Lcom/glympse/android/lib/CommonSink;->addListener(Lcom/glympse/android/api/GEventListener;)Z

    move-result v0

    return v0
.end method

.method public associateContext(JLjava/lang/Object;)V
    .locals 1

    .prologue
    .line 796
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1, p2, p3}, Lcom/glympse/android/lib/CommonSink;->associateContext(JLjava/lang/Object;)V

    .line 797
    return-void
.end method

.method public checkFavoritesVersion(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 281
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->lg:Lcom/glympse/android/hal/GVector;

    if-nez v0, :cond_0

    .line 283
    invoke-direct {p0}, Lcom/glympse/android/lib/bb;->bn()V

    .line 286
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->li:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/glympse/android/hal/Helpers;->safeEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 288
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getServerPost()Lcom/glympse/android/lib/GServerPost;

    move-result-object v0

    new-instance v1, Lcom/glympse/android/lib/bp;

    iget-object v2, p0, Lcom/glympse/android/lib/bb;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct {v1, v2}, Lcom/glympse/android/lib/bp;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;)V

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-interface {v0, v1, v2, v3}, Lcom/glympse/android/lib/GServerPost;->invokeEndpoint(Lcom/glympse/android/lib/GApiEndpoint;ZZ)V

    .line 290
    :cond_1
    return-void
.end method

.method public clearContext(J)V
    .locals 1

    .prologue
    .line 806
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1, p2}, Lcom/glympse/android/lib/CommonSink;->clearContext(J)V

    .line 807
    return-void
.end method

.method public deriveContext(Lcom/glympse/android/api/GEventSink;)V
    .locals 1

    .prologue
    .line 816
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1}, Lcom/glympse/android/lib/CommonSink;->deriveContext(Lcom/glympse/android/api/GEventSink;)V

    .line 817
    return-void
.end method

.method public eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V
    .locals 6

    .prologue
    .line 786
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/glympse/android/api/GEventSink;

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/glympse/android/lib/CommonSink;->eventsOccurred(Lcom/glympse/android/api/GEventSink;Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 787
    return-void
.end method

.method public findMatch(Lcom/glympse/android/api/GTicket;)Lcom/glympse/android/api/GTicket;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 142
    if-nez p1, :cond_1

    move-object v0, v1

    .line 159
    :cond_0
    :goto_0
    return-object v0

    .line 146
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->lg:Lcom/glympse/android/hal/GVector;

    if-nez v0, :cond_2

    .line 148
    invoke-direct {p0}, Lcom/glympse/android/lib/bb;->bn()V

    .line 150
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->lg:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->length()I

    move-result v3

    .line 151
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_3

    .line 153
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->lg:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, v2}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GTicket;

    .line 154
    invoke-interface {v0, p1}, Lcom/glympse/android/api/GTicket;->isEqual(Lcom/glympse/android/core/GCommon;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 151
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 159
    goto :goto_0
.end method

.method public getContext(J)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 801
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1, p2}, Lcom/glympse/android/lib/CommonSink;->getContext(J)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getContextKeys()Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Enumeration",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 811
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0}, Lcom/glympse/android/lib/CommonSink;->getContextKeys()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public getDraft()Lcom/glympse/android/api/GTicket;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->lg:Lcom/glympse/android/hal/GVector;

    if-nez v0, :cond_0

    .line 104
    invoke-direct {p0}, Lcom/glympse/android/lib/bb;->bn()V

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->lf:Lcom/glympse/android/api/GTicket;

    return-object v0
.end method

.method public getFavorites()Lcom/glympse/android/core/GArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/glympse/android/core/GArray",
            "<",
            "Lcom/glympse/android/api/GTicket;",
            ">;"
        }
    .end annotation

    .prologue
    .line 111
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->lg:Lcom/glympse/android/hal/GVector;

    if-nez v0, :cond_0

    .line 113
    invoke-direct {p0}, Lcom/glympse/android/lib/bb;->bn()V

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->lg:Lcom/glympse/android/hal/GVector;

    return-object v0
.end method

.method public getListeners()Lcom/glympse/android/core/GArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/glympse/android/core/GArray",
            "<",
            "Lcom/glympse/android/api/GEventListener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 781
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0}, Lcom/glympse/android/lib/CommonSink;->getListeners()Lcom/glympse/android/core/GArray;

    move-result-object v0

    return-object v0
.end method

.method public hasContext(J)Z
    .locals 1

    .prologue
    .line 791
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1, p2}, Lcom/glympse/android/lib/CommonSink;->hasContext(J)Z

    move-result v0

    return v0
.end method

.method public hasFavorite(Lcom/glympse/android/api/GTicket;)Z
    .locals 1

    .prologue
    .line 164
    invoke-virtual {p0, p1}, Lcom/glympse/android/lib/bb;->findMatch(Lcom/glympse/android/api/GTicket;)Lcom/glympse/android/api/GTicket;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load(Lcom/glympse/android/hal/GContextHolder;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 294
    iput-object p1, p0, Lcom/glympse/android/lib/bb;->iu:Lcom/glympse/android/hal/GContextHolder;

    .line 295
    iput-object p2, p0, Lcom/glympse/android/lib/bb;->iv:Ljava/lang/String;

    .line 296
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->jZ:Lcom/glympse/android/lib/fb;

    iget-object v1, p0, Lcom/glympse/android/lib/bb;->iu:Lcom/glympse/android/hal/GContextHolder;

    iget-object v2, p0, Lcom/glympse/android/lib/bb;->iv:Ljava/lang/String;

    const-string v3, "favorites_synced_v2"

    invoke-static {v3}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v4, v3}, Lcom/glympse/android/lib/fb;->a(Lcom/glympse/android/hal/GContextHolder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->le:Lcom/glympse/android/lib/fb;

    iget-object v1, p0, Lcom/glympse/android/lib/bb;->iu:Lcom/glympse/android/hal/GContextHolder;

    iget-object v2, p0, Lcom/glympse/android/lib/bb;->iv:Ljava/lang/String;

    const-string v3, "favorites_v2"

    invoke-static {v3}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v4, v3}, Lcom/glympse/android/lib/fb;->a(Lcom/glympse/android/hal/GContextHolder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    return-void
.end method

.method public moveFavorite(II)V
    .locals 4

    .prologue
    .line 218
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->lg:Lcom/glympse/android/hal/GVector;

    if-nez v0, :cond_0

    .line 220
    invoke-direct {p0}, Lcom/glympse/android/lib/bb;->bn()V

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->lg:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->length()I

    move-result v0

    .line 223
    const/4 v1, 0x2

    if-lt v0, v1, :cond_1

    if-ge p1, v0, :cond_1

    if-ge p2, v0, :cond_1

    if-ne p1, p2, :cond_2

    .line 241
    :cond_1
    :goto_0
    return-void

    .line 231
    :cond_2
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/bb;->t(I)J

    move-result-wide v0

    .line 232
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_1

    .line 237
    invoke-direct {p0, v0, v1, p2}, Lcom/glympse/android/lib/bb;->a(JI)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/bb;->f(Lcom/glympse/android/core/GPrimitive;)V

    .line 240
    invoke-direct {p0}, Lcom/glympse/android/lib/bb;->bs()V

    goto :goto_0
.end method

.method public removeFavorite(I)V
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->lg:Lcom/glympse/android/hal/GVector;

    if-nez v0, :cond_0

    .line 171
    invoke-direct {p0}, Lcom/glympse/android/lib/bb;->bn()V

    .line 173
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->lg:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->length()I

    move-result v0

    if-lt p1, v0, :cond_2

    .line 184
    :cond_1
    :goto_0
    return-void

    .line 177
    :cond_2
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/bb;->s(I)Z

    move-result v0

    .line 179
    if-eqz v0, :cond_1

    .line 182
    invoke-direct {p0}, Lcom/glympse/android/lib/bb;->bs()V

    goto :goto_0
.end method

.method public removeFavorite(Lcom/glympse/android/api/GTicket;)V
    .locals 3

    .prologue
    .line 188
    if-nez p1, :cond_1

    .line 214
    :cond_0
    :goto_0
    return-void

    .line 192
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->lg:Lcom/glympse/android/hal/GVector;

    if-nez v0, :cond_2

    .line 194
    invoke-direct {p0}, Lcom/glympse/android/lib/bb;->bn()V

    .line 197
    :cond_2
    const/4 v1, 0x0

    .line 199
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->lg:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v0

    .line 200
    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_1
    if-ltz v2, :cond_3

    .line 202
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->lg:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, v2}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GTicket;

    .line 203
    invoke-interface {v0, p1}, Lcom/glympse/android/api/GTicket;->isEqual(Lcom/glympse/android/core/GCommon;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 205
    invoke-direct {p0, v2}, Lcom/glympse/android/lib/bb;->s(I)Z

    move-result v0

    or-int/2addr v0, v1

    .line 200
    :goto_2
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    .line 209
    :cond_3
    if-eqz v1, :cond_0

    .line 212
    invoke-direct {p0}, Lcom/glympse/android/lib/bb;->bs()V

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public removeListener(Lcom/glympse/android/api/GEventListener;)Z
    .locals 1

    .prologue
    .line 776
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1}, Lcom/glympse/android/lib/CommonSink;->removeListener(Lcom/glympse/android/api/GEventListener;)Z

    move-result v0

    return v0
.end method

.method public setDraft(Lcom/glympse/android/api/GTicket;)V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->lg:Lcom/glympse/android/hal/GVector;

    if-nez v0, :cond_0

    .line 94
    invoke-direct {p0}, Lcom/glympse/android/lib/bb;->bn()V

    .line 96
    :cond_0
    iput-object p1, p0, Lcom/glympse/android/lib/bb;->lf:Lcom/glympse/android/api/GTicket;

    .line 97
    invoke-direct {p0}, Lcom/glympse/android/lib/bb;->save()V

    .line 98
    return-void
.end method

.method public start(Lcom/glympse/android/api/GGlympse;)V
    .locals 0

    .prologue
    .line 302
    check-cast p1, Lcom/glympse/android/lib/GGlympsePrivate;

    iput-object p1, p0, Lcom/glympse/android/lib/bb;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 303
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->jZ:Lcom/glympse/android/lib/fb;

    invoke-virtual {v0}, Lcom/glympse/android/lib/fb;->stop()V

    .line 308
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->le:Lcom/glympse/android/lib/fb;

    invoke-virtual {v0}, Lcom/glympse/android/lib/fb;->stop()V

    .line 309
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/bb;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 310
    return-void
.end method

.method public updateFavorite(Lcom/glympse/android/api/GTicket;I)V
    .locals 6

    .prologue
    .line 245
    if-nez p1, :cond_0

    .line 264
    :goto_0
    return-void

    .line 249
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->lg:Lcom/glympse/android/hal/GVector;

    if-nez v0, :cond_1

    .line 251
    invoke-direct {p0}, Lcom/glympse/android/lib/bb;->bn()V

    .line 253
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/bb;->lh:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "next_tag"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 254
    iget-object v2, p0, Lcom/glympse/android/lib/bb;->lh:Lcom/glympse/android/core/GPrimitive;

    const-string v3, "next_tag"

    invoke-static {v3}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-wide/16 v4, 0x1

    add-long/2addr v4, v0

    invoke-interface {v2, v3, v4, v5}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 256
    check-cast p1, Lcom/glympse/android/lib/GTicketPrivate;

    .line 257
    invoke-static {p1}, Lcom/glympse/android/lib/bb;->g(Lcom/glympse/android/lib/GTicketPrivate;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v2

    .line 259
    invoke-direct {p0, v0, v1, v2}, Lcom/glympse/android/lib/bb;->a(JLcom/glympse/android/core/GPrimitive;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/glympse/android/lib/bb;->f(Lcom/glympse/android/core/GPrimitive;)V

    .line 260
    invoke-direct {p0, v0, v1, p2}, Lcom/glympse/android/lib/bb;->a(JI)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/bb;->f(Lcom/glympse/android/core/GPrimitive;)V

    .line 261
    add-int/lit8 v0, p2, 0x1

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/bb;->s(I)Z

    .line 263
    invoke-direct {p0}, Lcom/glympse/android/lib/bb;->bs()V

    goto :goto_0
.end method

.method public updateFavorites(Lcom/glympse/android/core/GPrimitive;Ljava/lang/String;J)V
    .locals 1

    .prologue
    .line 272
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/glympse/android/lib/bb;->a(Lcom/glympse/android/core/GPrimitive;Ljava/lang/String;J)V

    .line 273
    invoke-direct {p0}, Lcom/glympse/android/lib/bb;->br()V

    .line 274
    invoke-direct {p0}, Lcom/glympse/android/lib/bb;->save()V

    .line 276
    invoke-direct {p0}, Lcom/glympse/android/lib/bb;->bs()V

    .line 277
    return-void
.end method
