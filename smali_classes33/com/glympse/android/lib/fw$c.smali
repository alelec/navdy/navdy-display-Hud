.class Lcom/glympse/android/lib/fw$c;
.super Ljava/lang/Object;
.source "TriggersManager.java"

# interfaces
.implements Lcom/glympse/android/api/GEventListener;
.implements Lcom/glympse/android/lib/fw$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/lib/fw;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "c"
.end annotation


# instance fields
.field private hS:Lcom/glympse/android/lib/GLocationManagerPrivate;

.field private ug:Lcom/glympse/android/core/GProximityListener;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 658
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/glympse/android/lib/fw$1;)V
    .locals 0

    .prologue
    .line 658
    invoke-direct {p0}, Lcom/glympse/android/lib/fw$c;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/core/GProximityListener;)V
    .locals 2

    .prologue
    .line 670
    invoke-interface {p1}, Lcom/glympse/android/lib/GGlympsePrivate;->getLocationManagerPrivate()Lcom/glympse/android/lib/GLocationManagerPrivate;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/fw$c;->hS:Lcom/glympse/android/lib/GLocationManagerPrivate;

    .line 671
    iput-object p2, p0, Lcom/glympse/android/lib/fw$c;->ug:Lcom/glympse/android/core/GProximityListener;

    .line 672
    iget-object v1, p0, Lcom/glympse/android/lib/fw$c;->hS:Lcom/glympse/android/lib/GLocationManagerPrivate;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GEventListener;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GLocationManagerPrivate;->addListener(Lcom/glympse/android/api/GEventListener;)Z

    .line 673
    return-void
.end method

.method public eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 698
    const/16 v0, 0x8

    if-ne v0, p2, :cond_0

    .line 700
    and-int/lit8 v0, p3, 0x10

    if-eqz v0, :cond_1

    .line 702
    iget-object v0, p0, Lcom/glympse/android/lib/fw$c;->ug:Lcom/glympse/android/core/GProximityListener;

    if-eqz v0, :cond_0

    .line 704
    check-cast p4, Lcom/glympse/android/core/GRegion;

    .line 705
    iget-object v0, p0, Lcom/glympse/android/lib/fw$c;->ug:Lcom/glympse/android/core/GProximityListener;

    invoke-interface {v0, p4}, Lcom/glympse/android/core/GProximityListener;->regionEntered(Lcom/glympse/android/core/GRegion;)V

    .line 717
    :cond_0
    :goto_0
    return-void

    .line 708
    :cond_1
    and-int/lit8 v0, p3, 0x20

    if-eqz v0, :cond_0

    .line 710
    iget-object v0, p0, Lcom/glympse/android/lib/fw$c;->ug:Lcom/glympse/android/core/GProximityListener;

    if-eqz v0, :cond_0

    .line 712
    check-cast p4, Lcom/glympse/android/core/GRegion;

    .line 713
    iget-object v0, p0, Lcom/glympse/android/lib/fw$c;->ug:Lcom/glympse/android/core/GProximityListener;

    invoke-interface {v0, p4}, Lcom/glympse/android/core/GProximityListener;->regionLeft(Lcom/glympse/android/core/GRegion;)V

    goto :goto_0
.end method

.method public startMonitoring(Lcom/glympse/android/core/GRegion;)V
    .locals 1

    .prologue
    .line 684
    iget-object v0, p0, Lcom/glympse/android/lib/fw$c;->hS:Lcom/glympse/android/lib/GLocationManagerPrivate;

    invoke-interface {v0, p1}, Lcom/glympse/android/lib/GLocationManagerPrivate;->startMonitoring(Lcom/glympse/android/core/GRegion;)V

    .line 685
    return-void
.end method

.method public stop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 677
    iget-object v1, p0, Lcom/glympse/android/lib/fw$c;->hS:Lcom/glympse/android/lib/GLocationManagerPrivate;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GEventListener;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GLocationManagerPrivate;->removeListener(Lcom/glympse/android/api/GEventListener;)Z

    .line 678
    iput-object v2, p0, Lcom/glympse/android/lib/fw$c;->hS:Lcom/glympse/android/lib/GLocationManagerPrivate;

    .line 679
    iput-object v2, p0, Lcom/glympse/android/lib/fw$c;->ug:Lcom/glympse/android/core/GProximityListener;

    .line 680
    return-void
.end method

.method public stopMonitoring(Lcom/glympse/android/core/GRegion;)V
    .locals 1

    .prologue
    .line 689
    iget-object v0, p0, Lcom/glympse/android/lib/fw$c;->hS:Lcom/glympse/android/lib/GLocationManagerPrivate;

    invoke-interface {v0, p1}, Lcom/glympse/android/lib/GLocationManagerPrivate;->stopMonitoring(Lcom/glympse/android/core/GRegion;)V

    .line 690
    return-void
.end method
