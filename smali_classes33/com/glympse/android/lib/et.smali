.class Lcom/glympse/android/lib/et;
.super Lcom/glympse/android/lib/e;
.source "RegisterDevice.java"


# instance fields
.field private _glympse:Lcom/glympse/android/lib/GGlympsePrivate;

.field private ga:Lcom/glympse/android/lib/g;

.field private sA:Ljava/lang/String;

.field private sB:Z

.field private sz:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/glympse/android/lib/GGlympsePrivate;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/glympse/android/lib/e;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/glympse/android/lib/et;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 36
    iput-object p2, p0, Lcom/glympse/android/lib/et;->sz:Ljava/lang/String;

    .line 37
    iput-object p3, p0, Lcom/glympse/android/lib/et;->sA:Ljava/lang/String;

    .line 38
    iget-object v0, p0, Lcom/glympse/android/lib/et;->sA:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/glympse/android/lib/et;->sB:Z

    .line 39
    new-instance v0, Lcom/glympse/android/lib/g;

    invoke-direct {v0}, Lcom/glympse/android/lib/g;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/et;->ga:Lcom/glympse/android/lib/g;

    .line 40
    iget-object v0, p0, Lcom/glympse/android/lib/et;->ga:Lcom/glympse/android/lib/g;

    iput-object v0, p0, Lcom/glympse/android/lib/et;->gB:Lcom/glympse/android/lib/f;

    .line 41
    return-void

    .line 38
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 94
    new-instance v0, Lcom/glympse/android/lib/g;

    invoke-direct {v0}, Lcom/glympse/android/lib/g;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/et;->ga:Lcom/glympse/android/lib/g;

    .line 95
    iget-object v0, p0, Lcom/glympse/android/lib/et;->ga:Lcom/glympse/android/lib/g;

    iput-object v0, p0, Lcom/glympse/android/lib/et;->gB:Lcom/glympse/android/lib/f;

    .line 96
    return-void
.end method

.method public methodType()I
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x2

    return v0
.end method

.method public process()Z
    .locals 5

    .prologue
    .line 69
    iget-object v0, p0, Lcom/glympse/android/lib/et;->ga:Lcom/glympse/android/lib/g;

    iget-object v0, v0, Lcom/glympse/android/lib/g;->gF:Ljava/lang/String;

    const-string v1, "ok"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 71
    iget-object v0, p0, Lcom/glympse/android/lib/et;->sz:Ljava/lang/String;

    invoke-static {}, Lcom/glympse/android/hal/Platform;->getPushType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 73
    iget-boolean v0, p0, Lcom/glympse/android/lib/et;->sB:Z

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/glympse/android/lib/et;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getConfig()Lcom/glympse/android/api/GConfig;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GConfigPrivate;

    .line 77
    iget-object v1, p0, Lcom/glympse/android/lib/et;->sA:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GConfigPrivate;->saveRegistrationToken(Ljava/lang/String;)V

    .line 81
    :cond_0
    iget-boolean v0, p0, Lcom/glympse/android/lib/et;->sB:Z

    if-eqz v0, :cond_2

    const/16 v0, 0x200

    .line 82
    :goto_0
    iget-object v1, p0, Lcom/glympse/android/lib/et;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget-object v2, p0, Lcom/glympse/android/lib/et;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const v3, 0x10002

    iget-object v4, p0, Lcom/glympse/android/lib/et;->sA:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v0, v4}, Lcom/glympse/android/lib/GGlympsePrivate;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 89
    :cond_1
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 81
    :cond_2
    const/16 v0, 0x400

    goto :goto_0

    .line 87
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public url(Ljava/lang/StringBuilder;)Z
    .locals 1

    .prologue
    .line 49
    const-string v0, "users/self/"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    iget-boolean v0, p0, Lcom/glympse/android/lib/et;->sB:Z

    if-eqz v0, :cond_1

    const-string v0, "register_device"

    :goto_0
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    const-string v0, "?provider="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    iget-object v0, p0, Lcom/glympse/android/lib/et;->sz:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    iget-boolean v0, p0, Lcom/glympse/android/lib/et;->sB:Z

    if-eqz v0, :cond_0

    .line 56
    const-string v0, "&auth_id="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    iget-object v0, p0, Lcom/glympse/android/lib/et;->sA:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    :cond_0
    const/4 v0, 0x1

    return v0

    .line 50
    :cond_1
    const-string v0, "unregister_device"

    goto :goto_0
.end method
