.class Lcom/glympse/android/lib/DemoMode$b;
.super Ljava/lang/Object;
.source "DemoMode.java"

# interfaces
.implements Lcom/glympse/android/core/GLocationProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/lib/DemoMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation


# instance fields
.field private ce:Lcom/glympse/android/core/GLocationListener;

.field private hp:Lcom/glympse/android/core/GLocation;


# direct methods
.method public constructor <init>(Lcom/glympse/android/core/GLocation;)V
    .locals 0

    .prologue
    .line 356
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 357
    iput-object p1, p0, Lcom/glympse/android/lib/DemoMode$b;->hp:Lcom/glympse/android/core/GLocation;

    .line 358
    return-void
.end method


# virtual methods
.method public applyProfile(Lcom/glympse/android/core/GLocationProfile;)V
    .locals 0

    .prologue
    .line 389
    return-void
.end method

.method public getLastKnownLocation()Lcom/glympse/android/core/GLocation;
    .locals 1

    .prologue
    .line 384
    const/4 v0, 0x0

    return-object v0
.end method

.method public isStarted()Z
    .locals 1

    .prologue
    .line 379
    const/4 v0, 0x0

    return v0
.end method

.method public setLocationListener(Lcom/glympse/android/core/GLocationListener;)V
    .locals 0

    .prologue
    .line 362
    iput-object p1, p0, Lcom/glympse/android/lib/DemoMode$b;->ce:Lcom/glympse/android/core/GLocationListener;

    .line 363
    return-void
.end method

.method public start()V
    .locals 2

    .prologue
    .line 367
    iget-object v0, p0, Lcom/glympse/android/lib/DemoMode$b;->ce:Lcom/glympse/android/core/GLocationListener;

    if-eqz v0, :cond_0

    .line 369
    iget-object v0, p0, Lcom/glympse/android/lib/DemoMode$b;->ce:Lcom/glympse/android/core/GLocationListener;

    iget-object v1, p0, Lcom/glympse/android/lib/DemoMode$b;->hp:Lcom/glympse/android/core/GLocation;

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GLocationListener;->locationChanged(Lcom/glympse/android/core/GLocation;)V

    .line 371
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 0

    .prologue
    .line 375
    return-void
.end method
