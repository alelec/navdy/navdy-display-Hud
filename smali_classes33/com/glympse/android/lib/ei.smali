.class Lcom/glympse/android/lib/ei;
.super Lcom/glympse/android/lib/HttpJob;
.source "PlaceSearchJob.java"


# instance fields
.field private f:Ljava/lang/String;

.field private gj:Ljava/lang/String;

.field private jm:Lcom/glympse/android/lib/GImageCache;

.field private jy:Z

.field private rQ:Lcom/glympse/android/lib/GPlaceSearchEnginePrivate;

.field private rS:Lcom/glympse/android/api/GPlaceSearchRequest;

.field private rT:Ljava/lang/String;

.field private rU:Lcom/glympse/android/hal/GVector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GVector",
            "<",
            "Lcom/glympse/android/api/GPlaceSearchResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/GPlaceSearchEnginePrivate;Lcom/glympse/android/api/GPlaceSearchRequest;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/glympse/android/lib/HttpJob;-><init>()V

    .line 35
    iput-object p2, p0, Lcom/glympse/android/lib/ei;->rQ:Lcom/glympse/android/lib/GPlaceSearchEnginePrivate;

    .line 36
    iput-object p3, p0, Lcom/glympse/android/lib/ei;->rS:Lcom/glympse/android/api/GPlaceSearchRequest;

    .line 37
    iput-object p4, p0, Lcom/glympse/android/lib/ei;->rT:Ljava/lang/String;

    .line 39
    invoke-interface {p1}, Lcom/glympse/android/lib/GGlympsePrivate;->getImageCache()Lcom/glympse/android/lib/GImageCache;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/ei;->jm:Lcom/glympse/android/lib/GImageCache;

    .line 40
    invoke-interface {p1}, Lcom/glympse/android/lib/GGlympsePrivate;->getBaseUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/ei;->f:Ljava/lang/String;

    .line 41
    invoke-interface {p1}, Lcom/glympse/android/lib/GGlympsePrivate;->getServerPost()Lcom/glympse/android/lib/GServerPost;

    move-result-object v0

    .line 42
    invoke-interface {v0}, Lcom/glympse/android/lib/GServerPost;->isSslEnabled()Z

    move-result v1

    iput-boolean v1, p0, Lcom/glympse/android/lib/ei;->jy:Z

    .line 43
    invoke-interface {v0}, Lcom/glympse/android/lib/GServerPost;->getAccessToken()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/ei;->gj:Ljava/lang/String;

    .line 44
    return-void
.end method


# virtual methods
.method public checkResponse(II)Z
    .locals 1

    .prologue
    .line 170
    const/16 v0, 0xc8

    if-ne v0, p1, :cond_0

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRetryInterval(I)I
    .locals 1

    .prologue
    .line 175
    const/4 v0, -0x1

    return v0
.end method

.method public onAbort()V
    .locals 2

    .prologue
    .line 194
    invoke-super {p0}, Lcom/glympse/android/lib/HttpJob;->onAbort()V

    .line 196
    iget-object v0, p0, Lcom/glympse/android/lib/ei;->rQ:Lcom/glympse/android/lib/GPlaceSearchEnginePrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/ei;->rS:Lcom/glympse/android/api/GPlaceSearchRequest;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GPlaceSearchEnginePrivate;->failed(Lcom/glympse/android/api/GPlaceSearchRequest;)V

    .line 197
    return-void
.end method

.method public onComplete()V
    .locals 4

    .prologue
    .line 180
    invoke-super {p0}, Lcom/glympse/android/lib/HttpJob;->onComplete()V

    .line 182
    iget-object v0, p0, Lcom/glympse/android/lib/ei;->rU:Lcom/glympse/android/hal/GVector;

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/glympse/android/lib/ei;->rQ:Lcom/glympse/android/lib/GPlaceSearchEnginePrivate;

    new-instance v1, Lcom/glympse/android/lib/el;

    iget-object v2, p0, Lcom/glympse/android/lib/ei;->rU:Lcom/glympse/android/hal/GVector;

    iget-object v3, p0, Lcom/glympse/android/lib/ei;->rS:Lcom/glympse/android/api/GPlaceSearchRequest;

    invoke-direct {v1, v2, v3}, Lcom/glympse/android/lib/el;-><init>(Lcom/glympse/android/core/GArray;Lcom/glympse/android/api/GPlaceSearchRequest;)V

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GPlaceSearchEnginePrivate;->completed(Lcom/glympse/android/api/GPlaceSearchResults;)V

    .line 190
    :goto_0
    return-void

    .line 188
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/ei;->rQ:Lcom/glympse/android/lib/GPlaceSearchEnginePrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/ei;->rS:Lcom/glympse/android/api/GPlaceSearchRequest;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GPlaceSearchEnginePrivate;->failed(Lcom/glympse/android/api/GPlaceSearchRequest;)V

    goto :goto_0
.end method

.method public onPreProcess()V
    .locals 8

    .prologue
    const/16 v2, 0x26

    const/4 v1, 0x1

    const/16 v3, 0x3f

    .line 49
    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v0, 0x100

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 50
    const-string v0, "https://"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    iget-object v0, p0, Lcom/glympse/android/lib/ei;->f:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    const-string v0, "/places/search"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    const/4 v0, 0x0

    .line 57
    iget-object v5, p0, Lcom/glympse/android/lib/ei;->rT:Ljava/lang/String;

    invoke-static {v5}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 59
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 61
    const-string v0, "locale="

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    iget-object v0, p0, Lcom/glympse/android/lib/ei;->rT:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    .line 66
    :cond_0
    iget-object v5, p0, Lcom/glympse/android/lib/ei;->rS:Lcom/glympse/android/api/GPlaceSearchRequest;

    invoke-interface {v5}, Lcom/glympse/android/api/GPlaceSearchRequest;->getLocation()Lcom/glympse/android/core/GLatLng;

    move-result-object v5

    .line 67
    if-eqz v5, :cond_1

    invoke-interface {v5}, Lcom/glympse/android/core/GLatLng;->hasLocation()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 69
    if-eqz v0, :cond_3

    move v0, v2

    :goto_0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 71
    const-string v0, "location="

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    invoke-interface {v5}, Lcom/glympse/android/core/GLatLng;->getLatitude()D

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 73
    const/16 v0, 0x2c

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 74
    invoke-interface {v5}, Lcom/glympse/android/core/GLatLng;->getLongitude()D

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move v0, v1

    .line 78
    :cond_1
    iget-object v1, p0, Lcom/glympse/android/lib/ei;->rS:Lcom/glympse/android/api/GPlaceSearchRequest;

    invoke-interface {v1}, Lcom/glympse/android/api/GPlaceSearchRequest;->getSearchTerm()Ljava/lang/String;

    move-result-object v1

    .line 79
    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 81
    if-eqz v0, :cond_4

    :goto_1
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 83
    const-string v0, "query="

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/ei;->_httpConnection:Lcom/glympse/android/hal/GHttpConnection;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/hal/GHttpConnection;->setUrl(Ljava/lang/String;)V

    .line 88
    iget-object v0, p0, Lcom/glympse/android/lib/ei;->gj:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/ei;->setAuthorization(Ljava/lang/String;)V

    .line 89
    return-void

    :cond_3
    move v0, v3

    .line 69
    goto :goto_0

    :cond_4
    move v2, v3

    .line 81
    goto :goto_1
.end method

.method public onProcessResponse()V
    .locals 15

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/glympse/android/lib/ei;->isSucceeded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 166
    :cond_0
    return-void

    .line 100
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/ei;->_httpConnection:Lcom/glympse/android/hal/GHttpConnection;

    invoke-interface {v0}, Lcom/glympse/android/hal/GHttpConnection;->getResponseDataString()Ljava/lang/String;

    move-result-object v0

    .line 101
    invoke-static {v0}, Lcom/glympse/android/lib/json/JsonSerializer;->toPrimitive(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 102
    if-eqz v0, :cond_0

    .line 108
    const-string v1, "result"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 109
    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "ok"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 115
    const-string v1, "response"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 116
    if-eqz v0, :cond_0

    .line 120
    const-string v1, "items"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v9

    .line 121
    if-eqz v9, :cond_0

    .line 126
    invoke-interface {v9}, Lcom/glympse/android/core/GPrimitive;->size()I

    move-result v10

    .line 127
    new-instance v0, Lcom/glympse/android/hal/GVector;

    invoke-direct {v0, v10}, Lcom/glympse/android/hal/GVector;-><init>(I)V

    iput-object v0, p0, Lcom/glympse/android/lib/ei;->rU:Lcom/glympse/android/hal/GVector;

    .line 128
    const/4 v0, 0x0

    move v8, v0

    :goto_0
    if-ge v8, v10, :cond_0

    .line 130
    invoke-interface {v9, v8}, Lcom/glympse/android/core/GPrimitive;->get(I)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 131
    const-string v1, "lat"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 132
    const-string v1, "lng"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 133
    new-instance v1, Lcom/glympse/android/lib/dh;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/glympse/android/lib/dh;-><init>(DD)V

    .line 134
    const-string v2, "name"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 135
    const-string v3, "address"

    invoke-static {v3}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 136
    const-string v4, "url"

    invoke-static {v4}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 137
    const-string v5, "icon"

    invoke-static {v5}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v5}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 139
    const/4 v6, 0x0

    .line 140
    const/4 v7, 0x0

    .line 141
    const-string v5, "phone_numbers"

    invoke-static {v5}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v5}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v5

    .line 142
    if-eqz v5, :cond_2

    .line 144
    invoke-interface {v5}, Lcom/glympse/android/core/GPrimitive;->size()I

    move-result v12

    .line 145
    if-lez v12, :cond_2

    .line 147
    new-instance v6, Lcom/glympse/android/hal/GVector;

    invoke-direct {v6, v12}, Lcom/glympse/android/hal/GVector;-><init>(I)V

    .line 148
    new-instance v7, Lcom/glympse/android/hal/GVector;

    invoke-direct {v7, v12}, Lcom/glympse/android/hal/GVector;-><init>(I)V

    .line 149
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v12, :cond_2

    .line 151
    invoke-interface {v5, v0}, Lcom/glympse/android/core/GPrimitive;->get(I)Lcom/glympse/android/core/GPrimitive;

    move-result-object v13

    .line 152
    const-string v14, "type"

    invoke-static {v14}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-interface {v13, v14}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v6, v14}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 153
    const-string v14, "number"

    invoke-static {v14}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-interface {v13, v14}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v7, v13}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 149
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 158
    :cond_2
    new-instance v5, Lcom/glympse/android/lib/cq;

    const/4 v0, 0x0

    invoke-direct {v5, v11, v0}, Lcom/glympse/android/lib/cq;-><init>(Ljava/lang/String;Lcom/glympse/android/core/GDrawable;)V

    .line 159
    iget-object v0, p0, Lcom/glympse/android/lib/ei;->jm:Lcom/glympse/android/lib/GImageCache;

    invoke-interface {v5, v0}, Lcom/glympse/android/lib/GImagePrivate;->attachCache(Lcom/glympse/android/lib/GImageCache;)V

    .line 161
    iget-object v11, p0, Lcom/glympse/android/lib/ei;->rU:Lcom/glympse/android/hal/GVector;

    new-instance v0, Lcom/glympse/android/lib/ek;

    invoke-direct/range {v0 .. v7}, Lcom/glympse/android/lib/ek;-><init>(Lcom/glympse/android/core/GLatLng;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/glympse/android/api/GImage;Lcom/glympse/android/hal/GVector;Lcom/glympse/android/hal/GVector;)V

    invoke-virtual {v11, v0}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 128
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto/16 :goto_0
.end method
