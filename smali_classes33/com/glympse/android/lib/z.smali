.class Lcom/glympse/android/lib/z;
.super Lcom/glympse/android/lib/e;
.source "ConfirmAccount.java"


# instance fields
.field protected _glympse:Lcom/glympse/android/lib/GGlympsePrivate;

.field protected fV:Lcom/glympse/android/api/GEventSink;

.field protected fX:Ljava/lang/String;

.field protected fY:Lcom/glympse/android/core/GPrimitive;

.field protected fZ:Ljava/lang/String;

.field protected ga:Lcom/glympse/android/lib/g;


# direct methods
.method public constructor <init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/api/GEventSink;Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/glympse/android/lib/e;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/glympse/android/lib/z;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 33
    iput-object p2, p0, Lcom/glympse/android/lib/z;->fV:Lcom/glympse/android/api/GEventSink;

    .line 34
    iput-object p3, p0, Lcom/glympse/android/lib/z;->fX:Ljava/lang/String;

    .line 35
    iput-object p4, p0, Lcom/glympse/android/lib/z;->fY:Lcom/glympse/android/core/GPrimitive;

    .line 36
    new-instance v0, Lcom/glympse/android/lib/g;

    invoke-direct {v0}, Lcom/glympse/android/lib/g;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/z;->ga:Lcom/glympse/android/lib/g;

    .line 37
    iget-object v0, p0, Lcom/glympse/android/lib/z;->ga:Lcom/glympse/android/lib/g;

    iput-object v0, p0, Lcom/glympse/android/lib/z;->gB:Lcom/glympse/android/lib/f;

    .line 38
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 99
    new-instance v0, Lcom/glympse/android/lib/g;

    invoke-direct {v0}, Lcom/glympse/android/lib/g;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/z;->ga:Lcom/glympse/android/lib/g;

    .line 100
    iget-object v0, p0, Lcom/glympse/android/lib/z;->ga:Lcom/glympse/android/lib/g;

    iput-object v0, p0, Lcom/glympse/android/lib/z;->gB:Lcom/glympse/android/lib/f;

    .line 101
    return-void
.end method

.method public methodType()I
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x2

    return v0
.end method

.method public post()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/glympse/android/lib/z;->fZ:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/glympse/android/lib/z;->fZ:Ljava/lang/String;

    .line 70
    :goto_0
    return-object v0

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/z;->fY:Lcom/glympse/android/core/GPrimitive;

    invoke-static {v0}, Lcom/glympse/android/lib/json/JsonSerializer;->toString(Lcom/glympse/android/core/GPrimitive;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/z;->fZ:Ljava/lang/String;

    .line 70
    iget-object v0, p0, Lcom/glympse/android/lib/z;->fZ:Ljava/lang/String;

    goto :goto_0
.end method

.method public process()Z
    .locals 7

    .prologue
    const/16 v6, 0x14

    const/4 v5, 0x2

    const/4 v1, 0x1

    .line 75
    iget-object v0, p0, Lcom/glympse/android/lib/z;->ga:Lcom/glympse/android/lib/g;

    iget-object v0, v0, Lcom/glympse/android/lib/g;->gF:Ljava/lang/String;

    const-string v2, "ok"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/glympse/android/lib/z;->fV:Lcom/glympse/android/api/GEventSink;

    iget-object v2, p0, Lcom/glympse/android/lib/z;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget-object v3, p0, Lcom/glympse/android/lib/z;->fY:Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v0, v2, v6, v1, v3}, Lcom/glympse/android/api/GEventSink;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    move v0, v1

    .line 93
    :goto_0
    return v0

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/z;->ga:Lcom/glympse/android/lib/g;

    iget-object v0, v0, Lcom/glympse/android/lib/g;->gG:Ljava/lang/String;

    const-string v2, "invalid_argument"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 85
    new-instance v0, Lcom/glympse/android/lib/ex;

    iget-object v1, p0, Lcom/glympse/android/lib/z;->ga:Lcom/glympse/android/lib/g;

    iget-object v1, v1, Lcom/glympse/android/lib/g;->gG:Ljava/lang/String;

    iget-object v2, p0, Lcom/glympse/android/lib/z;->ga:Lcom/glympse/android/lib/g;

    iget-object v2, v2, Lcom/glympse/android/lib/g;->gH:Ljava/lang/String;

    iget-object v3, p0, Lcom/glympse/android/lib/z;->fY:Lcom/glympse/android/core/GPrimitive;

    invoke-direct {v0, v5, v1, v2, v3}, Lcom/glympse/android/lib/ex;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 92
    :goto_1
    iget-object v1, p0, Lcom/glympse/android/lib/z;->fV:Lcom/glympse/android/api/GEventSink;

    iget-object v2, p0, Lcom/glympse/android/lib/z;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1, v2, v6, v5, v0}, Lcom/glympse/android/api/GEventSink;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 93
    const/4 v0, 0x0

    goto :goto_0

    .line 89
    :cond_1
    new-instance v0, Lcom/glympse/android/lib/ex;

    iget-object v2, p0, Lcom/glympse/android/lib/z;->ga:Lcom/glympse/android/lib/g;

    iget-object v2, v2, Lcom/glympse/android/lib/g;->gG:Ljava/lang/String;

    iget-object v3, p0, Lcom/glympse/android/lib/z;->ga:Lcom/glympse/android/lib/g;

    iget-object v3, v3, Lcom/glympse/android/lib/g;->gH:Ljava/lang/String;

    iget-object v4, p0, Lcom/glympse/android/lib/z;->fY:Lcom/glympse/android/core/GPrimitive;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/glympse/android/lib/ex;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public url(Ljava/lang/StringBuilder;)Z
    .locals 1

    .prologue
    .line 46
    const-string v0, "users/self/linked_accounts/"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    iget-object v0, p0, Lcom/glympse/android/lib/z;->fX:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    const-string v0, "/confirm"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    const-string v0, "?locale="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    invoke-static {}, Lcom/glympse/android/hal/Platform;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    const-string v0, "&region="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    invoke-static {}, Lcom/glympse/android/hal/Platform;->getRegion()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 53
    const/4 v0, 0x1

    return v0
.end method
