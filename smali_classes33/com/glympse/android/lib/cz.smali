.class Lcom/glympse/android/lib/cz;
.super Lcom/glympse/android/lib/json/JsonHandlerBasic;
.source "InviteTicketParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/lib/cz$a;,
        Lcom/glympse/android/lib/cz$c;,
        Lcom/glympse/android/lib/cz$b;
    }
.end annotation


# instance fields
.field private gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

.field private gE:Ljava/lang/String;

.field public jG:Lcom/glympse/android/hal/GVector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GVector",
            "<",
            "Lcom/glympse/android/api/GDataRow;",
            ">;"
        }
    .end annotation
.end field

.field private nd:I

.field public oZ:Lcom/glympse/android/lib/GUserPrivate;

.field public ou:Ljava/lang/String;

.field public pe:Lcom/glympse/android/lib/GTicketPrivate;

.field public pf:Z

.field public pg:Z

.field public ph:Z

.field public pi:Z

.field public pj:Z

.field public pk:Z

.field public pl:Z

.field public pm:Lcom/glympse/android/hal/GVector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GVector",
            "<",
            "Lcom/glympse/android/api/GDataRow;",
            ">;"
        }
    .end annotation
.end field

.field public pn:Lcom/glympse/android/hal/GVector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GVector",
            "<",
            "Lcom/glympse/android/api/GInvite;",
            ">;"
        }
    .end annotation
.end field

.field public po:Lcom/glympse/android/lib/GTicketPrivate;


# direct methods
.method public constructor <init>(Lcom/glympse/android/lib/json/GJsonHandlerStack;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 76
    invoke-direct {p0}, Lcom/glympse/android/lib/json/JsonHandlerBasic;-><init>()V

    .line 78
    iput-object p1, p0, Lcom/glympse/android/lib/cz;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    .line 79
    iput p2, p0, Lcom/glympse/android/lib/cz;->nd:I

    .line 82
    new-instance v0, Lcom/glympse/android/lib/gb;

    invoke-direct {v0}, Lcom/glympse/android/lib/gb;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/cz;->oZ:Lcom/glympse/android/lib/GUserPrivate;

    .line 83
    new-instance v0, Lcom/glympse/android/lib/fe;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/fe;-><init>(Z)V

    iput-object v0, p0, Lcom/glympse/android/lib/cz;->pe:Lcom/glympse/android/lib/GTicketPrivate;

    .line 84
    iput-boolean v2, p0, Lcom/glympse/android/lib/cz;->pf:Z

    .line 85
    iput-boolean v2, p0, Lcom/glympse/android/lib/cz;->pg:Z

    .line 86
    iput-boolean v2, p0, Lcom/glympse/android/lib/cz;->ph:Z

    .line 87
    iput-boolean v2, p0, Lcom/glympse/android/lib/cz;->pi:Z

    .line 88
    iput-boolean v2, p0, Lcom/glympse/android/lib/cz;->pj:Z

    .line 89
    iput-boolean v2, p0, Lcom/glympse/android/lib/cz;->pk:Z

    .line 90
    iput-boolean v2, p0, Lcom/glympse/android/lib/cz;->pl:Z

    .line 91
    new-instance v0, Lcom/glympse/android/hal/GVector;

    invoke-direct {v0}, Lcom/glympse/android/hal/GVector;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/cz;->jG:Lcom/glympse/android/hal/GVector;

    .line 92
    new-instance v0, Lcom/glympse/android/hal/GVector;

    invoke-direct {v0}, Lcom/glympse/android/hal/GVector;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/cz;->pm:Lcom/glympse/android/hal/GVector;

    .line 93
    new-instance v0, Lcom/glympse/android/hal/GVector;

    invoke-direct {v0}, Lcom/glympse/android/hal/GVector;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/cz;->pn:Lcom/glympse/android/hal/GVector;

    .line 94
    return-void
.end method


# virtual methods
.method public endObject(I)Z
    .locals 1

    .prologue
    .line 164
    iget v0, p0, Lcom/glympse/android/lib/cz;->nd:I

    if-ne p1, v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/glympse/android/lib/cz;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    invoke-interface {v0}, Lcom/glympse/android/lib/json/GJsonHandlerStack;->popHandler()V

    .line 169
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public primitive(ILcom/glympse/android/lib/json/GJsonPrimitive;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 104
    packed-switch p1, :pswitch_data_0

    .line 119
    :cond_0
    :goto_0
    return v4

    .line 108
    :pswitch_0
    iget-object v0, p0, Lcom/glympse/android/lib/cz;->gE:Ljava/lang/String;

    const-string v1, "next"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 110
    iget-object v0, p0, Lcom/glympse/android/lib/cz;->pe:Lcom/glympse/android/lib/GTicketPrivate;

    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getLong()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/glympse/android/lib/GTicketPrivate;->setNext(J)V

    goto :goto_0

    .line 112
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/cz;->gE:Ljava/lang/String;

    const-string v1, "reference"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    invoke-virtual {p2, v4}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/cz;->ou:Ljava/lang/String;

    goto :goto_0

    .line 104
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public startArray(I)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 138
    const/4 v0, 0x3

    if-ne v0, p1, :cond_1

    .line 140
    iget-object v0, p0, Lcom/glympse/android/lib/cz;->gE:Ljava/lang/String;

    const-string v1, "properties"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 141
    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/glympse/android/lib/cz;->gE:Ljava/lang/String;

    const-string v2, "data"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 144
    :cond_0
    iput-boolean v0, p0, Lcom/glympse/android/lib/cz;->pf:Z

    .line 146
    iget-object v1, p0, Lcom/glympse/android/lib/cz;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    new-instance v2, Lcom/glympse/android/lib/cz$c;

    iget-object v3, p0, Lcom/glympse/android/lib/cz;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/cz;

    invoke-direct {v2, v3, v0}, Lcom/glympse/android/lib/cz$c;-><init>(Lcom/glympse/android/lib/json/GJsonHandlerStack;Lcom/glympse/android/lib/cz;)V

    invoke-interface {v1, v2, v4}, Lcom/glympse/android/lib/json/GJsonHandlerStack;->pushHandler(Lcom/glympse/android/lib/json/GJsonHandler;I)V

    .line 159
    :cond_1
    :goto_0
    return v4

    .line 148
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/cz;->gE:Ljava/lang/String;

    const-string v1, "recipients"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 151
    iget-object v1, p0, Lcom/glympse/android/lib/cz;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    new-instance v2, Lcom/glympse/android/lib/cz$a;

    iget-object v3, p0, Lcom/glympse/android/lib/cz;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/cz;

    invoke-direct {v2, v3, p1, v0}, Lcom/glympse/android/lib/cz$a;-><init>(Lcom/glympse/android/lib/json/GJsonHandlerStack;ILcom/glympse/android/lib/cz;)V

    invoke-interface {v1, v2}, Lcom/glympse/android/lib/json/GJsonHandlerStack;->pushHandler(Lcom/glympse/android/lib/json/GJsonHandler;)V

    goto :goto_0

    .line 153
    :cond_3
    iget-object v0, p0, Lcom/glympse/android/lib/cz;->gE:Ljava/lang/String;

    const-string v1, "location"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 156
    iget-object v1, p0, Lcom/glympse/android/lib/cz;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    new-instance v2, Lcom/glympse/android/lib/cz$b;

    iget-object v3, p0, Lcom/glympse/android/lib/cz;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/cz;

    invoke-direct {v2, v3, p1, v0}, Lcom/glympse/android/lib/cz$b;-><init>(Lcom/glympse/android/lib/json/GJsonHandlerStack;ILcom/glympse/android/lib/cz;)V

    invoke-interface {v1, v2}, Lcom/glympse/android/lib/json/GJsonHandlerStack;->pushHandler(Lcom/glympse/android/lib/json/GJsonHandler;)V

    goto :goto_0
.end method

.method public startObject(I)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 124
    const/4 v0, 0x3

    if-ne v0, p1, :cond_0

    .line 126
    iget-object v0, p0, Lcom/glympse/android/lib/cz;->gE:Ljava/lang/String;

    const-string v1, "reply"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    new-instance v0, Lcom/glympse/android/lib/fe;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/fe;-><init>(Z)V

    iput-object v0, p0, Lcom/glympse/android/lib/cz;->po:Lcom/glympse/android/lib/GTicketPrivate;

    .line 130
    iget-object v0, p0, Lcom/glympse/android/lib/cz;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    new-instance v1, Lcom/glympse/android/lib/eu$c;

    iget-object v2, p0, Lcom/glympse/android/lib/cz;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    iget-object v3, p0, Lcom/glympse/android/lib/cz;->po:Lcom/glympse/android/lib/GTicketPrivate;

    invoke-direct {v1, v2, v3}, Lcom/glympse/android/lib/eu$c;-><init>(Lcom/glympse/android/lib/json/GJsonHandlerStack;Lcom/glympse/android/lib/GTicketPrivate;)V

    invoke-interface {v0, v1, v4}, Lcom/glympse/android/lib/json/GJsonHandlerStack;->pushHandler(Lcom/glympse/android/lib/json/GJsonHandler;I)V

    .line 133
    :cond_0
    return v4
.end method

.method public startPair(ILjava/lang/String;)Z
    .locals 1

    .prologue
    .line 98
    iput-object p2, p0, Lcom/glympse/android/lib/cz;->gE:Ljava/lang/String;

    .line 99
    const/4 v0, 0x1

    return v0
.end method
