.class public Lcom/glympse/android/lib/LocationProfile;
.super Ljava/lang/Object;
.source "LocationProfile.java"

# interfaces
.implements Lcom/glympse/android/lib/GLocationProfilePrivate;


# instance fields
.field private pC:I

.field private qA:I

.field private qB:I

.field private qC:D

.field private qD:D

.field private qE:I

.field private qF:I

.field private qG:I

.field private qH:Z

.field private qI:Z


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const/4 v0, -0x1

    iput v0, p0, Lcom/glympse/android/lib/LocationProfile;->qA:I

    .line 44
    iput v1, p0, Lcom/glympse/android/lib/LocationProfile;->pC:I

    .line 45
    const/4 v0, 0x3

    iput v0, p0, Lcom/glympse/android/lib/LocationProfile;->qB:I

    .line 46
    iput-wide v2, p0, Lcom/glympse/android/lib/LocationProfile;->qC:D

    .line 47
    iput-wide v2, p0, Lcom/glympse/android/lib/LocationProfile;->qD:D

    .line 48
    iput v1, p0, Lcom/glympse/android/lib/LocationProfile;->qE:I

    .line 49
    iput v1, p0, Lcom/glympse/android/lib/LocationProfile;->qF:I

    .line 50
    iput v1, p0, Lcom/glympse/android/lib/LocationProfile;->qG:I

    .line 51
    iput-boolean v1, p0, Lcom/glympse/android/lib/LocationProfile;->qH:Z

    .line 52
    iput-boolean v1, p0, Lcom/glympse/android/lib/LocationProfile;->qI:Z

    .line 53
    return-void
.end method

.method public static stringToEnum(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 188
    const-string v0, "viewing"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 189
    const/4 v0, 0x3

    .line 197
    :goto_0
    return v0

    .line 190
    :cond_0
    const-string v0, "not_viewing"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 191
    const/4 v0, 0x2

    goto :goto_0

    .line 192
    :cond_1
    const-string v0, "foreground"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 193
    const/4 v0, 0x1

    goto :goto_0

    .line 194
    :cond_2
    const-string v0, "background"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 195
    const/4 v0, 0x0

    goto :goto_0

    .line 197
    :cond_3
    const/4 v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public getAccuracy()D
    .locals 2

    .prologue
    .line 81
    iget-wide v0, p0, Lcom/glympse/android/lib/LocationProfile;->qD:D

    return-wide v0
.end method

.method public getActivity()I
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lcom/glympse/android/lib/LocationProfile;->qF:I

    return v0
.end method

.method public getDistance()D
    .locals 2

    .prologue
    .line 76
    iget-wide v0, p0, Lcom/glympse/android/lib/LocationProfile;->qC:D

    return-wide v0
.end method

.method public getFrequency()I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lcom/glympse/android/lib/LocationProfile;->qE:I

    return v0
.end method

.method public getMode()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/glympse/android/lib/LocationProfile;->pC:I

    return v0
.end method

.method public getPriority()I
    .locals 1

    .prologue
    .line 91
    iget v0, p0, Lcom/glympse/android/lib/LocationProfile;->qG:I

    return v0
.end method

.method public getProfile()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/glympse/android/lib/LocationProfile;->qA:I

    return v0
.end method

.method public getSource()I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/glympse/android/lib/LocationProfile;->qB:I

    return v0
.end method

.method public isAutoPauseEnabled()Z
    .locals 1

    .prologue
    .line 101
    iget-boolean v0, p0, Lcom/glympse/android/lib/LocationProfile;->qH:Z

    return v0
.end method

.method public isEqual(Lcom/glympse/android/core/GCommon;)Z
    .locals 4

    .prologue
    .line 169
    check-cast p1, Lcom/glympse/android/lib/LocationProfile;

    .line 170
    iget v0, p1, Lcom/glympse/android/lib/LocationProfile;->pC:I

    iget v1, p0, Lcom/glympse/android/lib/LocationProfile;->pC:I

    if-ne v0, v1, :cond_0

    iget v0, p1, Lcom/glympse/android/lib/LocationProfile;->qB:I

    iget v1, p0, Lcom/glympse/android/lib/LocationProfile;->qB:I

    if-ne v0, v1, :cond_0

    iget-wide v0, p1, Lcom/glympse/android/lib/LocationProfile;->qC:D

    iget-wide v2, p0, Lcom/glympse/android/lib/LocationProfile;->qC:D

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p1, Lcom/glympse/android/lib/LocationProfile;->qD:D

    iget-wide v2, p0, Lcom/glympse/android/lib/LocationProfile;->qD:D

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    iget v0, p1, Lcom/glympse/android/lib/LocationProfile;->qE:I

    iget v1, p0, Lcom/glympse/android/lib/LocationProfile;->qE:I

    if-ne v0, v1, :cond_0

    iget v0, p1, Lcom/glympse/android/lib/LocationProfile;->qF:I

    iget v1, p0, Lcom/glympse/android/lib/LocationProfile;->qF:I

    if-ne v0, v1, :cond_0

    iget v0, p1, Lcom/glympse/android/lib/LocationProfile;->qG:I

    iget v1, p0, Lcom/glympse/android/lib/LocationProfile;->qG:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p1, Lcom/glympse/android/lib/LocationProfile;->qH:Z

    iget-boolean v1, p0, Lcom/glympse/android/lib/LocationProfile;->qH:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p1, Lcom/glympse/android/lib/LocationProfile;->qI:Z

    iget-boolean v1, p0, Lcom/glympse/android/lib/LocationProfile;->qI:Z

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSignificantLocationChangeMonitoringEnabled()Z
    .locals 1

    .prologue
    .line 106
    iget-boolean v0, p0, Lcom/glympse/android/lib/LocationProfile;->qI:Z

    return v0
.end method

.method public setAccuracy(D)V
    .locals 1

    .prologue
    .line 135
    iput-wide p1, p0, Lcom/glympse/android/lib/LocationProfile;->qD:D

    .line 136
    return-void
.end method

.method public setActivity(I)V
    .locals 0

    .prologue
    .line 150
    iput p1, p0, Lcom/glympse/android/lib/LocationProfile;->qF:I

    .line 151
    return-void
.end method

.method public setAutoPauseEnabled(Z)V
    .locals 0

    .prologue
    .line 155
    iput-boolean p1, p0, Lcom/glympse/android/lib/LocationProfile;->qH:Z

    .line 156
    return-void
.end method

.method public setDistance(D)V
    .locals 1

    .prologue
    .line 130
    iput-wide p1, p0, Lcom/glympse/android/lib/LocationProfile;->qC:D

    .line 131
    return-void
.end method

.method public setFrequency(I)V
    .locals 0

    .prologue
    .line 140
    iput p1, p0, Lcom/glympse/android/lib/LocationProfile;->qE:I

    .line 141
    return-void
.end method

.method public setMode(I)V
    .locals 0

    .prologue
    .line 120
    iput p1, p0, Lcom/glympse/android/lib/LocationProfile;->pC:I

    .line 121
    return-void
.end method

.method public setPriority(I)V
    .locals 0

    .prologue
    .line 145
    iput p1, p0, Lcom/glympse/android/lib/LocationProfile;->qG:I

    .line 146
    return-void
.end method

.method public setProfile(I)V
    .locals 0

    .prologue
    .line 115
    iput p1, p0, Lcom/glympse/android/lib/LocationProfile;->qA:I

    .line 116
    return-void
.end method

.method public setSignificantLocationChangeMonitoringEnabled(Z)V
    .locals 0

    .prologue
    .line 160
    iput-boolean p1, p0, Lcom/glympse/android/lib/LocationProfile;->qI:Z

    .line 161
    return-void
.end method

.method public setSource(I)V
    .locals 0

    .prologue
    .line 125
    iput p1, p0, Lcom/glympse/android/lib/LocationProfile;->qB:I

    .line 126
    return-void
.end method
