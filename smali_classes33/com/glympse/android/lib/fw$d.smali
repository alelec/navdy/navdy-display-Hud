.class Lcom/glympse/android/lib/fw$d;
.super Ljava/lang/Object;
.source "TriggersManager.java"

# interfaces
.implements Lcom/glympse/android/lib/fw$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/lib/fw;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "d"
.end annotation


# instance fields
.field private uh:Lcom/glympse/android/core/GProximityProvider;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 723
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/glympse/android/lib/fw$1;)V
    .locals 0

    .prologue
    .line 723
    invoke-direct {p0}, Lcom/glympse/android/lib/fw$d;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/core/GProximityListener;)V
    .locals 1

    .prologue
    .line 733
    invoke-interface {p1}, Lcom/glympse/android/lib/GGlympsePrivate;->getContextHolder()Lcom/glympse/android/hal/GContextHolder;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/hal/GContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/glympse/android/hal/HalFactory;->createProximityProvider(Landroid/content/Context;)Lcom/glympse/android/core/GProximityProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/fw$d;->uh:Lcom/glympse/android/core/GProximityProvider;

    .line 734
    iget-object v0, p0, Lcom/glympse/android/lib/fw$d;->uh:Lcom/glympse/android/core/GProximityProvider;

    invoke-interface {v0, p2}, Lcom/glympse/android/core/GProximityProvider;->setProximityListener(Lcom/glympse/android/core/GProximityListener;)V

    .line 735
    return-void
.end method

.method public startMonitoring(Lcom/glympse/android/core/GRegion;)V
    .locals 1

    .prologue
    .line 744
    iget-object v0, p0, Lcom/glympse/android/lib/fw$d;->uh:Lcom/glympse/android/core/GProximityProvider;

    if-eqz v0, :cond_0

    .line 746
    iget-object v0, p0, Lcom/glympse/android/lib/fw$d;->uh:Lcom/glympse/android/core/GProximityProvider;

    invoke-interface {v0, p1}, Lcom/glympse/android/core/GProximityProvider;->startMonitoring(Lcom/glympse/android/core/GRegion;)V

    .line 748
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 739
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/fw$d;->uh:Lcom/glympse/android/core/GProximityProvider;

    .line 740
    return-void
.end method

.method public stopMonitoring(Lcom/glympse/android/core/GRegion;)V
    .locals 1

    .prologue
    .line 752
    iget-object v0, p0, Lcom/glympse/android/lib/fw$d;->uh:Lcom/glympse/android/core/GProximityProvider;

    if-eqz v0, :cond_0

    .line 754
    iget-object v0, p0, Lcom/glympse/android/lib/fw$d;->uh:Lcom/glympse/android/core/GProximityProvider;

    invoke-interface {v0, p1}, Lcom/glympse/android/core/GProximityProvider;->stopMonitoring(Lcom/glympse/android/core/GRegion;)V

    .line 756
    :cond_0
    return-void
.end method
