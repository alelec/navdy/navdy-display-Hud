.class Lcom/glympse/android/lib/v;
.super Ljava/lang/Object;
.source "ChronoTrigger.java"

# interfaces
.implements Lcom/glympse/android/api/GChronoTrigger;


# instance fields
.field private _time:J

.field private ic:Lcom/glympse/android/lib/fv;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Lcom/glympse/android/lib/fv;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/fv;-><init>(I)V

    iput-object v0, p0, Lcom/glympse/android/lib/v;->ic:Lcom/glympse/android/lib/fv;

    .line 25
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZLcom/glympse/android/api/GTicket;J)V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Lcom/glympse/android/lib/fv;

    const/4 v1, 0x2

    invoke-direct {v0, v1, p1, p2, p3}, Lcom/glympse/android/lib/fv;-><init>(ILjava/lang/String;ZLcom/glympse/android/api/GTicket;)V

    iput-object v0, p0, Lcom/glympse/android/lib/v;->ic:Lcom/glympse/android/lib/fv;

    .line 30
    iput-wide p4, p0, Lcom/glympse/android/lib/v;->_time:J

    .line 31
    return-void
.end method


# virtual methods
.method public autoSend()Z
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/glympse/android/lib/v;->ic:Lcom/glympse/android/lib/fv;

    invoke-virtual {v0}, Lcom/glympse/android/lib/fv;->autoSend()Z

    move-result v0

    return v0
.end method

.method public decode(Lcom/glympse/android/core/GPrimitive;)V
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/glympse/android/lib/v;->ic:Lcom/glympse/android/lib/fv;

    invoke-virtual {v0, p1}, Lcom/glympse/android/lib/fv;->decode(Lcom/glympse/android/core/GPrimitive;)V

    .line 91
    const-string v0, "time"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/glympse/android/lib/v;->_time:J

    .line 92
    return-void
.end method

.method public encode(Lcom/glympse/android/core/GPrimitive;I)V
    .locals 4

    .prologue
    .line 82
    iget-object v0, p0, Lcom/glympse/android/lib/v;->ic:Lcom/glympse/android/lib/fv;

    invoke-virtual {v0, p1, p2}, Lcom/glympse/android/lib/fv;->encode(Lcom/glympse/android/core/GPrimitive;I)V

    .line 84
    const-string v0, "time"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lcom/glympse/android/lib/v;->_time:J

    invoke-interface {p1, v0, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 85
    return-void
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/glympse/android/lib/v;->ic:Lcom/glympse/android/lib/fv;

    invoke-virtual {v0}, Lcom/glympse/android/lib/fv;->getId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/glympse/android/lib/v;->ic:Lcom/glympse/android/lib/fv;

    invoke-virtual {v0}, Lcom/glympse/android/lib/fv;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTicket()Lcom/glympse/android/api/GTicket;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/glympse/android/lib/v;->ic:Lcom/glympse/android/lib/fv;

    invoke-virtual {v0}, Lcom/glympse/android/lib/fv;->getTicket()Lcom/glympse/android/api/GTicket;

    move-result-object v0

    return-object v0
.end method

.method public getTime()J
    .locals 2

    .prologue
    .line 73
    iget-wide v0, p0, Lcom/glympse/android/lib/v;->_time:J

    return-wide v0
.end method

.method public getTimeConstraint()Lcom/glympse/android/api/GTimeConstraint;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/glympse/android/lib/v;->ic:Lcom/glympse/android/lib/fv;

    invoke-virtual {v0}, Lcom/glympse/android/lib/fv;->getTimeConstraint()Lcom/glympse/android/api/GTimeConstraint;

    move-result-object v0

    return-object v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/glympse/android/lib/v;->ic:Lcom/glympse/android/lib/fv;

    invoke-virtual {v0}, Lcom/glympse/android/lib/fv;->getType()I

    move-result v0

    return v0
.end method
