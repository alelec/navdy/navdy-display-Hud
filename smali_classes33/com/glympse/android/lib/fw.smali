.class Lcom/glympse/android/lib/fw;
.super Ljava/lang/Object;
.source "TriggersManager.java"

# interfaces
.implements Lcom/glympse/android/lib/GTriggersManagerPrivate;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/lib/fw$a;,
        Lcom/glympse/android/lib/fw$e;,
        Lcom/glympse/android/lib/fw$d;,
        Lcom/glympse/android/lib/fw$c;,
        Lcom/glympse/android/lib/fw$b;
    }
.end annotation


# instance fields
.field private _glympse:Lcom/glympse/android/lib/GGlympsePrivate;

.field private he:Lcom/glympse/android/lib/CommonSink;

.field private jZ:Lcom/glympse/android/lib/fb;

.field private tY:Lcom/glympse/android/hal/GVector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GVector",
            "<",
            "Lcom/glympse/android/api/GTrigger;",
            ">;"
        }
    .end annotation
.end field

.field private tZ:Lcom/glympse/android/hal/GVector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GVector",
            "<",
            "Lcom/glympse/android/api/GTrigger;",
            ">;"
        }
    .end annotation
.end field

.field private ua:Lcom/glympse/android/hal/GHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GHashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/glympse/android/api/GTrigger;",
            ">;"
        }
    .end annotation
.end field

.field private ub:Z

.field private uc:Lcom/glympse/android/lib/fw$b;

.field private ud:Lcom/glympse/android/lib/fw$e;

.field private ue:Lcom/glympse/android/lib/fw$a;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Lcom/glympse/android/hal/GVector;

    invoke-direct {v0}, Lcom/glympse/android/hal/GVector;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/fw;->tZ:Lcom/glympse/android/hal/GVector;

    .line 54
    new-instance v0, Lcom/glympse/android/hal/GHashtable;

    invoke-direct {v0}, Lcom/glympse/android/hal/GHashtable;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/fw;->ua:Lcom/glympse/android/hal/GHashtable;

    .line 55
    new-instance v0, Lcom/glympse/android/lib/fb;

    invoke-direct {v0}, Lcom/glympse/android/lib/fb;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/fw;->jZ:Lcom/glympse/android/lib/fb;

    .line 56
    new-instance v0, Lcom/glympse/android/lib/CommonSink;

    const-string v1, "TriggersManager"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/CommonSink;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/glympse/android/lib/fw;->he:Lcom/glympse/android/lib/CommonSink;

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/glympse/android/lib/fw;->ub:Z

    .line 58
    return-void
.end method

.method private B(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 394
    if-nez p1, :cond_1

    .line 422
    :cond_0
    :goto_0
    return-void

    .line 400
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->ua:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GTrigger;

    .line 401
    if-eqz v0, :cond_0

    .line 407
    invoke-interface {v0}, Lcom/glympse/android/api/GTrigger;->getType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 411
    :pswitch_0
    check-cast v0, Lcom/glympse/android/api/GChronoTrigger;

    .line 412
    invoke-direct {p0, v0}, Lcom/glympse/android/lib/fw;->f(Lcom/glympse/android/api/GTrigger;)V

    goto :goto_0

    .line 417
    :pswitch_1
    check-cast v0, Lcom/glympse/android/api/GEtaTrigger;

    .line 418
    invoke-direct {p0, v0}, Lcom/glympse/android/lib/fw;->b(Lcom/glympse/android/api/GEtaTrigger;)V

    goto :goto_0

    .line 407
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private C(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 480
    if-nez p1, :cond_1

    .line 498
    :cond_0
    :goto_0
    return-void

    .line 485
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->ua:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GTrigger;

    .line 486
    if-eqz v0, :cond_0

    .line 491
    check-cast v0, Lcom/glympse/android/api/GEtaTrigger;

    .line 492
    if-eqz v0, :cond_0

    .line 497
    invoke-direct {p0, v0}, Lcom/glympse/android/lib/fw;->b(Lcom/glympse/android/api/GEtaTrigger;)V

    goto :goto_0
.end method

.method private D(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 502
    if-nez p1, :cond_1

    .line 520
    :cond_0
    :goto_0
    return-void

    .line 507
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->ua:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GTrigger;

    .line 508
    if-eqz v0, :cond_0

    .line 513
    check-cast v0, Lcom/glympse/android/api/GEtaTrigger;

    .line 514
    if-eqz v0, :cond_0

    .line 519
    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/fw;->removeLocalTrigger(Lcom/glympse/android/api/GTrigger;)V

    goto :goto_0
.end method

.method private a(Lcom/glympse/android/api/GEtaTrigger;)V
    .locals 0

    .prologue
    .line 357
    invoke-virtual {p0, p1}, Lcom/glympse/android/lib/fw;->removeLocalTrigger(Lcom/glympse/android/api/GTrigger;)V

    .line 359
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/fw;->a(Lcom/glympse/android/api/GTrigger;)V

    .line 360
    return-void
.end method

.method private a(Lcom/glympse/android/api/GTrigger;)V
    .locals 3

    .prologue
    .line 365
    invoke-interface {p1}, Lcom/glympse/android/api/GTrigger;->getTimeConstraint()Lcom/glympse/android/api/GTimeConstraint;

    move-result-object v0

    .line 366
    if-eqz v0, :cond_1

    .line 368
    invoke-interface {v0}, Lcom/glympse/android/api/GTimeConstraint;->validate()Z

    move-result v0

    if-nez v0, :cond_1

    .line 390
    :cond_0
    :goto_0
    return-void

    .line 375
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/16 v1, 0xf

    const/4 v2, 0x4

    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/glympse/android/lib/fw;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 378
    invoke-interface {p1}, Lcom/glympse/android/api/GTrigger;->autoSend()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 380
    invoke-interface {p1}, Lcom/glympse/android/api/GTrigger;->getTicket()Lcom/glympse/android/api/GTicket;

    move-result-object v0

    .line 381
    if-eqz v0, :cond_0

    .line 384
    invoke-interface {v0}, Lcom/glympse/android/api/GTicket;->clone()Lcom/glympse/android/api/GTicket;

    move-result-object v0

    .line 387
    iget-object v1, p0, Lcom/glympse/android/lib/fw;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GGlympsePrivate;->sendTicket(Lcom/glympse/android/api/GTicket;)Z

    goto :goto_0
.end method

.method private a(Lcom/glympse/android/api/GTrigger;I)V
    .locals 1

    .prologue
    .line 333
    move-object v0, p1

    check-cast v0, Lcom/glympse/android/api/GGeoTrigger;

    .line 334
    if-nez v0, :cond_1

    .line 346
    :cond_0
    :goto_0
    return-void

    .line 340
    :cond_1
    invoke-interface {v0}, Lcom/glympse/android/api/GGeoTrigger;->getTransition()I

    move-result v0

    and-int/2addr v0, p2

    if-eqz v0, :cond_0

    .line 345
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/fw;->a(Lcom/glympse/android/api/GTrigger;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/glympse/android/lib/fw;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/fw;->B(Ljava/lang/String;)V

    return-void
.end method

.method private b(Lcom/glympse/android/api/GEtaTrigger;)V
    .locals 6

    .prologue
    .line 426
    invoke-interface {p1}, Lcom/glympse/android/api/GEtaTrigger;->getEtaTicket()Lcom/glympse/android/api/GTicket;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GTicketPrivate;

    .line 427
    const-wide/16 v2, 0x0

    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketPrivate;->getEtaRaw()J

    move-result-wide v0

    cmp-long v0, v2, v0

    if-nez v0, :cond_1

    .line 459
    :cond_0
    :goto_0
    return-void

    .line 433
    :cond_1
    invoke-interface {p1}, Lcom/glympse/android/api/GEtaTrigger;->getEtaTicket()Lcom/glympse/android/api/GTicket;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/api/GTicket;->getEta()J

    move-result-wide v0

    .line 434
    invoke-interface {p1}, Lcom/glympse/android/api/GEtaTrigger;->getThreshold()J

    move-result-wide v2

    .line 435
    invoke-interface {p1}, Lcom/glympse/android/api/GEtaTrigger;->getTransition()I

    move-result v4

    .line 437
    const/4 v5, 0x2

    if-ne v5, v4, :cond_2

    .line 439
    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 441
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/fw;->a(Lcom/glympse/android/api/GEtaTrigger;)V

    goto :goto_0

    .line 448
    :cond_2
    const/4 v5, 0x1

    if-ne v5, v4, :cond_0

    .line 450
    cmp-long v0, v0, v2

    if-gez v0, :cond_3

    .line 452
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/fw;->a(Lcom/glympse/android/api/GEtaTrigger;)V

    goto :goto_0

    .line 456
    :cond_3
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/fw;->c(Lcom/glympse/android/api/GEtaTrigger;)V

    goto :goto_0
.end method

.method private b(Lcom/glympse/android/api/GTrigger;)V
    .locals 3

    .prologue
    .line 250
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->tZ:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 251
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->ua:Lcom/glympse/android/hal/GHashtable;

    invoke-interface {p1}, Lcom/glympse/android/api/GTrigger;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/fw;->d(Lcom/glympse/android/api/GTrigger;)V

    .line 257
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/16 v1, 0xf

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/glympse/android/lib/fw;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 258
    return-void
.end method

.method static synthetic b(Lcom/glympse/android/lib/fw;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/fw;->C(Ljava/lang/String;)V

    return-void
.end method

.method private bn()V
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 551
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->jZ:Lcom/glympse/android/lib/fb;

    invoke-virtual {v0}, Lcom/glympse/android/lib/fb;->load()Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 552
    if-nez v0, :cond_1

    .line 634
    :cond_0
    return-void

    .line 558
    :cond_1
    const/4 v2, 0x1

    .line 561
    const-string v1, "triggers"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v5

    .line 562
    if-eqz v5, :cond_0

    .line 564
    invoke-interface {v5}, Lcom/glympse/android/core/GPrimitive;->size()I

    move-result v6

    move v4, v3

    .line 565
    :goto_0
    if-ge v4, v6, :cond_0

    .line 567
    invoke-interface {v5, v4}, Lcom/glympse/android/core/GPrimitive;->get(I)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 571
    const-string v1, "type"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    long-to-int v7, v8

    .line 572
    packed-switch v7, :pswitch_data_0

    move v0, v2

    .line 565
    :cond_2
    :goto_1
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v2, v0

    goto :goto_0

    .line 576
    :pswitch_0
    new-instance v1, Lcom/glympse/android/lib/bo;

    invoke-direct {v1}, Lcom/glympse/android/lib/bo;-><init>()V

    .line 596
    :goto_2
    invoke-interface {v1, v0}, Lcom/glympse/android/api/GTrigger;->decode(Lcom/glympse/android/core/GPrimitive;)V

    .line 597
    invoke-interface {v1}, Lcom/glympse/android/api/GTrigger;->getId()Ljava/lang/String;

    move-result-object v0

    .line 598
    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v2

    .line 600
    goto :goto_1

    .line 581
    :pswitch_1
    new-instance v1, Lcom/glympse/android/lib/v;

    invoke-direct {v1}, Lcom/glympse/android/lib/v;-><init>()V

    goto :goto_2

    .line 586
    :pswitch_2
    new-instance v1, Lcom/glympse/android/lib/aw;

    invoke-direct {v1}, Lcom/glympse/android/lib/aw;-><init>()V

    goto :goto_2

    .line 604
    :cond_3
    packed-switch v7, :pswitch_data_1

    move v0, v2

    .line 623
    goto :goto_1

    :pswitch_3
    move-object v0, v1

    .line 608
    check-cast v0, Lcom/glympse/android/lib/GEtaTriggerPrivate;

    .line 609
    iget-object v7, p0, Lcom/glympse/android/lib/fw;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v7}, Lcom/glympse/android/lib/GGlympsePrivate;->getHistoryManager()Lcom/glympse/android/api/GHistoryManager;

    move-result-object v7

    invoke-interface {v0}, Lcom/glympse/android/lib/GEtaTriggerPrivate;->getEtaTicketId()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lcom/glympse/android/api/GHistoryManager;->findTicketByTicketId(Ljava/lang/String;)Lcom/glympse/android/api/GTicket;

    move-result-object v7

    .line 610
    if-eqz v7, :cond_4

    const/16 v8, 0x40

    invoke-interface {v7}, Lcom/glympse/android/api/GTicket;->getState()I

    move-result v9

    if-eq v8, v9, :cond_4

    .line 612
    invoke-interface {v0, v7}, Lcom/glympse/android/lib/GEtaTriggerPrivate;->setEtaTicket(Lcom/glympse/android/api/GTicket;)V

    move v0, v2

    .line 627
    :goto_3
    if-eqz v0, :cond_2

    .line 630
    invoke-direct {p0, v1}, Lcom/glympse/android/lib/fw;->b(Lcom/glympse/android/api/GTrigger;)V

    goto :goto_1

    :cond_4
    move v0, v3

    .line 619
    goto :goto_3

    .line 572
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 604
    :pswitch_data_1
    .packed-switch 0x3
        :pswitch_3
    .end packed-switch
.end method

.method private c(Lcom/glympse/android/api/GEtaTrigger;)V
    .locals 4

    .prologue
    .line 463
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->ud:Lcom/glympse/android/lib/fw$e;

    invoke-interface {p1}, Lcom/glympse/android/api/GEtaTrigger;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/glympse/android/lib/fw$e;->E(Ljava/lang/String;)V

    .line 465
    invoke-interface {p1}, Lcom/glympse/android/api/GEtaTrigger;->getEtaTicket()Lcom/glympse/android/api/GTicket;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/api/GTicket;->getEta()J

    move-result-wide v0

    .line 466
    invoke-interface {p1}, Lcom/glympse/android/api/GEtaTrigger;->getThreshold()J

    move-result-wide v2

    .line 467
    sub-long/2addr v0, v2

    .line 468
    iget-object v2, p0, Lcom/glympse/android/lib/fw;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v2}, Lcom/glympse/android/lib/GGlympsePrivate;->getTime()J

    move-result-wide v2

    add-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    add-long/2addr v0, v2

    .line 470
    iget-object v2, p0, Lcom/glympse/android/lib/fw;->ud:Lcom/glympse/android/lib/fw$e;

    invoke-interface {p1}, Lcom/glympse/android/api/GEtaTrigger;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v1, v3}, Lcom/glympse/android/lib/fw$e;->d(JLjava/lang/String;)V

    .line 471
    return-void
.end method

.method private c(Lcom/glympse/android/api/GTrigger;)V
    .locals 3

    .prologue
    .line 263
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/fw;->e(Lcom/glympse/android/api/GTrigger;)V

    .line 266
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->tZ:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GVector;->removeElement(Ljava/lang/Object;)Z

    .line 267
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->ua:Lcom/glympse/android/hal/GHashtable;

    invoke-interface {p1}, Lcom/glympse/android/api/GTrigger;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GHashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/16 v1, 0xf

    const/4 v2, 0x2

    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/glympse/android/lib/fw;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 271
    return-void
.end method

.method static synthetic c(Lcom/glympse/android/lib/fw;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/fw;->D(Ljava/lang/String;)V

    return-void
.end method

.method private d(Lcom/glympse/android/api/GEtaTrigger;)V
    .locals 2

    .prologue
    .line 475
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->ud:Lcom/glympse/android/lib/fw$e;

    invoke-interface {p1}, Lcom/glympse/android/api/GEtaTrigger;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/glympse/android/lib/fw$e;->E(Ljava/lang/String;)V

    .line 476
    return-void
.end method

.method private d(Lcom/glympse/android/api/GTrigger;)V
    .locals 4

    .prologue
    .line 275
    invoke-interface {p1}, Lcom/glympse/android/api/GTrigger;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 303
    :goto_0
    return-void

    .line 279
    :pswitch_0
    check-cast p1, Lcom/glympse/android/api/GGeoTrigger;

    .line 280
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->uc:Lcom/glympse/android/lib/fw$b;

    invoke-interface {p1}, Lcom/glympse/android/api/GGeoTrigger;->getRegion()Lcom/glympse/android/core/GRegion;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/fw$b;->startMonitoring(Lcom/glympse/android/core/GRegion;)V

    goto :goto_0

    :pswitch_1
    move-object v0, p1

    .line 285
    check-cast v0, Lcom/glympse/android/api/GChronoTrigger;

    .line 286
    iget-object v1, p0, Lcom/glympse/android/lib/fw;->ud:Lcom/glympse/android/lib/fw$e;

    invoke-interface {v0}, Lcom/glympse/android/api/GChronoTrigger;->getTime()J

    move-result-wide v2

    invoke-interface {p1}, Lcom/glympse/android/api/GTrigger;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Lcom/glympse/android/lib/fw$e;->d(JLjava/lang/String;)V

    goto :goto_0

    :pswitch_2
    move-object v0, p1

    .line 291
    check-cast v0, Lcom/glympse/android/api/GEtaTrigger;

    .line 294
    invoke-interface {v0}, Lcom/glympse/android/api/GEtaTrigger;->getEtaTicket()Lcom/glympse/android/api/GTicket;

    move-result-object v1

    .line 295
    iget-object v2, p0, Lcom/glympse/android/lib/fw;->ue:Lcom/glympse/android/lib/fw$a;

    invoke-interface {p1}, Lcom/glympse/android/api/GTrigger;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lcom/glympse/android/lib/fw$a;->a(Lcom/glympse/android/api/GTicket;Ljava/lang/String;)V

    .line 298
    invoke-direct {p0, v0}, Lcom/glympse/android/lib/fw;->b(Lcom/glympse/android/api/GEtaTrigger;)V

    goto :goto_0

    .line 275
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private e(Lcom/glympse/android/api/GTrigger;)V
    .locals 2

    .prologue
    .line 307
    invoke-interface {p1}, Lcom/glympse/android/api/GTrigger;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 329
    :goto_0
    return-void

    .line 311
    :pswitch_0
    check-cast p1, Lcom/glympse/android/api/GGeoTrigger;

    .line 312
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->uc:Lcom/glympse/android/lib/fw$b;

    invoke-interface {p1}, Lcom/glympse/android/api/GGeoTrigger;->getRegion()Lcom/glympse/android/core/GRegion;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/fw$b;->stopMonitoring(Lcom/glympse/android/core/GRegion;)V

    goto :goto_0

    :pswitch_1
    move-object v0, p1

    .line 317
    check-cast v0, Lcom/glympse/android/api/GChronoTrigger;

    .line 318
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->ud:Lcom/glympse/android/lib/fw$e;

    invoke-interface {p1}, Lcom/glympse/android/api/GTrigger;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/glympse/android/lib/fw$e;->E(Ljava/lang/String;)V

    goto :goto_0

    .line 323
    :pswitch_2
    check-cast p1, Lcom/glympse/android/api/GEtaTrigger;

    .line 324
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->ue:Lcom/glympse/android/lib/fw$a;

    invoke-interface {p1}, Lcom/glympse/android/api/GEtaTrigger;->getEtaTicket()Lcom/glympse/android/api/GTicket;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/glympse/android/lib/fw$a;->j(Lcom/glympse/android/api/GTicket;)V

    .line 325
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/fw;->d(Lcom/glympse/android/api/GEtaTrigger;)V

    goto :goto_0

    .line 307
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private f(Lcom/glympse/android/api/GTrigger;)V
    .locals 0

    .prologue
    .line 350
    invoke-virtual {p0, p1}, Lcom/glympse/android/lib/fw;->removeLocalTrigger(Lcom/glympse/android/api/GTrigger;)V

    .line 352
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/fw;->a(Lcom/glympse/android/api/GTrigger;)V

    .line 353
    return-void
.end method

.method private isStarted()Z
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private save()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v2, 0x0

    .line 638
    new-instance v3, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v3, v7}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 641
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->tZ:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->length()I

    move-result v4

    .line 642
    new-instance v5, Lcom/glympse/android/lib/Primitive;

    const/4 v0, 0x1

    invoke-direct {v5, v0}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    move v1, v2

    .line 643
    :goto_0
    if-ge v1, v4, :cond_0

    .line 645
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->tZ:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GTrigger;

    .line 646
    new-instance v6, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v6, v7}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 647
    invoke-interface {v0, v6, v2}, Lcom/glympse/android/api/GTrigger;->encode(Lcom/glympse/android/core/GPrimitive;I)V

    .line 648
    invoke-interface {v5, v6}, Lcom/glympse/android/core/GPrimitive;->put(Lcom/glympse/android/core/GPrimitive;)V

    .line 643
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 650
    :cond_0
    const-string v0, "triggers"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0, v5}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 652
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->jZ:Lcom/glympse/android/lib/fb;

    invoke-virtual {v0, v3}, Lcom/glympse/android/lib/fb;->save(Lcom/glympse/android/core/GPrimitive;)V

    .line 653
    return-void
.end method


# virtual methods
.method public addListener(Lcom/glympse/android/api/GEventListener;)Z
    .locals 1

    .prologue
    .line 932
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1}, Lcom/glympse/android/lib/CommonSink;->addListener(Lcom/glympse/android/api/GEventListener;)Z

    move-result v0

    return v0
.end method

.method public addLocalTrigger(Lcom/glympse/android/api/GTrigger;)V
    .locals 2

    .prologue
    .line 66
    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/glympse/android/lib/fw;->isStarted()Z

    move-result v0

    if-nez v0, :cond_1

    .line 99
    :cond_0
    :goto_0
    return-void

    .line 70
    :cond_1
    invoke-interface {p1}, Lcom/glympse/android/api/GTrigger;->getId()Ljava/lang/String;

    move-result-object v0

    .line 71
    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/glympse/android/lib/fw;->ua:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v1, v0}, Lcom/glympse/android/hal/GHashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 76
    const/4 v0, 0x3

    invoke-interface {p1}, Lcom/glympse/android/api/GTrigger;->getType()I

    move-result v1

    if-ne v0, v1, :cond_2

    move-object v0, p1

    .line 79
    check-cast v0, Lcom/glympse/android/api/GEtaTrigger;

    .line 80
    invoke-interface {v0}, Lcom/glympse/android/api/GEtaTrigger;->getEtaTicket()Lcom/glympse/android/api/GTicket;

    move-result-object v0

    .line 81
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/glympse/android/api/GTicket;->getState()I

    move-result v0

    and-int/lit8 v0, v0, 0x12

    if-eqz v0, :cond_0

    .line 87
    :cond_2
    iget-boolean v0, p0, Lcom/glympse/android/lib/fw;->ub:Z

    if-eqz v0, :cond_3

    .line 90
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/fw;->b(Lcom/glympse/android/api/GTrigger;)V

    .line 93
    invoke-direct {p0}, Lcom/glympse/android/lib/fw;->save()V

    goto :goto_0

    .line 97
    :cond_3
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->tY:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public associateContext(JLjava/lang/Object;)V
    .locals 1

    .prologue
    .line 957
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1, p2, p3}, Lcom/glympse/android/lib/CommonSink;->associateContext(JLjava/lang/Object;)V

    .line 958
    return-void
.end method

.method public clearContext(J)V
    .locals 1

    .prologue
    .line 967
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1, p2}, Lcom/glympse/android/lib/CommonSink;->clearContext(J)V

    .line 968
    return-void
.end method

.method protected da()V
    .locals 3

    .prologue
    .line 524
    iget-boolean v0, p0, Lcom/glympse/android/lib/fw;->ub:Z

    if-nez v0, :cond_1

    .line 526
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/glympse/android/lib/fw;->ub:Z

    .line 529
    invoke-direct {p0}, Lcom/glympse/android/lib/fw;->bn()V

    .line 532
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->tY:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->length()I

    move-result v2

    .line 533
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 535
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->tY:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GVector;->at(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GTrigger;

    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/fw;->addLocalTrigger(Lcom/glympse/android/api/GTrigger;)V

    .line 533
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 539
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->tY:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->removeAllElements()V

    .line 540
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/fw;->tY:Lcom/glympse/android/hal/GVector;

    .line 542
    :cond_1
    return-void
.end method

.method public deriveContext(Lcom/glympse/android/api/GEventSink;)V
    .locals 1

    .prologue
    .line 977
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1}, Lcom/glympse/android/lib/CommonSink;->deriveContext(Lcom/glympse/android/api/GEventSink;)V

    .line 978
    return-void
.end method

.method public eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V
    .locals 6

    .prologue
    .line 947
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/glympse/android/api/GEventSink;

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/glympse/android/lib/CommonSink;->eventsOccurred(Lcom/glympse/android/api/GEventSink;Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 948
    return-void
.end method

.method public getContext(J)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 962
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1, p2}, Lcom/glympse/android/lib/CommonSink;->getContext(J)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getContextKeys()Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Enumeration",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 972
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0}, Lcom/glympse/android/lib/CommonSink;->getContextKeys()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public getListeners()Lcom/glympse/android/core/GArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/glympse/android/core/GArray",
            "<",
            "Lcom/glympse/android/api/GEventListener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 942
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0}, Lcom/glympse/android/lib/CommonSink;->getListeners()Lcom/glympse/android/core/GArray;

    move-result-object v0

    return-object v0
.end method

.method public getLocalTriggers()Lcom/glympse/android/core/GArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/glympse/android/core/GArray",
            "<",
            "Lcom/glympse/android/api/GTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133
    iget-boolean v0, p0, Lcom/glympse/android/lib/fw;->ub:Z

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->tZ:Lcom/glympse/android/hal/GVector;

    .line 139
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->tY:Lcom/glympse/android/hal/GVector;

    goto :goto_0
.end method

.method public hasContext(J)Z
    .locals 1

    .prologue
    .line 952
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1, p2}, Lcom/glympse/android/lib/CommonSink;->hasContext(J)Z

    move-result v0

    return v0
.end method

.method public regionEntered(Lcom/glympse/android/core/GRegion;)V
    .locals 2

    .prologue
    .line 217
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->ua:Lcom/glympse/android/hal/GHashtable;

    invoke-interface {p1}, Lcom/glympse/android/core/GRegion;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GTrigger;

    .line 218
    if-nez v0, :cond_0

    .line 224
    :goto_0
    return-void

    .line 223
    :cond_0
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/glympse/android/lib/fw;->a(Lcom/glympse/android/api/GTrigger;I)V

    goto :goto_0
.end method

.method public regionLeft(Lcom/glympse/android/core/GRegion;)V
    .locals 2

    .prologue
    .line 229
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->ua:Lcom/glympse/android/hal/GHashtable;

    invoke-interface {p1}, Lcom/glympse/android/core/GRegion;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GTrigger;

    .line 230
    if-nez v0, :cond_0

    .line 236
    :goto_0
    return-void

    .line 235
    :cond_0
    const/4 v1, 0x2

    invoke-direct {p0, v0, v1}, Lcom/glympse/android/lib/fw;->a(Lcom/glympse/android/api/GTrigger;I)V

    goto :goto_0
.end method

.method public removeListener(Lcom/glympse/android/api/GEventListener;)Z
    .locals 1

    .prologue
    .line 937
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1}, Lcom/glympse/android/lib/CommonSink;->removeListener(Lcom/glympse/android/api/GEventListener;)Z

    move-result v0

    return v0
.end method

.method public removeLocalTrigger(Lcom/glympse/android/api/GTrigger;)V
    .locals 2

    .prologue
    .line 103
    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/glympse/android/lib/fw;->isStarted()Z

    move-result v0

    if-nez v0, :cond_1

    .line 128
    :cond_0
    :goto_0
    return-void

    .line 109
    :cond_1
    invoke-interface {p1}, Lcom/glympse/android/api/GTrigger;->getId()Ljava/lang/String;

    move-result-object v0

    .line 110
    iget-object v1, p0, Lcom/glympse/android/lib/fw;->ua:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v1, v0}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GTrigger;

    .line 111
    if-eqz v0, :cond_0

    .line 116
    iget-boolean v1, p0, Lcom/glympse/android/lib/fw;->ub:Z

    if-eqz v1, :cond_2

    .line 119
    invoke-direct {p0, v0}, Lcom/glympse/android/lib/fw;->c(Lcom/glympse/android/api/GTrigger;)V

    .line 122
    invoke-direct {p0}, Lcom/glympse/android/lib/fw;->save()V

    goto :goto_0

    .line 126
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->tY:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GVector;->removeElement(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public start(Lcom/glympse/android/lib/GGlympsePrivate;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 149
    iput-object p1, p0, Lcom/glympse/android/lib/fw;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 150
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getHistoryManager()Lcom/glympse/android/api/GHistoryManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/api/GHistoryManager;->isSynced()Z

    move-result v0

    iput-boolean v0, p0, Lcom/glympse/android/lib/fw;->ub:Z

    .line 153
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getConfigPrivate()Lcom/glympse/android/lib/GConfigPrivate;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/lib/GConfigPrivate;->useGlympseProximity()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    new-instance v0, Lcom/glympse/android/lib/fw$d;

    invoke-direct {v0, v4}, Lcom/glympse/android/lib/fw$d;-><init>(Lcom/glympse/android/lib/fw$1;)V

    iput-object v0, p0, Lcom/glympse/android/lib/fw;->uc:Lcom/glympse/android/lib/fw$b;

    .line 161
    :goto_0
    iget-object v1, p0, Lcom/glympse/android/lib/fw;->uc:Lcom/glympse/android/lib/fw$b;

    iget-object v2, p0, Lcom/glympse/android/lib/fw;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GProximityListener;

    invoke-interface {v1, v2, v0}, Lcom/glympse/android/lib/fw$b;->a(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/core/GProximityListener;)V

    .line 163
    new-instance v0, Lcom/glympse/android/lib/fw$e;

    invoke-direct {v0, v4}, Lcom/glympse/android/lib/fw$e;-><init>(Lcom/glympse/android/lib/fw$1;)V

    iput-object v0, p0, Lcom/glympse/android/lib/fw;->ud:Lcom/glympse/android/lib/fw$e;

    .line 164
    iget-object v1, p0, Lcom/glympse/android/lib/fw;->ud:Lcom/glympse/android/lib/fw$e;

    iget-object v2, p0, Lcom/glympse/android/lib/fw;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/fw;

    invoke-virtual {v1, v2, v0}, Lcom/glympse/android/lib/fw$e;->a(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/fw;)V

    .line 166
    new-instance v0, Lcom/glympse/android/lib/fw$a;

    invoke-direct {v0, v4}, Lcom/glympse/android/lib/fw$a;-><init>(Lcom/glympse/android/lib/fw$1;)V

    iput-object v0, p0, Lcom/glympse/android/lib/fw;->ue:Lcom/glympse/android/lib/fw$a;

    .line 167
    iget-object v1, p0, Lcom/glympse/android/lib/fw;->ue:Lcom/glympse/android/lib/fw$a;

    iget-object v2, p0, Lcom/glympse/android/lib/fw;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/fw;

    invoke-virtual {v1, v2, v0}, Lcom/glympse/android/lib/fw$a;->a(Lcom/glympse/android/api/GGlympse;Lcom/glympse/android/lib/fw;)V

    .line 170
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->jZ:Lcom/glympse/android/lib/fb;

    iget-object v1, p0, Lcom/glympse/android/lib/fw;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->getContextHolder()Lcom/glympse/android/hal/GContextHolder;

    move-result-object v1

    iget-object v2, p0, Lcom/glympse/android/lib/fw;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v2}, Lcom/glympse/android/lib/GGlympsePrivate;->getPrefix()Ljava/lang/String;

    move-result-object v2

    const-string v3, "triggers_v2"

    invoke-static {v3}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v4, v3}, Lcom/glympse/android/lib/fb;->a(Lcom/glympse/android/hal/GContextHolder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    iget-boolean v0, p0, Lcom/glympse/android/lib/fw;->ub:Z

    if-eqz v0, :cond_1

    .line 174
    invoke-direct {p0}, Lcom/glympse/android/lib/fw;->bn()V

    .line 180
    :goto_1
    return-void

    .line 159
    :cond_0
    new-instance v0, Lcom/glympse/android/lib/fw$c;

    invoke-direct {v0, v4}, Lcom/glympse/android/lib/fw$c;-><init>(Lcom/glympse/android/lib/fw$1;)V

    iput-object v0, p0, Lcom/glympse/android/lib/fw;->uc:Lcom/glympse/android/lib/fw$b;

    goto :goto_0

    .line 178
    :cond_1
    new-instance v0, Lcom/glympse/android/hal/GVector;

    invoke-direct {v0}, Lcom/glympse/android/hal/GVector;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/fw;->tY:Lcom/glympse/android/hal/GVector;

    goto :goto_1
.end method

.method public stop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 185
    :goto_0
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->tZ:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->tZ:Lcom/glympse/android/hal/GVector;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GTrigger;

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/fw;->c(Lcom/glympse/android/api/GTrigger;)V

    goto :goto_0

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->jZ:Lcom/glympse/android/lib/fb;

    invoke-virtual {v0}, Lcom/glympse/android/lib/fb;->stop()V

    .line 194
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->uc:Lcom/glympse/android/lib/fw$b;

    invoke-interface {v0}, Lcom/glympse/android/lib/fw$b;->stop()V

    .line 195
    iput-object v2, p0, Lcom/glympse/android/lib/fw;->uc:Lcom/glympse/android/lib/fw$b;

    .line 196
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->ud:Lcom/glympse/android/lib/fw$e;

    invoke-virtual {v0}, Lcom/glympse/android/lib/fw$e;->stop()V

    .line 197
    iput-object v2, p0, Lcom/glympse/android/lib/fw;->ud:Lcom/glympse/android/lib/fw$e;

    .line 198
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->ue:Lcom/glympse/android/lib/fw$a;

    invoke-virtual {v0}, Lcom/glympse/android/lib/fw$a;->stop()V

    .line 199
    iput-object v2, p0, Lcom/glympse/android/lib/fw;->ue:Lcom/glympse/android/lib/fw$a;

    .line 201
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->tY:Lcom/glympse/android/hal/GVector;

    if-eqz v0, :cond_1

    .line 203
    iget-object v0, p0, Lcom/glympse/android/lib/fw;->tY:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->removeAllElements()V

    .line 204
    iput-object v2, p0, Lcom/glympse/android/lib/fw;->tY:Lcom/glympse/android/hal/GVector;

    .line 207
    :cond_1
    iput-object v2, p0, Lcom/glympse/android/lib/fw;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 208
    return-void
.end method
