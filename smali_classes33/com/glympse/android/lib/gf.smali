.class Lcom/glympse/android/lib/gf;
.super Ljava/lang/Object;
.source "UserManager.java"

# interfaces
.implements Lcom/glympse/android/lib/GUserManagerPrivate;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/lib/gf$b;,
        Lcom/glympse/android/lib/gf$c;,
        Lcom/glympse/android/lib/gf$a;
    }
.end annotation


# instance fields
.field private _glympse:Lcom/glympse/android/lib/GGlympsePrivate;

.field private hu:Lcom/glympse/android/lib/GUserPrivate;

.field private jZ:Lcom/glympse/android/lib/fb;

.field private mz:Z

.field private nr:Lcom/glympse/android/hal/GHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GHashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/glympse/android/api/GTicket;",
            ">;"
        }
    .end annotation
.end field

.field private uY:Lcom/glympse/android/lib/GTicketPrivate;

.field private uZ:Lcom/glympse/android/lib/GTrackPrivate;

.field private va:Lcom/glympse/android/hal/GHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GHashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/glympse/android/core/GCommon;",
            ">;"
        }
    .end annotation
.end field

.field private vb:Lcom/glympse/android/hal/GVector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GVector",
            "<",
            "Lcom/glympse/android/api/GUser;",
            ">;"
        }
    .end annotation
.end field

.field private vc:Lcom/glympse/android/hal/GHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GHashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/glympse/android/api/GUser;",
            ">;"
        }
    .end annotation
.end field

.field private vd:Lcom/glympse/android/hal/GHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GHashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/glympse/android/api/GUser;",
            ">;"
        }
    .end annotation
.end field

.field private ve:Lcom/glympse/android/hal/GVector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GVector",
            "<",
            "Lcom/glympse/android/api/GUser;",
            ">;"
        }
    .end annotation
.end field

.field private vf:Lcom/glympse/android/lib/eq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/lib/eq",
            "<",
            "Lcom/glympse/android/api/GUser;",
            ">;"
        }
    .end annotation
.end field

.field private vg:Z

.field private vh:Z

.field private vi:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/glympse/android/api/GUser;",
            ">;"
        }
    .end annotation
.end field

.field private vj:Z

.field private vk:I

.field private vl:Z

.field private vm:Lcom/glympse/android/lib/gf$b;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    new-instance v0, Lcom/glympse/android/hal/GHashtable;

    invoke-direct {v0}, Lcom/glympse/android/hal/GHashtable;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/gf;->va:Lcom/glympse/android/hal/GHashtable;

    .line 109
    new-instance v0, Lcom/glympse/android/hal/GVector;

    invoke-direct {v0}, Lcom/glympse/android/hal/GVector;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/gf;->vb:Lcom/glympse/android/hal/GVector;

    .line 110
    new-instance v0, Lcom/glympse/android/hal/GHashtable;

    invoke-direct {v0}, Lcom/glympse/android/hal/GHashtable;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/gf;->vc:Lcom/glympse/android/hal/GHashtable;

    .line 111
    new-instance v0, Lcom/glympse/android/hal/GHashtable;

    invoke-direct {v0}, Lcom/glympse/android/hal/GHashtable;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/gf;->vd:Lcom/glympse/android/hal/GHashtable;

    .line 112
    new-instance v0, Lcom/glympse/android/hal/GHashtable;

    invoke-direct {v0}, Lcom/glympse/android/hal/GHashtable;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/gf;->nr:Lcom/glympse/android/hal/GHashtable;

    .line 113
    new-instance v0, Lcom/glympse/android/hal/GVector;

    invoke-direct {v0}, Lcom/glympse/android/hal/GVector;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/gf;->ve:Lcom/glympse/android/hal/GVector;

    .line 114
    new-instance v0, Lcom/glympse/android/lib/eq;

    invoke-direct {v0}, Lcom/glympse/android/lib/eq;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/gf;->vf:Lcom/glympse/android/lib/eq;

    .line 115
    iput-boolean v2, p0, Lcom/glympse/android/lib/gf;->vg:Z

    .line 116
    iput-boolean v2, p0, Lcom/glympse/android/lib/gf;->vh:Z

    .line 117
    new-instance v0, Lcom/glympse/android/lib/gf$c;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/gf$c;-><init>(Lcom/glympse/android/lib/gf$1;)V

    iput-object v0, p0, Lcom/glympse/android/lib/gf;->vi:Ljava/util/Comparator;

    .line 118
    iput-boolean v2, p0, Lcom/glympse/android/lib/gf;->mz:Z

    .line 119
    iput-boolean v3, p0, Lcom/glympse/android/lib/gf;->vj:Z

    .line 120
    iput v3, p0, Lcom/glympse/android/lib/gf;->vk:I

    .line 121
    new-instance v0, Lcom/glympse/android/lib/fb;

    invoke-direct {v0}, Lcom/glympse/android/lib/fb;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/gf;->jZ:Lcom/glympse/android/lib/fb;

    .line 122
    iput-boolean v3, p0, Lcom/glympse/android/lib/gf;->vl:Z

    .line 123
    return-void
.end method

.method private J(Ljava/lang/String;)V
    .locals 14

    .prologue
    .line 892
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->jZ:Lcom/glympse/android/lib/fb;

    invoke-virtual {v0}, Lcom/glympse/android/lib/fb;->load()Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 893
    if-nez v0, :cond_1

    .line 990
    :cond_0
    :goto_0
    return-void

    .line 900
    :cond_1
    const-string v1, "self"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v1

    .line 901
    if-eqz v1, :cond_2

    .line 904
    new-instance v2, Lcom/glympse/android/lib/gb;

    invoke-direct {v2}, Lcom/glympse/android/lib/gb;-><init>()V

    .line 905
    invoke-interface {v2, v1}, Lcom/glympse/android/lib/GUserPrivate;->decode(Lcom/glympse/android/core/GPrimitive;)V

    .line 908
    invoke-interface {v2}, Lcom/glympse/android/lib/GUserPrivate;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/glympse/android/hal/Helpers;->safeEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 911
    iget-object v1, p0, Lcom/glympse/android/lib/gf;->hu:Lcom/glympse/android/lib/GUserPrivate;

    iget-object v3, p0, Lcom/glympse/android/lib/gf;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1, v2, v3}, Lcom/glympse/android/lib/GUserPrivate;->merge(Lcom/glympse/android/lib/GUserPrivate;Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 924
    :cond_2
    iget-boolean v1, p0, Lcom/glympse/android/lib/gf;->vj:Z

    if-eqz v1, :cond_0

    .line 927
    iget-object v1, p0, Lcom/glympse/android/lib/gf;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->getTime()J

    move-result-wide v4

    .line 930
    const-string v1, "users"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v6

    .line 931
    if-eqz v6, :cond_0

    .line 933
    invoke-interface {v6}, Lcom/glympse/android/core/GPrimitive;->size()I

    move-result v7

    .line 934
    const/4 v0, 0x0

    move v3, v0

    :goto_1
    if-ge v3, v7, :cond_0

    .line 937
    invoke-interface {v6, v3}, Lcom/glympse/android/core/GPrimitive;->get(I)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 938
    new-instance v8, Lcom/glympse/android/lib/gb;

    invoke-direct {v8}, Lcom/glympse/android/lib/gb;-><init>()V

    .line 939
    invoke-interface {v8, v0}, Lcom/glympse/android/lib/GUserPrivate;->decode(Lcom/glympse/android/core/GPrimitive;)V

    .line 942
    invoke-interface {v8}, Lcom/glympse/android/lib/GUserPrivate;->getTickets()Lcom/glympse/android/core/GArray;

    move-result-object v9

    .line 943
    invoke-interface {v9}, Lcom/glympse/android/core/GArray;->length()I

    move-result v2

    .line 944
    const/4 v1, 0x0

    :goto_2
    if-ge v1, v2, :cond_5

    .line 946
    invoke-interface {v9, v1}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GTicketPrivate;

    .line 952
    const/16 v10, 0x8

    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketPrivate;->getState()I

    move-result v11

    if-ne v10, v11, :cond_3

    .line 954
    const/16 v10, 0x10

    invoke-interface {v0, v10}, Lcom/glympse/android/lib/GTicketPrivate;->setState(I)Z

    .line 958
    :cond_3
    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketPrivate;->getExpireTime()J

    move-result-wide v10

    sub-long v10, v4, v10

    .line 959
    const-wide/32 v12, 0xa4cb800

    cmp-long v10, v10, v12

    if-lez v10, :cond_7

    .line 962
    invoke-interface {v8, v0}, Lcom/glympse/android/lib/GUserPrivate;->removeTicket(Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 963
    add-int/lit8 v0, v1, -0x1

    .line 964
    add-int/lit8 v1, v2, -0x1

    .line 944
    :goto_3
    add-int/lit8 v0, v0, 0x1

    move v2, v1

    move v1, v0

    goto :goto_2

    .line 916
    :cond_4
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->jZ:Lcom/glympse/android/lib/fb;

    invoke-virtual {v0}, Lcom/glympse/android/lib/fb;->remove()V

    goto/16 :goto_0

    .line 974
    :cond_5
    invoke-interface {v9}, Lcom/glympse/android/core/GArray;->length()I

    move-result v0

    if-nez v0, :cond_6

    .line 934
    :goto_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 981
    :cond_6
    invoke-direct {p0, v8}, Lcom/glympse/android/lib/gf;->a(Lcom/glympse/android/lib/GUserPrivate;)V

    .line 982
    invoke-virtual {p0, v8}, Lcom/glympse/android/lib/gf;->addUser(Lcom/glympse/android/lib/GUserPrivate;)V

    .line 983
    invoke-virtual {p0, v8}, Lcom/glympse/android/lib/gf;->addStandaloneUser(Lcom/glympse/android/lib/GUserPrivate;)V

    goto :goto_4

    :cond_7
    move v0, v1

    move v1, v2

    goto :goto_3
.end method

.method private K(Ljava/lang/String;)Lcom/glympse/android/api/GUser;
    .locals 1

    .prologue
    .line 1098
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->vd:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GUser;

    return-object v0
.end method

.method private a(Lcom/glympse/android/lib/GUserPrivate;)V
    .locals 2

    .prologue
    .line 639
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_0

    .line 641
    invoke-interface {p1}, Lcom/glympse/android/lib/GUserPrivate;->getAvatar()Lcom/glympse/android/api/GImage;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GImagePrivate;

    .line 642
    iget-object v1, p0, Lcom/glympse/android/lib/gf;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->getImageCache()Lcom/glympse/android/lib/GImageCache;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GImagePrivate;->attachCache(Lcom/glympse/android/lib/GImageCache;)V

    .line 646
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->va:Lcom/glympse/android/hal/GHashtable;

    invoke-interface {p1}, Lcom/glympse/android/lib/GUserPrivate;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 647
    return-void
.end method

.method private a(Ljava/util/Enumeration;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Enumeration",
            "<",
            "Lcom/glympse/android/api/GUser;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 262
    :cond_0
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 264
    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GUserPrivate;

    .line 267
    invoke-interface {v0}, Lcom/glympse/android/lib/GUserPrivate;->getActiveStandalone()Lcom/glympse/android/api/GTicket;

    move-result-object v0

    .line 271
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/glympse/android/api/GTicket;->isCompleted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 273
    const/4 v0, 0x1

    .line 276
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/glympse/android/hal/GDirectory;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 406
    const-string v0, "users_v2"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/glympse/android/lib/fb;->e(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/glympse/android/hal/GDirectory;->deleteFile(Ljava/lang/String;)Z

    .line 407
    return-void
.end method

.method private b(Lcom/glympse/android/lib/GUserPrivate;)V
    .locals 4

    .prologue
    .line 1046
    invoke-interface {p1}, Lcom/glympse/android/lib/GUserPrivate;->isSelf()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1048
    invoke-interface {p1}, Lcom/glympse/android/lib/GUserPrivate;->getTickets()Lcom/glympse/android/core/GArray;

    move-result-object v2

    .line 1049
    invoke-interface {v2}, Lcom/glympse/android/core/GArray;->length()I

    move-result v3

    .line 1050
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 1052
    invoke-interface {v2, v1}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GTicket;

    invoke-virtual {p0, p1, v0}, Lcom/glympse/android/lib/gf;->addTicketToInviteIndex(Lcom/glympse/android/api/GUser;Lcom/glympse/android/api/GTicket;)V

    .line 1050
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1055
    :cond_0
    return-void
.end method

.method private c(Lcom/glympse/android/lib/GUserPrivate;)V
    .locals 4

    .prologue
    .line 1072
    invoke-interface {p1}, Lcom/glympse/android/lib/GUserPrivate;->isSelf()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1074
    invoke-interface {p1}, Lcom/glympse/android/lib/GUserPrivate;->getTickets()Lcom/glympse/android/core/GArray;

    move-result-object v2

    .line 1075
    invoke-interface {v2}, Lcom/glympse/android/core/GArray;->length()I

    move-result v3

    .line 1076
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 1078
    invoke-interface {v2, v1}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GTicket;

    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/gf;->removeTicketFromInviteIndex(Lcom/glympse/android/api/GTicket;)V

    .line 1076
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1081
    :cond_0
    return-void
.end method

.method private clear()V
    .locals 2

    .prologue
    .line 1031
    :goto_0
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->vb:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1033
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->vb:Lcom/glympse/android/hal/GVector;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GUserPrivate;

    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/gf;->removeUser(Lcom/glympse/android/lib/GUserPrivate;)V

    goto :goto_0

    .line 1035
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->ve:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->removeAllElements()V

    .line 1036
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->vf:Lcom/glympse/android/lib/eq;

    invoke-virtual {v0}, Lcom/glympse/android/lib/eq;->ap()V

    .line 1037
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/gf;->hu:Lcom/glympse/android/lib/GUserPrivate;

    .line 1038
    return-void
.end method

.method private dd()V
    .locals 2

    .prologue
    .line 801
    iget-boolean v0, p0, Lcom/glympse/android/lib/gf;->mz:Z

    if-nez v0, :cond_1

    .line 817
    :cond_0
    :goto_0
    return-void

    .line 805
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/glympse/android/lib/gf;->mz:Z

    .line 810
    iget-boolean v0, p0, Lcom/glympse/android/lib/gf;->vh:Z

    if-eqz v0, :cond_0

    .line 813
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->vb:Lcom/glympse/android/hal/GVector;

    iget-object v1, p0, Lcom/glympse/android/lib/gf;->vi:Ljava/util/Comparator;

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GVector;->sort(Ljava/util/Comparator;)V

    goto :goto_0
.end method

.method private de()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v2, 0x1

    .line 995
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->hu:Lcom/glympse/android/lib/GUserPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GUserPrivate;->getId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/glympse/android/lib/gf;->vl:Z

    if-eqz v0, :cond_1

    .line 1026
    :cond_0
    :goto_0
    return-void

    .line 1001
    :cond_1
    new-instance v3, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v3, v7}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 1004
    new-instance v0, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v0, v7}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 1005
    iget-object v1, p0, Lcom/glympse/android/lib/gf;->hu:Lcom/glympse/android/lib/GUserPrivate;

    const/4 v4, 0x0

    invoke-interface {v1, v0, v4}, Lcom/glympse/android/lib/GUserPrivate;->encode(Lcom/glympse/android/core/GPrimitive;I)V

    .line 1006
    const-string v1, "self"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v1, v0}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 1009
    iget-boolean v0, p0, Lcom/glympse/android/lib/gf;->vj:Z

    if-eqz v0, :cond_3

    .line 1012
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->ve:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v4

    .line 1013
    new-instance v5, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v5, v2}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    move v1, v2

    .line 1014
    :goto_1
    if-ge v1, v4, :cond_2

    .line 1016
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->ve:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GUserPrivate;

    .line 1017
    new-instance v6, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v6, v7}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 1018
    invoke-interface {v0, v6, v2}, Lcom/glympse/android/lib/GUserPrivate;->encode(Lcom/glympse/android/core/GPrimitive;I)V

    .line 1019
    invoke-interface {v5, v6}, Lcom/glympse/android/core/GPrimitive;->put(Lcom/glympse/android/core/GPrimitive;)V

    .line 1014
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1021
    :cond_2
    const-string v0, "users"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0, v5}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 1025
    :cond_3
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->jZ:Lcom/glympse/android/lib/fb;

    invoke-virtual {v0, v3}, Lcom/glympse/android/lib/fb;->save(Lcom/glympse/android/core/GPrimitive;)V

    goto :goto_0
.end method

.method private load(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 886
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/gf;->J(Ljava/lang/String;)V

    .line 887
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/glympse/android/lib/gf;->vl:Z

    .line 888
    return-void
.end method

.method private o(Ljava/lang/String;)Lcom/glympse/android/api/GTicket;
    .locals 1

    .prologue
    .line 1103
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->nr:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GTicket;

    return-object v0
.end method


# virtual methods
.method public addStandaloneUser(Lcom/glympse/android/lib/GUserPrivate;)V
    .locals 4

    .prologue
    .line 704
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->ve:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 707
    invoke-interface {p1}, Lcom/glympse/android/lib/GUserPrivate;->addReference()V

    .line 710
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/gf;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const v2, 0x10002

    const/16 v3, 0x20

    invoke-interface {v0, v1, v2, v3, p1}, Lcom/glympse/android/lib/GGlympsePrivate;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 713
    invoke-direct {p0}, Lcom/glympse/android/lib/gf;->de()V

    .line 714
    return-void
.end method

.method public addTicketToInviteIndex(Lcom/glympse/android/api/GUser;Lcom/glympse/android/api/GTicket;)V
    .locals 2

    .prologue
    .line 1059
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->uY:Lcom/glympse/android/lib/GTicketPrivate;

    if-eq p2, v0, :cond_0

    .line 1061
    invoke-interface {p2}, Lcom/glympse/android/api/GTicket;->getCode()Ljava/lang/String;

    move-result-object v0

    .line 1062
    if-eqz v0, :cond_0

    .line 1064
    iget-object v1, p0, Lcom/glympse/android/lib/gf;->vd:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v1, v0, p1}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1065
    iget-object v1, p0, Lcom/glympse/android/lib/gf;->nr:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v1, v0, p2}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1068
    :cond_0
    return-void
.end method

.method public addUser(Lcom/glympse/android/lib/GUserPrivate;)V
    .locals 4

    .prologue
    .line 653
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->vb:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 656
    invoke-interface {p1}, Lcom/glympse/android/lib/GUserPrivate;->getId()Ljava/lang/String;

    move-result-object v0

    .line 657
    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 659
    iget-object v1, p0, Lcom/glympse/android/lib/gf;->vc:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v1, v0, p1}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 663
    :cond_0
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/gf;->b(Lcom/glympse/android/lib/GUserPrivate;)V

    .line 666
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {p1, v0}, Lcom/glympse/android/lib/GUserPrivate;->setGlympse(Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 669
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/gf;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/4 v2, 0x1

    const/16 v3, 0x2000

    invoke-interface {v0, v1, v2, v3, p1}, Lcom/glympse/android/lib/GGlympsePrivate;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 672
    invoke-interface {p1}, Lcom/glympse/android/lib/GUserPrivate;->orderChanged()V

    .line 677
    return-void
.end method

.method public anyActive()Z
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->ve:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/gf;->a(Ljava/util/Enumeration;)Z

    move-result v0

    return v0
.end method

.method public anyActiveTracked()Z
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->vf:Lcom/glympse/android/lib/eq;

    invoke-virtual {v0}, Lcom/glympse/android/lib/eq;->cC()Ljava/util/Enumeration;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/gf;->a(Ljava/util/Enumeration;)Z

    move-result v0

    return v0
.end method

.method public enablePersistence(Z)V
    .locals 1

    .prologue
    .line 164
    iput-boolean p1, p0, Lcom/glympse/android/lib/gf;->vj:Z

    .line 165
    iget-boolean v0, p0, Lcom/glympse/android/lib/gf;->vj:Z

    if-nez v0, :cond_0

    .line 168
    invoke-virtual {p0}, Lcom/glympse/android/lib/gf;->save()V

    .line 170
    :cond_0
    return-void
.end method

.method public enableSorting(Z)V
    .locals 0

    .prologue
    .line 147
    iput-boolean p1, p0, Lcom/glympse/android/lib/gf;->vh:Z

    .line 148
    invoke-virtual {p0}, Lcom/glympse/android/lib/gf;->orderChanged()V

    .line 149
    return-void
.end method

.method public extractFromCache(Ljava/lang/String;Z)Lcom/glympse/android/lib/GUserPrivate;
    .locals 1

    .prologue
    .line 615
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->va:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GUserPrivate;

    .line 616
    if-eqz v0, :cond_1

    .line 633
    :cond_0
    :goto_0
    return-object v0

    .line 622
    :cond_1
    new-instance v0, Lcom/glympse/android/lib/gb;

    invoke-direct {v0}, Lcom/glympse/android/lib/gb;-><init>()V

    .line 623
    invoke-interface {v0, p1}, Lcom/glympse/android/lib/GUserPrivate;->setId(Ljava/lang/String;)V

    .line 626
    invoke-direct {p0, v0}, Lcom/glympse/android/lib/gf;->a(Lcom/glympse/android/lib/GUserPrivate;)V

    .line 629
    if-eqz p2, :cond_0

    .line 631
    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/gf;->refreshUser(Lcom/glympse/android/lib/GUserPrivate;)V

    goto :goto_0
.end method

.method public findTicketByInviteCode(Ljava/lang/String;)Lcom/glympse/android/api/GTicket;
    .locals 1

    .prologue
    .line 300
    invoke-static {p1}, Lcom/glympse/android/lib/TicketCode;->cleanupInviteCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 301
    if-nez v0, :cond_0

    .line 303
    const/4 v0, 0x0

    .line 305
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, v0}, Lcom/glympse/android/lib/gf;->o(Ljava/lang/String;)Lcom/glympse/android/api/GTicket;

    move-result-object v0

    goto :goto_0
.end method

.method public findUserByInviteCode(Ljava/lang/String;)Lcom/glympse/android/api/GUser;
    .locals 1

    .prologue
    .line 290
    invoke-static {p1}, Lcom/glympse/android/lib/TicketCode;->cleanupInviteCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 291
    if-nez v0, :cond_0

    .line 293
    const/4 v0, 0x0

    .line 295
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, v0}, Lcom/glympse/android/lib/gf;->K(Ljava/lang/String;)Lcom/glympse/android/api/GUser;

    move-result-object v0

    goto :goto_0
.end method

.method public findUserByUserId(Ljava/lang/String;)Lcom/glympse/android/api/GUser;
    .locals 1

    .prologue
    .line 281
    invoke-static {p1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 283
    const/4 v0, 0x0

    .line 285
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->vc:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GUser;

    goto :goto_0
.end method

.method public getNumTrackers(Lcom/glympse/android/api/GUser;)I
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->vf:Lcom/glympse/android/lib/eq;

    invoke-virtual {v0, p1}, Lcom/glympse/android/lib/eq;->j(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getSelf()Lcom/glympse/android/api/GUser;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->hu:Lcom/glympse/android/lib/GUserPrivate;

    return-object v0
.end method

.method public getSelfTrack()Lcom/glympse/android/api/GTrack;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->uZ:Lcom/glympse/android/lib/GTrackPrivate;

    return-object v0
.end method

.method public getStandaloneUsers()Lcom/glympse/android/core/GArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/glympse/android/core/GArray",
            "<",
            "Lcom/glympse/android/api/GUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 158
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->ve:Lcom/glympse/android/hal/GVector;

    return-object v0
.end method

.method public getTracking()Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Enumeration",
            "<",
            "Lcom/glympse/android/api/GUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 252
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->vf:Lcom/glympse/android/lib/eq;

    invoke-virtual {v0}, Lcom/glympse/android/lib/eq;->cC()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public getUserTrackingMode()I
    .locals 1

    .prologue
    .line 179
    iget v0, p0, Lcom/glympse/android/lib/gf;->vk:I

    return v0
.end method

.method public getUsers()Lcom/glympse/android/core/GArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/glympse/android/core/GArray",
            "<",
            "Lcom/glympse/android/api/GUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 131
    invoke-direct {p0}, Lcom/glympse/android/lib/gf;->dd()V

    .line 132
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->vb:Lcom/glympse/android/hal/GVector;

    return-object v0
.end method

.method public isStarted()Z
    .locals 1

    .prologue
    .line 368
    iget-boolean v0, p0, Lcom/glympse/android/lib/gf;->vg:Z

    return v0
.end method

.method public modifyUser(Lcom/glympse/android/lib/GUserPrivate;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 771
    new-instance v0, Lcom/glympse/android/lib/gj;

    iget-object v1, p0, Lcom/glympse/android/lib/gf;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct {v0, v1, p1, p2, p3}, Lcom/glympse/android/lib/gj;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/GUserPrivate;Ljava/lang/String;Ljava/lang/String;)V

    .line 772
    iget-object v1, p0, Lcom/glympse/android/lib/gf;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->getServerPost()Lcom/glympse/android/lib/GServerPost;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2}, Lcom/glympse/android/lib/GServerPost;->invokeEndpoint(Lcom/glympse/android/lib/GApiEndpoint;Z)V

    .line 773
    return-void
.end method

.method public orderChanged()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 782
    iget-boolean v0, p0, Lcom/glympse/android/lib/gf;->mz:Z

    if-eqz v0, :cond_1

    .line 796
    :cond_0
    :goto_0
    return-void

    .line 788
    :cond_1
    iput-boolean v4, p0, Lcom/glympse/android/lib/gf;->mz:Z

    .line 792
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_0

    .line 794
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/gf;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/high16 v2, 0x10000

    const/4 v3, 0x0

    invoke-interface {v0, v1, v4, v2, v3}, Lcom/glympse/android/lib/GGlympsePrivate;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    goto :goto_0
.end method

.method public refreshUser(Lcom/glympse/android/lib/GUserPrivate;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 755
    invoke-interface {p1}, Lcom/glympse/android/lib/GUserPrivate;->isSelf()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 757
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getNotificationCenter()Lcom/glympse/android/lib/GNotificationCenter;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/glympse/android/lib/GNotificationCenter;->skipSync(I)V

    .line 762
    :cond_0
    invoke-interface {p1}, Lcom/glympse/android/lib/GUserPrivate;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 764
    new-instance v0, Lcom/glympse/android/lib/gk;

    iget-object v1, p0, Lcom/glympse/android/lib/gf;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct {v0, v1, p1}, Lcom/glympse/android/lib/gk;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/GUserPrivate;)V

    .line 765
    iget-object v1, p0, Lcom/glympse/android/lib/gf;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->getServerPost()Lcom/glympse/android/lib/GServerPost;

    move-result-object v1

    invoke-interface {v1, v0, v2}, Lcom/glympse/android/lib/GServerPost;->invokeEndpoint(Lcom/glympse/android/lib/GApiEndpoint;Z)V

    .line 767
    :cond_1
    return-void
.end method

.method public removeStandaloneUser(Lcom/glympse/android/lib/GUserPrivate;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 719
    invoke-interface {p1}, Lcom/glympse/android/lib/GUserPrivate;->getTickets()Lcom/glympse/android/core/GArray;

    move-result-object v3

    .line 720
    invoke-interface {v3}, Lcom/glympse/android/core/GArray;->length()I

    move-result v4

    .line 721
    new-instance v5, Lcom/glympse/android/hal/GVector;

    invoke-direct {v5, v4}, Lcom/glympse/android/hal/GVector;-><init>(I)V

    move v2, v1

    .line 722
    :goto_0
    if-ge v2, v4, :cond_1

    .line 724
    invoke-interface {v3, v2}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GTicketPrivate;

    .line 725
    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketPrivate;->isStandalone()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 727
    invoke-virtual {v5, v0}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 722
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 731
    :cond_1
    invoke-virtual {v5}, Lcom/glympse/android/hal/GVector;->length()I

    move-result v2

    .line 732
    :goto_1
    if-ge v1, v2, :cond_2

    .line 734
    invoke-virtual {v5, v1}, Lcom/glympse/android/hal/GVector;->at(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GTicketPrivate;

    .line 735
    invoke-interface {p1, v0}, Lcom/glympse/android/lib/GUserPrivate;->removeTicket(Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 732
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 739
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->ve:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GVector;->removeElement(Ljava/lang/Object;)Z

    .line 740
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->vf:Lcom/glympse/android/lib/eq;

    invoke-virtual {v0, p1}, Lcom/glympse/android/lib/eq;->i(Ljava/lang/Object;)I

    .line 743
    invoke-interface {p1}, Lcom/glympse/android/lib/GUserPrivate;->removeReference()V

    .line 746
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/gf;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const v2, 0x10002

    const/16 v3, 0x40

    invoke-interface {v0, v1, v2, v3, p1}, Lcom/glympse/android/lib/GGlympsePrivate;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 749
    invoke-direct {p0}, Lcom/glympse/android/lib/gf;->de()V

    .line 750
    return-void
.end method

.method public removeTicketFromInviteIndex(Lcom/glympse/android/api/GTicket;)V
    .locals 2

    .prologue
    .line 1085
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->uY:Lcom/glympse/android/lib/GTicketPrivate;

    if-eq p1, v0, :cond_0

    .line 1087
    invoke-interface {p1}, Lcom/glympse/android/api/GTicket;->getCode()Ljava/lang/String;

    move-result-object v0

    .line 1088
    if-eqz v0, :cond_0

    .line 1090
    iget-object v1, p0, Lcom/glympse/android/lib/gf;->vd:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v1, v0}, Lcom/glympse/android/hal/GHashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1091
    iget-object v1, p0, Lcom/glympse/android/lib/gf;->nr:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v1, v0}, Lcom/glympse/android/hal/GHashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1094
    :cond_0
    return-void
.end method

.method public removeUser(Lcom/glympse/android/lib/GUserPrivate;)V
    .locals 4

    .prologue
    .line 682
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lcom/glympse/android/lib/GUserPrivate;->setGlympse(Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 685
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->vb:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GVector;->removeElement(Ljava/lang/Object;)Z

    .line 688
    invoke-interface {p1}, Lcom/glympse/android/lib/GUserPrivate;->getId()Ljava/lang/String;

    move-result-object v0

    .line 689
    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 691
    iget-object v1, p0, Lcom/glympse/android/lib/gf;->vc:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v1, v0}, Lcom/glympse/android/hal/GHashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 695
    :cond_0
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/gf;->c(Lcom/glympse/android/lib/GUserPrivate;)V

    .line 698
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/gf;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/4 v2, 0x1

    const/16 v3, 0x4000

    invoke-interface {v0, v1, v2, v3, p1}, Lcom/glympse/android/lib/GGlympsePrivate;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 699
    return-void
.end method

.method public resolveUser(Lcom/glympse/android/lib/GUserPrivate;)Lcom/glympse/android/lib/GUserPrivate;
    .locals 2

    .prologue
    .line 590
    invoke-interface {p1}, Lcom/glympse/android/lib/GUserPrivate;->getId()Ljava/lang/String;

    move-result-object v0

    .line 591
    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 593
    const/4 p1, 0x0

    .line 608
    :goto_0
    return-object p1

    .line 597
    :cond_0
    iget-object v1, p0, Lcom/glympse/android/lib/gf;->va:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v1, v0}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GUserPrivate;

    .line 598
    if-nez v0, :cond_1

    .line 601
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/gf;->a(Lcom/glympse/android/lib/GUserPrivate;)V

    goto :goto_0

    .line 607
    :cond_1
    iget-object v1, p0, Lcom/glympse/android/lib/gf;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0, p1, v1}, Lcom/glympse/android/lib/GUserPrivate;->merge(Lcom/glympse/android/lib/GUserPrivate;Lcom/glympse/android/lib/GGlympsePrivate;)V

    move-object p1, v0

    .line 608
    goto :goto_0
.end method

.method public save()V
    .locals 1

    .prologue
    .line 397
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-nez v0, :cond_0

    .line 402
    :goto_0
    return-void

    .line 401
    :cond_0
    invoke-direct {p0}, Lcom/glympse/android/lib/gf;->de()V

    goto :goto_0
.end method

.method public setActive(Z)V
    .locals 4

    .prologue
    .line 374
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->vb:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->elements()Ljava/util/Enumeration;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 376
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GUser;

    .line 377
    invoke-interface {v0}, Lcom/glympse/android/api/GUser;->isSelf()Z

    move-result v1

    if-nez v1, :cond_0

    .line 379
    invoke-interface {v0}, Lcom/glympse/android/api/GUser;->getTickets()Lcom/glympse/android/core/GArray;

    move-result-object v3

    .line 380
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v3}, Lcom/glympse/android/core/GArray;->length()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 382
    invoke-interface {v3, v1}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GTicketPrivate;

    .line 383
    invoke-interface {v0, p1}, Lcom/glympse/android/lib/GTicketPrivate;->setActive(Z)V

    .line 380
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 388
    :cond_1
    if-nez p1, :cond_2

    .line 391
    invoke-virtual {p0}, Lcom/glympse/android/lib/gf;->save()V

    .line 393
    :cond_2
    return-void
.end method

.method public setSelfLocation(Lcom/glympse/android/core/GLocation;ZZ)V
    .locals 13

    .prologue
    .line 515
    iget-object v1, p0, Lcom/glympse/android/lib/gf;->hu:Lcom/glympse/android/lib/GUserPrivate;

    invoke-interface {v1, p1}, Lcom/glympse/android/lib/GUserPrivate;->setLocation(Lcom/glympse/android/core/GLocation;)V

    .line 518
    const/16 v1, 0x400

    .line 521
    if-eqz p3, :cond_3

    .line 524
    const v1, 0x8400

    move v9, v1

    .line 527
    :goto_0
    if-eqz p2, :cond_2

    .line 529
    iget-object v1, p0, Lcom/glympse/android/lib/gf;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->getTime()J

    move-result-wide v2

    .line 530
    iget-object v1, p0, Lcom/glympse/android/lib/gf;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->getConfig()Lcom/glympse/android/api/GConfig;

    move-result-object v1

    invoke-interface {v1}, Lcom/glympse/android/api/GConfig;->getTrackTrimLength()J

    move-result-wide v5

    .line 533
    iget-object v1, p0, Lcom/glympse/android/lib/gf;->uZ:Lcom/glympse/android/lib/GTrackPrivate;

    invoke-interface {v1, p1, v2, v3}, Lcom/glympse/android/lib/GTrackPrivate;->add(Lcom/glympse/android/core/GLocation;J)Z

    .line 535
    iget-object v1, p0, Lcom/glympse/android/lib/gf;->uZ:Lcom/glympse/android/lib/GTrackPrivate;

    const/4 v4, 0x0

    invoke-interface/range {v1 .. v6}, Lcom/glympse/android/lib/GTrackPrivate;->trim(JZJ)Z

    .line 538
    if-eqz p3, :cond_0

    .line 540
    iget-object v1, p0, Lcom/glympse/android/lib/gf;->uY:Lcom/glympse/android/lib/GTicketPrivate;

    iget-object v4, p0, Lcom/glympse/android/lib/gf;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/4 v7, 0x4

    const/16 v8, 0x80

    iget-object v10, p0, Lcom/glympse/android/lib/gf;->uY:Lcom/glympse/android/lib/GTicketPrivate;

    invoke-interface {v1, v4, v7, v8, v10}, Lcom/glympse/android/lib/GTicketPrivate;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 545
    :cond_0
    iget-object v1, p0, Lcom/glympse/android/lib/gf;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->getHistoryManager()Lcom/glympse/android/api/GHistoryManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/glympse/android/api/GHistoryManager;->getTickets()Lcom/glympse/android/core/GArray;

    move-result-object v10

    .line 546
    invoke-interface {v10}, Lcom/glympse/android/core/GArray;->length()I

    move-result v11

    .line 547
    const/4 v1, 0x0

    move v8, v1

    :goto_1
    if-ge v8, v11, :cond_2

    .line 549
    invoke-interface {v10, v8}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/glympse/android/lib/GTicketPrivate;

    .line 552
    invoke-interface {v7}, Lcom/glympse/android/lib/GTicketPrivate;->isActive()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v7}, Lcom/glympse/android/lib/GTicketPrivate;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 554
    invoke-interface {v7}, Lcom/glympse/android/lib/GTicketPrivate;->getTrack()Lcom/glympse/android/api/GTrack;

    move-result-object v1

    check-cast v1, Lcom/glympse/android/lib/GTrackPrivate;

    .line 557
    invoke-interface {v1, p1, v2, v3}, Lcom/glympse/android/lib/GTrackPrivate;->add(Lcom/glympse/android/core/GLocation;J)Z

    .line 559
    const/4 v4, 0x0

    invoke-interface/range {v1 .. v6}, Lcom/glympse/android/lib/GTrackPrivate;->trim(JZJ)Z

    .line 562
    if-eqz p3, :cond_1

    .line 564
    iget-object v1, p0, Lcom/glympse/android/lib/gf;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/4 v4, 0x4

    const/16 v12, 0x80

    invoke-interface {v7, v1, v4, v12, v7}, Lcom/glympse/android/lib/GTicketPrivate;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 547
    :cond_1
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    goto :goto_1

    .line 577
    :cond_2
    iget-object v1, p0, Lcom/glympse/android/lib/gf;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget-object v2, p0, Lcom/glympse/android/lib/gf;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-interface {v1, v2, v3, v9, v4}, Lcom/glympse/android/lib/GGlympsePrivate;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 578
    return-void

    :cond_3
    move v9, v1

    goto :goto_0
.end method

.method public setSelfUserId(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 491
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->hu:Lcom/glympse/android/lib/GUserPrivate;

    invoke-interface {v0, p1}, Lcom/glympse/android/lib/GUserPrivate;->setId(Ljava/lang/String;)V

    .line 494
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->hu:Lcom/glympse/android/lib/GUserPrivate;

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/gf;->a(Lcom/glympse/android/lib/GUserPrivate;)V

    .line 496
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->vc:Lcom/glympse/android/hal/GHashtable;

    iget-object v1, p0, Lcom/glympse/android/lib/gf;->hu:Lcom/glympse/android/lib/GUserPrivate;

    invoke-virtual {v0, p1, v1}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 500
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getConfigPrivate()Lcom/glympse/android/lib/GConfigPrivate;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/lib/GConfigPrivate;->areAccountsLinked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 502
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->hu:Lcom/glympse/android/lib/GUserPrivate;

    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/gf;->refreshUser(Lcom/glympse/android/lib/GUserPrivate;)V

    .line 505
    :cond_0
    if-eqz p2, :cond_1

    .line 508
    invoke-direct {p0}, Lcom/glympse/android/lib/gf;->de()V

    .line 510
    :cond_1
    return-void
.end method

.method public setUserTrackingMode(I)V
    .locals 0

    .prologue
    .line 174
    iput p1, p0, Lcom/glympse/android/lib/gf;->vk:I

    .line 175
    return-void
.end method

.method public start(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/core/GPrimitive;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    .line 314
    iput-object p1, p0, Lcom/glympse/android/lib/gf;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 315
    iget-object v1, p0, Lcom/glympse/android/lib/gf;->jZ:Lcom/glympse/android/lib/fb;

    const-string v2, "users_v2"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p1, v0, v2}, Lcom/glympse/android/lib/fb;->a(Lcom/glympse/android/lib/GGlympsePrivate;Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    new-instance v1, Lcom/glympse/android/lib/gb;

    invoke-direct {v1}, Lcom/glympse/android/lib/gb;-><init>()V

    iput-object v1, p0, Lcom/glympse/android/lib/gf;->hu:Lcom/glympse/android/lib/GUserPrivate;

    .line 319
    iget-object v1, p0, Lcom/glympse/android/lib/gf;->hu:Lcom/glympse/android/lib/GUserPrivate;

    invoke-interface {v1, v3}, Lcom/glympse/android/lib/GUserPrivate;->setSelf(Z)V

    .line 323
    if-eqz p2, :cond_0

    .line 325
    const-string v0, "un"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 328
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/glympse/android/lib/gf;->setSelfUserId(Ljava/lang/String;Z)V

    move-object v1, v0

    .line 332
    :goto_0
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->hu:Lcom/glympse/android/lib/GUserPrivate;

    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/gf;->addUser(Lcom/glympse/android/lib/GUserPrivate;)V

    .line 333
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->hu:Lcom/glympse/android/lib/GUserPrivate;

    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/gf;->addStandaloneUser(Lcom/glympse/android/lib/GUserPrivate;)V

    .line 336
    new-instance v0, Lcom/glympse/android/lib/fe;

    invoke-direct {v0, v3}, Lcom/glympse/android/lib/fe;-><init>(Z)V

    iput-object v0, p0, Lcom/glympse/android/lib/gf;->uY:Lcom/glympse/android/lib/GTicketPrivate;

    .line 337
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->uY:Lcom/glympse/android/lib/GTicketPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketPrivate;->getTrack()Lcom/glympse/android/api/GTrack;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GTrackPrivate;

    iput-object v0, p0, Lcom/glympse/android/lib/gf;->uZ:Lcom/glympse/android/lib/GTrackPrivate;

    .line 338
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->hu:Lcom/glympse/android/lib/GUserPrivate;

    iget-object v2, p0, Lcom/glympse/android/lib/gf;->uY:Lcom/glympse/android/lib/GTicketPrivate;

    invoke-interface {v0, v2}, Lcom/glympse/android/lib/GUserPrivate;->addTicket(Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 341
    new-instance v0, Lcom/glympse/android/lib/gf$b;

    iget-object v2, p0, Lcom/glympse/android/lib/gf;->uZ:Lcom/glympse/android/lib/GTrackPrivate;

    invoke-direct {v0, v2}, Lcom/glympse/android/lib/gf$b;-><init>(Lcom/glympse/android/lib/GTrackPrivate;)V

    iput-object v0, p0, Lcom/glympse/android/lib/gf;->vm:Lcom/glympse/android/lib/gf$b;

    .line 342
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->vm:Lcom/glympse/android/lib/gf$b;

    invoke-interface {p1, v0}, Lcom/glympse/android/lib/GGlympsePrivate;->addListener(Lcom/glympse/android/api/GEventListener;)Z

    .line 345
    invoke-direct {p0, v1}, Lcom/glympse/android/lib/gf;->load(Ljava/lang/String;)V

    .line 347
    iput-boolean v3, p0, Lcom/glympse/android/lib/gf;->vg:Z

    .line 348
    return-void

    :cond_0
    move-object v1, v0

    goto :goto_0
.end method

.method public startTracking(Lcom/glympse/android/api/GUser;)I
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 217
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->ve:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GVector;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/glympse/android/api/GUser;->isSelf()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 219
    :cond_0
    const/4 v0, -0x1

    .line 235
    :cond_1
    :goto_0
    return v0

    .line 222
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[User.startTracking] User: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p1}, Lcom/glympse/android/api/GUser;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->safeStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 225
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->vf:Lcom/glympse/android/lib/eq;

    invoke-virtual {v0, p1}, Lcom/glympse/android/lib/eq;->h(Ljava/lang/Object;)I

    move-result v0

    .line 228
    if-ne v2, v0, :cond_1

    iget-object v1, p0, Lcom/glympse/android/lib/gf;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v1, :cond_1

    .line 231
    iget-object v1, p0, Lcom/glympse/android/lib/gf;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->getServerPost()Lcom/glympse/android/lib/GServerPost;

    move-result-object v1

    const/16 v2, 0x1388

    invoke-interface {v1, v2}, Lcom/glympse/android/lib/GServerPost;->doPost(I)V

    goto :goto_0
.end method

.method public startTrackingAll()V
    .locals 3

    .prologue
    .line 184
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->ve:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v2

    .line 187
    const/4 v0, 0x1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 189
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->ve:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GUser;

    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/gf;->startTracking(Lcom/glympse/android/api/GUser;)I

    .line 187
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 191
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 353
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/gf;->vm:Lcom/glympse/android/lib/gf$b;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GGlympsePrivate;->removeListener(Lcom/glympse/android/api/GEventListener;)Z

    .line 354
    iput-object v2, p0, Lcom/glympse/android/lib/gf;->vm:Lcom/glympse/android/lib/gf$b;

    .line 357
    invoke-direct {p0}, Lcom/glympse/android/lib/gf;->de()V

    .line 358
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->jZ:Lcom/glympse/android/lib/fb;

    invoke-virtual {v0}, Lcom/glympse/android/lib/fb;->stop()V

    .line 360
    invoke-direct {p0}, Lcom/glympse/android/lib/gf;->clear()V

    .line 362
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/glympse/android/lib/gf;->vg:Z

    .line 363
    iput-object v2, p0, Lcom/glympse/android/lib/gf;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 364
    return-void
.end method

.method public stopTracking(Lcom/glympse/android/api/GUser;)I
    .locals 3

    .prologue
    .line 240
    const/4 v0, 0x1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[User.stopTracking] User: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lcom/glympse/android/api/GUser;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->safeStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 242
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->vf:Lcom/glympse/android/lib/eq;

    invoke-virtual {v0, p1}, Lcom/glympse/android/lib/eq;->i(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public stopTrackingAll(Z)V
    .locals 3

    .prologue
    .line 195
    if-eqz p1, :cond_1

    .line 199
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->vf:Lcom/glympse/android/lib/eq;

    invoke-virtual {v0}, Lcom/glympse/android/lib/eq;->ap()V

    .line 211
    :cond_0
    return-void

    .line 203
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->ve:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v2

    .line 206
    const/4 v0, 0x1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 208
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->ve:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GUser;

    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/gf;->stopTracking(Lcom/glympse/android/api/GUser;)I

    .line 206
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public viewTicket(Lcom/glympse/android/api/GUserTicket;)Lcom/glympse/android/api/GTicket;
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 419
    if-nez p1, :cond_0

    move-object v1, v5

    .line 485
    :goto_0
    return-object v1

    .line 423
    :cond_0
    invoke-interface {p1}, Lcom/glympse/android/api/GUserTicket;->getUser()Lcom/glympse/android/api/GUser;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GUserPrivate;

    .line 424
    invoke-interface {p1}, Lcom/glympse/android/api/GUserTicket;->getTicket()Lcom/glympse/android/api/GTicket;

    move-result-object v1

    check-cast v1, Lcom/glympse/android/lib/GTicketPrivate;

    .line 425
    if-eqz v0, :cond_1

    if-nez v1, :cond_2

    :cond_1
    move-object v1, v5

    .line 427
    goto :goto_0

    .line 429
    :cond_2
    invoke-interface {v0}, Lcom/glympse/android/lib/GUserPrivate;->getId()Ljava/lang/String;

    move-result-object v2

    .line 430
    invoke-interface {v1}, Lcom/glympse/android/lib/GTicketPrivate;->getCode()Ljava/lang/String;

    move-result-object v6

    .line 431
    invoke-interface {v0}, Lcom/glympse/android/lib/GUserPrivate;->isSelf()Z

    move-result v7

    if-nez v7, :cond_3

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_3

    invoke-static {v6}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    :cond_3
    move-object v1, v5

    .line 433
    goto :goto_0

    .line 440
    :cond_4
    invoke-virtual {p0, v2}, Lcom/glympse/android/lib/gf;->findUserByUserId(Ljava/lang/String;)Lcom/glympse/android/api/GUser;

    move-result-object v2

    check-cast v2, Lcom/glympse/android/lib/GUserPrivate;

    .line 441
    if-nez v2, :cond_7

    .line 444
    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/gf;->addUser(Lcom/glympse/android/lib/GUserPrivate;)V

    .line 450
    invoke-interface {v0, v6}, Lcom/glympse/android/lib/GUserPrivate;->findTicketByInviteCode(Ljava/lang/String;)Lcom/glympse/android/api/GTicket;

    move-result-object v2

    if-nez v2, :cond_6

    move v2, v3

    .line 465
    :goto_1
    if-eqz v2, :cond_5

    .line 467
    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GUserPrivate;->addTicket(Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 471
    :cond_5
    if-nez v4, :cond_a

    .line 473
    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/gf;->addStandaloneUser(Lcom/glympse/android/lib/GUserPrivate;)V

    .line 482
    :goto_2
    iget-object v0, p0, Lcom/glympse/android/lib/gf;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1, v0, v8, v8, v1}, Lcom/glympse/android/lib/GTicketPrivate;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    goto :goto_0

    :cond_6
    move v2, v4

    .line 450
    goto :goto_1

    .line 455
    :cond_7
    invoke-interface {v2, v6}, Lcom/glympse/android/lib/GUserPrivate;->findTicketByInviteCode(Ljava/lang/String;)Lcom/glympse/android/api/GTicket;

    move-result-object v0

    if-eqz v0, :cond_8

    move-object v1, v5

    .line 457
    goto :goto_0

    .line 461
    :cond_8
    invoke-interface {v2}, Lcom/glympse/android/lib/GUserPrivate;->getActiveStandalone()Lcom/glympse/android/api/GTicket;

    move-result-object v0

    if-eqz v0, :cond_9

    move v4, v3

    :cond_9
    move-object v0, v2

    move v2, v3

    goto :goto_1

    .line 478
    :cond_a
    invoke-direct {p0}, Lcom/glympse/android/lib/gf;->de()V

    goto :goto_2
.end method
