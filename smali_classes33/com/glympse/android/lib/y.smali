.class Lcom/glympse/android/lib/y;
.super Lcom/glympse/android/lib/e;
.source "ConfigEndpoint.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/lib/y$a;
    }
.end annotation


# instance fields
.field private _glympse:Lcom/glympse/android/lib/GGlympsePrivate;

.field private gf:Lcom/glympse/android/lib/GConfigPrivate;

.field private iQ:Lcom/glympse/android/lib/y$a;


# direct methods
.method public constructor <init>(Lcom/glympse/android/lib/GGlympsePrivate;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/glympse/android/lib/e;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/glympse/android/lib/y;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 27
    invoke-interface {p1}, Lcom/glympse/android/lib/GGlympsePrivate;->getConfig()Lcom/glympse/android/api/GConfig;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GConfigPrivate;

    iput-object v0, p0, Lcom/glympse/android/lib/y;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    .line 28
    new-instance v0, Lcom/glympse/android/lib/y$a;

    invoke-direct {v0}, Lcom/glympse/android/lib/y$a;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/y;->iQ:Lcom/glympse/android/lib/y$a;

    .line 29
    iget-object v0, p0, Lcom/glympse/android/lib/y;->iQ:Lcom/glympse/android/lib/y$a;

    iput-object v0, p0, Lcom/glympse/android/lib/y;->gB:Lcom/glympse/android/lib/f;

    .line 30
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 102
    new-instance v0, Lcom/glympse/android/lib/y$a;

    invoke-direct {v0}, Lcom/glympse/android/lib/y$a;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/y;->iQ:Lcom/glympse/android/lib/y$a;

    .line 103
    iget-object v0, p0, Lcom/glympse/android/lib/y;->iQ:Lcom/glympse/android/lib/y$a;

    iput-object v0, p0, Lcom/glympse/android/lib/y;->gB:Lcom/glympse/android/lib/f;

    .line 104
    return-void
.end method

.method public process()Z
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 44
    iget-object v0, p0, Lcom/glympse/android/lib/y;->iQ:Lcom/glympse/android/lib/y$a;

    iget-object v0, v0, Lcom/glympse/android/lib/y$a;->gF:Ljava/lang/String;

    const-string v1, "ok"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 47
    iget-object v0, p0, Lcom/glympse/android/lib/y;->iQ:Lcom/glympse/android/lib/y$a;

    iget-wide v0, v0, Lcom/glympse/android/lib/y$a;->iS:J

    cmp-long v0, v4, v0

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/glympse/android/lib/y;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/y;->iQ:Lcom/glympse/android/lib/y$a;

    iget-wide v2, v1, Lcom/glympse/android/lib/y$a;->iS:J

    long-to-int v1, v2

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GConfigPrivate;->setMaximumTicketDuration(I)V

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/y;->iQ:Lcom/glympse/android/lib/y$a;

    iget-wide v0, v0, Lcom/glympse/android/lib/y$a;->iT:J

    cmp-long v0, v4, v0

    if-eqz v0, :cond_1

    .line 55
    iget-object v0, p0, Lcom/glympse/android/lib/y;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/y;->iQ:Lcom/glympse/android/lib/y$a;

    iget-wide v2, v1, Lcom/glympse/android/lib/y$a;->iT:J

    long-to-int v1, v2

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GConfigPrivate;->setPostRatePeriod(I)V

    .line 59
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/y;->iQ:Lcom/glympse/android/lib/y$a;

    iget-wide v0, v0, Lcom/glympse/android/lib/y$a;->iU:J

    cmp-long v0, v4, v0

    if-eqz v0, :cond_2

    .line 61
    iget-object v0, p0, Lcom/glympse/android/lib/y;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/y;->iQ:Lcom/glympse/android/lib/y$a;

    iget-wide v2, v1, Lcom/glympse/android/lib/y$a;->iU:J

    long-to-int v1, v2

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GConfigPrivate;->setMaximumNicknameLength(I)V

    .line 64
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/y;->iQ:Lcom/glympse/android/lib/y$a;

    iget-object v0, v0, Lcom/glympse/android/lib/y$a;->ja:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/glympse/android/lib/y;->iQ:Lcom/glympse/android/lib/y$a;

    iget-object v0, v0, Lcom/glympse/android/lib/y$a;->ja:Ljava/lang/String;

    iget-object v1, p0, Lcom/glympse/android/lib/y;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GConfigPrivate;->getLogUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/glympse/android/hal/Helpers;->safeEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 66
    iget-object v0, p0, Lcom/glympse/android/lib/y;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/y;->iQ:Lcom/glympse/android/lib/y$a;

    iget-object v1, v1, Lcom/glympse/android/lib/y$a;->ja:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GConfigPrivate;->setLogUrl(Ljava/lang/String;)V

    .line 69
    :cond_3
    iget-object v0, p0, Lcom/glympse/android/lib/y;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/y;->iQ:Lcom/glympse/android/lib/y$a;

    iget-object v1, v1, Lcom/glympse/android/lib/y$a;->jb:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GConfigPrivate;->setFileLevel(Ljava/lang/String;)V

    .line 70
    iget-object v0, p0, Lcom/glympse/android/lib/y;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/y;->iQ:Lcom/glympse/android/lib/y$a;

    iget-object v1, v1, Lcom/glympse/android/lib/y$a;->jc:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GConfigPrivate;->setDebugLevel(Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcom/glympse/android/lib/y;->iQ:Lcom/glympse/android/lib/y$a;

    iget-wide v0, v0, Lcom/glympse/android/lib/y$a;->jd:J

    iget-object v2, p0, Lcom/glympse/android/lib/y;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v2}, Lcom/glympse/android/lib/GConfigPrivate;->getLogUploadFrequency()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_4

    .line 73
    iget-object v0, p0, Lcom/glympse/android/lib/y;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/y;->iQ:Lcom/glympse/android/lib/y$a;

    iget-wide v2, v1, Lcom/glympse/android/lib/y$a;->jd:J

    invoke-interface {v0, v2, v3}, Lcom/glympse/android/lib/GConfigPrivate;->setLogUploadFrequency(J)V

    .line 75
    :cond_4
    iget-object v0, p0, Lcom/glympse/android/lib/y;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-static {v0}, Lcom/glympse/android/lib/Debug;->updateLevels(Lcom/glympse/android/lib/GConfigPrivate;)V

    .line 77
    iget-object v0, p0, Lcom/glympse/android/lib/y;->iQ:Lcom/glympse/android/lib/y$a;

    iget-object v0, v0, Lcom/glympse/android/lib/y$a;->jf:Lcom/glympse/android/core/GPrimitive;

    if-eqz v0, :cond_5

    .line 79
    iget-object v0, p0, Lcom/glympse/android/lib/y;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/y;->iQ:Lcom/glympse/android/lib/y$a;

    iget-object v1, v1, Lcom/glympse/android/lib/y$a;->jf:Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GConfigPrivate;->setSupportedServers(Lcom/glympse/android/core/GPrimitive;)V

    .line 83
    :cond_5
    iget-object v0, p0, Lcom/glympse/android/lib/y;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/y;->iQ:Lcom/glympse/android/lib/y$a;

    iget-boolean v1, v1, Lcom/glympse/android/lib/y$a;->iV:Z

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GConfigPrivate;->setPostRatePluggedHigh(Z)V

    .line 84
    iget-object v0, p0, Lcom/glympse/android/lib/y;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/y;->iQ:Lcom/glympse/android/lib/y$a;

    iget-boolean v1, v1, Lcom/glympse/android/lib/y$a;->iW:Z

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GConfigPrivate;->setAccuracyPluggedHigh(Z)V

    .line 85
    iget-object v0, p0, Lcom/glympse/android/lib/y;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/y;->iQ:Lcom/glympse/android/lib/y$a;

    iget-boolean v1, v1, Lcom/glympse/android/lib/y$a;->iX:Z

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GConfigPrivate;->setPostRateStationaryLow(Z)V

    .line 86
    iget-object v0, p0, Lcom/glympse/android/lib/y;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/y;->iQ:Lcom/glympse/android/lib/y$a;

    iget-boolean v1, v1, Lcom/glympse/android/lib/y$a;->iY:Z

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GConfigPrivate;->setAccuracyStationaryLow(Z)V

    .line 87
    iget-object v0, p0, Lcom/glympse/android/lib/y;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/y;->iQ:Lcom/glympse/android/lib/y$a;

    iget-boolean v1, v1, Lcom/glympse/android/lib/y$a;->iZ:Z

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GConfigPrivate;->setInvitePollPushEnabled(Z)V

    .line 90
    iget-object v0, p0, Lcom/glympse/android/lib/y;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/y;->iQ:Lcom/glympse/android/lib/y$a;

    iget-object v1, v1, Lcom/glympse/android/lib/y$a;->iR:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GConfigPrivate;->setLabel(Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lcom/glympse/android/lib/y;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GConfigPrivate;->save()V

    .line 97
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 95
    :cond_6
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public url(Ljava/lang/StringBuilder;)Z
    .locals 1

    .prologue
    .line 38
    const-string v0, "config"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    const/4 v0, 0x0

    return v0
.end method
