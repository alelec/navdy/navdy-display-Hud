.class Lcom/glympse/android/lib/ey;
.super Ljava/lang/Object;
.source "ServerPost.java"

# interfaces
.implements Lcom/glympse/android/lib/GAccountListener;
.implements Lcom/glympse/android/lib/GServerPost;
.implements Lcom/glympse/android/lib/bf;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/lib/ey$a;,
        Lcom/glympse/android/lib/ey$b;
    }
.end annotation


# instance fields
.field private _glympse:Lcom/glympse/android/lib/GGlympsePrivate;

.field private f:Ljava/lang/String;

.field private gd:Ljava/lang/String;

.field private ge:Ljava/lang/String;

.field private gf:Lcom/glympse/android/lib/GConfigPrivate;

.field private gj:Ljava/lang/String;

.field private jy:Z

.field private nJ:Ljava/lang/String;

.field private sF:Lcom/glympse/android/api/GNetworkManager;

.field private sG:Lcom/glympse/android/hal/GVector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GVector",
            "<",
            "Lcom/glympse/android/lib/GApiEndpoint;",
            ">;"
        }
    .end annotation
.end field

.field private sH:Z

.field private sI:Z

.field private sJ:Lcom/glympse/android/lib/GTrackPrivate;

.field private sK:Lcom/glympse/android/lib/GAccountManager;

.field private sL:I

.field private sM:Ljava/lang/Runnable;

.field private sN:Ljava/lang/Runnable;

.field private sO:J

.field private sP:I

.field private sQ:I

.field private sR:Z

.field private sS:I

.field private sT:Lcom/glympse/android/lib/HttpJob;

.field private sU:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 182
    new-instance v0, Lcom/glympse/android/hal/GVector;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Lcom/glympse/android/hal/GVector;-><init>(I)V

    iput-object v0, p0, Lcom/glympse/android/lib/ey;->sG:Lcom/glympse/android/hal/GVector;

    .line 183
    iput-boolean v2, p0, Lcom/glympse/android/lib/ey;->sH:Z

    .line 184
    iput-boolean v2, p0, Lcom/glympse/android/lib/ey;->sI:Z

    .line 185
    new-instance v0, Lcom/glympse/android/lib/fs;

    invoke-direct {v0}, Lcom/glympse/android/lib/fs;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/ey;->sJ:Lcom/glympse/android/lib/GTrackPrivate;

    .line 186
    iput-boolean v3, p0, Lcom/glympse/android/lib/ey;->jy:Z

    .line 187
    iput v3, p0, Lcom/glympse/android/lib/ey;->sL:I

    .line 188
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/glympse/android/lib/ey;->sO:J

    .line 189
    const v0, 0x36ee80

    iput v0, p0, Lcom/glympse/android/lib/ey;->sP:I

    .line 190
    iget v0, p0, Lcom/glympse/android/lib/ey;->sP:I

    iput v0, p0, Lcom/glympse/android/lib/ey;->sQ:I

    .line 191
    iput-boolean v2, p0, Lcom/glympse/android/lib/ey;->sR:Z

    .line 192
    iget v0, p0, Lcom/glympse/android/lib/ey;->sP:I

    iput v0, p0, Lcom/glympse/android/lib/ey;->sS:I

    .line 193
    iput v2, p0, Lcom/glympse/android/lib/ey;->sU:I

    .line 194
    return-void
.end method

.method private am()Lcom/glympse/android/lib/GAccountListener;
    .locals 1

    .prologue
    .line 1383
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GAccountListener;

    return-object v0
.end method

.method private c(Lcom/glympse/android/lib/r;)V
    .locals 2

    .prologue
    .line 1194
    new-instance v0, Lcom/glympse/android/lib/co;

    iget-object v1, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct {v0, v1, p1}, Lcom/glympse/android/lib/co;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/r;)V

    iput-object v0, p0, Lcom/glympse/android/lib/ey;->sT:Lcom/glympse/android/lib/HttpJob;

    .line 1195
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getJobQueue()Lcom/glympse/android/lib/GJobQueue;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/lib/ey;->sT:Lcom/glympse/android/lib/HttpJob;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GJobQueue;->addJob(Lcom/glympse/android/lib/GJob;)V

    .line 1196
    return-void
.end method

.method private cD()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 397
    invoke-virtual {p0}, Lcom/glympse/android/lib/ey;->isPosting()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x3

    iget v3, p0, Lcom/glympse/android/lib/ey;->sL:I

    if-eq v2, v3, :cond_2

    :cond_0
    move v0, v1

    .line 421
    :cond_1
    :goto_0
    return v0

    .line 403
    :cond_2
    iget-object v2, p0, Lcom/glympse/android/lib/ey;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v2}, Lcom/glympse/android/lib/GConfigPrivate;->getAccessToken()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/glympse/android/lib/ey;->gj:Ljava/lang/String;

    .line 406
    iget-object v2, p0, Lcom/glympse/android/lib/ey;->gj:Ljava/lang/String;

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 412
    iput v0, p0, Lcom/glympse/android/lib/ey;->sL:I

    .line 419
    invoke-virtual {p0}, Lcom/glympse/android/lib/ey;->authenticate()V

    move v0, v1

    .line 421
    goto :goto_0
.end method

.method private cE()V
    .locals 3

    .prologue
    .line 466
    const/4 v0, 0x2

    iput v0, p0, Lcom/glympse/android/lib/ey;->sL:I

    .line 469
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->sK:Lcom/glympse/android/lib/GAccountManager;

    iget-object v1, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->getApiKey()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GAccountManager;->create(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 472
    const/4 v0, 0x1

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/glympse/android/lib/ey;->failedToCreate(ZILcom/glympse/android/api/GServerError;)V

    .line 474
    :cond_0
    return-void
.end method

.method private cF()V
    .locals 4

    .prologue
    .line 479
    const/4 v0, 0x2

    iput v0, p0, Lcom/glympse/android/lib/ey;->sL:I

    .line 482
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->sK:Lcom/glympse/android/lib/GAccountManager;

    iget-object v1, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->getApiKey()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/glympse/android/lib/ey;->gd:Ljava/lang/String;

    iget-object v3, p0, Lcom/glympse/android/lib/ey;->ge:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Lcom/glympse/android/lib/GAccountManager;->login(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 485
    const/16 v0, 0x20

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/glympse/android/lib/ey;->failedToLogin(ILcom/glympse/android/api/GServerError;)V

    .line 487
    :cond_0
    return-void
.end method

.method private cG()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 492
    const/4 v0, 0x3

    iput v0, p0, Lcom/glympse/android/lib/ey;->sL:I

    .line 495
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getUserManager()Lcom/glympse/android/api/GUserManager;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GUserManagerPrivate;

    .line 496
    invoke-interface {v0}, Lcom/glympse/android/lib/GUserManagerPrivate;->getSelf()Lcom/glympse/android/api/GUser;

    move-result-object v1

    check-cast v1, Lcom/glympse/android/lib/GUserPrivate;

    .line 497
    invoke-interface {v1}, Lcom/glympse/android/lib/GUserPrivate;->isNicknameSynced()Z

    move-result v2

    if-nez v2, :cond_0

    .line 500
    const-string v2, "launch"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v4, v2}, Lcom/glympse/android/lib/GUserManagerPrivate;->modifyUser(Lcom/glympse/android/lib/GUserPrivate;Ljava/lang/String;Ljava/lang/String;)V

    .line 504
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getHistoryManager()Lcom/glympse/android/api/GHistoryManager;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GHistoryManagerPrivate;

    .line 505
    invoke-interface {v0}, Lcom/glympse/android/lib/GHistoryManagerPrivate;->syncFresh()V

    .line 508
    invoke-virtual {p0}, Lcom/glympse/android/lib/ey;->doPost()V

    .line 511
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/4 v2, 0x1

    const/16 v3, 0x8

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/glympse/android/lib/GGlympsePrivate;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 512
    return-void
.end method

.method private cH()V
    .locals 1

    .prologue
    .line 717
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/ey;->sN:Ljava/lang/Runnable;

    .line 718
    return-void
.end method

.method private cI()V
    .locals 6

    .prologue
    .line 762
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/ey;->sM:Ljava/lang/Runnable;

    if-nez v0, :cond_1

    .line 785
    :cond_0
    :goto_0
    return-void

    .line 766
    :cond_1
    invoke-direct {p0}, Lcom/glympse/android/lib/ey;->cJ()V

    .line 770
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->isActive()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x2

    invoke-static {}, Lcom/glympse/android/hal/Concurrent;->getBackgroundMode()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 776
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->okToPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 779
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getHandler()Lcom/glympse/android/core/GHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/lib/ey;->sM:Ljava/lang/Runnable;

    iget v2, p0, Lcom/glympse/android/lib/ey;->sS:I

    int-to-long v2, v2

    invoke-interface {v0, v1, v2, v3}, Lcom/glympse/android/core/GHandler;->postDelayed(Ljava/lang/Runnable;J)V

    .line 782
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->sF:Lcom/glympse/android/api/GNetworkManager;

    iget-object v1, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/4 v2, 0x5

    const/16 v3, 0x10

    iget v4, p0, Lcom/glympse/android/lib/ey;->sS:I

    int-to-long v4, v4

    .line 783
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 782
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/glympse/android/api/GNetworkManager;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    goto :goto_0
.end method

.method private cJ()V
    .locals 2

    .prologue
    .line 789
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/ey;->sM:Ljava/lang/Runnable;

    if-nez v0, :cond_1

    .line 794
    :cond_0
    :goto_0
    return-void

    .line 793
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getHandler()Lcom/glympse/android/core/GHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/lib/ey;->sM:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GHandler;->cancel(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private cK()I
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v2, -0x1

    .line 799
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GConfigPrivate;->getGetRate()J

    move-result-wide v0

    long-to-int v3, v0

    .line 802
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getUserManager()Lcom/glympse/android/api/GUserManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/api/GUserManager;->getTracking()Ljava/util/Enumeration;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 804
    invoke-interface {v4}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GUserPrivate;

    .line 805
    invoke-interface {v0}, Lcom/glympse/android/lib/GUserPrivate;->getActiveStandalone()Lcom/glympse/android/api/GTicket;

    move-result-object v1

    check-cast v1, Lcom/glympse/android/lib/GTicketPrivate;

    .line 808
    invoke-interface {v0}, Lcom/glympse/android/lib/GUserPrivate;->isSelf()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz v1, :cond_0

    .line 811
    invoke-interface {v1}, Lcom/glympse/android/lib/GTicketPrivate;->getState()I

    move-result v0

    and-int/lit8 v0, v0, 0x12

    if-eqz v0, :cond_0

    move v1, v3

    .line 856
    :cond_1
    :goto_0
    return v1

    .line 820
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getGroupManager()Lcom/glympse/android/api/GGroupManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/api/GGroupManager;->getTracking()Ljava/util/Enumeration;

    move-result-object v6

    move v1, v2

    :cond_3
    invoke-interface {v6}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 822
    invoke-interface {v6}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GGroup;

    .line 826
    const/4 v4, 0x4

    invoke-interface {v0}, Lcom/glympse/android/api/GGroup;->getState()I

    move-result v7

    if-ne v4, v7, :cond_3

    invoke-interface {v0}, Lcom/glympse/android/api/GGroup;->getId()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 832
    const v1, 0xea60

    .line 835
    invoke-interface {v0, v5}, Lcom/glympse/android/api/GGroup;->getMembers(Z)Lcom/glympse/android/core/GArray;

    move-result-object v7

    .line 836
    invoke-interface {v7}, Lcom/glympse/android/core/GArray;->length()I

    move-result v8

    move v4, v5

    .line 837
    :goto_1
    if-ge v4, v8, :cond_3

    .line 839
    invoke-interface {v7, v4}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GGroupMember;

    .line 840
    invoke-interface {v0}, Lcom/glympse/android/api/GGroupMember;->getTicket()Lcom/glympse/android/api/GTicket;

    move-result-object v0

    .line 843
    if-eqz v0, :cond_4

    invoke-interface {v0}, Lcom/glympse/android/api/GTicket;->getState()I

    move-result v0

    and-int/lit8 v0, v0, 0x12

    if-eqz v0, :cond_4

    move v1, v3

    .line 845
    goto :goto_0

    .line 837
    :cond_4
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 850
    :cond_5
    if-ne v2, v1, :cond_1

    .line 856
    const v1, 0x36ee80

    goto :goto_0
.end method

.method private cL()V
    .locals 2

    .prologue
    .line 862
    iget v0, p0, Lcom/glympse/android/lib/ey;->sP:I

    .line 865
    iget-object v1, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->startStopLocation()V

    .line 868
    iget-boolean v1, p0, Lcom/glympse/android/lib/ey;->sR:Z

    if-eqz v1, :cond_2

    .line 873
    iget-object v1, p0, Lcom/glympse/android/lib/ey;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GConfigPrivate;->isPostRateStationaryLow()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 875
    iget-object v1, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->getDirectionsManager()Lcom/glympse/android/api/GDirectionsManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/glympse/android/api/GDirectionsManager;->isDeviceStationary()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 878
    iget v0, p0, Lcom/glympse/android/lib/ey;->sQ:I

    .line 897
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->isActive()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 901
    invoke-direct {p0}, Lcom/glympse/android/lib/ey;->cK()I

    move-result v1

    .line 905
    if-ge v1, v0, :cond_1

    move v0, v1

    :cond_1
    iput v0, p0, Lcom/glympse/android/lib/ey;->sS:I

    .line 912
    :goto_1
    return-void

    .line 887
    :cond_2
    iget-object v1, p0, Lcom/glympse/android/lib/ey;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GConfigPrivate;->isPostRatePluggedHigh()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 889
    iget-object v1, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->getBatteryManagerPrivate()Lcom/glympse/android/lib/GBatteryManagerPrivate;

    move-result-object v1

    invoke-interface {v1}, Lcom/glympse/android/lib/GBatteryManagerPrivate;->getProvider()Lcom/glympse/android/hal/GBatteryProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/glympse/android/hal/GBatteryProvider;->isPlugged()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 892
    iget v0, p0, Lcom/glympse/android/lib/ey;->sQ:I

    goto :goto_0

    .line 910
    :cond_3
    iput v0, p0, Lcom/glympse/android/lib/ey;->sS:I

    goto :goto_1
.end method

.method private cM()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/16 v6, 0x10

    .line 1147
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->sF:Lcom/glympse/android/api/GNetworkManager;

    iget-object v1, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/4 v3, 0x5

    const/16 v4, 0x20

    const/4 v5, 0x0

    invoke-interface {v0, v1, v3, v4, v5}, Lcom/glympse/android/api/GNetworkManager;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 1150
    iput-boolean v2, p0, Lcom/glympse/android/lib/ey;->sH:Z

    .line 1152
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->sG:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v0

    .line 1153
    if-ge v6, v0, :cond_2

    .line 1156
    const/4 v0, 0x3

    const-string v1, "[ServerPost.startBatch] First chunk of api endpoints is prepared"

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 1159
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/glympse/android/lib/ey;->sH:Z

    .line 1163
    new-instance v3, Lcom/glympse/android/hal/GVector;

    invoke-direct {v3, v6}, Lcom/glympse/android/hal/GVector;-><init>(I)V

    move v1, v2

    .line 1165
    :goto_0
    if-ge v1, v6, :cond_0

    .line 1167
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->sG:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GApiEndpoint;

    .line 1168
    invoke-virtual {v3, v0}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 1165
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1171
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->sG:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, v2, v6}, Lcom/glympse/android/hal/GVector;->removeRange(II)V

    .line 1173
    new-instance v0, Lcom/glympse/android/lib/r;

    iget-object v1, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct {p0}, Lcom/glympse/android/lib/ey;->cO()Lcom/glympse/android/lib/bf;

    move-result-object v2

    invoke-direct {v0, v1, v2, v3}, Lcom/glympse/android/lib/r;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/bf;Lcom/glympse/android/hal/GVector;)V

    .line 1174
    invoke-direct {p0, v0}, Lcom/glympse/android/lib/ey;->c(Lcom/glympse/android/lib/r;)V

    .line 1190
    :cond_1
    :goto_1
    return-void

    .line 1176
    :cond_2
    if-eqz v0, :cond_1

    .line 1184
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->sG:Lcom/glympse/android/hal/GVector;

    .line 1185
    new-instance v1, Lcom/glympse/android/hal/GVector;

    invoke-direct {v1}, Lcom/glympse/android/hal/GVector;-><init>()V

    iput-object v1, p0, Lcom/glympse/android/lib/ey;->sG:Lcom/glympse/android/hal/GVector;

    .line 1187
    new-instance v1, Lcom/glympse/android/lib/r;

    iget-object v2, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct {p0}, Lcom/glympse/android/lib/ey;->cO()Lcom/glympse/android/lib/bf;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, Lcom/glympse/android/lib/r;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/bf;Lcom/glympse/android/hal/GVector;)V

    .line 1188
    invoke-direct {p0, v1}, Lcom/glympse/android/lib/ey;->c(Lcom/glympse/android/lib/r;)V

    goto :goto_1
.end method

.method private cN()Lcom/glympse/android/lib/GServerPost;
    .locals 1

    .prologue
    .line 1378
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GServerPost;

    return-object v0
.end method

.method private cO()Lcom/glympse/android/lib/bf;
    .locals 1

    .prologue
    .line 1388
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/bf;

    return-object v0
.end method

.method private d(Lcom/glympse/android/lib/r;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1200
    iget-object v1, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v1, :cond_0

    .line 1203
    invoke-virtual {p0}, Lcom/glympse/android/lib/ey;->sendEvents()V

    .line 1207
    :cond_0
    iput-object v0, p0, Lcom/glympse/android/lib/ey;->sT:Lcom/glympse/android/lib/HttpJob;

    .line 1210
    if-eqz p1, :cond_1

    .line 1212
    invoke-virtual {p1}, Lcom/glympse/android/lib/r;->aB()Lcom/glympse/android/hal/GVector;

    move-result-object v0

    .line 1217
    :cond_1
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v1

    if-lez v1, :cond_3

    .line 1221
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/glympse/android/lib/ey;->sH:Z

    .line 1227
    iget-object v1, p0, Lcom/glympse/android/lib/ey;->sG:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v1}, Lcom/glympse/android/hal/GVector;->elements()Ljava/util/Enumeration;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1229
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    goto :goto_0

    .line 1231
    :cond_2
    iput-object v0, p0, Lcom/glympse/android/lib/ey;->sG:Lcom/glympse/android/hal/GVector;

    .line 1233
    :cond_3
    return-void
.end method


# virtual methods
.method public a(Lcom/glympse/android/lib/r;)V
    .locals 2

    .prologue
    .line 1243
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/ey;->d(Lcom/glympse/android/lib/r;)V

    .line 1245
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-nez v0, :cond_0

    .line 1267
    :goto_0
    return-void

    .line 1251
    :cond_0
    invoke-direct {p0}, Lcom/glympse/android/lib/ey;->cL()V

    .line 1254
    invoke-virtual {p0}, Lcom/glympse/android/lib/ey;->haveDataToPost()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1257
    invoke-virtual {p0}, Lcom/glympse/android/lib/ey;->doPost()V

    .line 1266
    :goto_1
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/glympse/android/lib/ey;->sO:J

    goto :goto_0

    .line 1262
    :cond_1
    invoke-direct {p0}, Lcom/glympse/android/lib/ey;->cI()V

    goto :goto_1
.end method

.method public accountCreated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 534
    iput-object p1, p0, Lcom/glympse/android/lib/ey;->gd:Ljava/lang/String;

    .line 535
    iput-object p2, p0, Lcom/glympse/android/lib/ey;->ge:Ljava/lang/String;

    .line 538
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getUserManager()Lcom/glympse/android/api/GUserManager;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GUserManagerPrivate;

    .line 539
    iget-object v1, p0, Lcom/glympse/android/lib/ey;->gd:Ljava/lang/String;

    invoke-interface {v0, v1, v4}, Lcom/glympse/android/lib/GUserManagerPrivate;->setSelfUserId(Ljava/lang/String;Z)V

    .line 542
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GConfigPrivate;->getCurrentAccount()Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 543
    if-nez v0, :cond_0

    .line 545
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/ey;->gd:Ljava/lang/String;

    iget-object v2, p0, Lcom/glympse/android/lib/ey;->ge:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/glympse/android/lib/GConfigPrivate;->saveCurrentAccount(Ljava/lang/String;Ljava/lang/String;)V

    .line 549
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-interface {v0, v1, v4, v2, v3}, Lcom/glympse/android/lib/GGlympsePrivate;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 552
    invoke-direct {p0}, Lcom/glympse/android/lib/ey;->cF()V

    .line 553
    return-void
.end method

.method public addLocation(JLcom/glympse/android/core/GLocation;)V
    .locals 7

    .prologue
    .line 325
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->sJ:Lcom/glympse/android/lib/GTrackPrivate;

    invoke-interface {v0, p3}, Lcom/glympse/android/lib/GTrackPrivate;->addCore(Lcom/glympse/android/core/GLocation;)V

    .line 329
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->sJ:Lcom/glympse/android/lib/GTrackPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GTrackPrivate;->length()I

    move-result v6

    .line 330
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->sJ:Lcom/glympse/android/lib/GTrackPrivate;

    const/4 v3, 0x0

    const-wide/32 v4, 0xa4cb800

    move-wide v1, p1

    invoke-interface/range {v0 .. v5}, Lcom/glympse/android/lib/GTrackPrivate;->trim(JZJ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 334
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->sJ:Lcom/glympse/android/lib/GTrackPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GTrackPrivate;->length()I

    move-result v0

    sub-int v0, v6, v0

    .line 335
    const/4 v1, 0x4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[ServerPost.addLocation] Locations were lost: "

    invoke-static {v3}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    int-to-long v4, v0

    .line 336
    invoke-static {v4, v5}, Lcom/glympse/android/hal/Helpers;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 335
    invoke-static {v1, v0}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 340
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->isActive()Z

    move-result v0

    if-nez v0, :cond_1

    .line 342
    const/4 v0, 0x3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[ServerPost.addLocation] last posted:"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/glympse/android/lib/ey;->sO:J

    sub-long v2, p1, v2

    .line 343
    invoke-static {v2, v3}, Lcom/glympse/android/hal/Helpers;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 342
    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 347
    const/4 v0, 0x1

    invoke-static {}, Lcom/glympse/android/hal/Concurrent;->getBackgroundMode()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 350
    iget-wide v0, p0, Lcom/glympse/android/lib/ey;->sO:J

    sub-long v0, p1, v0

    iget v2, p0, Lcom/glympse/android/lib/ey;->sS:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 353
    invoke-virtual {p0}, Lcom/glympse/android/lib/ey;->doPost()V

    .line 357
    :cond_1
    return-void
.end method

.method public areLocationsPartiallyUploaded()Z
    .locals 1

    .prologue
    .line 747
    iget-boolean v0, p0, Lcom/glympse/android/lib/ey;->sI:Z

    return v0
.end method

.method public authenticate()V
    .locals 2

    .prologue
    .line 430
    const/4 v0, 0x1

    iget v1, p0, Lcom/glympse/android/lib/ey;->sL:I

    if-eq v0, v1, :cond_0

    .line 461
    :goto_0
    return-void

    .line 444
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->gj:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 447
    invoke-direct {p0}, Lcom/glympse/android/lib/ey;->cG()V

    goto :goto_0

    .line 450
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->gd:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/glympse/android/lib/ey;->ge:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 453
    invoke-direct {p0}, Lcom/glympse/android/lib/ey;->cF()V

    goto :goto_0

    .line 459
    :cond_2
    invoke-direct {p0}, Lcom/glympse/android/lib/ey;->cE()V

    goto :goto_0
.end method

.method public b(Lcom/glympse/android/lib/r;)V
    .locals 0

    .prologue
    .line 1280
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/ey;->d(Lcom/glympse/android/lib/r;)V

    .line 1281
    return-void
.end method

.method public cancelEndpoint(Lcom/glympse/android/lib/GApiEndpoint;I)Z
    .locals 4

    .prologue
    .line 681
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getHandlerManager()Lcom/glympse/android/lib/GHandlerManager;

    move-result-object v0

    new-instance v1, Lcom/glympse/android/lib/ey$a;

    invoke-direct {p0}, Lcom/glympse/android/lib/ey;->cN()Lcom/glympse/android/lib/GServerPost;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Lcom/glympse/android/lib/ey$a;-><init>(Lcom/glympse/android/lib/GServerPost;Lcom/glympse/android/lib/GApiEndpoint;)V

    int-to-long v2, p2

    invoke-interface {v0, v1, v2, v3}, Lcom/glympse/android/lib/GHandlerManager;->postDelayed(Ljava/lang/Runnable;J)V

    .line 685
    const/4 v0, 0x1

    return v0
.end method

.method public d(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1290
    const/4 v0, 0x3

    iget v1, p0, Lcom/glympse/android/lib/ey;->sL:I

    if-eq v0, v1, :cond_1

    .line 1315
    :cond_0
    :goto_0
    return-void

    .line 1296
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->gj:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/glympse/android/lib/ey;->gj:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1304
    :cond_2
    const/16 v0, 0x40

    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/ey;->rememberEvents(I)V

    .line 1307
    const/4 v0, 0x1

    iput v0, p0, Lcom/glympse/android/lib/ey;->sL:I

    .line 1310
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/ey;->gj:Ljava/lang/String;

    .line 1311
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GConfigPrivate;->forgetAccessToken()V

    .line 1314
    invoke-virtual {p0}, Lcom/glympse/android/lib/ey;->authenticate()V

    goto :goto_0
.end method

.method public doPost()V
    .locals 15

    .prologue
    const/16 v14, 0x262

    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 940
    invoke-direct {p0}, Lcom/glympse/android/lib/ey;->cH()V

    .line 943
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/glympse/android/lib/ey;->cD()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1116
    :cond_0
    :goto_0
    return-void

    .line 951
    :cond_1
    :try_start_0
    invoke-direct {p0}, Lcom/glympse/android/lib/ey;->cJ()V

    .line 955
    iget-boolean v0, p0, Lcom/glympse/android/lib/ey;->sH:Z

    if-nez v0, :cond_d

    .line 961
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->isActive()Z

    move-result v0

    if-eqz v0, :cond_b

    iget-boolean v0, p0, Lcom/glympse/android/lib/ey;->sI:Z

    if-nez v0, :cond_b

    .line 964
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getUserManager()Lcom/glympse/android/api/GUserManager;

    move-result-object v5

    .line 965
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getUserManager()Lcom/glympse/android/api/GUserManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/api/GUserManager;->getTracking()Ljava/util/Enumeration;

    move-result-object v7

    :cond_2
    :goto_1
    invoke-interface {v7}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 967
    invoke-interface {v7}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GUserPrivate;

    .line 968
    invoke-interface {v0}, Lcom/glympse/android/lib/GUserPrivate;->isSelf()Z

    move-result v1

    if-nez v1, :cond_2

    .line 972
    invoke-interface {v0}, Lcom/glympse/android/lib/GUserPrivate;->getActiveStandalone()Lcom/glympse/android/api/GTicket;

    move-result-object v1

    check-cast v1, Lcom/glympse/android/lib/GTicketPrivate;

    .line 974
    invoke-interface {v5}, Lcom/glympse/android/api/GUserManager;->getUserTrackingMode()I

    move-result v2

    .line 975
    if-ne v8, v2, :cond_4

    .line 978
    if-eqz v1, :cond_2

    invoke-interface {v1}, Lcom/glympse/android/lib/GTicketPrivate;->isCompleted()Z

    move-result v2

    if-nez v2, :cond_2

    .line 984
    iget-object v2, p0, Lcom/glympse/android/lib/ey;->sG:Lcom/glympse/android/hal/GVector;

    new-instance v3, Lcom/glympse/android/lib/da;

    iget-object v4, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/4 v9, 0x1

    invoke-direct {v3, v4, v0, v1, v9}, Lcom/glympse/android/lib/da;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/GUserPrivate;Lcom/glympse/android/lib/GTicketPrivate;Z)V

    invoke-virtual {v2, v3}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1109
    :catch_0
    move-exception v0

    .line 1111
    invoke-static {v0, v8}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    .line 1115
    :cond_3
    :goto_2
    invoke-direct {p0}, Lcom/glympse/android/lib/ey;->cI()V

    goto :goto_0

    .line 989
    :cond_4
    :try_start_1
    invoke-interface {v0}, Lcom/glympse/android/lib/GUserPrivate;->getTickets()Lcom/glympse/android/core/GArray;

    move-result-object v9

    .line 990
    invoke-interface {v9}, Lcom/glympse/android/core/GArray;->length()I

    move-result v10

    move v4, v6

    .line 991
    :goto_3
    if-ge v4, v10, :cond_2

    .line 993
    invoke-interface {v9, v4}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/glympse/android/lib/GTicketPrivate;

    .line 994
    invoke-interface {v2}, Lcom/glympse/android/lib/GTicketPrivate;->isStandalone()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v2}, Lcom/glympse/android/lib/GTicketPrivate;->isCompleted()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 991
    :cond_5
    :goto_4
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_3

    .line 1000
    :cond_6
    if-ne v1, v2, :cond_7

    move v3, v8

    .line 1001
    :goto_5
    iget-object v11, p0, Lcom/glympse/android/lib/ey;->sG:Lcom/glympse/android/hal/GVector;

    new-instance v12, Lcom/glympse/android/lib/da;

    iget-object v13, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct {v12, v13, v0, v2, v3}, Lcom/glympse/android/lib/da;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/GUserPrivate;Lcom/glympse/android/lib/GTicketPrivate;Z)V

    invoke-virtual {v11, v12}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    goto :goto_4

    :cond_7
    move v3, v6

    .line 1000
    goto :goto_5

    .line 1008
    :cond_8
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getGroupManager()Lcom/glympse/android/api/GGroupManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/api/GGroupManager;->getTracking()Ljava/util/Enumeration;

    move-result-object v9

    :cond_9
    invoke-interface {v9}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1010
    invoke-interface {v9}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/glympse/android/lib/GGroupPrivate;

    .line 1014
    const/4 v0, 0x4

    invoke-interface {v2}, Lcom/glympse/android/lib/GGroupPrivate;->getState()I

    move-result v1

    if-ne v0, v1, :cond_9

    invoke-interface {v2}, Lcom/glympse/android/lib/GGroupPrivate;->getId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 1021
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->sG:Lcom/glympse/android/hal/GVector;

    new-instance v1, Lcom/glympse/android/lib/by;

    iget-object v3, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct {v1, v3, v2}, Lcom/glympse/android/lib/by;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/GGroupPrivate;)V

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 1024
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Lcom/glympse/android/lib/GGroupPrivate;->getMembers(Z)Lcom/glympse/android/core/GArray;

    move-result-object v10

    .line 1025
    invoke-interface {v10}, Lcom/glympse/android/core/GArray;->length()I

    move-result v11

    move v7, v6

    .line 1026
    :goto_6
    if-ge v7, v11, :cond_9

    .line 1028
    invoke-interface {v10, v7}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/glympse/android/lib/GGroupMemberPrivate;

    .line 1029
    invoke-interface {v3}, Lcom/glympse/android/lib/GGroupMemberPrivate;->getUser()Lcom/glympse/android/api/GUser;

    move-result-object v4

    check-cast v4, Lcom/glympse/android/lib/GUserPrivate;

    .line 1030
    invoke-interface {v3}, Lcom/glympse/android/lib/GGroupMemberPrivate;->getTicket()Lcom/glympse/android/api/GTicket;

    move-result-object v5

    check-cast v5, Lcom/glympse/android/lib/GTicketPrivate;

    .line 1033
    invoke-interface {v4}, Lcom/glympse/android/lib/GUserPrivate;->isSelf()Z

    move-result v0

    if-nez v0, :cond_a

    if-eqz v5, :cond_a

    invoke-interface {v5}, Lcom/glympse/android/lib/GTicketPrivate;->getState()I

    move-result v0

    and-int/lit8 v0, v0, 0x12

    if-eqz v0, :cond_a

    .line 1035
    iget-object v12, p0, Lcom/glympse/android/lib/ey;->sG:Lcom/glympse/android/hal/GVector;

    new-instance v0, Lcom/glympse/android/lib/cx;

    iget-object v1, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct/range {v0 .. v5}, Lcom/glympse/android/lib/cx;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/GGroupPrivate;Lcom/glympse/android/lib/GGroupMemberPrivate;Lcom/glympse/android/lib/GUserPrivate;Lcom/glympse/android/lib/GTicketPrivate;)V

    invoke-virtual {v12, v0}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 1026
    :cond_a
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_6

    .line 1042
    :cond_b
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->sJ:Lcom/glympse/android/lib/GTrackPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GTrackPrivate;->length()I

    move-result v0

    .line 1043
    if-eqz v0, :cond_f

    .line 1049
    if-le v0, v14, :cond_e

    .line 1051
    const/4 v0, 0x3

    const-string v1, "[ServerPost.doPost] First chunk of location points is prepared"

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 1054
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/glympse/android/lib/ey;->sI:Z

    .line 1060
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->sJ:Lcom/glympse/android/lib/GTrackPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GTrackPrivate;->getLocationsRaw()Lcom/glympse/android/hal/GLinkedList;

    move-result-object v2

    .line 1061
    new-instance v3, Lcom/glympse/android/hal/GLinkedList;

    invoke-direct {v3}, Lcom/glympse/android/hal/GLinkedList;-><init>()V

    move v1, v6

    .line 1062
    :goto_7
    if-ge v1, v14, :cond_c

    .line 1064
    invoke-virtual {v2}, Lcom/glympse/android/hal/GLinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GLocation;

    .line 1065
    invoke-virtual {v2}, Lcom/glympse/android/hal/GLinkedList;->removeFirst()Ljava/lang/Object;

    .line 1066
    invoke-virtual {v3, v0}, Lcom/glympse/android/hal/GLinkedList;->addLast(Ljava/lang/Object;)V

    .line 1062
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 1069
    :cond_c
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->sG:Lcom/glympse/android/hal/GVector;

    new-instance v1, Lcom/glympse/android/lib/dn;

    iget-object v2, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/4 v4, 0x0

    invoke-direct {v1, v2, v4, v3}, Lcom/glympse/android/lib/dn;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/GTicketPrivate;Lcom/glympse/android/hal/GLinkedList;)V

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 1103
    :cond_d
    :goto_8
    invoke-virtual {p0}, Lcom/glympse/android/lib/ey;->haveDataToPost()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1106
    invoke-direct {p0}, Lcom/glympse/android/lib/ey;->cM()V

    goto/16 :goto_2

    .line 1074
    :cond_e
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/glympse/android/lib/ey;->sI:Z

    .line 1077
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->sG:Lcom/glympse/android/hal/GVector;

    new-instance v1, Lcom/glympse/android/lib/dn;

    iget-object v2, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/glympse/android/lib/ey;->sJ:Lcom/glympse/android/lib/GTrackPrivate;

    invoke-interface {v4}, Lcom/glympse/android/lib/GTrackPrivate;->getLocationsRaw()Lcom/glympse/android/hal/GLinkedList;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/glympse/android/lib/dn;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/GTicketPrivate;Lcom/glympse/android/hal/GLinkedList;)V

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 1078
    new-instance v0, Lcom/glympse/android/lib/fs;

    invoke-direct {v0}, Lcom/glympse/android/lib/fs;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/ey;->sJ:Lcom/glympse/android/lib/GTrackPrivate;

    .line 1082
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getHistoryManager()Lcom/glympse/android/api/GHistoryManager;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GHistoryManagerPrivate;

    .line 1083
    invoke-interface {v0}, Lcom/glympse/android/lib/GHistoryManagerPrivate;->completePending()V

    goto :goto_8

    .line 1090
    :cond_f
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->isSharingSiblings()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1093
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getNotificationCenter()Lcom/glympse/android/lib/GNotificationCenter;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/lib/GNotificationCenter;->isPushEnabled()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 1094
    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getConfigPrivate()Lcom/glympse/android/lib/GConfigPrivate;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/lib/GConfigPrivate;->isInvitePollPushEnabled()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1096
    :cond_10
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getTicketProtocol()Lcom/glympse/android/lib/GTicketProtocol;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketProtocol;->refreshInvites()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_8
.end method

.method public doPost(I)V
    .locals 4

    .prologue
    .line 917
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getTime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/glympse/android/lib/ey;->sO:J

    sub-long/2addr v0, v2

    int-to-long v2, p1

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    .line 920
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->okToPost()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 922
    invoke-virtual {p0}, Lcom/glympse/android/lib/ey;->doPost()V

    .line 933
    :cond_0
    :goto_0
    return-void

    .line 928
    :cond_1
    invoke-direct {p0}, Lcom/glympse/android/lib/ey;->cL()V

    .line 931
    invoke-direct {p0}, Lcom/glympse/android/lib/ey;->cI()V

    goto :goto_0
.end method

.method public enableSsl(Z)V
    .locals 0

    .prologue
    .line 293
    iput-boolean p1, p0, Lcom/glympse/android/lib/ey;->jy:Z

    .line 294
    return-void
.end method

.method public failedToCreate(ZILcom/glympse/android/api/GServerError;)V
    .locals 0

    .prologue
    .line 578
    invoke-virtual {p0, p2, p3}, Lcom/glympse/android/lib/ey;->failedToLogin(ILcom/glympse/android/api/GServerError;)V

    .line 579
    return-void
.end method

.method public failedToLogin(ILcom/glympse/android/api/GServerError;)V
    .locals 3

    .prologue
    .line 584
    const/4 v0, 0x4

    iput v0, p0, Lcom/glympse/android/lib/ey;->sL:I

    .line 587
    if-eqz p2, :cond_0

    .line 598
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2, p1, p2}, Lcom/glympse/android/lib/GGlympsePrivate;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 601
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->stop()V

    .line 602
    return-void
.end method

.method public getAccessToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 521
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->gj:Ljava/lang/String;

    return-object v0
.end method

.method public getAuthState()I
    .locals 1

    .prologue
    .line 516
    iget v0, p0, Lcom/glympse/android/lib/ey;->sL:I

    return v0
.end method

.method public getBaseUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->f:Ljava/lang/String;

    return-object v0
.end method

.method public getLastPostTime()J
    .locals 2

    .prologue
    .line 752
    iget-wide v0, p0, Lcom/glympse/android/lib/ey;->sO:J

    return-wide v0
.end method

.method public getPendingLocationsCount()I
    .locals 1

    .prologue
    .line 757
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->sJ:Lcom/glympse/android/lib/GTrackPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GTrackPrivate;->length()I

    move-result v0

    return v0
.end method

.method public getPostRate()I
    .locals 1

    .prologue
    .line 383
    iget v0, p0, Lcom/glympse/android/lib/ey;->sS:I

    return v0
.end method

.method public getUserAgent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 366
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->nJ:Ljava/lang/String;

    return-object v0
.end method

.method public haveDataToPost()Z
    .locals 2

    .prologue
    .line 734
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->sG:Lcom/glympse/android/hal/GVector;

    .line 735
    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/ey;->sJ:Lcom/glympse/android/lib/GTrackPrivate;

    .line 736
    invoke-interface {v0}, Lcom/glympse/android/lib/GTrackPrivate;->length()I

    move-result v0

    iget v1, p0, Lcom/glympse/android/lib/ey;->sS:I

    div-int/lit16 v1, v1, 0x3e8

    if-ge v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/glympse/android/lib/ey;->sI:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 734
    :goto_0
    return v0

    .line 736
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public haveLocationsToPost()Z
    .locals 1

    .prologue
    .line 742
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->sJ:Lcom/glympse/android/lib/GTrackPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GTrackPrivate;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public invokeEndpoint(Lcom/glympse/android/lib/GApiEndpoint;Z)V
    .locals 1

    .prologue
    .line 610
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/glympse/android/lib/ey;->invokeEndpoint(Lcom/glympse/android/lib/GApiEndpoint;ZZ)V

    .line 611
    return-void
.end method

.method public invokeEndpoint(Lcom/glympse/android/lib/GApiEndpoint;ZZ)V
    .locals 3

    .prologue
    .line 616
    if-nez p2, :cond_3

    .line 619
    const/4 v1, 0x0

    .line 624
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->sG:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v0

    .line 625
    add-int/lit8 v0, v0, -0x1

    move v2, v0

    move v0, v1

    move v1, v2

    .line 626
    :goto_0
    if-ltz v1, :cond_0

    .line 628
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->sG:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GApiEndpoint;

    .line 629
    invoke-interface {v0, p1}, Lcom/glympse/android/lib/GApiEndpoint;->shouldAdd(Lcom/glympse/android/lib/GApiEndpoint;)I

    move-result v0

    .line 632
    if-eqz v0, :cond_2

    .line 639
    :cond_0
    packed-switch v0, :pswitch_data_0

    .line 671
    :goto_1
    if-eqz p3, :cond_1

    .line 673
    invoke-virtual {p0}, Lcom/glympse/android/lib/ey;->schedulePost()V

    .line 675
    :cond_1
    return-void

    .line 626
    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 644
    :pswitch_0
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->sG:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    goto :goto_1

    .line 649
    :pswitch_1
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->sG:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, p1, v1}, Lcom/glympse/android/hal/GVector;->setElementAt(Ljava/lang/Object;I)V

    goto :goto_1

    .line 654
    :pswitch_2
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->sG:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GVector;->removeElementAt(I)V

    .line 655
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->sG:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    goto :goto_1

    .line 667
    :cond_3
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->sG:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    goto :goto_1

    .line 639
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public isPosting()Z
    .locals 1

    .prologue
    .line 1133
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->sT:Lcom/glympse/android/lib/HttpJob;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isServerPostRateHigh()Z
    .locals 1

    .prologue
    .line 378
    iget-boolean v0, p0, Lcom/glympse/android/lib/ey;->sR:Z

    return v0
.end method

.method public isSslEnabled()Z
    .locals 1

    .prologue
    .line 298
    iget-boolean v0, p0, Lcom/glympse/android/lib/ey;->jy:Z

    return v0
.end method

.method public loggedIn(Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 563
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v0, p1, p2, p3}, Lcom/glympse/android/lib/GConfigPrivate;->saveAccessToken(Ljava/lang/String;J)V

    .line 566
    iput-object p1, p0, Lcom/glympse/android/lib/ey;->gj:Ljava/lang/String;

    .line 569
    invoke-direct {p0}, Lcom/glympse/android/lib/ey;->cG()V

    .line 570
    return-void
.end method

.method public rememberEvents(I)V
    .locals 1

    .prologue
    .line 1319
    iget v0, p0, Lcom/glympse/android/lib/ey;->sU:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/glympse/android/lib/ey;->sU:I

    .line 1320
    return-void
.end method

.method public removeEndpoint(Lcom/glympse/android/lib/GApiEndpoint;)Z
    .locals 1

    .prologue
    .line 691
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->sG:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GVector;->removeElement(Ljava/lang/Object;)Z

    move-result v0

    .line 694
    if-eqz v0, :cond_0

    .line 696
    invoke-interface {p1}, Lcom/glympse/android/lib/GApiEndpoint;->cancel()V

    .line 700
    :cond_0
    return v0
.end method

.method public resetPostTimer()V
    .locals 0

    .prologue
    .line 1121
    invoke-direct {p0}, Lcom/glympse/android/lib/ey;->cL()V

    .line 1124
    invoke-direct {p0}, Lcom/glympse/android/lib/ey;->cI()V

    .line 1125
    return-void
.end method

.method public retryAccountCreate(Lcom/glympse/android/core/GPrimitive;)V
    .locals 0

    .prologue
    .line 558
    return-void
.end method

.method public schedulePost()V
    .locals 2

    .prologue
    .line 705
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->sN:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    iget v0, p0, Lcom/glympse/android/lib/ey;->sL:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    .line 713
    :cond_0
    :goto_0
    return-void

    .line 711
    :cond_1
    new-instance v0, Lcom/glympse/android/lib/ey$b;

    invoke-direct {p0}, Lcom/glympse/android/lib/ey;->cN()Lcom/glympse/android/lib/GServerPost;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/ey$b;-><init>(Lcom/glympse/android/lib/GServerPost;)V

    iput-object v0, p0, Lcom/glympse/android/lib/ey;->sN:Ljava/lang/Runnable;

    .line 712
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getHandler()Lcom/glympse/android/core/GHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/lib/ey;->sN:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GHandler;->post(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public sendEvents()V
    .locals 5

    .prologue
    .line 1324
    iget v0, p0, Lcom/glympse/android/lib/ey;->sU:I

    if-eqz v0, :cond_0

    .line 1326
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/4 v2, 0x1

    iget v3, p0, Lcom/glympse/android/lib/ey;->sU:I

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/glympse/android/lib/GGlympsePrivate;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 1327
    const/4 v0, 0x0

    iput v0, p0, Lcom/glympse/android/lib/ey;->sU:I

    .line 1329
    :cond_0
    return-void
.end method

.method public setActive(Z)V
    .locals 2

    .prologue
    .line 267
    const/4 v0, 0x2

    invoke-static {}, Lcom/glympse/android/hal/Concurrent;->getBackgroundMode()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 269
    if-eqz p1, :cond_2

    .line 271
    new-instance v0, Lcom/glympse/android/lib/ey$b;

    invoke-direct {p0}, Lcom/glympse/android/lib/ey;->cN()Lcom/glympse/android/lib/GServerPost;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/ey$b;-><init>(Lcom/glympse/android/lib/GServerPost;)V

    iput-object v0, p0, Lcom/glympse/android/lib/ey;->sM:Ljava/lang/Runnable;

    .line 283
    :cond_0
    :goto_0
    if-eqz p1, :cond_1

    .line 287
    invoke-direct {p0}, Lcom/glympse/android/lib/ey;->cD()Z

    .line 289
    :cond_1
    return-void

    .line 275
    :cond_2
    invoke-direct {p0}, Lcom/glympse/android/lib/ey;->cJ()V

    .line 276
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/ey;->sM:Ljava/lang/Runnable;

    goto :goto_0
.end method

.method public setOfflineMode(Z)V
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_0

    .line 320
    :goto_0
    return-void

    .line 310
    :cond_0
    if-eqz p1, :cond_1

    .line 312
    const/4 v0, 0x3

    iput v0, p0, Lcom/glympse/android/lib/ey;->sL:I

    .line 313
    new-instance v0, Lcom/glympse/android/lib/HttpJob;

    invoke-direct {v0}, Lcom/glympse/android/lib/HttpJob;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/ey;->sT:Lcom/glympse/android/lib/HttpJob;

    goto :goto_0

    .line 317
    :cond_1
    const/4 v0, 0x1

    iput v0, p0, Lcom/glympse/android/lib/ey;->sL:I

    .line 318
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/ey;->sT:Lcom/glympse/android/lib/HttpJob;

    goto :goto_0
.end method

.method public setServerPostRate(IIZ)V
    .locals 0

    .prologue
    .line 371
    iput p1, p0, Lcom/glympse/android/lib/ey;->sP:I

    .line 372
    iput p2, p0, Lcom/glympse/android/lib/ey;->sQ:I

    .line 373
    iput-boolean p3, p0, Lcom/glympse/android/lib/ey;->sR:Z

    .line 374
    return-void
.end method

.method public start(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/core/GPrimitive;)V
    .locals 3

    .prologue
    .line 202
    iput-object p1, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 203
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getNetworkManager()Lcom/glympse/android/api/GNetworkManager;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/ey;->sF:Lcom/glympse/android/api/GNetworkManager;

    .line 204
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getConfig()Lcom/glympse/android/api/GConfig;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GConfigPrivate;

    iput-object v0, p0, Lcom/glympse/android/lib/ey;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    .line 206
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getBaseUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/glympse/android/lib/UrlParser;->prepareBaseUrlServer(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/ey;->f:Ljava/lang/String;

    .line 207
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getContextHolder()Lcom/glympse/android/hal/GContextHolder;

    move-result-object v0

    invoke-static {v0}, Lcom/glympse/android/lib/fc;->b(Lcom/glympse/android/hal/GContextHolder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/ey;->nJ:Ljava/lang/String;

    .line 208
    new-instance v0, Lcom/glympse/android/lib/ey$b;

    invoke-direct {p0}, Lcom/glympse/android/lib/ey;->cN()Lcom/glympse/android/lib/GServerPost;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/ey$b;-><init>(Lcom/glympse/android/lib/GServerPost;)V

    iput-object v0, p0, Lcom/glympse/android/lib/ey;->sM:Ljava/lang/Runnable;

    .line 209
    invoke-static {}, Lcom/glympse/android/hal/Concurrent;->getTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/glympse/android/lib/ey;->sO:J

    .line 212
    if-eqz p2, :cond_0

    .line 215
    const-string v0, "un"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/ey;->gd:Ljava/lang/String;

    .line 216
    const-string v0, "psw"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/ey;->ge:Ljava/lang/String;

    .line 220
    const-string v0, "key"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 221
    iget-object v1, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->getApiKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/glympse/android/hal/Helpers;->safeEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 224
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/ey;->gd:Ljava/lang/String;

    iget-object v2, p0, Lcom/glympse/android/lib/ey;->ge:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/glympse/android/lib/GConfigPrivate;->saveCurrentAccount(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GConfigPrivate;->forgetAccessToken()V

    .line 238
    :cond_0
    :goto_0
    new-instance v0, Lcom/glympse/android/lib/d;

    invoke-direct {v0}, Lcom/glympse/android/lib/d;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/ey;->sK:Lcom/glympse/android/lib/GAccountManager;

    .line 239
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->sK:Lcom/glympse/android/lib/GAccountManager;

    invoke-direct {p0}, Lcom/glympse/android/lib/ey;->am()Lcom/glympse/android/lib/GAccountListener;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GAccountManager;->setAccountListener(Lcom/glympse/android/lib/GAccountListener;)V

    .line 240
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->sK:Lcom/glympse/android/lib/GAccountManager;

    iget-object v1, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GAccountManager;->start(Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 241
    return-void

    .line 233
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GConfigPrivate;->getAccessToken()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/ey;->gj:Ljava/lang/String;

    goto :goto_0
.end method

.method public stop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 246
    iget-object v0, p0, Lcom/glympse/android/lib/ey;->sK:Lcom/glympse/android/lib/GAccountManager;

    invoke-interface {v0}, Lcom/glympse/android/lib/GAccountManager;->stop()V

    .line 247
    iput-object v1, p0, Lcom/glympse/android/lib/ey;->sK:Lcom/glympse/android/lib/GAccountManager;

    .line 250
    invoke-direct {p0}, Lcom/glympse/android/lib/ey;->cJ()V

    .line 251
    iput-object v1, p0, Lcom/glympse/android/lib/ey;->sM:Ljava/lang/Runnable;

    .line 254
    invoke-direct {p0}, Lcom/glympse/android/lib/ey;->cH()V

    .line 255
    iput-object v1, p0, Lcom/glympse/android/lib/ey;->sT:Lcom/glympse/android/lib/HttpJob;

    .line 258
    iput-object v1, p0, Lcom/glympse/android/lib/ey;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 259
    iput-object v1, p0, Lcom/glympse/android/lib/ey;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    .line 260
    iput-object v1, p0, Lcom/glympse/android/lib/ey;->sF:Lcom/glympse/android/api/GNetworkManager;

    .line 261
    iput-object v1, p0, Lcom/glympse/android/lib/ey;->gj:Ljava/lang/String;

    .line 262
    return-void
.end method
