.class Lcom/glympse/android/lib/cu;
.super Lcom/glympse/android/lib/e;
.source "InviteCreate.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/lib/cu$a;,
        Lcom/glympse/android/lib/cu$b;
    }
.end annotation


# instance fields
.field protected _glympse:Lcom/glympse/android/lib/GGlympsePrivate;

.field private fZ:Ljava/lang/String;

.field protected gr:Lcom/glympse/android/api/GEventSink;

.field private lu:Ljava/lang/String;

.field protected oC:I

.field protected oD:I

.field protected oE:I

.field protected oF:I

.field protected oG:Lcom/glympse/android/lib/GTicketPrivate;

.field protected oH:Lcom/glympse/android/lib/GInvitePrivate;

.field protected oI:Ljava/lang/String;

.field protected oJ:I

.field protected oK:Ljava/lang/String;

.field protected oL:Ljava/lang/String;

.field protected oM:Ljava/lang/String;

.field private oN:Ljava/lang/String;

.field private oO:Z

.field private oP:Ljava/lang/String;

.field private oQ:Ljava/lang/String;

.field private oR:Ljava/lang/String;

.field private oS:Ljava/lang/String;

.field private oT:Ljava/lang/String;

.field private oU:Ljava/lang/String;

.field private oV:Lcom/glympse/android/lib/cu$a;

.field private os:Z

.field private ot:Z

.field protected oy:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/glympse/android/lib/e;-><init>()V

    .line 52
    new-instance v0, Lcom/glympse/android/lib/cu$a;

    invoke-direct {v0}, Lcom/glympse/android/lib/cu$a;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/cu;->oV:Lcom/glympse/android/lib/cu$a;

    .line 53
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oV:Lcom/glympse/android/lib/cu$a;

    iput-object v0, p0, Lcom/glympse/android/lib/cu;->gB:Lcom/glympse/android/lib/f;

    .line 54
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/StringBuilder;)V
    .locals 0

    .prologue
    .line 65
    return-void
.end method

.method public b(Ljava/lang/StringBuilder;)V
    .locals 0

    .prologue
    .line 72
    return-void
.end method

.method public cancel()V
    .locals 1

    .prologue
    .line 580
    new-instance v0, Lcom/glympse/android/lib/cu$a;

    invoke-direct {v0}, Lcom/glympse/android/lib/cu$a;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/cu;->oV:Lcom/glympse/android/lib/cu$a;

    .line 581
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oV:Lcom/glympse/android/lib/cu$a;

    iput-object v0, p0, Lcom/glympse/android/lib/cu;->gB:Lcom/glympse/android/lib/f;

    .line 582
    return-void
.end method

.method public ck()V
    .locals 0

    .prologue
    .line 79
    return-void
.end method

.method public cl()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 88
    iget-object v2, p0, Lcom/glympse/android/lib/cu;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-interface {v2, v0}, Lcom/glympse/android/lib/GInvitePrivate;->setState(I)V

    .line 91
    iget-object v2, p0, Lcom/glympse/android/lib/cu;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-interface {v2}, Lcom/glympse/android/lib/GInvitePrivate;->getType()I

    move-result v2

    iput v2, p0, Lcom/glympse/android/lib/cu;->oJ:I

    .line 92
    iget v2, p0, Lcom/glympse/android/lib/cu;->oJ:I

    invoke-static {v2}, Lcom/glympse/android/lib/ct;->u(I)Z

    move-result v2

    iput-boolean v2, p0, Lcom/glympse/android/lib/cu;->oO:Z

    .line 93
    iget v2, p0, Lcom/glympse/android/lib/cu;->oJ:I

    packed-switch v2, :pswitch_data_0

    .line 172
    :cond_0
    :goto_0
    :pswitch_0
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GInvitePrivate;->getSubtype()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/cu;->oK:Ljava/lang/String;

    .line 173
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GInvitePrivate;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/cu;->oN:Ljava/lang/String;

    .line 174
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GInvitePrivate;->getAddress()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/cu;->oL:Ljava/lang/String;

    .line 175
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GInvitePrivate;->getCode()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/cu;->oM:Ljava/lang/String;

    .line 176
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GInvitePrivate;->getBrand()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/cu;->lu:Ljava/lang/String;

    .line 177
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GInvitePrivate;->isVisible()Z

    move-result v0

    iput-boolean v0, p0, Lcom/glympse/android/lib/cu;->os:Z

    .line 178
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GInvitePrivate;->isCreateOnly()Z

    move-result v0

    iput-boolean v0, p0, Lcom/glympse/android/lib/cu;->ot:Z

    .line 179
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GInvitePrivate;->getReference()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/cu;->oI:Ljava/lang/String;

    .line 183
    iget-boolean v0, p0, Lcom/glympse/android/lib/cu;->ot:Z

    if-eqz v0, :cond_1

    .line 185
    iput-boolean v1, p0, Lcom/glympse/android/lib/cu;->oO:Z

    .line 187
    :cond_1
    return-void

    .line 97
    :pswitch_1
    iget-object v2, p0, Lcom/glympse/android/lib/cu;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v2}, Lcom/glympse/android/lib/GGlympsePrivate;->canDeviceSendSms()I

    move-result v2

    .line 98
    iget-object v3, p0, Lcom/glympse/android/lib/cu;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v3}, Lcom/glympse/android/lib/GGlympsePrivate;->getSmsSendMode()I

    move-result v3

    .line 99
    packed-switch v3, :pswitch_data_1

    goto :goto_0

    .line 105
    :pswitch_2
    if-eq v0, v2, :cond_2

    :goto_1
    iput-boolean v0, p0, Lcom/glympse/android/lib/cu;->oO:Z

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    .line 113
    :pswitch_3
    const/4 v3, 0x3

    if-ne v3, v2, :cond_3

    :goto_2
    iput-boolean v0, p0, Lcom/glympse/android/lib/cu;->oO:Z

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_2

    .line 118
    :pswitch_4
    iput-boolean v0, p0, Lcom/glympse/android/lib/cu;->oO:Z

    goto :goto_0

    .line 126
    :pswitch_5
    iget-object v2, p0, Lcom/glympse/android/lib/cu;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v2}, Lcom/glympse/android/lib/GGlympsePrivate;->getLinkedAccountsManager()Lcom/glympse/android/api/GLinkedAccountsManager;

    move-result-object v2

    .line 127
    invoke-interface {v2}, Lcom/glympse/android/api/GLinkedAccountsManager;->getAccounts()Lcom/glympse/android/core/GArray;

    move-result-object v3

    invoke-interface {v3}, Lcom/glympse/android/core/GArray;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 133
    invoke-static {}, Lcom/glympse/android/api/GC;->LINKED_ACCOUNT_TYPE_TWITTER()Ljava/lang/String;

    move-result-object v3

    .line 134
    invoke-static {}, Lcom/glympse/android/api/GC;->LINKED_ACCOUNT_PROPERTY_INVITE_CLIENT_SEND()Ljava/lang/String;

    move-result-object v4

    .line 132
    invoke-interface {v2, v3, v4}, Lcom/glympse/android/api/GLinkedAccountsManager;->getAccountProperty(Ljava/lang/String;Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v2

    .line 135
    if-eqz v2, :cond_4

    invoke-interface {v2}, Lcom/glympse/android/core/GPrimitive;->getBool()Z

    move-result v2

    if-nez v2, :cond_5

    :cond_4
    :goto_3
    iput-boolean v0, p0, Lcom/glympse/android/lib/cu;->oO:Z

    goto/16 :goto_0

    :cond_5
    move v0, v1

    goto :goto_3

    .line 141
    :pswitch_6
    iget-object v2, p0, Lcom/glympse/android/lib/cu;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v2}, Lcom/glympse/android/lib/GGlympsePrivate;->getLinkedAccountsManager()Lcom/glympse/android/api/GLinkedAccountsManager;

    move-result-object v2

    .line 142
    invoke-interface {v2}, Lcom/glympse/android/api/GLinkedAccountsManager;->getAccounts()Lcom/glympse/android/core/GArray;

    move-result-object v3

    invoke-interface {v3}, Lcom/glympse/android/core/GArray;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 148
    invoke-static {}, Lcom/glympse/android/api/GC;->LINKED_ACCOUNT_TYPE_FACEBOOK()Ljava/lang/String;

    move-result-object v3

    .line 149
    invoke-static {}, Lcom/glympse/android/api/GC;->LINKED_ACCOUNT_PROPERTY_INVITE_CLIENT_SEND()Ljava/lang/String;

    move-result-object v4

    .line 147
    invoke-interface {v2, v3, v4}, Lcom/glympse/android/api/GLinkedAccountsManager;->getAccountProperty(Ljava/lang/String;Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v2

    .line 150
    if-eqz v2, :cond_6

    invoke-interface {v2}, Lcom/glympse/android/core/GPrimitive;->getBool()Z

    move-result v2

    if-nez v2, :cond_7

    :cond_6
    :goto_4
    iput-boolean v0, p0, Lcom/glympse/android/lib/cu;->oO:Z

    goto/16 :goto_0

    :cond_7
    move v0, v1

    goto :goto_4

    .line 156
    :pswitch_7
    iget-object v2, p0, Lcom/glympse/android/lib/cu;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v2}, Lcom/glympse/android/lib/GGlympsePrivate;->getLinkedAccountsManager()Lcom/glympse/android/api/GLinkedAccountsManager;

    move-result-object v2

    .line 157
    invoke-interface {v2}, Lcom/glympse/android/api/GLinkedAccountsManager;->getAccounts()Lcom/glympse/android/core/GArray;

    move-result-object v3

    invoke-interface {v3}, Lcom/glympse/android/core/GArray;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 163
    invoke-static {}, Lcom/glympse/android/api/GC;->LINKED_ACCOUNT_TYPE_EVERNOTE()Ljava/lang/String;

    move-result-object v3

    .line 164
    invoke-static {}, Lcom/glympse/android/api/GC;->LINKED_ACCOUNT_PROPERTY_INVITE_CLIENT_SEND()Ljava/lang/String;

    move-result-object v4

    .line 162
    invoke-interface {v2, v3, v4}, Lcom/glympse/android/api/GLinkedAccountsManager;->getAccountProperty(Ljava/lang/String;Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v2

    .line 165
    if-eqz v2, :cond_8

    invoke-interface {v2}, Lcom/glympse/android/core/GPrimitive;->getBool()Z

    move-result v2

    if-nez v2, :cond_9

    :cond_8
    :goto_5
    iput-boolean v0, p0, Lcom/glympse/android/lib/cu;->oO:Z

    goto/16 :goto_0

    :cond_9
    move v0, v1

    goto :goto_5

    .line 93
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_7
    .end packed-switch

    .line 99
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public i(Z)V
    .locals 5

    .prologue
    .line 590
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->isStarted()Z

    move-result v0

    if-nez v0, :cond_1

    .line 610
    :cond_0
    :goto_0
    return-void

    .line 597
    :cond_1
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/glympse/android/lib/cu;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GInvitePrivate;->getState()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 603
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-interface {v0, p1}, Lcom/glympse/android/lib/GInvitePrivate;->completeClientSideSend(Z)Z

    .line 606
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->gr:Lcom/glympse/android/api/GEventSink;

    if-eqz v0, :cond_0

    .line 608
    iget-object v1, p0, Lcom/glympse/android/lib/cu;->gr:Lcom/glympse/android/api/GEventSink;

    iget-object v2, p0, Lcom/glympse/android/lib/cu;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget v3, p0, Lcom/glympse/android/lib/cu;->oC:I

    if-eqz p1, :cond_2

    iget v0, p0, Lcom/glympse/android/lib/cu;->oE:I

    :goto_1
    iget-object v4, p0, Lcom/glympse/android/lib/cu;->gr:Lcom/glympse/android/api/GEventSink;

    invoke-interface {v1, v2, v3, v0, v4}, Lcom/glympse/android/api/GEventSink;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/glympse/android/lib/cu;->oF:I

    goto :goto_1
.end method

.method public methodType()I
    .locals 1

    .prologue
    .line 300
    const/4 v0, 0x2

    return v0
.end method

.method public post()Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v10, 0x2

    .line 330
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GInvitePrivate;->getRequestTicket()Lcom/glympse/android/api/GTicket;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GTicketPrivate;

    .line 331
    if-nez v0, :cond_0

    .line 333
    const/4 v0, 0x0

    .line 405
    :goto_0
    return-object v0

    .line 336
    :cond_0
    iget-object v1, p0, Lcom/glympse/android/lib/cu;->fZ:Ljava/lang/String;

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 338
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->fZ:Ljava/lang/String;

    goto :goto_0

    .line 341
    :cond_1
    new-instance v3, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v3, v10}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 342
    const-string v1, "duration"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketPrivate;->getDurationRaw()I

    move-result v2

    int-to-long v4, v2

    invoke-interface {v3, v1, v4, v5}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 344
    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketPrivate;->getExpireTime()J

    move-result-wide v4

    .line 345
    const-wide/16 v6, 0x0

    cmp-long v1, v6, v4

    if-eqz v1, :cond_2

    .line 347
    const-string v1, "end_time"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v1, v4, v5}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 350
    :cond_2
    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketPrivate;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 351
    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 353
    const-string v2, "message"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2, v1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    :cond_3
    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketPrivate;->getDestination()Lcom/glympse/android/api/GPlace;

    move-result-object v1

    .line 357
    if-eqz v1, :cond_5

    invoke-interface {v1}, Lcom/glympse/android/api/GPlace;->hasLocation()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 359
    new-instance v2, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v2, v10}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 361
    const-string v4, "lat"

    invoke-static {v4}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1}, Lcom/glympse/android/api/GPlace;->getLatitude()D

    move-result-wide v6

    invoke-interface {v2, v4, v6, v7}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;D)V

    .line 362
    const-string v4, "lng"

    invoke-static {v4}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1}, Lcom/glympse/android/api/GPlace;->getLongitude()D

    move-result-wide v6

    invoke-interface {v2, v4, v6, v7}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;D)V

    .line 363
    invoke-interface {v1}, Lcom/glympse/android/api/GPlace;->getName()Ljava/lang/String;

    move-result-object v1

    .line 364
    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 366
    const-string v4, "name"

    invoke-static {v4}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4, v1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    :cond_4
    const-string v1, "destination"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v1, v2}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 371
    :cond_5
    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketPrivate;->getInvites()Lcom/glympse/android/core/GArray;

    move-result-object v4

    .line 372
    invoke-interface {v4}, Lcom/glympse/android/core/GArray;->length()I

    move-result v5

    .line 373
    if-lez v5, :cond_9

    .line 375
    new-instance v6, Lcom/glympse/android/lib/Primitive;

    const/4 v1, 0x1

    invoke-direct {v6, v1}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 376
    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_8

    .line 378
    new-instance v7, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v7, v10}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 379
    invoke-interface {v4, v2}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/glympse/android/api/GInvite;

    .line 380
    const-string v8, "type"

    invoke-static {v8}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v1}, Lcom/glympse/android/api/GInvite;->getType()I

    move-result v9

    invoke-static {v9}, Lcom/glympse/android/lib/ct;->v(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    const-string v8, "address"

    invoke-static {v8}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v1}, Lcom/glympse/android/api/GInvite;->getAddress()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    invoke-interface {v1}, Lcom/glympse/android/api/GInvite;->getName()Ljava/lang/String;

    move-result-object v8

    .line 383
    invoke-static {v8}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_6

    .line 385
    const-string v9, "name"

    invoke-static {v9}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v9, v8}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    :cond_6
    invoke-interface {v1}, Lcom/glympse/android/api/GInvite;->getSubtype()Ljava/lang/String;

    move-result-object v1

    .line 388
    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_7

    .line 390
    const-string v8, "subtype"

    invoke-static {v8}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8, v1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    :cond_7
    invoke-interface {v6, v7}, Lcom/glympse/android/core/GPrimitive;->put(Lcom/glympse/android/core/GPrimitive;)V

    .line 376
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 394
    :cond_8
    const-string v1, "invites"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v1, v6}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 396
    :cond_9
    const-string v1, "no_reply"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketPrivate;->getRequestNoReply()Z

    move-result v2

    invoke-interface {v3, v1, v2}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Z)V

    .line 398
    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketPrivate;->getRequestScenario()Ljava/lang/String;

    move-result-object v0

    .line 399
    if-eqz v0, :cond_a

    .line 401
    const-string v1, "scenario"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v1, v0}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    :cond_a
    invoke-interface {v3}, Lcom/glympse/android/core/GPrimitive;->size()I

    move-result v0

    mul-int/lit16 v0, v0, 0x80

    invoke-static {v3, v0}, Lcom/glympse/android/lib/json/JsonSerializer;->toString(Lcom/glympse/android/core/GPrimitive;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/cu;->fZ:Ljava/lang/String;

    .line 405
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->fZ:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public process()Z
    .locals 7

    .prologue
    const/4 v1, 0x7

    const/4 v6, 0x3

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 410
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oV:Lcom/glympse/android/lib/cu$a;

    iget-object v0, v0, Lcom/glympse/android/lib/cu$a;->gF:Ljava/lang/String;

    const-string v4, "ok"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oV:Lcom/glympse/android/lib/cu$a;

    iget-object v0, v0, Lcom/glympse/android/lib/cu$a;->mw:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oV:Lcom/glympse/android/lib/cu$a;

    iget-object v0, v0, Lcom/glympse/android/lib/cu$a;->bn:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 413
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/cu;->oV:Lcom/glympse/android/lib/cu$a;

    iget-object v1, v1, Lcom/glympse/android/lib/cu$a;->mw:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GInvitePrivate;->setCode(Ljava/lang/String;)V

    .line 414
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/cu;->oV:Lcom/glympse/android/lib/cu$a;

    iget-object v1, v1, Lcom/glympse/android/lib/cu$a;->bn:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GInvitePrivate;->setUrl(Ljava/lang/String;)V

    .line 415
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/cu;->oV:Lcom/glympse/android/lib/cu$a;

    iget-object v1, v1, Lcom/glympse/android/lib/cu$a;->oy:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GInvitePrivate;->setText(Ljava/lang/String;)V

    .line 416
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/cu;->oV:Lcom/glympse/android/lib/cu$a;

    iget-wide v4, v1, Lcom/glympse/android/lib/cu$a;->ov:J

    invoke-interface {v0, v4, v5}, Lcom/glympse/android/lib/GInvitePrivate;->setCreatedTime(J)V

    .line 418
    iget-boolean v0, p0, Lcom/glympse/android/lib/cu;->oO:Z

    if-eqz v0, :cond_1

    .line 421
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/cu;->oV:Lcom/glympse/android/lib/cu$a;

    iget v1, v1, Lcom/glympse/android/lib/cu$a;->cg:I

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GInvitePrivate;->setState(I)V

    .line 422
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->gr:Lcom/glympse/android/api/GEventSink;

    if-eqz v0, :cond_0

    .line 425
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->gr:Lcom/glympse/android/api/GEventSink;

    iget-object v1, p0, Lcom/glympse/android/lib/cu;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget v3, p0, Lcom/glympse/android/lib/cu;->oC:I

    iget v4, p0, Lcom/glympse/android/lib/cu;->oE:I

    iget-object v5, p0, Lcom/glympse/android/lib/cu;->gr:Lcom/glympse/android/api/GEventSink;

    invoke-interface {v0, v1, v3, v4, v5}, Lcom/glympse/android/api/GEventSink;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 541
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/glympse/android/lib/cu;->ck()V

    .line 575
    :goto_1
    return v2

    .line 432
    :cond_1
    iget-boolean v0, p0, Lcom/glympse/android/lib/cu;->ot:Z

    if-eqz v0, :cond_2

    .line 437
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-interface {v0, v6}, Lcom/glympse/android/lib/GInvitePrivate;->setState(I)V

    .line 440
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->gr:Lcom/glympse/android/api/GEventSink;

    if-eqz v0, :cond_0

    .line 442
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->gr:Lcom/glympse/android/api/GEventSink;

    iget-object v1, p0, Lcom/glympse/android/lib/cu;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget v3, p0, Lcom/glympse/android/lib/cu;->oC:I

    iget v4, p0, Lcom/glympse/android/lib/cu;->oD:I

    iget-object v5, p0, Lcom/glympse/android/lib/cu;->gr:Lcom/glympse/android/api/GEventSink;

    invoke-interface {v0, v1, v3, v4, v5}, Lcom/glympse/android/api/GEventSink;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    goto :goto_0

    .line 449
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-interface {v0, v6}, Lcom/glympse/android/lib/GInvitePrivate;->setState(I)V

    .line 451
    iget v0, p0, Lcom/glympse/android/lib/cu;->oJ:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 455
    :pswitch_1
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->canDeviceSendSms()I

    move-result v0

    .line 456
    if-ne v2, v0, :cond_3

    .line 459
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GInvitePrivate;->initiateClientSideSend()Z

    .line 462
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getSmsProvider()Lcom/glympse/android/hal/GSmsProvider;

    move-result-object v1

    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    .line 463
    invoke-interface {v0}, Lcom/glympse/android/lib/GInvitePrivate;->getAddress()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    .line 464
    invoke-interface {v0}, Lcom/glympse/android/lib/GInvitePrivate;->getText()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/glympse/android/lib/cu$b;

    .line 465
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/cu;

    invoke-direct {v6, v0}, Lcom/glympse/android/lib/cu$b;-><init>(Lcom/glympse/android/lib/cu;)V

    .line 462
    invoke-interface {v1, v4, v5, v6}, Lcom/glympse/android/hal/GSmsProvider;->sendSms(Ljava/lang/String;Ljava/lang/String;Lcom/glympse/android/hal/GSmsListener;)Z

    move-result v0

    .line 468
    if-nez v0, :cond_0

    .line 471
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-interface {v0, v3}, Lcom/glympse/android/lib/GInvitePrivate;->completeClientSideSend(Z)Z

    .line 472
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->gr:Lcom/glympse/android/api/GEventSink;

    if-eqz v0, :cond_0

    .line 475
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->gr:Lcom/glympse/android/api/GEventSink;

    iget-object v1, p0, Lcom/glympse/android/lib/cu;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget v3, p0, Lcom/glympse/android/lib/cu;->oC:I

    iget v4, p0, Lcom/glympse/android/lib/cu;->oF:I

    iget-object v5, p0, Lcom/glympse/android/lib/cu;->gr:Lcom/glympse/android/api/GEventSink;

    invoke-interface {v0, v1, v3, v4, v5}, Lcom/glympse/android/api/GEventSink;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    goto :goto_0

    .line 479
    :cond_3
    const/4 v1, 0x2

    if-ne v1, v0, :cond_0

    .line 481
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->gr:Lcom/glympse/android/api/GEventSink;

    if-eqz v0, :cond_0

    .line 484
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->gr:Lcom/glympse/android/api/GEventSink;

    iget-object v1, p0, Lcom/glympse/android/lib/cu;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget v3, p0, Lcom/glympse/android/lib/cu;->oC:I

    iget v4, p0, Lcom/glympse/android/lib/cu;->oD:I

    iget-object v5, p0, Lcom/glympse/android/lib/cu;->gr:Lcom/glympse/android/api/GEventSink;

    invoke-interface {v0, v1, v3, v4, v5}, Lcom/glympse/android/api/GEventSink;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    goto/16 :goto_0

    .line 492
    :pswitch_2
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getContextHolder()Lcom/glympse/android/hal/GContextHolder;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/hal/GContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/glympse/android/hal/HalFactory;->createClipboard(Landroid/content/Context;)Lcom/glympse/android/hal/GClipboard;

    move-result-object v0

    .line 493
    iget-object v1, p0, Lcom/glympse/android/lib/cu;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GInvitePrivate;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/hal/GClipboard;->copy(Ljava/lang/String;)V

    .line 496
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-interface {v0, v2}, Lcom/glympse/android/lib/GInvitePrivate;->completeClientSideSend(Z)Z

    .line 499
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->gr:Lcom/glympse/android/api/GEventSink;

    if-eqz v0, :cond_0

    .line 501
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->gr:Lcom/glympse/android/api/GEventSink;

    iget-object v1, p0, Lcom/glympse/android/lib/cu;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget v3, p0, Lcom/glympse/android/lib/cu;->oC:I

    iget v4, p0, Lcom/glympse/android/lib/cu;->oE:I

    iget-object v5, p0, Lcom/glympse/android/lib/cu;->gr:Lcom/glympse/android/api/GEventSink;

    invoke-interface {v0, v1, v3, v4, v5}, Lcom/glympse/android/api/GEventSink;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    goto/16 :goto_0

    .line 512
    :pswitch_3
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->gr:Lcom/glympse/android/api/GEventSink;

    if-eqz v0, :cond_0

    .line 514
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->gr:Lcom/glympse/android/api/GEventSink;

    iget-object v1, p0, Lcom/glympse/android/lib/cu;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget v3, p0, Lcom/glympse/android/lib/cu;->oC:I

    iget v4, p0, Lcom/glympse/android/lib/cu;->oD:I

    iget-object v5, p0, Lcom/glympse/android/lib/cu;->gr:Lcom/glympse/android/api/GEventSink;

    invoke-interface {v0, v1, v3, v4, v5}, Lcom/glympse/android/api/GEventSink;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    goto/16 :goto_0

    .line 521
    :pswitch_4
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 522
    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getApplicationsManager()Lcom/glympse/android/api/GApplicationsManager;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GApplicationsManagerPrivate;

    .line 523
    iget-object v1, p0, Lcom/glympse/android/lib/cu;->oG:Lcom/glympse/android/lib/GTicketPrivate;

    iget-object v3, p0, Lcom/glympse/android/lib/cu;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-interface {v0, v1, v3}, Lcom/glympse/android/lib/GApplicationsManagerPrivate;->send(Lcom/glympse/android/api/GTicket;Lcom/glympse/android/api/GInvite;)Z

    move-result v1

    .line 526
    if-eqz v1, :cond_4

    iget v0, p0, Lcom/glympse/android/lib/cu;->oE:I

    .line 529
    :goto_2
    iget-object v3, p0, Lcom/glympse/android/lib/cu;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-interface {v3, v1}, Lcom/glympse/android/lib/GInvitePrivate;->completeClientSideSend(Z)Z

    .line 532
    iget-object v1, p0, Lcom/glympse/android/lib/cu;->gr:Lcom/glympse/android/api/GEventSink;

    if-eqz v1, :cond_0

    .line 534
    iget-object v1, p0, Lcom/glympse/android/lib/cu;->gr:Lcom/glympse/android/api/GEventSink;

    iget-object v3, p0, Lcom/glympse/android/lib/cu;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget v4, p0, Lcom/glympse/android/lib/cu;->oC:I

    iget-object v5, p0, Lcom/glympse/android/lib/cu;->gr:Lcom/glympse/android/api/GEventSink;

    invoke-interface {v1, v3, v4, v0, v5}, Lcom/glympse/android/api/GEventSink;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    goto/16 :goto_0

    .line 526
    :cond_4
    iget v0, p0, Lcom/glympse/android/lib/cu;->oF:I

    goto :goto_2

    .line 547
    :cond_5
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oV:Lcom/glympse/android/lib/cu$a;

    iget-object v0, v0, Lcom/glympse/android/lib/cu$a;->gG:Ljava/lang/String;

    const-string v4, "invalid_linked_account_token"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 549
    const/16 v0, 0x8

    .line 554
    iget-object v2, p0, Lcom/glympse/android/lib/cu;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v2}, Lcom/glympse/android/lib/GGlympsePrivate;->getLinkedAccountsManager()Lcom/glympse/android/api/GLinkedAccountsManager;

    move-result-object v2

    invoke-interface {v2}, Lcom/glympse/android/api/GLinkedAccountsManager;->refresh()Z

    .line 566
    :goto_3
    iget-object v2, p0, Lcom/glympse/android/lib/cu;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-interface {v2, v1}, Lcom/glympse/android/lib/GInvitePrivate;->setState(I)V

    .line 567
    iget-object v1, p0, Lcom/glympse/android/lib/cu;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    new-instance v2, Lcom/glympse/android/lib/ex;

    iget-object v4, p0, Lcom/glympse/android/lib/cu;->oV:Lcom/glympse/android/lib/cu$a;

    iget-object v4, v4, Lcom/glympse/android/lib/cu$a;->gG:Ljava/lang/String;

    iget-object v5, p0, Lcom/glympse/android/lib/cu;->oV:Lcom/glympse/android/lib/cu$a;

    iget-object v5, v5, Lcom/glympse/android/lib/cu$a;->gH:Ljava/lang/String;

    invoke-direct {v2, v0, v4, v5}, Lcom/glympse/android/lib/ex;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Lcom/glympse/android/lib/GInvitePrivate;->setError(Lcom/glympse/android/api/GServerError;)V

    .line 568
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->gr:Lcom/glympse/android/api/GEventSink;

    if-eqz v0, :cond_6

    .line 571
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->gr:Lcom/glympse/android/api/GEventSink;

    iget-object v1, p0, Lcom/glympse/android/lib/cu;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget v2, p0, Lcom/glympse/android/lib/cu;->oC:I

    iget v4, p0, Lcom/glympse/android/lib/cu;->oF:I

    iget-object v5, p0, Lcom/glympse/android/lib/cu;->gr:Lcom/glympse/android/api/GEventSink;

    invoke-interface {v0, v1, v2, v4, v5}, Lcom/glympse/android/api/GEventSink;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    :cond_6
    move v2, v3

    .line 573
    goto/16 :goto_1

    .line 556
    :cond_7
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oV:Lcom/glympse/android/lib/cu$a;

    iget-object v0, v0, Lcom/glympse/android/lib/cu$a;->gG:Ljava/lang/String;

    const-string v4, "not_linked"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 562
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getLinkedAccountsManager()Lcom/glympse/android/api/GLinkedAccountsManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/api/GLinkedAccountsManager;->refresh()Z

    move v0, v1

    goto :goto_3

    :cond_8
    move v0, v2

    goto :goto_3

    .line 451
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public url(Ljava/lang/StringBuilder;)Z
    .locals 2

    .prologue
    .line 192
    invoke-virtual {p0, p1}, Lcom/glympse/android/lib/cu;->a(Ljava/lang/StringBuilder;)V

    .line 195
    const-string v0, "?locale="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    invoke-static {}, Lcom/glympse/android/hal/Platform;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 197
    const-string v0, "&region="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 198
    invoke-static {}, Lcom/glympse/android/hal/Platform;->getRegion()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 199
    const-string v0, "&type="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    iget v0, p0, Lcom/glympse/android/lib/cu;->oJ:I

    invoke-static {v0}, Lcom/glympse/android/lib/ct;->v(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 202
    iget-boolean v0, p0, Lcom/glympse/android/lib/cu;->oO:Z

    if-eqz v0, :cond_9

    .line 204
    iget v0, p0, Lcom/glympse/android/lib/cu;->oJ:I

    sparse-switch v0, :sswitch_data_0

    .line 246
    :cond_0
    :goto_0
    const-string v0, "&send=server"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 252
    :goto_1
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oK:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 254
    const-string v0, "&subtype="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 255
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oK:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 257
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oL:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 259
    const-string v0, "&address="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 260
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oL:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 262
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oM:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 264
    const-string v0, "&bot="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 265
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oM:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267
    :cond_3
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oN:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 269
    const-string v0, "&name="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oN:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 272
    :cond_4
    iget-boolean v0, p0, Lcom/glympse/android/lib/cu;->os:Z

    if-eqz v0, :cond_5

    .line 274
    const-string v0, "&visible=all"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 276
    :cond_5
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oy:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 278
    const-string v0, "&text="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 279
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oy:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 281
    :cond_6
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->lu:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 283
    const-string v0, "&brand="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 284
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->lu:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 286
    :cond_7
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oI:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 288
    const-string v0, "&reference="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 289
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oI:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 293
    :cond_8
    invoke-virtual {p0, p1}, Lcom/glympse/android/lib/cu;->b(Ljava/lang/StringBuilder;)V

    .line 295
    const/4 v0, 0x1

    return v0

    .line 208
    :sswitch_0
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oS:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 210
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x400

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 211
    const-string v1, "consumer_key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 212
    iget-object v1, p0, Lcom/glympse/android/lib/cu;->oQ:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213
    const-string v1, "&consumer_secret="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 214
    iget-object v1, p0, Lcom/glympse/android/lib/cu;->oR:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215
    const-string v1, "&oauth_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    iget-object v1, p0, Lcom/glympse/android/lib/cu;->oS:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    const-string v1, "&oauth_token_secret="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 218
    iget-object v1, p0, Lcom/glympse/android/lib/cu;->oT:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 219
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 220
    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 221
    const-string v1, "&data="

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 222
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 228
    :sswitch_1
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oP:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 230
    const-string v0, "&data="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 231
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oP:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 237
    :sswitch_2
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oU:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 239
    const-string v0, "&data="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 240
    iget-object v0, p0, Lcom/glympse/android/lib/cu;->oU:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 250
    :cond_9
    const-string v0, "&send=client"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 204
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x5 -> :sswitch_1
        0xa -> :sswitch_2
    .end sparse-switch
.end method
