.class Lcom/glympse/android/lib/du$a;
.super Lcom/glympse/android/lib/HttpJob;
.source "NetworkManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/lib/du;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private _glympse:Lcom/glympse/android/lib/GGlympsePrivate;

.field private qV:Lcom/glympse/android/core/GPrimitive;

.field private qW:Lcom/glympse/android/core/GPrimitive;


# direct methods
.method public constructor <init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/core/GPrimitive;)V
    .locals 0

    .prologue
    .line 139
    invoke-direct {p0}, Lcom/glympse/android/lib/HttpJob;-><init>()V

    .line 140
    iput-object p1, p0, Lcom/glympse/android/lib/du$a;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 141
    iput-object p2, p0, Lcom/glympse/android/lib/du$a;->qV:Lcom/glympse/android/core/GPrimitive;

    .line 142
    return-void
.end method


# virtual methods
.method public checkResponse(II)Z
    .locals 1

    .prologue
    .line 273
    const/4 v0, 0x1

    return v0
.end method

.method public onComplete()V
    .locals 5

    .prologue
    .line 278
    invoke-super {p0}, Lcom/glympse/android/lib/HttpJob;->onComplete()V

    .line 281
    iget-object v0, p0, Lcom/glympse/android/lib/du$a;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->isStarted()Z

    move-result v0

    if-nez v0, :cond_1

    .line 307
    :cond_0
    :goto_0
    return-void

    .line 287
    :cond_1
    iget-boolean v0, p0, Lcom/glympse/android/lib/du$a;->_success:Z

    if-nez v0, :cond_2

    .line 290
    iget-object v0, p0, Lcom/glympse/android/lib/du$a;->qV:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "retries"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    long-to-int v0, v0

    .line 291
    const/4 v1, -0x1

    if-eq v1, v0, :cond_0

    iget v1, p0, Lcom/glympse/android/lib/du$a;->_failures:I

    if-ge v0, v1, :cond_0

    .line 298
    invoke-virtual {p0}, Lcom/glympse/android/lib/du$a;->abort()V

    .line 302
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/du$a;->qW:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "success"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-boolean v2, p0, Lcom/glympse/android/lib/du$a;->_success:Z

    invoke-interface {v0, v1, v2}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Z)V

    .line 305
    iget-object v0, p0, Lcom/glympse/android/lib/du$a;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getNetworkManager()Lcom/glympse/android/api/GNetworkManager;

    move-result-object v0

    .line 306
    iget-object v1, p0, Lcom/glympse/android/lib/du$a;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/4 v2, 0x5

    const/16 v3, 0x40

    iget-object v4, p0, Lcom/glympse/android/lib/du$a;->qW:Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/glympse/android/api/GNetworkManager;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    goto :goto_0
.end method

.method public onPreProcess()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    .line 151
    iget-object v0, p0, Lcom/glympse/android/lib/du$a;->_httpConnection:Lcom/glympse/android/hal/GHttpConnection;

    iget-object v1, p0, Lcom/glympse/android/lib/du$a;->qV:Lcom/glympse/android/core/GPrimitive;

    const-string v2, "url"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/hal/GHttpConnection;->setUrl(Ljava/lang/String;)V

    .line 154
    iget-object v0, p0, Lcom/glympse/android/lib/du$a;->qV:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "method"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 155
    if-eqz v0, :cond_0

    .line 157
    iget-object v1, p0, Lcom/glympse/android/lib/du$a;->_httpConnection:Lcom/glympse/android/hal/GHttpConnection;

    invoke-interface {v0}, Lcom/glympse/android/core/GPrimitive;->getLong()J

    move-result-wide v2

    long-to-int v0, v2

    invoke-interface {v1, v0}, Lcom/glympse/android/hal/GHttpConnection;->setRequestMethod(I)V

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/du$a;->qV:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "headers"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v1

    .line 162
    if-eqz v1, :cond_1

    invoke-interface {v1}, Lcom/glympse/android/core/GPrimitive;->type()I

    move-result v0

    if-ne v5, v0, :cond_1

    .line 164
    invoke-interface {v1}, Lcom/glympse/android/core/GPrimitive;->getKeys()Ljava/util/Enumeration;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 166
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 167
    invoke-interface {v1, v0}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 168
    iget-object v4, p0, Lcom/glympse/android/lib/du$a;->_httpConnection:Lcom/glympse/android/hal/GHttpConnection;

    invoke-interface {v4, v0, v3}, Lcom/glympse/android/hal/GHttpConnection;->setRequestHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 173
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/du$a;->qV:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "body"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v1

    .line 174
    if-eqz v1, :cond_2

    .line 176
    const/4 v0, 0x0

    .line 177
    invoke-interface {v1}, Lcom/glympse/android/core/GPrimitive;->type()I

    move-result v2

    .line 178
    sparse-switch v2, :sswitch_data_0

    .line 192
    :goto_1
    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 194
    iget-object v1, p0, Lcom/glympse/android/lib/du$a;->_httpConnection:Lcom/glympse/android/hal/GHttpConnection;

    invoke-interface {v1, v0}, Lcom/glympse/android/hal/GHttpConnection;->setRequestData(Ljava/lang/String;)V

    .line 199
    :cond_2
    invoke-static {}, Lcom/glympse/android/lib/Debug;->getLevel()I

    move-result v0

    if-gt v0, v5, :cond_3

    .line 201
    iget-object v0, p0, Lcom/glympse/android/lib/du$a;->qV:Lcom/glympse/android/core/GPrimitive;

    invoke-static {v0}, Lcom/glympse/android/lib/json/JsonSerializer;->toString(Lcom/glympse/android/core/GPrimitive;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/glympse/android/lib/Debug;->dumpPackets(Ljava/lang/String;)V

    .line 203
    :cond_3
    return-void

    .line 182
    :sswitch_0
    invoke-interface {v1}, Lcom/glympse/android/core/GPrimitive;->getString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 188
    :sswitch_1
    invoke-static {v1}, Lcom/glympse/android/lib/json/JsonSerializer;->toString(Lcom/glympse/android/core/GPrimitive;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 178
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_1
        0x20 -> :sswitch_0
    .end sparse-switch
.end method

.method public onProcessResponse()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 207
    iput-boolean v1, p0, Lcom/glympse/android/lib/du$a;->_success:Z

    .line 208
    new-instance v0, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v0, v6}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    iput-object v0, p0, Lcom/glympse/android/lib/du$a;->qW:Lcom/glympse/android/core/GPrimitive;

    .line 209
    iget-object v0, p0, Lcom/glympse/android/lib/du$a;->qW:Lcom/glympse/android/core/GPrimitive;

    const-string v3, "request"

    invoke-static {v3}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/glympse/android/lib/du$a;->qV:Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v0, v3, v4}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 210
    iget-object v0, p0, Lcom/glympse/android/lib/du$a;->qW:Lcom/glympse/android/core/GPrimitive;

    const-string v3, "code"

    invoke-static {v3}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lcom/glympse/android/lib/du$a;->_responseCode:I

    int-to-long v4, v4

    invoke-interface {v0, v3, v4, v5}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 213
    iget-object v0, p0, Lcom/glympse/android/lib/du$a;->qV:Lcom/glympse/android/core/GPrimitive;

    const-string v3, "criterion"

    invoke-static {v3}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    long-to-int v3, v4

    .line 216
    and-int/lit8 v0, v3, 0x1

    if-eqz v0, :cond_3

    .line 218
    iget v0, p0, Lcom/glympse/android/lib/du$a;->_responseCode:I

    if-lez v0, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/glympse/android/lib/du$a;->_success:Z

    .line 224
    :cond_0
    :goto_1
    iget-boolean v0, p0, Lcom/glympse/android/lib/du$a;->_success:Z

    if-nez v0, :cond_5

    .line 226
    iget-object v0, p0, Lcom/glympse/android/lib/du$a;->qW:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "reason"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "invalid_code"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    :cond_1
    :goto_2
    return-void

    :cond_2
    move v0, v2

    .line 218
    goto :goto_0

    .line 220
    :cond_3
    and-int/lit8 v0, v3, 0x2

    if-eqz v0, :cond_0

    .line 222
    iget v0, p0, Lcom/glympse/android/lib/du$a;->_responseCode:I

    const/16 v4, 0xc8

    if-lt v0, v4, :cond_4

    iget v0, p0, Lcom/glympse/android/lib/du$a;->_responseCode:I

    const/16 v4, 0x12c

    if-ge v0, v4, :cond_4

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/glympse/android/lib/du$a;->_success:Z

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_3

    .line 231
    :cond_5
    and-int/lit8 v0, v3, 0x30

    if-eqz v0, :cond_7

    .line 233
    iput-boolean v2, p0, Lcom/glympse/android/lib/du$a;->_success:Z

    .line 234
    iget-object v0, p0, Lcom/glympse/android/lib/du$a;->_httpConnection:Lcom/glympse/android/hal/GHttpConnection;

    invoke-interface {v0}, Lcom/glympse/android/hal/GHttpConnection;->getResponseDataString()Ljava/lang/String;

    move-result-object v0

    .line 235
    if-eqz v0, :cond_6

    .line 237
    invoke-static {v0}, Lcom/glympse/android/lib/json/JsonSerializer;->toPrimitive(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 238
    if-eqz v0, :cond_6

    .line 240
    iput-boolean v1, p0, Lcom/glympse/android/lib/du$a;->_success:Z

    .line 241
    iget-object v1, p0, Lcom/glympse/android/lib/du$a;->qW:Lcom/glympse/android/core/GPrimitive;

    const-string v2, "body"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 255
    :cond_6
    :goto_4
    iget-boolean v0, p0, Lcom/glympse/android/lib/du$a;->_success:Z

    if-nez v0, :cond_8

    .line 257
    iget-object v0, p0, Lcom/glympse/android/lib/du$a;->qW:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "reason"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "invalid_body"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 245
    :cond_7
    and-int/lit8 v0, v3, 0x10

    if-eqz v0, :cond_6

    .line 247
    iput-boolean v2, p0, Lcom/glympse/android/lib/du$a;->_success:Z

    .line 248
    iget-object v0, p0, Lcom/glympse/android/lib/du$a;->_httpConnection:Lcom/glympse/android/hal/GHttpConnection;

    invoke-interface {v0}, Lcom/glympse/android/hal/GHttpConnection;->getResponseDataString()Ljava/lang/String;

    move-result-object v0

    .line 249
    if-eqz v0, :cond_6

    .line 251
    iput-boolean v1, p0, Lcom/glympse/android/lib/du$a;->_success:Z

    .line 252
    iget-object v1, p0, Lcom/glympse/android/lib/du$a;->qW:Lcom/glympse/android/core/GPrimitive;

    const-string v2, "body"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 262
    :cond_8
    invoke-static {}, Lcom/glympse/android/lib/Debug;->getLevel()I

    move-result v0

    if-gt v0, v6, :cond_1

    .line 264
    iget-object v0, p0, Lcom/glympse/android/lib/du$a;->qW:Lcom/glympse/android/core/GPrimitive;

    invoke-static {v0}, Lcom/glympse/android/lib/json/JsonSerializer;->toString(Lcom/glympse/android/core/GPrimitive;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/glympse/android/lib/Debug;->dumpPackets(Ljava/lang/String;)V

    goto :goto_2
.end method
