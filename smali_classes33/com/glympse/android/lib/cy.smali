.class Lcom/glympse/android/lib/cy;
.super Lcom/glympse/android/lib/json/JsonHandlerBasic;
.source "InviteRequestParser.java"


# instance fields
.field private _name:Ljava/lang/String;

.field private dp:Ljava/lang/String;

.field private fX:Ljava/lang/String;

.field private gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

.field private gE:Ljava/lang/String;

.field private nd:I

.field public oG:Lcom/glympse/android/lib/GTicketPrivate;

.field public oZ:Lcom/glympse/android/lib/GUserPrivate;

.field public ou:Ljava/lang/String;

.field private pa:Z

.field private pb:Z

.field private pc:D

.field private pd:D


# direct methods
.method public constructor <init>(Lcom/glympse/android/lib/json/GJsonHandlerStack;I)V
    .locals 4

    .prologue
    const-wide/high16 v2, 0x7ff8000000000000L    # NaN

    const/4 v1, 0x0

    .line 40
    invoke-direct {p0}, Lcom/glympse/android/lib/json/JsonHandlerBasic;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/glympse/android/lib/cy;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    .line 43
    iput p2, p0, Lcom/glympse/android/lib/cy;->nd:I

    .line 46
    new-instance v0, Lcom/glympse/android/lib/gb;

    invoke-direct {v0}, Lcom/glympse/android/lib/gb;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/cy;->oZ:Lcom/glympse/android/lib/GUserPrivate;

    .line 47
    new-instance v0, Lcom/glympse/android/lib/fe;

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/fe;-><init>(Z)V

    iput-object v0, p0, Lcom/glympse/android/lib/cy;->oG:Lcom/glympse/android/lib/GTicketPrivate;

    .line 48
    iput-boolean v1, p0, Lcom/glympse/android/lib/cy;->pa:Z

    .line 49
    iput-boolean v1, p0, Lcom/glympse/android/lib/cy;->pb:Z

    .line 50
    iput-wide v2, p0, Lcom/glympse/android/lib/cy;->pc:D

    .line 51
    iput-wide v2, p0, Lcom/glympse/android/lib/cy;->pd:D

    .line 52
    return-void
.end method


# virtual methods
.method public endObject(I)Z
    .locals 7

    .prologue
    .line 158
    const/4 v0, 0x4

    if-ne v0, p1, :cond_2

    .line 160
    iget-boolean v0, p0, Lcom/glympse/android/lib/cy;->pa:Z

    if-eqz v0, :cond_1

    .line 162
    iget-object v0, p0, Lcom/glympse/android/lib/cy;->oG:Lcom/glympse/android/lib/GTicketPrivate;

    new-instance v1, Lcom/glympse/android/lib/eg;

    iget-wide v2, p0, Lcom/glympse/android/lib/cy;->pc:D

    iget-wide v4, p0, Lcom/glympse/android/lib/cy;->pd:D

    iget-object v6, p0, Lcom/glympse/android/lib/cy;->_name:Ljava/lang/String;

    invoke-direct/range {v1 .. v6}, Lcom/glympse/android/lib/eg;-><init>(DDLjava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GTicketPrivate;->setDestination(Lcom/glympse/android/api/GPlace;)V

    .line 175
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 164
    :cond_1
    iget-boolean v0, p0, Lcom/glympse/android/lib/cy;->pb:Z

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/glympse/android/lib/cy;->fX:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/lib/ct;->p(Ljava/lang/String;)I

    move-result v0

    .line 167
    iget-object v1, p0, Lcom/glympse/android/lib/cy;->oG:Lcom/glympse/android/lib/GTicketPrivate;

    new-instance v2, Lcom/glympse/android/lib/ct;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/glympse/android/lib/cy;->_name:Ljava/lang/String;

    iget-object v5, p0, Lcom/glympse/android/lib/cy;->dp:Ljava/lang/String;

    invoke-direct {v2, v0, v3, v4, v5}, Lcom/glympse/android/lib/ct;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Lcom/glympse/android/lib/GTicketPrivate;->addInvite(Lcom/glympse/android/api/GInvite;)Z

    goto :goto_0

    .line 170
    :cond_2
    iget v0, p0, Lcom/glympse/android/lib/cy;->nd:I

    if-ne p1, v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/glympse/android/lib/cy;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    invoke-interface {v0}, Lcom/glympse/android/lib/json/GJsonHandlerStack;->popHandler()V

    goto :goto_0
.end method

.method public endPair(I)Z
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x1

    return v0
.end method

.method public primitive(ILcom/glympse/android/lib/json/GJsonPrimitive;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 68
    packed-switch p1, :pswitch_data_0

    .line 134
    :cond_0
    :goto_0
    return v4

    .line 72
    :pswitch_0
    iget-object v0, p0, Lcom/glympse/android/lib/cy;->gE:Ljava/lang/String;

    const-string v1, "reference"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    invoke-virtual {p2, v4}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/cy;->ou:Ljava/lang/String;

    goto :goto_0

    .line 80
    :pswitch_1
    iget-object v0, p0, Lcom/glympse/android/lib/cy;->gE:Ljava/lang/String;

    const-string v1, "id"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 82
    iget-object v0, p0, Lcom/glympse/android/lib/cy;->oZ:Lcom/glympse/android/lib/GUserPrivate;

    invoke-virtual {p2, v4}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GUserPrivate;->setId(Ljava/lang/String;)V

    goto :goto_0

    .line 84
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/cy;->gE:Ljava/lang/String;

    const-string v1, "name"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 86
    iget-object v0, p0, Lcom/glympse/android/lib/cy;->oZ:Lcom/glympse/android/lib/GUserPrivate;

    invoke-virtual {p2, v2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GUserPrivate;->setNicknameCore(Ljava/lang/String;)V

    goto :goto_0

    .line 88
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/cy;->gE:Ljava/lang/String;

    const-string v1, "avatar"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 90
    iget-object v0, p0, Lcom/glympse/android/lib/cy;->oZ:Lcom/glympse/android/lib/GUserPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GUserPrivate;->getAvatar()Lcom/glympse/android/api/GImage;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GImagePrivate;

    .line 91
    invoke-virtual {p2, v2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GImagePrivate;->setUrl(Ljava/lang/String;)V

    goto :goto_0

    .line 93
    :cond_3
    iget-object v0, p0, Lcom/glympse/android/lib/cy;->gE:Ljava/lang/String;

    const-string v1, "duration"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 95
    iget-object v0, p0, Lcom/glympse/android/lib/cy;->oG:Lcom/glympse/android/lib/GTicketPrivate;

    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getLong()J

    move-result-wide v2

    long-to-int v1, v2

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GTicketPrivate;->setDuration(I)V

    goto :goto_0

    .line 97
    :cond_4
    iget-object v0, p0, Lcom/glympse/android/lib/cy;->gE:Ljava/lang/String;

    const-string v1, "end_time"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 101
    iget-object v0, p0, Lcom/glympse/android/lib/cy;->oG:Lcom/glympse/android/lib/GTicketPrivate;

    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getLong()J

    move-result-wide v2

    invoke-interface {v0, v2, v3, v4}, Lcom/glympse/android/lib/GTicketPrivate;->setExpireTime(JZ)V

    goto :goto_0

    .line 103
    :cond_5
    iget-object v0, p0, Lcom/glympse/android/lib/cy;->gE:Ljava/lang/String;

    const-string v1, "message"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/glympse/android/lib/cy;->oG:Lcom/glympse/android/lib/GTicketPrivate;

    invoke-virtual {p2, v2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GTicketPrivate;->setMessage(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 111
    :pswitch_2
    iget-object v0, p0, Lcom/glympse/android/lib/cy;->gE:Ljava/lang/String;

    const-string v1, "name"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 113
    invoke-virtual {p2, v2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/cy;->_name:Ljava/lang/String;

    goto/16 :goto_0

    .line 115
    :cond_6
    iget-object v0, p0, Lcom/glympse/android/lib/cy;->gE:Ljava/lang/String;

    const-string v1, "lat"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 117
    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/glympse/android/lib/cy;->pc:D

    goto/16 :goto_0

    .line 119
    :cond_7
    iget-object v0, p0, Lcom/glympse/android/lib/cy;->gE:Ljava/lang/String;

    const-string v1, "lng"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 121
    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/glympse/android/lib/cy;->pd:D

    goto/16 :goto_0

    .line 123
    :cond_8
    iget-object v0, p0, Lcom/glympse/android/lib/cy;->gE:Ljava/lang/String;

    const-string v1, "type"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 125
    invoke-virtual {p2, v4}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/cy;->fX:Ljava/lang/String;

    goto/16 :goto_0

    .line 127
    :cond_9
    iget-object v0, p0, Lcom/glympse/android/lib/cy;->gE:Ljava/lang/String;

    const-string v1, "address"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    invoke-virtual {p2, v2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/cy;->dp:Ljava/lang/String;

    goto/16 :goto_0

    .line 68
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public startObject(I)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 139
    const/4 v0, 0x4

    if-ne v0, p1, :cond_0

    .line 141
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/cy;->_name:Ljava/lang/String;

    .line 142
    iget-object v0, p0, Lcom/glympse/android/lib/cy;->gE:Ljava/lang/String;

    const-string v1, "destination"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 144
    iput-boolean v2, p0, Lcom/glympse/android/lib/cy;->pa:Z

    .line 145
    iput-boolean v3, p0, Lcom/glympse/android/lib/cy;->pb:Z

    .line 153
    :cond_0
    :goto_0
    return v2

    .line 147
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/cy;->gE:Ljava/lang/String;

    const-string v1, "recipient"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    iput-boolean v3, p0, Lcom/glympse/android/lib/cy;->pa:Z

    .line 150
    iput-boolean v2, p0, Lcom/glympse/android/lib/cy;->pb:Z

    goto :goto_0
.end method

.method public startPair(ILjava/lang/String;)Z
    .locals 1

    .prologue
    .line 56
    iput-object p2, p0, Lcom/glympse/android/lib/cy;->gE:Ljava/lang/String;

    .line 57
    const/4 v0, 0x1

    return v0
.end method
