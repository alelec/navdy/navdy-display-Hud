.class Lcom/glympse/android/lib/ct;
.super Ljava/lang/Object;
.source "Invite.java"

# interfaces
.implements Lcom/glympse/android/lib/GInvitePrivate;


# instance fields
.field private _glympse:Lcom/glympse/android/lib/GGlympsePrivate;

.field private _name:Ljava/lang/String;

.field private _type:I

.field private bn:Ljava/lang/String;

.field private cg:I

.field private dp:Ljava/lang/String;

.field private dq:Ljava/lang/String;

.field private gO:Ljava/lang/String;

.field private jC:Ljava/lang/String;

.field private lu:Ljava/lang/String;

.field private mw:Ljava/lang/String;

.field private mx:Lcom/glympse/android/api/GImage;

.field private ns:J

.field private oA:Lcom/glympse/android/lib/GPerson;

.field private oB:Lcom/glympse/android/api/GTicket;

.field private os:Z

.field private ot:Z

.field private ou:Ljava/lang/String;

.field private ov:J

.field private ow:I

.field private ox:I

.field private oy:Ljava/lang/String;

.field private oz:Lcom/glympse/android/api/GServerError;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    iput v0, p0, Lcom/glympse/android/lib/ct;->_type:I

    .line 90
    iput-boolean v0, p0, Lcom/glympse/android/lib/ct;->os:Z

    .line 91
    iput-boolean v0, p0, Lcom/glympse/android/lib/ct;->ot:Z

    .line 92
    iput v0, p0, Lcom/glympse/android/lib/ct;->cg:I

    .line 93
    iput-wide v2, p0, Lcom/glympse/android/lib/ct;->ov:J

    .line 94
    iput-wide v2, p0, Lcom/glympse/android/lib/ct;->ns:J

    .line 95
    iput v0, p0, Lcom/glympse/android/lib/ct;->ow:I

    .line 96
    iput v0, p0, Lcom/glympse/android/lib/ct;->ox:I

    .line 97
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    iput p1, p0, Lcom/glympse/android/lib/ct;->_type:I

    .line 102
    iput-object p2, p0, Lcom/glympse/android/lib/ct;->gO:Ljava/lang/String;

    .line 103
    iput-boolean v1, p0, Lcom/glympse/android/lib/ct;->os:Z

    .line 104
    iput-boolean v1, p0, Lcom/glympse/android/lib/ct;->ot:Z

    .line 111
    const/16 v0, 0x8

    if-ne v0, p1, :cond_0

    invoke-static {p4}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p3}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 113
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/ct;->_name:Ljava/lang/String;

    .line 114
    iput-object p3, p0, Lcom/glympse/android/lib/ct;->dp:Ljava/lang/String;

    .line 123
    :goto_0
    iput v1, p0, Lcom/glympse/android/lib/ct;->cg:I

    .line 124
    iput-wide v2, p0, Lcom/glympse/android/lib/ct;->ov:J

    .line 125
    iput-wide v2, p0, Lcom/glympse/android/lib/ct;->ns:J

    .line 126
    iput v1, p0, Lcom/glympse/android/lib/ct;->ow:I

    .line 127
    iput v1, p0, Lcom/glympse/android/lib/ct;->ox:I

    .line 128
    return-void

    .line 119
    :cond_0
    iput-object p3, p0, Lcom/glympse/android/lib/ct;->_name:Ljava/lang/String;

    .line 120
    iput-object p4, p0, Lcom/glympse/android/lib/ct;->dp:Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(C)Z
    .locals 1

    .prologue
    .line 808
    const/16 v0, 0x21

    if-eq v0, p0, :cond_0

    const/16 v0, 0x23

    if-ne v0, p0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static createInvite(ILjava/lang/String;Ljava/lang/String;)Lcom/glympse/android/api/GInvite;
    .locals 1

    .prologue
    .line 136
    const/4 v0, 0x0

    invoke-static {p0, v0, p1, p2}, Lcom/glympse/android/lib/ct;->createInvite(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/glympse/android/api/GInvite;

    move-result-object v0

    return-object v0
.end method

.method public static createInvite(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/glympse/android/api/GInvite;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 141
    .line 142
    const/4 v0, 0x0

    .line 145
    if-nez p0, :cond_0

    .line 147
    invoke-static {p3}, Lcom/glympse/android/lib/ct;->r(Ljava/lang/String;)I

    move-result p0

    .line 148
    if-nez p0, :cond_0

    move-object v0, v2

    .line 193
    :goto_0
    return-object v0

    .line 155
    :cond_0
    const/16 v3, 0xb

    if-ne v3, p0, :cond_1

    .line 157
    invoke-static {p1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    move-object v0, v2

    .line 159
    goto :goto_0

    .line 164
    :cond_1
    const/4 v3, 0x7

    if-ne v3, p0, :cond_3

    .line 167
    invoke-static {p3}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 169
    const-string v3, "#"

    invoke-virtual {p3, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v0, v1

    .line 176
    :cond_2
    invoke-static {p3, v1}, Lcom/glympse/android/lib/ct;->f(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object p3

    .line 177
    if-nez p3, :cond_3

    move-object v0, v2

    .line 179
    goto :goto_0

    .line 185
    :cond_3
    invoke-static {p1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 187
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 191
    :cond_4
    new-instance v1, Lcom/glympse/android/lib/ct;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/glympse/android/lib/ct;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    invoke-interface {v1, v0}, Lcom/glympse/android/api/GInvite;->setVisible(Z)V

    move-object v0, v1

    .line 193
    goto :goto_0
.end method

.method public static f(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 871
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 887
    :cond_0
    :goto_0
    return-object v0

    .line 875
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    .line 876
    :cond_2
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 882
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Lcom/glympse/android/lib/ct;->a(C)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 884
    const/4 v1, 0x1

    invoke-static {p0, v1}, Lcom/glympse/android/hal/Helpers;->substr(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p0

    .line 887
    :cond_3
    invoke-static {p0}, Lcom/glympse/android/lib/ct;->validateGroupName(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_4

    :goto_1
    move-object v0, p0

    goto :goto_0

    :cond_4
    move-object p0, v0

    goto :goto_1
.end method

.method public static isAddressRequired(I)Z
    .locals 1

    .prologue
    .line 619
    packed-switch p0, :pswitch_data_0

    .line 629
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 627
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 619
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static p(Ljava/lang/String;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 669
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 696
    :cond_0
    :goto_0
    return v0

    .line 671
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    .line 673
    const-string v2, "sms"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 674
    const/4 v0, 0x3

    goto :goto_0

    .line 675
    :cond_2
    const-string v2, "email"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 676
    const/4 v0, 0x2

    goto :goto_0

    .line 677
    :cond_3
    const-string v2, "twitter"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 678
    const/4 v0, 0x4

    goto :goto_0

    .line 679
    :cond_4
    const-string v2, "facebook"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 680
    const/4 v0, 0x5

    goto :goto_0

    .line 681
    :cond_5
    const-string v2, "link"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 682
    const/4 v0, 0x6

    goto :goto_0

    .line 683
    :cond_6
    const-string v2, "account"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 684
    const/4 v0, 0x1

    goto :goto_0

    .line 685
    :cond_7
    const-string v2, "group"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 686
    const/4 v0, 0x7

    goto :goto_0

    .line 687
    :cond_8
    const-string v2, "clipboard"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 688
    const/16 v0, 0x9

    goto :goto_0

    .line 689
    :cond_9
    const-string v2, "share"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 690
    const/16 v0, 0x8

    goto :goto_0

    .line 691
    :cond_a
    const-string v2, "evernote"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 692
    const/16 v0, 0xa

    goto :goto_0

    .line 693
    :cond_b
    const-string v2, "app"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 694
    const/16 v0, 0xb

    goto :goto_0
.end method

.method public static q(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 701
    const-string v0, "sent"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 702
    const/4 v0, 0x4

    .line 710
    :goto_0
    return v0

    .line 703
    :cond_0
    const-string v0, "sending"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 704
    const/4 v0, 0x1

    goto :goto_0

    .line 705
    :cond_1
    const-string v0, "client"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 706
    const/4 v0, 0x2

    goto :goto_0

    .line 707
    :cond_2
    const-string v0, "failed"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 708
    const/16 v0, 0x8

    goto :goto_0

    .line 710
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static r(Ljava/lang/String;)I
    .locals 9

    .prologue
    const/16 v8, 0x2e

    const/4 v3, 0x3

    const/4 v1, 0x2

    const/4 v2, 0x0

    .line 715
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 799
    :cond_0
    :goto_0
    return v2

    .line 723
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 724
    invoke-static {v4}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 729
    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    check-cast v0, [C

    .line 730
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    .line 733
    aget-char v6, v0, v2

    invoke-static {v6}, Lcom/glympse/android/lib/ct;->a(C)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 736
    if-lt v5, v1, :cond_0

    .line 738
    const/4 v2, 0x7

    goto :goto_0

    .line 745
    :cond_2
    const/16 v6, 0x40

    invoke-virtual {v4, v6, v2}, Ljava/lang/String;->indexOf(II)I

    move-result v6

    .line 746
    if-nez v6, :cond_3

    .line 748
    const/4 v2, 0x4

    goto :goto_0

    .line 752
    :cond_3
    invoke-virtual {v4, v8}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    .line 753
    if-lez v6, :cond_4

    add-int/lit8 v6, v6, 0x1

    if-le v4, v6, :cond_4

    add-int/lit8 v4, v4, 0x1

    if-ge v4, v5, :cond_4

    move v2, v1

    .line 755
    goto :goto_0

    :cond_4
    move v4, v2

    move v1, v2

    .line 760
    :goto_1
    if-ge v4, v5, :cond_0

    .line 762
    aget-char v6, v0, v4

    .line 765
    const/16 v7, 0x30

    if-lt v6, v7, :cond_5

    const/16 v7, 0x39

    if-gt v6, v7, :cond_5

    .line 768
    add-int/lit8 v1, v1, 0x1

    if-lt v1, v3, :cond_6

    move v2, v3

    .line 770
    goto :goto_0

    .line 778
    :cond_5
    if-eq v8, v6, :cond_6

    const/16 v7, 0x2d

    if-eq v7, v6, :cond_6

    const/16 v7, 0x20

    if-eq v7, v6, :cond_6

    const/16 v7, 0x28

    if-eq v7, v6, :cond_6

    const/16 v7, 0x29

    if-eq v7, v6, :cond_6

    move v1, v2

    .line 760
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method

.method public static u(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 587
    packed-switch p0, :pswitch_data_0

    :pswitch_0
    move v0, v1

    .line 613
    :goto_0
    :pswitch_1
    return v0

    :pswitch_2
    move v0, v1

    .line 592
    goto :goto_0

    :pswitch_3
    move v0, v1

    .line 598
    goto :goto_0

    :pswitch_4
    move v0, v1

    .line 600
    goto :goto_0

    :pswitch_5
    move v0, v1

    .line 604
    goto :goto_0

    .line 587
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_4
        :pswitch_3
        :pswitch_1
        :pswitch_5
    .end packed-switch
.end method

.method public static v(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 635
    packed-switch p0, :pswitch_data_0

    .line 662
    const-string v0, "unknown"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 638
    :pswitch_0
    const-string v0, "sms"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 640
    :pswitch_1
    const-string v0, "email"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 642
    :pswitch_2
    const-string v0, "twitter"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 644
    :pswitch_3
    const-string v0, "facebook"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 646
    :pswitch_4
    const-string v0, "link"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 648
    :pswitch_5
    const-string v0, "group"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 650
    :pswitch_6
    const-string v0, "account"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 652
    :pswitch_7
    const-string v0, "clipboard"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 654
    :pswitch_8
    const-string v0, "share"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 656
    :pswitch_9
    const-string v0, "evernote"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 658
    :pswitch_a
    const-string v0, "app"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 635
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_8
        :pswitch_7
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public static validateGroupName(Ljava/lang/String;)I
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 813
    .line 814
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 866
    :goto_0
    return v1

    .line 820
    :cond_0
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->toCharArray(Ljava/lang/String;)[C

    move-result-object v3

    .line 822
    aget-char v0, v3, v2

    invoke-static {v0}, Lcom/glympse/android/lib/ct;->a(C)Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    .line 829
    :goto_1
    array-length v4, v3

    move v7, v1

    move v1, v0

    move v0, v7

    .line 830
    :goto_2
    if-ge v1, v4, :cond_4

    .line 832
    aget-char v5, v3, v1

    .line 835
    invoke-static {v5}, Lcom/glympse/android/hal/Helpers;->isDigit(C)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 830
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 844
    :cond_1
    invoke-static {v5}, Lcom/glympse/android/hal/Helpers;->isLetter(C)Z

    move-result v0

    if-nez v0, :cond_6

    const/16 v0, 0x5f

    if-ne v0, v5, :cond_2

    move v0, v2

    .line 846
    goto :goto_3

    .line 851
    :cond_2
    const/16 v0, 0x7b

    if-eq v0, v5, :cond_6

    const/16 v0, 0x7d

    if-ne v0, v5, :cond_3

    move v0, v2

    .line 853
    goto :goto_3

    .line 857
    :cond_3
    const/4 v1, 0x2

    goto :goto_0

    .line 861
    :cond_4
    if-eqz v0, :cond_5

    .line 863
    const/4 v1, 0x3

    goto :goto_0

    :cond_5
    move v1, v2

    .line 866
    goto :goto_0

    :cond_6
    move v0, v2

    goto :goto_3

    :cond_7
    move v0, v2

    goto :goto_1
.end method


# virtual methods
.method public applyBrand(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/glympse/android/lib/ct;->lu:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 236
    invoke-virtual {p0, p1}, Lcom/glympse/android/lib/ct;->setBrand(Ljava/lang/String;)V

    .line 238
    :cond_0
    return-void
.end method

.method public clone()Lcom/glympse/android/api/GInvite;
    .locals 5

    .prologue
    .line 382
    new-instance v0, Lcom/glympse/android/lib/ct;

    iget v1, p0, Lcom/glympse/android/lib/ct;->_type:I

    iget-object v2, p0, Lcom/glympse/android/lib/ct;->gO:Ljava/lang/String;

    iget-object v3, p0, Lcom/glympse/android/lib/ct;->_name:Ljava/lang/String;

    iget-object v4, p0, Lcom/glympse/android/lib/ct;->dp:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/glympse/android/lib/ct;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/glympse/android/lib/ct;->clone()Lcom/glympse/android/api/GInvite;

    move-result-object v0

    return-object v0
.end method

.method public completeClientSideSend(Z)Z
    .locals 2

    .prologue
    .line 336
    const/4 v0, 0x3

    iget v1, p0, Lcom/glympse/android/lib/ct;->cg:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x2

    iget v1, p0, Lcom/glympse/android/lib/ct;->cg:I

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/ct;->mw:Ljava/lang/String;

    .line 338
    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 340
    :cond_1
    const/4 v0, 0x0

    .line 352
    :goto_0
    return v0

    .line 344
    :cond_2
    if-eqz p1, :cond_4

    const/4 v0, 0x4

    :goto_1
    iput v0, p0, Lcom/glympse/android/lib/ct;->cg:I

    .line 347
    iget-object v0, p0, Lcom/glympse/android/lib/ct;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_3

    .line 349
    iget-object v0, p0, Lcom/glympse/android/lib/ct;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getTicketProtocol()Lcom/glympse/android/lib/GTicketProtocol;

    move-result-object v1

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GInvite;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GTicketProtocol;->updateInvite(Lcom/glympse/android/api/GInvite;)V

    .line 352
    :cond_3
    const/4 v0, 0x1

    goto :goto_0

    .line 344
    :cond_4
    const/16 v0, 0x8

    goto :goto_1
.end method

.method public decode(Lcom/glympse/android/core/GPrimitive;)V
    .locals 2

    .prologue
    .line 568
    const-string v0, "type"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/glympse/android/lib/ct;->_type:I

    .line 569
    const-string v0, "subtype"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/ct;->gO:Ljava/lang/String;

    .line 570
    const-string v0, "name"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/ct;->_name:Ljava/lang/String;

    .line 571
    const-string v0, "addr"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/ct;->dp:Ljava/lang/String;

    .line 572
    const-string v0, "vis"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getBool(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/glympse/android/lib/ct;->os:Z

    .line 573
    const-string v0, "code"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 574
    invoke-static {v0}, Lcom/glympse/android/lib/TicketCode;->cleanupInviteCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/ct;->mw:Ljava/lang/String;

    .line 575
    const-string v0, "url"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/ct;->bn:Ljava/lang/String;

    .line 576
    const-string v0, "st"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/glympse/android/lib/ct;->cg:I

    .line 577
    const-string v0, "cts"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/glympse/android/lib/ct;->ov:J

    .line 578
    const-string v0, "text"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/ct;->oy:Ljava/lang/String;

    .line 579
    return-void
.end method

.method public encode(Lcom/glympse/android/core/GPrimitive;I)V
    .locals 4

    .prologue
    .line 536
    const-string v0, "type"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/glympse/android/lib/ct;->_type:I

    int-to-long v2, v1

    invoke-interface {p1, v0, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 537
    const-string v0, "st"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/glympse/android/lib/ct;->cg:I

    int-to-long v2, v1

    invoke-interface {p1, v0, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 538
    const-string v0, "cts"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lcom/glympse/android/lib/ct;->ov:J

    invoke-interface {p1, v0, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 539
    const-string v0, "vis"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, Lcom/glympse/android/lib/ct;->os:Z

    invoke-interface {p1, v0, v1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Z)V

    .line 540
    iget-object v0, p0, Lcom/glympse/android/lib/ct;->gO:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 542
    const-string v0, "subtype"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/lib/ct;->gO:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 544
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/ct;->_name:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 546
    const-string v0, "name"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/lib/ct;->_name:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 548
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/ct;->dp:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 550
    const-string v0, "addr"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/lib/ct;->dp:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 552
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/ct;->mw:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 554
    const-string v0, "code"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/lib/ct;->mw:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 556
    :cond_3
    iget-object v0, p0, Lcom/glympse/android/lib/ct;->bn:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 558
    const-string v0, "url"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/lib/ct;->bn:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 560
    :cond_4
    iget-object v0, p0, Lcom/glympse/android/lib/ct;->oy:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 562
    const-string v0, "text"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/lib/ct;->oy:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 564
    :cond_5
    return-void
.end method

.method public getAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/glympse/android/lib/ct;->dp:Ljava/lang/String;

    return-object v0
.end method

.method public getAvatar()Lcom/glympse/android/api/GImage;
    .locals 1

    .prologue
    .line 491
    iget-object v0, p0, Lcom/glympse/android/lib/ct;->mx:Lcom/glympse/android/api/GImage;

    return-object v0
.end method

.method public getBrand()Ljava/lang/String;
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/glympse/android/lib/ct;->lu:Ljava/lang/String;

    return-object v0
.end method

.method public getCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/glympse/android/lib/ct;->mw:Ljava/lang/String;

    return-object v0
.end method

.method public getCreatedTime()J
    .locals 2

    .prologue
    .line 301
    iget-wide v0, p0, Lcom/glympse/android/lib/ct;->ov:J

    return-wide v0
.end method

.method public getError()Lcom/glympse/android/api/GServerError;
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Lcom/glympse/android/lib/ct;->oz:Lcom/glympse/android/api/GServerError;

    return-object v0
.end method

.method public getGlympse()Lcom/glympse/android/lib/GGlympsePrivate;
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Lcom/glympse/android/lib/ct;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    return-object v0
.end method

.method public getLastViewTime()J
    .locals 2

    .prologue
    .line 306
    iget-wide v0, p0, Lcom/glympse/android/lib/ct;->ns:J

    return-wide v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 461
    iget-object v0, p0, Lcom/glympse/android/lib/ct;->dq:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/glympse/android/lib/ct;->_name:Ljava/lang/String;

    return-object v0
.end method

.method public getPerson()Lcom/glympse/android/lib/GPerson;
    .locals 1

    .prologue
    .line 481
    iget-object v0, p0, Lcom/glympse/android/lib/ct;->oA:Lcom/glympse/android/lib/GPerson;

    return-object v0
.end method

.method public getReference()Ljava/lang/String;
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lcom/glympse/android/lib/ct;->ou:Ljava/lang/String;

    return-object v0
.end method

.method public getRequestTicket()Lcom/glympse/android/api/GTicket;
    .locals 1

    .prologue
    .line 371
    iget-object v0, p0, Lcom/glympse/android/lib/ct;->oB:Lcom/glympse/android/api/GTicket;

    return-object v0
.end method

.method public getState()I
    .locals 1

    .prologue
    .line 296
    iget v0, p0, Lcom/glympse/android/lib/ct;->cg:I

    return v0
.end method

.method public getSubtype()Ljava/lang/String;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/glympse/android/lib/ct;->gO:Ljava/lang/String;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 357
    iget-object v0, p0, Lcom/glympse/android/lib/ct;->oy:Ljava/lang/String;

    return-object v0
.end method

.method public getTicketId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 501
    iget-object v0, p0, Lcom/glympse/android/lib/ct;->jC:Ljava/lang/String;

    return-object v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 202
    iget v0, p0, Lcom/glympse/android/lib/ct;->_type:I

    return v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/glympse/android/lib/ct;->bn:Ljava/lang/String;

    return-object v0
.end method

.method public getViewers()I
    .locals 1

    .prologue
    .line 311
    iget v0, p0, Lcom/glympse/android/lib/ct;->ow:I

    return v0
.end method

.method public getViewing()I
    .locals 1

    .prologue
    .line 316
    iget v0, p0, Lcom/glympse/android/lib/ct;->ox:I

    return v0
.end method

.method public initiateClientSideSend()Z
    .locals 2

    .prologue
    .line 321
    const/4 v0, 0x3

    iget v1, p0, Lcom/glympse/android/lib/ct;->cg:I

    if-eq v0, v1, :cond_0

    .line 323
    const/4 v0, 0x0

    .line 331
    :goto_0
    return v0

    .line 329
    :cond_0
    const/4 v0, 0x2

    iput v0, p0, Lcom/glympse/android/lib/ct;->cg:I

    .line 331
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isCreateOnly()Z
    .locals 1

    .prologue
    .line 257
    iget-boolean v0, p0, Lcom/glympse/android/lib/ct;->ot:Z

    return v0
.end method

.method public isEqual(Lcom/glympse/android/core/GCommon;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 510
    check-cast p1, Lcom/glympse/android/lib/ct;

    .line 511
    if-nez p1, :cond_1

    .line 527
    :cond_0
    :goto_0
    return v0

    .line 515
    :cond_1
    iget v1, p0, Lcom/glympse/android/lib/ct;->_type:I

    iget v2, p1, Lcom/glympse/android/lib/ct;->_type:I

    if-ne v1, v2, :cond_0

    .line 519
    iget-object v1, p0, Lcom/glympse/android/lib/ct;->gO:Ljava/lang/String;

    iget-object v2, p1, Lcom/glympse/android/lib/ct;->gO:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/glympse/android/hal/Helpers;->safeEqualsIgnoreCase(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 523
    iget-object v1, p0, Lcom/glympse/android/lib/ct;->dp:Ljava/lang/String;

    iget-object v2, p1, Lcom/glympse/android/lib/ct;->dp:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/glympse/android/hal/Helpers;->safeEqualsIgnoreCase(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 527
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 252
    iget-boolean v0, p0, Lcom/glympse/android/lib/ct;->os:Z

    return v0
.end method

.method public setAddress(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 421
    iput-object p1, p0, Lcom/glympse/android/lib/ct;->dp:Ljava/lang/String;

    .line 422
    return-void
.end method

.method public setAvatar(Lcom/glympse/android/api/GImage;)V
    .locals 0

    .prologue
    .line 486
    iput-object p1, p0, Lcom/glympse/android/lib/ct;->mx:Lcom/glympse/android/api/GImage;

    .line 487
    return-void
.end method

.method public setBrand(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 222
    invoke-static {p1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 224
    iput-object p1, p0, Lcom/glympse/android/lib/ct;->lu:Ljava/lang/String;

    .line 230
    :goto_0
    return-void

    .line 228
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/ct;->lu:Ljava/lang/String;

    goto :goto_0
.end method

.method public setCode(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 426
    iput-object p1, p0, Lcom/glympse/android/lib/ct;->mw:Ljava/lang/String;

    .line 427
    return-void
.end method

.method public setCreateOnly(Z)V
    .locals 1

    .prologue
    .line 262
    iget v0, p0, Lcom/glympse/android/lib/ct;->cg:I

    if-eqz v0, :cond_0

    .line 268
    :goto_0
    return-void

    .line 267
    :cond_0
    iput-boolean p1, p0, Lcom/glympse/android/lib/ct;->ot:Z

    goto :goto_0
.end method

.method public setCreatedTime(J)V
    .locals 1

    .prologue
    .line 436
    iput-wide p1, p0, Lcom/glympse/android/lib/ct;->ov:J

    .line 437
    return-void
.end method

.method public setError(Lcom/glympse/android/api/GServerError;)V
    .locals 0

    .prologue
    .line 471
    iput-object p1, p0, Lcom/glympse/android/lib/ct;->oz:Lcom/glympse/android/api/GServerError;

    .line 472
    return-void
.end method

.method public setGlympse(Lcom/glympse/android/lib/GGlympsePrivate;)V
    .locals 0

    .prologue
    .line 391
    iput-object p1, p0, Lcom/glympse/android/lib/ct;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 392
    return-void
.end method

.method public setLastViewTime(J)V
    .locals 1

    .prologue
    .line 441
    iput-wide p1, p0, Lcom/glympse/android/lib/ct;->ns:J

    .line 442
    return-void
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 456
    iput-object p1, p0, Lcom/glympse/android/lib/ct;->dq:Ljava/lang/String;

    .line 457
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 416
    iput-object p1, p0, Lcom/glympse/android/lib/ct;->_name:Ljava/lang/String;

    .line 417
    return-void
.end method

.method public setPerson(Lcom/glympse/android/lib/GPerson;)V
    .locals 0

    .prologue
    .line 476
    iput-object p1, p0, Lcom/glympse/android/lib/ct;->oA:Lcom/glympse/android/lib/GPerson;

    .line 477
    return-void
.end method

.method public setReference(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 282
    iget v0, p0, Lcom/glympse/android/lib/ct;->cg:I

    if-eqz v0, :cond_0

    .line 287
    :goto_0
    return-void

    .line 286
    :cond_0
    iput-object p1, p0, Lcom/glympse/android/lib/ct;->ou:Ljava/lang/String;

    goto :goto_0
.end method

.method public setRequestTicket(Lcom/glympse/android/api/GTicket;)V
    .locals 1

    .prologue
    .line 362
    iget v0, p0, Lcom/glympse/android/lib/ct;->cg:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/ct;->mw:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 367
    :cond_0
    :goto_0
    return-void

    .line 366
    :cond_1
    iput-object p1, p0, Lcom/glympse/android/lib/ct;->oB:Lcom/glympse/android/api/GTicket;

    goto :goto_0
.end method

.method public setState(I)V
    .locals 0

    .prologue
    .line 401
    iput p1, p0, Lcom/glympse/android/lib/ct;->cg:I

    .line 402
    return-void
.end method

.method public setSubtype(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 411
    iput-object p1, p0, Lcom/glympse/android/lib/ct;->gO:Ljava/lang/String;

    .line 412
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 466
    iput-object p1, p0, Lcom/glympse/android/lib/ct;->oy:Ljava/lang/String;

    .line 467
    return-void
.end method

.method public setTicketId(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 496
    iput-object p1, p0, Lcom/glympse/android/lib/ct;->jC:Ljava/lang/String;

    .line 497
    return-void
.end method

.method public setType(I)V
    .locals 0

    .prologue
    .line 406
    iput p1, p0, Lcom/glympse/android/lib/ct;->_type:I

    .line 407
    return-void
.end method

.method public setUrl(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 431
    iput-object p1, p0, Lcom/glympse/android/lib/ct;->bn:Ljava/lang/String;

    .line 432
    return-void
.end method

.method public setViewers(I)V
    .locals 0

    .prologue
    .line 446
    iput p1, p0, Lcom/glympse/android/lib/ct;->ow:I

    .line 447
    return-void
.end method

.method public setViewing(I)V
    .locals 0

    .prologue
    .line 451
    iput p1, p0, Lcom/glympse/android/lib/ct;->ox:I

    .line 452
    return-void
.end method

.method public setVisible(Z)V
    .locals 0

    .prologue
    .line 247
    iput-boolean p1, p0, Lcom/glympse/android/lib/ct;->os:Z

    .line 248
    return-void
.end method
