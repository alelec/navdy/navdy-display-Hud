.class Lcom/glympse/android/lib/ah;
.super Lcom/glympse/android/lib/e;
.source "DataAppend.java"


# instance fields
.field protected _glympse:Lcom/glympse/android/lib/GGlympsePrivate;

.field private fZ:Ljava/lang/String;

.field protected ga:Lcom/glympse/android/lib/g;

.field protected jC:Ljava/lang/String;

.field protected jD:Lcom/glympse/android/core/GPrimitive;

.field protected jE:Lcom/glympse/android/hal/GHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GHashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/glympse/android/core/GPrimitive;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/glympse/android/lib/GGlympsePrivate;Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/glympse/android/lib/e;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/glympse/android/lib/ah;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 35
    iput-object p2, p0, Lcom/glympse/android/lib/ah;->jC:Ljava/lang/String;

    .line 36
    iput-object p3, p0, Lcom/glympse/android/lib/ah;->jD:Lcom/glympse/android/core/GPrimitive;

    .line 37
    new-instance v0, Lcom/glympse/android/lib/g;

    invoke-direct {v0}, Lcom/glympse/android/lib/g;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/ah;->ga:Lcom/glympse/android/lib/g;

    .line 38
    iget-object v0, p0, Lcom/glympse/android/lib/ah;->ga:Lcom/glympse/android/lib/g;

    iput-object v0, p0, Lcom/glympse/android/lib/ah;->gB:Lcom/glympse/android/lib/f;

    .line 39
    return-void
.end method

.method public constructor <init>(Lcom/glympse/android/lib/GGlympsePrivate;Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;Lcom/glympse/android/hal/GHashtable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/glympse/android/lib/GGlympsePrivate;",
            "Ljava/lang/String;",
            "Lcom/glympse/android/core/GPrimitive;",
            "Lcom/glympse/android/hal/GHashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/glympse/android/core/GPrimitive;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/glympse/android/lib/e;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/glympse/android/lib/ah;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 45
    iput-object p2, p0, Lcom/glympse/android/lib/ah;->jC:Ljava/lang/String;

    .line 46
    iput-object p3, p0, Lcom/glympse/android/lib/ah;->jD:Lcom/glympse/android/core/GPrimitive;

    .line 47
    iput-object p4, p0, Lcom/glympse/android/lib/ah;->jE:Lcom/glympse/android/hal/GHashtable;

    .line 48
    new-instance v0, Lcom/glympse/android/lib/g;

    invoke-direct {v0}, Lcom/glympse/android/lib/g;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/ah;->ga:Lcom/glympse/android/lib/g;

    .line 49
    iget-object v0, p0, Lcom/glympse/android/lib/ah;->ga:Lcom/glympse/android/lib/g;

    iput-object v0, p0, Lcom/glympse/android/lib/ah;->gB:Lcom/glympse/android/lib/f;

    .line 50
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 137
    new-instance v0, Lcom/glympse/android/lib/g;

    invoke-direct {v0}, Lcom/glympse/android/lib/g;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/ah;->ga:Lcom/glympse/android/lib/g;

    .line 138
    iget-object v0, p0, Lcom/glympse/android/lib/ah;->ga:Lcom/glympse/android/lib/g;

    iput-object v0, p0, Lcom/glympse/android/lib/ah;->gB:Lcom/glympse/android/lib/f;

    .line 139
    return-void
.end method

.method public methodType()I
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x2

    return v0
.end method

.method public post()Ljava/lang/String;
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/glympse/android/lib/ah;->fZ:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/glympse/android/lib/ah;->fZ:Ljava/lang/String;

    .line 117
    :goto_0
    return-object v0

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/ah;->jD:Lcom/glympse/android/core/GPrimitive;

    iget-object v1, p0, Lcom/glympse/android/lib/ah;->jD:Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v1}, Lcom/glympse/android/core/GPrimitive;->size()I

    move-result v1

    mul-int/lit16 v1, v1, 0x80

    invoke-static {v0, v1}, Lcom/glympse/android/lib/json/JsonSerializer;->toString(Lcom/glympse/android/core/GPrimitive;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/ah;->fZ:Ljava/lang/String;

    .line 117
    iget-object v0, p0, Lcom/glympse/android/lib/ah;->fZ:Ljava/lang/String;

    goto :goto_0
.end method

.method public process()Z
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lcom/glympse/android/lib/ah;->ga:Lcom/glympse/android/lib/g;

    iget-object v0, v0, Lcom/glympse/android/lib/g;->gF:Ljava/lang/String;

    const-string v1, "ok"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/glympse/android/lib/ah;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getServerPost()Lcom/glympse/android/lib/GServerPost;

    move-result-object v0

    const/16 v1, 0x1000

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GServerPost;->rememberEvents(I)V

    .line 132
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 130
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public shouldAdd(Lcom/glympse/android/lib/GApiEndpoint;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 58
    instance-of v0, p1, Lcom/glympse/android/lib/ah;

    if-nez v0, :cond_0

    move v0, v1

    .line 89
    :goto_0
    return v0

    .line 63
    :cond_0
    check-cast p1, Lcom/glympse/android/lib/ah;

    .line 67
    iget-object v0, p0, Lcom/glympse/android/lib/ah;->jC:Ljava/lang/String;

    iget-object v2, p1, Lcom/glympse/android/lib/ah;->jC:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/glympse/android/hal/Helpers;->safeEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/glympse/android/lib/ah;->jE:Lcom/glympse/android/hal/GHashtable;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/glympse/android/lib/ah;->jE:Lcom/glympse/android/hal/GHashtable;

    if-nez v0, :cond_2

    :cond_1
    move v0, v1

    .line 70
    goto :goto_0

    .line 76
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/ah;->jE:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GHashtable;->keys()Ljava/util/Enumeration;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 78
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 79
    iget-object v3, p1, Lcom/glympse/android/lib/ah;->jE:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v3, v0}, Lcom/glympse/android/hal/GHashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 82
    goto :goto_0

    .line 89
    :cond_4
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public url(Ljava/lang/StringBuilder;)Z
    .locals 2

    .prologue
    .line 94
    const-string v0, "tickets/append_data"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    iget-object v0, p0, Lcom/glympse/android/lib/ah;->jC:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 96
    :goto_0
    if-eqz v0, :cond_0

    .line 98
    const-string v1, "?ids="

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    iget-object v1, p0, Lcom/glympse/android/lib/ah;->jC:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    :cond_0
    return v0

    .line 95
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
