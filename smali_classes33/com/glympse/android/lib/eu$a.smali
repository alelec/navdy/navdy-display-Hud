.class Lcom/glympse/android/lib/eu$a;
.super Lcom/glympse/android/lib/f;
.source "ReplyParsers.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/lib/eu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field _name:Ljava/lang/String;

.field pc:D

.field pd:D

.field sC:Lcom/glympse/android/lib/eu$c;


# direct methods
.method public constructor <init>(Lcom/glympse/android/lib/json/GJsonHandlerStack;Lcom/glympse/android/lib/eu$c;)V
    .locals 0

    .prologue
    .line 184
    invoke-direct {p0}, Lcom/glympse/android/lib/f;-><init>()V

    .line 185
    iput-object p1, p0, Lcom/glympse/android/lib/eu$a;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    .line 186
    iput-object p2, p0, Lcom/glympse/android/lib/eu$a;->sC:Lcom/glympse/android/lib/eu$c;

    .line 187
    return-void
.end method


# virtual methods
.method public endObject(I)Z
    .locals 7

    .prologue
    .line 191
    packed-switch p1, :pswitch_data_0

    .line 200
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 195
    :pswitch_0
    iget-object v0, p0, Lcom/glympse/android/lib/eu$a;->sC:Lcom/glympse/android/lib/eu$c;

    iget-object v0, v0, Lcom/glympse/android/lib/eu$c;->po:Lcom/glympse/android/lib/GTicketPrivate;

    new-instance v1, Lcom/glympse/android/lib/eg;

    iget-wide v2, p0, Lcom/glympse/android/lib/eu$a;->pc:D

    iget-wide v4, p0, Lcom/glympse/android/lib/eu$a;->pd:D

    iget-object v6, p0, Lcom/glympse/android/lib/eu$a;->_name:Ljava/lang/String;

    invoke-direct/range {v1 .. v6}, Lcom/glympse/android/lib/eg;-><init>(DDLjava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GTicketPrivate;->setDestination(Lcom/glympse/android/api/GPlace;)V

    .line 196
    iget-object v0, p0, Lcom/glympse/android/lib/eu$a;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    invoke-interface {v0}, Lcom/glympse/android/lib/json/GJsonHandlerStack;->popHandler()V

    goto :goto_0

    .line 191
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public primitive(ILcom/glympse/android/lib/json/GJsonPrimitive;)Z
    .locals 2

    .prologue
    .line 205
    packed-switch p1, :pswitch_data_0

    .line 224
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 209
    :pswitch_0
    iget-object v0, p0, Lcom/glympse/android/lib/eu$a;->gE:Ljava/lang/String;

    const-string v1, "name"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 211
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/eu$a;->_name:Ljava/lang/String;

    goto :goto_0

    .line 213
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/eu$a;->gE:Ljava/lang/String;

    const-string v1, "lat"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 215
    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/glympse/android/lib/eu$a;->pc:D

    goto :goto_0

    .line 217
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/eu$a;->gE:Ljava/lang/String;

    const-string v1, "lng"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 219
    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/glympse/android/lib/eu$a;->pd:D

    goto :goto_0

    .line 205
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
