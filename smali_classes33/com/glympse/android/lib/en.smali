.class Lcom/glympse/android/lib/en;
.super Ljava/lang/Object;
.source "PlatformArrivalProvider.java"

# interfaces
.implements Lcom/glympse/android/lib/be;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/lib/en$a;
    }
.end annotation


# instance fields
.field private cS:Lcom/glympse/android/api/GGlympse;

.field private hS:Lcom/glympse/android/lib/GLocationManagerPrivate;

.field private hk:Lcom/glympse/android/lib/bd;

.field private sc:Lcom/glympse/android/hal/GHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GHashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/glympse/android/api/GTicket;",
            ">;"
        }
    .end annotation
.end field

.field private sd:D

.field private se:D

.field private sf:Lcom/glympse/android/lib/en$a;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const-wide/high16 v0, 0x404e000000000000L    # 60.0

    iput-wide v0, p0, Lcom/glympse/android/lib/en;->sd:D

    .line 33
    const-wide v0, 0x408f400000000000L    # 1000.0

    iput-wide v0, p0, Lcom/glympse/android/lib/en;->se:D

    .line 34
    return-void
.end method

.method private c(Lcom/glympse/android/api/GTicket;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 174
    invoke-static {}, Lcom/glympse/android/lib/Debug;->getLevel()I

    move-result v0

    if-gt v0, v1, :cond_0

    .line 176
    const-string v0, "[Ticket arrival triggered] Provider: PlatformArrivalProvider "

    invoke-static {v1, v0}, Lcom/glympse/android/hal/Helpers;->log(ILjava/lang/String;)V

    .line 179
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/en;->hk:Lcom/glympse/android/lib/bd;

    if-eqz v0, :cond_1

    .line 181
    iget-object v0, p0, Lcom/glympse/android/lib/en;->hk:Lcom/glympse/android/lib/bd;

    invoke-interface {v0, p1}, Lcom/glympse/android/lib/bd;->c(Lcom/glympse/android/api/GTicket;)V

    .line 183
    :cond_1
    return-void
.end method

.method private d(Lcom/glympse/android/core/GRegion;)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 214
    iget-object v2, p0, Lcom/glympse/android/lib/en;->hS:Lcom/glympse/android/lib/GLocationManagerPrivate;

    invoke-interface {v2}, Lcom/glympse/android/lib/GLocationManagerPrivate;->getLocationProvider()Lcom/glympse/android/core/GLocationProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/glympse/android/core/GLocationProvider;->getLastKnownLocation()Lcom/glympse/android/core/GLocation;

    move-result-object v2

    .line 215
    if-nez v2, :cond_1

    .line 239
    :cond_0
    :goto_0
    return v0

    .line 220
    :cond_1
    invoke-interface {v2, p1}, Lcom/glympse/android/core/GLocation;->distanceTo(Lcom/glympse/android/core/GLatLng;)F

    move-result v2

    float-to-double v2, v2

    .line 222
    invoke-static {}, Lcom/glympse/android/lib/Debug;->getLevel()I

    move-result v4

    if-gt v4, v0, :cond_2

    .line 224
    const/16 v4, 0x1f4

    invoke-static {v4}, Lcom/glympse/android/core/CoreFactory;->createStringBuilder(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 225
    const-string v5, "[PlatformArrivalProvider.isLocationClose] Distance: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 226
    invoke-static {v2, v3, v1}, Lcom/glympse/android/hal/Helpers;->toString(DI)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 227
    const-string v5, " Filter radius: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 228
    iget-wide v6, p0, Lcom/glympse/android/lib/en;->se:D

    invoke-static {v6, v7, v1}, Lcom/glympse/android/hal/Helpers;->toString(DI)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 229
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/glympse/android/hal/Helpers;->log(ILjava/lang/String;)V

    .line 232
    :cond_2
    iget-wide v4, p0, Lcom/glympse/android/lib/en;->se:D

    cmpl-double v2, v2, v4

    if-lez v2, :cond_0

    .line 234
    const-string v2, "[PlatformArrivalProvider.isLocationClose] Filtering out region entered event with distance > filter radius."

    invoke-static {v0, v2}, Lcom/glympse/android/hal/Helpers;->log(ILjava/lang/String;)V

    move v0, v1

    .line 236
    goto :goto_0
.end method

.method private r(Lcom/glympse/android/lib/GTicketPrivate;)V
    .locals 11

    .prologue
    .line 134
    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getDestination()Lcom/glympse/android/api/GPlace;

    move-result-object v0

    .line 136
    if-nez v0, :cond_0

    .line 154
    :goto_0
    return-void

    .line 141
    :cond_0
    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getId()Ljava/lang/String;

    move-result-object v10

    .line 144
    new-instance v1, Lcom/glympse/android/lib/es;

    invoke-interface {v0}, Lcom/glympse/android/api/GPlace;->getLatitude()D

    move-result-wide v2

    invoke-interface {v0}, Lcom/glympse/android/api/GPlace;->getLongitude()D

    move-result-wide v4

    iget-wide v6, p0, Lcom/glympse/android/lib/en;->sd:D

    const-wide/high16 v8, 0x4008000000000000L    # 3.0

    invoke-direct/range {v1 .. v10}, Lcom/glympse/android/lib/es;-><init>(DDDDLjava/lang/String;)V

    .line 147
    iget-object v0, p0, Lcom/glympse/android/lib/en;->sc:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, v10, p1}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    invoke-interface {p1, v1}, Lcom/glympse/android/lib/GTicketPrivate;->setXoaRegion(Lcom/glympse/android/core/GRegion;)V

    .line 153
    iget-object v0, p0, Lcom/glympse/android/lib/en;->hS:Lcom/glympse/android/lib/GLocationManagerPrivate;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GLocationManagerPrivate;->startMonitoring(Lcom/glympse/android/core/GRegion;)V

    goto :goto_0
.end method

.method private s(Lcom/glympse/android/lib/GTicketPrivate;)V
    .locals 2

    .prologue
    .line 158
    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getXoaRegion()Lcom/glympse/android/core/GRegion;

    move-result-object v0

    .line 159
    if-eqz v0, :cond_0

    .line 162
    iget-object v1, p0, Lcom/glympse/android/lib/en;->hS:Lcom/glympse/android/lib/GLocationManagerPrivate;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GLocationManagerPrivate;->stopMonitoring(Lcom/glympse/android/core/GRegion;)V

    .line 165
    iget-object v0, p0, Lcom/glympse/android/lib/en;->sc:Lcom/glympse/android/hal/GHashtable;

    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GHashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lcom/glympse/android/lib/GTicketPrivate;->setXoaRegion(Lcom/glympse/android/core/GRegion;)V

    .line 170
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lcom/glympse/android/api/GTicket;)V
    .locals 0

    .prologue
    .line 92
    if-nez p1, :cond_0

    .line 98
    :goto_0
    return-void

    .line 97
    :cond_0
    check-cast p1, Lcom/glympse/android/lib/GTicketPrivate;

    invoke-direct {p0, p1}, Lcom/glympse/android/lib/en;->r(Lcom/glympse/android/lib/GTicketPrivate;)V

    goto :goto_0
.end method

.method public a(Lcom/glympse/android/lib/bd;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/glympse/android/lib/en;->hk:Lcom/glympse/android/lib/bd;

    .line 88
    return-void
.end method

.method public aq()V
    .locals 4

    .prologue
    .line 112
    new-instance v2, Lcom/glympse/android/hal/GVector;

    invoke-direct {v2}, Lcom/glympse/android/hal/GVector;-><init>()V

    .line 114
    iget-object v0, p0, Lcom/glympse/android/lib/en;->sc:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GHashtable;->keys()Ljava/util/Enumeration;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 117
    iget-object v3, p0, Lcom/glympse/android/lib/en;->sc:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v3, v0}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GTicket;

    .line 118
    invoke-virtual {v2, v0}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    goto :goto_0

    .line 121
    :cond_0
    invoke-virtual {v2}, Lcom/glympse/android/hal/GVector;->length()I

    move-result v3

    .line 122
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    .line 124
    invoke-virtual {v2, v1}, Lcom/glympse/android/hal/GVector;->at(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GTicket;

    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/en;->b(Lcom/glympse/android/api/GTicket;)V

    .line 122
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 126
    :cond_1
    return-void
.end method

.method public b(Lcom/glympse/android/api/GTicket;)V
    .locals 0

    .prologue
    .line 102
    if-nez p1, :cond_0

    .line 108
    :goto_0
    return-void

    .line 107
    :cond_0
    check-cast p1, Lcom/glympse/android/lib/GTicketPrivate;

    invoke-direct {p0, p1}, Lcom/glympse/android/lib/en;->s(Lcom/glympse/android/lib/GTicketPrivate;)V

    goto :goto_0
.end method

.method public b(Lcom/glympse/android/core/GPrimitive;)V
    .locals 4

    .prologue
    .line 66
    if-nez p1, :cond_1

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 71
    :cond_1
    const-string v0, "filter_radius"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 72
    const-string v1, "arrival_radius"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 74
    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 76
    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    iput-wide v2, p0, Lcom/glympse/android/lib/en;->se:D

    .line 79
    :cond_2
    invoke-interface {p1, v1}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    invoke-interface {p1, v1}, Lcom/glympse/android/core/GPrimitive;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/glympse/android/lib/en;->sd:D

    goto :goto_0
.end method

.method public c(Lcom/glympse/android/core/GRegion;)V
    .locals 2

    .prologue
    .line 187
    if-nez p1, :cond_1

    .line 207
    :cond_0
    :goto_0
    return-void

    .line 193
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/en;->sc:Lcom/glympse/android/hal/GHashtable;

    invoke-interface {p1}, Lcom/glympse/android/core/GRegion;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GTicket;

    .line 194
    if-eqz v0, :cond_0

    .line 200
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/en;->d(Lcom/glympse/android/core/GRegion;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 206
    invoke-direct {p0, v0}, Lcom/glympse/android/lib/en;->c(Lcom/glympse/android/api/GTicket;)V

    goto :goto_0
.end method

.method public start(Lcom/glympse/android/api/GGlympse;)V
    .locals 3

    .prologue
    .line 42
    new-instance v0, Lcom/glympse/android/hal/GHashtable;

    invoke-direct {v0}, Lcom/glympse/android/hal/GHashtable;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/en;->sc:Lcom/glympse/android/hal/GHashtable;

    .line 44
    iput-object p1, p0, Lcom/glympse/android/lib/en;->cS:Lcom/glympse/android/api/GGlympse;

    .line 45
    invoke-interface {p1}, Lcom/glympse/android/api/GGlympse;->getLocationManager()Lcom/glympse/android/api/GLocationManager;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GLocationManagerPrivate;

    iput-object v0, p0, Lcom/glympse/android/lib/en;->hS:Lcom/glympse/android/lib/GLocationManagerPrivate;

    .line 47
    new-instance v0, Lcom/glympse/android/lib/en$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/en$a;-><init>(Lcom/glympse/android/lib/en$1;)V

    iput-object v0, p0, Lcom/glympse/android/lib/en;->sf:Lcom/glympse/android/lib/en$a;

    .line 48
    iget-object v1, p0, Lcom/glympse/android/lib/en;->sf:Lcom/glympse/android/lib/en$a;

    iget-object v2, p0, Lcom/glympse/android/lib/en;->hS:Lcom/glympse/android/lib/GLocationManagerPrivate;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/en;

    invoke-virtual {v1, v2, v0}, Lcom/glympse/android/lib/en$a;->a(Lcom/glympse/android/api/GLocationManager;Lcom/glympse/android/lib/en;)V

    .line 49
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 53
    invoke-virtual {p0}, Lcom/glympse/android/lib/en;->aq()V

    .line 55
    iget-object v0, p0, Lcom/glympse/android/lib/en;->sc:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GHashtable;->clear()V

    .line 56
    iput-object v1, p0, Lcom/glympse/android/lib/en;->sc:Lcom/glympse/android/hal/GHashtable;

    .line 58
    iget-object v0, p0, Lcom/glympse/android/lib/en;->sf:Lcom/glympse/android/lib/en$a;

    invoke-virtual {v0}, Lcom/glympse/android/lib/en$a;->stop()V

    .line 59
    iput-object v1, p0, Lcom/glympse/android/lib/en;->sf:Lcom/glympse/android/lib/en$a;

    .line 61
    iput-object v1, p0, Lcom/glympse/android/lib/en;->cS:Lcom/glympse/android/api/GGlympse;

    .line 62
    return-void
.end method
