.class public interface abstract Lcom/glympse/android/lib/GHistoryManagerPrivate;
.super Ljava/lang/Object;
.source "GHistoryManagerPrivate.java"

# interfaces
.implements Lcom/glympse/android/api/GHistoryManager;
.implements Lcom/glympse/android/lib/GTicketParent;


# virtual methods
.method public abstract addTicket(Lcom/glympse/android/lib/GTicketPrivate;)V
.end method

.method public abstract clearLatestExpireTime()V
.end method

.method public abstract completePending()V
.end method

.method public abstract completeTicket(Lcom/glympse/android/lib/GTicketPrivate;)V
.end method

.method public abstract isActive(Lcom/glympse/android/api/GTicket;J)Z
.end method

.method public abstract removeTicket(Lcom/glympse/android/lib/GTicketPrivate;)V
.end method

.method public abstract sendTicket(Lcom/glympse/android/api/GTicket;)Z
.end method

.method public abstract sendTicketPhase2(Lcom/glympse/android/lib/GTicketPrivate;ILcom/glympse/android/hal/GHashtable;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/glympse/android/lib/GTicketPrivate;",
            "I",
            "Lcom/glympse/android/hal/GHashtable",
            "<",
            "Ljava/lang/Long;",
            "Lcom/glympse/android/hal/GHashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/glympse/android/api/GDataRow;",
            ">;>;)V"
        }
    .end annotation
.end method

.method public abstract setActive(Z)V
.end method

.method public abstract setLastViewTime(J)V
.end method

.method public abstract start(Lcom/glympse/android/lib/GGlympsePrivate;)V
.end method

.method public abstract stop()V
.end method

.method public abstract syncFresh()V
.end method

.method public abstract syncRefresh()V
.end method

.method public abstract syncedWithServer(Lcom/glympse/android/lib/InternalStructs$a;Z)V
.end method

.method public abstract updateState(J)V
.end method
