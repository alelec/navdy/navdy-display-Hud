.class Lcom/glympse/android/lib/d$c;
.super Ljava/lang/Object;
.source "AccountManager.java"

# interfaces
.implements Lcom/glympse/android/api/GEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/lib/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "c"
.end annotation


# instance fields
.field private _glympse:Lcom/glympse/android/lib/GGlympsePrivate;

.field private _jobQueue:Lcom/glympse/android/lib/GJobQueue;

.field private fX:Ljava/lang/String;

.field private fY:Lcom/glympse/android/core/GPrimitive;

.field private gr:Lcom/glympse/android/api/GEventSink;


# direct methods
.method public constructor <init>(Lcom/glympse/android/lib/GGlympsePrivate;Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V
    .locals 0

    .prologue
    .line 495
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 496
    iput-object p1, p0, Lcom/glympse/android/lib/d$c;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 497
    iput-object p2, p0, Lcom/glympse/android/lib/d$c;->fX:Ljava/lang/String;

    .line 498
    iput-object p3, p0, Lcom/glympse/android/lib/d$c;->fY:Lcom/glympse/android/core/GPrimitive;

    .line 499
    return-void
.end method

.method private ap()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 542
    iget-object v0, p0, Lcom/glympse/android/lib/d$c;->_jobQueue:Lcom/glympse/android/lib/GJobQueue;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GJobQueue;->stop(Z)V

    .line 543
    iput-object v2, p0, Lcom/glympse/android/lib/d$c;->_jobQueue:Lcom/glympse/android/lib/GJobQueue;

    .line 548
    iget-object v0, p0, Lcom/glympse/android/lib/d$c;->gr:Lcom/glympse/android/api/GEventSink;

    invoke-static {v0}, Lcom/glympse/android/lib/CommonSink;->removeAllListeners(Lcom/glympse/android/api/GEventSink;)V

    .line 549
    iput-object v2, p0, Lcom/glympse/android/lib/d$c;->gr:Lcom/glympse/android/api/GEventSink;

    .line 550
    return-void
.end method


# virtual methods
.method public ao()Lcom/glympse/android/api/GEventSink;
    .locals 7

    .prologue
    const/4 v2, 0x1

    .line 504
    new-instance v0, Lcom/glympse/android/lib/ay;

    const-string v1, "ConfirmationCode"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/ay;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/glympse/android/lib/d$c;->gr:Lcom/glympse/android/api/GEventSink;

    .line 507
    iget-object v1, p0, Lcom/glympse/android/lib/d$c;->gr:Lcom/glympse/android/api/GEventSink;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GEventListener;

    invoke-interface {v1, v0}, Lcom/glympse/android/api/GEventSink;->addListener(Lcom/glympse/android/api/GEventListener;)Z

    .line 510
    new-instance v0, Lcom/glympse/android/lib/df;

    iget-object v1, p0, Lcom/glympse/android/lib/d$c;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->getHandler()Lcom/glympse/android/core/GHandler;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/df;-><init>(Lcom/glympse/android/core/GHandler;)V

    iput-object v0, p0, Lcom/glympse/android/lib/d$c;->_jobQueue:Lcom/glympse/android/lib/GJobQueue;

    .line 511
    iget-object v0, p0, Lcom/glympse/android/lib/d$c;->_jobQueue:Lcom/glympse/android/lib/GJobQueue;

    invoke-interface {v0, v2}, Lcom/glympse/android/lib/GJobQueue;->start(I)Z

    .line 512
    iget-object v0, p0, Lcom/glympse/android/lib/d$c;->_jobQueue:Lcom/glympse/android/lib/GJobQueue;

    invoke-interface {v0, v2}, Lcom/glympse/android/lib/GJobQueue;->setActive(Z)V

    .line 515
    iget-object v0, p0, Lcom/glympse/android/lib/d$c;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getServerPost()Lcom/glympse/android/lib/GServerPost;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/lib/GServerPost;->isSslEnabled()Z

    move-result v0

    .line 516
    iget-object v1, p0, Lcom/glympse/android/lib/d$c;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/glympse/android/lib/UrlParser;->prepareAuthUrlServer(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 517
    new-instance v2, Lcom/glympse/android/lib/a;

    iget-object v3, p0, Lcom/glympse/android/lib/d$c;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget-object v4, p0, Lcom/glympse/android/lib/d$c;->gr:Lcom/glympse/android/api/GEventSink;

    iget-object v5, p0, Lcom/glympse/android/lib/d$c;->fX:Ljava/lang/String;

    iget-object v6, p0, Lcom/glympse/android/lib/d$c;->fY:Lcom/glympse/android/core/GPrimitive;

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/glympse/android/lib/a;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/api/GEventSink;Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 518
    new-instance v3, Lcom/glympse/android/lib/au;

    invoke-direct {v3, v0, v1, v2}, Lcom/glympse/android/lib/au;-><init>(ZLjava/lang/String;Lcom/glympse/android/lib/GApiEndpoint;)V

    .line 519
    iget-object v0, p0, Lcom/glympse/android/lib/d$c;->_jobQueue:Lcom/glympse/android/lib/GJobQueue;

    invoke-interface {v0, v3}, Lcom/glympse/android/lib/GJobQueue;->addJob(Lcom/glympse/android/lib/GJob;)V

    .line 521
    iget-object v0, p0, Lcom/glympse/android/lib/d$c;->gr:Lcom/glympse/android/api/GEventSink;

    return-object v0
.end method

.method public eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 526
    const/16 v0, 0x14

    if-ne v0, p2, :cond_1

    .line 528
    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_0

    .line 530
    invoke-direct {p0}, Lcom/glympse/android/lib/d$c;->ap()V

    .line 532
    :cond_0
    and-int/lit8 v0, p3, 0x2

    if-eqz v0, :cond_1

    .line 534
    invoke-direct {p0}, Lcom/glympse/android/lib/d$c;->ap()V

    .line 537
    :cond_1
    return-void
.end method
