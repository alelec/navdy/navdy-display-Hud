.class Lcom/glympse/android/lib/fw$a;
.super Ljava/lang/Object;
.source "TriggersManager.java"

# interfaces
.implements Lcom/glympse/android/api/GEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/lib/fw;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private cS:Lcom/glympse/android/api/GGlympse;

.field private sc:Lcom/glympse/android/hal/GHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GHashtable",
            "<",
            "Lcom/glympse/android/api/GTicket;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private uf:Lcom/glympse/android/lib/fw;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 838
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/glympse/android/lib/fw$1;)V
    .locals 0

    .prologue
    .line 838
    invoke-direct {p0}, Lcom/glympse/android/lib/fw$a;-><init>()V

    return-void
.end method

.method private k(Lcom/glympse/android/api/GTicket;)V
    .locals 2

    .prologue
    .line 916
    iget-object v1, p0, Lcom/glympse/android/lib/fw$a;->uf:Lcom/glympse/android/lib/fw;

    iget-object v0, p0, Lcom/glympse/android/lib/fw$a;->sc:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/glympse/android/lib/fw;->c(Lcom/glympse/android/lib/fw;Ljava/lang/String;)V

    .line 917
    invoke-virtual {p0, p1}, Lcom/glympse/android/lib/fw$a;->j(Lcom/glympse/android/api/GTicket;)V

    .line 918
    return-void
.end method


# virtual methods
.method public a(Lcom/glympse/android/api/GGlympse;Lcom/glympse/android/lib/fw;)V
    .locals 2

    .prologue
    .line 846
    iput-object p1, p0, Lcom/glympse/android/lib/fw$a;->cS:Lcom/glympse/android/api/GGlympse;

    .line 847
    iput-object p2, p0, Lcom/glympse/android/lib/fw$a;->uf:Lcom/glympse/android/lib/fw;

    .line 848
    new-instance v0, Lcom/glympse/android/hal/GHashtable;

    invoke-direct {v0}, Lcom/glympse/android/hal/GHashtable;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/fw$a;->sc:Lcom/glympse/android/hal/GHashtable;

    .line 850
    iget-object v0, p0, Lcom/glympse/android/lib/fw$a;->cS:Lcom/glympse/android/api/GGlympse;

    invoke-virtual {p0}, Lcom/glympse/android/lib/fw$a;->ar()Lcom/glympse/android/api/GEventListener;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/api/GGlympse;->addListener(Lcom/glympse/android/api/GEventListener;)Z

    .line 851
    return-void
.end method

.method public a(Lcom/glympse/android/api/GTicket;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 869
    invoke-virtual {p0}, Lcom/glympse/android/lib/fw$a;->ar()Lcom/glympse/android/api/GEventListener;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/api/GTicket;->addListener(Lcom/glympse/android/api/GEventListener;)Z

    .line 870
    iget-object v0, p0, Lcom/glympse/android/lib/fw$a;->sc:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1, p2}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 871
    return-void
.end method

.method protected ar()Lcom/glympse/android/api/GEventListener;
    .locals 1

    .prologue
    .line 922
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GEventListener;

    return-object v0
.end method

.method public eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 881
    const/4 v0, 0x1

    if-ne v0, p2, :cond_2

    .line 883
    and-int/lit16 v0, p3, 0x80

    if-eqz v0, :cond_0

    .line 885
    iget-object v0, p0, Lcom/glympse/android/lib/fw$a;->uf:Lcom/glympse/android/lib/fw;

    invoke-virtual {v0}, Lcom/glympse/android/lib/fw;->da()V

    .line 887
    :cond_0
    const/high16 v0, 0x40000

    and-int/2addr v0, p3

    if-eqz v0, :cond_1

    .line 889
    check-cast p4, Lcom/glympse/android/api/GTicket;

    .line 890
    iget-object v0, p0, Lcom/glympse/android/lib/fw$a;->sc:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p4}, Lcom/glympse/android/hal/GHashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 892
    invoke-direct {p0, p4}, Lcom/glympse/android/lib/fw$a;->k(Lcom/glympse/android/api/GTicket;)V

    .line 912
    :cond_1
    :goto_0
    return-void

    .line 896
    :cond_2
    const/4 v0, 0x4

    if-ne v0, p2, :cond_1

    .line 898
    and-int/lit8 v0, p3, 0x2

    if-eqz v0, :cond_3

    move-object v0, p4

    .line 900
    check-cast v0, Lcom/glympse/android/api/GTicket;

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/fw$a;->k(Lcom/glympse/android/api/GTicket;)V

    .line 902
    :cond_3
    const/high16 v0, 0x4000000

    and-int/2addr v0, p3

    if-eqz v0, :cond_4

    move-object v0, p4

    .line 904
    check-cast v0, Lcom/glympse/android/api/GTicket;

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/fw$a;->k(Lcom/glympse/android/api/GTicket;)V

    .line 906
    :cond_4
    and-int/lit16 v0, p3, 0x100

    if-eqz v0, :cond_1

    .line 908
    check-cast p4, Lcom/glympse/android/api/GTicket;

    .line 909
    iget-object v1, p0, Lcom/glympse/android/lib/fw$a;->uf:Lcom/glympse/android/lib/fw;

    iget-object v0, p0, Lcom/glympse/android/lib/fw$a;->sc:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p4}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/glympse/android/lib/fw;->b(Lcom/glympse/android/lib/fw;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public j(Lcom/glympse/android/api/GTicket;)V
    .locals 1

    .prologue
    .line 875
    invoke-virtual {p0}, Lcom/glympse/android/lib/fw$a;->ar()Lcom/glympse/android/api/GEventListener;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/api/GTicket;->removeListener(Lcom/glympse/android/api/GEventListener;)Z

    .line 876
    iget-object v0, p0, Lcom/glympse/android/lib/fw$a;->sc:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 877
    return-void
.end method

.method public stop()V
    .locals 3

    .prologue
    .line 855
    iget-object v0, p0, Lcom/glympse/android/lib/fw$a;->cS:Lcom/glympse/android/api/GGlympse;

    invoke-virtual {p0}, Lcom/glympse/android/lib/fw$a;->ar()Lcom/glympse/android/api/GEventListener;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/api/GGlympse;->removeListener(Lcom/glympse/android/api/GEventListener;)Z

    .line 857
    iget-object v0, p0, Lcom/glympse/android/lib/fw$a;->sc:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GHashtable;->keys()Ljava/util/Enumeration;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 859
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GTicket;

    .line 860
    invoke-virtual {p0}, Lcom/glympse/android/lib/fw$a;->ar()Lcom/glympse/android/api/GEventListener;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/glympse/android/api/GTicket;->removeListener(Lcom/glympse/android/api/GEventListener;)Z

    goto :goto_0

    .line 863
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/fw$a;->sc:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GHashtable;->clear()V

    .line 864
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/fw$a;->sc:Lcom/glympse/android/hal/GHashtable;

    .line 865
    return-void
.end method
