.class Lcom/glympse/android/lib/x;
.super Ljava/lang/Object;
.source "Config.java"

# interfaces
.implements Lcom/glympse/android/lib/GConfigPrivate;


# instance fields
.field private KEY_ACCOUNTS:Ljava/lang/String;

.field private _glympse:Lcom/glympse/android/lib/GGlympsePrivate;

.field private fW:Ljava/lang/String;

.field private he:Lcom/glympse/android/lib/CommonSink;

.field private iA:Z

.field private iB:Z

.field private iC:Z

.field private iD:Lcom/glympse/android/core/GPrimitive;

.field private iE:Lcom/glympse/android/hal/GKeychain;

.field private iF:Ljava/lang/String;

.field private iG:Ljava/lang/String;

.field private iH:Ljava/lang/String;

.field private iI:Lcom/glympse/android/core/GPrimitive;

.field private iJ:Lcom/glympse/android/core/GPrimitive;

.field private iK:Lcom/glympse/android/core/GPrimitive;

.field private iL:Lcom/glympse/android/hal/GVector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GVector",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private iM:Ljava/lang/String;

.field private iN:Ljava/lang/String;

.field private iO:Ljava/lang/String;

.field private iP:Ljava/lang/String;

.field private iu:Lcom/glympse/android/hal/GContextHolder;

.field private iv:Ljava/lang/String;

.field private iw:Ljava/lang/String;

.field private ix:Ljava/lang/String;

.field private iy:Lcom/glympse/android/lib/fb;

.field private iz:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 323
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 324
    new-instance v0, Lcom/glympse/android/lib/fb;

    invoke-direct {v0}, Lcom/glympse/android/lib/fb;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/x;->iy:Lcom/glympse/android/lib/fb;

    .line 325
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/glympse/android/lib/x;->iz:Z

    .line 326
    iput-boolean v1, p0, Lcom/glympse/android/lib/x;->iA:Z

    .line 327
    iput-boolean v1, p0, Lcom/glympse/android/lib/x;->iB:Z

    .line 328
    iput-boolean v1, p0, Lcom/glympse/android/lib/x;->iC:Z

    .line 329
    new-instance v0, Lcom/glympse/android/lib/CommonSink;

    const-string v1, "Config"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/CommonSink;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/glympse/android/lib/x;->he:Lcom/glympse/android/lib/CommonSink;

    .line 330
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/x;->iL:Lcom/glympse/android/hal/GVector;

    .line 332
    const-string v0, "glympse_device_v2"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/x;->iM:Ljava/lang/String;

    .line 333
    const-string v0, "glympse_accounts_v2"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/x;->KEY_ACCOUNTS:Ljava/lang/String;

    .line 334
    const-string v0, "glympse_tokens_v2"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/x;->iN:Ljava/lang/String;

    .line 335
    const-string v0, "g.shareLoc"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/x;->iO:Ljava/lang/String;

    .line 336
    const-string v0, "g.shareSp"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/x;->iP:Ljava/lang/String;

    .line 337
    return-void
.end method

.method public static a(Lcom/glympse/android/hal/GContextHolder;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 627
    invoke-interface {p0}, Lcom/glympse/android/hal/GContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/glympse/android/hal/Platform;->getAppId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 628
    invoke-static {v0}, Lcom/glympse/android/hal/Platform;->sha1(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/glympse/android/hal/GDirectory;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 634
    const-string v0, "config_v2"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/glympse/android/lib/fb;->e(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/glympse/android/hal/GDirectory;->deleteFile(Ljava/lang/String;)Z

    .line 635
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 819
    new-instance v0, Lcom/glympse/android/lib/Primitive;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 820
    const-string v1, "key"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 821
    const-string v1, "un"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 822
    const-string v1, "psw"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p4}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 823
    iget-object v1, p0, Lcom/glympse/android/lib/x;->iJ:Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v1, p1, v0}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 825
    invoke-direct {p0}, Lcom/glympse/android/lib/x;->aO()V

    .line 826
    return-void
.end method

.method private aK()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x7

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 170
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    if-nez v0, :cond_0

    .line 172
    new-instance v0, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v0, v6}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    iput-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.label"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 177
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.label"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    :cond_1
    const-string v0, "g.tkSt"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/glympse/android/lib/x;->d(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/glympse/android/lib/x;->iA:Z

    .line 182
    const-string v0, "g.accLnkd"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/glympse/android/lib/x;->d(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/glympse/android/lib/x;->iB:Z

    .line 183
    const-string v0, "g.prGr"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/glympse/android/lib/x;->d(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/glympse/android/lib/x;->iC:Z

    .line 186
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    iget-object v1, p0, Lcom/glympse/android/lib/x;->iO:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 187
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    iget-object v1, p0, Lcom/glympse/android/lib/x;->iO:Ljava/lang/String;

    invoke-interface {v0, v1, v4}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Z)V

    .line 188
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    iget-object v1, p0, Lcom/glympse/android/lib/x;->iP:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 189
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    iget-object v1, p0, Lcom/glympse/android/lib/x;->iP:Ljava/lang/String;

    invoke-interface {v0, v1, v4}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Z)V

    .line 190
    :cond_3
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.pGrAWa"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 191
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.pGrAWa"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v5}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Z)V

    .line 192
    :cond_4
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.tkTrmEn"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 193
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.tkTrmEn"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v4}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Z)V

    .line 194
    :cond_5
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.expOnAr"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 195
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.expOnAr"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x1

    invoke-interface {v0, v1, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 196
    :cond_6
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.frcGlyProx"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 197
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.frcGlyProx"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v5}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Z)V

    .line 198
    :cond_7
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.debug"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 199
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.debug"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v5}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Z)V

    .line 200
    :cond_8
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.initProf"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 201
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.initProf"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v4}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Z)V

    .line 202
    :cond_9
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.frcRfr"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 203
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.frcRfr"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v5}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Z)V

    .line 206
    :cond_a
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.dbgLog"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 207
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.dbgLog"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v8, v9}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 208
    :cond_b
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.fileLog"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 209
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.fileLog"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v8, v9}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 210
    :cond_c
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.logUrl"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 211
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.logUrl"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/glympse/android/lib/StaticConfig;->LOG_URL()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    :cond_d
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.logUpFrq"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 213
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.logUpFrq"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 216
    :cond_e
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.getRt"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 217
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.getRt"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x1388

    invoke-interface {v0, v1, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 218
    :cond_f
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.mxTktLen"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 219
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.mxTktLen"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-wide/32 v2, 0xdbba00

    invoke-interface {v0, v1, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 220
    :cond_10
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.ptRtPrd"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 221
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.ptRtPrd"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-wide/32 v2, 0x1d4c0

    invoke-interface {v0, v1, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 222
    :cond_11
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.mxNmLen"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_12

    .line 223
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.mxNmLen"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x40

    invoke-interface {v0, v1, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 224
    :cond_12
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.ptRtPlgHg"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_13

    .line 225
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.ptRtPlgHg"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v4}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Z)V

    .line 226
    :cond_13
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.accPlgHg"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_14

    .line 227
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.accPlgHg"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v4}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Z)V

    .line 228
    :cond_14
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.ptRtStLw"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_15

    .line 229
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.ptRtStLw"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v5}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Z)V

    .line 230
    :cond_15
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.accStLw"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_16

    .line 231
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.accStLw"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v5}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Z)V

    .line 232
    :cond_16
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.invPolPsh"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_17

    .line 233
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.invPolPsh"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v4}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Z)V

    .line 236
    :cond_17
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.dirProv"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_18

    .line 238
    new-instance v0, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v0, v6}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 239
    const-string v1, "src"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x4

    invoke-interface {v0, v1, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 240
    iget-object v1, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v2, "g.dirProv"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 244
    :cond_18
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.advXoaPrf"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_19

    .line 246
    invoke-static {v4}, Lcom/glympse/android/core/CoreFactory;->createPrimitive(I)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 248
    invoke-static {v6}, Lcom/glympse/android/core/CoreFactory;->createPrimitive(I)Lcom/glympse/android/core/GPrimitive;

    move-result-object v1

    .line 249
    const-string v2, "id"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "platform_prox"

    invoke-static {v3}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    const-string v2, "filter_radius"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-wide v4, 0x408f400000000000L    # 1000.0

    invoke-interface {v1, v2, v4, v5}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;D)V

    .line 251
    const-string v2, "arrival_radius"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-wide/high16 v4, 0x404e000000000000L    # 60.0

    invoke-interface {v1, v2, v4, v5}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;D)V

    .line 253
    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->put(Lcom/glympse/android/core/GPrimitive;)V

    .line 255
    iget-object v1, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v2, "g.advXoaPrf"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 259
    :cond_19
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iJ:Lcom/glympse/android/core/GPrimitive;

    if-nez v0, :cond_1a

    .line 261
    new-instance v0, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v0, v6}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    iput-object v0, p0, Lcom/glympse/android/lib/x;->iJ:Lcom/glympse/android/core/GPrimitive;

    .line 265
    :cond_1a
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iK:Lcom/glympse/android/core/GPrimitive;

    if-nez v0, :cond_1b

    .line 267
    new-instance v0, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v0, v6}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    iput-object v0, p0, Lcom/glympse/android/lib/x;->iK:Lcom/glympse/android/core/GPrimitive;

    .line 269
    :cond_1b
    return-void
.end method

.method private aL()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 273
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iI:Lcom/glympse/android/core/GPrimitive;

    if-nez v0, :cond_0

    .line 278
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iv:Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/glympse/android/lib/x;->iM:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/glympse/android/lib/fb;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 279
    iget-object v1, p0, Lcom/glympse/android/lib/x;->iE:Lcom/glympse/android/hal/GKeychain;

    invoke-interface {v1, v0, v3}, Lcom/glympse/android/hal/GKeychain;->load(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 280
    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 283
    new-instance v0, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v0, v4}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    iput-object v0, p0, Lcom/glympse/android/lib/x;->iI:Lcom/glympse/android/core/GPrimitive;

    .line 296
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iI:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "id"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 298
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iI:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "id"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/glympse/android/hal/Platform;->generateDeviceId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iI:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "push"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 304
    new-instance v0, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v0, v4}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 305
    iget-object v1, p0, Lcom/glympse/android/lib/x;->iI:Lcom/glympse/android/core/GPrimitive;

    const-string v2, "push"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 307
    :cond_2
    return-void

    .line 288
    :cond_3
    invoke-static {v1}, Lcom/glympse/android/lib/json/JsonSerializer;->toPrimitive(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v1

    iput-object v1, p0, Lcom/glympse/android/lib/x;->iI:Lcom/glympse/android/core/GPrimitive;

    .line 289
    iget-object v1, p0, Lcom/glympse/android/lib/x;->iE:Lcom/glympse/android/hal/GKeychain;

    invoke-interface {v1, v0, v3}, Lcom/glympse/android/hal/GKeychain;->remove(Ljava/lang/String;Z)Z

    .line 291
    invoke-direct {p0}, Lcom/glympse/android/lib/x;->aN()V

    goto :goto_0
.end method

.method private aM()V
    .locals 2

    .prologue
    .line 604
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iy:Lcom/glympse/android/lib/fb;

    iget-object v1, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    invoke-virtual {v0, v1}, Lcom/glympse/android/lib/fb;->save(Lcom/glympse/android/core/GPrimitive;)V

    .line 605
    return-void
.end method

.method private aN()V
    .locals 4

    .prologue
    .line 610
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iE:Lcom/glympse/android/hal/GKeychain;

    iget-object v1, p0, Lcom/glympse/android/lib/x;->iF:Ljava/lang/String;

    iget-object v2, p0, Lcom/glympse/android/lib/x;->iI:Lcom/glympse/android/core/GPrimitive;

    invoke-static {v2}, Lcom/glympse/android/lib/json/JsonSerializer;->toString(Lcom/glympse/android/core/GPrimitive;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v0, v1, v2, v3}, Lcom/glympse/android/hal/GKeychain;->save(Ljava/lang/String;Ljava/lang/String;Z)Z

    .line 611
    return-void
.end method

.method private aO()V
    .locals 4

    .prologue
    .line 616
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iE:Lcom/glympse/android/hal/GKeychain;

    iget-object v1, p0, Lcom/glympse/android/lib/x;->iG:Ljava/lang/String;

    iget-object v2, p0, Lcom/glympse/android/lib/x;->iJ:Lcom/glympse/android/core/GPrimitive;

    invoke-static {v2}, Lcom/glympse/android/lib/json/JsonSerializer;->toString(Lcom/glympse/android/core/GPrimitive;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/glympse/android/hal/GKeychain;->save(Ljava/lang/String;Ljava/lang/String;Z)Z

    .line 617
    return-void
.end method

.method private aP()V
    .locals 4

    .prologue
    .line 622
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iE:Lcom/glympse/android/hal/GKeychain;

    iget-object v1, p0, Lcom/glympse/android/lib/x;->iH:Ljava/lang/String;

    iget-object v2, p0, Lcom/glympse/android/lib/x;->iK:Lcom/glympse/android/core/GPrimitive;

    invoke-static {v2}, Lcom/glympse/android/lib/json/JsonSerializer;->toString(Lcom/glympse/android/core/GPrimitive;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v0, v1, v2, v3}, Lcom/glympse/android/hal/GKeychain;->save(Ljava/lang/String;Ljava/lang/String;Z)Z

    .line 623
    return-void
.end method

.method private aQ()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1051
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iu:Lcom/glympse/android/hal/GContextHolder;

    invoke-interface {v0}, Lcom/glympse/android/hal/GContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/glympse/android/hal/Platform;->getAppVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->filenameEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private d(Ljava/lang/String;Z)Z
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v0, p1}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v0, p1}, Lcom/glympse/android/core/GPrimitive;->getBool(Ljava/lang/String;)Z

    move-result p2

    .line 318
    :goto_0
    return p2

    .line 317
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v0, p1, p2}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method private static i(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 1076
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1078
    const-string v0, "info"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1080
    const-wide/16 v0, 0x1

    .line 1103
    :goto_0
    return-wide v0

    .line 1082
    :cond_0
    const-string v0, "dump"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1084
    const-wide/16 v0, 0x2

    goto :goto_0

    .line 1086
    :cond_1
    const-string v0, "notice"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1088
    const-wide/16 v0, 0x3

    goto :goto_0

    .line 1090
    :cond_2
    const-string v0, "warning"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1092
    const-wide/16 v0, 0x4

    goto :goto_0

    .line 1094
    :cond_3
    const-string v0, "error"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1096
    const-wide/16 v0, 0x5

    goto :goto_0

    .line 1098
    :cond_4
    const-string v0, "critical"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1100
    const-wide/16 v0, 0x6

    goto :goto_0

    .line 1103
    :cond_5
    const-wide/16 v0, 0x7

    goto :goto_0
.end method


# virtual methods
.method public addListener(Lcom/glympse/android/api/GEventListener;)Z
    .locals 1

    .prologue
    .line 1267
    iget-object v0, p0, Lcom/glympse/android/lib/x;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1}, Lcom/glympse/android/lib/CommonSink;->addListener(Lcom/glympse/android/api/GEventListener;)Z

    move-result v0

    return v0
.end method

.method public allowLocationSharing(Z)V
    .locals 4

    .prologue
    .line 367
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    iget-object v1, p0, Lcom/glympse/android/lib/x;->iO:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Z)V

    .line 368
    iget-object v0, p0, Lcom/glympse/android/lib/x;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_0

    .line 370
    iget-object v0, p0, Lcom/glympse/android/lib/x;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/16 v1, 0xb

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/glympse/android/lib/x;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 372
    :cond_0
    return-void
.end method

.method public allowSpeedSharing(Z)V
    .locals 2

    .prologue
    .line 381
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    iget-object v1, p0, Lcom/glympse/android/lib/x;->iP:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Z)V

    .line 382
    return-void
.end method

.method public areAccountsLinked()Z
    .locals 1

    .prologue
    .line 691
    iget-boolean v0, p0, Lcom/glympse/android/lib/x;->iB:Z

    return v0
.end method

.method public associateContext(JLjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1292
    iget-object v0, p0, Lcom/glympse/android/lib/x;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1, p2, p3}, Lcom/glympse/android/lib/CommonSink;->associateContext(JLjava/lang/Object;)V

    .line 1293
    return-void
.end method

.method public clearContext(J)V
    .locals 1

    .prologue
    .line 1302
    iget-object v0, p0, Lcom/glympse/android/lib/x;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1, p2}, Lcom/glympse/android/lib/CommonSink;->clearContext(J)V

    .line 1303
    return-void
.end method

.method public deriveContext(Lcom/glympse/android/api/GEventSink;)V
    .locals 1

    .prologue
    .line 1312
    iget-object v0, p0, Lcom/glympse/android/lib/x;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1}, Lcom/glympse/android/lib/CommonSink;->deriveContext(Lcom/glympse/android/api/GEventSink;)V

    .line 1313
    return-void
.end method

.method public disableInstance()V
    .locals 4

    .prologue
    .line 723
    new-instance v0, Lcom/glympse/android/lib/Primitive;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 724
    const-string v1, "ts"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/glympse/android/lib/x;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v2}, Lcom/glympse/android/lib/GGlympsePrivate;->getTime()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 725
    const-string v1, "av"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/glympse/android/lib/x;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v2}, Lcom/glympse/android/lib/GGlympsePrivate;->getContextHolder()Lcom/glympse/android/hal/GContextHolder;

    move-result-object v2

    invoke-interface {v2}, Lcom/glympse/android/hal/GContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/glympse/android/hal/Platform;->getAppVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 726
    const-string v1, "pv"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/glympse/android/lib/x;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v2}, Lcom/glympse/android/lib/GGlympsePrivate;->getApiVersion()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 727
    iget-object v1, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v2, "g.instDsbld"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 730
    invoke-direct {p0}, Lcom/glympse/android/lib/x;->aM()V

    .line 731
    return-void
.end method

.method public eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V
    .locals 6

    .prologue
    .line 1282
    iget-object v0, p0, Lcom/glympse/android/lib/x;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/glympse/android/api/GEventSink;

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/glympse/android/lib/CommonSink;->eventsOccurred(Lcom/glympse/android/api/GEventSink;Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 1283
    return-void
.end method

.method public forgetAccessToken()V
    .locals 2

    .prologue
    .line 914
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iK:Lcom/glympse/android/core/GPrimitive;

    iget-object v1, p0, Lcom/glympse/android/lib/x;->iw:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->remove(Ljava/lang/String;)V

    .line 916
    invoke-direct {p0}, Lcom/glympse/android/lib/x;->aP()V

    .line 917
    return-void
.end method

.method public getAccessToken()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 893
    iget-object v1, p0, Lcom/glympse/android/lib/x;->iK:Lcom/glympse/android/core/GPrimitive;

    iget-object v2, p0, Lcom/glympse/android/lib/x;->iw:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v1

    .line 894
    if-nez v1, :cond_0

    .line 908
    :goto_0
    return-object v0

    .line 900
    :cond_0
    const-string v2, "valid"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 901
    iget-object v4, p0, Lcom/glympse/android/lib/x;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v4}, Lcom/glympse/android/lib/GGlympsePrivate;->getTime()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    .line 903
    invoke-virtual {p0}, Lcom/glympse/android/lib/x;->forgetAccessToken()V

    goto :goto_0

    .line 908
    :cond_1
    const-string v0, "token"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getAccount(Lcom/glympse/android/hal/GContextHolder;Ljava/lang/String;Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 840
    iput-object p1, p0, Lcom/glympse/android/lib/x;->iu:Lcom/glympse/android/hal/GContextHolder;

    .line 841
    iget-object v1, p0, Lcom/glympse/android/lib/x;->KEY_ACCOUNTS:Ljava/lang/String;

    invoke-static {p2, v0, v1}, Lcom/glympse/android/lib/fb;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/glympse/android/lib/x;->iG:Ljava/lang/String;

    .line 844
    iget-object v1, p0, Lcom/glympse/android/lib/x;->iu:Lcom/glympse/android/hal/GContextHolder;

    invoke-interface {v1}, Lcom/glympse/android/hal/GContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p2}, Lcom/glympse/android/hal/HalFactory;->openKeychain(Landroid/content/Context;Ljava/lang/String;)Lcom/glympse/android/hal/GKeychain;

    move-result-object v1

    .line 845
    iget-object v2, p0, Lcom/glympse/android/lib/x;->iG:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/glympse/android/hal/GKeychain;->load(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 846
    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 859
    :cond_0
    :goto_0
    return-object v0

    .line 852
    :cond_1
    invoke-static {v1}, Lcom/glympse/android/lib/json/JsonSerializer;->toPrimitive(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v1

    iput-object v1, p0, Lcom/glympse/android/lib/x;->iJ:Lcom/glympse/android/core/GPrimitive;

    .line 853
    iget-object v1, p0, Lcom/glympse/android/lib/x;->iJ:Lcom/glympse/android/core/GPrimitive;

    if-eqz v1, :cond_0

    .line 859
    invoke-virtual {p0, p3}, Lcom/glympse/android/lib/x;->getAccount(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    goto :goto_0
.end method

.method public getAccount(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;
    .locals 1

    .prologue
    .line 780
    invoke-static {p1}, Lcom/glympse/android/lib/UrlParser;->cleanupBaseUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 781
    if-nez v0, :cond_0

    .line 783
    const/4 v0, 0x0

    .line 788
    :goto_0
    return-object v0

    .line 787
    :cond_0
    invoke-static {v0}, Lcom/glympse/android/lib/UrlParser;->prepareBaseUrlConfig(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 788
    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/x;->h(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    goto :goto_0
.end method

.method public getAccountCreationTime()J
    .locals 2

    .prologue
    .line 438
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.accCrtd"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getAccounts()Lcom/glympse/android/core/GPrimitive;
    .locals 1

    .prologue
    .line 864
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iJ:Lcom/glympse/android/core/GPrimitive;

    return-object v0
.end method

.method public getContents()Lcom/glympse/android/core/GPrimitive;
    .locals 1

    .prologue
    .line 448
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    return-object v0
.end method

.method public getContext(J)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1297
    iget-object v0, p0, Lcom/glympse/android/lib/x;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1, p2}, Lcom/glympse/android/lib/CommonSink;->getContext(J)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getContextKeys()Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Enumeration",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1307
    iget-object v0, p0, Lcom/glympse/android/lib/x;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0}, Lcom/glympse/android/lib/CommonSink;->getContextKeys()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentAccount()Lcom/glympse/android/core/GPrimitive;
    .locals 1

    .prologue
    .line 798
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iw:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/x;->h(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    return-object v0
.end method

.method public getDebugLevel()J
    .locals 2

    .prologue
    .line 1108
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.dbgLog"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getDeviceId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 458
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iI:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "id"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDirectionsProvider()Lcom/glympse/android/core/GPrimitive;
    .locals 2

    .prologue
    .line 510
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.dirProv"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    return-object v0
.end method

.method public getExpireOnArrival()I
    .locals 2

    .prologue
    .line 362
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.expOnAr"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public getFileLevel()J
    .locals 2

    .prologue
    .line 1119
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.fileLog"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getGetRate()J
    .locals 2

    .prologue
    .line 1150
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.getRt"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getInviteLifetime()J
    .locals 2

    .prologue
    .line 418
    const-wide/32 v0, 0xa4cb800

    return-wide v0
.end method

.method public getLabel()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1061
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.label"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getListeners()Lcom/glympse/android/core/GArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/glympse/android/core/GArray",
            "<",
            "Lcom/glympse/android/api/GEventListener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1277
    iget-object v0, p0, Lcom/glympse/android/lib/x;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0}, Lcom/glympse/android/lib/CommonSink;->getListeners()Lcom/glympse/android/core/GArray;

    move-result-object v0

    return-object v0
.end method

.method public getLogUploadFrequency()J
    .locals 2

    .prologue
    .line 1145
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.logUpFrq"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getLogUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1135
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.logUrl"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMaximumNicknameLength()I
    .locals 2

    .prologue
    .line 433
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.mxNmLen"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public getMaximumTicketDuration()I
    .locals 2

    .prologue
    .line 423
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.mxTktLen"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public getPostRatePeriod()I
    .locals 2

    .prologue
    .line 428
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.ptRtPrd"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public getRegistrationToken()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 1018
    iget-object v1, p0, Lcom/glympse/android/lib/x;->iI:Lcom/glympse/android/core/GPrimitive;

    const-string v2, "push"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v1

    .line 1019
    iget-object v2, p0, Lcom/glympse/android/lib/x;->iw:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v2

    .line 1020
    if-nez v2, :cond_0

    .line 1046
    :goto_0
    return-object v0

    .line 1026
    :cond_0
    invoke-direct {p0}, Lcom/glympse/android/lib/x;->aQ()Ljava/lang/String;

    move-result-object v3

    .line 1027
    const-string v4, "ver"

    invoke-static {v4}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1028
    invoke-static {v3, v4}, Lcom/glympse/android/hal/Helpers;->safeEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1031
    iget-object v2, p0, Lcom/glympse/android/lib/x;->iw:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/glympse/android/core/GPrimitive;->remove(Ljava/lang/String;)V

    .line 1032
    invoke-direct {p0}, Lcom/glympse/android/lib/x;->aN()V

    goto :goto_0

    .line 1037
    :cond_1
    const-string v3, "ts"

    invoke-static {v3}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 1038
    const-wide/32 v6, 0x240c8400

    add-long/2addr v4, v6

    invoke-static {}, Lcom/glympse/android/hal/Concurrent;->getTime()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-gez v3, :cond_2

    .line 1041
    iget-object v2, p0, Lcom/glympse/android/lib/x;->iw:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/glympse/android/core/GPrimitive;->remove(Ljava/lang/String;)V

    .line 1042
    invoke-direct {p0}, Lcom/glympse/android/lib/x;->aN()V

    goto :goto_0

    .line 1046
    :cond_2
    const-string v0, "id"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getSupportedServersAndSchemes()Lcom/glympse/android/core/GArray;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/glympse/android/core/GArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 935
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iL:Lcom/glympse/android/hal/GVector;

    if-nez v0, :cond_1

    .line 937
    new-instance v1, Lcom/glympse/android/hal/GVector;

    invoke-direct {v1}, Lcom/glympse/android/hal/GVector;-><init>()V

    .line 938
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v2, "g.iurls"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 939
    if-nez v0, :cond_2

    const/4 v0, 0x0

    .line 940
    :goto_0
    if-nez v0, :cond_3

    .line 942
    const-string v0, "glympse.com/"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 943
    const-string v0, "glympse.me/"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 960
    :cond_0
    const-string v0, "glympse:"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 961
    const-string v0, "glympse2:"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 962
    const-string v0, "glympse3:"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 963
    const-string v0, "glympse4:"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 965
    iput-object v1, p0, Lcom/glympse/android/lib/x;->iL:Lcom/glympse/android/hal/GVector;

    .line 967
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iL:Lcom/glympse/android/hal/GVector;

    return-object v0

    .line 939
    :cond_2
    iget-object v2, p0, Lcom/glympse/android/lib/x;->iw:Ljava/lang/String;

    invoke-interface {v0, v2}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    goto :goto_0

    .line 948
    :cond_3
    const-string v2, "/"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 949
    invoke-interface {v0}, Lcom/glympse/android/core/GPrimitive;->getKeys()Ljava/util/Enumeration;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 951
    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 952
    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 954
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 956
    :cond_4
    invoke-virtual {v1, v0}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public getTrackTrimLength()J
    .locals 2

    .prologue
    .line 411
    invoke-virtual {p0}, Lcom/glympse/android/lib/x;->isTrackTrimmingEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/32 v0, 0x927c0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/32 v0, 0x240c8400

    goto :goto_0
.end method

.method public getViewerToken()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1071
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.viewer"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getXoaProfile()Lcom/glympse/android/core/GPrimitive;
    .locals 2

    .prologue
    .line 1204
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.advXoaPrf"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    return-object v0
.end method

.method public h(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;
    .locals 1

    .prologue
    .line 830
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iJ:Lcom/glympse/android/core/GPrimitive;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iJ:Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v0, p1}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    goto :goto_0
.end method

.method public hasContext(J)Z
    .locals 1

    .prologue
    .line 1287
    iget-object v0, p0, Lcom/glympse/android/lib/x;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1, p2}, Lcom/glympse/android/lib/CommonSink;->hasContext(J)Z

    move-result v0

    return v0
.end method

.method public hasPrivateGroups()Z
    .locals 1

    .prologue
    .line 716
    iget-boolean v0, p0, Lcom/glympse/android/lib/x;->iC:Z

    return v0
.end method

.method public hasTicketBeenSent()Z
    .locals 1

    .prologue
    .line 665
    iget-boolean v0, p0, Lcom/glympse/android/lib/x;->iA:Z

    return v0
.end method

.method public isAccuracyPluggedHigh()Z
    .locals 2

    .prologue
    .line 1228
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.accPlgHg"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getBool(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isAccuracyStationaryLow()Z
    .locals 2

    .prologue
    .line 1248
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.accStLw"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getBool(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isDebug()Z
    .locals 2

    .prologue
    .line 453
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.debug"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getBool(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isFirstLaunch()Z
    .locals 1

    .prologue
    .line 639
    iget-boolean v0, p0, Lcom/glympse/android/lib/x;->iz:Z

    return v0
.end method

.method public isInstanceDisabled()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 739
    iget-object v1, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v2, "g.instDsbld"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v1

    .line 740
    if-nez v1, :cond_0

    .line 760
    :goto_0
    return v0

    .line 747
    :cond_0
    const-string v2, "av"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 748
    iget-object v2, p0, Lcom/glympse/android/lib/x;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v2}, Lcom/glympse/android/lib/GGlympsePrivate;->getContextHolder()Lcom/glympse/android/hal/GContextHolder;

    move-result-object v2

    invoke-interface {v2}, Lcom/glympse/android/hal/GContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/glympse/android/hal/Platform;->getAppVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 749
    invoke-static {v1, v2}, Lcom/glympse/android/hal/Helpers;->safeEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 752
    iget-object v1, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v2, "g.instDsbld"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/glympse/android/core/GPrimitive;->remove(Ljava/lang/String;)V

    .line 753
    invoke-direct {p0}, Lcom/glympse/android/lib/x;->aM()V

    goto :goto_0

    .line 760
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isInvitePollPushEnabled()Z
    .locals 2

    .prologue
    .line 1258
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.invPolPsh"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getBool(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isPostRatePluggedHigh()Z
    .locals 2

    .prologue
    .line 1218
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.ptRtPlgHg"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getBool(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isPostRateStationaryLow()Z
    .locals 2

    .prologue
    .line 1238
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.ptRtStLw"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getBool(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isPublicGroupAutoWatched()Z
    .locals 2

    .prologue
    .line 396
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.pGrAWa"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getBool(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isServerSupported(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 976
    invoke-static {p1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/glympse/android/lib/x;->iw:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 992
    :cond_0
    :goto_0
    return v0

    .line 980
    :cond_1
    iget-object v1, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v2, "g.iurls"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v1

    .line 981
    if-eqz v1, :cond_0

    .line 985
    iget-object v2, p0, Lcom/glympse/android/lib/x;->iw:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v1

    .line 986
    if-eqz v1, :cond_0

    invoke-interface {v1}, Lcom/glympse/android/core/GPrimitive;->size()I

    move-result v2

    if-eqz v2, :cond_0

    .line 992
    invoke-interface {v1, p1}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public isSharingLocation()Z
    .locals 2

    .prologue
    .line 376
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    iget-object v1, p0, Lcom/glympse/android/lib/x;->iO:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getBool(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isSharingSpeed()Z
    .locals 2

    .prologue
    .line 386
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    iget-object v1, p0, Lcom/glympse/android/lib/x;->iP:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getBool(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isTrackTrimmingEnabled()Z
    .locals 2

    .prologue
    .line 406
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.tkTrmEn"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getBool(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public load(Lcom/glympse/android/hal/GContextHolder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 545
    iput-object p1, p0, Lcom/glympse/android/lib/x;->iu:Lcom/glympse/android/hal/GContextHolder;

    .line 546
    iput-object p2, p0, Lcom/glympse/android/lib/x;->iv:Ljava/lang/String;

    .line 547
    invoke-static {p3}, Lcom/glympse/android/lib/UrlParser;->prepareBaseUrlConfig(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/x;->iw:Ljava/lang/String;

    .line 548
    iput-object p4, p0, Lcom/glympse/android/lib/x;->fW:Ljava/lang/String;

    .line 549
    invoke-static {p1}, Lcom/glympse/android/lib/x;->a(Lcom/glympse/android/hal/GContextHolder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/x;->ix:Ljava/lang/String;

    .line 552
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iy:Lcom/glympse/android/lib/fb;

    iget-object v4, p0, Lcom/glympse/android/lib/x;->iu:Lcom/glympse/android/hal/GContextHolder;

    iget-object v5, p0, Lcom/glympse/android/lib/x;->iv:Ljava/lang/String;

    const-string v6, "config_v2"

    invoke-static {v6}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v4, v5, v3, v6}, Lcom/glympse/android/lib/fb;->a(Lcom/glympse/android/hal/GContextHolder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 553
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iy:Lcom/glympse/android/lib/fb;

    invoke-virtual {v0}, Lcom/glympse/android/lib/fb;->cP()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/glympse/android/lib/x;->iz:Z

    .line 554
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iy:Lcom/glympse/android/lib/fb;

    invoke-virtual {v0}, Lcom/glympse/android/lib/fb;->load()Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    .line 557
    iget-object v0, p0, Lcom/glympse/android/lib/x;->ix:Ljava/lang/String;

    iget-object v4, p0, Lcom/glympse/android/lib/x;->iM:Ljava/lang/String;

    invoke-static {p2, v0, v4}, Lcom/glympse/android/lib/fb;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/x;->iF:Ljava/lang/String;

    .line 558
    iget-object v0, p0, Lcom/glympse/android/lib/x;->KEY_ACCOUNTS:Ljava/lang/String;

    invoke-static {p2, v3, v0}, Lcom/glympse/android/lib/fb;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/x;->iG:Ljava/lang/String;

    .line 559
    iget-object v0, p0, Lcom/glympse/android/lib/x;->ix:Ljava/lang/String;

    iget-object v4, p0, Lcom/glympse/android/lib/x;->iN:Ljava/lang/String;

    invoke-static {p2, v0, v4}, Lcom/glympse/android/lib/fb;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/x;->iH:Ljava/lang/String;

    .line 562
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iu:Lcom/glympse/android/hal/GContextHolder;

    invoke-interface {v0}, Lcom/glympse/android/hal/GContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/glympse/android/hal/HalFactory;->openKeychain(Landroid/content/Context;Ljava/lang/String;)Lcom/glympse/android/hal/GKeychain;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/x;->iE:Lcom/glympse/android/hal/GKeychain;

    .line 567
    iget-boolean v0, p0, Lcom/glympse/android/lib/x;->iz:Z

    if-nez v0, :cond_4

    .line 570
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iE:Lcom/glympse/android/hal/GKeychain;

    iget-object v4, p0, Lcom/glympse/android/lib/x;->iF:Ljava/lang/String;

    invoke-interface {v0, v4, v1}, Lcom/glympse/android/hal/GKeychain;->load(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 571
    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    move-object v0, v3

    :goto_1
    iput-object v0, p0, Lcom/glympse/android/lib/x;->iI:Lcom/glympse/android/core/GPrimitive;

    .line 572
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iE:Lcom/glympse/android/hal/GKeychain;

    iget-object v4, p0, Lcom/glympse/android/lib/x;->iG:Ljava/lang/String;

    invoke-interface {v0, v4, v2}, Lcom/glympse/android/hal/GKeychain;->load(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 573
    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    move-object v0, v3

    :goto_2
    iput-object v0, p0, Lcom/glympse/android/lib/x;->iJ:Lcom/glympse/android/core/GPrimitive;

    .line 574
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iE:Lcom/glympse/android/hal/GKeychain;

    iget-object v2, p0, Lcom/glympse/android/lib/x;->iH:Ljava/lang/String;

    invoke-interface {v0, v2, v1}, Lcom/glympse/android/hal/GKeychain;->load(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 575
    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    :goto_3
    iput-object v3, p0, Lcom/glympse/android/lib/x;->iK:Lcom/glympse/android/core/GPrimitive;

    .line 584
    :goto_4
    invoke-direct {p0}, Lcom/glympse/android/lib/x;->aK()V

    .line 586
    invoke-direct {p0}, Lcom/glympse/android/lib/x;->aL()V

    .line 587
    return-void

    :cond_0
    move v0, v2

    .line 553
    goto :goto_0

    .line 571
    :cond_1
    invoke-static {v0}, Lcom/glympse/android/lib/json/JsonSerializer;->toPrimitive(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    goto :goto_1

    .line 573
    :cond_2
    invoke-static {v0}, Lcom/glympse/android/lib/json/JsonSerializer;->toPrimitive(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    goto :goto_2

    .line 575
    :cond_3
    invoke-static {v0}, Lcom/glympse/android/lib/json/JsonSerializer;->toPrimitive(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v3

    goto :goto_3

    .line 580
    :cond_4
    invoke-virtual {p0}, Lcom/glympse/android/lib/x;->wipeAccounts()V

    goto :goto_4
.end method

.method public removeListener(Lcom/glympse/android/api/GEventListener;)Z
    .locals 1

    .prologue
    .line 1272
    iget-object v0, p0, Lcom/glympse/android/lib/x;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1}, Lcom/glympse/android/lib/CommonSink;->removeListener(Lcom/glympse/android/api/GEventListener;)Z

    move-result v0

    return v0
.end method

.method public save()V
    .locals 0

    .prologue
    .line 592
    invoke-direct {p0}, Lcom/glympse/android/lib/x;->aM()V

    .line 594
    invoke-direct {p0}, Lcom/glympse/android/lib/x;->aN()V

    .line 596
    invoke-direct {p0}, Lcom/glympse/android/lib/x;->aO()V

    .line 598
    invoke-direct {p0}, Lcom/glympse/android/lib/x;->aP()V

    .line 599
    return-void
.end method

.method public saveAccessToken(Ljava/lang/String;J)V
    .locals 4

    .prologue
    .line 882
    new-instance v0, Lcom/glympse/android/lib/Primitive;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 883
    const-string v1, "token"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 884
    const-string v1, "valid"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2, p3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 885
    iget-object v1, p0, Lcom/glympse/android/lib/x;->iK:Lcom/glympse/android/core/GPrimitive;

    iget-object v2, p0, Lcom/glympse/android/lib/x;->iw:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 887
    invoke-direct {p0}, Lcom/glympse/android/lib/x;->aP()V

    .line 888
    return-void
.end method

.method public saveAccount(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 766
    invoke-static {p1}, Lcom/glympse/android/lib/UrlParser;->cleanupBaseUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 767
    if-nez v0, :cond_0

    .line 775
    :goto_0
    return-void

    .line 773
    :cond_0
    invoke-static {v0}, Lcom/glympse/android/lib/UrlParser;->prepareBaseUrlConfig(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 774
    invoke-direct {p0, v0, p2, p3, p4}, Lcom/glympse/android/lib/x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public saveCurrentAccount(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 793
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iw:Ljava/lang/String;

    iget-object v1, p0, Lcom/glympse/android/lib/x;->fW:Ljava/lang/String;

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/glympse/android/lib/x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 794
    return-void
.end method

.method public saveCurrentDeviceId(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 804
    invoke-static {p1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/x;->iI:Lcom/glympse/android/core/GPrimitive;

    if-eqz v0, :cond_0

    .line 814
    :goto_0
    return-void

    .line 810
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iI:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "id"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 813
    invoke-direct {p0}, Lcom/glympse/android/lib/x;->aN()V

    goto :goto_0
.end method

.method public saveRegistrationToken(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 997
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iI:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "push"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 998
    if-nez v0, :cond_0

    .line 1013
    :goto_0
    return-void

    .line 1005
    :cond_0
    new-instance v1, Lcom/glympse/android/lib/Primitive;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 1006
    const-string v2, "ts"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/glympse/android/hal/Concurrent;->getTime()J

    move-result-wide v4

    invoke-interface {v1, v2, v4, v5}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 1007
    const-string v2, "id"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, p1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1008
    const-string v2, "ver"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lcom/glympse/android/lib/x;->aQ()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1009
    iget-object v2, p0, Lcom/glympse/android/lib/x;->iw:Ljava/lang/String;

    invoke-interface {v0, v2, v1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 1012
    invoke-direct {p0}, Lcom/glympse/android/lib/x;->aN()V

    goto :goto_0
.end method

.method public setAccountCreationTime(J)V
    .locals 3

    .prologue
    .line 696
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.accCrtd"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1, p2}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 697
    return-void
.end method

.method public setAccountsLinked(Z)V
    .locals 4

    .prologue
    .line 670
    iget-boolean v0, p0, Lcom/glympse/android/lib/x;->iB:Z

    if-ne p1, v0, :cond_1

    .line 687
    :cond_0
    :goto_0
    return-void

    .line 676
    :cond_1
    iput-boolean p1, p0, Lcom/glympse/android/lib/x;->iB:Z

    .line 677
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.accLnkd"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-boolean v2, p0, Lcom/glympse/android/lib/x;->iB:Z

    invoke-interface {v0, v1, v2}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Z)V

    .line 680
    invoke-direct {p0}, Lcom/glympse/android/lib/x;->aM()V

    .line 683
    iget-object v0, p0, Lcom/glympse/android/lib/x;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_0

    .line 685
    iget-object v0, p0, Lcom/glympse/android/lib/x;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/16 v1, 0xb

    const/16 v2, 0x8

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/glympse/android/lib/x;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    goto :goto_0
.end method

.method public setAccuracyPluggedHigh(Z)V
    .locals 2

    .prologue
    .line 1223
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.accPlgHg"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Z)V

    .line 1224
    return-void
.end method

.method public setAccuracyStationaryLow(Z)V
    .locals 2

    .prologue
    .line 1243
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.accStLw"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Z)V

    .line 1244
    return-void
.end method

.method public setAutoWatchPublicGroup(Z)V
    .locals 2

    .prologue
    .line 391
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.pGrAWa"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Z)V

    .line 392
    return-void
.end method

.method public setDebug(Z)V
    .locals 2

    .prologue
    .line 443
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.debug"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Z)V

    .line 444
    return-void
.end method

.method public setDebugLevel(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1113
    invoke-static {p1}, Lcom/glympse/android/lib/x;->i(Ljava/lang/String;)J

    move-result-wide v0

    .line 1114
    iget-object v2, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v3, "g.dbgLog"

    invoke-static {v3}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0, v1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 1115
    return-void
.end method

.method public setDirectionsProvider(Lcom/glympse/android/core/GPrimitive;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 464
    if-nez p1, :cond_1

    .line 505
    :cond_0
    :goto_0
    return v0

    .line 468
    :cond_1
    const-string v1, "src"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    long-to-int v1, v2

    .line 469
    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 500
    :cond_2
    :pswitch_1
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.dirProv"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 503
    invoke-direct {p0}, Lcom/glympse/android/lib/x;->aM()V

    .line 505
    const/4 v0, 0x1

    goto :goto_0

    .line 477
    :pswitch_2
    const-string v1, "data"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v1

    .line 478
    if-eqz v1, :cond_0

    .line 482
    const-string v2, "app_id"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "app_code"

    .line 483
    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 469
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public setExpireOnArrival(I)V
    .locals 4

    .prologue
    .line 346
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.expOnAr"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    long-to-int v0, v0

    .line 347
    if-eq v0, p1, :cond_0

    .line 350
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.expOnAr"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    int-to-long v2, p1

    invoke-interface {v0, v1, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 353
    iget-object v0, p0, Lcom/glympse/android/lib/x;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_0

    .line 355
    iget-object v0, p0, Lcom/glympse/android/lib/x;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/16 v1, 0xb

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/glympse/android/lib/x;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 358
    :cond_0
    return-void
.end method

.method public setFileLevel(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1124
    invoke-static {p1}, Lcom/glympse/android/lib/x;->i(Ljava/lang/String;)J

    move-result-wide v0

    .line 1125
    iget-object v2, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v3, "g.fileLog"

    invoke-static {v3}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0, v1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 1126
    return-void
.end method

.method public setForceRefresh(Z)V
    .locals 2

    .prologue
    .line 1170
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.frcRfr"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Z)V

    .line 1171
    return-void
.end method

.method public setInvitePollPushEnabled(Z)V
    .locals 2

    .prologue
    .line 1253
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.invPolPsh"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Z)V

    .line 1254
    return-void
.end method

.method public setLabel(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1056
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.label"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1057
    return-void
.end method

.method public setLogUploadFrequency(J)V
    .locals 3

    .prologue
    .line 1140
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.logUpFrq"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1, p2}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 1141
    return-void
.end method

.method public setLogUrl(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1130
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.logUrl"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1131
    return-void
.end method

.method public setMaximumNicknameLength(I)V
    .locals 4

    .prologue
    .line 1165
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.mxNmLen"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    int-to-long v2, p1

    invoke-interface {v0, v1, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 1166
    return-void
.end method

.method public setMaximumTicketDuration(I)V
    .locals 4

    .prologue
    .line 1155
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.mxTktLen"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    int-to-long v2, p1

    invoke-interface {v0, v1, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 1156
    return-void
.end method

.method public setPostRatePeriod(I)V
    .locals 4

    .prologue
    .line 1160
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.ptRtPrd"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    int-to-long v2, p1

    invoke-interface {v0, v1, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 1161
    return-void
.end method

.method public setPostRatePluggedHigh(Z)V
    .locals 2

    .prologue
    .line 1213
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.ptRtPlgHg"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Z)V

    .line 1214
    return-void
.end method

.method public setPostRateStationaryLow(Z)V
    .locals 2

    .prologue
    .line 1233
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.ptRtStLw"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Z)V

    .line 1234
    return-void
.end method

.method public setPrivateGroups(Z)V
    .locals 3

    .prologue
    .line 701
    iget-boolean v0, p0, Lcom/glympse/android/lib/x;->iC:Z

    if-ne p1, v0, :cond_0

    .line 712
    :goto_0
    return-void

    .line 707
    :cond_0
    iput-boolean p1, p0, Lcom/glympse/android/lib/x;->iC:Z

    .line 708
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.prGr"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-boolean v2, p0, Lcom/glympse/android/lib/x;->iC:Z

    invoke-interface {v0, v1, v2}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Z)V

    .line 711
    invoke-direct {p0}, Lcom/glympse/android/lib/x;->aM()V

    goto :goto_0
.end method

.method public setSupportedServers(Lcom/glympse/android/core/GPrimitive;)V
    .locals 3

    .prologue
    .line 921
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.iurls"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 922
    if-nez v0, :cond_0

    .line 924
    new-instance v0, Lcom/glympse/android/lib/Primitive;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 925
    iget-object v1, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v2, "g.iurls"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 927
    :cond_0
    iget-object v1, p0, Lcom/glympse/android/lib/x;->iw:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 930
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/x;->iL:Lcom/glympse/android/hal/GVector;

    .line 931
    return-void
.end method

.method public setTicketSent(Z)V
    .locals 4

    .prologue
    .line 644
    iget-boolean v0, p0, Lcom/glympse/android/lib/x;->iA:Z

    if-ne p1, v0, :cond_1

    .line 661
    :cond_0
    :goto_0
    return-void

    .line 650
    :cond_1
    iput-boolean p1, p0, Lcom/glympse/android/lib/x;->iA:Z

    .line 651
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.tkSt"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-boolean v2, p0, Lcom/glympse/android/lib/x;->iA:Z

    invoke-interface {v0, v1, v2}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Z)V

    .line 654
    invoke-direct {p0}, Lcom/glympse/android/lib/x;->aM()V

    .line 657
    iget-object v0, p0, Lcom/glympse/android/lib/x;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_0

    .line 659
    iget-object v0, p0, Lcom/glympse/android/lib/x;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/16 v1, 0xb

    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/glympse/android/lib/x;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    goto :goto_0
.end method

.method public setTrackTrimmingEnabled(Z)V
    .locals 2

    .prologue
    .line 401
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.tkTrmEn"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Z)V

    .line 402
    return-void
.end method

.method public setViewerToken(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1066
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.viewer"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1067
    return-void
.end method

.method public setXoaProfile(Lcom/glympse/android/core/GPrimitive;)V
    .locals 4

    .prologue
    .line 1189
    if-nez p1, :cond_1

    .line 1200
    :cond_0
    :goto_0
    return-void

    .line 1194
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.advXoaPrf"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 1196
    iget-object v0, p0, Lcom/glympse/android/lib/x;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_0

    .line 1198
    iget-object v0, p0, Lcom/glympse/android/lib/x;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/16 v1, 0xb

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/glympse/android/lib/x;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    goto :goto_0
.end method

.method public shouldForceRefresh()Z
    .locals 2

    .prologue
    .line 1175
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.frcRfr"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getBool(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public start(Lcom/glympse/android/lib/GGlympsePrivate;)V
    .locals 1

    .prologue
    .line 519
    iput-object p1, p0, Lcom/glympse/android/lib/x;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 524
    iget-boolean v0, p0, Lcom/glympse/android/lib/x;->iz:Z

    if-eqz v0, :cond_0

    .line 526
    invoke-virtual {p0}, Lcom/glympse/android/lib/x;->save()V

    .line 528
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 532
    iget-object v0, p0, Lcom/glympse/android/lib/x;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-nez v0, :cond_0

    .line 540
    :goto_0
    return-void

    .line 537
    :cond_0
    invoke-virtual {p0}, Lcom/glympse/android/lib/x;->save()V

    .line 538
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iy:Lcom/glympse/android/lib/fb;

    invoke-virtual {v0}, Lcom/glympse/android/lib/fb;->stop()V

    .line 539
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/x;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    goto :goto_0
.end method

.method public useGlympseProximity()Z
    .locals 2

    .prologue
    .line 1182
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iD:Lcom/glympse/android/core/GPrimitive;

    const-string v1, "g.frcGlyProx"

    .line 1183
    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getBool(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/x;->iu:Lcom/glympse/android/hal/GContextHolder;

    .line 1184
    invoke-interface {v0}, Lcom/glympse/android/hal/GContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/glympse/android/hal/HalFactory;->isProximityReliable(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 1182
    :goto_0
    return v0

    .line 1184
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public wipeAccounts()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x2

    .line 869
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iE:Lcom/glympse/android/hal/GKeychain;

    iget-object v1, p0, Lcom/glympse/android/lib/x;->iF:Ljava/lang/String;

    invoke-interface {v0, v1, v4}, Lcom/glympse/android/hal/GKeychain;->remove(Ljava/lang/String;Z)Z

    .line 870
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iE:Lcom/glympse/android/hal/GKeychain;

    iget-object v1, p0, Lcom/glympse/android/lib/x;->iG:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/glympse/android/hal/GKeychain;->remove(Ljava/lang/String;Z)Z

    .line 871
    iget-object v0, p0, Lcom/glympse/android/lib/x;->iE:Lcom/glympse/android/hal/GKeychain;

    iget-object v1, p0, Lcom/glympse/android/lib/x;->iH:Ljava/lang/String;

    invoke-interface {v0, v1, v4}, Lcom/glympse/android/hal/GKeychain;->remove(Ljava/lang/String;Z)Z

    .line 874
    new-instance v0, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v0, v3}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    iput-object v0, p0, Lcom/glympse/android/lib/x;->iI:Lcom/glympse/android/core/GPrimitive;

    .line 875
    new-instance v0, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v0, v3}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    iput-object v0, p0, Lcom/glympse/android/lib/x;->iJ:Lcom/glympse/android/core/GPrimitive;

    .line 876
    new-instance v0, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v0, v3}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    iput-object v0, p0, Lcom/glympse/android/lib/x;->iK:Lcom/glympse/android/core/GPrimitive;

    .line 877
    return-void
.end method
