.class public Lcom/glympse/android/lib/dc$a;
.super Lcom/glympse/android/lib/f;
.source "InviteView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/lib/dc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "a"
.end annotation


# instance fields
.field public pI:Lcom/glympse/android/lib/cz;

.field public pJ:Lcom/glympse/android/lib/cw;

.field public pK:Lcom/glympse/android/lib/cy;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 358
    invoke-direct {p0}, Lcom/glympse/android/lib/f;-><init>()V

    return-void
.end method


# virtual methods
.method public endObject(I)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 425
    if-ne v1, p1, :cond_0

    .line 427
    iget-object v0, p0, Lcom/glympse/android/lib/dc$a;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    invoke-interface {v0}, Lcom/glympse/android/lib/json/GJsonHandlerStack;->popHandler()V

    .line 429
    :cond_0
    return v1
.end method

.method public primitive(ILcom/glympse/android/lib/json/GJsonPrimitive;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 366
    packed-switch p1, :pswitch_data_0

    .line 420
    :cond_0
    :goto_0
    return v2

    .line 370
    :pswitch_0
    iget-object v0, p0, Lcom/glympse/android/lib/dc$a;->gE:Ljava/lang/String;

    const-string v1, "result"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 372
    invoke-virtual {p2, v2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/dc$a;->gF:Ljava/lang/String;

    goto :goto_0

    .line 378
    :pswitch_1
    iget-object v0, p0, Lcom/glympse/android/lib/dc$a;->gE:Ljava/lang/String;

    const-string v1, "type"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 380
    invoke-virtual {p2, v2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getString(Z)Ljava/lang/String;

    move-result-object v0

    .line 381
    const-string v1, "ticket_invite"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 384
    new-instance v0, Lcom/glympse/android/lib/cz;

    iget-object v1, p0, Lcom/glympse/android/lib/dc$a;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    invoke-direct {v0, v1, p1}, Lcom/glympse/android/lib/cz;-><init>(Lcom/glympse/android/lib/json/GJsonHandlerStack;I)V

    iput-object v0, p0, Lcom/glympse/android/lib/dc$a;->pI:Lcom/glympse/android/lib/cz;

    .line 385
    iget-object v0, p0, Lcom/glympse/android/lib/dc$a;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    iget-object v1, p0, Lcom/glympse/android/lib/dc$a;->pI:Lcom/glympse/android/lib/cz;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/json/GJsonHandlerStack;->pushHandler(Lcom/glympse/android/lib/json/GJsonHandler;)V

    goto :goto_0

    .line 387
    :cond_1
    const-string v1, "group_invite"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 390
    new-instance v0, Lcom/glympse/android/lib/cw;

    iget-object v1, p0, Lcom/glympse/android/lib/dc$a;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    invoke-direct {v0, v1, p1}, Lcom/glympse/android/lib/cw;-><init>(Lcom/glympse/android/lib/json/GJsonHandlerStack;I)V

    iput-object v0, p0, Lcom/glympse/android/lib/dc$a;->pJ:Lcom/glympse/android/lib/cw;

    .line 391
    iget-object v0, p0, Lcom/glympse/android/lib/dc$a;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    iget-object v1, p0, Lcom/glympse/android/lib/dc$a;->pJ:Lcom/glympse/android/lib/cw;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/json/GJsonHandlerStack;->pushHandler(Lcom/glympse/android/lib/json/GJsonHandler;)V

    goto :goto_0

    .line 393
    :cond_2
    const-string v1, "ticket_request"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 396
    new-instance v0, Lcom/glympse/android/lib/cy;

    iget-object v1, p0, Lcom/glympse/android/lib/dc$a;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    invoke-direct {v0, v1, p1}, Lcom/glympse/android/lib/cy;-><init>(Lcom/glympse/android/lib/json/GJsonHandlerStack;I)V

    iput-object v0, p0, Lcom/glympse/android/lib/dc$a;->pK:Lcom/glympse/android/lib/cy;

    .line 397
    iget-object v0, p0, Lcom/glympse/android/lib/dc$a;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    iget-object v1, p0, Lcom/glympse/android/lib/dc$a;->pK:Lcom/glympse/android/lib/cy;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/json/GJsonHandlerStack;->pushHandler(Lcom/glympse/android/lib/json/GJsonHandler;)V

    goto :goto_0

    .line 401
    :cond_3
    const-string v0, "failure"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/dc$a;->gF:Ljava/lang/String;

    .line 402
    const-string v0, "invite_code"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/dc$a;->gG:Ljava/lang/String;

    goto :goto_0

    .line 405
    :cond_4
    iget-object v0, p0, Lcom/glympse/android/lib/dc$a;->gE:Ljava/lang/String;

    const-string v1, "time"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 407
    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/glympse/android/lib/dc$a;->_time:J

    goto/16 :goto_0

    .line 409
    :cond_5
    iget-object v0, p0, Lcom/glympse/android/lib/dc$a;->gE:Ljava/lang/String;

    const-string v1, "error"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 411
    invoke-virtual {p2, v2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/dc$a;->gG:Ljava/lang/String;

    goto/16 :goto_0

    .line 413
    :cond_6
    iget-object v0, p0, Lcom/glympse/android/lib/dc$a;->gE:Ljava/lang/String;

    const-string v1, "error_detail"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 415
    invoke-virtual {p2, v2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/dc$a;->gH:Ljava/lang/String;

    goto/16 :goto_0

    .line 366
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
