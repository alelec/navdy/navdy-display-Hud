.class public interface abstract Lcom/glympse/android/lib/GCorrectedTime;
.super Ljava/lang/Object;
.source "GCorrectedTime.java"

# interfaces
.implements Lcom/glympse/android/core/GCommon;


# virtual methods
.method public abstract flushBiasSetFlag()V
.end method

.method public abstract getLastServerTime()J
.end method

.method public abstract getLastStateChangeTime()J
.end method

.method public abstract getPlatformStartTime()J
.end method

.method public abstract getStateChangesCount()I
.end method

.method public abstract getTime()J
.end method

.method public abstract getTime(J)J
.end method

.method public abstract isBiasSet()Z
.end method

.method public abstract setServerTime(J)V
.end method

.method public abstract stateChanged()V
.end method
