.class Lcom/glympse/android/lib/fv;
.super Ljava/lang/Object;
.source "Trigger.java"

# interfaces
.implements Lcom/glympse/android/api/GTrigger;


# instance fields
.field private _name:Ljava/lang/String;

.field private _type:I

.field private gM:Ljava/lang/String;

.field private jF:Lcom/glympse/android/api/GTicket;

.field private tW:Lcom/glympse/android/api/GTimeConstraint;

.field private tX:Z


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-static {}, Lcom/glympse/android/hal/Platform;->generateDeviceId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/fv;->gM:Ljava/lang/String;

    .line 33
    iput p1, p0, Lcom/glympse/android/lib/fv;->_type:I

    .line 34
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;ZLcom/glympse/android/api/GTicket;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput p1, p0, Lcom/glympse/android/lib/fv;->_type:I

    .line 39
    invoke-static {}, Lcom/glympse/android/hal/Platform;->generateDeviceId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/fv;->gM:Ljava/lang/String;

    .line 40
    iput-object p2, p0, Lcom/glympse/android/lib/fv;->_name:Ljava/lang/String;

    .line 41
    iput-boolean p3, p0, Lcom/glympse/android/lib/fv;->tX:Z

    .line 42
    return-void
.end method


# virtual methods
.method public autoSend()Z
    .locals 1

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/glympse/android/lib/fv;->tX:Z

    return v0
.end method

.method public decode(Lcom/glympse/android/core/GPrimitive;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 111
    const-string v0, "type"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/glympse/android/lib/fv;->_type:I

    .line 112
    const-string v0, "asnd"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getBool(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/glympse/android/lib/fv;->tX:Z

    .line 113
    const-string v0, "id"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/fv;->gM:Ljava/lang/String;

    .line 114
    const-string v0, "name"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/fv;->_name:Ljava/lang/String;

    .line 115
    const-string v0, "tmc"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 116
    if-eqz v0, :cond_0

    .line 123
    :cond_0
    const-string v0, "tckt"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v1

    .line 124
    if-eqz v1, :cond_1

    .line 126
    const/4 v0, 0x0

    invoke-static {v0, v2, v2}, Lcom/glympse/android/api/GlympseFactory;->createTicket(ILjava/lang/String;Lcom/glympse/android/api/GPlace;)Lcom/glympse/android/api/GTicket;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GTicketPrivate;

    .line 127
    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GTicketPrivate;->decode(Lcom/glympse/android/core/GPrimitive;)V

    .line 128
    iput-object v0, p0, Lcom/glympse/android/lib/fv;->jF:Lcom/glympse/android/api/GTicket;

    .line 130
    :cond_1
    return-void
.end method

.method public encode(Lcom/glympse/android/core/GPrimitive;I)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 84
    const-string v0, "type"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/glympse/android/lib/fv;->_type:I

    int-to-long v2, v1

    invoke-interface {p1, v0, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 85
    const-string v0, "asnd"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, Lcom/glympse/android/lib/fv;->tX:Z

    invoke-interface {p1, v0, v1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Z)V

    .line 86
    iget-object v0, p0, Lcom/glympse/android/lib/fv;->gM:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 88
    const-string v0, "id"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/lib/fv;->gM:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/fv;->_name:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 92
    const-string v0, "name"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/lib/fv;->_name:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/fv;->tW:Lcom/glympse/android/api/GTimeConstraint;

    if-eqz v0, :cond_2

    .line 96
    invoke-static {v4}, Lcom/glympse/android/core/CoreFactory;->createPrimitive(I)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 97
    iget-object v1, p0, Lcom/glympse/android/lib/fv;->tW:Lcom/glympse/android/api/GTimeConstraint;

    invoke-interface {v1, v0, p2}, Lcom/glympse/android/api/GTimeConstraint;->encode(Lcom/glympse/android/core/GPrimitive;I)V

    .line 98
    const-string v1, "tmc"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1, v0}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 100
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/fv;->jF:Lcom/glympse/android/api/GTicket;

    if-eqz v0, :cond_3

    .line 102
    iget-object v0, p0, Lcom/glympse/android/lib/fv;->jF:Lcom/glympse/android/api/GTicket;

    check-cast v0, Lcom/glympse/android/lib/GTicketPrivate;

    .line 103
    invoke-static {v4}, Lcom/glympse/android/core/CoreFactory;->createPrimitive(I)Lcom/glympse/android/core/GPrimitive;

    move-result-object v1

    .line 104
    invoke-interface {v0, v1, p2}, Lcom/glympse/android/lib/GTicketPrivate;->encode(Lcom/glympse/android/core/GPrimitive;I)V

    .line 105
    const-string v0, "tckt"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0, v1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 107
    :cond_3
    return-void
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/glympse/android/lib/fv;->gM:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/glympse/android/lib/fv;->_name:Ljava/lang/String;

    return-object v0
.end method

.method public getTicket()Lcom/glympse/android/api/GTicket;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/glympse/android/lib/fv;->jF:Lcom/glympse/android/api/GTicket;

    return-object v0
.end method

.method public getTimeConstraint()Lcom/glympse/android/api/GTimeConstraint;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/glympse/android/lib/fv;->tW:Lcom/glympse/android/api/GTimeConstraint;

    return-object v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lcom/glympse/android/lib/fv;->_type:I

    return v0
.end method
