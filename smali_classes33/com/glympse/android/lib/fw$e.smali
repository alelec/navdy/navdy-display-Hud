.class Lcom/glympse/android/lib/fw$e;
.super Ljava/lang/Object;
.source "TriggersManager.java"

# interfaces
.implements Lcom/glympse/android/core/GCommon;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/lib/fw;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "e"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/lib/fw$e$a;
    }
.end annotation


# instance fields
.field private _glympse:Lcom/glympse/android/lib/GGlympsePrivate;

.field private _handler:Lcom/glympse/android/core/GHandler;

.field private uf:Lcom/glympse/android/lib/fw;

.field private ui:Lcom/glympse/android/hal/GHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GHashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 759
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/glympse/android/lib/fw$1;)V
    .locals 0

    .prologue
    .line 759
    invoke-direct {p0}, Lcom/glympse/android/lib/fw$e;-><init>()V

    return-void
.end method

.method private B(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 816
    iget-object v0, p0, Lcom/glympse/android/lib/fw$e;->ui:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 817
    iget-object v0, p0, Lcom/glympse/android/lib/fw$e;->uf:Lcom/glympse/android/lib/fw;

    invoke-static {v0, p1}, Lcom/glympse/android/lib/fw;->a(Lcom/glympse/android/lib/fw;Ljava/lang/String;)V

    .line 818
    return-void
.end method

.method static synthetic a(Lcom/glympse/android/lib/fw$e;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 759
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/fw$e;->B(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public E(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 806
    iget-object v0, p0, Lcom/glympse/android/lib/fw$e;->ui:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 807
    if-eqz v0, :cond_0

    .line 809
    iget-object v1, p0, Lcom/glympse/android/lib/fw$e;->_handler:Lcom/glympse/android/core/GHandler;

    invoke-interface {v1, v0}, Lcom/glympse/android/core/GHandler;->cancel(Ljava/lang/Runnable;)V

    .line 810
    iget-object v0, p0, Lcom/glympse/android/lib/fw$e;->ui:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 812
    :cond_0
    return-void
.end method

.method public a(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/fw;)V
    .locals 1

    .prologue
    .line 768
    iput-object p1, p0, Lcom/glympse/android/lib/fw$e;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 769
    iput-object p2, p0, Lcom/glympse/android/lib/fw$e;->uf:Lcom/glympse/android/lib/fw;

    .line 770
    invoke-static {}, Lcom/glympse/android/hal/HalFactory;->createHandler()Lcom/glympse/android/core/GHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/fw$e;->_handler:Lcom/glympse/android/core/GHandler;

    .line 771
    new-instance v0, Lcom/glympse/android/hal/GHashtable;

    invoke-direct {v0}, Lcom/glympse/android/hal/GHashtable;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/fw$e;->ui:Lcom/glympse/android/hal/GHashtable;

    .line 772
    return-void
.end method

.method public d(JLjava/lang/String;)V
    .locals 5

    .prologue
    .line 789
    iget-object v0, p0, Lcom/glympse/android/lib/fw$e;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getTime()J

    move-result-wide v0

    .line 790
    sub-long v2, p1, v0

    .line 792
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-gtz v0, :cond_0

    .line 794
    iget-object v0, p0, Lcom/glympse/android/lib/fw$e;->uf:Lcom/glympse/android/lib/fw;

    invoke-static {v0, p3}, Lcom/glympse/android/lib/fw;->a(Lcom/glympse/android/lib/fw;Ljava/lang/String;)V

    .line 802
    :goto_0
    return-void

    .line 798
    :cond_0
    new-instance v1, Lcom/glympse/android/lib/fw$e$a;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/fw$e;

    invoke-direct {v1, v0, p3}, Lcom/glympse/android/lib/fw$e$a;-><init>(Lcom/glympse/android/lib/fw$e;Ljava/lang/String;)V

    .line 799
    iget-object v0, p0, Lcom/glympse/android/lib/fw$e;->ui:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p3, v1}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 800
    iget-object v0, p0, Lcom/glympse/android/lib/fw$e;->_handler:Lcom/glympse/android/core/GHandler;

    invoke-interface {v0, v1, v2, v3}, Lcom/glympse/android/core/GHandler;->postDelayed(Ljava/lang/Runnable;J)V

    goto :goto_0
.end method

.method public stop()V
    .locals 3

    .prologue
    .line 776
    iget-object v0, p0, Lcom/glympse/android/lib/fw$e;->ui:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GHashtable;->keys()Ljava/util/Enumeration;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 778
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 779
    iget-object v2, p0, Lcom/glympse/android/lib/fw$e;->ui:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v2, v0}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 780
    iget-object v2, p0, Lcom/glympse/android/lib/fw$e;->_handler:Lcom/glympse/android/core/GHandler;

    invoke-interface {v2, v0}, Lcom/glympse/android/core/GHandler;->cancel(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 783
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/fw$e;->ui:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GHashtable;->clear()V

    .line 784
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/fw$e;->ui:Lcom/glympse/android/hal/GHashtable;

    .line 785
    return-void
.end method
