.class Lcom/glympse/android/lib/br;
.super Ljava/lang/Object;
.source "Glympse.java"

# interfaces
.implements Lcom/glympse/android/lib/GGlympsePrivate;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/lib/br$a;
    }
.end annotation


# instance fields
.field private F:Z

.field private _handler:Lcom/glympse/android/core/GHandler;

.field private _jobQueue:Lcom/glympse/android/lib/GJobQueue;

.field private f:Ljava/lang/String;

.field private fW:Ljava/lang/String;

.field private gf:Lcom/glympse/android/lib/GConfigPrivate;

.field private hC:Lcom/glympse/android/lib/GImageCachePrivate;

.field private hS:Lcom/glympse/android/lib/GLocationManagerPrivate;

.field private hT:Lcom/glympse/android/lib/GHistoryManagerPrivate;

.field private he:Lcom/glympse/android/lib/CommonSink;

.field private iC:Z

.field private id:Lcom/glympse/android/lib/GPairingManagerPrivate;

.field private iu:Lcom/glympse/android/hal/GContextHolder;

.field private iv:Ljava/lang/String;

.field private lA:Z

.field private lB:Lcom/glympse/android/core/GPrimitive;

.field private lC:Z

.field private lD:Z

.field private lE:Z

.field private lF:I

.field private lG:Ljava/lang/Runnable;

.field private lH:Lcom/glympse/android/hal/GServiceWrapper;

.field private lI:Lcom/glympse/android/lib/GServerPost;

.field private lJ:Lcom/glympse/android/lib/GTicketProtocol;

.field private lK:Lcom/glympse/android/lib/GUserManagerPrivate;

.field private lL:Lcom/glympse/android/lib/GGroupManagerPrivate;

.field private lM:Lcom/glympse/android/lib/GNetworkManagerPrivate;

.field private lN:Lcom/glympse/android/lib/GBatteryManagerPrivate;

.field private lO:Lcom/glympse/android/lib/GLinkedAccountsManagerPrivate;

.field private lP:Lcom/glympse/android/lib/GApplicationsManagerPrivate;

.field private lQ:Lcom/glympse/android/lib/GHandoffManagerPrivate;

.field private lR:Lcom/glympse/android/hal/GSmsProvider;

.field private lS:Lcom/glympse/android/lib/GFavoritesManagerPrivate;

.field private lT:Lcom/glympse/android/lib/GRecipientsManager;

.field private lU:Lcom/glympse/android/lib/GMessagesManager;

.field private lV:Lcom/glympse/android/lib/GPlacesManager;

.field private lW:Lcom/glympse/android/lib/GDiagnosticsManager;

.field private lX:Lcom/glympse/android/lib/GCorrectedTime;

.field private lY:Lcom/glympse/android/lib/GHandlerManager;

.field private lZ:Lcom/glympse/android/lib/GContentResolver;

.field private lc:Z

.field private lu:Ljava/lang/String;

.field private lv:I

.field private lw:I

.field private lx:Z

.field private ly:J

.field private lz:Z

.field private ma:Lcom/glympse/android/lib/GAvatarUploader;

.field private mb:Lcom/glympse/android/lib/GMessageCenter;

.field private mc:Lcom/glympse/android/lib/GNotificationCenter;

.field private md:Lcom/glympse/android/lib/GPlaceSearchEnginePrivate;

.field private me:Lcom/glympse/android/lib/GDirectionsManagerPrivate;

.field private mf:Lcom/glympse/android/lib/GWifiManager;

.field private mg:Lcom/glympse/android/lib/GTriggersManagerPrivate;

.field private mh:Z

.field private mi:Z


# direct methods
.method public constructor <init>(Lcom/glympse/android/hal/GContextHolder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 238
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 239
    const-string v0, "[Glympse.Glympse]"

    invoke-static {v3, v0}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 241
    iput-object p3, p0, Lcom/glympse/android/lib/br;->fW:Ljava/lang/String;

    .line 242
    iput-object p4, p0, Lcom/glympse/android/lib/br;->iv:Ljava/lang/String;

    .line 243
    iput-boolean v2, p0, Lcom/glympse/android/lib/br;->lc:Z

    .line 244
    iput v2, p0, Lcom/glympse/android/lib/br;->lF:I

    .line 245
    iput-object p1, p0, Lcom/glympse/android/lib/br;->iu:Lcom/glympse/android/hal/GContextHolder;

    .line 249
    iput v3, p0, Lcom/glympse/android/lib/br;->lv:I

    .line 250
    iput v3, p0, Lcom/glympse/android/lib/br;->lw:I

    .line 251
    iput-boolean v2, p0, Lcom/glympse/android/lib/br;->lx:Z

    .line 252
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/glympse/android/lib/br;->ly:J

    .line 253
    iput-boolean v2, p0, Lcom/glympse/android/lib/br;->lz:Z

    .line 254
    iput-boolean v2, p0, Lcom/glympse/android/lib/br;->lA:Z

    .line 255
    iput-boolean v2, p0, Lcom/glympse/android/lib/br;->lC:Z

    .line 256
    iput-boolean v2, p0, Lcom/glympse/android/lib/br;->iC:Z

    .line 257
    iput-boolean v3, p0, Lcom/glympse/android/lib/br;->lD:Z

    .line 258
    iput-boolean v2, p0, Lcom/glympse/android/lib/br;->lE:Z

    .line 261
    iput-boolean v2, p0, Lcom/glympse/android/lib/br;->F:Z

    .line 262
    iput-boolean v2, p0, Lcom/glympse/android/lib/br;->mh:Z

    .line 263
    iput-boolean v2, p0, Lcom/glympse/android/lib/br;->mi:Z

    .line 267
    sput-object p2, Lcom/glympse/android/lib/fc;->f:Ljava/lang/String;

    .line 268
    iget-object v0, p0, Lcom/glympse/android/lib/br;->fW:Ljava/lang/String;

    sput-object v0, Lcom/glympse/android/lib/fc;->fW:Ljava/lang/String;

    .line 269
    iget-object v0, p0, Lcom/glympse/android/lib/br;->iu:Lcom/glympse/android/hal/GContextHolder;

    invoke-static {v0}, Lcom/glympse/android/lib/Debug;->init(Lcom/glympse/android/hal/GContextHolder;)V

    .line 272
    invoke-static {}, Lcom/glympse/android/lib/av;->init()V

    .line 275
    invoke-static {}, Lcom/glympse/android/hal/HalFactory;->createHandler()Lcom/glympse/android/core/GHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/br;->_handler:Lcom/glympse/android/core/GHandler;

    .line 276
    new-instance v0, Lcom/glympse/android/lib/af;

    invoke-direct {v0}, Lcom/glympse/android/lib/af;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/br;->lX:Lcom/glympse/android/lib/GCorrectedTime;

    .line 277
    new-instance v0, Lcom/glympse/android/lib/CommonSink;

    const-string v1, "Glympse"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/CommonSink;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/glympse/android/lib/br;->he:Lcom/glympse/android/lib/CommonSink;

    .line 278
    invoke-static {}, Lcom/glympse/android/hal/HalFactory;->createServiceWrapper()Lcom/glympse/android/hal/GServiceWrapper;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/br;->lH:Lcom/glympse/android/hal/GServiceWrapper;

    .line 279
    new-instance v0, Lcom/glympse/android/lib/x;

    invoke-direct {v0}, Lcom/glympse/android/lib/x;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/br;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    .line 280
    new-instance v0, Lcom/glympse/android/lib/ey;

    invoke-direct {v0}, Lcom/glympse/android/lib/ey;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/br;->lI:Lcom/glympse/android/lib/GServerPost;

    .line 281
    new-instance v0, Lcom/glympse/android/lib/dq;

    invoke-direct {v0}, Lcom/glympse/android/lib/dq;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/br;->mb:Lcom/glympse/android/lib/GMessageCenter;

    .line 284
    new-instance v0, Lcom/glympse/android/lib/s;

    invoke-direct {v0}, Lcom/glympse/android/lib/s;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/br;->lN:Lcom/glympse/android/lib/GBatteryManagerPrivate;

    .line 287
    new-instance v0, Lcom/glympse/android/lib/al;

    invoke-direct {v0}, Lcom/glympse/android/lib/al;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/br;->lW:Lcom/glympse/android/lib/GDiagnosticsManager;

    .line 290
    new-instance v0, Lcom/glympse/android/lib/ck;

    invoke-direct {v0}, Lcom/glympse/android/lib/ck;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/br;->lQ:Lcom/glympse/android/lib/GHandoffManagerPrivate;

    .line 293
    new-instance v0, Lcom/glympse/android/lib/fw;

    invoke-direct {v0}, Lcom/glympse/android/lib/fw;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/br;->mg:Lcom/glympse/android/lib/GTriggersManagerPrivate;

    .line 296
    invoke-static {p2}, Lcom/glympse/android/lib/UrlParser;->cleanupBaseUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/br;->f:Ljava/lang/String;

    .line 297
    iget-object v0, p0, Lcom/glympse/android/lib/br;->f:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 304
    :goto_0
    return-void

    .line 303
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/br;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/br;->iu:Lcom/glympse/android/hal/GContextHolder;

    iget-object v2, p0, Lcom/glympse/android/lib/br;->iv:Ljava/lang/String;

    iget-object v3, p0, Lcom/glympse/android/lib/br;->f:Ljava/lang/String;

    iget-object v4, p0, Lcom/glympse/android/lib/br;->fW:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/glympse/android/lib/GConfigPrivate;->load(Lcom/glympse/android/hal/GContextHolder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/glympse/android/lib/br;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/glympse/android/lib/br;->lG:Ljava/lang/Runnable;

    return-object p1
.end method

.method static synthetic a(Lcom/glympse/android/lib/br;Z)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/br;->e(Z)V

    return-void
.end method

.method static synthetic a(Lcom/glympse/android/lib/br;)Z
    .locals 1

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    return v0
.end method

.method private bG()Lcom/glympse/android/lib/GTriggersManagerPrivate;
    .locals 1

    .prologue
    .line 2066
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getTriggersManager()Lcom/glympse/android/api/GTriggersManager;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GTriggersManagerPrivate;

    return-object v0
.end method

.method private d(Lcom/glympse/android/lib/GGlympsePrivate;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 346
    iget-object v1, p0, Lcom/glympse/android/lib/br;->f:Ljava/lang/String;

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 356
    :cond_0
    :goto_0
    return v0

    .line 350
    :cond_1
    iget-object v1, p0, Lcom/glympse/android/lib/br;->fW:Ljava/lang/String;

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 356
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private e(Z)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 653
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->lc:Z

    if-eq v0, p1, :cond_2

    .line 655
    const/4 v0, 0x0

    .line 658
    if-nez p1, :cond_0

    .line 661
    iget-object v1, p0, Lcom/glympse/android/lib/br;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GConfigPrivate;->save()V

    .line 665
    :cond_0
    iput-boolean p1, p0, Lcom/glympse/android/lib/br;->lc:Z

    .line 668
    iget-object v1, p0, Lcom/glympse/android/lib/br;->_jobQueue:Lcom/glympse/android/lib/GJobQueue;

    invoke-interface {v1, p1}, Lcom/glympse/android/lib/GJobQueue;->setActive(Z)V

    .line 669
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lY:Lcom/glympse/android/lib/GHandlerManager;

    invoke-interface {v1, p1}, Lcom/glympse/android/lib/GHandlerManager;->setActive(Z)V

    .line 670
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lH:Lcom/glympse/android/hal/GServiceWrapper;

    iget-boolean v3, p0, Lcom/glympse/android/lib/br;->lc:Z

    invoke-interface {v1, v3}, Lcom/glympse/android/hal/GServiceWrapper;->setActive(Z)V

    .line 671
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lI:Lcom/glympse/android/lib/GServerPost;

    iget-boolean v3, p0, Lcom/glympse/android/lib/br;->lc:Z

    invoke-interface {v1, v3}, Lcom/glympse/android/lib/GServerPost;->setActive(Z)V

    .line 672
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lQ:Lcom/glympse/android/lib/GHandoffManagerPrivate;

    iget-boolean v3, p0, Lcom/glympse/android/lib/br;->lc:Z

    invoke-interface {v1, v3}, Lcom/glympse/android/lib/GHandoffManagerPrivate;->setActive(Z)V

    .line 675
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getNotificationCenter()Lcom/glympse/android/lib/GNotificationCenter;

    move-result-object v1

    iget-boolean v3, p0, Lcom/glympse/android/lib/br;->lc:Z

    invoke-interface {v1, v3}, Lcom/glympse/android/lib/GNotificationCenter;->setActive(Z)V

    .line 676
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getImageCachePrivate()Lcom/glympse/android/lib/GImageCachePrivate;

    move-result-object v1

    iget-boolean v3, p0, Lcom/glympse/android/lib/br;->lc:Z

    invoke-interface {v1, v3}, Lcom/glympse/android/lib/GImageCachePrivate;->setActive(Z)V

    .line 677
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getWifiManager()Lcom/glympse/android/lib/GWifiManager;

    move-result-object v1

    iget-boolean v3, p0, Lcom/glympse/android/lib/br;->lc:Z

    invoke-interface {v1, v3}, Lcom/glympse/android/lib/GWifiManager;->setActive(Z)V

    .line 678
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getDirectionsManagerPrivate()Lcom/glympse/android/lib/GDirectionsManagerPrivate;

    move-result-object v1

    iget-boolean v3, p0, Lcom/glympse/android/lib/br;->lc:Z

    invoke-interface {v1, v3}, Lcom/glympse/android/lib/GDirectionsManagerPrivate;->setActive(Z)V

    .line 679
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getLocationManagerPrivate()Lcom/glympse/android/lib/GLocationManagerPrivate;

    move-result-object v1

    iget-boolean v3, p0, Lcom/glympse/android/lib/br;->lc:Z

    invoke-interface {v1, v3}, Lcom/glympse/android/lib/GLocationManagerPrivate;->setActive(Z)V

    .line 680
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getUserManagerPrivate()Lcom/glympse/android/lib/GUserManagerPrivate;

    move-result-object v1

    iget-boolean v3, p0, Lcom/glympse/android/lib/br;->lc:Z

    invoke-interface {v1, v3}, Lcom/glympse/android/lib/GUserManagerPrivate;->setActive(Z)V

    .line 681
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getHistoryManagerPrivate()Lcom/glympse/android/lib/GHistoryManagerPrivate;

    move-result-object v1

    iget-boolean v3, p0, Lcom/glympse/android/lib/br;->lc:Z

    invoke-interface {v1, v3}, Lcom/glympse/android/lib/GHistoryManagerPrivate;->setActive(Z)V

    .line 684
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->okToPost()Z

    move-result v1

    if-eqz v1, :cond_6

    move v1, v2

    .line 690
    :goto_0
    if-eqz v1, :cond_1

    .line 692
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lI:Lcom/glympse/android/lib/GServerPost;

    invoke-interface {v0}, Lcom/glympse/android/lib/GServerPost;->doPost()V

    .line 696
    :cond_1
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GGlympsePrivate;

    .line 697
    const/high16 v3, 0x4000000

    const/4 v4, 0x0

    invoke-virtual {p0, v0, v2, v3, v4}, Lcom/glympse/android/lib/br;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 700
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lX:Lcom/glympse/android/lib/GCorrectedTime;

    invoke-interface {v0}, Lcom/glympse/android/lib/GCorrectedTime;->stateChanged()V

    .line 702
    iget-object v2, p0, Lcom/glympse/android/lib/br;->lW:Lcom/glympse/android/lib/GDiagnosticsManager;

    const-string v0, "platform"

    .line 703
    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v0, "state"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->lc:Z

    if-eqz v0, :cond_3

    const-string v0, "active"

    .line 704
    :goto_1
    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 702
    invoke-interface {v2, v3, v4, v0}, Lcom/glympse/android/lib/GDiagnosticsManager;->logEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 706
    const/4 v2, 0x3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[Glympse.setActive] active:"

    invoke-static {v3}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->lc:Z

    if-eqz v0, :cond_4

    const-string v0, "true post:"

    :goto_2
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz v1, :cond_5

    const-string v0, "true"

    :goto_3
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 710
    :cond_2
    return-void

    .line 703
    :cond_3
    const-string v0, "inactive"

    goto :goto_1

    .line 706
    :cond_4
    const-string v0, "false post:"

    goto :goto_2

    :cond_5
    const-string v0, "false"

    goto :goto_3

    :cond_6
    move v1, v0

    goto :goto_0
.end method

.method private isWatching()Z
    .locals 1

    .prologue
    .line 1645
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->lc:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getUserManager()Lcom/glympse/android/api/GUserManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/api/GUserManager;->anyActiveTracked()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getGroupManager()Lcom/glympse/android/api/GGroupManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/api/GGroupManager;->anyActiveTracked()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addListener(Lcom/glympse/android/api/GEventListener;)Z
    .locals 1

    .prologue
    .line 2161
    iget-object v0, p0, Lcom/glympse/android/lib/br;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1}, Lcom/glympse/android/lib/CommonSink;->addListener(Lcom/glympse/android/api/GEventListener;)Z

    move-result v0

    return v0
.end method

.method public addLocation(Lcom/glympse/android/core/GLocation;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2087
    iget-object v2, p0, Lcom/glympse/android/lib/br;->lX:Lcom/glympse/android/lib/GCorrectedTime;

    invoke-interface {v2}, Lcom/glympse/android/lib/GCorrectedTime;->getTime()J

    move-result-wide v2

    .line 2091
    iget-boolean v4, p0, Lcom/glympse/android/lib/br;->lc:Z

    if-nez v4, :cond_0

    .line 2095
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getHistoryManagerPrivate()Lcom/glympse/android/lib/GHistoryManagerPrivate;

    move-result-object v4

    invoke-interface {v4, v2, v3}, Lcom/glympse/android/lib/GHistoryManagerPrivate;->updateState(J)V

    .line 2100
    invoke-static {}, Lcom/glympse/android/hal/Concurrent;->getBackgroundMode()I

    move-result v4

    if-ne v0, v4, :cond_0

    .line 2103
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getBatteryManagerPrivate()Lcom/glympse/android/lib/GBatteryManagerPrivate;

    move-result-object v4

    invoke-interface {v4}, Lcom/glympse/android/lib/GBatteryManagerPrivate;->updateStatus()V

    .line 2106
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getBatteryManager()Lcom/glympse/android/api/GBatteryManager;

    move-result-object v4

    invoke-interface {v4}, Lcom/glympse/android/api/GBatteryManager;->isBatteryOk()Z

    move-result v4

    if-nez v4, :cond_0

    .line 2143
    :goto_0
    return-void

    .line 2116
    :cond_0
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->isSharing()Z

    move-result v4

    .line 2117
    if-eqz v4, :cond_2

    .line 2120
    iget-object v5, p0, Lcom/glympse/android/lib/br;->lI:Lcom/glympse/android/lib/GServerPost;

    invoke-interface {v5, v2, v3, p1}, Lcom/glympse/android/lib/GServerPost;->addLocation(JLcom/glympse/android/core/GLocation;)V

    .line 2142
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getUserManagerPrivate()Lcom/glympse/android/lib/GUserManagerPrivate;

    move-result-object v2

    iget-boolean v3, p0, Lcom/glympse/android/lib/br;->lc:Z

    if-eqz v3, :cond_3

    if-eqz v4, :cond_3

    :goto_2
    invoke-interface {v2, p1, v4, v0}, Lcom/glympse/android/lib/GUserManagerPrivate;->setSelfLocation(Lcom/glympse/android/core/GLocation;ZZ)V

    goto :goto_0

    .line 2125
    :cond_2
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getLocationManagerPrivate()Lcom/glympse/android/lib/GLocationManagerPrivate;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/glympse/android/lib/GLocationManagerPrivate;->startStopLocation(Z)V

    .line 2132
    iget-object v2, p0, Lcom/glympse/android/lib/br;->lI:Lcom/glympse/android/lib/GServerPost;

    invoke-interface {v2}, Lcom/glympse/android/lib/GServerPost;->haveLocationsToPost()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2135
    iget-object v2, p0, Lcom/glympse/android/lib/br;->lI:Lcom/glympse/android/lib/GServerPost;

    invoke-interface {v2}, Lcom/glympse/android/lib/GServerPost;->doPost()V

    goto :goto_1

    :cond_3
    move v0, v1

    .line 2142
    goto :goto_2
.end method

.method public allowSiblingTickets(Z)V
    .locals 1

    .prologue
    .line 1429
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-eqz v0, :cond_0

    .line 1436
    :goto_0
    return-void

    .line 1435
    :cond_0
    iput-boolean p1, p0, Lcom/glympse/android/lib/br;->lC:Z

    goto :goto_0
.end method

.method public applyInitialProfile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1755
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-nez v0, :cond_1

    .line 1767
    :cond_0
    :goto_0
    return-void

    .line 1759
    :cond_1
    invoke-static {p1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p2}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1765
    :cond_2
    new-instance v1, Lcom/glympse/android/lib/cs;

    invoke-direct {v1}, Lcom/glympse/android/lib/cs;-><init>()V

    .line 1766
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GGlympse;

    invoke-virtual {v1, v0, p1, p2}, Lcom/glympse/android/lib/cs;->a(Lcom/glympse/android/api/GGlympse;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public arePrivateGroupsEnabled()Z
    .locals 1

    .prologue
    .line 1457
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->iC:Z

    return v0
.end method

.method public areSiblingTicketsAllowed()Z
    .locals 1

    .prologue
    .line 1440
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->lC:Z

    return v0
.end method

.method public associateContext(JLjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2186
    iget-object v0, p0, Lcom/glympse/android/lib/br;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1, p2, p3}, Lcom/glympse/android/lib/CommonSink;->associateContext(JLjava/lang/Object;)V

    .line 2187
    return-void
.end method

.method public canDeviceSendSms()I
    .locals 1

    .prologue
    .line 1281
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getSmsProvider()Lcom/glympse/android/hal/GSmsProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/hal/GSmsProvider;->canDeviceSendSms()I

    move-result v0

    return v0
.end method

.method public cleanupInviteCode(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 994
    invoke-static {p1}, Lcom/glympse/android/lib/TicketCode;->cleanupInviteCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public clearContext(J)V
    .locals 1

    .prologue
    .line 2196
    iget-object v0, p0, Lcom/glympse/android/lib/br;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1, p2}, Lcom/glympse/android/lib/CommonSink;->clearContext(J)V

    .line 2197
    return-void
.end method

.method public compareInviteCodes(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 999
    invoke-static {p1}, Lcom/glympse/android/lib/TicketCode;->toLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 1000
    invoke-static {p2}, Lcom/glympse/android/lib/TicketCode;->toLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1003
    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public confirmAccount(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)Lcom/glympse/android/api/GEventSink;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 363
    iget-boolean v1, p0, Lcom/glympse/android/lib/br;->mh:Z

    if-eqz v1, :cond_1

    .line 376
    :cond_0
    :goto_0
    return-object v0

    .line 370
    :cond_1
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->hasUserAccount()Z

    move-result v1

    if-nez v1, :cond_0

    .line 376
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-static {v0, p1, p2}, Lcom/glympse/android/lib/d;->a(Lcom/glympse/android/lib/GGlympsePrivate;Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)Lcom/glympse/android/api/GEventSink;

    move-result-object v0

    goto :goto_0
.end method

.method public createInviteSnapshot(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;Lcom/glympse/android/core/GPrimitive;)Lcom/glympse/android/api/GImage;
    .locals 7

    .prologue
    .line 1214
    invoke-static {p1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/glympse/android/lib/br;->lI:Lcom/glympse/android/lib/GServerPost;

    .line 1215
    invoke-interface {v1}, Lcom/glympse/android/lib/GServerPost;->getAuthState()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 1217
    :cond_0
    const/4 v2, 0x0

    .line 1226
    :goto_0
    return-object v2

    .line 1221
    :cond_1
    new-instance v2, Lcom/glympse/android/lib/cq;

    invoke-direct {v2}, Lcom/glympse/android/lib/cq;-><init>()V

    .line 1224
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getImageCachePrivate()Lcom/glympse/android/lib/GImageCachePrivate;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/lib/GImageCachePrivate;->getJobQueue()Lcom/glympse/android/lib/GJobQueue;

    move-result-object v6

    new-instance v0, Lcom/glympse/android/lib/ag;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/glympse/android/lib/GGlympsePrivate;

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/glympse/android/lib/ag;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/GImagePrivate;Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;Lcom/glympse/android/core/GPrimitive;)V

    invoke-interface {v6, v0}, Lcom/glympse/android/lib/GJobQueue;->addJob(Lcom/glympse/android/lib/GJob;)V

    goto :goto_0
.end method

.method public declineLocation(Lcom/glympse/android/core/GLocation;)V
    .locals 2

    .prologue
    .line 2148
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->isSharing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2151
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getLocationManagerPrivate()Lcom/glympse/android/lib/GLocationManagerPrivate;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GLocationManagerPrivate;->startStopLocation(Z)V

    .line 2153
    :cond_0
    return-void
.end method

.method public decodeInvite(Ljava/lang/String;I)Lcom/glympse/android/api/GEventSink;
    .locals 1

    .prologue
    .line 1008
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/glympse/android/lib/br;->decodeInvite(Ljava/lang/String;ILcom/glympse/android/api/GInvite;)Lcom/glympse/android/api/GEventSink;

    move-result-object v0

    return-object v0
.end method

.method public decodeInvite(Ljava/lang/String;ILcom/glympse/android/api/GInvite;)Lcom/glympse/android/api/GEventSink;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 1014
    invoke-static {p1}, Lcom/glympse/android/lib/TicketCode;->cleanupInviteCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1015
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-eqz v0, :cond_0

    if-nez v2, :cond_1

    .line 1017
    :cond_0
    const/4 v3, 0x0

    .line 1030
    :goto_0
    return-object v3

    .line 1020
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[Glympse.decodeInvite] Code: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 1023
    new-instance v3, Lcom/glympse/android/lib/ay;

    invoke-direct {v3}, Lcom/glympse/android/lib/ay;-><init>()V

    .line 1024
    const-wide v0, 0x1000000000001L

    invoke-interface {v3, v0, v1, v2}, Lcom/glympse/android/api/GEventSink;->associateContext(JLjava/lang/Object;)V

    .line 1027
    new-instance v0, Lcom/glympse/android/lib/dc;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/glympse/android/lib/GGlympsePrivate;

    move v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/glympse/android/lib/dc;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Ljava/lang/String;Lcom/glympse/android/api/GEventSink;ILcom/glympse/android/api/GInvite;)V

    .line 1028
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lI:Lcom/glympse/android/lib/GServerPost;

    invoke-interface {v1, v0, v6}, Lcom/glympse/android/lib/GServerPost;->invokeEndpoint(Lcom/glympse/android/lib/GApiEndpoint;Z)V

    goto :goto_0
.end method

.method public deriveContext(Lcom/glympse/android/api/GEventSink;)V
    .locals 1

    .prologue
    .line 2206
    iget-object v0, p0, Lcom/glympse/android/lib/br;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1}, Lcom/glympse/android/lib/CommonSink;->deriveContext(Lcom/glympse/android/api/GEventSink;)V

    .line 2207
    return-void
.end method

.method public enableAccountSharing(Z)V
    .locals 1

    .prologue
    .line 1734
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-eqz v0, :cond_0

    .line 1741
    :goto_0
    return-void

    .line 1740
    :cond_0
    iput-boolean p1, p0, Lcom/glympse/android/lib/br;->lA:Z

    goto :goto_0
.end method

.method public enableApplicationsManager(Z)V
    .locals 1

    .prologue
    .line 1485
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-eqz v0, :cond_0

    .line 1492
    :goto_0
    return-void

    .line 1491
    :cond_0
    iput-boolean p1, p0, Lcom/glympse/android/lib/br;->lE:Z

    goto :goto_0
.end method

.method public enablePrivateGroups(Z)V
    .locals 1

    .prologue
    .line 1446
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-eqz v0, :cond_0

    .line 1453
    :goto_0
    return-void

    .line 1452
    :cond_0
    iput-boolean p1, p0, Lcom/glympse/android/lib/br;->iC:Z

    goto :goto_0
.end method

.method public enablePushEchoing(Z)V
    .locals 0

    .prologue
    .line 1629
    iput-boolean p1, p0, Lcom/glympse/android/lib/br;->lD:Z

    .line 1630
    return-void
.end method

.method public enableSmsScraping(Z)V
    .locals 1

    .prologue
    .line 1312
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-eqz v0, :cond_0

    .line 1317
    :goto_0
    return-void

    .line 1316
    :cond_0
    iput-boolean p1, p0, Lcom/glympse/android/lib/br;->lz:Z

    goto :goto_0
.end method

.method public eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V
    .locals 6

    .prologue
    .line 2176
    iget-object v0, p0, Lcom/glympse/android/lib/br;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/glympse/android/api/GEventSink;

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/glympse/android/lib/CommonSink;->eventsOccurred(Lcom/glympse/android/api/GEventSink;Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 2177
    return-void
.end method

.method public extractInviteCodes(Ljava/lang/String;)Lcom/glympse/android/core/GArray;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/glympse/android/core/GArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1176
    invoke-static {p1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1178
    const/4 v0, 0x0

    .line 1186
    :goto_0
    return-object v0

    .line 1182
    :cond_0
    new-instance v0, Lcom/glympse/android/lib/UrlParser;

    invoke-direct {v0}, Lcom/glympse/android/lib/UrlParser;-><init>()V

    .line 1183
    iget-object v1, p0, Lcom/glympse/android/lib/br;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GConfigPrivate;->getSupportedServersAndSchemes()Lcom/glympse/android/core/GArray;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lcom/glympse/android/lib/UrlParser;->parseUrls(Ljava/lang/String;Lcom/glympse/android/core/GArray;Z)Z

    .line 1186
    invoke-virtual {v0}, Lcom/glympse/android/lib/UrlParser;->getInviteCodes()Lcom/glympse/android/core/GArray;

    move-result-object v0

    goto :goto_0
.end method

.method public findSinks(Lcom/glympse/android/api/GEventListener;)Lcom/glympse/android/core/GArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/glympse/android/api/GEventListener;",
            ")",
            "Lcom/glympse/android/core/GArray",
            "<",
            "Lcom/glympse/android/api/GEventSink;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1384
    if-nez p1, :cond_0

    .line 1386
    const/4 v0, 0x0

    .line 1388
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-static {v0, p1}, Lcom/glympse/android/lib/fa;->a(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/api/GEventListener;)Lcom/glympse/android/core/GArray;

    move-result-object v0

    goto :goto_0
.end method

.method public getAccessToken()Ljava/lang/String;
    .locals 3

    .prologue
    .line 719
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-nez v0, :cond_0

    .line 721
    const/4 v0, 0x0

    .line 743
    :goto_0
    return-object v0

    .line 726
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/br;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GConfigPrivate;->getAccessToken()Ljava/lang/String;

    move-result-object v1

    .line 729
    if-nez v1, :cond_1

    .line 733
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lI:Lcom/glympse/android/lib/GServerPost;

    invoke-interface {v0}, Lcom/glympse/android/lib/GServerPost;->getAccessToken()Ljava/lang/String;

    move-result-object v2

    .line 734
    if-eqz v2, :cond_1

    .line 738
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lI:Lcom/glympse/android/lib/GServerPost;

    check-cast v0, Lcom/glympse/android/lib/bf;

    .line 739
    invoke-interface {v0, v2}, Lcom/glympse/android/lib/bf;->d(Ljava/lang/String;)V

    :cond_1
    move-object v0, v1

    .line 743
    goto :goto_0
.end method

.method public getAccountProfile()Lcom/glympse/android/core/GPrimitive;
    .locals 1

    .prologue
    .line 1929
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lB:Lcom/glympse/android/core/GPrimitive;

    return-object v0
.end method

.method public getApiKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1506
    iget-object v0, p0, Lcom/glympse/android/lib/br;->fW:Ljava/lang/String;

    return-object v0
.end method

.method public getApiVersion()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0x2e

    .line 1522
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1523
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1524
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1525
    const/16 v1, 0x1c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1526
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1527
    const/16 v1, 0x85

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1528
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getApiVersionFull()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1533
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1534
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getApiVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1556
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getApplicationsManager()Lcom/glympse/android/api/GApplicationsManager;
    .locals 2

    .prologue
    .line 907
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->mh:Z

    if-nez v0, :cond_0

    .line 909
    const/4 v0, 0x0

    .line 922
    :goto_0
    return-object v0

    .line 912
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lP:Lcom/glympse/android/lib/GApplicationsManagerPrivate;

    if-nez v0, :cond_1

    .line 914
    new-instance v0, Lcom/glympse/android/lib/l;

    invoke-direct {v0}, Lcom/glympse/android/lib/l;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/br;->lP:Lcom/glympse/android/lib/GApplicationsManagerPrivate;

    .line 916
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-eqz v0, :cond_1

    .line 918
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lP:Lcom/glympse/android/lib/GApplicationsManagerPrivate;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GApplicationsManagerPrivate;->start(Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 922
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lP:Lcom/glympse/android/lib/GApplicationsManagerPrivate;

    goto :goto_0
.end method

.method public getAvatarUploader()Lcom/glympse/android/lib/GAvatarUploader;
    .locals 2

    .prologue
    .line 1974
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-nez v0, :cond_0

    .line 1976
    const/4 v0, 0x0

    .line 1985
    :goto_0
    return-object v0

    .line 1979
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/br;->ma:Lcom/glympse/android/lib/GAvatarUploader;

    if-nez v0, :cond_1

    .line 1981
    new-instance v0, Lcom/glympse/android/lib/p;

    invoke-direct {v0}, Lcom/glympse/android/lib/p;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/br;->ma:Lcom/glympse/android/lib/GAvatarUploader;

    .line 1982
    iget-object v1, p0, Lcom/glympse/android/lib/br;->ma:Lcom/glympse/android/lib/GAvatarUploader;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GAvatarUploader;->start(Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 1985
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/br;->ma:Lcom/glympse/android/lib/GAvatarUploader;

    goto :goto_0
.end method

.method public getBaseUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1501
    iget-object v0, p0, Lcom/glympse/android/lib/br;->f:Ljava/lang/String;

    return-object v0
.end method

.method public getBatteryManager()Lcom/glympse/android/api/GBatteryManager;
    .locals 1

    .prologue
    .line 753
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->mh:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/br;->lN:Lcom/glympse/android/lib/GBatteryManagerPrivate;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBatteryManagerPrivate()Lcom/glympse/android/lib/GBatteryManagerPrivate;
    .locals 1

    .prologue
    .line 2061
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getBatteryManager()Lcom/glympse/android/api/GBatteryManager;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GBatteryManagerPrivate;

    return-object v0
.end method

.method public getBrand()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1479
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lu:Ljava/lang/String;

    return-object v0
.end method

.method public getConfig()Lcom/glympse/android/api/GConfig;
    .locals 1

    .prologue
    .line 748
    iget-object v0, p0, Lcom/glympse/android/lib/br;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    return-object v0
.end method

.method public getConfigPrivate()Lcom/glympse/android/lib/GConfigPrivate;
    .locals 1

    .prologue
    .line 2021
    iget-object v0, p0, Lcom/glympse/android/lib/br;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    return-object v0
.end method

.method public getContentResolver()Lcom/glympse/android/lib/GContentResolver;
    .locals 1

    .prologue
    .line 1959
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-nez v0, :cond_0

    .line 1961
    const/4 v0, 0x0

    .line 1969
    :goto_0
    return-object v0

    .line 1964
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lZ:Lcom/glympse/android/lib/GContentResolver;

    if-nez v0, :cond_1

    .line 1966
    new-instance v0, Lcom/glympse/android/lib/ae;

    invoke-direct {v0}, Lcom/glympse/android/lib/ae;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/br;->lZ:Lcom/glympse/android/lib/GContentResolver;

    .line 1969
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lZ:Lcom/glympse/android/lib/GContentResolver;

    goto :goto_0
.end method

.method public getContext(J)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2191
    iget-object v0, p0, Lcom/glympse/android/lib/br;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1, p2}, Lcom/glympse/android/lib/CommonSink;->getContext(J)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getContextHolder()Lcom/glympse/android/hal/GContextHolder;
    .locals 1

    .prologue
    .line 1728
    iget-object v0, p0, Lcom/glympse/android/lib/br;->iu:Lcom/glympse/android/hal/GContextHolder;

    return-object v0
.end method

.method public getContextKeys()Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Enumeration",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2201
    iget-object v0, p0, Lcom/glympse/android/lib/br;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0}, Lcom/glympse/android/lib/CommonSink;->getContextKeys()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public getCorrectedTime()Lcom/glympse/android/lib/GCorrectedTime;
    .locals 1

    .prologue
    .line 2011
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lX:Lcom/glympse/android/lib/GCorrectedTime;

    return-object v0
.end method

.method public getDebugLoggingLevel()I
    .locals 1

    .prologue
    .line 1892
    invoke-static {}, Lcom/glympse/android/lib/Debug;->getDebugLoggingLevel()I

    move-result v0

    return v0
.end method

.method public getDiagnosticsManager()Lcom/glympse/android/lib/GDiagnosticsManager;
    .locals 1

    .prologue
    .line 1771
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lW:Lcom/glympse/android/lib/GDiagnosticsManager;

    return-object v0
.end method

.method public getDirectionsManager()Lcom/glympse/android/api/GDirectionsManager;
    .locals 2

    .prologue
    .line 927
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-nez v0, :cond_0

    .line 929
    const/4 v0, 0x0

    .line 938
    :goto_0
    return-object v0

    .line 932
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/br;->me:Lcom/glympse/android/lib/GDirectionsManagerPrivate;

    if-nez v0, :cond_1

    .line 934
    new-instance v0, Lcom/glympse/android/lib/ar;

    invoke-direct {v0}, Lcom/glympse/android/lib/ar;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/br;->me:Lcom/glympse/android/lib/GDirectionsManagerPrivate;

    .line 935
    iget-object v1, p0, Lcom/glympse/android/lib/br;->me:Lcom/glympse/android/lib/GDirectionsManagerPrivate;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GDirectionsManagerPrivate;->start(Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 938
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/br;->me:Lcom/glympse/android/lib/GDirectionsManagerPrivate;

    goto :goto_0
.end method

.method public getDirectionsManagerPrivate()Lcom/glympse/android/lib/GDirectionsManagerPrivate;
    .locals 1

    .prologue
    .line 2036
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getDirectionsManager()Lcom/glympse/android/api/GDirectionsManager;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GDirectionsManagerPrivate;

    return-object v0
.end method

.method public getEtaMode()I
    .locals 1

    .prologue
    .line 1335
    iget v0, p0, Lcom/glympse/android/lib/br;->lw:I

    return v0
.end method

.method public getExpireOnArrival()I
    .locals 1

    .prologue
    .line 1349
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-eqz v0, :cond_0

    .line 1351
    const/4 v0, -0x1

    .line 1353
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/br;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GConfigPrivate;->getExpireOnArrival()I

    move-result v0

    goto :goto_0
.end method

.method public getFavoritesManager()Lcom/glympse/android/api/GFavoritesManager;
    .locals 3

    .prologue
    .line 1795
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lS:Lcom/glympse/android/lib/GFavoritesManagerPrivate;

    if-nez v0, :cond_0

    .line 1797
    new-instance v0, Lcom/glympse/android/lib/bb;

    invoke-direct {v0}, Lcom/glympse/android/lib/bb;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/br;->lS:Lcom/glympse/android/lib/GFavoritesManagerPrivate;

    .line 1798
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lS:Lcom/glympse/android/lib/GFavoritesManagerPrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/br;->iu:Lcom/glympse/android/hal/GContextHolder;

    iget-object v2, p0, Lcom/glympse/android/lib/br;->iv:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/glympse/android/lib/GFavoritesManagerPrivate;->load(Lcom/glympse/android/hal/GContextHolder;Ljava/lang/String;)V

    .line 1800
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-eqz v0, :cond_0

    .line 1802
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lS:Lcom/glympse/android/lib/GFavoritesManagerPrivate;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GGlympse;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GFavoritesManagerPrivate;->start(Lcom/glympse/android/api/GGlympse;)V

    .line 1806
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lS:Lcom/glympse/android/lib/GFavoritesManagerPrivate;

    return-object v0
.end method

.method public getFileLoggingLevel()I
    .locals 1

    .prologue
    .line 1915
    invoke-static {}, Lcom/glympse/android/lib/Debug;->getFileLoggingLevel()I

    move-result v0

    return v0
.end method

.method public getGroupManager()Lcom/glympse/android/api/GGroupManager;
    .locals 2

    .prologue
    .line 826
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->mh:Z

    if-nez v0, :cond_0

    .line 828
    const/4 v0, 0x0

    .line 841
    :goto_0
    return-object v0

    .line 831
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lL:Lcom/glympse/android/lib/GGroupManagerPrivate;

    if-nez v0, :cond_1

    .line 833
    new-instance v0, Lcom/glympse/android/lib/ce;

    invoke-direct {v0}, Lcom/glympse/android/lib/ce;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/br;->lL:Lcom/glympse/android/lib/GGroupManagerPrivate;

    .line 835
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-eqz v0, :cond_1

    .line 837
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lL:Lcom/glympse/android/lib/GGroupManagerPrivate;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GGroupManagerPrivate;->start(Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 841
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lL:Lcom/glympse/android/lib/GGroupManagerPrivate;

    goto :goto_0
.end method

.method public getHandler()Lcom/glympse/android/core/GHandler;
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lcom/glympse/android/lib/br;->_handler:Lcom/glympse/android/core/GHandler;

    return-object v0
.end method

.method public getHandlerManager()Lcom/glympse/android/lib/GHandlerManager;
    .locals 1

    .prologue
    .line 2016
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lY:Lcom/glympse/android/lib/GHandlerManager;

    return-object v0
.end method

.method public getHandoffManager()Lcom/glympse/android/api/GHandoffManager;
    .locals 1

    .prologue
    .line 979
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lQ:Lcom/glympse/android/lib/GHandoffManagerPrivate;

    return-object v0
.end method

.method public getHistoryLookback()J
    .locals 2

    .prologue
    .line 1423
    iget-wide v0, p0, Lcom/glympse/android/lib/br;->ly:J

    return-wide v0
.end method

.method public getHistoryManager()Lcom/glympse/android/api/GHistoryManager;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 798
    iget-boolean v1, p0, Lcom/glympse/android/lib/br;->mh:Z

    if-nez v1, :cond_1

    .line 821
    :cond_0
    :goto_0
    return-object v0

    .line 803
    :cond_1
    iget-object v1, p0, Lcom/glympse/android/lib/br;->hT:Lcom/glympse/android/lib/GHistoryManagerPrivate;

    if-nez v1, :cond_2

    .line 808
    iget-object v1, p0, Lcom/glympse/android/lib/br;->_handler:Lcom/glympse/android/core/GHandler;

    invoke-interface {v1}, Lcom/glympse/android/core/GHandler;->isMainThread()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 813
    new-instance v0, Lcom/glympse/android/lib/cm;

    invoke-direct {v0}, Lcom/glympse/android/lib/cm;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/br;->hT:Lcom/glympse/android/lib/GHistoryManagerPrivate;

    .line 815
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-eqz v0, :cond_2

    .line 817
    iget-object v1, p0, Lcom/glympse/android/lib/br;->hT:Lcom/glympse/android/lib/GHistoryManagerPrivate;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GHistoryManagerPrivate;->start(Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 821
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/br;->hT:Lcom/glympse/android/lib/GHistoryManagerPrivate;

    goto :goto_0
.end method

.method public getHistoryManagerPrivate()Lcom/glympse/android/lib/GHistoryManagerPrivate;
    .locals 1

    .prologue
    .line 2056
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getHistoryManager()Lcom/glympse/android/api/GHistoryManager;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GHistoryManagerPrivate;

    return-object v0
.end method

.method public getImageCache()Lcom/glympse/android/lib/GImageCache;
    .locals 2

    .prologue
    .line 1858
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-nez v0, :cond_0

    .line 1860
    const/4 v0, 0x0

    .line 1869
    :goto_0
    return-object v0

    .line 1863
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/br;->hC:Lcom/glympse/android/lib/GImageCachePrivate;

    if-nez v0, :cond_1

    .line 1865
    new-instance v0, Lcom/glympse/android/lib/cr;

    invoke-direct {v0}, Lcom/glympse/android/lib/cr;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/br;->hC:Lcom/glympse/android/lib/GImageCachePrivate;

    .line 1866
    iget-object v1, p0, Lcom/glympse/android/lib/br;->hC:Lcom/glympse/android/lib/GImageCachePrivate;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GImageCachePrivate;->start(Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 1869
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/br;->hC:Lcom/glympse/android/lib/GImageCachePrivate;

    goto :goto_0
.end method

.method public getImageCachePrivate()Lcom/glympse/android/lib/GImageCachePrivate;
    .locals 1

    .prologue
    .line 2031
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getImageCache()Lcom/glympse/android/lib/GImageCache;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GImageCachePrivate;

    return-object v0
.end method

.method public getInviteAspect(Ljava/lang/String;)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1195
    invoke-static {p1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1208
    :cond_0
    :goto_0
    return v0

    .line 1201
    :cond_1
    invoke-static {p1}, Lcom/glympse/android/lib/TicketCode;->toLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1202
    const-wide/16 v4, 0x0

    cmp-long v1, v4, v2

    if-eqz v1, :cond_0

    .line 1208
    invoke-static {v2, v3}, Lcom/glympse/android/lib/TicketCode;->getInviteAspect(J)I

    move-result v0

    goto :goto_0
.end method

.method public getJobQueue()Lcom/glympse/android/lib/GJobQueue;
    .locals 1

    .prologue
    .line 1939
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/br;->_jobQueue:Lcom/glympse/android/lib/GJobQueue;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLinkedAccountsManager()Lcom/glympse/android/api/GLinkedAccountsManager;
    .locals 2

    .prologue
    .line 866
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->mh:Z

    if-nez v0, :cond_0

    .line 868
    const/4 v0, 0x0

    .line 881
    :goto_0
    return-object v0

    .line 871
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lO:Lcom/glympse/android/lib/GLinkedAccountsManagerPrivate;

    if-nez v0, :cond_1

    .line 873
    new-instance v0, Lcom/glympse/android/lib/LinkedAccountsManager;

    invoke-direct {v0}, Lcom/glympse/android/lib/LinkedAccountsManager;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/br;->lO:Lcom/glympse/android/lib/GLinkedAccountsManagerPrivate;

    .line 875
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-eqz v0, :cond_1

    .line 877
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lO:Lcom/glympse/android/lib/GLinkedAccountsManagerPrivate;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GLinkedAccountsManagerPrivate;->start(Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 881
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lO:Lcom/glympse/android/lib/GLinkedAccountsManagerPrivate;

    goto :goto_0
.end method

.method public getLinkedAccountsManagerPrivate()Lcom/glympse/android/lib/GLinkedAccountsManagerPrivate;
    .locals 1

    .prologue
    .line 2046
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getLinkedAccountsManager()Lcom/glympse/android/api/GLinkedAccountsManager;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GLinkedAccountsManagerPrivate;

    return-object v0
.end method

.method public getListeners()Lcom/glympse/android/core/GArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/glympse/android/core/GArray",
            "<",
            "Lcom/glympse/android/api/GEventListener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2171
    iget-object v0, p0, Lcom/glympse/android/lib/br;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0}, Lcom/glympse/android/lib/CommonSink;->getListeners()Lcom/glympse/android/core/GArray;

    move-result-object v0

    return-object v0
.end method

.method public getLocationManager()Lcom/glympse/android/api/GLocationManager;
    .locals 2

    .prologue
    .line 758
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->mh:Z

    if-nez v0, :cond_0

    .line 760
    const/4 v0, 0x0

    .line 773
    :goto_0
    return-object v0

    .line 763
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/br;->hS:Lcom/glympse/android/lib/GLocationManagerPrivate;

    if-nez v0, :cond_1

    .line 765
    new-instance v0, Lcom/glympse/android/lib/do;

    invoke-direct {v0}, Lcom/glympse/android/lib/do;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/br;->hS:Lcom/glympse/android/lib/GLocationManagerPrivate;

    .line 767
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-eqz v0, :cond_1

    .line 769
    iget-object v1, p0, Lcom/glympse/android/lib/br;->hS:Lcom/glympse/android/lib/GLocationManagerPrivate;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GLocationManagerPrivate;->start(Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 773
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/br;->hS:Lcom/glympse/android/lib/GLocationManagerPrivate;

    goto :goto_0
.end method

.method public getLocationManagerPrivate()Lcom/glympse/android/lib/GLocationManagerPrivate;
    .locals 1

    .prologue
    .line 2041
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getLocationManager()Lcom/glympse/android/api/GLocationManager;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GLocationManagerPrivate;

    return-object v0
.end method

.method public getLogFile()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1379
    invoke-static {}, Lcom/glympse/android/hal/Concurrent;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->getLogFile(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMessageCenter()Lcom/glympse/android/lib/GMessageCenter;
    .locals 1

    .prologue
    .line 2006
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/br;->mb:Lcom/glympse/android/lib/GMessageCenter;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMessagesManager()Lcom/glympse/android/lib/GMessagesManager;
    .locals 3

    .prologue
    .line 1811
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lU:Lcom/glympse/android/lib/GMessagesManager;

    if-nez v0, :cond_0

    .line 1813
    new-instance v0, Lcom/glympse/android/lib/dr;

    invoke-direct {v0}, Lcom/glympse/android/lib/dr;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/br;->lU:Lcom/glympse/android/lib/GMessagesManager;

    .line 1814
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lU:Lcom/glympse/android/lib/GMessagesManager;

    iget-object v1, p0, Lcom/glympse/android/lib/br;->iu:Lcom/glympse/android/hal/GContextHolder;

    iget-object v2, p0, Lcom/glympse/android/lib/br;->iv:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/glympse/android/lib/GMessagesManager;->load(Lcom/glympse/android/hal/GContextHolder;Ljava/lang/String;)V

    .line 1816
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-eqz v0, :cond_0

    .line 1818
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lU:Lcom/glympse/android/lib/GMessagesManager;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GGlympse;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GMessagesManager;->start(Lcom/glympse/android/api/GGlympse;)V

    .line 1822
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lU:Lcom/glympse/android/lib/GMessagesManager;

    return-object v0
.end method

.method public getNetworkManager()Lcom/glympse/android/api/GNetworkManager;
    .locals 2

    .prologue
    .line 846
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->mh:Z

    if-nez v0, :cond_0

    .line 848
    const/4 v0, 0x0

    .line 861
    :goto_0
    return-object v0

    .line 851
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lM:Lcom/glympse/android/lib/GNetworkManagerPrivate;

    if-nez v0, :cond_1

    .line 853
    new-instance v0, Lcom/glympse/android/lib/du;

    invoke-direct {v0}, Lcom/glympse/android/lib/du;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/br;->lM:Lcom/glympse/android/lib/GNetworkManagerPrivate;

    .line 855
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-eqz v0, :cond_1

    .line 857
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lM:Lcom/glympse/android/lib/GNetworkManagerPrivate;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GNetworkManagerPrivate;->start(Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 861
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lM:Lcom/glympse/android/lib/GNetworkManagerPrivate;

    goto :goto_0
.end method

.method public getNetworkManagerPrivate()Lcom/glympse/android/lib/GNetworkManagerPrivate;
    .locals 1

    .prologue
    .line 2026
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getNetworkManager()Lcom/glympse/android/api/GNetworkManager;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GNetworkManagerPrivate;

    return-object v0
.end method

.method public getNotificationCenter()Lcom/glympse/android/lib/GNotificationCenter;
    .locals 3

    .prologue
    .line 1990
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-nez v0, :cond_0

    .line 1992
    const/4 v0, 0x0

    .line 2001
    :goto_0
    return-object v0

    .line 1995
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/br;->mc:Lcom/glympse/android/lib/GNotificationCenter;

    if-nez v0, :cond_1

    .line 1997
    new-instance v0, Lcom/glympse/android/lib/dx;

    iget-object v1, p0, Lcom/glympse/android/lib/br;->iu:Lcom/glympse/android/hal/GContextHolder;

    iget-object v2, p0, Lcom/glympse/android/lib/br;->iv:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/glympse/android/lib/dx;-><init>(Lcom/glympse/android/hal/GContextHolder;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/glympse/android/lib/br;->mc:Lcom/glympse/android/lib/GNotificationCenter;

    .line 1998
    iget-object v1, p0, Lcom/glympse/android/lib/br;->mc:Lcom/glympse/android/lib/GNotificationCenter;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GNotificationCenter;->start(Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 2001
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/br;->mc:Lcom/glympse/android/lib/GNotificationCenter;

    goto :goto_0
.end method

.method public getPairingManager()Lcom/glympse/android/api/GPairingManager;
    .locals 2

    .prologue
    .line 886
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->mh:Z

    if-nez v0, :cond_0

    .line 888
    const/4 v0, 0x0

    .line 901
    :goto_0
    return-object v0

    .line 891
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/br;->id:Lcom/glympse/android/lib/GPairingManagerPrivate;

    if-nez v0, :cond_1

    .line 893
    new-instance v0, Lcom/glympse/android/lib/PairingManager;

    invoke-direct {v0}, Lcom/glympse/android/lib/PairingManager;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/br;->id:Lcom/glympse/android/lib/GPairingManagerPrivate;

    .line 895
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-eqz v0, :cond_1

    .line 897
    iget-object v1, p0, Lcom/glympse/android/lib/br;->id:Lcom/glympse/android/lib/GPairingManagerPrivate;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GPairingManagerPrivate;->start(Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 901
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/br;->id:Lcom/glympse/android/lib/GPairingManagerPrivate;

    goto :goto_0
.end method

.method public getPlaceSearchEngine()Lcom/glympse/android/api/GPlaceSearchEngine;
    .locals 2

    .prologue
    .line 959
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-nez v0, :cond_0

    .line 961
    const/4 v0, 0x0

    .line 970
    :goto_0
    return-object v0

    .line 964
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/br;->md:Lcom/glympse/android/lib/GPlaceSearchEnginePrivate;

    if-nez v0, :cond_1

    .line 966
    new-instance v0, Lcom/glympse/android/lib/eh;

    invoke-direct {v0}, Lcom/glympse/android/lib/eh;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/br;->md:Lcom/glympse/android/lib/GPlaceSearchEnginePrivate;

    .line 967
    iget-object v1, p0, Lcom/glympse/android/lib/br;->md:Lcom/glympse/android/lib/GPlaceSearchEnginePrivate;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GPlaceSearchEnginePrivate;->start(Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 970
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/br;->md:Lcom/glympse/android/lib/GPlaceSearchEnginePrivate;

    goto :goto_0
.end method

.method public getPlacesManager()Lcom/glympse/android/lib/GPlacesManager;
    .locals 3

    .prologue
    .line 1842
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lV:Lcom/glympse/android/lib/GPlacesManager;

    if-nez v0, :cond_0

    .line 1844
    new-instance v0, Lcom/glympse/android/lib/em;

    invoke-direct {v0}, Lcom/glympse/android/lib/em;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/br;->lV:Lcom/glympse/android/lib/GPlacesManager;

    .line 1845
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lV:Lcom/glympse/android/lib/GPlacesManager;

    iget-object v1, p0, Lcom/glympse/android/lib/br;->iu:Lcom/glympse/android/hal/GContextHolder;

    iget-object v2, p0, Lcom/glympse/android/lib/br;->iv:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/glympse/android/lib/GPlacesManager;->load(Lcom/glympse/android/hal/GContextHolder;Ljava/lang/String;)V

    .line 1847
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-eqz v0, :cond_0

    .line 1849
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lV:Lcom/glympse/android/lib/GPlacesManager;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GGlympse;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GPlacesManager;->start(Lcom/glympse/android/api/GGlympse;)V

    .line 1853
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lV:Lcom/glympse/android/lib/GPlacesManager;

    return-object v0
.end method

.method public getPrefix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1511
    iget-object v0, p0, Lcom/glympse/android/lib/br;->iv:Ljava/lang/String;

    return-object v0
.end method

.method public getRecipientsManager()Lcom/glympse/android/lib/GRecipientsManager;
    .locals 2

    .prologue
    .line 1780
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lT:Lcom/glympse/android/lib/GRecipientsManager;

    if-nez v0, :cond_0

    .line 1782
    new-instance v0, Lcom/glympse/android/lib/ep;

    invoke-direct {v0}, Lcom/glympse/android/lib/ep;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/br;->lT:Lcom/glympse/android/lib/GRecipientsManager;

    .line 1784
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-eqz v0, :cond_0

    .line 1786
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lT:Lcom/glympse/android/lib/GRecipientsManager;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GGlympse;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GRecipientsManager;->start(Lcom/glympse/android/api/GGlympse;)V

    .line 1790
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lT:Lcom/glympse/android/lib/GRecipientsManager;

    return-object v0
.end method

.method public getServerPost()Lcom/glympse/android/lib/GServerPost;
    .locals 1

    .prologue
    .line 1934
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lI:Lcom/glympse/android/lib/GServerPost;

    return-object v0
.end method

.method public getSmsProvider()Lcom/glympse/android/hal/GSmsProvider;
    .locals 1

    .prologue
    .line 984
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lR:Lcom/glympse/android/hal/GSmsProvider;

    if-nez v0, :cond_0

    .line 986
    iget-object v0, p0, Lcom/glympse/android/lib/br;->iu:Lcom/glympse/android/hal/GContextHolder;

    invoke-interface {v0}, Lcom/glympse/android/hal/GContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/glympse/android/hal/HalFactory;->createSmsProvider(Landroid/content/Context;)Lcom/glympse/android/hal/GSmsProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/br;->lR:Lcom/glympse/android/hal/GSmsProvider;

    .line 989
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lR:Lcom/glympse/android/hal/GSmsProvider;

    return-object v0
.end method

.method public getSmsSendMode()I
    .locals 1

    .prologue
    .line 1307
    iget v0, p0, Lcom/glympse/android/lib/br;->lv:I

    return v0
.end method

.method public getTicketProtocol()Lcom/glympse/android/lib/GTicketProtocol;
    .locals 2

    .prologue
    .line 1944
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lJ:Lcom/glympse/android/lib/GTicketProtocol;

    if-nez v0, :cond_0

    .line 1946
    new-instance v0, Lcom/glympse/android/lib/fp;

    invoke-direct {v0}, Lcom/glympse/android/lib/fp;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/br;->lJ:Lcom/glympse/android/lib/GTicketProtocol;

    .line 1948
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-eqz v0, :cond_0

    .line 1950
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lJ:Lcom/glympse/android/lib/GTicketProtocol;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GTicketProtocol;->start(Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 1954
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lJ:Lcom/glympse/android/lib/GTicketProtocol;

    return-object v0
.end method

.method public getTime()J
    .locals 2

    .prologue
    .line 335
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lX:Lcom/glympse/android/lib/GCorrectedTime;

    invoke-interface {v0}, Lcom/glympse/android/lib/GCorrectedTime;->getTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public getTriggersManager()Lcom/glympse/android/api/GTriggersManager;
    .locals 2

    .prologue
    .line 943
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-nez v0, :cond_0

    .line 945
    const/4 v0, 0x0

    .line 954
    :goto_0
    return-object v0

    .line 948
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/br;->mg:Lcom/glympse/android/lib/GTriggersManagerPrivate;

    if-nez v0, :cond_1

    .line 950
    new-instance v0, Lcom/glympse/android/lib/fw;

    invoke-direct {v0}, Lcom/glympse/android/lib/fw;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/br;->mg:Lcom/glympse/android/lib/GTriggersManagerPrivate;

    .line 951
    iget-object v1, p0, Lcom/glympse/android/lib/br;->mg:Lcom/glympse/android/lib/GTriggersManagerPrivate;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GTriggersManagerPrivate;->start(Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 954
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/br;->mg:Lcom/glympse/android/lib/GTriggersManagerPrivate;

    goto :goto_0
.end method

.method public getUserManager()Lcom/glympse/android/api/GUserManager;
    .locals 3

    .prologue
    .line 778
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->mh:Z

    if-nez v0, :cond_0

    .line 780
    const/4 v0, 0x0

    .line 793
    :goto_0
    return-object v0

    .line 783
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lK:Lcom/glympse/android/lib/GUserManagerPrivate;

    if-nez v0, :cond_1

    .line 785
    new-instance v0, Lcom/glympse/android/lib/gf;

    invoke-direct {v0}, Lcom/glympse/android/lib/gf;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/br;->lK:Lcom/glympse/android/lib/GUserManagerPrivate;

    .line 787
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-eqz v0, :cond_1

    .line 789
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lK:Lcom/glympse/android/lib/GUserManagerPrivate;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GGlympsePrivate;

    iget-object v2, p0, Lcom/glympse/android/lib/br;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v2}, Lcom/glympse/android/lib/GConfigPrivate;->getCurrentAccount()Lcom/glympse/android/core/GPrimitive;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/glympse/android/lib/GUserManagerPrivate;->start(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/core/GPrimitive;)V

    .line 793
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lK:Lcom/glympse/android/lib/GUserManagerPrivate;

    goto :goto_0
.end method

.method public getUserManagerPrivate()Lcom/glympse/android/lib/GUserManagerPrivate;
    .locals 1

    .prologue
    .line 2051
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getUserManager()Lcom/glympse/android/api/GUserManager;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GUserManagerPrivate;

    return-object v0
.end method

.method public getWifiManager()Lcom/glympse/android/lib/GWifiManager;
    .locals 2

    .prologue
    .line 1827
    iget-object v0, p0, Lcom/glympse/android/lib/br;->mf:Lcom/glympse/android/lib/GWifiManager;

    if-nez v0, :cond_0

    .line 1829
    new-instance v0, Lcom/glympse/android/lib/gm;

    invoke-direct {v0}, Lcom/glympse/android/lib/gm;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/br;->mf:Lcom/glympse/android/lib/GWifiManager;

    .line 1831
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-eqz v0, :cond_0

    .line 1833
    iget-object v1, p0, Lcom/glympse/android/lib/br;->mf:Lcom/glympse/android/lib/GWifiManager;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GWifiManager;->start(Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 1837
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/br;->mf:Lcom/glympse/android/lib/GWifiManager;

    return-object v0
.end method

.method public handleRemoteNotification(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1608
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/glympse/android/lib/br;->handleRemoteNotification(Ljava/lang/String;Ljava/lang/String;)V

    .line 1609
    return-void
.end method

.method public handleRemoteNotification(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1614
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-nez v0, :cond_0

    .line 1625
    :goto_0
    return-void

    .line 1620
    :cond_0
    const/4 v0, 0x2

    const-string v1, "[Glympse.handleRemoteNotification] PUSH notification was received"

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 1621
    invoke-static {p1}, Lcom/glympse/android/lib/Debug;->dumpPackets(Ljava/lang/String;)V

    .line 1624
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getNotificationCenter()Lcom/glympse/android/lib/GNotificationCenter;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/glympse/android/lib/GNotificationCenter;->handle(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public hasContext(J)Z
    .locals 1

    .prologue
    .line 2181
    iget-object v0, p0, Lcom/glympse/android/lib/br;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1, p2}, Lcom/glympse/android/lib/CommonSink;->hasContext(J)Z

    move-result v0

    return v0
.end method

.method public hasUserAccount()Z
    .locals 1

    .prologue
    .line 1462
    iget-object v0, p0, Lcom/glympse/android/lib/br;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GConfigPrivate;->getCurrentAccount()Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasValidDeviceToken()Z
    .locals 1

    .prologue
    .line 1603
    iget-object v0, p0, Lcom/glympse/android/lib/br;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GConfigPrivate;->getRegistrationToken()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isAccountSharingEnabled()Z
    .locals 2

    .prologue
    .line 1749
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->lA:Z

    if-nez v0, :cond_0

    invoke-static {}, Lcom/glympse/android/hal/Platform;->getOsName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "bb"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isActive()Z
    .locals 1

    .prologue
    .line 714
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->lc:Z

    return v0
.end method

.method public isApplicationsManagerEnabled()Z
    .locals 1

    .prologue
    .line 1496
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->lE:Z

    return v0
.end method

.method public isFirstLaunch()Z
    .locals 1

    .prologue
    .line 589
    iget-object v0, p0, Lcom/glympse/android/lib/br;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GConfigPrivate;->isFirstLaunch()Z

    move-result v0

    return v0
.end method

.method public isHistoryRestored()Z
    .locals 1

    .prologue
    .line 1403
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->lx:Z

    return v0
.end method

.method public isPushEchoingEbabled()Z
    .locals 1

    .prologue
    .line 1634
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->lD:Z

    return v0
.end method

.method public isSharing()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1651
    iget-object v1, p0, Lcom/glympse/android/lib/br;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GConfigPrivate;->isSharingLocation()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getHistoryManager()Lcom/glympse/android/api/GHistoryManager;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/glympse/android/api/GHistoryManager;->anyActive(Z)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    move v1, v0

    .line 1652
    :goto_0
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->mi:Z

    if-eq v1, v0, :cond_0

    .line 1655
    iput-boolean v1, p0, Lcom/glympse/android/lib/br;->mi:Z

    .line 1658
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->mi:Z

    if-eqz v0, :cond_2

    const/16 v0, 0x2000

    move v2, v0

    .line 1659
    :goto_1
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GGlympse;

    const v3, 0x10002

    const/4 v4, 0x0

    invoke-virtual {p0, v0, v3, v2, v4}, Lcom/glympse/android/lib/br;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 1661
    :cond_0
    return v1

    :cond_1
    move v1, v0

    .line 1651
    goto :goto_0

    .line 1658
    :cond_2
    const/16 v0, 0x4000

    move v2, v0

    goto :goto_1
.end method

.method public isSharingSiblings()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1666
    iget-boolean v1, p0, Lcom/glympse/android/lib/br;->lc:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getHistoryManager()Lcom/glympse/android/api/GHistoryManager;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/glympse/android/api/GHistoryManager;->anyActive(Z)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSmsScrapingEnabled()Z
    .locals 1

    .prologue
    .line 1321
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->lz:Z

    return v0
.end method

.method public isStarted()Z
    .locals 1

    .prologue
    .line 594
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    return v0
.end method

.method public login(Lcom/glympse/android/core/GPrimitive;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 383
    iget-boolean v1, p0, Lcom/glympse/android/lib/br;->mh:Z

    if-eqz v1, :cond_1

    .line 399
    :cond_0
    :goto_0
    return v0

    .line 390
    :cond_1
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->hasUserAccount()Z

    move-result v1

    if-nez v1, :cond_0

    .line 397
    iput-object p1, p0, Lcom/glympse/android/lib/br;->lB:Lcom/glympse/android/core/GPrimitive;

    .line 399
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public logout()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 405
    iget-boolean v1, p0, Lcom/glympse/android/lib/br;->F:Z

    if-eqz v1, :cond_0

    .line 407
    const/4 v0, 0x0

    .line 427
    :goto_0
    return v0

    .line 411
    :cond_0
    iget-object v1, p0, Lcom/glympse/android/lib/br;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GConfigPrivate;->wipeAccounts()V

    .line 414
    iget-object v1, p0, Lcom/glympse/android/lib/br;->iu:Lcom/glympse/android/hal/GContextHolder;

    invoke-interface {v1}, Lcom/glympse/android/hal/GContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2, v0}, Lcom/glympse/android/hal/HalFactory;->openDirectory(Landroid/content/Context;Ljava/lang/String;Z)Lcom/glympse/android/hal/GDirectory;

    move-result-object v1

    .line 415
    if-eqz v1, :cond_1

    .line 417
    iget-object v2, p0, Lcom/glympse/android/lib/br;->iv:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/glympse/android/lib/x;->a(Lcom/glympse/android/hal/GDirectory;Ljava/lang/String;)V

    .line 418
    iget-object v2, p0, Lcom/glympse/android/lib/br;->iv:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/glympse/android/lib/gf;->b(Lcom/glympse/android/hal/GDirectory;Ljava/lang/String;)V

    .line 419
    iget-object v2, p0, Lcom/glympse/android/lib/br;->iv:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/glympse/android/lib/ce;->b(Lcom/glympse/android/hal/GDirectory;Ljava/lang/String;)V

    .line 420
    iget-object v2, p0, Lcom/glympse/android/lib/br;->iv:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/glympse/android/lib/cn;->b(Lcom/glympse/android/hal/GDirectory;Ljava/lang/String;)V

    .line 421
    iget-object v2, p0, Lcom/glympse/android/lib/br;->iv:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/glympse/android/lib/bb;->b(Lcom/glympse/android/hal/GDirectory;Ljava/lang/String;)V

    .line 425
    :cond_1
    iget-object v1, p0, Lcom/glympse/android/lib/br;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    iget-object v2, p0, Lcom/glympse/android/lib/br;->iu:Lcom/glympse/android/hal/GContextHolder;

    iget-object v3, p0, Lcom/glympse/android/lib/br;->iv:Ljava/lang/String;

    iget-object v4, p0, Lcom/glympse/android/lib/br;->f:Ljava/lang/String;

    iget-object v5, p0, Lcom/glympse/android/lib/br;->fW:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/glympse/android/lib/GConfigPrivate;->load(Lcom/glympse/android/hal/GContextHolder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public okToPost()Z
    .locals 2

    .prologue
    .line 1671
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->isSharing()Z

    move-result v0

    .line 1675
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getLocationManagerPrivate()Lcom/glympse/android/lib/GLocationManagerPrivate;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GLocationManagerPrivate;->startStopLocation(Z)V

    .line 1678
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getBatteryManagerPrivate()Lcom/glympse/android/lib/GBatteryManagerPrivate;

    move-result-object v1

    invoke-interface {v1}, Lcom/glympse/android/lib/GBatteryManagerPrivate;->setKeepAwake()V

    .line 1683
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getBatteryManager()Lcom/glympse/android/api/GBatteryManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/glympse/android/api/GBatteryManager;->isBatteryOk()Z

    move-result v1

    if-eqz v1, :cond_1

    if-nez v0, :cond_0

    .line 1684
    invoke-direct {p0}, Lcom/glympse/android/lib/br;->isWatching()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->isSharingSiblings()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/br;->lI:Lcom/glympse/android/lib/GServerPost;

    invoke-interface {v0}, Lcom/glympse/android/lib/GServerPost;->haveDataToPost()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 1682
    :goto_0
    return v0

    .line 1684
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onLowMemory()V
    .locals 1

    .prologue
    .line 2071
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-nez v0, :cond_0

    .line 2078
    :goto_0
    return-void

    .line 2077
    :cond_0
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getImageCachePrivate()Lcom/glympse/android/lib/GImageCachePrivate;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/lib/GImageCachePrivate;->onLowMemory()V

    goto :goto_0
.end method

.method public openUrl(Ljava/lang/String;ILcom/glympse/android/api/GInvite;)Z
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const v10, 0x10002

    .line 1036
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v3

    .line 1162
    :goto_0
    return v0

    .line 1041
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[Glympse.openUrl] Message: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 1043
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GGlympse;

    .line 1046
    new-instance v5, Lcom/glympse/android/lib/UrlParser;

    invoke-direct {v5}, Lcom/glympse/android/lib/UrlParser;-><init>()V

    .line 1047
    iget-object v1, p0, Lcom/glympse/android/lib/br;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GConfigPrivate;->getSupportedServersAndSchemes()Lcom/glympse/android/core/GArray;

    move-result-object v1

    invoke-virtual {v5, p1, v1, v3}, Lcom/glympse/android/lib/UrlParser;->parseUrls(Ljava/lang/String;Lcom/glympse/android/core/GArray;Z)Z

    .line 1050
    invoke-virtual {v5}, Lcom/glympse/android/lib/UrlParser;->getInitialNickname()Ljava/lang/String;

    move-result-object v1

    .line 1051
    invoke-virtual {v5}, Lcom/glympse/android/lib/UrlParser;->getInitialAvatar()Ljava/lang/String;

    move-result-object v4

    .line 1052
    invoke-virtual {p0, v1, v4}, Lcom/glympse/android/lib/br;->applyInitialProfile(Ljava/lang/String;Ljava/lang/String;)V

    .line 1055
    invoke-virtual {v5}, Lcom/glympse/android/lib/UrlParser;->getServer()Ljava/lang/String;

    move-result-object v1

    .line 1056
    iget-object v4, p0, Lcom/glympse/android/lib/br;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v4, v1}, Lcom/glympse/android/lib/GConfigPrivate;->isServerSupported(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 1059
    invoke-virtual {v5}, Lcom/glympse/android/lib/UrlParser;->getInviteCodes()Lcom/glympse/android/core/GArray;

    move-result-object v6

    .line 1060
    if-eqz v6, :cond_4

    .line 1062
    invoke-interface {v6}, Lcom/glympse/android/core/GArray;->length()I

    move-result v7

    .line 1063
    if-lez v7, :cond_4

    .line 1065
    new-instance v8, Lcom/glympse/android/hal/GVector;

    invoke-direct {v8, v7}, Lcom/glympse/android/hal/GVector;-><init>(I)V

    move v4, v3

    .line 1066
    :goto_1
    if-ge v4, v7, :cond_3

    .line 1068
    invoke-interface {v6, v4}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1071
    invoke-virtual {p0, v1, p2, p3}, Lcom/glympse/android/lib/br;->decodeInvite(Ljava/lang/String;ILcom/glympse/android/api/GInvite;)Lcom/glympse/android/api/GEventSink;

    move-result-object v1

    .line 1072
    if-nez v1, :cond_2

    .line 1066
    :goto_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    .line 1078
    :cond_2
    invoke-virtual {v8, v1}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    goto :goto_2

    .line 1082
    :cond_3
    invoke-virtual {p0, v0, v10, v2, v8}, Lcom/glympse/android/lib/br;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 1087
    :cond_4
    invoke-virtual {v5}, Lcom/glympse/android/lib/UrlParser;->getPublicGroups()Lcom/glympse/android/core/GArray;

    move-result-object v4

    .line 1088
    if-eqz v4, :cond_7

    .line 1090
    invoke-interface {v4}, Lcom/glympse/android/core/GArray;->length()I

    move-result v6

    .line 1091
    if-lez v6, :cond_7

    .line 1093
    new-instance v7, Lcom/glympse/android/hal/GVector;

    invoke-direct {v7, v6}, Lcom/glympse/android/hal/GVector;-><init>(I)V

    .line 1094
    :goto_3
    if-ge v3, v6, :cond_6

    .line 1096
    invoke-interface {v4, v3}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1097
    and-int/lit8 v8, p2, 0x1

    if-eqz v8, :cond_5

    .line 1100
    new-instance v8, Lcom/glympse/android/lib/ca;

    invoke-direct {v8, v1, p3}, Lcom/glympse/android/lib/ca;-><init>(Ljava/lang/String;Lcom/glympse/android/api/GInvite;)V

    .line 1101
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getGroupManager()Lcom/glympse/android/api/GGroupManager;

    move-result-object v1

    const/16 v9, 0x9

    invoke-interface {v1, v0, v9, v2, v8}, Lcom/glympse/android/api/GGroupManager;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 1094
    :goto_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 1106
    :cond_5
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getGroupManager()Lcom/glympse/android/api/GGroupManager;

    move-result-object v8

    invoke-interface {v8, v1}, Lcom/glympse/android/api/GGroupManager;->viewGroup(Ljava/lang/String;)Lcom/glympse/android/api/GGroup;

    move-result-object v1

    .line 1109
    invoke-virtual {v7, v1}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    goto :goto_4

    .line 1114
    :cond_6
    const/16 v1, 0x8

    invoke-virtual {p0, v0, v10, v1, v7}, Lcom/glympse/android/lib/br;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 1125
    :cond_7
    :goto_5
    invoke-virtual {v5}, Lcom/glympse/android/lib/UrlParser;->getViewer()Ljava/lang/String;

    move-result-object v1

    .line 1126
    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 1128
    iget-object v3, p0, Lcom/glympse/android/lib/br;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v3, v1}, Lcom/glympse/android/lib/GConfigPrivate;->setViewerToken(Ljava/lang/String;)V

    .line 1132
    :cond_8
    invoke-virtual {v5}, Lcom/glympse/android/lib/UrlParser;->getTicket()Lcom/glympse/android/api/GTicket;

    move-result-object v1

    .line 1133
    if-eqz v1, :cond_9

    .line 1135
    const/4 v3, 0x2

    invoke-virtual {p0, v0, v10, v3, v1}, Lcom/glympse/android/lib/br;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 1139
    :cond_9
    invoke-virtual {v5}, Lcom/glympse/android/lib/UrlParser;->getScreen()Ljava/lang/String;

    move-result-object v1

    .line 1140
    if-eqz v1, :cond_a

    .line 1142
    const/4 v3, 0x4

    invoke-virtual {p0, v0, v10, v3, v1}, Lcom/glympse/android/lib/br;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 1146
    :cond_a
    invoke-virtual {v5}, Lcom/glympse/android/lib/UrlParser;->getLogLevel()Ljava/lang/String;

    move-result-object v1

    .line 1147
    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_b

    .line 1150
    iget-object v3, p0, Lcom/glympse/android/lib/br;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v3, v1}, Lcom/glympse/android/lib/GConfigPrivate;->setFileLevel(Ljava/lang/String;)V

    .line 1151
    iget-object v3, p0, Lcom/glympse/android/lib/br;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v3, v1}, Lcom/glympse/android/lib/GConfigPrivate;->setDebugLevel(Ljava/lang/String;)V

    .line 1152
    iget-object v1, p0, Lcom/glympse/android/lib/br;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GConfigPrivate;->save()V

    .line 1155
    iget-object v1, p0, Lcom/glympse/android/lib/br;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-static {v1}, Lcom/glympse/android/lib/Debug;->updateLevels(Lcom/glympse/android/lib/GConfigPrivate;)V

    .line 1158
    const-wide/16 v4, 0x5

    iget-object v1, p0, Lcom/glympse/android/lib/br;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GConfigPrivate;->getFileLevel()J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-lez v1, :cond_d

    const/16 v1, 0x80

    .line 1159
    :goto_6
    const/4 v3, 0x0

    invoke-virtual {p0, v0, v10, v1, v3}, Lcom/glympse/android/lib/br;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    :cond_b
    move v0, v2

    .line 1162
    goto/16 :goto_0

    .line 1121
    :cond_c
    const v3, 0x8000

    invoke-virtual {p0, v0, v10, v3, v1}, Lcom/glympse/android/lib/br;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    goto :goto_5

    .line 1158
    :cond_d
    const/16 v1, 0x100

    goto :goto_6
.end method

.method public overrideDebugLoggingLevel(I)V
    .locals 4

    .prologue
    .line 1875
    const/4 v0, 0x1

    if-lt p1, v0, :cond_0

    const/4 v0, 0x7

    if-le p1, v0, :cond_1

    .line 1888
    :cond_0
    :goto_0
    return-void

    .line 1883
    :cond_1
    invoke-static {p1}, Lcom/glympse/android/lib/Debug;->overrideDebugLoggingLevel(I)V

    .line 1886
    iget-object v0, p0, Lcom/glympse/android/lib/br;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GConfigPrivate;->getContents()Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    const-string v1, "g.dbgLog"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    int-to-long v2, p1

    invoke-interface {v0, v1, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 1887
    iget-object v0, p0, Lcom/glympse/android/lib/br;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GConfigPrivate;->save()V

    goto :goto_0
.end method

.method public overrideFileLoggingLevel(I)V
    .locals 4

    .prologue
    .line 1898
    const/4 v0, 0x1

    if-lt p1, v0, :cond_0

    const/4 v0, 0x7

    if-le p1, v0, :cond_1

    .line 1911
    :cond_0
    :goto_0
    return-void

    .line 1906
    :cond_1
    invoke-static {p1}, Lcom/glympse/android/lib/Debug;->overrideFileLoggingLevel(I)V

    .line 1909
    iget-object v0, p0, Lcom/glympse/android/lib/br;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GConfigPrivate;->getContents()Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    const-string v1, "g.fileLog"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    int-to-long v2, p1

    invoke-interface {v0, v1, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 1910
    iget-object v0, p0, Lcom/glympse/android/lib/br;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GConfigPrivate;->save()V

    goto :goto_0
.end method

.method public overrideLoggingLevels(II)V
    .locals 2

    .prologue
    const/4 v1, 0x7

    const/4 v0, 0x1

    .line 1367
    if-lt p1, v0, :cond_0

    if-gt p1, v1, :cond_0

    if-lt p2, v0, :cond_0

    if-le p2, v1, :cond_1

    .line 1375
    :cond_0
    :goto_0
    return-void

    .line 1374
    :cond_1
    invoke-static {p1, p2}, Lcom/glympse/android/lib/Debug;->overrideLoggingLevels(II)V

    goto :goto_0
.end method

.method public registerDeviceToken(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1565
    invoke-static {}, Lcom/glympse/android/hal/Platform;->getPushType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/glympse/android/lib/br;->registerDeviceToken(Ljava/lang/String;Ljava/lang/String;)V

    .line 1566
    return-void
.end method

.method public registerDeviceToken(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1571
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-eqz v0, :cond_0

    invoke-static {p2}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1580
    :cond_0
    :goto_0
    return-void

    .line 1576
    :cond_1
    const/4 v0, 0x2

    const-string v1, "[Glympse.registerDeviceToken]"

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 1579
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lI:Lcom/glympse/android/lib/GServerPost;

    new-instance v2, Lcom/glympse/android/lib/et;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct {v2, v0, p1, p2}, Lcom/glympse/android/lib/et;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-interface {v1, v2, v0}, Lcom/glympse/android/lib/GServerPost;->invokeEndpoint(Lcom/glympse/android/lib/GApiEndpoint;Z)V

    goto :goto_0
.end method

.method public removeListener(Lcom/glympse/android/api/GEventListener;)Z
    .locals 1

    .prologue
    .line 2166
    iget-object v0, p0, Lcom/glympse/android/lib/br;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1}, Lcom/glympse/android/lib/CommonSink;->removeListener(Lcom/glympse/android/api/GEventListener;)Z

    move-result v0

    return v0
.end method

.method public requestTicket(Lcom/glympse/android/api/GTicket;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1253
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-nez v0, :cond_1

    .line 1277
    :cond_0
    :goto_0
    return-void

    .line 1258
    :cond_1
    invoke-interface {p1}, Lcom/glympse/android/api/GTicket;->getInvites()Lcom/glympse/android/core/GArray;

    move-result-object v0

    .line 1259
    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/glympse/android/core/GArray;->length()I

    move-result v1

    if-ne v1, v3, :cond_2

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GInvite;

    move-object v1, v0

    .line 1261
    :goto_1
    if-eqz p1, :cond_0

    if-eqz v1, :cond_0

    .line 1262
    invoke-interface {p1}, Lcom/glympse/android/api/GTicket;->getState()I

    move-result v0

    if-ne v3, v0, :cond_0

    .line 1268
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lu:Ljava/lang/String;

    invoke-interface {v1, v0}, Lcom/glympse/android/api/GInvite;->applyBrand(Ljava/lang/String;)V

    .line 1271
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getRecipientsManager()Lcom/glympse/android/lib/GRecipientsManager;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GRecipientsManager;->addRecipient(Lcom/glympse/android/api/GInvite;)V

    .line 1274
    new-instance v2, Lcom/glympse/android/lib/ev;

    .line 1275
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GGlympsePrivate;

    check-cast p1, Lcom/glympse/android/lib/GTicketPrivate;

    check-cast v1, Lcom/glympse/android/lib/GInvitePrivate;

    invoke-direct {v2, v0, p1, v1}, Lcom/glympse/android/lib/ev;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/GTicketPrivate;Lcom/glympse/android/lib/GInvitePrivate;)V

    .line 1276
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lI:Lcom/glympse/android/lib/GServerPost;

    invoke-interface {v0, v2, v3}, Lcom/glympse/android/lib/GServerPost;->invokeEndpoint(Lcom/glympse/android/lib/GApiEndpoint;Z)V

    goto :goto_0

    .line 1259
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public sendTicket(Lcom/glympse/android/api/GTicket;)Z
    .locals 1

    .prologue
    .line 1242
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-nez v0, :cond_0

    .line 1244
    const/4 v0, 0x0

    .line 1248
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getHistoryManagerPrivate()Lcom/glympse/android/lib/GHistoryManagerPrivate;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/glympse/android/lib/GHistoryManagerPrivate;->sendTicket(Lcom/glympse/android/api/GTicket;)Z

    move-result v0

    goto :goto_0
.end method

.method public setActive(Z)I
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v0, -0x1

    const/4 v1, 0x1

    .line 599
    iget-boolean v2, p0, Lcom/glympse/android/lib/br;->F:Z

    if-nez v2, :cond_0

    .line 647
    :goto_0
    return v0

    .line 605
    :cond_0
    iget v2, p0, Lcom/glympse/android/lib/br;->lF:I

    if-eqz p1, :cond_1

    move v0, v1

    :cond_1
    add-int/2addr v0, v2

    iput v0, p0, Lcom/glympse/android/lib/br;->lF:I

    .line 608
    iget v0, p0, Lcom/glympse/android/lib/br;->lF:I

    if-gtz v0, :cond_4

    .line 611
    iput v3, p0, Lcom/glympse/android/lib/br;->lF:I

    .line 615
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->lc:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/glympse/android/lib/br;->lG:Ljava/lang/Runnable;

    if-nez v0, :cond_2

    .line 617
    invoke-static {}, Lcom/glympse/android/hal/Concurrent;->getBackgroundMode()I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_3

    .line 619
    new-instance v2, Lcom/glympse/android/lib/br$a;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/br;

    invoke-direct {v2, v0}, Lcom/glympse/android/lib/br$a;-><init>(Lcom/glympse/android/lib/br;)V

    iput-object v2, p0, Lcom/glympse/android/lib/br;->lG:Ljava/lang/Runnable;

    .line 620
    iget-object v0, p0, Lcom/glympse/android/lib/br;->_handler:Lcom/glympse/android/core/GHandler;

    iget-object v2, p0, Lcom/glympse/android/lib/br;->lG:Ljava/lang/Runnable;

    const-wide/16 v4, 0x7d0

    invoke-interface {v0, v2, v4, v5}, Lcom/glympse/android/core/GHandler;->postDelayed(Ljava/lang/Runnable;J)V

    .line 645
    :cond_2
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Glympse.setActive] Active references: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/glympse/android/lib/br;->lF:I

    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/glympse/android/hal/Helpers;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 647
    iget v0, p0, Lcom/glympse/android/lib/br;->lF:I

    goto :goto_0

    .line 624
    :cond_3
    invoke-direct {p0, v3}, Lcom/glympse/android/lib/br;->e(Z)V

    goto :goto_1

    .line 632
    :cond_4
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lG:Ljava/lang/Runnable;

    if-eqz v0, :cond_5

    .line 634
    iget-object v0, p0, Lcom/glympse/android/lib/br;->_handler:Lcom/glympse/android/core/GHandler;

    iget-object v2, p0, Lcom/glympse/android/lib/br;->lG:Ljava/lang/Runnable;

    invoke-interface {v0, v2}, Lcom/glympse/android/core/GHandler;->cancel(Ljava/lang/Runnable;)V

    .line 635
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/br;->lG:Ljava/lang/Runnable;

    .line 639
    :cond_5
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->lc:Z

    if-nez v0, :cond_2

    .line 641
    invoke-direct {p0, v1}, Lcom/glympse/android/lib/br;->e(Z)V

    goto :goto_1
.end method

.method public setBrand(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1467
    invoke-static {p1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1469
    iput-object p1, p0, Lcom/glympse/android/lib/br;->lu:Ljava/lang/String;

    .line 1475
    :goto_0
    return-void

    .line 1473
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/br;->lu:Ljava/lang/String;

    goto :goto_0
.end method

.method public setBuildName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1924
    sput-object p1, Lcom/glympse/android/lib/StaticConfig;->BUILD_NAME:Ljava/lang/String;

    .line 1925
    return-void
.end method

.method public setEtaMode(I)V
    .locals 1

    .prologue
    .line 1326
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-eqz v0, :cond_0

    .line 1331
    :goto_0
    return-void

    .line 1330
    :cond_0
    iput p1, p0, Lcom/glympse/android/lib/br;->lw:I

    goto :goto_0
.end method

.method public setExpireOnArrival(I)V
    .locals 1

    .prologue
    .line 1340
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-eqz v0, :cond_0

    .line 1345
    :goto_0
    return-void

    .line 1344
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/br;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v0, p1}, Lcom/glympse/android/lib/GConfigPrivate;->setExpireOnArrival(I)V

    goto :goto_0
.end method

.method public setHandler(Lcom/glympse/android/core/GHandler;)V
    .locals 1

    .prologue
    .line 324
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    .line 331
    :cond_0
    :goto_0
    return-void

    .line 330
    :cond_1
    iput-object p1, p0, Lcom/glympse/android/lib/br;->_handler:Lcom/glympse/android/core/GHandler;

    goto :goto_0
.end method

.method public setHistoryLookback(J)V
    .locals 3

    .prologue
    .line 1409
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 1412
    const-wide/16 v0, -0x1

    cmp-long v0, v0, p1

    if-eqz v0, :cond_0

    .line 1419
    :goto_0
    return-void

    .line 1418
    :cond_0
    iput-wide p1, p0, Lcom/glympse/android/lib/br;->ly:J

    goto :goto_0
.end method

.method public setRestoreHistory(Z)V
    .locals 1

    .prologue
    .line 1393
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-eqz v0, :cond_0

    .line 1399
    :goto_0
    return-void

    .line 1398
    :cond_0
    iput-boolean p1, p0, Lcom/glympse/android/lib/br;->lx:Z

    goto :goto_0
.end method

.method public setSmsSendMode(I)V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 1287
    const/4 v0, 0x1

    if-ne v0, p1, :cond_1

    .line 1289
    iput p1, p0, Lcom/glympse/android/lib/br;->lv:I

    .line 1303
    :cond_0
    :goto_0
    return-void

    .line 1291
    :cond_1
    if-ne v1, p1, :cond_2

    .line 1293
    iput p1, p0, Lcom/glympse/android/lib/br;->lv:I

    goto :goto_0

    .line 1295
    :cond_2
    const/4 v0, 0x2

    if-ne v0, p1, :cond_0

    .line 1298
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->canDeviceSendSms()I

    move-result v0

    if-eq v1, v0, :cond_0

    .line 1300
    iput p1, p0, Lcom/glympse/android/lib/br;->lv:I

    goto :goto_0
.end method

.method public showDebugView()V
    .locals 1

    .prologue
    .line 1360
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/glympse/android/hal/DebugBase;->showDebugView(Ljava/lang/Object;)V

    .line 1362
    return-void
.end method

.method public start()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 432
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->mh:Z

    if-eqz v0, :cond_1

    .line 508
    :cond_0
    :goto_0
    return-void

    .line 437
    :cond_1
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GGlympsePrivate;

    .line 440
    invoke-direct {p0, v0}, Lcom/glympse/android/lib/br;->d(Lcom/glympse/android/lib/GGlympsePrivate;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 446
    iput-boolean v4, p0, Lcom/glympse/android/lib/br;->F:Z

    .line 447
    iput-boolean v4, p0, Lcom/glympse/android/lib/br;->mh:Z

    .line 448
    const/4 v1, 0x0

    iput v1, p0, Lcom/glympse/android/lib/br;->lF:I

    .line 451
    invoke-static {v0}, Lcom/glympse/android/lib/Debug;->start(Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 454
    iget-object v1, p0, Lcom/glympse/android/lib/br;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GConfigPrivate;->getCurrentAccount()Lcom/glympse/android/core/GPrimitive;

    move-result-object v1

    .line 458
    new-instance v2, Lcom/glympse/android/lib/cj;

    iget-object v3, p0, Lcom/glympse/android/lib/br;->_handler:Lcom/glympse/android/core/GHandler;

    invoke-direct {v2, v3}, Lcom/glympse/android/lib/cj;-><init>(Lcom/glympse/android/core/GHandler;)V

    iput-object v2, p0, Lcom/glympse/android/lib/br;->lY:Lcom/glympse/android/lib/GHandlerManager;

    .line 461
    new-instance v2, Lcom/glympse/android/lib/df;

    iget-object v3, p0, Lcom/glympse/android/lib/br;->_handler:Lcom/glympse/android/core/GHandler;

    invoke-direct {v2, v3}, Lcom/glympse/android/lib/df;-><init>(Lcom/glympse/android/core/GHandler;)V

    iput-object v2, p0, Lcom/glympse/android/lib/br;->_jobQueue:Lcom/glympse/android/lib/GJobQueue;

    .line 462
    iget-object v2, p0, Lcom/glympse/android/lib/br;->_jobQueue:Lcom/glympse/android/lib/GJobQueue;

    const/4 v3, 0x2

    invoke-interface {v2, v3}, Lcom/glympse/android/lib/GJobQueue;->start(I)Z

    .line 465
    iget-object v2, p0, Lcom/glympse/android/lib/br;->lH:Lcom/glympse/android/hal/GServiceWrapper;

    invoke-interface {v2, v0}, Lcom/glympse/android/hal/GServiceWrapper;->start(Lcom/glympse/android/api/GGlympse;)V

    .line 466
    iget-object v2, p0, Lcom/glympse/android/lib/br;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v2, v0}, Lcom/glympse/android/lib/GConfigPrivate;->start(Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 467
    iget-object v2, p0, Lcom/glympse/android/lib/br;->lI:Lcom/glympse/android/lib/GServerPost;

    invoke-interface {v2, v0, v1}, Lcom/glympse/android/lib/GServerPost;->start(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/core/GPrimitive;)V

    .line 468
    iget-object v1, p0, Lcom/glympse/android/lib/br;->mb:Lcom/glympse/android/lib/GMessageCenter;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GMessageCenter;->start(Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 469
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lN:Lcom/glympse/android/lib/GBatteryManagerPrivate;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GBatteryManagerPrivate;->start(Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 470
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lW:Lcom/glympse/android/lib/GDiagnosticsManager;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GDiagnosticsManager;->start(Lcom/glympse/android/api/GGlympse;)V

    .line 471
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lQ:Lcom/glympse/android/lib/GHandoffManagerPrivate;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GHandoffManagerPrivate;->start(Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 472
    iget-object v1, p0, Lcom/glympse/android/lib/br;->mg:Lcom/glympse/android/lib/GTriggersManagerPrivate;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GTriggersManagerPrivate;->start(Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 475
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lT:Lcom/glympse/android/lib/GRecipientsManager;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/glympse/android/lib/br;->lT:Lcom/glympse/android/lib/GRecipientsManager;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GRecipientsManager;->start(Lcom/glympse/android/api/GGlympse;)V

    .line 476
    :cond_2
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lS:Lcom/glympse/android/lib/GFavoritesManagerPrivate;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/glympse/android/lib/br;->lS:Lcom/glympse/android/lib/GFavoritesManagerPrivate;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GFavoritesManagerPrivate;->start(Lcom/glympse/android/api/GGlympse;)V

    .line 477
    :cond_3
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lU:Lcom/glympse/android/lib/GMessagesManager;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/glympse/android/lib/br;->lU:Lcom/glympse/android/lib/GMessagesManager;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GMessagesManager;->start(Lcom/glympse/android/api/GGlympse;)V

    .line 478
    :cond_4
    iget-object v1, p0, Lcom/glympse/android/lib/br;->mf:Lcom/glympse/android/lib/GWifiManager;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/glympse/android/lib/br;->mf:Lcom/glympse/android/lib/GWifiManager;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GWifiManager;->start(Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 479
    :cond_5
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lV:Lcom/glympse/android/lib/GPlacesManager;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/glympse/android/lib/br;->lV:Lcom/glympse/android/lib/GPlacesManager;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GPlacesManager;->start(Lcom/glympse/android/api/GGlympse;)V

    .line 480
    :cond_6
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lJ:Lcom/glympse/android/lib/GTicketProtocol;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/glympse/android/lib/br;->lJ:Lcom/glympse/android/lib/GTicketProtocol;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GTicketProtocol;->start(Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 481
    :cond_7
    iget-object v1, p0, Lcom/glympse/android/lib/br;->id:Lcom/glympse/android/lib/GPairingManagerPrivate;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/glympse/android/lib/br;->id:Lcom/glympse/android/lib/GPairingManagerPrivate;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GPairingManagerPrivate;->start(Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 484
    :cond_8
    invoke-static {v0}, Lcom/glympse/android/lib/p;->b(Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 487
    iget-object v1, p0, Lcom/glympse/android/lib/br;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GConfigPrivate;->areAccountsLinked()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 491
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getLinkedAccountsManager()Lcom/glympse/android/api/GLinkedAccountsManager;

    .line 495
    :cond_9
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lW:Lcom/glympse/android/lib/GDiagnosticsManager;

    invoke-static {v1}, Lcom/glympse/android/lib/av;->a(Lcom/glympse/android/lib/GDiagnosticsManager;)V

    .line 498
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lI:Lcom/glympse/android/lib/GServerPost;

    invoke-interface {v1}, Lcom/glympse/android/lib/GServerPost;->authenticate()V

    .line 500
    const/16 v1, 0x100

    .line 501
    iget-object v2, p0, Lcom/glympse/android/lib/br;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v2}, Lcom/glympse/android/lib/GConfigPrivate;->isFirstLaunch()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 503
    const v1, 0x1000100

    .line 507
    :cond_a
    const/4 v2, 0x0

    invoke-virtual {p0, v0, v4, v1, v2}, Lcom/glympse/android/lib/br;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public startStopLocation()V
    .locals 2

    .prologue
    .line 1689
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getLocationManagerPrivate()Lcom/glympse/android/lib/GLocationManagerPrivate;

    move-result-object v0

    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->isSharing()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GLocationManagerPrivate;->startStopLocation(Z)V

    .line 1690
    return-void
.end method

.method public stop()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 512
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-nez v0, :cond_0

    .line 585
    :goto_0
    return-void

    .line 517
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lW:Lcom/glympse/android/lib/GDiagnosticsManager;

    const-string v1, "platform"

    .line 518
    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "state"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "stopped"

    invoke-static {v3}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 517
    invoke-interface {v0, v1, v2, v3}, Lcom/glympse/android/lib/GDiagnosticsManager;->logEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    iput-boolean v5, p0, Lcom/glympse/android/lib/br;->F:Z

    .line 522
    iput v5, p0, Lcom/glympse/android/lib/br;->lF:I

    .line 524
    iget-object v0, p0, Lcom/glympse/android/lib/br;->lG:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    .line 526
    iget-object v0, p0, Lcom/glympse/android/lib/br;->_handler:Lcom/glympse/android/core/GHandler;

    iget-object v1, p0, Lcom/glympse/android/lib/br;->lG:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GHandler;->cancel(Ljava/lang/Runnable;)V

    .line 527
    iput-object v4, p0, Lcom/glympse/android/lib/br;->lG:Ljava/lang/Runnable;

    .line 531
    :cond_1
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GGlympsePrivate;

    .line 532
    const/4 v1, 0x1

    const/16 v2, 0x200

    invoke-virtual {p0, v0, v1, v2, v4}, Lcom/glympse/android/lib/br;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 542
    iget-object v1, p0, Lcom/glympse/android/lib/br;->_jobQueue:Lcom/glympse/android/lib/GJobQueue;

    invoke-static {}, Lcom/glympse/android/lib/StaticConfig;->canAbortNetworkRequest()Z

    move-result v2

    invoke-interface {v1, v2}, Lcom/glympse/android/lib/GJobQueue;->stop(Z)V

    .line 543
    iput-object v4, p0, Lcom/glympse/android/lib/br;->_jobQueue:Lcom/glympse/android/lib/GJobQueue;

    .line 547
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lW:Lcom/glympse/android/lib/GDiagnosticsManager;

    invoke-static {v1}, Lcom/glympse/android/lib/av;->b(Lcom/glympse/android/lib/GDiagnosticsManager;)V

    .line 550
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lY:Lcom/glympse/android/lib/GHandlerManager;

    invoke-interface {v1}, Lcom/glympse/android/lib/GHandlerManager;->stop()V

    .line 551
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lH:Lcom/glympse/android/hal/GServiceWrapper;

    invoke-interface {v1}, Lcom/glympse/android/hal/GServiceWrapper;->stop()V

    .line 552
    iget-object v1, p0, Lcom/glympse/android/lib/br;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GConfigPrivate;->stop()V

    .line 553
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lI:Lcom/glympse/android/lib/GServerPost;

    invoke-interface {v1}, Lcom/glympse/android/lib/GServerPost;->stop()V

    .line 554
    iget-object v1, p0, Lcom/glympse/android/lib/br;->mb:Lcom/glympse/android/lib/GMessageCenter;

    invoke-interface {v1}, Lcom/glympse/android/lib/GMessageCenter;->stop()V

    .line 555
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lN:Lcom/glympse/android/lib/GBatteryManagerPrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GBatteryManagerPrivate;->stop()V

    .line 556
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lW:Lcom/glympse/android/lib/GDiagnosticsManager;

    invoke-interface {v1}, Lcom/glympse/android/lib/GDiagnosticsManager;->stop()V

    .line 557
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lQ:Lcom/glympse/android/lib/GHandoffManagerPrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GHandoffManagerPrivate;->stop()V

    .line 558
    iget-object v1, p0, Lcom/glympse/android/lib/br;->mg:Lcom/glympse/android/lib/GTriggersManagerPrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GTriggersManagerPrivate;->stop()V

    .line 561
    iget-object v1, p0, Lcom/glympse/android/lib/br;->mc:Lcom/glympse/android/lib/GNotificationCenter;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/glympse/android/lib/br;->mc:Lcom/glympse/android/lib/GNotificationCenter;

    invoke-interface {v1}, Lcom/glympse/android/lib/GNotificationCenter;->stop()V

    .line 562
    :cond_2
    iget-object v1, p0, Lcom/glympse/android/lib/br;->md:Lcom/glympse/android/lib/GPlaceSearchEnginePrivate;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/glympse/android/lib/br;->md:Lcom/glympse/android/lib/GPlaceSearchEnginePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GPlaceSearchEnginePrivate;->stop()V

    .line 563
    :cond_3
    iget-object v1, p0, Lcom/glympse/android/lib/br;->hC:Lcom/glympse/android/lib/GImageCachePrivate;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/glympse/android/lib/br;->hC:Lcom/glympse/android/lib/GImageCachePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GImageCachePrivate;->stop()V

    .line 564
    :cond_4
    iget-object v1, p0, Lcom/glympse/android/lib/br;->ma:Lcom/glympse/android/lib/GAvatarUploader;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/glympse/android/lib/br;->ma:Lcom/glympse/android/lib/GAvatarUploader;

    invoke-interface {v1}, Lcom/glympse/android/lib/GAvatarUploader;->stop()V

    .line 565
    :cond_5
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lJ:Lcom/glympse/android/lib/GTicketProtocol;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/glympse/android/lib/br;->lJ:Lcom/glympse/android/lib/GTicketProtocol;

    invoke-interface {v1}, Lcom/glympse/android/lib/GTicketProtocol;->stop()V

    .line 566
    :cond_6
    iget-object v1, p0, Lcom/glympse/android/lib/br;->mf:Lcom/glympse/android/lib/GWifiManager;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/glympse/android/lib/br;->mf:Lcom/glympse/android/lib/GWifiManager;

    invoke-interface {v1}, Lcom/glympse/android/lib/GWifiManager;->stop()V

    .line 567
    :cond_7
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lO:Lcom/glympse/android/lib/GLinkedAccountsManagerPrivate;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/glympse/android/lib/br;->lO:Lcom/glympse/android/lib/GLinkedAccountsManagerPrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GLinkedAccountsManagerPrivate;->stop()V

    .line 568
    :cond_8
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lP:Lcom/glympse/android/lib/GApplicationsManagerPrivate;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/glympse/android/lib/br;->lP:Lcom/glympse/android/lib/GApplicationsManagerPrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GApplicationsManagerPrivate;->stop()V

    .line 569
    :cond_9
    iget-object v1, p0, Lcom/glympse/android/lib/br;->me:Lcom/glympse/android/lib/GDirectionsManagerPrivate;

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/glympse/android/lib/br;->me:Lcom/glympse/android/lib/GDirectionsManagerPrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GDirectionsManagerPrivate;->stop()V

    .line 570
    :cond_a
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lS:Lcom/glympse/android/lib/GFavoritesManagerPrivate;

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/glympse/android/lib/br;->lS:Lcom/glympse/android/lib/GFavoritesManagerPrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GFavoritesManagerPrivate;->stop()V

    .line 571
    :cond_b
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lT:Lcom/glympse/android/lib/GRecipientsManager;

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/glympse/android/lib/br;->lT:Lcom/glympse/android/lib/GRecipientsManager;

    invoke-interface {v1}, Lcom/glympse/android/lib/GRecipientsManager;->stop()V

    .line 572
    :cond_c
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lU:Lcom/glympse/android/lib/GMessagesManager;

    if-eqz v1, :cond_d

    iget-object v1, p0, Lcom/glympse/android/lib/br;->lU:Lcom/glympse/android/lib/GMessagesManager;

    invoke-interface {v1}, Lcom/glympse/android/lib/GMessagesManager;->stop()V

    .line 573
    :cond_d
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lV:Lcom/glympse/android/lib/GPlacesManager;

    if-eqz v1, :cond_e

    iget-object v1, p0, Lcom/glympse/android/lib/br;->lV:Lcom/glympse/android/lib/GPlacesManager;

    invoke-interface {v1}, Lcom/glympse/android/lib/GPlacesManager;->stop()V

    .line 574
    :cond_e
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lM:Lcom/glympse/android/lib/GNetworkManagerPrivate;

    if-eqz v1, :cond_f

    iget-object v1, p0, Lcom/glympse/android/lib/br;->lM:Lcom/glympse/android/lib/GNetworkManagerPrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GNetworkManagerPrivate;->stop()V

    .line 575
    :cond_f
    iget-object v1, p0, Lcom/glympse/android/lib/br;->hS:Lcom/glympse/android/lib/GLocationManagerPrivate;

    if-eqz v1, :cond_10

    iget-object v1, p0, Lcom/glympse/android/lib/br;->hS:Lcom/glympse/android/lib/GLocationManagerPrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GLocationManagerPrivate;->stop()V

    .line 576
    :cond_10
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lK:Lcom/glympse/android/lib/GUserManagerPrivate;

    if-eqz v1, :cond_11

    iget-object v1, p0, Lcom/glympse/android/lib/br;->lK:Lcom/glympse/android/lib/GUserManagerPrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GUserManagerPrivate;->stop()V

    .line 577
    :cond_11
    iget-object v1, p0, Lcom/glympse/android/lib/br;->hT:Lcom/glympse/android/lib/GHistoryManagerPrivate;

    if-eqz v1, :cond_12

    iget-object v1, p0, Lcom/glympse/android/lib/br;->hT:Lcom/glympse/android/lib/GHistoryManagerPrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GHistoryManagerPrivate;->stop()V

    .line 578
    :cond_12
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lL:Lcom/glympse/android/lib/GGroupManagerPrivate;

    if-eqz v1, :cond_13

    iget-object v1, p0, Lcom/glympse/android/lib/br;->lL:Lcom/glympse/android/lib/GGroupManagerPrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGroupManagerPrivate;->stop()V

    .line 581
    :cond_13
    iget-object v1, p0, Lcom/glympse/android/lib/br;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v1}, Lcom/glympse/android/lib/CommonSink;->removeAllListeners()Z

    .line 584
    invoke-static {v0}, Lcom/glympse/android/lib/Debug;->stop(Lcom/glympse/android/lib/GGlympsePrivate;)V

    goto/16 :goto_0
.end method

.method public unregisterDeviceToken()V
    .locals 1

    .prologue
    .line 1584
    invoke-static {}, Lcom/glympse/android/hal/Platform;->getPushType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/br;->unregisterDeviceToken(Ljava/lang/String;)V

    .line 1585
    return-void
.end method

.method public unregisterDeviceToken(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1590
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-nez v0, :cond_0

    .line 1599
    :goto_0
    return-void

    .line 1595
    :cond_0
    const/4 v0, 0x2

    const-string v1, "[Glympse.unregisterDeviceToken]"

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 1598
    iget-object v1, p0, Lcom/glympse/android/lib/br;->lI:Lcom/glympse/android/lib/GServerPost;

    new-instance v2, Lcom/glympse/android/lib/et;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GGlympsePrivate;

    const/4 v3, 0x0

    invoke-direct {v2, v0, p1, v3}, Lcom/glympse/android/lib/et;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-interface {v1, v2, v0}, Lcom/glympse/android/lib/GServerPost;->invokeEndpoint(Lcom/glympse/android/lib/GApiEndpoint;Z)V

    goto :goto_0
.end method

.method public verify()V
    .locals 2

    .prologue
    .line 1517
    iget-object v1, p0, Lcom/glympse/android/lib/br;->iu:Lcom/glympse/android/hal/GContextHolder;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GGlympse;

    invoke-interface {v1, v0}, Lcom/glympse/android/hal/GContextHolder;->verifyFullConfiguration(Lcom/glympse/android/api/GGlympse;)V

    .line 1518
    return-void
.end method

.method public viewTicket(Lcom/glympse/android/api/GUserTicket;)Lcom/glympse/android/api/GTicket;
    .locals 1

    .prologue
    .line 1231
    iget-boolean v0, p0, Lcom/glympse/android/lib/br;->F:Z

    if-nez v0, :cond_0

    .line 1233
    const/4 v0, 0x0

    .line 1237
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/glympse/android/lib/br;->getUserManagerPrivate()Lcom/glympse/android/lib/GUserManagerPrivate;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/glympse/android/lib/GUserManagerPrivate;->viewTicket(Lcom/glympse/android/api/GUserTicket;)Lcom/glympse/android/api/GTicket;

    move-result-object v0

    goto :goto_0
.end method
