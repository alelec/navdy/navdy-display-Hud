.class Lcom/glympse/android/lib/cp;
.super Ljava/lang/Object;
.source "HybridArrivalProvider.java"

# interfaces
.implements Lcom/glympse/android/lib/be;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/lib/cp$a;
    }
.end annotation


# instance fields
.field private cS:Lcom/glympse/android/api/GGlympse;

.field private hk:Lcom/glympse/android/lib/bd;

.field private nM:Lcom/glympse/android/hal/GHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GHashtable",
            "<",
            "Lcom/glympse/android/api/GTrigger;",
            "Lcom/glympse/android/api/GTicket;",
            ">;"
        }
    .end annotation
.end field

.field private nN:Lcom/glympse/android/hal/GHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GHashtable",
            "<",
            "Lcom/glympse/android/api/GTicket;",
            "Lcom/glympse/android/api/GGeoTrigger;",
            ">;"
        }
    .end annotation
.end field

.field private nO:Lcom/glympse/android/hal/GHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GHashtable",
            "<",
            "Lcom/glympse/android/api/GTicket;",
            "Lcom/glympse/android/api/GChronoTrigger;",
            ">;"
        }
    .end annotation
.end field

.field private nP:Lcom/glympse/android/hal/GHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GHashtable",
            "<",
            "Lcom/glympse/android/api/GTicket;",
            "Lcom/glympse/android/api/GEtaTrigger;",
            ">;"
        }
    .end annotation
.end field

.field private nQ:Lcom/glympse/android/api/GTriggersManager;

.field private nR:Z

.field private nS:J

.field private nT:J

.field private nU:Z

.field private nV:J

.field private nW:J

.field private nX:J

.field private nY:Lcom/glympse/android/lib/cp$a;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-boolean v0, p0, Lcom/glympse/android/lib/cp;->nR:Z

    .line 42
    iput-boolean v0, p0, Lcom/glympse/android/lib/cp;->nU:Z

    .line 43
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/glympse/android/lib/cp;->nX:J

    .line 44
    return-void
.end method

.method private a(Lcom/glympse/android/api/GTrigger;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 250
    iget-object v0, p0, Lcom/glympse/android/lib/cp;->nM:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GTicketPrivate;

    .line 251
    if-nez v0, :cond_1

    .line 297
    :cond_0
    :goto_0
    return-void

    .line 256
    :cond_1
    invoke-interface {p1}, Lcom/glympse/android/api/GTrigger;->getType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 260
    :pswitch_0
    check-cast p1, Lcom/glympse/android/api/GGeoTrigger;

    .line 261
    invoke-interface {p1}, Lcom/glympse/android/api/GGeoTrigger;->getTransition()I

    move-result v1

    if-ne v2, v1, :cond_2

    .line 263
    invoke-direct {p0, v0}, Lcom/glympse/android/lib/cp;->n(Lcom/glympse/android/lib/GTicketPrivate;)V

    goto :goto_0

    .line 265
    :cond_2
    invoke-interface {p1}, Lcom/glympse/android/api/GGeoTrigger;->getTransition()I

    move-result v1

    if-ne v3, v1, :cond_0

    .line 267
    invoke-direct {p0, v0}, Lcom/glympse/android/lib/cp;->o(Lcom/glympse/android/lib/GTicketPrivate;)V

    goto :goto_0

    .line 274
    :pswitch_1
    check-cast p1, Lcom/glympse/android/api/GChronoTrigger;

    .line 275
    iget-object v1, p0, Lcom/glympse/android/lib/cp;->nO:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v1, v0}, Lcom/glympse/android/hal/GHashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 277
    invoke-direct {p0, v0}, Lcom/glympse/android/lib/cp;->p(Lcom/glympse/android/lib/GTicketPrivate;)V

    goto :goto_0

    .line 284
    :pswitch_2
    check-cast p1, Lcom/glympse/android/api/GEtaTrigger;

    .line 285
    invoke-interface {p1}, Lcom/glympse/android/api/GEtaTrigger;->getTransition()I

    move-result v1

    if-ne v2, v1, :cond_3

    .line 287
    invoke-direct {p0, v0}, Lcom/glympse/android/lib/cp;->n(Lcom/glympse/android/lib/GTicketPrivate;)V

    goto :goto_0

    .line 289
    :cond_3
    invoke-interface {p1}, Lcom/glympse/android/api/GEtaTrigger;->getTransition()I

    move-result v1

    if-ne v3, v1, :cond_0

    .line 291
    invoke-direct {p0, v0}, Lcom/glympse/android/lib/cp;->o(Lcom/glympse/android/lib/GTicketPrivate;)V

    goto :goto_0

    .line 256
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic a(Lcom/glympse/android/lib/cp;Lcom/glympse/android/api/GTrigger;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/cp;->a(Lcom/glympse/android/api/GTrigger;)V

    return-void
.end method

.method private c(Lcom/glympse/android/api/GTicket;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 301
    invoke-static {}, Lcom/glympse/android/lib/Debug;->getLevel()I

    move-result v0

    if-gt v0, v1, :cond_0

    .line 303
    const-string v0, "[Ticket arrival triggered] Provider: HybridArrivalProvider "

    invoke-static {v1, v0}, Lcom/glympse/android/hal/Helpers;->log(ILjava/lang/String;)V

    .line 306
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/cp;->hk:Lcom/glympse/android/lib/bd;

    if-eqz v0, :cond_1

    .line 308
    iget-object v0, p0, Lcom/glympse/android/lib/cp;->hk:Lcom/glympse/android/lib/bd;

    invoke-interface {v0, p1}, Lcom/glympse/android/lib/bd;->c(Lcom/glympse/android/api/GTicket;)V

    .line 310
    :cond_1
    return-void
.end method

.method private l(Lcom/glympse/android/lib/GTicketPrivate;)V
    .locals 14

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 144
    iget-boolean v0, p0, Lcom/glympse/android/lib/cp;->nR:Z

    if-eqz v0, :cond_0

    .line 146
    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getDestination()Lcom/glympse/android/api/GPlace;

    move-result-object v0

    .line 147
    invoke-interface {v0}, Lcom/glympse/android/api/GPlace;->getLatitude()D

    move-result-wide v4

    invoke-interface {v0}, Lcom/glympse/android/api/GPlace;->getLongitude()D

    move-result-wide v8

    invoke-static {v4, v5, v8, v9}, Lcom/glympse/android/core/CoreFactory;->createLocation(DD)Lcom/glympse/android/core/GLocation;

    move-result-object v3

    .line 148
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "init_geo_"

    invoke-static {v4}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 149
    iget-wide v4, p0, Lcom/glympse/android/lib/cp;->nS:J

    long-to-double v4, v4

    invoke-static/range {v0 .. v6}, Lcom/glympse/android/api/GlympseFactory;->createGeoTrigger(Ljava/lang/String;ZLcom/glympse/android/api/GTicket;Lcom/glympse/android/core/GLatLng;DI)Lcom/glympse/android/api/GGeoTrigger;

    move-result-object v0

    .line 152
    iget-object v3, p0, Lcom/glympse/android/lib/cp;->nM:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v3, v0, p1}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    iget-object v3, p0, Lcom/glympse/android/lib/cp;->nN:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v3, p1, v0}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    iget-object v3, p0, Lcom/glympse/android/lib/cp;->nQ:Lcom/glympse/android/api/GTriggersManager;

    invoke-interface {v3, v0}, Lcom/glympse/android/api/GTriggersManager;->addLocalTrigger(Lcom/glympse/android/api/GTrigger;)V

    .line 157
    :cond_0
    iget-boolean v0, p0, Lcom/glympse/android/lib/cp;->nU:Z

    if-eqz v0, :cond_1

    .line 159
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "init_eta_"

    invoke-static {v3}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 160
    iget-wide v10, p0, Lcom/glympse/android/lib/cp;->nV:J

    move v8, v1

    move-object v9, v2

    move v12, v6

    move-object v13, p1

    invoke-static/range {v7 .. v13}, Lcom/glympse/android/api/GlympseFactory;->createEtaTrigger(Ljava/lang/String;ZLcom/glympse/android/api/GTicket;JILcom/glympse/android/api/GTicket;)Lcom/glympse/android/api/GEtaTrigger;

    move-result-object v0

    .line 163
    iget-object v1, p0, Lcom/glympse/android/lib/cp;->nM:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v1, v0, p1}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    iget-object v1, p0, Lcom/glympse/android/lib/cp;->nP:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v1, p1, v0}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 165
    iget-object v1, p0, Lcom/glympse/android/lib/cp;->nQ:Lcom/glympse/android/api/GTriggersManager;

    invoke-interface {v1, v0}, Lcom/glympse/android/api/GTriggersManager;->addLocalTrigger(Lcom/glympse/android/api/GTrigger;)V

    .line 167
    :cond_1
    return-void
.end method

.method private m(Lcom/glympse/android/lib/GTicketPrivate;)V
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Lcom/glympse/android/lib/cp;->nO:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GChronoTrigger;

    .line 172
    if-eqz v0, :cond_0

    .line 174
    iget-object v1, p0, Lcom/glympse/android/lib/cp;->nQ:Lcom/glympse/android/api/GTriggersManager;

    invoke-interface {v1, v0}, Lcom/glympse/android/api/GTriggersManager;->removeLocalTrigger(Lcom/glympse/android/api/GTrigger;)V

    .line 175
    iget-object v1, p0, Lcom/glympse/android/lib/cp;->nM:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v1, v0}, Lcom/glympse/android/hal/GHashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    iget-object v0, p0, Lcom/glympse/android/lib/cp;->nO:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/cp;->nN:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GGeoTrigger;

    .line 180
    if-eqz v0, :cond_1

    .line 182
    iget-object v1, p0, Lcom/glympse/android/lib/cp;->nQ:Lcom/glympse/android/api/GTriggersManager;

    invoke-interface {v1, v0}, Lcom/glympse/android/api/GTriggersManager;->removeLocalTrigger(Lcom/glympse/android/api/GTrigger;)V

    .line 183
    iget-object v1, p0, Lcom/glympse/android/lib/cp;->nM:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v1, v0}, Lcom/glympse/android/hal/GHashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    iget-object v0, p0, Lcom/glympse/android/lib/cp;->nN:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/cp;->nP:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GEtaTrigger;

    .line 188
    if-eqz v0, :cond_2

    .line 190
    iget-object v1, p0, Lcom/glympse/android/lib/cp;->nQ:Lcom/glympse/android/api/GTriggersManager;

    invoke-interface {v1, v0}, Lcom/glympse/android/api/GTriggersManager;->removeLocalTrigger(Lcom/glympse/android/api/GTrigger;)V

    .line 191
    iget-object v1, p0, Lcom/glympse/android/lib/cp;->nM:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v1, v0}, Lcom/glympse/android/hal/GHashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    iget-object v0, p0, Lcom/glympse/android/lib/cp;->nP:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    :cond_2
    return-void
.end method

.method private n(Lcom/glympse/android/lib/GTicketPrivate;)V
    .locals 14

    .prologue
    const/4 v6, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 198
    iget-boolean v0, p0, Lcom/glympse/android/lib/cp;->nR:Z

    if-eqz v0, :cond_0

    .line 200
    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getDestination()Lcom/glympse/android/api/GPlace;

    move-result-object v0

    .line 201
    invoke-interface {v0}, Lcom/glympse/android/api/GPlace;->getLatitude()D

    move-result-wide v4

    invoke-interface {v0}, Lcom/glympse/android/api/GPlace;->getLongitude()D

    move-result-wide v8

    invoke-static {v4, v5, v8, v9}, Lcom/glympse/android/core/CoreFactory;->createLocation(DD)Lcom/glympse/android/core/GLocation;

    move-result-object v3

    .line 202
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cancel_geo_"

    invoke-static {v4}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 203
    iget-wide v4, p0, Lcom/glympse/android/lib/cp;->nT:J

    long-to-double v4, v4

    invoke-static/range {v0 .. v6}, Lcom/glympse/android/api/GlympseFactory;->createGeoTrigger(Ljava/lang/String;ZLcom/glympse/android/api/GTicket;Lcom/glympse/android/core/GLatLng;DI)Lcom/glympse/android/api/GGeoTrigger;

    move-result-object v0

    .line 206
    iget-object v3, p0, Lcom/glympse/android/lib/cp;->nM:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v3, v0, p1}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    iget-object v3, p0, Lcom/glympse/android/lib/cp;->nN:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v3, p1, v0}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    iget-object v3, p0, Lcom/glympse/android/lib/cp;->nQ:Lcom/glympse/android/api/GTriggersManager;

    invoke-interface {v3, v0}, Lcom/glympse/android/api/GTriggersManager;->addLocalTrigger(Lcom/glympse/android/api/GTrigger;)V

    .line 210
    :cond_0
    iget-boolean v0, p0, Lcom/glympse/android/lib/cp;->nU:Z

    if-eqz v0, :cond_1

    .line 212
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cancel_eta_"

    invoke-static {v3}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 213
    iget-wide v10, p0, Lcom/glympse/android/lib/cp;->nW:J

    move v8, v1

    move-object v9, v2

    move v12, v6

    move-object v13, p1

    invoke-static/range {v7 .. v13}, Lcom/glympse/android/api/GlympseFactory;->createEtaTrigger(Ljava/lang/String;ZLcom/glympse/android/api/GTicket;JILcom/glympse/android/api/GTicket;)Lcom/glympse/android/api/GEtaTrigger;

    move-result-object v0

    .line 216
    iget-object v3, p0, Lcom/glympse/android/lib/cp;->nM:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v3, v0, p1}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    iget-object v3, p0, Lcom/glympse/android/lib/cp;->nP:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v3, p1, v0}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    iget-object v3, p0, Lcom/glympse/android/lib/cp;->nQ:Lcom/glympse/android/api/GTriggersManager;

    invoke-interface {v3, v0}, Lcom/glympse/android/api/GTriggersManager;->addLocalTrigger(Lcom/glympse/android/api/GTrigger;)V

    .line 221
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "timeout_"

    invoke-static {v3}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 222
    iget-object v3, p0, Lcom/glympse/android/lib/cp;->cS:Lcom/glympse/android/api/GGlympse;

    invoke-interface {v3}, Lcom/glympse/android/api/GGlympse;->getTime()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/glympse/android/lib/cp;->nX:J

    add-long/2addr v4, v6

    .line 223
    invoke-static {v0, v1, v2, v4, v5}, Lcom/glympse/android/api/GlympseFactory;->createChronoTrigger(Ljava/lang/String;ZLcom/glympse/android/api/GTicket;J)Lcom/glympse/android/api/GChronoTrigger;

    move-result-object v0

    .line 225
    iget-object v1, p0, Lcom/glympse/android/lib/cp;->nM:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v1, v0, p1}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    iget-object v1, p0, Lcom/glympse/android/lib/cp;->nO:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v1, p1, v0}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    iget-object v1, p0, Lcom/glympse/android/lib/cp;->nQ:Lcom/glympse/android/api/GTriggersManager;

    invoke-interface {v1, v0}, Lcom/glympse/android/api/GTriggersManager;->addLocalTrigger(Lcom/glympse/android/api/GTrigger;)V

    .line 228
    return-void
.end method

.method private o(Lcom/glympse/android/lib/GTicketPrivate;)V
    .locals 2

    .prologue
    .line 232
    iget-object v0, p0, Lcom/glympse/android/lib/cp;->nO:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GChronoTrigger;

    .line 233
    if-eqz v0, :cond_0

    .line 235
    iget-object v1, p0, Lcom/glympse/android/lib/cp;->nQ:Lcom/glympse/android/api/GTriggersManager;

    invoke-interface {v1, v0}, Lcom/glympse/android/api/GTriggersManager;->removeLocalTrigger(Lcom/glympse/android/api/GTrigger;)V

    .line 236
    iget-object v1, p0, Lcom/glympse/android/lib/cp;->nM:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v1, v0}, Lcom/glympse/android/hal/GHashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 237
    iget-object v0, p0, Lcom/glympse/android/lib/cp;->nO:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    :cond_0
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/cp;->l(Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 241
    return-void
.end method

.method private p(Lcom/glympse/android/lib/GTicketPrivate;)V
    .locals 0

    .prologue
    .line 245
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/cp;->c(Lcom/glympse/android/api/GTicket;)V

    .line 246
    return-void
.end method


# virtual methods
.method public a(Lcom/glympse/android/api/GTicket;)V
    .locals 0

    .prologue
    .line 112
    check-cast p1, Lcom/glympse/android/lib/GTicketPrivate;

    invoke-direct {p0, p1}, Lcom/glympse/android/lib/cp;->l(Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 113
    return-void
.end method

.method public a(Lcom/glympse/android/lib/bd;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/glympse/android/lib/cp;->hk:Lcom/glympse/android/lib/bd;

    .line 108
    return-void
.end method

.method public aq()V
    .locals 4

    .prologue
    .line 122
    new-instance v2, Lcom/glympse/android/hal/GVector;

    invoke-direct {v2}, Lcom/glympse/android/hal/GVector;-><init>()V

    .line 124
    iget-object v0, p0, Lcom/glympse/android/lib/cp;->nM:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GHashtable;->keys()Ljava/util/Enumeration;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GTrigger;

    .line 127
    iget-object v3, p0, Lcom/glympse/android/lib/cp;->nM:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v3, v0}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GTicket;

    .line 128
    invoke-virtual {v2, v0}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    goto :goto_0

    .line 131
    :cond_0
    invoke-virtual {v2}, Lcom/glympse/android/hal/GVector;->length()I

    move-result v3

    .line 132
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    .line 134
    invoke-virtual {v2, v1}, Lcom/glympse/android/hal/GVector;->at(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GTicketPrivate;

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/cp;->m(Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 132
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 136
    :cond_1
    return-void
.end method

.method public b(Lcom/glympse/android/api/GTicket;)V
    .locals 0

    .prologue
    .line 117
    check-cast p1, Lcom/glympse/android/lib/GTicketPrivate;

    invoke-direct {p0, p1}, Lcom/glympse/android/lib/cp;->m(Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 118
    return-void
.end method

.method public b(Lcom/glympse/android/core/GPrimitive;)V
    .locals 8

    .prologue
    const/4 v5, 0x1

    .line 82
    const-string v0, "timeout"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 83
    const-string v1, "timer_cancel_radius"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 84
    const-string v2, "timer_cancel_eta"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 85
    const-string v3, "timer_init_radius"

    invoke-static {v3}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 86
    const-string v4, "timer_init_eta"

    invoke-static {v4}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 88
    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/glympse/android/lib/cp;->nX:J

    .line 90
    invoke-interface {p1, v3}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1, v1}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    iput-boolean v5, p0, Lcom/glympse/android/lib/cp;->nR:Z

    .line 93
    invoke-interface {p1, v3}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/glympse/android/lib/cp;->nS:J

    .line 94
    invoke-interface {p1, v1}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/glympse/android/lib/cp;->nT:J

    .line 97
    :cond_0
    invoke-interface {p1, v4}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1, v2}, Lcom/glympse/android/core/GPrimitive;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 99
    iput-boolean v5, p0, Lcom/glympse/android/lib/cp;->nU:Z

    .line 100
    invoke-interface {p1, v4}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/glympse/android/lib/cp;->nV:J

    .line 101
    invoke-interface {p1, v2}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/glympse/android/lib/cp;->nW:J

    .line 103
    :cond_1
    return-void
.end method

.method public start(Lcom/glympse/android/api/GGlympse;)V
    .locals 3

    .prologue
    .line 52
    new-instance v0, Lcom/glympse/android/hal/GHashtable;

    invoke-direct {v0}, Lcom/glympse/android/hal/GHashtable;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/cp;->nM:Lcom/glympse/android/hal/GHashtable;

    .line 53
    new-instance v0, Lcom/glympse/android/hal/GHashtable;

    invoke-direct {v0}, Lcom/glympse/android/hal/GHashtable;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/cp;->nN:Lcom/glympse/android/hal/GHashtable;

    .line 54
    new-instance v0, Lcom/glympse/android/hal/GHashtable;

    invoke-direct {v0}, Lcom/glympse/android/hal/GHashtable;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/cp;->nO:Lcom/glympse/android/hal/GHashtable;

    .line 55
    new-instance v0, Lcom/glympse/android/hal/GHashtable;

    invoke-direct {v0}, Lcom/glympse/android/hal/GHashtable;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/cp;->nP:Lcom/glympse/android/hal/GHashtable;

    .line 57
    iput-object p1, p0, Lcom/glympse/android/lib/cp;->cS:Lcom/glympse/android/api/GGlympse;

    .line 58
    iget-object v0, p0, Lcom/glympse/android/lib/cp;->cS:Lcom/glympse/android/api/GGlympse;

    invoke-interface {v0}, Lcom/glympse/android/api/GGlympse;->getTriggersManager()Lcom/glympse/android/api/GTriggersManager;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/cp;->nQ:Lcom/glympse/android/api/GTriggersManager;

    .line 60
    new-instance v0, Lcom/glympse/android/lib/cp$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/cp$a;-><init>(Lcom/glympse/android/lib/cp$1;)V

    iput-object v0, p0, Lcom/glympse/android/lib/cp;->nY:Lcom/glympse/android/lib/cp$a;

    .line 61
    iget-object v1, p0, Lcom/glympse/android/lib/cp;->nY:Lcom/glympse/android/lib/cp$a;

    iget-object v2, p0, Lcom/glympse/android/lib/cp;->nQ:Lcom/glympse/android/api/GTriggersManager;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/cp;

    invoke-virtual {v1, v2, v0}, Lcom/glympse/android/lib/cp$a;->a(Lcom/glympse/android/api/GTriggersManager;Lcom/glympse/android/lib/cp;)V

    .line 62
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 66
    invoke-virtual {p0}, Lcom/glympse/android/lib/cp;->aq()V

    .line 68
    iput-object v1, p0, Lcom/glympse/android/lib/cp;->hk:Lcom/glympse/android/lib/bd;

    .line 69
    iput-object v1, p0, Lcom/glympse/android/lib/cp;->nM:Lcom/glympse/android/hal/GHashtable;

    .line 70
    iput-object v1, p0, Lcom/glympse/android/lib/cp;->nN:Lcom/glympse/android/hal/GHashtable;

    .line 71
    iput-object v1, p0, Lcom/glympse/android/lib/cp;->nO:Lcom/glympse/android/hal/GHashtable;

    .line 72
    iput-object v1, p0, Lcom/glympse/android/lib/cp;->nP:Lcom/glympse/android/hal/GHashtable;

    .line 74
    iget-object v0, p0, Lcom/glympse/android/lib/cp;->nY:Lcom/glympse/android/lib/cp$a;

    invoke-virtual {v0}, Lcom/glympse/android/lib/cp$a;->stop()V

    .line 75
    iput-object v1, p0, Lcom/glympse/android/lib/cp;->nY:Lcom/glympse/android/lib/cp$a;

    .line 77
    iput-object v1, p0, Lcom/glympse/android/lib/cp;->cS:Lcom/glympse/android/api/GGlympse;

    .line 78
    return-void
.end method
