.class Lcom/glympse/android/lib/dp;
.super Ljava/lang/Object;
.source "MemoryCache.java"

# interfaces
.implements Lcom/glympse/android/lib/GMemoryCache;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/lib/dp$a;,
        Lcom/glympse/android/lib/dp$b;
    }
.end annotation


# instance fields
.field private jE:Lcom/glympse/android/hal/GHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GHashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/glympse/android/lib/dp$b;",
            ">;"
        }
    .end annotation
.end field

.field private qK:I

.field private qL:I

.field private qM:Lcom/glympse/android/lib/bk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/lib/bk",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(II)V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput p1, p0, Lcom/glympse/android/lib/dp;->qK:I

    .line 40
    iput p2, p0, Lcom/glympse/android/lib/dp;->qL:I

    .line 41
    new-instance v0, Lcom/glympse/android/hal/GHashtable;

    iget v1, p0, Lcom/glympse/android/lib/dp;->qL:I

    add-int/lit8 v1, v1, 0x1

    invoke-direct {v0, v1}, Lcom/glympse/android/hal/GHashtable;-><init>(I)V

    iput-object v0, p0, Lcom/glympse/android/lib/dp;->jE:Lcom/glympse/android/hal/GHashtable;

    .line 42
    new-instance v0, Lcom/glympse/android/lib/dv;

    invoke-direct {v0}, Lcom/glympse/android/lib/dv;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/dp;->qM:Lcom/glympse/android/lib/bk;

    .line 43
    return-void
.end method

.method private cj()V
    .locals 3

    .prologue
    .line 123
    iget-object v0, p0, Lcom/glympse/android/lib/dp;->qM:Lcom/glympse/android/lib/bk;

    invoke-interface {v0}, Lcom/glympse/android/lib/bk;->size()I

    move-result v0

    .line 124
    iget v1, p0, Lcom/glympse/android/lib/dp;->qL:I

    if-ge v0, v1, :cond_1

    .line 137
    :cond_0
    return-void

    .line 130
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/glympse/android/lib/dp;->qM:Lcom/glympse/android/lib/bk;

    invoke-interface {v0}, Lcom/glympse/android/lib/bk;->size()I

    move-result v0

    iget v1, p0, Lcom/glympse/android/lib/dp;->qK:I

    if-le v0, v1, :cond_0

    .line 132
    iget-object v0, p0, Lcom/glympse/android/lib/dp;->qM:Lcom/glympse/android/lib/bk;

    invoke-interface {v0}, Lcom/glympse/android/lib/bk;->bB()Lcom/glympse/android/lib/bi;

    move-result-object v0

    .line 134
    iget-object v1, p0, Lcom/glympse/android/lib/dp;->jE:Lcom/glympse/android/hal/GHashtable;

    invoke-interface {v0}, Lcom/glympse/android/lib/bi;->bx()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/glympse/android/hal/GHashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    iget-object v1, p0, Lcom/glympse/android/lib/dp;->qM:Lcom/glympse/android/lib/bk;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/bk;->c(Lcom/glympse/android/lib/bi;)V

    goto :goto_0
.end method

.method private t(Ljava/lang/String;)Lcom/glympse/android/lib/dp$b;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/glympse/android/lib/dp;->jE:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/dp$b;

    return-object v0
.end method


# virtual methods
.method public cache(Ljava/lang/String;Lcom/glympse/android/core/GCommon;)V
    .locals 3

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/dp;->t(Ljava/lang/String;)Lcom/glympse/android/lib/dp$b;

    move-result-object v0

    .line 67
    if-nez v0, :cond_0

    .line 70
    new-instance v0, Lcom/glympse/android/lib/dp$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/dp$a;-><init>(Lcom/glympse/android/lib/dp$1;)V

    .line 71
    iput-object p2, v0, Lcom/glympse/android/lib/dp$b;->qN:Lcom/glympse/android/core/GCommon;

    .line 72
    iget-object v1, p0, Lcom/glympse/android/lib/dp;->qM:Lcom/glympse/android/lib/bk;

    invoke-interface {v1, p1}, Lcom/glympse/android/lib/bk;->d(Ljava/lang/Object;)Lcom/glympse/android/lib/bi;

    move-result-object v1

    iput-object v1, v0, Lcom/glympse/android/lib/dp$b;->qO:Lcom/glympse/android/lib/bi;

    .line 73
    iget-object v1, p0, Lcom/glympse/android/lib/dp;->jE:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v1, p1, v0}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    invoke-direct {p0}, Lcom/glympse/android/lib/dp;->cj()V

    .line 87
    :goto_0
    return-void

    .line 81
    :cond_0
    iput-object p2, v0, Lcom/glympse/android/lib/dp$b;->qN:Lcom/glympse/android/core/GCommon;

    .line 84
    iget-object v1, p0, Lcom/glympse/android/lib/dp;->qM:Lcom/glympse/android/lib/bk;

    iget-object v2, v0, Lcom/glympse/android/lib/dp$b;->qO:Lcom/glympse/android/lib/bi;

    invoke-interface {v1, v2}, Lcom/glympse/android/lib/bk;->c(Lcom/glympse/android/lib/bi;)V

    .line 85
    iget-object v1, p0, Lcom/glympse/android/lib/dp;->qM:Lcom/glympse/android/lib/bk;

    invoke-interface {v1, p1}, Lcom/glympse/android/lib/bk;->d(Ljava/lang/Object;)Lcom/glympse/android/lib/bi;

    move-result-object v1

    iput-object v1, v0, Lcom/glympse/android/lib/dp$b;->qO:Lcom/glympse/android/lib/bi;

    goto :goto_0
.end method

.method public extract(Ljava/lang/String;)Lcom/glympse/android/core/GCommon;
    .locals 3

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/dp;->t(Ljava/lang/String;)Lcom/glympse/android/lib/dp$b;

    move-result-object v0

    .line 52
    if-nez v0, :cond_0

    .line 54
    const/4 v0, 0x0

    .line 61
    :goto_0
    return-object v0

    .line 58
    :cond_0
    iget-object v1, p0, Lcom/glympse/android/lib/dp;->qM:Lcom/glympse/android/lib/bk;

    iget-object v2, v0, Lcom/glympse/android/lib/dp$b;->qO:Lcom/glympse/android/lib/bi;

    invoke-interface {v1, v2}, Lcom/glympse/android/lib/bk;->c(Lcom/glympse/android/lib/bi;)V

    .line 59
    iget-object v1, p0, Lcom/glympse/android/lib/dp;->qM:Lcom/glympse/android/lib/bk;

    invoke-interface {v1, p1}, Lcom/glympse/android/lib/bk;->d(Ljava/lang/Object;)Lcom/glympse/android/lib/bi;

    move-result-object v1

    iput-object v1, v0, Lcom/glympse/android/lib/dp$b;->qO:Lcom/glympse/android/lib/bi;

    .line 61
    iget-object v0, v0, Lcom/glympse/android/lib/dp$b;->qN:Lcom/glympse/android/core/GCommon;

    goto :goto_0
.end method

.method public onLowMemory()V
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/glympse/android/lib/dp;->jE:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GHashtable;->clear()V

    .line 109
    iget-object v0, p0, Lcom/glympse/android/lib/dp;->qM:Lcom/glympse/android/lib/bk;

    invoke-interface {v0}, Lcom/glympse/android/lib/bk;->removeAll()V

    .line 110
    return-void
.end method

.method public remove(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/dp;->t(Ljava/lang/String;)Lcom/glympse/android/lib/dp$b;

    move-result-object v0

    .line 93
    if-nez v0, :cond_0

    .line 103
    :goto_0
    return-void

    .line 99
    :cond_0
    iget-object v1, p0, Lcom/glympse/android/lib/dp;->jE:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v1, p1}, Lcom/glympse/android/hal/GHashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    iget-object v1, p0, Lcom/glympse/android/lib/dp;->qM:Lcom/glympse/android/lib/bk;

    iget-object v0, v0, Lcom/glympse/android/lib/dp$b;->qO:Lcom/glympse/android/lib/bi;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/bk;->c(Lcom/glympse/android/lib/bi;)V

    goto :goto_0
.end method
