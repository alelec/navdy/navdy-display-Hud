.class Lcom/glympse/android/lib/cu$a;
.super Lcom/glympse/android/lib/f;
.source "InviteCreate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/lib/cu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field public bn:Ljava/lang/String;

.field public cg:I

.field public mw:Ljava/lang/String;

.field public ov:J

.field public oy:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 655
    invoke-direct {p0}, Lcom/glympse/android/lib/f;-><init>()V

    .line 656
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/glympse/android/lib/cu$a;->ov:J

    .line 657
    const/4 v0, 0x4

    iput v0, p0, Lcom/glympse/android/lib/cu$a;->cg:I

    .line 658
    return-void
.end method


# virtual methods
.method public endObject(I)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 662
    if-ne v1, p1, :cond_0

    .line 664
    iget-object v0, p0, Lcom/glympse/android/lib/cu$a;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    invoke-interface {v0}, Lcom/glympse/android/lib/json/GJsonHandlerStack;->popHandler()V

    .line 666
    :cond_0
    return v1
.end method

.method public primitive(ILcom/glympse/android/lib/json/GJsonPrimitive;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 671
    packed-switch p1, :pswitch_data_0

    .line 720
    :cond_0
    :goto_0
    return v2

    .line 675
    :pswitch_0
    iget-object v0, p0, Lcom/glympse/android/lib/cu$a;->gE:Ljava/lang/String;

    const-string v1, "result"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 677
    invoke-virtual {p2, v2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/cu$a;->gF:Ljava/lang/String;

    goto :goto_0

    .line 683
    :pswitch_1
    iget-object v0, p0, Lcom/glympse/android/lib/cu$a;->gE:Ljava/lang/String;

    const-string v1, "id"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 685
    invoke-virtual {p2, v2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v0

    .line 686
    invoke-static {v0}, Lcom/glympse/android/lib/TicketCode;->cleanupInviteCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/cu$a;->mw:Ljava/lang/String;

    goto :goto_0

    .line 688
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/cu$a;->gE:Ljava/lang/String;

    const-string v1, "status"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 690
    invoke-virtual {p2, v2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getString(Z)Ljava/lang/String;

    move-result-object v0

    .line 691
    invoke-static {v0}, Lcom/glympse/android/lib/ct;->q(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/glympse/android/lib/cu$a;->cg:I

    goto :goto_0

    .line 693
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/cu$a;->gE:Ljava/lang/String;

    const-string v1, "url"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 695
    invoke-virtual {p2, v2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/cu$a;->bn:Ljava/lang/String;

    goto :goto_0

    .line 697
    :cond_3
    iget-object v0, p0, Lcom/glympse/android/lib/cu$a;->gE:Ljava/lang/String;

    const-string v1, "text"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 699
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/cu$a;->oy:Ljava/lang/String;

    goto :goto_0

    .line 701
    :cond_4
    iget-object v0, p0, Lcom/glympse/android/lib/cu$a;->gE:Ljava/lang/String;

    const-string v1, "created"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 703
    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/glympse/android/lib/cu$a;->ov:J

    goto :goto_0

    .line 705
    :cond_5
    iget-object v0, p0, Lcom/glympse/android/lib/cu$a;->gE:Ljava/lang/String;

    const-string v1, "time"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 707
    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/glympse/android/lib/cu$a;->_time:J

    goto :goto_0

    .line 709
    :cond_6
    iget-object v0, p0, Lcom/glympse/android/lib/cu$a;->gE:Ljava/lang/String;

    const-string v1, "error"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 711
    invoke-virtual {p2, v2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/cu$a;->gG:Ljava/lang/String;

    goto/16 :goto_0

    .line 713
    :cond_7
    iget-object v0, p0, Lcom/glympse/android/lib/cu$a;->gE:Ljava/lang/String;

    const-string v1, "error_detail"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 715
    invoke-virtual {p2, v2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/cu$a;->gH:Ljava/lang/String;

    goto/16 :goto_0

    .line 671
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
