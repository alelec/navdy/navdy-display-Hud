.class public Lcom/glympse/android/lib/Debug;
.super Ljava/lang/Object;
.source "Debug.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/lib/Debug$a;
    }
.end annotation


# static fields
.field public static final CRITICAL:I = 0x6

.field public static final DUMP:I = 0x2

.field public static final ERROR:I = 0x5

.field public static final FILE_ONLY:I = 0x10000

.field public static final INFO:I = 0x1

.field public static final LEVEL_MASK:I = 0xffff

.field public static final NONE:I = 0x7

.field public static final NOTICE:I = 0x3

.field public static final RELEASE:Z = true

.field public static final WARNING:I = 0x4

.field public static final _levelCodes:[C

.field private static iu:Lcom/glympse/android/hal/GContextHolder;

.field private static jJ:Z

.field private static jK:I

.field private static jL:I

.field private static jM:Ljava/lang/String;

.field private static jN:Z

.field private static jO:Ljava/lang/String;

.field private static jP:Ljava/lang/String;

.field private static jQ:J

.field private static jR:J

.field private static jS:Z

.field private static jT:Z

.field private static jU:Lcom/glympse/android/hal/GVector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GVector",
            "<",
            "Lcom/glympse/android/lib/GGlympsePrivate;",
            ">;"
        }
    .end annotation
.end field

.field private static jV:Lcom/glympse/android/hal/GMutex;

.field private static jW:Ljava/lang/String;

.field private static jX:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x7

    .line 54
    const/4 v0, 0x0

    sput-boolean v0, Lcom/glympse/android/lib/Debug;->jJ:Z

    .line 59
    sput v1, Lcom/glympse/android/lib/Debug;->jK:I

    .line 64
    sput v1, Lcom/glympse/android/lib/Debug;->jL:I

    .line 89
    sput-wide v2, Lcom/glympse/android/lib/Debug;->jQ:J

    .line 94
    sput-wide v2, Lcom/glympse/android/lib/Debug;->jR:J

    .line 114
    invoke-static {}, Lcom/glympse/android/hal/HalFactory;->createMutex()Lcom/glympse/android/hal/GMutex;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/lib/Debug;->jV:Lcom/glympse/android/hal/GMutex;

    .line 124
    const/4 v0, 0x1

    sput-boolean v0, Lcom/glympse/android/lib/Debug;->jX:Z

    .line 543
    new-array v0, v1, [C

    fill-array-data v0, :array_0

    sput-object v0, Lcom/glympse/android/lib/Debug;->_levelCodes:[C

    return-void

    nop

    :array_0
    .array-data 2
        0x41s
        0x49s
        0x44s
        0x4es
        0x57s
        0x45s
        0x43s
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(IZZLjava/lang/String;Z)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v10, 0x1

    .line 551
    sget-boolean v0, Lcom/glympse/android/lib/Debug;->jN:Z

    if-eqz v0, :cond_0

    .line 601
    :goto_0
    return-void

    .line 555
    :cond_0
    sput-boolean v10, Lcom/glympse/android/lib/Debug;->jN:Z

    .line 556
    sget-object v0, Lcom/glympse/android/lib/Debug;->jV:Lcom/glympse/android/hal/GMutex;

    invoke-interface {v0}, Lcom/glympse/android/hal/GMutex;->block()V

    .line 561
    :try_start_0
    invoke-static {}, Lcom/glympse/android/hal/Concurrent;->getTime()J

    move-result-wide v4

    .line 563
    const-string v0, "\n"

    invoke-static {p3, v0}, Lcom/glympse/android/hal/Helpers;->split(Ljava/lang/String;Ljava/lang/String;)Lcom/glympse/android/hal/GVector;

    move-result-object v3

    .line 564
    invoke-virtual {v3}, Lcom/glympse/android/hal/GVector;->length()I

    move-result v6

    move v1, v2

    .line 565
    :goto_1
    if-ge v1, v6, :cond_3

    .line 567
    invoke-virtual {v3, v1}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 568
    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 570
    new-instance v7, Ljava/lang/StringBuilder;

    const/16 v8, 0x100

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 571
    const/16 v8, 0x5b

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 572
    invoke-static {}, Lcom/glympse/android/hal/Platform;->getThreadId()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 573
    const-string v8, "]["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 574
    sget-object v8, Lcom/glympse/android/lib/Debug;->_levelCodes:[C

    aget-char v8, v8, p0

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 575
    const-string v8, "] "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 576
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 577
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 580
    if-eqz p2, :cond_1

    sget-object v7, Lcom/glympse/android/lib/Debug;->iu:Lcom/glympse/android/hal/GContextHolder;

    if-eqz v7, :cond_1

    .line 582
    invoke-static {v4, v5, v0}, Lcom/glympse/android/lib/Debug;->c(JLjava/lang/String;)V

    .line 586
    :cond_1
    if-eqz p1, :cond_2

    .line 588
    invoke-static {p4, v0}, Lcom/glympse/android/hal/DebugBase;->writeConsole(ZLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 565
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 593
    :catch_0
    move-exception v0

    .line 597
    :cond_3
    invoke-static {v10, v10}, Lcom/glympse/android/lib/Debug;->a(ZZ)V

    .line 599
    sget-object v0, Lcom/glympse/android/lib/Debug;->jV:Lcom/glympse/android/hal/GMutex;

    invoke-interface {v0}, Lcom/glympse/android/hal/GMutex;->unblock()V

    .line 600
    sput-boolean v2, Lcom/glympse/android/lib/Debug;->jN:Z

    goto :goto_0
.end method

.method private static a(ZZ)V
    .locals 4

    .prologue
    .line 725
    if-eqz p1, :cond_2

    .line 727
    sget-boolean v0, Lcom/glympse/android/lib/Debug;->jT:Z

    if-eqz v0, :cond_1

    .line 745
    :cond_0
    :goto_0
    return-void

    .line 732
    :cond_1
    sget-wide v0, Lcom/glympse/android/lib/Debug;->jQ:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 737
    invoke-static {}, Lcom/glympse/android/lib/Debug;->aU()J

    move-result-wide v0

    .line 738
    sget-wide v2, Lcom/glympse/android/lib/Debug;->jR:J

    sub-long/2addr v0, v2

    sget-wide v2, Lcom/glympse/android/lib/Debug;->jQ:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 743
    :cond_2
    const/4 v0, 0x1

    sput-boolean v0, Lcom/glympse/android/lib/Debug;->jT:Z

    .line 744
    new-instance v0, Lcom/glympse/android/lib/Debug$a;

    invoke-direct {v0, p0}, Lcom/glympse/android/lib/Debug$a;-><init>(Z)V

    invoke-static {v0}, Lcom/glympse/android/hal/DebugBase;->runInBackground(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private static a(Lcom/glympse/android/hal/GContextHolder;Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 899
    invoke-interface {p0}, Lcom/glympse/android/hal/GContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/glympse/android/hal/DebugBase;->getFileSize(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v0

    const-wide/32 v2, 0x100000

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 901
    invoke-interface {p0}, Lcom/glympse/android/hal/GContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/glympse/android/hal/DebugBase;->isOnWifi(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 903
    const/4 v0, 0x1

    .line 906
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static aU()J
    .locals 2

    .prologue
    .line 886
    sget-object v0, Lcom/glympse/android/lib/Debug;->jU:Lcom/glympse/android/hal/GVector;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 887
    :goto_0
    if-lez v0, :cond_1

    .line 889
    sget-object v1, Lcom/glympse/android/lib/Debug;->jU:Lcom/glympse/android/hal/GVector;

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getTime()J

    move-result-wide v0

    .line 891
    :goto_1
    return-wide v0

    .line 886
    :cond_0
    sget-object v0, Lcom/glympse/android/lib/Debug;->jU:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v0

    goto :goto_0

    .line 891
    :cond_1
    invoke-static {}, Lcom/glympse/android/hal/Concurrent;->getTime()J

    move-result-wide v0

    goto :goto_1
.end method

.method private static aV()V
    .locals 4

    .prologue
    .line 926
    :try_start_0
    sget-object v0, Lcom/glympse/android/lib/Debug;->jU:Lcom/glympse/android/hal/GVector;

    if-eqz v0, :cond_2

    .line 928
    sget-object v0, Lcom/glympse/android/lib/Debug;->jU:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v3

    .line 929
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_2

    .line 931
    sget-object v0, Lcom/glympse/android/lib/Debug;->jU:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, v2}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GGlympsePrivate;

    .line 932
    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->isStarted()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->isActive()Z

    move-result v1

    if-nez v1, :cond_1

    .line 929
    :cond_0
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 945
    :cond_1
    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getConfig()Lcom/glympse/android/api/GConfig;

    move-result-object v1

    check-cast v1, Lcom/glympse/android/lib/GConfigPrivate;

    .line 946
    invoke-interface {v1}, Lcom/glympse/android/lib/GConfigPrivate;->save()V

    .line 949
    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getUserManager()Lcom/glympse/android/api/GUserManager;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GUserManagerPrivate;

    .line 950
    invoke-interface {v0}, Lcom/glympse/android/lib/GUserManagerPrivate;->save()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 954
    :catch_0
    move-exception v0

    .line 957
    :cond_2
    return-void
.end method

.method public static areSignalHandlersEnabled()Z
    .locals 1

    .prologue
    .line 380
    sget-boolean v0, Lcom/glympse/android/lib/Debug;->jX:Z

    return v0
.end method

.method private static b(Z)V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v3, 0x0

    .line 770
    :try_start_0
    sget-object v0, Lcom/glympse/android/lib/Debug;->jV:Lcom/glympse/android/hal/GMutex;

    invoke-interface {v0}, Lcom/glympse/android/hal/GMutex;->block()V

    .line 771
    sget-boolean v0, Lcom/glympse/android/lib/Debug;->jS:Z

    if-eqz v0, :cond_0

    .line 773
    sget-object v0, Lcom/glympse/android/lib/Debug;->jV:Lcom/glympse/android/hal/GMutex;

    invoke-interface {v0}, Lcom/glympse/android/hal/GMutex;->unblock()V

    .line 877
    :goto_0
    return-void

    .line 776
    :cond_0
    sget-object v0, Lcom/glympse/android/lib/Debug;->iu:Lcom/glympse/android/hal/GContextHolder;

    invoke-interface {v0}, Lcom/glympse/android/hal/GContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/glympse/android/hal/HalFactory;->openDirectory(Landroid/content/Context;Ljava/lang/String;Z)Lcom/glympse/android/hal/GDirectory;

    move-result-object v5

    .line 777
    if-nez v5, :cond_2

    .line 779
    sget-object v0, Lcom/glympse/android/lib/Debug;->jV:Lcom/glympse/android/hal/GMutex;

    invoke-interface {v0}, Lcom/glympse/android/hal/GMutex;->unblock()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 868
    :catch_0
    move-exception v0

    .line 870
    invoke-static {v0, v11}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    .line 872
    :cond_1
    sget-object v0, Lcom/glympse/android/lib/Debug;->jV:Lcom/glympse/android/hal/GMutex;

    invoke-interface {v0}, Lcom/glympse/android/hal/GMutex;->block()V

    .line 873
    invoke-static {}, Lcom/glympse/android/lib/Debug;->aU()J

    move-result-wide v0

    sput-wide v0, Lcom/glympse/android/lib/Debug;->jR:J

    .line 874
    sput-boolean v3, Lcom/glympse/android/lib/Debug;->jS:Z

    .line 875
    sput-boolean v3, Lcom/glympse/android/lib/Debug;->jT:Z

    .line 876
    sget-object v0, Lcom/glympse/android/lib/Debug;->jV:Lcom/glympse/android/hal/GMutex;

    invoke-interface {v0}, Lcom/glympse/android/hal/GMutex;->unblock()V

    goto :goto_0

    .line 782
    :cond_2
    const/4 v0, 0x1

    :try_start_1
    sput-boolean v0, Lcom/glympse/android/lib/Debug;->jS:Z

    .line 784
    if-eqz p0, :cond_3

    .line 787
    const/4 v0, 0x0

    sput-object v0, Lcom/glympse/android/lib/Debug;->jP:Ljava/lang/String;

    .line 791
    :cond_3
    sget-object v6, Lcom/glympse/android/lib/Debug;->jP:Ljava/lang/String;

    .line 793
    sget-object v0, Lcom/glympse/android/lib/Debug;->jV:Lcom/glympse/android/hal/GMutex;

    invoke-interface {v0}, Lcom/glympse/android/hal/GMutex;->unblock()V

    .line 796
    invoke-interface {v5}, Lcom/glympse/android/hal/GDirectory;->getContents()Lcom/glympse/android/hal/GVector;

    move-result-object v7

    move v4, v3

    .line 800
    :goto_1
    const/4 v0, 0x2

    if-ge v4, v0, :cond_1

    .line 803
    invoke-virtual {v7}, Lcom/glympse/android/hal/GVector;->length()I

    move-result v8

    move v2, v3

    .line 804
    :goto_2
    if-ge v2, v8, :cond_a

    .line 806
    invoke-virtual {v7, v2}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 809
    if-nez v4, :cond_5

    const-string v1, ".err"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 804
    :cond_4
    :goto_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 815
    :cond_5
    if-ne v11, v4, :cond_6

    const-string v1, ".log"

    .line 816
    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {v6}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    invoke-virtual {v6, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 821
    :cond_6
    sget-object v1, Lcom/glympse/android/lib/Debug;->iu:Lcom/glympse/android/hal/GContextHolder;

    invoke-static {v1, v0}, Lcom/glympse/android/lib/Debug;->a(Lcom/glympse/android/hal/GContextHolder;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 826
    const/4 v1, 0x3

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "LogUpload - Starting "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v1, v9}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 829
    new-instance v9, Ljava/lang/StringBuilder;

    const/16 v1, 0x400

    invoke-direct {v9, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 830
    sget-object v1, Lcom/glympse/android/lib/Debug;->jM:Ljava/lang/String;

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 831
    const-string v1, "?type="

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 832
    invoke-static {}, Lcom/glympse/android/hal/Platform;->getOsName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 833
    const/16 v1, 0x5f

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 834
    sget-object v1, Lcom/glympse/android/lib/StaticConfig;->BUILD_NAME:Ljava/lang/String;

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 835
    const/16 v1, 0x5f

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 836
    if-nez v4, :cond_7

    const-string v1, "error"

    :goto_4
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 837
    const-string v1, "&name="

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 838
    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 841
    sget-object v1, Lcom/glympse/android/lib/Debug;->iu:Lcom/glympse/android/hal/GContextHolder;

    invoke-interface {v1}, Lcom/glympse/android/hal/GContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v1, v9, v0}, Lcom/glympse/android/hal/DebugBase;->httpUpload(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 844
    const/16 v9, 0xc8

    if-ne v9, v1, :cond_9

    .line 847
    invoke-interface {v5, v0}, Lcom/glympse/android/hal/GDirectory;->deleteFile(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 849
    const/4 v1, 0x3

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "LogUpload - Finished "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    goto/16 :goto_3

    .line 836
    :cond_7
    const-string v1, "log"

    goto :goto_4

    .line 853
    :cond_8
    const/4 v1, 0x3

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "LogUpload - Failed to delete "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    goto/16 :goto_3

    .line 858
    :cond_9
    new-instance v9, Ljava/lang/StringBuilder;

    const/16 v10, 0x400

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 859
    const-string v10, "LogUpload - Failed ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 860
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 861
    const-string v1, "] to upload "

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 862
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 863
    const/4 v0, 0x5

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_3

    .line 800
    :cond_a
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto/16 :goto_1
.end method

.method public static buildFilepath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 652
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x400

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 653
    sget-object v1, Lcom/glympse/android/lib/Debug;->iu:Lcom/glympse/android/hal/GContextHolder;

    invoke-interface {v1}, Lcom/glympse/android/hal/GContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/glympse/android/hal/DebugBase;->appendPathToFile(Landroid/content/Context;Ljava/lang/StringBuilder;)V

    .line 654
    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 655
    const/16 v1, 0xf7

    invoke-static {p0, v2, v1}, Lcom/glympse/android/hal/Helpers;->substrlen(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 656
    invoke-static {p1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 658
    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 659
    const/4 v1, 0x3

    invoke-static {p1, v2, v1}, Lcom/glympse/android/hal/Helpers;->substrlen(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 661
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static c(JLjava/lang/String;)V
    .locals 4

    .prologue
    .line 612
    :try_start_0
    sget-object v0, Lcom/glympse/android/lib/Debug;->jP:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 615
    invoke-static {p0, p1}, Lcom/glympse/android/lib/Debug;->getLogFile(J)Ljava/lang/String;

    .line 618
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 619
    const/16 v1, 0x5b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 620
    invoke-static {p0, p1}, Lcom/glympse/android/hal/Platform;->timeToString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 621
    const-string v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 622
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 623
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 624
    sget-object v1, Lcom/glympse/android/lib/Debug;->iu:Lcom/glympse/android/hal/GContextHolder;

    invoke-interface {v1}, Lcom/glympse/android/hal/GContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/glympse/android/lib/Debug;->jP:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x1

    invoke-static {v1, v2, v0, v3}, Lcom/glympse/android/hal/DebugBase;->writeTextFile(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 629
    :goto_0
    return-void

    .line 626
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic c(Z)V
    .locals 0

    .prologue
    .line 16
    invoke-static {p0}, Lcom/glympse/android/lib/Debug;->b(Z)V

    return-void
.end method

.method public static dumpLocation(Lcom/glympse/android/core/GLocation;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x2

    const/4 v2, 0x0

    .line 411
    sget v0, Lcom/glympse/android/lib/Debug;->jK:I

    if-lt v4, v0, :cond_0

    move v0, v1

    .line 412
    :goto_0
    sget v3, Lcom/glympse/android/lib/Debug;->jL:I

    if-lt v4, v3, :cond_1

    .line 413
    :goto_1
    if-nez v0, :cond_2

    if-nez v1, :cond_2

    .line 419
    :goto_2
    return-void

    :cond_0
    move v0, v2

    .line 411
    goto :goto_0

    :cond_1
    move v1, v2

    .line 412
    goto :goto_1

    .line 418
    :cond_2
    invoke-static {p0}, Lcom/glympse/android/lib/Debug;->formatLocation(Lcom/glympse/android/core/GLocation;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v0, v1, v3, v2}, Lcom/glympse/android/lib/Debug;->a(IZZLjava/lang/String;Z)V

    goto :goto_2
.end method

.method public static dumpPackets(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x2

    const/4 v2, 0x0

    .line 389
    if-nez p0, :cond_1

    .line 406
    :cond_0
    :goto_0
    return-void

    .line 395
    :cond_1
    sget v0, Lcom/glympse/android/lib/Debug;->jK:I

    if-lt v6, v0, :cond_3

    move v0, v1

    .line 396
    :goto_1
    sget v3, Lcom/glympse/android/lib/Debug;->jL:I

    if-lt v6, v3, :cond_4

    .line 397
    :goto_2
    if-nez v0, :cond_2

    if-eqz v1, :cond_0

    .line 402
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "vvvvvvvv Packet (Size: "

    invoke-static {v4}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    int-to-long v4, v4

    invoke-static {v4, v5}, Lcom/glympse/android/hal/Helpers;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv"

    .line 403
    invoke-static {v4}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 402
    invoke-static {v6, v0, v1, v3, v2}, Lcom/glympse/android/lib/Debug;->a(IZZLjava/lang/String;Z)V

    .line 404
    invoke-static {v6, v0, v1, p0, v2}, Lcom/glympse/android/lib/Debug;->a(IZZLjava/lang/String;Z)V

    .line 405
    const-string v3, "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"

    invoke-static {v3}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v0, v1, v3, v2}, Lcom/glympse/android/lib/Debug;->a(IZZLjava/lang/String;Z)V

    goto :goto_0

    :cond_3
    move v0, v2

    .line 395
    goto :goto_1

    :cond_4
    move v1, v2

    .line 396
    goto :goto_2
.end method

.method private static e(J)V
    .locals 2

    .prologue
    .line 641
    sget-object v0, Lcom/glympse/android/lib/Debug;->iu:Lcom/glympse/android/hal/GContextHolder;

    const/4 v1, 0x1

    invoke-static {v0, p0, p1, v1}, Lcom/glympse/android/lib/fc;->a(Lcom/glympse/android/hal/GContextHolder;JZ)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/lib/Debug;->jO:Ljava/lang/String;

    .line 642
    sget-object v0, Lcom/glympse/android/lib/Debug;->jO:Ljava/lang/String;

    const-string v1, "log"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->buildFilepath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/lib/Debug;->jP:Ljava/lang/String;

    .line 643
    return-void
.end method

.method public static enableSignalHandlers(Z)V
    .locals 1

    .prologue
    .line 372
    sget-boolean v0, Lcom/glympse/android/lib/Debug;->jX:Z

    if-eq v0, p0, :cond_0

    sget-object v0, Lcom/glympse/android/lib/Debug;->iu:Lcom/glympse/android/hal/GContextHolder;

    if-nez v0, :cond_0

    .line 374
    sput-boolean p0, Lcom/glympse/android/lib/Debug;->jX:Z

    .line 376
    :cond_0
    return-void
.end method

.method public static ex(Ljava/lang/String;Z)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x4

    const/4 v1, 0x1

    .line 490
    if-nez p0, :cond_1

    .line 506
    :cond_0
    :goto_0
    return-void

    .line 496
    :cond_1
    sget v0, Lcom/glympse/android/lib/Debug;->jK:I

    if-lt v4, v0, :cond_4

    move v0, v1

    .line 497
    :goto_1
    sget v3, Lcom/glympse/android/lib/Debug;->jL:I

    if-lt v4, v3, :cond_2

    move v2, v1

    .line 498
    :cond_2
    if-nez v0, :cond_3

    if-eqz v2, :cond_0

    .line 503
    :cond_3
    const-string v3, "#######################################################################"

    invoke-static {v3}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v0, v2, v3, v1}, Lcom/glympse/android/lib/Debug;->a(IZZLjava/lang/String;Z)V

    .line 504
    invoke-static {v4, v0, v2, p0, v1}, Lcom/glympse/android/lib/Debug;->a(IZZLjava/lang/String;Z)V

    .line 505
    const-string v3, "#######################################################################"

    invoke-static {v3}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v0, v2, v3, v1}, Lcom/glympse/android/lib/Debug;->a(IZZLjava/lang/String;Z)V

    goto :goto_0

    :cond_4
    move v0, v2

    .line 496
    goto :goto_1
.end method

.method public static ex(Ljava/lang/Throwable;Z)V
    .locals 3

    .prologue
    .line 474
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x1000

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 475
    invoke-static {p0, v0}, Lcom/glympse/android/hal/DebugBase;->extractExceptionString(Ljava/lang/Throwable;Ljava/lang/StringBuilder;)V

    .line 476
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 479
    invoke-static {v0, p1}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/String;Z)V

    .line 482
    if-eqz p1, :cond_0

    if-eqz v0, :cond_0

    .line 484
    const-string v1, "err"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/glympse/android/lib/Debug;->reportError(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 486
    :cond_0
    return-void
.end method

.method public static ex(Z)V
    .locals 2

    .prologue
    .line 468
    const/4 v0, 0x0

    .line 469
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->ex(Ljava/lang/Throwable;Z)V

    .line 470
    return-void
.end method

.method public static formatLocation(Lcom/glympse/android/core/GLocation;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 423
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x50

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 424
    const-string v1, "\n[Location] ts:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 425
    invoke-interface {p0}, Lcom/glympse/android/core/GLocation;->getTime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 426
    const-string v1, " lat:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 427
    invoke-interface {p0}, Lcom/glympse/android/core/GLocation;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 428
    const-string v1, " lng:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 429
    invoke-interface {p0}, Lcom/glympse/android/core/GLocation;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 430
    const-string v1, " sp:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 431
    invoke-interface {p0}, Lcom/glympse/android/core/GLocation;->getSpeed()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 432
    const-string v1, " hd:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 433
    invoke-interface {p0}, Lcom/glympse/android/core/GLocation;->getBearing()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 434
    const-string v1, " alt:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 435
    invoke-interface {p0}, Lcom/glympse/android/core/GLocation;->getAltitude()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 436
    const-string v1, " hac:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 437
    invoke-interface {p0}, Lcom/glympse/android/core/GLocation;->getHAccuracy()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 438
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getDebugLoggingLevel()I
    .locals 1

    .prologue
    .line 313
    sget v0, Lcom/glympse/android/lib/Debug;->jK:I

    return v0
.end method

.method public static getFileLoggingLevel()I
    .locals 1

    .prologue
    .line 324
    sget v0, Lcom/glympse/android/lib/Debug;->jL:I

    return v0
.end method

.method public static getLevel()I
    .locals 2

    .prologue
    .line 332
    sget v0, Lcom/glympse/android/lib/Debug;->jL:I

    sget v1, Lcom/glympse/android/lib/Debug;->jK:I

    if-ge v0, v1, :cond_0

    sget v0, Lcom/glympse/android/lib/Debug;->jL:I

    :goto_0
    return v0

    :cond_0
    sget v0, Lcom/glympse/android/lib/Debug;->jK:I

    goto :goto_0
.end method

.method public static getLogFile(J)Ljava/lang/String;
    .locals 4

    .prologue
    .line 342
    sget-object v0, Lcom/glympse/android/lib/Debug;->jV:Lcom/glympse/android/hal/GMutex;

    invoke-interface {v0}, Lcom/glympse/android/hal/GMutex;->block()V

    .line 345
    :try_start_0
    sget-object v0, Lcom/glympse/android/lib/Debug;->jP:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 348
    invoke-static {p0, p1}, Lcom/glympse/android/lib/Debug;->e(J)V

    .line 351
    sget-object v0, Lcom/glympse/android/lib/Debug;->jO:Ljava/lang/String;

    sget-object v1, Lcom/glympse/android/lib/Debug;->jW:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v0, p0, p1, v1, v2}, Lcom/glympse/android/lib/fc;->a(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 354
    sget-object v1, Lcom/glympse/android/lib/Debug;->iu:Lcom/glympse/android/hal/GContextHolder;

    invoke-interface {v1}, Lcom/glympse/android/hal/GContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/glympse/android/lib/Debug;->jP:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3}, Lcom/glympse/android/hal/DebugBase;->writeTextFile(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 355
    sget-object v0, Lcom/glympse/android/lib/Debug;->iu:Lcom/glympse/android/hal/GContextHolder;

    invoke-interface {v0}, Lcom/glympse/android/hal/GContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/glympse/android/lib/Debug;->jP:Ljava/lang/String;

    const-string v2, "\n\n-----------------------------------------------------------------------\n"

    .line 356
    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    .line 355
    invoke-static {v0, v1, v2, v3}, Lcom/glympse/android/hal/DebugBase;->writeTextFile(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 359
    :cond_0
    invoke-static {}, Lcom/glympse/android/hal/DebugBase;->flushAllLogs()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 364
    :goto_0
    sget-object v0, Lcom/glympse/android/lib/Debug;->jV:Lcom/glympse/android/hal/GMutex;

    invoke-interface {v0}, Lcom/glympse/android/hal/GMutex;->unblock()V

    .line 366
    sget-object v0, Lcom/glympse/android/lib/Debug;->jP:Ljava/lang/String;

    return-object v0

    .line 361
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static init(Lcom/glympse/android/hal/GContextHolder;)V
    .locals 3

    .prologue
    .line 135
    sget-object v0, Lcom/glympse/android/lib/Debug;->jV:Lcom/glympse/android/hal/GMutex;

    invoke-interface {v0}, Lcom/glympse/android/hal/GMutex;->block()V

    .line 138
    :try_start_0
    sget-object v0, Lcom/glympse/android/lib/Debug;->iu:Lcom/glympse/android/hal/GContextHolder;

    if-nez v0, :cond_0

    .line 141
    sput-object p0, Lcom/glympse/android/lib/fc;->iu:Lcom/glympse/android/hal/GContextHolder;

    .line 144
    sput-object p0, Lcom/glympse/android/lib/Debug;->iu:Lcom/glympse/android/hal/GContextHolder;

    .line 145
    const/4 v0, 0x0

    sput-boolean v0, Lcom/glympse/android/lib/Debug;->jN:Z

    .line 146
    const/4 v0, 0x0

    sput-boolean v0, Lcom/glympse/android/lib/Debug;->jS:Z

    .line 147
    const/4 v0, 0x0

    sput-boolean v0, Lcom/glympse/android/lib/Debug;->jT:Z

    .line 148
    const/4 v0, 0x0

    sput-object v0, Lcom/glympse/android/lib/Debug;->jW:Ljava/lang/String;

    .line 151
    const/4 v0, 0x7

    sput v0, Lcom/glympse/android/lib/Debug;->jK:I

    .line 152
    const/4 v0, 0x7

    sput v0, Lcom/glympse/android/lib/Debug;->jL:I

    .line 153
    invoke-static {}, Lcom/glympse/android/lib/StaticConfig;->LOG_URL()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/lib/Debug;->jM:Ljava/lang/String;

    .line 157
    sget-object v0, Lcom/glympse/android/lib/Debug;->iu:Lcom/glympse/android/hal/GContextHolder;

    invoke-interface {v0}, Lcom/glympse/android/hal/GContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/glympse/android/hal/HalFactory;->openDirectory(Landroid/content/Context;Ljava/lang/String;Z)Lcom/glympse/android/hal/GDirectory;

    move-result-object v0

    if-nez v0, :cond_0

    .line 159
    sget-object v0, Lcom/glympse/android/lib/Debug;->iu:Lcom/glympse/android/hal/GContextHolder;

    invoke-interface {v0}, Lcom/glympse/android/hal/GContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/glympse/android/hal/HalFactory;->createDirectory(Landroid/content/Context;Ljava/lang/String;Z)Lcom/glympse/android/hal/GDirectory;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 167
    :cond_0
    :goto_0
    sget-object v0, Lcom/glympse/android/lib/Debug;->jV:Lcom/glympse/android/hal/GMutex;

    invoke-interface {v0}, Lcom/glympse/android/hal/GMutex;->unblock()V

    .line 168
    return-void

    .line 163
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static log(ILjava/lang/String;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 519
    const/high16 v0, 0x10000

    and-int/2addr v0, p0

    if-eqz v0, :cond_1

    move v0, v1

    .line 520
    :goto_0
    const v3, 0xffff

    and-int v4, p0, v3

    .line 523
    const/4 v3, 0x7

    if-lt v4, v3, :cond_2

    .line 537
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 519
    goto :goto_0

    .line 529
    :cond_2
    sget v3, Lcom/glympse/android/lib/Debug;->jK:I

    if-lt v4, v3, :cond_4

    move v3, v1

    :goto_2
    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    and-int/2addr v0, v3

    .line 530
    sget v3, Lcom/glympse/android/lib/Debug;->jL:I

    if-lt v4, v3, :cond_6

    .line 531
    :goto_4
    if-nez v0, :cond_3

    if-eqz v1, :cond_0

    .line 536
    :cond_3
    invoke-static {v4, v0, v1, p1, v2}, Lcom/glympse/android/lib/Debug;->a(IZZLjava/lang/String;Z)V

    goto :goto_1

    :cond_4
    move v3, v2

    .line 529
    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_3

    :cond_6
    move v1, v2

    .line 530
    goto :goto_4
.end method

.method public static overrideDebugLoggingLevel(I)V
    .locals 1

    .prologue
    .line 307
    sput p0, Lcom/glympse/android/lib/Debug;->jK:I

    .line 308
    const/4 v0, 0x1

    sput-boolean v0, Lcom/glympse/android/lib/Debug;->jJ:Z

    .line 309
    return-void
.end method

.method public static overrideFileLoggingLevel(I)V
    .locals 1

    .prologue
    .line 318
    sput p0, Lcom/glympse/android/lib/Debug;->jL:I

    .line 319
    const/4 v0, 0x1

    sput-boolean v0, Lcom/glympse/android/lib/Debug;->jJ:Z

    .line 320
    return-void
.end method

.method public static overrideLoggingLevels(II)V
    .locals 1

    .prologue
    .line 300
    sput p0, Lcom/glympse/android/lib/Debug;->jL:I

    .line 301
    sput p1, Lcom/glympse/android/lib/Debug;->jK:I

    .line 302
    const/4 v0, 0x1

    sput-boolean v0, Lcom/glympse/android/lib/Debug;->jJ:Z

    .line 303
    return-void
.end method

.method public static report(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 447
    const-string v0, "???"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 450
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x1000

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 451
    const-string v2, "\nException type:    AssertCondition"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 452
    const-string v2, "\nException name:    "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-nez p0, :cond_0

    move-object p0, v0

    :cond_0
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 453
    const-string v2, "\nReason:            "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-nez p1, :cond_1

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 456
    invoke-static {v1}, Lcom/glympse/android/hal/DebugBase;->appendCurrentStack(Ljava/lang/StringBuilder;)V

    .line 459
    invoke-static {v1}, Lcom/glympse/android/hal/DebugBase;->appendBinaryImages(Ljava/lang/StringBuilder;)V

    .line 462
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 463
    const-string v1, "err"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/glympse/android/lib/Debug;->reportError(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 464
    return-void

    :cond_1
    move-object v0, p1

    .line 453
    goto :goto_0
.end method

.method public static reportError(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 674
    sget-object v0, Lcom/glympse/android/lib/Debug;->iu:Lcom/glympse/android/hal/GContextHolder;

    if-nez v0, :cond_1

    .line 714
    :cond_0
    :goto_0
    return-void

    .line 680
    :cond_1
    invoke-static {}, Lcom/glympse/android/hal/Concurrent;->getTime()J

    move-result-wide v0

    .line 683
    sget-object v2, Lcom/glympse/android/lib/Debug;->iu:Lcom/glympse/android/hal/GContextHolder;

    invoke-static {v2, v0, v1, v4}, Lcom/glympse/android/lib/fc;->a(Lcom/glympse/android/hal/GContextHolder;JZ)Ljava/lang/String;

    move-result-object v2

    .line 687
    if-eqz p2, :cond_2

    .line 689
    sget-object v3, Lcom/glympse/android/lib/Debug;->jU:Lcom/glympse/android/hal/GVector;

    invoke-static {v3, v4}, Lcom/glympse/android/lib/fc;->a(Lcom/glympse/android/hal/GVector;Z)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/glympse/android/lib/Debug;->jW:Ljava/lang/String;

    .line 693
    :cond_2
    invoke-static {p0}, Lcom/glympse/android/hal/DebugBase;->originInGlympseCode(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 699
    sget-object v3, Lcom/glympse/android/lib/Debug;->jW:Ljava/lang/String;

    invoke-static {v2, v0, v1, v3, p0}, Lcom/glympse/android/lib/fc;->a(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 702
    sget-object v1, Lcom/glympse/android/lib/Debug;->iu:Lcom/glympse/android/hal/GContextHolder;

    .line 703
    invoke-interface {v1}, Lcom/glympse/android/hal/GContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 704
    invoke-static {v2, p1}, Lcom/glympse/android/lib/Debug;->buildFilepath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    .line 702
    invoke-static {v1, v2, v0, v3}, Lcom/glympse/android/hal/DebugBase;->writeTextFile(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 706
    if-eqz p2, :cond_0

    .line 709
    invoke-static {}, Lcom/glympse/android/lib/Debug;->aV()V

    .line 712
    invoke-static {v4}, Lcom/glympse/android/lib/Debug;->b(Z)V

    goto :goto_0
.end method

.method public static start(Lcom/glympse/android/lib/GGlympsePrivate;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 175
    sget-object v1, Lcom/glympse/android/lib/Debug;->jV:Lcom/glympse/android/hal/GMutex;

    invoke-interface {v1}, Lcom/glympse/android/hal/GMutex;->block()V

    .line 183
    :try_start_0
    sget-object v1, Lcom/glympse/android/lib/Debug;->jU:Lcom/glympse/android/hal/GVector;

    if-nez v1, :cond_0

    .line 185
    const/4 v0, 0x1

    .line 188
    new-instance v1, Lcom/glympse/android/hal/GVector;

    invoke-direct {v1}, Lcom/glympse/android/hal/GVector;-><init>()V

    sput-object v1, Lcom/glympse/android/lib/Debug;->jU:Lcom/glympse/android/hal/GVector;

    .line 192
    :cond_0
    sget-object v1, Lcom/glympse/android/lib/Debug;->jU:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v1, p0}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 195
    if-eqz v0, :cond_2

    .line 198
    invoke-interface {p0}, Lcom/glympse/android/lib/GGlympsePrivate;->getConfig()Lcom/glympse/android/api/GConfig;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GConfigPrivate;

    .line 199
    sget-boolean v1, Lcom/glympse/android/lib/Debug;->jJ:Z

    if-nez v1, :cond_1

    .line 201
    invoke-interface {v0}, Lcom/glympse/android/lib/GConfigPrivate;->getDebugLevel()J

    move-result-wide v2

    long-to-int v1, v2

    sput v1, Lcom/glympse/android/lib/Debug;->jK:I

    .line 202
    invoke-interface {v0}, Lcom/glympse/android/lib/GConfigPrivate;->getFileLevel()J

    move-result-wide v2

    long-to-int v1, v2

    sput v1, Lcom/glympse/android/lib/Debug;->jL:I

    .line 204
    :cond_1
    invoke-interface {v0}, Lcom/glympse/android/lib/GConfigPrivate;->getLogUrl()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/glympse/android/lib/Debug;->jM:Ljava/lang/String;

    .line 205
    invoke-interface {v0}, Lcom/glympse/android/lib/GConfigPrivate;->getLogUploadFrequency()J

    move-result-wide v2

    sput-wide v2, Lcom/glympse/android/lib/Debug;->jQ:J

    .line 206
    invoke-interface {v0}, Lcom/glympse/android/lib/GConfigPrivate;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/lib/fc;->gh:Ljava/lang/String;

    .line 210
    sget-object v0, Lcom/glympse/android/lib/Debug;->jU:Lcom/glympse/android/hal/GVector;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/glympse/android/lib/fc;->a(Lcom/glympse/android/hal/GVector;Z)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/lib/Debug;->jW:Ljava/lang/String;

    .line 213
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->a(ZZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 220
    :cond_2
    :goto_0
    sget-object v0, Lcom/glympse/android/lib/Debug;->jV:Lcom/glympse/android/hal/GMutex;

    invoke-interface {v0}, Lcom/glympse/android/hal/GMutex;->unblock()V

    .line 221
    return-void

    .line 216
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static stop(Lcom/glympse/android/lib/GGlympsePrivate;)V
    .locals 1

    .prologue
    .line 228
    sget-object v0, Lcom/glympse/android/lib/Debug;->jV:Lcom/glympse/android/hal/GMutex;

    invoke-interface {v0}, Lcom/glympse/android/hal/GMutex;->block()V

    .line 232
    :try_start_0
    sget-object v0, Lcom/glympse/android/lib/Debug;->jU:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, p0}, Lcom/glympse/android/hal/GVector;->removeElement(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 237
    :goto_0
    sget-object v0, Lcom/glympse/android/lib/Debug;->jV:Lcom/glympse/android/hal/GMutex;

    invoke-interface {v0}, Lcom/glympse/android/hal/GMutex;->unblock()V

    .line 238
    return-void

    .line 234
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static updateLevels(Lcom/glympse/android/lib/GConfigPrivate;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const-wide/16 v4, 0x0

    .line 246
    sget-object v0, Lcom/glympse/android/lib/Debug;->jV:Lcom/glympse/android/hal/GMutex;

    invoke-interface {v0}, Lcom/glympse/android/hal/GMutex;->block()V

    .line 249
    sget-boolean v0, Lcom/glympse/android/lib/Debug;->jJ:Z

    if-nez v0, :cond_1

    .line 251
    invoke-interface {p0}, Lcom/glympse/android/lib/GConfigPrivate;->getFileLevel()J

    move-result-wide v0

    .line 252
    cmp-long v2, v4, v0

    if-eqz v2, :cond_0

    sget v2, Lcom/glympse/android/lib/Debug;->jL:I

    int-to-long v2, v2

    cmp-long v2, v2, v0

    if-eqz v2, :cond_0

    .line 254
    long-to-int v0, v0

    sput v0, Lcom/glympse/android/lib/Debug;->jL:I

    .line 257
    sget-object v0, Lcom/glympse/android/lib/Debug;->jU:Lcom/glympse/android/hal/GVector;

    invoke-static {v0, v6}, Lcom/glympse/android/lib/fc;->a(Lcom/glympse/android/hal/GVector;Z)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/glympse/android/lib/Debug;->jW:Ljava/lang/String;

    .line 263
    const/4 v0, 0x0

    :try_start_0
    sput-object v0, Lcom/glympse/android/lib/Debug;->jP:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 269
    :cond_0
    :goto_0
    invoke-interface {p0}, Lcom/glympse/android/lib/GConfigPrivate;->getFileLevel()J

    move-result-wide v0

    .line 270
    cmp-long v2, v4, v0

    if-eqz v2, :cond_1

    .line 272
    long-to-int v0, v0

    sput v0, Lcom/glympse/android/lib/Debug;->jK:I

    .line 275
    :cond_1
    invoke-interface {p0}, Lcom/glympse/android/lib/GConfigPrivate;->getLogUrl()Ljava/lang/String;

    move-result-object v0

    .line 276
    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 278
    sput-object v0, Lcom/glympse/android/lib/Debug;->jM:Ljava/lang/String;

    .line 281
    :cond_2
    invoke-interface {p0}, Lcom/glympse/android/lib/GConfigPrivate;->getLogUploadFrequency()J

    move-result-wide v0

    .line 282
    sget-wide v2, Lcom/glympse/android/lib/Debug;->jQ:J

    cmp-long v2, v0, v2

    if-eqz v2, :cond_3

    .line 284
    sput-wide v0, Lcom/glympse/android/lib/Debug;->jQ:J

    .line 285
    sget-wide v0, Lcom/glympse/android/lib/Debug;->jQ:J

    cmp-long v0, v4, v0

    if-nez v0, :cond_3

    .line 288
    const/4 v0, 0x0

    invoke-static {v6, v0}, Lcom/glympse/android/lib/Debug;->a(ZZ)V

    .line 291
    :cond_3
    sget-object v0, Lcom/glympse/android/lib/Debug;->jV:Lcom/glympse/android/hal/GMutex;

    invoke-interface {v0}, Lcom/glympse/android/hal/GMutex;->unblock()V

    .line 292
    return-void

    .line 265
    :catch_0
    move-exception v0

    goto :goto_0
.end method
