.class Lcom/glympse/android/lib/k;
.super Ljava/lang/Object;
.source "ApplicationsList.java"

# interfaces
.implements Lcom/glympse/android/core/GCommon;


# static fields
.field public static final FLAG_ACTIVITY_NEW_TASK:I = 0x10000000


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/glympse/android/lib/GGlympsePrivate;Ljava/lang/String;)Lcom/glympse/android/api/GImage;
    .locals 2

    .prologue
    .line 136
    new-instance v0, Lcom/glympse/android/lib/cq;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/glympse/android/lib/cq;-><init>(Ljava/lang/String;Lcom/glympse/android/core/GDrawable;)V

    .line 137
    invoke-interface {p0}, Lcom/glympse/android/lib/GGlympsePrivate;->getImageCache()Lcom/glympse/android/lib/GImageCache;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GImagePrivate;->attachCache(Lcom/glympse/android/lib/GImageCache;)V

    .line 138
    return-object v0
.end method

.method public static a(Lcom/glympse/android/lib/GGlympsePrivate;)Lcom/glympse/android/hal/GVector;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/glympse/android/lib/GGlympsePrivate;",
            ")",
            "Lcom/glympse/android/hal/GVector",
            "<",
            "Lcom/glympse/android/api/GApplication;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    new-instance v0, Lcom/glympse/android/hal/GVector;

    invoke-direct {v0}, Lcom/glympse/android/hal/GVector;-><init>()V

    .line 31
    invoke-static {}, Lcom/glympse/android/hal/Platform;->getOsName()Ljava/lang/String;

    move-result-object v1

    .line 32
    const-string v2, "ios"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 34
    invoke-static {p0, v0}, Lcom/glympse/android/lib/k;->a(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/hal/GVector;)V

    .line 41
    :cond_0
    :goto_0
    return-object v0

    .line 36
    :cond_1
    const-string v2, "android"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 38
    invoke-static {p0, v0}, Lcom/glympse/android/lib/k;->b(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/hal/GVector;)V

    goto :goto_0
.end method

.method private static a(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/hal/GVector;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/glympse/android/lib/GGlympsePrivate;",
            "Lcom/glympse/android/hal/GVector",
            "<",
            "Lcom/glympse/android/api/GApplication;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 70
    new-instance v0, Lcom/glympse/android/lib/j;

    invoke-direct {v0}, Lcom/glympse/android/lib/j;-><init>()V

    .line 71
    const-string v1, "messagext"

    invoke-static {v1}, Lcom/glympse/android/lib/k;->str(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GApplicationPrivate;->setSubtype(Ljava/lang/String;)V

    .line 72
    const-string v1, "iMessage App Extension"

    invoke-static {v1}, Lcom/glympse/android/lib/k;->str(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GApplicationPrivate;->setName(Ljava/lang/String;)V

    .line 73
    const-string v1, "http://cdn.glympse.cc/icons/imessage.png"

    invoke-static {v1}, Lcom/glympse/android/lib/k;->str(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/glympse/android/lib/k;->a(Lcom/glympse/android/lib/GGlympsePrivate;Ljava/lang/String;)Lcom/glympse/android/api/GImage;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GApplicationPrivate;->setIcon(Lcom/glympse/android/api/GImage;)V

    .line 74
    invoke-interface {v0, v2}, Lcom/glympse/android/lib/GApplicationPrivate;->setInstallScheme(Ljava/lang/String;)V

    .line 75
    invoke-interface {v0, v2}, Lcom/glympse/android/lib/GApplicationPrivate;->setInstallUri(Ljava/lang/String;)V

    .line 76
    const-string v1, "none"

    invoke-static {v1}, Lcom/glympse/android/lib/k;->str(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GApplicationPrivate;->setLaunchMode(Ljava/lang/String;)V

    .line 77
    invoke-interface {v0, v2}, Lcom/glympse/android/lib/GApplicationPrivate;->setLaunchUri(Ljava/lang/String;)V

    .line 78
    invoke-virtual {p1, v0}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 79
    return-void
.end method

.method private static b(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/hal/GVector;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/glympse/android/lib/GGlympsePrivate;",
            "Lcom/glympse/android/hal/GVector",
            "<",
            "Lcom/glympse/android/api/GApplication;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 117
    new-instance v0, Lcom/glympse/android/lib/j;

    invoke-direct {v0}, Lcom/glympse/android/lib/j;-><init>()V

    .line 118
    const-string v1, "messagext"

    invoke-static {v1}, Lcom/glympse/android/lib/k;->str(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GApplicationPrivate;->setSubtype(Ljava/lang/String;)V

    .line 119
    const-string v1, "iMessage App Extension"

    invoke-static {v1}, Lcom/glympse/android/lib/k;->str(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GApplicationPrivate;->setName(Ljava/lang/String;)V

    .line 120
    const-string v1, "http://cdn.glympse.cc/icons/imessage.png"

    invoke-static {v1}, Lcom/glympse/android/lib/k;->str(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/glympse/android/lib/k;->a(Lcom/glympse/android/lib/GGlympsePrivate;Ljava/lang/String;)Lcom/glympse/android/api/GImage;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GApplicationPrivate;->setIcon(Lcom/glympse/android/api/GImage;)V

    .line 121
    invoke-interface {v0, v2}, Lcom/glympse/android/lib/GApplicationPrivate;->setInstallScheme(Ljava/lang/String;)V

    .line 122
    invoke-interface {v0, v2}, Lcom/glympse/android/lib/GApplicationPrivate;->setInstallUri(Ljava/lang/String;)V

    .line 123
    const-string v1, "none"

    invoke-static {v1}, Lcom/glympse/android/lib/k;->str(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GApplicationPrivate;->setLaunchMode(Ljava/lang/String;)V

    .line 124
    invoke-interface {v0, v2}, Lcom/glympse/android/lib/GApplicationPrivate;->setLaunchUri(Ljava/lang/String;)V

    .line 125
    invoke-virtual {p1, v0}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 126
    return-void
.end method

.method public static str(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 144
    return-object p0
.end method
