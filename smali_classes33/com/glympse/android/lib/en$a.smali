.class Lcom/glympse/android/lib/en$a;
.super Ljava/lang/Object;
.source "PlatformArrivalProvider.java"

# interfaces
.implements Lcom/glympse/android/api/GEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/lib/en;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private sg:Lcom/glympse/android/lib/en;

.field private sh:Lcom/glympse/android/api/GLocationManager;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 242
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/glympse/android/lib/en$1;)V
    .locals 0

    .prologue
    .line 242
    invoke-direct {p0}, Lcom/glympse/android/lib/en$a;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/glympse/android/api/GLocationManager;Lcom/glympse/android/lib/en;)V
    .locals 2

    .prologue
    .line 249
    iput-object p1, p0, Lcom/glympse/android/lib/en$a;->sh:Lcom/glympse/android/api/GLocationManager;

    .line 250
    iput-object p2, p0, Lcom/glympse/android/lib/en$a;->sg:Lcom/glympse/android/lib/en;

    .line 252
    iget-object v0, p0, Lcom/glympse/android/lib/en$a;->sh:Lcom/glympse/android/api/GLocationManager;

    invoke-virtual {p0}, Lcom/glympse/android/lib/en$a;->ar()Lcom/glympse/android/api/GEventListener;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/api/GLocationManager;->addListener(Lcom/glympse/android/api/GEventListener;)Z

    .line 253
    return-void
.end method

.method protected ar()Lcom/glympse/android/api/GEventListener;
    .locals 1

    .prologue
    .line 275
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GEventListener;

    return-object v0
.end method

.method public eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 262
    const/16 v0, 0x8

    if-ne v0, p2, :cond_0

    .line 264
    and-int/lit8 v0, p3, 0x10

    if-eqz v0, :cond_0

    .line 267
    check-cast p4, Lcom/glympse/android/core/GRegion;

    .line 268
    iget-object v0, p0, Lcom/glympse/android/lib/en$a;->sg:Lcom/glympse/android/lib/en;

    invoke-virtual {v0, p4}, Lcom/glympse/android/lib/en;->c(Lcom/glympse/android/core/GRegion;)V

    .line 271
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 257
    iget-object v0, p0, Lcom/glympse/android/lib/en$a;->sh:Lcom/glympse/android/api/GLocationManager;

    invoke-virtual {p0}, Lcom/glympse/android/lib/en$a;->ar()Lcom/glympse/android/api/GEventListener;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/api/GLocationManager;->removeListener(Lcom/glympse/android/api/GEventListener;)Z

    .line 258
    return-void
.end method
