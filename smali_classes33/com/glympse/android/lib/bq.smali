.class Lcom/glympse/android/lib/bq;
.super Lcom/glympse/android/lib/e;
.source "GetPairingCode.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/lib/bq$a;
    }
.end annotation


# instance fields
.field private fW:Ljava/lang/String;

.field private fY:Lcom/glympse/android/core/GPrimitive;

.field private gb:Lcom/glympse/android/lib/GAccountListener;

.field private id:Lcom/glympse/android/lib/GPairingManagerPrivate;

.field private ls:Lcom/glympse/android/lib/bq$a;


# direct methods
.method public constructor <init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/GAccountListener;Lcom/glympse/android/core/GPrimitive;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/glympse/android/lib/e;-><init>()V

    .line 28
    invoke-interface {p1}, Lcom/glympse/android/lib/GGlympsePrivate;->getPairingManager()Lcom/glympse/android/api/GPairingManager;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GPairingManagerPrivate;

    iput-object v0, p0, Lcom/glympse/android/lib/bq;->id:Lcom/glympse/android/lib/GPairingManagerPrivate;

    .line 29
    iput-object p2, p0, Lcom/glympse/android/lib/bq;->gb:Lcom/glympse/android/lib/GAccountListener;

    .line 30
    invoke-interface {p1}, Lcom/glympse/android/lib/GGlympsePrivate;->getApiKey()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/bq;->fW:Ljava/lang/String;

    .line 31
    new-instance v0, Lcom/glympse/android/lib/bq$a;

    invoke-direct {v0}, Lcom/glympse/android/lib/bq$a;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/bq;->ls:Lcom/glympse/android/lib/bq$a;

    .line 32
    iget-object v0, p0, Lcom/glympse/android/lib/bq;->ls:Lcom/glympse/android/lib/bq$a;

    iput-object v0, p0, Lcom/glympse/android/lib/bq;->gB:Lcom/glympse/android/lib/f;

    .line 33
    iput-object p3, p0, Lcom/glympse/android/lib/bq;->fY:Lcom/glympse/android/core/GPrimitive;

    .line 34
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 77
    new-instance v0, Lcom/glympse/android/lib/bq$a;

    invoke-direct {v0}, Lcom/glympse/android/lib/bq$a;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/bq;->ls:Lcom/glympse/android/lib/bq$a;

    .line 78
    iget-object v0, p0, Lcom/glympse/android/lib/bq;->ls:Lcom/glympse/android/lib/bq$a;

    iput-object v0, p0, Lcom/glympse/android/lib/bq;->gB:Lcom/glympse/android/lib/f;

    .line 79
    return-void
.end method

.method public methodType()I
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x1

    return v0
.end method

.method public process()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 54
    iget-object v1, p0, Lcom/glympse/android/lib/bq;->ls:Lcom/glympse/android/lib/bq$a;

    iget-object v1, v1, Lcom/glympse/android/lib/bq$a;->gF:Ljava/lang/String;

    const-string v2, "ok"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 57
    iget-object v1, p0, Lcom/glympse/android/lib/bq;->fY:Lcom/glympse/android/core/GPrimitive;

    const-string v2, "code"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/glympse/android/lib/bq;->ls:Lcom/glympse/android/lib/bq$a;

    iget-object v3, v3, Lcom/glympse/android/lib/bq$a;->ie:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    iget-object v1, p0, Lcom/glympse/android/lib/bq;->fY:Lcom/glympse/android/core/GPrimitive;

    const-string v2, "expires"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/glympse/android/lib/bq;->ls:Lcom/glympse/android/lib/bq$a;

    iget-wide v4, v3, Lcom/glympse/android/lib/bq$a;->lt:J

    invoke-interface {v1, v2, v4, v5}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 59
    iget-object v1, p0, Lcom/glympse/android/lib/bq;->id:Lcom/glympse/android/lib/GPairingManagerPrivate;

    iget-object v2, p0, Lcom/glympse/android/lib/bq;->ls:Lcom/glympse/android/lib/bq$a;

    iget-object v2, v2, Lcom/glympse/android/lib/bq$a;->ie:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/glympse/android/lib/GPairingManagerPrivate;->getCodeSucceeded(Ljava/lang/String;)V

    .line 72
    :goto_0
    return v0

    .line 63
    :cond_0
    new-instance v1, Lcom/glympse/android/lib/ex;

    iget-object v2, p0, Lcom/glympse/android/lib/bq;->ls:Lcom/glympse/android/lib/bq$a;

    iget-object v2, v2, Lcom/glympse/android/lib/bq$a;->gG:Ljava/lang/String;

    iget-object v3, p0, Lcom/glympse/android/lib/bq;->ls:Lcom/glympse/android/lib/bq$a;

    iget-object v3, v3, Lcom/glympse/android/lib/bq$a;->gH:Ljava/lang/String;

    invoke-direct {v1, v0, v2, v3}, Lcom/glympse/android/lib/ex;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 66
    iget-object v2, p0, Lcom/glympse/android/lib/bq;->id:Lcom/glympse/android/lib/GPairingManagerPrivate;

    invoke-interface {v2, v1}, Lcom/glympse/android/lib/GPairingManagerPrivate;->getCodeFailed(Lcom/glympse/android/api/GServerError;)V

    .line 69
    iget-object v2, p0, Lcom/glympse/android/lib/bq;->gb:Lcom/glympse/android/lib/GAccountListener;

    const/4 v3, 0x4

    invoke-interface {v2, v0, v3, v1}, Lcom/glympse/android/lib/GAccountListener;->failedToCreate(ZILcom/glympse/android/api/GServerError;)V

    .line 70
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public url(Ljava/lang/StringBuilder;)Z
    .locals 1

    .prologue
    .line 42
    const-string v0, "account/pairing?api_key="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    iget-object v0, p0, Lcom/glympse/android/lib/bq;->fW:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    const/4 v0, 0x1

    return v0
.end method
