.class public Lcom/glympse/android/lib/Primitive;
.super Ljava/lang/Object;
.source "Primitive.java"

# interfaces
.implements Lcom/glympse/android/core/GPrimitive;


# instance fields
.field private _bool:Z

.field private _type:I

.field private si:Lcom/glympse/android/lib/cl;

.field private sj:Lcom/glympse/android/hal/GVector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GVector",
            "<",
            "Lcom/glympse/android/core/GPrimitive;",
            ">;"
        }
    .end annotation
.end field

.field private sk:Lcom/glympse/android/hal/GHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GHashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/glympse/android/core/GPrimitive;",
            ">;"
        }
    .end annotation
.end field

.field private sl:J

.field private sm:D

.field private sn:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    const/16 v0, 0x40

    iput v0, p0, Lcom/glympse/android/lib/Primitive;->_type:I

    .line 74
    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    iput-wide v0, p0, Lcom/glympse/android/lib/Primitive;->sm:D

    .line 75
    return-void
.end method

.method public constructor <init>(D)V
    .locals 3

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const/4 v0, 0x4

    iput v0, p0, Lcom/glympse/android/lib/Primitive;->_type:I

    .line 48
    iput-wide p1, p0, Lcom/glympse/android/lib/Primitive;->sm:D

    .line 49
    double-to-long v0, p1

    iput-wide v0, p0, Lcom/glympse/android/lib/Primitive;->sl:J

    .line 50
    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput p1, p0, Lcom/glympse/android/lib/Primitive;->_type:I

    .line 41
    const/4 v0, 0x1

    if-ne v0, p1, :cond_1

    new-instance v0, Lcom/glympse/android/hal/GVector;

    invoke-direct {v0}, Lcom/glympse/android/hal/GVector;-><init>()V

    :goto_0
    iput-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    .line 42
    const/4 v0, 0x2

    if-ne v0, p1, :cond_0

    new-instance v1, Lcom/glympse/android/hal/GHashtable;

    invoke-direct {v1}, Lcom/glympse/android/hal/GHashtable;-><init>()V

    :cond_0
    iput-object v1, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    .line 43
    return-void

    :cond_1
    move-object v0, v1

    .line 41
    goto :goto_0
.end method

.method public constructor <init>(J)V
    .locals 3

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    const/16 v0, 0x8

    iput v0, p0, Lcom/glympse/android/lib/Primitive;->_type:I

    .line 55
    iput-wide p1, p0, Lcom/glympse/android/lib/Primitive;->sl:J

    .line 56
    long-to-double v0, p1

    iput-wide v0, p0, Lcom/glympse/android/lib/Primitive;->sm:D

    .line 57
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    const/16 v0, 0x20

    iput v0, p0, Lcom/glympse/android/lib/Primitive;->_type:I

    .line 68
    iput-object p1, p0, Lcom/glympse/android/lib/Primitive;->sn:Ljava/lang/String;

    .line 69
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    const/16 v0, 0x10

    iput v0, p0, Lcom/glympse/android/lib/Primitive;->_type:I

    .line 62
    iput-boolean p1, p0, Lcom/glympse/android/lib/Primitive;->_bool:Z

    .line 63
    return-void
.end method

.method private static b(Lcom/glympse/android/core/GPrimitive;Lcom/glympse/android/core/GPrimitive;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 259
    .line 260
    invoke-interface {p0}, Lcom/glympse/android/core/GPrimitive;->type()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 268
    invoke-interface {p1}, Lcom/glympse/android/core/GPrimitive;->type()I

    move-result v2

    invoke-interface {p0}, Lcom/glympse/android/core/GPrimitive;->type()I

    move-result v3

    if-ne v2, v3, :cond_0

    .line 290
    :goto_0
    :pswitch_0
    return v0

    :cond_0
    move v0, v1

    .line 268
    goto :goto_0

    .line 274
    :pswitch_1
    invoke-interface {p1}, Lcom/glympse/android/core/GPrimitive;->type()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 277
    invoke-interface {p1}, Lcom/glympse/android/core/GPrimitive;->getKeys()Ljava/util/Enumeration;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 279
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 280
    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v3

    .line 281
    invoke-interface {p0, v0}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 282
    if-eqz v0, :cond_1

    .line 284
    invoke-static {v0, v3}, Lcom/glympse/android/lib/Primitive;->b(Lcom/glympse/android/core/GPrimitive;Lcom/glympse/android/core/GPrimitive;)Z

    move-result v0

    and-int/2addr v0, v1

    :goto_2
    move v1, v0

    .line 286
    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_0

    .line 260
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public clone()Lcom/glympse/android/core/GPrimitive;
    .locals 5

    .prologue
    .line 130
    new-instance v2, Lcom/glympse/android/lib/Primitive;

    iget v0, p0, Lcom/glympse/android/lib/Primitive;->_type:I

    invoke-direct {v2, v0}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 132
    iget-wide v0, p0, Lcom/glympse/android/lib/Primitive;->sl:J

    iput-wide v0, v2, Lcom/glympse/android/lib/Primitive;->sl:J

    .line 133
    iget-boolean v0, p0, Lcom/glympse/android/lib/Primitive;->_bool:Z

    iput-boolean v0, v2, Lcom/glympse/android/lib/Primitive;->_bool:Z

    .line 134
    iget-wide v0, p0, Lcom/glympse/android/lib/Primitive;->sm:D

    iput-wide v0, v2, Lcom/glympse/android/lib/Primitive;->sm:D

    .line 135
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sn:Ljava/lang/String;

    iput-object v0, v2, Lcom/glympse/android/lib/Primitive;->sn:Ljava/lang/String;

    .line 138
    iget v0, p0, Lcom/glympse/android/lib/Primitive;->_type:I

    packed-switch v0, :pswitch_data_0

    .line 168
    :cond_0
    return-object v2

    .line 143
    :pswitch_0
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v3

    .line 145
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 147
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GPrimitive;

    .line 149
    iget-object v4, v2, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    invoke-interface {v0}, Lcom/glympse/android/core/GPrimitive;->clone()Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 145
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 157
    :pswitch_1
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GHashtable;->keys()Ljava/util/Enumeration;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 160
    iget-object v1, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v1, v0}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/glympse/android/core/GPrimitive;

    .line 162
    iget-object v4, v2, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    invoke-interface {v1}, Lcom/glympse/android/core/GPrimitive;->clone()Lcom/glympse/android/core/GPrimitive;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 138
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 16
    invoke-virtual {p0}, Lcom/glympse/android/lib/Primitive;->clone()Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 750
    check-cast p1, Lcom/glympse/android/core/GCommon;

    invoke-virtual {p0, p1}, Lcom/glympse/android/lib/Primitive;->isEqual(Lcom/glympse/android/core/GCommon;)Z

    move-result v0

    return v0
.end method

.method public get(I)Lcom/glympse/android/core/GPrimitive;
    .locals 1

    .prologue
    .line 393
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v0

    if-gt v0, p1, :cond_1

    .line 394
    :cond_0
    const/4 v0, 0x0

    .line 395
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GPrimitive;

    goto :goto_0
.end method

.method public get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    if-nez v0, :cond_0

    .line 324
    const/4 v0, 0x0

    .line 325
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GPrimitive;

    goto :goto_0
.end method

.method public getArray()Lcom/glympse/android/core/GArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/glympse/android/core/GArray",
            "<",
            "Lcom/glympse/android/core/GPrimitive;",
            ">;"
        }
    .end annotation

    .prologue
    .line 388
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    return-object v0
.end method

.method public getBool()Z
    .locals 1

    .prologue
    .line 309
    iget-boolean v0, p0, Lcom/glympse/android/lib/Primitive;->_bool:Z

    return v0
.end method

.method public getBool(I)Z
    .locals 1

    .prologue
    .line 416
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v0

    if-gt v0, p1, :cond_1

    .line 417
    :cond_0
    const/4 v0, 0x0

    .line 419
    :goto_0
    return v0

    .line 418
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GPrimitive;

    .line 419
    invoke-interface {v0}, Lcom/glympse/android/core/GPrimitive;->getBool()Z

    move-result v0

    goto :goto_0
.end method

.method public getBool(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 350
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    if-nez v0, :cond_0

    move v0, v1

    .line 355
    :goto_0
    return v0

    .line 352
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GPrimitive;

    .line 353
    if-nez v0, :cond_1

    move v0, v1

    .line 354
    goto :goto_0

    .line 355
    :cond_1
    invoke-interface {v0}, Lcom/glympse/android/core/GPrimitive;->getBool()Z

    move-result v0

    goto :goto_0
.end method

.method public getDouble()D
    .locals 2

    .prologue
    .line 299
    iget-wide v0, p0, Lcom/glympse/android/lib/Primitive;->sm:D

    return-wide v0
.end method

.method public getDouble(I)D
    .locals 2

    .prologue
    .line 400
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v0

    if-gt v0, p1, :cond_1

    .line 401
    :cond_0
    const-wide/16 v0, 0x0

    .line 403
    :goto_0
    return-wide v0

    .line 402
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GPrimitive;

    .line 403
    invoke-interface {v0}, Lcom/glympse/android/core/GPrimitive;->getDouble()D

    move-result-wide v0

    goto :goto_0
.end method

.method public getDouble(Ljava/lang/String;)D
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 330
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    if-nez v0, :cond_0

    move-wide v0, v2

    .line 335
    :goto_0
    return-wide v0

    .line 332
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GPrimitive;

    .line 333
    if-nez v0, :cond_1

    move-wide v0, v2

    .line 334
    goto :goto_0

    .line 335
    :cond_1
    invoke-interface {v0}, Lcom/glympse/android/core/GPrimitive;->getDouble()D

    move-result-wide v0

    goto :goto_0
.end method

.method public getKeys()Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Enumeration",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 370
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    if-nez v0, :cond_0

    .line 371
    const/4 v0, 0x0

    .line 372
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GHashtable;->keys()Ljava/util/Enumeration;

    move-result-object v0

    goto :goto_0
.end method

.method public getLong()J
    .locals 2

    .prologue
    .line 304
    iget-wide v0, p0, Lcom/glympse/android/lib/Primitive;->sl:J

    return-wide v0
.end method

.method public getLong(I)J
    .locals 2

    .prologue
    .line 408
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v0

    if-gt v0, p1, :cond_1

    .line 409
    :cond_0
    const-wide/16 v0, 0x0

    .line 411
    :goto_0
    return-wide v0

    .line 410
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GPrimitive;

    .line 411
    invoke-interface {v0}, Lcom/glympse/android/core/GPrimitive;->getLong()J

    move-result-wide v0

    goto :goto_0
.end method

.method public getLong(Ljava/lang/String;)J
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 340
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    if-nez v0, :cond_0

    move-wide v0, v2

    .line 345
    :goto_0
    return-wide v0

    .line 342
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GPrimitive;

    .line 343
    if-nez v0, :cond_1

    move-wide v0, v2

    .line 344
    goto :goto_0

    .line 345
    :cond_1
    invoke-interface {v0}, Lcom/glympse/android/core/GPrimitive;->getLong()J

    move-result-wide v0

    goto :goto_0
.end method

.method public getString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sn:Ljava/lang/String;

    return-object v0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 424
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v0

    if-gt v0, p1, :cond_1

    .line 425
    :cond_0
    const/4 v0, 0x0

    .line 427
    :goto_0
    return-object v0

    .line 426
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GPrimitive;

    .line 427
    invoke-interface {v0}, Lcom/glympse/android/core/GPrimitive;->getString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 360
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    if-nez v0, :cond_0

    move-object v0, v1

    .line 365
    :goto_0
    return-object v0

    .line 362
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GPrimitive;

    .line 363
    if-nez v0, :cond_1

    move-object v0, v1

    .line 364
    goto :goto_0

    .line 365
    :cond_1
    invoke-interface {v0}, Lcom/glympse/android/core/GPrimitive;->getString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public hasKey(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    if-nez v0, :cond_0

    .line 378
    const/4 v0, 0x0

    .line 379
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 755
    iget-object v1, p0, Lcom/glympse/android/lib/Primitive;->si:Lcom/glympse/android/lib/cl;

    if-nez v1, :cond_0

    .line 757
    new-instance v1, Lcom/glympse/android/lib/cl;

    invoke-direct {v1}, Lcom/glympse/android/lib/cl;-><init>()V

    iput-object v1, p0, Lcom/glympse/android/lib/Primitive;->si:Lcom/glympse/android/lib/cl;

    .line 759
    iget v1, p0, Lcom/glympse/android/lib/Primitive;->_type:I

    sparse-switch v1, :sswitch_data_0

    .line 807
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->si:Lcom/glympse/android/lib/cl;

    invoke-virtual {v0}, Lcom/glympse/android/lib/cl;->bZ()I

    move-result v0

    return v0

    .line 763
    :sswitch_0
    iget-object v1, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v1}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v2

    move v1, v0

    .line 764
    :goto_1
    if-ge v1, v2, :cond_0

    .line 766
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GPrimitive;

    .line 767
    iget-object v3, p0, Lcom/glympse/android/lib/Primitive;->si:Lcom/glympse/android/lib/cl;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-virtual {v3, v0}, Lcom/glympse/android/lib/cl;->append(I)V

    .line 764
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 773
    :sswitch_1
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GHashtable;->keys()Ljava/util/Enumeration;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 775
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GPrimitive;

    .line 776
    iget-object v2, p0, Lcom/glympse/android/lib/Primitive;->si:Lcom/glympse/android/lib/cl;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/glympse/android/lib/cl;->append(I)V

    goto :goto_2

    .line 782
    :sswitch_2
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->si:Lcom/glympse/android/lib/cl;

    iget-wide v2, p0, Lcom/glympse/android/lib/Primitive;->sm:D

    invoke-virtual {v0, v2, v3}, Lcom/glympse/android/lib/cl;->a(D)V

    goto :goto_0

    .line 787
    :sswitch_3
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->si:Lcom/glympse/android/lib/cl;

    iget-wide v2, p0, Lcom/glympse/android/lib/Primitive;->sl:J

    invoke-virtual {v0, v2, v3}, Lcom/glympse/android/lib/cl;->g(J)V

    goto :goto_0

    .line 792
    :sswitch_4
    iget-object v1, p0, Lcom/glympse/android/lib/Primitive;->si:Lcom/glympse/android/lib/cl;

    iget-boolean v2, p0, Lcom/glympse/android/lib/Primitive;->_bool:Z

    if-eqz v2, :cond_1

    :goto_3
    invoke-virtual {v1, v0}, Lcom/glympse/android/lib/cl;->append(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_3

    .line 797
    :sswitch_5
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->si:Lcom/glympse/android/lib/cl;

    iget-object v1, p0, Lcom/glympse/android/lib/Primitive;->sn:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/glympse/android/lib/cl;->append(Ljava/lang/String;)V

    goto :goto_0

    .line 802
    :sswitch_6
    iget-object v1, p0, Lcom/glympse/android/lib/Primitive;->si:Lcom/glympse/android/lib/cl;

    invoke-virtual {v1, v0}, Lcom/glympse/android/lib/cl;->append(I)V

    goto :goto_0

    .line 759
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x4 -> :sswitch_2
        0x8 -> :sswitch_3
        0x10 -> :sswitch_4
        0x20 -> :sswitch_5
        0x40 -> :sswitch_6
    .end sparse-switch
.end method

.method public insert(ILcom/glympse/android/core/GPrimitive;)V
    .locals 1

    .prologue
    .line 587
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v0

    if-ge v0, p1, :cond_1

    .line 590
    :cond_0
    :goto_0
    return-void

    .line 589
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, p2, p1}, Lcom/glympse/android/hal/GVector;->insertElementAt(Ljava/lang/Object;I)V

    goto :goto_0
.end method

.method public isArray()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 88
    iget v1, p0, Lcom/glympse/android/lib/Primitive;->_type:I

    if-ne v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isBool()Z
    .locals 2

    .prologue
    .line 108
    const/16 v0, 0x10

    iget v1, p0, Lcom/glympse/android/lib/Primitive;->_type:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDouble()Z
    .locals 2

    .prologue
    .line 98
    const/4 v0, 0x4

    iget v1, p0, Lcom/glympse/android/lib/Primitive;->_type:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEqual(Lcom/glympse/android/core/GCommon;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 654
    check-cast p1, Lcom/glympse/android/lib/Primitive;

    .line 655
    if-nez p1, :cond_1

    .line 741
    :cond_0
    :goto_0
    return v3

    .line 661
    :cond_1
    iget v0, p0, Lcom/glympse/android/lib/Primitive;->_type:I

    iget v1, p1, Lcom/glympse/android/lib/Primitive;->_type:I

    if-ne v0, v1, :cond_0

    .line 667
    iget v0, p0, Lcom/glympse/android/lib/Primitive;->_type:I

    sparse-switch v0, :sswitch_data_0

    :cond_2
    move v3, v2

    .line 741
    goto :goto_0

    .line 672
    :sswitch_0
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v5

    .line 673
    iget-object v0, p1, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v0

    if-ne v5, v0, :cond_0

    move v4, v3

    .line 679
    :goto_1
    if-ge v4, v5, :cond_2

    .line 681
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, v4}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GPrimitive;

    .line 682
    iget-object v1, p1, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v1, v4}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/glympse/android/core/GPrimitive;

    .line 685
    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->isEqual(Lcom/glympse/android/core/GCommon;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 679
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 697
    :sswitch_1
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GHashtable;->size()I

    move-result v0

    iget-object v1, p1, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v1}, Lcom/glympse/android/hal/GHashtable;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 705
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GHashtable;->keys()Ljava/util/Enumeration;

    move-result-object v4

    :cond_3
    invoke-interface {v4}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 707
    invoke-interface {v4}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 708
    iget-object v1, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v1, v0}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/glympse/android/core/GPrimitive;

    .line 709
    iget-object v5, p1, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v5, v0}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GPrimitive;

    .line 712
    if-eqz v0, :cond_0

    invoke-interface {v1, v0}, Lcom/glympse/android/core/GPrimitive;->isEqual(Lcom/glympse/android/core/GCommon;)Z

    move-result v0

    if-nez v0, :cond_3

    goto :goto_0

    .line 721
    :sswitch_2
    iget-wide v0, p0, Lcom/glympse/android/lib/Primitive;->sm:D

    iget-wide v4, p1, Lcom/glympse/android/lib/Primitive;->sm:D

    cmpl-double v0, v0, v4

    if-nez v0, :cond_4

    move v0, v2

    :goto_2
    move v3, v0

    goto :goto_0

    :cond_4
    move v0, v3

    goto :goto_2

    .line 725
    :sswitch_3
    iget-wide v0, p0, Lcom/glympse/android/lib/Primitive;->sl:J

    iget-wide v4, p1, Lcom/glympse/android/lib/Primitive;->sl:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_5

    :goto_3
    move v3, v2

    goto/16 :goto_0

    :cond_5
    move v2, v3

    goto :goto_3

    .line 729
    :sswitch_4
    iget-boolean v0, p0, Lcom/glympse/android/lib/Primitive;->_bool:Z

    iget-boolean v1, p1, Lcom/glympse/android/lib/Primitive;->_bool:Z

    if-ne v0, v1, :cond_6

    :goto_4
    move v3, v2

    goto/16 :goto_0

    :cond_6
    move v2, v3

    goto :goto_4

    .line 733
    :sswitch_5
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sn:Ljava/lang/String;

    iget-object v1, p1, Lcom/glympse/android/lib/Primitive;->sn:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/glympse/android/hal/Helpers;->safeEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    goto/16 :goto_0

    :sswitch_6
    move v3, v2

    .line 737
    goto/16 :goto_0

    .line 667
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x4 -> :sswitch_2
        0x8 -> :sswitch_3
        0x10 -> :sswitch_4
        0x20 -> :sswitch_5
        0x40 -> :sswitch_6
    .end sparse-switch
.end method

.method public isLong()Z
    .locals 2

    .prologue
    .line 103
    const/16 v0, 0x8

    iget v1, p0, Lcom/glympse/android/lib/Primitive;->_type:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNull()Z
    .locals 2

    .prologue
    .line 118
    const/16 v0, 0x40

    iget v1, p0, Lcom/glympse/android/lib/Primitive;->_type:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isObject()Z
    .locals 2

    .prologue
    .line 93
    const/4 v0, 0x2

    iget v1, p0, Lcom/glympse/android/lib/Primitive;->_type:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isString()Z
    .locals 2

    .prologue
    .line 113
    const/16 v0, 0x20

    iget v1, p0, Lcom/glympse/android/lib/Primitive;->_type:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public merge(Lcom/glympse/android/core/GPrimitive;Z)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 174
    if-nez p2, :cond_0

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GPrimitive;

    invoke-static {v0, p1}, Lcom/glympse/android/lib/Primitive;->b(Lcom/glympse/android/core/GPrimitive;Lcom/glympse/android/core/GPrimitive;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 251
    :goto_0
    return v0

    .line 179
    :cond_0
    iget v0, p0, Lcom/glympse/android/lib/Primitive;->_type:I

    if-ne v2, v0, :cond_3

    .line 182
    invoke-interface {p1}, Lcom/glympse/android/core/GPrimitive;->type()I

    move-result v0

    if-ne v2, v0, :cond_1

    .line 185
    invoke-interface {p1}, Lcom/glympse/android/core/GPrimitive;->getArray()Lcom/glympse/android/core/GArray;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/core/GArray;->elements()Ljava/util/Enumeration;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 187
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v0}, Lcom/glympse/android/core/GPrimitive;->clone()Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/Primitive;->put(Lcom/glympse/android/core/GPrimitive;)V

    goto :goto_1

    .line 192
    :cond_1
    invoke-virtual {p0, p1}, Lcom/glympse/android/lib/Primitive;->put(Lcom/glympse/android/core/GPrimitive;)V

    :cond_2
    move v0, v2

    .line 194
    goto :goto_0

    .line 196
    :cond_3
    if-nez p2, :cond_4

    iget v0, p0, Lcom/glympse/android/lib/Primitive;->_type:I

    invoke-interface {p1}, Lcom/glympse/android/core/GPrimitive;->type()I

    move-result v3

    if-ne v0, v3, :cond_8

    .line 199
    :cond_4
    invoke-interface {p1}, Lcom/glympse/android/core/GPrimitive;->type()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_5
    :goto_2
    move v0, v2

    .line 246
    goto :goto_0

    .line 202
    :sswitch_0
    invoke-virtual {p0}, Lcom/glympse/android/lib/Primitive;->setArray()V

    .line 203
    invoke-interface {p1}, Lcom/glympse/android/core/GPrimitive;->getArray()Lcom/glympse/android/core/GArray;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/core/GArray;->elements()Ljava/util/Enumeration;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 205
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GPrimitive;

    invoke-interface {v0}, Lcom/glympse/android/core/GPrimitive;->clone()Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/Primitive;->put(Lcom/glympse/android/core/GPrimitive;)V

    goto :goto_3

    .line 209
    :sswitch_1
    const/4 v0, 0x2

    iget v1, p0, Lcom/glympse/android/lib/Primitive;->_type:I

    if-eq v0, v1, :cond_6

    .line 211
    invoke-virtual {p0}, Lcom/glympse/android/lib/Primitive;->setObject()V

    .line 213
    :cond_6
    invoke-interface {p1}, Lcom/glympse/android/core/GPrimitive;->getKeys()Ljava/util/Enumeration;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 215
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 216
    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v3

    .line 217
    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/Primitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v4

    .line 218
    if-eqz v4, :cond_7

    .line 220
    invoke-interface {v4, v3, p2}, Lcom/glympse/android/core/GPrimitive;->merge(Lcom/glympse/android/core/GPrimitive;Z)Z

    goto :goto_4

    .line 224
    :cond_7
    invoke-interface {v3}, Lcom/glympse/android/core/GPrimitive;->clone()Lcom/glympse/android/core/GPrimitive;

    move-result-object v3

    invoke-virtual {p0, v0, v3}, Lcom/glympse/android/lib/Primitive;->put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    goto :goto_4

    .line 229
    :sswitch_2
    invoke-virtual {p0}, Lcom/glympse/android/lib/Primitive;->setNull()V

    goto :goto_2

    .line 232
    :sswitch_3
    invoke-interface {p1}, Lcom/glympse/android/core/GPrimitive;->getLong()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/glympse/android/lib/Primitive;->set(J)V

    goto :goto_2

    .line 235
    :sswitch_4
    invoke-interface {p1}, Lcom/glympse/android/core/GPrimitive;->getDouble()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/glympse/android/lib/Primitive;->set(D)V

    goto :goto_2

    .line 238
    :sswitch_5
    invoke-interface {p1}, Lcom/glympse/android/core/GPrimitive;->getString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/Primitive;->set(Ljava/lang/String;)V

    goto :goto_2

    .line 241
    :sswitch_6
    invoke-interface {p1}, Lcom/glympse/android/core/GPrimitive;->getBool()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/Primitive;->set(Z)V

    goto :goto_2

    :cond_8
    move v0, v1

    .line 251
    goto/16 :goto_0

    .line 199
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x4 -> :sswitch_4
        0x8 -> :sswitch_3
        0x10 -> :sswitch_6
        0x20 -> :sswitch_5
        0x40 -> :sswitch_2
    .end sparse-switch
.end method

.method public put(D)V
    .locals 3

    .prologue
    .line 559
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    if-nez v0, :cond_0

    .line 562
    :goto_0
    return-void

    .line 561
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    new-instance v1, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v1, p1, p2}, Lcom/glympse/android/lib/Primitive;-><init>(D)V

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public put(ID)V
    .locals 2

    .prologue
    .line 601
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v0

    if-gt v0, p1, :cond_1

    .line 604
    :cond_0
    :goto_0
    return-void

    .line 603
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    new-instance v1, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v1, p2, p3}, Lcom/glympse/android/lib/Primitive;-><init>(D)V

    invoke-virtual {v0, v1, p1}, Lcom/glympse/android/hal/GVector;->setElementAt(Ljava/lang/Object;I)V

    goto :goto_0
.end method

.method public put(IJ)V
    .locals 2

    .prologue
    .line 608
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v0

    if-gt v0, p1, :cond_1

    .line 611
    :cond_0
    :goto_0
    return-void

    .line 610
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    new-instance v1, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v1, p2, p3}, Lcom/glympse/android/lib/Primitive;-><init>(J)V

    invoke-virtual {v0, v1, p1}, Lcom/glympse/android/hal/GVector;->setElementAt(Ljava/lang/Object;I)V

    goto :goto_0
.end method

.method public put(ILcom/glympse/android/core/GPrimitive;)V
    .locals 1

    .prologue
    .line 594
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v0

    if-gt v0, p1, :cond_1

    .line 597
    :cond_0
    :goto_0
    return-void

    .line 596
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, p2, p1}, Lcom/glympse/android/hal/GVector;->setElementAt(Ljava/lang/Object;I)V

    goto :goto_0
.end method

.method public put(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 622
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v0

    if-gt v0, p1, :cond_1

    .line 625
    :cond_0
    :goto_0
    return-void

    .line 624
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    new-instance v1, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v1, p2}, Lcom/glympse/android/lib/Primitive;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, p1}, Lcom/glympse/android/hal/GVector;->setElementAt(Ljava/lang/Object;I)V

    goto :goto_0
.end method

.method public put(IZ)V
    .locals 2

    .prologue
    .line 615
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v0

    if-gt v0, p1, :cond_1

    .line 618
    :cond_0
    :goto_0
    return-void

    .line 617
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    new-instance v1, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v1, p2}, Lcom/glympse/android/lib/Primitive;-><init>(Z)V

    invoke-virtual {v0, v1, p1}, Lcom/glympse/android/hal/GVector;->setElementAt(Ljava/lang/Object;I)V

    goto :goto_0
.end method

.method public put(J)V
    .locals 3

    .prologue
    .line 566
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    if-nez v0, :cond_0

    .line 569
    :goto_0
    return-void

    .line 568
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    new-instance v1, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v1, p1, p2}, Lcom/glympse/android/lib/Primitive;-><init>(J)V

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public put(Lcom/glympse/android/core/GPrimitive;)V
    .locals 1

    .prologue
    .line 552
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    if-nez v0, :cond_0

    .line 555
    :goto_0
    return-void

    .line 554
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public put(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 580
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    if-nez v0, :cond_0

    .line 583
    :goto_0
    return-void

    .line 582
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    new-instance v1, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v1, p1}, Lcom/glympse/android/lib/Primitive;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public put(Ljava/lang/String;D)V
    .locals 2

    .prologue
    .line 506
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    if-nez v0, :cond_0

    .line 509
    :goto_0
    return-void

    .line 508
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    new-instance v1, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v1, p2, p3}, Lcom/glympse/android/lib/Primitive;-><init>(D)V

    invoke-virtual {v0, p1, v1}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public put(Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 513
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    if-nez v0, :cond_0

    .line 516
    :goto_0
    return-void

    .line 515
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    new-instance v1, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v1, p2, p3}, Lcom/glympse/android/lib/Primitive;-><init>(J)V

    invoke-virtual {v0, p1, v1}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V
    .locals 1

    .prologue
    .line 499
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    if-nez v0, :cond_0

    .line 502
    :goto_0
    return-void

    .line 501
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1, p2}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public put(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 527
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    if-nez v0, :cond_0

    .line 530
    :goto_0
    return-void

    .line 529
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    new-instance v1, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v1, p2}, Lcom/glympse/android/lib/Primitive;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1, v1}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public put(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 520
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    if-nez v0, :cond_0

    .line 523
    :goto_0
    return-void

    .line 522
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    new-instance v1, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v1, p2}, Lcom/glympse/android/lib/Primitive;-><init>(Z)V

    invoke-virtual {v0, p1, v1}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public put(Z)V
    .locals 2

    .prologue
    .line 573
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    if-nez v0, :cond_0

    .line 576
    :goto_0
    return-void

    .line 575
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    new-instance v1, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v1, p1}, Lcom/glympse/android/lib/Primitive;-><init>(Z)V

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public putNull(I)V
    .locals 2

    .prologue
    .line 629
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v0

    if-gt v0, p1, :cond_1

    .line 632
    :cond_0
    :goto_0
    return-void

    .line 631
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    new-instance v1, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v1}, Lcom/glympse/android/lib/Primitive;-><init>()V

    invoke-virtual {v0, v1, p1}, Lcom/glympse/android/hal/GVector;->setElementAt(Ljava/lang/Object;I)V

    goto :goto_0
.end method

.method public putNull(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 534
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    if-nez v0, :cond_0

    .line 537
    :goto_0
    return-void

    .line 536
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    new-instance v1, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v1}, Lcom/glympse/android/lib/Primitive;-><init>()V

    invoke-virtual {v0, p1, v1}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public remove(I)V
    .locals 1

    .prologue
    .line 636
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    if-nez v0, :cond_0

    .line 639
    :goto_0
    return-void

    .line 638
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GVector;->removeElementAt(I)V

    goto :goto_0
.end method

.method public remove(Lcom/glympse/android/core/GPrimitive;)V
    .locals 1

    .prologue
    .line 643
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    if-nez v0, :cond_0

    .line 646
    :goto_0
    return-void

    .line 645
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GVector;->removeElement(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public remove(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 541
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    if-nez v0, :cond_0

    .line 544
    :goto_0
    return-void

    .line 543
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public set(D)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 436
    const/4 v0, 0x4

    iput v0, p0, Lcom/glympse/android/lib/Primitive;->_type:I

    .line 437
    iput-wide p1, p0, Lcom/glympse/android/lib/Primitive;->sm:D

    .line 438
    iput-object v1, p0, Lcom/glympse/android/lib/Primitive;->sn:Ljava/lang/String;

    .line 439
    iput-object v1, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    .line 440
    iput-object v1, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    .line 441
    return-void
.end method

.method public set(J)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 445
    const/16 v0, 0x8

    iput v0, p0, Lcom/glympse/android/lib/Primitive;->_type:I

    .line 446
    iput-wide p1, p0, Lcom/glympse/android/lib/Primitive;->sl:J

    .line 447
    iput-object v1, p0, Lcom/glympse/android/lib/Primitive;->sn:Ljava/lang/String;

    .line 448
    iput-object v1, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    .line 449
    iput-object v1, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    .line 450
    return-void
.end method

.method public set(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 463
    const/16 v0, 0x20

    iput v0, p0, Lcom/glympse/android/lib/Primitive;->_type:I

    .line 464
    iput-object p1, p0, Lcom/glympse/android/lib/Primitive;->sn:Ljava/lang/String;

    .line 465
    iput-object v1, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    .line 466
    iput-object v1, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    .line 467
    return-void
.end method

.method public set(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 454
    const/16 v0, 0x10

    iput v0, p0, Lcom/glympse/android/lib/Primitive;->_type:I

    .line 455
    iput-boolean p1, p0, Lcom/glympse/android/lib/Primitive;->_bool:Z

    .line 456
    iput-object v1, p0, Lcom/glympse/android/lib/Primitive;->sn:Ljava/lang/String;

    .line 457
    iput-object v1, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    .line 458
    iput-object v1, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    .line 459
    return-void
.end method

.method public setArray()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 479
    const/4 v0, 0x1

    iput v0, p0, Lcom/glympse/android/lib/Primitive;->_type:I

    .line 480
    iput-object v1, p0, Lcom/glympse/android/lib/Primitive;->sn:Ljava/lang/String;

    .line 481
    new-instance v0, Lcom/glympse/android/hal/GVector;

    invoke-direct {v0}, Lcom/glympse/android/hal/GVector;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    .line 482
    iput-object v1, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    .line 483
    return-void
.end method

.method public setNull()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 471
    const/16 v0, 0x40

    iput v0, p0, Lcom/glympse/android/lib/Primitive;->_type:I

    .line 472
    iput-object v1, p0, Lcom/glympse/android/lib/Primitive;->sn:Ljava/lang/String;

    .line 473
    iput-object v1, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    .line 474
    iput-object v1, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    .line 475
    return-void
.end method

.method public setObject()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 487
    const/4 v0, 0x2

    iput v0, p0, Lcom/glympse/android/lib/Primitive;->_type:I

    .line 488
    iput-object v1, p0, Lcom/glympse/android/lib/Primitive;->sn:Ljava/lang/String;

    .line 489
    iput-object v1, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    .line 490
    new-instance v0, Lcom/glympse/android/hal/GHashtable;

    invoke-direct {v0}, Lcom/glympse/android/hal/GHashtable;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    .line 491
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sj:Lcom/glympse/android/hal/GVector;

    .line 124
    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v0

    .line 123
    :goto_0
    return v0

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/glympse/android/lib/Primitive;->sk:Lcom/glympse/android/hal/GHashtable;

    .line 125
    invoke-virtual {v0}, Lcom/glympse/android/hal/GHashtable;->size()I

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public type()I
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lcom/glympse/android/lib/Primitive;->_type:I

    return v0
.end method
