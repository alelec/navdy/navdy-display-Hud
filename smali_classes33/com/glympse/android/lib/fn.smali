.class Lcom/glympse/android/lib/fn;
.super Lcom/glympse/android/lib/e;
.source "TicketOwn.java"


# instance fields
.field private _glympse:Lcom/glympse/android/lib/GGlympsePrivate;

.field private gM:Ljava/lang/String;

.field private oG:Lcom/glympse/android/lib/GTicketPrivate;

.field private tB:Lcom/glympse/android/lib/fr$a;


# direct methods
.method public constructor <init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/GTicketPrivate;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/glympse/android/lib/e;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/glympse/android/lib/fn;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 27
    iput-object p2, p0, Lcom/glympse/android/lib/fn;->oG:Lcom/glympse/android/lib/GTicketPrivate;

    .line 28
    invoke-interface {p2}, Lcom/glympse/android/lib/GTicketPrivate;->getId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/fn;->gM:Ljava/lang/String;

    .line 30
    new-instance v0, Lcom/glympse/android/lib/fr$a;

    invoke-direct {v0}, Lcom/glympse/android/lib/fr$a;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/fn;->tB:Lcom/glympse/android/lib/fr$a;

    .line 31
    iget-object v0, p0, Lcom/glympse/android/lib/fn;->tB:Lcom/glympse/android/lib/fr$a;

    iput-object v0, p0, Lcom/glympse/android/lib/fn;->gB:Lcom/glympse/android/lib/f;

    .line 32
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 101
    new-instance v0, Lcom/glympse/android/lib/fr$a;

    invoke-direct {v0}, Lcom/glympse/android/lib/fr$a;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/fn;->tB:Lcom/glympse/android/lib/fr$a;

    .line 102
    iget-object v0, p0, Lcom/glympse/android/lib/fn;->tB:Lcom/glympse/android/lib/fr$a;

    iput-object v0, p0, Lcom/glympse/android/lib/fn;->gB:Lcom/glympse/android/lib/f;

    .line 103
    return-void
.end method

.method public methodType()I
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x2

    return v0
.end method

.method public process()Z
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x0

    const/4 v1, 0x1

    .line 53
    iget-object v0, p0, Lcom/glympse/android/lib/fn;->tB:Lcom/glympse/android/lib/fr$a;

    iget-object v0, v0, Lcom/glympse/android/lib/fr$a;->gF:Ljava/lang/String;

    const-string v2, "ok"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 56
    iget-object v0, p0, Lcom/glympse/android/lib/fn;->oG:Lcom/glympse/android/lib/GTicketPrivate;

    iget-object v2, p0, Lcom/glympse/android/lib/fn;->tB:Lcom/glympse/android/lib/fr$a;

    iget-object v2, v2, Lcom/glympse/android/lib/fr$a;->tR:Lcom/glympse/android/lib/fo$c;

    iget-object v2, v2, Lcom/glympse/android/lib/fo$c;->oG:Lcom/glympse/android/lib/GTicketPrivate;

    iget-object v3, p0, Lcom/glympse/android/lib/fn;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0, v2, v3, v1, v1}, Lcom/glympse/android/lib/GTicketPrivate;->merge(Lcom/glympse/android/lib/GTicketPrivate;Lcom/glympse/android/lib/GGlympsePrivate;ZZ)V

    .line 59
    iget-object v0, p0, Lcom/glympse/android/lib/fn;->oG:Lcom/glympse/android/lib/GTicketPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketPrivate;->isMine()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 63
    iget-object v0, p0, Lcom/glympse/android/lib/fn;->oG:Lcom/glympse/android/lib/GTicketPrivate;

    const-string v2, "route"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v6, v7, v2}, Lcom/glympse/android/lib/GTicketPrivate;->getProperty(JLjava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/glympse/android/lib/fn;->oG:Lcom/glympse/android/lib/GTicketPrivate;

    invoke-interface {v0, v4}, Lcom/glympse/android/lib/GTicketPrivate;->updateRoute(Lcom/glympse/android/api/GTrack;)V

    .line 70
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/fn;->oG:Lcom/glympse/android/lib/GTicketPrivate;

    const-string v2, "travel_mode"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v6, v7, v2}, Lcom/glympse/android/lib/GTicketPrivate;->getProperty(JLjava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 72
    iget-object v0, p0, Lcom/glympse/android/lib/fn;->oG:Lcom/glympse/android/lib/GTicketPrivate;

    invoke-interface {v0, v4}, Lcom/glympse/android/lib/GTicketPrivate;->updateTravelMode(Lcom/glympse/android/api/GTravelMode;)V

    .line 76
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/fn;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getHistoryManager()Lcom/glympse/android/api/GHistoryManager;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GHistoryManagerPrivate;

    .line 77
    iget-object v2, p0, Lcom/glympse/android/lib/fn;->tB:Lcom/glympse/android/lib/fr$a;

    iget-object v2, v2, Lcom/glympse/android/lib/fr$a;->tR:Lcom/glympse/android/lib/fo$c;

    iget-wide v2, v2, Lcom/glympse/android/lib/fo$c;->ns:J

    invoke-interface {v0, v2, v3}, Lcom/glympse/android/lib/GHistoryManagerPrivate;->setLastViewTime(J)V

    .line 80
    iget-object v0, p0, Lcom/glympse/android/lib/fn;->oG:Lcom/glympse/android/lib/GTicketPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketPrivate;->updateWatchingState()Z

    .line 83
    iget-object v0, p0, Lcom/glympse/android/lib/fn;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->okToPost()Z

    :goto_0
    move v0, v1

    .line 96
    :goto_1
    return v0

    .line 88
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/fn;->oG:Lcom/glympse/android/lib/GTicketPrivate;

    iget-object v2, p0, Lcom/glympse/android/lib/fn;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/4 v3, 0x4

    const/high16 v4, 0x2000000

    iget-object v5, p0, Lcom/glympse/android/lib/fn;->oG:Lcom/glympse/android/lib/GTicketPrivate;

    invoke-interface {v0, v2, v3, v4, v5}, Lcom/glympse/android/lib/GTicketPrivate;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    goto :goto_0

    .line 94
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public url(Ljava/lang/StringBuilder;)Z
    .locals 1

    .prologue
    .line 40
    const-string v0, "tickets/"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    iget-object v0, p0, Lcom/glympse/android/lib/fn;->gM:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    const-string v0, "/take_ownership?properties=true&invites=true"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    const/4 v0, 0x1

    return v0
.end method
