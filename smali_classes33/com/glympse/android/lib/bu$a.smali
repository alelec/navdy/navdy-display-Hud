.class Lcom/glympse/android/lib/bu$a;
.super Ljava/lang/Object;
.source "GogoService.java"

# interfaces
.implements Lcom/glympse/android/lib/bs$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/lib/bu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/lib/bu$a$a;
    }
.end annotation


# instance fields
.field private _handler:Lcom/glympse/android/core/GHandler;

.field private _jobQueue:Lcom/glympse/android/lib/GJobQueue;

.field private ml:Ljava/lang/String;

.field private mo:Ljava/lang/Runnable;

.field private mu:Lcom/glympse/android/lib/bh;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 487
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 488
    iput-object p1, p0, Lcom/glympse/android/lib/bu$a;->ml:Ljava/lang/String;

    .line 489
    return-void
.end method


# virtual methods
.method public a(Lcom/glympse/android/lib/bh;Lcom/glympse/android/core/GHandler;Lcom/glympse/android/lib/GJobQueue;)V
    .locals 2

    .prologue
    .line 493
    iput-object p1, p0, Lcom/glympse/android/lib/bu$a;->mu:Lcom/glympse/android/lib/bh;

    .line 494
    iput-object p2, p0, Lcom/glympse/android/lib/bu$a;->_handler:Lcom/glympse/android/core/GHandler;

    .line 495
    iput-object p3, p0, Lcom/glympse/android/lib/bu$a;->_jobQueue:Lcom/glympse/android/lib/GJobQueue;

    .line 497
    new-instance v1, Lcom/glympse/android/lib/bu$a$a;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/bu$a;

    invoke-direct {v1, v0}, Lcom/glympse/android/lib/bu$a$a;-><init>(Lcom/glympse/android/lib/bu$a;)V

    iput-object v1, p0, Lcom/glympse/android/lib/bu$a;->mo:Ljava/lang/Runnable;

    .line 500
    invoke-virtual {p0}, Lcom/glympse/android/lib/bu$a;->bR()V

    .line 501
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;Z)V
    .locals 1

    .prologue
    .line 535
    iget-object v0, p0, Lcom/glympse/android/lib/bu$a;->mu:Lcom/glympse/android/lib/bh;

    if-eqz v0, :cond_0

    .line 537
    iget-object v0, p0, Lcom/glympse/android/lib/bu$a;->mu:Lcom/glympse/android/lib/bh;

    invoke-interface {v0, p1, p2, p3}, Lcom/glympse/android/lib/bh;->a(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;Z)V

    .line 539
    :cond_0
    return-void
.end method

.method public bH()V
    .locals 0

    .prologue
    .line 544
    invoke-virtual {p0}, Lcom/glympse/android/lib/bu$a;->bJ()V

    .line 545
    return-void
.end method

.method protected bJ()V
    .locals 4

    .prologue
    .line 553
    iget-object v0, p0, Lcom/glympse/android/lib/bu$a;->_handler:Lcom/glympse/android/core/GHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/bu$a;->mo:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 555
    iget-object v0, p0, Lcom/glympse/android/lib/bu$a;->_handler:Lcom/glympse/android/core/GHandler;

    iget-object v1, p0, Lcom/glympse/android/lib/bu$a;->mo:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2710

    invoke-interface {v0, v1, v2, v3}, Lcom/glympse/android/core/GHandler;->postDelayed(Ljava/lang/Runnable;J)V

    .line 557
    :cond_0
    return-void
.end method

.method protected bR()V
    .locals 4

    .prologue
    .line 518
    iget-object v0, p0, Lcom/glympse/android/lib/bu$a;->_jobQueue:Lcom/glympse/android/lib/GJobQueue;

    if-eqz v0, :cond_0

    .line 520
    iget-object v1, p0, Lcom/glympse/android/lib/bu$a;->_jobQueue:Lcom/glympse/android/lib/GJobQueue;

    new-instance v2, Lcom/glympse/android/lib/bs;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/bs$a;

    iget-object v3, p0, Lcom/glympse/android/lib/bu$a;->ml:Ljava/lang/String;

    invoke-direct {v2, v0, v3}, Lcom/glympse/android/lib/bs;-><init>(Lcom/glympse/android/lib/bs$a;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Lcom/glympse/android/lib/GJobQueue;->addJob(Lcom/glympse/android/lib/GJob;)V

    .line 522
    :cond_0
    return-void
.end method

.method public locationChanged(Lcom/glympse/android/core/GLocation;)V
    .locals 0

    .prologue
    .line 531
    return-void
.end method

.method public stop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 505
    iget-object v0, p0, Lcom/glympse/android/lib/bu$a;->mo:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 507
    iget-object v0, p0, Lcom/glympse/android/lib/bu$a;->_handler:Lcom/glympse/android/core/GHandler;

    iget-object v1, p0, Lcom/glympse/android/lib/bu$a;->mo:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GHandler;->cancel(Ljava/lang/Runnable;)V

    .line 508
    iput-object v2, p0, Lcom/glympse/android/lib/bu$a;->mo:Ljava/lang/Runnable;

    .line 511
    :cond_0
    iput-object v2, p0, Lcom/glympse/android/lib/bu$a;->mu:Lcom/glympse/android/lib/bh;

    .line 512
    iput-object v2, p0, Lcom/glympse/android/lib/bu$a;->_handler:Lcom/glympse/android/core/GHandler;

    .line 513
    iput-object v2, p0, Lcom/glympse/android/lib/bu$a;->_jobQueue:Lcom/glympse/android/lib/GJobQueue;

    .line 514
    return-void
.end method
