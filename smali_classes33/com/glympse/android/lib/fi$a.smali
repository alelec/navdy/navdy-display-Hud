.class Lcom/glympse/android/lib/fi$a;
.super Ljava/lang/Object;
.source "TicketDirectionsProvider.java"

# interfaces
.implements Lcom/glympse/android/api/GEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/lib/fi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private tH:Lcom/glympse/android/lib/fi;


# direct methods
.method public constructor <init>(Lcom/glympse/android/lib/fi;)V
    .locals 0

    .prologue
    .line 310
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 311
    iput-object p1, p0, Lcom/glympse/android/lib/fi$a;->tH:Lcom/glympse/android/lib/fi;

    .line 312
    return-void
.end method


# virtual methods
.method public eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V
    .locals 7

    .prologue
    .line 316
    const/16 v0, 0x13

    if-ne v0, p2, :cond_0

    .line 318
    and-int/lit8 v0, p3, 0x3

    if-eqz v0, :cond_0

    .line 320
    const/4 v0, 0x3

    const-string v1, "[DirectionListener.eventsOccurred] Directions Changed"

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 321
    check-cast p4, Lcom/glympse/android/lib/GDirectionsPrivate;

    .line 322
    iget-object v1, p0, Lcom/glympse/android/lib/fi$a;->tH:Lcom/glympse/android/lib/fi;

    invoke-interface {p4}, Lcom/glympse/android/lib/GDirectionsPrivate;->getEtaTs()J

    move-result-wide v2

    invoke-interface {p4}, Lcom/glympse/android/lib/GDirectionsPrivate;->getEta()J

    move-result-wide v4

    invoke-interface {p4}, Lcom/glympse/android/lib/GDirectionsPrivate;->getTrack()Lcom/glympse/android/api/GTrack;

    move-result-object v6

    invoke-static/range {v1 .. v6}, Lcom/glympse/android/lib/fi;->a(Lcom/glympse/android/lib/fi;JJLcom/glympse/android/api/GTrack;)V

    .line 323
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GEventListener;

    invoke-interface {p4, v0}, Lcom/glympse/android/lib/GDirectionsPrivate;->removeListener(Lcom/glympse/android/api/GEventListener;)Z

    .line 326
    :cond_0
    return-void
.end method
