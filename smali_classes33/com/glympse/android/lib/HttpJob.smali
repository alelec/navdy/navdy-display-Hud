.class public Lcom/glympse/android/lib/HttpJob;
.super Lcom/glympse/android/lib/de;
.source "HttpJob.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/lib/HttpJob$a;
    }
.end annotation


# static fields
.field public static final RESPONSE_CODE_MAX_RETRY_LOWER_BOUND:I = 0x190

.field public static final RESPONSE_CODE_MAX_RETRY_UPPER_BOUND:I = 0x257

.field public static final STOP:I = -0x1


# instance fields
.field protected _abortHttp:Z

.field protected _backOffPolicy:Lcom/glympse/android/lib/GBackOffPolicy;

.field protected _failures:I

.field protected _httpConnection:Lcom/glympse/android/hal/GHttpConnection;

.field protected _readResponseForFailedCall:Z

.field protected _responseCode:I

.field protected _success:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 48
    invoke-direct {p0}, Lcom/glympse/android/lib/de;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/HttpJob;->_httpConnection:Lcom/glympse/android/hal/GHttpConnection;

    .line 50
    iput v1, p0, Lcom/glympse/android/lib/HttpJob;->_responseCode:I

    .line 51
    iput-boolean v1, p0, Lcom/glympse/android/lib/HttpJob;->_readResponseForFailedCall:Z

    .line 52
    iput-boolean v1, p0, Lcom/glympse/android/lib/HttpJob;->_abortHttp:Z

    .line 53
    iput-boolean v1, p0, Lcom/glympse/android/lib/HttpJob;->_success:Z

    .line 54
    iput v1, p0, Lcom/glympse/android/lib/HttpJob;->_failures:I

    .line 55
    invoke-static {}, Lcom/glympse/android/lib/LibFactory;->createExponentialBackOff()Lcom/glympse/android/lib/GBackOffPolicy;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/HttpJob;->_backOffPolicy:Lcom/glympse/android/lib/GBackOffPolicy;

    .line 56
    return-void
.end method

.method public static httpMethodEnumToString(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    .line 95
    packed-switch p0, :pswitch_data_0

    .line 115
    const-string v0, "GET"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 119
    :goto_0
    return-object v0

    .line 99
    :pswitch_0
    const-string v0, "POST"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 104
    :pswitch_1
    const-string v0, "PUT"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 109
    :pswitch_2
    const-string v0, "DELETE"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 95
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static httpMethodStringToEnum(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 127
    const-string v0, "POST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    const/4 v0, 0x2

    .line 145
    :goto_0
    return v0

    .line 131
    :cond_0
    const-string v0, "GET"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 133
    const/4 v0, 0x1

    goto :goto_0

    .line 135
    :cond_1
    const-string v0, "PUT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 137
    const/4 v0, 0x3

    goto :goto_0

    .line 139
    :cond_2
    const-string v0, "DELETE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 141
    const/4 v0, 0x4

    goto :goto_0

    .line 145
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static pickMethod(ILjava/lang/String;)I
    .locals 1

    .prologue
    .line 67
    invoke-static {p1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 68
    :goto_0
    invoke-static {p0, v0}, Lcom/glympse/android/lib/HttpJob;->pickMethod(IZ)I

    move-result v0

    return v0

    .line 67
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static pickMethod(IZ)I
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 76
    .line 77
    if-nez p0, :cond_1

    move v0, v1

    .line 82
    :goto_0
    if-eqz p1, :cond_0

    if-ne v1, v0, :cond_0

    .line 84
    const/4 v0, 0x2

    .line 86
    :cond_0
    return v0

    :cond_1
    move v0, p0

    goto :goto_0
.end method

.method public static setAuthorization(Lcom/glympse/android/hal/GHttpConnection;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 151
    if-nez p1, :cond_0

    .line 160
    :goto_0
    return-void

    .line 155
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 156
    const-string v1, "Bearer "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 157
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 159
    const-string v1, "Authorization"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v1, v0}, Lcom/glympse/android/hal/GHttpConnection;->setRequestHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public abort()V
    .locals 0

    .prologue
    .line 331
    invoke-super {p0}, Lcom/glympse/android/lib/de;->abort()V

    .line 334
    invoke-virtual {p0}, Lcom/glympse/android/lib/HttpJob;->cancel()V

    .line 335
    return-void
.end method

.method public cancel()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 181
    iput-boolean v0, p0, Lcom/glympse/android/lib/HttpJob;->_abortHttp:Z

    .line 182
    iput-boolean v0, p0, Lcom/glympse/android/lib/HttpJob;->_success:Z

    .line 183
    iget-object v0, p0, Lcom/glympse/android/lib/HttpJob;->_httpConnection:Lcom/glympse/android/hal/GHttpConnection;

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/glympse/android/lib/HttpJob;->_httpConnection:Lcom/glympse/android/hal/GHttpConnection;

    invoke-interface {v0}, Lcom/glympse/android/hal/GHttpConnection;->close()V

    .line 187
    :cond_0
    return-void
.end method

.method public checkResponse(II)Z
    .locals 1

    .prologue
    .line 350
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRetryInterval(I)I
    .locals 2

    .prologue
    .line 358
    iget v0, p0, Lcom/glympse/android/lib/HttpJob;->_responseCode:I

    const/16 v1, 0x190

    if-lt v0, v1, :cond_0

    iget v0, p0, Lcom/glympse/android/lib/HttpJob;->_responseCode:I

    const/16 v1, 0x257

    if-gt v0, v1, :cond_0

    .line 360
    iget-object v0, p0, Lcom/glympse/android/lib/HttpJob;->_backOffPolicy:Lcom/glympse/android/lib/GBackOffPolicy;

    invoke-interface {v0}, Lcom/glympse/android/lib/GBackOffPolicy;->maxOutBackOffInterval()V

    .line 362
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/HttpJob;->_backOffPolicy:Lcom/glympse/android/lib/GBackOffPolicy;

    invoke-interface {v0}, Lcom/glympse/android/lib/GBackOffPolicy;->getNextBackOffMillis()I

    move-result v0

    return v0
.end method

.method public bridge synthetic isAborted()Z
    .locals 1

    .prologue
    .line 16
    invoke-super {p0}, Lcom/glympse/android/lib/de;->isAborted()Z

    move-result v0

    return v0
.end method

.method public isSucceeded()Z
    .locals 1

    .prologue
    .line 176
    iget-boolean v0, p0, Lcom/glympse/android/lib/HttpJob;->_success:Z

    return v0
.end method

.method public onAbort()V
    .locals 0

    .prologue
    .line 318
    invoke-super {p0}, Lcom/glympse/android/lib/de;->onAbort()V

    .line 319
    return-void
.end method

.method public onComplete()V
    .locals 6

    .prologue
    const/4 v4, 0x4

    .line 287
    invoke-super {p0}, Lcom/glympse/android/lib/de;->onComplete()V

    .line 290
    iget-boolean v0, p0, Lcom/glympse/android/lib/HttpJob;->_abortHttp:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/glympse/android/lib/HttpJob;->isSucceeded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 293
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "HttpJob.onComplete request failed with code: "

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/glympse/android/lib/HttpJob;->_responseCode:I

    int-to-long v2, v1

    .line 294
    invoke-static {v2, v3}, Lcom/glympse/android/hal/Helpers;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " url: "

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/lib/HttpJob;->_httpConnection:Lcom/glympse/android/hal/GHttpConnection;

    .line 295
    invoke-interface {v1}, Lcom/glympse/android/hal/GHttpConnection;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->safeStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 293
    invoke-static {v4, v0}, Lcom/glympse/android/hal/Helpers;->log(ILjava/lang/String;)V

    .line 297
    iget-object v0, p0, Lcom/glympse/android/lib/HttpJob;->_handler:Lcom/glympse/android/core/GHandler;

    if-eqz v0, :cond_0

    .line 299
    iget v0, p0, Lcom/glympse/android/lib/HttpJob;->_failures:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/glympse/android/lib/HttpJob;->_failures:I

    .line 300
    iget v0, p0, Lcom/glympse/android/lib/HttpJob;->_failures:I

    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/HttpJob;->getRetryInterval(I)I

    move-result v1

    .line 301
    const/4 v0, -0x1

    if-eq v0, v1, :cond_0

    .line 303
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HttpJob.onComplete retry scheduled after failures: "

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/glympse/android/lib/HttpJob;->_failures:I

    int-to-long v2, v2

    .line 304
    invoke-static {v2, v3}, Lcom/glympse/android/hal/Helpers;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 303
    invoke-static {v4, v0}, Lcom/glympse/android/hal/Helpers;->log(ILjava/lang/String;)V

    .line 305
    iget-object v2, p0, Lcom/glympse/android/lib/HttpJob;->_jobQueue:Lcom/glympse/android/lib/GJobQueue;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GJob;

    int-to-long v4, v1

    invoke-interface {v2, v0, v4, v5}, Lcom/glympse/android/lib/GJobQueue;->retryDelayed(Lcom/glympse/android/lib/GJob;J)V

    .line 314
    :cond_0
    :goto_0
    return-void

    .line 311
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/glympse/android/lib/HttpJob;->_failures:I

    .line 312
    iget-object v0, p0, Lcom/glympse/android/lib/HttpJob;->_backOffPolicy:Lcom/glympse/android/lib/GBackOffPolicy;

    invoke-interface {v0}, Lcom/glympse/android/lib/GBackOffPolicy;->reset()V

    goto :goto_0
.end method

.method public bridge synthetic onDetach()V
    .locals 0

    .prologue
    .line 16
    invoke-super {p0}, Lcom/glympse/android/lib/de;->onDetach()V

    return-void
.end method

.method public onPreProcess()V
    .locals 0

    .prologue
    .line 168
    return-void
.end method

.method public onProcess()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 206
    iput-boolean v4, p0, Lcom/glympse/android/lib/HttpJob;->_abortHttp:Z

    .line 207
    iput-boolean v4, p0, Lcom/glympse/android/lib/HttpJob;->_success:Z

    .line 210
    invoke-static {}, Lcom/glympse/android/lib/LibFactory;->createHttpConnection()Lcom/glympse/android/hal/GHttpConnection;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/HttpJob;->_httpConnection:Lcom/glympse/android/hal/GHttpConnection;

    .line 213
    invoke-virtual {p0}, Lcom/glympse/android/lib/HttpJob;->onPreProcess()V

    .line 216
    iget-boolean v0, p0, Lcom/glympse/android/lib/HttpJob;->_abortHttp:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/glympse/android/lib/HttpJob;->_aborted:Z

    if-eqz v0, :cond_1

    .line 283
    :cond_0
    :goto_0
    return-void

    .line 222
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/HttpJob;->_httpConnection:Lcom/glympse/android/hal/GHttpConnection;

    iget-object v1, p0, Lcom/glympse/android/lib/HttpJob;->_backOffPolicy:Lcom/glympse/android/lib/GBackOffPolicy;

    invoke-interface {v1}, Lcom/glympse/android/lib/GBackOffPolicy;->getOsConnectTimeout()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/glympse/android/hal/GHttpConnection;->setConnectTimeout(I)V

    .line 223
    iget-object v0, p0, Lcom/glympse/android/lib/HttpJob;->_httpConnection:Lcom/glympse/android/hal/GHttpConnection;

    iget-object v1, p0, Lcom/glympse/android/lib/HttpJob;->_backOffPolicy:Lcom/glympse/android/lib/GBackOffPolicy;

    invoke-interface {v1}, Lcom/glympse/android/lib/GBackOffPolicy;->getOsReadTimeout()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/glympse/android/hal/GHttpConnection;->setReadTimeout(I)V

    .line 226
    const/4 v0, 0x0

    .line 227
    iget-object v1, p0, Lcom/glympse/android/lib/HttpJob;->_handler:Lcom/glympse/android/core/GHandler;

    if-eqz v1, :cond_4

    .line 229
    new-instance v1, Lcom/glympse/android/lib/HttpJob$a;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/HttpJob;

    invoke-direct {v1, v0}, Lcom/glympse/android/lib/HttpJob$a;-><init>(Lcom/glympse/android/lib/HttpJob;)V

    .line 230
    iget-object v0, p0, Lcom/glympse/android/lib/HttpJob;->_handler:Lcom/glympse/android/core/GHandler;

    iget-object v2, p0, Lcom/glympse/android/lib/HttpJob;->_backOffPolicy:Lcom/glympse/android/lib/GBackOffPolicy;

    invoke-interface {v2}, Lcom/glympse/android/lib/GBackOffPolicy;->getPlatformTimeout()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Lcom/glympse/android/core/GHandler;->postDelayed(Ljava/lang/Runnable;J)V

    .line 236
    :goto_1
    :try_start_0
    iget-object v0, p0, Lcom/glympse/android/lib/HttpJob;->_httpConnection:Lcom/glympse/android/hal/GHttpConnection;

    invoke-interface {v0}, Lcom/glympse/android/hal/GHttpConnection;->establish()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 244
    :goto_2
    if-eqz v1, :cond_2

    .line 246
    invoke-virtual {v1}, Lcom/glympse/android/lib/HttpJob$a;->abort()V

    .line 247
    iget-object v0, p0, Lcom/glympse/android/lib/HttpJob;->_handler:Lcom/glympse/android/core/GHandler;

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GHandler;->cancel(Ljava/lang/Runnable;)V

    .line 252
    :cond_2
    iget-boolean v0, p0, Lcom/glympse/android/lib/HttpJob;->_abortHttp:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/glympse/android/lib/HttpJob;->_aborted:Z

    if-nez v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/glympse/android/lib/HttpJob;->_httpConnection:Lcom/glympse/android/hal/GHttpConnection;

    invoke-interface {v0}, Lcom/glympse/android/hal/GHttpConnection;->getResponseCode()I

    move-result v0

    iput v0, p0, Lcom/glympse/android/lib/HttpJob;->_responseCode:I

    .line 259
    iget-object v0, p0, Lcom/glympse/android/lib/HttpJob;->_httpConnection:Lcom/glympse/android/hal/GHttpConnection;

    invoke-interface {v0}, Lcom/glympse/android/hal/GHttpConnection;->getResponseDataLength()I

    move-result v0

    .line 262
    iget-boolean v1, p0, Lcom/glympse/android/lib/HttpJob;->_abortHttp:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/glympse/android/lib/HttpJob;->_aborted:Z

    if-nez v1, :cond_0

    .line 270
    iget v1, p0, Lcom/glympse/android/lib/HttpJob;->_responseCode:I

    invoke-virtual {p0, v1, v0}, Lcom/glympse/android/lib/HttpJob;->checkResponse(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/glympse/android/lib/HttpJob;->_success:Z

    .line 273
    iget-boolean v0, p0, Lcom/glympse/android/lib/HttpJob;->_success:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/glympse/android/lib/HttpJob;->_readResponseForFailedCall:Z

    if-eqz v0, :cond_0

    .line 279
    :cond_3
    invoke-virtual {p0}, Lcom/glympse/android/lib/HttpJob;->onProcessResponse()V

    .line 282
    iget-object v0, p0, Lcom/glympse/android/lib/HttpJob;->_httpConnection:Lcom/glympse/android/hal/GHttpConnection;

    invoke-interface {v0}, Lcom/glympse/android/hal/GHttpConnection;->close()V

    goto :goto_0

    .line 238
    :catch_0
    move-exception v0

    .line 240
    invoke-static {v0, v4}, Lcom/glympse/android/hal/Helpers;->ex(Ljava/lang/Throwable;Z)V

    goto :goto_2

    :cond_4
    move-object v1, v0

    goto :goto_1
.end method

.method public onProcessResponse()V
    .locals 0

    .prologue
    .line 172
    return-void
.end method

.method public bridge synthetic onRetry()V
    .locals 0

    .prologue
    .line 16
    invoke-super {p0}, Lcom/glympse/android/lib/de;->onRetry()V

    return-void
.end method

.method public bridge synthetic onSchedule(Lcom/glympse/android/lib/GJobQueue;Lcom/glympse/android/core/GHandler;)V
    .locals 0

    .prologue
    .line 16
    invoke-super {p0, p1, p2}, Lcom/glympse/android/lib/de;->onSchedule(Lcom/glympse/android/lib/GJobQueue;Lcom/glympse/android/core/GHandler;)V

    return-void
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 323
    invoke-super {p0}, Lcom/glympse/android/lib/de;->reset()V

    .line 325
    const/4 v0, 0x0

    iput v0, p0, Lcom/glympse/android/lib/HttpJob;->_failures:I

    .line 326
    iget-object v0, p0, Lcom/glympse/android/lib/HttpJob;->_backOffPolicy:Lcom/glympse/android/lib/GBackOffPolicy;

    invoke-interface {v0}, Lcom/glympse/android/lib/GBackOffPolicy;->reset()V

    .line 327
    return-void
.end method

.method public setAuthorization(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/glympse/android/lib/HttpJob;->_httpConnection:Lcom/glympse/android/hal/GHttpConnection;

    invoke-static {v0, p1}, Lcom/glympse/android/lib/HttpJob;->setAuthorization(Lcom/glympse/android/hal/GHttpConnection;Ljava/lang/String;)V

    .line 197
    return-void
.end method

.method public useHandler()Z
    .locals 1

    .prologue
    .line 191
    const/4 v0, 0x1

    return v0
.end method
