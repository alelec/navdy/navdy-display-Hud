.class Lcom/glympse/android/lib/dc;
.super Lcom/glympse/android/lib/e;
.source "InviteView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/lib/dc$a;
    }
.end annotation


# instance fields
.field protected _glympse:Lcom/glympse/android/lib/GGlympsePrivate;

.field private gr:Lcom/glympse/android/api/GEventSink;

.field protected mM:J

.field private mP:Lcom/glympse/android/api/GInvite;

.field protected mQ:Ljava/lang/String;

.field protected oC:I

.field protected pA:I

.field protected pB:I

.field private pC:I

.field protected pD:Z

.field private pE:Z

.field private pF:Z

.field private pG:Z

.field protected pH:Lcom/glympse/android/lib/dc$a;


# direct methods
.method public constructor <init>(Lcom/glympse/android/lib/GGlympsePrivate;Ljava/lang/String;Lcom/glympse/android/api/GEventSink;ILcom/glympse/android/api/GInvite;)V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 39
    invoke-direct {p0}, Lcom/glympse/android/lib/e;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/glympse/android/lib/dc;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 41
    iput-object p2, p0, Lcom/glympse/android/lib/dc;->mQ:Ljava/lang/String;

    .line 42
    iput-object p3, p0, Lcom/glympse/android/lib/dc;->gr:Lcom/glympse/android/api/GEventSink;

    .line 43
    iput v0, p0, Lcom/glympse/android/lib/dc;->oC:I

    .line 44
    iput v1, p0, Lcom/glympse/android/lib/dc;->pA:I

    .line 45
    iput v0, p0, Lcom/glympse/android/lib/dc;->pB:I

    .line 46
    iput p4, p0, Lcom/glympse/android/lib/dc;->pC:I

    .line 47
    iput-object p5, p0, Lcom/glympse/android/lib/dc;->mP:Lcom/glympse/android/api/GInvite;

    .line 50
    iput-boolean v1, p0, Lcom/glympse/android/lib/dc;->pD:Z

    .line 54
    iget v0, p0, Lcom/glympse/android/lib/dc;->pC:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/glympse/android/lib/dc;->pE:Z

    .line 56
    iget v0, p0, Lcom/glympse/android/lib/dc;->pC:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/glympse/android/lib/dc;->pF:Z

    .line 58
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/glympse/android/lib/dc;->mM:J

    .line 59
    iget-object v0, p0, Lcom/glympse/android/lib/dc;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getConfigPrivate()Lcom/glympse/android/lib/GConfigPrivate;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/lib/GConfigPrivate;->isTrackTrimmingEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/glympse/android/lib/dc;->pG:Z

    .line 61
    new-instance v0, Lcom/glympse/android/lib/dc$a;

    invoke-direct {v0}, Lcom/glympse/android/lib/dc$a;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/dc;->pH:Lcom/glympse/android/lib/dc$a;

    .line 62
    iget-object v0, p0, Lcom/glympse/android/lib/dc;->pH:Lcom/glympse/android/lib/dc$a;

    iput-object v0, p0, Lcom/glympse/android/lib/dc;->gB:Lcom/glympse/android/lib/f;

    .line 63
    return-void

    :cond_0
    move v0, v2

    .line 54
    goto :goto_0

    :cond_1
    move v1, v2

    .line 56
    goto :goto_1
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 149
    new-instance v0, Lcom/glympse/android/lib/dc$a;

    invoke-direct {v0}, Lcom/glympse/android/lib/dc$a;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/dc;->pH:Lcom/glympse/android/lib/dc$a;

    .line 150
    iget-object v0, p0, Lcom/glympse/android/lib/dc;->pH:Lcom/glympse/android/lib/dc$a;

    iput-object v0, p0, Lcom/glympse/android/lib/dc;->gB:Lcom/glympse/android/lib/f;

    .line 151
    return-void
.end method

.method protected cm()Z
    .locals 5

    .prologue
    .line 334
    iget-object v0, p0, Lcom/glympse/android/lib/dc;->gr:Lcom/glympse/android/api/GEventSink;

    if-eqz v0, :cond_0

    .line 337
    iget-object v0, p0, Lcom/glympse/android/lib/dc;->gr:Lcom/glympse/android/api/GEventSink;

    iget-object v1, p0, Lcom/glympse/android/lib/dc;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget v2, p0, Lcom/glympse/android/lib/dc;->oC:I

    iget v3, p0, Lcom/glympse/android/lib/dc;->pB:I

    iget-object v4, p0, Lcom/glympse/android/lib/dc;->mQ:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/glympse/android/api/GEventSink;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 339
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method protected cn()Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 167
    iget-object v0, p0, Lcom/glympse/android/lib/dc;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getUserManager()Lcom/glympse/android/api/GUserManager;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GUserManagerPrivate;

    .line 168
    iget-object v1, p0, Lcom/glympse/android/lib/dc;->pH:Lcom/glympse/android/lib/dc$a;

    iget-object v1, v1, Lcom/glympse/android/lib/dc$a;->pI:Lcom/glympse/android/lib/cz;

    iget-object v2, v1, Lcom/glympse/android/lib/cz;->oZ:Lcom/glympse/android/lib/GUserPrivate;

    .line 169
    iget-object v1, p0, Lcom/glympse/android/lib/dc;->pH:Lcom/glympse/android/lib/dc$a;

    iget-object v1, v1, Lcom/glympse/android/lib/dc$a;->pI:Lcom/glympse/android/lib/cz;

    iget-object v3, v1, Lcom/glympse/android/lib/cz;->pe:Lcom/glympse/android/lib/GTicketPrivate;

    .line 174
    invoke-interface {v3}, Lcom/glympse/android/lib/GTicketPrivate;->getTrack()Lcom/glympse/android/api/GTrack;

    move-result-object v1

    check-cast v1, Lcom/glympse/android/lib/GTrackPrivate;

    .line 175
    invoke-interface {v1}, Lcom/glympse/android/lib/GTrackPrivate;->length()I

    move-result v4

    if-lez v4, :cond_0

    .line 177
    invoke-interface {v1}, Lcom/glympse/android/lib/GTrackPrivate;->getLocationsRaw()Lcom/glympse/android/hal/GLinkedList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/glympse/android/hal/GLinkedList;->getLast()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/glympse/android/core/GLocation;

    invoke-interface {v2, v1}, Lcom/glympse/android/lib/GUserPrivate;->setLocation(Lcom/glympse/android/core/GLocation;)V

    .line 181
    :cond_0
    iget-object v1, p0, Lcom/glympse/android/lib/dc;->mQ:Ljava/lang/String;

    invoke-interface {v3, v1}, Lcom/glympse/android/lib/GTicketPrivate;->setCode(Ljava/lang/String;)V

    .line 182
    iget-object v1, p0, Lcom/glympse/android/lib/dc;->mQ:Ljava/lang/String;

    invoke-interface {v3, v1}, Lcom/glympse/android/lib/GTicketPrivate;->setId(Ljava/lang/String;)V

    .line 185
    iget-object v1, p0, Lcom/glympse/android/lib/dc;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->getTime()J

    move-result-wide v4

    invoke-interface {v3, v4, v5}, Lcom/glympse/android/lib/GTicketPrivate;->updateState(J)Z

    .line 188
    iget-object v1, p0, Lcom/glympse/android/lib/dc;->pH:Lcom/glympse/android/lib/dc$a;

    iget-object v1, v1, Lcom/glympse/android/lib/dc$a;->pI:Lcom/glympse/android/lib/cz;

    iget-object v1, v1, Lcom/glympse/android/lib/cz;->po:Lcom/glympse/android/lib/GTicketPrivate;

    .line 189
    if-eqz v1, :cond_1

    .line 191
    invoke-virtual {p0, v1}, Lcom/glympse/android/lib/dc;->q(Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 192
    invoke-interface {v3, v1}, Lcom/glympse/android/lib/GTicketPrivate;->setRequestTicket(Lcom/glympse/android/api/GTicket;)V

    .line 196
    :cond_1
    iget-object v1, p0, Lcom/glympse/android/lib/dc;->pH:Lcom/glympse/android/lib/dc$a;

    iget-object v1, v1, Lcom/glympse/android/lib/dc$a;->pI:Lcom/glympse/android/lib/cz;

    iget-object v1, v1, Lcom/glympse/android/lib/cz;->ou:Ljava/lang/String;

    .line 197
    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 199
    invoke-interface {v3, v1}, Lcom/glympse/android/lib/GTicketPrivate;->setReference(Ljava/lang/String;)V

    .line 204
    :cond_2
    invoke-interface {v0, v2}, Lcom/glympse/android/lib/GUserManagerPrivate;->resolveUser(Lcom/glympse/android/lib/GUserPrivate;)Lcom/glympse/android/lib/GUserPrivate;

    move-result-object v1

    .line 205
    if-nez v1, :cond_4

    .line 244
    :cond_3
    :goto_0
    return v6

    .line 212
    :cond_4
    new-instance v2, Lcom/glympse/android/lib/gh;

    iget-object v4, p0, Lcom/glympse/android/lib/dc;->mP:Lcom/glympse/android/api/GInvite;

    invoke-direct {v2, v1, v3, v4}, Lcom/glympse/android/lib/gh;-><init>(Lcom/glympse/android/api/GUser;Lcom/glympse/android/api/GTicket;Lcom/glympse/android/api/GInvite;)V

    .line 214
    iget-boolean v4, p0, Lcom/glympse/android/lib/dc;->pE:Z

    if-eqz v4, :cond_5

    .line 220
    const-wide/16 v4, 0x0

    invoke-interface {v3, v4, v5}, Lcom/glympse/android/lib/GTicketPrivate;->setNext(J)V

    .line 224
    iget-object v0, p0, Lcom/glympse/android/lib/dc;->mQ:Ljava/lang/String;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GUserPrivate;->findTicketByInviteCode(Ljava/lang/String;)Lcom/glympse/android/api/GTicket;

    move-result-object v0

    if-nez v0, :cond_3

    .line 231
    iget-object v0, p0, Lcom/glympse/android/lib/dc;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/dc;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/high16 v3, 0x400000

    invoke-interface {v0, v1, v6, v3, v2}, Lcom/glympse/android/lib/GGlympsePrivate;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    goto :goto_0

    .line 236
    :cond_5
    invoke-interface {v0, v2}, Lcom/glympse/android/lib/GUserManagerPrivate;->viewTicket(Lcom/glympse/android/api/GUserTicket;)Lcom/glympse/android/api/GTicket;

    .line 239
    iget-boolean v2, p0, Lcom/glympse/android/lib/dc;->pF:Z

    if-eqz v2, :cond_3

    .line 241
    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GUserManagerPrivate;->startTracking(Lcom/glympse/android/api/GUser;)I

    goto :goto_0
.end method

.method protected co()Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    .line 250
    iget-object v0, p0, Lcom/glympse/android/lib/dc;->pH:Lcom/glympse/android/lib/dc$a;

    iget-object v2, v0, Lcom/glympse/android/lib/dc$a;->pJ:Lcom/glympse/android/lib/cw;

    .line 251
    iget-object v0, v2, Lcom/glympse/android/lib/cw;->mI:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 255
    invoke-virtual {p0}, Lcom/glympse/android/lib/dc;->cm()Z

    move-result v0

    .line 297
    :goto_0
    return v0

    .line 258
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/dc;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getGroupManager()Lcom/glympse/android/api/GGroupManager;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GGroupManagerPrivate;

    .line 261
    iget-object v3, v2, Lcom/glympse/android/lib/cw;->mI:Ljava/lang/String;

    invoke-interface {v0, v3}, Lcom/glympse/android/lib/GGroupManagerPrivate;->findGroupByGroupId(Ljava/lang/String;)Lcom/glympse/android/api/GGroup;

    move-result-object v3

    .line 262
    if-eqz v3, :cond_1

    move v0, v1

    .line 265
    goto :goto_0

    .line 267
    :cond_1
    iget-object v3, v2, Lcom/glympse/android/lib/cw;->mI:Ljava/lang/String;

    invoke-interface {v0, v3}, Lcom/glympse/android/lib/GGroupManagerPrivate;->findPendingGroupByGroupId(Ljava/lang/String;)Lcom/glympse/android/api/GGroup;

    move-result-object v3

    .line 268
    if-eqz v3, :cond_2

    move v0, v1

    .line 271
    goto :goto_0

    .line 275
    :cond_2
    new-instance v3, Lcom/glympse/android/lib/bw;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/glympse/android/lib/bw;-><init>(Z)V

    .line 276
    iget-object v4, p0, Lcom/glympse/android/lib/dc;->mQ:Ljava/lang/String;

    invoke-interface {v3, v4}, Lcom/glympse/android/lib/GGroupPrivate;->setCode(Ljava/lang/String;)V

    .line 279
    invoke-interface {v0, v3}, Lcom/glympse/android/lib/GGroupManagerPrivate;->addPendingGroup(Lcom/glympse/android/lib/GGroupPrivate;)V

    .line 282
    iget-object v4, v2, Lcom/glympse/android/lib/cw;->mI:Ljava/lang/String;

    invoke-interface {v3, v4}, Lcom/glympse/android/lib/GGroupPrivate;->setId(Ljava/lang/String;)V

    .line 284
    iget-object v2, v2, Lcom/glympse/android/lib/cw;->oX:Lcom/glympse/android/lib/GUserPrivate;

    invoke-interface {v2}, Lcom/glympse/android/lib/GUserPrivate;->getId()Ljava/lang/String;

    move-result-object v2

    .line 285
    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 288
    new-instance v4, Lcom/glympse/android/lib/cf;

    invoke-direct {v4, v5, v5}, Lcom/glympse/android/lib/cf;-><init>(Lcom/glympse/android/api/GUser;Lcom/glympse/android/api/GTicket;)V

    .line 289
    invoke-interface {v4, v2}, Lcom/glympse/android/lib/GGroupMemberPrivate;->setUserId(Ljava/lang/String;)V

    .line 290
    invoke-interface {v3, v4}, Lcom/glympse/android/lib/GGroupPrivate;->addMember(Lcom/glympse/android/lib/GGroupMemberPrivate;)V

    .line 294
    :cond_3
    const/4 v2, 0x3

    invoke-interface {v3, v2}, Lcom/glympse/android/lib/GGroupPrivate;->setState(I)V

    .line 295
    iget-object v2, p0, Lcom/glympse/android/lib/dc;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/16 v4, 0x9

    const/4 v5, 0x2

    invoke-interface {v0, v2, v4, v5, v3}, Lcom/glympse/android/lib/GGroupManagerPrivate;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    move v0, v1

    .line 297
    goto :goto_0
.end method

.method protected cp()Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 305
    iget-object v0, p0, Lcom/glympse/android/lib/dc;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getUserManager()Lcom/glympse/android/api/GUserManager;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GUserManagerPrivate;

    .line 306
    iget-object v1, p0, Lcom/glympse/android/lib/dc;->pH:Lcom/glympse/android/lib/dc$a;

    iget-object v1, v1, Lcom/glympse/android/lib/dc$a;->pK:Lcom/glympse/android/lib/cy;

    iget-object v1, v1, Lcom/glympse/android/lib/cy;->oZ:Lcom/glympse/android/lib/GUserPrivate;

    .line 308
    iget-object v2, p0, Lcom/glympse/android/lib/dc;->pH:Lcom/glympse/android/lib/dc$a;

    iget-object v2, v2, Lcom/glympse/android/lib/dc$a;->pK:Lcom/glympse/android/lib/cy;

    iget-object v2, v2, Lcom/glympse/android/lib/cy;->oG:Lcom/glympse/android/lib/GTicketPrivate;

    .line 311
    iget-object v3, p0, Lcom/glympse/android/lib/dc;->mQ:Ljava/lang/String;

    invoke-interface {v2, v3}, Lcom/glympse/android/lib/GTicketPrivate;->setCode(Ljava/lang/String;)V

    .line 314
    iget-object v3, p0, Lcom/glympse/android/lib/dc;->pH:Lcom/glympse/android/lib/dc$a;

    iget-object v3, v3, Lcom/glympse/android/lib/dc$a;->pK:Lcom/glympse/android/lib/cy;

    iget-object v3, v3, Lcom/glympse/android/lib/cy;->ou:Ljava/lang/String;

    .line 315
    invoke-static {v3}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 317
    invoke-interface {v2, v3}, Lcom/glympse/android/lib/GTicketPrivate;->setReference(Ljava/lang/String;)V

    .line 321
    :cond_0
    invoke-virtual {p0, v2}, Lcom/glympse/android/lib/dc;->q(Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 324
    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GUserManagerPrivate;->resolveUser(Lcom/glympse/android/lib/GUserPrivate;)Lcom/glympse/android/lib/GUserPrivate;

    move-result-object v0

    .line 327
    new-instance v1, Lcom/glympse/android/lib/gh;

    iget-object v3, p0, Lcom/glympse/android/lib/dc;->mP:Lcom/glympse/android/api/GInvite;

    invoke-direct {v1, v0, v2, v3}, Lcom/glympse/android/lib/gh;-><init>(Lcom/glympse/android/api/GUser;Lcom/glympse/android/api/GTicket;Lcom/glympse/android/api/GInvite;)V

    .line 328
    iget-object v0, p0, Lcom/glympse/android/lib/dc;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget-object v2, p0, Lcom/glympse/android/lib/dc;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/high16 v3, 0x800000

    invoke-interface {v0, v2, v5, v3, v1}, Lcom/glympse/android/lib/GGlympsePrivate;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 329
    return v5
.end method

.method public cq()Lcom/glympse/android/api/GTicket;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/glympse/android/lib/dc;->pH:Lcom/glympse/android/lib/dc$a;

    iget-object v0, v0, Lcom/glympse/android/lib/dc$a;->pI:Lcom/glympse/android/lib/cz;

    iget-object v0, v0, Lcom/glympse/android/lib/cz;->pe:Lcom/glympse/android/lib/GTicketPrivate;

    return-object v0
.end method

.method public process()Z
    .locals 6

    .prologue
    .line 109
    iget-object v0, p0, Lcom/glympse/android/lib/dc;->pH:Lcom/glympse/android/lib/dc$a;

    iget-object v0, v0, Lcom/glympse/android/lib/dc$a;->gF:Ljava/lang/String;

    const-string v1, "ok"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 114
    iget-object v0, p0, Lcom/glympse/android/lib/dc;->pH:Lcom/glympse/android/lib/dc$a;

    iget-object v0, v0, Lcom/glympse/android/lib/dc$a;->pI:Lcom/glympse/android/lib/cz;

    if-eqz v0, :cond_1

    .line 116
    invoke-virtual {p0}, Lcom/glympse/android/lib/dc;->cn()Z

    move-result v0

    .line 133
    :goto_0
    iget-object v1, p0, Lcom/glympse/android/lib/dc;->gr:Lcom/glympse/android/api/GEventSink;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/glympse/android/lib/dc;->pA:I

    if-eqz v1, :cond_0

    .line 135
    iget-object v1, p0, Lcom/glympse/android/lib/dc;->gr:Lcom/glympse/android/api/GEventSink;

    iget-object v2, p0, Lcom/glympse/android/lib/dc;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget v3, p0, Lcom/glympse/android/lib/dc;->oC:I

    iget v4, p0, Lcom/glympse/android/lib/dc;->pA:I

    iget-object v5, p0, Lcom/glympse/android/lib/dc;->mQ:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/glympse/android/api/GEventSink;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 144
    :cond_0
    :goto_1
    return v0

    .line 118
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/dc;->pH:Lcom/glympse/android/lib/dc$a;

    iget-object v0, v0, Lcom/glympse/android/lib/dc$a;->pJ:Lcom/glympse/android/lib/cw;

    if-eqz v0, :cond_2

    .line 120
    invoke-virtual {p0}, Lcom/glympse/android/lib/dc;->co()Z

    move-result v0

    goto :goto_0

    .line 122
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/dc;->pH:Lcom/glympse/android/lib/dc$a;

    iget-object v0, v0, Lcom/glympse/android/lib/dc$a;->pK:Lcom/glympse/android/lib/cy;

    if-eqz v0, :cond_3

    .line 124
    invoke-virtual {p0}, Lcom/glympse/android/lib/dc;->cp()Z

    move-result v0

    goto :goto_0

    .line 129
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 140
    :cond_4
    iget-object v0, p0, Lcom/glympse/android/lib/dc;->pH:Lcom/glympse/android/lib/dc$a;

    iget-object v0, v0, Lcom/glympse/android/lib/dc$a;->gG:Ljava/lang/String;

    const-string v1, "invite_code"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 142
    invoke-virtual {p0}, Lcom/glympse/android/lib/dc;->cm()Z

    move-result v0

    goto :goto_1

    .line 144
    :cond_5
    const/4 v0, 0x1

    goto :goto_1
.end method

.method protected q(Lcom/glympse/android/lib/GTicketPrivate;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 345
    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getInvites()Lcom/glympse/android/core/GArray;

    move-result-object v3

    .line 346
    if-nez v3, :cond_0

    move v1, v0

    :goto_0
    move v2, v0

    .line 347
    :goto_1
    if-ge v2, v1, :cond_1

    .line 349
    invoke-interface {v3, v2}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GInvitePrivate;

    .line 350
    iget-object v4, p0, Lcom/glympse/android/lib/dc;->mQ:Ljava/lang/String;

    invoke-interface {v0, v4}, Lcom/glympse/android/lib/GInvitePrivate;->setReference(Ljava/lang/String;)V

    .line 347
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 346
    :cond_0
    invoke-interface {v3}, Lcom/glympse/android/core/GArray;->length()I

    move-result v1

    goto :goto_0

    .line 352
    :cond_1
    return-void
.end method

.method public url(Ljava/lang/StringBuilder;)Z
    .locals 6

    .prologue
    const/16 v1, 0x26

    .line 71
    const-string v0, "invites/"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    iget-object v0, p0, Lcom/glympse/android/lib/dc;->mQ:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    const/16 v0, 0x3f

    .line 74
    iget-boolean v2, p0, Lcom/glympse/android/lib/dc;->pE:Z

    if-eqz v2, :cond_2

    .line 76
    const-string v2, "/properties"

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    :cond_0
    :goto_0
    iget-boolean v2, p0, Lcom/glympse/android/lib/dc;->pG:Z

    if-nez v2, :cond_1

    .line 95
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 96
    const-string v0, "full_trail=true"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    .line 99
    :cond_1
    if-ne v1, v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 80
    :cond_2
    iget-boolean v2, p0, Lcom/glympse/android/lib/dc;->pD:Z

    if-nez v2, :cond_3

    .line 82
    const-string v0, "?limit=0"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    .line 85
    :cond_3
    const-wide/16 v2, 0x0

    iget-wide v4, p0, Lcom/glympse/android/lib/dc;->mM:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 87
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 88
    const-string v0, "next="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    iget-wide v2, p0, Lcom/glympse/android/lib/dc;->mM:J

    invoke-virtual {p1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move v0, v1

    .line 90
    goto :goto_0

    .line 99
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public userAgent()Z
    .locals 1

    .prologue
    .line 104
    const/4 v0, 0x1

    return v0
.end method
