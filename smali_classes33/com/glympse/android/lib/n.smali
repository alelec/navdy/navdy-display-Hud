.class Lcom/glympse/android/lib/n;
.super Ljava/lang/Object;
.source "AutoProxArrivalWrapper.java"

# interfaces
.implements Lcom/glympse/android/lib/be;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/lib/n$a;,
        Lcom/glympse/android/lib/n$b;
    }
.end annotation


# instance fields
.field private cS:Lcom/glympse/android/api/GGlympse;

.field private cW:Lcom/glympse/android/hal/GHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GHashtable",
            "<",
            "Lcom/glympse/android/api/GTicket;",
            "Lcom/glympse/android/core/GRegion;",
            ">;"
        }
    .end annotation
.end field

.field private hk:Lcom/glympse/android/lib/bd;

.field private hl:Lcom/glympse/android/core/GProximityProvider;

.field private hm:Lcom/glympse/android/lib/n$a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    return-void
.end method


# virtual methods
.method public a(Lcom/glympse/android/api/GTicket;)V
    .locals 11

    .prologue
    .line 67
    invoke-interface {p1}, Lcom/glympse/android/api/GTicket;->getDestination()Lcom/glympse/android/api/GPlace;

    move-result-object v0

    .line 68
    invoke-interface {p1}, Lcom/glympse/android/api/GTicket;->getId()Ljava/lang/String;

    move-result-object v10

    .line 71
    const-wide/high16 v6, 0x404e000000000000L    # 60.0

    .line 73
    new-instance v1, Lcom/glympse/android/lib/es;

    invoke-interface {v0}, Lcom/glympse/android/api/GPlace;->getLatitude()D

    move-result-wide v2

    invoke-interface {v0}, Lcom/glympse/android/api/GPlace;->getLongitude()D

    move-result-wide v4

    const-wide/high16 v8, 0x4008000000000000L    # 3.0

    invoke-direct/range {v1 .. v10}, Lcom/glympse/android/lib/es;-><init>(DDDDLjava/lang/String;)V

    .line 74
    iget-object v0, p0, Lcom/glympse/android/lib/n;->cW:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1, v1}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    iget-object v0, p0, Lcom/glympse/android/lib/n;->hl:Lcom/glympse/android/core/GProximityProvider;

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GProximityProvider;->startMonitoring(Lcom/glympse/android/core/GRegion;)V

    .line 76
    return-void
.end method

.method public a(Lcom/glympse/android/lib/bd;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/glympse/android/lib/n;->hk:Lcom/glympse/android/lib/bd;

    .line 63
    return-void
.end method

.method public aq()V
    .locals 4

    .prologue
    .line 91
    new-instance v2, Lcom/glympse/android/hal/GVector;

    invoke-direct {v2}, Lcom/glympse/android/hal/GVector;-><init>()V

    .line 93
    iget-object v0, p0, Lcom/glympse/android/lib/n;->cW:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GHashtable;->keys()Ljava/util/Enumeration;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 95
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    goto :goto_0

    .line 98
    :cond_0
    invoke-virtual {v2}, Lcom/glympse/android/hal/GVector;->length()I

    move-result v3

    .line 99
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    .line 101
    invoke-virtual {v2, v1}, Lcom/glympse/android/hal/GVector;->at(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GTicket;

    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/n;->b(Lcom/glympse/android/api/GTicket;)V

    .line 99
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 103
    :cond_1
    return-void
.end method

.method public b(Lcom/glympse/android/api/GTicket;)V
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/glympse/android/lib/n;->cW:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GRegion;

    .line 81
    if-eqz v0, :cond_0

    .line 83
    iget-object v1, p0, Lcom/glympse/android/lib/n;->hl:Lcom/glympse/android/core/GProximityProvider;

    invoke-interface {v1, v0}, Lcom/glympse/android/core/GProximityProvider;->stopMonitoring(Lcom/glympse/android/core/GRegion;)V

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/n;->cW:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    return-void
.end method

.method public b(Lcom/glympse/android/core/GPrimitive;)V
    .locals 0

    .prologue
    .line 58
    return-void
.end method

.method public locationChanged(Lcom/glympse/android/core/GLocation;)V
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/glympse/android/lib/n;->hl:Lcom/glympse/android/core/GProximityProvider;

    invoke-interface {v0, p1}, Lcom/glympse/android/core/GProximityProvider;->locationChanged(Lcom/glympse/android/core/GLocation;)V

    .line 134
    return-void
.end method

.method public regionEntered(Lcom/glympse/android/core/GRegion;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 107
    const/4 v1, 0x0

    .line 108
    iget-object v0, p0, Lcom/glympse/android/lib/n;->cW:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GHashtable;->keys()Ljava/util/Enumeration;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GTicket;

    .line 111
    iget-object v3, p0, Lcom/glympse/android/lib/n;->cW:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v3, v0}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-ne v3, p1, :cond_3

    :goto_1
    move-object v1, v0

    .line 115
    goto :goto_0

    .line 117
    :cond_0
    if-eqz v1, :cond_2

    .line 119
    invoke-static {}, Lcom/glympse/android/lib/Debug;->getLevel()I

    move-result v0

    if-gt v0, v4, :cond_1

    .line 121
    const-string v0, "[Ticket arrival triggered] Provider: AutoProxProvider "

    invoke-static {v4, v0}, Lcom/glympse/android/hal/Helpers;->log(ILjava/lang/String;)V

    .line 124
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/n;->hk:Lcom/glympse/android/lib/bd;

    if-eqz v0, :cond_2

    .line 126
    iget-object v0, p0, Lcom/glympse/android/lib/n;->hk:Lcom/glympse/android/lib/bd;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/bd;->c(Lcom/glympse/android/api/GTicket;)V

    .line 129
    :cond_2
    return-void

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method public start(Lcom/glympse/android/api/GGlympse;)V
    .locals 3

    .prologue
    .line 32
    new-instance v0, Lcom/glympse/android/hal/GHashtable;

    invoke-direct {v0}, Lcom/glympse/android/hal/GHashtable;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/n;->cW:Lcom/glympse/android/hal/GHashtable;

    .line 34
    iput-object p1, p0, Lcom/glympse/android/lib/n;->cS:Lcom/glympse/android/api/GGlympse;

    .line 36
    new-instance v0, Lcom/glympse/android/lib/o;

    iget-object v1, p0, Lcom/glympse/android/lib/n;->cS:Lcom/glympse/android/api/GGlympse;

    invoke-interface {v1}, Lcom/glympse/android/api/GGlympse;->getHandler()Lcom/glympse/android/core/GHandler;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/o;-><init>(Lcom/glympse/android/core/GHandler;)V

    iput-object v0, p0, Lcom/glympse/android/lib/n;->hl:Lcom/glympse/android/core/GProximityProvider;

    .line 37
    new-instance v1, Lcom/glympse/android/lib/n$b;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/n;

    invoke-direct {v1, v0}, Lcom/glympse/android/lib/n$b;-><init>(Lcom/glympse/android/lib/n;)V

    .line 38
    iget-object v0, p0, Lcom/glympse/android/lib/n;->hl:Lcom/glympse/android/core/GProximityProvider;

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GProximityProvider;->setProximityListener(Lcom/glympse/android/core/GProximityListener;)V

    .line 39
    new-instance v0, Lcom/glympse/android/lib/n$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/n$a;-><init>(Lcom/glympse/android/lib/n$1;)V

    iput-object v0, p0, Lcom/glympse/android/lib/n;->hm:Lcom/glympse/android/lib/n$a;

    .line 40
    iget-object v1, p0, Lcom/glympse/android/lib/n;->hm:Lcom/glympse/android/lib/n$a;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/n;

    iget-object v2, p0, Lcom/glympse/android/lib/n;->cS:Lcom/glympse/android/api/GGlympse;

    invoke-virtual {v1, v0, v2}, Lcom/glympse/android/lib/n$a;->a(Lcom/glympse/android/lib/n;Lcom/glympse/android/api/GGlympse;)V

    .line 41
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-virtual {p0}, Lcom/glympse/android/lib/n;->aq()V

    .line 47
    iput-object v1, p0, Lcom/glympse/android/lib/n;->cW:Lcom/glympse/android/hal/GHashtable;

    .line 48
    iput-object v1, p0, Lcom/glympse/android/lib/n;->hk:Lcom/glympse/android/lib/bd;

    .line 49
    iget-object v0, p0, Lcom/glympse/android/lib/n;->hm:Lcom/glympse/android/lib/n$a;

    invoke-virtual {v0}, Lcom/glympse/android/lib/n$a;->stop()V

    .line 50
    iput-object v1, p0, Lcom/glympse/android/lib/n;->hm:Lcom/glympse/android/lib/n$a;

    .line 52
    iput-object v1, p0, Lcom/glympse/android/lib/n;->cS:Lcom/glympse/android/api/GGlympse;

    .line 53
    return-void
.end method
