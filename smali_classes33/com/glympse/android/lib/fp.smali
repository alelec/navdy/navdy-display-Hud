.class Lcom/glympse/android/lib/fp;
.super Ljava/lang/Object;
.source "TicketProtocol.java"

# interfaces
.implements Lcom/glympse/android/lib/GTicketProtocol;


# instance fields
.field private _glympse:Lcom/glympse/android/lib/GGlympsePrivate;

.field private lI:Lcom/glympse/android/lib/GServerPost;

.field private lX:Lcom/glympse/android/lib/GCorrectedTime;

.field private tM:Z

.field private tN:Z

.field private tO:J


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/glympse/android/api/GTravelMode;)Lcom/glympse/android/core/GPrimitive;
    .locals 3

    .prologue
    .line 381
    if-nez p1, :cond_1

    .line 383
    new-instance v0, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v0}, Lcom/glympse/android/lib/Primitive;-><init>()V

    .line 399
    :cond_0
    :goto_0
    return-object v0

    .line 386
    :cond_1
    invoke-interface {p1}, Lcom/glympse/android/api/GTravelMode;->getMode()I

    move-result v1

    .line 387
    if-nez v1, :cond_2

    .line 389
    new-instance v0, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v0}, Lcom/glympse/android/lib/Primitive;-><init>()V

    goto :goto_0

    .line 392
    :cond_2
    new-instance v0, Lcom/glympse/android/lib/Primitive;

    const/4 v2, 0x2

    invoke-direct {v0, v2}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 393
    const-string v2, "type"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, Lcom/glympse/android/lib/fu;->C(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    invoke-interface {p1}, Lcom/glympse/android/api/GTravelMode;->getSettings()Lcom/glympse/android/core/GPrimitive;

    move-result-object v1

    .line 395
    if-eqz v1, :cond_0

    .line 397
    const-string v2, "settings"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    goto :goto_0
.end method


# virtual methods
.method public addInvite(Lcom/glympse/android/lib/GTicketPrivate;Lcom/glympse/android/lib/GInvitePrivate;)V
    .locals 3

    .prologue
    .line 198
    invoke-interface {p2}, Lcom/glympse/android/lib/GInvitePrivate;->getType()I

    move-result v0

    .line 199
    if-nez v0, :cond_0

    .line 202
    const/16 v0, 0x8

    invoke-interface {p2, v0}, Lcom/glympse/android/lib/GInvitePrivate;->setState(I)V

    .line 204
    iget-object v0, p0, Lcom/glympse/android/lib/fp;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/4 v1, 0x4

    const/high16 v2, 0x10000

    invoke-interface {p1, v0, v1, v2, p1}, Lcom/glympse/android/lib/GTicketPrivate;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 213
    :goto_0
    return-void

    .line 209
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/fp;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getRecipientsManager()Lcom/glympse/android/lib/GRecipientsManager;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/glympse/android/lib/GRecipientsManager;->addRecipient(Lcom/glympse/android/api/GInvite;)V

    .line 212
    iget-object v0, p0, Lcom/glympse/android/lib/fp;->lI:Lcom/glympse/android/lib/GServerPost;

    new-instance v1, Lcom/glympse/android/lib/fm;

    iget-object v2, p0, Lcom/glympse/android/lib/fp;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct {v1, p1, p2, v2}, Lcom/glympse/android/lib/fm;-><init>(Lcom/glympse/android/lib/GTicketPrivate;Lcom/glympse/android/lib/GInvitePrivate;Lcom/glympse/android/lib/GGlympsePrivate;)V

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/glympse/android/lib/GServerPost;->invokeEndpoint(Lcom/glympse/android/lib/GApiEndpoint;Z)V

    goto :goto_0
.end method

.method public appendCompleted(Lcom/glympse/android/lib/GTicketPrivate;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 257
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[TicketProtocol.appendCompleted] Completing: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->safeStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 260
    new-instance v0, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v0, v8}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 262
    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getExpireTime()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    const-string v1, "completed"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v7, v8}, Lcom/glympse/android/lib/Primitive;-><init>(Z)V

    move-object v1, p0

    .line 261
    invoke-virtual/range {v1 .. v7}, Lcom/glympse/android/lib/fp;->prepareProperty(JJLjava/lang/String;Lcom/glympse/android/core/GPrimitive;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v1

    .line 263
    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->put(Lcom/glympse/android/core/GPrimitive;)V

    .line 266
    iget-object v1, p0, Lcom/glympse/android/lib/fp;->lI:Lcom/glympse/android/lib/GServerPost;

    new-instance v2, Lcom/glympse/android/lib/ff;

    iget-object v3, p0, Lcom/glympse/android/lib/fp;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct {v2, v3, p1, v0}, Lcom/glympse/android/lib/ff;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/GTicketPrivate;Lcom/glympse/android/core/GPrimitive;)V

    invoke-interface {v1, v2, v8}, Lcom/glympse/android/lib/GServerPost;->invokeEndpoint(Lcom/glympse/android/lib/GApiEndpoint;Z)V

    .line 267
    return-void
.end method

.method public appendData(Lcom/glympse/android/lib/GTicketPrivate;JLjava/lang/String;Lcom/glympse/android/core/GPrimitive;)V
    .locals 10

    .prologue
    const/4 v8, 0x1

    .line 176
    iget-object v0, p0, Lcom/glympse/android/lib/fp;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getCorrectedTime()Lcom/glympse/android/lib/GCorrectedTime;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/lib/GCorrectedTime;->getTime()J

    move-result-wide v2

    .line 177
    new-instance v0, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v0, v8}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    move-object v1, p0

    move-wide v4, p2

    move-object v6, p4

    move-object v7, p5

    .line 178
    invoke-virtual/range {v1 .. v7}, Lcom/glympse/android/lib/fp;->prepareProperty(JJLjava/lang/String;Lcom/glympse/android/core/GPrimitive;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->put(Lcom/glympse/android/core/GPrimitive;)V

    .line 179
    iget-object v1, p0, Lcom/glympse/android/lib/fp;->lI:Lcom/glympse/android/lib/GServerPost;

    new-instance v2, Lcom/glympse/android/lib/ah;

    iget-object v3, p0, Lcom/glympse/android/lib/fp;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4, v0}, Lcom/glympse/android/lib/ah;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    invoke-interface {v1, v2, v8}, Lcom/glympse/android/lib/GServerPost;->invokeEndpoint(Lcom/glympse/android/lib/GApiEndpoint;Z)V

    .line 180
    return-void
.end method

.method public clearTicket(Lcom/glympse/android/api/GTicket;ZZ)V
    .locals 9

    .prologue
    const-wide/16 v4, 0x0

    const/4 v8, 0x1

    .line 150
    iget-object v0, p0, Lcom/glympse/android/lib/fp;->lX:Lcom/glympse/android/lib/GCorrectedTime;

    invoke-interface {v0}, Lcom/glympse/android/lib/GCorrectedTime;->getTime()J

    move-result-wide v2

    .line 153
    new-instance v0, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v0, v8}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 156
    if-eqz p2, :cond_0

    .line 158
    const-string v1, "message"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v7}, Lcom/glympse/android/lib/Primitive;-><init>()V

    move-object v1, p0

    invoke-virtual/range {v1 .. v7}, Lcom/glympse/android/lib/fp;->prepareProperty(JJLjava/lang/String;Lcom/glympse/android/core/GPrimitive;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->put(Lcom/glympse/android/core/GPrimitive;)V

    .line 162
    :cond_0
    if-eqz p3, :cond_1

    .line 164
    const-string v1, "destination"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v7}, Lcom/glympse/android/lib/Primitive;-><init>()V

    move-object v1, p0

    invoke-virtual/range {v1 .. v7}, Lcom/glympse/android/lib/fp;->prepareProperty(JJLjava/lang/String;Lcom/glympse/android/core/GPrimitive;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->put(Lcom/glympse/android/core/GPrimitive;)V

    .line 168
    :cond_1
    invoke-interface {v0}, Lcom/glympse/android/core/GPrimitive;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 170
    iget-object v1, p0, Lcom/glympse/android/lib/fp;->lI:Lcom/glympse/android/lib/GServerPost;

    new-instance v2, Lcom/glympse/android/lib/ah;

    iget-object v3, p0, Lcom/glympse/android/lib/fp;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {p1}, Lcom/glympse/android/api/GTicket;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4, v0}, Lcom/glympse/android/lib/ah;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    invoke-interface {v1, v2, v8}, Lcom/glympse/android/lib/GServerPost;->invokeEndpoint(Lcom/glympse/android/lib/GApiEndpoint;Z)V

    .line 172
    :cond_2
    return-void
.end method

.method public deleteInvite(Lcom/glympse/android/lib/GTicketPrivate;Lcom/glympse/android/lib/GInvitePrivate;)V
    .locals 3

    .prologue
    .line 217
    iget-object v0, p0, Lcom/glympse/android/lib/fp;->lI:Lcom/glympse/android/lib/GServerPost;

    new-instance v1, Lcom/glympse/android/lib/cv;

    iget-object v2, p0, Lcom/glympse/android/lib/fp;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct {v1, v2, p1, p2}, Lcom/glympse/android/lib/cv;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/GTicketPrivate;Lcom/glympse/android/lib/GInvitePrivate;)V

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/glympse/android/lib/GServerPost;->invokeEndpoint(Lcom/glympse/android/lib/GApiEndpoint;Z)V

    .line 218
    return-void
.end method

.method public deleteTicket(Lcom/glympse/android/api/GTicket;)V
    .locals 3

    .prologue
    .line 184
    iget-object v0, p0, Lcom/glympse/android/lib/fp;->lI:Lcom/glympse/android/lib/GServerPost;

    new-instance v1, Lcom/glympse/android/lib/fh;

    iget-object v2, p0, Lcom/glympse/android/lib/fp;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct {v1, v2, p1}, Lcom/glympse/android/lib/fh;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/api/GTicket;)V

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/glympse/android/lib/GServerPost;->invokeEndpoint(Lcom/glympse/android/lib/GApiEndpoint;Z)V

    .line 185
    return-void
.end method

.method public doneRefreshingInvites()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 296
    iput-boolean v1, p0, Lcom/glympse/android/lib/fp;->tM:Z

    .line 299
    iget-boolean v0, p0, Lcom/glympse/android/lib/fp;->tN:Z

    if-eqz v0, :cond_0

    .line 301
    iput-boolean v1, p0, Lcom/glympse/android/lib/fp;->tN:Z

    .line 302
    invoke-virtual {p0}, Lcom/glympse/android/lib/fp;->refreshInvites()V

    .line 304
    :cond_0
    return-void
.end method

.method public expireTicket(Lcom/glympse/android/lib/GTicketPrivate;)V
    .locals 3

    .prologue
    .line 237
    iget-object v0, p0, Lcom/glympse/android/lib/fp;->lI:Lcom/glympse/android/lib/GServerPost;

    new-instance v1, Lcom/glympse/android/lib/fj;

    iget-object v2, p0, Lcom/glympse/android/lib/fp;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct {v1, v2, p1}, Lcom/glympse/android/lib/fj;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/api/GTicket;)V

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/glympse/android/lib/GServerPost;->invokeEndpoint(Lcom/glympse/android/lib/GApiEndpoint;Z)V

    .line 238
    return-void
.end method

.method public getLastInviteRefreshTime()J
    .locals 2

    .prologue
    .line 313
    iget-wide v0, p0, Lcom/glympse/android/lib/fp;->tO:J

    return-wide v0
.end method

.method public handoffTicket(Lcom/glympse/android/lib/GTicketPrivate;)V
    .locals 5

    .prologue
    .line 227
    iget-object v0, p0, Lcom/glympse/android/lib/fp;->lI:Lcom/glympse/android/lib/GServerPost;

    new-instance v1, Lcom/glympse/android/lib/fk;

    iget-object v2, p0, Lcom/glympse/android/lib/fp;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getProviderId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getProviderData()Lcom/glympse/android/core/GPrimitive;

    move-result-object v4

    invoke-direct {v1, v2, p1, v3, v4}, Lcom/glympse/android/lib/fk;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/GTicketPrivate;Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/glympse/android/lib/GServerPost;->invokeEndpoint(Lcom/glympse/android/lib/GApiEndpoint;Z)V

    .line 228
    return-void
.end method

.method public ownTicket(Lcom/glympse/android/lib/GTicketPrivate;)V
    .locals 3

    .prologue
    .line 222
    iget-object v0, p0, Lcom/glympse/android/lib/fp;->lI:Lcom/glympse/android/lib/GServerPost;

    new-instance v1, Lcom/glympse/android/lib/fn;

    iget-object v2, p0, Lcom/glympse/android/lib/fp;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct {v1, v2, p1}, Lcom/glympse/android/lib/fn;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/GTicketPrivate;)V

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/glympse/android/lib/GServerPost;->invokeEndpoint(Lcom/glympse/android/lib/GApiEndpoint;Z)V

    .line 223
    return-void
.end method

.method public prepareEtaProperty(JJJ)Lcom/glympse/android/core/GPrimitive;
    .locals 9

    .prologue
    .line 348
    new-instance v7, Lcom/glympse/android/lib/Primitive;

    const/4 v0, 0x2

    invoke-direct {v7, v0}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 349
    const-string v0, "eta"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v7, v0, p5, p6}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 350
    const-string v0, "eta_ts"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v7, v0, p3, p4}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 351
    const-wide/16 v4, 0x0

    const-string v0, "eta"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object v1, p0

    move-wide v2, p1

    invoke-virtual/range {v1 .. v7}, Lcom/glympse/android/lib/fp;->prepareProperty(JJLjava/lang/String;Lcom/glympse/android/core/GPrimitive;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    return-object v0
.end method

.method public prepareMessageProperty(JLjava/lang/String;)Lcom/glympse/android/core/GPrimitive;
    .locals 9

    .prologue
    .line 323
    iget-object v0, p0, Lcom/glympse/android/lib/fp;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getMessagesManager()Lcom/glympse/android/lib/GMessagesManager;

    move-result-object v0

    invoke-interface {v0, p3}, Lcom/glympse/android/lib/GMessagesManager;->addMessage(Ljava/lang/String;)V

    .line 326
    const-wide/16 v4, 0x0

    const-string v0, "message"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v7, p3}, Lcom/glympse/android/lib/Primitive;-><init>(Ljava/lang/String;)V

    move-object v1, p0

    move-wide v2, p1

    invoke-virtual/range {v1 .. v7}, Lcom/glympse/android/lib/fp;->prepareProperty(JJLjava/lang/String;Lcom/glympse/android/core/GPrimitive;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    return-object v0
.end method

.method public preparePlaceProperty(JLcom/glympse/android/api/GPlace;)Lcom/glympse/android/core/GPrimitive;
    .locals 9

    .prologue
    .line 332
    iget-object v0, p0, Lcom/glympse/android/lib/fp;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getPlacesManager()Lcom/glympse/android/lib/GPlacesManager;

    move-result-object v0

    invoke-interface {v0, p3}, Lcom/glympse/android/lib/GPlacesManager;->addPlace(Lcom/glympse/android/api/GPlace;)V

    .line 335
    new-instance v7, Lcom/glympse/android/lib/Primitive;

    const/4 v0, 0x2

    invoke-direct {v7, v0}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 336
    const-string v0, "lat"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p3}, Lcom/glympse/android/api/GPlace;->getLatitude()D

    move-result-wide v2

    invoke-interface {v7, v0, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;D)V

    .line 337
    const-string v0, "lng"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p3}, Lcom/glympse/android/api/GPlace;->getLongitude()D

    move-result-wide v2

    invoke-interface {v7, v0, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;D)V

    .line 338
    invoke-interface {p3}, Lcom/glympse/android/api/GPlace;->getName()Ljava/lang/String;

    move-result-object v0

    .line 339
    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 341
    const-string v1, "name"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v7, v1, v0}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    :cond_0
    const-wide/16 v4, 0x0

    const-string v0, "destination"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object v1, p0

    move-wide v2, p1

    invoke-virtual/range {v1 .. v7}, Lcom/glympse/android/lib/fp;->prepareProperty(JJLjava/lang/String;Lcom/glympse/android/core/GPrimitive;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    return-object v0
.end method

.method public prepareProperty(JJLjava/lang/String;Lcom/glympse/android/core/GPrimitive;)Lcom/glympse/android/core/GPrimitive;
    .locals 3

    .prologue
    .line 59
    new-instance v0, Lcom/glympse/android/lib/Primitive;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 60
    const-string v1, "t"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1, p2}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 61
    const-string v1, "pid"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p3, p4}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 62
    const-string v1, "n"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p5}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    const-string v1, "v"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p6}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 64
    return-object v0
.end method

.method public prepareRouteProperty(JLcom/glympse/android/api/GTrack;)Lcom/glympse/android/core/GPrimitive;
    .locals 9

    .prologue
    .line 356
    new-instance v7, Lcom/glympse/android/lib/Primitive;

    const/4 v0, 0x2

    invoke-direct {v7, v0}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 357
    invoke-static {p3}, Lcom/glympse/android/lib/an;->a(Lcom/glympse/android/api/GTrack;)Ljava/lang/String;

    move-result-object v0

    .line 358
    const-string v1, "points"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v7, v1, v0}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    check-cast p3, Lcom/glympse/android/lib/GTrackPrivate;

    .line 361
    invoke-interface {p3}, Lcom/glympse/android/lib/GTrackPrivate;->getSource()I

    move-result v0

    .line 362
    if-eqz v0, :cond_0

    .line 364
    const-string v1, "src"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/glympse/android/lib/Primitive;

    int-to-long v4, v0

    invoke-direct {v2, v4, v5}, Lcom/glympse/android/lib/Primitive;-><init>(J)V

    invoke-interface {v7, v1, v2}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 366
    :cond_0
    invoke-interface {p3}, Lcom/glympse/android/lib/GTrackPrivate;->getDistance()I

    move-result v0

    .line 367
    if-eqz v0, :cond_1

    .line 369
    const-string v1, "distance"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/glympse/android/lib/Primitive;

    int-to-long v4, v0

    invoke-direct {v2, v4, v5}, Lcom/glympse/android/lib/Primitive;-><init>(J)V

    invoke-interface {v7, v1, v2}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 371
    :cond_1
    const-wide/16 v4, 0x0

    const-string v0, "route"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object v1, p0

    move-wide v2, p1

    invoke-virtual/range {v1 .. v7}, Lcom/glympse/android/lib/fp;->prepareProperty(JJLjava/lang/String;Lcom/glympse/android/core/GPrimitive;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    return-object v0
.end method

.method public prepareTravelModeProperty(JLcom/glympse/android/api/GTravelMode;)Lcom/glympse/android/core/GPrimitive;
    .locals 9

    .prologue
    .line 376
    const-wide/16 v4, 0x0

    const-string v0, "travel_mode"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, p3}, Lcom/glympse/android/lib/fp;->a(Lcom/glympse/android/api/GTravelMode;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v7

    move-object v1, p0

    move-wide v2, p1

    invoke-virtual/range {v1 .. v7}, Lcom/glympse/android/lib/fp;->prepareProperty(JJLjava/lang/String;Lcom/glympse/android/core/GPrimitive;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    return-object v0
.end method

.method public refreshInvites()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 277
    iget-boolean v0, p0, Lcom/glympse/android/lib/fp;->tM:Z

    if-eqz v0, :cond_0

    .line 279
    iput-boolean v3, p0, Lcom/glympse/android/lib/fp;->tN:Z

    .line 285
    :goto_0
    return-void

    .line 284
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/fp;->lI:Lcom/glympse/android/lib/GServerPost;

    new-instance v1, Lcom/glympse/android/lib/ge;

    iget-object v2, p0, Lcom/glympse/android/lib/fp;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct {v1, v2}, Lcom/glympse/android/lib/ge;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;)V

    invoke-interface {v0, v1, v3}, Lcom/glympse/android/lib/GServerPost;->invokeEndpoint(Lcom/glympse/android/lib/GApiEndpoint;Z)V

    goto :goto_0
.end method

.method public refreshTicket(Lcom/glympse/android/lib/GTicketPrivate;Z)V
    .locals 3

    .prologue
    .line 242
    iget-object v0, p0, Lcom/glympse/android/lib/fp;->lI:Lcom/glympse/android/lib/GServerPost;

    new-instance v1, Lcom/glympse/android/lib/fr;

    iget-object v2, p0, Lcom/glympse/android/lib/fp;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct {v1, v2, p1, p2}, Lcom/glympse/android/lib/fr;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/GTicketPrivate;Z)V

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/glympse/android/lib/GServerPost;->invokeEndpoint(Lcom/glympse/android/lib/GApiEndpoint;Z)V

    .line 243
    return-void
.end method

.method public setLastInviteRefreshTime(J)V
    .locals 1

    .prologue
    .line 308
    iput-wide p1, p0, Lcom/glympse/android/lib/fp;->tO:J

    .line 309
    return-void
.end method

.method public setVisibility(Lcom/glympse/android/lib/GTicketPrivate;Lcom/glympse/android/core/GPrimitive;)V
    .locals 3

    .prologue
    .line 247
    iget-object v0, p0, Lcom/glympse/android/lib/fp;->lI:Lcom/glympse/android/lib/GServerPost;

    new-instance v1, Lcom/glympse/android/lib/ez;

    iget-object v2, p0, Lcom/glympse/android/lib/fp;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct {v1, v2, p1, p2}, Lcom/glympse/android/lib/ez;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/api/GTicket;Lcom/glympse/android/core/GPrimitive;)V

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/glympse/android/lib/GServerPost;->invokeEndpoint(Lcom/glympse/android/lib/GApiEndpoint;Z)V

    .line 248
    return-void
.end method

.method public start(Lcom/glympse/android/lib/GGlympsePrivate;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    iput-object p1, p0, Lcom/glympse/android/lib/fp;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 39
    invoke-interface {p1}, Lcom/glympse/android/lib/GGlympsePrivate;->getServerPost()Lcom/glympse/android/lib/GServerPost;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/fp;->lI:Lcom/glympse/android/lib/GServerPost;

    .line 40
    invoke-interface {p1}, Lcom/glympse/android/lib/GGlympsePrivate;->getCorrectedTime()Lcom/glympse/android/lib/GCorrectedTime;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/fp;->lX:Lcom/glympse/android/lib/GCorrectedTime;

    .line 41
    iput-boolean v1, p0, Lcom/glympse/android/lib/fp;->tM:Z

    .line 42
    iput-boolean v1, p0, Lcom/glympse/android/lib/fp;->tN:Z

    .line 43
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/glympse/android/lib/fp;->tO:J

    .line 44
    return-void
.end method

.method public startedRefreshingInvites()V
    .locals 1

    .prologue
    .line 290
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/glympse/android/lib/fp;->tM:Z

    .line 291
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 48
    iput-object v0, p0, Lcom/glympse/android/lib/fp;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 49
    iput-object v0, p0, Lcom/glympse/android/lib/fp;->lI:Lcom/glympse/android/lib/GServerPost;

    .line 50
    iput-object v0, p0, Lcom/glympse/android/lib/fp;->lX:Lcom/glympse/android/lib/GCorrectedTime;

    .line 51
    return-void
.end method

.method public updateInvite(Lcom/glympse/android/api/GInvite;)V
    .locals 3

    .prologue
    .line 189
    iget-object v0, p0, Lcom/glympse/android/lib/fp;->lI:Lcom/glympse/android/lib/GServerPost;

    new-instance v1, Lcom/glympse/android/lib/db;

    iget-object v2, p0, Lcom/glympse/android/lib/fp;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct {v1, v2, p1}, Lcom/glympse/android/lib/db;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/api/GInvite;)V

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/glympse/android/lib/GServerPost;->invokeEndpoint(Lcom/glympse/android/lib/GApiEndpoint;Z)V

    .line 190
    return-void
.end method

.method public updateTicket(Lcom/glympse/android/api/GTicket;Ljava/lang/String;Lcom/glympse/android/api/GPlace;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 74
    new-instance v0, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v0, v5}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 75
    iget-object v1, p0, Lcom/glympse/android/lib/fp;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->getCorrectedTime()Lcom/glympse/android/lib/GCorrectedTime;

    move-result-object v1

    invoke-interface {v1}, Lcom/glympse/android/lib/GCorrectedTime;->getTime()J

    move-result-wide v2

    .line 78
    invoke-static {p2}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 80
    invoke-virtual {p0, v2, v3, p2}, Lcom/glympse/android/lib/fp;->prepareMessageProperty(JLjava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->put(Lcom/glympse/android/core/GPrimitive;)V

    .line 84
    :cond_0
    if-eqz p3, :cond_1

    .line 86
    invoke-virtual {p0, v2, v3, p3}, Lcom/glympse/android/lib/fp;->preparePlaceProperty(JLcom/glympse/android/api/GPlace;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->put(Lcom/glympse/android/core/GPrimitive;)V

    .line 90
    :cond_1
    invoke-interface {v0}, Lcom/glympse/android/core/GPrimitive;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 92
    iget-object v1, p0, Lcom/glympse/android/lib/fp;->lI:Lcom/glympse/android/lib/GServerPost;

    new-instance v2, Lcom/glympse/android/lib/ah;

    iget-object v3, p0, Lcom/glympse/android/lib/fp;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {p1}, Lcom/glympse/android/api/GTicket;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4, v0}, Lcom/glympse/android/lib/ah;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    invoke-interface {v1, v2, v5}, Lcom/glympse/android/lib/GServerPost;->invokeEndpoint(Lcom/glympse/android/lib/GApiEndpoint;Z)V

    .line 94
    :cond_2
    return-void
.end method

.method public updateTicket(Lcom/glympse/android/lib/GTicketPrivate;)V
    .locals 3

    .prologue
    .line 232
    iget-object v0, p0, Lcom/glympse/android/lib/fp;->lI:Lcom/glympse/android/lib/GServerPost;

    new-instance v1, Lcom/glympse/android/lib/fq;

    iget-object v2, p0, Lcom/glympse/android/lib/fp;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct {v1, v2, p1}, Lcom/glympse/android/lib/fq;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/GTicketPrivate;)V

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/glympse/android/lib/GServerPost;->invokeEndpoint(Lcom/glympse/android/lib/GApiEndpoint;Z)V

    .line 233
    return-void
.end method

.method public updateTicketEta(Lcom/glympse/android/api/GTicket;IJJILcom/glympse/android/api/GTrack;)V
    .locals 13

    .prologue
    .line 98
    iget-object v2, p0, Lcom/glympse/android/lib/fp;->lX:Lcom/glympse/android/lib/GCorrectedTime;

    invoke-interface {v2}, Lcom/glympse/android/lib/GCorrectedTime;->getTime()J

    move-result-wide v4

    .line 99
    new-instance v11, Lcom/glympse/android/lib/Primitive;

    const/4 v2, 0x1

    invoke-direct {v11, v2}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 100
    new-instance v12, Lcom/glympse/android/hal/GHashtable;

    invoke-direct {v12}, Lcom/glympse/android/hal/GHashtable;-><init>()V

    .line 101
    const/4 v10, 0x1

    .line 104
    const/4 v2, 0x0

    .line 105
    if-nez p2, :cond_4

    .line 107
    const-wide/16 v6, 0x0

    const-string v2, "eta"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v9}, Lcom/glympse/android/lib/Primitive;-><init>()V

    move-object v3, p0

    invoke-virtual/range {v3 .. v9}, Lcom/glympse/android/lib/fp;->prepareProperty(JJLjava/lang/String;Lcom/glympse/android/core/GPrimitive;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v2

    .line 108
    invoke-interface {v11, v2}, Lcom/glympse/android/core/GPrimitive;->put(Lcom/glympse/android/core/GPrimitive;)V

    .line 116
    :cond_0
    :goto_0
    if-eqz v2, :cond_1

    .line 118
    const-string v3, "eta"

    invoke-static {v3}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v3, v2}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    :cond_1
    const/4 v2, 0x0

    .line 123
    if-nez p7, :cond_5

    .line 125
    const-wide/16 v6, 0x0

    const-string v2, "route"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v9}, Lcom/glympse/android/lib/Primitive;-><init>()V

    move-object v3, p0

    invoke-virtual/range {v3 .. v9}, Lcom/glympse/android/lib/fp;->prepareProperty(JJLjava/lang/String;Lcom/glympse/android/core/GPrimitive;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v2

    .line 126
    invoke-interface {v11, v2}, Lcom/glympse/android/core/GPrimitive;->put(Lcom/glympse/android/core/GPrimitive;)V

    move v3, v10

    .line 134
    :goto_1
    if-eqz v2, :cond_2

    .line 136
    const-string v4, "route"

    invoke-static {v4}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v12, v4, v2}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    :cond_2
    invoke-interface {v11}, Lcom/glympse/android/core/GPrimitive;->size()I

    move-result v2

    if-lez v2, :cond_3

    .line 144
    iget-object v2, p0, Lcom/glympse/android/lib/fp;->lI:Lcom/glympse/android/lib/GServerPost;

    new-instance v4, Lcom/glympse/android/lib/ah;

    iget-object v5, p0, Lcom/glympse/android/lib/fp;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {p1}, Lcom/glympse/android/api/GTicket;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6, v11, v12}, Lcom/glympse/android/lib/ah;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;Lcom/glympse/android/hal/GHashtable;)V

    const/4 v5, 0x0

    invoke-interface {v2, v4, v5, v3}, Lcom/glympse/android/lib/GServerPost;->invokeEndpoint(Lcom/glympse/android/lib/GApiEndpoint;ZZ)V

    .line 146
    :cond_3
    return-void

    .line 110
    :cond_4
    if-lez p2, :cond_0

    const-wide/16 v6, 0x0

    cmp-long v3, p5, v6

    if-lez v3, :cond_0

    move-object v3, p0

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    .line 112
    invoke-virtual/range {v3 .. v9}, Lcom/glympse/android/lib/fp;->prepareEtaProperty(JJJ)Lcom/glympse/android/core/GPrimitive;

    move-result-object v2

    .line 113
    invoke-interface {v11, v2}, Lcom/glympse/android/core/GPrimitive;->put(Lcom/glympse/android/core/GPrimitive;)V

    .line 114
    const/4 v10, 0x0

    goto :goto_0

    .line 128
    :cond_5
    if-lez p7, :cond_6

    if-eqz p8, :cond_6

    .line 130
    move-object/from16 v0, p8

    invoke-virtual {p0, v4, v5, v0}, Lcom/glympse/android/lib/fp;->prepareRouteProperty(JLcom/glympse/android/api/GTrack;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v2

    .line 131
    invoke-interface {v11, v2}, Lcom/glympse/android/core/GPrimitive;->put(Lcom/glympse/android/core/GPrimitive;)V

    .line 132
    const/4 v3, 0x0

    goto :goto_1

    :cond_6
    move v3, v10

    goto :goto_1
.end method

.method public updateTravelMode(Lcom/glympse/android/lib/GTicketPrivate;Lcom/glympse/android/api/GTravelMode;)V
    .locals 6

    .prologue
    .line 252
    const-wide/16 v2, 0x0

    const-string v0, "travel_mode"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, p2}, Lcom/glympse/android/lib/fp;->a(Lcom/glympse/android/api/GTravelMode;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/glympse/android/lib/fp;->appendData(Lcom/glympse/android/lib/GTicketPrivate;JLjava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 253
    return-void
.end method
