.class Lcom/glympse/android/lib/as;
.super Ljava/lang/Object;
.source "DirectionsProvider.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/glympse/android/lib/GJobQueue;Lcom/glympse/android/core/GPrimitive;Lcom/glympse/android/core/GPrimitive;Lcom/glympse/android/lib/GDirectionsPrivate;)V
    .locals 3

    .prologue
    .line 21
    const-string v0, "src"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    long-to-int v0, v0

    .line 23
    packed-switch v0, :pswitch_data_0

    .line 49
    :goto_0
    :pswitch_0
    return-void

    .line 27
    :pswitch_1
    new-instance v0, Lcom/glympse/android/lib/ap;

    invoke-direct {v0, p3}, Lcom/glympse/android/lib/ap;-><init>(Lcom/glympse/android/lib/GDirectionsPrivate;)V

    invoke-interface {p0, v0}, Lcom/glympse/android/lib/GJobQueue;->addJob(Lcom/glympse/android/lib/GJob;)V

    goto :goto_0

    .line 32
    :pswitch_2
    new-instance v0, Lcom/glympse/android/lib/aq;

    invoke-direct {v0, p1, p3}, Lcom/glympse/android/lib/aq;-><init>(Lcom/glympse/android/core/GPrimitive;Lcom/glympse/android/lib/GDirectionsPrivate;)V

    invoke-interface {p0, v0}, Lcom/glympse/android/lib/GJobQueue;->addJob(Lcom/glympse/android/lib/GJob;)V

    goto :goto_0

    .line 37
    :pswitch_3
    const-string v0, "base_url"

    invoke-static {v0}, Lcom/glympse/android/core/CoreFactory;->createString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 38
    const-string v1, "access_token"

    invoke-static {v1}, Lcom/glympse/android/core/CoreFactory;->createString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 40
    new-instance v2, Lcom/glympse/android/lib/ao;

    invoke-direct {v2, v0, p1, v1, p3}, Lcom/glympse/android/lib/ao;-><init>(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;Ljava/lang/String;Lcom/glympse/android/lib/GDirectionsPrivate;)V

    invoke-interface {p0, v2}, Lcom/glympse/android/lib/GJobQueue;->addJob(Lcom/glympse/android/lib/GJob;)V

    goto :goto_0

    .line 23
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
