.class Lcom/glympse/android/lib/dx;
.super Ljava/lang/Object;
.source "NotificationCenter.java"

# interfaces
.implements Lcom/glympse/android/lib/GNotificationCenter;


# instance fields
.field private _glympse:Lcom/glympse/android/lib/GGlympsePrivate;

.field private he:Lcom/glympse/android/lib/CommonSink;

.field private nB:Lcom/glympse/android/hal/GSharedPreferences;

.field private rg:Ljava/lang/String;

.field private rh:Z


# direct methods
.method public constructor <init>(Lcom/glympse/android/hal/GContextHolder;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-interface {p1}, Lcom/glympse/android/hal/GContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/glympse/android/hal/HalFactory;->openSharedPreferences(Landroid/content/Context;Ljava/lang/String;)Lcom/glympse/android/hal/GSharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/dx;->nB:Lcom/glympse/android/hal/GSharedPreferences;

    .line 45
    new-instance v0, Lcom/glympse/android/lib/CommonSink;

    const-string v1, "Notifications"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/CommonSink;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/glympse/android/lib/dx;->he:Lcom/glympse/android/lib/CommonSink;

    .line 47
    const-string v0, "sync_flags_v2"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/dx;->rg:Ljava/lang/String;

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/glympse/android/lib/dx;->rh:Z

    .line 50
    return-void
.end method

.method private a(Lcom/glympse/android/core/GPrimitive;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 182
    const-string v1, "code"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 183
    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 199
    :goto_0
    return-object v0

    .line 188
    :cond_0
    iget-object v2, p0, Lcom/glympse/android/lib/dx;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v2, :cond_1

    .line 191
    new-instance v2, Lcom/glympse/android/lib/ct;

    const/4 v3, 0x6

    invoke-direct {v2, v3, v0, v0, v0}, Lcom/glympse/android/lib/ct;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    invoke-interface {v2, p2}, Lcom/glympse/android/lib/GInvitePrivate;->setText(Ljava/lang/String;)V

    .line 193
    invoke-interface {v2, p3}, Lcom/glympse/android/lib/GInvitePrivate;->setAddress(Ljava/lang/String;)V

    .line 196
    iget-object v0, p0, Lcom/glympse/android/lib/dx;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/4 v3, 0x1

    invoke-interface {v0, v1, v3, v2}, Lcom/glympse/android/lib/GGlympsePrivate;->decodeInvite(Ljava/lang/String;ILcom/glympse/android/api/GInvite;)Lcom/glympse/android/api/GEventSink;

    :cond_1
    move-object v0, v1

    .line 199
    goto :goto_0
.end method

.method private cy()V
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/glympse/android/lib/dx;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/dx;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->okToPost()Z

    move-result v0

    if-nez v0, :cond_1

    .line 220
    :cond_0
    :goto_0
    return-void

    .line 212
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/dx;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->isActive()Z

    move-result v0

    if-nez v0, :cond_2

    .line 215
    iget-object v0, p0, Lcom/glympse/android/lib/dx;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getTicketProtocol()Lcom/glympse/android/lib/GTicketProtocol;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketProtocol;->refreshInvites()V

    .line 219
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/dx;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getServerPost()Lcom/glympse/android/lib/GServerPost;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/lib/GServerPost;->doPost()V

    goto :goto_0
.end method

.method private cz()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 415
    iget-object v0, p0, Lcom/glympse/android/lib/dx;->nB:Lcom/glympse/android/hal/GSharedPreferences;

    iget-object v1, p0, Lcom/glympse/android/lib/dx;->rg:Ljava/lang/String;

    invoke-interface {v0, v1, v4, v5}, Lcom/glympse/android/hal/GSharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    long-to-int v0, v0

    .line 416
    iget-object v1, p0, Lcom/glympse/android/lib/dx;->nB:Lcom/glympse/android/hal/GSharedPreferences;

    iget-object v2, p0, Lcom/glympse/android/lib/dx;->rg:Ljava/lang/String;

    invoke-interface {v1, v2, v4, v5}, Lcom/glympse/android/hal/GSharedPreferences;->putLong(Ljava/lang/String;J)V

    .line 417
    return v0
.end method

.method private l(Lcom/glympse/android/core/GPrimitive;)Lcom/glympse/android/lib/GUserMessage;
    .locals 10

    .prologue
    const/4 v8, 0x0

    .line 225
    const-string v0, "id"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 226
    const-string v0, "data"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 229
    invoke-static {v9}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v8

    .line 267
    :goto_0
    return-object v0

    .line 233
    :cond_1
    const-string v1, "t"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 234
    const-string v1, "pid"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 235
    const-string v1, "n"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 236
    const-string v1, "v"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v7

    .line 237
    const-wide/16 v0, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    if-eqz v6, :cond_2

    if-nez v7, :cond_3

    :cond_2
    move-object v0, v8

    .line 239
    goto :goto_0

    .line 241
    :cond_3
    new-instance v1, Lcom/glympse/android/lib/aj;

    invoke-direct/range {v1 .. v7}, Lcom/glympse/android/lib/aj;-><init>(JJLjava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 245
    iget-object v0, p0, Lcom/glympse/android/lib/dx;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_5

    .line 248
    iget-object v0, p0, Lcom/glympse/android/lib/dx;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getUserManager()Lcom/glympse/android/api/GUserManager;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GUserManagerPrivate;

    .line 249
    const/4 v2, 0x1

    invoke-interface {v0, v9, v2}, Lcom/glympse/android/lib/GUserManagerPrivate;->extractFromCache(Ljava/lang/String;Z)Lcom/glympse/android/lib/GUserPrivate;

    move-result-object v2

    .line 250
    if-nez v2, :cond_4

    move-object v0, v8

    .line 252
    goto :goto_0

    .line 256
    :cond_4
    new-instance v0, Lcom/glympse/android/lib/gg;

    invoke-direct {v0, v2, v1}, Lcom/glympse/android/lib/gg;-><init>(Lcom/glympse/android/api/GUser;Lcom/glympse/android/api/GDataRow;)V

    .line 257
    iget-object v1, p0, Lcom/glympse/android/lib/dx;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget-object v2, p0, Lcom/glympse/android/lib/dx;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const v3, 0x10002

    const/16 v4, 0x1000

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/glympse/android/lib/GGlympsePrivate;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    goto :goto_0

    .line 262
    :cond_5
    new-instance v2, Lcom/glympse/android/lib/gb;

    invoke-direct {v2}, Lcom/glympse/android/lib/gb;-><init>()V

    .line 263
    invoke-interface {v2, v9}, Lcom/glympse/android/lib/GUserPrivate;->setId(Ljava/lang/String;)V

    .line 264
    new-instance v0, Lcom/glympse/android/lib/gg;

    invoke-direct {v0, v2, v1}, Lcom/glympse/android/lib/gg;-><init>(Lcom/glympse/android/api/GUser;Lcom/glympse/android/api/GDataRow;)V

    goto :goto_0
.end method

.method private m(Lcom/glympse/android/core/GPrimitive;)Ljava/lang/Long;
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 280
    const-string v1, "flags"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 281
    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v0, v2

    .line 313
    :goto_0
    return-object v0

    .line 287
    :cond_0
    const-string v3, ","

    invoke-static {v1, v3}, Lcom/glympse/android/hal/Helpers;->split(Ljava/lang/String;Ljava/lang/String;)Lcom/glympse/android/hal/GVector;

    move-result-object v4

    .line 288
    invoke-virtual {v4}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v5

    move v3, v0

    move v1, v0

    .line 289
    :goto_1
    if-ge v3, v5, :cond_3

    .line 291
    invoke-virtual {v4, v3}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 292
    const-string v6, "user"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 294
    or-int/lit8 v0, v1, 0x1

    .line 289
    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    .line 296
    :cond_1
    const-string v6, "history"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 298
    or-int/lit8 v0, v1, 0x2

    goto :goto_2

    .line 300
    :cond_2
    const-string v6, "linked_accounts"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 302
    or-int/lit8 v0, v1, 0x4

    goto :goto_2

    .line 305
    :cond_3
    if-nez v1, :cond_4

    move-object v0, v2

    .line 307
    goto :goto_0

    .line 311
    :cond_4
    invoke-virtual {p0, v1}, Lcom/glympse/android/lib/dx;->sync(I)V

    .line 313
    int-to-long v0, v1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_2
.end method

.method private n(Lcom/glympse/android/core/GPrimitive;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 326
    const-string v0, "data"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 327
    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 329
    const/4 v0, 0x0

    .line 344
    :cond_0
    :goto_0
    return-object v0

    .line 332
    :cond_1
    iget-object v1, p0, Lcom/glympse/android/lib/dx;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/glympse/android/lib/dx;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->isStarted()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 335
    iget-object v1, p0, Lcom/glympse/android/lib/dx;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->isPushEchoingEbabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 338
    iget-object v1, p0, Lcom/glympse/android/lib/dx;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->getServerPost()Lcom/glympse/android/lib/GServerPost;

    move-result-object v1

    new-instance v2, Lcom/glympse/android/lib/gc;

    iget-object v3, p0, Lcom/glympse/android/lib/dx;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct {v2, v3, v0}, Lcom/glympse/android/lib/gc;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/glympse/android/lib/GServerPost;->invokeEndpoint(Lcom/glympse/android/lib/GApiEndpoint;Z)V

    .line 340
    iget-object v1, p0, Lcom/glympse/android/lib/dx;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->getTicketProtocol()Lcom/glympse/android/lib/GTicketProtocol;

    move-result-object v1

    invoke-interface {v1}, Lcom/glympse/android/lib/GTicketProtocol;->refreshInvites()V

    goto :goto_0
.end method

.method private o(Lcom/glympse/android/core/GPrimitive;)Lcom/glympse/android/core/GPrimitive;
    .locals 2

    .prologue
    .line 357
    const-string v0, "favorites"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 358
    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 360
    iget-object v0, p0, Lcom/glympse/android/lib/dx;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/dx;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->isStarted()Z

    move-result v0

    if-nez v0, :cond_2

    .line 362
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/dx;->w(I)V

    .line 371
    :cond_1
    :goto_0
    return-object p1

    .line 366
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/dx;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getFavoritesManager()Lcom/glympse/android/api/GFavoritesManager;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GFavoritesManagerPrivate;

    .line 367
    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GFavoritesManagerPrivate;->checkFavoritesVersion(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private p(Lcom/glympse/android/core/GPrimitive;)V
    .locals 3

    .prologue
    .line 383
    iget-object v0, p0, Lcom/glympse/android/lib/dx;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getServerPost()Lcom/glympse/android/lib/GServerPost;

    move-result-object v0

    new-instance v1, Lcom/glympse/android/lib/y;

    iget-object v2, p0, Lcom/glympse/android/lib/dx;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct {v1, v2}, Lcom/glympse/android/lib/y;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;)V

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/glympse/android/lib/GServerPost;->invokeEndpoint(Lcom/glympse/android/lib/GApiEndpoint;Z)V

    .line 384
    return-void
.end method

.method private w(I)V
    .locals 6

    .prologue
    .line 422
    iget-object v0, p0, Lcom/glympse/android/lib/dx;->nB:Lcom/glympse/android/hal/GSharedPreferences;

    iget-object v1, p0, Lcom/glympse/android/lib/dx;->rg:Ljava/lang/String;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/glympse/android/hal/GSharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    long-to-int v0, v0

    .line 423
    or-int/2addr v0, p1

    .line 424
    iget-object v1, p0, Lcom/glympse/android/lib/dx;->nB:Lcom/glympse/android/hal/GSharedPreferences;

    iget-object v2, p0, Lcom/glympse/android/lib/dx;->rg:Ljava/lang/String;

    int-to-long v4, v0

    invoke-interface {v1, v2, v4, v5}, Lcom/glympse/android/hal/GSharedPreferences;->putLong(Ljava/lang/String;J)V

    .line 425
    return-void
.end method

.method private x(I)V
    .locals 2

    .prologue
    .line 430
    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_0

    .line 432
    iget-object v0, p0, Lcom/glympse/android/lib/dx;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getUserManager()Lcom/glympse/android/api/GUserManager;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GUserManagerPrivate;

    .line 433
    invoke-interface {v0}, Lcom/glympse/android/lib/GUserManagerPrivate;->getSelf()Lcom/glympse/android/api/GUser;

    move-result-object v1

    check-cast v1, Lcom/glympse/android/lib/GUserPrivate;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GUserManagerPrivate;->refreshUser(Lcom/glympse/android/lib/GUserPrivate;)V

    .line 437
    :cond_0
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_1

    .line 439
    iget-object v0, p0, Lcom/glympse/android/lib/dx;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getTicketProtocol()Lcom/glympse/android/lib/GTicketProtocol;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketProtocol;->refreshInvites()V

    .line 443
    :cond_1
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_2

    .line 445
    iget-object v0, p0, Lcom/glympse/android/lib/dx;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getLinkedAccountsManager()Lcom/glympse/android/api/GLinkedAccountsManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/api/GLinkedAccountsManager;->refresh()Z

    .line 447
    :cond_2
    return-void
.end method


# virtual methods
.method public addListener(Lcom/glympse/android/api/GEventListener;)Z
    .locals 1

    .prologue
    .line 455
    iget-object v0, p0, Lcom/glympse/android/lib/dx;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1}, Lcom/glympse/android/lib/CommonSink;->addListener(Lcom/glympse/android/api/GEventListener;)Z

    move-result v0

    return v0
.end method

.method public associateContext(JLjava/lang/Object;)V
    .locals 1

    .prologue
    .line 483
    iget-object v0, p0, Lcom/glympse/android/lib/dx;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1, p2, p3}, Lcom/glympse/android/lib/CommonSink;->associateContext(JLjava/lang/Object;)V

    .line 484
    return-void
.end method

.method public clearContext(J)V
    .locals 1

    .prologue
    .line 493
    iget-object v0, p0, Lcom/glympse/android/lib/dx;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1, p2}, Lcom/glympse/android/lib/CommonSink;->clearContext(J)V

    .line 494
    return-void
.end method

.method public deriveContext(Lcom/glympse/android/api/GEventSink;)V
    .locals 1

    .prologue
    .line 503
    iget-object v0, p0, Lcom/glympse/android/lib/dx;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1}, Lcom/glympse/android/lib/CommonSink;->deriveContext(Lcom/glympse/android/api/GEventSink;)V

    .line 504
    return-void
.end method

.method public eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 473
    iget-object v0, p0, Lcom/glympse/android/lib/dx;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/glympse/android/lib/CommonSink;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 474
    return-void
.end method

.method public getContext(J)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 488
    iget-object v0, p0, Lcom/glympse/android/lib/dx;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1, p2}, Lcom/glympse/android/lib/CommonSink;->getContext(J)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getContextKeys()Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Enumeration",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 498
    iget-object v0, p0, Lcom/glympse/android/lib/dx;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0}, Lcom/glympse/android/lib/CommonSink;->getContextKeys()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public getListeners()Lcom/glympse/android/core/GArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/glympse/android/core/GArray",
            "<",
            "Lcom/glympse/android/api/GEventListener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 465
    iget-object v0, p0, Lcom/glympse/android/lib/dx;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0}, Lcom/glympse/android/lib/CommonSink;->getListeners()Lcom/glympse/android/core/GArray;

    move-result-object v0

    return-object v0
.end method

.method public handle(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/glympse/android/lib/dx;->handle(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    return-void
.end method

.method public handle(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 91
    iget-object v2, p0, Lcom/glympse/android/lib/dx;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/glympse/android/lib/dx;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v2}, Lcom/glympse/android/lib/GGlympsePrivate;->isStarted()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/glympse/android/lib/dx;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v2}, Lcom/glympse/android/lib/GGlympsePrivate;->getConfigPrivate()Lcom/glympse/android/lib/GConfigPrivate;

    move-result-object v2

    invoke-interface {v2}, Lcom/glympse/android/lib/GConfigPrivate;->shouldForceRefresh()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 93
    iget-object v2, p0, Lcom/glympse/android/lib/dx;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v2}, Lcom/glympse/android/lib/GGlympsePrivate;->getJobQueue()Lcom/glympse/android/lib/GJobQueue;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/glympse/android/lib/GJobQueue;->retryAll(Z)V

    .line 97
    :cond_0
    invoke-static {p1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 168
    :cond_1
    :goto_0
    return-void

    .line 102
    :cond_2
    invoke-static {p1}, Lcom/glympse/android/lib/json/JsonSerializer;->toPrimitive(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v3

    .line 103
    if-eqz v3, :cond_1

    .line 109
    const-string v2, "type"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 110
    invoke-static {v4}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 116
    iput-boolean v0, p0, Lcom/glympse/android/lib/dx;->rh:Z

    .line 120
    const/4 v2, 0x0

    .line 123
    const-string v5, "n_invite"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 125
    invoke-direct {p0, v3, p1, p2}, Lcom/glympse/android/lib/dx;->a(Lcom/glympse/android/core/GPrimitive;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 126
    if-eqz v2, :cond_4

    :goto_1
    move v1, v0

    move-object v0, v2

    .line 164
    :cond_3
    :goto_2
    if-eqz v1, :cond_1

    .line 166
    iget-object v2, p0, Lcom/glympse/android/lib/dx;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const v3, 0x10006

    invoke-virtual {p0, v2, v3, v1, v0}, Lcom/glympse/android/lib/dx;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    goto :goto_0

    :cond_4
    move v0, v1

    .line 126
    goto :goto_1

    .line 128
    :cond_5
    const-string v0, "n_group"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    move-object v0, v2

    goto :goto_2

    .line 132
    :cond_6
    const-string v0, "n_viewer"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 134
    invoke-direct {p0}, Lcom/glympse/android/lib/dx;->cy()V

    .line 135
    const/4 v1, 0x4

    move-object v0, v2

    goto :goto_2

    .line 137
    :cond_7
    const-string v0, "n_data_value"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 139
    invoke-direct {p0, v3}, Lcom/glympse/android/lib/dx;->l(Lcom/glympse/android/core/GPrimitive;)Lcom/glympse/android/lib/GUserMessage;

    move-result-object v0

    .line 140
    if-eqz v0, :cond_3

    const/16 v1, 0x8

    goto :goto_2

    .line 142
    :cond_8
    const-string v0, "n_refresh"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 144
    invoke-direct {p0, v3}, Lcom/glympse/android/lib/dx;->m(Lcom/glympse/android/core/GPrimitive;)Ljava/lang/Long;

    move-result-object v0

    .line 145
    if-eqz v0, :cond_3

    const/16 v1, 0x10

    goto :goto_2

    .line 147
    :cond_9
    const-string v0, "n_echo"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 149
    invoke-direct {p0, v3}, Lcom/glympse/android/lib/dx;->n(Lcom/glympse/android/core/GPrimitive;)Ljava/lang/String;

    move-result-object v0

    .line 150
    if-eqz v0, :cond_3

    const/16 v1, 0x20

    goto :goto_2

    .line 152
    :cond_a
    const-string v0, "n_sync"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 154
    invoke-direct {p0, v3}, Lcom/glympse/android/lib/dx;->o(Lcom/glympse/android/core/GPrimitive;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 155
    if-eqz v0, :cond_3

    const/16 v1, 0x40

    goto :goto_2

    .line 157
    :cond_b
    const-string v0, "n_config"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 159
    invoke-direct {p0, v3}, Lcom/glympse/android/lib/dx;->p(Lcom/glympse/android/core/GPrimitive;)V

    .line 160
    const/16 v1, 0x80

    move-object v0, v2

    goto :goto_2

    :cond_c
    move-object v0, v2

    goto :goto_2
.end method

.method public hasContext(J)Z
    .locals 1

    .prologue
    .line 478
    iget-object v0, p0, Lcom/glympse/android/lib/dx;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1, p2}, Lcom/glympse/android/lib/CommonSink;->hasContext(J)Z

    move-result v0

    return v0
.end method

.method public isPushEnabled()Z
    .locals 1

    .prologue
    .line 172
    iget-boolean v0, p0, Lcom/glympse/android/lib/dx;->rh:Z

    return v0
.end method

.method public removeListener(Lcom/glympse/android/api/GEventListener;)Z
    .locals 1

    .prologue
    .line 460
    iget-object v0, p0, Lcom/glympse/android/lib/dx;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1}, Lcom/glympse/android/lib/CommonSink;->removeListener(Lcom/glympse/android/api/GEventListener;)Z

    move-result v0

    return v0
.end method

.method public setActive(Z)V
    .locals 1

    .prologue
    .line 71
    if-eqz p1, :cond_0

    .line 75
    invoke-direct {p0}, Lcom/glympse/android/lib/dx;->cz()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/dx;->x(I)V

    .line 77
    :cond_0
    return-void
.end method

.method public skipSync(I)V
    .locals 6

    .prologue
    .line 408
    iget-object v0, p0, Lcom/glympse/android/lib/dx;->nB:Lcom/glympse/android/hal/GSharedPreferences;

    iget-object v1, p0, Lcom/glympse/android/lib/dx;->rg:Ljava/lang/String;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/glympse/android/hal/GSharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    long-to-int v0, v0

    .line 409
    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    .line 410
    iget-object v1, p0, Lcom/glympse/android/lib/dx;->nB:Lcom/glympse/android/hal/GSharedPreferences;

    iget-object v2, p0, Lcom/glympse/android/lib/dx;->rg:Ljava/lang/String;

    int-to-long v4, v0

    invoke-interface {v1, v2, v4, v5}, Lcom/glympse/android/hal/GSharedPreferences;->putLong(Ljava/lang/String;J)V

    .line 411
    return-void
.end method

.method public start(Lcom/glympse/android/lib/GGlympsePrivate;)V
    .locals 1

    .prologue
    .line 58
    iput-object p1, p0, Lcom/glympse/android/lib/dx;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 61
    invoke-direct {p0}, Lcom/glympse/android/lib/dx;->cz()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/dx;->x(I)V

    .line 62
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/dx;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 67
    return-void
.end method

.method public sync(I)V
    .locals 2

    .prologue
    .line 395
    iget-object v0, p0, Lcom/glympse/android/lib/dx;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/dx;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/dx;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 396
    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getConfigPrivate()Lcom/glympse/android/lib/GConfigPrivate;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/lib/GConfigPrivate;->shouldForceRefresh()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/glympse/android/lib/dx;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->isActive()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/glympse/android/lib/dx;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getHistoryManager()Lcom/glympse/android/api/GHistoryManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/glympse/android/api/GHistoryManager;->anyActive(Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 398
    :cond_0
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/dx;->w(I)V

    .line 404
    :goto_0
    return-void

    .line 402
    :cond_1
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/dx;->x(I)V

    goto :goto_0
.end method
