.class public Lcom/glympse/android/lib/LocationProfileBuilder;
.super Ljava/lang/Object;
.source "LocationProfileBuilder.java"

# interfaces
.implements Lcom/glympse/android/core/GLocationProfileBuilder;


# instance fields
.field private qJ:Lcom/glympse/android/lib/GLocationProfilePrivate;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-static {p1}, Lcom/glympse/android/hal/HalFactory;->createLocationProfile(I)Lcom/glympse/android/lib/GLocationProfilePrivate;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/LocationProfileBuilder;->qJ:Lcom/glympse/android/lib/GLocationProfilePrivate;

    .line 23
    return-void
.end method


# virtual methods
.method public getLocationProfile()Lcom/glympse/android/core/GLocationProfile;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/glympse/android/lib/LocationProfileBuilder;->qJ:Lcom/glympse/android/lib/GLocationProfilePrivate;

    return-object v0
.end method

.method public setAccuracy(D)V
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/glympse/android/lib/LocationProfileBuilder;->qJ:Lcom/glympse/android/lib/GLocationProfilePrivate;

    invoke-interface {v0, p1, p2}, Lcom/glympse/android/lib/GLocationProfilePrivate;->setAccuracy(D)V

    .line 43
    return-void
.end method

.method public setActivity(I)V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/glympse/android/lib/LocationProfileBuilder;->qJ:Lcom/glympse/android/lib/GLocationProfilePrivate;

    invoke-interface {v0, p1}, Lcom/glympse/android/lib/GLocationProfilePrivate;->setActivity(I)V

    .line 58
    return-void
.end method

.method public setAutoPauseEnabled(Z)V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/glympse/android/lib/LocationProfileBuilder;->qJ:Lcom/glympse/android/lib/GLocationProfilePrivate;

    invoke-interface {v0, p1}, Lcom/glympse/android/lib/GLocationProfilePrivate;->setAutoPauseEnabled(Z)V

    .line 63
    return-void
.end method

.method public setDistance(D)V
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/glympse/android/lib/LocationProfileBuilder;->qJ:Lcom/glympse/android/lib/GLocationProfilePrivate;

    invoke-interface {v0, p1, p2}, Lcom/glympse/android/lib/GLocationProfilePrivate;->setDistance(D)V

    .line 38
    return-void
.end method

.method public setFrequency(I)V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/glympse/android/lib/LocationProfileBuilder;->qJ:Lcom/glympse/android/lib/GLocationProfilePrivate;

    invoke-interface {v0, p1}, Lcom/glympse/android/lib/GLocationProfilePrivate;->setFrequency(I)V

    .line 48
    return-void
.end method

.method public setMode(I)V
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/glympse/android/lib/LocationProfileBuilder;->qJ:Lcom/glympse/android/lib/GLocationProfilePrivate;

    invoke-interface {v0, p1}, Lcom/glympse/android/lib/GLocationProfilePrivate;->setMode(I)V

    .line 28
    return-void
.end method

.method public setPriority(I)V
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/glympse/android/lib/LocationProfileBuilder;->qJ:Lcom/glympse/android/lib/GLocationProfilePrivate;

    invoke-interface {v0, p1}, Lcom/glympse/android/lib/GLocationProfilePrivate;->setPriority(I)V

    .line 53
    return-void
.end method

.method public setSignificantLocationChangeMonitoringEnabled(Z)V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/glympse/android/lib/LocationProfileBuilder;->qJ:Lcom/glympse/android/lib/GLocationProfilePrivate;

    invoke-interface {v0, p1}, Lcom/glympse/android/lib/GLocationProfilePrivate;->setSignificantLocationChangeMonitoringEnabled(Z)V

    .line 68
    return-void
.end method

.method public setSource(I)V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/glympse/android/lib/LocationProfileBuilder;->qJ:Lcom/glympse/android/lib/GLocationProfilePrivate;

    invoke-interface {v0, p1}, Lcom/glympse/android/lib/GLocationProfilePrivate;->setSource(I)V

    .line 33
    return-void
.end method
