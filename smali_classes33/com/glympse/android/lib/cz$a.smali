.class Lcom/glympse/android/lib/cz$a;
.super Lcom/glympse/android/lib/json/JsonHandlerBasic;
.source "InviteTicketParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/lib/cz;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

.field private gE:Ljava/lang/String;

.field private nd:I

.field private oH:Lcom/glympse/android/lib/GInvitePrivate;

.field private pp:Lcom/glympse/android/lib/cz;

.field private pq:Z


# direct methods
.method public constructor <init>(Lcom/glympse/android/lib/json/GJsonHandlerStack;ILcom/glympse/android/lib/cz;)V
    .locals 0

    .prologue
    .line 494
    invoke-direct {p0}, Lcom/glympse/android/lib/json/JsonHandlerBasic;-><init>()V

    .line 495
    iput-object p1, p0, Lcom/glympse/android/lib/cz$a;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    .line 496
    iput p2, p0, Lcom/glympse/android/lib/cz$a;->nd:I

    .line 497
    iput-object p3, p0, Lcom/glympse/android/lib/cz$a;->pp:Lcom/glympse/android/lib/cz;

    .line 498
    return-void
.end method


# virtual methods
.method public endArray(I)Z
    .locals 1

    .prologue
    .line 566
    iget v0, p0, Lcom/glympse/android/lib/cz$a;->nd:I

    if-ne v0, p1, :cond_0

    .line 569
    iget-object v0, p0, Lcom/glympse/android/lib/cz$a;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    invoke-interface {v0}, Lcom/glympse/android/lib/json/GJsonHandlerStack;->popHandler()V

    .line 571
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public endObject(I)Z
    .locals 2

    .prologue
    .line 512
    const/4 v0, 0x4

    if-ne v0, p1, :cond_0

    .line 514
    iget-boolean v0, p0, Lcom/glympse/android/lib/cz$a;->pq:Z

    if-eqz v0, :cond_1

    .line 517
    iget-object v0, p0, Lcom/glympse/android/lib/cz$a;->pp:Lcom/glympse/android/lib/cz;

    iget-object v0, v0, Lcom/glympse/android/lib/cz;->pe:Lcom/glympse/android/lib/GTicketPrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/cz$a;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GTicketPrivate;->addInviteCore(Lcom/glympse/android/api/GInvite;)V

    .line 524
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/cz$a;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    .line 526
    :cond_0
    const/4 v0, 0x1

    return v0

    .line 522
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/cz$a;->pp:Lcom/glympse/android/lib/cz;

    iget-object v0, v0, Lcom/glympse/android/lib/cz;->pn:Lcom/glympse/android/hal/GVector;

    iget-object v1, p0, Lcom/glympse/android/lib/cz$a;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public primitive(ILcom/glympse/android/lib/json/GJsonPrimitive;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 531
    const/4 v0, 0x4

    if-ne v0, p1, :cond_0

    .line 533
    iget-object v0, p0, Lcom/glympse/android/lib/cz$a;->gE:Ljava/lang/String;

    const-string v1, "id"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 537
    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getLong()J

    move-result-wide v0

    .line 538
    iget-object v2, p0, Lcom/glympse/android/lib/cz$a;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-static {v0, v1}, Lcom/glympse/android/hal/Helpers;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Lcom/glympse/android/lib/GInvitePrivate;->setCode(Ljava/lang/String;)V

    .line 561
    :cond_0
    :goto_0
    return v3

    .line 540
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/cz$a;->gE:Ljava/lang/String;

    const-string v1, "type"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 542
    invoke-virtual {p2, v3}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getString(Z)Ljava/lang/String;

    move-result-object v0

    .line 543
    iget-object v1, p0, Lcom/glympse/android/lib/cz$a;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-static {v0}, Lcom/glympse/android/lib/ct;->p(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GInvitePrivate;->setType(I)V

    .line 546
    iput-boolean v3, p0, Lcom/glympse/android/lib/cz$a;->pq:Z

    goto :goto_0

    .line 548
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/cz$a;->gE:Ljava/lang/String;

    const-string v1, "subtype"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 550
    iget-object v0, p0, Lcom/glympse/android/lib/cz$a;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-virtual {p2, v2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GInvitePrivate;->setSubtype(Ljava/lang/String;)V

    goto :goto_0

    .line 552
    :cond_3
    iget-object v0, p0, Lcom/glympse/android/lib/cz$a;->gE:Ljava/lang/String;

    const-string v1, "name"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 554
    iget-object v0, p0, Lcom/glympse/android/lib/cz$a;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-virtual {p2, v2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GInvitePrivate;->setName(Ljava/lang/String;)V

    goto :goto_0

    .line 556
    :cond_4
    iget-object v0, p0, Lcom/glympse/android/lib/cz$a;->gE:Ljava/lang/String;

    const-string v1, "address"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 558
    iget-object v0, p0, Lcom/glympse/android/lib/cz$a;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-virtual {p2, v2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GInvitePrivate;->setAddress(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public startObject(I)Z
    .locals 1

    .prologue
    .line 502
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 504
    new-instance v0, Lcom/glympse/android/lib/ct;

    invoke-direct {v0}, Lcom/glympse/android/lib/ct;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/cz$a;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    .line 505
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/glympse/android/lib/cz$a;->pq:Z

    .line 507
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public startPair(ILjava/lang/String;)Z
    .locals 1

    .prologue
    .line 576
    iput-object p2, p0, Lcom/glympse/android/lib/cz$a;->gE:Ljava/lang/String;

    .line 577
    const/4 v0, 0x1

    return v0
.end method
