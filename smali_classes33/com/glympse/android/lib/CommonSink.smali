.class public Lcom/glympse/android/lib/CommonSink;
.super Ljava/lang/Object;
.source "CommonSink.java"

# interfaces
.implements Lcom/glympse/android/core/GCommon;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/lib/CommonSink$b;,
        Lcom/glympse/android/lib/CommonSink$a;
    }
.end annotation


# static fields
.field private static final ii:I = 0x1

.field private static final ij:I = 0x2


# instance fields
.field private ik:Ljava/lang/String;

.field private il:Z

.field private im:Lcom/glympse/android/hal/GVector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GVector",
            "<",
            "Lcom/glympse/android/api/GEventListener;",
            ">;"
        }
    .end annotation
.end field

.field private in:Lcom/glympse/android/hal/GVector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GVector",
            "<",
            "Lcom/glympse/android/lib/CommonSink$b;",
            ">;"
        }
    .end annotation
.end field

.field private io:Lcom/glympse/android/hal/GHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GHashtable",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/glympse/android/lib/CommonSink;->ik:Ljava/lang/String;

    .line 34
    new-instance v0, Lcom/glympse/android/hal/GVector;

    invoke-direct {v0}, Lcom/glympse/android/hal/GVector;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/CommonSink;->im:Lcom/glympse/android/hal/GVector;

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/glympse/android/lib/CommonSink;->il:Z

    .line 36
    new-instance v0, Lcom/glympse/android/hal/GVector;

    invoke-direct {v0}, Lcom/glympse/android/hal/GVector;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/CommonSink;->in:Lcom/glympse/android/hal/GVector;

    .line 37
    new-instance v0, Lcom/glympse/android/hal/GHashtable;

    invoke-direct {v0}, Lcom/glympse/android/hal/GHashtable;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/CommonSink;->io:Lcom/glympse/android/hal/GHashtable;

    .line 38
    return-void
.end method

.method private a(Lcom/glympse/android/api/GEventListener;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 43
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v3

    .line 44
    iget-object v0, p0, Lcom/glympse/android/lib/CommonSink;->im:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v4

    move v2, v1

    .line 45
    :goto_0
    if-ge v2, v4, :cond_1

    .line 47
    iget-object v0, p0, Lcom/glympse/android/lib/CommonSink;->im:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, v2}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GEventListener;

    .line 48
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v5

    if-ne v5, v3, :cond_0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    const/4 v0, 0x4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[CommonSink.addListenerCore] Trying to subscribe the same listener "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 51
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " on "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/glympse/android/lib/CommonSink;->ik:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 50
    invoke-static {v0, v2}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    move v0, v1

    .line 62
    :goto_1
    return v0

    .line 45
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 61
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/CommonSink;->im:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 62
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public static removeAllListeners(Lcom/glympse/android/api/GEventSink;)V
    .locals 4

    .prologue
    .line 285
    invoke-interface {p0}, Lcom/glympse/android/api/GEventSink;->getListeners()Lcom/glympse/android/core/GArray;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/core/GArray;->clone()Lcom/glympse/android/core/GArray;

    move-result-object v2

    .line 288
    invoke-interface {v2}, Lcom/glympse/android/core/GArray;->length()I

    move-result v3

    .line 289
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 291
    invoke-interface {v2, v1}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GEventListener;

    .line 292
    invoke-interface {p0, v0}, Lcom/glympse/android/api/GEventSink;->removeListener(Lcom/glympse/android/api/GEventListener;)Z

    .line 289
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 294
    :cond_0
    return-void
.end method


# virtual methods
.method public addListener(Lcom/glympse/android/api/GEventListener;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 67
    if-nez p1, :cond_0

    .line 69
    const/4 v0, 0x0

    .line 78
    :goto_0
    return v0

    .line 71
    :cond_0
    iget-boolean v1, p0, Lcom/glympse/android/lib/CommonSink;->il:Z

    if-eqz v1, :cond_1

    .line 73
    iget-object v1, p0, Lcom/glympse/android/lib/CommonSink;->in:Lcom/glympse/android/hal/GVector;

    new-instance v2, Lcom/glympse/android/lib/CommonSink$b;

    invoke-direct {v2, p1, v0}, Lcom/glympse/android/lib/CommonSink$b;-><init>(Lcom/glympse/android/api/GEventListener;I)V

    invoke-virtual {v1, v2}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    goto :goto_0

    .line 78
    :cond_1
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/CommonSink;->a(Lcom/glympse/android/api/GEventListener;)Z

    move-result v0

    goto :goto_0
.end method

.method public associateContext(JLjava/lang/Object;)V
    .locals 3

    .prologue
    .line 202
    iget-object v0, p0, Lcom/glympse/android/lib/CommonSink;->io:Lcom/glympse/android/hal/GHashtable;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    return-void
.end method

.method public clearContext(J)V
    .locals 3

    .prologue
    .line 212
    iget-object v0, p0, Lcom/glympse/android/lib/CommonSink;->io:Lcom/glympse/android/hal/GHashtable;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GHashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    return-void
.end method

.method public deriveContext(Lcom/glympse/android/api/GEventSink;)V
    .locals 6

    .prologue
    .line 222
    invoke-interface {p1}, Lcom/glympse/android/api/GEventSink;->getContextKeys()Ljava/util/Enumeration;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 224
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 225
    iget-object v2, p0, Lcom/glympse/android/lib/CommonSink;->io:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-interface {p1, v4, v5}, Lcom/glympse/android/api/GEventSink;->getContext(J)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 227
    :cond_0
    return-void
.end method

.method public eventsOccurred(Lcom/glympse/android/api/GEventSink;Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V
    .locals 7

    .prologue
    const/4 v4, 0x1

    .line 126
    iget-object v0, p0, Lcom/glympse/android/lib/CommonSink;->im:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 154
    :goto_0
    return-void

    .line 132
    :cond_0
    invoke-interface {p2}, Lcom/glympse/android/api/GGlympse;->getHandler()Lcom/glympse/android/core/GHandler;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/core/GHandler;->isMainThread()Z

    move-result v0

    if-nez v0, :cond_1

    .line 134
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[CommonSink.eventsOccurred] Listener fired event on background "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    int-to-long v2, p3

    invoke-static {v2, v3}, Lcom/glympse/android/hal/Helpers;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 137
    invoke-interface {p2}, Lcom/glympse/android/api/GGlympse;->getHandler()Lcom/glympse/android/core/GHandler;

    move-result-object v6

    new-instance v0, Lcom/glympse/android/lib/CommonSink$a;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/glympse/android/lib/CommonSink$a;-><init>(Lcom/glympse/android/api/GEventSink;Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    invoke-interface {v6, v0}, Lcom/glympse/android/core/GHandler;->post(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 142
    :cond_1
    iget-boolean v0, p0, Lcom/glympse/android/lib/CommonSink;->il:Z

    if-eqz v0, :cond_2

    .line 144
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[CommonSink.eventsOccurred] Reentrant event was detected on "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/lib/CommonSink;->ik:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " listener: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    int-to-long v2, p3

    .line 145
    invoke-static {v2, v3}, Lcom/glympse/android/hal/Helpers;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " events: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    int-to-long v2, p4

    invoke-static {v2, v3}, Lcom/glympse/android/hal/Helpers;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 144
    invoke-static {v4, v0}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 148
    invoke-interface {p2}, Lcom/glympse/android/api/GGlympse;->getHandler()Lcom/glympse/android/core/GHandler;

    move-result-object v6

    new-instance v0, Lcom/glympse/android/lib/CommonSink$a;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/glympse/android/lib/CommonSink$a;-><init>(Lcom/glympse/android/api/GEventSink;Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    invoke-interface {v6, v0}, Lcom/glympse/android/core/GHandler;->post(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 153
    :cond_2
    invoke-virtual {p0, p2, p3, p4, p5}, Lcom/glympse/android/lib/CommonSink;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 159
    iput-boolean v6, p0, Lcom/glympse/android/lib/CommonSink;->il:Z

    .line 161
    iget-object v0, p0, Lcom/glympse/android/lib/CommonSink;->im:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v3

    move v2, v1

    .line 162
    :goto_0
    if-ge v2, v3, :cond_0

    .line 164
    iget-object v0, p0, Lcom/glympse/android/lib/CommonSink;->im:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, v2}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GEventListener;

    .line 167
    :try_start_0
    invoke-interface {v0, p1, p2, p3, p4}, Lcom/glympse/android/api/GEventListener;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 169
    :catch_0
    move-exception v4

    .line 171
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[CommonSink.eventsOccurred] "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ".eventsOccurred trown an exception"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    goto :goto_1

    .line 175
    :cond_0
    iput-boolean v1, p0, Lcom/glympse/android/lib/CommonSink;->il:Z

    .line 178
    iget-object v0, p0, Lcom/glympse/android/lib/CommonSink;->in:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v2

    .line 179
    :goto_2
    if-ge v1, v2, :cond_3

    .line 181
    iget-object v0, p0, Lcom/glympse/android/lib/CommonSink;->in:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/CommonSink$b;

    .line 183
    invoke-virtual {v0}, Lcom/glympse/android/lib/CommonSink$b;->getAction()I

    move-result v3

    if-ne v6, v3, :cond_2

    .line 185
    invoke-virtual {v0}, Lcom/glympse/android/lib/CommonSink$b;->aJ()Lcom/glympse/android/api/GEventListener;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/CommonSink;->a(Lcom/glympse/android/api/GEventListener;)Z

    .line 179
    :cond_1
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 187
    :cond_2
    const/4 v3, 0x2

    invoke-virtual {v0}, Lcom/glympse/android/lib/CommonSink$b;->getAction()I

    move-result v4

    if-ne v3, v4, :cond_1

    .line 189
    iget-object v3, p0, Lcom/glympse/android/lib/CommonSink;->im:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/lib/CommonSink$b;->aJ()Lcom/glympse/android/api/GEventListener;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/glympse/android/hal/GVector;->removeElement(Ljava/lang/Object;)Z

    goto :goto_3

    .line 192
    :cond_3
    iget-object v0, p0, Lcom/glympse/android/lib/CommonSink;->in:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->removeAllElements()V

    .line 193
    return-void
.end method

.method public getContext(J)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 207
    iget-object v0, p0, Lcom/glympse/android/lib/CommonSink;->io:Lcom/glympse/android/hal/GHashtable;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getContextKeys()Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Enumeration",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 217
    iget-object v0, p0, Lcom/glympse/android/lib/CommonSink;->io:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GHashtable;->keys()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public getListeners()Lcom/glympse/android/core/GArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/glympse/android/core/GArray",
            "<",
            "Lcom/glympse/android/api/GEventListener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 120
    iget-object v0, p0, Lcom/glympse/android/lib/CommonSink;->im:Lcom/glympse/android/hal/GVector;

    return-object v0
.end method

.method public hasContext(J)Z
    .locals 3

    .prologue
    .line 197
    iget-object v0, p0, Lcom/glympse/android/lib/CommonSink;->io:Lcom/glympse/android/hal/GHashtable;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GHashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public removeAllListeners()Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 101
    iget-boolean v0, p0, Lcom/glympse/android/lib/CommonSink;->il:Z

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/glympse/android/lib/CommonSink;->im:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v2

    .line 104
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 106
    iget-object v0, p0, Lcom/glympse/android/lib/CommonSink;->im:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GEventListener;

    .line 107
    iget-object v3, p0, Lcom/glympse/android/lib/CommonSink;->in:Lcom/glympse/android/hal/GVector;

    new-instance v4, Lcom/glympse/android/lib/CommonSink$b;

    const/4 v5, 0x2

    invoke-direct {v4, v0, v5}, Lcom/glympse/android/lib/CommonSink$b;-><init>(Lcom/glympse/android/api/GEventListener;I)V

    invoke-virtual {v3, v4}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 104
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/CommonSink;->im:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->removeAllElements()V

    .line 114
    :cond_1
    return v6
.end method

.method public removeListener(Lcom/glympse/android/api/GEventListener;)Z
    .locals 3

    .prologue
    .line 84
    if-nez p1, :cond_0

    .line 86
    const/4 v0, 0x0

    .line 95
    :goto_0
    return v0

    .line 88
    :cond_0
    iget-boolean v0, p0, Lcom/glympse/android/lib/CommonSink;->il:Z

    if-eqz v0, :cond_1

    .line 90
    iget-object v0, p0, Lcom/glympse/android/lib/CommonSink;->in:Lcom/glympse/android/hal/GVector;

    new-instance v1, Lcom/glympse/android/lib/CommonSink$b;

    const/4 v2, 0x2

    invoke-direct {v1, p1, v2}, Lcom/glympse/android/lib/CommonSink$b;-><init>(Lcom/glympse/android/api/GEventListener;I)V

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 91
    const/4 v0, 0x1

    goto :goto_0

    .line 95
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/CommonSink;->im:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GVector;->removeElement(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method
