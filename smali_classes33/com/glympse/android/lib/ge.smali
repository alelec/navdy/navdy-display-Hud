.class Lcom/glympse/android/lib/ge;
.super Lcom/glympse/android/lib/e;
.source "UserInvites.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/lib/ge$a;
    }
.end annotation


# instance fields
.field private _glympse:Lcom/glympse/android/lib/GGlympsePrivate;

.field private ib:J

.field private lC:Z

.field private qm:Lcom/glympse/android/lib/ge$a;


# direct methods
.method public constructor <init>(Lcom/glympse/android/lib/GGlympsePrivate;)V
    .locals 4

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/glympse/android/lib/e;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/glympse/android/lib/ge;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 31
    iget-object v0, p0, Lcom/glympse/android/lib/ge;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->areSiblingTicketsAllowed()Z

    move-result v0

    iput-boolean v0, p0, Lcom/glympse/android/lib/ge;->lC:Z

    .line 33
    new-instance v0, Lcom/glympse/android/lib/ge$a;

    invoke-direct {v0}, Lcom/glympse/android/lib/ge$a;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/ge;->qm:Lcom/glympse/android/lib/ge$a;

    .line 34
    iget-object v0, p0, Lcom/glympse/android/lib/ge;->qm:Lcom/glympse/android/lib/ge$a;

    iput-object v0, p0, Lcom/glympse/android/lib/ge;->gB:Lcom/glympse/android/lib/f;

    .line 36
    iget-object v0, p0, Lcom/glympse/android/lib/ge;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getTicketProtocol()Lcom/glympse/android/lib/GTicketProtocol;

    move-result-object v0

    .line 37
    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketProtocol;->getLastInviteRefreshTime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/glympse/android/lib/ge;->ib:J

    .line 38
    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketProtocol;->startedRefreshingInvites()V

    .line 39
    return-void
.end method

.method public static a(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/ge$a;)V
    .locals 18

    .prologue
    .line 108
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/glympse/android/lib/ge$a;->uX:Lcom/glympse/android/lib/LocationProfile;

    if-eqz v2, :cond_0

    .line 110
    invoke-interface/range {p0 .. p0}, Lcom/glympse/android/lib/GGlympsePrivate;->getLocationManager()Lcom/glympse/android/api/GLocationManager;

    move-result-object v2

    .line 112
    const/4 v3, 0x1

    invoke-interface {v2, v3}, Lcom/glympse/android/api/GLocationManager;->enableProfiles(Z)V

    .line 114
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/glympse/android/lib/ge$a;->uX:Lcom/glympse/android/lib/LocationProfile;

    invoke-interface {v2, v3}, Lcom/glympse/android/api/GLocationManager;->updateProfile(Lcom/glympse/android/core/GLocationProfile;)Z

    .line 117
    :cond_0
    const-wide/16 v2, 0x0

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/glympse/android/lib/ge$a;->ib:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 120
    invoke-interface/range {p0 .. p0}, Lcom/glympse/android/lib/GGlympsePrivate;->getTicketProtocol()Lcom/glympse/android/lib/GTicketProtocol;

    move-result-object v2

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/glympse/android/lib/ge$a;->ib:J

    invoke-interface {v2, v4, v5}, Lcom/glympse/android/lib/GTicketProtocol;->setLastInviteRefreshTime(J)V

    .line 124
    :cond_1
    new-instance v11, Lcom/glympse/android/hal/GVector;

    invoke-direct {v11}, Lcom/glympse/android/hal/GVector;-><init>()V

    .line 128
    const-wide/16 v6, 0x0

    .line 130
    invoke-interface/range {p0 .. p0}, Lcom/glympse/android/lib/GGlympsePrivate;->getHistoryManager()Lcom/glympse/android/api/GHistoryManager;

    move-result-object v2

    check-cast v2, Lcom/glympse/android/lib/GHistoryManagerPrivate;

    .line 132
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/glympse/android/lib/ge$a;->mD:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v3}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v12

    .line 133
    const/4 v3, 0x0

    move v10, v3

    :goto_0
    if-ge v10, v12, :cond_8

    .line 135
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/glympse/android/lib/ge$a;->mD:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v3, v10}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/glympse/android/lib/GInvitePrivate;

    .line 138
    invoke-interface {v3}, Lcom/glympse/android/lib/GInvitePrivate;->getTicketId()Ljava/lang/String;

    move-result-object v5

    .line 140
    invoke-interface {v2, v5}, Lcom/glympse/android/lib/GHistoryManagerPrivate;->findTicketByTicketId(Ljava/lang/String;)Lcom/glympse/android/api/GTicket;

    move-result-object v4

    check-cast v4, Lcom/glympse/android/lib/GTicketPrivate;

    .line 141
    if-eqz v4, :cond_7

    .line 144
    invoke-interface {v3}, Lcom/glympse/android/lib/GInvitePrivate;->getCode()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/glympse/android/lib/GTicketPrivate;->findInviteByCode(Ljava/lang/String;)Lcom/glympse/android/api/GInvite;

    move-result-object v5

    check-cast v5, Lcom/glympse/android/lib/GInvitePrivate;

    .line 145
    if-eqz v5, :cond_5

    .line 148
    invoke-interface {v3}, Lcom/glympse/android/lib/GInvitePrivate;->getLastViewTime()J

    move-result-wide v8

    .line 149
    invoke-interface {v3}, Lcom/glympse/android/lib/GInvitePrivate;->getViewers()I

    move-result v13

    .line 150
    invoke-interface {v3}, Lcom/glympse/android/lib/GInvitePrivate;->getViewing()I

    move-result v14

    .line 151
    invoke-interface {v5}, Lcom/glympse/android/lib/GInvitePrivate;->getLastViewTime()J

    move-result-wide v16

    cmp-long v15, v16, v8

    if-nez v15, :cond_2

    .line 152
    invoke-interface {v5}, Lcom/glympse/android/lib/GInvitePrivate;->getViewers()I

    move-result v15

    if-ne v15, v13, :cond_2

    .line 153
    invoke-interface {v5}, Lcom/glympse/android/lib/GInvitePrivate;->getViewing()I

    move-result v15

    if-eq v15, v14, :cond_4

    .line 155
    :cond_2
    invoke-interface {v5, v8, v9}, Lcom/glympse/android/lib/GInvitePrivate;->setLastViewTime(J)V

    .line 156
    invoke-interface {v5, v13}, Lcom/glympse/android/lib/GInvitePrivate;->setViewers(I)V

    .line 157
    invoke-interface {v5, v14}, Lcom/glympse/android/lib/GInvitePrivate;->setViewing(I)V

    .line 158
    invoke-virtual {v11, v4}, Lcom/glympse/android/hal/GVector;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_3

    .line 160
    invoke-virtual {v11, v4}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 164
    :cond_3
    invoke-interface {v4}, Lcom/glympse/android/lib/GTicketPrivate;->isMine()Z

    move-result v13

    if-eqz v13, :cond_4

    cmp-long v13, v8, v6

    if-lez v13, :cond_4

    move-wide v6, v8

    .line 170
    :cond_4
    invoke-interface {v3}, Lcom/glympse/android/lib/GInvitePrivate;->getState()I

    move-result v3

    .line 171
    if-eqz v3, :cond_5

    invoke-interface {v5}, Lcom/glympse/android/lib/GInvitePrivate;->getState()I

    move-result v8

    if-eq v8, v3, :cond_5

    .line 174
    invoke-interface {v5, v3}, Lcom/glympse/android/lib/GInvitePrivate;->setState(I)V

    .line 177
    const/4 v5, 0x4

    if-ne v5, v3, :cond_6

    .line 179
    const/4 v3, 0x4

    const/16 v5, 0x2000

    move-object/from16 v0, p0

    invoke-interface {v4, v0, v3, v5, v4}, Lcom/glympse/android/lib/GTicketPrivate;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 133
    :cond_5
    :goto_1
    add-int/lit8 v3, v10, 0x1

    move v10, v3

    goto :goto_0

    .line 181
    :cond_6
    const/16 v5, 0x8

    if-ne v5, v3, :cond_5

    .line 183
    const/4 v3, 0x4

    const/high16 v5, 0x10000

    move-object/from16 v0, p0

    invoke-interface {v4, v0, v3, v5, v4}, Lcom/glympse/android/lib/GTicketPrivate;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    goto :goto_1

    .line 191
    :cond_7
    new-instance v3, Lcom/glympse/android/lib/fe;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/glympse/android/lib/fe;-><init>(Z)V

    .line 192
    invoke-interface {v3, v5}, Lcom/glympse/android/lib/GTicketPrivate;->setId(Ljava/lang/String;)V

    .line 195
    invoke-interface/range {p0 .. p0}, Lcom/glympse/android/lib/GGlympsePrivate;->getTicketProtocol()Lcom/glympse/android/lib/GTicketProtocol;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v4, v3, v5}, Lcom/glympse/android/lib/GTicketProtocol;->refreshTicket(Lcom/glympse/android/lib/GTicketPrivate;Z)V

    goto :goto_1

    .line 200
    :cond_8
    move-object/from16 v0, p1

    iget-boolean v3, v0, Lcom/glympse/android/lib/ge$a;->uW:Z

    if-eqz v3, :cond_9

    .line 204
    invoke-interface {v2}, Lcom/glympse/android/lib/GHistoryManagerPrivate;->syncRefresh()V

    .line 208
    :cond_9
    invoke-interface {v2, v6, v7}, Lcom/glympse/android/lib/GHistoryManagerPrivate;->setLastViewTime(J)V

    .line 212
    invoke-virtual {v11}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v5

    .line 213
    const/4 v3, 0x0

    move v4, v3

    :goto_2
    if-ge v4, v5, :cond_a

    .line 215
    invoke-virtual {v11, v4}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/glympse/android/lib/GTicketPrivate;

    .line 218
    invoke-interface {v3}, Lcom/glympse/android/lib/GTicketPrivate;->updateWatchingState()Z

    .line 221
    const/4 v6, 0x4

    const/16 v7, 0x4000

    move-object/from16 v0, p0

    invoke-interface {v3, v0, v6, v7, v3}, Lcom/glympse/android/lib/GTicketPrivate;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 222
    const/4 v6, 0x1

    const/high16 v7, 0x80000

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-interface {v0, v1, v6, v7, v3}, Lcom/glympse/android/lib/GGlympsePrivate;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 213
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_2

    .line 227
    :cond_a
    invoke-interface {v2}, Lcom/glympse/android/lib/GHistoryManagerPrivate;->getTickets()Lcom/glympse/android/core/GArray;

    move-result-object v4

    .line 228
    invoke-interface {v4}, Lcom/glympse/android/core/GArray;->length()I

    move-result v5

    .line 229
    const/4 v2, 0x0

    move v3, v2

    :goto_3
    if-ge v3, v5, :cond_b

    .line 231
    invoke-interface {v4, v3}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/glympse/android/lib/GTicketPrivate;

    .line 234
    invoke-interface {v2}, Lcom/glympse/android/lib/GTicketPrivate;->isActive()Z

    move-result v6

    if-nez v6, :cond_e

    .line 256
    :cond_b
    move-object/from16 v0, p1

    iget v2, v0, Lcom/glympse/android/lib/ge$a;->sS:I

    if-eqz v2, :cond_d

    .line 259
    move-object/from16 v0, p1

    iget v2, v0, Lcom/glympse/android/lib/ge$a;->uU:I

    if-nez v2, :cond_c

    .line 261
    move-object/from16 v0, p1

    iget v2, v0, Lcom/glympse/android/lib/ge$a;->sS:I

    move-object/from16 v0, p1

    iput v2, v0, Lcom/glympse/android/lib/ge$a;->uU:I

    .line 263
    :cond_c
    invoke-interface/range {p0 .. p0}, Lcom/glympse/android/lib/GGlympsePrivate;->getServerPost()Lcom/glympse/android/lib/GServerPost;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Lcom/glympse/android/lib/ge$a;->sS:I

    move-object/from16 v0, p1

    iget v4, v0, Lcom/glympse/android/lib/ge$a;->uU:I

    move-object/from16 v0, p1

    iget-boolean v5, v0, Lcom/glympse/android/lib/ge$a;->uV:Z

    invoke-interface {v2, v3, v4, v5}, Lcom/glympse/android/lib/GServerPost;->setServerPostRate(IIZ)V

    .line 266
    :cond_d
    return-void

    .line 240
    :cond_e
    invoke-virtual {v11, v2}, Lcom/glympse/android/hal/GVector;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 242
    invoke-virtual {v11, v2}, Lcom/glympse/android/hal/GVector;->removeElement(Ljava/lang/Object;)Z

    .line 229
    :cond_f
    :goto_4
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_3

    .line 247
    :cond_10
    invoke-interface {v2}, Lcom/glympse/android/lib/GTicketPrivate;->updateWatchingState()Z

    move-result v6

    if-eqz v6, :cond_f

    .line 250
    const/4 v6, 0x4

    const/16 v7, 0x4000

    move-object/from16 v0, p0

    invoke-interface {v2, v0, v6, v7, v2}, Lcom/glympse/android/lib/GTicketPrivate;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 251
    const/4 v6, 0x1

    const/high16 v7, 0x80000

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-interface {v0, v1, v6, v7, v2}, Lcom/glympse/android/lib/GGlympsePrivate;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    goto :goto_4
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 97
    new-instance v0, Lcom/glympse/android/lib/ge$a;

    invoke-direct {v0}, Lcom/glympse/android/lib/ge$a;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/ge;->qm:Lcom/glympse/android/lib/ge$a;

    .line 98
    iget-object v0, p0, Lcom/glympse/android/lib/ge;->qm:Lcom/glympse/android/lib/ge$a;

    iput-object v0, p0, Lcom/glympse/android/lib/ge;->gB:Lcom/glympse/android/lib/f;

    .line 99
    return-void
.end method

.method public process()Z
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/glympse/android/lib/ge;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getTicketProtocol()Lcom/glympse/android/lib/GTicketProtocol;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketProtocol;->doneRefreshingInvites()V

    .line 84
    iget-object v0, p0, Lcom/glympse/android/lib/ge;->qm:Lcom/glympse/android/lib/ge$a;

    iget-object v0, v0, Lcom/glympse/android/lib/ge$a;->gF:Ljava/lang/String;

    const-string v1, "ok"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/glympse/android/lib/ge;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/ge;->qm:Lcom/glympse/android/lib/ge$a;

    invoke-static {v0, v1}, Lcom/glympse/android/lib/ge;->a(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/ge$a;)V

    .line 92
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 90
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public shouldAdd(Lcom/glympse/android/lib/GApiEndpoint;)I
    .locals 1

    .prologue
    .line 47
    instance-of v0, p1, Lcom/glympse/android/lib/ge;

    if-eqz v0, :cond_0

    .line 49
    const/4 v0, 0x4

    .line 56
    :goto_0
    return v0

    .line 51
    :cond_0
    instance-of v0, p1, Lcom/glympse/android/lib/dn;

    if-eqz v0, :cond_1

    .line 53
    const/4 v0, 0x3

    goto :goto_0

    .line 56
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public url(Ljava/lang/StringBuilder;)Z
    .locals 4

    .prologue
    .line 61
    const-string v0, "users/self/invites?only_views=true&since="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    iget-wide v0, p0, Lcom/glympse/android/lib/ge;->ib:J

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 66
    const-wide/16 v0, 0x0

    iget-wide v2, p0, Lcom/glympse/android/lib/ge;->ib:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 68
    const-string v0, "&expired=true"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    :cond_0
    iget-boolean v0, p0, Lcom/glympse/android/lib/ge;->lC:Z

    if-eqz v0, :cond_1

    .line 73
    const-string v0, "&siblings=true"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    :cond_1
    const/4 v0, 0x1

    return v0
.end method
