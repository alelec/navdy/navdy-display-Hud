.class public Lcom/glympse/android/lib/cz$c;
.super Lcom/glympse/android/lib/f;
.source "InviteTicketParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/lib/cz;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation


# instance fields
.field protected jH:J

.field private pp:Lcom/glympse/android/lib/cz;

.field protected pv:J

.field protected pw:Ljava/lang/String;

.field private px:Lcom/glympse/android/lib/json/JsonSerializer$a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 270
    invoke-direct {p0}, Lcom/glympse/android/lib/f;-><init>()V

    .line 271
    return-void
.end method

.method public constructor <init>(Lcom/glympse/android/lib/json/GJsonHandlerStack;Lcom/glympse/android/lib/cz;)V
    .locals 0

    .prologue
    .line 274
    invoke-direct {p0}, Lcom/glympse/android/lib/f;-><init>()V

    .line 275
    iput-object p1, p0, Lcom/glympse/android/lib/cz$c;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    .line 276
    iput-object p2, p0, Lcom/glympse/android/lib/cz$c;->pp:Lcom/glympse/android/lib/cz;

    .line 277
    return-void
.end method


# virtual methods
.method public endArray(I)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 476
    if-ne v1, p1, :cond_0

    .line 478
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    invoke-interface {v0}, Lcom/glympse/android/lib/json/GJsonHandlerStack;->popHandler()V

    .line 480
    :cond_0
    return v1
.end method

.method public endObject(I)Z
    .locals 1

    .prologue
    .line 443
    const/4 v0, 0x2

    if-ne v0, p1, :cond_0

    .line 445
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->px:Lcom/glympse/android/lib/json/JsonSerializer$a;

    if-eqz v0, :cond_0

    .line 447
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->px:Lcom/glympse/android/lib/json/JsonSerializer$a;

    invoke-virtual {v0}, Lcom/glympse/android/lib/json/JsonSerializer$a;->dt()Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/cz$c;->k(Lcom/glympse/android/core/GPrimitive;)V

    .line 448
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/cz$c;->px:Lcom/glympse/android/lib/json/JsonSerializer$a;

    .line 451
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic endPair(I)Z
    .locals 1

    .prologue
    .line 261
    invoke-super {p0, p1}, Lcom/glympse/android/lib/f;->endPair(I)Z

    move-result v0

    return v0
.end method

.method public k(Lcom/glympse/android/core/GPrimitive;)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v8, 0x1

    .line 285
    if-nez p1, :cond_1

    .line 412
    :cond_0
    :goto_0
    return-void

    .line 291
    :cond_1
    new-instance v1, Lcom/glympse/android/lib/aj;

    iget-wide v2, p0, Lcom/glympse/android/lib/cz$c;->pv:J

    iget-wide v4, p0, Lcom/glympse/android/lib/cz$c;->jH:J

    iget-object v6, p0, Lcom/glympse/android/lib/cz$c;->pw:Ljava/lang/String;

    move-object v7, p1

    invoke-direct/range {v1 .. v7}, Lcom/glympse/android/lib/aj;-><init>(JJLjava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 292
    const/16 v0, 0x40

    invoke-interface {p1}, Lcom/glympse/android/core/GPrimitive;->type()I

    move-result v2

    if-ne v0, v2, :cond_2

    move v0, v8

    .line 293
    :goto_1
    if-eqz v0, :cond_3

    .line 295
    iget-object v2, p0, Lcom/glympse/android/lib/cz$c;->pp:Lcom/glympse/android/lib/cz;

    iget-object v2, v2, Lcom/glympse/android/lib/cz;->pm:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v2, v1}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 304
    :goto_2
    iget-wide v2, p0, Lcom/glympse/android/lib/cz$c;->jH:J

    cmp-long v1, v10, v2

    if-nez v1, :cond_0

    .line 310
    if-eqz v0, :cond_9

    .line 312
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->pw:Ljava/lang/String;

    const-string v1, "message"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 314
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->pp:Lcom/glympse/android/lib/cz;

    iput-boolean v8, v0, Lcom/glympse/android/lib/cz;->pg:Z

    goto :goto_0

    .line 292
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 299
    :cond_3
    iget-object v2, p0, Lcom/glympse/android/lib/cz$c;->pp:Lcom/glympse/android/lib/cz;

    iget-object v2, v2, Lcom/glympse/android/lib/cz;->jG:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v2, v1}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 300
    iget-object v2, p0, Lcom/glympse/android/lib/cz$c;->pp:Lcom/glympse/android/lib/cz;

    iget-object v2, v2, Lcom/glympse/android/lib/cz;->pe:Lcom/glympse/android/lib/GTicketPrivate;

    invoke-interface {v2, v1}, Lcom/glympse/android/lib/GTicketPrivate;->setPropertyData(Lcom/glympse/android/api/GDataRow;)V

    goto :goto_2

    .line 316
    :cond_4
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->pw:Ljava/lang/String;

    const-string v1, "destination"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 318
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->pp:Lcom/glympse/android/lib/cz;

    iput-boolean v8, v0, Lcom/glympse/android/lib/cz;->ph:Z

    goto :goto_0

    .line 320
    :cond_5
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->pw:Ljava/lang/String;

    const-string v1, "app"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 322
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->pp:Lcom/glympse/android/lib/cz;

    iput-boolean v8, v0, Lcom/glympse/android/lib/cz;->pi:Z

    goto :goto_0

    .line 324
    :cond_6
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->pw:Ljava/lang/String;

    const-string v1, "eta"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 326
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->pp:Lcom/glympse/android/lib/cz;

    iput-boolean v8, v0, Lcom/glympse/android/lib/cz;->pj:Z

    goto :goto_0

    .line 328
    :cond_7
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->pw:Ljava/lang/String;

    const-string v1, "route"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 330
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->pp:Lcom/glympse/android/lib/cz;

    iput-boolean v8, v0, Lcom/glympse/android/lib/cz;->pk:Z

    goto/16 :goto_0

    .line 332
    :cond_8
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->pw:Ljava/lang/String;

    const-string v1, "travel_mode"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 334
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->pp:Lcom/glympse/android/lib/cz;

    iput-boolean v8, v0, Lcom/glympse/android/lib/cz;->pl:Z

    goto/16 :goto_0

    .line 340
    :cond_9
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->pw:Ljava/lang/String;

    const-string v1, "end_time"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 342
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->pp:Lcom/glympse/android/lib/cz;

    iget-object v0, v0, Lcom/glympse/android/lib/cz;->pe:Lcom/glympse/android/lib/GTicketPrivate;

    invoke-interface {p1}, Lcom/glympse/android/core/GPrimitive;->getLong()J

    move-result-wide v2

    invoke-interface {v0, v2, v3, v8}, Lcom/glympse/android/lib/GTicketPrivate;->setExpireTime(JZ)V

    goto/16 :goto_0

    .line 344
    :cond_a
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->pw:Ljava/lang/String;

    const-string v1, "message"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 346
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->pp:Lcom/glympse/android/lib/cz;

    iget-object v0, v0, Lcom/glympse/android/lib/cz;->pe:Lcom/glympse/android/lib/GTicketPrivate;

    invoke-interface {p1}, Lcom/glympse/android/core/GPrimitive;->getString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GTicketPrivate;->setMessage(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 348
    :cond_b
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->pw:Ljava/lang/String;

    const-string v1, "destination"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 350
    const-string v0, "lat"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 351
    const-string v0, "lng"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 352
    const-string v0, "name"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 353
    new-instance v1, Lcom/glympse/android/lib/eg;

    invoke-direct/range {v1 .. v6}, Lcom/glympse/android/lib/eg;-><init>(DDLjava/lang/String;)V

    .line 354
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->pp:Lcom/glympse/android/lib/cz;

    iget-object v0, v0, Lcom/glympse/android/lib/cz;->pe:Lcom/glympse/android/lib/GTicketPrivate;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GTicketPrivate;->setDestination(Lcom/glympse/android/api/GPlace;)V

    goto/16 :goto_0

    .line 356
    :cond_c
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->pw:Ljava/lang/String;

    const-string v1, "app"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 358
    const-string v0, "id"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 359
    const-string v1, "name"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 360
    const-string v2, "icon"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 361
    iget-object v3, p0, Lcom/glympse/android/lib/cz$c;->pp:Lcom/glympse/android/lib/cz;

    iget-object v3, v3, Lcom/glympse/android/lib/cz;->pe:Lcom/glympse/android/lib/GTicketPrivate;

    new-instance v4, Lcom/glympse/android/lib/i;

    invoke-direct {v4, v0, v1, v2}, Lcom/glympse/android/lib/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v4}, Lcom/glympse/android/lib/GTicketPrivate;->setOwner(Lcom/glympse/android/api/GAppProfile;)V

    goto/16 :goto_0

    .line 363
    :cond_d
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->pw:Ljava/lang/String;

    const-string v1, "eta"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 365
    const-string v0, "eta"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 366
    const-string v0, "eta_ts"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 367
    iget-object v4, p0, Lcom/glympse/android/lib/cz$c;->pp:Lcom/glympse/android/lib/cz;

    iget-object v4, v4, Lcom/glympse/android/lib/cz;->pe:Lcom/glympse/android/lib/GTicketPrivate;

    cmp-long v5, v10, v0

    if-nez v5, :cond_e

    iget-wide v0, p0, Lcom/glympse/android/lib/cz$c;->pv:J

    :cond_e
    invoke-interface {v4, v0, v1, v2, v3}, Lcom/glympse/android/lib/GTicketPrivate;->setEta(JJ)V

    goto/16 :goto_0

    .line 369
    :cond_f
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->pw:Ljava/lang/String;

    const-string v1, "route"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 371
    const-string v0, "src"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    long-to-int v1, v0

    .line 372
    const-string v0, "distance"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    long-to-int v2, v2

    .line 373
    const-string v0, "points"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 374
    invoke-static {v0}, Lcom/glympse/android/lib/an;->k(Ljava/lang/String;)Lcom/glympse/android/api/GTrack;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GTrackPrivate;

    .line 375
    if-eqz v0, :cond_10

    .line 377
    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GTrackPrivate;->setSource(I)V

    .line 378
    invoke-interface {v0, v2}, Lcom/glympse/android/lib/GTrackPrivate;->setDistance(I)V

    .line 380
    :cond_10
    iget-object v1, p0, Lcom/glympse/android/lib/cz$c;->pp:Lcom/glympse/android/lib/cz;

    iget-object v1, v1, Lcom/glympse/android/lib/cz;->pe:Lcom/glympse/android/lib/GTicketPrivate;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GTicketPrivate;->setRoute(Lcom/glympse/android/api/GTrack;)V

    goto/16 :goto_0

    .line 382
    :cond_11
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->pw:Ljava/lang/String;

    const-string v1, "travel_mode"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 384
    const-string v0, "type"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 385
    invoke-static {v0}, Lcom/glympse/android/lib/fu;->A(Ljava/lang/String;)I

    move-result v0

    .line 386
    const-string v1, "settings"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v1

    .line 387
    iget-object v2, p0, Lcom/glympse/android/lib/cz$c;->pp:Lcom/glympse/android/lib/cz;

    iget-object v2, v2, Lcom/glympse/android/lib/cz;->pe:Lcom/glympse/android/lib/GTicketPrivate;

    new-instance v3, Lcom/glympse/android/lib/fu;

    invoke-direct {v3, v0, v1}, Lcom/glympse/android/lib/fu;-><init>(ILcom/glympse/android/core/GPrimitive;)V

    invoke-interface {v2, v3}, Lcom/glympse/android/lib/GTicketPrivate;->setTravelMode(Lcom/glympse/android/api/GTravelMode;)V

    goto/16 :goto_0

    .line 389
    :cond_12
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->pw:Ljava/lang/String;

    const-string v1, "start_time"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 391
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->pp:Lcom/glympse/android/lib/cz;

    iget-object v0, v0, Lcom/glympse/android/lib/cz;->pe:Lcom/glympse/android/lib/GTicketPrivate;

    invoke-interface {p1}, Lcom/glympse/android/core/GPrimitive;->getLong()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/glympse/android/lib/GTicketPrivate;->setStartTime(J)V

    goto/16 :goto_0

    .line 393
    :cond_13
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->pw:Ljava/lang/String;

    const-string v1, "completed"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 395
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->pp:Lcom/glympse/android/lib/cz;

    iget-object v0, v0, Lcom/glympse/android/lib/cz;->pe:Lcom/glympse/android/lib/GTicketPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketPrivate;->setCompleted()V

    goto/16 :goto_0

    .line 398
    :cond_14
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->pw:Ljava/lang/String;

    const-string v1, "name"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 400
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->pp:Lcom/glympse/android/lib/cz;

    iget-object v0, v0, Lcom/glympse/android/lib/cz;->oZ:Lcom/glympse/android/lib/GUserPrivate;

    invoke-interface {p1}, Lcom/glympse/android/core/GPrimitive;->getString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GUserPrivate;->setNicknameCore(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 402
    :cond_15
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->pw:Ljava/lang/String;

    const-string v1, "avatar"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 404
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->pp:Lcom/glympse/android/lib/cz;

    iget-object v0, v0, Lcom/glympse/android/lib/cz;->oZ:Lcom/glympse/android/lib/GUserPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GUserPrivate;->getAvatar()Lcom/glympse/android/api/GImage;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GImagePrivate;

    .line 405
    invoke-interface {p1}, Lcom/glympse/android/core/GPrimitive;->getString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GImagePrivate;->setUrl(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 407
    :cond_16
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->pw:Ljava/lang/String;

    const-string v1, "owner"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 409
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->pp:Lcom/glympse/android/lib/cz;

    iget-object v0, v0, Lcom/glympse/android/lib/cz;->oZ:Lcom/glympse/android/lib/GUserPrivate;

    invoke-interface {p1}, Lcom/glympse/android/core/GPrimitive;->getString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GUserPrivate;->setId(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public primitive(ILcom/glympse/android/lib/json/GJsonPrimitive;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 456
    const/4 v0, 0x2

    if-ne v0, p1, :cond_0

    .line 458
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->gE:Ljava/lang/String;

    const-string v1, "t"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 460
    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/glympse/android/lib/cz$c;->pv:J

    .line 471
    :cond_0
    :goto_0
    return v2

    .line 462
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->gE:Ljava/lang/String;

    const-string v1, "pid"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 464
    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/glympse/android/lib/cz$c;->jH:J

    goto :goto_0

    .line 466
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->gE:Ljava/lang/String;

    const-string v1, "n"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 468
    invoke-virtual {p2, v2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/cz$c;->pw:Ljava/lang/String;

    goto :goto_0
.end method

.method public startObject(I)Z
    .locals 2

    .prologue
    .line 434
    const/4 v0, 0x2

    if-ne v0, p1, :cond_0

    .line 436
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/glympse/android/lib/cz$c;->jH:J

    .line 438
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public startPair(ILjava/lang/String;)Z
    .locals 2

    .prologue
    .line 420
    iput-object p2, p0, Lcom/glympse/android/lib/cz$c;->gE:Ljava/lang/String;

    .line 421
    const/4 v0, 0x2

    if-ne v0, p1, :cond_0

    .line 423
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->gE:Ljava/lang/String;

    const-string v1, "v"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 425
    new-instance v0, Lcom/glympse/android/lib/json/JsonSerializer$a;

    iget-object v1, p0, Lcom/glympse/android/lib/cz$c;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    invoke-direct {v0, v1, p1}, Lcom/glympse/android/lib/json/JsonSerializer$a;-><init>(Lcom/glympse/android/lib/json/GJsonHandlerStack;I)V

    iput-object v0, p0, Lcom/glympse/android/lib/cz$c;->px:Lcom/glympse/android/lib/json/JsonSerializer$a;

    .line 426
    iget-object v0, p0, Lcom/glympse/android/lib/cz$c;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    iget-object v1, p0, Lcom/glympse/android/lib/cz$c;->px:Lcom/glympse/android/lib/json/JsonSerializer$a;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/json/GJsonHandlerStack;->pushHandler(Lcom/glympse/android/lib/json/GJsonHandler;)V

    .line 429
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
