.class Lcom/glympse/android/lib/bo;
.super Ljava/lang/Object;
.source "GeoTrigger.java"

# interfaces
.implements Lcom/glympse/android/api/GGeoTrigger;


# instance fields
.field private db:Lcom/glympse/android/core/GRegion;

.field private ic:Lcom/glympse/android/lib/fv;

.field private kO:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Lcom/glympse/android/lib/fv;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/fv;-><init>(I)V

    iput-object v0, p0, Lcom/glympse/android/lib/bo;->ic:Lcom/glympse/android/lib/fv;

    .line 27
    const/4 v0, 0x2

    iput v0, p0, Lcom/glympse/android/lib/bo;->kO:I

    .line 28
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZLcom/glympse/android/api/GTicket;Lcom/glympse/android/core/GLatLng;DI)V
    .locals 7

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Lcom/glympse/android/lib/fv;

    const/4 v1, 0x1

    invoke-direct {v0, v1, p1, p2, p3}, Lcom/glympse/android/lib/fv;-><init>(ILjava/lang/String;ZLcom/glympse/android/api/GTicket;)V

    iput-object v0, p0, Lcom/glympse/android/lib/bo;->ic:Lcom/glympse/android/lib/fv;

    .line 33
    invoke-interface {p4}, Lcom/glympse/android/core/GLatLng;->getLatitude()D

    move-result-wide v0

    invoke-interface {p4}, Lcom/glympse/android/core/GLatLng;->getLongitude()D

    move-result-wide v2

    iget-object v4, p0, Lcom/glympse/android/lib/bo;->ic:Lcom/glympse/android/lib/fv;

    invoke-virtual {v4}, Lcom/glympse/android/lib/fv;->getId()Ljava/lang/String;

    move-result-object v6

    move-wide v4, p5

    invoke-static/range {v0 .. v6}, Lcom/glympse/android/core/CoreFactory;->createRegion(DDDLjava/lang/String;)Lcom/glympse/android/core/GRegion;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/bo;->db:Lcom/glympse/android/core/GRegion;

    .line 34
    iput p7, p0, Lcom/glympse/android/lib/bo;->kO:I

    .line 35
    return-void
.end method


# virtual methods
.method public autoSend()Z
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/glympse/android/lib/bo;->ic:Lcom/glympse/android/lib/fv;

    invoke-virtual {v0}, Lcom/glympse/android/lib/fv;->autoSend()Z

    move-result v0

    return v0
.end method

.method public decode(Lcom/glympse/android/core/GPrimitive;)V
    .locals 8

    .prologue
    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    .line 104
    iget-object v2, p0, Lcom/glympse/android/lib/bo;->ic:Lcom/glympse/android/lib/fv;

    invoke-virtual {v2, p1}, Lcom/glympse/android/lib/fv;->decode(Lcom/glympse/android/core/GPrimitive;)V

    .line 106
    const-string v2, "rgn"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v7

    .line 107
    if-eqz v7, :cond_0

    .line 109
    const/4 v6, 0x0

    move-wide v2, v0

    move-wide v4, v0

    invoke-static/range {v0 .. v6}, Lcom/glympse/android/core/CoreFactory;->createRegion(DDDLjava/lang/String;)Lcom/glympse/android/core/GRegion;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/bo;->db:Lcom/glympse/android/core/GRegion;

    .line 110
    iget-object v0, p0, Lcom/glympse/android/lib/bo;->db:Lcom/glympse/android/core/GRegion;

    invoke-interface {v0, v7}, Lcom/glympse/android/core/GRegion;->decode(Lcom/glympse/android/core/GPrimitive;)V

    .line 112
    :cond_0
    const-string v0, "trns"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/glympse/android/lib/bo;->kO:I

    .line 113
    return-void
.end method

.method public encode(Lcom/glympse/android/core/GPrimitive;I)V
    .locals 4

    .prologue
    .line 91
    iget-object v0, p0, Lcom/glympse/android/lib/bo;->ic:Lcom/glympse/android/lib/fv;

    invoke-virtual {v0, p1, p2}, Lcom/glympse/android/lib/fv;->encode(Lcom/glympse/android/core/GPrimitive;I)V

    .line 93
    iget-object v0, p0, Lcom/glympse/android/lib/bo;->db:Lcom/glympse/android/core/GRegion;

    if-eqz v0, :cond_0

    .line 95
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/glympse/android/core/CoreFactory;->createPrimitive(I)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 96
    iget-object v1, p0, Lcom/glympse/android/lib/bo;->db:Lcom/glympse/android/core/GRegion;

    invoke-interface {v1, v0, p2}, Lcom/glympse/android/core/GRegion;->encode(Lcom/glympse/android/core/GPrimitive;I)V

    .line 97
    const-string v1, "rgn"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1, v0}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 99
    :cond_0
    const-string v0, "trns"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/glympse/android/lib/bo;->kO:I

    int-to-long v2, v1

    invoke-interface {p1, v0, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 100
    return-void
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/glympse/android/lib/bo;->ic:Lcom/glympse/android/lib/fv;

    invoke-virtual {v0}, Lcom/glympse/android/lib/fv;->getId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/glympse/android/lib/bo;->ic:Lcom/glympse/android/lib/fv;

    invoke-virtual {v0}, Lcom/glympse/android/lib/fv;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRegion()Lcom/glympse/android/core/GRegion;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/glympse/android/lib/bo;->db:Lcom/glympse/android/core/GRegion;

    return-object v0
.end method

.method public getTicket()Lcom/glympse/android/api/GTicket;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/glympse/android/lib/bo;->ic:Lcom/glympse/android/lib/fv;

    invoke-virtual {v0}, Lcom/glympse/android/lib/fv;->getTicket()Lcom/glympse/android/api/GTicket;

    move-result-object v0

    return-object v0
.end method

.method public getTimeConstraint()Lcom/glympse/android/api/GTimeConstraint;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/glympse/android/lib/bo;->ic:Lcom/glympse/android/lib/fv;

    invoke-virtual {v0}, Lcom/glympse/android/lib/fv;->getTimeConstraint()Lcom/glympse/android/api/GTimeConstraint;

    move-result-object v0

    return-object v0
.end method

.method public getTransition()I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/glympse/android/lib/bo;->kO:I

    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/glympse/android/lib/bo;->ic:Lcom/glympse/android/lib/fv;

    invoke-virtual {v0}, Lcom/glympse/android/lib/fv;->getType()I

    move-result v0

    return v0
.end method
