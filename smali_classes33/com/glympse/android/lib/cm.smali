.class Lcom/glympse/android/lib/cm;
.super Ljava/lang/Object;
.source "HistoryManager.java"

# interfaces
.implements Lcom/glympse/android/lib/GHistoryManagerPrivate;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/lib/cm$c;,
        Lcom/glympse/android/lib/cm$b;,
        Lcom/glympse/android/lib/cm$a;
    }
.end annotation


# instance fields
.field private _glympse:Lcom/glympse/android/lib/GGlympsePrivate;

.field private gf:Lcom/glympse/android/lib/GConfigPrivate;

.field private lI:Lcom/glympse/android/lib/GServerPost;

.field private lJ:Lcom/glympse/android/lib/GTicketProtocol;

.field private lX:Lcom/glympse/android/lib/GCorrectedTime;

.field private mz:Z

.field private nA:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/glympse/android/api/GTicket;",
            ">;"
        }
    .end annotation
.end field

.field private nB:Lcom/glympse/android/hal/GSharedPreferences;

.field private nC:Lcom/glympse/android/lib/go;

.field private nn:Lcom/glympse/android/lib/cn;

.field private no:Z

.field private np:Lcom/glympse/android/hal/GVector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GVector",
            "<",
            "Lcom/glympse/android/api/GTicket;",
            ">;"
        }
    .end annotation
.end field

.field private nq:Lcom/glympse/android/hal/GHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GHashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/glympse/android/api/GTicket;",
            ">;"
        }
    .end annotation
.end field

.field private nr:Lcom/glympse/android/hal/GHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GHashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/glympse/android/api/GTicket;",
            ">;"
        }
    .end annotation
.end field

.field private ns:J

.field private nt:J

.field private nu:J

.field private nv:Lcom/glympse/android/hal/GVector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GVector",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private nw:Z

.field private nx:Z

.field private ny:I

.field private nz:I


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    new-instance v0, Lcom/glympse/android/lib/cn;

    invoke-direct {v0}, Lcom/glympse/android/lib/cn;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/cm;->nn:Lcom/glympse/android/lib/cn;

    .line 102
    iput-boolean v2, p0, Lcom/glympse/android/lib/cm;->no:Z

    .line 103
    new-instance v0, Lcom/glympse/android/hal/GVector;

    invoke-direct {v0}, Lcom/glympse/android/hal/GVector;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/cm;->np:Lcom/glympse/android/hal/GVector;

    .line 104
    new-instance v0, Lcom/glympse/android/hal/GHashtable;

    invoke-direct {v0}, Lcom/glympse/android/hal/GHashtable;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/cm;->nq:Lcom/glympse/android/hal/GHashtable;

    .line 105
    new-instance v0, Lcom/glympse/android/hal/GHashtable;

    invoke-direct {v0}, Lcom/glympse/android/hal/GHashtable;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/cm;->nr:Lcom/glympse/android/hal/GHashtable;

    .line 106
    iput-wide v4, p0, Lcom/glympse/android/lib/cm;->ns:J

    .line 107
    iput-wide v4, p0, Lcom/glympse/android/lib/cm;->nt:J

    .line 108
    iput-wide v4, p0, Lcom/glympse/android/lib/cm;->nu:J

    .line 109
    new-instance v0, Lcom/glympse/android/hal/GVector;

    invoke-direct {v0}, Lcom/glympse/android/hal/GVector;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/cm;->nv:Lcom/glympse/android/hal/GVector;

    .line 110
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/glympse/android/lib/cm;->nw:Z

    .line 111
    iput-boolean v2, p0, Lcom/glympse/android/lib/cm;->nx:Z

    .line 112
    const v0, 0xea60

    iput v0, p0, Lcom/glympse/android/lib/cm;->ny:I

    .line 113
    iput v2, p0, Lcom/glympse/android/lib/cm;->nz:I

    .line 114
    new-instance v0, Lcom/glympse/android/lib/cm$c;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/cm$c;-><init>(Lcom/glympse/android/lib/cm$1;)V

    iput-object v0, p0, Lcom/glympse/android/lib/cm;->nA:Ljava/util/Comparator;

    .line 115
    iput-boolean v2, p0, Lcom/glympse/android/lib/cm;->mz:Z

    .line 116
    return-void
.end method

.method private a(Lcom/glympse/android/api/GInvite;)V
    .locals 2

    .prologue
    .line 1191
    invoke-interface {p1}, Lcom/glympse/android/api/GInvite;->getCode()Ljava/lang/String;

    move-result-object v0

    .line 1192
    if-eqz v0, :cond_0

    .line 1194
    iget-object v1, p0, Lcom/glympse/android/lib/cm;->nr:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v1, v0}, Lcom/glympse/android/hal/GHashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1196
    :cond_0
    return-void
.end method

.method private a(Lcom/glympse/android/api/GTicket;Lcom/glympse/android/api/GInvite;)V
    .locals 2

    .prologue
    .line 1172
    invoke-interface {p2}, Lcom/glympse/android/api/GInvite;->getCode()Ljava/lang/String;

    move-result-object v0

    .line 1173
    if-eqz v0, :cond_0

    .line 1175
    iget-object v1, p0, Lcom/glympse/android/lib/cm;->nr:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v1, v0, p1}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1177
    :cond_0
    return-void
.end method

.method private a(Lcom/glympse/android/lib/GTicketPrivate;Z)V
    .locals 4

    .prologue
    .line 1038
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->np:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 1041
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/cm;->h(Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 1044
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/cm;->j(Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 1047
    iget-object v1, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GTicketParent;

    invoke-interface {p1, v1, v0}, Lcom/glympse/android/lib/GTicketPrivate;->setGlympse(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/GTicketParent;)V

    .line 1050
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->nC:Lcom/glympse/android/lib/go;

    invoke-virtual {v0, p1}, Lcom/glympse/android/lib/go;->v(Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 1056
    invoke-virtual {p0}, Lcom/glympse/android/lib/cm;->orderChanged()V

    .line 1060
    if-eqz p2, :cond_1

    iget-boolean v0, p0, Lcom/glympse/android/lib/cm;->no:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/glympse/android/lib/cm;->nw:Z

    if-eqz v0, :cond_1

    .line 1063
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/4 v2, 0x1

    const/high16 v3, 0x20000

    invoke-interface {v0, v1, v2, v3, p1}, Lcom/glympse/android/lib/GGlympsePrivate;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 1065
    :cond_1
    return-void
.end method

.method private ca()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 500
    iget-object v1, p0, Lcom/glympse/android/lib/cm;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GConfigPrivate;->shouldForceRefresh()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 517
    :cond_0
    :goto_0
    return v0

    .line 504
    :cond_1
    iget-object v1, p0, Lcom/glympse/android/lib/cm;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GConfigPrivate;->areAccountsLinked()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->isAccountSharingEnabled()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 506
    :cond_2
    iget-object v1, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->areSiblingTicketsAllowed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 512
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GConfigPrivate;->hasTicketBeenSent()Z

    move-result v0

    goto :goto_0

    .line 517
    :cond_3
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GConfigPrivate;->hasTicketBeenSent()Z

    move-result v0

    goto :goto_0
.end method

.method private cb()V
    .locals 3

    .prologue
    .line 784
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getHandler()Lcom/glympse/android/core/GHandler;

    move-result-object v0

    new-instance v1, Lcom/glympse/android/lib/cm$a;

    iget-object v2, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct {v1, v2}, Lcom/glympse/android/lib/cm$a;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;)V

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GHandler;->post(Ljava/lang/Runnable;)V

    .line 785
    return-void
.end method

.method private cc()V
    .locals 2

    .prologue
    .line 1249
    iget-boolean v0, p0, Lcom/glympse/android/lib/cm;->mz:Z

    if-nez v0, :cond_0

    .line 1260
    :goto_0
    return-void

    .line 1253
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/glympse/android/lib/cm;->mz:Z

    .line 1256
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->np:Lcom/glympse/android/hal/GVector;

    iget-object v1, p0, Lcom/glympse/android/lib/cm;->nA:Ljava/util/Comparator;

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GVector;->sort(Ljava/util/Comparator;)V

    .line 1259
    invoke-direct {p0}, Lcom/glympse/android/lib/cm;->cd()V

    goto :goto_0
.end method

.method private cd()V
    .locals 2

    .prologue
    .line 1273
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->np:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/cm;->np:Lcom/glympse/android/hal/GVector;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GTicket;

    invoke-interface {v0}, Lcom/glympse/android/api/GTicket;->getExpireTime()J

    move-result-wide v0

    :goto_0
    invoke-direct {p0, v0, v1}, Lcom/glympse/android/lib/cm;->i(J)V

    .line 1274
    return-void

    .line 1273
    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method private ce()V
    .locals 2

    .prologue
    .line 1301
    new-instance v0, Lcom/glympse/android/lib/go;

    invoke-direct {v0}, Lcom/glympse/android/lib/go;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/cm;->nC:Lcom/glympse/android/lib/go;

    .line 1302
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->nC:Lcom/glympse/android/lib/go;

    iget-object v1, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-virtual {v0, v1}, Lcom/glympse/android/lib/go;->start(Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 1305
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getConfig()Lcom/glympse/android/api/GConfig;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GConfigPrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/cm;->nC:Lcom/glympse/android/lib/go;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GConfigPrivate;->addListener(Lcom/glympse/android/api/GEventListener;)Z

    .line 1309
    return-void
.end method

.method private cf()V
    .locals 2

    .prologue
    .line 1314
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getConfig()Lcom/glympse/android/api/GConfig;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GConfigPrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/cm;->nC:Lcom/glympse/android/lib/go;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GConfigPrivate;->removeListener(Lcom/glympse/android/api/GEventListener;)Z

    .line 1315
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->nC:Lcom/glympse/android/lib/go;

    invoke-virtual {v0}, Lcom/glympse/android/lib/go;->stop()V

    .line 1316
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/cm;->nC:Lcom/glympse/android/lib/go;

    .line 1317
    return-void
.end method

.method private g(Z)V
    .locals 10

    .prologue
    const-wide/16 v0, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 546
    iget-object v2, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v2}, Lcom/glympse/android/lib/GGlympsePrivate;->getNotificationCenter()Lcom/glympse/android/lib/GNotificationCenter;

    move-result-object v2

    const/4 v5, 0x2

    invoke-interface {v2, v5}, Lcom/glympse/android/lib/GNotificationCenter;->skipSync(I)V

    .line 550
    if-eqz p1, :cond_3

    .line 555
    iget-object v2, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v2}, Lcom/glympse/android/lib/GGlympsePrivate;->getHistoryLookback()J

    move-result-wide v6

    .line 556
    iget-object v2, p0, Lcom/glympse/android/lib/cm;->nn:Lcom/glympse/android/lib/cn;

    iget-wide v8, v2, Lcom/glympse/android/lib/cn;->nu:J

    cmp-long v2, v0, v8

    if-nez v2, :cond_2

    const-wide/16 v8, -0x1

    cmp-long v2, v8, v6

    if-nez v2, :cond_1

    .line 577
    :goto_0
    iget-object v2, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v2}, Lcom/glympse/android/lib/GGlympsePrivate;->getConfigPrivate()Lcom/glympse/android/lib/GConfigPrivate;

    move-result-object v2

    invoke-interface {v2}, Lcom/glympse/android/lib/GConfigPrivate;->shouldForceRefresh()Z

    move-result v2

    if-nez v2, :cond_0

    if-eqz p1, :cond_4

    :cond_0
    move v2, v4

    .line 580
    :goto_1
    new-instance v5, Lcom/glympse/android/lib/gi;

    iget-object v6, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct {v5, v6, v2, v0, v1}, Lcom/glympse/android/lib/gi;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;ZJ)V

    .line 581
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->lI:Lcom/glympse/android/lib/GServerPost;

    invoke-interface {v0, v5, v3, v4}, Lcom/glympse/android/lib/GServerPost;->invokeEndpoint(Lcom/glympse/android/lib/GApiEndpoint;ZZ)V

    .line 582
    return-void

    .line 556
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->lX:Lcom/glympse/android/lib/GCorrectedTime;

    .line 564
    invoke-interface {v0}, Lcom/glympse/android/lib/GCorrectedTime;->getTime()J

    move-result-wide v0

    sub-long/2addr v0, v6

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->nn:Lcom/glympse/android/lib/cn;

    iget-wide v0, v0, Lcom/glympse/android/lib/cn;->nu:J

    goto :goto_0

    .line 572
    :cond_3
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->lX:Lcom/glympse/android/lib/GCorrectedTime;

    .line 573
    invoke-interface {v0}, Lcom/glympse/android/lib/GCorrectedTime;->getTime()J

    move-result-wide v0

    const-wide/32 v6, 0xdbba00

    sub-long/2addr v0, v6

    iget-wide v6, p0, Lcom/glympse/android/lib/cm;->nu:J

    .line 572
    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    goto :goto_0

    :cond_4
    move v2, v3

    .line 577
    goto :goto_1
.end method

.method private h(J)V
    .locals 3

    .prologue
    .line 482
    iput-wide p1, p0, Lcom/glympse/android/lib/cm;->ns:J

    .line 485
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GConfigPrivate;->getPostRatePeriod()I

    move-result v0

    int-to-long v0, v0

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/glympse/android/lib/cm;->nt:J

    .line 486
    return-void
.end method

.method private h(Lcom/glympse/android/lib/GTicketPrivate;)V
    .locals 2

    .prologue
    .line 1139
    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getId()Ljava/lang/String;

    move-result-object v0

    .line 1140
    if-eqz v0, :cond_0

    .line 1142
    iget-object v1, p0, Lcom/glympse/android/lib/cm;->nq:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v1, v0, p1}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1144
    :cond_0
    return-void
.end method

.method private i(J)V
    .locals 5

    .prologue
    .line 1279
    iput-wide p1, p0, Lcom/glympse/android/lib/cm;->nu:J

    .line 1285
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->nB:Lcom/glympse/android/hal/GSharedPreferences;

    const-string v1, "latest_expire_time_v2"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-wide v2, p0, Lcom/glympse/android/lib/cm;->nu:J

    invoke-interface {v0, v1, v2, v3}, Lcom/glympse/android/hal/GSharedPreferences;->putLong(Ljava/lang/String;J)V

    .line 1286
    return-void
.end method

.method private i(Lcom/glympse/android/lib/GTicketPrivate;)V
    .locals 2

    .prologue
    .line 1148
    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getId()Ljava/lang/String;

    move-result-object v0

    .line 1149
    if-eqz v0, :cond_0

    .line 1151
    iget-object v1, p0, Lcom/glympse/android/lib/cm;->nq:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v1, v0}, Lcom/glympse/android/hal/GHashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1153
    :cond_0
    return-void
.end method

.method private j(Lcom/glympse/android/lib/GTicketPrivate;)V
    .locals 4

    .prologue
    .line 1162
    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getInvites()Lcom/glympse/android/core/GArray;

    move-result-object v2

    .line 1163
    invoke-interface {v2}, Lcom/glympse/android/core/GArray;->length()I

    move-result v3

    .line 1164
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 1166
    invoke-interface {v2, v1}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GInvite;

    invoke-direct {p0, p1, v0}, Lcom/glympse/android/lib/cm;->a(Lcom/glympse/android/api/GTicket;Lcom/glympse/android/api/GInvite;)V

    .line 1164
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1168
    :cond_0
    return-void
.end method

.method private k(Lcom/glympse/android/lib/GTicketPrivate;)V
    .locals 4

    .prologue
    .line 1181
    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getInvites()Lcom/glympse/android/core/GArray;

    move-result-object v2

    .line 1182
    invoke-interface {v2}, Lcom/glympse/android/core/GArray;->length()I

    move-result v3

    .line 1183
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 1185
    invoke-interface {v2, v1}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GInvite;

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/cm;->a(Lcom/glympse/android/api/GInvite;)V

    .line 1183
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1187
    :cond_0
    return-void
.end method

.method private n(Ljava/lang/String;)Lcom/glympse/android/api/GTicket;
    .locals 1

    .prologue
    .line 1157
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->nq:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GTicket;

    return-object v0
.end method

.method private o(Ljava/lang/String;)Lcom/glympse/android/api/GTicket;
    .locals 1

    .prologue
    .line 1200
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->nr:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GTicket;

    return-object v0
.end method


# virtual methods
.method public addTicket(Lcom/glympse/android/lib/GTicketPrivate;)V
    .locals 1

    .prologue
    .line 1029
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/glympse/android/lib/cm;->a(Lcom/glympse/android/lib/GTicketPrivate;Z)V

    .line 1030
    return-void
.end method

.method public anyActive()Z
    .locals 1

    .prologue
    .line 134
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/cm;->anyActive(Z)Z

    move-result v0

    return v0
.end method

.method public anyActive(Z)Z
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/glympse/android/lib/cm;->anyActive(ZZ)Z

    move-result v0

    return v0
.end method

.method public anyActive(ZZ)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 145
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->lX:Lcom/glympse/android/lib/GCorrectedTime;

    invoke-interface {v0}, Lcom/glympse/android/lib/GCorrectedTime;->getTime()J

    move-result-wide v4

    .line 150
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->np:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 152
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->np:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, v2}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GTicket;

    .line 155
    invoke-virtual {p0, v0, v4, v5}, Lcom/glympse/android/lib/cm;->isActive(Lcom/glympse/android/api/GTicket;J)Z

    move-result v3

    if-eqz v3, :cond_2

    if-nez p1, :cond_0

    invoke-interface {v0}, Lcom/glympse/android/api/GTicket;->isMine()Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    if-eqz p2, :cond_1

    invoke-interface {v0}, Lcom/glympse/android/api/GTicket;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 163
    :cond_1
    :goto_0
    return v1

    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->nn:Lcom/glympse/android/lib/cn;

    iget-wide v6, v0, Lcom/glympse/android/lib/cn;->nG:J

    cmp-long v0, v6, v4

    if-lez v0, :cond_3

    move v0, v1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method public arePreSyncEventsEnabled()Z
    .locals 1

    .prologue
    .line 264
    iget-boolean v0, p0, Lcom/glympse/android/lib/cm;->nw:Z

    return v0
.end method

.method public canSend(Lcom/glympse/android/api/GInvite;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 218
    iget-object v1, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v1, :cond_0

    if-nez p1, :cond_1

    .line 254
    :cond_0
    :goto_0
    return v0

    .line 224
    :cond_1
    invoke-interface {p1}, Lcom/glympse/android/api/GInvite;->getType()I

    move-result v1

    .line 225
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 234
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 239
    :pswitch_1
    invoke-static {}, Lcom/glympse/android/hal/Platform;->getOsName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 246
    :pswitch_2
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getLinkedAccountsManager()Lcom/glympse/android/api/GLinkedAccountsManager;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/glympse/android/api/GLinkedAccountsManager;->canSend(Lcom/glympse/android/api/GInvite;)Z

    move-result v0

    goto :goto_0

    .line 251
    :pswitch_3
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getApplicationsManager()Lcom/glympse/android/api/GApplicationsManager;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/glympse/android/api/GApplicationsManager;->canSend(Lcom/glympse/android/api/GInvite;)Z

    move-result v0

    goto :goto_0

    .line 225
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public clearLatestExpireTime()V
    .locals 2

    .prologue
    .line 1268
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/glympse/android/lib/cm;->i(J)V

    .line 1269
    return-void
.end method

.method public completePending()V
    .locals 4

    .prologue
    .line 1111
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->nv:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v2

    .line 1112
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 1114
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->nv:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1115
    invoke-direct {p0, v0}, Lcom/glympse/android/lib/cm;->n(Ljava/lang/String;)Lcom/glympse/android/api/GTicket;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GTicketPrivate;

    .line 1116
    if-eqz v0, :cond_0

    .line 1122
    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketPrivate;->isCompleted()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1124
    iget-object v3, p0, Lcom/glympse/android/lib/cm;->lJ:Lcom/glympse/android/lib/GTicketProtocol;

    invoke-interface {v3, v0}, Lcom/glympse/android/lib/GTicketProtocol;->appendCompleted(Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 1112
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1130
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->nv:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->removeAllElements()V

    .line 1131
    return-void
.end method

.method public completeTicket(Lcom/glympse/android/lib/GTicketPrivate;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1094
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->lI:Lcom/glympse/android/lib/GServerPost;

    invoke-interface {v0}, Lcom/glympse/android/lib/GServerPost;->areLocationsPartiallyUploaded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1096
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->nv:Lcom/glympse/android/hal/GVector;

    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 1098
    const-string v0, "[HistoryManager.completeTicket] Still uploading locations..."

    invoke-static {v2, v0}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 1106
    :goto_0
    return-void

    .line 1102
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->lJ:Lcom/glympse/android/lib/GTicketProtocol;

    invoke-interface {v0, p1}, Lcom/glympse/android/lib/GTicketProtocol;->appendCompleted(Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 1104
    const-string v0, "[HistoryManager.completeTicket] \"completed\" event was queued up"

    invoke-static {v2, v0}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public enableCancellationTimer(Z)V
    .locals 0

    .prologue
    .line 269
    iput-boolean p1, p0, Lcom/glympse/android/lib/cm;->nx:Z

    .line 270
    return-void
.end method

.method public enablePreSyncEvents(Z)V
    .locals 0

    .prologue
    .line 259
    iput-boolean p1, p0, Lcom/glympse/android/lib/cm;->nw:Z

    .line 260
    return-void
.end method

.method public findTicketByInviteCode(Ljava/lang/String;)Lcom/glympse/android/api/GTicket;
    .locals 1

    .prologue
    .line 361
    invoke-static {p1}, Lcom/glympse/android/lib/TicketCode;->cleanupInviteCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 362
    if-nez v0, :cond_0

    .line 364
    const/4 v0, 0x0

    .line 366
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, v0}, Lcom/glympse/android/lib/cm;->o(Ljava/lang/String;)Lcom/glympse/android/api/GTicket;

    move-result-object v0

    goto :goto_0
.end method

.method public findTicketByTicketId(Ljava/lang/String;)Lcom/glympse/android/api/GTicket;
    .locals 1

    .prologue
    .line 352
    invoke-static {p1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 354
    const/4 v0, 0x0

    .line 356
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/cm;->n(Ljava/lang/String;)Lcom/glympse/android/api/GTicket;

    move-result-object v0

    goto :goto_0
.end method

.method public getCancellationTimeout()I
    .locals 1

    .prologue
    .line 284
    iget v0, p0, Lcom/glympse/android/lib/cm;->ny:I

    return v0
.end method

.method public getExpirationMode()I
    .locals 1

    .prologue
    .line 315
    iget v0, p0, Lcom/glympse/android/lib/cm;->nz:I

    return v0
.end method

.method public getLastViewTime()J
    .locals 2

    .prologue
    .line 341
    iget-wide v0, p0, Lcom/glympse/android/lib/cm;->ns:J

    return-wide v0
.end method

.method public getTickets()Lcom/glympse/android/core/GArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/glympse/android/core/GArray",
            "<",
            "Lcom/glympse/android/api/GTicket;",
            ">;"
        }
    .end annotation

    .prologue
    .line 129
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->np:Lcom/glympse/android/hal/GVector;

    return-object v0
.end method

.method public inviteCreated(Lcom/glympse/android/api/GTicket;Lcom/glympse/android/api/GInvite;)V
    .locals 0

    .prologue
    .line 1236
    invoke-direct {p0, p1, p2}, Lcom/glympse/android/lib/cm;->a(Lcom/glympse/android/api/GTicket;Lcom/glympse/android/api/GInvite;)V

    .line 1237
    return-void
.end method

.method public inviteRemoved(Lcom/glympse/android/api/GTicket;Lcom/glympse/android/api/GInvite;)V
    .locals 0

    .prologue
    .line 1243
    invoke-direct {p0, p2}, Lcom/glympse/android/lib/cm;->a(Lcom/glympse/android/api/GInvite;)V

    .line 1244
    return-void
.end method

.method public isActive(Lcom/glympse/android/api/GTicket;J)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 433
    iget v2, p0, Lcom/glympse/android/lib/cm;->nz:I

    if-nez v2, :cond_2

    .line 436
    invoke-interface {p1}, Lcom/glympse/android/api/GTicket;->getExpireTime()J

    move-result-wide v2

    cmp-long v2, v2, p2

    if-lez v2, :cond_1

    .line 441
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 436
    goto :goto_0

    .line 441
    :cond_2
    invoke-interface {p1}, Lcom/glympse/android/api/GTicket;->getState()I

    move-result v2

    and-int/lit8 v2, v2, 0x12

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public isCancellationTimerEnabled()Z
    .locals 1

    .prologue
    .line 274
    iget-boolean v0, p0, Lcom/glympse/android/lib/cm;->nx:Z

    return v0
.end method

.method public isSomeoneWatching()Z
    .locals 4

    .prologue
    .line 347
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->lX:Lcom/glympse/android/lib/GCorrectedTime;

    invoke-interface {v0}, Lcom/glympse/android/lib/GCorrectedTime;->getTime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/glympse/android/lib/cm;->nt:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSynced()Z
    .locals 1

    .prologue
    .line 124
    iget-boolean v0, p0, Lcom/glympse/android/lib/cm;->no:Z

    return v0
.end method

.method public orderChanged()V
    .locals 5

    .prologue
    .line 1210
    iget-boolean v0, p0, Lcom/glympse/android/lib/cm;->mz:Z

    if-eqz v0, :cond_1

    .line 1230
    :cond_0
    :goto_0
    return-void

    .line 1218
    :cond_1
    iget-boolean v0, p0, Lcom/glympse/android/lib/cm;->no:Z

    iput-boolean v0, p0, Lcom/glympse/android/lib/cm;->mz:Z

    .line 1221
    iget-boolean v0, p0, Lcom/glympse/android/lib/cm;->mz:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_0

    .line 1224
    invoke-direct {p0}, Lcom/glympse/android/lib/cm;->cc()V

    .line 1228
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/4 v2, 0x1

    const/high16 v3, 0x100000

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/glympse/android/lib/GGlympsePrivate;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    goto :goto_0
.end method

.method public refresh()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 168
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-nez v0, :cond_0

    .line 192
    :goto_0
    return-void

    .line 173
    :cond_0
    const-string v0, "[HistoryManager.refresh]"

    invoke-static {v1, v0}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 177
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->isSharingSiblings()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 179
    const-string v0, "[HistoryManager.refresh] cancelled: Active ticket"

    invoke-static {v1, v0}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    goto :goto_0

    .line 184
    :cond_1
    invoke-direct {p0}, Lcom/glympse/android/lib/cm;->ca()Z

    move-result v0

    if-nez v0, :cond_2

    .line 186
    const-string v0, "[HistoryManager.refresh] cancelled: No need to sync history"

    invoke-static {v1, v0}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    goto :goto_0

    .line 191
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->lJ:Lcom/glympse/android/lib/GTicketProtocol;

    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketProtocol;->refreshInvites()V

    goto :goto_0
.end method

.method public removeTicket(Lcom/glympse/android/lib/GTicketPrivate;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1070
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->nC:Lcom/glympse/android/lib/go;

    invoke-virtual {v0, p1}, Lcom/glympse/android/lib/go;->w(Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 1073
    invoke-interface {p1, v1, v1}, Lcom/glympse/android/lib/GTicketPrivate;->setGlympse(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/GTicketParent;)V

    .line 1076
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->np:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GVector;->removeElement(Ljava/lang/Object;)Z

    .line 1079
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/cm;->i(Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 1082
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/cm;->k(Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 1085
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/4 v2, 0x1

    const/high16 v3, 0x40000

    invoke-interface {v0, v1, v2, v3, p1}, Lcom/glympse/android/lib/GGlympsePrivate;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 1086
    return-void
.end method

.method public sendTicket(Lcom/glympse/android/api/GTicket;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 794
    if-nez p1, :cond_1

    .line 860
    :cond_0
    :goto_0
    return v0

    .line 800
    :cond_1
    invoke-interface {p1}, Lcom/glympse/android/api/GTicket;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 808
    iget-object v2, p0, Lcom/glympse/android/lib/cm;->np:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v2, p1}, Lcom/glympse/android/hal/GVector;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 814
    iget-object v2, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v2}, Lcom/glympse/android/lib/GGlympsePrivate;->getBrand()Ljava/lang/String;

    move-result-object v3

    .line 815
    invoke-static {v3}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 817
    invoke-interface {p1}, Lcom/glympse/android/api/GTicket;->getInvites()Lcom/glympse/android/core/GArray;

    move-result-object v4

    .line 818
    invoke-interface {v4}, Lcom/glympse/android/core/GArray;->length()I

    move-result v5

    move v2, v0

    .line 819
    :goto_1
    if-ge v2, v5, :cond_2

    .line 821
    invoke-interface {v4, v2}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GInvite;

    .line 822
    invoke-interface {v0, v3}, Lcom/glympse/android/api/GInvite;->applyBrand(Ljava/lang/String;)V

    .line 819
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 827
    :cond_2
    check-cast p1, Lcom/glympse/android/lib/GTicketPrivate;

    .line 828
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->lX:Lcom/glympse/android/lib/GCorrectedTime;

    invoke-interface {v0}, Lcom/glympse/android/lib/GCorrectedTime;->getTime()J

    move-result-wide v2

    .line 829
    invoke-interface {p1, v2, v3}, Lcom/glympse/android/lib/GTicketPrivate;->setStartTime(J)V

    .line 834
    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getDurationRaw()I

    move-result v0

    .line 835
    int-to-long v4, v0

    add-long/2addr v2, v4

    invoke-interface {p1, v2, v3, v1}, Lcom/glympse/android/lib/GTicketPrivate;->setExpireTime(JZ)V

    .line 838
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Lcom/glympse/android/lib/GTicketPrivate;->setState(I)Z

    .line 840
    invoke-virtual {p0, p1}, Lcom/glympse/android/lib/cm;->addTicket(Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 843
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getLocationManager()Lcom/glympse/android/api/GLocationManager;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GLocationManagerPrivate;

    .line 844
    iget-object v2, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v2}, Lcom/glympse/android/lib/GGlympsePrivate;->isSharing()Z

    move-result v2

    invoke-interface {v0, v2}, Lcom/glympse/android/lib/GLocationManagerPrivate;->startStopLocation(Z)V

    .line 847
    new-instance v0, Lcom/glympse/android/lib/fg;

    iget-object v2, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct {v0, v2, p1}, Lcom/glympse/android/lib/fg;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 848
    iget-object v2, p0, Lcom/glympse/android/lib/cm;->lI:Lcom/glympse/android/lib/GServerPost;

    invoke-interface {v2, v0, v1}, Lcom/glympse/android/lib/GServerPost;->invokeEndpoint(Lcom/glympse/android/lib/GApiEndpoint;Z)V

    .line 851
    iget-boolean v2, p0, Lcom/glympse/android/lib/cm;->nx:Z

    if-eqz v2, :cond_3

    .line 854
    iget-object v2, p0, Lcom/glympse/android/lib/cm;->lI:Lcom/glympse/android/lib/GServerPost;

    iget v3, p0, Lcom/glympse/android/lib/cm;->ny:I

    invoke-interface {v2, v0, v3}, Lcom/glympse/android/lib/GServerPost;->cancelEndpoint(Lcom/glympse/android/lib/GApiEndpoint;I)Z

    .line 858
    :cond_3
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GConfigPrivate;->setTicketSent(Z)V

    move v0, v1

    .line 860
    goto :goto_0
.end method

.method public sendTicketPhase2(Lcom/glympse/android/lib/GTicketPrivate;ILcom/glympse/android/hal/GHashtable;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/glympse/android/lib/GTicketPrivate;",
            "I",
            "Lcom/glympse/android/hal/GHashtable",
            "<",
            "Ljava/lang/Long;",
            "Lcom/glympse/android/hal/GHashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/glympse/android/api/GDataRow;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 870
    invoke-interface/range {p1 .. p1}, Lcom/glympse/android/lib/GTicketPrivate;->getDurationRaw()I

    move-result v2

    move/from16 v0, p2

    if-eq v2, v0, :cond_0

    .line 873
    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-interface {v0, v1, v2, v3}, Lcom/glympse/android/lib/GTicketPrivate;->modify(ILjava/lang/String;Lcom/glympse/android/api/GPlace;)Z

    .line 879
    :cond_0
    new-instance v13, Lcom/glympse/android/lib/Primitive;

    const/4 v2, 0x1

    invoke-direct {v13, v2}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 883
    invoke-interface/range {p1 .. p1}, Lcom/glympse/android/lib/GTicketPrivate;->getStartTime()J

    move-result-wide v4

    .line 886
    invoke-interface/range {p1 .. p1}, Lcom/glympse/android/lib/GTicketPrivate;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 887
    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 889
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/glympse/android/lib/cm;->lJ:Lcom/glympse/android/lib/GTicketProtocol;

    invoke-interface {v3, v4, v5, v2}, Lcom/glympse/android/lib/GTicketProtocol;->prepareMessageProperty(JLjava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v2

    invoke-interface {v13, v2}, Lcom/glympse/android/core/GPrimitive;->put(Lcom/glympse/android/core/GPrimitive;)V

    .line 893
    :cond_1
    invoke-interface/range {p1 .. p1}, Lcom/glympse/android/lib/GTicketPrivate;->getDestination()Lcom/glympse/android/api/GPlace;

    move-result-object v2

    .line 894
    if-eqz v2, :cond_2

    .line 896
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/glympse/android/lib/cm;->lJ:Lcom/glympse/android/lib/GTicketProtocol;

    invoke-interface {v3, v4, v5, v2}, Lcom/glympse/android/lib/GTicketProtocol;->preparePlaceProperty(JLcom/glympse/android/api/GPlace;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v2

    invoke-interface {v13, v2}, Lcom/glympse/android/core/GPrimitive;->put(Lcom/glympse/android/core/GPrimitive;)V

    .line 900
    :cond_2
    invoke-interface/range {p1 .. p1}, Lcom/glympse/android/lib/GTicketPrivate;->getEtaRaw()J

    move-result-wide v8

    .line 901
    const-wide/16 v2, 0x0

    cmp-long v2, v2, v8

    if-eqz v2, :cond_3

    .line 903
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/glympse/android/lib/cm;->lJ:Lcom/glympse/android/lib/GTicketProtocol;

    invoke-interface/range {p1 .. p1}, Lcom/glympse/android/lib/GTicketPrivate;->getEtaTs()J

    move-result-wide v6

    invoke-interface/range {v3 .. v9}, Lcom/glympse/android/lib/GTicketProtocol;->prepareEtaProperty(JJJ)Lcom/glympse/android/core/GPrimitive;

    move-result-object v2

    invoke-interface {v13, v2}, Lcom/glympse/android/core/GPrimitive;->put(Lcom/glympse/android/core/GPrimitive;)V

    .line 905
    :cond_3
    invoke-interface/range {p1 .. p1}, Lcom/glympse/android/lib/GTicketPrivate;->getRoute()Lcom/glympse/android/api/GTrack;

    move-result-object v2

    .line 906
    if-eqz v2, :cond_4

    .line 908
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/glympse/android/lib/cm;->lJ:Lcom/glympse/android/lib/GTicketProtocol;

    invoke-interface {v3, v4, v5, v2}, Lcom/glympse/android/lib/GTicketProtocol;->prepareRouteProperty(JLcom/glympse/android/api/GTrack;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v2

    invoke-interface {v13, v2}, Lcom/glympse/android/core/GPrimitive;->put(Lcom/glympse/android/core/GPrimitive;)V

    .line 912
    :cond_4
    invoke-interface/range {p1 .. p1}, Lcom/glympse/android/lib/GTicketPrivate;->getTravelMode()Lcom/glympse/android/api/GTravelMode;

    move-result-object v2

    .line 913
    if-eqz v2, :cond_5

    .line 915
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/glympse/android/lib/cm;->lJ:Lcom/glympse/android/lib/GTicketProtocol;

    invoke-interface {v3, v4, v5, v2}, Lcom/glympse/android/lib/GTicketProtocol;->prepareTravelModeProperty(JLcom/glympse/android/api/GTravelMode;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v2

    invoke-interface {v13, v2}, Lcom/glympse/android/core/GPrimitive;->put(Lcom/glympse/android/core/GPrimitive;)V

    .line 922
    :cond_5
    invoke-virtual/range {p3 .. p3}, Lcom/glympse/android/hal/GHashtable;->keys()Ljava/util/Enumeration;

    move-result-object v14

    :cond_6
    invoke-interface {v14}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 924
    invoke-interface {v14}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 925
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v10, v3

    check-cast v10, Lcom/glympse/android/hal/GHashtable;

    .line 927
    invoke-virtual {v10}, Lcom/glympse/android/hal/GHashtable;->keys()Ljava/util/Enumeration;

    move-result-object v15

    :goto_0
    invoke-interface {v15}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 929
    invoke-interface {v15}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    move-object v11, v3

    check-cast v11, Ljava/lang/String;

    .line 930
    invoke-virtual {v10, v11}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v12, v3

    check-cast v12, Lcom/glympse/android/api/GDataRow;

    .line 933
    new-instance v3, Lcom/glympse/android/lib/aj;

    invoke-interface {v12}, Lcom/glympse/android/api/GDataRow;->getPartnerId()J

    move-result-wide v6

    invoke-interface {v12}, Lcom/glympse/android/api/GDataRow;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v12}, Lcom/glympse/android/api/GDataRow;->getValue()Lcom/glympse/android/core/GPrimitive;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, Lcom/glympse/android/lib/aj;-><init>(JJLjava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Lcom/glympse/android/lib/GTicketPrivate;->setPropertyData(Lcom/glympse/android/api/GDataRow;)V

    .line 938
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-nez v3, :cond_7

    const-string v3, "visibility"

    invoke-static {v3}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v11, v3}, Lcom/glympse/android/hal/Helpers;->safeEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 941
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/glympse/android/lib/cm;->lJ:Lcom/glympse/android/lib/GTicketProtocol;

    invoke-interface {v12}, Lcom/glympse/android/api/GDataRow;->getValue()Lcom/glympse/android/core/GPrimitive;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-interface {v3, v0, v6}, Lcom/glympse/android/lib/GTicketProtocol;->setVisibility(Lcom/glympse/android/lib/GTicketPrivate;Lcom/glympse/android/core/GPrimitive;)V

    goto :goto_0

    .line 946
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/glympse/android/lib/cm;->lJ:Lcom/glympse/android/lib/GTicketProtocol;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-interface {v12}, Lcom/glympse/android/api/GDataRow;->getValue()Lcom/glympse/android/core/GPrimitive;

    move-result-object v9

    move-object v8, v11

    invoke-interface/range {v3 .. v9}, Lcom/glympse/android/lib/GTicketProtocol;->prepareProperty(JJLjava/lang/String;Lcom/glympse/android/core/GPrimitive;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v3

    invoke-interface {v13, v3}, Lcom/glympse/android/core/GPrimitive;->put(Lcom/glympse/android/core/GPrimitive;)V

    goto :goto_0

    .line 951
    :cond_8
    invoke-interface {v13}, Lcom/glympse/android/core/GPrimitive;->size()I

    move-result v2

    if-lez v2, :cond_9

    .line 953
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/glympse/android/lib/cm;->lI:Lcom/glympse/android/lib/GServerPost;

    new-instance v3, Lcom/glympse/android/lib/ah;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface/range {p1 .. p1}, Lcom/glympse/android/lib/GTicketPrivate;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v6, v7, v13}, Lcom/glympse/android/lib/ah;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    const/4 v6, 0x0

    invoke-interface {v2, v3, v6}, Lcom/glympse/android/lib/GServerPost;->invokeEndpoint(Lcom/glympse/android/lib/GApiEndpoint;Z)V

    .line 956
    :cond_9
    invoke-interface/range {p1 .. p1}, Lcom/glympse/android/lib/GTicketPrivate;->getProviderId()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_a

    invoke-interface/range {p1 .. p1}, Lcom/glympse/android/lib/GTicketPrivate;->getProviderData()Lcom/glympse/android/core/GPrimitive;

    move-result-object v2

    if-eqz v2, :cond_a

    .line 958
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/glympse/android/lib/cm;->lI:Lcom/glympse/android/lib/GServerPost;

    new-instance v3, Lcom/glympse/android/lib/fk;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface/range {p1 .. p1}, Lcom/glympse/android/lib/GTicketPrivate;->getProviderId()Ljava/lang/String;

    move-result-object v5

    invoke-interface/range {p1 .. p1}, Lcom/glympse/android/lib/GTicketPrivate;->getProviderData()Lcom/glympse/android/core/GPrimitive;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-direct {v3, v4, v0, v5, v6}, Lcom/glympse/android/lib/fk;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/GTicketPrivate;Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Lcom/glympse/android/lib/GServerPost;->invokeEndpoint(Lcom/glympse/android/lib/GApiEndpoint;Z)V

    .line 1008
    :goto_1
    invoke-interface/range {p1 .. p1}, Lcom/glympse/android/lib/GTicketPrivate;->getInvites()Lcom/glympse/android/core/GArray;

    move-result-object v4

    .line 1009
    const/4 v2, 0x0

    move v3, v2

    :goto_2
    invoke-interface {v4}, Lcom/glympse/android/core/GArray;->length()I

    move-result v2

    if-ge v3, v2, :cond_d

    .line 1011
    invoke-interface {v4, v3}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/glympse/android/lib/GInvitePrivate;

    .line 1012
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/glympse/android/lib/cm;->lJ:Lcom/glympse/android/lib/GTicketProtocol;

    move-object/from16 v0, p1

    invoke-interface {v5, v0, v2}, Lcom/glympse/android/lib/GTicketProtocol;->addInvite(Lcom/glympse/android/lib/GTicketPrivate;Lcom/glympse/android/lib/GInvitePrivate;)V

    .line 1009
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 964
    :cond_a
    const v2, 0xea60

    invoke-interface/range {p1 .. p1}, Lcom/glympse/android/lib/GTicketPrivate;->getDuration()I

    move-result v3

    if-lt v2, v3, :cond_c

    .line 970
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v2}, Lcom/glympse/android/lib/GGlympsePrivate;->getLocationManager()Lcom/glympse/android/api/GLocationManager;

    move-result-object v2

    invoke-interface {v2}, Lcom/glympse/android/api/GLocationManager;->getLocation()Lcom/glympse/android/core/GLocation;

    move-result-object v2

    .line 971
    if-eqz v2, :cond_b

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/glympse/android/lib/cm;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v3}, Lcom/glympse/android/lib/GConfigPrivate;->isSharingLocation()Z

    move-result v3

    if-eqz v3, :cond_b

    .line 974
    invoke-interface {v2}, Lcom/glympse/android/core/GLocation;->clone()Lcom/glympse/android/core/GLocation;

    move-result-object v2

    check-cast v2, Lcom/glympse/android/lib/GLocationPrivate;

    .line 977
    invoke-interface {v2, v4, v5}, Lcom/glympse/android/lib/GLocationPrivate;->setTime(J)V

    .line 980
    new-instance v3, Lcom/glympse/android/hal/GLinkedList;

    invoke-direct {v3}, Lcom/glympse/android/hal/GLinkedList;-><init>()V

    .line 981
    invoke-virtual {v3, v2}, Lcom/glympse/android/hal/GLinkedList;->addLast(Ljava/lang/Object;)V

    .line 982
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/glympse/android/lib/cm;->lI:Lcom/glympse/android/lib/GServerPost;

    new-instance v5, Lcom/glympse/android/lib/dn;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    move-object/from16 v0, p1

    invoke-direct {v5, v6, v0, v3}, Lcom/glympse/android/lib/dn;-><init>(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/GTicketPrivate;Lcom/glympse/android/hal/GLinkedList;)V

    const/4 v3, 0x0

    invoke-interface {v4, v5, v3}, Lcom/glympse/android/lib/GServerPost;->invokeEndpoint(Lcom/glympse/android/lib/GApiEndpoint;Z)V

    .line 986
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v3}, Lcom/glympse/android/lib/GGlympsePrivate;->getUserManager()Lcom/glympse/android/api/GUserManager;

    move-result-object v3

    check-cast v3, Lcom/glympse/android/lib/GUserManagerPrivate;

    .line 987
    invoke-interface {v3}, Lcom/glympse/android/lib/GUserManagerPrivate;->getSelfTrack()Lcom/glympse/android/api/GTrack;

    move-result-object v4

    invoke-interface {v4}, Lcom/glympse/android/api/GTrack;->length()I

    move-result v4

    if-nez v4, :cond_b

    .line 989
    const/4 v4, 0x1

    const/4 v5, 0x1

    invoke-interface {v3, v2, v4, v5}, Lcom/glympse/android/lib/GUserManagerPrivate;->setSelfLocation(Lcom/glympse/android/core/GLocation;ZZ)V

    .line 1004
    :cond_b
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/glympse/android/lib/cm;->lJ:Lcom/glympse/android/lib/GTicketProtocol;

    invoke-interface {v2}, Lcom/glympse/android/lib/GTicketProtocol;->refreshInvites()V

    goto/16 :goto_1

    .line 999
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v2}, Lcom/glympse/android/lib/GGlympsePrivate;->getLocationManager()Lcom/glympse/android/api/GLocationManager;

    move-result-object v2

    check-cast v2, Lcom/glympse/android/lib/GLocationManagerPrivate;

    .line 1000
    invoke-interface {v2}, Lcom/glympse/android/lib/GLocationManagerPrivate;->restartProvider()V

    goto :goto_3

    .line 1016
    :cond_d
    invoke-direct/range {p0 .. p1}, Lcom/glympse/android/lib/cm;->h(Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 1019
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/4 v3, 0x4

    const/4 v4, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, p1

    invoke-interface {v0, v2, v3, v4, v1}, Lcom/glympse/android/lib/GTicketPrivate;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 1020
    return-void
.end method

.method public setActive(Z)V
    .locals 2

    .prologue
    .line 421
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->nn:Lcom/glympse/android/lib/cn;

    iget-object v1, p0, Lcom/glympse/android/lib/cm;->np:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, p1, v1}, Lcom/glympse/android/lib/cn;->a(ZLcom/glympse/android/hal/GVector;)V

    .line 424
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->np:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->elements()Ljava/util/Enumeration;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 426
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GTicketPrivate;

    .line 427
    invoke-interface {v0, p1}, Lcom/glympse/android/lib/GTicketPrivate;->setActive(Z)V

    goto :goto_0

    .line 429
    :cond_0
    return-void
.end method

.method public setCancellationTimeout(I)V
    .locals 0

    .prologue
    .line 279
    iput p1, p0, Lcom/glympse/android/lib/cm;->ny:I

    .line 280
    return-void
.end method

.method public setExpirationMode(I)V
    .locals 1

    .prologue
    .line 290
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/glympse/android/lib/cm;->no:Z

    if-eqz v0, :cond_1

    .line 311
    :cond_0
    :goto_0
    return-void

    .line 296
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 310
    :pswitch_0
    iput p1, p0, Lcom/glympse/android/lib/cm;->nz:I

    goto :goto_0

    .line 296
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public setLastViewTime(J)V
    .locals 3

    .prologue
    .line 473
    iget-wide v0, p0, Lcom/glympse/android/lib/cm;->ns:J

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 475
    invoke-direct {p0, p1, p2}, Lcom/glympse/android/lib/cm;->h(J)V

    .line 477
    :cond_0
    return-void
.end method

.method public setXoAListener(Lcom/glympse/android/api/GXoAListener;)V
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->nC:Lcom/glympse/android/lib/go;

    if-nez v0, :cond_0

    .line 326
    :goto_0
    return-void

    .line 325
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->nC:Lcom/glympse/android/lib/go;

    invoke-virtual {v0, p1}, Lcom/glympse/android/lib/go;->setXoAListener(Lcom/glympse/android/api/GXoAListener;)V

    goto :goto_0
.end method

.method public simulateAddedEvents(Lcom/glympse/android/api/GEventListener;)V
    .locals 6

    .prologue
    .line 197
    iget-boolean v0, p0, Lcom/glympse/android/lib/cm;->no:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/glympse/android/lib/cm;->nw:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-nez v0, :cond_1

    .line 213
    :cond_0
    return-void

    .line 203
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->np:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v2

    .line 204
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 206
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->np:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GTicket;

    .line 207
    invoke-interface {v0}, Lcom/glympse/android/api/GTicket;->getState()I

    move-result v3

    and-int/lit8 v3, v3, 0x12

    if-eqz v3, :cond_0

    .line 211
    iget-object v3, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/4 v4, 0x1

    const/high16 v5, 0x20000

    invoke-interface {p1, v3, v4, v5, v0}, Lcom/glympse/android/api/GEventListener;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 204
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public start(Lcom/glympse/android/lib/GGlympsePrivate;)V
    .locals 2

    .prologue
    .line 375
    iput-object p1, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 376
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getServerPost()Lcom/glympse/android/lib/GServerPost;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/cm;->lI:Lcom/glympse/android/lib/GServerPost;

    .line 377
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getTicketProtocol()Lcom/glympse/android/lib/GTicketProtocol;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/cm;->lJ:Lcom/glympse/android/lib/GTicketProtocol;

    .line 378
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getConfig()Lcom/glympse/android/api/GConfig;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GConfigPrivate;

    iput-object v0, p0, Lcom/glympse/android/lib/cm;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    .line 379
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getCorrectedTime()Lcom/glympse/android/lib/GCorrectedTime;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/cm;->lX:Lcom/glympse/android/lib/GCorrectedTime;

    .line 382
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getContextHolder()Lcom/glympse/android/hal/GContextHolder;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/hal/GContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->getPrefix()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/glympse/android/hal/HalFactory;->openSharedPreferences(Landroid/content/Context;Ljava/lang/String;)Lcom/glympse/android/hal/GSharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/cm;->nB:Lcom/glympse/android/hal/GSharedPreferences;

    .line 384
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->nn:Lcom/glympse/android/lib/cn;

    invoke-virtual {v0, p1}, Lcom/glympse/android/lib/cn;->start(Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 387
    invoke-direct {p0}, Lcom/glympse/android/lib/cm;->ce()V

    .line 388
    return-void
.end method

.method public stop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 393
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->nn:Lcom/glympse/android/lib/cn;

    iget-object v1, p0, Lcom/glympse/android/lib/cm;->np:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, v1}, Lcom/glympse/android/lib/cn;->d(Lcom/glympse/android/hal/GVector;)V

    .line 397
    :goto_0
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->np:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 399
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->np:Lcom/glympse/android/hal/GVector;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GTicketPrivate;

    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/cm;->removeTicket(Lcom/glympse/android/lib/GTicketPrivate;)V

    goto :goto_0

    .line 403
    :cond_0
    invoke-direct {p0}, Lcom/glympse/android/lib/cm;->cf()V

    .line 406
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/glympse/android/lib/cm;->i(J)V

    .line 409
    iput-object v2, p0, Lcom/glympse/android/lib/cm;->nn:Lcom/glympse/android/lib/cn;

    .line 410
    iput-object v2, p0, Lcom/glympse/android/lib/cm;->nB:Lcom/glympse/android/hal/GSharedPreferences;

    .line 411
    iput-object v2, p0, Lcom/glympse/android/lib/cm;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    .line 412
    iput-object v2, p0, Lcom/glympse/android/lib/cm;->lI:Lcom/glympse/android/lib/GServerPost;

    .line 413
    iput-object v2, p0, Lcom/glympse/android/lib/cm;->lJ:Lcom/glympse/android/lib/GTicketProtocol;

    .line 414
    iput-object v2, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 416
    return-void
.end method

.method public syncFresh()V
    .locals 1

    .prologue
    .line 523
    invoke-virtual {p0}, Lcom/glympse/android/lib/cm;->isSynced()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 536
    :goto_0
    return-void

    .line 528
    :cond_0
    invoke-direct {p0}, Lcom/glympse/android/lib/cm;->ca()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 530
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/cm;->g(Z)V

    goto :goto_0

    .line 534
    :cond_1
    invoke-direct {p0}, Lcom/glympse/android/lib/cm;->cb()V

    goto :goto_0
.end method

.method public syncRefresh()V
    .locals 1

    .prologue
    .line 540
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/cm;->g(Z)V

    .line 541
    return-void
.end method

.method public syncedWithServer(Lcom/glympse/android/lib/InternalStructs$a;Z)V
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/4 v4, 0x0

    .line 587
    iget-boolean v5, p0, Lcom/glympse/android/lib/cm;->no:Z

    .line 590
    iget-boolean v0, p0, Lcom/glympse/android/lib/cm;->no:Z

    if-eqz v0, :cond_a

    .line 597
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->np:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v3

    move v2, v4

    .line 598
    :goto_0
    if-ge v2, v3, :cond_1

    .line 600
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->np:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, v2}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GTicketPrivate;

    .line 601
    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketPrivate;->getId()Ljava/lang/String;

    move-result-object v6

    .line 604
    invoke-static {v6}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v0, v2

    move v1, v3

    .line 598
    :goto_1
    add-int/lit8 v2, v0, 0x1

    move v3, v1

    goto :goto_0

    .line 610
    :cond_0
    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketPrivate;->getExpireTime()J

    move-result-wide v8

    iget-wide v10, p1, Lcom/glympse/android/lib/InternalStructs$a;->or:J

    cmp-long v1, v8, v10

    if-gez v1, :cond_3

    .line 638
    :cond_1
    iget-object v0, p1, Lcom/glympse/android/lib/InternalStructs$a;->jE:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GHashtable;->keys()Ljava/util/Enumeration;

    move-result-object v1

    :cond_2
    :goto_2
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 640
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 641
    iget-object v2, p1, Lcom/glympse/android/lib/InternalStructs$a;->jE:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v2, v0}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GTicketPrivate;

    .line 644
    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketPrivate;->isSibling()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 647
    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/cm;->addTicket(Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 650
    iget-object v2, p0, Lcom/glympse/android/lib/cm;->lJ:Lcom/glympse/android/lib/GTicketProtocol;

    invoke-interface {v2, v0, v4}, Lcom/glympse/android/lib/GTicketProtocol;->refreshTicket(Lcom/glympse/android/lib/GTicketPrivate;Z)V

    goto :goto_2

    .line 618
    :cond_3
    iget-object v1, p1, Lcom/glympse/android/lib/InternalStructs$a;->jE:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v1, v6}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/glympse/android/lib/GTicketPrivate;

    .line 619
    if-eqz v1, :cond_4

    .line 622
    iget-object v7, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0, v1, v7, p2, p2}, Lcom/glympse/android/lib/GTicketPrivate;->merge(Lcom/glympse/android/lib/GTicketPrivate;Lcom/glympse/android/lib/GGlympsePrivate;ZZ)V

    .line 625
    iget-object v0, p1, Lcom/glympse/android/lib/InternalStructs$a;->jE:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, v6}, Lcom/glympse/android/hal/GHashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v2

    move v1, v3

    goto :goto_1

    .line 630
    :cond_4
    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/cm;->removeTicket(Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 631
    add-int/lit8 v0, v2, -0x1

    .line 632
    add-int/lit8 v1, v3, -0x1

    goto :goto_1

    .line 655
    :cond_5
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->okToPost()Z

    .line 707
    :goto_3
    iget-wide v0, p1, Lcom/glympse/android/lib/InternalStructs$a;->ns:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_6

    .line 711
    iget-wide v0, p1, Lcom/glympse/android/lib/InternalStructs$a;->ns:J

    invoke-direct {p0, v0, v1}, Lcom/glympse/android/lib/cm;->h(J)V

    .line 715
    :cond_6
    invoke-virtual {p0}, Lcom/glympse/android/lib/cm;->orderChanged()V

    .line 718
    if-nez v5, :cond_9

    .line 721
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-static {v0}, Lcom/glympse/android/lib/cs;->a(Lcom/glympse/android/api/GGlympse;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-static {v0}, Lcom/glympse/android/lib/cs;->b(Lcom/glympse/android/api/GGlympse;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 724
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getContextHolder()Lcom/glympse/android/hal/GContextHolder;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/hal/GContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/glympse/android/hal/HalFactory;->createUserProfile(Landroid/content/Context;)Lcom/glympse/android/hal/GUserProfile;

    move-result-object v0

    .line 725
    invoke-interface {v0}, Lcom/glympse/android/hal/GUserProfile;->fetch()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 728
    new-instance v1, Lcom/glympse/android/lib/cs;

    invoke-direct {v1}, Lcom/glympse/android/lib/cs;-><init>()V

    .line 729
    iget-object v2, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/hal/GUserProfile;->getName()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    invoke-interface {v0}, Lcom/glympse/android/hal/GUserProfile;->getAvatar()Lcom/glympse/android/core/GDrawable;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v5, v0}, Lcom/glympse/android/lib/cs;->a(Lcom/glympse/android/api/GGlympse;Ljava/lang/String;Ljava/lang/String;Lcom/glympse/android/core/GDrawable;)Z

    .line 734
    :cond_7
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->nn:Lcom/glympse/android/lib/cn;

    iget-object v1, p0, Lcom/glympse/android/lib/cm;->np:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, v1}, Lcom/glympse/android/lib/cn;->e(Lcom/glympse/android/hal/GVector;)V

    .line 736
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->np:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v0

    if-nez v0, :cond_f

    .line 740
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->gf:Lcom/glympse/android/lib/GConfigPrivate;

    invoke-interface {v0, v4}, Lcom/glympse/android/lib/GConfigPrivate;->setTicketSent(Z)V

    .line 753
    :cond_8
    :goto_4
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getServerPost()Lcom/glympse/android/lib/GServerPost;

    move-result-object v0

    const/16 v1, 0x80

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GServerPost;->rememberEvents(I)V

    .line 755
    :cond_9
    return-void

    .line 662
    :cond_a
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getDirectionsManager()Lcom/glympse/android/api/GDirectionsManager;

    .line 665
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->nn:Lcom/glympse/android/lib/cn;

    iget-object v0, v0, Lcom/glympse/android/lib/cn;->nH:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->elements()Ljava/util/Enumeration;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 667
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GTicketPrivate;

    .line 671
    invoke-direct {p0, v0, v4}, Lcom/glympse/android/lib/cm;->a(Lcom/glympse/android/lib/GTicketPrivate;Z)V

    goto :goto_5

    .line 675
    :cond_b
    iget-object v0, p1, Lcom/glympse/android/lib/InternalStructs$a;->np:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->elements()Ljava/util/Enumeration;

    move-result-object v2

    :cond_c
    :goto_6
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 677
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GTicketPrivate;

    .line 678
    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketPrivate;->getId()Ljava/lang/String;

    move-result-object v1

    .line 682
    invoke-direct {p0, v1}, Lcom/glympse/android/lib/cm;->n(Ljava/lang/String;)Lcom/glympse/android/api/GTicket;

    move-result-object v1

    check-cast v1, Lcom/glympse/android/lib/GTicketPrivate;

    .line 683
    if-nez v1, :cond_d

    .line 686
    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/cm;->addTicket(Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 695
    :goto_7
    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketPrivate;->isActive()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 697
    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketPrivate;->updateWatchingState()Z

    goto :goto_6

    .line 691
    :cond_d
    iget-object v3, p0, Lcom/glympse/android/lib/cm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1, v0, v3, v4, v4}, Lcom/glympse/android/lib/GTicketPrivate;->merge(Lcom/glympse/android/lib/GTicketPrivate;Lcom/glympse/android/lib/GGlympsePrivate;ZZ)V

    goto :goto_7

    .line 702
    :cond_e
    iput-boolean v12, p0, Lcom/glympse/android/lib/cm;->no:Z

    goto/16 :goto_3

    .line 745
    :cond_f
    invoke-virtual {p0, v4, v12}, Lcom/glympse/android/lib/cm;->anyActive(ZZ)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 747
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->lJ:Lcom/glympse/android/lib/GTicketProtocol;

    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketProtocol;->refreshInvites()V

    goto :goto_4
.end method

.method public triggerXoAUpdate()V
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->nC:Lcom/glympse/android/lib/go;

    if-nez v0, :cond_0

    .line 336
    :goto_0
    return-void

    .line 335
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->nC:Lcom/glympse/android/lib/go;

    invoke-virtual {v0}, Lcom/glympse/android/lib/go;->triggerXoAUpdate()V

    goto :goto_0
.end method

.method public updateState(J)V
    .locals 7

    .prologue
    .line 448
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->np:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v2

    .line 449
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 451
    iget-object v0, p0, Lcom/glympse/android/lib/cm;->np:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GTicketPrivate;

    .line 452
    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketPrivate;->getState()I

    move-result v3

    .line 455
    const/16 v4, 0x40

    if-ne v4, v3, :cond_1

    .line 469
    :cond_0
    return-void

    .line 461
    :cond_1
    invoke-interface {v0, p1, p2}, Lcom/glympse/android/lib/GTicketPrivate;->updateState(J)Z

    .line 464
    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketPrivate;->getExpireTime()J

    move-result-wide v4

    cmp-long v3, v4, p1

    if-gez v3, :cond_2

    const/16 v3, 0x10

    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketPrivate;->getState()I

    move-result v0

    if-ne v3, v0, :cond_2

    .line 466
    const/4 v0, 0x4

    const-string v3, "[HistoryManager.updateState] Invalid ticket state"

    invoke-static {v0, v3}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 449
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method
