.class Lcom/glympse/android/lib/n$a;
.super Ljava/lang/Object;
.source "AutoProxArrivalWrapper.java"

# interfaces
.implements Lcom/glympse/android/api/GEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/lib/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private cS:Lcom/glympse/android/api/GGlympse;

.field private hn:Lcom/glympse/android/lib/n;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/glympse/android/lib/n$1;)V
    .locals 0

    .prologue
    .line 155
    invoke-direct {p0}, Lcom/glympse/android/lib/n$a;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/glympse/android/lib/n;Lcom/glympse/android/api/GGlympse;)V
    .locals 2

    .prologue
    .line 162
    iput-object p1, p0, Lcom/glympse/android/lib/n$a;->hn:Lcom/glympse/android/lib/n;

    .line 163
    iput-object p2, p0, Lcom/glympse/android/lib/n$a;->cS:Lcom/glympse/android/api/GGlympse;

    .line 165
    iget-object v0, p0, Lcom/glympse/android/lib/n$a;->cS:Lcom/glympse/android/api/GGlympse;

    invoke-virtual {p0}, Lcom/glympse/android/lib/n$a;->ar()Lcom/glympse/android/api/GEventListener;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/api/GGlympse;->addListener(Lcom/glympse/android/api/GEventListener;)Z

    .line 166
    return-void
.end method

.method protected ar()Lcom/glympse/android/api/GEventListener;
    .locals 1

    .prologue
    .line 187
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GEventListener;

    return-object v0
.end method

.method public eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 175
    const/4 v0, 0x1

    if-ne v0, p2, :cond_0

    .line 177
    and-int/lit16 v0, p3, 0x400

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/glympse/android/lib/n$a;->cS:Lcom/glympse/android/api/GGlympse;

    invoke-interface {v0}, Lcom/glympse/android/api/GGlympse;->getUserManager()Lcom/glympse/android/api/GUserManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/api/GUserManager;->getSelf()Lcom/glympse/android/api/GUser;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/api/GUser;->getLocation()Lcom/glympse/android/core/GLocation;

    move-result-object v0

    .line 180
    iget-object v1, p0, Lcom/glympse/android/lib/n$a;->hn:Lcom/glympse/android/lib/n;

    invoke-virtual {v1, v0}, Lcom/glympse/android/lib/n;->locationChanged(Lcom/glympse/android/core/GLocation;)V

    .line 183
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 170
    iget-object v0, p0, Lcom/glympse/android/lib/n$a;->cS:Lcom/glympse/android/api/GGlympse;

    invoke-virtual {p0}, Lcom/glympse/android/lib/n$a;->ar()Lcom/glympse/android/api/GEventListener;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/api/GGlympse;->removeListener(Lcom/glympse/android/api/GEventListener;)Z

    .line 171
    return-void
.end method
