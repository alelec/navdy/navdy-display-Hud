.class Lcom/glympse/android/lib/au;
.super Lcom/glympse/android/lib/HttpJob;
.source "EndpointJob.java"


# instance fields
.field private f:Ljava/lang/String;

.field private jy:Z

.field private kH:Lcom/glympse/android/lib/GApiEndpoint;

.field private kI:Ljava/lang/String;


# direct methods
.method public constructor <init>(ZLjava/lang/String;Lcom/glympse/android/lib/GApiEndpoint;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/glympse/android/lib/HttpJob;-><init>()V

    .line 29
    iput-boolean p1, p0, Lcom/glympse/android/lib/au;->jy:Z

    .line 30
    iput-object p2, p0, Lcom/glympse/android/lib/au;->f:Ljava/lang/String;

    .line 31
    iput-object p3, p0, Lcom/glympse/android/lib/au;->kH:Lcom/glympse/android/lib/GApiEndpoint;

    .line 32
    return-void
.end method

.method private d(Z)V
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lcom/glympse/android/lib/au;->kH:Lcom/glympse/android/lib/GApiEndpoint;

    iget v1, p0, Lcom/glympse/android/lib/au;->_failures:I

    invoke-interface {v0, p1, v1}, Lcom/glympse/android/lib/GApiEndpoint;->shouldRetry(ZI)Z

    move-result v0

    if-nez v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/glympse/android/lib/au;->kH:Lcom/glympse/android/lib/GApiEndpoint;

    invoke-interface {v0}, Lcom/glympse/android/lib/GApiEndpoint;->cancel()V

    .line 130
    invoke-virtual {p0}, Lcom/glympse/android/lib/au;->abort()V

    .line 132
    :cond_0
    return-void
.end method


# virtual methods
.method public onComplete()V
    .locals 3

    .prologue
    .line 82
    invoke-super {p0}, Lcom/glympse/android/lib/HttpJob;->onComplete()V

    .line 84
    invoke-virtual {p0}, Lcom/glympse/android/lib/au;->isSucceeded()Z

    move-result v0

    .line 89
    if-nez v0, :cond_0

    .line 92
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/au;->d(Z)V

    .line 111
    :goto_0
    return-void

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/au;->kH:Lcom/glympse/android/lib/GApiEndpoint;

    invoke-interface {v0}, Lcom/glympse/android/lib/GApiEndpoint;->process()Z

    move-result v0

    .line 102
    if-eqz v0, :cond_1

    .line 104
    const/4 v0, 0x3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/glympse/android/lib/au;->kI:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " succeeded"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    goto :goto_0

    .line 108
    :cond_1
    const/4 v0, 0x4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/glympse/android/lib/au;->kI:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " failed with error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/glympse/android/lib/au;->kH:Lcom/glympse/android/lib/GApiEndpoint;

    .line 109
    invoke-interface {v2}, Lcom/glympse/android/lib/GApiEndpoint;->getError()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " details: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/glympse/android/lib/au;->kH:Lcom/glympse/android/lib/GApiEndpoint;

    invoke-interface {v2}, Lcom/glympse/android/lib/GApiEndpoint;->getErrorDetail()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 108
    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public onPreProcess()V
    .locals 5

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/glympse/android/lib/au;->jy:Z

    iget-object v1, p0, Lcom/glympse/android/lib/au;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/glympse/android/lib/au;->kH:Lcom/glympse/android/lib/GApiEndpoint;

    invoke-static {v0, v1, v2}, Lcom/glympse/android/lib/e;->a(ZLjava/lang/String;Lcom/glympse/android/lib/GApiEndpoint;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/au;->kI:Ljava/lang/String;

    .line 38
    iget-object v0, p0, Lcom/glympse/android/lib/au;->_httpConnection:Lcom/glympse/android/hal/GHttpConnection;

    iget-object v1, p0, Lcom/glympse/android/lib/au;->kI:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/glympse/android/hal/GHttpConnection;->setUrl(Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lcom/glympse/android/lib/au;->kH:Lcom/glympse/android/lib/GApiEndpoint;

    invoke-interface {v0}, Lcom/glympse/android/lib/GApiEndpoint;->post()Ljava/lang/String;

    move-result-object v0

    .line 41
    iget-object v1, p0, Lcom/glympse/android/lib/au;->kH:Lcom/glympse/android/lib/GApiEndpoint;

    invoke-interface {v1}, Lcom/glympse/android/lib/GApiEndpoint;->methodType()I

    move-result v1

    invoke-static {v1, v0}, Lcom/glympse/android/lib/au;->pickMethod(ILjava/lang/String;)I

    move-result v1

    .line 44
    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 46
    iget-object v2, p0, Lcom/glympse/android/lib/au;->_httpConnection:Lcom/glympse/android/hal/GHttpConnection;

    const-string v3, "Content-Type"

    invoke-static {v3}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "application/json"

    invoke-static {v4}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/glympse/android/hal/GHttpConnection;->setRequestHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    :cond_0
    iget-object v2, p0, Lcom/glympse/android/lib/au;->_httpConnection:Lcom/glympse/android/hal/GHttpConnection;

    invoke-interface {v2, v1}, Lcom/glympse/android/hal/GHttpConnection;->setRequestMethod(I)V

    .line 49
    iget-object v1, p0, Lcom/glympse/android/lib/au;->_httpConnection:Lcom/glympse/android/hal/GHttpConnection;

    invoke-interface {v1, v0}, Lcom/glympse/android/hal/GHttpConnection;->setRequestData(Ljava/lang/String;)V

    .line 52
    invoke-static {v0}, Lcom/glympse/android/lib/Debug;->dumpPackets(Ljava/lang/String;)V

    .line 53
    return-void
.end method

.method public onProcessResponse()V
    .locals 3

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/glympse/android/lib/au;->isSucceeded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 78
    :goto_0
    return-void

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/au;->_httpConnection:Lcom/glympse/android/hal/GHttpConnection;

    invoke-interface {v0}, Lcom/glympse/android/hal/GHttpConnection;->getResponseDataString()Ljava/lang/String;

    move-result-object v0

    .line 67
    invoke-static {v0}, Lcom/glympse/android/lib/Debug;->dumpPackets(Ljava/lang/String;)V

    .line 70
    new-instance v1, Lcom/glympse/android/lib/json/JsonParser;

    invoke-direct {v1}, Lcom/glympse/android/lib/json/JsonParser;-><init>()V

    .line 71
    iget-object v2, p0, Lcom/glympse/android/lib/au;->kH:Lcom/glympse/android/lib/GApiEndpoint;

    invoke-interface {v2, v1}, Lcom/glympse/android/lib/GApiEndpoint;->getHandler(Lcom/glympse/android/lib/json/GJsonHandlerStack;)Lcom/glympse/android/lib/json/GJsonHandler;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/glympse/android/lib/json/GJsonParser;->pushHandler(Lcom/glympse/android/lib/json/GJsonHandler;)V

    .line 72
    invoke-interface {v1, v0}, Lcom/glympse/android/lib/json/GJsonParser;->parse(Ljava/lang/String;)Z

    move-result v0

    .line 73
    invoke-interface {v1}, Lcom/glympse/android/lib/json/GJsonParser;->clearStack()V

    .line 77
    iput-boolean v0, p0, Lcom/glympse/android/lib/au;->_success:Z

    goto :goto_0
.end method

.method public onRetry()V
    .locals 1

    .prologue
    .line 115
    invoke-super {p0}, Lcom/glympse/android/lib/HttpJob;->onRetry()V

    .line 118
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/au;->d(Z)V

    .line 119
    return-void
.end method
