.class Lcom/glympse/android/lib/am;
.super Ljava/lang/Object;
.source "Directions.java"

# interfaces
.implements Lcom/glympse/android/lib/GDirectionsPrivate;


# instance fields
.field private cS:Lcom/glympse/android/api/GGlympse;

.field private cg:I

.field private he:Lcom/glympse/android/lib/CommonSink;

.field private kc:J

.field private kd:Lcom/glympse/android/core/GLatLng;

.field private ke:Lcom/glympse/android/core/GLatLng;

.field private kf:I

.field private kg:J

.field private kh:J

.field private ki:Lcom/glympse/android/api/GTrack;


# direct methods
.method public constructor <init>(JLcom/glympse/android/core/GLatLng;Lcom/glympse/android/core/GLatLng;I)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const/4 v0, 0x0

    iput v0, p0, Lcom/glympse/android/lib/am;->cg:I

    .line 44
    iput-wide p1, p0, Lcom/glympse/android/lib/am;->kc:J

    .line 45
    iput-object p3, p0, Lcom/glympse/android/lib/am;->kd:Lcom/glympse/android/core/GLatLng;

    .line 46
    iput-object p4, p0, Lcom/glympse/android/lib/am;->ke:Lcom/glympse/android/core/GLatLng;

    .line 47
    iput p5, p0, Lcom/glympse/android/lib/am;->kf:I

    .line 49
    iput-wide v2, p0, Lcom/glympse/android/lib/am;->kg:J

    .line 50
    iput-wide v2, p0, Lcom/glympse/android/lib/am;->kh:J

    .line 52
    new-instance v0, Lcom/glympse/android/lib/CommonSink;

    const-string v1, "Directions"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/CommonSink;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/glympse/android/lib/am;->he:Lcom/glympse/android/lib/CommonSink;

    .line 53
    return-void
.end method

.method private aY()V
    .locals 4

    .prologue
    .line 133
    iget-object v0, p0, Lcom/glympse/android/lib/am;->cS:Lcom/glympse/android/api/GGlympse;

    if-nez v0, :cond_0

    .line 139
    :goto_0
    return-void

    .line 137
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/am;->cS:Lcom/glympse/android/api/GGlympse;

    const/16 v1, 0x13

    const/4 v2, 0x3

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/glympse/android/lib/am;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 138
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/am;->cS:Lcom/glympse/android/api/GGlympse;

    goto :goto_0
.end method

.method private aZ()V
    .locals 4

    .prologue
    .line 143
    iget-object v0, p0, Lcom/glympse/android/lib/am;->cS:Lcom/glympse/android/api/GGlympse;

    if-nez v0, :cond_0

    .line 149
    :goto_0
    return-void

    .line 147
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/am;->cS:Lcom/glympse/android/api/GGlympse;

    const/16 v1, 0x13

    const/4 v2, 0x2

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/glympse/android/lib/am;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 148
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/am;->cS:Lcom/glympse/android/api/GGlympse;

    goto :goto_0
.end method


# virtual methods
.method public addListener(Lcom/glympse/android/api/GEventListener;)Z
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/glympse/android/lib/am;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1}, Lcom/glympse/android/lib/CommonSink;->addListener(Lcom/glympse/android/api/GEventListener;)Z

    move-result v0

    return v0
.end method

.method public associateContext(JLjava/lang/Object;)V
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/glympse/android/lib/am;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1, p2, p3}, Lcom/glympse/android/lib/CommonSink;->associateContext(JLjava/lang/Object;)V

    .line 183
    return-void
.end method

.method public clearContext(J)V
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/glympse/android/lib/am;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1, p2}, Lcom/glympse/android/lib/CommonSink;->clearContext(J)V

    .line 193
    return-void
.end method

.method public deriveContext(Lcom/glympse/android/api/GEventSink;)V
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/glympse/android/lib/am;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1}, Lcom/glympse/android/lib/CommonSink;->deriveContext(Lcom/glympse/android/api/GEventSink;)V

    .line 203
    return-void
.end method

.method public eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V
    .locals 6

    .prologue
    .line 172
    iget-object v0, p0, Lcom/glympse/android/lib/am;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/glympse/android/api/GEventSink;

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/glympse/android/lib/CommonSink;->eventsOccurred(Lcom/glympse/android/api/GEventSink;Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 173
    return-void
.end method

.method public failed()V
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x3

    iput v0, p0, Lcom/glympse/android/lib/am;->cg:I

    .line 124
    invoke-direct {p0}, Lcom/glympse/android/lib/am;->aZ()V

    .line 125
    return-void
.end method

.method public fetchingStarted(Lcom/glympse/android/api/GGlympse;)V
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x1

    iput v0, p0, Lcom/glympse/android/lib/am;->cg:I

    .line 106
    iput-object p1, p0, Lcom/glympse/android/lib/am;->cS:Lcom/glympse/android/api/GGlympse;

    .line 107
    return-void
.end method

.method public getContext(J)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/glympse/android/lib/am;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1, p2}, Lcom/glympse/android/lib/CommonSink;->getContext(J)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getContextKeys()Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Enumeration",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 197
    iget-object v0, p0, Lcom/glympse/android/lib/am;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0}, Lcom/glympse/android/lib/CommonSink;->getContextKeys()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public getDestination()Lcom/glympse/android/core/GLatLng;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/glympse/android/lib/am;->ke:Lcom/glympse/android/core/GLatLng;

    return-object v0
.end method

.method public getEta()J
    .locals 2

    .prologue
    .line 91
    iget-wide v0, p0, Lcom/glympse/android/lib/am;->kh:J

    return-wide v0
.end method

.method public getEtaTs()J
    .locals 2

    .prologue
    .line 86
    iget-wide v0, p0, Lcom/glympse/android/lib/am;->kg:J

    return-wide v0
.end method

.method public getListeners()Lcom/glympse/android/core/GArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/glympse/android/core/GArray",
            "<",
            "Lcom/glympse/android/api/GEventListener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 167
    iget-object v0, p0, Lcom/glympse/android/lib/am;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0}, Lcom/glympse/android/lib/CommonSink;->getListeners()Lcom/glympse/android/core/GArray;

    move-result-object v0

    return-object v0
.end method

.method public getOrigin()Lcom/glympse/android/core/GLatLng;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/glympse/android/lib/am;->kd:Lcom/glympse/android/core/GLatLng;

    return-object v0
.end method

.method public getRequestTime()J
    .locals 2

    .prologue
    .line 66
    iget-wide v0, p0, Lcom/glympse/android/lib/am;->kc:J

    return-wide v0
.end method

.method public getState()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/glympse/android/lib/am;->cg:I

    return v0
.end method

.method public getTrack()Lcom/glympse/android/api/GTrack;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/glympse/android/lib/am;->ki:Lcom/glympse/android/api/GTrack;

    return-object v0
.end method

.method public getTravelMode()I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/glympse/android/lib/am;->kf:I

    return v0
.end method

.method public hasContext(J)Z
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/glympse/android/lib/am;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1, p2}, Lcom/glympse/android/lib/CommonSink;->hasContext(J)Z

    move-result v0

    return v0
.end method

.method public removeListener(Lcom/glympse/android/api/GEventListener;)Z
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/glympse/android/lib/am;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1}, Lcom/glympse/android/lib/CommonSink;->removeListener(Lcom/glympse/android/api/GEventListener;)Z

    move-result v0

    return v0
.end method

.method public set(JJLcom/glympse/android/api/GTrack;)V
    .locals 1

    .prologue
    .line 114
    iput-wide p1, p0, Lcom/glympse/android/lib/am;->kg:J

    .line 115
    iput-wide p3, p0, Lcom/glympse/android/lib/am;->kh:J

    .line 116
    iput-object p5, p0, Lcom/glympse/android/lib/am;->ki:Lcom/glympse/android/api/GTrack;

    .line 117
    const/4 v0, 0x2

    iput v0, p0, Lcom/glympse/android/lib/am;->cg:I

    .line 118
    invoke-direct {p0}, Lcom/glympse/android/lib/am;->aY()V

    .line 119
    return-void
.end method
