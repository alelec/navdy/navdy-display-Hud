.class Lcom/glympse/android/lib/eu$b;
.super Lcom/glympse/android/lib/f;
.source "ReplyParsers.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/lib/eu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation


# instance fields
.field private mD:Lcom/glympse/android/hal/GVector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GVector",
            "<",
            "Lcom/glympse/android/lib/GInvitePrivate;",
            ">;"
        }
    .end annotation
.end field

.field private oH:Lcom/glympse/android/lib/GInvitePrivate;


# direct methods
.method public constructor <init>(Lcom/glympse/android/lib/json/GJsonHandlerStack;Lcom/glympse/android/hal/GVector;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/glympse/android/lib/json/GJsonHandlerStack;",
            "Lcom/glympse/android/hal/GVector",
            "<",
            "Lcom/glympse/android/lib/GInvitePrivate;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/glympse/android/lib/f;-><init>()V

    .line 106
    iput-object p1, p0, Lcom/glympse/android/lib/eu$b;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    .line 107
    iput-object p2, p0, Lcom/glympse/android/lib/eu$b;->mD:Lcom/glympse/android/hal/GVector;

    .line 108
    return-void
.end method


# virtual methods
.method public endArray(I)Z
    .locals 1

    .prologue
    .line 135
    packed-switch p1, :pswitch_data_0

    .line 143
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 139
    :pswitch_0
    iget-object v0, p0, Lcom/glympse/android/lib/eu$b;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    invoke-interface {v0}, Lcom/glympse/android/lib/json/GJsonHandlerStack;->popHandler()V

    goto :goto_0

    .line 135
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public endObject(I)Z
    .locals 2

    .prologue
    .line 121
    packed-switch p1, :pswitch_data_0

    .line 130
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 125
    :pswitch_0
    iget-object v0, p0, Lcom/glympse/android/lib/eu$b;->mD:Lcom/glympse/android/hal/GVector;

    iget-object v1, p0, Lcom/glympse/android/lib/eu$b;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 126
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/eu$b;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    goto :goto_0

    .line 121
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public primitive(ILcom/glympse/android/lib/json/GJsonPrimitive;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 148
    packed-switch p1, :pswitch_data_0

    .line 172
    :cond_0
    :goto_0
    return v3

    .line 152
    :pswitch_0
    iget-object v0, p0, Lcom/glympse/android/lib/eu$b;->gE:Ljava/lang/String;

    const-string v1, "type"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 154
    invoke-virtual {p2, v3}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getString(Z)Ljava/lang/String;

    move-result-object v0

    .line 155
    iget-object v1, p0, Lcom/glympse/android/lib/eu$b;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-static {v0}, Lcom/glympse/android/lib/ct;->p(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GInvitePrivate;->setType(I)V

    goto :goto_0

    .line 157
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/eu$b;->gE:Ljava/lang/String;

    const-string v1, "subtype"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 159
    iget-object v0, p0, Lcom/glympse/android/lib/eu$b;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-virtual {p2, v2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GInvitePrivate;->setSubtype(Ljava/lang/String;)V

    goto :goto_0

    .line 161
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/eu$b;->gE:Ljava/lang/String;

    const-string v1, "name"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 163
    iget-object v0, p0, Lcom/glympse/android/lib/eu$b;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-virtual {p2, v2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GInvitePrivate;->setName(Ljava/lang/String;)V

    goto :goto_0

    .line 165
    :cond_3
    iget-object v0, p0, Lcom/glympse/android/lib/eu$b;->gE:Ljava/lang/String;

    const-string v1, "address"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/glympse/android/lib/eu$b;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-virtual {p2, v2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GInvitePrivate;->setAddress(Ljava/lang/String;)V

    goto :goto_0

    .line 148
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public startObject(I)Z
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 114
    new-instance v0, Lcom/glympse/android/lib/ct;

    invoke-direct {v0}, Lcom/glympse/android/lib/ct;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/eu$b;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    .line 116
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
