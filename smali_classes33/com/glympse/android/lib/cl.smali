.class Lcom/glympse/android/lib/cl;
.super Ljava/lang/Object;
.source "HashCodeBuilder.java"

# interfaces
.implements Lcom/glympse/android/core/GCommon;


# instance fields
.field private nl:I

.field private nm:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/16 v0, 0x25

    iput v0, p0, Lcom/glympse/android/lib/cl;->nl:I

    .line 31
    const/16 v0, 0x11

    iput v0, p0, Lcom/glympse/android/lib/cl;->nm:I

    .line 32
    return-void
.end method


# virtual methods
.method public a(D)V
    .locals 3

    .prologue
    .line 79
    invoke-static {p1, p2}, Lcom/glympse/android/hal/Helpers;->doubleToLongBits(D)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/glympse/android/lib/cl;->g(J)V

    .line 80
    return-void
.end method

.method public a(F)V
    .locals 1

    .prologue
    .line 69
    invoke-static {p1}, Lcom/glympse/android/hal/Helpers;->floatToIntBits(F)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/cl;->append(I)V

    .line 70
    return-void
.end method

.method public append(I)V
    .locals 2

    .prologue
    .line 45
    iget v0, p0, Lcom/glympse/android/lib/cl;->nm:I

    iget v1, p0, Lcom/glympse/android/lib/cl;->nl:I

    mul-int/2addr v0, v1

    add-int/2addr v0, p1

    iput v0, p0, Lcom/glympse/android/lib/cl;->nm:I

    .line 46
    return-void
.end method

.method public append(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 89
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/cl;->append(I)V

    .line 90
    return-void
.end method

.method public bZ()I
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lcom/glympse/android/lib/cl;->nm:I

    return v0
.end method

.method public g(J)V
    .locals 3

    .prologue
    .line 59
    const/16 v0, 0x20

    shr-long v0, p1, v0

    xor-long/2addr v0, p1

    long-to-int v0, v0

    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/cl;->append(I)V

    .line 60
    return-void
.end method
