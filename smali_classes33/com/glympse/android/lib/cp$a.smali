.class Lcom/glympse/android/lib/cp$a;
.super Ljava/lang/Object;
.source "HybridArrivalProvider.java"

# interfaces
.implements Lcom/glympse/android/api/GEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/lib/cp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private nQ:Lcom/glympse/android/api/GTriggersManager;

.field private nZ:Lcom/glympse/android/lib/cp;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 312
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/glympse/android/lib/cp$1;)V
    .locals 0

    .prologue
    .line 312
    invoke-direct {p0}, Lcom/glympse/android/lib/cp$a;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/glympse/android/api/GTriggersManager;Lcom/glympse/android/lib/cp;)V
    .locals 2

    .prologue
    .line 319
    iput-object p1, p0, Lcom/glympse/android/lib/cp$a;->nQ:Lcom/glympse/android/api/GTriggersManager;

    .line 320
    iput-object p2, p0, Lcom/glympse/android/lib/cp$a;->nZ:Lcom/glympse/android/lib/cp;

    .line 322
    iget-object v0, p0, Lcom/glympse/android/lib/cp$a;->nQ:Lcom/glympse/android/api/GTriggersManager;

    invoke-virtual {p0}, Lcom/glympse/android/lib/cp$a;->ar()Lcom/glympse/android/api/GEventListener;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/api/GTriggersManager;->addListener(Lcom/glympse/android/api/GEventListener;)Z

    .line 323
    return-void
.end method

.method protected ar()Lcom/glympse/android/api/GEventListener;
    .locals 1

    .prologue
    .line 345
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GEventListener;

    return-object v0
.end method

.method public eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 332
    const/16 v0, 0xf

    if-ne v0, p2, :cond_0

    .line 334
    and-int/lit8 v0, p3, 0x4

    if-eqz v0, :cond_0

    .line 336
    check-cast p4, Lcom/glympse/android/api/GTrigger;

    .line 338
    iget-object v0, p0, Lcom/glympse/android/lib/cp$a;->nZ:Lcom/glympse/android/lib/cp;

    invoke-static {v0, p4}, Lcom/glympse/android/lib/cp;->a(Lcom/glympse/android/lib/cp;Lcom/glympse/android/api/GTrigger;)V

    .line 341
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 327
    iget-object v0, p0, Lcom/glympse/android/lib/cp$a;->nQ:Lcom/glympse/android/api/GTriggersManager;

    invoke-virtual {p0}, Lcom/glympse/android/lib/cp$a;->ar()Lcom/glympse/android/api/GEventListener;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/api/GTriggersManager;->removeListener(Lcom/glympse/android/api/GEventListener;)Z

    .line 328
    return-void
.end method
