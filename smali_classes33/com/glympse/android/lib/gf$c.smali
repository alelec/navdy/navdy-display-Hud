.class Lcom/glympse/android/lib/gf$c;
.super Ljava/lang/Object;
.source "UserManager.java"

# interfaces
.implements Lcom/glympse/android/lib/gf$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/lib/gf;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "c"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 834
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/glympse/android/lib/gf$1;)V
    .locals 0

    .prologue
    .line 834
    invoke-direct {p0}, Lcom/glympse/android/lib/gf$c;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/glympse/android/api/GUser;Lcom/glympse/android/api/GUser;)I
    .locals 8

    .prologue
    const/16 v7, 0x10

    const/4 v0, 0x0

    const/4 v3, -0x1

    const/4 v1, 0x1

    .line 839
    invoke-interface {p1}, Lcom/glympse/android/api/GUser;->isSelf()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 874
    :cond_0
    :goto_0
    return v3

    .line 843
    :cond_1
    invoke-interface {p2}, Lcom/glympse/android/api/GUser;->isSelf()Z

    move-result v2

    if-eqz v2, :cond_2

    move v3, v1

    .line 845
    goto :goto_0

    .line 849
    :cond_2
    invoke-interface {p1}, Lcom/glympse/android/api/GUser;->getActive()Lcom/glympse/android/api/GTicket;

    move-result-object v5

    .line 850
    invoke-interface {p2}, Lcom/glympse/android/api/GUser;->getActive()Lcom/glympse/android/api/GTicket;

    move-result-object v6

    .line 851
    if-nez v5, :cond_4

    .line 853
    if-nez v6, :cond_3

    :goto_1
    move v3, v0

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    .line 855
    :cond_4
    if-eqz v6, :cond_0

    .line 861
    invoke-interface {v5}, Lcom/glympse/android/api/GTicket;->getState()I

    move-result v2

    if-ne v2, v7, :cond_6

    move v2, v1

    .line 862
    :goto_2
    invoke-interface {v6}, Lcom/glympse/android/api/GTicket;->getState()I

    move-result v4

    if-ne v4, v7, :cond_7

    move v4, v1

    .line 865
    :goto_3
    if-eq v2, v4, :cond_8

    .line 868
    if-eqz v2, :cond_5

    move v1, v3

    :cond_5
    move v3, v1

    goto :goto_0

    :cond_6
    move v2, v0

    .line 861
    goto :goto_2

    :cond_7
    move v4, v0

    .line 862
    goto :goto_3

    .line 872
    :cond_8
    invoke-interface {v5}, Lcom/glympse/android/api/GTicket;->getStartTime()J

    move-result-wide v4

    .line 873
    invoke-interface {v6}, Lcom/glympse/android/api/GTicket;->getStartTime()J

    move-result-wide v6

    .line 874
    cmp-long v2, v4, v6

    if-gtz v2, :cond_0

    cmp-long v2, v4, v6

    if-gez v2, :cond_9

    move v3, v1

    goto :goto_0

    :cond_9
    move v3, v0

    goto :goto_0
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 834
    check-cast p1, Lcom/glympse/android/api/GUser;

    check-cast p2, Lcom/glympse/android/api/GUser;

    invoke-virtual {p0, p1, p2}, Lcom/glympse/android/lib/gf$c;->a(Lcom/glympse/android/api/GUser;Lcom/glympse/android/api/GUser;)I

    move-result v0

    return v0
.end method
