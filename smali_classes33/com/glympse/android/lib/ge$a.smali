.class public Lcom/glympse/android/lib/ge$a;
.super Lcom/glympse/android/lib/f;
.source "UserInvites.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/lib/ge;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field public ib:J

.field public mD:Lcom/glympse/android/hal/GVector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GVector",
            "<",
            "Lcom/glympse/android/lib/GInvitePrivate;",
            ">;"
        }
    .end annotation
.end field

.field private oH:Lcom/glympse/android/lib/GInvitePrivate;

.field public sS:I

.field public uU:I

.field public uV:Z

.field public uW:Z

.field public uX:Lcom/glympse/android/lib/LocationProfile;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 284
    invoke-direct {p0}, Lcom/glympse/android/lib/f;-><init>()V

    .line 285
    new-instance v0, Lcom/glympse/android/hal/GVector;

    invoke-direct {v0}, Lcom/glympse/android/hal/GVector;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/ge$a;->mD:Lcom/glympse/android/hal/GVector;

    .line 286
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/glympse/android/lib/ge$a;->ib:J

    .line 287
    iput v2, p0, Lcom/glympse/android/lib/ge$a;->sS:I

    .line 288
    iput v2, p0, Lcom/glympse/android/lib/ge$a;->uU:I

    .line 289
    iput-boolean v2, p0, Lcom/glympse/android/lib/ge$a;->uV:Z

    .line 290
    iput-boolean v2, p0, Lcom/glympse/android/lib/ge$a;->uW:Z

    .line 291
    return-void
.end method


# virtual methods
.method public endObject(I)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 304
    if-ne v2, p1, :cond_1

    .line 306
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    invoke-interface {v0}, Lcom/glympse/android/lib/json/GJsonHandlerStack;->popHandler()V

    .line 317
    :cond_0
    :goto_0
    return v2

    .line 308
    :cond_1
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 311
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GInvitePrivate;->getTicketId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GInvitePrivate;->getCode()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 313
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->mD:Lcom/glympse/android/hal/GVector;

    iget-object v1, p0, Lcom/glympse/android/lib/ge$a;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 315
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/ge$a;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    goto :goto_0
.end method

.method public bridge synthetic endPair(I)Z
    .locals 1

    .prologue
    .line 272
    invoke-super {p0, p1}, Lcom/glympse/android/lib/f;->endPair(I)Z

    move-result v0

    return v0
.end method

.method public primitive(ILcom/glympse/android/lib/json/GJsonPrimitive;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 335
    packed-switch p1, :pswitch_data_0

    .line 462
    :cond_0
    :goto_0
    return v4

    .line 339
    :pswitch_0
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->gE:Ljava/lang/String;

    const-string v1, "result"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 341
    invoke-virtual {p2, v4}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/ge$a;->gF:Ljava/lang/String;

    goto :goto_0

    .line 347
    :pswitch_1
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->gE:Ljava/lang/String;

    const-string v1, "last_refresh"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 349
    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/glympse/android/lib/ge$a;->ib:J

    goto :goto_0

    .line 351
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->gE:Ljava/lang/String;

    const-string v1, "post_rate"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 353
    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getLong()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/glympse/android/lib/ge$a;->sS:I

    goto :goto_0

    .line 355
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->gE:Ljava/lang/String;

    const-string v1, "post_rate_limit"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 357
    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getLong()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/glympse/android/lib/ge$a;->uU:I

    goto :goto_0

    .line 359
    :cond_3
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->gE:Ljava/lang/String;

    const-string v1, "post_rate_is_high"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 361
    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/glympse/android/lib/ge$a;->uV:Z

    goto :goto_0

    .line 363
    :cond_4
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->gE:Ljava/lang/String;

    const-string v1, "refresh_history"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 365
    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/glympse/android/lib/ge$a;->uW:Z

    goto :goto_0

    .line 367
    :cond_5
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->gE:Ljava/lang/String;

    const-string v1, "time"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 369
    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/glympse/android/lib/ge$a;->_time:J

    goto :goto_0

    .line 371
    :cond_6
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->gE:Ljava/lang/String;

    const-string v1, "error"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 373
    invoke-virtual {p2, v4}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/ge$a;->gG:Ljava/lang/String;

    goto/16 :goto_0

    .line 375
    :cond_7
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->gE:Ljava/lang/String;

    const-string v1, "error_detail"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 377
    invoke-virtual {p2, v4}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/ge$a;->gH:Ljava/lang/String;

    goto/16 :goto_0

    .line 383
    :pswitch_2
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->uX:Lcom/glympse/android/lib/LocationProfile;

    if-eqz v0, :cond_0

    .line 386
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->gE:Ljava/lang/String;

    const-string v1, "profile"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 388
    invoke-virtual {p2, v4}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/glympse/android/lib/LocationProfile;->stringToEnum(Ljava/lang/String;)I

    move-result v0

    .line 389
    iget-object v1, p0, Lcom/glympse/android/lib/ge$a;->uX:Lcom/glympse/android/lib/LocationProfile;

    invoke-virtual {v1, v0}, Lcom/glympse/android/lib/LocationProfile;->setProfile(I)V

    goto/16 :goto_0

    .line 391
    :cond_8
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->gE:Ljava/lang/String;

    const-string v1, "mode"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 393
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->uX:Lcom/glympse/android/lib/LocationProfile;

    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getLong()J

    move-result-wide v2

    long-to-int v1, v2

    invoke-virtual {v0, v1}, Lcom/glympse/android/lib/LocationProfile;->setMode(I)V

    goto/16 :goto_0

    .line 395
    :cond_9
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->gE:Ljava/lang/String;

    const-string v1, "dist"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 397
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->uX:Lcom/glympse/android/lib/LocationProfile;

    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getDouble()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/glympse/android/lib/LocationProfile;->setDistance(D)V

    goto/16 :goto_0

    .line 400
    :cond_a
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->gE:Ljava/lang/String;

    const-string v1, "accuracy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 402
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->uX:Lcom/glympse/android/lib/LocationProfile;

    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getDouble()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/glympse/android/lib/LocationProfile;->setAccuracy(D)V

    goto/16 :goto_0

    .line 404
    :cond_b
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->gE:Ljava/lang/String;

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 406
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->uX:Lcom/glympse/android/lib/LocationProfile;

    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getLong()J

    move-result-wide v2

    long-to-int v1, v2

    invoke-virtual {v0, v1}, Lcom/glympse/android/lib/LocationProfile;->setActivity(I)V

    goto/16 :goto_0

    .line 408
    :cond_c
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->gE:Ljava/lang/String;

    const-string v1, "pause"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 410
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->uX:Lcom/glympse/android/lib/LocationProfile;

    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getBool()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/glympse/android/lib/LocationProfile;->setAutoPauseEnabled(Z)V

    goto/16 :goto_0

    .line 413
    :cond_d
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->gE:Ljava/lang/String;

    const-string v1, "source"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 415
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->uX:Lcom/glympse/android/lib/LocationProfile;

    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getLong()J

    move-result-wide v2

    long-to-int v1, v2

    invoke-virtual {v0, v1}, Lcom/glympse/android/lib/LocationProfile;->setSource(I)V

    goto/16 :goto_0

    .line 417
    :cond_e
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->gE:Ljava/lang/String;

    const-string v1, "freq"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 419
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->uX:Lcom/glympse/android/lib/LocationProfile;

    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getLong()J

    move-result-wide v2

    long-to-int v1, v2

    invoke-virtual {v0, v1}, Lcom/glympse/android/lib/LocationProfile;->setFrequency(I)V

    goto/16 :goto_0

    .line 421
    :cond_f
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->gE:Ljava/lang/String;

    const-string v1, "priority"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 423
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->uX:Lcom/glympse/android/lib/LocationProfile;

    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getLong()J

    move-result-wide v2

    long-to-int v1, v2

    invoke-virtual {v0, v1}, Lcom/glympse/android/lib/LocationProfile;->setPriority(I)V

    goto/16 :goto_0

    .line 430
    :pswitch_3
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->gE:Ljava/lang/String;

    const-string v1, "id"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 432
    invoke-virtual {p2, v4}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v0

    .line 433
    iget-object v1, p0, Lcom/glympse/android/lib/ge$a;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-static {v0}, Lcom/glympse/android/lib/TicketCode;->cleanupInviteCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GInvitePrivate;->setCode(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 435
    :cond_10
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->gE:Ljava/lang/String;

    const-string v1, "ticket_id"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 437
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-virtual {p2, v4}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GInvitePrivate;->setTicketId(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 439
    :cond_11
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->gE:Ljava/lang/String;

    const-string v1, "status"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 441
    invoke-virtual {p2, v4}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getString(Z)Ljava/lang/String;

    move-result-object v0

    .line 442
    iget-object v1, p0, Lcom/glympse/android/lib/ge$a;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-static {v0}, Lcom/glympse/android/lib/ct;->q(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GInvitePrivate;->setState(I)V

    goto/16 :goto_0

    .line 444
    :cond_12
    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 446
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->gE:Ljava/lang/String;

    const-string v1, "viewers"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 448
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getLong()J

    move-result-wide v2

    long-to-int v1, v2

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GInvitePrivate;->setViewers(I)V

    goto/16 :goto_0

    .line 450
    :cond_13
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->gE:Ljava/lang/String;

    const-string v1, "viewing"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 452
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getLong()J

    move-result-wide v2

    long-to-int v1, v2

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GInvitePrivate;->setViewing(I)V

    goto/16 :goto_0

    .line 454
    :cond_14
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->gE:Ljava/lang/String;

    const-string v1, "last_view"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 456
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getLong()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/glympse/android/lib/GInvitePrivate;->setLastViewTime(J)V

    goto/16 :goto_0

    .line 335
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public startObject(I)Z
    .locals 1

    .prologue
    .line 295
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 297
    new-instance v0, Lcom/glympse/android/lib/ct;

    invoke-direct {v0}, Lcom/glympse/android/lib/ct;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/ge$a;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    .line 299
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public startPair(ILjava/lang/String;)Z
    .locals 2

    .prologue
    .line 322
    iput-object p2, p0, Lcom/glympse/android/lib/ge$a;->gE:Ljava/lang/String;

    .line 323
    const/4 v0, 0x2

    if-ne v0, p1, :cond_0

    .line 325
    iget-object v0, p0, Lcom/glympse/android/lib/ge$a;->gE:Ljava/lang/String;

    const-string v1, "loc_profile"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 327
    new-instance v0, Lcom/glympse/android/lib/LocationProfile;

    invoke-direct {v0}, Lcom/glympse/android/lib/LocationProfile;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/ge$a;->uX:Lcom/glympse/android/lib/LocationProfile;

    .line 330
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
