.class Lcom/glympse/android/lib/fe;
.super Ljava/lang/Object;
.source "Ticket.java"

# interfaces
.implements Lcom/glympse/android/lib/GTicketPrivate;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/lib/fe$c;,
        Lcom/glympse/android/lib/fe$a;,
        Lcom/glympse/android/lib/fe$b;
    }
.end annotation


# instance fields
.field private _glympse:Lcom/glympse/android/lib/GGlympsePrivate;

.field private _name:Ljava/lang/String;

.field private cg:I

.field private dq:Ljava/lang/String;

.field private gM:Ljava/lang/String;

.field private hW:J

.field private he:Lcom/glympse/android/lib/CommonSink;

.field private ih:J

.field private kg:J

.field private kl:Lcom/glympse/android/api/GTrack;

.field private lJ:Lcom/glympse/android/lib/GTicketProtocol;

.field private lX:Lcom/glympse/android/lib/GCorrectedTime;

.field private mD:Lcom/glympse/android/hal/GVector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GVector",
            "<",
            "Lcom/glympse/android/api/GInvite;",
            ">;"
        }
    .end annotation
.end field

.field private mM:J

.field private mw:Ljava/lang/String;

.field private nG:J

.field private nt:J

.field private oB:Lcom/glympse/android/api/GTicket;

.field private ou:Ljava/lang/String;

.field private pr:Lcom/glympse/android/lib/GTrackPrivate;

.field private so:Ljava/lang/String;

.field private td:Z

.field private te:Z

.field private tf:Z

.field private tg:Ljava/lang/String;

.field private th:Lcom/glympse/android/api/GAppProfile;

.field private ti:Lcom/glympse/android/core/GPrimitive;

.field private tj:I

.field private tk:Lcom/glympse/android/api/GPlace;

.field private tl:J

.field private tm:Lcom/glympse/android/api/GTravelMode;

.field private tn:Z

.field private to:Lcom/glympse/android/hal/GHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GHashtable",
            "<",
            "Ljava/lang/Long;",
            "Lcom/glympse/android/hal/GHashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/glympse/android/api/GDataRow;",
            ">;>;"
        }
    .end annotation
.end field

.field private tp:I

.field private tq:Z

.field private tr:I

.field private ts:Lcom/glympse/android/lib/GTicketParent;

.field private tt:Ljava/lang/Runnable;

.field private tu:Ljava/lang/Runnable;

.field private tv:Lcom/glympse/android/core/GRegion;

.field private tw:Z

.field private tx:Ljava/lang/String;


# direct methods
.method public constructor <init>(Z)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    .line 224
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 225
    iput-boolean p1, p0, Lcom/glympse/android/lib/fe;->td:Z

    .line 226
    iput-boolean v0, p0, Lcom/glympse/android/lib/fe;->te:Z

    .line 227
    iput-boolean v0, p0, Lcom/glympse/android/lib/fe;->tf:Z

    .line 228
    iput v0, p0, Lcom/glympse/android/lib/fe;->cg:I

    .line 229
    new-instance v0, Lcom/glympse/android/hal/GVector;

    invoke-direct {v0}, Lcom/glympse/android/hal/GVector;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/fe;->mD:Lcom/glympse/android/hal/GVector;

    .line 230
    iput-wide v2, p0, Lcom/glympse/android/lib/fe;->hW:J

    .line 231
    iput-wide v2, p0, Lcom/glympse/android/lib/fe;->ih:J

    .line 232
    iput-wide v2, p0, Lcom/glympse/android/lib/fe;->nG:J

    .line 233
    iput v4, p0, Lcom/glympse/android/lib/fe;->tj:I

    .line 234
    new-instance v0, Lcom/glympse/android/lib/fs;

    invoke-direct {v0}, Lcom/glympse/android/lib/fs;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/fe;->pr:Lcom/glympse/android/lib/GTrackPrivate;

    .line 235
    iput-wide v2, p0, Lcom/glympse/android/lib/fe;->tl:J

    .line 236
    iput-wide v2, p0, Lcom/glympse/android/lib/fe;->kg:J

    .line 237
    iput-boolean v4, p0, Lcom/glympse/android/lib/fe;->tn:Z

    .line 238
    iput-wide v2, p0, Lcom/glympse/android/lib/fe;->mM:J

    .line 239
    new-instance v0, Lcom/glympse/android/hal/GHashtable;

    invoke-direct {v0}, Lcom/glympse/android/hal/GHashtable;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/fe;->to:Lcom/glympse/android/hal/GHashtable;

    .line 240
    const v0, 0x1d4c0

    iput v0, p0, Lcom/glympse/android/lib/fe;->tp:I

    .line 241
    iput-wide v2, p0, Lcom/glympse/android/lib/fe;->nt:J

    .line 242
    iput-boolean v4, p0, Lcom/glympse/android/lib/fe;->tq:Z

    .line 243
    const/16 v0, 0x3e0

    iput v0, p0, Lcom/glympse/android/lib/fe;->tr:I

    .line 249
    new-instance v0, Lcom/glympse/android/lib/CommonSink;

    const-string v1, "Ticket"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/glympse/android/lib/CommonSink;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/glympse/android/lib/fe;->he:Lcom/glympse/android/lib/CommonSink;

    .line 250
    iput-boolean v4, p0, Lcom/glympse/android/lib/fe;->tw:Z

    .line 251
    return-void
.end method

.method public static A(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2663
    sparse-switch p0, :sswitch_data_0

    .line 2686
    const-string v0, "unknown"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 2666
    :sswitch_0
    const-string v0, "none"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2668
    :sswitch_1
    const-string v0, "adding"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2670
    :sswitch_2
    const-string v0, "invalid"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2672
    :sswitch_3
    const-string v0, "active"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2674
    :sswitch_4
    const-string v0, "expiring"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2676
    :sswitch_5
    const-string v0, "expired"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2678
    :sswitch_6
    const-string v0, "deleting"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2680
    :sswitch_7
    const-string v0, "deleted"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2682
    :sswitch_8
    const-string v0, "failed_to_create"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2684
    :sswitch_9
    const-string v0, "cancelled"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2663
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x4 -> :sswitch_2
        0x10 -> :sswitch_3
        0x20 -> :sswitch_4
        0x40 -> :sswitch_5
        0x80 -> :sswitch_6
        0x100 -> :sswitch_7
        0x200 -> :sswitch_8
        0x400 -> :sswitch_9
    .end sparse-switch
.end method

.method private a(Lcom/glympse/android/api/GTicket;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2135
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1, p2, p1}, Lcom/glympse/android/lib/fe;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 2138
    iget-boolean v0, p0, Lcom/glympse/android/lib/fe;->td:Z

    if-eqz v0, :cond_0

    .line 2140
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const v2, 0x8000

    const/4 v3, 0x0

    invoke-interface {v0, v1, v4, v2, v3}, Lcom/glympse/android/lib/GGlympsePrivate;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 2146
    :goto_0
    return-void

    .line 2144
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/high16 v2, 0x80000

    invoke-interface {v0, v1, v4, v2, p1}, Lcom/glympse/android/lib/GGlympsePrivate;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/glympse/android/lib/fe;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/glympse/android/lib/fe;->cS()V

    return-void
.end method

.method private a(ZJ)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 2298
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->tu:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    .line 2337
    :cond_0
    :goto_0
    return-void

    .line 2304
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->gM:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/16 v0, 0x40

    iget v1, p0, Lcom/glympse/android/lib/fe;->cg:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/glympse/android/lib/fe;->tn:Z

    if-nez v0, :cond_0

    .line 2310
    iget-wide v0, p0, Lcom/glympse/android/lib/fe;->ih:J

    const-wide/32 v2, 0x1d4c0

    add-long/2addr v0, v2

    sub-long v2, v0, p2

    .line 2313
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-lez v0, :cond_3

    .line 2316
    if-eqz p1, :cond_2

    iget-boolean v0, p0, Lcom/glympse/android/lib/fe;->td:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/glympse/android/lib/fe;->te:Z

    if-eqz v0, :cond_2

    .line 2318
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getHistoryManager()Lcom/glympse/android/api/GHistoryManager;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GHistoryManagerPrivate;

    .line 2319
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/glympse/android/lib/GTicketPrivate;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GHistoryManagerPrivate;->completeTicket(Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 2321
    const-string v0, "[Ticket.initiateCompletion] Completion initiated"

    invoke-static {v4, v0}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 2325
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->isActive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2327
    new-instance v1, Lcom/glympse/android/lib/fe$a;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/fe;

    invoke-direct {v1, p0, v0}, Lcom/glympse/android/lib/fe$a;-><init>(Lcom/glympse/android/lib/fe;Lcom/glympse/android/lib/fe;)V

    iput-object v1, p0, Lcom/glympse/android/lib/fe;->tu:Ljava/lang/Runnable;

    .line 2328
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getHandler()Lcom/glympse/android/core/GHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/lib/fe;->tu:Ljava/lang/Runnable;

    invoke-interface {v0, v1, v2, v3}, Lcom/glympse/android/core/GHandler;->postDelayed(Ljava/lang/Runnable;J)V

    .line 2330
    const-string v0, "[Ticket.initiateCompletion] Timer scheduled"

    invoke-static {v4, v0}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    goto :goto_0

    .line 2335
    :cond_3
    invoke-virtual {p0}, Lcom/glympse/android/lib/fe;->setCompleted()V

    goto :goto_0
.end method

.method private b(ZZ)V
    .locals 2

    .prologue
    .line 2181
    const/4 v0, 0x2

    invoke-static {}, Lcom/glympse/android/hal/Concurrent;->getBackgroundMode()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 2200
    :cond_0
    :goto_0
    return-void

    .line 2186
    :cond_1
    if-eqz p1, :cond_2

    .line 2189
    if-nez p2, :cond_0

    iget v0, p0, Lcom/glympse/android/lib/fe;->cg:I

    and-int/lit8 v0, v0, 0x32

    if-eqz v0, :cond_0

    .line 2191
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getTime()J

    move-result-wide v0

    .line 2192
    invoke-direct {p0, v0, v1}, Lcom/glympse/android/lib/fe;->j(J)V

    goto :goto_0

    .line 2198
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/fe;->e(Lcom/glympse/android/lib/GGlympsePrivate;)V

    goto :goto_0
.end method

.method private cQ()V
    .locals 2

    .prologue
    .line 2160
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->th:Lcom/glympse/android/api/GAppProfile;

    invoke-interface {v0}, Lcom/glympse/android/api/GAppProfile;->getIcon()Lcom/glympse/android/api/GImage;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GImagePrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->getImageCache()Lcom/glympse/android/lib/GImageCache;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GImagePrivate;->attachCache(Lcom/glympse/android/lib/GImageCache;)V

    .line 2161
    return-void
.end method

.method private cS()V
    .locals 1

    .prologue
    .line 2352
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-nez v0, :cond_0

    .line 2362
    :goto_0
    return-void

    .line 2358
    :cond_0
    invoke-virtual {p0}, Lcom/glympse/android/lib/fe;->setCompleted()V

    .line 2361
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/fe;->tu:Ljava/lang/Runnable;

    goto :goto_0
.end method

.method private cT()V
    .locals 3

    .prologue
    .line 2387
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getHandler()Lcom/glympse/android/core/GHandler;

    move-result-object v1

    new-instance v2, Lcom/glympse/android/lib/fe$c;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/fe;

    invoke-direct {v2, v0}, Lcom/glympse/android/lib/fe$c;-><init>(Lcom/glympse/android/lib/fe;)V

    invoke-interface {v1, v2}, Lcom/glympse/android/core/GHandler;->post(Ljava/lang/Runnable;)V

    .line 2388
    return-void
.end method

.method private e(Lcom/glympse/android/api/GTicket;)V
    .locals 4

    .prologue
    .line 2169
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getDiagnosticsManager()Lcom/glympse/android/lib/GDiagnosticsManager;

    move-result-object v0

    const-string v1, "ticket"

    .line 2170
    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "state"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "truncated"

    .line 2171
    invoke-static {v3}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2169
    invoke-interface {v0, p1, v1, v2, v3}, Lcom/glympse/android/lib/GDiagnosticsManager;->logEvent(Lcom/glympse/android/api/GTicket;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2172
    return-void
.end method

.method private e(Lcom/glympse/android/lib/GGlympsePrivate;)V
    .locals 2

    .prologue
    .line 2250
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->tt:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 2252
    const/4 v0, 0x1

    const-string v1, "[Ticket.cancelExpireTimer] Cancelled"

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 2254
    invoke-interface {p1}, Lcom/glympse/android/lib/GGlympsePrivate;->getHandler()Lcom/glympse/android/core/GHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/lib/fe;->tt:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GHandler;->cancel(Ljava/lang/Runnable;)V

    .line 2255
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/fe;->tt:Ljava/lang/Runnable;

    .line 2257
    :cond_0
    return-void
.end method

.method private f(Lcom/glympse/android/lib/GGlympsePrivate;)V
    .locals 2

    .prologue
    .line 2341
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->tu:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 2343
    const/4 v0, 0x1

    const-string v1, "[Ticket.cancelCompletionTimer] Cancelled"

    invoke-static {v0, v1}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 2345
    invoke-interface {p1}, Lcom/glympse/android/lib/GGlympsePrivate;->getHandler()Lcom/glympse/android/core/GHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/lib/fe;->tu:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GHandler;->cancel(Ljava/lang/Runnable;)V

    .line 2346
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/fe;->tu:Ljava/lang/Runnable;

    .line 2348
    :cond_0
    return-void
.end method

.method private getTime()J
    .locals 2

    .prologue
    .line 2204
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getTime()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Lcom/glympse/android/hal/Concurrent;->getTime()J

    move-result-wide v0

    goto :goto_0
.end method

.method private j(J)V
    .locals 5

    .prologue
    .line 2209
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->tt:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 2211
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getHandler()Lcom/glympse/android/core/GHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/lib/fe;->tt:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GHandler;->cancel(Ljava/lang/Runnable;)V

    .line 2214
    :cond_0
    new-instance v1, Lcom/glympse/android/lib/fe$b;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/fe;

    invoke-direct {v1, p0, v0}, Lcom/glympse/android/lib/fe$b;-><init>(Lcom/glympse/android/lib/fe;Lcom/glympse/android/lib/fe;)V

    iput-object v1, p0, Lcom/glympse/android/lib/fe;->tt:Ljava/lang/Runnable;

    .line 2215
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->isActive()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2246
    :goto_0
    return-void

    .line 2220
    :cond_1
    iget-wide v0, p0, Lcom/glympse/android/lib/fe;->ih:J

    cmp-long v0, v0, p1

    if-gez v0, :cond_3

    .line 2222
    iget-wide v0, p0, Lcom/glympse/android/lib/fe;->nG:J

    cmp-long v0, v0, p1

    if-gez v0, :cond_2

    .line 2224
    const-wide/16 v0, 0x32

    add-long/2addr v0, p1

    .line 2242
    :goto_1
    sub-long/2addr v0, p1

    const-wide/16 v2, 0xfa

    add-long/2addr v0, v2

    .line 2243
    iget-object v2, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v2}, Lcom/glympse/android/lib/GGlympsePrivate;->getHandler()Lcom/glympse/android/core/GHandler;

    move-result-object v2

    iget-object v3, p0, Lcom/glympse/android/lib/fe;->tt:Ljava/lang/Runnable;

    invoke-interface {v2, v3, v0, v1}, Lcom/glympse/android/core/GHandler;->postDelayed(Ljava/lang/Runnable;J)V

    .line 2245
    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[Ticket.startExpireTimer] Scheduling timer:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0, v1}, Lcom/glympse/android/hal/Helpers;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    goto :goto_0

    .line 2228
    :cond_2
    iget-wide v0, p0, Lcom/glympse/android/lib/fe;->nG:J

    goto :goto_1

    .line 2233
    :cond_3
    iget-wide v0, p0, Lcom/glympse/android/lib/fe;->nG:J

    cmp-long v0, v0, p1

    if-gez v0, :cond_4

    .line 2235
    iget-wide v0, p0, Lcom/glympse/android/lib/fe;->ih:J

    goto :goto_1

    .line 2239
    :cond_4
    iget-wide v0, p0, Lcom/glympse/android/lib/fe;->ih:J

    iget-wide v2, p0, Lcom/glympse/android/lib/fe;->nG:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_5

    iget-wide v0, p0, Lcom/glympse/android/lib/fe;->ih:J

    goto :goto_1

    :cond_5
    iget-wide v0, p0, Lcom/glympse/android/lib/fe;->nG:J

    goto :goto_1
.end method

.method public static q(Ljava/lang/String;)I
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2693
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2717
    :cond_0
    :goto_0
    return v0

    .line 2695
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    .line 2697
    const-string v2, "adding"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2698
    const/4 v0, 0x2

    goto :goto_0

    .line 2699
    :cond_2
    const-string v2, "invalid"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2700
    const/4 v0, 0x4

    goto :goto_0

    .line 2701
    :cond_3
    const-string v2, "active"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2702
    const/16 v0, 0x10

    goto :goto_0

    .line 2703
    :cond_4
    const-string v2, "expiring"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2704
    const/16 v0, 0x20

    goto :goto_0

    .line 2705
    :cond_5
    const-string v2, "expired"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2706
    const/16 v0, 0x40

    goto :goto_0

    .line 2707
    :cond_6
    const-string v2, "de;etomg"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2708
    const/16 v0, 0x80

    goto :goto_0

    .line 2709
    :cond_7
    const-string v2, "delete"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2710
    const/16 v0, 0x100

    goto :goto_0

    .line 2711
    :cond_8
    const-string v2, "failed_to_create"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 2712
    const/16 v0, 0x200

    goto :goto_0

    .line 2713
    :cond_9
    const-string v2, "cancelled"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2714
    const/16 v0, 0x400

    goto :goto_0
.end method

.method private save()V
    .locals 1

    .prologue
    .line 2150
    iget-boolean v0, p0, Lcom/glympse/android/lib/fe;->td:Z

    if-eqz v0, :cond_0

    .line 2152
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getUserManager()Lcom/glympse/android/api/GUserManager;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GUserManagerPrivate;

    .line 2153
    invoke-interface {v0}, Lcom/glympse/android/lib/GUserManagerPrivate;->save()V

    .line 2155
    :cond_0
    return-void
.end method

.method private z(I)V
    .locals 1

    .prologue
    .line 2129
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GTicket;

    invoke-direct {p0, v0, p1}, Lcom/glympse/android/lib/fe;->a(Lcom/glympse/android/api/GTicket;I)V

    .line 2130
    return-void
.end method


# virtual methods
.method public a(JLjava/lang/String;Lcom/glympse/android/core/GPrimitive;)V
    .locals 9

    .prologue
    .line 1377
    const-wide/16 v2, 0x0

    .line 1378
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_0

    .line 1380
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getCorrectedTime()Lcom/glympse/android/lib/GCorrectedTime;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/lib/GCorrectedTime;->getTime()J

    move-result-wide v2

    :cond_0
    move-object v1, p0

    move-wide v4, p1

    move-object v6, p3

    move-object v7, p4

    .line 1383
    invoke-virtual/range {v1 .. v7}, Lcom/glympse/android/lib/fe;->setProperty(JJLjava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 1384
    return-void
.end method

.method public addInvite(Lcom/glympse/android/api/GInvite;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 604
    iget-boolean v2, p0, Lcom/glympse/android/lib/fe;->td:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/glympse/android/lib/fe;->te:Z

    if-eqz v2, :cond_0

    const/16 v2, 0x40

    iget v3, p0, Lcom/glympse/android/lib/fe;->cg:I

    if-eq v2, v3, :cond_0

    if-nez p1, :cond_1

    .line 634
    :cond_0
    :goto_0
    return v0

    .line 610
    :cond_1
    invoke-virtual {p0, p1}, Lcom/glympse/android/lib/fe;->canAddInvite(Lcom/glympse/android/api/GInvite;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 616
    invoke-virtual {p0, p1}, Lcom/glympse/android/lib/fe;->addInviteCore(Lcom/glympse/android/api/GInvite;)V

    .line 619
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-nez v0, :cond_2

    move v0, v1

    .line 621
    goto :goto_0

    .line 625
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getBrand()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/api/GInvite;->applyBrand(Ljava/lang/String;)V

    .line 628
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->gM:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 631
    iget-object v2, p0, Lcom/glympse/android/lib/fe;->lJ:Lcom/glympse/android/lib/GTicketProtocol;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GTicketPrivate;

    check-cast p1, Lcom/glympse/android/lib/GInvitePrivate;

    invoke-interface {v2, v0, p1}, Lcom/glympse/android/lib/GTicketProtocol;->addInvite(Lcom/glympse/android/lib/GTicketPrivate;Lcom/glympse/android/lib/GInvitePrivate;)V

    :cond_3
    move v0, v1

    .line 634
    goto :goto_0
.end method

.method public addInviteCore(Lcom/glympse/android/api/GInvite;)V
    .locals 2

    .prologue
    .line 2039
    move-object v0, p1

    check-cast v0, Lcom/glympse/android/lib/GInvitePrivate;

    .line 2040
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GInvitePrivate;->setGlympse(Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 2041
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->mD:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 2042
    return-void
.end method

.method public addListener(Lcom/glympse/android/api/GEventListener;)Z
    .locals 1

    .prologue
    .line 2726
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1}, Lcom/glympse/android/lib/CommonSink;->addListener(Lcom/glympse/android/api/GEventListener;)Z

    move-result v0

    return v0
.end method

.method public appendData(JLjava/lang/String;Lcom/glympse/android/core/GPrimitive;)Z
    .locals 11

    .prologue
    const/16 v2, 0x40

    const/4 v9, 0x1

    .line 1039
    iget-boolean v0, p0, Lcom/glympse/android/lib/fe;->td:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/glympse/android/lib/fe;->te:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/glympse/android/lib/fe;->cg:I

    if-eq v2, v0, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    .line 1040
    invoke-static {p3}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p4, :cond_1

    .line 1042
    :cond_0
    const/4 v0, 0x0

    .line 1081
    :goto_0
    return v0

    .line 1048
    :cond_1
    invoke-interface {p4}, Lcom/glympse/android/core/GPrimitive;->type()I

    move-result v0

    if-ne v2, v0, :cond_2

    .line 1050
    invoke-virtual {p0, p1, p2, p3}, Lcom/glympse/android/lib/fe;->clearProperty(JLjava/lang/String;)V

    .line 1051
    const/high16 v0, 0x800000

    move v8, v0

    .line 1060
    :goto_1
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-nez v0, :cond_3

    move v0, v9

    .line 1062
    goto :goto_0

    .line 1055
    :cond_2
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/glympse/android/lib/fe;->a(JLjava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 1056
    const/high16 v0, 0x400000

    move v8, v0

    goto :goto_1

    .line 1066
    :cond_3
    new-instance v1, Lcom/glympse/android/lib/aj;

    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getTime()J

    move-result-wide v2

    move-wide v4, p1

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v1 .. v7}, Lcom/glympse/android/lib/aj;-><init>(JJLjava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 1067
    new-instance v2, Lcom/glympse/android/hal/GVector;

    invoke-direct {v2}, Lcom/glympse/android/hal/GVector;-><init>()V

    .line 1068
    invoke-virtual {v2, v1}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    .line 1069
    new-instance v1, Lcom/glympse/android/lib/ai;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GTicket;

    invoke-direct {v1, v0, v2}, Lcom/glympse/android/lib/ai;-><init>(Lcom/glympse/android/api/GTicket;Lcom/glympse/android/hal/GVector;)V

    .line 1072
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/4 v2, 0x4

    invoke-virtual {p0, v0, v2, v8, v1}, Lcom/glympse/android/lib/fe;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 1075
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->gM:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1078
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->lJ:Lcom/glympse/android/lib/GTicketProtocol;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/glympse/android/lib/GTicketPrivate;

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-interface/range {v0 .. v5}, Lcom/glympse/android/lib/GTicketProtocol;->appendData(Lcom/glympse/android/lib/GTicketPrivate;JLjava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    :cond_4
    move v0, v9

    .line 1081
    goto :goto_0
.end method

.method public associateContext(JLjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2751
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1, p2, p3}, Lcom/glympse/android/lib/CommonSink;->associateContext(JLjava/lang/Object;)V

    .line 2752
    return-void
.end method

.method protected cR()V
    .locals 4

    .prologue
    .line 2261
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/fe;->tt:Ljava/lang/Runnable;

    .line 2262
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_1

    .line 2264
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getCorrectedTime()Lcom/glympse/android/lib/GCorrectedTime;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/lib/GCorrectedTime;->getTime()J

    move-result-wide v0

    .line 2265
    iget-wide v2, p0, Lcom/glympse/android/lib/fe;->ih:J

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 2267
    const/4 v2, 0x4

    const-string v3, "[Ticket.completeExpireTimer] Timer has fired prematurely"

    invoke-static {v2, v3}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 2270
    :cond_0
    invoke-virtual {p0, v0, v1}, Lcom/glympse/android/lib/fe;->updateState(J)Z

    .line 2272
    :cond_1
    return-void
.end method

.method public cU()V
    .locals 1

    .prologue
    .line 2392
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/fe;->ts:Lcom/glympse/android/lib/GTicketParent;

    if-eqz v0, :cond_0

    .line 2394
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->ts:Lcom/glympse/android/lib/GTicketParent;

    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketParent;->orderChanged()V

    .line 2396
    :cond_0
    return-void
.end method

.method public canAddInvite(Lcom/glympse/android/api/GInvite;)Z
    .locals 5

    .prologue
    const/16 v4, 0xb

    const/16 v3, 0x8

    const/4 v1, 0x0

    .line 518
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->mD:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GVector;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 598
    :goto_0
    return v0

    :cond_0
    move-object v0, p1

    .line 524
    check-cast v0, Lcom/glympse/android/lib/GInvitePrivate;

    .line 525
    invoke-interface {v0}, Lcom/glympse/android/lib/GInvitePrivate;->getGlympse()Lcom/glympse/android/lib/GGlympsePrivate;

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 527
    goto :goto_0

    .line 531
    :cond_1
    invoke-interface {p1}, Lcom/glympse/android/api/GInvite;->getType()I

    move-result v0

    .line 532
    packed-switch v0, :pswitch_data_0

    .line 589
    :pswitch_0
    invoke-interface {p1}, Lcom/glympse/android/api/GInvite;->getAddress()Ljava/lang/String;

    move-result-object v0

    .line 590
    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/fe;->findInviteByAddress(Ljava/lang/String;)Lcom/glympse/android/api/GInvite;

    move-result-object v0

    if-eqz v0, :cond_6

    :cond_2
    move v0, v1

    .line 592
    goto :goto_0

    .line 541
    :pswitch_1
    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/fe;->findInviteByType(I)Lcom/glympse/android/api/GInvite;

    move-result-object v0

    if-eqz v0, :cond_6

    move v0, v1

    .line 543
    goto :goto_0

    .line 553
    :pswitch_2
    invoke-virtual {p0, v4}, Lcom/glympse/android/lib/fe;->findInviteByType(I)Lcom/glympse/android/api/GInvite;

    move-result-object v2

    if-nez v2, :cond_3

    .line 554
    invoke-virtual {p0, v3}, Lcom/glympse/android/lib/fe;->findInviteByType(I)Lcom/glympse/android/api/GInvite;

    move-result-object v2

    if-eqz v2, :cond_4

    :cond_3
    move v0, v1

    .line 556
    goto :goto_0

    .line 559
    :cond_4
    if-ne v3, v0, :cond_5

    .line 561
    invoke-interface {p1}, Lcom/glympse/android/api/GInvite;->getAddress()Ljava/lang/String;

    move-result-object v2

    .line 562
    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    move v0, v1

    .line 564
    goto :goto_0

    .line 568
    :cond_5
    if-ne v4, v0, :cond_6

    .line 570
    invoke-interface {p1}, Lcom/glympse/android/api/GInvite;->getSubtype()Ljava/lang/String;

    move-result-object v0

    .line 571
    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    .line 573
    goto :goto_0

    .line 598
    :cond_6
    :pswitch_3
    const/4 v0, 0x1

    goto :goto_0

    .line 532
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public clear(ZZ)Z
    .locals 10

    .prologue
    const-wide/16 v2, 0x0

    const/4 v9, 0x1

    const/4 v7, 0x0

    const/4 v1, 0x0

    .line 984
    iget-boolean v0, p0, Lcom/glympse/android/lib/fe;->td:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/glympse/android/lib/fe;->te:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x40

    iget v4, p0, Lcom/glympse/android/lib/fe;->cg:I

    if-ne v0, v4, :cond_1

    .line 1028
    :cond_0
    :goto_0
    return v1

    .line 993
    :cond_1
    if-eqz p1, :cond_5

    .line 995
    const/16 v0, 0x20

    .line 996
    iput-object v7, p0, Lcom/glympse/android/lib/fe;->dq:Ljava/lang/String;

    .line 998
    :goto_1
    if-eqz p2, :cond_4

    .line 1001
    or-int/lit8 v8, v0, 0x40

    .line 1002
    invoke-virtual {p0, v7}, Lcom/glympse/android/lib/fe;->setDestination(Lcom/glympse/android/api/GPlace;)V

    move-object v0, p0

    move-wide v4, v2

    move v6, v1

    .line 1005
    invoke-virtual/range {v0 .. v7}, Lcom/glympse/android/lib/fe;->updateEta(IJJILcom/glympse/android/api/GTrack;)V

    move v1, v8

    .line 1009
    :goto_2
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_3

    .line 1011
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GTicket;

    .line 1014
    iget-object v2, p0, Lcom/glympse/android/lib/fe;->gM:Ljava/lang/String;

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1017
    iget-object v2, p0, Lcom/glympse/android/lib/fe;->lJ:Lcom/glympse/android/lib/GTicketProtocol;

    invoke-interface {v2, v0, p1, p2}, Lcom/glympse/android/lib/GTicketProtocol;->clearTicket(Lcom/glympse/android/api/GTicket;ZZ)V

    .line 1021
    :cond_2
    if-eqz v1, :cond_3

    .line 1023
    iget-object v2, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/4 v3, 0x4

    invoke-virtual {p0, v2, v3, v1, v0}, Lcom/glympse/android/lib/fe;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 1024
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget-object v2, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/high16 v3, 0x80000

    invoke-interface {v1, v2, v9, v3, v0}, Lcom/glympse/android/lib/GGlympsePrivate;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    :cond_3
    move v1, v9

    .line 1028
    goto :goto_0

    :cond_4
    move v1, v0

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public clearContext(J)V
    .locals 1

    .prologue
    .line 2761
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1, p2}, Lcom/glympse/android/lib/CommonSink;->clearContext(J)V

    .line 2762
    return-void
.end method

.method public clearProperty(JLjava/lang/String;)V
    .locals 3

    .prologue
    .line 1411
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->to:Lcom/glympse/android/hal/GHashtable;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/hal/GHashtable;

    .line 1412
    if-eqz v0, :cond_0

    .line 1415
    invoke-virtual {v0, p3}, Lcom/glympse/android/hal/GHashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1417
    :cond_0
    return-void
.end method

.method public clone()Lcom/glympse/android/api/GTicket;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1175
    new-instance v2, Lcom/glympse/android/lib/fe;

    invoke-direct {v2, v0}, Lcom/glympse/android/lib/fe;-><init>(Z)V

    .line 1177
    iget v1, p0, Lcom/glympse/android/lib/fe;->tj:I

    iput v1, v2, Lcom/glympse/android/lib/fe;->tj:I

    .line 1179
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->dq:Ljava/lang/String;

    iput-object v1, v2, Lcom/glympse/android/lib/fe;->dq:Ljava/lang/String;

    .line 1181
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->tk:Lcom/glympse/android/api/GPlace;

    if-eqz v1, :cond_0

    .line 1184
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->tk:Lcom/glympse/android/api/GPlace;

    invoke-interface {v1}, Lcom/glympse/android/api/GPlace;->clone()Lcom/glympse/android/api/GPlace;

    move-result-object v1

    iput-object v1, v2, Lcom/glympse/android/lib/fe;->tk:Lcom/glympse/android/api/GPlace;

    .line 1187
    :cond_0
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->mD:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v1}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v3

    move v1, v0

    .line 1188
    :goto_0
    if-ge v1, v3, :cond_1

    .line 1190
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->mD:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GInvite;

    .line 1192
    invoke-interface {v0}, Lcom/glympse/android/api/GInvite;->clone()Lcom/glympse/android/api/GInvite;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/glympse/android/lib/fe;->addInvite(Lcom/glympse/android/api/GInvite;)Z

    .line 1188
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1194
    :cond_1
    return-object v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/glympse/android/lib/fe;->clone()Lcom/glympse/android/api/GTicket;

    move-result-object v0

    return-object v0
.end method

.method public decode(Lcom/glympse/android/core/GPrimitive;)V
    .locals 10

    .prologue
    const/4 v0, 0x1

    .line 2587
    const-string v1, "id"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/glympse/android/lib/fe;->gM:Ljava/lang/String;

    .line 2588
    const-string v1, "code"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2589
    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2594
    invoke-static {v1}, Lcom/glympse/android/lib/TicketCode;->cleanupInviteCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2595
    invoke-virtual {p0, v1}, Lcom/glympse/android/lib/fe;->setCode(Ljava/lang/String;)V

    .line 2596
    invoke-virtual {p0, v1}, Lcom/glympse/android/lib/fe;->setId(Ljava/lang/String;)V

    .line 2598
    :cond_0
    const-string v1, "ref"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/glympse/android/lib/fe;->ou:Ljava/lang/String;

    .line 2599
    const-string v1, "stal"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/glympse/android/core/GPrimitive;->getBool(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/glympse/android/lib/fe;->tf:Z

    .line 2600
    const-string v1, "cmpl"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/glympse/android/core/GPrimitive;->getBool(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/glympse/android/lib/fe;->tn:Z

    .line 2601
    const-string v1, "sts"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2602
    const-string v1, "ets"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 2603
    const-string v1, "dur"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 2604
    const-string v1, "st"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    long-to-int v1, v8

    .line 2605
    invoke-virtual {p0, v2, v3}, Lcom/glympse/android/lib/fe;->setStartTime(J)V

    .line 2606
    invoke-virtual {p0, v4, v5, v0}, Lcom/glympse/android/lib/fe;->setExpireTime(JZ)V

    .line 2607
    long-to-int v2, v6

    invoke-virtual {p0, v2}, Lcom/glympse/android/lib/fe;->setDuration(I)V

    .line 2608
    if-nez v1, :cond_2

    :goto_0
    iput v0, p0, Lcom/glympse/android/lib/fe;->cg:I

    .line 2609
    const-string v0, "name"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/fe;->_name:Ljava/lang/String;

    .line 2610
    const-string v0, "msg"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/fe;->dq:Ljava/lang/String;

    .line 2611
    const-string v0, "dst"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v1

    .line 2612
    if-eqz v1, :cond_1

    .line 2614
    new-instance v0, Lcom/glympse/android/lib/eg;

    invoke-direct {v0}, Lcom/glympse/android/lib/eg;-><init>()V

    .line 2615
    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPersistable;->decode(Lcom/glympse/android/core/GPrimitive;)V

    .line 2616
    check-cast v0, Lcom/glympse/android/api/GPlace;

    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/fe;->setDestination(Lcom/glympse/android/api/GPlace;)V

    .line 2620
    :cond_1
    const-string v0, "recs"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v1

    .line 2621
    if-eqz v1, :cond_3

    .line 2623
    invoke-interface {v1}, Lcom/glympse/android/core/GPrimitive;->size()I

    move-result v2

    .line 2624
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_3

    .line 2626
    invoke-interface {v1, v0}, Lcom/glympse/android/core/GPrimitive;->get(I)Lcom/glympse/android/core/GPrimitive;

    move-result-object v3

    .line 2627
    new-instance v4, Lcom/glympse/android/lib/ct;

    invoke-direct {v4}, Lcom/glympse/android/lib/ct;-><init>()V

    .line 2628
    invoke-interface {v4, v3}, Lcom/glympse/android/lib/GInvitePrivate;->decode(Lcom/glympse/android/core/GPrimitive;)V

    .line 2629
    invoke-virtual {p0, v4}, Lcom/glympse/android/lib/fe;->addInviteCore(Lcom/glympse/android/api/GInvite;)V

    .line 2624
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 2608
    goto :goto_0

    .line 2634
    :cond_3
    const-string v0, "eta"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/glympse/android/lib/fe;->tl:J

    .line 2635
    const-string v0, "etaTs"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/glympse/android/lib/fe;->kg:J

    .line 2637
    const-string v0, "route"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 2638
    if-eqz v0, :cond_4

    .line 2640
    new-instance v1, Lcom/glympse/android/lib/fs;

    invoke-direct {v1}, Lcom/glympse/android/lib/fs;-><init>()V

    .line 2641
    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GTrackPrivate;->decode(Lcom/glympse/android/core/GPrimitive;)V

    .line 2642
    iput-object v1, p0, Lcom/glympse/android/lib/fe;->kl:Lcom/glympse/android/api/GTrack;

    .line 2646
    :cond_4
    const-string v0, "owner"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 2647
    if-eqz v0, :cond_5

    .line 2649
    new-instance v1, Lcom/glympse/android/lib/i;

    invoke-direct {v1}, Lcom/glympse/android/lib/i;-><init>()V

    iput-object v1, p0, Lcom/glympse/android/lib/fe;->th:Lcom/glympse/android/api/GAppProfile;

    .line 2650
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->th:Lcom/glympse/android/api/GAppProfile;

    invoke-interface {v1, v0}, Lcom/glympse/android/api/GAppProfile;->decode(Lcom/glympse/android/core/GPrimitive;)V

    .line 2653
    :cond_5
    const-string v0, "trMd"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/glympse/android/core/GPrimitive;->get(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 2654
    if-eqz v0, :cond_6

    .line 2656
    new-instance v1, Lcom/glympse/android/lib/fu;

    invoke-direct {v1}, Lcom/glympse/android/lib/fu;-><init>()V

    iput-object v1, p0, Lcom/glympse/android/lib/fe;->tm:Lcom/glympse/android/api/GTravelMode;

    .line 2657
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->tm:Lcom/glympse/android/api/GTravelMode;

    invoke-interface {v1, v0}, Lcom/glympse/android/api/GTravelMode;->decode(Lcom/glympse/android/core/GPrimitive;)V

    .line 2659
    :cond_6
    return-void
.end method

.method public deleteInvite(Lcom/glympse/android/api/GInvite;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 640
    iget-boolean v2, p0, Lcom/glympse/android/lib/fe;->td:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/glympse/android/lib/fe;->te:Z

    if-eqz v2, :cond_0

    const/16 v2, 0x40

    iget v3, p0, Lcom/glympse/android/lib/fe;->cg:I

    if-ne v2, v3, :cond_1

    .line 688
    :cond_0
    :goto_0
    return v0

    .line 646
    :cond_1
    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/glympse/android/lib/fe;->mD:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v2, p1}, Lcom/glympse/android/hal/GVector;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 652
    invoke-interface {p1}, Lcom/glympse/android/api/GInvite;->getState()I

    move-result v2

    .line 653
    const/4 v3, 0x5

    if-eq v3, v2, :cond_0

    const/4 v3, 0x6

    if-eq v3, v2, :cond_0

    .line 661
    const/4 v3, 0x7

    if-eq v3, v2, :cond_2

    const/16 v3, 0x9

    if-ne v3, v2, :cond_3

    .line 665
    :cond_2
    invoke-virtual {p0, p1, v1}, Lcom/glympse/android/lib/fe;->removeInvite(Lcom/glympse/android/api/GInvite;Z)V

    move v0, v1

    .line 666
    goto :goto_0

    .line 670
    :cond_3
    iget-object v2, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/glympse/android/lib/fe;->gM:Ljava/lang/String;

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 673
    invoke-interface {p1}, Lcom/glympse/android/api/GInvite;->getCode()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 679
    iget-object v2, p0, Lcom/glympse/android/lib/fe;->lJ:Lcom/glympse/android/lib/GTicketProtocol;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GTicketPrivate;

    check-cast p1, Lcom/glympse/android/lib/GInvitePrivate;

    invoke-interface {v2, v0, p1}, Lcom/glympse/android/lib/GTicketProtocol;->deleteInvite(Lcom/glympse/android/lib/GTicketPrivate;Lcom/glympse/android/lib/GInvitePrivate;)V

    :goto_1
    move v0, v1

    .line 688
    goto :goto_0

    .line 685
    :cond_4
    invoke-virtual {p0, p1, v0}, Lcom/glympse/android/lib/fe;->removeInvite(Lcom/glympse/android/api/GInvite;Z)V

    goto :goto_1
.end method

.method public deleteTicket()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1149
    iget-boolean v1, p0, Lcom/glympse/android/lib/fe;->td:Z

    if-eqz v1, :cond_1

    .line 1170
    :cond_0
    :goto_0
    return v0

    .line 1155
    :cond_1
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v1, :cond_0

    .line 1162
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->gM:Ljava/lang/String;

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/16 v1, 0x20

    iget v2, p0, Lcom/glympse/android/lib/fe;->cg:I

    if-eq v1, v2, :cond_0

    .line 1168
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->lJ:Lcom/glympse/android/lib/GTicketProtocol;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GTicket;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GTicketProtocol;->deleteTicket(Lcom/glympse/android/api/GTicket;)V

    .line 1170
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public deriveContext(Lcom/glympse/android/api/GEventSink;)V
    .locals 1

    .prologue
    .line 2771
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1}, Lcom/glympse/android/lib/CommonSink;->deriveContext(Lcom/glympse/android/api/GEventSink;)V

    .line 2772
    return-void
.end method

.method public encode(Lcom/glympse/android/core/GPrimitive;I)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    .line 2506
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->gM:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2508
    const-string v0, "id"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/lib/fe;->gM:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2510
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->mw:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2512
    const-string v0, "code"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/lib/fe;->mw:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2514
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->ou:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2516
    const-string v0, "ref"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/lib/fe;->ou:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2518
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_name:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2520
    const-string v0, "name"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/lib/fe;->_name:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2522
    :cond_3
    const-string v0, "stal"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, Lcom/glympse/android/lib/fe;->tf:Z

    invoke-interface {p1, v0, v1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Z)V

    .line 2523
    const-string v0, "sts"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lcom/glympse/android/lib/fe;->hW:J

    invoke-interface {p1, v0, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 2524
    const-string v0, "ets"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lcom/glympse/android/lib/fe;->ih:J

    invoke-interface {p1, v0, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 2525
    const-string v0, "dur"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/glympse/android/lib/fe;->tj:I

    int-to-long v2, v1

    invoke-interface {p1, v0, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 2526
    const-string v0, "st"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/glympse/android/lib/fe;->cg:I

    int-to-long v2, v1

    invoke-interface {p1, v0, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 2527
    const-string v0, "cmpl"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, Lcom/glympse/android/lib/fe;->tn:Z

    invoke-interface {p1, v0, v1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Z)V

    .line 2528
    const-string v0, "lets"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lcom/glympse/android/lib/fe;->nG:J

    invoke-interface {p1, v0, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 2529
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->dq:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2531
    const-string v0, "msg"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/lib/fe;->dq:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2533
    :cond_4
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->tk:Lcom/glympse/android/api/GPlace;

    if-eqz v0, :cond_5

    .line 2535
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->tk:Lcom/glympse/android/api/GPlace;

    check-cast v0, Lcom/glympse/android/core/GPersistable;

    .line 2536
    new-instance v1, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v1, v5}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 2537
    invoke-interface {v0, v1, p2}, Lcom/glympse/android/core/GPersistable;->encode(Lcom/glympse/android/core/GPrimitive;I)V

    .line 2538
    const-string v0, "dst"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0, v1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 2542
    :cond_5
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->mD:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->length()I

    move-result v2

    .line 2543
    if-lez v2, :cond_7

    .line 2545
    new-instance v3, Lcom/glympse/android/lib/Primitive;

    const/4 v0, 0x1

    invoke-direct {v3, v0}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 2546
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_6

    .line 2548
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->mD:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GInvitePrivate;

    .line 2549
    new-instance v4, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v4, v5}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 2550
    invoke-interface {v0, v4, p2}, Lcom/glympse/android/lib/GInvitePrivate;->encode(Lcom/glympse/android/core/GPrimitive;I)V

    .line 2551
    invoke-interface {v3, v4}, Lcom/glympse/android/core/GPrimitive;->put(Lcom/glympse/android/core/GPrimitive;)V

    .line 2546
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2553
    :cond_6
    const-string v0, "recs"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 2557
    :cond_7
    const-string v0, "eta"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lcom/glympse/android/lib/fe;->tl:J

    invoke-interface {p1, v0, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 2558
    const-string v0, "etaTs"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lcom/glympse/android/lib/fe;->kg:J

    invoke-interface {p1, v0, v2, v3}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;J)V

    .line 2560
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->kl:Lcom/glympse/android/api/GTrack;

    if-eqz v0, :cond_8

    .line 2562
    new-instance v1, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v1, v5}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 2563
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->kl:Lcom/glympse/android/api/GTrack;

    check-cast v0, Lcom/glympse/android/lib/GTrackPrivate;

    .line 2564
    invoke-interface {v0, v1, p2}, Lcom/glympse/android/lib/GTrackPrivate;->encode(Lcom/glympse/android/core/GPrimitive;I)V

    .line 2565
    const-string v0, "route"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0, v1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 2569
    :cond_8
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->th:Lcom/glympse/android/api/GAppProfile;

    if-eqz v0, :cond_9

    .line 2571
    new-instance v0, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v0, v5}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 2572
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->th:Lcom/glympse/android/api/GAppProfile;

    invoke-interface {v1, v0, p2}, Lcom/glympse/android/api/GAppProfile;->encode(Lcom/glympse/android/core/GPrimitive;I)V

    .line 2573
    const-string v1, "owner"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1, v0}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 2576
    :cond_9
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->tm:Lcom/glympse/android/api/GTravelMode;

    if-eqz v0, :cond_a

    .line 2578
    new-instance v0, Lcom/glympse/android/lib/Primitive;

    invoke-direct {v0, v5}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 2579
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->tm:Lcom/glympse/android/api/GTravelMode;

    invoke-interface {v1, v0, p2}, Lcom/glympse/android/api/GTravelMode;->encode(Lcom/glympse/android/core/GPrimitive;I)V

    .line 2580
    const-string v1, "trMd"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1, v0}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 2582
    :cond_a
    return-void
.end method

.method public eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V
    .locals 6

    .prologue
    .line 2741
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/glympse/android/api/GEventSink;

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/glympse/android/lib/CommonSink;->eventsOccurred(Lcom/glympse/android/api/GEventSink;Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 2742
    return-void
.end method

.method public expire()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1033
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1, v1}, Lcom/glympse/android/lib/fe;->modify(ILjava/lang/String;Lcom/glympse/android/api/GPlace;)Z

    move-result v0

    return v0
.end method

.method public extend(I)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v0, 0x0

    .line 919
    iget-boolean v1, p0, Lcom/glympse/android/lib/fe;->td:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/glympse/android/lib/fe;->tr:I

    iget v2, p0, Lcom/glympse/android/lib/fe;->cg:I

    and-int/2addr v1, v2

    if-nez v1, :cond_0

    if-gtz p1, :cond_1

    .line 978
    :cond_0
    :goto_0
    return v0

    .line 927
    :cond_1
    const-wide/16 v2, 0x0

    iget-wide v4, p0, Lcom/glympse/android/lib/fe;->hW:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_2

    .line 931
    iget v0, p0, Lcom/glympse/android/lib/fe;->tj:I

    .line 975
    :goto_1
    add-int/2addr v0, p1

    .line 978
    invoke-virtual {p0, v0, v6, v6}, Lcom/glympse/android/lib/fe;->modify(ILjava/lang/String;Lcom/glympse/android/api/GPlace;)Z

    move-result v0

    goto :goto_0

    .line 933
    :cond_2
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->gM:Ljava/lang/String;

    if-nez v1, :cond_3

    .line 938
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v1, :cond_0

    .line 940
    iget-wide v0, p0, Lcom/glympse/android/lib/fe;->ih:J

    iget-object v2, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v2}, Lcom/glympse/android/lib/GGlympsePrivate;->getTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    long-to-int v0, v0

    goto :goto_1

    .line 950
    :cond_3
    iget-wide v2, p0, Lcom/glympse/android/lib/fe;->ih:J

    iget-wide v4, p0, Lcom/glympse/android/lib/fe;->hW:J

    sub-long/2addr v2, v4

    long-to-int v1, v2

    .line 951
    iget v2, p0, Lcom/glympse/android/lib/fe;->tj:I

    if-ne v2, v1, :cond_4

    .line 955
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v1, :cond_0

    .line 957
    iget-wide v0, p0, Lcom/glympse/android/lib/fe;->ih:J

    iget-object v2, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v2}, Lcom/glympse/android/lib/GGlympsePrivate;->getTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    long-to-int v0, v0

    goto :goto_1

    .line 970
    :cond_4
    iget v0, p0, Lcom/glympse/android/lib/fe;->tj:I

    goto :goto_1
.end method

.method public findInviteByAddress(Ljava/lang/String;)Lcom/glympse/android/api/GInvite;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 484
    invoke-static {p1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 498
    :cond_0
    :goto_0
    return-object v0

    .line 488
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->mD:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v3

    .line 489
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_3

    .line 491
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->mD:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, v2}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GInvite;

    .line 492
    invoke-interface {v0}, Lcom/glympse/android/api/GInvite;->getAddress()Ljava/lang/String;

    move-result-object v4

    .line 493
    invoke-static {v4}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 489
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 498
    goto :goto_0
.end method

.method public findInviteByCode(Ljava/lang/String;)Lcom/glympse/android/api/GInvite;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 465
    invoke-static {p1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 479
    :cond_0
    :goto_0
    return-object v0

    .line 469
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->mD:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v3

    .line 470
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_3

    .line 472
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->mD:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, v2}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GInvite;

    .line 473
    invoke-interface {v0}, Lcom/glympse/android/api/GInvite;->getCode()Ljava/lang/String;

    move-result-object v4

    .line 474
    invoke-static {v4}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 470
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 479
    goto :goto_0
.end method

.method public findInviteByType(I)Lcom/glympse/android/api/GInvite;
    .locals 4

    .prologue
    .line 503
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->mD:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v2

    .line 504
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 506
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->mD:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GInvite;

    .line 507
    invoke-interface {v0}, Lcom/glympse/android/api/GInvite;->getType()I

    move-result v3

    if-ne v3, p1, :cond_0

    .line 512
    :goto_1
    return-object v0

    .line 504
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 512
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public generateUri()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1253
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GTicket;

    iget-object v1, p0, Lcom/glympse/android/lib/fe;->tg:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/glympse/android/lib/UrlGenerator;->generateUri(Lcom/glympse/android/api/GTicket;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->mw:Ljava/lang/String;

    return-object v0
.end method

.method public getContext(J)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2756
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1, p2}, Lcom/glympse/android/lib/CommonSink;->getContext(J)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getContextKeys()Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Enumeration",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2766
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0}, Lcom/glympse/android/lib/CommonSink;->getContextKeys()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public getDestination()Lcom/glympse/android/api/GPlace;
    .locals 1

    .prologue
    .line 400
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->tk:Lcom/glympse/android/api/GPlace;

    return-object v0
.end method

.method public getDuration()I
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 373
    iget-wide v0, p0, Lcom/glympse/android/lib/fe;->hW:J

    cmp-long v0, v2, v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/glympse/android/lib/fe;->ih:J

    cmp-long v0, v2, v0

    if-eqz v0, :cond_0

    .line 375
    iget-wide v0, p0, Lcom/glympse/android/lib/fe;->ih:J

    iget-wide v2, p0, Lcom/glympse/android/lib/fe;->hW:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    .line 379
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/glympse/android/lib/fe;->tj:I

    goto :goto_0
.end method

.method public getDurationRaw()I
    .locals 1

    .prologue
    .line 1268
    iget v0, p0, Lcom/glympse/android/lib/fe;->tj:I

    return v0
.end method

.method public getEta()J
    .locals 6

    .prologue
    .line 422
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    iget-wide v2, p0, Lcom/glympse/android/lib/fe;->tl:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 425
    :cond_0
    iget-wide v0, p0, Lcom/glympse/android/lib/fe;->tl:J

    .line 430
    :goto_0
    return-wide v0

    :cond_1
    iget-wide v0, p0, Lcom/glympse/android/lib/fe;->tl:J

    iget-object v2, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v2}, Lcom/glympse/android/lib/GGlympsePrivate;->getTime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/glympse/android/lib/fe;->kg:J

    sub-long/2addr v2, v4

    sub-long/2addr v0, v2

    goto :goto_0
.end method

.method public getEtaRaw()J
    .locals 2

    .prologue
    .line 455
    iget-wide v0, p0, Lcom/glympse/android/lib/fe;->tl:J

    return-wide v0
.end method

.method public getEtaTs()J
    .locals 2

    .prologue
    .line 460
    iget-wide v0, p0, Lcom/glympse/android/lib/fe;->kg:J

    return-wide v0
.end method

.method public getExpireTime()J
    .locals 2

    .prologue
    .line 390
    iget-wide v0, p0, Lcom/glympse/android/lib/fe;->ih:J

    return-wide v0
.end method

.method public getGlympse()Lcom/glympse/android/lib/GGlympsePrivate;
    .locals 1

    .prologue
    .line 2029
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->gM:Ljava/lang/String;

    return-object v0
.end method

.method public getInvites()Lcom/glympse/android/core/GArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/glympse/android/core/GArray",
            "<",
            "Lcom/glympse/android/api/GInvite;",
            ">;"
        }
    .end annotation

    .prologue
    .line 368
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->mD:Lcom/glympse/android/hal/GVector;

    return-object v0
.end method

.method public getListeners()Lcom/glympse/android/core/GArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/glympse/android/core/GArray",
            "<",
            "Lcom/glympse/android/api/GEventListener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2736
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0}, Lcom/glympse/android/lib/CommonSink;->getListeners()Lcom/glympse/android/core/GArray;

    move-result-object v0

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 395
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->dq:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1238
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_name:Ljava/lang/String;

    return-object v0
.end method

.method public getNext()J
    .locals 2

    .prologue
    .line 1263
    iget-wide v0, p0, Lcom/glympse/android/lib/fe;->mM:J

    return-wide v0
.end method

.method public getOwner()Lcom/glympse/android/api/GAppProfile;
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->th:Lcom/glympse/android/api/GAppProfile;

    return-object v0
.end method

.method public getParent()Lcom/glympse/android/lib/GTicketParent;
    .locals 1

    .prologue
    .line 2034
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->ts:Lcom/glympse/android/lib/GTicketParent;

    return-object v0
.end method

.method public getPartnerData(J)Lcom/glympse/android/core/GMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lcom/glympse/android/core/GMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/glympse/android/api/GDataRow;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1426
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->to:Lcom/glympse/android/hal/GHashtable;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/hal/GHashtable;

    .line 1427
    return-object v0
.end method

.method public getPartnerIds()Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Enumeration",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1421
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->to:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GHashtable;->keys()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public getProperties()Lcom/glympse/android/hal/GHashtable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/glympse/android/hal/GHashtable",
            "<",
            "Ljava/lang/Long;",
            "Lcom/glympse/android/hal/GHashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/glympse/android/api/GDataRow;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1723
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->to:Lcom/glympse/android/hal/GHashtable;

    return-object v0
.end method

.method public getProperty(JLjava/lang/String;)Lcom/glympse/android/core/GPrimitive;
    .locals 1

    .prologue
    .line 1087
    invoke-virtual {p0, p1, p2, p3}, Lcom/glympse/android/lib/fe;->getPropertyData(JLjava/lang/String;)Lcom/glympse/android/api/GDataRow;

    move-result-object v0

    .line 1088
    if-nez v0, :cond_0

    .line 1090
    const/4 v0, 0x0

    .line 1094
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, Lcom/glympse/android/api/GDataRow;->getValue()Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    goto :goto_0
.end method

.method public getPropertyData(JLjava/lang/String;)Lcom/glympse/android/api/GDataRow;
    .locals 3

    .prologue
    .line 1100
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->to:Lcom/glympse/android/hal/GHashtable;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/hal/GHashtable;

    .line 1101
    if-nez v0, :cond_0

    .line 1103
    const/4 v0, 0x0

    .line 1107
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0, p3}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GDataRow;

    goto :goto_0
.end method

.method public getProviderData()Lcom/glympse/android/core/GPrimitive;
    .locals 1

    .prologue
    .line 326
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->ti:Lcom/glympse/android/core/GPrimitive;

    return-object v0
.end method

.method public getProviderId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 321
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->so:Ljava/lang/String;

    return-object v0
.end method

.method public getReference()Ljava/lang/String;
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->ou:Ljava/lang/String;

    return-object v0
.end method

.method public getRequestNoReply()Z
    .locals 1

    .prologue
    .line 735
    iget-boolean v0, p0, Lcom/glympse/android/lib/fe;->tw:Z

    return v0
.end method

.method public getRequestScenario()Ljava/lang/String;
    .locals 1

    .prologue
    .line 751
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->tx:Ljava/lang/String;

    return-object v0
.end method

.method public getRequestTicket()Lcom/glympse/android/api/GTicket;
    .locals 1

    .prologue
    .line 719
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->oB:Lcom/glympse/android/api/GTicket;

    return-object v0
.end method

.method public getRoute()Lcom/glympse/android/api/GTrack;
    .locals 1

    .prologue
    .line 445
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->kl:Lcom/glympse/android/api/GTrack;

    return-object v0
.end method

.method public getSource()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1248
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->tg:Ljava/lang/String;

    return-object v0
.end method

.method public getStartTime()J
    .locals 2

    .prologue
    .line 385
    iget-wide v0, p0, Lcom/glympse/android/lib/fe;->hW:J

    return-wide v0
.end method

.method public getState()I
    .locals 1

    .prologue
    .line 311
    iget v0, p0, Lcom/glympse/android/lib/fe;->cg:I

    return v0
.end method

.method public getTrack()Lcom/glympse/android/api/GTrack;
    .locals 1

    .prologue
    .line 405
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->pr:Lcom/glympse/android/lib/GTrackPrivate;

    return-object v0
.end method

.method public getTravelMode()Lcom/glympse/android/api/GTravelMode;
    .locals 1

    .prologue
    .line 450
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->tm:Lcom/glympse/android/api/GTravelMode;

    return-object v0
.end method

.method public getUser()Lcom/glympse/android/api/GUser;
    .locals 2

    .prologue
    .line 333
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-nez v0, :cond_0

    .line 335
    const/4 v0, 0x0

    .line 346
    :goto_0
    return-object v0

    .line 340
    :cond_0
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->ts:Lcom/glympse/android/lib/GTicketParent;

    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getHistoryManager()Lcom/glympse/android/api/GHistoryManager;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GTicketParent;

    if-ne v1, v0, :cond_1

    .line 342
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getUserManager()Lcom/glympse/android/api/GUserManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/api/GUserManager;->getSelf()Lcom/glympse/android/api/GUser;

    move-result-object v0

    goto :goto_0

    .line 346
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->ts:Lcom/glympse/android/lib/GTicketParent;

    check-cast v0, Lcom/glympse/android/api/GUser;

    goto :goto_0
.end method

.method public getVisibility()Lcom/glympse/android/core/GPrimitive;
    .locals 3

    .prologue
    .line 279
    const-wide/16 v0, 0x0

    const-string v2, "visibility"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/glympse/android/lib/fe;->getProperty(JLjava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 280
    if-eqz v0, :cond_0

    .line 282
    invoke-interface {v0}, Lcom/glympse/android/core/GPrimitive;->clone()Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 285
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getXoaRegion()Lcom/glympse/android/core/GRegion;
    .locals 1

    .prologue
    .line 2120
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->tv:Lcom/glympse/android/core/GRegion;

    return-object v0
.end method

.method public hasContext(J)Z
    .locals 1

    .prologue
    .line 2746
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1, p2}, Lcom/glympse/android/lib/CommonSink;->hasContext(J)Z

    move-result v0

    return v0
.end method

.method public isActive()Z
    .locals 1

    .prologue
    .line 351
    iget v0, p0, Lcom/glympse/android/lib/fe;->cg:I

    and-int/lit8 v0, v0, 0x12

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/glympse/android/lib/fe;->te:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCompleted()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 356
    iget-boolean v1, p0, Lcom/glympse/android/lib/fe;->tn:Z

    if-eqz v1, :cond_1

    .line 361
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/16 v1, 0x40

    iget v2, p0, Lcom/glympse/android/lib/fe;->cg:I

    if-ne v1, v2, :cond_2

    iget-wide v2, p0, Lcom/glympse/android/lib/fe;->ih:J

    const-wide/32 v4, 0x1d4c0

    add-long/2addr v2, v4

    .line 363
    invoke-direct {p0}, Lcom/glympse/android/lib/fe;->getTime()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCompletedRaw()Z
    .locals 1

    .prologue
    .line 1457
    iget-boolean v0, p0, Lcom/glympse/android/lib/fe;->tn:Z

    return v0
.end method

.method public isEqual(Lcom/glympse/android/core/GCommon;)Z
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2419
    move-object v0, p1

    check-cast v0, Lcom/glympse/android/lib/fe;

    .line 2420
    if-nez v0, :cond_0

    move v0, v3

    .line 2496
    :goto_0
    return v0

    .line 2426
    :cond_0
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-ne v1, p1, :cond_1

    move v0, v4

    .line 2428
    goto :goto_0

    .line 2432
    :cond_1
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->dq:Ljava/lang/String;

    iget-object v2, v0, Lcom/glympse/android/lib/fe;->dq:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/glympse/android/hal/Helpers;->safeEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    move v0, v3

    .line 2434
    goto :goto_0

    .line 2438
    :cond_2
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->tk:Lcom/glympse/android/api/GPlace;

    if-eqz v1, :cond_3

    .line 2441
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->tk:Lcom/glympse/android/api/GPlace;

    iget-object v2, v0, Lcom/glympse/android/lib/fe;->tk:Lcom/glympse/android/api/GPlace;

    invoke-interface {v1, v2}, Lcom/glympse/android/api/GPlace;->isEqual(Lcom/glympse/android/core/GCommon;)Z

    move-result v1

    if-nez v1, :cond_4

    move v0, v3

    .line 2443
    goto :goto_0

    .line 2446
    :cond_3
    iget-object v1, v0, Lcom/glympse/android/lib/fe;->tk:Lcom/glympse/android/api/GPlace;

    if-eqz v1, :cond_4

    move v0, v3

    .line 2449
    goto :goto_0

    .line 2457
    :cond_4
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->mD:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v1}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v7

    .line 2458
    iget-object v1, v0, Lcom/glympse/android/lib/fe;->mD:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v1}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v8

    .line 2459
    if-eq v7, v8, :cond_5

    move v0, v3

    .line 2461
    goto :goto_0

    :cond_5
    move v6, v3

    move v1, v4

    .line 2469
    :goto_1
    if-ge v6, v7, :cond_9

    .line 2471
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->mD:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v1, v6}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/glympse/android/api/GInvite;

    move v5, v3

    .line 2475
    :goto_2
    if-ge v5, v8, :cond_8

    .line 2477
    iget-object v2, v0, Lcom/glympse/android/lib/fe;->mD:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v2, v5}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/glympse/android/api/GInvite;

    .line 2480
    invoke-interface {v1, v2}, Lcom/glympse/android/api/GInvite;->isEqual(Lcom/glympse/android/core/GCommon;)Z

    move-result v2

    if-eqz v2, :cond_6

    move v1, v4

    .line 2488
    :goto_3
    if-nez v1, :cond_7

    move v0, v1

    .line 2490
    goto :goto_0

    .line 2475
    :cond_6
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_2

    .line 2469
    :cond_7
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_1

    :cond_8
    move v1, v3

    goto :goto_3

    :cond_9
    move v0, v1

    goto :goto_0
.end method

.method public isMine()Z
    .locals 1

    .prologue
    .line 269
    iget-boolean v0, p0, Lcom/glympse/android/lib/fe;->te:Z

    return v0
.end method

.method public isSibling()Z
    .locals 1

    .prologue
    .line 274
    iget-boolean v0, p0, Lcom/glympse/android/lib/fe;->te:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSomeoneWatching()Z
    .locals 4

    .prologue
    .line 695
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->lX:Lcom/glympse/android/lib/GCorrectedTime;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/fe;->lX:Lcom/glympse/android/lib/GCorrectedTime;

    invoke-interface {v0}, Lcom/glympse/android/lib/GCorrectedTime;->getTime()J

    move-result-wide v0

    .line 698
    :goto_0
    iget-wide v2, p0, Lcom/glympse/android/lib/fe;->nt:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 695
    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0

    .line 698
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public isStandalone()Z
    .locals 1

    .prologue
    .line 1228
    iget-boolean v0, p0, Lcom/glympse/android/lib/fe;->tf:Z

    return v0
.end method

.method public isVisible()Z
    .locals 2

    .prologue
    .line 290
    invoke-virtual {p0}, Lcom/glympse/android/lib/fe;->getVisibility()Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 291
    if-nez v0, :cond_0

    .line 293
    const/4 v0, 0x1

    .line 296
    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lcom/glympse/android/api/GC;->TICKET_VISIBILITY_KEY_LOCATION()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/glympse/android/api/GC;->TICKET_VISIBILITY_LOCATION_VISIBLE()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/glympse/android/hal/Helpers;->safeEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public isWatching()Z
    .locals 1

    .prologue
    .line 264
    iget-boolean v0, p0, Lcom/glympse/android/lib/fe;->td:Z

    return v0
.end method

.method public merge(Lcom/glympse/android/lib/GTicketPrivate;Lcom/glympse/android/lib/GGlympsePrivate;ZZ)V
    .locals 11

    .prologue
    .line 1516
    const/4 v0, 0x0

    .line 1519
    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->isMine()Z

    move-result v1

    .line 1520
    iget-boolean v2, p0, Lcom/glympse/android/lib/fe;->te:Z

    if-eq v1, v2, :cond_0

    .line 1522
    invoke-virtual {p0, v1}, Lcom/glympse/android/lib/fe;->setMine(Z)V

    .line 1523
    const/high16 v0, 0x1000000

    .line 1527
    :cond_0
    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getExpireTime()J

    move-result-wide v2

    .line 1528
    iget-wide v4, p0, Lcom/glympse/android/lib/fe;->ih:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_18

    .line 1530
    const/4 v1, 0x1

    invoke-virtual {p0, v2, v3, v1}, Lcom/glympse/android/lib/fe;->setExpireTime(JZ)V

    .line 1531
    or-int/lit8 v1, v0, 0x10

    .line 1534
    :goto_0
    if-eqz p3, :cond_17

    .line 1537
    invoke-virtual {p0}, Lcom/glympse/android/lib/fe;->removeAllInvites()V

    .line 1540
    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getInvites()Lcom/glympse/android/core/GArray;

    move-result-object v3

    .line 1541
    invoke-interface {v3}, Lcom/glympse/android/core/GArray;->length()I

    move-result v4

    .line 1542
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_1

    .line 1544
    invoke-interface {v3, v2}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GInvite;

    .line 1545
    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/fe;->addInviteCore(Lcom/glympse/android/api/GInvite;)V

    .line 1542
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1549
    :cond_1
    or-int/lit16 v0, v1, 0x4000

    .line 1552
    :goto_2
    if-eqz p4, :cond_16

    .line 1555
    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 1556
    iget-object v2, p0, Lcom/glympse/android/lib/fe;->dq:Ljava/lang/String;

    invoke-static {v2, v1}, Lcom/glympse/android/hal/Helpers;->safeEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1558
    iput-object v1, p0, Lcom/glympse/android/lib/fe;->dq:Ljava/lang/String;

    .line 1559
    or-int/lit8 v0, v0, 0x20

    .line 1562
    :cond_2
    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getDestination()Lcom/glympse/android/api/GPlace;

    move-result-object v1

    .line 1563
    if-nez v1, :cond_a

    .line 1565
    iget-object v2, p0, Lcom/glympse/android/lib/fe;->tk:Lcom/glympse/android/api/GPlace;

    if-eqz v2, :cond_3

    .line 1567
    iput-object v1, p0, Lcom/glympse/android/lib/fe;->tk:Lcom/glympse/android/api/GPlace;

    .line 1568
    or-int/lit8 v0, v0, 0x40

    .line 1588
    :cond_3
    :goto_3
    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getOwner()Lcom/glympse/android/api/GAppProfile;

    move-result-object v1

    .line 1589
    if-nez v1, :cond_c

    .line 1591
    iget-object v2, p0, Lcom/glympse/android/lib/fe;->th:Lcom/glympse/android/api/GAppProfile;

    if-eqz v2, :cond_4

    .line 1593
    invoke-virtual {p0, v1}, Lcom/glympse/android/lib/fe;->setOwner(Lcom/glympse/android/api/GAppProfile;)V

    .line 1594
    const/high16 v1, 0x1000000

    or-int/2addr v0, v1

    .line 1614
    :cond_4
    :goto_4
    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getTravelMode()Lcom/glympse/android/api/GTravelMode;

    move-result-object v1

    .line 1615
    if-nez v1, :cond_e

    .line 1617
    iget-object v2, p0, Lcom/glympse/android/lib/fe;->tm:Lcom/glympse/android/api/GTravelMode;

    if-eqz v2, :cond_5

    .line 1619
    invoke-virtual {p0, v1}, Lcom/glympse/android/lib/fe;->setTravelMode(Lcom/glympse/android/api/GTravelMode;)V

    .line 1620
    or-int/lit16 v0, v0, 0x400

    .line 1640
    :cond_5
    :goto_5
    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->isVisible()Z

    move-result v1

    invoke-virtual {p0}, Lcom/glympse/android/lib/fe;->isVisible()Z

    move-result v2

    if-eq v1, v2, :cond_15

    .line 1642
    or-int/lit8 v0, v0, 0x8

    move v2, v0

    .line 1645
    :goto_6
    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->isCompletedRaw()Z

    move-result v0

    .line 1646
    iget-boolean v1, p0, Lcom/glympse/android/lib/fe;->tn:Z

    if-nez v1, :cond_6

    iget-boolean v1, p0, Lcom/glympse/android/lib/fe;->tn:Z

    if-eq v0, v1, :cond_6

    .line 1648
    invoke-virtual {p0}, Lcom/glympse/android/lib/fe;->setCompleted()V

    .line 1652
    :cond_6
    new-instance v3, Lcom/glympse/android/hal/GVector;

    invoke-direct {v3}, Lcom/glympse/android/hal/GVector;-><init>()V

    .line 1653
    new-instance v4, Lcom/glympse/android/hal/GVector;

    invoke-direct {v4}, Lcom/glympse/android/hal/GVector;-><init>()V

    .line 1656
    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getPartnerIds()Ljava/util/Enumeration;

    move-result-object v5

    :cond_7
    invoke-interface {v5}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1658
    invoke-interface {v5}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 1660
    invoke-interface {p1, v6, v7}, Lcom/glympse/android/lib/GTicketPrivate;->getPartnerData(J)Lcom/glympse/android/core/GMap;

    move-result-object v8

    .line 1661
    if-eqz v8, :cond_7

    .line 1667
    invoke-interface {v8}, Lcom/glympse/android/core/GMap;->keys()Ljava/util/Enumeration;

    move-result-object v9

    :cond_8
    :goto_7
    invoke-interface {v9}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1669
    invoke-interface {v9}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1670
    invoke-interface {v8, v0}, Lcom/glympse/android/core/GMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/glympse/android/api/GDataRow;

    .line 1672
    invoke-virtual {p0, v6, v7, v0}, Lcom/glympse/android/lib/fe;->getPropertyData(JLjava/lang/String;)Lcom/glympse/android/api/GDataRow;

    move-result-object v0

    .line 1675
    if-eqz v0, :cond_9

    invoke-interface {v0}, Lcom/glympse/android/api/GDataRow;->getValue()Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    invoke-interface {v1}, Lcom/glympse/android/api/GDataRow;->getValue()Lcom/glympse/android/core/GPrimitive;

    move-result-object v10

    invoke-interface {v0, v10}, Lcom/glympse/android/core/GPrimitive;->isEqual(Lcom/glympse/android/core/GCommon;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1678
    :cond_9
    const/16 v0, 0x40

    invoke-interface {v1}, Lcom/glympse/android/api/GDataRow;->getValue()Lcom/glympse/android/core/GPrimitive;

    move-result-object v10

    invoke-interface {v10}, Lcom/glympse/android/core/GPrimitive;->type()I

    move-result v10

    if-ne v0, v10, :cond_10

    .line 1680
    invoke-virtual {v4, v1}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    goto :goto_7

    .line 1573
    :cond_a
    iget-object v2, p0, Lcom/glympse/android/lib/fe;->tk:Lcom/glympse/android/api/GPlace;

    if-eqz v2, :cond_b

    .line 1575
    iget-object v2, p0, Lcom/glympse/android/lib/fe;->tk:Lcom/glympse/android/api/GPlace;

    invoke-interface {v1, v2}, Lcom/glympse/android/api/GPlace;->isEqual(Lcom/glympse/android/core/GCommon;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1577
    iput-object v1, p0, Lcom/glympse/android/lib/fe;->tk:Lcom/glympse/android/api/GPlace;

    .line 1578
    or-int/lit8 v0, v0, 0x40

    goto/16 :goto_3

    .line 1583
    :cond_b
    iput-object v1, p0, Lcom/glympse/android/lib/fe;->tk:Lcom/glympse/android/api/GPlace;

    .line 1584
    or-int/lit8 v0, v0, 0x40

    goto/16 :goto_3

    .line 1599
    :cond_c
    iget-object v2, p0, Lcom/glympse/android/lib/fe;->th:Lcom/glympse/android/api/GAppProfile;

    if-eqz v2, :cond_d

    .line 1601
    iget-object v2, p0, Lcom/glympse/android/lib/fe;->th:Lcom/glympse/android/api/GAppProfile;

    invoke-interface {v1, v2}, Lcom/glympse/android/api/GAppProfile;->isEqual(Lcom/glympse/android/core/GCommon;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1603
    invoke-virtual {p0, v1}, Lcom/glympse/android/lib/fe;->setOwner(Lcom/glympse/android/api/GAppProfile;)V

    .line 1604
    const/high16 v1, 0x1000000

    or-int/2addr v0, v1

    goto/16 :goto_4

    .line 1609
    :cond_d
    invoke-virtual {p0, v1}, Lcom/glympse/android/lib/fe;->setOwner(Lcom/glympse/android/api/GAppProfile;)V

    .line 1610
    const/high16 v1, 0x1000000

    or-int/2addr v0, v1

    goto/16 :goto_4

    .line 1625
    :cond_e
    iget-object v2, p0, Lcom/glympse/android/lib/fe;->tm:Lcom/glympse/android/api/GTravelMode;

    if-eqz v2, :cond_f

    .line 1627
    iget-object v2, p0, Lcom/glympse/android/lib/fe;->tm:Lcom/glympse/android/api/GTravelMode;

    invoke-interface {v1, v2}, Lcom/glympse/android/api/GTravelMode;->isEqual(Lcom/glympse/android/core/GCommon;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 1629
    invoke-virtual {p0, v1}, Lcom/glympse/android/lib/fe;->setTravelMode(Lcom/glympse/android/api/GTravelMode;)V

    .line 1630
    or-int/lit16 v0, v0, 0x400

    goto/16 :goto_5

    .line 1635
    :cond_f
    invoke-virtual {p0, v1}, Lcom/glympse/android/lib/fe;->setTravelMode(Lcom/glympse/android/api/GTravelMode;)V

    .line 1636
    or-int/lit16 v0, v0, 0x400

    goto/16 :goto_5

    .line 1684
    :cond_10
    invoke-virtual {v3, v1}, Lcom/glympse/android/hal/GVector;->addElement(Ljava/lang/Object;)V

    goto/16 :goto_7

    .line 1691
    :cond_11
    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getProperties()Lcom/glympse/android/hal/GHashtable;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/fe;->to:Lcom/glympse/android/hal/GHashtable;

    .line 1694
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GTicket;

    .line 1695
    invoke-virtual {v3}, Lcom/glympse/android/hal/GVector;->length()I

    move-result v1

    if-lez v1, :cond_12

    .line 1697
    new-instance v1, Lcom/glympse/android/lib/ai;

    invoke-direct {v1, v0, v3}, Lcom/glympse/android/lib/ai;-><init>(Lcom/glympse/android/api/GTicket;Lcom/glympse/android/hal/GVector;)V

    .line 1698
    iget-object v3, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/4 v5, 0x4

    const/high16 v6, 0x400000

    invoke-virtual {p0, v3, v5, v6, v1}, Lcom/glympse/android/lib/fe;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 1700
    :cond_12
    invoke-virtual {v4}, Lcom/glympse/android/hal/GVector;->length()I

    move-result v1

    if-lez v1, :cond_13

    .line 1702
    new-instance v1, Lcom/glympse/android/lib/ai;

    invoke-direct {v1, v0, v4}, Lcom/glympse/android/lib/ai;-><init>(Lcom/glympse/android/api/GTicket;Lcom/glympse/android/hal/GVector;)V

    .line 1703
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/4 v3, 0x4

    const/high16 v4, 0x800000

    invoke-virtual {p0, v0, v3, v4, v1}, Lcom/glympse/android/lib/fe;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 1708
    :cond_13
    :goto_8
    if-eqz v2, :cond_14

    .line 1710
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GTicket;

    .line 1711
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/4 v3, 0x4

    invoke-virtual {p0, v1, v3, v2, v0}, Lcom/glympse/android/lib/fe;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 1712
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget-object v2, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/4 v3, 0x1

    const/high16 v4, 0x80000

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/glympse/android/lib/GGlympsePrivate;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 1714
    :cond_14
    return-void

    :cond_15
    move v2, v0

    goto/16 :goto_6

    :cond_16
    move v2, v0

    goto :goto_8

    :cond_17
    move v0, v1

    goto/16 :goto_2

    :cond_18
    move v1, v0

    goto/16 :goto_0
.end method

.method public modify(ILjava/lang/String;Lcom/glympse/android/api/GPlace;)Z
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v3, 0x1

    const/4 v8, -0x1

    const/4 v1, 0x0

    .line 820
    iget-boolean v0, p0, Lcom/glympse/android/lib/fe;->td:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/glympse/android/lib/fe;->tr:I

    iget v2, p0, Lcom/glympse/android/lib/fe;->cg:I

    and-int/2addr v0, v2

    if-eqz v0, :cond_1

    .line 913
    :cond_0
    :goto_0
    return v1

    .line 832
    :cond_1
    if-eq v8, p1, :cond_c

    .line 835
    if-ltz p1, :cond_0

    .line 842
    const/16 v0, 0x10

    .line 843
    iput p1, p0, Lcom/glympse/android/lib/fe;->tj:I

    .line 845
    :goto_1
    invoke-static {p2}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 847
    or-int/lit8 v0, v0, 0x20

    .line 848
    iput-object p2, p0, Lcom/glympse/android/lib/fe;->dq:Ljava/lang/String;

    .line 850
    :cond_2
    if-eqz p3, :cond_3

    .line 856
    or-int/lit8 v0, v0, 0x40

    .line 857
    invoke-virtual {p0, p3}, Lcom/glympse/android/lib/fe;->setDestination(Lcom/glympse/android/api/GPlace;)V

    :cond_3
    move v2, v0

    .line 861
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-nez v0, :cond_4

    move v1, v3

    .line 863
    goto :goto_0

    .line 867
    :cond_4
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GTicketPrivate;

    .line 870
    if-eq v8, p1, :cond_6

    .line 872
    if-nez p1, :cond_5

    .line 874
    invoke-direct {p0, v0}, Lcom/glympse/android/lib/fe;->e(Lcom/glympse/android/api/GTicket;)V

    .line 877
    :cond_5
    iget v4, p0, Lcom/glympse/android/lib/fe;->tj:I

    iget-object v5, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v5}, Lcom/glympse/android/lib/GGlympsePrivate;->getConfig()Lcom/glympse/android/api/GConfig;

    move-result-object v5

    invoke-interface {v5}, Lcom/glympse/android/api/GConfig;->getMaximumTicketDuration()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    iput v4, p0, Lcom/glympse/android/lib/fe;->tj:I

    .line 878
    iget-object v4, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v4}, Lcom/glympse/android/lib/GGlympsePrivate;->getTime()J

    move-result-wide v4

    iget v6, p0, Lcom/glympse/android/lib/fe;->tj:I

    int-to-long v6, v6

    add-long/2addr v4, v6

    invoke-virtual {p0, v4, v5, v1}, Lcom/glympse/android/lib/fe;->setExpireTime(JZ)V

    .line 882
    :cond_6
    if-eqz p3, :cond_7

    iget-object v1, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->getEtaMode()I

    move-result v1

    const/4 v4, 0x2

    if-ne v1, v4, :cond_7

    .line 884
    or-int/lit16 v2, v2, 0x300

    .line 885
    invoke-virtual {p0, v10, v11, v10, v11}, Lcom/glympse/android/lib/fe;->setEta(JJ)V

    .line 886
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/glympse/android/lib/fe;->setRoute(Lcom/glympse/android/api/GTrack;)V

    .line 890
    :cond_7
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->gM:Ljava/lang/String;

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 893
    if-nez p1, :cond_b

    .line 895
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->lJ:Lcom/glympse/android/lib/GTicketProtocol;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GTicketProtocol;->expireTicket(Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 903
    :cond_8
    :goto_2
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->lJ:Lcom/glympse/android/lib/GTicketProtocol;

    invoke-interface {v1, v0, p2, p3}, Lcom/glympse/android/lib/GTicketProtocol;->updateTicket(Lcom/glympse/android/api/GTicket;Ljava/lang/String;Lcom/glympse/android/api/GPlace;)V

    .line 907
    :cond_9
    if-eqz v2, :cond_a

    .line 909
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/4 v4, 0x4

    invoke-virtual {p0, v1, v4, v2, v0}, Lcom/glympse/android/lib/fe;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 910
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget-object v2, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/high16 v4, 0x80000

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/glympse/android/lib/GGlympsePrivate;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    :cond_a
    move v1, v3

    .line 913
    goto/16 :goto_0

    .line 897
    :cond_b
    if-eq v8, p1, :cond_8

    .line 899
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->lJ:Lcom/glympse/android/lib/GTicketProtocol;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GTicketProtocol;->updateTicket(Lcom/glympse/android/lib/GTicketPrivate;)V

    goto :goto_2

    :cond_c
    move v0, v1

    goto/16 :goto_1
.end method

.method public own()Z
    .locals 2

    .prologue
    .line 1113
    iget-boolean v0, p0, Lcom/glympse/android/lib/fe;->td:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/glympse/android/lib/fe;->te:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/fe;->gM:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/glympse/android/lib/fe;->cg:I

    and-int/lit8 v0, v0, 0x12

    if-nez v0, :cond_1

    .line 1116
    :cond_0
    const/4 v0, 0x0

    .line 1122
    :goto_0
    return v0

    .line 1120
    :cond_1
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->lJ:Lcom/glympse/android/lib/GTicketProtocol;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GTicketPrivate;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GTicketProtocol;->ownTicket(Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 1122
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public removeAllInvites()V
    .locals 2

    .prologue
    .line 2080
    .line 2081
    :goto_0
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->mD:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 2083
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->mD:Lcom/glympse/android/hal/GVector;

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GInvite;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/glympse/android/lib/fe;->removeInvite(Lcom/glympse/android/api/GInvite;Z)V

    goto :goto_0

    .line 2085
    :cond_0
    return-void
.end method

.method public removeInvite(Lcom/glympse/android/api/GInvite;Z)V
    .locals 4

    .prologue
    .line 2047
    move-object v0, p1

    check-cast v0, Lcom/glympse/android/lib/GInvitePrivate;

    .line 2048
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GInvitePrivate;->setGlympse(Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 2049
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->mD:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, p1}, Lcom/glympse/android/hal/GVector;->removeElement(Ljava/lang/Object;)Z

    .line 2052
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->ts:Lcom/glympse/android/lib/GTicketParent;

    if-eqz v0, :cond_0

    .line 2054
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->ts:Lcom/glympse/android/lib/GTicketParent;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GTicket;

    invoke-interface {v1, v0, p1}, Lcom/glympse/android/lib/GTicketParent;->inviteRemoved(Lcom/glympse/android/api/GTicket;Lcom/glympse/android/api/GInvite;)V

    .line 2058
    :cond_0
    if-eqz p2, :cond_1

    .line 2061
    invoke-virtual {p0}, Lcom/glympse/android/lib/fe;->updateWatchingState()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2064
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_1

    .line 2066
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/4 v1, 0x4

    const/16 v2, 0x4000

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/glympse/android/lib/fe;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 2076
    :cond_1
    return-void
.end method

.method public removeListener(Lcom/glympse/android/api/GEventListener;)Z
    .locals 1

    .prologue
    .line 2731
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->he:Lcom/glympse/android/lib/CommonSink;

    invoke-virtual {v0, p1}, Lcom/glympse/android/lib/CommonSink;->removeListener(Lcom/glympse/android/api/GEventListener;)Z

    move-result v0

    return v0
.end method

.method public setActive(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1789
    if-eqz p1, :cond_1

    .line 1791
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getTime()J

    move-result-wide v0

    .line 1793
    iget-boolean v2, p0, Lcom/glympse/android/lib/fe;->td:Z

    if-nez v2, :cond_0

    .line 1796
    invoke-virtual {p0, v0, v1}, Lcom/glympse/android/lib/fe;->updateState(J)Z

    move-result v2

    .line 1799
    invoke-direct {p0, p1, v2}, Lcom/glympse/android/lib/fe;->b(ZZ)V

    .line 1805
    :cond_0
    invoke-direct {p0, v3, v0, v1}, Lcom/glympse/android/lib/fe;->a(ZJ)V

    .line 1818
    :goto_0
    return-void

    .line 1809
    :cond_1
    iget-boolean v0, p0, Lcom/glympse/android/lib/fe;->td:Z

    if-nez v0, :cond_2

    .line 1812
    invoke-direct {p0, p1, v3}, Lcom/glympse/android/lib/fe;->b(ZZ)V

    .line 1816
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/fe;->f(Lcom/glympse/android/lib/GGlympsePrivate;)V

    goto :goto_0
.end method

.method public setCode(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1213
    iput-object p1, p0, Lcom/glympse/android/lib/fe;->mw:Ljava/lang/String;

    .line 1214
    return-void
.end method

.method public setCompleted()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1432
    iget-boolean v0, p0, Lcom/glympse/android/lib/fe;->tn:Z

    if-eqz v0, :cond_1

    .line 1453
    :cond_0
    :goto_0
    return-void

    .line 1436
    :cond_1
    iput-boolean v2, p0, Lcom/glympse/android/lib/fe;->tn:Z

    .line 1438
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_0

    .line 1444
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/fe;->f(Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 1447
    const/high16 v0, 0x4000000

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/fe;->z(I)V

    .line 1450
    invoke-direct {p0}, Lcom/glympse/android/lib/fe;->save()V

    .line 1452
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[Ticket.setCompleted] Ticket completed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/lib/fe;->gM:Ljava/lang/String;

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->safeStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public setDestination(Lcom/glympse/android/api/GPlace;)V
    .locals 0

    .prologue
    .line 1305
    iput-object p1, p0, Lcom/glympse/android/lib/fe;->tk:Lcom/glympse/android/api/GPlace;

    .line 1306
    return-void
.end method

.method public setDuration(I)V
    .locals 1

    .prologue
    .line 1283
    const/4 v0, -0x1

    if-ne v0, p1, :cond_0

    .line 1296
    :goto_0
    return-void

    .line 1289
    :cond_0
    if-gez p1, :cond_1

    .line 1291
    const/4 p1, 0x0

    .line 1295
    :cond_1
    iput p1, p0, Lcom/glympse/android/lib/fe;->tj:I

    goto :goto_0
.end method

.method public setEta(JJ)V
    .locals 1

    .prologue
    .line 1311
    iput-wide p3, p0, Lcom/glympse/android/lib/fe;->tl:J

    .line 1312
    iput-wide p1, p0, Lcom/glympse/android/lib/fe;->kg:J

    .line 1313
    return-void
.end method

.method public setEtaTs(J)V
    .locals 1

    .prologue
    .line 1318
    iput-wide p1, p0, Lcom/glympse/android/lib/fe;->kg:J

    .line 1319
    return-void
.end method

.method public setExpireTime(JZ)V
    .locals 7

    .prologue
    .line 1984
    iget-wide v0, p0, Lcom/glympse/android/lib/fe;->nG:J

    cmp-long v0, v0, p1

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 1985
    :goto_0
    iput-wide p1, p0, Lcom/glympse/android/lib/fe;->nG:J

    .line 1987
    if-eqz p3, :cond_0

    .line 1989
    iput-wide p1, p0, Lcom/glympse/android/lib/fe;->ih:J

    .line 1990
    const-wide/16 v2, 0x0

    iget-wide v4, p0, Lcom/glympse/android/lib/fe;->hW:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 1992
    iget-wide v2, p0, Lcom/glympse/android/lib/fe;->ih:J

    iget-wide v4, p0, Lcom/glympse/android/lib/fe;->hW:J

    sub-long/2addr v2, v4

    long-to-int v1, v2

    iput v1, p0, Lcom/glympse/android/lib/fe;->tj:I

    .line 1997
    :cond_0
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-nez v1, :cond_3

    .line 2025
    :cond_1
    :goto_1
    return-void

    .line 1984
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 2005
    :cond_3
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->ts:Lcom/glympse/android/lib/GTicketParent;

    if-eqz v1, :cond_4

    if-eqz p3, :cond_4

    .line 2008
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->ts:Lcom/glympse/android/lib/GTicketParent;

    invoke-interface {v1}, Lcom/glympse/android/lib/GTicketParent;->orderChanged()V

    .line 2012
    :cond_4
    iget-boolean v1, p0, Lcom/glympse/android/lib/fe;->td:Z

    if-nez v1, :cond_1

    .line 2015
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GGlympsePrivate;->getTime()J

    move-result-wide v2

    .line 2016
    invoke-virtual {p0, v2, v3}, Lcom/glympse/android/lib/fe;->updateState(J)Z

    move-result v1

    .line 2020
    if-eqz v0, :cond_1

    if-nez v1, :cond_1

    iget v0, p0, Lcom/glympse/android/lib/fe;->cg:I

    and-int/lit8 v0, v0, 0x32

    if-eqz v0, :cond_1

    .line 2022
    invoke-direct {p0, v2, v3}, Lcom/glympse/android/lib/fe;->j(J)V

    goto :goto_1
.end method

.method public setGlympse(Lcom/glympse/android/lib/GGlympsePrivate;Lcom/glympse/android/lib/GTicketParent;)V
    .locals 5

    .prologue
    .line 1728
    iget-object v2, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 1729
    iput-object p1, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 1730
    iput-object p2, p0, Lcom/glympse/android/lib/fe;->ts:Lcom/glympse/android/lib/GTicketParent;

    .line 1733
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->mD:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v3

    .line 1734
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 1736
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->mD:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GInvitePrivate;

    .line 1737
    iget-object v4, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0, v4}, Lcom/glympse/android/lib/GInvitePrivate;->setGlympse(Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 1734
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1740
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-nez v0, :cond_3

    .line 1743
    if-eqz v2, :cond_1

    .line 1746
    invoke-direct {p0, v2}, Lcom/glympse/android/lib/fe;->e(Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 1749
    invoke-direct {p0, v2}, Lcom/glympse/android/lib/fe;->f(Lcom/glympse/android/lib/GGlympsePrivate;)V

    .line 1753
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/fe;->lJ:Lcom/glympse/android/lib/GTicketProtocol;

    .line 1785
    :cond_2
    :goto_1
    return-void

    .line 1758
    :cond_3
    iget-boolean v0, p0, Lcom/glympse/android/lib/fe;->td:Z

    if-nez v0, :cond_5

    .line 1763
    iget v0, p0, Lcom/glympse/android/lib/fe;->cg:I

    and-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_4

    .line 1765
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getTime()J

    move-result-wide v0

    .line 1766
    invoke-virtual {p0, v0, v1}, Lcom/glympse/android/lib/fe;->updateState(J)Z

    .line 1772
    :cond_4
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getConfig()Lcom/glympse/android/api/GConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/api/GConfig;->getPostRatePeriod()I

    move-result v0

    iput v0, p0, Lcom/glympse/android/lib/fe;->tp:I

    .line 1775
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getCorrectedTime()Lcom/glympse/android/lib/GCorrectedTime;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/fe;->lX:Lcom/glympse/android/lib/GCorrectedTime;

    .line 1776
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getTicketProtocol()Lcom/glympse/android/lib/GTicketProtocol;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/fe;->lJ:Lcom/glympse/android/lib/GTicketProtocol;

    .line 1780
    :cond_5
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->th:Lcom/glympse/android/api/GAppProfile;

    if-eqz v0, :cond_2

    .line 1782
    invoke-direct {p0}, Lcom/glympse/android/lib/fe;->cQ()V

    goto :goto_1
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1208
    iput-object p1, p0, Lcom/glympse/android/lib/fe;->gM:Ljava/lang/String;

    .line 1209
    return-void
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1300
    iput-object p1, p0, Lcom/glympse/android/lib/fe;->dq:Ljava/lang/String;

    .line 1301
    return-void
.end method

.method public setMine(Z)V
    .locals 0

    .prologue
    .line 1203
    iput-boolean p1, p0, Lcom/glympse/android/lib/fe;->te:Z

    .line 1204
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1233
    iput-object p1, p0, Lcom/glympse/android/lib/fe;->_name:Ljava/lang/String;

    .line 1234
    return-void
.end method

.method public setNext(J)V
    .locals 1

    .prologue
    .line 1258
    iput-wide p1, p0, Lcom/glympse/android/lib/fe;->mM:J

    .line 1259
    return-void
.end method

.method public setOwner(Lcom/glympse/android/api/GAppProfile;)V
    .locals 1

    .prologue
    .line 1972
    iput-object p1, p0, Lcom/glympse/android/lib/fe;->th:Lcom/glympse/android/api/GAppProfile;

    .line 1974
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/fe;->th:Lcom/glympse/android/api/GAppProfile;

    if-nez v0, :cond_1

    .line 1980
    :cond_0
    :goto_0
    return-void

    .line 1979
    :cond_1
    invoke-direct {p0}, Lcom/glympse/android/lib/fe;->cQ()V

    goto :goto_0
.end method

.method public setOwnership(Ljava/lang/String;Lcom/glympse/android/core/GPrimitive;)V
    .locals 2

    .prologue
    .line 1127
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1143
    :cond_0
    :goto_0
    return-void

    .line 1132
    :cond_1
    iput-object p1, p0, Lcom/glympse/android/lib/fe;->so:Ljava/lang/String;

    .line 1133
    iput-object p2, p0, Lcom/glympse/android/lib/fe;->ti:Lcom/glympse/android/core/GPrimitive;

    .line 1135
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->gM:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_0

    .line 1141
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->lJ:Lcom/glympse/android/lib/GTicketProtocol;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GTicketPrivate;

    invoke-interface {v1, v0}, Lcom/glympse/android/lib/GTicketProtocol;->handoffTicket(Lcom/glympse/android/lib/GTicketPrivate;)V

    goto :goto_0
.end method

.method public setProperties(Lcom/glympse/android/hal/GHashtable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/glympse/android/hal/GHashtable",
            "<",
            "Ljava/lang/Long;",
            "Lcom/glympse/android/hal/GHashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/glympse/android/api/GDataRow;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1718
    iput-object p1, p0, Lcom/glympse/android/lib/fe;->to:Lcom/glympse/android/hal/GHashtable;

    .line 1719
    return-void
.end method

.method public setProperty(JJLjava/lang/String;Lcom/glympse/android/core/GPrimitive;)V
    .locals 9

    .prologue
    .line 1388
    new-instance v1, Lcom/glympse/android/lib/aj;

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/glympse/android/lib/aj;-><init>(JJLjava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 1389
    invoke-virtual {p0, v1}, Lcom/glympse/android/lib/fe;->setPropertyData(Lcom/glympse/android/api/GDataRow;)V

    .line 1390
    return-void
.end method

.method public setPropertyData(Lcom/glympse/android/api/GDataRow;)V
    .locals 3

    .prologue
    .line 1395
    invoke-interface {p1}, Lcom/glympse/android/api/GDataRow;->getPartnerId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 1396
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->to:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/hal/GHashtable;

    .line 1397
    if-nez v0, :cond_0

    .line 1400
    new-instance v0, Lcom/glympse/android/hal/GHashtable;

    invoke-direct {v0}, Lcom/glympse/android/hal/GHashtable;-><init>()V

    .line 1401
    iget-object v2, p0, Lcom/glympse/android/lib/fe;->to:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v2, v1, v0}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1405
    :cond_0
    invoke-interface {p1}, Lcom/glympse/android/api/GDataRow;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1406
    return-void
.end method

.method public setReference(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1218
    iput-object p1, p0, Lcom/glympse/android/lib/fe;->ou:Ljava/lang/String;

    .line 1219
    return-void
.end method

.method public setRequestEndTime(J)V
    .locals 1

    .prologue
    .line 704
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/fe;->gM:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/fe;->mw:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 710
    :cond_0
    :goto_0
    return-void

    .line 709
    :cond_1
    iput-wide p1, p0, Lcom/glympse/android/lib/fe;->ih:J

    goto :goto_0
.end method

.method public setRequestNoReply(Z)V
    .locals 1

    .prologue
    .line 725
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/fe;->gM:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/fe;->mw:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 731
    :cond_0
    :goto_0
    return-void

    .line 730
    :cond_1
    iput-boolean p1, p0, Lcom/glympse/android/lib/fe;->tw:Z

    goto :goto_0
.end method

.method public setRequestScenario(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 741
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/fe;->gM:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/fe;->mw:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 747
    :cond_0
    :goto_0
    return-void

    .line 746
    :cond_1
    iput-object p1, p0, Lcom/glympse/android/lib/fe;->tx:Ljava/lang/String;

    goto :goto_0
.end method

.method public setRequestTicket(Lcom/glympse/android/api/GTicket;)V
    .locals 0

    .prologue
    .line 714
    iput-object p1, p0, Lcom/glympse/android/lib/fe;->oB:Lcom/glympse/android/api/GTicket;

    .line 715
    return-void
.end method

.method public setRoute(Lcom/glympse/android/api/GTrack;)V
    .locals 0

    .prologue
    .line 1324
    iput-object p1, p0, Lcom/glympse/android/lib/fe;->kl:Lcom/glympse/android/api/GTrack;

    .line 1325
    return-void
.end method

.method public setSource(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1243
    iput-object p1, p0, Lcom/glympse/android/lib/fe;->tg:Ljava/lang/String;

    .line 1244
    return-void
.end method

.method public setStandalone(Z)V
    .locals 0

    .prologue
    .line 1223
    iput-boolean p1, p0, Lcom/glympse/android/lib/fe;->tf:Z

    .line 1224
    return-void
.end method

.method public setStartTime(J)V
    .locals 5

    .prologue
    .line 1273
    iput-wide p1, p0, Lcom/glympse/android/lib/fe;->hW:J

    .line 1274
    const-wide/16 v0, 0x0

    iget-wide v2, p0, Lcom/glympse/android/lib/fe;->ih:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 1276
    iget-wide v0, p0, Lcom/glympse/android/lib/fe;->ih:J

    iget-wide v2, p0, Lcom/glympse/android/lib/fe;->hW:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, Lcom/glympse/android/lib/fe;->tj:I

    .line 1278
    :cond_0
    return-void
.end method

.method public setState(I)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1822
    iget v0, p0, Lcom/glympse/android/lib/fe;->cg:I

    if-eq v0, p1, :cond_4

    move v0, v1

    .line 1825
    :goto_0
    iget v3, p0, Lcom/glympse/android/lib/fe;->cg:I

    sparse-switch v3, :sswitch_data_0

    .line 1853
    :cond_0
    iput p1, p0, Lcom/glympse/android/lib/fe;->cg:I

    .line 1855
    iget-object v2, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v2, :cond_3

    if-eqz v0, :cond_3

    .line 1857
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[Ticket.setState] New state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/glympse/android/lib/fe;->cg:I

    int-to-long v4, v3

    invoke-static {v4, v5}, Lcom/glympse/android/hal/Helpers;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 1859
    iget-object v2, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v2}, Lcom/glympse/android/lib/GGlympsePrivate;->getTime()J

    move-result-wide v2

    .line 1862
    const/16 v4, 0x40

    if-ne v4, p1, :cond_5

    .line 1865
    invoke-direct {p0}, Lcom/glympse/android/lib/fe;->cT()V

    .line 1868
    iget-boolean v4, p0, Lcom/glympse/android/lib/fe;->td:Z

    if-nez v4, :cond_1

    .line 1871
    iget-object v4, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v4}, Lcom/glympse/android/lib/GGlympsePrivate;->getServerPost()Lcom/glympse/android/lib/GServerPost;

    move-result-object v4

    .line 1872
    invoke-interface {v4}, Lcom/glympse/android/lib/GServerPost;->haveLocationsToPost()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1875
    iget-object v5, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v5}, Lcom/glympse/android/lib/GGlympsePrivate;->okToPost()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1877
    invoke-interface {v4}, Lcom/glympse/android/lib/GServerPost;->doPost()V

    .line 1883
    :cond_1
    const/4 v4, 0x2

    invoke-direct {p0, v4}, Lcom/glympse/android/lib/fe;->z(I)V

    .line 1886
    invoke-direct {p0, v1, v2, v3}, Lcom/glympse/android/lib/fe;->a(ZJ)V

    .line 1889
    invoke-direct {p0}, Lcom/glympse/android/lib/fe;->save()V

    .line 1891
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[Ticket.setState] Ticket expired: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/glympse/android/lib/fe;->gM:Ljava/lang/String;

    invoke-static {v5}, Lcom/glympse/android/hal/Helpers;->safeStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/glympse/android/lib/Debug;->log(ILjava/lang/String;)V

    .line 1900
    :cond_2
    :goto_1
    iget-boolean v1, p0, Lcom/glympse/android/lib/fe;->td:Z

    if-nez v1, :cond_3

    .line 1903
    and-int/lit8 v1, p1, 0x12

    if-eqz v1, :cond_6

    .line 1906
    invoke-direct {p0, v2, v3}, Lcom/glympse/android/lib/fe;->j(J)V

    :cond_3
    :goto_2
    move v2, v0

    .line 1916
    :goto_3
    return v2

    :cond_4
    move v0, v2

    .line 1822
    goto/16 :goto_0

    .line 1829
    :sswitch_0
    and-int/lit16 v3, p1, 0xc0

    if-nez v3, :cond_0

    goto :goto_3

    .line 1837
    :sswitch_1
    and-int/lit16 v3, p1, 0x80

    if-nez v3, :cond_0

    goto :goto_3

    .line 1845
    :sswitch_2
    and-int/lit16 v3, p1, 0x100

    if-nez v3, :cond_0

    goto :goto_3

    .line 1893
    :cond_5
    const/16 v1, 0x20

    if-ne v1, p1, :cond_2

    .line 1896
    invoke-direct {p0}, Lcom/glympse/android/lib/fe;->cT()V

    goto :goto_1

    .line 1911
    :cond_6
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-direct {p0, v1}, Lcom/glympse/android/lib/fe;->e(Lcom/glympse/android/lib/GGlympsePrivate;)V

    goto :goto_2

    .line 1825
    :sswitch_data_0
    .sparse-switch
        0x20 -> :sswitch_0
        0x40 -> :sswitch_1
        0x80 -> :sswitch_2
    .end sparse-switch
.end method

.method public setTravelMode(Lcom/glympse/android/api/GTravelMode;)V
    .locals 0

    .prologue
    .line 1346
    iput-object p1, p0, Lcom/glympse/android/lib/fe;->tm:Lcom/glympse/android/api/GTravelMode;

    .line 1347
    return-void
.end method

.method public setVisibility(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 757
    iget-boolean v2, p0, Lcom/glympse/android/lib/fe;->td:Z

    if-nez v2, :cond_0

    iget v2, p0, Lcom/glympse/android/lib/fe;->tr:I

    iget v3, p0, Lcom/glympse/android/lib/fe;->cg:I

    and-int/2addr v2, v3

    if-eqz v2, :cond_1

    .line 814
    :cond_0
    :goto_0
    return v0

    .line 763
    :cond_1
    invoke-static {}, Lcom/glympse/android/api/GC;->TICKET_VISIBILITY_LOCATION_HIDDEN()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/glympse/android/hal/Helpers;->safeEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 764
    invoke-static {}, Lcom/glympse/android/api/GC;->TICKET_VISIBILITY_LOCATION_VISIBLE()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/glympse/android/hal/Helpers;->safeEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 770
    :cond_2
    invoke-virtual {p0}, Lcom/glympse/android/lib/fe;->getVisibility()Lcom/glympse/android/core/GPrimitive;

    move-result-object v2

    .line 771
    if-eqz v2, :cond_3

    .line 773
    invoke-static {}, Lcom/glympse/android/api/GC;->TICKET_VISIBILITY_KEY_LOCATION()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 774
    invoke-static {}, Lcom/glympse/android/api/GC;->TICKET_VISIBILITY_KEY_CONTEXT()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 776
    invoke-static {v3, p1}, Lcom/glympse/android/hal/Helpers;->safeEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-static {v2, p2}, Lcom/glympse/android/hal/Helpers;->safeEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 783
    :cond_3
    new-instance v2, Lcom/glympse/android/lib/Primitive;

    const/4 v0, 0x2

    invoke-direct {v2, v0}, Lcom/glympse/android/lib/Primitive;-><init>(I)V

    .line 784
    invoke-static {}, Lcom/glympse/android/api/GC;->TICKET_VISIBILITY_KEY_LOCATION()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0, p1}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 785
    invoke-static {}, Lcom/glympse/android/api/GC;->TICKET_VISIBILITY_KEY_CONTEXT()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0, p2}, Lcom/glympse/android/core/GPrimitive;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 786
    const-wide/16 v4, 0x0

    const-string v0, "visibility"

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v4, v5, v0, v2}, Lcom/glympse/android/lib/fe;->a(JLjava/lang/String;Lcom/glympse/android/core/GPrimitive;)V

    .line 789
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-nez v0, :cond_4

    move v0, v1

    .line 791
    goto :goto_0

    .line 795
    :cond_4
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->ts:Lcom/glympse/android/lib/GTicketParent;

    if-eqz v0, :cond_5

    .line 797
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->ts:Lcom/glympse/android/lib/GTicketParent;

    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketParent;->orderChanged()V

    .line 801
    :cond_5
    invoke-virtual {p0}, Lcom/glympse/android/lib/fe;->getId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 804
    iget-object v3, p0, Lcom/glympse/android/lib/fe;->lJ:Lcom/glympse/android/lib/GTicketProtocol;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GTicketPrivate;

    invoke-interface {v3, v0, v2}, Lcom/glympse/android/lib/GTicketProtocol;->setVisibility(Lcom/glympse/android/lib/GTicketPrivate;Lcom/glympse/android/core/GPrimitive;)V

    .line 808
    :cond_6
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->startStopLocation()V

    .line 812
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/4 v2, 0x4

    const/16 v3, 0x8

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p0, v0, v2, v3, v4}, Lcom/glympse/android/lib/fe;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    move v0, v1

    .line 814
    goto/16 :goto_0
.end method

.method public setXoaRegion(Lcom/glympse/android/core/GRegion;)V
    .locals 0

    .prologue
    .line 2115
    iput-object p1, p0, Lcom/glympse/android/lib/fe;->tv:Lcom/glympse/android/core/GRegion;

    .line 2116
    return-void
.end method

.method public updateEta(IJJILcom/glympse/android/api/GTrack;)V
    .locals 12

    .prologue
    .line 1463
    iget-boolean v2, p0, Lcom/glympse/android/lib/fe;->td:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/glympse/android/lib/fe;->te:Z

    if-eqz v2, :cond_0

    const/16 v2, 0x40

    iget v3, p0, Lcom/glympse/android/lib/fe;->cg:I

    if-ne v2, v3, :cond_1

    .line 1512
    :cond_0
    :goto_0
    return-void

    .line 1468
    :cond_1
    const/4 v2, 0x0

    .line 1471
    if-nez p1, :cond_4

    .line 1473
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/glympse/android/lib/fe;->tl:J

    .line 1474
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/glympse/android/lib/fe;->kg:J

    .line 1475
    const/16 v2, 0x100

    .line 1485
    :cond_2
    :goto_1
    if-nez p6, :cond_5

    .line 1487
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/glympse/android/lib/fe;->kl:Lcom/glympse/android/api/GTrack;

    .line 1488
    or-int/lit16 v2, v2, 0x200

    .line 1497
    :cond_3
    :goto_2
    iget-object v3, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v3, :cond_0

    if-eqz v2, :cond_0

    .line 1501
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/glympse/android/api/GTicket;

    .line 1502
    invoke-direct {p0, v4, v2}, Lcom/glympse/android/lib/fe;->a(Lcom/glympse/android/api/GTicket;I)V

    .line 1505
    iget-object v2, p0, Lcom/glympse/android/lib/fe;->gM:Ljava/lang/String;

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1511
    iget-object v3, p0, Lcom/glympse/android/lib/fe;->lJ:Lcom/glympse/android/lib/GTicketProtocol;

    iget-wide v6, p0, Lcom/glympse/android/lib/fe;->kg:J

    iget-wide v8, p0, Lcom/glympse/android/lib/fe;->tl:J

    iget-object v11, p0, Lcom/glympse/android/lib/fe;->kl:Lcom/glympse/android/api/GTrack;

    move v5, p1

    move/from16 v10, p6

    invoke-interface/range {v3 .. v11}, Lcom/glympse/android/lib/GTicketProtocol;->updateTicketEta(Lcom/glympse/android/api/GTicket;IJJILcom/glympse/android/api/GTrack;)V

    goto :goto_0

    .line 1477
    :cond_4
    if-lez p1, :cond_2

    const-wide/16 v4, 0x0

    cmp-long v3, p4, v4

    if-lez v3, :cond_2

    .line 1479
    move-wide/from16 v0, p4

    iput-wide v0, p0, Lcom/glympse/android/lib/fe;->tl:J

    .line 1480
    iput-wide p2, p0, Lcom/glympse/android/lib/fe;->kg:J

    .line 1481
    const/16 v2, 0x100

    goto :goto_1

    .line 1490
    :cond_5
    if-lez p6, :cond_3

    if-eqz p7, :cond_3

    .line 1492
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/glympse/android/lib/fe;->kl:Lcom/glympse/android/api/GTrack;

    .line 1493
    or-int/lit16 v2, v2, 0x200

    goto :goto_2
.end method

.method public updateEta(J)V
    .locals 9

    .prologue
    .line 411
    const-wide/16 v0, 0x0

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    const/4 v1, 0x0

    .line 414
    :goto_0
    invoke-direct {p0}, Lcom/glympse/android/lib/fe;->getTime()J

    move-result-wide v2

    .line 417
    const/4 v6, -0x1

    const/4 v7, 0x0

    move-object v0, p0

    move-wide v4, p1

    invoke-virtual/range {v0 .. v7}, Lcom/glympse/android/lib/fe;->updateEta(IJJILcom/glympse/android/api/GTrack;)V

    .line 418
    return-void

    .line 411
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public updateRoute(Lcom/glympse/android/api/GTrack;)V
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    .line 437
    if-nez p1, :cond_0

    const/4 v6, 0x0

    .line 440
    :goto_0
    const/4 v1, -0x1

    move-object v0, p0

    move-wide v4, v2

    move-object v7, p1

    invoke-virtual/range {v0 .. v7}, Lcom/glympse/android/lib/fe;->updateEta(IJJILcom/glympse/android/api/GTrack;)V

    .line 441
    return-void

    .line 437
    :cond_0
    const/4 v6, 0x1

    goto :goto_0
.end method

.method public updateState(J)Z
    .locals 7

    .prologue
    const/16 v5, 0x40

    const/16 v4, 0x10

    .line 1922
    const-wide/16 v0, 0x0

    iget-wide v2, p0, Lcom/glympse/android/lib/fe;->ih:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1924
    const/4 v0, 0x0

    .line 1965
    :goto_0
    return v0

    .line 1927
    :cond_0
    iget-wide v0, p0, Lcom/glympse/android/lib/fe;->nG:J

    iget-wide v2, p0, Lcom/glympse/android/lib/fe;->ih:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_4

    .line 1932
    iget-wide v0, p0, Lcom/glympse/android/lib/fe;->nG:J

    iget-wide v2, p0, Lcom/glympse/android/lib/fe;->ih:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 1934
    iget-wide v2, p0, Lcom/glympse/android/lib/fe;->nG:J

    .line 1935
    iget-wide v0, p0, Lcom/glympse/android/lib/fe;->ih:J

    .line 1943
    :goto_1
    cmp-long v2, p1, v2

    if-gez v2, :cond_2

    .line 1945
    invoke-virtual {p0, v4}, Lcom/glympse/android/lib/fe;->setState(I)Z

    move-result v0

    goto :goto_0

    .line 1939
    :cond_1
    iget-wide v2, p0, Lcom/glympse/android/lib/fe;->ih:J

    .line 1940
    iget-wide v0, p0, Lcom/glympse/android/lib/fe;->nG:J

    goto :goto_1

    .line 1947
    :cond_2
    cmp-long v0, p1, v0

    if-lez v0, :cond_3

    .line 1949
    invoke-virtual {p0, v5}, Lcom/glympse/android/lib/fe;->setState(I)Z

    move-result v0

    goto :goto_0

    .line 1953
    :cond_3
    const/16 v0, 0x20

    invoke-virtual {p0, v0}, Lcom/glympse/android/lib/fe;->setState(I)Z

    move-result v0

    goto :goto_0

    .line 1959
    :cond_4
    iget-wide v0, p0, Lcom/glympse/android/lib/fe;->ih:J

    cmp-long v0, p1, v0

    if-gtz v0, :cond_5

    .line 1961
    invoke-virtual {p0, v4}, Lcom/glympse/android/lib/fe;->setState(I)Z

    move-result v0

    goto :goto_0

    .line 1965
    :cond_5
    invoke-virtual {p0, v5}, Lcom/glympse/android/lib/fe;->setState(I)Z

    move-result v0

    goto :goto_0
.end method

.method public updateTravelMode(Lcom/glympse/android/api/GTravelMode;)V
    .locals 2

    .prologue
    .line 1352
    iget-boolean v0, p0, Lcom/glympse/android/lib/fe;->td:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/glympse/android/lib/fe;->te:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/glympse/android/lib/fe;->tr:I

    iget v1, p0, Lcom/glympse/android/lib/fe;->cg:I

    and-int/2addr v0, v1

    if-eqz v0, :cond_1

    .line 1373
    :cond_0
    :goto_0
    return-void

    .line 1358
    :cond_1
    invoke-virtual {p0, p1}, Lcom/glympse/android/lib/fe;->setTravelMode(Lcom/glympse/android/api/GTravelMode;)V

    .line 1361
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/fe;->gM:Ljava/lang/String;

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1366
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GTicketPrivate;

    .line 1369
    iget-object v1, p0, Lcom/glympse/android/lib/fe;->lJ:Lcom/glympse/android/lib/GTicketProtocol;

    invoke-interface {v1, v0, p1}, Lcom/glympse/android/lib/GTicketProtocol;->updateTravelMode(Lcom/glympse/android/lib/GTicketPrivate;Lcom/glympse/android/api/GTravelMode;)V

    .line 1372
    const/16 v1, 0x400

    invoke-direct {p0, v0, v1}, Lcom/glympse/android/lib/fe;->a(Lcom/glympse/android/api/GTicket;I)V

    goto :goto_0
.end method

.method public updateWatchingState()Z
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2089
    const-wide/16 v2, 0x0

    .line 2092
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->mD:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v7

    move v6, v5

    .line 2093
    :goto_0
    if-ge v6, v7, :cond_0

    .line 2095
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->mD:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, v6}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GInvite;

    .line 2096
    invoke-interface {v0}, Lcom/glympse/android/api/GInvite;->getLastViewTime()J

    move-result-wide v0

    .line 2097
    cmp-long v8, v0, v2

    if-lez v8, :cond_3

    .line 2093
    :goto_1
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    move-wide v2, v0

    goto :goto_0

    .line 2104
    :cond_0
    iget v0, p0, Lcom/glympse/android/lib/fe;->tp:I

    int-to-long v0, v0

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/glympse/android/lib/fe;->nt:J

    .line 2107
    iget-boolean v1, p0, Lcom/glympse/android/lib/fe;->tq:Z

    .line 2108
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->lX:Lcom/glympse/android/lib/GCorrectedTime;

    invoke-interface {v0}, Lcom/glympse/android/lib/GCorrectedTime;->getTime()J

    move-result-wide v2

    iget-wide v6, p0, Lcom/glympse/android/lib/fe;->nt:J

    cmp-long v0, v2, v6

    if-gez v0, :cond_1

    move v0, v4

    :goto_2
    iput-boolean v0, p0, Lcom/glympse/android/lib/fe;->tq:Z

    .line 2110
    iget-boolean v0, p0, Lcom/glympse/android/lib/fe;->tq:Z

    if-eq v1, v0, :cond_2

    :goto_3
    return v4

    :cond_1
    move v0, v5

    .line 2108
    goto :goto_2

    :cond_2
    move v4, v5

    .line 2110
    goto :goto_3

    :cond_3
    move-wide v0, v2

    goto :goto_1
.end method

.method public visibilityChanged()V
    .locals 4

    .prologue
    .line 1330
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->ts:Lcom/glympse/android/lib/GTicketParent;

    if-eqz v0, :cond_0

    .line 1332
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->ts:Lcom/glympse/android/lib/GTicketParent;

    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketParent;->orderChanged()V

    .line 1336
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_1

    .line 1340
    iget-object v0, p0, Lcom/glympse/android/lib/fe;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/4 v1, 0x4

    const/16 v2, 0x8

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/glympse/android/lib/fe;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 1342
    :cond_1
    return-void
.end method
