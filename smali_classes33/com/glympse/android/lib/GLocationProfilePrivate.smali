.class public interface abstract Lcom/glympse/android/lib/GLocationProfilePrivate;
.super Ljava/lang/Object;
.source "GLocationProfilePrivate.java"

# interfaces
.implements Lcom/glympse/android/core/GLocationProfile;


# virtual methods
.method public abstract setAccuracy(D)V
.end method

.method public abstract setActivity(I)V
.end method

.method public abstract setAutoPauseEnabled(Z)V
.end method

.method public abstract setDistance(D)V
.end method

.method public abstract setFrequency(I)V
.end method

.method public abstract setMode(I)V
.end method

.method public abstract setPriority(I)V
.end method

.method public abstract setProfile(I)V
.end method

.method public abstract setSignificantLocationChangeMonitoringEnabled(Z)V
.end method

.method public abstract setSource(I)V
.end method
