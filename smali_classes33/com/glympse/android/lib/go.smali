.class Lcom/glympse/android/lib/go;
.super Ljava/lang/Object;
.source "XoaManager.java"

# interfaces
.implements Lcom/glympse/android/api/GEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/lib/go$a;
    }
.end annotation


# instance fields
.field private _glympse:Lcom/glympse/android/lib/GGlympsePrivate;

.field private kv:Lcom/glympse/android/api/GHistoryManager;

.field private vF:Lcom/glympse/android/api/GConfig;

.field private vG:Lcom/glympse/android/api/GXoAListener;

.field private vH:Lcom/glympse/android/hal/GHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GHashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/glympse/android/core/GPrimitive;",
            ">;"
        }
    .end annotation
.end field

.field private vI:Lcom/glympse/android/lib/be;

.field private vJ:Lcom/glympse/android/lib/be;

.field private vK:Lcom/glympse/android/lib/be;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    return-void
.end method

.method private A(Lcom/glympse/android/core/GPrimitive;)V
    .locals 0

    .prologue
    .line 283
    if-eqz p1, :cond_0

    .line 285
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/go;->B(Lcom/glympse/android/core/GPrimitive;)V

    .line 291
    :goto_0
    return-void

    .line 289
    :cond_0
    invoke-direct {p0}, Lcom/glympse/android/lib/go;->dl()V

    goto :goto_0
.end method

.method private B(Lcom/glympse/android/core/GPrimitive;)V
    .locals 3

    .prologue
    .line 295
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vJ:Lcom/glympse/android/lib/be;

    if-nez v0, :cond_0

    .line 297
    new-instance v0, Lcom/glympse/android/lib/cp;

    invoke-direct {v0}, Lcom/glympse/android/lib/cp;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/go;->vJ:Lcom/glympse/android/lib/be;

    .line 298
    iget-object v1, p0, Lcom/glympse/android/lib/go;->vJ:Lcom/glympse/android/lib/be;

    new-instance v2, Lcom/glympse/android/lib/go$a;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/go;

    invoke-direct {v2, v0}, Lcom/glympse/android/lib/go$a;-><init>(Lcom/glympse/android/lib/go;)V

    invoke-interface {v1, v2}, Lcom/glympse/android/lib/be;->a(Lcom/glympse/android/lib/bd;)V

    .line 299
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vJ:Lcom/glympse/android/lib/be;

    iget-object v1, p0, Lcom/glympse/android/lib/go;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/be;->start(Lcom/glympse/android/api/GGlympse;)V

    .line 301
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vJ:Lcom/glympse/android/lib/be;

    invoke-interface {v0, p1}, Lcom/glympse/android/lib/be;->b(Lcom/glympse/android/core/GPrimitive;)V

    .line 302
    return-void
.end method

.method static synthetic a(Lcom/glympse/android/lib/go;Lcom/glympse/android/api/GTicket;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/go;->c(Lcom/glympse/android/api/GTicket;)V

    return-void
.end method

.method private c(Lcom/glympse/android/api/GTicket;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/high16 v5, 0x200000

    const/4 v4, 0x1

    .line 379
    const-string v0, "[XoaManager.ticketArrived]"

    invoke-static {v4, v0}, Lcom/glympse/android/hal/Helpers;->log(ILjava/lang/String;)V

    move-object v0, p1

    .line 381
    check-cast v0, Lcom/glympse/android/lib/GTicketPrivate;

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/go;->s(Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 383
    iget-object v0, p0, Lcom/glympse/android/lib/go;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getDiagnosticsManager()Lcom/glympse/android/lib/GDiagnosticsManager;

    move-result-object v0

    const-string v1, "ticket"

    .line 384
    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "state"

    invoke-static {v2}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "arrived"

    .line 385
    invoke-static {v3}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 383
    invoke-interface {v0, p1, v1, v2, v3}, Lcom/glympse/android/lib/GDiagnosticsManager;->logEvent(Lcom/glympse/android/api/GTicket;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    invoke-direct {p0}, Lcom/glympse/android/lib/go;->di()I

    move-result v0

    .line 389
    const/4 v1, 0x2

    if-ne v1, v0, :cond_0

    .line 392
    const/4 v0, 0x0

    invoke-interface {p1, v0, v6, v6}, Lcom/glympse/android/api/GTicket;->modify(ILjava/lang/String;Lcom/glympse/android/api/GPlace;)Z

    .line 396
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/go;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    const/4 v1, 0x4

    invoke-interface {p1, v0, v1, v5, p1}, Lcom/glympse/android/api/GTicket;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 397
    iget-object v0, p0, Lcom/glympse/android/lib/go;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    iget-object v1, p0, Lcom/glympse/android/lib/go;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0, v1, v4, v5, p1}, Lcom/glympse/android/lib/GGlympsePrivate;->eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V

    .line 398
    return-void
.end method

.method private di()I
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vF:Lcom/glympse/android/api/GConfig;

    invoke-interface {v0}, Lcom/glympse/android/api/GConfig;->getExpireOnArrival()I

    move-result v0

    return v0
.end method

.method private dj()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 234
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vI:Lcom/glympse/android/lib/be;

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vI:Lcom/glympse/android/lib/be;

    invoke-interface {v0}, Lcom/glympse/android/lib/be;->stop()V

    .line 237
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vI:Lcom/glympse/android/lib/be;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/be;->a(Lcom/glympse/android/lib/bd;)V

    .line 238
    iput-object v1, p0, Lcom/glympse/android/lib/go;->vI:Lcom/glympse/android/lib/be;

    .line 240
    :cond_0
    return-void
.end method

.method private dk()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 270
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vK:Lcom/glympse/android/lib/be;

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vK:Lcom/glympse/android/lib/be;

    invoke-interface {v0}, Lcom/glympse/android/lib/be;->stop()V

    .line 273
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vK:Lcom/glympse/android/lib/be;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/be;->a(Lcom/glympse/android/lib/bd;)V

    .line 274
    iput-object v1, p0, Lcom/glympse/android/lib/go;->vK:Lcom/glympse/android/lib/be;

    .line 276
    :cond_0
    return-void
.end method

.method private dl()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 306
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vJ:Lcom/glympse/android/lib/be;

    if-eqz v0, :cond_0

    .line 308
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vJ:Lcom/glympse/android/lib/be;

    invoke-interface {v0}, Lcom/glympse/android/lib/be;->stop()V

    .line 309
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vJ:Lcom/glympse/android/lib/be;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/be;->a(Lcom/glympse/android/lib/bd;)V

    .line 310
    iput-object v1, p0, Lcom/glympse/android/lib/go;->vJ:Lcom/glympse/android/lib/be;

    .line 312
    :cond_0
    return-void
.end method

.method private r(Lcom/glympse/android/lib/GTicketPrivate;)V
    .locals 1

    .prologue
    .line 341
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/go;->s(Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 343
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vI:Lcom/glympse/android/lib/be;

    if-eqz v0, :cond_0

    .line 345
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vI:Lcom/glympse/android/lib/be;

    invoke-interface {v0, p1}, Lcom/glympse/android/lib/be;->a(Lcom/glympse/android/api/GTicket;)V

    .line 348
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vJ:Lcom/glympse/android/lib/be;

    if-eqz v0, :cond_1

    .line 350
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vJ:Lcom/glympse/android/lib/be;

    invoke-interface {v0, p1}, Lcom/glympse/android/lib/be;->a(Lcom/glympse/android/api/GTicket;)V

    .line 353
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vK:Lcom/glympse/android/lib/be;

    if-eqz v0, :cond_2

    .line 355
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vK:Lcom/glympse/android/lib/be;

    invoke-interface {v0, p1}, Lcom/glympse/android/lib/be;->a(Lcom/glympse/android/api/GTicket;)V

    .line 357
    :cond_2
    return-void
.end method

.method private s(Lcom/glympse/android/lib/GTicketPrivate;)V
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vI:Lcom/glympse/android/lib/be;

    if-eqz v0, :cond_0

    .line 363
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vI:Lcom/glympse/android/lib/be;

    invoke-interface {v0, p1}, Lcom/glympse/android/lib/be;->b(Lcom/glympse/android/api/GTicket;)V

    .line 366
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vJ:Lcom/glympse/android/lib/be;

    if-eqz v0, :cond_1

    .line 368
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vJ:Lcom/glympse/android/lib/be;

    invoke-interface {v0, p1}, Lcom/glympse/android/lib/be;->b(Lcom/glympse/android/api/GTicket;)V

    .line 371
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vK:Lcom/glympse/android/lib/be;

    if-eqz v0, :cond_2

    .line 373
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vK:Lcom/glympse/android/lib/be;

    invoke-interface {v0, p1}, Lcom/glympse/android/lib/be;->b(Lcom/glympse/android/api/GTicket;)V

    .line 375
    :cond_2
    return-void
.end method

.method private setXoaProfile(Lcom/glympse/android/core/GPrimitive;)V
    .locals 6

    .prologue
    .line 70
    const/4 v0, 0x1

    invoke-interface {p1}, Lcom/glympse/android/core/GPrimitive;->type()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 99
    :goto_0
    return-void

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vH:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GHashtable;->clear()V

    .line 77
    invoke-interface {p1}, Lcom/glympse/android/core/GPrimitive;->getArray()Lcom/glympse/android/core/GArray;

    move-result-object v2

    .line 78
    invoke-interface {v2}, Lcom/glympse/android/core/GArray;->length()I

    move-result v3

    .line 79
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    .line 81
    invoke-interface {v2, v1}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GPrimitive;

    .line 82
    const-string v4, "id"

    invoke-static {v4}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Lcom/glympse/android/core/GPrimitive;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 84
    iget-object v5, p0, Lcom/glympse/android/lib/go;->vH:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v5, v4, v0}, Lcom/glympse/android/hal/GHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 88
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vH:Lcom/glympse/android/hal/GHashtable;

    const-string v1, "platform_prox"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GPrimitive;

    .line 89
    invoke-direct {p0, v0}, Lcom/glympse/android/lib/go;->w(Lcom/glympse/android/core/GPrimitive;)V

    .line 91
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vH:Lcom/glympse/android/hal/GHashtable;

    const-string v1, "auto_prox"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GPrimitive;

    .line 92
    invoke-direct {p0, v0}, Lcom/glympse/android/lib/go;->y(Lcom/glympse/android/core/GPrimitive;)V

    .line 94
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vH:Lcom/glympse/android/hal/GHashtable;

    const-string v1, "hybrid_prox"

    invoke-static {v1}, Lcom/glympse/android/hal/Helpers;->staticString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/core/GPrimitive;

    .line 95
    invoke-direct {p0, v0}, Lcom/glympse/android/lib/go;->A(Lcom/glympse/android/core/GPrimitive;)V

    .line 98
    invoke-virtual {p0}, Lcom/glympse/android/lib/go;->triggerXoAUpdate()V

    goto :goto_0
.end method

.method private u(Lcom/glympse/android/lib/GTicketPrivate;)Z
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vF:Lcom/glympse/android/api/GConfig;

    invoke-interface {v0}, Lcom/glympse/android/api/GConfig;->getExpireOnArrival()I

    move-result v0

    if-nez v0, :cond_0

    .line 106
    const/4 v0, 0x0

    .line 114
    :goto_0
    return v0

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vG:Lcom/glympse/android/api/GXoAListener;

    if-eqz v0, :cond_1

    .line 111
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vG:Lcom/glympse/android/api/GXoAListener;

    invoke-interface {v0, p1}, Lcom/glympse/android/api/GXoAListener;->shouldMonitorTicket(Lcom/glympse/android/api/GTicket;)Z

    move-result v0

    goto :goto_0

    .line 114
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private w(Lcom/glympse/android/core/GPrimitive;)V
    .locals 0

    .prologue
    .line 211
    if-eqz p1, :cond_0

    .line 213
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/go;->x(Lcom/glympse/android/core/GPrimitive;)V

    .line 219
    :goto_0
    return-void

    .line 217
    :cond_0
    invoke-direct {p0}, Lcom/glympse/android/lib/go;->dj()V

    goto :goto_0
.end method

.method private x(Lcom/glympse/android/core/GPrimitive;)V
    .locals 3

    .prologue
    .line 223
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vI:Lcom/glympse/android/lib/be;

    if-nez v0, :cond_0

    .line 225
    new-instance v0, Lcom/glympse/android/lib/en;

    invoke-direct {v0}, Lcom/glympse/android/lib/en;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/go;->vI:Lcom/glympse/android/lib/be;

    .line 226
    iget-object v1, p0, Lcom/glympse/android/lib/go;->vI:Lcom/glympse/android/lib/be;

    new-instance v2, Lcom/glympse/android/lib/go$a;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/go;

    invoke-direct {v2, v0}, Lcom/glympse/android/lib/go$a;-><init>(Lcom/glympse/android/lib/go;)V

    invoke-interface {v1, v2}, Lcom/glympse/android/lib/be;->a(Lcom/glympse/android/lib/bd;)V

    .line 227
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vI:Lcom/glympse/android/lib/be;

    iget-object v1, p0, Lcom/glympse/android/lib/go;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/be;->start(Lcom/glympse/android/api/GGlympse;)V

    .line 229
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vI:Lcom/glympse/android/lib/be;

    invoke-interface {v0, p1}, Lcom/glympse/android/lib/be;->b(Lcom/glympse/android/core/GPrimitive;)V

    .line 230
    return-void
.end method

.method private x(Lcom/glympse/android/lib/GTicketPrivate;)V
    .locals 1

    .prologue
    .line 316
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/go;->y(Lcom/glympse/android/lib/GTicketPrivate;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 318
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/go;->s(Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 320
    :cond_0
    return-void
.end method

.method private y(Lcom/glympse/android/core/GPrimitive;)V
    .locals 0

    .prologue
    .line 247
    if-eqz p1, :cond_0

    .line 249
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/go;->z(Lcom/glympse/android/core/GPrimitive;)V

    .line 255
    :goto_0
    return-void

    .line 253
    :cond_0
    invoke-direct {p0}, Lcom/glympse/android/lib/go;->dk()V

    goto :goto_0
.end method

.method private y(Lcom/glympse/android/lib/GTicketPrivate;)Z
    .locals 1

    .prologue
    .line 324
    .line 326
    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->isActive()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/glympse/android/hal/Helpers;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 328
    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getDestination()Lcom/glympse/android/api/GPlace;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 330
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/go;->u(Lcom/glympse/android/lib/GTicketPrivate;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 332
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/go;->r(Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 333
    const/4 v0, 0x1

    .line 335
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private z(Lcom/glympse/android/core/GPrimitive;)V
    .locals 3

    .prologue
    .line 259
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vK:Lcom/glympse/android/lib/be;

    if-nez v0, :cond_0

    .line 261
    new-instance v0, Lcom/glympse/android/lib/n;

    invoke-direct {v0}, Lcom/glympse/android/lib/n;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/go;->vK:Lcom/glympse/android/lib/be;

    .line 262
    iget-object v1, p0, Lcom/glympse/android/lib/go;->vK:Lcom/glympse/android/lib/be;

    new-instance v2, Lcom/glympse/android/lib/go$a;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/go;

    invoke-direct {v2, v0}, Lcom/glympse/android/lib/go$a;-><init>(Lcom/glympse/android/lib/go;)V

    invoke-interface {v1, v2}, Lcom/glympse/android/lib/be;->a(Lcom/glympse/android/lib/bd;)V

    .line 263
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vK:Lcom/glympse/android/lib/be;

    iget-object v1, p0, Lcom/glympse/android/lib/go;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/be;->start(Lcom/glympse/android/api/GGlympse;)V

    .line 265
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vK:Lcom/glympse/android/lib/be;

    invoke-interface {v0, p1}, Lcom/glympse/android/lib/be;->b(Lcom/glympse/android/core/GPrimitive;)V

    .line 266
    return-void
.end method


# virtual methods
.method public eventsOccurred(Lcom/glympse/android/api/GGlympse;IILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 161
    const/4 v0, 0x4

    if-ne v0, p2, :cond_5

    .line 163
    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_0

    move-object v0, p4

    .line 165
    check-cast v0, Lcom/glympse/android/lib/GTicketPrivate;

    .line 166
    invoke-direct {p0, v0}, Lcom/glympse/android/lib/go;->y(Lcom/glympse/android/lib/GTicketPrivate;)Z

    .line 168
    :cond_0
    and-int/lit8 v0, p3, 0x40

    if-eqz v0, :cond_1

    move-object v0, p4

    .line 171
    check-cast v0, Lcom/glympse/android/lib/GTicketPrivate;

    .line 172
    invoke-direct {p0, v0}, Lcom/glympse/android/lib/go;->x(Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 174
    :cond_1
    const/high16 v0, 0x1000000

    and-int/2addr v0, p3

    if-eqz v0, :cond_2

    move-object v0, p4

    .line 176
    check-cast v0, Lcom/glympse/android/lib/GTicketPrivate;

    .line 177
    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketPrivate;->isMine()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 179
    invoke-direct {p0, v0}, Lcom/glympse/android/lib/go;->y(Lcom/glympse/android/lib/GTicketPrivate;)Z

    .line 188
    :cond_2
    :goto_0
    and-int/lit8 v0, p3, 0x2

    if-eqz v0, :cond_3

    .line 192
    check-cast p4, Lcom/glympse/android/lib/GTicketPrivate;

    .line 193
    invoke-virtual {p0, p4}, Lcom/glympse/android/lib/go;->w(Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 204
    :cond_3
    :goto_1
    return-void

    .line 185
    :cond_4
    invoke-direct {p0, v0}, Lcom/glympse/android/lib/go;->s(Lcom/glympse/android/lib/GTicketPrivate;)V

    goto :goto_0

    .line 196
    :cond_5
    const/16 v0, 0xb

    if-ne v0, p2, :cond_3

    .line 198
    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_3

    .line 200
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vF:Lcom/glympse/android/api/GConfig;

    invoke-interface {v0}, Lcom/glympse/android/api/GConfig;->getXoaProfile()Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    .line 201
    invoke-direct {p0, v0}, Lcom/glympse/android/lib/go;->setXoaProfile(Lcom/glympse/android/core/GPrimitive;)V

    goto :goto_1
.end method

.method public setXoAListener(Lcom/glympse/android/api/GXoAListener;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/glympse/android/lib/go;->vG:Lcom/glympse/android/api/GXoAListener;

    .line 65
    invoke-virtual {p0}, Lcom/glympse/android/lib/go;->triggerXoAUpdate()V

    .line 66
    return-void
.end method

.method public start(Lcom/glympse/android/lib/GGlympsePrivate;)V
    .locals 2

    .prologue
    .line 38
    new-instance v0, Lcom/glympse/android/hal/GHashtable;

    invoke-direct {v0}, Lcom/glympse/android/hal/GHashtable;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/go;->vH:Lcom/glympse/android/hal/GHashtable;

    .line 40
    iput-object p1, p0, Lcom/glympse/android/lib/go;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 41
    invoke-interface {p1}, Lcom/glympse/android/lib/GGlympsePrivate;->getHistoryManager()Lcom/glympse/android/api/GHistoryManager;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/go;->kv:Lcom/glympse/android/api/GHistoryManager;

    .line 43
    invoke-interface {p1}, Lcom/glympse/android/lib/GGlympsePrivate;->getConfig()Lcom/glympse/android/api/GConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/go;->vF:Lcom/glympse/android/api/GConfig;

    .line 44
    iget-object v1, p0, Lcom/glympse/android/lib/go;->vF:Lcom/glympse/android/api/GConfig;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GEventListener;

    invoke-interface {v1, v0}, Lcom/glympse/android/api/GConfig;->addListener(Lcom/glympse/android/api/GEventListener;)Z

    .line 46
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vF:Lcom/glympse/android/api/GConfig;

    invoke-interface {v0}, Lcom/glympse/android/api/GConfig;->getXoaProfile()Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/glympse/android/lib/go;->setXoaProfile(Lcom/glympse/android/core/GPrimitive;)V

    .line 47
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 51
    iget-object v1, p0, Lcom/glympse/android/lib/go;->vF:Lcom/glympse/android/api/GConfig;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GEventListener;

    invoke-interface {v1, v0}, Lcom/glympse/android/api/GConfig;->removeListener(Lcom/glympse/android/api/GEventListener;)Z

    .line 53
    invoke-direct {p0}, Lcom/glympse/android/lib/go;->dj()V

    .line 54
    invoke-direct {p0}, Lcom/glympse/android/lib/go;->dk()V

    .line 55
    invoke-direct {p0}, Lcom/glympse/android/lib/go;->dl()V

    .line 57
    iget-object v0, p0, Lcom/glympse/android/lib/go;->vH:Lcom/glympse/android/hal/GHashtable;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GHashtable;->clear()V

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/go;->vH:Lcom/glympse/android/hal/GHashtable;

    .line 59
    return-void
.end method

.method public triggerXoAUpdate()V
    .locals 4

    .prologue
    .line 125
    iget-object v0, p0, Lcom/glympse/android/lib/go;->kv:Lcom/glympse/android/api/GHistoryManager;

    invoke-interface {v0}, Lcom/glympse/android/api/GHistoryManager;->getTickets()Lcom/glympse/android/core/GArray;

    move-result-object v2

    .line 126
    invoke-interface {v2}, Lcom/glympse/android/core/GArray;->length()I

    move-result v3

    .line 127
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 129
    invoke-interface {v2, v1}, Lcom/glympse/android/core/GArray;->at(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GTicketPrivate;

    .line 130
    invoke-direct {p0, v0}, Lcom/glympse/android/lib/go;->x(Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 127
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 132
    :cond_0
    return-void
.end method

.method public v(Lcom/glympse/android/lib/GTicketPrivate;)V
    .locals 1

    .prologue
    .line 144
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GEventListener;

    invoke-interface {p1, v0}, Lcom/glympse/android/lib/GTicketPrivate;->addListener(Lcom/glympse/android/api/GEventListener;)Z

    .line 147
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/go;->y(Lcom/glympse/android/lib/GTicketPrivate;)Z

    .line 148
    return-void
.end method

.method public w(Lcom/glympse/android/lib/GTicketPrivate;)V
    .locals 1

    .prologue
    .line 153
    invoke-direct {p0, p1}, Lcom/glympse/android/lib/go;->s(Lcom/glympse/android/lib/GTicketPrivate;)V

    .line 156
    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/api/GEventListener;

    invoke-interface {p1, v0}, Lcom/glympse/android/lib/GTicketPrivate;->removeListener(Lcom/glympse/android/api/GEventListener;)Z

    .line 157
    return-void
.end method
