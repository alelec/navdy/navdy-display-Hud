.class Lcom/glympse/android/lib/ap;
.super Lcom/glympse/android/lib/HttpJob;
.source "DirectionsGoogleJob.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/glympse/android/lib/ap$a;,
        Lcom/glympse/android/lib/ap$b;
    }
.end annotation


# instance fields
.field private kk:Lcom/glympse/android/lib/GDirectionsPrivate;

.field private km:Lcom/glympse/android/lib/ap$b;


# direct methods
.method public constructor <init>(Lcom/glympse/android/lib/GDirectionsPrivate;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/glympse/android/lib/HttpJob;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/glympse/android/lib/ap;->kk:Lcom/glympse/android/lib/GDirectionsPrivate;

    .line 28
    return-void
.end method


# virtual methods
.method public checkResponse(II)Z
    .locals 1

    .prologue
    .line 91
    const/16 v0, 0xc8

    if-ne v0, p1, :cond_0

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRetryInterval(I)I
    .locals 1

    .prologue
    .line 96
    const/4 v0, -0x1

    return v0
.end method

.method public onAbort()V
    .locals 1

    .prologue
    .line 123
    invoke-super {p0}, Lcom/glympse/android/lib/HttpJob;->onAbort()V

    .line 124
    iget-object v0, p0, Lcom/glympse/android/lib/ap;->kk:Lcom/glympse/android/lib/GDirectionsPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GDirectionsPrivate;->failed()V

    .line 125
    return-void
.end method

.method public onComplete()V
    .locals 7

    .prologue
    .line 101
    invoke-super {p0}, Lcom/glympse/android/lib/HttpJob;->onComplete()V

    .line 103
    iget-object v0, p0, Lcom/glympse/android/lib/ap;->km:Lcom/glympse/android/lib/ap$b;

    if-eqz v0, :cond_1

    .line 106
    iget-object v0, p0, Lcom/glympse/android/lib/ap;->km:Lcom/glympse/android/lib/ap$b;

    invoke-virtual {v0}, Lcom/glympse/android/lib/ap$b;->ba()Lcom/glympse/android/lib/GTrackPrivate;

    move-result-object v6

    .line 107
    if-eqz v6, :cond_0

    .line 109
    iget-object v0, p0, Lcom/glympse/android/lib/ap;->km:Lcom/glympse/android/lib/ap$b;

    invoke-virtual {v0}, Lcom/glympse/android/lib/ap$b;->getDistance()I

    move-result v0

    invoke-interface {v6, v0}, Lcom/glympse/android/lib/GTrackPrivate;->setDistance(I)V

    .line 113
    :cond_0
    iget-object v1, p0, Lcom/glympse/android/lib/ap;->kk:Lcom/glympse/android/lib/GDirectionsPrivate;

    iget-object v0, p0, Lcom/glympse/android/lib/ap;->kk:Lcom/glympse/android/lib/GDirectionsPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GDirectionsPrivate;->getRequestTime()J

    move-result-wide v2

    iget-object v0, p0, Lcom/glympse/android/lib/ap;->km:Lcom/glympse/android/lib/ap$b;

    invoke-virtual {v0}, Lcom/glympse/android/lib/ap$b;->getEta()J

    move-result-wide v4

    invoke-interface/range {v1 .. v6}, Lcom/glympse/android/lib/GDirectionsPrivate;->set(JJLcom/glympse/android/api/GTrack;)V

    .line 119
    :goto_0
    return-void

    .line 117
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/ap;->kk:Lcom/glympse/android/lib/GDirectionsPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GDirectionsPrivate;->failed()V

    goto :goto_0
.end method

.method public onPreProcess()V
    .locals 5

    .prologue
    const/16 v4, 0x2c

    .line 33
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 34
    const-string v1, "http://maps.googleapis.com/maps/api/directions/json?sensor=true&origin="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    iget-object v1, p0, Lcom/glympse/android/lib/ap;->kk:Lcom/glympse/android/lib/GDirectionsPrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GDirectionsPrivate;->getOrigin()Lcom/glympse/android/core/GLatLng;

    move-result-object v1

    .line 38
    invoke-interface {v1}, Lcom/glympse/android/core/GLatLng;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 39
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 40
    invoke-interface {v1}, Lcom/glympse/android/core/GLatLng;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 43
    iget-object v1, p0, Lcom/glympse/android/lib/ap;->kk:Lcom/glympse/android/lib/GDirectionsPrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GDirectionsPrivate;->getDestination()Lcom/glympse/android/core/GLatLng;

    move-result-object v1

    .line 44
    const-string v2, "&destination="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    invoke-interface {v1}, Lcom/glympse/android/core/GLatLng;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 46
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 47
    invoke-interface {v1}, Lcom/glympse/android/core/GLatLng;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 50
    iget-object v1, p0, Lcom/glympse/android/lib/ap;->kk:Lcom/glympse/android/lib/GDirectionsPrivate;

    invoke-interface {v1}, Lcom/glympse/android/lib/GDirectionsPrivate;->getTravelMode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 69
    :goto_0
    :pswitch_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 70
    iget-object v1, p0, Lcom/glympse/android/lib/ap;->_httpConnection:Lcom/glympse/android/hal/GHttpConnection;

    invoke-interface {v1, v0}, Lcom/glympse/android/hal/GHttpConnection;->setUrl(Ljava/lang/String;)V

    .line 71
    return-void

    .line 53
    :pswitch_1
    const-string v1, "&mode=driving"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 56
    :pswitch_2
    const-string v1, "&mode=bicycling"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 59
    :pswitch_3
    const-string v1, "&mode=walking"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 62
    :pswitch_4
    const-string v1, "&mode=transit"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 50
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public onProcessResponse()V
    .locals 2

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/glympse/android/lib/ap;->isSucceeded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    :goto_0
    return-void

    .line 82
    :cond_0
    new-instance v0, Lcom/glympse/android/lib/json/JsonParser;

    invoke-direct {v0}, Lcom/glympse/android/lib/json/JsonParser;-><init>()V

    .line 83
    new-instance v1, Lcom/glympse/android/lib/ap$b;

    invoke-direct {v1}, Lcom/glympse/android/lib/ap$b;-><init>()V

    iput-object v1, p0, Lcom/glympse/android/lib/ap;->km:Lcom/glympse/android/lib/ap$b;

    .line 84
    iget-object v1, p0, Lcom/glympse/android/lib/ap;->km:Lcom/glympse/android/lib/ap$b;

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/json/GJsonParser;->pushHandler(Lcom/glympse/android/lib/json/GJsonHandler;)V

    .line 85
    iget-object v1, p0, Lcom/glympse/android/lib/ap;->_httpConnection:Lcom/glympse/android/hal/GHttpConnection;

    invoke-interface {v1}, Lcom/glympse/android/hal/GHttpConnection;->getResponseDataString()Ljava/lang/String;

    move-result-object v1

    .line 86
    invoke-interface {v0, v1}, Lcom/glympse/android/lib/json/GJsonParser;->parse(Ljava/lang/String;)Z

    goto :goto_0
.end method
