.class Lcom/glympse/android/lib/ap$b;
.super Lcom/glympse/android/lib/json/JsonHandlerBasic;
.source "DirectionsGoogleJob.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/lib/ap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation


# instance fields
.field private gE:Ljava/lang/String;

.field private kh:J

.field private kn:I

.field private ko:Lcom/glympse/android/lib/GTrackPrivate;

.field private kp:Z

.field private kq:Z

.field private kr:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 139
    invoke-direct {p0}, Lcom/glympse/android/lib/json/JsonHandlerBasic;-><init>()V

    .line 140
    iput-boolean v2, p0, Lcom/glympse/android/lib/ap$b;->kp:Z

    .line 141
    iput-boolean v2, p0, Lcom/glympse/android/lib/ap$b;->kq:Z

    .line 142
    iput-boolean v2, p0, Lcom/glympse/android/lib/ap$b;->kr:Z

    .line 143
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/glympse/android/lib/ap$b;->kh:J

    .line 144
    iput v2, p0, Lcom/glympse/android/lib/ap$b;->kn:I

    .line 145
    return-void
.end method


# virtual methods
.method public ba()Lcom/glympse/android/lib/GTrackPrivate;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/glympse/android/lib/ap$b;->ko:Lcom/glympse/android/lib/GTrackPrivate;

    return-object v0
.end method

.method public endPair(I)Z
    .locals 1

    .prologue
    .line 188
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/glympse/android/lib/ap$b;->gE:Ljava/lang/String;

    .line 189
    const/4 v0, 0x1

    return v0
.end method

.method public getDistance()I
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/glympse/android/lib/ap$b;->kn:I

    return v0
.end method

.method public getEta()J
    .locals 2

    .prologue
    .line 149
    iget-wide v0, p0, Lcom/glympse/android/lib/ap$b;->kh:J

    return-wide v0
.end method

.method public primitive(ILcom/glympse/android/lib/json/GJsonPrimitive;)Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 194
    iget-boolean v0, p0, Lcom/glympse/android/lib/ap$b;->kp:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/glympse/android/lib/ap$b;->gE:Ljava/lang/String;

    const-string v1, "value"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 197
    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getLong()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/glympse/android/lib/ap$b;->kh:J

    .line 198
    iput-boolean v4, p0, Lcom/glympse/android/lib/ap$b;->kp:Z

    .line 211
    :cond_0
    :goto_0
    return v5

    .line 200
    :cond_1
    iget-boolean v0, p0, Lcom/glympse/android/lib/ap$b;->kq:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/glympse/android/lib/ap$b;->gE:Ljava/lang/String;

    const-string v1, "value"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 203
    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getLong()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/glympse/android/lib/ap$b;->kn:I

    .line 204
    iput-boolean v4, p0, Lcom/glympse/android/lib/ap$b;->kq:Z

    goto :goto_0

    .line 206
    :cond_2
    iget-boolean v0, p0, Lcom/glympse/android/lib/ap$b;->kr:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/ap$b;->gE:Ljava/lang/String;

    const-string v1, "points"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 208
    invoke-virtual {p2, v5}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getString(Z)Ljava/lang/String;

    move-result-object v0

    .line 209
    invoke-static {v0}, Lcom/glympse/android/lib/ap$a;->l(Ljava/lang/String;)Lcom/glympse/android/lib/GTrackPrivate;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/ap$b;->ko:Lcom/glympse/android/lib/GTrackPrivate;

    goto :goto_0
.end method

.method public startPair(ILjava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 164
    iput-object p2, p0, Lcom/glympse/android/lib/ap$b;->gE:Ljava/lang/String;

    .line 165
    const/4 v0, 0x3

    if-ne v0, p1, :cond_1

    .line 167
    iget-object v0, p0, Lcom/glympse/android/lib/ap$b;->gE:Ljava/lang/String;

    const-string v1, "overview_polyline"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 169
    iput-boolean v2, p0, Lcom/glympse/android/lib/ap$b;->kr:Z

    .line 183
    :cond_0
    :goto_0
    return v2

    .line 172
    :cond_1
    const/4 v0, 0x5

    if-ne v0, p1, :cond_0

    .line 174
    iget-object v0, p0, Lcom/glympse/android/lib/ap$b;->gE:Ljava/lang/String;

    const-string v1, "duration"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 176
    iput-boolean v2, p0, Lcom/glympse/android/lib/ap$b;->kp:Z

    goto :goto_0

    .line 178
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/ap$b;->gE:Ljava/lang/String;

    const-string v1, "distance"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 180
    iput-boolean v2, p0, Lcom/glympse/android/lib/ap$b;->kq:Z

    goto :goto_0
.end method
