.class public Lcom/glympse/android/lib/eu$c;
.super Lcom/glympse/android/lib/f;
.source "ReplyParsers.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/glympse/android/lib/eu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation


# instance fields
.field private mD:Lcom/glympse/android/hal/GVector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/glympse/android/hal/GVector",
            "<",
            "Lcom/glympse/android/lib/GInvitePrivate;",
            ">;"
        }
    .end annotation
.end field

.field public po:Lcom/glympse/android/lib/GTicketPrivate;


# direct methods
.method public constructor <init>(Lcom/glympse/android/lib/json/GJsonHandlerStack;Lcom/glympse/android/lib/GTicketPrivate;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/glympse/android/lib/f;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/glympse/android/lib/eu$c;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    .line 26
    iput-object p2, p0, Lcom/glympse/android/lib/eu$c;->po:Lcom/glympse/android/lib/GTicketPrivate;

    .line 27
    new-instance v0, Lcom/glympse/android/hal/GVector;

    invoke-direct {v0}, Lcom/glympse/android/hal/GVector;-><init>()V

    iput-object v0, p0, Lcom/glympse/android/lib/eu$c;->mD:Lcom/glympse/android/hal/GVector;

    .line 28
    return-void
.end method


# virtual methods
.method public endObject(I)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 45
    if-ne v4, p1, :cond_1

    .line 48
    iget-object v0, p0, Lcom/glympse/android/lib/eu$c;->mD:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0}, Lcom/glympse/android/hal/GVector;->size()I

    move-result v2

    .line 49
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 51
    iget-object v0, p0, Lcom/glympse/android/lib/eu$c;->mD:Lcom/glympse/android/hal/GVector;

    invoke-virtual {v0, v1}, Lcom/glympse/android/hal/GVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/GInvitePrivate;

    .line 52
    iget-object v3, p0, Lcom/glympse/android/lib/eu$c;->po:Lcom/glympse/android/lib/GTicketPrivate;

    invoke-interface {v3, v0}, Lcom/glympse/android/lib/GTicketPrivate;->addInviteCore(Lcom/glympse/android/api/GInvite;)V

    .line 49
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/glympse/android/lib/eu$c;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    invoke-interface {v0}, Lcom/glympse/android/lib/json/GJsonHandlerStack;->popHandler()V

    .line 58
    :cond_1
    return v4
.end method

.method public bridge synthetic endPair(I)Z
    .locals 1

    .prologue
    .line 18
    invoke-super {p0, p1}, Lcom/glympse/android/lib/f;->endPair(I)Z

    move-result v0

    return v0
.end method

.method public primitive(ILcom/glympse/android/lib/json/GJsonPrimitive;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 76
    if-ne v4, p1, :cond_0

    .line 78
    iget-object v0, p0, Lcom/glympse/android/lib/eu$c;->gE:Ljava/lang/String;

    const-string v1, "message"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 80
    iget-object v0, p0, Lcom/glympse/android/lib/eu$c;->po:Lcom/glympse/android/lib/GTicketPrivate;

    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GTicketPrivate;->setMessage(Ljava/lang/String;)V

    .line 95
    :cond_0
    :goto_0
    return v4

    .line 82
    :cond_1
    iget-object v0, p0, Lcom/glympse/android/lib/eu$c;->gE:Ljava/lang/String;

    const-string v1, "duration"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 84
    iget-object v0, p0, Lcom/glympse/android/lib/eu$c;->po:Lcom/glympse/android/lib/GTicketPrivate;

    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getLong()J

    move-result-wide v2

    long-to-int v1, v2

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GTicketPrivate;->setDuration(I)V

    goto :goto_0

    .line 86
    :cond_2
    iget-object v0, p0, Lcom/glympse/android/lib/eu$c;->gE:Ljava/lang/String;

    const-string v1, "no_reply"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 88
    iget-object v0, p0, Lcom/glympse/android/lib/eu$c;->po:Lcom/glympse/android/lib/GTicketPrivate;

    invoke-virtual {p2}, Lcom/glympse/android/lib/json/GJsonPrimitive;->getBool()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GTicketPrivate;->setRequestNoReply(Z)V

    goto :goto_0

    .line 90
    :cond_3
    iget-object v0, p0, Lcom/glympse/android/lib/eu$c;->gE:Ljava/lang/String;

    const-string v1, "scenario"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/glympse/android/lib/eu$c;->po:Lcom/glympse/android/lib/GTicketPrivate;

    invoke-virtual {p2, v4}, Lcom/glympse/android/lib/json/GJsonPrimitive;->ownString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/glympse/android/lib/GTicketPrivate;->setRequestScenario(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public startObject(I)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 32
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 34
    iget-object v0, p0, Lcom/glympse/android/lib/eu$c;->gE:Ljava/lang/String;

    const-string v1, "destination"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    iget-object v1, p0, Lcom/glympse/android/lib/eu$c;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    new-instance v2, Lcom/glympse/android/lib/eu$a;

    iget-object v3, p0, Lcom/glympse/android/lib/eu$c;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    invoke-static {p0}, Lcom/glympse/android/hal/Helpers;->wrapThis(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/glympse/android/lib/eu$c;

    invoke-direct {v2, v3, v0}, Lcom/glympse/android/lib/eu$a;-><init>(Lcom/glympse/android/lib/json/GJsonHandlerStack;Lcom/glympse/android/lib/eu$c;)V

    invoke-interface {v1, v2, v4}, Lcom/glympse/android/lib/json/GJsonHandlerStack;->pushHandler(Lcom/glympse/android/lib/json/GJsonHandler;I)V

    .line 39
    :cond_0
    return v4
.end method

.method public startPair(ILjava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 63
    iput-object p2, p0, Lcom/glympse/android/lib/eu$c;->gE:Ljava/lang/String;

    .line 64
    if-ne v4, p1, :cond_0

    .line 66
    iget-object v0, p0, Lcom/glympse/android/lib/eu$c;->gE:Ljava/lang/String;

    const-string v1, "invites"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/glympse/android/lib/eu$c;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    new-instance v1, Lcom/glympse/android/lib/eu$b;

    iget-object v2, p0, Lcom/glympse/android/lib/eu$c;->gD:Lcom/glympse/android/lib/json/GJsonHandlerStack;

    iget-object v3, p0, Lcom/glympse/android/lib/eu$c;->mD:Lcom/glympse/android/hal/GVector;

    invoke-direct {v1, v2, v3}, Lcom/glympse/android/lib/eu$b;-><init>(Lcom/glympse/android/lib/json/GJsonHandlerStack;Lcom/glympse/android/hal/GVector;)V

    invoke-interface {v0, v1, v4}, Lcom/glympse/android/lib/json/GJsonHandlerStack;->pushHandler(Lcom/glympse/android/lib/json/GJsonHandler;I)V

    .line 71
    :cond_0
    return v4
.end method
