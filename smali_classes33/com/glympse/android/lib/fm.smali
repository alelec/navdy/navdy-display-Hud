.class Lcom/glympse/android/lib/fm;
.super Lcom/glympse/android/lib/cu;
.source "TicketInviteCreate.java"


# instance fields
.field private gM:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/glympse/android/lib/GTicketPrivate;Lcom/glympse/android/lib/GInvitePrivate;Lcom/glympse/android/lib/GGlympsePrivate;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/glympse/android/lib/cu;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/glympse/android/lib/fm;->gr:Lcom/glympse/android/api/GEventSink;

    .line 24
    const/4 v0, 0x4

    iput v0, p0, Lcom/glympse/android/lib/fm;->oC:I

    .line 25
    const/16 v0, 0x1000

    iput v0, p0, Lcom/glympse/android/lib/fm;->oD:I

    .line 26
    const/16 v0, 0x2000

    iput v0, p0, Lcom/glympse/android/lib/fm;->oE:I

    .line 27
    const/high16 v0, 0x10000

    iput v0, p0, Lcom/glympse/android/lib/fm;->oF:I

    .line 28
    iput-object p1, p0, Lcom/glympse/android/lib/fm;->oG:Lcom/glympse/android/lib/GTicketPrivate;

    .line 29
    iput-object p2, p0, Lcom/glympse/android/lib/fm;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    .line 30
    iput-object p3, p0, Lcom/glympse/android/lib/fm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    .line 31
    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/fm;->oy:Ljava/lang/String;

    .line 34
    invoke-interface {p1}, Lcom/glympse/android/lib/GTicketPrivate;->getId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/glympse/android/lib/fm;->gM:Ljava/lang/String;

    .line 37
    invoke-virtual {p0}, Lcom/glympse/android/lib/fm;->cl()V

    .line 38
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/StringBuilder;)V
    .locals 1

    .prologue
    .line 46
    const-string v0, "tickets/"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    iget-object v0, p0, Lcom/glympse/android/lib/fm;->gM:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    const-string v0, "/create_invite"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    return-void
.end method

.method public ck()V
    .locals 3

    .prologue
    .line 54
    iget-object v0, p0, Lcom/glympse/android/lib/fm;->oG:Lcom/glympse/android/lib/GTicketPrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GTicketPrivate;->getParent()Lcom/glympse/android/lib/GTicketParent;

    move-result-object v0

    .line 55
    iget-object v1, p0, Lcom/glympse/android/lib/fm;->oG:Lcom/glympse/android/lib/GTicketPrivate;

    iget-object v2, p0, Lcom/glympse/android/lib/fm;->oH:Lcom/glympse/android/lib/GInvitePrivate;

    invoke-interface {v0, v1, v2}, Lcom/glympse/android/lib/GTicketParent;->inviteCreated(Lcom/glympse/android/api/GTicket;Lcom/glympse/android/api/GInvite;)V

    .line 58
    const/4 v0, 0x7

    iget v1, p0, Lcom/glympse/android/lib/fm;->oJ:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/glympse/android/lib/fm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getConfig()Lcom/glympse/android/api/GConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/glympse/android/api/GConfig;->isPublicGroupAutoWatched()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/glympse/android/lib/fm;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    invoke-interface {v0}, Lcom/glympse/android/lib/GGlympsePrivate;->getGroupManager()Lcom/glympse/android/api/GGroupManager;

    move-result-object v0

    iget-object v1, p0, Lcom/glympse/android/lib/fm;->oL:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/glympse/android/api/GGroupManager;->viewGroup(Ljava/lang/String;)Lcom/glympse/android/api/GGroup;

    .line 63
    :cond_0
    return-void
.end method
