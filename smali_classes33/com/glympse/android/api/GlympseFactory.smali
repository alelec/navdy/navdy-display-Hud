.class public abstract Lcom/glympse/android/api/GlympseFactory;
.super Ljava/lang/Object;
.source "GlympseFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized createApiStatus(Ljava/lang/String;)Lcom/glympse/android/api/GApiStatus;
    .locals 2

    .prologue
    .line 36
    const-class v0, Lcom/glympse/android/api/GlympseFactory;

    monitor-enter v0

    :try_start_0
    invoke-static {p0}, Lcom/glympse/android/lib/LibFactory;->createApiStatus(Ljava/lang/String;)Lcom/glympse/android/api/GApiStatus;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static createChronoTrigger(Ljava/lang/String;ZLcom/glympse/android/api/GTicket;J)Lcom/glympse/android/api/GChronoTrigger;
    .locals 1

    .prologue
    .line 344
    invoke-static {p0, p1, p2, p3, p4}, Lcom/glympse/android/lib/LibFactory;->createChronoTrigger(Ljava/lang/String;ZLcom/glympse/android/api/GTicket;J)Lcom/glympse/android/api/GChronoTrigger;

    move-result-object v0

    return-object v0
.end method

.method public static createEmailAccountProfile(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;
    .locals 1

    .prologue
    .line 292
    invoke-static {p0}, Lcom/glympse/android/lib/LinkedAccountsManager;->createEmailAccountProfile(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    return-object v0
.end method

.method public static createEtaTrigger(Ljava/lang/String;ZLcom/glympse/android/api/GTicket;JILcom/glympse/android/api/GTicket;)Lcom/glympse/android/api/GEtaTrigger;
    .locals 1

    .prologue
    .line 354
    invoke-static/range {p0 .. p6}, Lcom/glympse/android/lib/LibFactory;->createEtaTrigger(Ljava/lang/String;ZLcom/glympse/android/api/GTicket;JILcom/glympse/android/api/GTicket;)Lcom/glympse/android/api/GEtaTrigger;

    move-result-object v0

    return-object v0
.end method

.method public static createEvernoteAccountProfile(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;
    .locals 1

    .prologue
    .line 240
    invoke-static {p0}, Lcom/glympse/android/lib/LinkedAccountsManager;->createEvernoteAccountProfile(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    return-object v0
.end method

.method public static createFacebookAccountProfile(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;
    .locals 1

    .prologue
    .line 214
    invoke-static {p0}, Lcom/glympse/android/lib/LinkedAccountsManager;->createFacebookAccountProfile(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    return-object v0
.end method

.method public static createGeoTrigger(Ljava/lang/String;ZLcom/glympse/android/api/GTicket;Lcom/glympse/android/core/GLatLng;DI)Lcom/glympse/android/api/GGeoTrigger;
    .locals 2

    .prologue
    .line 334
    invoke-static/range {p0 .. p6}, Lcom/glympse/android/lib/LibFactory;->createGeoTrigger(Ljava/lang/String;ZLcom/glympse/android/api/GTicket;Lcom/glympse/android/core/GLatLng;DI)Lcom/glympse/android/api/GGeoTrigger;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized createGlympse(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/glympse/android/api/GGlympse;
    .locals 2

    .prologue
    .line 46
    const-class v0, Lcom/glympse/android/api/GlympseFactory;

    monitor-enter v0

    const/4 v1, 0x0

    :try_start_0
    invoke-static {p0, p1, p2, v1}, Lcom/glympse/android/api/GlympseFactory;->createGlympse(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/glympse/android/api/GGlympse;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized createGlympse(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/glympse/android/api/GGlympse;
    .locals 2

    .prologue
    .line 58
    const-class v1, Lcom/glympse/android/api/GlympseFactory;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/glympse/android/hal/GlympseService;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;

    if-eqz v0, :cond_0

    .line 60
    sget-object v0, Lcom/glympse/android/hal/GlympseService;->_glympse:Lcom/glympse/android/lib/GGlympsePrivate;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 64
    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    :try_start_1
    invoke-static {p0, p1, p2, p3}, Lcom/glympse/android/lib/LibFactory;->createGlympse(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/glympse/android/api/GGlympse;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 58
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static createGoogleAccountProfile(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;
    .locals 1

    .prologue
    .line 257
    invoke-static {p0}, Lcom/glympse/android/lib/LinkedAccountsManager;->createGoogleAccountProfile(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    return-object v0
.end method

.method public static createGoogleServerAuthorizationProfile(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;
    .locals 1

    .prologue
    .line 314
    invoke-static {p0}, Lcom/glympse/android/lib/LinkedAccountsManager;->createGoogleServerAuthorizationProfile(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    return-object v0
.end method

.method public static createImage(Lcom/glympse/android/core/GDrawable;)Lcom/glympse/android/api/GImage;
    .locals 1

    .prologue
    .line 203
    invoke-static {p0}, Lcom/glympse/android/lib/LibFactory;->createImage(Lcom/glympse/android/core/GDrawable;)Lcom/glympse/android/lib/GImagePrivate;

    move-result-object v0

    return-object v0
.end method

.method public static createInvite(ILjava/lang/String;Ljava/lang/String;)Lcom/glympse/android/api/GInvite;
    .locals 1

    .prologue
    .line 128
    invoke-static {p0, p1, p2}, Lcom/glympse/android/lib/LibFactory;->createInvite(ILjava/lang/String;Ljava/lang/String;)Lcom/glympse/android/api/GInvite;

    move-result-object v0

    return-object v0
.end method

.method public static createInvite(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/glympse/android/api/GInvite;
    .locals 1

    .prologue
    .line 141
    invoke-static {p0, p1, p2, p3}, Lcom/glympse/android/lib/LibFactory;->createInvite(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/glympse/android/api/GInvite;

    move-result-object v0

    return-object v0
.end method

.method public static createPairingCodeProfile()Lcom/glympse/android/core/GPrimitive;
    .locals 1

    .prologue
    .line 268
    invoke-static {}, Lcom/glympse/android/lib/LinkedAccountsManager;->createPairingCodeProfile()Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    return-object v0
.end method

.method public static createPhoneAccountProfile(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;
    .locals 1

    .prologue
    .line 280
    invoke-static {p0}, Lcom/glympse/android/lib/LinkedAccountsManager;->createPhoneAccountProfile(Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    return-object v0
.end method

.method public static createPlace(DDLjava/lang/String;)Lcom/glympse/android/api/GPlace;
    .locals 2

    .prologue
    .line 195
    invoke-static {p0, p1, p2, p3, p4}, Lcom/glympse/android/lib/LibFactory;->createPlace(DDLjava/lang/String;)Lcom/glympse/android/api/GPlace;

    move-result-object v0

    return-object v0
.end method

.method public static createPublicInvite(Ljava/lang/String;)Lcom/glympse/android/api/GInvite;
    .locals 2

    .prologue
    .line 155
    const/4 v0, 0x7

    const/4 v1, 0x0

    invoke-static {v0, v1, p0}, Lcom/glympse/android/lib/LibFactory;->createInvite(ILjava/lang/String;Ljava/lang/String;)Lcom/glympse/android/api/GInvite;

    move-result-object v0

    return-object v0
.end method

.method public static createRequest(Lcom/glympse/android/api/GTicket;Lcom/glympse/android/api/GInvite;)Lcom/glympse/android/api/GTicket;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 184
    invoke-interface {p1, p0}, Lcom/glympse/android/api/GInvite;->setRequestTicket(Lcom/glympse/android/api/GTicket;)V

    .line 185
    const/4 v0, 0x0

    invoke-static {v0, v1, v1}, Lcom/glympse/android/api/GlympseFactory;->createTicket(ILjava/lang/String;Lcom/glympse/android/api/GPlace;)Lcom/glympse/android/api/GTicket;

    move-result-object v0

    .line 186
    invoke-interface {v0, p1}, Lcom/glympse/android/api/GTicket;->addInvite(Lcom/glympse/android/api/GInvite;)Z

    .line 187
    return-object v0
.end method

.method public static createTicket(ILjava/lang/String;Lcom/glympse/android/api/GPlace;)Lcom/glympse/android/api/GTicket;
    .locals 1

    .prologue
    .line 168
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/glympse/android/lib/LibFactory;->createTicket(Z)Lcom/glympse/android/lib/GTicketPrivate;

    move-result-object v0

    .line 169
    invoke-interface {v0, p0}, Lcom/glympse/android/lib/GTicketPrivate;->setDuration(I)V

    .line 170
    invoke-interface {v0, p1}, Lcom/glympse/android/lib/GTicketPrivate;->setMessage(Ljava/lang/String;)V

    .line 171
    invoke-interface {v0, p2}, Lcom/glympse/android/lib/GTicketPrivate;->setDestination(Lcom/glympse/android/api/GPlace;)V

    .line 172
    return-object v0
.end method

.method public static createTrackBuilder()Lcom/glympse/android/api/GTrackBuilder;
    .locals 1

    .prologue
    .line 324
    invoke-static {}, Lcom/glympse/android/lib/LibFactory;->createTrackBuilder()Lcom/glympse/android/api/GTrackBuilder;

    move-result-object v0

    return-object v0
.end method

.method public static createTwitterAccountProfile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;
    .locals 1

    .prologue
    .line 229
    invoke-static {p0, p1, p2, p3}, Lcom/glympse/android/lib/LinkedAccountsManager;->createTwitterAccountProfile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/glympse/android/core/GPrimitive;

    move-result-object v0

    return-object v0
.end method
