.class public interface abstract Lcom/glympse/android/api/GDirections;
.super Ljava/lang/Object;
.source "GDirections.java"

# interfaces
.implements Lcom/glympse/android/api/GEventSink;


# virtual methods
.method public abstract getDestination()Lcom/glympse/android/core/GLatLng;
.end method

.method public abstract getEta()J
.end method

.method public abstract getEtaTs()J
.end method

.method public abstract getOrigin()Lcom/glympse/android/core/GLatLng;
.end method

.method public abstract getRequestTime()J
.end method

.method public abstract getState()I
.end method

.method public abstract getTrack()Lcom/glympse/android/api/GTrack;
.end method

.method public abstract getTravelMode()I
.end method
