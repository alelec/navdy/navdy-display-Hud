.class public interface abstract Lcom/glympse/android/api/GDirectionsManager;
.super Ljava/lang/Object;
.source "GDirectionsManager.java"

# interfaces
.implements Lcom/glympse/android/core/GCommon;


# virtual methods
.method public abstract enableActivityRecognition(Z)V
.end method

.method public abstract getDeviceActivity()I
.end method

.method public abstract getTravelMode()I
.end method

.method public abstract isActivityRecognitionEnabled()Z
.end method

.method public abstract isDeviceStationary()Z
.end method

.method public abstract queryDirections(Lcom/glympse/android/core/GLatLng;Lcom/glympse/android/core/GLatLng;I)Lcom/glympse/android/api/GDirections;
.end method

.method public abstract setTravelMode(I)V
.end method
