.class public final Lcom/navdy/hud/app/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final active_trip_menu_lyt:I = 0x7f030000

.field public static final active_trip_menu_mask_lyt:I = 0x7f030001

.field public static final active_trip_menu_trip_lyt:I = 0x7f030002

.field public static final activity_fullscreen:I = 0x7f030003

.field public static final calendar_widget:I = 0x7f030004

.field public static final carousel_fast_scroll_image_lyt:I = 0x7f030005

.field public static final carousel_view_destinations:I = 0x7f030006

.field public static final carousel_view_menu:I = 0x7f030007

.field public static final carousel_view_recents:I = 0x7f030008

.field public static final choices2_lyt:I = 0x7f030009

.field public static final choices2_lyt_item:I = 0x7f03000a

.field public static final choices_lyt:I = 0x7f03000b

.field public static final confirmation_lyt:I = 0x7f03000c

.field public static final contact_carousel_image:I = 0x7f03000d

.field public static final crossfade_image_bkcolor_lyt:I = 0x7f03000e

.field public static final crossfade_image_color_lyt:I = 0x7f03000f

.field public static final crossfade_image_lyt:I = 0x7f030010

.field public static final debug_gesture_engine:I = 0x7f030011

.field public static final digital_clock1_layout:I = 0x7f030012

.field public static final digital_clock2_layout:I = 0x7f030013

.field public static final drive_score_gauge_layout:I = 0x7f030014

.field public static final drive_score_map_layout:I = 0x7f030015

.field public static final empty_gauge_layout:I = 0x7f030016

.field public static final engine_temp_gauge_left:I = 0x7f030017

.field public static final engine_temp_gauge_right:I = 0x7f030018

.field public static final eta_gauge_layout:I = 0x7f030019

.field public static final force_update_lyt:I = 0x7f03001a

.field public static final fuel_consumption_gauge:I = 0x7f03001b

.field public static final fuel_gauge_left:I = 0x7f03001c

.field public static final fuel_gauge_right:I = 0x7f03001d

.field public static final gauge_view_scrims:I = 0x7f03001e

.field public static final gesture_learning_lyt:I = 0x7f03001f

.field public static final gesture_video_capture:I = 0x7f030020

.field public static final glance_large_calendar:I = 0x7f030021

.field public static final glance_large_message:I = 0x7f030022

.field public static final glance_large_message_single:I = 0x7f030023

.field public static final glance_large_multi_text:I = 0x7f030024

.field public static final glance_large_text:I = 0x7f030025

.field public static final glance_reply_exit:I = 0x7f030026

.field public static final glance_small_image:I = 0x7f030027

.field public static final glance_small_message:I = 0x7f030028

.field public static final glance_small_sign:I = 0x7f030029

.field public static final hockeyapp_activity_expiry_info:I = 0x7f03002a

.field public static final hockeyapp_activity_feedback:I = 0x7f03002b

.field public static final hockeyapp_activity_login:I = 0x7f03002c

.field public static final hockeyapp_activity_update:I = 0x7f03002d

.field public static final hockeyapp_fragment_update:I = 0x7f03002e

.field public static final hockeyapp_view_feedback_message:I = 0x7f03002f

.field public static final junction_view_lyt:I = 0x7f030030

.field public static final main_view_lyt:I = 0x7f030031

.field public static final maps_traffic_incident_widget:I = 0x7f030032

.field public static final music_details_album_art:I = 0x7f030033

.field public static final music_widget:I = 0x7f030034

.field public static final notification_adaptive_auto_brightness:I = 0x7f030035

.field public static final notification_alert:I = 0x7f030036

.field public static final notification_auto_brightness:I = 0x7f030037

.field public static final notification_brightness:I = 0x7f030038

.field public static final notification_glympse:I = 0x7f030039

.field public static final notification_message:I = 0x7f03003a

.field public static final notification_message_prompt:I = 0x7f03003b

.field public static final notification_music:I = 0x7f03003c

.field public static final notification_phonecall:I = 0x7f03003d

.field public static final notification_place_type_search:I = 0x7f03003e

.field public static final notification_route_calc:I = 0x7f03003f

.field public static final notification_sms:I = 0x7f030040

.field public static final notification_traffic_event:I = 0x7f030041

.field public static final notification_traffic_jam:I = 0x7f030042

.field public static final notification_voice_assist:I = 0x7f030043

.field public static final notification_voice_search:I = 0x7f030044

.field public static final screen_auto_brightness:I = 0x7f030045

.field public static final screen_destination_picker:I = 0x7f030046

.field public static final screen_destination_picker_content:I = 0x7f030047

.field public static final screen_dial_manager:I = 0x7f030048

.field public static final screen_dial_update_confirmation:I = 0x7f030049

.field public static final screen_dial_update_progress:I = 0x7f03004a

.field public static final screen_factory_reset:I = 0x7f03004b

.field public static final screen_first_launch:I = 0x7f03004c

.field public static final screen_home:I = 0x7f03004d

.field public static final screen_home_active_eta:I = 0x7f03004e

.field public static final screen_home_lane_guidance:I = 0x7f03004f

.field public static final screen_home_map:I = 0x7f030050

.field public static final screen_home_map_no_location:I = 0x7f030051

.field public static final screen_home_map_speed:I = 0x7f030052

.field public static final screen_home_mpg:I = 0x7f030053

.field public static final screen_home_open_road:I = 0x7f030054

.field public static final screen_home_route_calc:I = 0x7f030055

.field public static final screen_home_route_recalc:I = 0x7f030056

.field public static final screen_home_smartdash:I = 0x7f030057

.field public static final screen_home_system_info_gps_satellite:I = 0x7f030058

.field public static final screen_home_tbt:I = 0x7f030059

.field public static final screen_home_tbt_early:I = 0x7f03005a

.field public static final screen_home_tbt_shrink_distance:I = 0x7f03005b

.field public static final screen_home_time:I = 0x7f03005c

.field public static final screen_learn_gesture_layout:I = 0x7f03005d

.field public static final screen_main_menu_2:I = 0x7f03005e

.field public static final screen_main_menu_2_scrim:I = 0x7f03005f

.field public static final screen_music_details:I = 0x7f030060

.field public static final screen_toast:I = 0x7f030061

.field public static final screen_update_confirmation:I = 0x7f030062

.field public static final screen_welcome:I = 0x7f030063

.field public static final screen_welcome_download_app:I = 0x7f030064

.field public static final screen_welcome_logo:I = 0x7f030065

.field public static final screen_welcome_searching:I = 0x7f030066

.field public static final scrollable_text_presenter_layout:I = 0x7f030067

.field public static final shutdown_confirmation_lyt:I = 0x7f030068

.field public static final small_gauge_view:I = 0x7f030069

.field public static final smart_dash_widget_circular_content_layout:I = 0x7f03006a

.field public static final smart_dash_widget_layout:I = 0x7f03006b

.field public static final speed_limit_sign_gauge:I = 0x7f03006c

.field public static final speed_limit_sign_us:I = 0x7f03006d

.field public static final speed_sign_eu:I = 0x7f03006e

.field public static final speedo_meter_gauge:I = 0x7f03006f

.field public static final speedo_meter_gauge_2:I = 0x7f030070

.field public static final system_alert_lyt:I = 0x7f030071

.field public static final system_info_gen_pref_lyt:I = 0x7f030072

.field public static final system_info_lyt:I = 0x7f030073

.field public static final system_info_maps_lyt:I = 0x7f030074

.field public static final system_info_nav_pref_lyt:I = 0x7f030075

.field public static final system_info_stats_lyt:I = 0x7f030076

.field public static final system_info_vehicle_lyt:I = 0x7f030077

.field public static final system_info_version_lyt:I = 0x7f030078

.field public static final system_tray:I = 0x7f030079

.field public static final tachometer_gauge:I = 0x7f03007a

.field public static final tbt_container_lyt:I = 0x7f03007b

.field public static final tbt_direction_lyt:I = 0x7f03007c

.field public static final tbt_distance_lyt:I = 0x7f03007d

.field public static final tbt_lyt:I = 0x7f03007e

.field public static final tbt_next_maneuver_lyt:I = 0x7f03007f

.field public static final tbt_text_instruction_lyt:I = 0x7f030080

.field public static final temperature_warning_lyt:I = 0x7f030081

.field public static final tooltip_lyt:I = 0x7f030082

.field public static final vlist_blank_item:I = 0x7f030083

.field public static final vlist_content_loading:I = 0x7f030084

.field public static final vlist_halo_image:I = 0x7f030085

.field public static final vlist_icon_options:I = 0x7f030086

.field public static final vlist_item:I = 0x7f030087

.field public static final vlist_loading_item:I = 0x7f030088

.field public static final vlist_scroll_item:I = 0x7f030089

.field public static final vlist_title:I = 0x7f03008a

.field public static final vlist_title_subtitle:I = 0x7f03008b

.field public static final vlist_toggle_switch:I = 0x7f03008c

.field public static final vmenu_lyt:I = 0x7f03008d

.field public static final voice_search_tips_layout:I = 0x7f03008e


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3511
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
