.class public Lcom/navdy/hud/app/config/Setting;
.super Ljava/lang/Object;
.source "Setting.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/config/Setting$IObserver;
    }
.end annotation


# static fields
.field private static MAX_PROP_LEN:I


# instance fields
.field private description:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private observer:Lcom/navdy/hud/app/config/Setting$IObserver;

.field private path:Ljava/lang/String;

.field private property:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const/16 v0, 0x1f

    sput v0, Lcom/navdy/hud/app/config/Setting;->MAX_PROP_LEN:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "description"    # Ljava/lang/String;

    .prologue
    .line 34
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "persist.sys."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/navdy/hud/app/config/Setting;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "description"    # Ljava/lang/String;
    .param p4, "property"    # Ljava/lang/String;

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/navdy/hud/app/config/Setting;->name:Ljava/lang/String;

    .line 48
    iput-object p2, p0, Lcom/navdy/hud/app/config/Setting;->path:Ljava/lang/String;

    .line 49
    iput-object p3, p0, Lcom/navdy/hud/app/config/Setting;->description:Ljava/lang/String;

    .line 50
    if-eqz p4, :cond_0

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v0

    sget v1, Lcom/navdy/hud/app/config/Setting;->MAX_PROP_LEN:I

    if-le v0, v1, :cond_0

    .line 51
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "property name ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") too long"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 53
    :cond_0
    iput-object p4, p0, Lcom/navdy/hud/app/config/Setting;->property:Ljava/lang/String;

    .line 54
    return-void
.end method


# virtual methods
.method protected changed()V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/navdy/hud/app/config/Setting;->observer:Lcom/navdy/hud/app/config/Setting$IObserver;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/navdy/hud/app/config/Setting;->observer:Lcom/navdy/hud/app/config/Setting$IObserver;

    invoke-interface {v0, p0}, Lcom/navdy/hud/app/config/Setting$IObserver;->onChanged(Lcom/navdy/hud/app/config/Setting;)V

    .line 80
    :cond_0
    return-void
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/navdy/hud/app/config/Setting;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/navdy/hud/app/config/Setting;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/navdy/hud/app/config/Setting;->path:Ljava/lang/String;

    return-object v0
.end method

.method public getProperty()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/navdy/hud/app/config/Setting;->property:Ljava/lang/String;

    return-object v0
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x1

    return v0
.end method

.method public setObserver(Lcom/navdy/hud/app/config/Setting$IObserver;)V
    .locals 0
    .param p1, "observer"    # Lcom/navdy/hud/app/config/Setting$IObserver;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/navdy/hud/app/config/Setting;->observer:Lcom/navdy/hud/app/config/Setting$IObserver;

    .line 84
    return-void
.end method
