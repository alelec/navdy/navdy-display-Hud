.class public Lcom/navdy/hud/app/config/BooleanSetting;
.super Lcom/navdy/hud/app/config/Setting;
.source "BooleanSetting.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/config/BooleanSetting$Scope;
    }
.end annotation


# instance fields
.field private changed:Z

.field private defaultValue:Z

.field private enabled:Z

.field private scope:Lcom/navdy/hud/app/config/BooleanSetting$Scope;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/navdy/hud/app/config/BooleanSetting$Scope;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "scope"    # Lcom/navdy/hud/app/config/BooleanSetting$Scope;
    .param p3, "path"    # Ljava/lang/String;
    .param p4, "description"    # Ljava/lang/String;

    .prologue
    .line 28
    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/config/BooleanSetting;-><init>(Ljava/lang/String;Lcom/navdy/hud/app/config/BooleanSetting$Scope;ZLjava/lang/String;Ljava/lang/String;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/navdy/hud/app/config/BooleanSetting$Scope;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "scope"    # Lcom/navdy/hud/app/config/BooleanSetting$Scope;
    .param p3, "defaultValue"    # Z
    .param p4, "path"    # Ljava/lang/String;
    .param p5, "description"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-direct {p0, p1, p4, p5}, Lcom/navdy/hud/app/config/Setting;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    iput-object p2, p0, Lcom/navdy/hud/app/config/BooleanSetting;->scope:Lcom/navdy/hud/app/config/BooleanSetting$Scope;

    .line 35
    iput-boolean p3, p0, Lcom/navdy/hud/app/config/BooleanSetting;->defaultValue:Z

    .line 37
    invoke-virtual {p0}, Lcom/navdy/hud/app/config/BooleanSetting;->load()V

    .line 38
    return-void
.end method


# virtual methods
.method public isEnabled()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 41
    sget-object v2, Lcom/navdy/hud/app/config/BooleanSetting$1;->$SwitchMap$com$navdy$hud$app$config$BooleanSetting$Scope:[I

    iget-object v3, p0, Lcom/navdy/hud/app/config/BooleanSetting;->scope:Lcom/navdy/hud/app/config/BooleanSetting$Scope;

    invoke-virtual {v3}, Lcom/navdy/hud/app/config/BooleanSetting$Scope;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 55
    :goto_0
    :pswitch_0
    return v1

    .line 43
    :pswitch_1
    iget-boolean v1, p0, Lcom/navdy/hud/app/config/BooleanSetting;->enabled:Z

    goto :goto_0

    .line 47
    :pswitch_2
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isUserBuild()Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_1

    :pswitch_3
    move v1, v0

    .line 53
    goto :goto_0

    .line 41
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public load()V
    .locals 3

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/navdy/hud/app/config/BooleanSetting;->getProperty()Ljava/lang/String;

    move-result-object v0

    .line 86
    .local v0, "propName":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 87
    invoke-static {v0}, Lcom/navdy/hud/app/util/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 88
    .local v1, "setting":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 89
    sget-object v2, Lcom/navdy/hud/app/config/BooleanSetting$Scope;->CUSTOM:Lcom/navdy/hud/app/config/BooleanSetting$Scope;

    iput-object v2, p0, Lcom/navdy/hud/app/config/BooleanSetting;->scope:Lcom/navdy/hud/app/config/BooleanSetting$Scope;

    .line 90
    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "1"

    .line 91
    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "yes"

    .line 92
    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, p0, Lcom/navdy/hud/app/config/BooleanSetting;->enabled:Z

    .line 97
    .end local v1    # "setting":Ljava/lang/String;
    :cond_1
    :goto_1
    return-void

    .line 92
    .restart local v1    # "setting":Ljava/lang/String;
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    .line 94
    :cond_3
    iget-boolean v2, p0, Lcom/navdy/hud/app/config/BooleanSetting;->defaultValue:Z

    iput-boolean v2, p0, Lcom/navdy/hud/app/config/BooleanSetting;->enabled:Z

    goto :goto_1
.end method

.method public save()V
    .locals 2

    .prologue
    .line 73
    iget-boolean v1, p0, Lcom/navdy/hud/app/config/BooleanSetting;->changed:Z

    if-eqz v1, :cond_0

    .line 74
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/hud/app/config/BooleanSetting;->changed:Z

    .line 75
    invoke-virtual {p0}, Lcom/navdy/hud/app/config/BooleanSetting;->getProperty()Ljava/lang/String;

    move-result-object v0

    .line 76
    .local v0, "propName":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 77
    iget-boolean v1, p0, Lcom/navdy/hud/app/config/BooleanSetting;->enabled:Z

    invoke-static {v1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/navdy/hud/app/util/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    .end local v0    # "propName":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public setEnabled(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    .line 59
    iget-object v0, p0, Lcom/navdy/hud/app/config/BooleanSetting;->scope:Lcom/navdy/hud/app/config/BooleanSetting$Scope;

    sget-object v1, Lcom/navdy/hud/app/config/BooleanSetting$Scope;->CUSTOM:Lcom/navdy/hud/app/config/BooleanSetting$Scope;

    if-eq v0, v1, :cond_0

    .line 60
    sget-object v0, Lcom/navdy/hud/app/config/BooleanSetting$Scope;->CUSTOM:Lcom/navdy/hud/app/config/BooleanSetting$Scope;

    iput-object v0, p0, Lcom/navdy/hud/app/config/BooleanSetting;->scope:Lcom/navdy/hud/app/config/BooleanSetting$Scope;

    .line 62
    :cond_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/config/BooleanSetting;->enabled:Z

    if-eq v0, p1, :cond_1

    .line 63
    iput-boolean p1, p0, Lcom/navdy/hud/app/config/BooleanSetting;->enabled:Z

    .line 64
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/config/BooleanSetting;->changed:Z

    .line 65
    invoke-virtual {p0}, Lcom/navdy/hud/app/config/BooleanSetting;->changed()V

    .line 66
    invoke-virtual {p0}, Lcom/navdy/hud/app/config/BooleanSetting;->save()V

    .line 68
    :cond_1
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 101
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "BooleanSetting{"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 102
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/navdy/hud/app/config/BooleanSetting;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    const-string v1, "enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/navdy/hud/app/config/BooleanSetting;->enabled:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 105
    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 106
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
