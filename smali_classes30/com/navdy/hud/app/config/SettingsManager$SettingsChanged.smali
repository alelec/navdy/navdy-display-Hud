.class public Lcom/navdy/hud/app/config/SettingsManager$SettingsChanged;
.super Ljava/lang/Object;
.source "SettingsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/config/SettingsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SettingsChanged"
.end annotation


# instance fields
.field public final path:Ljava/lang/String;

.field public final setting:Lcom/navdy/hud/app/config/Setting;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/navdy/hud/app/config/Setting;)V
    .locals 0
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "setting"    # Lcom/navdy/hud/app/config/Setting;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/navdy/hud/app/config/SettingsManager$SettingsChanged;->path:Ljava/lang/String;

    .line 24
    iput-object p2, p0, Lcom/navdy/hud/app/config/SettingsManager$SettingsChanged;->setting:Lcom/navdy/hud/app/config/Setting;

    .line 25
    return-void
.end method
