.class Lcom/navdy/hud/app/profile/DriverProfileManager$3;
.super Ljava/lang/Object;
.source "DriverProfileManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/profile/DriverProfileManager;->onDriverProfilePreferencesUpdate(Lcom/navdy/service/library/events/preferences/DriverProfilePreferencesUpdate;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/profile/DriverProfileManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/profile/DriverProfileManager;

    .prologue
    .line 257
    iput-object p1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$3;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 260
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$3;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    const/4 v1, 0x0

    # invokes: Lcom/navdy/hud/app/profile/DriverProfileManager;->changeLocale(Z)V
    invoke-static {v0, v1}, Lcom/navdy/hud/app/profile/DriverProfileManager;->access$100(Lcom/navdy/hud/app/profile/DriverProfileManager;Z)V

    .line 261
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$3;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    # getter for: Lcom/navdy/hud/app/profile/DriverProfileManager;->mBus:Lcom/squareup/otto/Bus;
    invoke-static {v0}, Lcom/navdy/hud/app/profile/DriverProfileManager;->access$200(Lcom/navdy/hud/app/profile/DriverProfileManager;)Lcom/squareup/otto/Bus;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/event/DriverProfileUpdated;

    sget-object v2, Lcom/navdy/hud/app/event/DriverProfileUpdated$State;->UP_TO_DATE:Lcom/navdy/hud/app/event/DriverProfileUpdated$State;

    invoke-direct {v1, v2}, Lcom/navdy/hud/app/event/DriverProfileUpdated;-><init>(Lcom/navdy/hud/app/event/DriverProfileUpdated$State;)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 262
    return-void
.end method
