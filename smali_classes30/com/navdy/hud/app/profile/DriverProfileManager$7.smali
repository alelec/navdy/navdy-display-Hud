.class Lcom/navdy/hud/app/profile/DriverProfileManager$7;
.super Ljava/lang/Object;
.source "DriverProfileManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/profile/DriverProfileManager;->onInputPreferencesUpdate(Lcom/navdy/service/library/events/preferences/InputPreferencesUpdate;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

.field final synthetic val$update:Lcom/navdy/service/library/events/preferences/InputPreferencesUpdate;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/profile/DriverProfileManager;Lcom/navdy/service/library/events/preferences/InputPreferencesUpdate;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/profile/DriverProfileManager;

    .prologue
    .line 371
    iput-object p1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$7;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    iput-object p2, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$7;->val$update:Lcom/navdy/service/library/events/preferences/InputPreferencesUpdate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 374
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$7;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    # getter for: Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-static {v0}, Lcom/navdy/hud/app/profile/DriverProfileManager;->access$500(Lcom/navdy/hud/app/profile/DriverProfileManager;)Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$7;->val$update:Lcom/navdy/service/library/events/preferences/InputPreferencesUpdate;

    iget-object v1, v1, Lcom/navdy/service/library/events/preferences/InputPreferencesUpdate;->preferences:Lcom/navdy/service/library/events/preferences/InputPreferences;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/profile/DriverProfile;->setInputPreferences(Lcom/navdy/service/library/events/preferences/InputPreferences;)V

    .line 375
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$7;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    # getter for: Lcom/navdy/hud/app/profile/DriverProfileManager;->theDefaultProfile:Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-static {v0}, Lcom/navdy/hud/app/profile/DriverProfileManager;->access$600(Lcom/navdy/hud/app/profile/DriverProfileManager;)Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$7;->val$update:Lcom/navdy/service/library/events/preferences/InputPreferencesUpdate;

    iget-object v1, v1, Lcom/navdy/service/library/events/preferences/InputPreferencesUpdate;->preferences:Lcom/navdy/service/library/events/preferences/InputPreferences;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/profile/DriverProfile;->setInputPreferences(Lcom/navdy/service/library/events/preferences/InputPreferences;)V

    .line 376
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$7;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    # getter for: Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-static {v0}, Lcom/navdy/hud/app/profile/DriverProfileManager;->access$500(Lcom/navdy/hud/app/profile/DriverProfileManager;)Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordPreferenceChange(Lcom/navdy/hud/app/profile/DriverProfile;)V

    .line 377
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$7;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    # getter for: Lcom/navdy/hud/app/profile/DriverProfileManager;->mBus:Lcom/squareup/otto/Bus;
    invoke-static {v0}, Lcom/navdy/hud/app/profile/DriverProfileManager;->access$200(Lcom/navdy/hud/app/profile/DriverProfileManager;)Lcom/squareup/otto/Bus;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$7;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    # getter for: Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-static {v1}, Lcom/navdy/hud/app/profile/DriverProfileManager;->access$500(Lcom/navdy/hud/app/profile/DriverProfileManager;)Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/profile/DriverProfile;->getInputPreferences()Lcom/navdy/service/library/events/preferences/InputPreferences;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 378
    return-void
.end method
