.class Lcom/navdy/hud/app/profile/DriverProfileManager$6;
.super Ljava/lang/Object;
.source "DriverProfileManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/profile/DriverProfileManager;->onNavigationPreferencesUpdate(Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

.field final synthetic val$update:Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/profile/DriverProfileManager;Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/profile/DriverProfileManager;

    .prologue
    .line 356
    iput-object p1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$6;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    iput-object p2, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$6;->val$update:Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 359
    iget-object v1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$6;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    # getter for: Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-static {v1}, Lcom/navdy/hud/app/profile/DriverProfileManager;->access$500(Lcom/navdy/hud/app/profile/DriverProfileManager;)Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$6;->val$update:Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;

    iget-object v2, v2, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;->preferences:Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/profile/DriverProfile;->setNavigationPreferences(Lcom/navdy/service/library/events/preferences/NavigationPreferences;)V

    .line 360
    iget-object v1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$6;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    # getter for: Lcom/navdy/hud/app/profile/DriverProfileManager;->theDefaultProfile:Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-static {v1}, Lcom/navdy/hud/app/profile/DriverProfileManager;->access$600(Lcom/navdy/hud/app/profile/DriverProfileManager;)Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$6;->val$update:Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;

    iget-object v2, v2, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;->preferences:Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/profile/DriverProfile;->setNavigationPreferences(Lcom/navdy/service/library/events/preferences/NavigationPreferences;)V

    .line 361
    iget-object v1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$6;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    # getter for: Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-static {v1}, Lcom/navdy/hud/app/profile/DriverProfileManager;->access$500(Lcom/navdy/hud/app/profile/DriverProfileManager;)Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/profile/DriverProfile;->getNavigationPreferences()Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    move-result-object v0

    .line 362
    .local v0, "preferences":Lcom/navdy/service/library/events/preferences/NavigationPreferences;
    iget-object v1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$6;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    # getter for: Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-static {v1}, Lcom/navdy/hud/app/profile/DriverProfileManager;->access$500(Lcom/navdy/hud/app/profile/DriverProfileManager;)Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v1

    invoke-static {v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordNavigationPreferenceChange(Lcom/navdy/hud/app/profile/DriverProfile;)V

    .line 363
    iget-object v1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$6;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    # getter for: Lcom/navdy/hud/app/profile/DriverProfileManager;->mSessionPrefs:Lcom/navdy/hud/app/profile/DriverSessionPreferences;
    invoke-static {v1}, Lcom/navdy/hud/app/profile/DriverProfileManager;->access$700(Lcom/navdy/hud/app/profile/DriverProfileManager;)Lcom/navdy/hud/app/profile/DriverSessionPreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/profile/DriverSessionPreferences;->getNavigationSessionPreference()Lcom/navdy/hud/app/maps/NavSessionPreferences;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/maps/NavSessionPreferences;->setDefault(Lcom/navdy/service/library/events/preferences/NavigationPreferences;)V

    .line 364
    iget-object v1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$6;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    # getter for: Lcom/navdy/hud/app/profile/DriverProfileManager;->mBus:Lcom/squareup/otto/Bus;
    invoke-static {v1}, Lcom/navdy/hud/app/profile/DriverProfileManager;->access$200(Lcom/navdy/hud/app/profile/DriverProfileManager;)Lcom/squareup/otto/Bus;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 365
    return-void
.end method
