.class Lcom/navdy/hud/app/profile/DriverProfileManager$5;
.super Ljava/lang/Object;
.source "DriverProfileManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/profile/DriverProfileManager;->onDriverProfilePreferencesUpdate(Lcom/navdy/service/library/events/preferences/DriverProfilePreferencesUpdate;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

.field final synthetic val$preferences:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/profile/DriverProfileManager;Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/profile/DriverProfileManager;

    .prologue
    .line 283
    iput-object p1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$5;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    iput-object p2, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$5;->val$preferences:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 286
    iget-object v4, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$5;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    iget-object v5, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$5;->val$preferences:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    .line 287
    # invokes: Lcom/navdy/hud/app/profile/DriverProfileManager;->findSupportedLocale(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;
    invoke-static {v4, v5}, Lcom/navdy/hud/app/profile/DriverProfileManager;->access$400(Lcom/navdy/hud/app/profile/DriverProfileManager;Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    move-result-object v0

    .line 288
    .local v0, "driverProfilePreferences":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;
    iget-object v4, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$5;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    # getter for: Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-static {v4}, Lcom/navdy/hud/app/profile/DriverProfileManager;->access$500(Lcom/navdy/hud/app/profile/DriverProfileManager;)Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/profile/DriverProfile;->getLocale()Ljava/util/Locale;

    move-result-object v3

    .line 289
    .local v3, "oldLocale":Ljava/util/Locale;
    iget-object v4, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$5;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    # getter for: Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-static {v4}, Lcom/navdy/hud/app/profile/DriverProfileManager;->access$500(Lcom/navdy/hud/app/profile/DriverProfileManager;)Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/navdy/hud/app/profile/DriverProfile;->setDriverProfilePreferences(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;)V

    .line 290
    iget-object v4, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$5;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    # getter for: Lcom/navdy/hud/app/profile/DriverProfileManager;->theDefaultProfile:Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-static {v4}, Lcom/navdy/hud/app/profile/DriverProfileManager;->access$600(Lcom/navdy/hud/app/profile/DriverProfileManager;)Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/navdy/hud/app/profile/DriverProfile;->setDriverProfilePreferences(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;)V

    .line 291
    iget-object v4, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$5;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    # getter for: Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-static {v4}, Lcom/navdy/hud/app/profile/DriverProfileManager;->access$500(Lcom/navdy/hud/app/profile/DriverProfileManager;)Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/profile/DriverProfile;->getLocale()Ljava/util/Locale;

    move-result-object v2

    .line 292
    .local v2, "newLocale":Ljava/util/Locale;
    invoke-virtual {v3, v2}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v1, 0x1

    .line 293
    .local v1, "localeChanged":Z
    :goto_0
    # getter for: Lcom/navdy/hud/app/profile/DriverProfileManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/profile/DriverProfileManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[HUD-locale] changed["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] current["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] new["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 294
    iget-object v4, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$5;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    # invokes: Lcom/navdy/hud/app/profile/DriverProfileManager;->changeLocale(Z)V
    invoke-static {v4, v1}, Lcom/navdy/hud/app/profile/DriverProfileManager;->access$100(Lcom/navdy/hud/app/profile/DriverProfileManager;Z)V

    .line 295
    iget-object v4, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$5;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    # getter for: Lcom/navdy/hud/app/profile/DriverProfileManager;->mBus:Lcom/squareup/otto/Bus;
    invoke-static {v4}, Lcom/navdy/hud/app/profile/DriverProfileManager;->access$200(Lcom/navdy/hud/app/profile/DriverProfileManager;)Lcom/squareup/otto/Bus;

    move-result-object v4

    new-instance v5, Lcom/navdy/hud/app/event/DriverProfileUpdated;

    sget-object v6, Lcom/navdy/hud/app/event/DriverProfileUpdated$State;->UPDATED:Lcom/navdy/hud/app/event/DriverProfileUpdated$State;

    invoke-direct {v5, v6}, Lcom/navdy/hud/app/event/DriverProfileUpdated;-><init>(Lcom/navdy/hud/app/event/DriverProfileUpdated$State;)V

    invoke-virtual {v4, v5}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 296
    return-void

    .line 292
    .end local v1    # "localeChanged":Z
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
