.class public Lcom/navdy/hud/app/profile/NotificationSettings;
.super Ljava/lang/Object;
.source "NotificationSettings.java"


# static fields
.field public static final APP_ID_CALENDAR:Ljava/lang/String; = "com.apple.mobilecal"

.field public static final APP_ID_MAIL:Ljava/lang/String; = "com.apple.mobilemail"

.field public static final APP_ID_PHONE:Ljava/lang/String; = "com.apple.mobilephone"

.field public static final APP_ID_REMINDERS:Ljava/lang/String; = "com.apple.reminders"

.field public static final APP_ID_SMS:Ljava/lang/String; = "com.apple.MobileSMS"

.field private static sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private enabledApps:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final lock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/profile/NotificationSettings;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/profile/NotificationSettings;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/preferences/NotificationPreferences;)V
    .locals 1
    .param p1, "preferences"    # Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/profile/NotificationSettings;->enabledApps:Ljava/util/Map;

    .line 30
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/profile/NotificationSettings;->lock:Ljava/lang/Object;

    .line 33
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/profile/NotificationSettings;->update(Lcom/navdy/service/library/events/preferences/NotificationPreferences;)V

    .line 34
    return-void
.end method


# virtual methods
.method public enabled(Lcom/navdy/ancs/AppleNotification;)Ljava/lang/Boolean;
    .locals 1
    .param p1, "notification"    # Lcom/navdy/ancs/AppleNotification;

    .prologue
    .line 69
    invoke-virtual {p1}, Lcom/navdy/ancs/AppleNotification;->getAppId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/profile/NotificationSettings;->enabled(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public enabled(Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 3
    .param p1, "appId"    # Ljava/lang/String;

    .prologue
    .line 55
    iget-object v2, p0, Lcom/navdy/hud/app/profile/NotificationSettings;->lock:Ljava/lang/Object;

    monitor-enter v2

    .line 56
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/profile/NotificationSettings;->enabledApps:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 57
    .local v0, "setting":Ljava/lang/Boolean;
    monitor-exit v2

    .line 59
    return-object v0

    .line 57
    .end local v0    # "setting":Ljava/lang/Boolean;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public enabledApps()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 73
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 74
    .local v1, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/navdy/hud/app/profile/NotificationSettings;->lock:Ljava/lang/Object;

    monitor-enter v3

    .line 75
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/profile/NotificationSettings;->enabledApps:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 76
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Boolean;>;"
    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 77
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 80
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Boolean;>;"
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_1
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 81
    return-object v1
.end method

.method public update(Lcom/navdy/service/library/events/preferences/NotificationPreferences;)V
    .locals 6
    .param p1, "preferences"    # Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    .prologue
    .line 37
    iget-object v2, p0, Lcom/navdy/hud/app/profile/NotificationSettings;->lock:Ljava/lang/Object;

    monitor-enter v2

    .line 38
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/profile/NotificationSettings;->enabledApps:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 39
    if-eqz p1, :cond_0

    iget-object v1, p1, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->settings:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 40
    iget-object v1, p1, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->settings:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/notification/NotificationSetting;

    .line 41
    .local v0, "setting":Lcom/navdy/service/library/events/notification/NotificationSetting;
    iget-object v3, p0, Lcom/navdy/hud/app/profile/NotificationSettings;->enabledApps:Ljava/util/Map;

    iget-object v4, v0, Lcom/navdy/service/library/events/notification/NotificationSetting;->app:Ljava/lang/String;

    iget-object v5, v0, Lcom/navdy/service/library/events/notification/NotificationSetting;->enabled:Ljava/lang/Boolean;

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 44
    .end local v0    # "setting":Lcom/navdy/service/library/events/notification/NotificationSetting;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_0
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 45
    return-void
.end method
