.class Lcom/navdy/hud/app/profile/DriverProfileManager$9;
.super Ljava/lang/Object;
.source "DriverProfileManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/profile/DriverProfileManager;->onNotificationPreferencesUpdate(Lcom/navdy/service/library/events/preferences/NotificationPreferencesUpdate;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

.field final synthetic val$update:Lcom/navdy/service/library/events/preferences/NotificationPreferencesUpdate;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/profile/DriverProfileManager;Lcom/navdy/service/library/events/preferences/NotificationPreferencesUpdate;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/profile/DriverProfileManager;

    .prologue
    .line 397
    iput-object p1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$9;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    iput-object p2, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$9;->val$update:Lcom/navdy/service/library/events/preferences/NotificationPreferencesUpdate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 400
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$9;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    # getter for: Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-static {v0}, Lcom/navdy/hud/app/profile/DriverProfileManager;->access$500(Lcom/navdy/hud/app/profile/DriverProfileManager;)Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$9;->val$update:Lcom/navdy/service/library/events/preferences/NotificationPreferencesUpdate;

    iget-object v1, v1, Lcom/navdy/service/library/events/preferences/NotificationPreferencesUpdate;->preferences:Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/profile/DriverProfile;->setNotificationPreferences(Lcom/navdy/service/library/events/preferences/NotificationPreferences;)V

    .line 401
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$9;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    # getter for: Lcom/navdy/hud/app/profile/DriverProfileManager;->theDefaultProfile:Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-static {v0}, Lcom/navdy/hud/app/profile/DriverProfileManager;->access$600(Lcom/navdy/hud/app/profile/DriverProfileManager;)Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$9;->val$update:Lcom/navdy/service/library/events/preferences/NotificationPreferencesUpdate;

    iget-object v1, v1, Lcom/navdy/service/library/events/preferences/NotificationPreferencesUpdate;->preferences:Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/profile/DriverProfile;->setNotificationPreferences(Lcom/navdy/service/library/events/preferences/NotificationPreferences;)V

    .line 402
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$9;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    # getter for: Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-static {v0}, Lcom/navdy/hud/app/profile/DriverProfileManager;->access$500(Lcom/navdy/hud/app/profile/DriverProfileManager;)Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordPreferenceChange(Lcom/navdy/hud/app/profile/DriverProfile;)V

    .line 403
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$9;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    # getter for: Lcom/navdy/hud/app/profile/DriverProfileManager;->mBus:Lcom/squareup/otto/Bus;
    invoke-static {v0}, Lcom/navdy/hud/app/profile/DriverProfileManager;->access$200(Lcom/navdy/hud/app/profile/DriverProfileManager;)Lcom/squareup/otto/Bus;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$9;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    # getter for: Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-static {v1}, Lcom/navdy/hud/app/profile/DriverProfileManager;->access$500(Lcom/navdy/hud/app/profile/DriverProfileManager;)Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/profile/DriverProfile;->getNotificationPreferences()Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 404
    return-void
.end method
