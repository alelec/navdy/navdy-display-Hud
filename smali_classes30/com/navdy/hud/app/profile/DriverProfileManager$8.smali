.class Lcom/navdy/hud/app/profile/DriverProfileManager$8;
.super Ljava/lang/Object;
.source "DriverProfileManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/profile/DriverProfileManager;->onDisplaySpeakerPreferencesUpdate(Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

.field final synthetic val$update:Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/profile/DriverProfileManager;Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/profile/DriverProfileManager;

    .prologue
    .line 384
    iput-object p1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$8;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    iput-object p2, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$8;->val$update:Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 387
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$8;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    # getter for: Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-static {v0}, Lcom/navdy/hud/app/profile/DriverProfileManager;->access$500(Lcom/navdy/hud/app/profile/DriverProfileManager;)Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$8;->val$update:Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate;

    iget-object v1, v1, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate;->preferences:Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/profile/DriverProfile;->setSpeakerPreferences(Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;)V

    .line 388
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$8;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    # getter for: Lcom/navdy/hud/app/profile/DriverProfileManager;->theDefaultProfile:Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-static {v0}, Lcom/navdy/hud/app/profile/DriverProfileManager;->access$600(Lcom/navdy/hud/app/profile/DriverProfileManager;)Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$8;->val$update:Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate;

    iget-object v1, v1, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate;->preferences:Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/profile/DriverProfile;->setSpeakerPreferences(Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;)V

    .line 389
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$8;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    # getter for: Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-static {v0}, Lcom/navdy/hud/app/profile/DriverProfileManager;->access$500(Lcom/navdy/hud/app/profile/DriverProfileManager;)Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordPreferenceChange(Lcom/navdy/hud/app/profile/DriverProfile;)V

    .line 390
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$8;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    # getter for: Lcom/navdy/hud/app/profile/DriverProfileManager;->mBus:Lcom/squareup/otto/Bus;
    invoke-static {v0}, Lcom/navdy/hud/app/profile/DriverProfileManager;->access$200(Lcom/navdy/hud/app/profile/DriverProfileManager;)Lcom/squareup/otto/Bus;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$8;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    # getter for: Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-static {v1}, Lcom/navdy/hud/app/profile/DriverProfileManager;->access$500(Lcom/navdy/hud/app/profile/DriverProfileManager;)Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/profile/DriverProfile;->getSpeakerPreferences()Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 391
    return-void
.end method
