.class public Lcom/navdy/hud/app/profile/DriverProfile;
.super Ljava/lang/Object;
.source "DriverProfile.java"


# static fields
.field private static final CANNED_MESSAGES:Ljava/lang/String; = "CannedMessages"

.field private static final CONTACTS_IMAGE_CACHE_DIR:Ljava/lang/String; = "contacts"

.field public static final DRIVER_PROFILE_IMAGE:Ljava/lang/String; = "DriverImage"

.field private static final DRIVER_PROFILE_PREFERENCES:Ljava/lang/String; = "DriverProfilePreferences"

.field private static final INPUT_PREFERENCES:Ljava/lang/String; = "InputPreferences"

.field private static final LOCALE_SEPARATOR:Ljava/lang/String; = "_"

.field private static final LOCAL_PREFERENCES:Ljava/lang/String; = "LocalPreferences"

.field private static final MUSIC_IMAGE_CACHE_DIR:Ljava/lang/String; = "music"

.field private static final NAVIGATION_PREFERENCES:Ljava/lang/String; = "NavigationPreferences"

.field private static final NOTIFICATION_PREFERENCES:Ljava/lang/String; = "NotificationPreferences"

.field private static final PLACES_IMAGE_CACHE_DIR:Ljava/lang/String; = "places"

.field private static final PREFERENCES_DIRECTORY:Ljava/lang/String; = "Preferences"

.field private static final SPEAKER_PREFERENCES:Ljava/lang/String; = "SpeakerPreferences"

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field mCannedMessages:Lcom/navdy/service/library/events/glances/CannedMessagesUpdate;

.field private mContactsImageDirectory:Ljava/io/File;

.field private mDriverImage:Landroid/graphics/Bitmap;

.field mDriverProfilePreferences:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

.field mInputPreferences:Lcom/navdy/service/library/events/preferences/InputPreferences;

.field private mLocalPreferences:Lcom/navdy/service/library/events/preferences/LocalPreferences;

.field private mLocale:Ljava/util/Locale;

.field private mMessageStore:Lcom/navdy/service/library/events/MessageStore;

.field private mMusicImageDirectory:Ljava/io/File;

.field mNavigationPreferences:Lcom/navdy/service/library/events/preferences/NavigationPreferences;

.field mNotificationPreferences:Lcom/navdy/service/library/events/preferences/NotificationPreferences;

.field private mNotificationSettings:Lcom/navdy/hud/app/profile/NotificationSettings;

.field private mPlacesImageDirectory:Ljava/io/File;

.field private mPreferencesDirectory:Ljava/io/File;

.field private mProfileDirectory:Ljava/io/File;

.field private mProfileName:Ljava/lang/String;

.field private mSpeakerPreferences:Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;

.field private mTrafficEnabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 42
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/profile/DriverProfile;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method protected constructor <init>(Ljava/io/File;)V
    .locals 7
    .param p1, "profileDirectory"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mProfileName:Ljava/lang/String;

    .line 106
    iput-object p1, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mProfileDirectory:Ljava/io/File;

    .line 108
    new-instance v4, Ljava/io/File;

    const-string v5, "Preferences"

    invoke-direct {v4, p1, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v4, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mPreferencesDirectory:Ljava/io/File;

    .line 110
    new-instance v4, Lcom/navdy/service/library/events/MessageStore;

    iget-object v5, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mPreferencesDirectory:Ljava/io/File;

    invoke-direct {v4, v5}, Lcom/navdy/service/library/events/MessageStore;-><init>(Ljava/io/File;)V

    iput-object v4, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mMessageStore:Lcom/navdy/service/library/events/MessageStore;

    .line 112
    new-instance v4, Ljava/io/File;

    const-string v5, "places"

    invoke-direct {v4, p1, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v4, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mPlacesImageDirectory:Ljava/io/File;

    .line 113
    iget-object v4, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mPlacesImageDirectory:Ljava/io/File;

    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->createDirectory(Ljava/io/File;)Z

    .line 114
    new-instance v4, Ljava/io/File;

    const-string v5, "contacts"

    invoke-direct {v4, p1, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v4, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mContactsImageDirectory:Ljava/io/File;

    .line 115
    iget-object v4, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mContactsImageDirectory:Ljava/io/File;

    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->createDirectory(Ljava/io/File;)Z

    .line 116
    new-instance v4, Ljava/io/File;

    const-string v5, "music"

    invoke-direct {v4, p1, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v4, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mMusicImageDirectory:Ljava/io/File;

    .line 117
    iget-object v4, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mMusicImageDirectory:Ljava/io/File;

    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->createDirectory(Ljava/io/File;)Z

    .line 119
    const-string v4, "DriverProfilePreferences"

    const-class v5, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    new-instance v6, Lcom/navdy/hud/app/profile/DriverProfile$1;

    invoke-direct {v6, p0}, Lcom/navdy/hud/app/profile/DriverProfile$1;-><init>(Lcom/navdy/hud/app/profile/DriverProfile;)V

    invoke-direct {p0, v4, v5, v6}, Lcom/navdy/hud/app/profile/DriverProfile;->readPreference(Ljava/lang/String;Ljava/lang/Class;Lcom/navdy/service/library/events/NavdyEventUtil$Initializer;)Lcom/squareup/wire/Message;

    move-result-object v4

    check-cast v4, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    iput-object v4, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mDriverProfilePreferences:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    .line 132
    const-string v4, "NavigationPreferences"

    const-class v5, Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    invoke-direct {p0, v4, v5}, Lcom/navdy/hud/app/profile/DriverProfile;->readPreference(Ljava/lang/String;Ljava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v4

    check-cast v4, Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    iput-object v4, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mNavigationPreferences:Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    .line 133
    invoke-direct {p0}, Lcom/navdy/hud/app/profile/DriverProfile;->setTraffic()V

    .line 135
    const-string v4, "InputPreferences"

    const-class v5, Lcom/navdy/service/library/events/preferences/InputPreferences;

    invoke-direct {p0, v4, v5}, Lcom/navdy/hud/app/profile/DriverProfile;->readPreference(Ljava/lang/String;Ljava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v4

    check-cast v4, Lcom/navdy/service/library/events/preferences/InputPreferences;

    iput-object v4, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mInputPreferences:Lcom/navdy/service/library/events/preferences/InputPreferences;

    .line 137
    const-string v4, "SpeakerPreferences"

    const-class v5, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;

    invoke-direct {p0, v4, v5}, Lcom/navdy/hud/app/profile/DriverProfile;->readPreference(Ljava/lang/String;Ljava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v4

    check-cast v4, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;

    iput-object v4, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mSpeakerPreferences:Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;

    .line 139
    const-string v4, "NotificationPreferences"

    const-class v5, Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    invoke-direct {p0, v4, v5}, Lcom/navdy/hud/app/profile/DriverProfile;->readPreference(Ljava/lang/String;Ljava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v4

    check-cast v4, Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    iput-object v4, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mNotificationPreferences:Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    .line 140
    new-instance v4, Lcom/navdy/hud/app/profile/NotificationSettings;

    iget-object v5, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mNotificationPreferences:Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    invoke-direct {v4, v5}, Lcom/navdy/hud/app/profile/NotificationSettings;-><init>(Lcom/navdy/service/library/events/preferences/NotificationPreferences;)V

    iput-object v4, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mNotificationSettings:Lcom/navdy/hud/app/profile/NotificationSettings;

    .line 142
    const-string v4, "LocalPreferences"

    const-class v5, Lcom/navdy/service/library/events/preferences/LocalPreferences;

    invoke-direct {p0, v4, v5}, Lcom/navdy/hud/app/profile/DriverProfile;->readPreference(Ljava/lang/String;Ljava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v4

    check-cast v4, Lcom/navdy/service/library/events/preferences/LocalPreferences;

    iput-object v4, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mLocalPreferences:Lcom/navdy/service/library/events/preferences/LocalPreferences;

    .line 144
    const-string v4, "CannedMessages"

    const-class v5, Lcom/navdy/service/library/events/glances/CannedMessagesUpdate;

    invoke-direct {p0, v4, v5}, Lcom/navdy/hud/app/profile/DriverProfile;->readPreference(Ljava/lang/String;Ljava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v4

    check-cast v4, Lcom/navdy/service/library/events/glances/CannedMessagesUpdate;

    iput-object v4, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mCannedMessages:Lcom/navdy/service/library/events/glances/CannedMessagesUpdate;

    .line 146
    const/4 v1, 0x0

    .line 148
    .local v1, "driverImageStream":Ljava/io/InputStream;
    :try_start_0
    new-instance v0, Ljava/io/File;

    iget-object v4, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mPreferencesDirectory:Ljava/io/File;

    const-string v5, "DriverImage"

    invoke-direct {v0, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 149
    .local v0, "driverImageFile":Ljava/io/File;
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 150
    .end local v1    # "driverImageStream":Ljava/io/InputStream;
    .local v2, "driverImageStream":Ljava/io/InputStream;
    :try_start_1
    invoke-static {v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mDriverImage:Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 154
    invoke-static {v2}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    move-object v1, v2

    .line 157
    .end local v0    # "driverImageFile":Ljava/io/File;
    .end local v2    # "driverImageStream":Ljava/io/InputStream;
    .restart local v1    # "driverImageStream":Ljava/io/InputStream;
    :goto_0
    invoke-direct {p0}, Lcom/navdy/hud/app/profile/DriverProfile;->setLocale()V

    .line 158
    return-void

    .line 151
    :catch_0
    move-exception v3

    .line 152
    .local v3, "e":Ljava/io/FileNotFoundException;
    :goto_1
    const/4 v4, 0x0

    :try_start_2
    iput-object v4, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mDriverImage:Landroid/graphics/Bitmap;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 154
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .end local v3    # "e":Ljava/io/FileNotFoundException;
    :catchall_0
    move-exception v4

    :goto_2
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v4

    .end local v1    # "driverImageStream":Ljava/io/InputStream;
    .restart local v0    # "driverImageFile":Ljava/io/File;
    .restart local v2    # "driverImageStream":Ljava/io/InputStream;
    :catchall_1
    move-exception v4

    move-object v1, v2

    .end local v2    # "driverImageStream":Ljava/io/InputStream;
    .restart local v1    # "driverImageStream":Ljava/io/InputStream;
    goto :goto_2

    .line 151
    .end local v1    # "driverImageStream":Ljava/io/InputStream;
    .restart local v2    # "driverImageStream":Ljava/io/InputStream;
    :catch_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "driverImageStream":Ljava/io/InputStream;
    .restart local v1    # "driverImageStream":Ljava/io/InputStream;
    goto :goto_1
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/profile/DriverProfile;)Ljava/io/File;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/profile/DriverProfile;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mProfileDirectory:Ljava/io/File;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/profile/DriverProfile;)Ljava/io/File;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/profile/DriverProfile;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mPreferencesDirectory:Ljava/io/File;

    return-object v0
.end method

.method static createProfileForId(Ljava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/profile/DriverProfile;
    .locals 6
    .param p0, "profileName"    # Ljava/lang/String;
    .param p1, "profileDirectoryName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 82
    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {p0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "."

    invoke-virtual {p0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 83
    :cond_0
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Profile names can\'t refer to directories"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 86
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 87
    .local v3, "profilePath":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 88
    .local v2, "profileDirectory":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->mkdir()Z

    move-result v4

    if-nez v4, :cond_2

    .line 89
    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-nez v4, :cond_2

    .line 90
    new-instance v4, Ljava/io/IOException;

    const-string v5, "could not create profile"

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 93
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "Preferences"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 94
    .local v1, "preferencesDirectoryPath":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 95
    .local v0, "preferencesDirectory":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    move-result v4

    if-nez v4, :cond_3

    .line 96
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-nez v4, :cond_3

    .line 97
    new-instance v4, Ljava/io/IOException;

    const-string v5, "could not create preferences directory"

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 100
    :cond_3
    new-instance v4, Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-direct {v4, v2}, Lcom/navdy/hud/app/profile/DriverProfile;-><init>(Ljava/io/File;)V

    return-object v4
.end method

.method private readPreference(Ljava/lang/String;Ljava/lang/Class;)Lcom/squareup/wire/Message;
    .locals 1
    .param p1, "filename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/wire/Message;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 161
    .local p2, "type":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mMessageStore:Lcom/navdy/service/library/events/MessageStore;

    invoke-virtual {v0, p1, p2}, Lcom/navdy/service/library/events/MessageStore;->readMessage(Ljava/lang/String;Ljava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v0

    return-object v0
.end method

.method private readPreference(Ljava/lang/String;Ljava/lang/Class;Lcom/navdy/service/library/events/NavdyEventUtil$Initializer;)Lcom/squareup/wire/Message;
    .locals 1
    .param p1, "filename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/wire/Message;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Lcom/navdy/service/library/events/NavdyEventUtil$Initializer",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 165
    .local p2, "type":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    .local p3, "initializer":Lcom/navdy/service/library/events/NavdyEventUtil$Initializer;, "Lcom/navdy/service/library/events/NavdyEventUtil$Initializer<TT;>;"
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mMessageStore:Lcom/navdy/service/library/events/MessageStore;

    invoke-virtual {v0, p1, p2, p3}, Lcom/navdy/service/library/events/MessageStore;->readMessage(Ljava/lang/String;Ljava/lang/Class;Lcom/navdy/service/library/events/NavdyEventUtil$Initializer;)Lcom/squareup/wire/Message;

    move-result-object v0

    return-object v0
.end method

.method private removeDriverImage()V
    .locals 3

    .prologue
    .line 391
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mDriverImage:Landroid/graphics/Bitmap;

    .line 392
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/profile/DriverProfile$2;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/profile/DriverProfile$2;-><init>(Lcom/navdy/hud/app/profile/DriverProfile;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 399
    return-void
.end method

.method private removeNulls(Lcom/squareup/wire/Message;)Lcom/squareup/wire/Message;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/wire/Message;",
            ">(TT;)TT;"
        }
    .end annotation

    .prologue
    .line 243
    .local p1, "message":Lcom/squareup/wire/Message;, "TT;"
    invoke-static {p1}, Lcom/navdy/service/library/events/NavdyEventUtil;->applyDefaults(Lcom/squareup/wire/Message;)Lcom/squareup/wire/Message;

    move-result-object v0

    return-object v0
.end method

.method private setLocale()V
    .locals 7

    .prologue
    .line 310
    const/4 v4, 0x0

    :try_start_0
    iput-object v4, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mLocale:Ljava/util/Locale;

    .line 311
    iget-object v4, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mDriverProfilePreferences:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    iget-object v2, v4, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->locale:Ljava/lang/String;

    .line 312
    .local v2, "localeStr":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 313
    invoke-static {v2}, Lcom/navdy/hud/app/profile/HudLocale;->getLocaleForID(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v1

    .line 314
    .local v1, "locale":Ljava/util/Locale;
    sget-object v4, Lcom/navdy/hud/app/profile/DriverProfile;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "locale string:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " locale:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " profile:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/navdy/hud/app/profile/DriverProfile;->getProfileName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " email:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/navdy/hud/app/profile/DriverProfile;->getDriverEmail()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 317
    :try_start_1
    invoke-virtual {v1}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v0

    .line 318
    .local v0, "language":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 319
    sget-object v4, Lcom/navdy/hud/app/profile/DriverProfile;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "locale no language"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 341
    .end local v0    # "language":Ljava/lang/String;
    .end local v1    # "locale":Ljava/util/Locale;
    .end local v2    # "localeStr":Ljava/lang/String;
    :goto_0
    return-void

    .line 331
    .restart local v0    # "language":Ljava/lang/String;
    .restart local v1    # "locale":Ljava/util/Locale;
    .restart local v2    # "localeStr":Ljava/lang/String;
    :cond_0
    iput-object v1, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mLocale:Ljava/util/Locale;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 332
    .end local v0    # "language":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 333
    .local v3, "t":Ljava/lang/Throwable;
    :try_start_2
    sget-object v4, Lcom/navdy/hud/app/profile/DriverProfile;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "locale not valid"

    invoke-virtual {v4, v5, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 338
    .end local v1    # "locale":Ljava/util/Locale;
    .end local v2    # "localeStr":Ljava/lang/String;
    .end local v3    # "t":Ljava/lang/Throwable;
    :catch_1
    move-exception v3

    .line 339
    .restart local v3    # "t":Ljava/lang/Throwable;
    sget-object v4, Lcom/navdy/hud/app/profile/DriverProfile;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "setLocale"

    invoke-virtual {v4, v5, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 336
    .end local v3    # "t":Ljava/lang/Throwable;
    .restart local v2    # "localeStr":Ljava/lang/String;
    :cond_1
    :try_start_3
    sget-object v4, Lcom/navdy/hud/app/profile/DriverProfile;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "no locale string"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0
.end method

.method private setTraffic()V
    .locals 1

    .prologue
    .line 354
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mTrafficEnabled:Z

    .line 359
    return-void
.end method

.method private writeMessageToFile(Lcom/squareup/wire/Message;Ljava/lang/String;)V
    .locals 1
    .param p1, "message"    # Lcom/squareup/wire/Message;
    .param p2, "fileName"    # Ljava/lang/String;

    .prologue
    .line 239
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mMessageStore:Lcom/navdy/service/library/events/MessageStore;

    invoke-virtual {v0, p1, p2}, Lcom/navdy/service/library/events/MessageStore;->writeMessage(Lcom/squareup/wire/Message;Ljava/lang/String;)V

    .line 240
    return-void
.end method


# virtual methods
.method copy(Lcom/navdy/hud/app/profile/DriverProfile;)V
    .locals 3
    .param p1, "profile"    # Lcom/navdy/hud/app/profile/DriverProfile;

    .prologue
    .line 434
    new-instance v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;

    iget-object v1, p1, Lcom/navdy/hud/app/profile/DriverProfile;->mDriverProfilePreferences:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;-><init>(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;)V

    invoke-virtual {v0}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->build()Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mDriverProfilePreferences:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    .line 435
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mDriverProfilePreferences:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    const-string v1, "DriverProfilePreferences"

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/profile/DriverProfile;->writeMessageToFile(Lcom/squareup/wire/Message;Ljava/lang/String;)V

    .line 437
    new-instance v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;

    iget-object v1, p1, Lcom/navdy/hud/app/profile/DriverProfile;->mNavigationPreferences:Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;-><init>(Lcom/navdy/service/library/events/preferences/NavigationPreferences;)V

    invoke-virtual {v0}, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->build()Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mNavigationPreferences:Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    .line 438
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mNavigationPreferences:Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    const-string v1, "NavigationPreferences"

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/profile/DriverProfile;->writeMessageToFile(Lcom/squareup/wire/Message;Ljava/lang/String;)V

    .line 440
    new-instance v0, Lcom/navdy/service/library/events/preferences/InputPreferences$Builder;

    iget-object v1, p1, Lcom/navdy/hud/app/profile/DriverProfile;->mInputPreferences:Lcom/navdy/service/library/events/preferences/InputPreferences;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/events/preferences/InputPreferences$Builder;-><init>(Lcom/navdy/service/library/events/preferences/InputPreferences;)V

    invoke-virtual {v0}, Lcom/navdy/service/library/events/preferences/InputPreferences$Builder;->build()Lcom/navdy/service/library/events/preferences/InputPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mInputPreferences:Lcom/navdy/service/library/events/preferences/InputPreferences;

    .line 441
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mInputPreferences:Lcom/navdy/service/library/events/preferences/InputPreferences;

    const-string v1, "InputPreferences"

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/profile/DriverProfile;->writeMessageToFile(Lcom/squareup/wire/Message;Ljava/lang/String;)V

    .line 443
    new-instance v0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences$Builder;

    iget-object v1, p1, Lcom/navdy/hud/app/profile/DriverProfile;->mSpeakerPreferences:Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences$Builder;-><init>(Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;)V

    invoke-virtual {v0}, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences$Builder;->build()Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mSpeakerPreferences:Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;

    .line 444
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mSpeakerPreferences:Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;

    const-string v1, "SpeakerPreferences"

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/profile/DriverProfile;->writeMessageToFile(Lcom/squareup/wire/Message;Ljava/lang/String;)V

    .line 446
    new-instance v0, Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;

    iget-object v1, p1, Lcom/navdy/hud/app/profile/DriverProfile;->mNotificationPreferences:Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;-><init>(Lcom/navdy/service/library/events/preferences/NotificationPreferences;)V

    invoke-virtual {v0}, Lcom/navdy/service/library/events/preferences/NotificationPreferences$Builder;->build()Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mNotificationPreferences:Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    .line 447
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mNotificationPreferences:Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    const-string v1, "NotificationPreferences"

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/profile/DriverProfile;->writeMessageToFile(Lcom/squareup/wire/Message;Ljava/lang/String;)V

    .line 449
    new-instance v0, Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;

    iget-object v1, p1, Lcom/navdy/hud/app/profile/DriverProfile;->mLocalPreferences:Lcom/navdy/service/library/events/preferences/LocalPreferences;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;-><init>(Lcom/navdy/service/library/events/preferences/LocalPreferences;)V

    invoke-virtual {v0}, Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;->build()Lcom/navdy/service/library/events/preferences/LocalPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mLocalPreferences:Lcom/navdy/service/library/events/preferences/LocalPreferences;

    .line 450
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mLocalPreferences:Lcom/navdy/service/library/events/preferences/LocalPreferences;

    const-string v1, "LocalPreferences"

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/profile/DriverProfile;->writeMessageToFile(Lcom/squareup/wire/Message;Ljava/lang/String;)V

    .line 452
    sget-object v0, Lcom/navdy/hud/app/profile/DriverProfile;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "copy done:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mPreferencesDirectory:Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 453
    return-void
.end method

.method public getCannedMessages()Lcom/navdy/service/library/events/glances/CannedMessagesUpdate;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mCannedMessages:Lcom/navdy/service/library/events/glances/CannedMessagesUpdate;

    return-object v0
.end method

.method public getCarMake()Ljava/lang/String;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mDriverProfilePreferences:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    iget-object v0, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->car_make:Ljava/lang/String;

    return-object v0
.end method

.method public getCarModel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mDriverProfilePreferences:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    iget-object v0, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->car_model:Ljava/lang/String;

    return-object v0
.end method

.method public getCarYear()Ljava/lang/String;
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mDriverProfilePreferences:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    iget-object v0, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->car_year:Ljava/lang/String;

    return-object v0
.end method

.method public getContactsImageDir()Ljava/io/File;
    .locals 1

    .prologue
    .line 410
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mContactsImageDirectory:Ljava/io/File;

    return-object v0
.end method

.method public getDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mDriverProfilePreferences:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    iget-object v0, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->device_name:Ljava/lang/String;

    return-object v0
.end method

.method public getDisplayFormat()Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mDriverProfilePreferences:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    iget-object v0, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->display_format:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

    return-object v0
.end method

.method public getDriverEmail()Ljava/lang/String;
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mDriverProfilePreferences:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    iget-object v0, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->driver_email:Ljava/lang/String;

    return-object v0
.end method

.method public getDriverImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 383
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mDriverImage:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getDriverImageFile()Ljava/io/File;
    .locals 3

    .prologue
    .line 387
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mPreferencesDirectory:Ljava/io/File;

    const-string v2, "DriverImage"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public getDriverName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mDriverProfilePreferences:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    iget-object v0, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->driver_name:Ljava/lang/String;

    return-object v0
.end method

.method public getFeatureMode()Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mDriverProfilePreferences:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    iget-object v0, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->feature_mode:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    return-object v0
.end method

.method public getFirstName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 251
    iget-object v2, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mDriverProfilePreferences:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    iget-object v0, v2, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->driver_name:Ljava/lang/String;

    .line 252
    .local v0, "driverName":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v2, v0

    .line 260
    :goto_0
    return-object v2

    .line 255
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 256
    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 257
    .local v1, "firstWordEnd":I
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 258
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_1
    move-object v2, v0

    .line 260
    goto :goto_0
.end method

.method public getInputPreferences()Lcom/navdy/service/library/events/preferences/InputPreferences;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mInputPreferences:Lcom/navdy/service/library/events/preferences/InputPreferences;

    return-object v0
.end method

.method public getLocalPreferences()Lcom/navdy/service/library/events/preferences/LocalPreferences;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mLocalPreferences:Lcom/navdy/service/library/events/preferences/LocalPreferences;

    return-object v0
.end method

.method public getLocale()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mLocale:Ljava/util/Locale;

    if-eqz v0, :cond_0

    .line 302
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mLocale:Ljava/util/Locale;

    .line 304
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    goto :goto_0
.end method

.method public getLongPressAction()Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;
    .locals 1

    .prologue
    .line 288
    invoke-static {}, Lcom/navdy/hud/app/ui/component/UISettings;->isLongPressActionPlaceSearch()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 289
    sget-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;->DIAL_LONG_PRESS_PLACE_SEARCH:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

    .line 291
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mDriverProfilePreferences:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    iget-object v0, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->dial_long_press_action:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

    goto :goto_0
.end method

.method public getMusicImageDir()Ljava/io/File;
    .locals 1

    .prologue
    .line 414
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mMusicImageDirectory:Ljava/io/File;

    return-object v0
.end method

.method public getNavigationPreferences()Lcom/navdy/service/library/events/preferences/NavigationPreferences;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mNavigationPreferences:Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    return-object v0
.end method

.method public getNotificationPreferences()Lcom/navdy/service/library/events/preferences/NotificationPreferences;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mNotificationPreferences:Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    return-object v0
.end method

.method public getNotificationSettings()Lcom/navdy/hud/app/profile/NotificationSettings;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mNotificationSettings:Lcom/navdy/hud/app/profile/NotificationSettings;

    return-object v0
.end method

.method public getObdBlacklistModificationTime()J
    .locals 2

    .prologue
    .line 370
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mDriverProfilePreferences:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    iget-object v0, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->obdBlacklistLastModified:Ljava/lang/Long;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mDriverProfilePreferences:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    iget-object v0, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->obdBlacklistLastModified:Ljava/lang/Long;

    .line 371
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public getObdScanSetting()Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;
    .locals 1

    .prologue
    .line 366
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mDriverProfilePreferences:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    iget-object v0, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->obdScanSetting:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    return-object v0
.end method

.method public getPlacesImageDir()Ljava/io/File;
    .locals 1

    .prologue
    .line 406
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mPlacesImageDirectory:Ljava/io/File;

    return-object v0
.end method

.method public getPreferencesDirectory()Ljava/io/File;
    .locals 1

    .prologue
    .line 418
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mPreferencesDirectory:Ljava/io/File;

    return-object v0
.end method

.method public getProfileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 402
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mProfileName:Ljava/lang/String;

    return-object v0
.end method

.method public getSpeakerPreferences()Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mSpeakerPreferences:Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;

    return-object v0
.end method

.method public getUnitSystem()Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mDriverProfilePreferences:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    iget-object v0, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->unit_system:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    return-object v0
.end method

.method public isAutoOnEnabled()Z
    .locals 2

    .prologue
    .line 362
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mDriverProfilePreferences:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    iget-object v0, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->auto_on_enabled:Ljava/lang/Boolean;

    sget-object v1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->DEFAULT_AUTO_ON_ENABLED:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isDefaultProfile()Z
    .locals 1

    .prologue
    .line 423
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getDriverProfileManager()Lcom/navdy/hud/app/profile/DriverProfileManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/profile/DriverProfileManager;->isDefaultProfile(Lcom/navdy/hud/app/profile/DriverProfile;)Z

    move-result v0

    return v0
.end method

.method public isLimitBandwidthModeOn()Z
    .locals 2

    .prologue
    .line 375
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v1, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mDriverProfilePreferences:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    iget-object v1, v1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->limit_bandwidth:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isProfilePublic()Z
    .locals 2

    .prologue
    .line 379
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v1, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mDriverProfilePreferences:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    iget-object v1, v1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->profile_is_public:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isTrafficEnabled()Z
    .locals 1

    .prologue
    .line 344
    iget-boolean v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mTrafficEnabled:Z

    return v0
.end method

.method setCannedMessages(Lcom/navdy/service/library/events/glances/CannedMessagesUpdate;)V
    .locals 3
    .param p1, "cannedMessages"    # Lcom/navdy/service/library/events/glances/CannedMessagesUpdate;

    .prologue
    .line 233
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/profile/DriverProfile;->removeNulls(Lcom/squareup/wire/Message;)Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/glances/CannedMessagesUpdate;

    iput-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mCannedMessages:Lcom/navdy/service/library/events/glances/CannedMessagesUpdate;

    .line 234
    sget-object v0, Lcom/navdy/hud/app/profile/DriverProfile;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mProfileName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] updating canned messages preferences to ver["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/service/library/events/glances/CannedMessagesUpdate;->serial_number:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 235
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mCannedMessages:Lcom/navdy/service/library/events/glances/CannedMessagesUpdate;

    const-string v1, "CannedMessages"

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/profile/DriverProfile;->writeMessageToFile(Lcom/squareup/wire/Message;Ljava/lang/String;)V

    .line 236
    return-void
.end method

.method setDriverProfilePreferences(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;)V
    .locals 3
    .param p1, "preferences"    # Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    .prologue
    .line 169
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/profile/DriverProfile;->removeNulls(Lcom/squareup/wire/Message;)Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    iput-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mDriverProfilePreferences:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    .line 170
    invoke-direct {p0}, Lcom/navdy/hud/app/profile/DriverProfile;->setLocale()V

    .line 171
    sget-object v0, Lcom/navdy/hud/app/profile/DriverProfile;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mProfileName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] updating driver profile preferences to ver["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->serial_number:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mDriverProfilePreferences:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 172
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mDriverProfilePreferences:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    const-string v1, "DriverProfilePreferences"

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/profile/DriverProfile;->writeMessageToFile(Lcom/squareup/wire/Message;Ljava/lang/String;)V

    .line 173
    return-void
.end method

.method setInputPreferences(Lcom/navdy/service/library/events/preferences/InputPreferences;)V
    .locals 3
    .param p1, "preferences"    # Lcom/navdy/service/library/events/preferences/InputPreferences;

    .prologue
    .line 207
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/profile/DriverProfile;->removeNulls(Lcom/squareup/wire/Message;)Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/preferences/InputPreferences;

    iput-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mInputPreferences:Lcom/navdy/service/library/events/preferences/InputPreferences;

    .line 208
    sget-object v0, Lcom/navdy/hud/app/profile/DriverProfile;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mProfileName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] updating input preferences to ver["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/service/library/events/preferences/InputPreferences;->serial_number:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mInputPreferences:Lcom/navdy/service/library/events/preferences/InputPreferences;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 209
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mInputPreferences:Lcom/navdy/service/library/events/preferences/InputPreferences;

    const-string v1, "InputPreferences"

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/profile/DriverProfile;->writeMessageToFile(Lcom/squareup/wire/Message;Ljava/lang/String;)V

    .line 210
    return-void
.end method

.method public setLocalPreferences(Lcom/navdy/service/library/events/preferences/LocalPreferences;)V
    .locals 3
    .param p1, "preferences"    # Lcom/navdy/service/library/events/preferences/LocalPreferences;

    .prologue
    .line 227
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/profile/DriverProfile;->removeNulls(Lcom/squareup/wire/Message;)Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/preferences/LocalPreferences;

    iput-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mLocalPreferences:Lcom/navdy/service/library/events/preferences/LocalPreferences;

    .line 228
    sget-object v0, Lcom/navdy/hud/app/profile/DriverProfile;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mProfileName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] updating local preferences to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mLocalPreferences:Lcom/navdy/service/library/events/preferences/LocalPreferences;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 229
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mLocalPreferences:Lcom/navdy/service/library/events/preferences/LocalPreferences;

    const-string v1, "LocalPreferences"

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/profile/DriverProfile;->writeMessageToFile(Lcom/squareup/wire/Message;Ljava/lang/String;)V

    .line 230
    return-void
.end method

.method setNavigationPreferences(Lcom/navdy/service/library/events/preferences/NavigationPreferences;)V
    .locals 3
    .param p1, "preferences"    # Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    .prologue
    .line 200
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/profile/DriverProfile;->removeNulls(Lcom/squareup/wire/Message;)Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    iput-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mNavigationPreferences:Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    .line 201
    sget-object v0, Lcom/navdy/hud/app/profile/DriverProfile;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mProfileName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] updating nav preferences to ver["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->serial_number:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mNavigationPreferences:Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 202
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mNavigationPreferences:Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    const-string v1, "NavigationPreferences"

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/profile/DriverProfile;->writeMessageToFile(Lcom/squareup/wire/Message;Ljava/lang/String;)V

    .line 203
    invoke-direct {p0}, Lcom/navdy/hud/app/profile/DriverProfile;->setTraffic()V

    .line 204
    return-void
.end method

.method setNotificationPreferences(Lcom/navdy/service/library/events/preferences/NotificationPreferences;)V
    .locals 3
    .param p1, "preferences"    # Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    .prologue
    .line 219
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/profile/DriverProfile;->removeNulls(Lcom/squareup/wire/Message;)Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    iput-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mNotificationPreferences:Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    .line 220
    sget-object v0, Lcom/navdy/hud/app/profile/DriverProfile;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mProfileName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] updating notification preferences to ver["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->serial_number:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mNotificationPreferences:Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 221
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mNotificationPreferences:Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    const-string v1, "NotificationPreferences"

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/profile/DriverProfile;->writeMessageToFile(Lcom/squareup/wire/Message;Ljava/lang/String;)V

    .line 223
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mNotificationSettings:Lcom/navdy/hud/app/profile/NotificationSettings;

    iget-object v1, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mNotificationPreferences:Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/profile/NotificationSettings;->update(Lcom/navdy/service/library/events/preferences/NotificationPreferences;)V

    .line 224
    return-void
.end method

.method setSpeakerPreferences(Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;)V
    .locals 3
    .param p1, "preferences"    # Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;

    .prologue
    .line 213
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/profile/DriverProfile;->removeNulls(Lcom/squareup/wire/Message;)Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;

    iput-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mSpeakerPreferences:Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;

    .line 214
    sget-object v0, Lcom/navdy/hud/app/profile/DriverProfile;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mProfileName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] updating speaker preferences to ver["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->serial_number:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mSpeakerPreferences:Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 215
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mSpeakerPreferences:Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;

    const-string v1, "SpeakerPreferences"

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/profile/DriverProfile;->writeMessageToFile(Lcom/squareup/wire/Message;Ljava/lang/String;)V

    .line 216
    return-void
.end method

.method public setTrafficEnabled(Z)V
    .locals 0
    .param p1, "b"    # Z

    .prologue
    .line 348
    iput-boolean p1, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mTrafficEnabled:Z

    .line 349
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 428
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DriverProfile{mProfileName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/profile/DriverProfile;->mProfileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
