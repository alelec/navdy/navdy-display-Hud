.class Lcom/navdy/hud/app/profile/DriverProfile$1;
.super Lcom/navdy/service/library/events/NavdyEventUtil$Initializer;
.source "DriverProfile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/profile/DriverProfile;-><init>(Ljava/io/File;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/navdy/service/library/events/NavdyEventUtil$Initializer",
        "<",
        "Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/profile/DriverProfile;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/profile/DriverProfile;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/profile/DriverProfile;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/navdy/hud/app/profile/DriverProfile$1;->this$0:Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-direct {p0}, Lcom/navdy/service/library/events/NavdyEventUtil$Initializer;-><init>()V

    return-void
.end method


# virtual methods
.method public build(Lcom/squareup/wire/Message$Builder;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/wire/Message$Builder",
            "<",
            "Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;",
            ">;)",
            "Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;"
        }
    .end annotation

    .prologue
    .line 124
    .local p1, "builder":Lcom/squareup/wire/Message$Builder;, "Lcom/squareup/wire/Message$Builder<Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;>;"
    move-object v0, p1

    check-cast v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;

    .line 126
    .local v0, "profileBuilder":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Phone "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/profile/DriverProfile$1;->this$0:Lcom/navdy/hud/app/profile/DriverProfile;

    .line 127
    # getter for: Lcom/navdy/hud/app/profile/DriverProfile;->mProfileDirectory:Ljava/io/File;
    invoke-static {v2}, Lcom/navdy/hud/app/profile/DriverProfile;->access$000(Lcom/navdy/hud/app/profile/DriverProfile;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->device_name(Ljava/lang/String;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;

    move-result-object v1

    .line 128
    invoke-virtual {v1}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->build()Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic build(Lcom/squareup/wire/Message$Builder;)Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 121
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/profile/DriverProfile$1;->build(Lcom/squareup/wire/Message$Builder;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    move-result-object v0

    return-object v0
.end method
