.class Lcom/navdy/hud/app/profile/DriverProfileManager$12;
.super Ljava/lang/Object;
.source "DriverProfileManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/profile/DriverProfileManager;->copyCurrentProfileToDefault()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/profile/DriverProfileManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/profile/DriverProfileManager;

    .prologue
    .line 540
    iput-object p1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$12;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 543
    # getter for: Lcom/navdy/hud/app/profile/DriverProfileManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/profile/DriverProfileManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "copyCurrentProfileToDefault"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 544
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$12;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    # getter for: Lcom/navdy/hud/app/profile/DriverProfileManager;->theDefaultProfile:Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-static {v0}, Lcom/navdy/hud/app/profile/DriverProfileManager;->access$600(Lcom/navdy/hud/app/profile/DriverProfileManager;)Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$12;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    # getter for: Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-static {v1}, Lcom/navdy/hud/app/profile/DriverProfileManager;->access$500(Lcom/navdy/hud/app/profile/DriverProfileManager;)Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/profile/DriverProfile;->copy(Lcom/navdy/hud/app/profile/DriverProfile;)V

    .line 545
    return-void
.end method
