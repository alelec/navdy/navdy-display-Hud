.class public Lcom/navdy/hud/app/profile/DriverSessionPreferences;
.super Ljava/lang/Object;
.source "DriverSessionPreferences.java"


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private bus:Lcom/squareup/otto/Bus;

.field private clockConfiguration:Lcom/navdy/service/library/events/settings/DateTimeConfiguration;

.field private defaultClockConfiguration:Lcom/navdy/service/library/events/settings/DateTimeConfiguration;

.field private navSessionPreferences:Lcom/navdy/hud/app/maps/NavSessionPreferences;

.field private timeHelper:Lcom/navdy/hud/app/common/TimeHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 29
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/profile/DriverSessionPreferences;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/profile/DriverSessionPreferences;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/profile/DriverProfile;Lcom/navdy/hud/app/common/TimeHelper;)V
    .locals 4
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .param p2, "profile"    # Lcom/navdy/hud/app/profile/DriverProfile;
    .param p3, "timeHelper"    # Lcom/navdy/hud/app/common/TimeHelper;

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/navdy/hud/app/profile/DriverSessionPreferences;->bus:Lcom/squareup/otto/Bus;

    .line 40
    iput-object p3, p0, Lcom/navdy/hud/app/profile/DriverSessionPreferences;->timeHelper:Lcom/navdy/hud/app/common/TimeHelper;

    .line 41
    new-instance v0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;->CLOCK_12_HOUR:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;-><init>(Ljava/lang/Long;Ljava/lang/String;Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;)V

    iput-object v0, p0, Lcom/navdy/hud/app/profile/DriverSessionPreferences;->defaultClockConfiguration:Lcom/navdy/service/library/events/settings/DateTimeConfiguration;

    .line 42
    sget-object v0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;->CLOCK_12_HOUR:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    invoke-virtual {p3, v0}, Lcom/navdy/hud/app/common/TimeHelper;->setFormat(Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;)V

    .line 43
    new-instance v0, Lcom/navdy/hud/app/maps/NavSessionPreferences;

    invoke-virtual {p2}, Lcom/navdy/hud/app/profile/DriverProfile;->getNavigationPreferences()Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/maps/NavSessionPreferences;-><init>(Lcom/navdy/service/library/events/preferences/NavigationPreferences;)V

    iput-object v0, p0, Lcom/navdy/hud/app/profile/DriverSessionPreferences;->navSessionPreferences:Lcom/navdy/hud/app/maps/NavSessionPreferences;

    .line 44
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverSessionPreferences;->defaultClockConfiguration:Lcom/navdy/service/library/events/settings/DateTimeConfiguration;

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/profile/DriverSessionPreferences;->setClockConfiguration(Lcom/navdy/service/library/events/settings/DateTimeConfiguration;)V

    .line 45
    return-void
.end method


# virtual methods
.method public getNavigationSessionPreference()Lcom/navdy/hud/app/maps/NavSessionPreferences;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverSessionPreferences;->navSessionPreferences:Lcom/navdy/hud/app/maps/NavSessionPreferences;

    return-object v0
.end method

.method public setClockConfiguration(Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;)V
    .locals 3
    .param p1, "format"    # Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    .prologue
    .line 69
    if-eqz p1, :cond_0

    .line 70
    sget-object v0, Lcom/navdy/hud/app/profile/DriverSessionPreferences$1;->$SwitchMap$com$navdy$service$library$events$settings$DateTimeConfiguration$Clock:[I

    invoke-virtual {p1}, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 79
    :goto_0
    sget-object v0, Lcom/navdy/hud/app/profile/DriverSessionPreferences;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setClockConfiguration:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 80
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverSessionPreferences;->bus:Lcom/squareup/otto/Bus;

    sget-object v1, Lcom/navdy/hud/app/common/TimeHelper;->CLOCK_CHANGED:Lcom/navdy/hud/app/common/TimeHelper$UpdateClock;

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 82
    :cond_0
    return-void

    .line 72
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverSessionPreferences;->timeHelper:Lcom/navdy/hud/app/common/TimeHelper;

    sget-object v1, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;->CLOCK_12_HOUR:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/common/TimeHelper;->setFormat(Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;)V

    goto :goto_0

    .line 76
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverSessionPreferences;->timeHelper:Lcom/navdy/hud/app/common/TimeHelper;

    sget-object v1, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;->CLOCK_24_HOUR:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/common/TimeHelper;->setFormat(Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;)V

    goto :goto_0

    .line 70
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setClockConfiguration(Lcom/navdy/service/library/events/settings/DateTimeConfiguration;)V
    .locals 1
    .param p1, "clockConfiguration"    # Lcom/navdy/service/library/events/settings/DateTimeConfiguration;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/navdy/hud/app/profile/DriverSessionPreferences;->clockConfiguration:Lcom/navdy/service/library/events/settings/DateTimeConfiguration;

    .line 63
    iget-object v0, p1, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->format:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p1, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->format:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/profile/DriverSessionPreferences;->setClockConfiguration(Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;)V

    .line 66
    :cond_0
    return-void
.end method

.method public setDefault(Lcom/navdy/hud/app/profile/DriverProfile;Z)V
    .locals 3
    .param p1, "profile"    # Lcom/navdy/hud/app/profile/DriverProfile;
    .param p2, "isDefault"    # Z

    .prologue
    .line 52
    invoke-virtual {p1}, Lcom/navdy/hud/app/profile/DriverProfile;->getLocalPreferences()Lcom/navdy/service/library/events/preferences/LocalPreferences;

    move-result-object v0

    .line 53
    .local v0, "localPreferences":Lcom/navdy/service/library/events/preferences/LocalPreferences;
    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/navdy/service/library/events/preferences/LocalPreferences;->clockFormat:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    if-eqz v1, :cond_1

    .line 54
    iget-object v1, v0, Lcom/navdy/service/library/events/preferences/LocalPreferences;->clockFormat:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/profile/DriverSessionPreferences;->setClockConfiguration(Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;)V

    .line 58
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/navdy/hud/app/profile/DriverSessionPreferences;->navSessionPreferences:Lcom/navdy/hud/app/maps/NavSessionPreferences;

    invoke-virtual {p1}, Lcom/navdy/hud/app/profile/DriverProfile;->getNavigationPreferences()Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/maps/NavSessionPreferences;->setDefault(Lcom/navdy/service/library/events/preferences/NavigationPreferences;)V

    .line 59
    return-void

    .line 55
    :cond_1
    if-eqz p2, :cond_0

    .line 56
    iget-object v1, p0, Lcom/navdy/hud/app/profile/DriverSessionPreferences;->defaultClockConfiguration:Lcom/navdy/service/library/events/settings/DateTimeConfiguration;

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/profile/DriverSessionPreferences;->setClockConfiguration(Lcom/navdy/service/library/events/settings/DateTimeConfiguration;)V

    goto :goto_0
.end method
