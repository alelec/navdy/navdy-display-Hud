.class Lcom/navdy/hud/app/profile/DriverProfileManager$2;
.super Ljava/lang/Object;
.source "DriverProfileManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/profile/DriverProfileManager;->loadProfileForId(Lcom/navdy/service/library/device/NavdyDeviceId;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

.field final synthetic val$deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/profile/DriverProfileManager;Lcom/navdy/service/library/device/NavdyDeviceId;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/profile/DriverProfileManager;

    .prologue
    .line 135
    iput-object p1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$2;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    iput-object p2, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$2;->val$deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 138
    # getter for: Lcom/navdy/hud/app/profile/DriverProfileManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/profile/DriverProfileManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "loading profile:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$2;->val$deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 139
    iget-object v1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$2;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    iget-object v2, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$2;->val$deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getProfileForId(Lcom/navdy/service/library/device/NavdyDeviceId;)Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v0

    .line 140
    .local v0, "profile":Lcom/navdy/hud/app/profile/DriverProfile;
    if-nez v0, :cond_0

    .line 141
    # getter for: Lcom/navdy/hud/app/profile/DriverProfileManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/profile/DriverProfileManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "profile not loaded:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$2;->val$deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 142
    iget-object v1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$2;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    iget-object v2, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$2;->val$deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/profile/DriverProfileManager;->createProfileForId(Lcom/navdy/service/library/device/NavdyDeviceId;)Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v0

    .line 144
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager$2;->this$0:Lcom/navdy/hud/app/profile/DriverProfileManager;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/profile/DriverProfileManager;->setCurrentProfile(Lcom/navdy/hud/app/profile/DriverProfile;)V

    .line 145
    return-void
.end method
