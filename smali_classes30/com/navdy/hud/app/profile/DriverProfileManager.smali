.class public Lcom/navdy/hud/app/profile/DriverProfileManager;
.super Ljava/lang/Object;
.source "DriverProfileManager.java"


# static fields
.field private static final DEFAULT_PROFILE_NAME:Ljava/lang/String; = "DefaultProfile"

.field private static final DRIVER_PROFILE_CHANGED:Lcom/navdy/hud/app/event/DriverProfileChanged;

.field private static final LOCAL_PREFERENCES_SUFFIX:Ljava/lang/String; = "_preferences"

.field private static final MOCK_PROFILE_DEVICE_ID:Lcom/navdy/service/library/device/NavdyDeviceId;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private lastUserEmail:Ljava/lang/String;

.field private lastUserName:Ljava/lang/String;

.field private mAllProfiles:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/navdy/hud/app/profile/DriverProfile;",
            ">;"
        }
    .end annotation
.end field

.field private mBus:Lcom/squareup/otto/Bus;

.field private mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;

.field private mProfileDirectoryName:Ljava/lang/String;

.field private mPublicProfiles:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/navdy/hud/app/profile/DriverProfile;",
            ">;"
        }
    .end annotation
.end field

.field private mSessionPrefs:Lcom/navdy/hud/app/profile/DriverSessionPreferences;

.field private theDefaultProfile:Lcom/navdy/hud/app/profile/DriverProfile;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 64
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/profile/DriverProfileManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/profile/DriverProfileManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 65
    sget-object v0, Lcom/navdy/service/library/device/NavdyDeviceId;->UNKNOWN_ID:Lcom/navdy/service/library/device/NavdyDeviceId;

    sput-object v0, Lcom/navdy/hud/app/profile/DriverProfileManager;->MOCK_PROFILE_DEVICE_ID:Lcom/navdy/service/library/device/NavdyDeviceId;

    .line 68
    new-instance v0, Lcom/navdy/hud/app/event/DriverProfileChanged;

    invoke-direct {v0}, Lcom/navdy/hud/app/event/DriverProfileChanged;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/profile/DriverProfileManager;->DRIVER_PROFILE_CHANGED:Lcom/navdy/hud/app/event/DriverProfileChanged;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/storage/PathManager;Lcom/navdy/hud/app/common/TimeHelper;)V
    .locals 10
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .param p2, "pathManager"    # Lcom/navdy/hud/app/storage/PathManager;
    .param p3, "timeHelper"    # Lcom/navdy/hud/app/common/TimeHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    iput-object v7, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mAllProfiles:Ljava/util/HashMap;

    .line 75
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    iput-object v7, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mPublicProfiles:Ljava/util/Set;

    .line 86
    invoke-virtual {p2}, Lcom/navdy/hud/app/storage/PathManager;->getDriverProfilesDir()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mProfileDirectoryName:Ljava/lang/String;

    .line 87
    iput-object p1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mBus:Lcom/squareup/otto/Bus;

    .line 88
    iget-object v7, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mBus:Lcom/squareup/otto/Bus;

    invoke-virtual {v7, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 90
    sget-object v7, Lcom/navdy/hud/app/profile/DriverProfileManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "initializing in "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mProfileDirectoryName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 91
    new-instance v6, Ljava/io/File;

    iget-object v7, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mProfileDirectoryName:Ljava/lang/String;

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 92
    .local v6, "profilesDirectory":Ljava/io/File;
    new-instance v7, Lcom/navdy/hud/app/profile/DriverProfileManager$1;

    invoke-direct {v7, p0}, Lcom/navdy/hud/app/profile/DriverProfileManager$1;-><init>(Lcom/navdy/hud/app/profile/DriverProfileManager;)V

    invoke-virtual {v6, v7}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v3

    .line 99
    .local v3, "profileDirectories":[Ljava/io/File;
    array-length v8, v3

    const/4 v7, 0x0

    :goto_0
    if-ge v7, v8, :cond_2

    aget-object v4, v3, v7

    .line 102
    .local v4, "profileDirectory":Ljava/io/File;
    :try_start_0
    new-instance v2, Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-direct {v2, v4}, Lcom/navdy/hud/app/profile/DriverProfile;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 107
    .local v2, "profile":Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    .line 108
    .local v5, "profileName":Ljava/lang/String;
    iget-object v9, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mAllProfiles:Ljava/util/HashMap;

    invoke-virtual {v9, v5, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    invoke-virtual {v2}, Lcom/navdy/hud/app/profile/DriverProfile;->isProfilePublic()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 110
    iget-object v9, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mPublicProfiles:Ljava/util/Set;

    invoke-interface {v9, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 112
    :cond_0
    sget-object v9, Lcom/navdy/hud/app/profile/DriverProfileManager;->MOCK_PROFILE_DEVICE_ID:Lcom/navdy/service/library/device/NavdyDeviceId;

    invoke-direct {p0, v9}, Lcom/navdy/hud/app/profile/DriverProfileManager;->profileNameForId(Lcom/navdy/service/library/device/NavdyDeviceId;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 113
    iput-object v2, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->theDefaultProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    .line 99
    :cond_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 103
    .end local v2    # "profile":Lcom/navdy/hud/app/profile/DriverProfile;
    .end local v5    # "profileName":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 104
    .local v1, "e":Ljava/lang/Exception;
    sget-object v7, Lcom/navdy/hud/app/profile/DriverProfileManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "could not load profile from "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 116
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v4    # "profileDirectory":Ljava/io/File;
    :cond_2
    iget-object v7, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->theDefaultProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    if-nez v7, :cond_3

    .line 117
    sget-object v7, Lcom/navdy/hud/app/profile/DriverProfileManager;->MOCK_PROFILE_DEVICE_ID:Lcom/navdy/service/library/device/NavdyDeviceId;

    invoke-virtual {p0, v7}, Lcom/navdy/hud/app/profile/DriverProfileManager;->createProfileForId(Lcom/navdy/service/library/device/NavdyDeviceId;)Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v7

    iput-object v7, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->theDefaultProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    .line 121
    :cond_3
    iget-object v7, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->theDefaultProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-virtual {v7}, Lcom/navdy/hud/app/profile/DriverProfile;->getNavigationPreferences()Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    move-result-object v0

    .line 122
    .local v0, "defaultNavPrefs":Lcom/navdy/service/library/events/preferences/NavigationPreferences;
    new-instance v7, Lcom/navdy/hud/app/profile/DriverSessionPreferences;

    iget-object v8, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mBus:Lcom/squareup/otto/Bus;

    iget-object v9, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->theDefaultProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-direct {v7, v8, v9, p3}, Lcom/navdy/hud/app/profile/DriverSessionPreferences;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/profile/DriverProfile;Lcom/navdy/hud/app/common/TimeHelper;)V

    iput-object v7, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mSessionPrefs:Lcom/navdy/hud/app/profile/DriverSessionPreferences;

    .line 123
    iget-object v7, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->theDefaultProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-virtual {p0, v7}, Lcom/navdy/hud/app/profile/DriverProfileManager;->setCurrentProfile(Lcom/navdy/hud/app/profile/DriverProfile;)V

    .line 124
    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/navdy/hud/app/profile/DriverProfileManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/profile/DriverProfileManager;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/profile/DriverProfileManager;
    .param p1, "x1"    # Z

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/profile/DriverProfileManager;->changeLocale(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/profile/DriverProfileManager;)Lcom/squareup/otto/Bus;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/profile/DriverProfileManager;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mBus:Lcom/squareup/otto/Bus;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/profile/DriverProfileManager;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/profile/DriverProfileManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/profile/DriverProfileManager;->refreshProfileImageIfNeeded(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/profile/DriverProfileManager;Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/profile/DriverProfileManager;
    .param p1, "x1"    # Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/profile/DriverProfileManager;->findSupportedLocale(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/profile/DriverProfileManager;)Lcom/navdy/hud/app/profile/DriverProfile;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/profile/DriverProfileManager;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/profile/DriverProfileManager;)Lcom/navdy/hud/app/profile/DriverProfile;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/profile/DriverProfileManager;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->theDefaultProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    return-object v0
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/profile/DriverProfileManager;)Lcom/navdy/hud/app/profile/DriverSessionPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/profile/DriverProfileManager;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mSessionPrefs:Lcom/navdy/hud/app/profile/DriverSessionPreferences;

    return-object v0
.end method

.method private changeLocale(Z)V
    .locals 5
    .param p1, "showLanguageNotSupportedToast"    # Z

    .prologue
    .line 558
    :try_start_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v0

    .line 559
    .local v0, "locale":Ljava/util/Locale;
    sget-object v2, Lcom/navdy/hud/app/profile/DriverProfileManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[HUD-locale] changeLocale current="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 560
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/navdy/hud/app/profile/HudLocale;->switchLocale(Landroid/content/Context;Ljava/util/Locale;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 561
    sget-object v2, Lcom/navdy/hud/app/profile/DriverProfileManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "[HUD-locale] change, restarting..."

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 562
    iget-object v2, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mBus:Lcom/squareup/otto/Bus;

    new-instance v3, Lcom/navdy/hud/app/event/InitEvents$InitPhase;

    sget-object v4, Lcom/navdy/hud/app/event/InitEvents$Phase;->SWITCHING_LOCALE:Lcom/navdy/hud/app/event/InitEvents$Phase;

    invoke-direct {v3, v4}, Lcom/navdy/hud/app/event/InitEvents$InitPhase;-><init>(Lcom/navdy/hud/app/event/InitEvents$Phase;)V

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 563
    invoke-static {}, Lcom/navdy/hud/app/profile/HudLocale;->showLocaleChangeToast()V

    .line 578
    .end local v0    # "locale":Ljava/util/Locale;
    :cond_0
    :goto_0
    return-void

    .line 565
    .restart local v0    # "locale":Ljava/util/Locale;
    :cond_1
    sget-object v2, Lcom/navdy/hud/app/profile/DriverProfileManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "[HUD-locale] not changed"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 566
    iget-object v2, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mBus:Lcom/squareup/otto/Bus;

    new-instance v3, Lcom/navdy/hud/app/event/InitEvents$InitPhase;

    sget-object v4, Lcom/navdy/hud/app/event/InitEvents$Phase;->LOCALE_UP_TO_DATE:Lcom/navdy/hud/app/event/InitEvents$Phase;

    invoke-direct {v3, v4}, Lcom/navdy/hud/app/event/InitEvents$InitPhase;-><init>(Lcom/navdy/hud/app/event/InitEvents$Phase;)V

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 567
    invoke-static {v0}, Lcom/navdy/hud/app/profile/HudLocale;->isLocaleSupported(Ljava/util/Locale;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 568
    if-eqz p1, :cond_0

    .line 569
    invoke-virtual {v0}, Ljava/util/Locale;->getDisplayLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/navdy/hud/app/profile/HudLocale;->showLocaleNotSupportedToast(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 575
    .end local v0    # "locale":Ljava/util/Locale;
    :catch_0
    move-exception v1

    .line 576
    .local v1, "t":Ljava/lang/Throwable;
    sget-object v2, Lcom/navdy/hud/app/profile/DriverProfileManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "[HUD-locale] changeLocale"

    invoke-virtual {v2, v3, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 572
    .end local v1    # "t":Ljava/lang/Throwable;
    .restart local v0    # "locale":Ljava/util/Locale;
    :cond_2
    :try_start_1
    invoke-static {}, Lcom/navdy/hud/app/profile/HudLocale;->dismissToast()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private copyCurrentProfileToDefault()V
    .locals 3

    .prologue
    .line 539
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    iget-object v1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->theDefaultProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    if-eq v0, v1, :cond_0

    .line 540
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/profile/DriverProfileManager$12;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/profile/DriverProfileManager$12;-><init>(Lcom/navdy/hud/app/profile/DriverProfileManager;)V

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 548
    :cond_0
    return-void
.end method

.method private findSupportedLocale(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;
    .locals 9
    .param p1, "preferences"    # Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    .prologue
    .line 312
    iget-object v5, p1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->locale:Ljava/lang/String;

    invoke-static {v5}, Lcom/navdy/hud/app/profile/HudLocale;->getLocaleForID(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v3

    .line 313
    .local v3, "newLocale":Ljava/util/Locale;
    move-object v2, p1

    .line 314
    .local v2, "driverProfilePreferences":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;
    invoke-static {v3}, Lcom/navdy/hud/app/profile/HudLocale;->isLocaleSupported(Ljava/util/Locale;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 316
    iget-object v5, v2, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->additionalLocales:Ljava/util/List;

    if-eqz v5, :cond_2

    .line 317
    const/4 v4, 0x0

    .line 318
    .local v4, "supportedAdditionalLocale":Ljava/util/Locale;
    iget-object v5, v2, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->additionalLocales:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 319
    .local v1, "additionalLocaleStr":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 321
    invoke-static {v1}, Lcom/navdy/hud/app/profile/HudLocale;->getLocaleForID(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v0

    .line 322
    .local v0, "additionalLocale":Ljava/util/Locale;
    invoke-static {v0}, Lcom/navdy/hud/app/profile/HudLocale;->isLocaleSupported(Ljava/util/Locale;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 323
    sget-object v5, Lcom/navdy/hud/app/profile/DriverProfileManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "HUD-locale additional locale supported:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 324
    move-object v4, v0

    .line 330
    .end local v0    # "additionalLocale":Ljava/util/Locale;
    .end local v1    # "additionalLocaleStr":Ljava/lang/String;
    :cond_1
    if-eqz v4, :cond_2

    .line 331
    new-instance v5, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;

    invoke-direct {v5, v2}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;-><init>(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;)V

    .line 332
    invoke-virtual {v4}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->locale(Ljava/lang/String;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;

    move-result-object v5

    .line 333
    invoke-virtual {v5}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->build()Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    move-result-object v2

    .line 337
    .end local v4    # "supportedAdditionalLocale":Ljava/util/Locale;
    :cond_2
    return-object v2

    .line 327
    .restart local v0    # "additionalLocale":Ljava/util/Locale;
    .restart local v1    # "additionalLocaleStr":Ljava/lang/String;
    .restart local v4    # "supportedAdditionalLocale":Ljava/util/Locale;
    :cond_3
    sget-object v6, Lcom/navdy/hud/app/profile/DriverProfileManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "HUD-locale additional locale not supported:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private handleUpdateInBackground(Lcom/squareup/wire/Message;Lcom/navdy/service/library/events/RequestStatus;Lcom/squareup/wire/Message;Ljava/lang/Runnable;)V
    .locals 4
    .param p1, "request"    # Lcom/squareup/wire/Message;
    .param p2, "status"    # Lcom/navdy/service/library/events/RequestStatus;
    .param p3, "preferences"    # Lcom/squareup/wire/Message;
    .param p4, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 437
    iget-object v1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    iget-object v2, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->theDefaultProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    if-ne v1, v2, :cond_1

    .line 460
    :cond_0
    :goto_0
    return-void

    .line 440
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 441
    .local v0, "requestType":Ljava/lang/String;
    sget-object v1, Lcom/navdy/hud/app/profile/DriverProfileManager$13;->$SwitchMap$com$navdy$service$library$events$RequestStatus:[I

    invoke-virtual {p2}, Lcom/navdy/service/library/events/RequestStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 456
    sget-object v1, Lcom/navdy/hud/app/profile/DriverProfileManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "profile status for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", which I didn\'t expect; ignoring."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 443
    :pswitch_0
    sget-object v1, Lcom/navdy/hud/app/profile/DriverProfileManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " returned up to date!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 446
    :pswitch_1
    sget-object v1, Lcom/navdy/hud/app/profile/DriverProfileManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Received "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", applying"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 447
    iget-object v1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    if-eqz v1, :cond_0

    .line 448
    if-eqz p3, :cond_2

    .line 449
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v1

    const/16 v2, 0xa

    invoke-virtual {v1, p4, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto/16 :goto_0

    .line 451
    :cond_2
    sget-object v1, Lcom/navdy/hud/app/profile/DriverProfileManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with no preferences!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 441
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private profileNameForId(Lcom/navdy/service/library/device/NavdyDeviceId;)Ljava/lang/String;
    .locals 2
    .param p1, "deviceId"    # Lcom/navdy/service/library/device/NavdyDeviceId;

    .prologue
    .line 190
    invoke-virtual {p1}, Lcom/navdy/service/library/device/NavdyDeviceId;->getBluetoothAddress()Ljava/lang/String;

    move-result-object v0

    .line 191
    .local v0, "profileName":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 192
    const-string v0, "DefaultProfile"

    .line 195
    :cond_0
    invoke-static {v0}, Lcom/navdy/hud/app/util/GenericUtil;->normalizeToFilename(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private refreshProfileImageIfNeeded(Ljava/lang/String;)V
    .locals 6
    .param p1, "remoteChecksum"    # Ljava/lang/String;

    .prologue
    .line 341
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->getInstance()Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;

    move-result-object v0

    .line 342
    .local v0, "downloader":Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;
    const-string v2, "DriverImage"

    sget-object v3, Lcom/navdy/service/library/events/photo/PhotoType;->PHOTO_DRIVER_PROFILE:Lcom/navdy/service/library/events/photo/PhotoType;

    invoke-virtual {v0, v2, v3}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->hashFor(Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;)Ljava/lang/String;

    move-result-object v1

    .line 344
    .local v1, "localChecksum":Ljava/lang/String;
    invoke-static {p1, v1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 345
    sget-object v2, Lcom/navdy/hud/app/profile/DriverProfileManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "asking for new photo - local:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " remote:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 347
    const-string v2, "DriverImage"

    sget-object v3, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;->NORMAL:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;

    sget-object v4, Lcom/navdy/service/library/events/photo/PhotoType;->PHOTO_DRIVER_PROFILE:Lcom/navdy/service/library/events/photo/PhotoType;

    const/4 v5, 0x0

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;->submitDownload(Ljava/lang/String;Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$Priority;Lcom/navdy/service/library/events/photo/PhotoType;Ljava/lang/String;)V

    .line 352
    :cond_0
    return-void
.end method

.method private requestPreferenceUpdates(Lcom/navdy/hud/app/profile/DriverProfile;)V
    .locals 7
    .param p1, "profile"    # Lcom/navdy/hud/app/profile/DriverProfile;

    .prologue
    .line 199
    sget-object v4, Lcom/navdy/hud/app/profile/DriverProfileManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "requestPreferenceUpdates:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Lcom/navdy/hud/app/profile/DriverProfile;->getProfileName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 200
    new-instance v4, Lcom/navdy/service/library/events/preferences/DriverProfilePreferencesRequest$Builder;

    invoke-direct {v4}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferencesRequest$Builder;-><init>()V

    iget-object v5, p1, Lcom/navdy/hud/app/profile/DriverProfile;->mDriverProfilePreferences:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    iget-object v5, v5, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->serial_number:Ljava/lang/Long;

    .line 201
    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferencesRequest$Builder;->serial_number(Ljava/lang/Long;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferencesRequest$Builder;

    move-result-object v4

    .line 202
    invoke-virtual {v4}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferencesRequest$Builder;->build()Lcom/navdy/service/library/events/preferences/DriverProfilePreferencesRequest;

    move-result-object v3

    .line 203
    .local v3, "request":Lcom/navdy/service/library/events/preferences/DriverProfilePreferencesRequest;
    invoke-direct {p0, v3}, Lcom/navdy/hud/app/profile/DriverProfileManager;->sendEventToClient(Lcom/squareup/wire/Message;)V

    .line 205
    sget-object v4, Lcom/navdy/hud/app/profile/DriverProfileManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "requestPreferenceUpdates: nav:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p1, Lcom/navdy/hud/app/profile/DriverProfile;->mNavigationPreferences:Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    iget-object v6, v6, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->serial_number:Ljava/lang/Long;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 206
    new-instance v4, Lcom/navdy/service/library/events/preferences/NavigationPreferencesRequest$Builder;

    invoke-direct {v4}, Lcom/navdy/service/library/events/preferences/NavigationPreferencesRequest$Builder;-><init>()V

    iget-object v5, p1, Lcom/navdy/hud/app/profile/DriverProfile;->mNavigationPreferences:Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    iget-object v5, v5, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->serial_number:Ljava/lang/Long;

    .line 207
    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/preferences/NavigationPreferencesRequest$Builder;->serial_number(Ljava/lang/Long;)Lcom/navdy/service/library/events/preferences/NavigationPreferencesRequest$Builder;

    move-result-object v4

    .line 208
    invoke-virtual {v4}, Lcom/navdy/service/library/events/preferences/NavigationPreferencesRequest$Builder;->build()Lcom/navdy/service/library/events/preferences/NavigationPreferencesRequest;

    move-result-object v1

    .line 209
    .local v1, "navRequest":Lcom/navdy/service/library/events/preferences/NavigationPreferencesRequest;
    invoke-direct {p0, v1}, Lcom/navdy/hud/app/profile/DriverProfileManager;->sendEventToClient(Lcom/squareup/wire/Message;)V

    .line 211
    sget-object v4, Lcom/navdy/hud/app/profile/DriverProfileManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "requestPreferenceUpdates: notif:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p1, Lcom/navdy/hud/app/profile/DriverProfile;->mNotificationPreferences:Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    iget-object v6, v6, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->serial_number:Ljava/lang/Long;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 212
    new-instance v4, Lcom/navdy/service/library/events/preferences/NotificationPreferencesRequest$Builder;

    invoke-direct {v4}, Lcom/navdy/service/library/events/preferences/NotificationPreferencesRequest$Builder;-><init>()V

    iget-object v5, p1, Lcom/navdy/hud/app/profile/DriverProfile;->mNotificationPreferences:Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    iget-object v5, v5, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->serial_number:Ljava/lang/Long;

    .line 213
    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/preferences/NotificationPreferencesRequest$Builder;->serial_number(Ljava/lang/Long;)Lcom/navdy/service/library/events/preferences/NotificationPreferencesRequest$Builder;

    move-result-object v4

    .line 214
    invoke-virtual {v4}, Lcom/navdy/service/library/events/preferences/NotificationPreferencesRequest$Builder;->build()Lcom/navdy/service/library/events/preferences/NotificationPreferencesRequest;

    move-result-object v2

    .line 215
    .local v2, "notifRequest":Lcom/navdy/service/library/events/preferences/NotificationPreferencesRequest;
    invoke-direct {p0, v2}, Lcom/navdy/hud/app/profile/DriverProfileManager;->sendEventToClient(Lcom/squareup/wire/Message;)V

    .line 217
    sget-object v4, Lcom/navdy/hud/app/profile/DriverProfileManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "requestPreferenceUpdates: input:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p1, Lcom/navdy/hud/app/profile/DriverProfile;->mInputPreferences:Lcom/navdy/service/library/events/preferences/InputPreferences;

    iget-object v6, v6, Lcom/navdy/service/library/events/preferences/InputPreferences;->serial_number:Ljava/lang/Long;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 218
    new-instance v4, Lcom/navdy/service/library/events/preferences/InputPreferencesRequest$Builder;

    invoke-direct {v4}, Lcom/navdy/service/library/events/preferences/InputPreferencesRequest$Builder;-><init>()V

    iget-object v5, p1, Lcom/navdy/hud/app/profile/DriverProfile;->mInputPreferences:Lcom/navdy/service/library/events/preferences/InputPreferences;

    iget-object v5, v5, Lcom/navdy/service/library/events/preferences/InputPreferences;->serial_number:Ljava/lang/Long;

    .line 219
    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/preferences/InputPreferencesRequest$Builder;->serial_number(Ljava/lang/Long;)Lcom/navdy/service/library/events/preferences/InputPreferencesRequest$Builder;

    move-result-object v4

    .line 220
    invoke-virtual {v4}, Lcom/navdy/service/library/events/preferences/InputPreferencesRequest$Builder;->build()Lcom/navdy/service/library/events/preferences/InputPreferencesRequest;

    move-result-object v0

    .line 221
    .local v0, "inputPrefRequest":Lcom/navdy/service/library/events/preferences/InputPreferencesRequest;
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/profile/DriverProfileManager;->sendEventToClient(Lcom/squareup/wire/Message;)V

    .line 222
    return-void
.end method

.method private requestUpdates(Lcom/navdy/hud/app/profile/DriverProfile;)V
    .locals 4
    .param p1, "profile"    # Lcom/navdy/hud/app/profile/DriverProfile;

    .prologue
    .line 225
    sget-object v1, Lcom/navdy/hud/app/profile/DriverProfileManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "requestUpdates(canned-msgs) ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/hud/app/profile/DriverProfile;->mCannedMessages:Lcom/navdy/service/library/events/glances/CannedMessagesUpdate;

    iget-object v3, v3, Lcom/navdy/service/library/events/glances/CannedMessagesUpdate;->serial_number:Ljava/lang/Long;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 226
    new-instance v1, Lcom/navdy/service/library/events/glances/CannedMessagesRequest$Builder;

    invoke-direct {v1}, Lcom/navdy/service/library/events/glances/CannedMessagesRequest$Builder;-><init>()V

    iget-object v2, p1, Lcom/navdy/hud/app/profile/DriverProfile;->mCannedMessages:Lcom/navdy/service/library/events/glances/CannedMessagesUpdate;

    iget-object v2, v2, Lcom/navdy/service/library/events/glances/CannedMessagesUpdate;->serial_number:Ljava/lang/Long;

    .line 227
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/glances/CannedMessagesRequest$Builder;->serial_number(Ljava/lang/Long;)Lcom/navdy/service/library/events/glances/CannedMessagesRequest$Builder;

    move-result-object v1

    .line 228
    invoke-virtual {v1}, Lcom/navdy/service/library/events/glances/CannedMessagesRequest$Builder;->build()Lcom/navdy/service/library/events/glances/CannedMessagesRequest;

    move-result-object v0

    .line 229
    .local v0, "cannedMessagesRequest":Lcom/navdy/service/library/events/glances/CannedMessagesRequest;
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/profile/DriverProfileManager;->sendEventToClient(Lcom/squareup/wire/Message;)V

    .line 230
    return-void
.end method

.method private sendEventToClient(Lcom/squareup/wire/Message;)V
    .locals 2
    .param p1, "message"    # Lcom/squareup/wire/Message;

    .prologue
    .line 233
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mBus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/event/RemoteEvent;

    invoke-direct {v1, p1}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 234
    return-void
.end method


# virtual methods
.method public createProfileForId(Lcom/navdy/service/library/device/NavdyDeviceId;)Lcom/navdy/hud/app/profile/DriverProfile;
    .locals 6
    .param p1, "deviceId"    # Lcom/navdy/service/library/device/NavdyDeviceId;

    .prologue
    .line 178
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/profile/DriverProfileManager;->profileNameForId(Lcom/navdy/service/library/device/NavdyDeviceId;)Ljava/lang/String;

    move-result-object v2

    .line 180
    .local v2, "profileName":Ljava/lang/String;
    :try_start_0
    iget-object v3, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mProfileDirectoryName:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/navdy/hud/app/profile/DriverProfile;->createProfileForId(Ljava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/profile/DriverProfile;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 185
    .local v1, "newProfile":Lcom/navdy/hud/app/profile/DriverProfile;
    iget-object v3, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mAllProfiles:Ljava/util/HashMap;

    invoke-virtual {v3, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    .end local v1    # "newProfile":Lcom/navdy/hud/app/profile/DriverProfile;
    :goto_0
    return-object v1

    .line 181
    :catch_0
    move-exception v0

    .line 182
    .local v0, "e":Ljava/io/IOException;
    sget-object v3, Lcom/navdy/hud/app/profile/DriverProfileManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "could not create new profile: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 183
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public disableTraffic()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 530
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/profile/DriverProfile;->setTrafficEnabled(Z)V

    .line 531
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->theDefaultProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/profile/DriverProfile;->setTrafficEnabled(Z)V

    .line 532
    return-void
.end method

.method public enableTraffic()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 525
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/profile/DriverProfile;->setTrafficEnabled(Z)V

    .line 526
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->theDefaultProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/profile/DriverProfile;->setTrafficEnabled(Z)V

    .line 527
    return-void
.end method

.method public getCurrentLocale()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 513
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->getLocale()Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    return-object v0
.end method

.method public getLastUserEmail()Ljava/lang/String;
    .locals 1

    .prologue
    .line 521
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->lastUserEmail:Ljava/lang/String;

    return-object v0
.end method

.method public getLastUserName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 517
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->lastUserName:Ljava/lang/String;

    return-object v0
.end method

.method public getLocalPreferences()Lcom/navdy/service/library/events/preferences/LocalPreferences;
    .locals 1

    .prologue
    .line 433
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->getLocalPreferences()Lcom/navdy/service/library/events/preferences/LocalPreferences;

    move-result-object v0

    return-object v0
.end method

.method public getLocalPreferencesForCurrentDriverProfile(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 496
    invoke-virtual {p0}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getLocalPreferencesForDriverProfile(Landroid/content/Context;Lcom/navdy/hud/app/profile/DriverProfile;)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public getLocalPreferencesForDriverProfile(Landroid/content/Context;Lcom/navdy/hud/app/profile/DriverProfile;)Landroid/content/SharedPreferences;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "profile"    # Lcom/navdy/hud/app/profile/DriverProfile;

    .prologue
    .line 491
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/navdy/hud/app/profile/DriverProfile;->getProfileName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_preferences"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 492
    .local v0, "preferencesFileName":Ljava/lang/String;
    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    return-object v1
.end method

.method public getProfileForId(Lcom/navdy/service/library/device/NavdyDeviceId;)Lcom/navdy/hud/app/profile/DriverProfile;
    .locals 2
    .param p1, "deviceId"    # Lcom/navdy/service/library/device/NavdyDeviceId;

    .prologue
    .line 168
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/profile/DriverProfileManager;->profileNameForId(Lcom/navdy/service/library/device/NavdyDeviceId;)Ljava/lang/String;

    move-result-object v0

    .line 169
    .local v0, "profileName":Ljava/lang/String;
    iget-object v1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mAllProfiles:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/profile/DriverProfile;

    return-object v1
.end method

.method public getPublicProfiles()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/navdy/hud/app/profile/DriverProfile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 173
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mPublicProfiles:Ljava/util/Set;

    return-object v0
.end method

.method public getSessionPreferences()Lcom/navdy/hud/app/profile/DriverSessionPreferences;
    .locals 1

    .prologue
    .line 500
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mSessionPrefs:Lcom/navdy/hud/app/profile/DriverSessionPreferences;

    return-object v0
.end method

.method isDefaultProfile(Lcom/navdy/hud/app/profile/DriverProfile;)Z
    .locals 1
    .param p1, "profile"    # Lcom/navdy/hud/app/profile/DriverProfile;

    .prologue
    .line 481
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->theDefaultProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isManualZoom()Z
    .locals 3

    .prologue
    .line 504
    invoke-virtual {p0}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getLocalPreferences()Lcom/navdy/service/library/events/preferences/LocalPreferences;

    move-result-object v0

    .line 505
    .local v0, "localPreferences":Lcom/navdy/service/library/events/preferences/LocalPreferences;
    if-eqz v0, :cond_0

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v2, v0, Lcom/navdy/service/library/events/preferences/LocalPreferences;->manualZoom:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 506
    const/4 v1, 0x1

    .line 508
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isTrafficEnabled()Z
    .locals 1

    .prologue
    .line 535
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->isTrafficEnabled()Z

    move-result v0

    return v0
.end method

.method public loadProfileForId(Lcom/navdy/service/library/device/NavdyDeviceId;)V
    .locals 3
    .param p1, "deviceId"    # Lcom/navdy/service/library/device/NavdyDeviceId;

    .prologue
    .line 135
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/profile/DriverProfileManager$2;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/profile/DriverProfileManager$2;-><init>(Lcom/navdy/hud/app/profile/DriverProfileManager;Lcom/navdy/service/library/device/NavdyDeviceId;)V

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 147
    return-void
.end method

.method public onCannedMessagesUpdate(Lcom/navdy/service/library/events/glances/CannedMessagesUpdate;)V
    .locals 2
    .param p1, "update"    # Lcom/navdy/service/library/events/glances/CannedMessagesUpdate;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 410
    iget-object v0, p1, Lcom/navdy/service/library/events/glances/CannedMessagesUpdate;->status:Lcom/navdy/service/library/events/RequestStatus;

    new-instance v1, Lcom/navdy/hud/app/profile/DriverProfileManager$10;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/profile/DriverProfileManager$10;-><init>(Lcom/navdy/hud/app/profile/DriverProfileManager;Lcom/navdy/service/library/events/glances/CannedMessagesUpdate;)V

    invoke-direct {p0, p1, v0, p1, v1}, Lcom/navdy/hud/app/profile/DriverProfileManager;->handleUpdateInBackground(Lcom/squareup/wire/Message;Lcom/navdy/service/library/events/RequestStatus;Lcom/squareup/wire/Message;Ljava/lang/Runnable;)V

    .line 416
    return-void
.end method

.method public onDeviceSyncRequired(Lcom/navdy/hud/app/service/ConnectionHandler$DeviceSyncEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/service/ConnectionHandler$DeviceSyncEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 243
    sget-object v0, Lcom/navdy/hud/app/profile/DriverProfileManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "calling requestPreferenceUpdates"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 244
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/profile/DriverProfileManager;->requestPreferenceUpdates(Lcom/navdy/hud/app/profile/DriverProfile;)V

    .line 245
    sget-object v0, Lcom/navdy/hud/app/profile/DriverProfileManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "calling requestUpdates"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 246
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/profile/DriverProfileManager;->requestUpdates(Lcom/navdy/hud/app/profile/DriverProfile;)V

    .line 247
    return-void
.end method

.method public onDisplaySpeakerPreferencesUpdate(Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate;)V
    .locals 3
    .param p1, "update"    # Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 384
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate;->status:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v1, p1, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate;->preferences:Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;

    new-instance v2, Lcom/navdy/hud/app/profile/DriverProfileManager$8;

    invoke-direct {v2, p0, p1}, Lcom/navdy/hud/app/profile/DriverProfileManager$8;-><init>(Lcom/navdy/hud/app/profile/DriverProfileManager;Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate;)V

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/navdy/hud/app/profile/DriverProfileManager;->handleUpdateInBackground(Lcom/squareup/wire/Message;Lcom/navdy/service/library/events/RequestStatus;Lcom/squareup/wire/Message;Ljava/lang/Runnable;)V

    .line 393
    return-void
.end method

.method public onDriverProfileChange(Lcom/navdy/hud/app/event/DriverProfileChanged;)V
    .locals 2
    .param p1, "changed"    # Lcom/navdy/hud/app/event/DriverProfileChanged;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 238
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mBus:Lcom/squareup/otto/Bus;

    iget-object v1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-virtual {v1}, Lcom/navdy/hud/app/profile/DriverProfile;->getNavigationPreferences()Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 239
    return-void
.end method

.method public onDriverProfilePreferencesUpdate(Lcom/navdy/service/library/events/preferences/DriverProfilePreferencesUpdate;)V
    .locals 4
    .param p1, "update"    # Lcom/navdy/service/library/events/preferences/DriverProfilePreferencesUpdate;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/16 v3, 0xa

    .line 251
    iget-object v1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    iget-object v2, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->theDefaultProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    if-ne v1, v2, :cond_1

    .line 307
    :cond_0
    :goto_0
    return-void

    .line 254
    :cond_1
    sget-object v1, Lcom/navdy/hud/app/profile/DriverProfileManager$13;->$SwitchMap$com$navdy$service$library$events$RequestStatus:[I

    iget-object v2, p1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferencesUpdate;->status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/RequestStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 304
    sget-object v1, Lcom/navdy/hud/app/profile/DriverProfileManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "profile status was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferencesUpdate;->status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", which I didn\'t expect; ignoring."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 256
    :pswitch_0
    sget-object v1, Lcom/navdy/hud/app/profile/DriverProfileManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "prefs are up to date!"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 257
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/profile/DriverProfileManager$3;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/profile/DriverProfileManager$3;-><init>(Lcom/navdy/hud/app/profile/DriverProfileManager;)V

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0

    .line 266
    :pswitch_1
    sget-object v1, Lcom/navdy/hud/app/profile/DriverProfileManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Received profile update, applying"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 267
    iget-object v1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    if-eqz v1, :cond_0

    .line 268
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferencesUpdate;->preferences:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    .line 269
    .local v0, "preferences":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;
    if-eqz v0, :cond_3

    .line 270
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/profile/DriverProfileManager$4;

    invoke-direct {v2, p0, v0}, Lcom/navdy/hud/app/profile/DriverProfileManager$4;-><init>(Lcom/navdy/hud/app/profile/DriverProfileManager;Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;)V

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 277
    iget-object v1, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->profile_is_public:Ljava/lang/Boolean;

    sget-object v2, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->DEFAULT_PROFILE_IS_PUBLIC:Ljava/lang/Boolean;

    invoke-static {v1, v2}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 278
    iget-object v1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mPublicProfiles:Ljava/util/Set;

    iget-object v2, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 283
    :goto_1
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/profile/DriverProfileManager$5;

    invoke-direct {v2, p0, v0}, Lcom/navdy/hud/app/profile/DriverProfileManager$5;-><init>(Lcom/navdy/hud/app/profile/DriverProfileManager;Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;)V

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto/16 :goto_0

    .line 280
    :cond_2
    iget-object v1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mPublicProfiles:Ljava/util/Set;

    iget-object v2, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 299
    :cond_3
    sget-object v1, Lcom/navdy/hud/app/profile/DriverProfileManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "preferences update with no preferences!"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 254
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onInputPreferencesUpdate(Lcom/navdy/service/library/events/preferences/InputPreferencesUpdate;)V
    .locals 3
    .param p1, "update"    # Lcom/navdy/service/library/events/preferences/InputPreferencesUpdate;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 371
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/InputPreferencesUpdate;->status:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v1, p1, Lcom/navdy/service/library/events/preferences/InputPreferencesUpdate;->preferences:Lcom/navdy/service/library/events/preferences/InputPreferences;

    new-instance v2, Lcom/navdy/hud/app/profile/DriverProfileManager$7;

    invoke-direct {v2, p0, p1}, Lcom/navdy/hud/app/profile/DriverProfileManager$7;-><init>(Lcom/navdy/hud/app/profile/DriverProfileManager;Lcom/navdy/service/library/events/preferences/InputPreferencesUpdate;)V

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/navdy/hud/app/profile/DriverProfileManager;->handleUpdateInBackground(Lcom/squareup/wire/Message;Lcom/navdy/service/library/events/RequestStatus;Lcom/squareup/wire/Message;Ljava/lang/Runnable;)V

    .line 380
    return-void
.end method

.method public onNavigationPreferencesUpdate(Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;)V
    .locals 3
    .param p1, "update"    # Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 356
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;->status:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v1, p1, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;->preferences:Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    new-instance v2, Lcom/navdy/hud/app/profile/DriverProfileManager$6;

    invoke-direct {v2, p0, p1}, Lcom/navdy/hud/app/profile/DriverProfileManager$6;-><init>(Lcom/navdy/hud/app/profile/DriverProfileManager;Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;)V

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/navdy/hud/app/profile/DriverProfileManager;->handleUpdateInBackground(Lcom/squareup/wire/Message;Lcom/navdy/service/library/events/RequestStatus;Lcom/squareup/wire/Message;Ljava/lang/Runnable;)V

    .line 367
    return-void
.end method

.method public onNotificationPreferencesUpdate(Lcom/navdy/service/library/events/preferences/NotificationPreferencesUpdate;)V
    .locals 3
    .param p1, "update"    # Lcom/navdy/service/library/events/preferences/NotificationPreferencesUpdate;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 397
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/NotificationPreferencesUpdate;->status:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v1, p1, Lcom/navdy/service/library/events/preferences/NotificationPreferencesUpdate;->preferences:Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    new-instance v2, Lcom/navdy/hud/app/profile/DriverProfileManager$9;

    invoke-direct {v2, p0, p1}, Lcom/navdy/hud/app/profile/DriverProfileManager$9;-><init>(Lcom/navdy/hud/app/profile/DriverProfileManager;Lcom/navdy/service/library/events/preferences/NotificationPreferencesUpdate;)V

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/navdy/hud/app/profile/DriverProfileManager;->handleUpdateInBackground(Lcom/squareup/wire/Message;Lcom/navdy/service/library/events/RequestStatus;Lcom/squareup/wire/Message;Ljava/lang/Runnable;)V

    .line 406
    return-void
.end method

.method public onPhotoDownload(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;)V
    .locals 2
    .param p1, "photoStatus"    # Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 464
    iget-object v0, p1, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    sget-object v1, Lcom/navdy/service/library/events/photo/PhotoType;->PHOTO_DRIVER_PROFILE:Lcom/navdy/service/library/events/photo/PhotoType;

    if-eq v0, v1, :cond_1

    .line 478
    :cond_0
    :goto_0
    return-void

    .line 467
    :cond_1
    iget-boolean v0, p1, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;->alreadyDownloaded:Z

    if-nez v0, :cond_0

    .line 470
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    iget-object v1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->theDefaultProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    if-eq v0, v1, :cond_0

    .line 473
    iget-boolean v0, p1, Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;->success:Z

    if-eqz v0, :cond_2

    .line 474
    sget-object v0, Lcom/navdy/hud/app/profile/DriverProfileManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "applying new photo"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 476
    :cond_2
    sget-object v0, Lcom/navdy/hud/app/profile/DriverProfileManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "driver photo not downloaded"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setCurrentProfile(Lcom/navdy/hud/app/profile/DriverProfile;)V
    .locals 3
    .param p1, "profile"    # Lcom/navdy/hud/app/profile/DriverProfile;

    .prologue
    .line 150
    sget-object v0, Lcom/navdy/hud/app/profile/DriverProfileManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setting current profile to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 151
    if-nez p1, :cond_0

    .line 152
    iget-object p1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->theDefaultProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    if-eq v0, p1, :cond_1

    .line 155
    iput-object p1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    .line 156
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mSessionPrefs:Lcom/navdy/hud/app/profile/DriverSessionPreferences;

    iget-object v1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    iget-object v2, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-virtual {p0, v2}, Lcom/navdy/hud/app/profile/DriverProfileManager;->isDefaultProfile(Lcom/navdy/hud/app/profile/DriverProfile;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/profile/DriverSessionPreferences;->setDefault(Lcom/navdy/hud/app/profile/DriverProfile;Z)V

    .line 157
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->theDefaultProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    iget-object v1, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-virtual {v1}, Lcom/navdy/hud/app/profile/DriverProfile;->isTrafficEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/profile/DriverProfile;->setTrafficEnabled(Z)V

    .line 158
    invoke-direct {p0}, Lcom/navdy/hud/app/profile/DriverProfileManager;->copyCurrentProfileToDefault()V

    .line 159
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mBus:Lcom/squareup/otto/Bus;

    sget-object v1, Lcom/navdy/hud/app/profile/DriverProfileManager;->DRIVER_PROFILE_CHANGED:Lcom/navdy/hud/app/event/DriverProfileChanged;

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 162
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->getDriverName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->lastUserName:Ljava/lang/String;

    .line 163
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->getDriverEmail()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->lastUserEmail:Ljava/lang/String;

    .line 164
    sget-object v0, Lcom/navdy/hud/app/profile/DriverProfileManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "lastUserEmail:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->lastUserEmail:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 165
    return-void
.end method

.method public updateLocalPreferences(Lcom/navdy/service/library/events/preferences/LocalPreferences;)V
    .locals 3
    .param p1, "preferences"    # Lcom/navdy/service/library/events/preferences/LocalPreferences;

    .prologue
    .line 419
    iget-object v0, p0, Lcom/navdy/hud/app/profile/DriverProfileManager;->mCurrentProfile:Lcom/navdy/hud/app/profile/DriverProfile;

    if-eqz v0, :cond_0

    .line 420
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/profile/DriverProfileManager$11;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/profile/DriverProfileManager$11;-><init>(Lcom/navdy/hud/app/profile/DriverProfileManager;Lcom/navdy/service/library/events/preferences/LocalPreferences;)V

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 430
    :cond_0
    return-void
.end method
