.class final Lcom/navdy/hud/app/profile/HudLocale$1;
.super Ljava/lang/Object;
.source "HudLocale.java"

# interfaces
.implements Lcom/navdy/hud/app/framework/toast/IToastCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/profile/HudLocale;->showLocaleChangeToast()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field animator:Landroid/animation/ObjectAnimator;

.field final synthetic val$resources:Landroid/content/res/Resources;


# direct methods
.method constructor <init>(Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lcom/navdy/hud/app/profile/HudLocale$1;->val$resources:Landroid/content/res/Resources;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public executeChoiceItem(II)V
    .locals 0
    .param p1, "pos"    # I
    .param p2, "id"    # I

    .prologue
    .line 226
    return-void
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    .line 223
    const/4 v0, 0x0

    return v0
.end method

.method public onStart(Lcom/navdy/hud/app/view/ToastView;)V
    .locals 9
    .param p1, "view"    # Lcom/navdy/hud/app/view/ToastView;

    .prologue
    const/16 v6, 0x8

    const/4 v8, 0x0

    .line 184
    invoke-virtual {p1}, Lcom/navdy/hud/app/view/ToastView;->getView()Lcom/navdy/hud/app/ui/component/ConfirmationLayout;

    move-result-object v1

    .line 185
    .local v1, "lyt":Lcom/navdy/hud/app/ui/component/ConfirmationLayout;
    iget-object v4, v1, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->screenImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    invoke-virtual {v4}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 186
    .local v0, "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v4, p0, Lcom/navdy/hud/app/profile/HudLocale$1;->val$resources:Landroid/content/res/Resources;

    const v5, 0x7f0b00bb

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 187
    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 188
    iget-object v4, v1, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title1:Landroid/widget/TextView;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 189
    iget-object v4, v1, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title3:Landroid/widget/TextView;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 190
    iget-object v4, v1, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->title4:Landroid/widget/TextView;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 191
    iget-object v4, v1, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->screenImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    invoke-virtual {v4}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout$LayoutParams;

    .line 192
    .local v2, "params":Landroid/widget/FrameLayout$LayoutParams;
    const/16 v4, 0x11

    iput v4, v2, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 193
    const v4, 0x7f0e00bf

    invoke-virtual {v1, v4}, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 194
    .local v3, "rightContainer":Landroid/view/View;
    invoke-virtual {v3, v8, v8, v8, v8}, Landroid/view/View;->setPadding(IIII)V

    .line 195
    iget-object v4, p0, Lcom/navdy/hud/app/profile/HudLocale$1;->animator:Landroid/animation/ObjectAnimator;

    if-nez v4, :cond_0

    .line 196
    iget-object v4, v1, Lcom/navdy/hud/app/ui/component/ConfirmationLayout;->screenImage:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    sget-object v5, Landroid/view/View;->ROTATION:Landroid/util/Property;

    const/4 v6, 0x1

    new-array v6, v6, [F

    const/high16 v7, 0x43b40000    # 360.0f

    aput v7, v6, v8

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/hud/app/profile/HudLocale$1;->animator:Landroid/animation/ObjectAnimator;

    .line 197
    iget-object v4, p0, Lcom/navdy/hud/app/profile/HudLocale$1;->animator:Landroid/animation/ObjectAnimator;

    const-wide/16 v6, 0x1f4

    invoke-virtual {v4, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 198
    iget-object v4, p0, Lcom/navdy/hud/app/profile/HudLocale$1;->animator:Landroid/animation/ObjectAnimator;

    new-instance v5, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v4, v5}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 199
    iget-object v4, p0, Lcom/navdy/hud/app/profile/HudLocale$1;->animator:Landroid/animation/ObjectAnimator;

    new-instance v5, Lcom/navdy/hud/app/profile/HudLocale$1$1;

    invoke-direct {v5, p0}, Lcom/navdy/hud/app/profile/HudLocale$1$1;-><init>(Lcom/navdy/hud/app/profile/HudLocale$1;)V

    invoke-virtual {v4, v5}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 209
    :cond_0
    iget-object v4, p0, Lcom/navdy/hud/app/profile/HudLocale$1;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v4}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v4

    if-nez v4, :cond_1

    .line 210
    iget-object v4, p0, Lcom/navdy/hud/app/profile/HudLocale$1;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v4}, Landroid/animation/ObjectAnimator;->start()V

    .line 212
    :cond_1
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/navdy/hud/app/profile/HudLocale$1;->animator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/navdy/hud/app/profile/HudLocale$1;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->removeAllListeners()V

    .line 218
    iget-object v0, p0, Lcom/navdy/hud/app/profile/HudLocale$1;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 220
    :cond_0
    return-void
.end method
