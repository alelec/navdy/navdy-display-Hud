.class public Lcom/navdy/hud/app/audio/SoundUtils;
.super Ljava/lang/Object;
.source "SoundUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/audio/SoundUtils$Sound;
    }
.end annotation


# static fields
.field private static initialized:Z

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static soundIdMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/navdy/hud/app/audio/SoundUtils$Sound;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static soundPool:Landroid/media/SoundPool;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/audio/SoundUtils;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/audio/SoundUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 35
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/audio/SoundUtils;->soundIdMap:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static init()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 38
    sget-boolean v3, Lcom/navdy/hud/app/audio/SoundUtils;->initialized:Z

    if-nez v3, :cond_0

    .line 39
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 40
    sput-boolean v6, Lcom/navdy/hud/app/audio/SoundUtils;->initialized:Z

    .line 41
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    .line 43
    .local v1, "context":Landroid/content/Context;
    new-instance v3, Landroid/media/AudioAttributes$Builder;

    invoke-direct {v3}, Landroid/media/AudioAttributes$Builder;-><init>()V

    const/16 v4, 0xd

    .line 44
    invoke-virtual {v3, v4}, Landroid/media/AudioAttributes$Builder;->setUsage(I)Landroid/media/AudioAttributes$Builder;

    move-result-object v3

    const/4 v4, 0x4

    .line 45
    invoke-virtual {v3, v4}, Landroid/media/AudioAttributes$Builder;->setContentType(I)Landroid/media/AudioAttributes$Builder;

    move-result-object v3

    .line 46
    invoke-virtual {v3}, Landroid/media/AudioAttributes$Builder;->build()Landroid/media/AudioAttributes;

    move-result-object v0

    .line 48
    .local v0, "attributes":Landroid/media/AudioAttributes;
    new-instance v3, Landroid/media/SoundPool$Builder;

    invoke-direct {v3}, Landroid/media/SoundPool$Builder;-><init>()V

    invoke-virtual {v3, v0}, Landroid/media/SoundPool$Builder;->setAudioAttributes(Landroid/media/AudioAttributes;)Landroid/media/SoundPool$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/media/SoundPool$Builder;->build()Landroid/media/SoundPool;

    move-result-object v3

    sput-object v3, Lcom/navdy/hud/app/audio/SoundUtils;->soundPool:Landroid/media/SoundPool;

    .line 50
    sget-object v3, Lcom/navdy/hud/app/audio/SoundUtils;->soundPool:Landroid/media/SoundPool;

    const v4, 0x7f07000a

    invoke-virtual {v3, v1, v4, v6}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v2

    .line 51
    .local v2, "id":I
    sget-object v3, Lcom/navdy/hud/app/audio/SoundUtils;->soundIdMap:Ljava/util/HashMap;

    sget-object v4, Lcom/navdy/hud/app/audio/SoundUtils$Sound;->MENU_MOVE:Lcom/navdy/hud/app/audio/SoundUtils$Sound;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    sget-object v3, Lcom/navdy/hud/app/audio/SoundUtils;->soundPool:Landroid/media/SoundPool;

    const v4, 0x7f07000b

    invoke-virtual {v3, v1, v4, v6}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v2

    .line 54
    sget-object v3, Lcom/navdy/hud/app/audio/SoundUtils;->soundIdMap:Ljava/util/HashMap;

    sget-object v4, Lcom/navdy/hud/app/audio/SoundUtils$Sound;->MENU_SELECT:Lcom/navdy/hud/app/audio/SoundUtils$Sound;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    sget-object v3, Lcom/navdy/hud/app/audio/SoundUtils;->soundPool:Landroid/media/SoundPool;

    const v4, 0x7f07000d

    invoke-virtual {v3, v1, v4, v6}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v2

    .line 57
    sget-object v3, Lcom/navdy/hud/app/audio/SoundUtils;->soundIdMap:Ljava/util/HashMap;

    sget-object v4, Lcom/navdy/hud/app/audio/SoundUtils$Sound;->STARTUP:Lcom/navdy/hud/app/audio/SoundUtils$Sound;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    sget-object v3, Lcom/navdy/hud/app/audio/SoundUtils;->soundPool:Landroid/media/SoundPool;

    const v4, 0x7f07000c

    invoke-virtual {v3, v1, v4, v6}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v2

    .line 60
    sget-object v3, Lcom/navdy/hud/app/audio/SoundUtils;->soundIdMap:Ljava/util/HashMap;

    sget-object v4, Lcom/navdy/hud/app/audio/SoundUtils$Sound;->SHUTDOWN:Lcom/navdy/hud/app/audio/SoundUtils$Sound;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    sget-object v3, Lcom/navdy/hud/app/audio/SoundUtils;->soundPool:Landroid/media/SoundPool;

    const v4, 0x7f070009

    invoke-virtual {v3, v1, v4, v6}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v2

    .line 63
    sget-object v3, Lcom/navdy/hud/app/audio/SoundUtils;->soundIdMap:Ljava/util/HashMap;

    sget-object v4, Lcom/navdy/hud/app/audio/SoundUtils$Sound;->ALERT_POSITIVE:Lcom/navdy/hud/app/audio/SoundUtils$Sound;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    sget-object v3, Lcom/navdy/hud/app/audio/SoundUtils;->soundPool:Landroid/media/SoundPool;

    const v4, 0x7f070008

    invoke-virtual {v3, v1, v4, v6}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v2

    .line 66
    sget-object v3, Lcom/navdy/hud/app/audio/SoundUtils;->soundIdMap:Ljava/util/HashMap;

    sget-object v4, Lcom/navdy/hud/app/audio/SoundUtils$Sound;->ALERT_NEGATIVE:Lcom/navdy/hud/app/audio/SoundUtils$Sound;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    sget-object v3, Lcom/navdy/hud/app/audio/SoundUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "sound pool created"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 69
    :cond_0
    return-void
.end method

.method public static playSound(Lcom/navdy/hud/app/audio/SoundUtils$Sound;)V
    .locals 9
    .param p0, "sound"    # Lcom/navdy/hud/app/audio/SoundUtils$Sound;

    .prologue
    .line 73
    :try_start_0
    sget-boolean v0, Lcom/navdy/hud/app/audio/SoundUtils;->initialized:Z

    if-nez v0, :cond_0

    .line 85
    :goto_0
    return-void

    .line 76
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/audio/SoundUtils;->soundIdMap:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    .line 77
    .local v7, "soundId":Ljava/lang/Integer;
    if-nez v7, :cond_1

    .line 78
    sget-object v0, Lcom/navdy/hud/app/audio/SoundUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "soundId not found:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 82
    .end local v7    # "soundId":Ljava/lang/Integer;
    :catch_0
    move-exception v8

    .line 83
    .local v8, "t":Ljava/lang/Throwable;
    sget-object v0, Lcom/navdy/hud/app/audio/SoundUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v0, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 81
    .end local v8    # "t":Ljava/lang/Throwable;
    .restart local v7    # "soundId":Ljava/lang/Integer;
    :cond_1
    :try_start_1
    sget-object v0, Lcom/navdy/hud/app/audio/SoundUtils;->soundPool:Landroid/media/SoundPool;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
