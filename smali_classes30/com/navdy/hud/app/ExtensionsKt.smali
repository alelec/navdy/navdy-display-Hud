.class public final Lcom/navdy/hud/app/ExtensionsKt;
.super Ljava/lang/Object;
.source "Extensions.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0006\n\u0002\u0008\u0003\n\u0002\u0010\u0007\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u001a\n\u0010\u0003\u001a\u00020\u0004*\u00020\u0004\u001a\u001a\u0010\u0005\u001a\u00020\u0004*\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u0004\u001a\u001a\u0010\u0005\u001a\u00020\u0008*\u00020\u00082\u0006\u0010\u0006\u001a\u00020\u00082\u0006\u0010\u0007\u001a\u00020\u0008\u00a8\u0006\t"
    }
    d2 = {
        "cacheKey",
        "",
        "Lcom/navdy/service/library/events/audio/MusicCollectionRequest;",
        "celsiusToFahrenheit",
        "",
        "clamp",
        "min",
        "max",
        "",
        "app_hudRelease"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# direct methods
.method public static final cacheKey(Lcom/navdy/service/library/events/audio/MusicCollectionRequest;)Ljava/lang/String;
    .locals 3
    .param p0, "$receiver"    # Lcom/navdy/service/library/events/audio/MusicCollectionRequest;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    const-string v1, "$receiver"

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    check-cast p0, Lcom/squareup/wire/Message;

    .end local p0    # "$receiver":Lcom/navdy/service/library/events/audio/MusicCollectionRequest;
    invoke-static {p0}, Lcom/navdy/service/library/events/MessageStore;->removeNulls(Lcom/squareup/wire/Message;)Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    .line 36
    .local v0, "defaults":Lcom/navdy/service/library/events/audio/MusicCollectionRequest;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "#"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "#"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "#"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->offset:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static final celsiusToFahrenheit(D)D
    .locals 4
    .param p0, "$receiver"    # D

    .prologue
    .line 31
    const/16 v0, 0x9

    int-to-double v0, v0

    mul-double/2addr v0, p0

    const/4 v2, 0x5

    int-to-double v2, v2

    div-double/2addr v0, v2

    const/16 v2, 0x20

    int-to-double v2, v2

    add-double/2addr v0, v2

    return-wide v0
.end method

.method public static final clamp(DDD)D
    .locals 2
    .param p0, "$receiver"    # D
    .param p2, "min"    # D
    .param p4, "max"    # D

    .prologue
    .line 22
    cmpg-double v0, p0, p2

    if-gez v0, :cond_0

    .line 26
    .end local p2    # "min":D
    :goto_0
    return-wide p2

    .line 24
    .restart local p2    # "min":D
    :cond_0
    cmpl-double v0, p0, p4

    if-lez v0, :cond_1

    move-wide p2, p4

    .line 25
    goto :goto_0

    :cond_1
    move-wide p2, p0

    .line 26
    goto :goto_0
.end method

.method public static final clamp(FFF)F
    .locals 1
    .param p0, "$receiver"    # F
    .param p1, "min"    # F
    .param p2, "max"    # F

    .prologue
    .line 13
    cmpg-float v0, p0, p1

    if-gez v0, :cond_0

    .line 17
    .end local p1    # "min":F
    :goto_0
    return p1

    .line 15
    .restart local p1    # "min":F
    :cond_0
    cmpl-float v0, p0, p2

    if-lez v0, :cond_1

    move p1, p2

    .line 16
    goto :goto_0

    :cond_1
    move p1, p0

    .line 17
    goto :goto_0
.end method
