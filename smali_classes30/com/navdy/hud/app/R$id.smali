.class public final Lcom/navdy/hud/app/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final activeEtaContainer:I = 0x7f0e0188

.field public static final activeMapDistance:I = 0x7f0e01b8

.field public static final activeMapIcon:I = 0x7f0e01ba

.field public static final activeMapInstruction:I = 0x7f0e01be

.field public static final activeMapRoadInfoContainer:I = 0x7f0e01b7

.field public static final active_trip_contact:I = 0x7f0e0000

.field public static final active_trip_distance:I = 0x7f0e00b8

.field public static final active_trip_duration:I = 0x7f0e00b6

.field public static final active_trip_eta:I = 0x7f0e00b9

.field public static final active_trip_maneuver:I = 0x7f0e0001

.field public static final active_trip_maneuver_title:I = 0x7f0e0002

.field public static final active_trip_progress:I = 0x7f0e00b7

.field public static final active_trip_subtitle:I = 0x7f0e00b4

.field public static final active_trip_title:I = 0x7f0e00b3

.field public static final albumArt:I = 0x7f0e013f

.field public static final all:I = 0x7f0e009c

.field public static final animation_mode_navdy_road_view:I = 0x7f0e0003

.field public static final animation_mode_position:I = 0x7f0e0004

.field public static final animation_mode_position_animation:I = 0x7f0e0005

.field public static final animation_road_view:I = 0x7f0e0006

.field public static final animation_road_view_no_zoom:I = 0x7f0e0007

.field public static final animatorView:I = 0x7f0e01d8

.field public static final app_store_img_1:I = 0x7f0e01d4

.field public static final app_store_img_2:I = 0x7f0e01d5

.field public static final apple_podcasts:I = 0x7f0e0008

.field public static final arrive_icon:I = 0x7f0e00e9

.field public static final audioFeedback:I = 0x7f0e0113

.field public static final audio_source_icon:I = 0x7f0e0248

.field public static final audio_source_information_holder:I = 0x7f0e0247

.field public static final audio_source_text:I = 0x7f0e0249

.field public static final auto_brightness_root_container:I = 0x7f0e0172

.field public static final badge:I = 0x7f0e0151

.field public static final badge_container:I = 0x7f0e0150

.field public static final badge_icon:I = 0x7f0e0152

.field public static final basic:I = 0x7f0e009d

.field public static final big:I = 0x7f0e00d6

.field public static final border:I = 0x7f0e014a

.field public static final bottomScrub:I = 0x7f0e010c

.field public static final button_add_response:I = 0x7f0e0122

.field public static final button_attachment:I = 0x7f0e011d

.field public static final button_login:I = 0x7f0e0127

.field public static final button_refresh:I = 0x7f0e0123

.field public static final button_send:I = 0x7f0e011e

.field public static final button_update:I = 0x7f0e012b

.field public static final callConfirmContainer:I = 0x7f0e00cb

.field public static final callStatusImage:I = 0x7f0e015d

.field public static final callerImage:I = 0x7f0e015c

.field public static final callerName:I = 0x7f0e015a

.field public static final callerStatus:I = 0x7f0e015b

.field public static final capture_instructions_lyt:I = 0x7f0e01cd

.field public static final carousel:I = 0x7f0e01db

.field public static final center_image:I = 0x7f0e00fc

.field public static final chains:I = 0x7f0e009e

.field public static final choiceContainer:I = 0x7f0e00c9

.field public static final choiceHighlight:I = 0x7f0e00ca

.field public static final choiceLayout:I = 0x7f0e00d2

.field public static final choice_layout:I = 0x7f0e0153

.field public static final circle:I = 0x7f0e00a8

.field public static final circle_progress:I = 0x7f0e0147

.field public static final clamp:I = 0x7f0e00aa

.field public static final closeContainer:I = 0x7f0e023b

.field public static final closeContainerScrim:I = 0x7f0e023a

.field public static final closeHalo:I = 0x7f0e023d

.field public static final closeIconContainer:I = 0x7f0e023c

.field public static final confirmationLayout:I = 0x7f0e0174

.field public static final connected:I = 0x7f0e00a0

.field public static final connectedView:I = 0x7f0e0179

.field public static final connecting:I = 0x7f0e00a1

.field public static final connection_failed:I = 0x7f0e00a2

.field public static final contacts_menu_recent:I = 0x7f0e0009

.field public static final container:I = 0x7f0e0132

.field public static final counter:I = 0x7f0e0140

.field public static final crossFadeImageView:I = 0x7f0e00d5

.field public static final customNotificationContainer:I = 0x7f0e0149

.field public static final custom_drawable:I = 0x7f0e00db

.field public static final debug_text:I = 0x7f0e00bc

.field public static final description:I = 0x7f0e00c4

.field public static final destination_label:I = 0x7f0e00c0

.field public static final destination_subtitle:I = 0x7f0e00c2

.field public static final destination_title:I = 0x7f0e00c1

.field public static final destinations_exit:I = 0x7f0e000a

.field public static final dial:I = 0x7f0e0211

.field public static final dial_manager:I = 0x7f0e0175

.field public static final dial_ota:I = 0x7f0e00ad

.field public static final dismiss:I = 0x7f0e000b

.field public static final distance:I = 0x7f0e021a

.field public static final down:I = 0x7f0e00a6

.field public static final download_app:I = 0x7f0e00b0

.field public static final download_imgs:I = 0x7f0e01d3

.field public static final download_text_1:I = 0x7f0e01d1

.field public static final download_text_2:I = 0x7f0e01d2

.field public static final download_text_container:I = 0x7f0e01d0

.field public static final drive_score_events:I = 0x7f0e0185

.field public static final earlyManeuverContainer:I = 0x7f0e01c1

.field public static final earlyManeuverImage:I = 0x7f0e01c2

.field public static final enable_discrete_mode:I = 0x7f0e00da

.field public static final enable_gesture:I = 0x7f0e00d8

.field public static final enable_preview:I = 0x7f0e00d9

.field public static final engine_temp_icon:I = 0x7f0e00e2

.field public static final etaStaticTextView:I = 0x7f0e0189

.field public static final etaTextView:I = 0x7f0e018a

.field public static final etaTimeAmPm:I = 0x7f0e018b

.field public static final etaTimeLeft:I = 0x7f0e018c

.field public static final eta_layout:I = 0x7f0e01b0

.field public static final eta_time:I = 0x7f0e01b1

.field public static final eta_time_amPm:I = 0x7f0e01b2

.field public static final eta_view:I = 0x7f0e00e3

.field public static final exit:I = 0x7f0e000c

.field public static final exitContainer:I = 0x7f0e010e

.field public static final expandedNotifCoverView:I = 0x7f0e0136

.field public static final expandedNotifView:I = 0x7f0e0137

.field public static final factory_reset_confirmation:I = 0x7f0e017e

.field public static final fastScrollText:I = 0x7f0e0238

.field public static final fastScrollView:I = 0x7f0e0237

.field public static final fav_contact_exit:I = 0x7f0e000d

.field public static final firstContainer:I = 0x7f0e017a

.field public static final firstLaunch:I = 0x7f0e017f

.field public static final firstLaunchLogo:I = 0x7f0e0180

.field public static final fluctuator:I = 0x7f0e00ce

.field public static final fuel_tank_side_indicator_left:I = 0x7f0e00f3

.field public static final fuel_tank_side_indicator_right:I = 0x7f0e00f5

.field public static final fuel_type_icon:I = 0x7f0e00f4

.field public static final gauge_time_container:I = 0x7f0e0166

.field public static final gesture_learning_view:I = 0x7f0e01ca

.field public static final gesture_progress_indicator:I = 0x7f0e00f7

.field public static final glanceContainer:I = 0x7f0e0110

.field public static final glance_can_delete:I = 0x7f0e000e

.field public static final glance_view_type:I = 0x7f0e000f

.field public static final go:I = 0x7f0e0010

.field public static final gps:I = 0x7f0e0210

.field public static final gps_signal:I = 0x7f0e01f3

.field public static final groupContainer:I = 0x7f0e021c

.field public static final halo:I = 0x7f0e00c7

.field public static final home_screen_speed_limit_sign:I = 0x7f0e0184

.field public static final icon:I = 0x7f0e014d

.field public static final iconColorView:I = 0x7f0e015e

.field public static final iconContainer:I = 0x7f0e00c8

.field public static final iconInitialsView:I = 0x7f0e0164

.field public static final iconSide:I = 0x7f0e0163

.field public static final iconSpinner:I = 0x7f0e0162

.field public static final iconSpinnerContainer:I = 0x7f0e0161

.field public static final image:I = 0x7f0e00cf

.field public static final imageContainer:I = 0x7f0e00c6

.field public static final imageIconBkColor:I = 0x7f0e00d0

.field public static final imageView:I = 0x7f0e010f

.field public static final image_voice_search_active:I = 0x7f0e016f

.field public static final image_voice_search_background:I = 0x7f0e016a

.field public static final image_voice_search_inactive:I = 0x7f0e016e

.field public static final img:I = 0x7f0e0227

.field public static final imgContainer:I = 0x7f0e0225

.field public static final img_album_art:I = 0x7f0e0141

.field public static final img_left_hand:I = 0x7f0e00ff

.field public static final img_left_hand_overlay:I = 0x7f0e0100

.field public static final img_loop:I = 0x7f0e0226

.field public static final img_processing_badge:I = 0x7f0e0170

.field public static final img_right_hand:I = 0x7f0e0102

.field public static final img_right_hand_overlay:I = 0x7f0e0103

.field public static final img_status_badge:I = 0x7f0e0171

.field public static final img_warning:I = 0x7f0e00df

.field public static final inactivity:I = 0x7f0e00ae

.field public static final incidentInfo:I = 0x7f0e013e

.field public static final indicator:I = 0x7f0e0239

.field public static final info:I = 0x7f0e017c

.field public static final infoContainer:I = 0x7f0e00bf

.field public static final infoRoute:I = 0x7f0e01a6

.field public static final infoStreet:I = 0x7f0e01a5

.field public static final input_email:I = 0x7f0e0119

.field public static final input_message:I = 0x7f0e011b

.field public static final input_name:I = 0x7f0e0118

.field public static final input_password:I = 0x7f0e0126

.field public static final input_subject:I = 0x7f0e011a

.field public static final itemViewContainer:I = 0x7f0e01ce

.field public static final item_id:I = 0x7f0e0011

.field public static final item_touch_helper_previous_elevation:I = 0x7f0e0012

.field public static final label:I = 0x7f0e00c5

.field public static final label_author:I = 0x7f0e012d

.field public static final label_date:I = 0x7f0e012e

.field public static final label_last_updated:I = 0x7f0e0120

.field public static final label_message:I = 0x7f0e0115

.field public static final label_text:I = 0x7f0e012f

.field public static final label_title:I = 0x7f0e0129

.field public static final label_version:I = 0x7f0e012a

.field public static final laneGuidance:I = 0x7f0e018d

.field public static final lane_guidance_map_icon_indicator:I = 0x7f0e0187

.field public static final leftContainer:I = 0x7f0e022e

.field public static final leftDot:I = 0x7f0e01d9

.field public static final leftSwipe:I = 0x7f0e00d3

.field public static final left_gauge:I = 0x7f0e01ac

.field public static final line:I = 0x7f0e00a9

.field public static final list:I = 0x7f0e0013

.field public static final list_attachments:I = 0x7f0e0130

.field public static final list_feedback_messages:I = 0x7f0e0124

.field public static final loading:I = 0x7f0e022b

.field public static final locale_full_text:I = 0x7f0e024c

.field public static final locale_information_holder:I = 0x7f0e024a

.field public static final locale_tag_text:I = 0x7f0e024b

.field public static final locationContainer:I = 0x7f0e0107

.field public static final logo:I = 0x7f0e00b1

.field public static final low_fuel_indicator_left:I = 0x7f0e00f2

.field public static final low_fuel_indicator_right:I = 0x7f0e00f6

.field public static final lyt_left_swipe:I = 0x7f0e00fd

.field public static final lyt_right_swipe:I = 0x7f0e0101

.field public static final lyt_time:I = 0x7f0e00dc

.field public static final main:I = 0x7f0e01dc

.field public static final mainImage:I = 0x7f0e0112

.field public static final mainImageContainer:I = 0x7f0e0114

.field public static final mainLowerView:I = 0x7f0e0139

.field public static final mainSection:I = 0x7f0e00cd

.field public static final mainTitle:I = 0x7f0e00cc

.field public static final mainView:I = 0x7f0e00bb

.field public static final main_contacts:I = 0x7f0e0014

.field public static final main_menu_active_trip:I = 0x7f0e0015

.field public static final main_menu_activity_tray:I = 0x7f0e0016

.field public static final main_menu_brightness:I = 0x7f0e0017

.field public static final main_menu_call:I = 0x7f0e0018

.field public static final main_menu_connect:I = 0x7f0e0019

.field public static final main_menu_exit:I = 0x7f0e001a

.field public static final main_menu_fav_contacts:I = 0x7f0e001b

.field public static final main_menu_fav_places:I = 0x7f0e001c

.field public static final main_menu_gesture_collector:I = 0x7f0e001d

.field public static final main_menu_glances:I = 0x7f0e001e

.field public static final main_menu_map_update_mode:I = 0x7f0e001f

.field public static final main_menu_maps:I = 0x7f0e0020

.field public static final main_menu_maps_options:I = 0x7f0e0021

.field public static final main_menu_music:I = 0x7f0e0022

.field public static final main_menu_music_browse:I = 0x7f0e0023

.field public static final main_menu_no_fav_contacts:I = 0x7f0e0024

.field public static final main_menu_no_fav_places:I = 0x7f0e0025

.field public static final main_menu_no_recent_contacts:I = 0x7f0e0026

.field public static final main_menu_no_suggested_places:I = 0x7f0e0027

.field public static final main_menu_now_playing:I = 0x7f0e0028

.field public static final main_menu_options_auto_zoom:I = 0x7f0e0029

.field public static final main_menu_options_end_trip:I = 0x7f0e002a

.field public static final main_menu_options_manual_zoom:I = 0x7f0e002b

.field public static final main_menu_options_mute_tbt:I = 0x7f0e002c

.field public static final main_menu_options_pause_demo:I = 0x7f0e002d

.field public static final main_menu_options_play_demo:I = 0x7f0e002e

.field public static final main_menu_options_raw_gps:I = 0x7f0e002f

.field public static final main_menu_options_report_issue:I = 0x7f0e0030

.field public static final main_menu_options_restart_demo:I = 0x7f0e0031

.field public static final main_menu_options_scroll_left:I = 0x7f0e0032

.field public static final main_menu_options_scroll_right:I = 0x7f0e0033

.field public static final main_menu_options_select_center_gauge:I = 0x7f0e0034

.field public static final main_menu_options_side_gauges:I = 0x7f0e0035

.field public static final main_menu_options_speedometer:I = 0x7f0e0036

.field public static final main_menu_options_tachometer:I = 0x7f0e0037

.field public static final main_menu_options_unmute_tbt:I = 0x7f0e0038

.field public static final main_menu_places:I = 0x7f0e0039

.field public static final main_menu_recent_contacts:I = 0x7f0e003a

.field public static final main_menu_search:I = 0x7f0e003b

.field public static final main_menu_settings:I = 0x7f0e003c

.field public static final main_menu_settings_connect_phone:I = 0x7f0e003d

.field public static final main_menu_shutdown:I = 0x7f0e003e

.field public static final main_menu_smart_dash:I = 0x7f0e003f

.field public static final main_menu_smart_dash_options:I = 0x7f0e0040

.field public static final main_menu_software_update:I = 0x7f0e0041

.field public static final main_menu_suggested_places:I = 0x7f0e0042

.field public static final main_menu_system_info:I = 0x7f0e0043

.field public static final main_menu_test:I = 0x7f0e0044

.field public static final main_menu_voice:I = 0x7f0e0045

.field public static final main_menu_voice_search:I = 0x7f0e0046

.field public static final mainscreenRightSection:I = 0x7f0e0182

.field public static final mapIconIndicator:I = 0x7f0e0191

.field public static final mapView:I = 0x7f0e018f

.field public static final map_container:I = 0x7f0e018e

.field public static final map_mask:I = 0x7f0e00b5

.field public static final map_view_speed_container:I = 0x7f0e0183

.field public static final menu_back:I = 0x7f0e0047

.field public static final menu_call:I = 0x7f0e0048

.field public static final menu_call_me_if_you_need:I = 0x7f0e0049

.field public static final menu_message:I = 0x7f0e004a

.field public static final menu_on_my_way:I = 0x7f0e004b

.field public static final menu_running_late:I = 0x7f0e004c

.field public static final menu_send_without_message:I = 0x7f0e004d

.field public static final menu_share_location:I = 0x7f0e004e

.field public static final menu_share_trip:I = 0x7f0e004f

.field public static final message:I = 0x7f0e010a

.field public static final message_lyt:I = 0x7f0e01dd

.field public static final message_prev_choice:I = 0x7f0e0050

.field public static final message_secondary_screen:I = 0x7f0e0051

.field public static final middle_gauge:I = 0x7f0e01ae

.field public static final mileage:I = 0x7f0e0198

.field public static final minus:I = 0x7f0e0145

.field public static final mirror:I = 0x7f0e00ab

.field public static final msgPresentation:I = 0x7f0e0154

.field public static final music_browser_albums:I = 0x7f0e0052

.field public static final music_browser_artists:I = 0x7f0e0053

.field public static final music_browser_exit:I = 0x7f0e0054

.field public static final music_browser_playlists:I = 0x7f0e0055

.field public static final music_browser_shuffle:I = 0x7f0e0056

.field public static final music_collection_info:I = 0x7f0e0057

.field public static final music_controls:I = 0x7f0e0058

.field public static final music_forward:I = 0x7f0e0059

.field public static final music_pause:I = 0x7f0e005a

.field public static final music_play:I = 0x7f0e005b

.field public static final music_progress:I = 0x7f0e0159

.field public static final music_rewind:I = 0x7f0e005c

.field public static final music_thumbsdown:I = 0x7f0e005d

.field public static final music_thumbsup:I = 0x7f0e005e

.field public static final music_track_info:I = 0x7f0e005f

.field public static final navigation_views_container:I = 0x7f0e0186

.field public static final nextManeuverIcon:I = 0x7f0e021e

.field public static final nextManeuverText:I = 0x7f0e021f

.field public static final nextMapIcon:I = 0x7f0e01bd

.field public static final nextMapIconContainer:I = 0x7f0e01bc

.field public static final nextMapInstruction:I = 0x7f0e01c0

.field public static final nextMapInstructionContainer:I = 0x7f0e01bf

.field public static final nextNotificationColorView:I = 0x7f0e0148

.field public static final noLocationContainer:I = 0x7f0e0192

.field public static final noLocationImage:I = 0x7f0e0193

.field public static final noLocationTextContainer:I = 0x7f0e0194

.field public static final none:I = 0x7f0e009f

.field public static final notifIndicator:I = 0x7f0e013b

.field public static final notifScrollIndicator:I = 0x7f0e013c

.field public static final notification:I = 0x7f0e013a

.field public static final notificationColorView:I = 0x7f0e0134

.field public static final notificationContainer:I = 0x7f0e01e3

.field public static final notificationExtensionView:I = 0x7f0e0138

.field public static final notification_icon:I = 0x7f0e014e

.field public static final notification_text:I = 0x7f0e01e4

.field public static final notification_user_image:I = 0x7f0e014f

.field public static final now:I = 0x7f0e021b

.field public static final now_icon:I = 0x7f0e01b9

.field public static final now_shrunk_icon:I = 0x7f0e01c6

.field public static final obd_data_lyt:I = 0x7f0e01fb

.field public static final openMapRoadInfo:I = 0x7f0e019c

.field public static final openMapRoadInfoContainer:I = 0x7f0e019b

.field public static final ota:I = 0x7f0e00af

.field public static final pMessageImage:I = 0x7f0e0158

.field public static final pSenderImage:I = 0x7f0e0157

.field public static final pSenderName:I = 0x7f0e0155

.field public static final pStatus:I = 0x7f0e0156

.field public static final packed:I = 0x7f0e009a

.field public static final paired:I = 0x7f0e00a3

.field public static final pairing:I = 0x7f0e00a4

.field public static final parent:I = 0x7f0e0097

.field public static final phone:I = 0x7f0e0214

.field public static final phoneGroup:I = 0x7f0e0212

.field public static final phoneNetwork:I = 0x7f0e0213

.field public static final picking:I = 0x7f0e00b2

.field public static final placeholder:I = 0x7f0e01ad

.field public static final places_exit:I = 0x7f0e0060

.field public static final places_fav:I = 0x7f0e0061

.field public static final places_home:I = 0x7f0e0062

.field public static final places_menu_recent:I = 0x7f0e0063

.field public static final places_queue:I = 0x7f0e0064

.field public static final places_recent:I = 0x7f0e0065

.field public static final places_work:I = 0x7f0e0066

.field public static final play_store_img_1:I = 0x7f0e01d6

.field public static final play_store_img_2:I = 0x7f0e01d7

.field public static final plus:I = 0x7f0e0146

.field public static final progress:I = 0x7f0e017d

.field public static final progress_bar:I = 0x7f0e01bb

.field public static final progress_bar_shrunk:I = 0x7f0e01c4

.field public static final recalcAnimator:I = 0x7f0e01a9

.field public static final recalcContainer:I = 0x7f0e01a8

.field public static final recalcImageView:I = 0x7f0e01aa

.field public static final recalcRouteContainer:I = 0x7f0e01a7

.field public static final recent_call_exit:I = 0x7f0e0067

.field public static final recyclerView:I = 0x7f0e0235

.field public static final repair_text:I = 0x7f0e0178

.field public static final repeat:I = 0x7f0e00ac

.field public static final reply_msg_exit:I = 0x7f0e0068

.field public static final report_issue_menu_eta_inaccurate:I = 0x7f0e0069

.field public static final report_issue_menu_not_fastest_route:I = 0x7f0e006a

.field public static final report_issue_menu_permanent_closure:I = 0x7f0e006b

.field public static final report_issue_menu_road_blocked:I = 0x7f0e006c

.field public static final report_issue_menu_wrong_direction:I = 0x7f0e006d

.field public static final report_issue_menu_wrong_road_name:I = 0x7f0e006e

.field public static final resultsCount:I = 0x7f0e015f

.field public static final resultsLabel:I = 0x7f0e0160

.field public static final results_count:I = 0x7f0e016c

.field public static final results_count_container:I = 0x7f0e016b

.field public static final retry:I = 0x7f0e006f

.field public static final rightBackground:I = 0x7f0e0173

.field public static final rightContainer:I = 0x7f0e0234

.field public static final rightDot:I = 0x7f0e01da

.field public static final rightSwipe:I = 0x7f0e00d4

.field public static final right_gauge:I = 0x7f0e01af

.field public static final rootContainer:I = 0x7f0e014b

.field public static final routeCalcContainer:I = 0x7f0e019d

.field public static final routeCalcFluctuator:I = 0x7f0e01a1

.field public static final routeCalcIndicator:I = 0x7f0e019e

.field public static final routeCalcInfoContainer:I = 0x7f0e01a4

.field public static final routeCalcName:I = 0x7f0e019f

.field public static final routeCalcProgress:I = 0x7f0e01a3

.field public static final routeCalcProgressContainer:I = 0x7f0e01a0

.field public static final routeCalcProgressImage:I = 0x7f0e01a2

.field public static final routeTtaView:I = 0x7f0e0165

.field public static final satellite_cno:I = 0x7f0e01b5

.field public static final satellite_id:I = 0x7f0e01b3

.field public static final satellite_strength:I = 0x7f0e01b6

.field public static final satellite_type:I = 0x7f0e01b4

.field public static final screenContainer:I = 0x7f0e0133

.field public static final scrim:I = 0x7f0e022a

.field public static final scrim_cover:I = 0x7f0e01cf

.field public static final scrollView:I = 0x7f0e0109

.field public static final scrollable_text_presenter:I = 0x7f0e01cb

.field public static final search_again:I = 0x7f0e0070

.field public static final search_menu_atm:I = 0x7f0e0071

.field public static final search_menu_coffee:I = 0x7f0e0072

.field public static final search_menu_food:I = 0x7f0e0073

.field public static final search_menu_gas:I = 0x7f0e0074

.field public static final search_menu_grocery_store:I = 0x7f0e0075

.field public static final search_menu_hospital:I = 0x7f0e0076

.field public static final search_menu_parking:I = 0x7f0e0077

.field public static final search_menu_parks:I = 0x7f0e0078

.field public static final searching:I = 0x7f0e00a5

.field public static final secondContainer:I = 0x7f0e017b

.field public static final selectedCustomView:I = 0x7f0e0233

.field public static final selectedIconColorImage:I = 0x7f0e0230

.field public static final selectedIconImage:I = 0x7f0e0231

.field public static final selectedImage:I = 0x7f0e022f

.field public static final selectedText:I = 0x7f0e0232

.field public static final sensor_blocked_message:I = 0x7f0e01cc

.field public static final settings_menu_auto_brightness:I = 0x7f0e0079

.field public static final settings_menu_back:I = 0x7f0e007a

.field public static final settings_menu_brightness:I = 0x7f0e007b

.field public static final settings_menu_connect_phone:I = 0x7f0e007c

.field public static final settings_menu_dial_pairing:I = 0x7f0e007d

.field public static final settings_menu_dial_update:I = 0x7f0e007e

.field public static final settings_menu_exit:I = 0x7f0e007f

.field public static final settings_menu_factory_reset:I = 0x7f0e0080

.field public static final settings_menu_learning_gestures:I = 0x7f0e0081

.field public static final settings_menu_shutdown:I = 0x7f0e0082

.field public static final settings_menu_software_update:I = 0x7f0e0083

.field public static final si_auto_on:I = 0x7f0e01e6

.field public static final si_check_engine_light:I = 0x7f0e0204

.field public static final si_device_mileage:I = 0x7f0e01f6

.field public static final si_dial_bt:I = 0x7f0e020e

.field public static final si_dial_fw:I = 0x7f0e020c

.field public static final si_display_bt:I = 0x7f0e020f

.field public static final si_display_fw:I = 0x7f0e020b

.field public static final si_eng_oil_pressure:I = 0x7f0e0208

.field public static final si_eng_temp:I = 0x7f0e0202

.field public static final si_eng_trip_fuel:I = 0x7f0e020a

.field public static final si_engine_oil_pressure_lyt:I = 0x7f0e0207

.field public static final si_engine_trouble_codes:I = 0x7f0e0205

.field public static final si_fix:I = 0x7f0e01f4

.field public static final si_fuel:I = 0x7f0e01fc

.field public static final si_gestures:I = 0x7f0e01e5

.field public static final si_gps:I = 0x7f0e01f1

.field public static final si_gps_speed:I = 0x7f0e01f7

.field public static final si_here_sdk_version:I = 0x7f0e01e9

.field public static final si_maf:I = 0x7f0e0201

.field public static final si_make:I = 0x7f0e01f8

.field public static final si_map_version:I = 0x7f0e01eb

.field public static final si_maps_data_verified:I = 0x7f0e01ed

.field public static final si_maps_voices_verified:I = 0x7f0e01ee

.field public static final si_model:I = 0x7f0e01f9

.field public static final si_obd_data:I = 0x7f0e01e7

.field public static final si_obd_fw:I = 0x7f0e020d

.field public static final si_odometer:I = 0x7f0e0200

.field public static final si_odometer_lyt:I = 0x7f0e01ff

.field public static final si_offline_maps:I = 0x7f0e01ec

.field public static final si_route_calc:I = 0x7f0e01ef

.field public static final si_route_pref:I = 0x7f0e01f0

.field public static final si_rpm:I = 0x7f0e01fd

.field public static final si_satellites:I = 0x7f0e01f2

.field public static final si_special_pids_lyt:I = 0x7f0e0206

.field public static final si_speed:I = 0x7f0e01fe

.field public static final si_temperature:I = 0x7f0e01f5

.field public static final si_trip_fuel_layout:I = 0x7f0e0209

.field public static final si_ui_scaling:I = 0x7f0e01e8

.field public static final si_updated:I = 0x7f0e01ea

.field public static final si_voltage:I = 0x7f0e0203

.field public static final si_year:I = 0x7f0e01fa

.field public static final sideImage:I = 0x7f0e00d1

.field public static final small:I = 0x7f0e00d7

.field public static final smartDashContainer:I = 0x7f0e01ab

.field public static final smart_dash_place_holder:I = 0x7f0e0181

.field public static final speedContainer:I = 0x7f0e0195

.field public static final speedUnitView:I = 0x7f0e0196

.field public static final speedView:I = 0x7f0e0197

.field public static final speed_limit_sign:I = 0x7f0e01de

.field public static final speed_limit_sign_eu:I = 0x7f0e01e2

.field public static final speed_limit_sign_us:I = 0x7f0e01e0

.field public static final splitter:I = 0x7f0e0131

.field public static final spread:I = 0x7f0e0098

.field public static final spread_inside:I = 0x7f0e009b

.field public static final startDestinationFluctuator:I = 0x7f0e0190

.field public static final status:I = 0x7f0e0168

.field public static final subStatus:I = 0x7f0e0169

.field public static final subTitle:I = 0x7f0e0111

.field public static final subTitle2:I = 0x7f0e0229

.field public static final subtitle:I = 0x7f0e014c

.field public static final systemTray:I = 0x7f0e0135

.field public static final take_snapshot_drive_score:I = 0x7f0e0084

.field public static final take_snapshot_maps:I = 0x7f0e0085

.field public static final take_snapshot_navigation:I = 0x7f0e0086

.field public static final take_snapshot_smart_dash:I = 0x7f0e0087

.field public static final tbtDirectionView:I = 0x7f0e0218

.field public static final tbtDistanceView:I = 0x7f0e0219

.field public static final tbtNextManeuverView:I = 0x7f0e021d

.field public static final tbtShrunkDistance:I = 0x7f0e01c5

.field public static final tbtShrunkDistanceContainer:I = 0x7f0e01c3

.field public static final tbtTextInstructionView:I = 0x7f0e0220

.field public static final text1:I = 0x7f0e0105

.field public static final text2:I = 0x7f0e0106

.field public static final text3:I = 0x7f0e0108

.field public static final textContainer:I = 0x7f0e0228

.field public static final textView:I = 0x7f0e010d

.field public static final text_headline:I = 0x7f0e0125

.field public static final time:I = 0x7f0e01c7

.field public static final tip_holder_1:I = 0x7f0e0240

.field public static final tip_holder_2:I = 0x7f0e0243

.field public static final tip_icon_1:I = 0x7f0e0241

.field public static final tip_icon_2:I = 0x7f0e0244

.field public static final tip_text_1:I = 0x7f0e0242

.field public static final tip_text_2:I = 0x7f0e0245

.field public static final tipsText1:I = 0x7f0e00fa

.field public static final tipsText2:I = 0x7f0e00fb

.field public static final tips_lyt:I = 0x7f0e00f8

.field public static final tips_scroller:I = 0x7f0e00f9

.field public static final title:I = 0x7f0e00c3

.field public static final title1:I = 0x7f0e0088

.field public static final title2:I = 0x7f0e0089

.field public static final title3:I = 0x7f0e008a

.field public static final title4:I = 0x7f0e008b

.field public static final toastView:I = 0x7f0e013d

.field public static final toggleSwitchBackground:I = 0x7f0e022c

.field public static final toggleSwitchThumb:I = 0x7f0e022d

.field public static final tooltip:I = 0x7f0e0236

.field public static final tooltip_container:I = 0x7f0e0221

.field public static final tooltip_scrim:I = 0x7f0e0223

.field public static final tooltip_text:I = 0x7f0e0222

.field public static final tooltip_triangle:I = 0x7f0e0224

.field public static final topContainer:I = 0x7f0e00ba

.field public static final topScrub:I = 0x7f0e010b

.field public static final traffic_jam_progress:I = 0x7f0e0167

.field public static final tripDistance:I = 0x7f0e00f0

.field public static final tripIcon:I = 0x7f0e00f1

.field public static final tripOpenMap:I = 0x7f0e00ee

.field public static final tripTime:I = 0x7f0e00ef

.field public static final tta:I = 0x7f0e00e4

.field public static final txt_ampm:I = 0x7f0e01c9

.field public static final txt_author:I = 0x7f0e0142

.field public static final txt_date:I = 0x7f0e00dd

.field public static final txt_day:I = 0x7f0e00de

.field public static final txt_empty_gauge:I = 0x7f0e00e1

.field public static final txt_eta:I = 0x7f0e00ec

.field public static final txt_eta_am_pm:I = 0x7f0e00ed

.field public static final txt_left:I = 0x7f0e00fe

.field public static final txt_mpg:I = 0x7f0e019a

.field public static final txt_mpg_label:I = 0x7f0e0199

.field public static final txt_name:I = 0x7f0e0143

.field public static final txt_player_state:I = 0x7f0e0144

.field public static final txt_remaining_distance:I = 0x7f0e00ea

.field public static final txt_remaining_distance_unit:I = 0x7f0e00eb

.field public static final txt_right:I = 0x7f0e0104

.field public static final txt_rpm_label:I = 0x7f0e0215

.field public static final txt_say_something_like_this:I = 0x7f0e023f

.field public static final txt_speed_limit:I = 0x7f0e01e1

.field public static final txt_speed_limit_unavailable:I = 0x7f0e01df

.field public static final txt_summary:I = 0x7f0e00e0

.field public static final txt_time:I = 0x7f0e01c8

.field public static final txt_tta1:I = 0x7f0e00e5

.field public static final txt_tta2:I = 0x7f0e00e6

.field public static final txt_tta3:I = 0x7f0e00e7

.field public static final txt_tta4:I = 0x7f0e00e8

.field public static final txt_unit:I = 0x7f0e00bd

.field public static final txt_value:I = 0x7f0e00be

.field public static final up:I = 0x7f0e00a7

.field public static final videoContainer:I = 0x7f0e0176

.field public static final videoView:I = 0x7f0e0177

.field public static final view1:I = 0x7f0e0216

.field public static final view2:I = 0x7f0e0217

.field public static final view_header:I = 0x7f0e0128

.field public static final vlist_blank_item:I = 0x7f0e008c

.field public static final vlist_content_loading:I = 0x7f0e008d

.field public static final vlist_image:I = 0x7f0e008e

.field public static final vlist_loading:I = 0x7f0e008f

.field public static final vlist_scroll_item:I = 0x7f0e0090

.field public static final vlist_title:I = 0x7f0e0091

.field public static final vlist_title_subtitle:I = 0x7f0e0092

.field public static final voice_search:I = 0x7f0e0093

.field public static final voice_search_information:I = 0x7f0e0246

.field public static final voice_search_listening_feedback:I = 0x7f0e016d

.field public static final voice_search_tips:I = 0x7f0e023e

.field public static final web_update_details:I = 0x7f0e012c

.field public static final welcome_menu_add_driver:I = 0x7f0e0094

.field public static final welcome_menu_connecting:I = 0x7f0e0095

.field public static final welcome_menu_searching:I = 0x7f0e0096

.field public static final wrap:I = 0x7f0e0099

.field public static final wrapper_attachments:I = 0x7f0e011c

.field public static final wrapper_feedback:I = 0x7f0e0117

.field public static final wrapper_feedback_scroll:I = 0x7f0e0116

.field public static final wrapper_messages:I = 0x7f0e011f

.field public static final wrapper_messages_buttons:I = 0x7f0e0121


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2904
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
