.class Lcom/navdy/hud/app/device/dial/DialManagerHelper$7$2;
.super Ljava/lang/Object;
.source "DialManagerHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;->onServiceDisconnected(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;

    .prologue
    .line 252
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7$2;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 256
    :try_start_0
    # getter for: Lcom/navdy/hud/app/device/dial/DialManagerHelper;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "[Dial]disconnectFromDial onServiceDisconnected: could not connect proxy"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 257
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7$2;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;

    iget-boolean v1, v1, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;->eventSent:Z

    if-nez v1, :cond_0

    .line 258
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7$2;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;->eventSent:Z

    .line 259
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7$2;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;

    iget-object v1, v1, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;->val$callBack:Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;->onAttemptDisconnection(Z)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 264
    :cond_0
    :goto_0
    return-void

    .line 261
    :catch_0
    move-exception v0

    .line 262
    .local v0, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/device/dial/DialManagerHelper;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "[Dial]"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
