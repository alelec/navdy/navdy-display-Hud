.class public final enum Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;
.super Ljava/lang/Enum;
.source "DialFirmwareUpdater.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Error"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;

.field public static final enum CANCELLED:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;

.field public static final enum INVALID_COMMAND:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;

.field public static final enum INVALID_IMAGE_SIZE:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;

.field public static final enum INVALID_RESPONSE:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;

.field public static final enum NONE:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;

.field public static final enum TIMEDOUT:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 110
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;->NONE:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;

    .line 111
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;

    const-string v1, "INVALID_IMAGE_SIZE"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;->INVALID_IMAGE_SIZE:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;

    .line 112
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;

    const-string v1, "INVALID_COMMAND"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;->INVALID_COMMAND:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;

    .line 113
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;

    const-string v1, "INVALID_RESPONSE"

    invoke-direct {v0, v1, v6}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;->INVALID_RESPONSE:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;

    .line 114
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;

    const-string v1, "CANCELLED"

    invoke-direct {v0, v1, v7}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;->CANCELLED:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;

    .line 115
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;

    const-string v1, "TIMEDOUT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;->TIMEDOUT:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;

    .line 109
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;

    sget-object v1, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;->NONE:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;->INVALID_IMAGE_SIZE:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;->INVALID_COMMAND:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;->INVALID_RESPONSE:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;->CANCELLED:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;->TIMEDOUT:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;->$VALUES:[Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 109
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 109
    const-class v0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;
    .locals 1

    .prologue
    .line 109
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;->$VALUES:[Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;

    return-object v0
.end method
