.class final Lcom/navdy/hud/app/device/dial/DialManagerHelper$1;
.super Ljava/lang/Object;
.source "DialManagerHelper.java"

# interfaces
.implements Landroid/bluetooth/BluetoothProfile$ServiceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/device/dial/DialManagerHelper;->isDialConnected(Landroid/bluetooth/BluetoothAdapter;Landroid/bluetooth/BluetoothDevice;Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnectionStatus;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$callBack:Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnectionStatus;

.field final synthetic val$device:Landroid/bluetooth/BluetoothDevice;


# direct methods
.method constructor <init>(Landroid/bluetooth/BluetoothDevice;Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnectionStatus;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$1;->val$device:Landroid/bluetooth/BluetoothDevice;

    iput-object p2, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$1;->val$callBack:Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnectionStatus;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V
    .locals 3
    .param p1, "profile"    # I
    .param p2, "proxy"    # Landroid/bluetooth/BluetoothProfile;

    .prologue
    .line 49
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/device/dial/DialManagerHelper$1$1;

    invoke-direct {v1, p0, p2}, Lcom/navdy/hud/app/device/dial/DialManagerHelper$1$1;-><init>(Lcom/navdy/hud/app/device/dial/DialManagerHelper$1;Landroid/bluetooth/BluetoothProfile;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 69
    return-void
.end method

.method public onServiceDisconnected(I)V
    .locals 3
    .param p1, "profile"    # I

    .prologue
    .line 73
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/device/dial/DialManagerHelper$1$2;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/device/dial/DialManagerHelper$1$2;-><init>(Lcom/navdy/hud/app/device/dial/DialManagerHelper$1;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 84
    return-void
.end method
