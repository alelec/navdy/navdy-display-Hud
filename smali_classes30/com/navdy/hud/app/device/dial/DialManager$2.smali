.class Lcom/navdy/hud/app/device/dial/DialManager$2;
.super Ljava/lang/Object;
.source "DialManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/device/dial/DialManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/device/dial/DialManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/dial/DialManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 184
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager$2;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 188
    :try_start_0
    sget-object v2, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "[Dial]bond hang detected"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 189
    const/4 v0, 0x0

    .line 190
    .local v0, "dialName":Ljava/lang/String;
    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager$2;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->bondingDial:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v2}, Lcom/navdy/hud/app/device/dial/DialManager;->access$000(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 191
    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager$2;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->bondingDial:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v2}, Lcom/navdy/hud/app/device/dial/DialManager;->access$000(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v0

    .line 193
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager$2;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    const/4 v3, 0x0

    # setter for: Lcom/navdy/hud/app/device/dial/DialManager;->bondingDial:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v2, v3}, Lcom/navdy/hud/app/device/dial/DialManager;->access$002(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;

    .line 194
    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->DIAL_NOT_CONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->access$100()Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

    move-result-object v2

    iput-object v0, v2, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;->dialName:Ljava/lang/String;

    .line 195
    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager$2;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v2}, Lcom/navdy/hud/app/device/dial/DialManager;->access$200(Lcom/navdy/hud/app/device/dial/DialManager;)Lcom/squareup/otto/Bus;

    move-result-object v2

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->DIAL_NOT_CONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->access$100()Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 199
    .end local v0    # "dialName":Ljava/lang/String;
    :goto_0
    return-void

    .line 196
    :catch_0
    move-exception v1

    .line 197
    .local v1, "t":Ljava/lang/Throwable;
    sget-object v2, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "[Dial]"

    invoke-virtual {v2, v3, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
