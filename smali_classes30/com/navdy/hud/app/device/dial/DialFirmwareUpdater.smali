.class public Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;
.super Ljava/lang/Object;
.source "DialFirmwareUpdater.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$UpdateProgressListener;,
        Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;,
        Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$UpdateListener;,
        Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;
    }
.end annotation


# static fields
.field private static final FIRMWARE_CHUNK_SIZE:I = 0x10

.field public static final FIRMWARE_INCREMENTAL_VERSION_PREFIX:Ljava/lang/String; = "Firmware incremental version: "

.field public static final FIRMWARE_LOCAL_FILE:Ljava/lang/String; = "/system/etc/firmware/dial.ota"

.field public static final FIRMWARE_VERSION_PREFIX:Ljava/lang/String; = "Firmware version: "

.field public static final LOG_PROGRESS_PERIOD:I = 0x1e

.field private static final OTA_COMMAND_BOOT_DFU:I = 0x3

.field private static final OTA_COMMAND_ERASE_BLOCK_0:I = 0x0

.field private static final OTA_COMMAND_ERASE_BLOCK_1:I = 0x1

.field private static final OTA_COMMAND_PREPARE:I = 0x4

.field private static final OTA_COMMAND_RESET_DFU_PTR:I = 0x2

.field private static final OTA_STATE_CANCELLED:I = 0x1

.field private static final OTA_STATE_NONE:I = 0x0

.field private static final OTA_STATE_READY_TO_REBOOT:I = 0x20

.field private static final OTA_STATE_STARTED:I = 0x10

.field private static final RESP_ERROR_OTA_ALREADY_STARTED:I = 0x83

.field private static final RESP_ERROR_OTA_INVALID_COMMAND:I = 0x80

.field private static final RESP_ERROR_OTA_TOO_LARGE:I = 0x81

.field private static final RESP_ERROR_OTA_UNKNOWN_STATE:I = 0x82

.field private static final RESP_OK:I = 0x0

.field private static final UNDETERMINED_FIRMWARE_VERSION:I = -0x1

.field private static final UPDATE_FINISH_DELAY:J = 0x1388L

.field private static final UPDATE_INTERPACKET_DELAY:J = 0x32L

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private commandSent:I

.field private deviceInfoFirmwareRevisionCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

.field private final dialManager:Lcom/navdy/hud/app/device/dial/DialManager;

.field private dialName:Ljava/lang/String;

.field private firmwareData:[B

.field private firmwareOffset:I

.field private handler:Landroid/os/Handler;

.field private otaControlCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

.field private otaDataCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

.field private otaIncrementalVersionCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

.field private sendFirmwareChunkRunnable:Ljava/lang/Runnable;

.field private updateListener:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$UpdateListener;

.field private updateProgressListener:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$UpdateProgressListener;

.field private updateRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private versions:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 22
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/os/Handler;)V
    .locals 5
    .param p1, "dialManager"    # Lcom/navdy/hud/app/device/dial/DialManager;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    new-instance v2, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;-><init>(Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;)V

    iput-object v2, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->versions:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;

    .line 93
    new-instance v2, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v2, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->updateRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 118
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->dialName:Ljava/lang/String;

    .line 253
    new-instance v2, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$1;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$1;-><init>(Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;)V

    iput-object v2, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->sendFirmwareChunkRunnable:Ljava/lang/Runnable;

    .line 131
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->dialManager:Lcom/navdy/hud/app/device/dial/DialManager;

    .line 132
    iput-object p2, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->handler:Landroid/os/Handler;

    .line 134
    :try_start_0
    const-string v2, "/system/etc/firmware/dial.ota"

    invoke-static {v2}, Lcom/navdy/service/library/util/IOUtils;->readBinaryFile(Ljava/lang/String;)[B

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->firmwareData:[B

    .line 135
    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->versions:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;

    iget-object v2, v2, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;->local:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;

    invoke-virtual {v2}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;->reset()V

    .line 136
    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->firmwareData:[B

    const-string v3, "Firmware incremental version: "

    invoke-static {v2, v3}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->extractVersion([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 137
    .local v1, "v":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 138
    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->versions:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;

    iget-object v2, v2, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;->local:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;->incrementalVersion:I

    .line 140
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->versions:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;

    iget-object v2, v2, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;->local:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;

    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->firmwareData:[B

    const-string v4, "Firmware version: "

    invoke-static {v3, v4}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->extractVersion([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;->revisionString:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 146
    .end local v1    # "v":Ljava/lang/String;
    :goto_0
    return-void

    .line 141
    :catch_0
    move-exception v0

    .line 142
    .local v0, "e":Ljava/io/FileNotFoundException;
    sget-object v2, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "no dial firmware OTA file found"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 143
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 144
    .local v0, "e":Ljava/lang/Throwable;
    sget-object v2, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "can\'t read dial firmware OTA file"

    invoke-virtual {v2, v3, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->sendFirmwareChunk()V

    return-void
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;
    .param p1, "x1"    # Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->finishUpdate(Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;Ljava/lang/String;)V

    return-void
.end method

.method private static extractVersion([BLjava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "firmwareData"    # [B
    .param p1, "prefix"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 149
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-static {p0, v3}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->indexOf([B[B)I

    move-result v1

    .line 150
    .local v1, "j":I
    if-gez v1, :cond_1

    .line 160
    :cond_0
    :goto_0
    return-object v2

    .line 153
    :cond_1
    move v0, v1

    .line 154
    .local v0, "i":I
    :goto_1
    array-length v3, p0

    if-ge v0, v3, :cond_0

    .line 155
    aget-byte v3, p0, v0

    if-eqz v3, :cond_2

    aget-byte v3, p0, v0

    const/16 v4, 0xd

    if-ne v3, v4, :cond_3

    .line 156
    :cond_2
    new-instance v2, Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v3, v1

    invoke-static {p0, v3, v0}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([B)V

    goto :goto_0

    .line 158
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private finishUpdate(Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;Ljava/lang/String;)V
    .locals 3
    .param p1, "error"    # Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;
    .param p2, "detail"    # Ljava/lang/String;

    .prologue
    .line 374
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "finished dial update error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 375
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->updateProgressListener:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$UpdateProgressListener;

    if-eqz v0, :cond_0

    .line 376
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->updateProgressListener:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$UpdateProgressListener;

    invoke-interface {v0, p1, p2}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$UpdateProgressListener;->onFinished(Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;Ljava/lang/String;)V

    .line 378
    :cond_0
    return-void
.end method

.method public static indexOf([B[B)I
    .locals 5
    .param p0, "array"    # [B
    .param p1, "target"    # [B

    .prologue
    .line 164
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, p0

    array-length v4, p1

    sub-int/2addr v3, v4

    add-int/lit8 v3, v3, 0x1

    if-ge v1, v3, :cond_3

    .line 165
    const/4 v0, 0x1

    .line 166
    .local v0, "found":Z
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    array-length v3, p1

    if-ge v2, v3, :cond_0

    .line 167
    add-int v3, v1, v2

    aget-byte v3, p0, v3

    aget-byte v4, p1, v2

    if-eq v3, v4, :cond_1

    .line 168
    const/4 v0, 0x0

    .line 172
    :cond_0
    if-eqz v0, :cond_2

    .line 174
    .end local v0    # "found":Z
    .end local v1    # "i":I
    .end local v2    # "j":I
    :goto_2
    return v1

    .line 166
    .restart local v0    # "found":Z
    .restart local v1    # "i":I
    .restart local v2    # "j":I
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 164
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 174
    .end local v0    # "found":Z
    .end local v2    # "j":I
    :cond_3
    const/4 v1, -0x1

    goto :goto_2
.end method

.method private sendCommand(I)V
    .locals 5
    .param p1, "command"    # I

    .prologue
    .line 314
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sendCommand: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 315
    iput p1, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->commandSent:I

    .line 316
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->dialManager:Lcom/navdy/hud/app/device/dial/DialManager;

    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->otaControlCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    const/4 v2, 0x1

    new-array v2, v2, [B

    const/4 v3, 0x0

    int-to-byte v4, p1

    aput-byte v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/device/dial/DialManager;->queueWrite(Landroid/bluetooth/BluetoothGattCharacteristic;[B)V

    .line 317
    return-void
.end method

.method private sendFirmwareChunk()V
    .locals 4

    .prologue
    .line 362
    iget v1, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->firmwareOffset:I

    div-int/lit8 v1, v1, 0x10

    rem-int/lit8 v1, v1, 0x1e

    if-nez v1, :cond_0

    .line 363
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "firmwareOffset: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->firmwareOffset:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 364
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->updateProgressListener:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$UpdateProgressListener;

    if-eqz v1, :cond_0

    .line 365
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->updateProgressListener:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$UpdateProgressListener;

    iget v2, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->firmwareOffset:I

    mul-int/lit8 v2, v2, 0x64

    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->firmwareData:[B

    array-length v3, v3

    div-int/2addr v2, v3

    invoke-interface {v1, v2}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$UpdateProgressListener;->onProgress(I)V

    .line 368
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->firmwareData:[B

    iget v2, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->firmwareOffset:I

    iget v3, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->firmwareOffset:I

    add-int/lit8 v3, v3, 0x10

    invoke-static {v1, v2, v3}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v0

    .line 369
    .local v0, "buf":[B
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->dialManager:Lcom/navdy/hud/app/device/dial/DialManager;

    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->otaDataCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    invoke-virtual {v1, v2, v0}, Lcom/navdy/hud/app/device/dial/DialManager;->queueWrite(Landroid/bluetooth/BluetoothGattCharacteristic;[B)V

    .line 370
    iget v1, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->firmwareOffset:I

    add-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->firmwareOffset:I

    .line 371
    return-void
.end method

.method private startFirmwareTransfer()V
    .locals 1

    .prologue
    .line 357
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->firmwareOffset:I

    .line 358
    invoke-direct {p0}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->sendFirmwareChunk()V

    .line 359
    return-void
.end method


# virtual methods
.method public cancelUpdate()V
    .locals 3

    .prologue
    .line 347
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->updateRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 348
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;->CANCELLED:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->finishUpdate(Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;Ljava/lang/String;)V

    .line 350
    :cond_0
    return-void
.end method

.method public characteristicsAvailable()Z
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->otaControlCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->otaDataCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->otaIncrementalVersionCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->deviceInfoFirmwareRevisionCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDialName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 353
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->dialName:Ljava/lang/String;

    return-object v0
.end method

.method public getVersions()Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->versions:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;

    return-object v0
.end method

.method public onCharacteristicRead(Landroid/bluetooth/BluetoothGattCharacteristic;I)V
    .locals 6
    .param p1, "characteristic"    # Landroid/bluetooth/BluetoothGattCharacteristic;
    .param p2, "status"    # I

    .prologue
    .line 225
    invoke-virtual {p0}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->characteristicsAvailable()Z

    move-result v1

    if-nez v1, :cond_1

    .line 251
    :cond_0
    :goto_0
    return-void

    .line 228
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->otaIncrementalVersionCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    if-ne p1, v1, :cond_2

    .line 229
    if-nez p2, :cond_3

    .line 230
    new-instance v0, Ljava/lang/String;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getValue()[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    .line 231
    .local v0, "ver":Ljava/lang/String;
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->versions:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;

    iget-object v1, v1, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;->dial:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;->incrementalVersion:I

    .line 232
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "updateAvailable: %s (%d -> %d)"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->versions:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;

    .line 233
    invoke-virtual {v5}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;->isUpdateAvailable()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->versions:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;

    iget-object v5, v5, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;->dial:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;

    iget v5, v5, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;->incrementalVersion:I

    .line 234
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->versions:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;

    iget-object v5, v5, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;->local:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;

    iget v5, v5, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;->incrementalVersion:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 232
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 235
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->updateListener:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$UpdateListener;

    if-eqz v1, :cond_2

    .line 236
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->updateListener:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$UpdateListener;

    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->versions:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;

    invoke-virtual {v2}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;->isUpdateAvailable()Z

    move-result v2

    invoke-interface {v1, v2}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$UpdateListener;->onUpdateState(Z)V

    .line 242
    .end local v0    # "ver":Ljava/lang/String;
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->deviceInfoFirmwareRevisionCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    if-ne p1, v1, :cond_0

    .line 243
    if-nez p2, :cond_4

    .line 244
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->versions:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;

    iget-object v1, v1, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;->dial:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;

    new-instance v2, Ljava/lang/String;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getValue()[B

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([B)V

    iput-object v2, v1, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;->revisionString:Ljava/lang/String;

    .line 245
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dial.revisionString: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->versions:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;

    iget-object v3, v3, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;->dial:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;

    iget-object v3, v3, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;->revisionString:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 246
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->dialManager:Lcom/navdy/hud/app/device/dial/DialManager;

    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->otaIncrementalVersionCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/device/dial/DialManager;->queueRead(Landroid/bluetooth/BluetoothGattCharacteristic;)V

    goto/16 :goto_0

    .line 239
    :cond_3
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "can\'t read dial firmware incremental version"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_1

    .line 248
    :cond_4
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "can\'t read dial firmware revisionString string"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public onCharacteristicWrite(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;I)V
    .locals 6
    .param p1, "gatt"    # Landroid/bluetooth/BluetoothGatt;
    .param p2, "characteristic"    # Landroid/bluetooth/BluetoothGattCharacteristic;
    .param p3, "status"    # I

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    .line 261
    invoke-virtual {p0}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->characteristicsAvailable()Z

    move-result v1

    if-nez v1, :cond_1

    .line 311
    :cond_0
    :goto_0
    return-void

    .line 264
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->otaControlCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    if-ne p2, v1, :cond_3

    .line 265
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dial OTA response for command "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->commandSent:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", status: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 266
    if-nez p3, :cond_2

    .line 267
    iget v1, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->commandSent:I

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 272
    :pswitch_1
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->sendCommand(I)V

    goto :goto_0

    .line 269
    :pswitch_2
    invoke-direct {p0, v4}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->sendCommand(I)V

    goto :goto_0

    .line 275
    :pswitch_3
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->sendCommand(I)V

    goto :goto_0

    .line 278
    :pswitch_4
    invoke-direct {p0}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->startFirmwareTransfer()V

    goto :goto_0

    .line 282
    :cond_2
    iget v1, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->commandSent:I

    if-eq v1, v5, :cond_0

    .line 283
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;->INVALID_COMMAND:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;

    iget v2, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->commandSent:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->finishUpdate(Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;Ljava/lang/String;)V

    goto :goto_0

    .line 286
    :cond_3
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->otaDataCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    if-ne p2, v1, :cond_0

    .line 287
    if-nez p3, :cond_5

    .line 288
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->updateRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 289
    iget v1, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->firmwareOffset:I

    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->firmwareData:[B

    array-length v2, v2

    if-ge v1, v2, :cond_4

    .line 290
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->sendFirmwareChunkRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x32

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 293
    :cond_4
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "f/w upgrade complete, send OTA_COMMAND_BOOT_DFU, mark updating false"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 294
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->updateRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 295
    invoke-direct {p0, v5}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->sendCommand(I)V

    .line 296
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGatt;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    .line 297
    .local v0, "device":Landroid/bluetooth/BluetoothDevice;
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->handler:Landroid/os/Handler;

    new-instance v2, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$2;

    invoke-direct {v2, p0, v0}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$2;-><init>(Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;Landroid/bluetooth/BluetoothDevice;)V

    const-wide/16 v4, 0x1388

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 307
    .end local v0    # "device":Landroid/bluetooth/BluetoothDevice;
    :cond_5
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dial OTA error response for data chunk, status: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 308
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;->INVALID_RESPONSE:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->finishUpdate(Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 267
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onServicesDiscovered(Landroid/bluetooth/BluetoothGatt;)V
    .locals 7
    .param p1, "gatt"    # Landroid/bluetooth/BluetoothGatt;

    .prologue
    const/4 v3, 0x0

    .line 185
    iput-object v3, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->otaControlCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 186
    iput-object v3, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->otaDataCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 187
    iput-object v3, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->otaIncrementalVersionCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 188
    iput-object v3, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->deviceInfoFirmwareRevisionCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 190
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->dialManager:Lcom/navdy/hud/app/device/dial/DialManager;

    invoke-virtual {v3}, Lcom/navdy/hud/app/device/dial/DialManager;->getDialName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->dialName:Ljava/lang/String;

    .line 192
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGatt;->getServices()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothGattService;

    .line 193
    .local v1, "service":Landroid/bluetooth/BluetoothGattService;
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothGattService;->getUuid()Ljava/util/UUID;

    move-result-object v2

    .line 194
    .local v2, "uuid":Ljava/util/UUID;
    sget-object v4, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "service: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 195
    sget-object v4, Lcom/navdy/hud/app/device/dial/DialConstants;->OTA_SERVICE_UUID:Ljava/util/UUID;

    invoke-virtual {v4, v2}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 196
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothGattService;->getCharacteristics()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 197
    .local v0, "characteristic":Landroid/bluetooth/BluetoothGattCharacteristic;
    sget-object v5, Lcom/navdy/hud/app/device/dial/DialConstants;->OTA_CONTROL_CHARACTERISTIC_UUID:Ljava/util/UUID;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 198
    iput-object v0, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->otaControlCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 199
    iget-object v5, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->otaControlCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    const/4 v6, 0x2

    invoke-virtual {v5, v6}, Landroid/bluetooth/BluetoothGattCharacteristic;->setWriteType(I)V

    goto :goto_0

    .line 200
    :cond_2
    sget-object v5, Lcom/navdy/hud/app/device/dial/DialConstants;->OTA_DATA_CHARACTERISTIC_UUID:Ljava/util/UUID;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 201
    iput-object v0, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->otaDataCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 202
    iget-object v5, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->otaDataCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/bluetooth/BluetoothGattCharacteristic;->setWriteType(I)V

    goto :goto_0

    .line 203
    :cond_3
    sget-object v5, Lcom/navdy/hud/app/device/dial/DialConstants;->OTA_INCREMENTAL_VERSION_CHARACTERISTIC_UUID:Ljava/util/UUID;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 204
    iput-object v0, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->otaIncrementalVersionCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    goto :goto_0

    .line 207
    .end local v0    # "characteristic":Landroid/bluetooth/BluetoothGattCharacteristic;
    :cond_4
    sget-object v4, Lcom/navdy/hud/app/device/dial/DialConstants;->DEVICE_INFO_SERVICE_UUID:Ljava/util/UUID;

    invoke-virtual {v4, v2}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 208
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothGattService;->getCharacteristics()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_5
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 209
    .restart local v0    # "characteristic":Landroid/bluetooth/BluetoothGattCharacteristic;
    sget-object v5, Lcom/navdy/hud/app/device/dial/DialConstants;->FIRMWARE_REVISION_CHARACTERISTIC_UUID:Ljava/util/UUID;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 210
    iput-object v0, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->deviceInfoFirmwareRevisionCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    goto :goto_1

    .line 216
    .end local v0    # "characteristic":Landroid/bluetooth/BluetoothGattCharacteristic;
    .end local v1    # "service":Landroid/bluetooth/BluetoothGattService;
    .end local v2    # "uuid":Ljava/util/UUID;
    :cond_6
    invoke-virtual {p0}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->characteristicsAvailable()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 217
    sget-object v3, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "required OTA characteristics found"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 218
    invoke-virtual {p0}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->startVersionDetection()Z

    .line 222
    :goto_2
    return-void

    .line 220
    :cond_7
    sget-object v3, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "required OTA characteristics not found"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public setUpdateListener(Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$UpdateListener;)V
    .locals 0
    .param p1, "updateListener"    # Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$UpdateListener;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->updateListener:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$UpdateListener;

    return-void
.end method

.method public startUpdate(Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$UpdateProgressListener;)Z
    .locals 5
    .param p1, "progressListener"    # Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$UpdateProgressListener;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 330
    sget-object v3, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "starting dial firmware update"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 331
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->firmwareData:[B

    array-length v3, v3

    rem-int/lit8 v3, v3, 0x10

    if-eqz v3, :cond_1

    .line 332
    const-string v0, "dial firmware OTA file size is not divisible by 16, aborting update"

    .line 333
    .local v0, "err":Ljava/lang/String;
    sget-object v2, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 334
    sget-object v2, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;->INVALID_IMAGE_SIZE:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;

    invoke-interface {p1, v2, v0}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$UpdateProgressListener;->onFinished(Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Error;Ljava/lang/String;)V

    .line 342
    .end local v0    # "err":Ljava/lang/String;
    :cond_0
    :goto_0
    return v1

    .line 337
    :cond_1
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->updateRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v3, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 338
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->updateProgressListener:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$UpdateProgressListener;

    .line 339
    const/4 v1, 0x4

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->sendCommand(I)V

    move v1, v2

    .line 340
    goto :goto_0
.end method

.method public startVersionDetection()Z
    .locals 2

    .prologue
    .line 320
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->versions:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;

    iget-object v0, v0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;->dial:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;

    invoke-virtual {v0}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;->reset()V

    .line 321
    invoke-virtual {p0}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->characteristicsAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 322
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->dialManager:Lcom/navdy/hud/app/device/dial/DialManager;

    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->deviceInfoFirmwareRevisionCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/device/dial/DialManager;->queueRead(Landroid/bluetooth/BluetoothGattCharacteristic;)V

    .line 323
    const/4 v0, 0x1

    .line 325
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
