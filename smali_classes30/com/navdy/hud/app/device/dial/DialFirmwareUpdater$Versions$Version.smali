.class public Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;
.super Ljava/lang/Object;
.source "DialFirmwareUpdater.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Version"
.end annotation


# instance fields
.field public incrementalVersion:I

.field public revisionString:Ljava/lang/String;

.field final synthetic this$1:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;->this$1:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;->reset()V

    return-void
.end method


# virtual methods
.method reset()V
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;->revisionString:Ljava/lang/String;

    .line 64
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;->incrementalVersion:I

    .line 65
    return-void
.end method
