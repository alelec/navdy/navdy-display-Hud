.class final Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;
.super Ljava/lang/Object;
.source "DialManagerHelper.java"

# interfaces
.implements Landroid/bluetooth/BluetoothProfile$ServiceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/device/dial/DialManagerHelper;->disconnectFromDial(Landroid/bluetooth/BluetoothAdapter;Landroid/bluetooth/BluetoothDevice;Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field eventSent:Z

.field final synthetic val$callBack:Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;

.field final synthetic val$device:Landroid/bluetooth/BluetoothDevice;


# direct methods
.method constructor <init>(Landroid/bluetooth/BluetoothDevice;Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;)V
    .locals 1

    .prologue
    .line 205
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;->val$device:Landroid/bluetooth/BluetoothDevice;

    iput-object p2, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;->val$callBack:Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 206
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;->eventSent:Z

    return-void
.end method


# virtual methods
.method public onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V
    .locals 3
    .param p1, "profile"    # I
    .param p2, "proxy"    # Landroid/bluetooth/BluetoothProfile;

    .prologue
    .line 209
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7$1;

    invoke-direct {v1, p0, p2}, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7$1;-><init>(Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;Landroid/bluetooth/BluetoothProfile;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 248
    return-void
.end method

.method public onServiceDisconnected(I)V
    .locals 3
    .param p1, "profile"    # I

    .prologue
    .line 252
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7$2;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7$2;-><init>(Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 266
    return-void
.end method
