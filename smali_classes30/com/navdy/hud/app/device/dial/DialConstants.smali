.class public Lcom/navdy/hud/app/device/dial/DialConstants;
.super Ljava/lang/Object;
.source "DialConstants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;,
        Lcom/navdy/hud/app/device/dial/DialConstants$BatteryChangeEvent;,
        Lcom/navdy/hud/app/device/dial/DialConstants$DialManagerInitEvent;,
        Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;,
        Lcom/navdy/hud/app/device/dial/DialConstants$PairingRule;
    }
.end annotation


# static fields
.field public static final ACTION_CONNECTION_STATE_CHANGED:Ljava/lang/String; = "android.bluetooth.input.profile.action.CONNECTION_STATE_CHANGED"

.field static final BATTERY_CHANGE_EVENT:Lcom/navdy/hud/app/device/dial/DialConstants$BatteryChangeEvent;

.field static final BATTERY_CHECK_INTERVAL:I = 0x493e0

.field static final BATTERY_DISPLAY_SCALE:F = 0.1f

.field public static final BATTERY_LEVEL_UNKNOWN:I = -0x1

.field static final BATTERY_LEVEL_UUID:Ljava/util/UUID;

.field static final BATTERY_SERVICE_UUID:Ljava/util/UUID;

.field static final CONNECT:Ljava/lang/String; = "CONNECT"

.field static final CONNECTION_NOT_DONE_TIMEOUT:I = 0x1388

.field static final DEVICE_INFO_FIRMWARE_VERSION_UUID:Ljava/util/UUID;

.field static final DEVICE_INFO_HARDWARE_VERSION_UUID:Ljava/util/UUID;

.field static final DEVICE_INFO_SERVICE_UUID:Ljava/util/UUID;

.field public static final DIAL_ADDRESS:Ljava/lang/String; = "Dial_Address"

.field public static final DIAL_BATTERY_LEVEL:Ljava/lang/String; = "Battery_Level"

.field static final DIAL_EVENTS_PORT:I = 0x5c66

.field public static final DIAL_EVENT_CONNECT:Ljava/lang/String; = "Connect"

.field public static final DIAL_EVENT_INPUT_REPORT_DESCRIPTOR:Ljava/lang/String; = "INPUTDESCRIPTOR_WRITTEN"

.field public static final DIAL_EVENT_PAIR:Ljava/lang/String; = "Pair_Attempt"

.field public static final DIAL_EVENT_REBOOT:Ljava/lang/String; = "Reboot"

.field public static final DIAL_EVENT_TYPE:Ljava/lang/String; = "Type"

.field static final DIAL_EXTREMELY_LOW_BATTERY_PROPERTY:Ljava/lang/String; = "last_extremely_low_battery_notification"

.field public static final DIAL_FIRST_TIME:Ljava/lang/String; = "First_Time"

.field static final DIAL_FORGET_KEYS_MAGIC:[B

.field static final DIAL_FORGET_KEYS_UUID:Ljava/util/UUID;

.field public static final DIAL_FW_VERSION:Ljava/lang/String; = "FW_Version"

.field public static final DIAL_HW_VERSION:Ljava/lang/String; = "HW_Version"

.field public static final DIAL_INCREMENTAL_VERSION:Ljava/lang/String; = "Incremental_Version"

.field public static final DIAL_PAIRING_EVENT:Ljava/lang/String; = "Dial_Pairing"

.field public static final DIAL_PAIRING_SUCCESS:Ljava/lang/String; = "Success"

.field public static final DIAL_RAW_BATTERY_LEVEL:Ljava/lang/String; = "Raw_Battery_Level"

.field static final DIAL_REBOOT_MAGIC:[B

.field public static final DIAL_REBOOT_PROPERTY:Ljava/lang/String; = "last_dial_reboot"

.field static final DIAL_REBOOT_UUID:Ljava/util/UUID;

.field public static final DIAL_RESULT:Ljava/lang/String; = "Result"

.field public static final DIAL_TEMPERATURE:Ljava/lang/String; = "System_Temperature"

.field static final DIAL_VERY_LOW_BATTERY_PROPERTY:Ljava/lang/String; = "last_very_low_battery_notification"

.field static final DISCONNECT:Ljava/lang/String; = "DISCONNECT"

.field static final DISCONNECTED_THRESHOLD:I = 0x1388

.field static final ENCRYPTION_FAILED:Ljava/lang/String; = "ENCRYPTION_FAILED"

.field static final EXTREMELY_LOW_BATTERY:I = 0xa

.field static final FIRMWARE_REVISION_CHARACTERISTIC_UUID:Ljava/util/UUID;

.field public static final GATT_READ_DELAY:I = 0x1388

.field static final HID_HOST_READY_UUID:Ljava/util/UUID;

.field static final HID_PROFILE:I = 0x4

.field static final HID_SERVICE_UUID:Ljava/util/UUID;

.field static final INIT_EVENT:Lcom/navdy/hud/app/device/dial/DialConstants$DialManagerInitEvent;

.field static final LOW_BATTERY:I = 0x50

.field static final MAXIMUM_BOND_TIME:I = 0x7530

.field static final MAX_BOND_RETRIES:I = 0x3

.field static final MAX_ENCRYPTION_RETRY:I = 0x2

.field static final MIN_TIME_BETWEEN_EXTREMELY_LOW_WARNINGS:J

.field static final MIN_TIME_BETWEEN_REBOOTS:J

.field static final MIN_TIME_BETWEEN_VERY_LOW_WARNINGS:J

.field static final NAVDY_DIAL_FILTER:[Ljava/lang/String;

.field static final OTA_CONTROL_CHARACTERISTIC_UUID:Ljava/util/UUID;

.field static final OTA_DATA_CHARACTERISTIC_UUID:Ljava/util/UUID;

.field public static final OTA_DIAL_NAME_KEY:Ljava/lang/String; = "OtaDialNameKey"

.field static final OTA_INCREMENTAL_VERSION_CHARACTERISTIC_UUID:Ljava/util/UUID;

.field static final OTA_SERVICE_UUID:Ljava/util/UUID;

.field static final PAIRING_RETRY_DELAY:I = 0xfa0

.field static final PAIRING_RULE:Lcom/navdy/hud/app/device/dial/DialConstants$PairingRule;

.field static final RAW_BATTERY_LEVEL_UUID:Ljava/util/UUID;

.field static final SCAN_HANG_TIME:I = 0x7530

.field static final SYSTEM_TEMPERATURE_UUID:Ljava/util/UUID;

.field public static final VERSION_PREFIX:Ljava/lang/String; = "1.0."

.field static final VERY_LOW_BATTERY:I = 0x32

.field public static final VIDEO_SHOWN_PREF:Ljava/lang/String; = "first_run_dial_video_shown"


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x1e

    .line 30
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialConstants$DialManagerInitEvent;

    invoke-direct {v0}, Lcom/navdy/hud/app/device/dial/DialConstants$DialManagerInitEvent;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialConstants;->INIT_EVENT:Lcom/navdy/hud/app/device/dial/DialConstants$DialManagerInitEvent;

    .line 36
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialConstants$BatteryChangeEvent;

    invoke-direct {v0}, Lcom/navdy/hud/app/device/dial/DialConstants$BatteryChangeEvent;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialConstants;->BATTERY_CHANGE_EVENT:Lcom/navdy/hud/app/device/dial/DialConstants$BatteryChangeEvent;

    .line 58
    const-string v0, "00001812-0000-1000-8000-00805f9b34fb"

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialConstants;->HID_SERVICE_UUID:Ljava/util/UUID;

    .line 59
    const-string v0, "0000180F-0000-1000-8000-00805f9b34fb"

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialConstants;->BATTERY_SERVICE_UUID:Ljava/util/UUID;

    .line 60
    const-string v0, "0000180a-0000-1000-8000-00805f9b34fb"

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialConstants;->DEVICE_INFO_SERVICE_UUID:Ljava/util/UUID;

    .line 61
    const-string v0, "1d14d6ee-fd63-4fa1-bfa4-8f47b42119f0"

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialConstants;->OTA_SERVICE_UUID:Ljava/util/UUID;

    .line 64
    const-string v0, "00002a19-0000-1000-8000-00805f9b34fb"

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialConstants;->BATTERY_LEVEL_UUID:Ljava/util/UUID;

    .line 65
    const-string v0, "00002a6e-0000-1000-8000-00805f9b34fb"

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialConstants;->SYSTEM_TEMPERATURE_UUID:Ljava/util/UUID;

    .line 66
    const-string v0, "40cc0c40-3caa-11e6-b55c-0002a5d5c51c"

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialConstants;->RAW_BATTERY_LEVEL_UUID:Ljava/util/UUID;

    .line 67
    const-string v0, "00002a27-0000-1000-8000-00805f9b34fb"

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialConstants;->DEVICE_INFO_HARDWARE_VERSION_UUID:Ljava/util/UUID;

    .line 68
    const-string v0, "00002a26-0000-1000-8000-00805f9b34fb"

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialConstants;->DEVICE_INFO_FIRMWARE_VERSION_UUID:Ljava/util/UUID;

    .line 69
    const-string v0, "40cc0c40-3caa-11e6-b55c-0002a5d5c51b"

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialConstants;->HID_HOST_READY_UUID:Ljava/util/UUID;

    .line 70
    const-string v0, "f7bf3564-fb6d-4e53-88a4-5e37e0326063"

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialConstants;->OTA_CONTROL_CHARACTERISTIC_UUID:Ljava/util/UUID;

    .line 71
    const-string v0, "984227f3-34fc-4045-a5d0-2c581f81a153"

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialConstants;->OTA_DATA_CHARACTERISTIC_UUID:Ljava/util/UUID;

    .line 72
    const-string v0, "2a9e7517-f977-4775-a993-b827ae0fff34"

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialConstants;->OTA_INCREMENTAL_VERSION_CHARACTERISTIC_UUID:Ljava/util/UUID;

    .line 73
    const-string v0, "00002a26-0000-1000-8000-00805f9b34fb"

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialConstants;->FIRMWARE_REVISION_CHARACTERISTIC_UUID:Ljava/util/UUID;

    .line 74
    const-string v0, "87c72b94-46fc-44ae-bd8d-d307bceb3b9f"

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialConstants;->DIAL_FORGET_KEYS_UUID:Ljava/util/UUID;

    .line 75
    const-string v0, "87c72b00-46fc-44ae-bd8d-d307bceb3b9f"

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialConstants;->DIAL_REBOOT_UUID:Ljava/util/UUID;

    .line 77
    const-string v0, "DIE"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialConstants;->DIAL_FORGET_KEYS_MAGIC:[B

    .line 78
    const-string v0, "REBOOT"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialConstants;->DIAL_REBOOT_MAGIC:[B

    .line 92
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "Navdy Dial"

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialConstants;->NAVDY_DIAL_FILTER:[Ljava/lang/String;

    .line 101
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x7

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/device/dial/DialConstants;->MIN_TIME_BETWEEN_VERY_LOW_WARNINGS:J

    .line 102
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/device/dial/DialConstants;->MIN_TIME_BETWEEN_EXTREMELY_LOW_WARNINGS:J

    .line 109
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialConstants$PairingRule;->FIRST:Lcom/navdy/hud/app/device/dial/DialConstants$PairingRule;

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialConstants;->PAIRING_RULE:Lcom/navdy/hud/app/device/dial/DialConstants$PairingRule;

    .line 119
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/device/dial/DialConstants;->MIN_TIME_BETWEEN_REBOOTS:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
