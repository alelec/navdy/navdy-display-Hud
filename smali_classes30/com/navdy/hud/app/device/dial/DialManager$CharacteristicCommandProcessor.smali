.class Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;
.super Ljava/lang/Object;
.source "DialManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/device/dial/DialManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "CharacteristicCommandProcessor"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor$WriteCommand;,
        Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor$ReadCommand;,
        Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor$Command;
    }
.end annotation


# instance fields
.field private bluetoothGatt:Landroid/bluetooth/BluetoothGatt;

.field private commandsQueue:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor$Command;",
            ">;"
        }
    .end annotation
.end field

.field private handler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/bluetooth/BluetoothGatt;Landroid/os/Handler;)V
    .locals 1
    .param p1, "bluetoothGatt"    # Landroid/bluetooth/BluetoothGatt;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 590
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 591
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;->bluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    .line 592
    iput-object p2, p0, Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;->handler:Landroid/os/Handler;

    .line 593
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;->commandsQueue:Ljava/util/LinkedList;

    .line 594
    return-void
.end method

.method static synthetic access$2500(Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;)Landroid/bluetooth/BluetoothGatt;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;

    .prologue
    .line 544
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;->bluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized commandFinished()V
    .locals 2

    .prologue
    .line 624
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;->commandsQueue:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 625
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;->commandsQueue:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor$Command;

    .line 627
    .local v0, "command":Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor$Command;
    invoke-virtual {p0}, Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;->submitNext()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 629
    .end local v0    # "command":Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor$Command;
    :cond_0
    monitor-exit p0

    return-void

    .line 624
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized process(Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor$Command;)V
    .locals 2
    .param p1, "command"    # Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor$Command;

    .prologue
    .line 597
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;->commandsQueue:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 598
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;->commandsQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 599
    invoke-virtual {p0}, Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;->submitNext()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 601
    :cond_0
    monitor-exit p0

    return-void

    .line 597
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 632
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;->commandsQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 633
    iput-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;->handler:Landroid/os/Handler;

    .line 634
    iput-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;->bluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    .line 635
    return-void
.end method

.method public declared-synchronized submitNext()V
    .locals 3

    .prologue
    .line 604
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;->commandsQueue:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor$Command;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 605
    .local v0, "command":Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor$Command;
    if-nez v0, :cond_0

    .line 621
    :goto_0
    monitor-exit p0

    return-void

    .line 608
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;->handler:Landroid/os/Handler;

    new-instance v2, Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor$1;

    invoke-direct {v2, p0, v0}, Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor$1;-><init>(Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor$Command;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 604
    .end local v0    # "command":Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor$Command;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
