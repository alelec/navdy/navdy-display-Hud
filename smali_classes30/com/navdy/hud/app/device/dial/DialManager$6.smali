.class Lcom/navdy/hud/app/device/dial/DialManager$6;
.super Landroid/content/BroadcastReceiver;
.source "DialManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/device/dial/DialManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/device/dial/DialManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/dial/DialManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 243
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager$6;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 247
    :try_start_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 248
    .local v0, "action":Ljava/lang/String;
    const-string v6, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    .line 249
    .local v1, "device":Landroid/bluetooth/BluetoothDevice;
    if-eqz v1, :cond_0

    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager$6;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->bondingDial:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v6}, Lcom/navdy/hud/app/device/dial/DialManager;->access$000(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v6

    if-nez v6, :cond_1

    .line 327
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "device":Landroid/bluetooth/BluetoothDevice;
    :cond_0
    :goto_0
    return-void

    .line 253
    .restart local v0    # "action":Ljava/lang/String;
    .restart local v1    # "device":Landroid/bluetooth/BluetoothDevice;
    :cond_1
    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager$6;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    invoke-virtual {v6, v1}, Lcom/navdy/hud/app/device/dial/DialManager;->isDialDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 254
    sget-object v6, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[Dial]btBondRecvr: notification not for dial:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " addr:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 255
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 254
    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 324
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "device":Landroid/bluetooth/BluetoothDevice;
    :catch_0
    move-exception v5

    .line 325
    .local v5, "t":Ljava/lang/Throwable;
    sget-object v6, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "[Dial]"

    invoke-virtual {v6, v7, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 259
    .end local v5    # "t":Ljava/lang/Throwable;
    .restart local v0    # "action":Ljava/lang/String;
    .restart local v1    # "device":Landroid/bluetooth/BluetoothDevice;
    :cond_2
    :try_start_1
    sget-object v6, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[Dial]btBondRecvr ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] action["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 261
    const-string v6, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 262
    const-string v6, "android.bluetooth.device.extra.BOND_STATE"

    const/high16 v7, -0x80000000

    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 263
    .local v4, "state":I
    const-string v6, "android.bluetooth.device.extra.PREVIOUS_BOND_STATE"

    const/high16 v7, -0x80000000

    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 264
    .local v3, "prevState":I
    sget-object v6, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[Dial]btBondRecvr: prev state:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " new state:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 265
    const/16 v6, 0xc

    if-ne v4, v6, :cond_5

    .line 266
    sget-object v6, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "btBondRecvr removed hang runnable"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 267
    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager$6;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->handler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/navdy/hud/app/device/dial/DialManager;->access$400(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/os/Handler;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/device/dial/DialManager$6;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->bondHangRunnable:Ljava/lang/Runnable;
    invoke-static {v7}, Lcom/navdy/hud/app/device/dial/DialManager;->access$600(Lcom/navdy/hud/app/device/dial/DialManager;)Ljava/lang/Runnable;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 268
    sget-object v6, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "[Dial]btBondRecvr: got pairing event"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 271
    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager$6;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->bondingDial:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v6}, Lcom/navdy/hud/app/device/dial/DialManager;->access$000(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v6

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager$6;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->bondingDial:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v6}, Lcom/navdy/hud/app/device/dial/DialManager;->access$000(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v6

    invoke-virtual {v6, v1}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 273
    sget-object v6, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[Dial]btBondRecvr: bonding active bondDial["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/navdy/hud/app/device/dial/DialManager$6;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->bondingDial:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v8}, Lcom/navdy/hud/app/device/dial/DialManager;->access$000(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] device["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 274
    sget-object v6, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "btBondRecvr:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "state is:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 275
    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager$6;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # invokes: Lcom/navdy/hud/app/device/dial/DialManager;->addtoBondList(Landroid/bluetooth/BluetoothDevice;)Z
    invoke-static {v6, v1}, Lcom/navdy/hud/app/device/dial/DialManager;->access$700(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothDevice;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 276
    sget-object v6, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "btBondRecvr: added to bond list"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 280
    :goto_1
    sget-object v6, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "btConnRecvr: attempt connection reqd"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 281
    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager$6;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    const/4 v7, 0x0

    # setter for: Lcom/navdy/hud/app/device/dial/DialManager;->bondingDial:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v6, v7}, Lcom/navdy/hud/app/device/dial/DialManager;->access$002(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;

    .line 282
    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager$6;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v6}, Lcom/navdy/hud/app/device/dial/DialManager;->access$800(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v6

    new-instance v7, Lcom/navdy/hud/app/device/dial/DialManager$6$1;

    invoke-direct {v7, p0, v1}, Lcom/navdy/hud/app/device/dial/DialManager$6$1;-><init>(Lcom/navdy/hud/app/device/dial/DialManager$6;Landroid/bluetooth/BluetoothDevice;)V

    invoke-static {v6, v1, v7}, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->connectToDial(Landroid/bluetooth/BluetoothAdapter;Landroid/bluetooth/BluetoothDevice;Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;)V

    goto/16 :goto_0

    .line 278
    :cond_3
    sget-object v6, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "btConnRecvr: already in bond list"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_1

    .line 292
    :cond_4
    sget-object v6, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[Dial]no bonding active bondDial["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/navdy/hud/app/device/dial/DialManager$6;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->bondingDial:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v8}, Lcom/navdy/hud/app/device/dial/DialManager;->access$000(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] device["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 295
    :cond_5
    const/16 v6, 0xa

    if-ne v4, v6, :cond_0

    .line 296
    sget-object v6, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[Dial]btBondRecvr: not paired tries["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/navdy/hud/app/device/dial/DialManager$6;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->bondTries:I
    invoke-static {v8}, Lcom/navdy/hud/app/device/dial/DialManager;->access$900(Lcom/navdy/hud/app/device/dial/DialManager;)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 297
    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager$6;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->bondTries:I
    invoke-static {v6}, Lcom/navdy/hud/app/device/dial/DialManager;->access$900(Lcom/navdy/hud/app/device/dial/DialManager;)I

    move-result v6

    const/4 v7, 0x3

    if-ne v6, v7, :cond_7

    .line 298
    sget-object v6, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "btBondRecvr removed runnable"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 299
    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager$6;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->handler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/navdy/hud/app/device/dial/DialManager;->access$400(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/os/Handler;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/device/dial/DialManager$6;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->bondHangRunnable:Ljava/lang/Runnable;
    invoke-static {v7}, Lcom/navdy/hud/app/device/dial/DialManager;->access$600(Lcom/navdy/hud/app/device/dial/DialManager;)Ljava/lang/Runnable;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 300
    const/4 v2, 0x0

    .line 301
    .local v2, "dialName":Ljava/lang/String;
    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager$6;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->bondingDial:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v6}, Lcom/navdy/hud/app/device/dial/DialManager;->access$000(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v6

    if-eqz v6, :cond_6

    .line 302
    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager$6;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->bondingDial:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v6}, Lcom/navdy/hud/app/device/dial/DialManager;->access$000(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v6

    invoke-virtual {v6}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v2

    .line 304
    :cond_6
    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager$6;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    const/4 v7, 0x0

    # setter for: Lcom/navdy/hud/app/device/dial/DialManager;->bondingDial:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v6, v7}, Lcom/navdy/hud/app/device/dial/DialManager;->access$002(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;

    .line 305
    sget-object v6, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "[Dial]btBondRecvr: giving up"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 306
    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->DIAL_NOT_CONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->access$100()Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

    move-result-object v6

    iput-object v2, v6, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;->dialName:Ljava/lang/String;

    .line 307
    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager$6;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v6}, Lcom/navdy/hud/app/device/dial/DialManager;->access$200(Lcom/navdy/hud/app/device/dial/DialManager;)Lcom/squareup/otto/Bus;

    move-result-object v6

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->DIAL_NOT_CONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->access$100()Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 309
    .end local v2    # "dialName":Ljava/lang/String;
    :cond_7
    sget-object v6, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "[Dial]btBondRecvr: trying to bond again"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 310
    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager$6;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # operator++ for: Lcom/navdy/hud/app/device/dial/DialManager;->bondTries:I
    invoke-static {v6}, Lcom/navdy/hud/app/device/dial/DialManager;->access$908(Lcom/navdy/hud/app/device/dial/DialManager;)I

    .line 311
    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager$6;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->handler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/navdy/hud/app/device/dial/DialManager;->access$400(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/os/Handler;

    move-result-object v6

    new-instance v7, Lcom/navdy/hud/app/device/dial/DialManager$6$2;

    invoke-direct {v7, p0, v1}, Lcom/navdy/hud/app/device/dial/DialManager$6$2;-><init>(Lcom/navdy/hud/app/device/dial/DialManager$6;Landroid/bluetooth/BluetoothDevice;)V

    const-wide/16 v8, 0xfa0

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
