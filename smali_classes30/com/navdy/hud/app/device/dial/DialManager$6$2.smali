.class Lcom/navdy/hud/app/device/dial/DialManager$6$2;
.super Ljava/lang/Object;
.source "DialManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/device/dial/DialManager$6;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/device/dial/DialManager$6;

.field final synthetic val$device:Landroid/bluetooth/BluetoothDevice;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/dial/DialManager$6;Landroid/bluetooth/BluetoothDevice;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/device/dial/DialManager$6;

    .prologue
    .line 311
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager$6$2;->this$1:Lcom/navdy/hud/app/device/dial/DialManager$6;

    iput-object p2, p0, Lcom/navdy/hud/app/device/dial/DialManager$6$2;->val$device:Landroid/bluetooth/BluetoothDevice;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 314
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "btBondRecvr removed runnable"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 315
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager$6$2;->this$1:Lcom/navdy/hud/app/device/dial/DialManager$6;

    iget-object v0, v0, Lcom/navdy/hud/app/device/dial/DialManager$6;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/device/dial/DialManager;->access$400(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager$6$2;->this$1:Lcom/navdy/hud/app/device/dial/DialManager$6;

    iget-object v1, v1, Lcom/navdy/hud/app/device/dial/DialManager$6;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->bondHangRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/navdy/hud/app/device/dial/DialManager;->access$600(Lcom/navdy/hud/app/device/dial/DialManager;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 316
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager$6$2;->val$device:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->createBond()Z

    .line 317
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager$6$2;->this$1:Lcom/navdy/hud/app/device/dial/DialManager$6;

    iget-object v0, v0, Lcom/navdy/hud/app/device/dial/DialManager$6;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/device/dial/DialManager;->access$400(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager$6$2;->this$1:Lcom/navdy/hud/app/device/dial/DialManager$6;

    iget-object v1, v1, Lcom/navdy/hud/app/device/dial/DialManager$6;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->bondHangRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/navdy/hud/app/device/dial/DialManager;->access$600(Lcom/navdy/hud/app/device/dial/DialManager;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 318
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "btBondRecvr posted runnable"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 319
    return-void
.end method
