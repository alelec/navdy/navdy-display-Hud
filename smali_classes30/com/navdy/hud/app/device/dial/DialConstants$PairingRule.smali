.class final enum Lcom/navdy/hud/app/device/dial/DialConstants$PairingRule;
.super Ljava/lang/Enum;
.source "DialConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/device/dial/DialConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "PairingRule"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/device/dial/DialConstants$PairingRule;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/device/dial/DialConstants$PairingRule;

.field public static final enum FIRST:Lcom/navdy/hud/app/device/dial/DialConstants$PairingRule;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 14
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialConstants$PairingRule;

    const-string v1, "FIRST"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/device/dial/DialConstants$PairingRule;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialConstants$PairingRule;->FIRST:Lcom/navdy/hud/app/device/dial/DialConstants$PairingRule;

    .line 12
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/navdy/hud/app/device/dial/DialConstants$PairingRule;

    sget-object v1, Lcom/navdy/hud/app/device/dial/DialConstants$PairingRule;->FIRST:Lcom/navdy/hud/app/device/dial/DialConstants$PairingRule;

    aput-object v1, v0, v2

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialConstants$PairingRule;->$VALUES:[Lcom/navdy/hud/app/device/dial/DialConstants$PairingRule;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/device/dial/DialConstants$PairingRule;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 12
    const-class v0, Lcom/navdy/hud/app/device/dial/DialConstants$PairingRule;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/device/dial/DialConstants$PairingRule;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/device/dial/DialConstants$PairingRule;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialConstants$PairingRule;->$VALUES:[Lcom/navdy/hud/app/device/dial/DialConstants$PairingRule;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/device/dial/DialConstants$PairingRule;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/device/dial/DialConstants$PairingRule;

    return-object v0
.end method
