.class Lcom/navdy/hud/app/device/dial/DialManager$1;
.super Ljava/lang/Object;
.source "DialManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/device/dial/DialManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/device/dial/DialManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/dial/DialManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 172
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 176
    :try_start_0
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "scan hang runnable"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 177
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/device/dial/DialManager;->stopScan(Z)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 181
    :goto_0
    return-void

    .line 178
    :catch_0
    move-exception v0

    .line 179
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "[Dial]"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
