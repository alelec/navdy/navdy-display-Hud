.class public Lcom/navdy/hud/app/device/dial/DialNotification;
.super Ljava/lang/Object;
.source "DialNotification.java"


# static fields
.field private static final BATTERY_TOASTS:[Ljava/lang/String;

.field private static final DIAL_BATTERY_TIMEOUT:I = 0x7d0

.field private static final DIAL_BATTERY_TIMEOUT_EXTREMELY_LOW:I = 0x4e20

.field public static final DIAL_CONNECTED_TIMEOUT:I = 0x1388

.field public static final DIAL_CONNECT_ID:Ljava/lang/String; = "dial-connect"

.field private static final DIAL_DISCONNECTED_TIMEOUT:I = 0x3e8

.field public static final DIAL_DISCONNECT_ID:Ljava/lang/String; = "dial-disconnect"

.field public static final DIAL_EXTREMELY_LOW_BATTERY_ID:Ljava/lang/String; = "dial-exlow-battery"

.field public static final DIAL_FORGOTTEN_ID:Ljava/lang/String; = "dial-forgotten"

.field private static final DIAL_FORGOTTEN_TIMEOUT:I = 0x7d0

.field public static final DIAL_LOW_BATTERY_ID:Ljava/lang/String; = "dial-low-battery"

.field private static final DIAL_TOASTS:[Ljava/lang/String;

.field public static final DIAL_VERY_LOW_BATTERY_ID:Ljava/lang/String; = "dial-vlow-battery"

.field private static final EMPTY:Ljava/lang/String; = ""

.field private static final battery_unknown:Ljava/lang/String;

.field private static final connected:Ljava/lang/String;

.field private static final disconnected:Ljava/lang/String;

.field private static final forgotten:Ljava/lang/String;

.field private static final forgotten_plural:Ljava/lang/String;

.field private static resources:Landroid/content/res/Resources;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 23
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/device/dial/DialNotification;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 40
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "dial-low-battery"

    aput-object v1, v0, v2

    const-string v1, "dial-vlow-battery"

    aput-object v1, v0, v3

    const-string v1, "dial-exlow-battery"

    aput-object v1, v0, v4

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialNotification;->BATTERY_TOASTS:[Ljava/lang/String;

    .line 46
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "dial-connect"

    aput-object v1, v0, v2

    const-string v1, "dial-disconnect"

    aput-object v1, v0, v3

    const-string v1, "dial-forgotten"

    aput-object v1, v0, v4

    const-string v1, "dial-low-battery"

    aput-object v1, v0, v5

    const/4 v1, 0x4

    const-string v2, "dial-vlow-battery"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "dial-exlow-battery"

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialNotification;->DIAL_TOASTS:[Ljava/lang/String;

    .line 65
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialNotification;->resources:Landroid/content/res/Resources;

    .line 66
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialNotification;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0900ac

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialNotification;->disconnected:Ljava/lang/String;

    .line 67
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialNotification;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0900c9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialNotification;->connected:Ljava/lang/String;

    .line 68
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialNotification;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0900c1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialNotification;->forgotten:Ljava/lang/String;

    .line 69
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialNotification;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0900c2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialNotification;->forgotten_plural:Ljava/lang/String;

    .line 70
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialNotification;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090215

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialNotification;->battery_unknown:Ljava/lang/String;

    .line 71
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static dismissAllBatteryToasts()V
    .locals 2

    .prologue
    .line 198
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v0

    .line 199
    .local v0, "toastManager":Lcom/navdy/hud/app/framework/toast/ToastManager;
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialNotification;->BATTERY_TOASTS:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->dismissCurrentToast([Ljava/lang/String;)V

    .line 200
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialNotification;->BATTERY_TOASTS:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->clearPendingToast([Ljava/lang/String;)V

    .line 201
    return-void
.end method

.method public static getDialAddressPart(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "dialName"    # Ljava/lang/String;

    .prologue
    .line 223
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 224
    const-string v1, "("

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 225
    .local v0, "index":I
    if-ltz v0, :cond_0

    .line 226
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 227
    const-string v1, ")"

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 228
    if-ltz v0, :cond_0

    .line 229
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 234
    .end local v0    # "index":I
    :cond_0
    return-object p0
.end method

.method public static getDialBatteryLevel()Ljava/lang/String;
    .locals 2

    .prologue
    .line 238
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->getInstance()Lcom/navdy/hud/app/device/dial/DialManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/device/dial/DialManager;->getLastKnownBatteryLevel()I

    move-result v0

    .line 239
    .local v0, "level":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 240
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialNotification;->battery_unknown:Ljava/lang/String;

    .line 242
    :goto_0
    return-object v1

    :cond_0
    invoke-static {v0}, Lcom/navdy/hud/app/device/dial/DialManager;->getDisplayBatteryLevel(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static getDialName()Ljava/lang/String;
    .locals 7

    .prologue
    .line 204
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->getInstance()Lcom/navdy/hud/app/device/dial/DialManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/device/dial/DialManager;->getDialDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    .line 205
    .local v0, "device":Landroid/bluetooth/BluetoothDevice;
    const/4 v1, 0x0

    .line 207
    .local v1, "dialName":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 208
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v1

    .line 209
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 210
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    .line 211
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    .line 212
    .local v2, "len":I
    const/4 v3, 0x4

    if-le v2, v3, :cond_0

    .line 213
    add-int/lit8 v3, v2, -0x4

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 215
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0900d1

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 219
    .end local v2    # "len":I
    :cond_1
    return-object v1
.end method

.method public static showConnectedToast()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 74
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 75
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "13"

    const/16 v3, 0x1388

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 76
    const-string v2, "8"

    const v3, 0x7f0200fc

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 77
    const-string v2, "11"

    const v3, 0x7f0201e6

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 78
    const-string v2, "4"

    sget-object v3, Lcom/navdy/hud/app/device/dial/DialNotification;->resources:Landroid/content/res/Resources;

    const v4, 0x7f0901cd

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    const-string v2, "5"

    const v3, 0x7f0c000a

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 81
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialNotification;->getDialName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/navdy/hud/app/device/dial/DialNotification;->getDialAddressPart(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 82
    .local v1, "dialName":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 83
    const-string v2, "6"

    sget-object v3, Lcom/navdy/hud/app/device/dial/DialNotification;->resources:Landroid/content/res/Resources;

    const v4, 0x7f0901ce

    new-array v5, v7, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    :cond_0
    const-string v2, "17"

    sget-object v3, Lcom/navdy/hud/app/framework/voice/TTSUtils;->TTS_DIAL_CONNECTED:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    const-string v2, "dial-connect"

    invoke-static {v2, v0, v7}, Lcom/navdy/hud/app/device/dial/DialNotification;->showDialToast(Ljava/lang/String;Landroid/os/Bundle;Z)V

    .line 89
    return-void
.end method

.method private static showDialToast(Ljava/lang/String;Landroid/os/Bundle;Z)V
    .locals 10
    .param p0, "toastId"    # Ljava/lang/String;
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "removeOnDisable"    # Z

    .prologue
    const/4 v5, 0x0

    .line 181
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v8

    .line 182
    .local v8, "toastManager":Lcom/navdy/hud/app/framework/toast/ToastManager;
    invoke-virtual {v8}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getCurrentToastId()Ljava/lang/String;

    move-result-object v6

    .line 183
    .local v6, "currentToastId":Ljava/lang/String;
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialNotification;->DIAL_TOASTS:[Ljava/lang/String;

    invoke-virtual {v8, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->clearPendingToast([Ljava/lang/String;)V

    .line 185
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialNotification;->DIAL_TOASTS:[Ljava/lang/String;

    array-length v2, v1

    move v0, v5

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v7, v1, v0

    .line 186
    .local v7, "s":Ljava/lang/String;
    invoke-static {v7, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 185
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 189
    :cond_0
    invoke-virtual {v8, v7}, Lcom/navdy/hud/app/framework/toast/ToastManager;->dismissCurrentToast(Ljava/lang/String;)V

    goto :goto_1

    .line 192
    .end local v7    # "s":Ljava/lang/String;
    :cond_1
    invoke-static {p0, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 193
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v9

    new-instance v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    const/4 v3, 0x0

    move-object v1, p0

    move-object v2, p1

    move v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;-><init>(Ljava/lang/String;Landroid/os/Bundle;Lcom/navdy/hud/app/framework/toast/IToastCallback;ZZ)V

    invoke-virtual {v9, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->addToast(Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;)V

    .line 195
    :cond_2
    return-void
.end method

.method public static showDisconnectedToast(Ljava/lang/String;)V
    .locals 5
    .param p0, "dialName"    # Ljava/lang/String;

    .prologue
    .line 92
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v2

    .line 93
    .local v2, "uiStateManager":Lcom/navdy/hud/app/ui/framework/UIStateManager;
    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getCurrentScreen()Lcom/navdy/hud/app/screen/BaseScreen;

    move-result-object v1

    .line 94
    .local v1, "screen":Lcom/navdy/hud/app/screen/BaseScreen;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/navdy/hud/app/screen/BaseScreen;->getScreen()Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v3

    sget-object v4, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DIAL_PAIRING:Lcom/navdy/service/library/events/ui/Screen;

    if-ne v3, v4, :cond_0

    .line 95
    sget-object v3, Lcom/navdy/hud/app/device/dial/DialNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "not showing dial disconnected toast"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 112
    :goto_0
    return-void

    .line 98
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 99
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v3, "13"

    const/16 v4, 0x3e8

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 100
    const-string v3, "8"

    const v4, 0x7f0200ff

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 101
    const-string v3, "11"

    const v4, 0x7f0201e4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 102
    const-string v3, "4"

    sget-object v4, Lcom/navdy/hud/app/device/dial/DialNotification;->disconnected:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    const-string v3, "5"

    const v4, 0x7f0c0009

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 105
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 106
    const-string v3, "6"

    invoke-virtual {v0, v3, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    :cond_1
    const-string v3, "17"

    sget-object v4, Lcom/navdy/hud/app/framework/voice/TTSUtils;->TTS_DIAL_DISCONNECTED:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    const-string v3, "dial-disconnect"

    const/4 v4, 0x1

    invoke-static {v3, v0, v4}, Lcom/navdy/hud/app/device/dial/DialNotification;->showDialToast(Ljava/lang/String;Landroid/os/Bundle;Z)V

    goto :goto_0
.end method

.method public static showExtremelyLowBatteryToast()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 166
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 167
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "13"

    const/16 v3, 0x4e20

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 168
    const-string v2, "8"

    const v3, 0x7f02020e

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 170
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialNotification;->getDialBatteryLevel()Ljava/lang/String;

    move-result-object v1

    .line 171
    .local v1, "level":Ljava/lang/String;
    const-string v2, "4"

    sget-object v3, Lcom/navdy/hud/app/device/dial/DialNotification;->resources:Landroid/content/res/Resources;

    const v4, 0x7f0900ba

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v1, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    const-string v2, "5"

    const v3, 0x7f0c0009

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 174
    const-string v2, "12"

    invoke-virtual {v0, v2, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 175
    const-string v2, "17"

    sget-object v3, Lcom/navdy/hud/app/framework/voice/TTSUtils;->TTS_DIAL_BATTERY_EXTREMELY_LOW:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    const-string v2, "dial-exlow-battery"

    invoke-static {v2, v0, v6}, Lcom/navdy/hud/app/device/dial/DialNotification;->showDialToast(Ljava/lang/String;Landroid/os/Bundle;Z)V

    .line 178
    return-void
.end method

.method public static showForgottenToast(ZLjava/lang/String;)V
    .locals 4
    .param p0, "plural"    # Z
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 115
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 116
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "13"

    const/16 v3, 0x7d0

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 117
    const-string v2, "8"

    const v3, 0x7f0200ff

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 118
    const-string v2, "11"

    const v3, 0x7f0201e4

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 119
    if-eqz p0, :cond_1

    .line 120
    const-string v2, "4"

    sget-object v3, Lcom/navdy/hud/app/device/dial/DialNotification;->forgotten_plural:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    :goto_0
    move-object v1, p1

    .line 126
    .local v1, "dialName":Ljava/lang/String;
    if-nez p0, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 127
    const-string v2, "6"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    const-string v2, "7"

    const v3, 0x7f0c002e

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 131
    :cond_0
    const-string v2, "5"

    const v3, 0x7f0c0009

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 132
    const-string v2, "17"

    sget-object v3, Lcom/navdy/hud/app/framework/voice/TTSUtils;->TTS_DIAL_FORGOTTEN:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    const-string v2, "dial-forgotten"

    const/4 v3, 0x1

    invoke-static {v2, v0, v3}, Lcom/navdy/hud/app/device/dial/DialNotification;->showDialToast(Ljava/lang/String;Landroid/os/Bundle;Z)V

    .line 135
    return-void

    .line 122
    .end local v1    # "dialName":Ljava/lang/String;
    :cond_1
    const-string v2, "4"

    sget-object v3, Lcom/navdy/hud/app/device/dial/DialNotification;->forgotten:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static showLowBatteryToast()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 138
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 139
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "13"

    const/16 v3, 0x7d0

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 140
    const-string v2, "8"

    const v3, 0x7f02020d

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 142
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialNotification;->getDialBatteryLevel()Ljava/lang/String;

    move-result-object v1

    .line 143
    .local v1, "level":Ljava/lang/String;
    const-string v2, "4"

    sget-object v3, Lcom/navdy/hud/app/device/dial/DialNotification;->resources:Landroid/content/res/Resources;

    const v4, 0x7f0900c3

    new-array v5, v7, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    const-string v2, "5"

    const v3, 0x7f0c0009

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 146
    const-string v2, "17"

    sget-object v3, Lcom/navdy/hud/app/framework/voice/TTSUtils;->TTS_DIAL_BATTERY_LOW:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    const-string v2, "dial-low-battery"

    invoke-static {v2, v0, v7}, Lcom/navdy/hud/app/device/dial/DialNotification;->showDialToast(Ljava/lang/String;Landroid/os/Bundle;Z)V

    .line 149
    return-void
.end method

.method public static showVeryLowBatteryToast()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 152
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 153
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "13"

    const/16 v3, 0x7d0

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 154
    const-string v2, "8"

    const v3, 0x7f02020d

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 156
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialNotification;->getDialBatteryLevel()Ljava/lang/String;

    move-result-object v1

    .line 157
    .local v1, "level":Ljava/lang/String;
    const-string v2, "4"

    sget-object v3, Lcom/navdy/hud/app/device/dial/DialNotification;->resources:Landroid/content/res/Resources;

    const v4, 0x7f0900c3

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v1, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    const-string v2, "5"

    const v3, 0x7f0c0009

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 160
    const-string v2, "17"

    sget-object v3, Lcom/navdy/hud/app/framework/voice/TTSUtils;->TTS_DIAL_BATTERY_VERY_LOW:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    const-string v2, "dial-vlow-battery"

    invoke-static {v2, v0, v6}, Lcom/navdy/hud/app/device/dial/DialNotification;->showDialToast(Ljava/lang/String;Landroid/os/Bundle;Z)V

    .line 163
    return-void
.end method
