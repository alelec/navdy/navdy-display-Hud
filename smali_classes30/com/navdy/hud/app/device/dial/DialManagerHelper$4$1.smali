.class Lcom/navdy/hud/app/device/dial/DialManagerHelper$4$1;
.super Ljava/lang/Object;
.source "DialManagerHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;->onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;

.field final synthetic val$proxy:Landroid/bluetooth/BluetoothProfile;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;Landroid/bluetooth/BluetoothProfile;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;

    iput-object p2, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4$1;->val$proxy:Landroid/bluetooth/BluetoothProfile;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 122
    :try_start_0
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4$1;->val$proxy:Landroid/bluetooth/BluetoothProfile;

    iget-object v4, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;

    iget-object v4, v4, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;->val$device:Landroid/bluetooth/BluetoothDevice;

    invoke-interface {v3, v4}, Landroid/bluetooth/BluetoothProfile;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    move-result v1

    .line 123
    .local v1, "state":I
    # getter for: Lcom/navdy/hud/app/device/dial/DialManagerHelper;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[Dial]connectToDial Device "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;

    iget-object v5, v5, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;->val$device:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v5}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " state is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 124
    invoke-static {v1}, Lcom/navdy/hud/app/bluetooth/utils/BluetoothUtils;->getConnectedState(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 123
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 126
    const/4 v3, 0x2

    if-ne v1, v3, :cond_1

    .line 127
    # getter for: Lcom/navdy/hud/app/device/dial/DialManagerHelper;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[Dial]connectToDial already connected:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;

    iget-object v5, v5, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;->val$device:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 128
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;

    iget-boolean v3, v3, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;->eventSent:Z

    if-nez v3, :cond_0

    .line 129
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;

    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;->eventSent:Z

    .line 130
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;

    iget-object v3, v3, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;->val$callBack:Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;

    const/4 v4, 0x1

    invoke-interface {v3, v4}, Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;->onAttemptConnection(Z)V

    .line 155
    .end local v1    # "state":I
    :cond_0
    :goto_0
    return-void

    .line 133
    .restart local v1    # "state":I
    :cond_1
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4$1;->val$proxy:Landroid/bluetooth/BluetoothProfile;

    iget-object v4, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;

    iget-object v4, v4, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;->val$device:Landroid/bluetooth/BluetoothDevice;

    # invokes: Lcom/navdy/hud/app/device/dial/DialManagerHelper;->forceConnect(Landroid/bluetooth/BluetoothProfile;Landroid/bluetooth/BluetoothDevice;)Z
    invoke-static {v3, v4}, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->access$100(Landroid/bluetooth/BluetoothProfile;Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    .line 134
    .local v0, "ret":Z
    if-nez v0, :cond_2

    .line 135
    # getter for: Lcom/navdy/hud/app/device/dial/DialManagerHelper;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "[Dial]connectToDial proxy-connect to hid device failed"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 136
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;

    iget-boolean v3, v3, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;->eventSent:Z

    if-nez v3, :cond_0

    .line 137
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;

    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;->eventSent:Z

    .line 138
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;

    iget-object v3, v3, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;->val$callBack:Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;->onAttemptConnection(Z)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 148
    .end local v0    # "ret":Z
    .end local v1    # "state":I
    :catch_0
    move-exception v2

    .line 149
    .local v2, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/device/dial/DialManagerHelper;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "[Dial] connectToDial"

    invoke-virtual {v3, v4, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 150
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;

    iget-boolean v3, v3, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;->eventSent:Z

    if-nez v3, :cond_0

    .line 151
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;

    iput-boolean v6, v3, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;->eventSent:Z

    .line 152
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;

    iget-object v3, v3, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;->val$callBack:Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;

    invoke-interface {v3, v7}, Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;->onAttemptConnection(Z)V

    goto :goto_0

    .line 141
    .end local v2    # "t":Ljava/lang/Throwable;
    .restart local v0    # "ret":Z
    .restart local v1    # "state":I
    :cond_2
    :try_start_1
    # getter for: Lcom/navdy/hud/app/device/dial/DialManagerHelper;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "[Dial]connectToDial proxy-connect to hid device successful"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 142
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;

    iget-boolean v3, v3, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;->eventSent:Z

    if-nez v3, :cond_0

    .line 143
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;

    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;->eventSent:Z

    .line 144
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;

    iget-object v3, v3, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;->val$callBack:Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;

    const/4 v4, 0x1

    invoke-interface {v3, v4}, Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;->onAttemptConnection(Z)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
