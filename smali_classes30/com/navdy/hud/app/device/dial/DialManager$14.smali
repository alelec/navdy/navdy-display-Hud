.class Lcom/navdy/hud/app/device/dial/DialManager$14;
.super Ljava/lang/Object;
.source "DialManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/device/dial/DialManager;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/device/dial/DialManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/dial/DialManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 933
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager$14;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 936
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager$14;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->dialManagerInitialized:Z
    invoke-static {v0}, Lcom/navdy/hud/app/device/dial/DialManager;->access$1000(Lcom/navdy/hud/app/device/dial/DialManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 948
    :goto_0
    return-void

    .line 941
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager$14;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # invokes: Lcom/navdy/hud/app/device/dial/DialManager;->buildDialList()V
    invoke-static {v0}, Lcom/navdy/hud/app/device/dial/DialManager;->access$4200(Lcom/navdy/hud/app/device/dial/DialManager;)V

    .line 944
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager$14;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # invokes: Lcom/navdy/hud/app/device/dial/DialManager;->checkDialConnections()V
    invoke-static {v0}, Lcom/navdy/hud/app/device/dial/DialManager;->access$4300(Lcom/navdy/hud/app/device/dial/DialManager;)V

    .line 945
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager$14;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    const/4 v1, 0x1

    # setter for: Lcom/navdy/hud/app/device/dial/DialManager;->dialManagerInitialized:Z
    invoke-static {v0, v1}, Lcom/navdy/hud/app/device/dial/DialManager;->access$1002(Lcom/navdy/hud/app/device/dial/DialManager;Z)Z

    .line 946
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager$14;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v0}, Lcom/navdy/hud/app/device/dial/DialManager;->access$200(Lcom/navdy/hud/app/device/dial/DialManager;)Lcom/squareup/otto/Bus;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/device/dial/DialConstants;->INIT_EVENT:Lcom/navdy/hud/app/device/dial/DialConstants$DialManagerInitEvent;

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 947
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "dial manager initialized"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method
