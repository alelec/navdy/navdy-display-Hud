.class Lcom/navdy/hud/app/device/dial/DialManager$17;
.super Ljava/lang/Object;
.source "DialManager.java"

# interfaces
.implements Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialForgotten;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/device/dial/DialManager;->bondWithDial(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/le/ScanRecord;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/device/dial/DialManager;

.field final synthetic val$device:Landroid/bluetooth/BluetoothDevice;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothDevice;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 1182
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager$17;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    iput-object p2, p0, Lcom/navdy/hud/app/device/dial/DialManager$17;->val$device:Landroid/bluetooth/BluetoothDevice;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onForgotten()V
    .locals 5

    .prologue
    .line 1185
    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager$17;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->bondedDials:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/navdy/hud/app/device/dial/DialManager;->access$4400(Lcom/navdy/hud/app/device/dial/DialManager;)Ljava/util/ArrayList;

    move-result-object v3

    monitor-enter v3

    .line 1186
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager$17;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->bondedDials:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/navdy/hud/app/device/dial/DialManager;->access$4400(Lcom/navdy/hud/app/device/dial/DialManager;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1187
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/bluetooth/BluetoothDevice;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1188
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 1189
    .local v0, "d":Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/navdy/hud/app/device/dial/DialManager$17;->val$device:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1190
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 1194
    .end local v0    # "d":Landroid/bluetooth/BluetoothDevice;
    :cond_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1195
    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager$17;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    const/4 v3, 0x0

    # setter for: Lcom/navdy/hud/app/device/dial/DialManager;->bondTries:I
    invoke-static {v2, v3}, Lcom/navdy/hud/app/device/dial/DialManager;->access$902(Lcom/navdy/hud/app/device/dial/DialManager;I)I

    .line 1196
    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->DIAL_NOT_CONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->access$100()Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManager$17;->val$device:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;->dialName:Ljava/lang/String;

    .line 1197
    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager$17;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v2}, Lcom/navdy/hud/app/device/dial/DialManager;->access$200(Lcom/navdy/hud/app/device/dial/DialManager;)Lcom/squareup/otto/Bus;

    move-result-object v2

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->DIAL_NOT_CONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->access$100()Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 1198
    return-void

    .line 1194
    .end local v1    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/bluetooth/BluetoothDevice;>;"
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method
