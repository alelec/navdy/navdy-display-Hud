.class Lcom/navdy/hud/app/device/dial/DialManagerHelper$4$2;
.super Ljava/lang/Object;
.source "DialManagerHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;->onServiceDisconnected(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;

    .prologue
    .line 161
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4$2;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 165
    :try_start_0
    # getter for: Lcom/navdy/hud/app/device/dial/DialManagerHelper;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "[Dial]connectToDial onServiceDisconnected: could not connect proxy"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 166
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4$2;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;

    iget-boolean v1, v1, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;->eventSent:Z

    if-nez v1, :cond_0

    .line 167
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4$2;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;->eventSent:Z

    .line 168
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4$2;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;

    iget-object v1, v1, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;->val$callBack:Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;->onAttemptConnection(Z)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 173
    :cond_0
    :goto_0
    return-void

    .line 170
    :catch_0
    move-exception v0

    .line 171
    .local v0, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/device/dial/DialManagerHelper;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "[Dial]"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
