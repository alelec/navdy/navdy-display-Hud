.class Lcom/navdy/hud/app/device/dial/DialManager$21;
.super Ljava/lang/Object;
.source "DialManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/device/dial/DialManager;->startDialEventListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/device/dial/DialManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/dial/DialManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 1681
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager$21;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    .prologue
    const/4 v12, 0x0

    .line 1684
    const/4 v7, 0x0

    .line 1686
    .local v7, "socket":Ljava/net/DatagramSocket;
    :try_start_0
    sget-object v11, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v13, "DialEventsListener thread enter"

    invoke-virtual {v11, v13}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1687
    new-instance v8, Ljava/net/DatagramSocket;

    const/16 v11, 0x5c66

    const-string v13, "127.0.0.1"

    invoke-static {v13}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v13

    invoke-direct {v8, v11, v13}, Ljava/net/DatagramSocket;-><init>(ILjava/net/InetAddress;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2

    .line 1688
    .end local v7    # "socket":Ljava/net/DatagramSocket;
    .local v8, "socket":Ljava/net/DatagramSocket;
    const/16 v11, 0x38

    :try_start_1
    new-array v2, v11, [B

    .line 1689
    .local v2, "data":[B
    new-instance v6, Ljava/net/DatagramPacket;

    array-length v11, v2

    invoke-direct {v6, v2, v11}, Ljava/net/DatagramPacket;-><init>([BI)V

    .line 1691
    .local v6, "packet":Ljava/net/DatagramPacket;
    :goto_0
    invoke-virtual {v8, v6}, Ljava/net/DatagramSocket;->receive(Ljava/net/DatagramPacket;)V

    .line 1692
    invoke-virtual {v6}, Ljava/net/DatagramPacket;->getLength()I

    move-result v5

    .line 1693
    .local v5, "len":I
    new-instance v9, Ljava/lang/String;

    const/4 v11, 0x0

    invoke-direct {v9, v2, v11, v5}, Ljava/lang/String;-><init>([BII)V

    .line 1694
    .local v9, "str":Ljava/lang/String;
    sget-object v11, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "[DialEventsListener] ["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "]"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 1697
    :try_start_2
    const-string v11, ","

    invoke-virtual {v9, v11}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 1698
    .local v4, "index":I
    if-gez v4, :cond_1

    .line 1699
    sget-object v11, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v13, "[DialEventsListener] invalid data"

    invoke-virtual {v11, v13}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 1735
    .end local v4    # "index":I
    :catch_0
    move-exception v10

    .line 1736
    .local v10, "t":Ljava/lang/Throwable;
    :try_start_3
    sget-object v11, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v13, "[DialEventsListener]"

    invoke-virtual {v11, v13, v10}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 1739
    .end local v2    # "data":[B
    .end local v5    # "len":I
    .end local v6    # "packet":Ljava/net/DatagramPacket;
    .end local v9    # "str":Ljava/lang/String;
    .end local v10    # "t":Ljava/lang/Throwable;
    :catch_1
    move-exception v10

    move-object v7, v8

    .line 1740
    .end local v8    # "socket":Ljava/net/DatagramSocket;
    .restart local v7    # "socket":Ljava/net/DatagramSocket;
    .restart local v10    # "t":Ljava/lang/Throwable;
    :goto_1
    sget-object v11, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v11, v10}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 1743
    if-eqz v7, :cond_0

    .line 1744
    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 1746
    :cond_0
    sget-object v11, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v12, "DialEventsListener thread exit"

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1747
    return-void

    .line 1702
    .end local v7    # "socket":Ljava/net/DatagramSocket;
    .end local v10    # "t":Ljava/lang/Throwable;
    .restart local v2    # "data":[B
    .restart local v4    # "index":I
    .restart local v5    # "len":I
    .restart local v6    # "packet":Ljava/net/DatagramPacket;
    .restart local v8    # "socket":Ljava/net/DatagramSocket;
    .restart local v9    # "str":Ljava/lang/String;
    :cond_1
    const/4 v11, 0x0

    :try_start_4
    invoke-virtual {v9, v11, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 1703
    .local v3, "event":Ljava/lang/String;
    add-int/lit8 v11, v4, 0x1

    invoke-virtual {v9, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 1705
    .local v0, "addr":Ljava/lang/String;
    iget-object v11, p0, Lcom/navdy/hud/app/device/dial/DialManager$21;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v11}, Lcom/navdy/hud/app/device/dial/DialManager;->access$800(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothAdapter;

    invoke-static {v0}, Landroid/bluetooth/BluetoothAdapter;->checkBluetoothAddress(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_2

    .line 1706
    sget-object v11, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v13, "[DialEventsListener] not a valid bluetooth address"

    invoke-virtual {v11, v13}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 1710
    :cond_2
    iget-object v11, p0, Lcom/navdy/hud/app/device/dial/DialManager$21;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v11}, Lcom/navdy/hud/app/device/dial/DialManager;->access$800(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v11

    invoke-virtual {v11, v0}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    .line 1711
    .local v1, "bluetoothDevice":Landroid/bluetooth/BluetoothDevice;
    sget-object v11, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "[DialEventsListener] event for ["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "]"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1713
    iget-object v11, p0, Lcom/navdy/hud/app/device/dial/DialManager$21;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # invokes: Lcom/navdy/hud/app/device/dial/DialManager;->isPresentInBondList(Landroid/bluetooth/BluetoothDevice;)Z
    invoke-static {v11, v1}, Lcom/navdy/hud/app/device/dial/DialManager;->access$4600(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothDevice;)Z

    move-result v11

    if-nez v11, :cond_3

    .line 1714
    sget-object v11, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "[DialEventsListener] "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " not present in bond list, ignore"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1718
    :cond_3
    const/4 v11, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v13

    sparse-switch v13, :sswitch_data_0

    :cond_4
    :goto_2
    packed-switch v11, :pswitch_data_0

    goto/16 :goto_0

    .line 1720
    :pswitch_0
    iget-object v11, p0, Lcom/navdy/hud/app/device/dial/DialManager$21;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # invokes: Lcom/navdy/hud/app/device/dial/DialManager;->connectEvent(Landroid/bluetooth/BluetoothDevice;)V
    invoke-static {v11, v1}, Lcom/navdy/hud/app/device/dial/DialManager;->access$4700(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothDevice;)V

    goto/16 :goto_0

    .line 1718
    :sswitch_0
    const-string v13, "CONNECT"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    move v11, v12

    goto :goto_2

    :sswitch_1
    const-string v13, "DISCONNECT"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    const/4 v11, 0x1

    goto :goto_2

    :sswitch_2
    const-string v13, "ENCRYPTION_FAILED"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    const/4 v11, 0x2

    goto :goto_2

    :sswitch_3
    const-string v13, "INPUTDESCRIPTOR_WRITTEN"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    const/4 v11, 0x3

    goto :goto_2

    .line 1724
    :pswitch_1
    iget-object v11, p0, Lcom/navdy/hud/app/device/dial/DialManager$21;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # invokes: Lcom/navdy/hud/app/device/dial/DialManager;->disconnectEvent(Landroid/bluetooth/BluetoothDevice;)V
    invoke-static {v11, v1}, Lcom/navdy/hud/app/device/dial/DialManager;->access$4800(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothDevice;)V

    goto/16 :goto_0

    .line 1728
    :pswitch_2
    iget-object v11, p0, Lcom/navdy/hud/app/device/dial/DialManager$21;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # invokes: Lcom/navdy/hud/app/device/dial/DialManager;->encryptionFailedEvent(Landroid/bluetooth/BluetoothDevice;)V
    invoke-static {v11, v1}, Lcom/navdy/hud/app/device/dial/DialManager;->access$4900(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothDevice;)V

    goto/16 :goto_0

    .line 1732
    :pswitch_3
    iget-object v11, p0, Lcom/navdy/hud/app/device/dial/DialManager$21;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # invokes: Lcom/navdy/hud/app/device/dial/DialManager;->handleInputReportDescriptor(Landroid/bluetooth/BluetoothDevice;)V
    invoke-static {v11, v1}, Lcom/navdy/hud/app/device/dial/DialManager;->access$5000(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothDevice;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_0

    .line 1739
    .end local v0    # "addr":Ljava/lang/String;
    .end local v1    # "bluetoothDevice":Landroid/bluetooth/BluetoothDevice;
    .end local v2    # "data":[B
    .end local v3    # "event":Ljava/lang/String;
    .end local v4    # "index":I
    .end local v5    # "len":I
    .end local v6    # "packet":Ljava/net/DatagramPacket;
    .end local v8    # "socket":Ljava/net/DatagramSocket;
    .end local v9    # "str":Ljava/lang/String;
    .restart local v7    # "socket":Ljava/net/DatagramSocket;
    :catch_2
    move-exception v10

    goto/16 :goto_1

    .line 1718
    :sswitch_data_0
    .sparse-switch
        -0x54f842af -> :sswitch_3
        -0x4eeddf87 -> :sswitch_2
        0x3c87449c -> :sswitch_1
        0x638004ca -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
