.class final Lcom/navdy/hud/app/device/dial/DialManagerHelper$10;
.super Ljava/lang/Object;
.source "DialManagerHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/device/dial/DialManagerHelper;->sendRebootLocalyticsEvent(Landroid/os/Handler;ZLandroid/bluetooth/BluetoothDevice;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$device:Landroid/bluetooth/BluetoothDevice;

.field final synthetic val$result:Ljava/lang/String;

.field final synthetic val$successful:Z


# direct methods
.method constructor <init>(Ljava/lang/String;ZLandroid/bluetooth/BluetoothDevice;)V
    .locals 0

    .prologue
    .line 316
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$10;->val$result:Ljava/lang/String;

    iput-boolean p2, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$10;->val$successful:Z

    iput-object p3, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$10;->val$device:Landroid/bluetooth/BluetoothDevice;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    .line 320
    :try_start_0
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 321
    .local v6, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v10, "Type"

    const-string v11, "Reboot"

    invoke-interface {v6, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 322
    const-string v10, "Result"

    iget-object v11, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$10;->val$result:Ljava/lang/String;

    invoke-interface {v6, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 323
    const-string v11, "Success"

    iget-boolean v10, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$10;->val$successful:Z

    if-eqz v10, :cond_1

    const-string v10, "true"

    :goto_0
    invoke-interface {v6, v11, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 324
    const/4 v0, 0x0

    .line 325
    .local v0, "address":Ljava/lang/String;
    iget-object v10, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$10;->val$device:Landroid/bluetooth/BluetoothDevice;

    if-eqz v10, :cond_0

    .line 326
    iget-object v10, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$10;->val$device:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v10}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    .line 328
    :cond_0
    const-string v10, "Dial_Address"

    invoke-interface {v6, v10, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 329
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->getInstance()Lcom/navdy/hud/app/device/dial/DialManager;

    move-result-object v2

    .line 330
    .local v2, "dialManager":Lcom/navdy/hud/app/device/dial/DialManager;
    invoke-virtual {v2}, Lcom/navdy/hud/app/device/dial/DialManager;->getLastKnownBatteryLevel()I

    move-result v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 331
    .local v1, "battery":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/navdy/hud/app/device/dial/DialManager;->getLastKnownRawBatteryLevel()Ljava/lang/Integer;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 332
    .local v7, "rawBattery":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/navdy/hud/app/device/dial/DialManager;->getLastKnownSystemTemperature()Ljava/lang/Integer;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 333
    .local v9, "temperature":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/navdy/hud/app/device/dial/DialManager;->getFirmWareVersion()Ljava/lang/String;

    move-result-object v3

    .line 334
    .local v3, "fw":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/navdy/hud/app/device/dial/DialManager;->getHardwareVersion()Ljava/lang/String;

    move-result-object v4

    .line 335
    .local v4, "hw":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/navdy/hud/app/device/dial/DialManager;->getDialFirmwareUpdater()Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;

    move-result-object v10

    invoke-virtual {v10}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->getVersions()Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;

    move-result-object v10

    iget-object v10, v10, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;->dial:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;

    iget v5, v10, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;->incrementalVersion:I

    .line 336
    .local v5, "incremental":I
    const-string v10, "Battery_Level"

    invoke-interface {v6, v10, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 337
    const-string v10, "Raw_Battery_Level"

    invoke-interface {v6, v10, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 338
    const-string v10, "System_Temperature"

    invoke-interface {v6, v10, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 339
    const-string v10, "FW_Version"

    invoke-interface {v6, v10, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 340
    const-string v10, "HW_Version"

    invoke-interface {v6, v10, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 341
    const-string v10, "Incremental_Version"

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v6, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 342
    # getter for: Lcom/navdy/hud/app/device/dial/DialManagerHelper;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "sending foundDial["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-boolean v12, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$10;->val$successful:Z

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "] battery["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "] rawBattery["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "] temperature["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "]"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " fw["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "] hw["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "] incremental["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "] type["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "Reboot"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "] result ["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$10;->val$result:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "] address ["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "]"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 345
    const-string v10, "Dial_Pairing"

    invoke-static {v10, v6}, Lcom/localytics/android/Localytics;->tagEvent(Ljava/lang/String;Ljava/util/Map;)V

    .line 349
    .end local v0    # "address":Ljava/lang/String;
    .end local v1    # "battery":Ljava/lang/String;
    .end local v2    # "dialManager":Lcom/navdy/hud/app/device/dial/DialManager;
    .end local v3    # "fw":Ljava/lang/String;
    .end local v4    # "hw":Ljava/lang/String;
    .end local v5    # "incremental":I
    .end local v6    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v7    # "rawBattery":Ljava/lang/String;
    .end local v9    # "temperature":Ljava/lang/String;
    :goto_1
    return-void

    .line 323
    .restart local v6    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_1
    const-string v10, "false"
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 346
    .end local v6    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :catch_0
    move-exception v8

    .line 347
    .local v8, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/device/dial/DialManagerHelper;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v10

    invoke-virtual {v10, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_1
.end method
