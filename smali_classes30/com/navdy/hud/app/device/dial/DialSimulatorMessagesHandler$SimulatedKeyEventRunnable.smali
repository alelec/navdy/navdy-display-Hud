.class Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler$SimulatedKeyEventRunnable;
.super Ljava/lang/Object;
.source "DialSimulatorMessagesHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SimulatedKeyEventRunnable"
.end annotation


# instance fields
.field private eventAction:Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

.field final synthetic this$0:Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler;


# direct methods
.method public constructor <init>(Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler;Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;)V
    .locals 0
    .param p2, "eventAction"    # Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler$SimulatedKeyEventRunnable;->this$0:Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p2, p0, Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler$SimulatedKeyEventRunnable;->eventAction:Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

    .line 43
    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 47
    new-instance v2, Landroid/app/Instrumentation;

    invoke-direct {v2}, Landroid/app/Instrumentation;-><init>()V

    .line 48
    .local v2, "instrumentation":Landroid/app/Instrumentation;
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler$SimulatedKeyEventRunnable;->eventAction:Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

    sget-object v4, Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;->DIAL_LONG_CLICK:Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

    if-ne v3, v4, :cond_0

    .line 49
    new-instance v0, Landroid/view/KeyEvent;

    const/16 v3, 0x42

    invoke-direct {v0, v8, v3}, Landroid/view/KeyEvent;-><init>(II)V

    .line 50
    .local v0, "event":Landroid/view/KeyEvent;
    invoke-virtual {v0}, Landroid/view/KeyEvent;->getFlags()I

    move-result v1

    .line 52
    .local v1, "initFlags":I
    invoke-virtual {v0}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v4

    or-int/lit16 v3, v1, 0x80

    .line 51
    invoke-static {v0, v4, v5, v9, v3}, Landroid/view/KeyEvent;->changeTimeRepeat(Landroid/view/KeyEvent;JII)Landroid/view/KeyEvent;

    move-result-object v0

    .line 54
    invoke-virtual {v2, v0}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    .line 57
    invoke-virtual {v0}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v4

    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler$SimulatedKeyEventRunnable;->this$0:Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler;

    # getter for: Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler;->longPressTimeThreshold:I
    invoke-static {v3}, Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler;->access$000(Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler;)I

    move-result v3

    int-to-long v6, v3

    add-long/2addr v4, v6

    or-int/lit16 v3, v1, 0x280

    .line 56
    invoke-static {v0, v4, v5, v8, v3}, Landroid/view/KeyEvent;->changeTimeRepeat(Landroid/view/KeyEvent;JII)Landroid/view/KeyEvent;

    move-result-object v0

    .line 59
    invoke-static {v0, v9}, Landroid/view/KeyEvent;->changeAction(Landroid/view/KeyEvent;I)Landroid/view/KeyEvent;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    .line 63
    .end local v0    # "event":Landroid/view/KeyEvent;
    .end local v1    # "initFlags":I
    :goto_0
    return-void

    .line 61
    :cond_0
    # getter for: Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler;->SIMULATION_TO_KEY_EVENT:Ljava/util/Map;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler;->access$100()Ljava/util/Map;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler$SimulatedKeyEventRunnable;->eventAction:Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/app/Instrumentation;->sendKeyDownUpSync(I)V

    goto :goto_0
.end method
