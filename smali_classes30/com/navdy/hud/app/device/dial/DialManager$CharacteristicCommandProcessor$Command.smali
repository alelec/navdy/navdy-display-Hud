.class abstract Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor$Command;
.super Ljava/lang/Object;
.source "DialManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "Command"
.end annotation


# instance fields
.field characteristic:Landroid/bluetooth/BluetoothGattCharacteristic;


# direct methods
.method public constructor <init>(Landroid/bluetooth/BluetoothGattCharacteristic;)V
    .locals 0
    .param p1, "characteristic"    # Landroid/bluetooth/BluetoothGattCharacteristic;

    .prologue
    .line 547
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 548
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor$Command;->characteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 549
    return-void
.end method


# virtual methods
.method public abstract process(Landroid/bluetooth/BluetoothGatt;)V
.end method
