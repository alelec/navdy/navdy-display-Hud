.class public final enum Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;
.super Ljava/lang/Enum;
.source "DialConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/device/dial/DialConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NotificationReason"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

.field public static final enum CONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

.field public static final enum DISCONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

.field public static final enum EXTREMELY_LOW_BATTERY:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

.field public static final enum LOW_BATTERY:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

.field public static final enum OK_BATTERY:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

.field public static final enum VERY_LOW_BATTERY:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 18
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    const-string v1, "DISCONNECTED"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->DISCONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    .line 19
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    const-string v1, "CONNECTED"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->CONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    .line 20
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    const-string v1, "LOW_BATTERY"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->LOW_BATTERY:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    .line 21
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    const-string v1, "VERY_LOW_BATTERY"

    invoke-direct {v0, v1, v6}, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->VERY_LOW_BATTERY:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    .line 22
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    const-string v1, "EXTREMELY_LOW_BATTERY"

    invoke-direct {v0, v1, v7}, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->EXTREMELY_LOW_BATTERY:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    .line 23
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    const-string v1, "OK_BATTERY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->OK_BATTERY:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    .line 17
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    sget-object v1, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->DISCONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->CONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->LOW_BATTERY:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->VERY_LOW_BATTERY:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->EXTREMELY_LOW_BATTERY:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->OK_BATTERY:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->$VALUES:[Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 17
    const-class v0, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->$VALUES:[Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    return-object v0
.end method
