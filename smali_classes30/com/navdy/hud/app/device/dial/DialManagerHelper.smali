.class public Lcom/navdy/hud/app/device/dial/DialManagerHelper;
.super Ljava/lang/Object;
.source "DialManagerHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialForgotten;,
        Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;,
        Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnectionStatus;
    }
.end annotation


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100(Landroid/bluetooth/BluetoothProfile;Landroid/bluetooth/BluetoothDevice;)Z
    .locals 1
    .param p0, "x0"    # Landroid/bluetooth/BluetoothProfile;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 22
    invoke-static {p0, p1}, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->forceConnect(Landroid/bluetooth/BluetoothProfile;Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Landroid/bluetooth/BluetoothProfile;Landroid/bluetooth/BluetoothDevice;)Z
    .locals 1
    .param p0, "x0"    # Landroid/bluetooth/BluetoothProfile;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 22
    invoke-static {p0, p1}, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->forceDisconnect(Landroid/bluetooth/BluetoothProfile;Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    return v0
.end method

.method public static connectToDial(Landroid/bluetooth/BluetoothAdapter;Landroid/bluetooth/BluetoothDevice;Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;)V
    .locals 6
    .param p0, "bluetoothAdapter"    # Landroid/bluetooth/BluetoothAdapter;
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;
    .param p2, "callBack"    # Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;

    .prologue
    const/4 v5, 0x1

    .line 111
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    .line 112
    :cond_0
    :try_start_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 187
    :catch_0
    move-exception v1

    .line 188
    .local v1, "t":Ljava/lang/Throwable;
    sget-object v2, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "connectToDial"

    invoke-virtual {v2, v3, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 189
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v2

    new-instance v3, Lcom/navdy/hud/app/device/dial/DialManagerHelper$6;

    invoke-direct {v3, p2}, Lcom/navdy/hud/app/device/dial/DialManagerHelper$6;-><init>(Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;)V

    invoke-virtual {v2, v3, v5}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 196
    .end local v1    # "t":Ljava/lang/Throwable;
    :cond_1
    :goto_0
    return-void

    .line 114
    :cond_2
    :try_start_1
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;

    invoke-direct {v3, p1, p2}, Lcom/navdy/hud/app/device/dial/DialManagerHelper$4;-><init>(Landroid/bluetooth/BluetoothDevice;Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;)V

    const/4 v4, 0x4

    invoke-virtual {p0, v2, v3, v4}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    move-result v0

    .line 178
    .local v0, "ret":Z
    sget-object v2, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[Dial]connectToDial getProfileProxy returned:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 179
    if-nez v0, :cond_1

    .line 180
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v2

    new-instance v3, Lcom/navdy/hud/app/device/dial/DialManagerHelper$5;

    invoke-direct {v3, p2}, Lcom/navdy/hud/app/device/dial/DialManagerHelper$5;-><init>(Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;)V

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public static disconnectFromDial(Landroid/bluetooth/BluetoothAdapter;Landroid/bluetooth/BluetoothDevice;Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;)V
    .locals 6
    .param p0, "bluetoothAdapter"    # Landroid/bluetooth/BluetoothAdapter;
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;
    .param p2, "callBack"    # Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;

    .prologue
    const/4 v5, 0x1

    .line 202
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    .line 203
    :cond_0
    :try_start_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 279
    :catch_0
    move-exception v1

    .line 280
    .local v1, "t":Ljava/lang/Throwable;
    sget-object v2, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "disconnectFromDial"

    invoke-virtual {v2, v3, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 281
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v2

    new-instance v3, Lcom/navdy/hud/app/device/dial/DialManagerHelper$9;

    invoke-direct {v3, p2}, Lcom/navdy/hud/app/device/dial/DialManagerHelper$9;-><init>(Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;)V

    invoke-virtual {v2, v3, v5}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 288
    .end local v1    # "t":Ljava/lang/Throwable;
    :cond_1
    :goto_0
    return-void

    .line 205
    :cond_2
    :try_start_1
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;

    invoke-direct {v3, p1, p2}, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;-><init>(Landroid/bluetooth/BluetoothDevice;Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;)V

    const/4 v4, 0x4

    invoke-virtual {p0, v2, v3, v4}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    move-result v0

    .line 269
    .local v0, "ret":Z
    sget-object v2, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[Dial]disconnectFromDial getProfileProxy returned:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 270
    if-nez v0, :cond_1

    .line 271
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v2

    new-instance v3, Lcom/navdy/hud/app/device/dial/DialManagerHelper$8;

    invoke-direct {v3, p2}, Lcom/navdy/hud/app/device/dial/DialManagerHelper$8;-><init>(Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;)V

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private static forceConnect(Landroid/bluetooth/BluetoothProfile;Landroid/bluetooth/BluetoothDevice;)Z
    .locals 1
    .param p0, "proxy"    # Landroid/bluetooth/BluetoothProfile;
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 291
    const-string v0, "connect"

    invoke-static {p0, p1, v0}, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->invokeMethod(Landroid/bluetooth/BluetoothProfile;Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static forceDisconnect(Landroid/bluetooth/BluetoothProfile;Landroid/bluetooth/BluetoothDevice;)Z
    .locals 1
    .param p0, "proxy"    # Landroid/bluetooth/BluetoothProfile;
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 295
    const-string v0, "disconnect"

    invoke-static {p0, p1, v0}, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->invokeMethod(Landroid/bluetooth/BluetoothProfile;Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static invokeMethod(Landroid/bluetooth/BluetoothProfile;Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;)Z
    .locals 9
    .param p0, "proxy"    # Landroid/bluetooth/BluetoothProfile;
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;
    .param p2, "methodName"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 300
    const/4 v5, 0x0

    :try_start_0
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    .line 302
    .local v3, "returnValue":Ljava/lang/Boolean;
    :try_start_1
    const-string v5, "android.bluetooth.BluetoothInputDevice"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 303
    .local v1, "class1":Ljava/lang/Class;
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Landroid/bluetooth/BluetoothDevice;

    aput-object v8, v5, v7

    invoke-virtual {v1, p2, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 304
    .local v2, "method":Ljava/lang/reflect/Method;
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p1, v5, v7

    invoke-virtual {v2, p0, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Ljava/lang/Boolean;

    move-object v3, v0
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 308
    .end local v1    # "class1":Ljava/lang/Class;
    .end local v2    # "method":Ljava/lang/reflect/Method;
    :goto_0
    :try_start_2
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    .line 311
    .end local v3    # "returnValue":Ljava/lang/Boolean;
    :goto_1
    return v5

    .line 305
    .restart local v3    # "returnValue":Ljava/lang/Boolean;
    :catch_0
    move-exception v4

    .line 306
    .local v4, "t":Ljava/lang/Throwable;
    sget-object v5, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "[Dial]"

    invoke-virtual {v5, v7, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 309
    .end local v3    # "returnValue":Ljava/lang/Boolean;
    .end local v4    # "t":Ljava/lang/Throwable;
    :catch_1
    move-exception v4

    .line 310
    .restart local v4    # "t":Ljava/lang/Throwable;
    sget-object v5, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "[Dial]"

    invoke-virtual {v5, v7, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    move v5, v6

    .line 311
    goto :goto_1
.end method

.method public static isDialConnected(Landroid/bluetooth/BluetoothAdapter;Landroid/bluetooth/BluetoothDevice;Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnectionStatus;)V
    .locals 6
    .param p0, "bluetoothAdapter"    # Landroid/bluetooth/BluetoothAdapter;
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;
    .param p2, "callBack"    # Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnectionStatus;

    .prologue
    const/4 v5, 0x1

    .line 42
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    .line 43
    :cond_0
    :try_start_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    :catch_0
    move-exception v1

    .line 97
    .local v1, "t":Ljava/lang/Throwable;
    sget-object v2, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "isDialConnected"

    invoke-virtual {v2, v3, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 98
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v2

    new-instance v3, Lcom/navdy/hud/app/device/dial/DialManagerHelper$3;

    invoke-direct {v3, p2}, Lcom/navdy/hud/app/device/dial/DialManagerHelper$3;-><init>(Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnectionStatus;)V

    invoke-virtual {v2, v3, v5}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 105
    .end local v1    # "t":Ljava/lang/Throwable;
    :cond_1
    :goto_0
    return-void

    .line 46
    :cond_2
    :try_start_1
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Lcom/navdy/hud/app/device/dial/DialManagerHelper$1;

    invoke-direct {v3, p1, p2}, Lcom/navdy/hud/app/device/dial/DialManagerHelper$1;-><init>(Landroid/bluetooth/BluetoothDevice;Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnectionStatus;)V

    const/4 v4, 0x4

    invoke-virtual {p0, v2, v3, v4}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    move-result v0

    .line 87
    .local v0, "ret":Z
    sget-object v2, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[Dial]getProfileProxy returned:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 88
    if-nez v0, :cond_1

    .line 89
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v2

    new-instance v3, Lcom/navdy/hud/app/device/dial/DialManagerHelper$2;

    invoke-direct {v3, p2}, Lcom/navdy/hud/app/device/dial/DialManagerHelper$2;-><init>(Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnectionStatus;)V

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public static sendLocalyticsEvent(Landroid/os/Handler;ZZIZ)V
    .locals 6
    .param p0, "handler"    # Landroid/os/Handler;
    .param p1, "firstTime"    # Z
    .param p2, "foundDial"    # Z
    .param p3, "delay"    # I
    .param p4, "pair"    # Z

    .prologue
    .line 358
    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-static/range {v0 .. v5}, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->sendLocalyticsEvent(Landroid/os/Handler;ZZIZLjava/lang/String;)V

    .line 359
    return-void
.end method

.method public static sendLocalyticsEvent(Landroid/os/Handler;ZZIZLjava/lang/String;)V
    .locals 6
    .param p0, "handler"    # Landroid/os/Handler;
    .param p1, "firstTime"    # Z
    .param p2, "foundDial"    # Z
    .param p3, "delay"    # I
    .param p4, "pair"    # Z
    .param p5, "result"    # Ljava/lang/String;

    .prologue
    .line 367
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->getInstance()Lcom/navdy/hud/app/device/dial/DialManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/device/dial/DialManager;->getDialDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v5

    .line 369
    .local v5, "device":Landroid/bluetooth/BluetoothDevice;
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$11;

    move v1, p1

    move v2, p2

    move v3, p4

    move-object v4, p5

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/device/dial/DialManagerHelper$11;-><init>(ZZZLjava/lang/String;Landroid/bluetooth/BluetoothDevice;)V

    if-eqz p2, :cond_0

    int-to-long v2, p3

    :goto_0
    invoke-virtual {p0, v0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 410
    return-void

    .line 369
    :cond_0
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public static sendRebootLocalyticsEvent(Landroid/os/Handler;ZLandroid/bluetooth/BluetoothDevice;Ljava/lang/String;)V
    .locals 1
    .param p0, "handler"    # Landroid/os/Handler;
    .param p1, "successful"    # Z
    .param p2, "device"    # Landroid/bluetooth/BluetoothDevice;
    .param p3, "result"    # Ljava/lang/String;

    .prologue
    .line 316
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$10;

    invoke-direct {v0, p3, p1, p2}, Lcom/navdy/hud/app/device/dial/DialManagerHelper$10;-><init>(Ljava/lang/String;ZLandroid/bluetooth/BluetoothDevice;)V

    invoke-virtual {p0, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 351
    return-void
.end method
