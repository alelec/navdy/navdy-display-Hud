.class Lcom/navdy/hud/app/device/dial/DialManagerHelper$1$1;
.super Ljava/lang/Object;
.source "DialManagerHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/device/dial/DialManagerHelper$1;->onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$1;

.field final synthetic val$proxy:Landroid/bluetooth/BluetoothProfile;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/dial/DialManagerHelper$1;Landroid/bluetooth/BluetoothProfile;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/device/dial/DialManagerHelper$1;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$1$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$1;

    iput-object p2, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$1$1;->val$proxy:Landroid/bluetooth/BluetoothProfile;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 53
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$1$1;->val$proxy:Landroid/bluetooth/BluetoothProfile;

    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$1$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$1;

    iget-object v3, v3, Lcom/navdy/hud/app/device/dial/DialManagerHelper$1;->val$device:Landroid/bluetooth/BluetoothDevice;

    invoke-interface {v2, v3}, Landroid/bluetooth/BluetoothProfile;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    move-result v0

    .line 54
    .local v0, "state":I
    # getter for: Lcom/navdy/hud/app/device/dial/DialManagerHelper;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[Dial]Device "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$1$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$1;

    iget-object v4, v4, Lcom/navdy/hud/app/device/dial/DialManagerHelper$1;->val$device:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " state is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 55
    invoke-static {v0}, Lcom/navdy/hud/app/bluetooth/utils/BluetoothUtils;->getConnectedState(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 54
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 57
    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    .line 58
    # getter for: Lcom/navdy/hud/app/device/dial/DialManagerHelper;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "[Dial]already connected"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 59
    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$1$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$1;

    iget-object v2, v2, Lcom/navdy/hud/app/device/dial/DialManagerHelper$1;->val$callBack:Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnectionStatus;

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnectionStatus;->onStatus(Z)V

    .line 67
    .end local v0    # "state":I
    :goto_0
    return-void

    .line 61
    .restart local v0    # "state":I
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$1$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$1;

    iget-object v2, v2, Lcom/navdy/hud/app/device/dial/DialManagerHelper$1;->val$callBack:Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnectionStatus;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnectionStatus;->onStatus(Z)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 63
    .end local v0    # "state":I
    :catch_0
    move-exception v1

    .line 64
    .local v1, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/device/dial/DialManagerHelper;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "[Dial]"

    invoke-virtual {v2, v3, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 65
    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$1$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$1;

    iget-object v2, v2, Lcom/navdy/hud/app/device/dial/DialManagerHelper$1;->val$callBack:Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnectionStatus;

    invoke-interface {v2, v5}, Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnectionStatus;->onStatus(Z)V

    goto :goto_0
.end method
