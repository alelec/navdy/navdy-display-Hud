.class public Lcom/navdy/hud/app/device/dial/DialManager;
.super Ljava/lang/Object;
.source "DialManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;,
        Lcom/navdy/hud/app/device/dial/DialManager$DialUpdateStatus;
    }
.end annotation


# static fields
.field private static final BALANCED:Ljava/lang/String; = "balanced"

.field private static final DIAL_CONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

.field private static final DIAL_CONNECTING:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

.field private static final DIAL_CONNECTION_FAILED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

.field private static final DIAL_NOT_CONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

.field private static final DIAL_NOT_FOUND:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

.field private static final DIAL_POWER_SETTING:Ljava/lang/String; = "persist.sys.bt.dial.pwr"

.field private static final HIGH_POWER:Ljava/lang/String; = "high"

.field private static final LOW_POWER:Ljava/lang/String; = "low"

.field private static final PowerMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field static final TAG_DIAL:Ljava/lang/String; = "[Dial]"

.field static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static final singleton:Lcom/navdy/hud/app/device/dial/DialManager;


# instance fields
.field private batteryCharateristic:Landroid/bluetooth/BluetoothGattCharacteristic;

.field private batteryReadRunnable:Ljava/lang/Runnable;

.field private bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private bluetoothBondingReceiver:Landroid/content/BroadcastReceiver;

.field private bluetoothConnectivityReceiver:Landroid/content/BroadcastReceiver;

.field private bluetoothGatt:Landroid/bluetooth/BluetoothGatt;

.field private bluetoothLEScanner:Landroid/bluetooth/le/BluetoothLeScanner;

.field private bluetoothManager:Landroid/bluetooth/BluetoothManager;

.field private bluetoothOnOffReceiver:Landroid/content/BroadcastReceiver;

.field private bondHangRunnable:Ljava/lang/Runnable;

.field private bondTries:I

.field private bondedDials:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation
.end field

.field private bondingDial:Landroid/bluetooth/BluetoothDevice;

.field private bus:Lcom/squareup/otto/Bus;

.field private volatile connectedDial:Landroid/bluetooth/BluetoothDevice;

.field private connectionErrorRunnable:Ljava/lang/Runnable;

.field private deviceInfoGattReadRunnable:Ljava/lang/Runnable;

.field private dialEventsListenerThread:Ljava/lang/Thread;

.field private dialFirmwareUpdater:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;

.field private dialForgetKeysCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

.field private volatile dialManagerInitialized:Z

.field private dialRebootCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

.field private disconnectedDial:Ljava/lang/String;

.field private encryptionRetryCount:I

.field private firmWareVersion:Ljava/lang/String;

.field private firmwareVersionCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

.field private gattCallback:Landroid/bluetooth/BluetoothGattCallback;

.field private gattCharacteristicProcessor:Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;

.field private handler:Landroid/os/Handler;

.field private handlerThread:Landroid/os/HandlerThread;

.field private hardwareVersion:Ljava/lang/String;

.field private hardwareVersionCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

.field private hidHostReadyCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

.field private hidHostReadyReadRunnable:Ljava/lang/Runnable;

.field private ignoredNonNavdyDials:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation
.end field

.field private volatile isGattOn:Z

.field private volatile isScanning:Z

.field private lastKnownBatteryLevel:I

.field private lastKnownRawBatteryLevel:Ljava/lang/Integer;

.field private lastKnownSystemTemperature:Ljava/lang/Integer;

.field private leScanCallback:Landroid/bluetooth/le/ScanCallback;

.field private notificationReasonBattery:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

.field private rawBatteryCharateristic:Landroid/bluetooth/BluetoothGattCharacteristic;

.field private scanHangRunnable:Ljava/lang/Runnable;

.field private scannedNavdyDials:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            "Landroid/bluetooth/le/ScanRecord;",
            ">;"
        }
    .end annotation
.end field

.field private sendLocalyticsSuccessRunnable:Ljava/lang/Runnable;

.field private sharedPreferences:Landroid/content/SharedPreferences;

.field private stopScanRunnable:Ljava/lang/Runnable;

.field private volatile tempConnectedDial:Landroid/bluetooth/BluetoothDevice;

.field private temperatureCharateristic:Landroid/bluetooth/BluetoothGattCharacteristic;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 68
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/device/dial/DialManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 73
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialManager;

    invoke-direct {v0}, Lcom/navdy/hud/app/device/dial/DialManager;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->singleton:Lcom/navdy/hud/app/device/dial/DialManager;

    .line 89
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

    sget-object v1, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;->CONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;-><init>(Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;)V

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->DIAL_CONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

    .line 92
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

    sget-object v1, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;->DISCONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;-><init>(Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;)V

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->DIAL_NOT_CONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

    .line 95
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

    sget-object v1, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;->CONNECTING:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;-><init>(Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;)V

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->DIAL_CONNECTING:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

    .line 99
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

    sget-object v1, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;->CONNECTION_FAILED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;-><init>(Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;)V

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->DIAL_CONNECTION_FAILED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

    .line 102
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

    sget-object v1, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;->NO_DIAL_FOUND:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;-><init>(Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;)V

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->DIAL_NOT_FOUND:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

    .line 157
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->PowerMap:Ljava/util/Map;

    .line 160
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->PowerMap:Ljava/util/Map;

    const-string v1, "low"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->PowerMap:Ljava/util/Map;

    const-string v1, "balanced"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->PowerMap:Ljava/util/Map;

    const-string v1, "high"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 887
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->lastKnownBatteryLevel:I

    .line 118
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondedDials:Ljava/util/ArrayList;

    .line 134
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->scannedNavdyDials:Ljava/util/Map;

    .line 135
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->ignoredNonNavdyDials:Ljava/util/Set;

    .line 143
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->OK_BATTERY:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    iput-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->notificationReasonBattery:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    .line 172
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialManager$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/device/dial/DialManager$1;-><init>(Lcom/navdy/hud/app/device/dial/DialManager;)V

    iput-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->scanHangRunnable:Ljava/lang/Runnable;

    .line 184
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialManager$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/device/dial/DialManager$2;-><init>(Lcom/navdy/hud/app/device/dial/DialManager;)V

    iput-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondHangRunnable:Ljava/lang/Runnable;

    .line 202
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialManager$3;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/device/dial/DialManager$3;-><init>(Lcom/navdy/hud/app/device/dial/DialManager;)V

    iput-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->sendLocalyticsSuccessRunnable:Ljava/lang/Runnable;

    .line 212
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialManager$4;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/device/dial/DialManager$4;-><init>(Lcom/navdy/hud/app/device/dial/DialManager;)V

    iput-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->leScanCallback:Landroid/bluetooth/le/ScanCallback;

    .line 231
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialManager$5;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/device/dial/DialManager$5;-><init>(Lcom/navdy/hud/app/device/dial/DialManager;)V

    iput-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->stopScanRunnable:Ljava/lang/Runnable;

    .line 243
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialManager$6;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/device/dial/DialManager$6;-><init>(Lcom/navdy/hud/app/device/dial/DialManager;)V

    iput-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bluetoothBondingReceiver:Landroid/content/BroadcastReceiver;

    .line 331
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialManager$7;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/device/dial/DialManager$7;-><init>(Lcom/navdy/hud/app/device/dial/DialManager;)V

    iput-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bluetoothOnOffReceiver:Landroid/content/BroadcastReceiver;

    .line 369
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialManager$8;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/device/dial/DialManager$8;-><init>(Lcom/navdy/hud/app/device/dial/DialManager;)V

    iput-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->connectionErrorRunnable:Ljava/lang/Runnable;

    .line 398
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialManager$9;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/device/dial/DialManager$9;-><init>(Lcom/navdy/hud/app/device/dial/DialManager;)V

    iput-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bluetoothConnectivityReceiver:Landroid/content/BroadcastReceiver;

    .line 492
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialManager$10;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/device/dial/DialManager$10;-><init>(Lcom/navdy/hud/app/device/dial/DialManager;)V

    iput-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->batteryReadRunnable:Ljava/lang/Runnable;

    .line 512
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialManager$11;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/device/dial/DialManager$11;-><init>(Lcom/navdy/hud/app/device/dial/DialManager;)V

    iput-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->deviceInfoGattReadRunnable:Ljava/lang/Runnable;

    .line 528
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialManager$12;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/device/dial/DialManager$12;-><init>(Lcom/navdy/hud/app/device/dial/DialManager;)V

    iput-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->hidHostReadyReadRunnable:Ljava/lang/Runnable;

    .line 656
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialManager$13;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/device/dial/DialManager$13;-><init>(Lcom/navdy/hud/app/device/dial/DialManager;)V

    iput-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->gattCallback:Landroid/bluetooth/BluetoothGattCallback;

    .line 888
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "[Dial]initializing..."

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 889
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getApplication()Lcom/navdy/hud/app/HudApplication;

    move-result-object v0

    if-nez v0, :cond_0

    .line 925
    :goto_0
    return-void

    .line 893
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bus:Lcom/squareup/otto/Bus;

    .line 894
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 897
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "bluetooth"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothManager;

    iput-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bluetoothManager:Landroid/bluetooth/BluetoothManager;

    .line 898
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bluetoothManager:Landroid/bluetooth/BluetoothManager;

    if-nez v0, :cond_1

    .line 899
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "[Dial]Bluetooth manager not available"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 903
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bluetoothManager:Landroid/bluetooth/BluetoothManager;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothManager;->getAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 904
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-nez v0, :cond_2

    .line 905
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "[Dial]Bluetooth is not available"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 910
    :cond_2
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "DialHandlerThread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->handlerThread:Landroid/os/HandlerThread;

    .line 911
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->handlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 912
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->handlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->handler:Landroid/os/Handler;

    .line 914
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_3

    .line 915
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "[Dial]Bluetooth is not enabled, should be booting up, wait for the event"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 922
    :goto_1
    invoke-direct {p0}, Lcom/navdy/hud/app/device/dial/DialManager;->startDialEventListener()V

    .line 924
    invoke-direct {p0}, Lcom/navdy/hud/app/device/dial/DialManager;->registerReceivers()V

    goto :goto_0

    .line 917
    :cond_3
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "[Dial]Bluetooth is enabled"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 918
    invoke-direct {p0}, Lcom/navdy/hud/app/device/dial/DialManager;->init()V

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothDevice;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondingDial:Landroid/bluetooth/BluetoothDevice;

    return-object v0
.end method

.method static synthetic access$002(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondingDial:Landroid/bluetooth/BluetoothDevice;

    return-object p1
.end method

.method static synthetic access$100()Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->DIAL_NOT_CONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/navdy/hud/app/device/dial/DialManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->dialManagerInitialized:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/navdy/hud/app/device/dial/DialManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;
    .param p1, "x1"    # Z

    .prologue
    .line 67
    iput-boolean p1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->dialManagerInitialized:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/navdy/hud/app/device/dial/DialManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/navdy/hud/app/device/dial/DialManager;->init()V

    return-void
.end method

.method static synthetic access$1200(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothDevice;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->tempConnectedDial:Landroid/bluetooth/BluetoothDevice;

    return-object v0
.end method

.method static synthetic access$1300()Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->DIAL_CONNECTION_FAILED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothDevice;Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialForgotten;I)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothDevice;
    .param p2, "x2"    # Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialForgotten;
    .param p3, "x3"    # I

    .prologue
    .line 67
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/hud/app/device/dial/DialManager;->disconectAndRemoveBond(Landroid/bluetooth/BluetoothDevice;Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialForgotten;I)V

    return-void
.end method

.method static synthetic access$1500(Lcom/navdy/hud/app/device/dial/DialManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/navdy/hud/app/device/dial/DialManager;->handleDialConnection()V

    return-void
.end method

.method static synthetic access$1600(Lcom/navdy/hud/app/device/dial/DialManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->disconnectedDial:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/navdy/hud/app/device/dial/DialManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->disconnectedDial:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/navdy/hud/app/device/dial/DialManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/navdy/hud/app/device/dial/DialManager;->clearConnectedDial()V

    return-void
.end method

.method static synthetic access$1800(Lcom/navdy/hud/app/device/dial/DialManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/navdy/hud/app/device/dial/DialManager;->stopGATTClient()V

    return-void
.end method

.method static synthetic access$1900(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothGattCharacteristic;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->batteryCharateristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    return-object v0
.end method

.method static synthetic access$1902(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothGattCharacteristic;)Landroid/bluetooth/BluetoothGattCharacteristic;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothGattCharacteristic;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->batteryCharateristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    return-object p1
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/device/dial/DialManager;)Lcom/squareup/otto/Bus;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bus:Lcom/squareup/otto/Bus;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothGattCharacteristic;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->rawBatteryCharateristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    return-object v0
.end method

.method static synthetic access$2002(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothGattCharacteristic;)Landroid/bluetooth/BluetoothGattCharacteristic;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothGattCharacteristic;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->rawBatteryCharateristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothGattCharacteristic;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->temperatureCharateristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    return-object v0
.end method

.method static synthetic access$2102(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothGattCharacteristic;)Landroid/bluetooth/BluetoothGattCharacteristic;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothGattCharacteristic;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->temperatureCharateristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    return-object p1
.end method

.method static synthetic access$2200(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothGattCharacteristic;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->firmwareVersionCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    return-object v0
.end method

.method static synthetic access$2202(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothGattCharacteristic;)Landroid/bluetooth/BluetoothGattCharacteristic;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothGattCharacteristic;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->firmwareVersionCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    return-object p1
.end method

.method static synthetic access$2300(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothGattCharacteristic;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->hardwareVersionCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    return-object v0
.end method

.method static synthetic access$2302(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothGattCharacteristic;)Landroid/bluetooth/BluetoothGattCharacteristic;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothGattCharacteristic;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->hardwareVersionCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    return-object p1
.end method

.method static synthetic access$2400(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothGattCharacteristic;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->hidHostReadyCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    return-object v0
.end method

.method static synthetic access$2402(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothGattCharacteristic;)Landroid/bluetooth/BluetoothGattCharacteristic;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothGattCharacteristic;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->hidHostReadyCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    return-object p1
.end method

.method static synthetic access$2600()Ljava/util/Map;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->PowerMap:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/navdy/hud/app/device/dial/DialManager;)Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->gattCharacteristicProcessor:Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;

    return-object v0
.end method

.method static synthetic access$2702(Lcom/navdy/hud/app/device/dial/DialManager;Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;)Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;
    .param p1, "x1"    # Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->gattCharacteristicProcessor:Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;

    return-object p1
.end method

.method static synthetic access$2800(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothGattCharacteristic;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->dialForgetKeysCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    return-object v0
.end method

.method static synthetic access$2802(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothGattCharacteristic;)Landroid/bluetooth/BluetoothGattCharacteristic;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothGattCharacteristic;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->dialForgetKeysCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    return-object p1
.end method

.method static synthetic access$2900(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothGattCharacteristic;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->dialRebootCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    return-object v0
.end method

.method static synthetic access$2902(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothGattCharacteristic;)Landroid/bluetooth/BluetoothGattCharacteristic;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothGattCharacteristic;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->dialRebootCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    return-object p1
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothDevice;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->connectedDial:Landroid/bluetooth/BluetoothDevice;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/navdy/hud/app/device/dial/DialManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->isGattOn:Z

    return v0
.end method

.method static synthetic access$302(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->connectedDial:Landroid/bluetooth/BluetoothDevice;

    return-object p1
.end method

.method static synthetic access$3100(Lcom/navdy/hud/app/device/dial/DialManager;)Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->dialFirmwareUpdater:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;

    return-object v0
.end method

.method static synthetic access$3102(Lcom/navdy/hud/app/device/dial/DialManager;Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;)Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;
    .param p1, "x1"    # Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->dialFirmwareUpdater:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;

    return-object p1
.end method

.method static synthetic access$3200(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->sharedPreferences:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/navdy/hud/app/device/dial/DialManager;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->hidHostReadyReadRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/navdy/hud/app/device/dial/DialManager;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->batteryReadRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/navdy/hud/app/device/dial/DialManager;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->sendLocalyticsSuccessRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/navdy/hud/app/device/dial/DialManager;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->deviceInfoGattReadRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/navdy/hud/app/device/dial/DialManager;I)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;
    .param p1, "x1"    # I

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/device/dial/DialManager;->processBatteryLevel(I)V

    return-void
.end method

.method static synthetic access$3800(Lcom/navdy/hud/app/device/dial/DialManager;Ljava/lang/Integer;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;
    .param p1, "x1"    # Ljava/lang/Integer;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/device/dial/DialManager;->processRawBatteryLevel(Ljava/lang/Integer;)V

    return-void
.end method

.method static synthetic access$3900(Lcom/navdy/hud/app/device/dial/DialManager;Ljava/lang/Integer;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;
    .param p1, "x1"    # Ljava/lang/Integer;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/device/dial/DialManager;->processSystemTemperature(Ljava/lang/Integer;)V

    return-void
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/navdy/hud/app/device/dial/DialManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->hardwareVersion:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4002(Lcom/navdy/hud/app/device/dial/DialManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->hardwareVersion:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$4100(Lcom/navdy/hud/app/device/dial/DialManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->firmWareVersion:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4102(Lcom/navdy/hud/app/device/dial/DialManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->firmWareVersion:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$4200(Lcom/navdy/hud/app/device/dial/DialManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/navdy/hud/app/device/dial/DialManager;->buildDialList()V

    return-void
.end method

.method static synthetic access$4300(Lcom/navdy/hud/app/device/dial/DialManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/navdy/hud/app/device/dial/DialManager;->checkDialConnections()V

    return-void
.end method

.method static synthetic access$4400(Lcom/navdy/hud/app/device/dial/DialManager;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondedDials:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$4500(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothDevice;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/device/dial/DialManager;->removeBond(Landroid/bluetooth/BluetoothDevice;)V

    return-void
.end method

.method static synthetic access$4600(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothDevice;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/device/dial/DialManager;->isPresentInBondList(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$4700(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothDevice;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/device/dial/DialManager;->connectEvent(Landroid/bluetooth/BluetoothDevice;)V

    return-void
.end method

.method static synthetic access$4800(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothDevice;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/device/dial/DialManager;->disconnectEvent(Landroid/bluetooth/BluetoothDevice;)V

    return-void
.end method

.method static synthetic access$4900(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothDevice;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/device/dial/DialManager;->encryptionFailedEvent(Landroid/bluetooth/BluetoothDevice;)V

    return-void
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothDevice;ILandroid/bluetooth/le/ScanRecord;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothDevice;
    .param p2, "x2"    # I
    .param p3, "x3"    # Landroid/bluetooth/le/ScanRecord;

    .prologue
    .line 67
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/hud/app/device/dial/DialManager;->handleScannedDevice(Landroid/bluetooth/BluetoothDevice;ILandroid/bluetooth/le/ScanRecord;)V

    return-void
.end method

.method static synthetic access$5000(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothDevice;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/device/dial/DialManager;->handleInputReportDescriptor(Landroid/bluetooth/BluetoothDevice;)V

    return-void
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/device/dial/DialManager;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondHangRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothDevice;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/device/dial/DialManager;->addtoBondList(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    return-object v0
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/device/dial/DialManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 67
    iget v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondTries:I

    return v0
.end method

.method static synthetic access$902(Lcom/navdy/hud/app/device/dial/DialManager;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;
    .param p1, "x1"    # I

    .prologue
    .line 67
    iput p1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondTries:I

    return p1
.end method

.method static synthetic access$908(Lcom/navdy/hud/app/device/dial/DialManager;)I
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 67
    iget v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondTries:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondTries:I

    return v0
.end method

.method private addtoBondList(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 6
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    const/4 v2, 0x0

    .line 1621
    if-nez p1, :cond_0

    .line 1639
    :goto_0
    return v2

    .line 1625
    :cond_0
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondedDials:Ljava/util/ArrayList;

    monitor-enter v3

    .line 1626
    const/4 v1, 0x0

    .line 1627
    .local v1, "found":Z
    :try_start_0
    iget-object v4, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondedDials:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 1628
    .local v0, "d":Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1629
    const/4 v1, 0x1

    goto :goto_1

    .line 1632
    .end local v0    # "d":Landroid/bluetooth/BluetoothDevice;
    :cond_2
    if-nez v1, :cond_3

    .line 1633
    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondedDials:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1634
    sget-object v2, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "bonded device added to list ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1635
    const/4 v2, 0x1

    monitor-exit v3

    goto :goto_0

    .line 1637
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_3
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private bondWithDial(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/le/ScanRecord;)V
    .locals 6
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;
    .param p2, "scanRecord"    # Landroid/bluetooth/le/ScanRecord;

    .prologue
    const/16 v4, 0xc

    .line 1168
    const/4 v1, 0x0

    iput v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondTries:I

    .line 1169
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondingDial:Landroid/bluetooth/BluetoothDevice;

    .line 1170
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[Dial]calling createBond ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] addr["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1171
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " rssi["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1172
    invoke-virtual {p2}, Landroid/bluetooth/le/ScanRecord;->getTxPowerLevel()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] state["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v3

    invoke-static {v3}, Lcom/navdy/hud/app/bluetooth/utils/BluetoothUtils;->getBondState(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1170
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1173
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/device/dial/DialManager;->removeFromBondList(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    .line 1174
    .local v0, "removed":Z
    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v1

    if-ne v1, v4, :cond_3

    .line 1175
    :cond_0
    if-eqz v0, :cond_1

    .line 1176
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[Dial]present in bond list:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " invalid state"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1178
    :cond_1
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v1

    if-ne v1, v4, :cond_2

    .line 1179
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[Dial]already bonded:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " invalid state"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1181
    :cond_2
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondingDial:Landroid/bluetooth/BluetoothDevice;

    .line 1182
    new-instance v1, Lcom/navdy/hud/app/device/dial/DialManager$17;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/device/dial/DialManager$17;-><init>(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothDevice;)V

    const/16 v2, 0x7d0

    invoke-direct {p0, p1, v1, v2}, Lcom/navdy/hud/app/device/dial/DialManager;->disconectAndRemoveBond(Landroid/bluetooth/BluetoothDevice;Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialForgotten;I)V

    .line 1208
    :goto_0
    return-void

    .line 1201
    :cond_3
    iget v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondTries:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondTries:I

    .line 1202
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondHangRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1203
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialManager;->DIAL_CONNECTING:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;->dialName:Ljava/lang/String;

    .line 1204
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bus:Lcom/squareup/otto/Bus;

    sget-object v2, Lcom/navdy/hud/app/device/dial/DialManager;->DIAL_CONNECTING:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 1205
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->createBond()Z

    .line 1206
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondHangRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x7530

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private buildDialList()V
    .locals 8

    .prologue
    .line 975
    :try_start_0
    sget-object v4, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "[Dial]trying to find paired dials"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 976
    iget-object v4, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v0

    .line 977
    .local v0, "bondedDevices":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    sget-object v5, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[Dial]Bonded devices:"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    if-nez v0, :cond_1

    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 978
    new-instance v2, Ljava/util/ArrayList;

    const/16 v4, 0x8

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 979
    .local v2, "foundDevices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    .line 980
    .local v1, "device":Landroid/bluetooth/BluetoothDevice;
    sget-object v5, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[Dial]Bonded Device Address["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] name["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 981
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " type["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 982
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getType()I

    move-result v7

    invoke-static {v7}, Lcom/navdy/hud/app/bluetooth/utils/BluetoothUtils;->getDeviceType(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 980
    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 984
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/navdy/hud/app/device/dial/DialManager;->isDialDeviceName(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 985
    sget-object v5, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[Dial]found paired dial Address["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] name["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 986
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 998
    .end local v0    # "bondedDevices":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    .end local v1    # "device":Landroid/bluetooth/BluetoothDevice;
    .end local v2    # "foundDevices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/bluetooth/BluetoothDevice;>;"
    :catch_0
    move-exception v3

    .line 999
    .local v3, "t":Ljava/lang/Throwable;
    sget-object v4, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "[Dial]"

    invoke-virtual {v4, v5, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1001
    .end local v3    # "t":Ljava/lang/Throwable;
    :goto_2
    return-void

    .line 977
    .restart local v0    # "bondedDevices":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    :cond_1
    :try_start_1
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v4

    goto/16 :goto_0

    .line 989
    .restart local v2    # "foundDevices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/bluetooth/BluetoothDevice;>;"
    :cond_2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_3

    .line 990
    sget-object v4, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "[Dial]no dial device found"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_2

    .line 992
    :cond_3
    sget-object v4, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[Dial]found paired dials:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 993
    iget-object v5, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondedDials:Ljava/util/ArrayList;

    monitor-enter v5
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 994
    :try_start_2
    iget-object v4, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondedDials:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 995
    iget-object v4, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondedDials:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 996
    monitor-exit v5

    goto :goto_2

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v4
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
.end method

.method private checkDialConnections()V
    .locals 6

    .prologue
    .line 1005
    :try_start_0
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondedDials:Ljava/util/ArrayList;

    monitor-enter v3
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1006
    :try_start_1
    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondedDials:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 1007
    .local v0, "device":Landroid/bluetooth/BluetoothDevice;
    iget-object v4, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    new-instance v5, Lcom/navdy/hud/app/device/dial/DialManager$16;

    invoke-direct {v5, p0, v0}, Lcom/navdy/hud/app/device/dial/DialManager$16;-><init>(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothDevice;)V

    invoke-static {v4, v0, v5}, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->isDialConnected(Landroid/bluetooth/BluetoothAdapter;Landroid/bluetooth/BluetoothDevice;Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnectionStatus;)V

    goto :goto_0

    .line 1023
    .end local v0    # "device":Landroid/bluetooth/BluetoothDevice;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v2
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 1024
    :catch_0
    move-exception v1

    .line 1025
    .local v1, "t":Ljava/lang/Throwable;
    sget-object v2, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "[Dial]"

    invoke-virtual {v2, v3, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1027
    .end local v1    # "t":Ljava/lang/Throwable;
    :goto_1
    return-void

    .line 1023
    :cond_0
    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method private clearConnectedDial()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1484
    iput-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->firmWareVersion:Ljava/lang/String;

    .line 1485
    iput-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->hardwareVersion:Ljava/lang/String;

    .line 1486
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->lastKnownBatteryLevel:I

    .line 1487
    iput-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->lastKnownRawBatteryLevel:Ljava/lang/Integer;

    .line 1488
    iput-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->lastKnownSystemTemperature:Ljava/lang/Integer;

    .line 1489
    iput-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->connectedDial:Landroid/bluetooth/BluetoothDevice;

    .line 1490
    return-void
.end method

.method private connectEvent(Landroid/bluetooth/BluetoothDevice;)V
    .locals 4
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 1760
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[DialEvent-connect] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1762
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondHangRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1763
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->connectionErrorRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1765
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->connectedDial:Landroid/bluetooth/BluetoothDevice;

    if-eqz v0, :cond_0

    .line 1766
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[DialEvent-connect] ignore already connected dial:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager;->connectedDial:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1780
    :goto_0
    return-void

    .line 1770
    :cond_0
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/device/dial/DialManager;->isPresentInBondList(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1771
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "[DialEvent-connect] not on dial pairing screen, mark as connected"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1772
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->connectedDial:Landroid/bluetooth/BluetoothDevice;

    .line 1773
    invoke-direct {p0}, Lcom/navdy/hud/app/device/dial/DialManager;->handleDialConnection()V

    goto :goto_0

    .line 1777
    :cond_1
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "launch connectionErrorRunnable"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1778
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->tempConnectedDial:Landroid/bluetooth/BluetoothDevice;

    .line 1779
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->connectionErrorRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private disconectAndRemoveBond(Landroid/bluetooth/BluetoothDevice;Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialForgotten;I)V
    .locals 2
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;
    .param p2, "callback"    # Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialForgotten;
    .param p3, "delay"    # I

    .prologue
    .line 1242
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    new-instance v1, Lcom/navdy/hud/app/device/dial/DialManager$18;

    invoke-direct {v1, p0, p1, p3, p2}, Lcom/navdy/hud/app/device/dial/DialManager$18;-><init>(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothDevice;ILcom/navdy/hud/app/device/dial/DialManagerHelper$IDialForgotten;)V

    invoke-static {v0, p1, v1}, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->disconnectFromDial(Landroid/bluetooth/BluetoothAdapter;Landroid/bluetooth/BluetoothDevice;Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;)V

    .line 1259
    return-void
.end method

.method private disconnectEvent(Landroid/bluetooth/BluetoothDevice;)V
    .locals 3
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 1807
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondHangRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1808
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->connectionErrorRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1809
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->tempConnectedDial:Landroid/bluetooth/BluetoothDevice;

    .line 1811
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->connectedDial:Landroid/bluetooth/BluetoothDevice;

    if-nez v0, :cond_0

    .line 1812
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "[DialEvent-disconnect] no connected dial"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1828
    :goto_0
    return-void

    .line 1816
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->connectedDial:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1817
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[DialEvent-disconnect] notif not for connected dial connected["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager;->connectedDial:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 1821
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->connectedDial:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->disconnectedDial:Ljava/lang/String;

    .line 1822
    invoke-direct {p0}, Lcom/navdy/hud/app/device/dial/DialManager;->clearConnectedDial()V

    .line 1823
    invoke-direct {p0}, Lcom/navdy/hud/app/device/dial/DialManager;->stopGATTClient()V

    .line 1825
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->DIAL_NOT_CONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->disconnectedDial:Ljava/lang/String;

    iput-object v1, v0, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;->dialName:Ljava/lang/String;

    .line 1826
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bus:Lcom/squareup/otto/Bus;

    sget-object v1, Lcom/navdy/hud/app/device/dial/DialManager;->DIAL_NOT_CONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 1827
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "[DialEvent-disconnect] dial marked not connected"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private encryptionFailedEvent(Landroid/bluetooth/BluetoothDevice;)V
    .locals 4
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    const/16 v3, 0x7d0

    .line 1831
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/device/dial/DialManager;->isPresentInBondList(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1832
    iget v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->encryptionRetryCount:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 1833
    iget v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->encryptionRetryCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->encryptionRetryCount:I

    .line 1834
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[DialEvent-encryptfail] Attempt "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/navdy/hud/app/device/dial/DialManager;->encryptionRetryCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " will retry."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1835
    invoke-direct {p0}, Lcom/navdy/hud/app/device/dial/DialManager;->stopGATTClient()V

    .line 1836
    invoke-static {v3}, Lcom/navdy/hud/app/util/GenericUtil;->sleep(I)V

    .line 1837
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    new-instance v1, Lcom/navdy/hud/app/device/dial/DialManager$22;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/device/dial/DialManager$22;-><init>(Lcom/navdy/hud/app/device/dial/DialManager;)V

    invoke-static {v0, p1, v1}, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->disconnectFromDial(Landroid/bluetooth/BluetoothAdapter;Landroid/bluetooth/BluetoothDevice;Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;)V

    .line 1888
    :goto_0
    return-void

    .line 1849
    :cond_0
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/device/dial/DialManager;->removeFromBondList(Landroid/bluetooth/BluetoothDevice;)Z

    .line 1850
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "[DialEvent-encryptfail] device found in bonded dial list, stop gatt"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1851
    invoke-direct {p0}, Lcom/navdy/hud/app/device/dial/DialManager;->stopGATTClient()V

    .line 1853
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "[DialEvent-encryptfail] sleeping"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1854
    invoke-static {v3}, Lcom/navdy/hud/app/util/GenericUtil;->sleep(I)V

    .line 1855
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "[DialEvent-encryptfail] sleep"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1857
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    new-instance v1, Lcom/navdy/hud/app/device/dial/DialManager$23;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/device/dial/DialManager$23;-><init>(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothDevice;)V

    invoke-static {v0, p1, v1}, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->disconnectFromDial(Landroid/bluetooth/BluetoothAdapter;Landroid/bluetooth/BluetoothDevice;Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;)V

    goto :goto_0

    .line 1885
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->encryptionRetryCount:I

    .line 1886
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "[DialEventsListener] device not in bonded dial list"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getBatteryLevelCategory(I)Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;
    .locals 3
    .param p0, "level"    # I

    .prologue
    const/16 v2, 0x32

    const/16 v1, 0xa

    .line 1311
    if-le p0, v2, :cond_0

    const/16 v0, 0x50

    if-gt p0, v0, :cond_0

    .line 1312
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->LOW_BATTERY:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    .line 1318
    :goto_0
    return-object v0

    .line 1313
    :cond_0
    if-le p0, v1, :cond_1

    if-gt p0, v2, :cond_1

    .line 1314
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->VERY_LOW_BATTERY:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    goto :goto_0

    .line 1315
    :cond_1
    if-gt p0, v1, :cond_2

    .line 1316
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->EXTREMELY_LOW_BATTERY:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    goto :goto_0

    .line 1318
    :cond_2
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->OK_BATTERY:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    goto :goto_0
.end method

.method private getBluetoothDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 1519
    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondedDials:Ljava/util/ArrayList;

    monitor-enter v2

    .line 1520
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondedDials:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 1521
    .local v0, "device":Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1522
    monitor-exit v2

    .line 1527
    .end local v0    # "device":Landroid/bluetooth/BluetoothDevice;
    :goto_0
    return-object v0

    .line 1525
    :cond_1
    monitor-exit v2

    .line 1527
    const/4 v0, 0x0

    goto :goto_0

    .line 1525
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static getDisplayBatteryLevel(I)I
    .locals 2
    .param p0, "level"    # I

    .prologue
    .line 1323
    int-to-float v0, p0

    const v1, 0x3dcccccd    # 0.1f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public static getInstance()Lcom/navdy/hud/app/device/dial/DialManager;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->singleton:Lcom/navdy/hud/app/device/dial/DialManager;

    return-object v0
.end method

.method private handleDialConnection()V
    .locals 3

    .prologue
    .line 1398
    const/4 v1, 0x0

    iput v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->encryptionRetryCount:I

    .line 1399
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v0

    .line 1400
    .local v0, "toastManager":Lcom/navdy/hud/app/framework/toast/ToastManager;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialNotification;->dismissAllBatteryToasts()V

    .line 1401
    const-string v1, "dial-disconnect"

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->dismissCurrentToast(Ljava/lang/String;)V

    .line 1402
    const-string v1, "dial-disconnect"

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/toast/ToastManager;->clearPendingToast(Ljava/lang/String;)V

    .line 1403
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialManager;->DIAL_CONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

    invoke-virtual {p0}, Lcom/navdy/hud/app/device/dial/DialManager;->getDialName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;->dialName:Ljava/lang/String;

    .line 1404
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bus:Lcom/squareup/otto/Bus;

    sget-object v2, Lcom/navdy/hud/app/device/dial/DialManager;->DIAL_CONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 1405
    invoke-direct {p0}, Lcom/navdy/hud/app/device/dial/DialManager;->startGATTClient()V

    .line 1406
    return-void
.end method

.method private handleInputReportDescriptor(Landroid/bluetooth/BluetoothDevice;)V
    .locals 3
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 1783
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->connectionErrorRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1784
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[DialEvent-handleInputReportDescriptor] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1786
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->tempConnectedDial:Landroid/bluetooth/BluetoothDevice;

    if-nez v0, :cond_1

    .line 1787
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "[DialEvent-handleInputReportDescriptor] no temp connected dial"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1804
    :cond_0
    :goto_0
    return-void

    .line 1791
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->connectedDial:Landroid/bluetooth/BluetoothDevice;

    if-eqz v0, :cond_2

    .line 1792
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[DialEvent-handleInputReportDescriptor] dial already connected["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager;->connectedDial:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1793
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->connectedDial:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1794
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->DIAL_CONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

    invoke-virtual {p0}, Lcom/navdy/hud/app/device/dial/DialManager;->getDialName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;->dialName:Ljava/lang/String;

    .line 1795
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bus:Lcom/squareup/otto/Bus;

    sget-object v1, Lcom/navdy/hud/app/device/dial/DialManager;->DIAL_CONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 1800
    :cond_2
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "[DialEvent-handleInputReportDescriptor] done"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1801
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->tempConnectedDial:Landroid/bluetooth/BluetoothDevice;

    .line 1802
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->connectedDial:Landroid/bluetooth/BluetoothDevice;

    .line 1803
    invoke-direct {p0}, Lcom/navdy/hud/app/device/dial/DialManager;->handleDialConnection()V

    goto :goto_0
.end method

.method private handleScannedDevice(Landroid/bluetooth/BluetoothDevice;ILandroid/bluetooth/le/ScanRecord;)V
    .locals 8
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;
    .param p2, "rssi"    # I
    .param p3, "scanRecord"    # Landroid/bluetooth/le/ScanRecord;

    .prologue
    .line 1030
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getType()I

    move-result v5

    const/4 v6, 0x2

    if-eq v5, v6, :cond_1

    .line 1090
    :cond_0
    :goto_0
    return-void

    .line 1034
    :cond_1
    iget-boolean v5, p0, Lcom/navdy/hud/app/device/dial/DialManager;->isScanning:Z

    if-nez v5, :cond_2

    .line 1035
    sget-object v5, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[Dial]not scanning anymore ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 1038
    :cond_2
    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager;->scannedNavdyDials:Ljava/util/Map;

    monitor-enter v6

    .line 1039
    :try_start_0
    iget-object v5, p0, Lcom/navdy/hud/app/device/dial/DialManager;->scannedNavdyDials:Ljava/util/Map;

    invoke-interface {v5, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1041
    monitor-exit v6

    goto :goto_0

    .line 1043
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    :cond_3
    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1044
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v1

    .line 1045
    .local v1, "name":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1046
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/device/dial/DialManager;->isDialDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v2

    .line 1048
    .local v2, "nameMatched":Z
    if-nez v2, :cond_4

    .line 1049
    iget-object v5, p0, Lcom/navdy/hud/app/device/dial/DialManager;->ignoredNonNavdyDials:Ljava/util/Set;

    invoke-interface {v5, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1050
    sget-object v5, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[Dial]ignoring ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1051
    iget-object v5, p0, Lcom/navdy/hud/app/device/dial/DialManager;->ignoredNonNavdyDials:Ljava/util/Set;

    invoke-interface {v5, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1056
    :cond_4
    invoke-virtual {p3}, Landroid/bluetooth/le/ScanRecord;->getServiceUuids()Ljava/util/List;

    move-result-object v3

    .line 1057
    .local v3, "serviceUuids":Ljava/util/List;, "Ljava/util/List<Landroid/os/ParcelUuid;>;"
    if-nez v3, :cond_5

    .line 1058
    sget-object v5, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[Dial]no service uuid found ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1061
    :cond_5
    const/4 v0, 0x0

    .line 1062
    .local v0, "hidServiceFound":Z
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_6
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/ParcelUuid;

    .line 1063
    .local v4, "uuid":Landroid/os/ParcelUuid;
    invoke-virtual {v4}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v6

    sget-object v7, Lcom/navdy/hud/app/device/dial/DialConstants;->HID_SERVICE_UUID:Ljava/util/UUID;

    invoke-virtual {v6, v7}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1064
    const/4 v0, 0x1

    .line 1068
    .end local v4    # "uuid":Landroid/os/ParcelUuid;
    :cond_7
    if-nez v0, :cond_8

    .line 1069
    iget-object v5, p0, Lcom/navdy/hud/app/device/dial/DialManager;->ignoredNonNavdyDials:Ljava/util/Set;

    invoke-interface {v5, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1070
    sget-object v5, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[Dial]HID service not found ignoring ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1071
    iget-object v5, p0, Lcom/navdy/hud/app/device/dial/DialManager;->ignoredNonNavdyDials:Ljava/util/Set;

    invoke-interface {v5, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1075
    :cond_8
    sget-object v5, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[Dial]Scanned name["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] mac["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 1076
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] bonded["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 1077
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v7

    invoke-static {v7}, Lcom/navdy/hud/app/bluetooth/utils/BluetoothUtils;->getBondState(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] rssi["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " type["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 1079
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getType()I

    move-result v7

    invoke-static {v7}, Lcom/navdy/hud/app/bluetooth/utils/BluetoothUtils;->getDeviceType(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1075
    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1081
    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager;->scannedNavdyDials:Ljava/util/Map;

    monitor-enter v6

    .line 1082
    :try_start_2
    iget-object v5, p0, Lcom/navdy/hud/app/device/dial/DialManager;->scannedNavdyDials:Ljava/util/Map;

    invoke-interface {v5, p1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1083
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1084
    sget-object v5, Lcom/navdy/hud/app/device/dial/DialConstants;->PAIRING_RULE:Lcom/navdy/hud/app/device/dial/DialConstants$PairingRule;

    sget-object v6, Lcom/navdy/hud/app/device/dial/DialConstants$PairingRule;->FIRST:Lcom/navdy/hud/app/device/dial/DialConstants$PairingRule;

    if-ne v5, v6, :cond_0

    .line 1085
    sget-object v5, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "[Dial]pairing rule is first, stopping scan..."

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1086
    iget-object v5, p0, Lcom/navdy/hud/app/device/dial/DialManager;->handler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager;->stopScanRunnable:Ljava/lang/Runnable;

    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1087
    iget-object v5, p0, Lcom/navdy/hud/app/device/dial/DialManager;->handler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager;->stopScanRunnable:Ljava/lang/Runnable;

    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 1083
    :catchall_1
    move-exception v5

    :try_start_3
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v5
.end method

.method private declared-synchronized init()V
    .locals 4

    .prologue
    .line 928
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->getAddress()Ljava/lang/String;

    move-result-object v0

    .line 929
    .local v0, "macAddr":Ljava/lang/String;
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[Dial]Client Address:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " name:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 930
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->getBluetoothLeScanner()Landroid/bluetooth/le/BluetoothLeScanner;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bluetoothLEScanner:Landroid/bluetooth/le/BluetoothLeScanner;

    .line 931
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[Dial]btle scanner:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bluetoothLEScanner:Landroid/bluetooth/le/BluetoothLeScanner;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 933
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->handler:Landroid/os/Handler;

    new-instance v2, Lcom/navdy/hud/app/device/dial/DialManager$14;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/device/dial/DialManager$14;-><init>(Lcom/navdy/hud/app/device/dial/DialManager;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 951
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->handler:Landroid/os/Handler;

    new-instance v2, Lcom/navdy/hud/app/device/dial/DialManager$15;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/device/dial/DialManager$15;-><init>(Lcom/navdy/hud/app/device/dial/DialManager;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 967
    monitor-exit p0

    return-void

    .line 928
    .end local v0    # "macAddr":Ljava/lang/String;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public static isDialDeviceName(Ljava/lang/String;)Z
    .locals 3
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 1548
    if-nez p0, :cond_1

    .line 1558
    :cond_0
    :goto_0
    return v1

    .line 1552
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    sget-object v2, Lcom/navdy/hud/app/device/dial/DialConstants;->NAVDY_DIAL_FILTER:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 1553
    sget-object v2, Lcom/navdy/hud/app/device/dial/DialConstants;->NAVDY_DIAL_FILTER:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1554
    const/4 v1, 0x1

    goto :goto_0

    .line 1552
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private isPresentInBondList(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 5
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    const/4 v1, 0x0

    .line 1664
    if-nez p1, :cond_0

    .line 1676
    :goto_0
    return v1

    .line 1668
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondedDials:Ljava/util/ArrayList;

    monitor-enter v2

    .line 1669
    :try_start_0
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondedDials:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 1670
    .local v0, "d":Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1671
    const/4 v1, 0x1

    monitor-exit v2

    goto :goto_0

    .line 1674
    .end local v0    # "d":Landroid/bluetooth/BluetoothDevice;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_2
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private processBatteryLevel(I)V
    .locals 4
    .param p1, "level"    # I

    .prologue
    .line 1327
    invoke-static {p1}, Lcom/navdy/hud/app/device/dial/DialManager;->getBatteryLevelCategory(I)Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    move-result-object v0

    .line 1328
    .local v0, "reason":Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;
    iput p1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->lastKnownBatteryLevel:I

    .line 1330
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "battery status current["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] previous["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManager;->notificationReasonBattery:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1331
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->OK_BATTERY:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    if-ne v0, v1, :cond_1

    .line 1333
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->notificationReasonBattery:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->notificationReasonBattery:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    sget-object v2, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->OK_BATTERY:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    if-eq v1, v2, :cond_0

    .line 1335
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->notificationReasonBattery:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    .line 1337
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialNotification;->dismissAllBatteryToasts()V

    .line 1383
    :goto_0
    return-void

    .line 1340
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->notificationReasonBattery:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    if-ne v1, v0, :cond_2

    .line 1342
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "battery no change"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 1345
    :cond_2
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialManager$24;->$SwitchMap$com$navdy$hud$app$device$dial$DialConstants$NotificationReason:[I

    invoke-virtual {v0}, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1347
    :pswitch_0
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "battery notification change"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1348
    iput-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->notificationReasonBattery:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    goto :goto_0

    .line 1356
    :pswitch_1
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->notificationReasonBattery:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    sget-object v2, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->EXTREMELY_LOW_BATTERY:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    if-eq v1, v2, :cond_3

    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->notificationReasonBattery:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    sget-object v2, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->VERY_LOW_BATTERY:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    if-ne v1, v2, :cond_4

    .line 1358
    :cond_3
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "battery threshold change-ignore"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 1361
    :cond_4
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "battery notification change"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1362
    iput-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->notificationReasonBattery:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    goto :goto_0

    .line 1367
    :pswitch_2
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->notificationReasonBattery:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    sget-object v2, Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;->EXTREMELY_LOW_BATTERY:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    if-ne v1, v2, :cond_5

    .line 1368
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "battery threshold change-ignore"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 1371
    :cond_5
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "battery notification change"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1372
    iput-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->notificationReasonBattery:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    goto :goto_0

    .line 1345
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private processRawBatteryLevel(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "level"    # Ljava/lang/Integer;

    .prologue
    .line 1386
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->lastKnownRawBatteryLevel:Ljava/lang/Integer;

    .line 1387
    return-void
.end method

.method private processSystemTemperature(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "level"    # Ljava/lang/Integer;

    .prologue
    .line 1390
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->lastKnownSystemTemperature:Ljava/lang/Integer;

    .line 1391
    return-void
.end method

.method private registerReceivers()V
    .locals 3

    .prologue
    .line 1153
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 1154
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 1155
    .local v1, "intent":Landroid/content/IntentFilter;
    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bluetoothBondingReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1157
    new-instance v1, Landroid/content/IntentFilter;

    .end local v1    # "intent":Landroid/content/IntentFilter;
    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 1158
    .restart local v1    # "intent":Landroid/content/IntentFilter;
    const-string v2, "android.bluetooth.device.action.ACL_CONNECTED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1159
    const-string v2, "android.bluetooth.device.action.ACL_DISCONNECTED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1160
    const-string v2, "android.bluetooth.input.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1161
    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bluetoothConnectivityReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1163
    new-instance v1, Landroid/content/IntentFilter;

    .end local v1    # "intent":Landroid/content/IntentFilter;
    const-string v2, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 1164
    .restart local v1    # "intent":Landroid/content/IntentFilter;
    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bluetoothOnOffReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1165
    return-void
.end method

.method private removeBond(Landroid/bluetooth/BluetoothDevice;)V
    .locals 5
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 1263
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "removeBond"

    const/4 v2, 0x0

    check-cast v2, [Ljava/lang/Class;

    invoke-virtual {v3, v4, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 1264
    .local v1, "m":Ljava/lang/reflect/Method;
    if-eqz v1, :cond_0

    .line 1265
    sget-object v2, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[Dial]removingBond ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1266
    const/4 v2, 0x0

    check-cast v2, [Ljava/lang/Object;

    invoke-virtual {v1, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 1267
    sget-object v2, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "[Dial]removedBond"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1274
    .end local v1    # "m":Ljava/lang/reflect/Method;
    :goto_0
    return-void

    .line 1269
    .restart local v1    # "m":Ljava/lang/reflect/Method;
    :cond_0
    sget-object v2, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "[Dial]cannot get to removeBond api"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1271
    .end local v1    # "m":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v0

    .line 1272
    .local v0, "e":Ljava/lang/Throwable;
    sget-object v2, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "[Dial]"

    invoke-virtual {v2, v3, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private removeFromBondList(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 6
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    const/4 v2, 0x0

    .line 1643
    if-nez p1, :cond_0

    .line 1660
    :goto_0
    return v2

    .line 1647
    :cond_0
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondedDials:Ljava/util/ArrayList;

    monitor-enter v3

    .line 1648
    const/4 v1, 0x0

    .line 1649
    .local v1, "found":Z
    :try_start_0
    iget-object v4, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondedDials:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 1650
    .local v0, "d":Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1651
    const/4 v1, 0x1

    goto :goto_1

    .line 1654
    .end local v0    # "d":Landroid/bluetooth/BluetoothDevice;
    :cond_2
    if-eqz v1, :cond_3

    .line 1655
    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondedDials:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1656
    const/4 v2, 0x1

    monitor-exit v3

    goto :goto_0

    .line 1658
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_3
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private declared-synchronized startDialEventListener()V
    .locals 2

    .prologue
    .line 1680
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->dialEventsListenerThread:Ljava/lang/Thread;

    if-nez v0, :cond_0

    .line 1681
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/navdy/hud/app/device/dial/DialManager$21;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/device/dial/DialManager$21;-><init>(Lcom/navdy/hud/app/device/dial/DialManager;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->dialEventsListenerThread:Ljava/lang/Thread;

    .line 1750
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->dialEventsListenerThread:Ljava/lang/Thread;

    const-string v1, "dialEventListener"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 1751
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->dialEventsListenerThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 1752
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "dialEventListener thread started"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1754
    :cond_0
    monitor-exit p0

    return-void

    .line 1680
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized startGATTClient()V
    .locals 5

    .prologue
    .line 1212
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bluetoothGatt:Landroid/bluetooth/BluetoothGatt;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 1222
    :goto_0
    monitor-exit p0

    return-void

    .line 1215
    :cond_0
    :try_start_1
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "[Dial]startGATTClient, launched gatt"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1216
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->isGattOn:Z

    .line 1217
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->connectedDial:Landroid/bluetooth/BluetoothDevice;

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/navdy/hud/app/device/dial/DialManager;->gattCallback:Landroid/bluetooth/BluetoothGattCallback;

    invoke-virtual {v1, v2, v3, v4}, Landroid/bluetooth/BluetoothDevice;->connectGatt(Landroid/content/Context;ZLandroid/bluetooth/BluetoothGattCallback;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bluetoothGatt:Landroid/bluetooth/BluetoothGatt;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1218
    :catch_0
    move-exception v0

    .line 1219
    .local v0, "t":Ljava/lang/Throwable;
    :try_start_2
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "[Dial]"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1220
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->isGattOn:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1212
    .end local v0    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private declared-synchronized stopGATTClient()V
    .locals 3

    .prologue
    .line 1226
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bluetoothGatt:Landroid/bluetooth/BluetoothGatt;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez v1, :cond_0

    .line 1236
    const/4 v1, 0x0

    :try_start_1
    iput-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    .line 1237
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->isGattOn:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1239
    :goto_0
    monitor-exit p0

    return-void

    .line 1229
    :cond_0
    :try_start_2
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager;->batteryReadRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1230
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothGatt;->close()V

    .line 1231
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "[Dial]stopGATTClient, gatt closed"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1232
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->dialFirmwareUpdater:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;

    invoke-virtual {v1}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->cancelUpdate()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1236
    const/4 v1, 0x0

    :try_start_3
    iput-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    .line 1237
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->isGattOn:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 1226
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 1233
    :catch_0
    move-exception v0

    .line 1234
    .local v0, "t":Ljava/lang/Throwable;
    :try_start_4
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "[Dial]gatt closed"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1236
    const/4 v1, 0x0

    :try_start_5
    iput-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    .line 1237
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->isGattOn:Z

    goto :goto_0

    .line 1236
    .end local v0    # "t":Ljava/lang/Throwable;
    :catchall_1
    move-exception v1

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    .line 1237
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/navdy/hud/app/device/dial/DialManager;->isGattOn:Z

    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method


# virtual methods
.method public forgetAllDials(Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialForgotten;)V
    .locals 7
    .param p1, "callback"    # Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialForgotten;

    .prologue
    .line 1587
    invoke-virtual {p0}, Lcom/navdy/hud/app/device/dial/DialManager;->requestDialForgetKeys()V

    .line 1588
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManager;->handler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondHangRunnable:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1589
    iget-object v4, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondedDials:Ljava/util/ArrayList;

    monitor-enter v4

    .line 1590
    :try_start_0
    sget-object v3, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "forget all dials:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondedDials:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1591
    new-instance v0, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondedDials:Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1592
    .local v0, "copy":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/bluetooth/BluetoothDevice;>;"
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondedDials:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 1593
    invoke-direct {p0}, Lcom/navdy/hud/app/device/dial/DialManager;->clearConnectedDial()V

    .line 1595
    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 1597
    .local v1, "counter":Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/bluetooth/BluetoothDevice;

    .line 1598
    .local v2, "device":Landroid/bluetooth/BluetoothDevice;
    new-instance v5, Lcom/navdy/hud/app/device/dial/DialManager$20;

    invoke-direct {v5, p0, v1, p1}, Lcom/navdy/hud/app/device/dial/DialManager$20;-><init>(Lcom/navdy/hud/app/device/dial/DialManager;Ljava/util/concurrent/atomic/AtomicInteger;Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialForgotten;)V

    const/16 v6, 0x7d0

    invoke-direct {p0, v2, v5, v6}, Lcom/navdy/hud/app/device/dial/DialManager;->disconectAndRemoveBond(Landroid/bluetooth/BluetoothDevice;Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialForgotten;I)V

    goto :goto_0

    .line 1613
    .end local v0    # "copy":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/bluetooth/BluetoothDevice;>;"
    .end local v1    # "counter":Ljava/util/concurrent/atomic/AtomicInteger;
    .end local v2    # "device":Landroid/bluetooth/BluetoothDevice;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .restart local v0    # "copy":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/bluetooth/BluetoothDevice;>;"
    .restart local v1    # "counter":Ljava/util/concurrent/atomic/AtomicInteger;
    :cond_0
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1614
    return-void
.end method

.method public forgetAllDials(ZLcom/navdy/hud/app/device/dial/DialManagerHelper$IDialForgotten;)V
    .locals 7
    .param p1, "showToast"    # Z
    .param p2, "callback"    # Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialForgotten;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1562
    invoke-virtual {p0}, Lcom/navdy/hud/app/device/dial/DialManager;->getBondedDialCount()I

    move-result v0

    .line 1564
    .local v0, "count":I
    invoke-virtual {p0}, Lcom/navdy/hud/app/device/dial/DialManager;->getBondedDialList()Ljava/lang/String;

    move-result-object v1

    .line 1565
    .local v1, "dialName":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v5

    new-instance v6, Lcom/navdy/hud/app/device/dial/DialManager$19;

    invoke-direct {v6, p0, p2}, Lcom/navdy/hud/app/device/dial/DialManager$19;-><init>(Lcom/navdy/hud/app/device/dial/DialManager;Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialForgotten;)V

    invoke-virtual {v5, v6, v3}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 1572
    if-eqz p1, :cond_0

    .line 1573
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v2

    .line 1574
    .local v2, "toastManager":Lcom/navdy/hud/app/framework/toast/ToastManager;
    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/toast/ToastManager;->dismissCurrentToast()V

    .line 1575
    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/toast/ToastManager;->clearAllPendingToast()V

    .line 1576
    invoke-virtual {v2, v4}, Lcom/navdy/hud/app/framework/toast/ToastManager;->disableToasts(Z)V

    .line 1577
    if-le v0, v3, :cond_1

    :goto_0
    invoke-static {v3, v1}, Lcom/navdy/hud/app/device/dial/DialNotification;->showForgottenToast(ZLjava/lang/String;)V

    .line 1579
    .end local v2    # "toastManager":Lcom/navdy/hud/app/framework/toast/ToastManager;
    :cond_0
    return-void

    .restart local v2    # "toastManager":Lcom/navdy/hud/app/framework/toast/ToastManager;
    :cond_1
    move v3, v4

    .line 1577
    goto :goto_0
.end method

.method public forgetDial(Landroid/bluetooth/BluetoothDevice;)V
    .locals 0
    .param p1, "dial"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 1582
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/device/dial/DialManager;->removeBond(Landroid/bluetooth/BluetoothDevice;)V

    .line 1583
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/device/dial/DialManager;->removeFromBondList(Landroid/bluetooth/BluetoothDevice;)Z

    .line 1584
    return-void
.end method

.method public getBatteryNotificationReason()Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;
    .locals 1

    .prologue
    .line 1307
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->notificationReasonBattery:Lcom/navdy/hud/app/device/dial/DialConstants$NotificationReason;

    return-object v0
.end method

.method public getBondedDialCount()I
    .locals 2

    .prologue
    .line 1497
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondedDials:Ljava/util/ArrayList;

    monitor-enter v1

    .line 1498
    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondedDials:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    monitor-exit v1

    return v0

    .line 1499
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getBondedDialList()Ljava/lang/String;
    .locals 5

    .prologue
    .line 1503
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1505
    .local v0, "builder":Ljava/lang/StringBuilder;
    iget-object v4, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondedDials:Ljava/util/ArrayList;

    monitor-enter v4

    .line 1506
    :try_start_0
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondedDials:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1507
    .local v2, "len":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 1508
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondedDials:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1509
    add-int/lit8 v3, v1, 0x1

    if-ge v3, v2, :cond_0

    .line 1510
    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1507
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1513
    :cond_1
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1515
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 1513
    .end local v1    # "i":I
    .end local v2    # "len":I
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method

.method getDialDevice()Landroid/bluetooth/BluetoothDevice;
    .locals 1

    .prologue
    .line 970
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->connectedDial:Landroid/bluetooth/BluetoothDevice;

    return-object v0
.end method

.method public getDialFirmwareUpdater()Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;
    .locals 1

    .prologue
    .line 1468
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->dialFirmwareUpdater:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;

    return-object v0
.end method

.method public getDialName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1421
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->connectedDial:Landroid/bluetooth/BluetoothDevice;

    if-eqz v0, :cond_0

    .line 1422
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->connectedDial:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v0

    .line 1425
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFirmWareVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1472
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->firmWareVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getHardwareVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1476
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->hardwareVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getLastKnownBatteryLevel()I
    .locals 1

    .prologue
    .line 1409
    iget v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->lastKnownBatteryLevel:I

    return v0
.end method

.method public getLastKnownRawBatteryLevel()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1413
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->lastKnownRawBatteryLevel:Ljava/lang/Integer;

    return-object v0
.end method

.method public getLastKnownSystemTemperature()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1417
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->lastKnownSystemTemperature:Ljava/lang/Integer;

    return-object v0
.end method

.method public isDialConnected()Z
    .locals 1

    .prologue
    .line 1480
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->connectedDial:Landroid/bluetooth/BluetoothDevice;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDialDevice(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 3
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    const/4 v1, 0x0

    .line 1531
    if-nez p1, :cond_1

    .line 1544
    :cond_0
    :goto_0
    return v1

    .line 1535
    :cond_1
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v0

    .line 1536
    .local v0, "name":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 1540
    invoke-static {v0}, Lcom/navdy/hud/app/device/dial/DialManager;->isDialDeviceName(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1541
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isDialPaired()Z
    .locals 1

    .prologue
    .line 1493
    invoke-virtual {p0}, Lcom/navdy/hud/app/device/dial/DialManager;->getBondedDialCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 1394
    iget-boolean v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->dialManagerInitialized:Z

    return v0
.end method

.method public isScanning()Z
    .locals 1

    .prologue
    .line 1617
    iget-boolean v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->isScanning:Z

    return v0
.end method

.method public queueRead(Landroid/bluetooth/BluetoothGattCharacteristic;)V
    .locals 2
    .param p1, "characteristic"    # Landroid/bluetooth/BluetoothGattCharacteristic;

    .prologue
    .line 640
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->gattCharacteristicProcessor:Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;

    if-nez v0, :cond_0

    .line 641
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "gattCharacteristicProcessor not initialized"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 645
    :goto_0
    return-void

    .line 644
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->gattCharacteristicProcessor:Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;

    new-instance v1, Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor$ReadCommand;

    invoke-direct {v1, p1}, Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor$ReadCommand;-><init>(Landroid/bluetooth/BluetoothGattCharacteristic;)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;->process(Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor$Command;)V

    goto :goto_0
.end method

.method public queueWrite(Landroid/bluetooth/BluetoothGattCharacteristic;[B)V
    .locals 2
    .param p1, "characteristic"    # Landroid/bluetooth/BluetoothGattCharacteristic;
    .param p2, "data"    # [B

    .prologue
    .line 648
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->gattCharacteristicProcessor:Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;

    if-nez v0, :cond_0

    .line 649
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "gattCharacteristicProcessor not initialized"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 653
    :goto_0
    return-void

    .line 652
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager;->gattCharacteristicProcessor:Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;

    new-instance v1, Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor$WriteCommand;

    invoke-direct {v1, p1, p2}, Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor$WriteCommand;-><init>(Landroid/bluetooth/BluetoothGattCharacteristic;[B)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;->process(Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor$Command;)V

    goto :goto_0
.end method

.method public reportInputEvent(Landroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1429
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v0

    .line 1430
    .local v0, "device":Landroid/view/InputDevice;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/InputDevice;->isVirtual()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Landroid/view/InputDevice;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/navdy/hud/app/device/dial/DialManager;->isDialDeviceName(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1431
    :cond_0
    const/4 v2, 0x0

    .line 1464
    :goto_0
    return v2

    .line 1434
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/hud/app/device/dial/DialManager;->isDialConnected()Z

    move-result v2

    if-nez v2, :cond_5

    .line 1436
    sget-object v2, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "reportInputEvent:got dial connection via input:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/view/InputDevice;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1437
    invoke-virtual {v0}, Landroid/view/InputDevice;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/navdy/hud/app/device/dial/DialManager;->getBluetoothDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager;->connectedDial:Landroid/bluetooth/BluetoothDevice;

    .line 1438
    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager;->handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManager;->connectionErrorRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1439
    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager;->handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondHangRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1440
    sget-object v2, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "reportInputEvent remove connectionErrorRunnable"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1441
    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager;->connectedDial:Landroid/bluetooth/BluetoothDevice;

    if-eqz v2, :cond_3

    .line 1442
    sget-object v2, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "reportInputEvent: got device from bonded list"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1443
    invoke-direct {p0}, Lcom/navdy/hud/app/device/dial/DialManager;->handleDialConnection()V

    .line 1464
    :cond_2
    :goto_1
    const/4 v2, 0x1

    goto :goto_0

    .line 1445
    :cond_3
    sget-object v2, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "reportInputEvent: did not get device from bonded list"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 1446
    invoke-direct {p0}, Lcom/navdy/hud/app/device/dial/DialManager;->buildDialList()V

    .line 1447
    invoke-virtual {v0}, Landroid/view/InputDevice;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/navdy/hud/app/device/dial/DialManager;->getBluetoothDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager;->connectedDial:Landroid/bluetooth/BluetoothDevice;

    .line 1448
    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager;->connectedDial:Landroid/bluetooth/BluetoothDevice;

    if-eqz v2, :cond_4

    .line 1449
    sget-object v2, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "reportInputEvent: got device from new bonded list"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1450
    invoke-direct {p0}, Lcom/navdy/hud/app/device/dial/DialManager;->handleDialConnection()V

    goto :goto_1

    .line 1452
    :cond_4
    sget-object v2, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "reportInputEvent: did not get device from new bonded list"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_1

    .line 1456
    :cond_5
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getCurrentScreen()Lcom/navdy/hud/app/screen/BaseScreen;

    move-result-object v1

    .line 1457
    .local v1, "screen":Lcom/navdy/hud/app/screen/BaseScreen;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/navdy/hud/app/screen/BaseScreen;->getScreen()Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v2

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DIAL_PAIRING:Lcom/navdy/service/library/events/ui/Screen;

    if-ne v2, v3, :cond_2

    .line 1458
    sget-object v2, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "reportInputEvent: sent connected event"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1459
    sget-object v2, Lcom/navdy/hud/app/device/dial/DialManager;->DIAL_CONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

    invoke-virtual {p0}, Lcom/navdy/hud/app/device/dial/DialManager;->getDialName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;->dialName:Ljava/lang/String;

    .line 1460
    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bus:Lcom/squareup/otto/Bus;

    sget-object v3, Lcom/navdy/hud/app/device/dial/DialManager;->DIAL_CONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public requestDialForgetKeys()V
    .locals 3

    .prologue
    .line 1278
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->dialForgetKeysCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    if-eqz v1, :cond_0

    .line 1279
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Queueing dial forget keys request"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1280
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->dialForgetKeysCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    sget-object v2, Lcom/navdy/hud/app/device/dial/DialConstants;->DIAL_FORGET_KEYS_MAGIC:[B

    invoke-virtual {p0, v1, v2}, Lcom/navdy/hud/app/device/dial/DialManager;->queueWrite(Landroid/bluetooth/BluetoothGattCharacteristic;[B)V

    .line 1287
    :goto_0
    return-void

    .line 1282
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Not queuing dial forget keys request: null"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1284
    :catch_0
    move-exception v0

    .line 1285
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "[Dial]"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public requestDialReboot(Z)V
    .locals 8
    .param p1, "force"    # Z

    .prologue
    .line 1290
    if-nez p1, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v4, "last_dial_reboot"

    const-wide/16 v6, 0x0

    invoke-interface {v1, v4, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    sub-long/2addr v2, v4

    sget-wide v4, Lcom/navdy/hud/app/device/dial/DialConstants;->MIN_TIME_BETWEEN_REBOOTS:J

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 1291
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Not rebooting dial, last reboot too recent."

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1304
    :goto_0
    return-void

    .line 1295
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->dialRebootCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    if-eqz v1, :cond_1

    .line 1296
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Queueing dial reboot request"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1297
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->dialRebootCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;

    sget-object v2, Lcom/navdy/hud/app/device/dial/DialConstants;->DIAL_REBOOT_MAGIC:[B

    invoke-virtual {p0, v1, v2}, Lcom/navdy/hud/app/device/dial/DialManager;->queueWrite(Landroid/bluetooth/BluetoothGattCharacteristic;[B)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1301
    :catch_0
    move-exception v0

    .line 1302
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "[Dial]"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1299
    .end local v0    # "t":Ljava/lang/Throwable;
    :cond_1
    :try_start_1
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Not queuing dial reboot request: null"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public startScan()V
    .locals 6

    .prologue
    .line 1094
    :try_start_0
    iget-boolean v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->isScanning:Z

    if-eqz v1, :cond_0

    .line 1095
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "scan already running"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1113
    :goto_0
    return-void

    .line 1099
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager;->scanHangRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1100
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bondingDial:Landroid/bluetooth/BluetoothDevice;

    .line 1102
    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager;->scannedNavdyDials:Ljava/util/Map;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1103
    :try_start_1
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->scannedNavdyDials:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 1104
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->ignoredNonNavdyDials:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 1105
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1107
    :try_start_2
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bluetoothLEScanner:Landroid/bluetooth/le/BluetoothLeScanner;

    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager;->leScanCallback:Landroid/bluetooth/le/ScanCallback;

    invoke-virtual {v1, v2}, Landroid/bluetooth/le/BluetoothLeScanner;->startScan(Landroid/bluetooth/le/ScanCallback;)V

    .line 1108
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager;->scanHangRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x7530

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1109
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/hud/app/device/dial/DialManager;->isScanning:Z
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 1110
    :catch_0
    move-exception v0

    .line 1111
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1105
    .end local v0    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
.end method

.method public stopScan(Z)V
    .locals 9
    .param p1, "sendEvent"    # Z

    .prologue
    .line 1116
    iget-boolean v5, p0, Lcom/navdy/hud/app/device/dial/DialManager;->isScanning:Z

    if-eqz v5, :cond_2

    .line 1117
    sget-object v5, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "[Dial]stopping btle scan..."

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1118
    iget-object v5, p0, Lcom/navdy/hud/app/device/dial/DialManager;->handler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager;->scanHangRunnable:Ljava/lang/Runnable;

    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1119
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/navdy/hud/app/device/dial/DialManager;->isScanning:Z

    .line 1121
    :try_start_0
    iget-object v5, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bluetoothLEScanner:Landroid/bluetooth/le/BluetoothLeScanner;

    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager;->leScanCallback:Landroid/bluetooth/le/ScanCallback;

    invoke-virtual {v5, v6}, Landroid/bluetooth/le/BluetoothLeScanner;->stopScan(Landroid/bluetooth/le/ScanCallback;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1126
    :goto_0
    const/4 v2, 0x0

    .line 1127
    .local v2, "deviceToBond":Landroid/bluetooth/BluetoothDevice;
    const/4 v1, 0x0

    .line 1128
    .local v1, "deviceScanRecord":Landroid/bluetooth/le/ScanRecord;
    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager;->scannedNavdyDials:Ljava/util/Map;

    monitor-enter v6

    .line 1129
    :try_start_1
    iget-object v5, p0, Lcom/navdy/hud/app/device/dial/DialManager;->ignoredNonNavdyDials:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->clear()V

    .line 1130
    sget-object v5, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[Dial]stopscan dials found:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/navdy/hud/app/device/dial/DialManager;->scannedNavdyDials:Ljava/util/Map;

    invoke-interface {v8}, Ljava/util/Map;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1131
    iget-object v5, p0, Lcom/navdy/hud/app/device/dial/DialManager;->scannedNavdyDials:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v5

    if-nez v5, :cond_1

    .line 1132
    sget-object v5, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "[Dial]No dials found"

    invoke-virtual {v5, v7}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 1133
    if-eqz p1, :cond_0

    .line 1134
    iget-object v5, p0, Lcom/navdy/hud/app/device/dial/DialManager;->bus:Lcom/squareup/otto/Bus;

    sget-object v7, Lcom/navdy/hud/app/device/dial/DialManager;->DIAL_NOT_FOUND:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

    invoke-virtual {v5, v7}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 1136
    :cond_0
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1150
    .end local v1    # "deviceScanRecord":Landroid/bluetooth/le/ScanRecord;
    .end local v2    # "deviceToBond":Landroid/bluetooth/BluetoothDevice;
    :goto_1
    return-void

    .line 1122
    :catch_0
    move-exception v4

    .line 1123
    .local v4, "t":Ljava/lang/Throwable;
    sget-object v5, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v5, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1139
    .end local v4    # "t":Ljava/lang/Throwable;
    .restart local v1    # "deviceScanRecord":Landroid/bluetooth/le/ScanRecord;
    .restart local v2    # "deviceToBond":Landroid/bluetooth/BluetoothDevice;
    :cond_1
    :try_start_2
    iget-object v5, p0, Lcom/navdy/hud/app/device/dial/DialManager;->scannedNavdyDials:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 1140
    .local v3, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/le/ScanRecord;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    move-object v2, v0

    .line 1141
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Landroid/bluetooth/le/ScanRecord;

    move-object v1, v0

    .line 1142
    iget-object v5, p0, Lcom/navdy/hud/app/device/dial/DialManager;->scannedNavdyDials:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->clear()V

    .line 1144
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1146
    invoke-direct {p0, v2, v1}, Lcom/navdy/hud/app/device/dial/DialManager;->bondWithDial(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/le/ScanRecord;)V

    goto :goto_1

    .line 1144
    .end local v3    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/le/ScanRecord;>;"
    :catchall_0
    move-exception v5

    :try_start_3
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v5

    .line 1148
    .end local v1    # "deviceScanRecord":Landroid/bluetooth/le/ScanRecord;
    .end local v2    # "deviceToBond":Landroid/bluetooth/BluetoothDevice;
    :cond_2
    sget-object v5, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "[Dial]not currently scanning"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_1
.end method
