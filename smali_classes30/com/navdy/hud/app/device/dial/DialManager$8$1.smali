.class Lcom/navdy/hud/app/device/dial/DialManager$8$1;
.super Ljava/lang/Object;
.source "DialManager.java"

# interfaces
.implements Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialForgotten;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/device/dial/DialManager$8;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/device/dial/DialManager$8;

.field final synthetic val$dialName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/dial/DialManager$8;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/device/dial/DialManager$8;

    .prologue
    .line 375
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager$8$1;->this$1:Lcom/navdy/hud/app/device/dial/DialManager$8;

    iput-object p2, p0, Lcom/navdy/hud/app/device/dial/DialManager$8$1;->val$dialName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onForgotten()V
    .locals 2

    .prologue
    .line 378
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "connectionErrorRunnable: dial forgotten"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 379
    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->DIAL_CONNECTION_FAILED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->access$1300()Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager$8$1;->val$dialName:Ljava/lang/String;

    iput-object v1, v0, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;->dialName:Ljava/lang/String;

    .line 380
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager$8$1;->this$1:Lcom/navdy/hud/app/device/dial/DialManager$8;

    iget-object v0, v0, Lcom/navdy/hud/app/device/dial/DialManager$8;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v0}, Lcom/navdy/hud/app/device/dial/DialManager;->access$200(Lcom/navdy/hud/app/device/dial/DialManager;)Lcom/squareup/otto/Bus;

    move-result-object v0

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->DIAL_CONNECTION_FAILED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->access$1300()Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 381
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager$8$1;->this$1:Lcom/navdy/hud/app/device/dial/DialManager$8;

    iget-object v0, v0, Lcom/navdy/hud/app/device/dial/DialManager$8;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/device/dial/DialManager;->access$400(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/device/dial/DialManager$8$1$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/device/dial/DialManager$8$1$1;-><init>(Lcom/navdy/hud/app/device/dial/DialManager$8$1;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 389
    return-void
.end method
