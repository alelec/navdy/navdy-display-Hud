.class public Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;
.super Ljava/lang/Object;
.source "DialFirmwareUpdater.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Versions"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;
    }
.end annotation


# instance fields
.field public dial:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;

.field public local:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;

.field final synthetic this$0:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;


# direct methods
.method public constructor <init>(Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;)V
    .locals 1
    .param p1, "this$0"    # Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;->this$0:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;-><init>(Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;)V

    iput-object v0, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;->dial:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;

    .line 68
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;-><init>(Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;)V

    iput-object v0, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;->local:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;

    return-void
.end method

.method private isUpdateOK()Z
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;->dial:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;

    iget v0, v0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;->incrementalVersion:I

    const/16 v1, 0x1d

    if-ge v0, v1, :cond_0

    .line 82
    # getter for: Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "New dial firmware update logic is disabled (Current version < 29)"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 83
    const/4 v0, 0x0

    .line 86
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public isUpdateAvailable()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, -0x1

    .line 71
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;->local:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;

    iget v1, v1, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;->incrementalVersion:I

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;->dial:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;

    iget v1, v1, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;->incrementalVersion:I

    if-ne v1, v2, :cond_1

    .line 76
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;->dial:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;

    iget v1, v1, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;->incrementalVersion:I

    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;->local:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;

    iget v2, v2, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;->incrementalVersion:I

    if-ge v1, v2, :cond_0

    invoke-direct {p0}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;->isUpdateOK()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
