.class Lcom/navdy/hud/app/device/dial/DialManager$23;
.super Ljava/lang/Object;
.source "DialManager.java"

# interfaces
.implements Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/device/dial/DialManager;->encryptionFailedEvent(Landroid/bluetooth/BluetoothDevice;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/device/dial/DialManager;

.field final synthetic val$device:Landroid/bluetooth/BluetoothDevice;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothDevice;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 1857
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager$23;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    iput-object p2, p0, Lcom/navdy/hud/app/device/dial/DialManager$23;->val$device:Landroid/bluetooth/BluetoothDevice;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAttemptConnection(Z)V
    .locals 0
    .param p1, "success"    # Z

    .prologue
    .line 1859
    return-void
.end method

.method public onAttemptDisconnection(Z)V
    .locals 4
    .param p1, "success"    # Z

    .prologue
    .line 1863
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "[DialEvent-encryptfail] disconnected"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1864
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager$23;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/device/dial/DialManager;->access$400(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/device/dial/DialManager$23$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/device/dial/DialManager$23$1;-><init>(Lcom/navdy/hud/app/device/dial/DialManager$23;)V

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1881
    return-void
.end method
