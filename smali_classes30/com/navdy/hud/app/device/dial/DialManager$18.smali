.class Lcom/navdy/hud/app/device/dial/DialManager$18;
.super Ljava/lang/Object;
.source "DialManager.java"

# interfaces
.implements Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/device/dial/DialManager;->disconectAndRemoveBond(Landroid/bluetooth/BluetoothDevice;Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialForgotten;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/device/dial/DialManager;

.field final synthetic val$callback:Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialForgotten;

.field final synthetic val$delay:I

.field final synthetic val$device:Landroid/bluetooth/BluetoothDevice;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothDevice;ILcom/navdy/hud/app/device/dial/DialManagerHelper$IDialForgotten;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 1242
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager$18;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    iput-object p2, p0, Lcom/navdy/hud/app/device/dial/DialManager$18;->val$device:Landroid/bluetooth/BluetoothDevice;

    iput p3, p0, Lcom/navdy/hud/app/device/dial/DialManager$18;->val$delay:I

    iput-object p4, p0, Lcom/navdy/hud/app/device/dial/DialManager$18;->val$callback:Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialForgotten;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAttemptConnection(Z)V
    .locals 0
    .param p1, "success"    # Z

    .prologue
    .line 1244
    return-void
.end method

.method public onAttemptDisconnection(Z)V
    .locals 3
    .param p1, "success"    # Z

    .prologue
    .line 1248
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 1249
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "attempt dis-connection ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager$18;->val$device:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ] success["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1250
    iget v0, p0, Lcom/navdy/hud/app/device/dial/DialManager$18;->val$delay:I

    if-lez v0, :cond_0

    .line 1251
    iget v0, p0, Lcom/navdy/hud/app/device/dial/DialManager$18;->val$delay:I

    invoke-static {v0}, Lcom/navdy/hud/app/util/GenericUtil;->sleep(I)V

    .line 1253
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager$18;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager$18;->val$device:Landroid/bluetooth/BluetoothDevice;

    # invokes: Lcom/navdy/hud/app/device/dial/DialManager;->removeBond(Landroid/bluetooth/BluetoothDevice;)V
    invoke-static {v0, v1}, Lcom/navdy/hud/app/device/dial/DialManager;->access$4500(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothDevice;)V

    .line 1254
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager$18;->val$callback:Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialForgotten;

    if-eqz v0, :cond_1

    .line 1255
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager$18;->val$callback:Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialForgotten;

    invoke-interface {v0}, Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialForgotten;->onForgotten()V

    .line 1257
    :cond_1
    return-void
.end method
