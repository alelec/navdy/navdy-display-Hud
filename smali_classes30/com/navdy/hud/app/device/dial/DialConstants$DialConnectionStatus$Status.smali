.class public final enum Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;
.super Ljava/lang/Enum;
.source "DialConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Status"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

.field public static final enum CONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

.field public static final enum CONNECTING:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

.field public static final enum CONNECTION_FAILED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

.field public static final enum DISCONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

.field public static final enum NO_DIAL_FOUND:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 40
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

    const-string v1, "CONNECTED"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;->CONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

    .line 41
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

    const-string v1, "DISCONNECTED"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;->DISCONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

    .line 42
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

    const-string v1, "CONNECTING"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;->CONNECTING:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

    .line 43
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

    const-string v1, "CONNECTION_FAILED"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;->CONNECTION_FAILED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

    .line 44
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

    const-string v1, "NO_DIAL_FOUND"

    invoke-direct {v0, v1, v6}, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;->NO_DIAL_FOUND:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

    .line 39
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

    sget-object v1, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;->CONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;->DISCONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;->CONNECTING:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;->CONNECTION_FAILED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;->NO_DIAL_FOUND:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

    aput-object v1, v0, v6

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;->$VALUES:[Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 39
    const-class v0, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;->$VALUES:[Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus$Status;

    return-object v0
.end method
