.class Lcom/navdy/hud/app/device/dial/DialManager$7;
.super Landroid/content/BroadcastReceiver;
.source "DialManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/device/dial/DialManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/device/dial/DialManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/dial/DialManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 331
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager$7;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 335
    :try_start_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 336
    .local v0, "action":Ljava/lang/String;
    const-string v3, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 337
    const-string v3, "android.bluetooth.adapter.extra.STATE"

    const/high16 v4, -0x80000000

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 338
    .local v1, "state":I
    packed-switch v1, :pswitch_data_0

    .line 362
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "state":I
    :cond_0
    :goto_0
    return-void

    .line 340
    .restart local v0    # "action":Ljava/lang/String;
    .restart local v1    # "state":I
    :pswitch_0
    sget-object v3, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "[Dial]onoff: state off"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 359
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "state":I
    :catch_0
    move-exception v2

    .line 360
    .local v2, "t":Ljava/lang/Throwable;
    sget-object v3, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "[Dial]"

    invoke-virtual {v3, v4, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 344
    .end local v2    # "t":Ljava/lang/Throwable;
    .restart local v0    # "action":Ljava/lang/String;
    .restart local v1    # "state":I
    :pswitch_1
    :try_start_1
    sget-object v3, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "[Dial]onoff: state turning off"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 348
    :pswitch_2
    sget-object v3, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "[Dial]onoff: state on"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 349
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManager$7;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->dialManagerInitialized:Z
    invoke-static {v3}, Lcom/navdy/hud/app/device/dial/DialManager;->access$1000(Lcom/navdy/hud/app/device/dial/DialManager;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 350
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManager$7;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # invokes: Lcom/navdy/hud/app/device/dial/DialManager;->init()V
    invoke-static {v3}, Lcom/navdy/hud/app/device/dial/DialManager;->access$1100(Lcom/navdy/hud/app/device/dial/DialManager;)V

    goto :goto_0

    .line 355
    :pswitch_3
    sget-object v3, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "[Dial]onoff: state turning on"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 338
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
