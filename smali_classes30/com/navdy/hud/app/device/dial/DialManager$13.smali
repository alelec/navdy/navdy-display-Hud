.class Lcom/navdy/hud/app/device/dial/DialManager$13;
.super Landroid/bluetooth/BluetoothGattCallback;
.source "DialManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/device/dial/DialManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/device/dial/DialManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/dial/DialManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 656
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    invoke-direct {p0}, Landroid/bluetooth/BluetoothGattCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onCharacteristicChanged(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;)V
    .locals 2
    .param p1, "gatt"    # Landroid/bluetooth/BluetoothGatt;
    .param p2, "characteristic"    # Landroid/bluetooth/BluetoothGattCharacteristic;

    .prologue
    .line 821
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "[Dial]onCharacteristicChanged"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 822
    invoke-virtual {p0, p2}, Lcom/navdy/hud/app/device/dial/DialManager$13;->print(Landroid/bluetooth/BluetoothGattCharacteristic;)V

    .line 823
    return-void
.end method

.method public onCharacteristicRead(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;I)V
    .locals 3
    .param p1, "gatt"    # Landroid/bluetooth/BluetoothGatt;
    .param p2, "characteristic"    # Landroid/bluetooth/BluetoothGattCharacteristic;
    .param p3, "status"    # I

    .prologue
    .line 786
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->dialFirmwareUpdater:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;
    invoke-static {v0}, Lcom/navdy/hud/app/device/dial/DialManager;->access$3100(Lcom/navdy/hud/app/device/dial/DialManager;)Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->onCharacteristicRead(Landroid/bluetooth/BluetoothGattCharacteristic;I)V

    .line 788
    if-nez p3, :cond_1

    .line 789
    invoke-virtual {p0, p2}, Lcom/navdy/hud/app/device/dial/DialManager$13;->print(Landroid/bluetooth/BluetoothGattCharacteristic;)V

    .line 793
    :goto_0
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onCharacteristicRead finished"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 794
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->gattCharacteristicProcessor:Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;
    invoke-static {v0}, Lcom/navdy/hud/app/device/dial/DialManager;->access$2700(Lcom/navdy/hud/app/device/dial/DialManager;)Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 795
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->gattCharacteristicProcessor:Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;
    invoke-static {v0}, Lcom/navdy/hud/app/device/dial/DialManager;->access$2700(Lcom/navdy/hud/app/device/dial/DialManager;)Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;->commandFinished()V

    .line 797
    :cond_0
    return-void

    .line 791
    :cond_1
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Dial]onCharacteristicRead fail:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCharacteristicWrite(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;I)V
    .locals 6
    .param p1, "gatt"    # Landroid/bluetooth/BluetoothGatt;
    .param p2, "characteristic"    # Landroid/bluetooth/BluetoothGattCharacteristic;
    .param p3, "status"    # I

    .prologue
    .line 802
    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->dialFirmwareUpdater:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;
    invoke-static {v2}, Lcom/navdy/hud/app/device/dial/DialManager;->access$3100(Lcom/navdy/hud/app/device/dial/DialManager;)Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;

    move-result-object v2

    invoke-virtual {v2, p1, p2, p3}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->onCharacteristicWrite(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;I)V

    .line 803
    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->dialForgetKeysCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;
    invoke-static {v2}, Lcom/navdy/hud/app/device/dial/DialManager;->access$2800(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v2

    if-ne p2, v2, :cond_0

    .line 804
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGatt;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    .line 805
    .local v0, "device":Landroid/bluetooth/BluetoothDevice;
    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    invoke-virtual {v2, v0}, Lcom/navdy/hud/app/device/dial/DialManager;->forgetDial(Landroid/bluetooth/BluetoothDevice;)V

    .line 807
    .end local v0    # "device":Landroid/bluetooth/BluetoothDevice;
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->dialRebootCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;
    invoke-static {v2}, Lcom/navdy/hud/app/device/dial/DialManager;->access$2900(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v2

    if-ne p2, v2, :cond_2

    .line 808
    if-nez p3, :cond_4

    const/4 v1, 0x1

    .line 809
    .local v1, "successful":Z
    :goto_0
    if-eqz v1, :cond_1

    .line 810
    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->sharedPreferences:Landroid/content/SharedPreferences;
    invoke-static {v2}, Lcom/navdy/hud/app/device/dial/DialManager;->access$3200(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "last_dial_reboot"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 812
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/navdy/hud/app/device/dial/DialManager;->access$400(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGatt;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v1, v3, v4}, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->sendRebootLocalyticsEvent(Landroid/os/Handler;ZLandroid/bluetooth/BluetoothDevice;Ljava/lang/String;)V

    .line 814
    .end local v1    # "successful":Z
    :cond_2
    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->gattCharacteristicProcessor:Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;
    invoke-static {v2}, Lcom/navdy/hud/app/device/dial/DialManager;->access$2700(Lcom/navdy/hud/app/device/dial/DialManager;)Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 815
    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->gattCharacteristicProcessor:Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;
    invoke-static {v2}, Lcom/navdy/hud/app/device/dial/DialManager;->access$2700(Lcom/navdy/hud/app/device/dial/DialManager;)Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;->commandFinished()V

    .line 817
    :cond_3
    return-void

    .line 808
    :cond_4
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onConnectionStateChange(Landroid/bluetooth/BluetoothGatt;II)V
    .locals 6
    .param p1, "gatt"    # Landroid/bluetooth/BluetoothGatt;
    .param p2, "status"    # I
    .param p3, "newState"    # I

    .prologue
    const/4 v0, 0x0

    .line 661
    :try_start_0
    sget-object v3, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[Dial]gatt state change:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " newState="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 662
    const/4 v3, 0x2

    if-ne p3, v3, :cond_4

    .line 663
    if-eqz p1, :cond_3

    .line 664
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isNavdyDevice()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 667
    const-string v3, "persist.sys.bt.dial.pwr"

    invoke-static {v3}, Lcom/navdy/hud/app/util/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 668
    .local v1, "powerSetting":Ljava/lang/String;
    if-eqz v1, :cond_0

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->PowerMap:Ljava/util/Map;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->access$2600()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    move-object v0, v3

    .line 669
    .local v0, "powerLevel":Ljava/lang/Integer;
    :cond_0
    if-eqz v0, :cond_1

    .line 670
    sget-object v3, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[Dial] setting BT power to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 671
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v3}, Landroid/bluetooth/BluetoothGatt;->requestConnectionPriority(I)Z

    .line 674
    .end local v0    # "powerLevel":Ljava/lang/Integer;
    .end local v1    # "powerSetting":Ljava/lang/String;
    :cond_1
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    new-instance v4, Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;

    iget-object v5, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->handler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/navdy/hud/app/device/dial/DialManager;->access$400(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/os/Handler;

    move-result-object v5

    invoke-direct {v4, p1, v5}, Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;-><init>(Landroid/bluetooth/BluetoothGatt;Landroid/os/Handler;)V

    # setter for: Lcom/navdy/hud/app/device/dial/DialManager;->gattCharacteristicProcessor:Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;
    invoke-static {v3, v4}, Lcom/navdy/hud/app/device/dial/DialManager;->access$2702(Lcom/navdy/hud/app/device/dial/DialManager;Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;)Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;

    .line 675
    sget-object v3, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "[Dial]gatt connected, discover"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 676
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGatt;->discoverServices()Z

    .line 691
    :cond_2
    :goto_0
    return-void

    .line 678
    :cond_3
    sget-object v3, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "[Dial]gatt is null, cannot discover"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 688
    :catch_0
    move-exception v2

    .line 689
    .local v2, "t":Ljava/lang/Throwable;
    sget-object v3, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "[Dial]"

    invoke-virtual {v3, v4, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 680
    .end local v2    # "t":Ljava/lang/Throwable;
    :cond_4
    if-nez p3, :cond_2

    .line 681
    :try_start_1
    sget-object v3, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "[Dial]gatt disconnected"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 682
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->gattCharacteristicProcessor:Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;
    invoke-static {v3}, Lcom/navdy/hud/app/device/dial/DialManager;->access$2700(Lcom/navdy/hud/app/device/dial/DialManager;)Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 683
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->gattCharacteristicProcessor:Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;
    invoke-static {v3}, Lcom/navdy/hud/app/device/dial/DialManager;->access$2700(Lcom/navdy/hud/app/device/dial/DialManager;)Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;->release()V

    .line 684
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    const/4 v4, 0x0

    # setter for: Lcom/navdy/hud/app/device/dial/DialManager;->gattCharacteristicProcessor:Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;
    invoke-static {v3, v4}, Lcom/navdy/hud/app/device/dial/DialManager;->access$2702(Lcom/navdy/hud/app/device/dial/DialManager;Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;)Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;

    .line 686
    :cond_5
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # invokes: Lcom/navdy/hud/app/device/dial/DialManager;->stopGATTClient()V
    invoke-static {v3}, Lcom/navdy/hud/app/device/dial/DialManager;->access$1800(Lcom/navdy/hud/app/device/dial/DialManager;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public onDescriptorRead(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattDescriptor;I)V
    .locals 2
    .param p1, "gatt"    # Landroid/bluetooth/BluetoothGatt;
    .param p2, "descriptor"    # Landroid/bluetooth/BluetoothGattDescriptor;
    .param p3, "status"    # I

    .prologue
    .line 827
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "[Dial]onDescriptorRead"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 828
    return-void
.end method

.method public onDescriptorWrite(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattDescriptor;I)V
    .locals 3
    .param p1, "gatt"    # Landroid/bluetooth/BluetoothGatt;
    .param p2, "descriptor"    # Landroid/bluetooth/BluetoothGattDescriptor;
    .param p3, "status"    # I

    .prologue
    .line 832
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Dial]onDescriptorWrite : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez p3, :cond_0

    const-string v0, "success"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 833
    return-void

    .line 832
    :cond_0
    const-string v0, "fail"

    goto :goto_0
.end method

.method public onServicesDiscovered(Landroid/bluetooth/BluetoothGatt;I)V
    .locals 12
    .param p1, "gatt"    # Landroid/bluetooth/BluetoothGatt;
    .param p2, "status"    # I

    .prologue
    const/4 v8, 0x0

    .line 695
    iget-object v7, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # setter for: Lcom/navdy/hud/app/device/dial/DialManager;->hardwareVersionCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;
    invoke-static {v7, v8}, Lcom/navdy/hud/app/device/dial/DialManager;->access$2302(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothGattCharacteristic;)Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 696
    iget-object v7, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # setter for: Lcom/navdy/hud/app/device/dial/DialManager;->firmwareVersionCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;
    invoke-static {v7, v8}, Lcom/navdy/hud/app/device/dial/DialManager;->access$2202(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothGattCharacteristic;)Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 697
    iget-object v7, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # setter for: Lcom/navdy/hud/app/device/dial/DialManager;->hidHostReadyCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;
    invoke-static {v7, v8}, Lcom/navdy/hud/app/device/dial/DialManager;->access$2402(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothGattCharacteristic;)Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 698
    iget-object v7, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # setter for: Lcom/navdy/hud/app/device/dial/DialManager;->dialForgetKeysCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;
    invoke-static {v7, v8}, Lcom/navdy/hud/app/device/dial/DialManager;->access$2802(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothGattCharacteristic;)Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 699
    iget-object v7, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # setter for: Lcom/navdy/hud/app/device/dial/DialManager;->dialRebootCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;
    invoke-static {v7, v8}, Lcom/navdy/hud/app/device/dial/DialManager;->access$2902(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothGattCharacteristic;)Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 700
    iget-object v7, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # setter for: Lcom/navdy/hud/app/device/dial/DialManager;->batteryCharateristic:Landroid/bluetooth/BluetoothGattCharacteristic;
    invoke-static {v7, v8}, Lcom/navdy/hud/app/device/dial/DialManager;->access$1902(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothGattCharacteristic;)Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 701
    iget-object v7, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # setter for: Lcom/navdy/hud/app/device/dial/DialManager;->rawBatteryCharateristic:Landroid/bluetooth/BluetoothGattCharacteristic;
    invoke-static {v7, v8}, Lcom/navdy/hud/app/device/dial/DialManager;->access$2002(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothGattCharacteristic;)Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 702
    iget-object v7, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # setter for: Lcom/navdy/hud/app/device/dial/DialManager;->temperatureCharateristic:Landroid/bluetooth/BluetoothGattCharacteristic;
    invoke-static {v7, v8}, Lcom/navdy/hud/app/device/dial/DialManager;->access$2102(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothGattCharacteristic;)Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 705
    :try_start_0
    iget-object v7, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->isGattOn:Z
    invoke-static {v7}, Lcom/navdy/hud/app/device/dial/DialManager;->access$3000(Lcom/navdy/hud/app/device/dial/DialManager;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 706
    sget-object v7, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "gatt is not on but onServicesDiscovered called"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 782
    :cond_0
    :goto_0
    return-void

    .line 709
    :cond_1
    if-nez p2, :cond_11

    .line 710
    sget-object v7, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "[Dial]onServicesDiscovered"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 712
    iget-object v7, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->dialFirmwareUpdater:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;
    invoke-static {v7}, Lcom/navdy/hud/app/device/dial/DialManager;->access$3100(Lcom/navdy/hud/app/device/dial/DialManager;)Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;

    move-result-object v7

    invoke-virtual {v7, p1}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->onServicesDiscovered(Landroid/bluetooth/BluetoothGatt;)V

    .line 714
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGatt;->getServices()Ljava/util/List;

    move-result-object v4

    .line 715
    .local v4, "services":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothGattService;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/bluetooth/BluetoothGattService;

    .line 716
    .local v2, "service":Landroid/bluetooth/BluetoothGattService;
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothGattService;->getUuid()Ljava/util/UUID;

    move-result-object v6

    .line 717
    .local v6, "uuid":Ljava/util/UUID;
    sget-object v8, Lcom/navdy/hud/app/device/dial/DialConstants;->HID_SERVICE_UUID:Ljava/util/UUID;

    invoke-virtual {v8, v6}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 718
    sget-object v8, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Hid service found "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 719
    iget-object v8, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    sget-object v9, Lcom/navdy/hud/app/device/dial/DialConstants;->HID_HOST_READY_UUID:Ljava/util/UUID;

    invoke-virtual {v2, v9}, Landroid/bluetooth/BluetoothGattService;->getCharacteristic(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v9

    # setter for: Lcom/navdy/hud/app/device/dial/DialManager;->hidHostReadyCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;
    invoke-static {v8, v9}, Lcom/navdy/hud/app/device/dial/DialManager;->access$2402(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothGattCharacteristic;)Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 720
    iget-object v8, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->hidHostReadyCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;
    invoke-static {v8}, Lcom/navdy/hud/app/device/dial/DialManager;->access$2400(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v8

    if-nez v8, :cond_3

    .line 721
    sget-object v8, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "Hid host ready Characteristic is null"

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 726
    :goto_2
    iget-object v8, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->handler:Landroid/os/Handler;
    invoke-static {v8}, Lcom/navdy/hud/app/device/dial/DialManager;->access$400(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/os/Handler;

    move-result-object v8

    iget-object v9, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->hidHostReadyReadRunnable:Ljava/lang/Runnable;
    invoke-static {v9}, Lcom/navdy/hud/app/device/dial/DialManager;->access$3300(Lcom/navdy/hud/app/device/dial/DialManager;)Ljava/lang/Runnable;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 779
    .end local v2    # "service":Landroid/bluetooth/BluetoothGattService;
    .end local v4    # "services":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothGattService;>;"
    .end local v6    # "uuid":Ljava/util/UUID;
    :catch_0
    move-exception v5

    .line 780
    .local v5, "t":Ljava/lang/Throwable;
    sget-object v7, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "[Dial]"

    invoke-virtual {v7, v8, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 723
    .end local v5    # "t":Ljava/lang/Throwable;
    .restart local v2    # "service":Landroid/bluetooth/BluetoothGattService;
    .restart local v4    # "services":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothGattService;>;"
    .restart local v6    # "uuid":Ljava/util/UUID;
    :cond_3
    :try_start_1
    sget-object v8, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "Found Hid host ready Characteristic"

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 724
    iget-object v8, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->sharedPreferences:Landroid/content/SharedPreferences;
    invoke-static {v8}, Lcom/navdy/hud/app/device/dial/DialManager;->access$3200(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/content/SharedPreferences;

    move-result-object v8

    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    const-string v9, "first_run_dial_video_shown"

    const/4 v10, 0x1

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    invoke-interface {v8}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_2

    .line 727
    :cond_4
    sget-object v8, Lcom/navdy/hud/app/device/dial/DialConstants;->BATTERY_SERVICE_UUID:Ljava/util/UUID;

    invoke-virtual {v8, v6}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 728
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothGattService;->getUuid()Ljava/util/UUID;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    .line 729
    .local v3, "serviceUuid":Ljava/lang/String;
    sget-object v8, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[Dial]Service: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 730
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothGattService;->getCharacteristics()Ljava/util/List;

    move-result-object v1

    .line 731
    .local v1, "characteristics":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothGattCharacteristic;>;"
    if-nez v1, :cond_5

    .line 732
    sget-object v7, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "[Dial]No characteristic"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 735
    :cond_5
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_6
    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_9

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 736
    .local v0, "characteristic":Landroid/bluetooth/BluetoothGattCharacteristic;
    sget-object v9, Lcom/navdy/hud/app/device/dial/DialConstants;->BATTERY_LEVEL_UUID:Ljava/util/UUID;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 737
    iget-object v9, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # setter for: Lcom/navdy/hud/app/device/dial/DialManager;->batteryCharateristic:Landroid/bluetooth/BluetoothGattCharacteristic;
    invoke-static {v9, v0}, Lcom/navdy/hud/app/device/dial/DialManager;->access$1902(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothGattCharacteristic;)Landroid/bluetooth/BluetoothGattCharacteristic;

    goto :goto_3

    .line 738
    :cond_7
    sget-object v9, Lcom/navdy/hud/app/device/dial/DialConstants;->RAW_BATTERY_LEVEL_UUID:Ljava/util/UUID;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 739
    iget-object v9, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # setter for: Lcom/navdy/hud/app/device/dial/DialManager;->rawBatteryCharateristic:Landroid/bluetooth/BluetoothGattCharacteristic;
    invoke-static {v9, v0}, Lcom/navdy/hud/app/device/dial/DialManager;->access$2002(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothGattCharacteristic;)Landroid/bluetooth/BluetoothGattCharacteristic;

    goto :goto_3

    .line 740
    :cond_8
    sget-object v9, Lcom/navdy/hud/app/device/dial/DialConstants;->SYSTEM_TEMPERATURE_UUID:Ljava/util/UUID;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 741
    iget-object v9, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # setter for: Lcom/navdy/hud/app/device/dial/DialManager;->temperatureCharateristic:Landroid/bluetooth/BluetoothGattCharacteristic;
    invoke-static {v9, v0}, Lcom/navdy/hud/app/device/dial/DialManager;->access$2102(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothGattCharacteristic;)Landroid/bluetooth/BluetoothGattCharacteristic;

    goto :goto_3

    .line 744
    .end local v0    # "characteristic":Landroid/bluetooth/BluetoothGattCharacteristic;
    :cond_9
    iget-object v8, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->batteryCharateristic:Landroid/bluetooth/BluetoothGattCharacteristic;
    invoke-static {v8}, Lcom/navdy/hud/app/device/dial/DialManager;->access$1900(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v8

    if-eqz v8, :cond_a

    .line 745
    iget-object v8, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->handler:Landroid/os/Handler;
    invoke-static {v8}, Lcom/navdy/hud/app/device/dial/DialManager;->access$400(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/os/Handler;

    move-result-object v8

    iget-object v9, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->batteryReadRunnable:Ljava/lang/Runnable;
    invoke-static {v9}, Lcom/navdy/hud/app/device/dial/DialManager;->access$3400(Lcom/navdy/hud/app/device/dial/DialManager;)Ljava/lang/Runnable;

    move-result-object v9

    const-wide/16 v10, 0x1388

    invoke-virtual {v8, v9, v10, v11}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 747
    iget-object v8, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->handler:Landroid/os/Handler;
    invoke-static {v8}, Lcom/navdy/hud/app/device/dial/DialManager;->access$400(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/os/Handler;

    move-result-object v8

    iget-object v9, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->sendLocalyticsSuccessRunnable:Ljava/lang/Runnable;
    invoke-static {v9}, Lcom/navdy/hud/app/device/dial/DialManager;->access$3500(Lcom/navdy/hud/app/device/dial/DialManager;)Ljava/lang/Runnable;

    move-result-object v9

    const-wide/16 v10, 0x2710

    invoke-virtual {v8, v9, v10, v11}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_1

    .line 749
    :cond_a
    sget-object v8, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "[Dial] battery characteristic not found"

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 751
    .end local v1    # "characteristics":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothGattCharacteristic;>;"
    .end local v3    # "serviceUuid":Ljava/lang/String;
    :cond_b
    sget-object v8, Lcom/navdy/hud/app/device/dial/DialConstants;->DEVICE_INFO_SERVICE_UUID:Ljava/util/UUID;

    invoke-virtual {v8, v6}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_e

    .line 752
    sget-object v8, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Device info service found "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 753
    iget-object v8, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    sget-object v9, Lcom/navdy/hud/app/device/dial/DialConstants;->DEVICE_INFO_FIRMWARE_VERSION_UUID:Ljava/util/UUID;

    invoke-virtual {v2, v9}, Landroid/bluetooth/BluetoothGattService;->getCharacteristic(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v9

    # setter for: Lcom/navdy/hud/app/device/dial/DialManager;->firmwareVersionCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;
    invoke-static {v8, v9}, Lcom/navdy/hud/app/device/dial/DialManager;->access$2202(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothGattCharacteristic;)Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 754
    iget-object v8, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->firmwareVersionCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;
    invoke-static {v8}, Lcom/navdy/hud/app/device/dial/DialManager;->access$2200(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v8

    if-nez v8, :cond_c

    .line 755
    sget-object v8, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "Firmware Version Characteristic is null"

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 757
    :cond_c
    iget-object v8, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    sget-object v9, Lcom/navdy/hud/app/device/dial/DialConstants;->DEVICE_INFO_HARDWARE_VERSION_UUID:Ljava/util/UUID;

    invoke-virtual {v2, v9}, Landroid/bluetooth/BluetoothGattService;->getCharacteristic(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v9

    # setter for: Lcom/navdy/hud/app/device/dial/DialManager;->hardwareVersionCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;
    invoke-static {v8, v9}, Lcom/navdy/hud/app/device/dial/DialManager;->access$2302(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothGattCharacteristic;)Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 758
    iget-object v8, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->hardwareVersionCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;
    invoke-static {v8}, Lcom/navdy/hud/app/device/dial/DialManager;->access$2300(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v8

    if-nez v8, :cond_d

    .line 759
    sget-object v8, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "Hardware Version Characteristic is null"

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 761
    :cond_d
    iget-object v8, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->handler:Landroid/os/Handler;
    invoke-static {v8}, Lcom/navdy/hud/app/device/dial/DialManager;->access$400(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/os/Handler;

    move-result-object v8

    iget-object v9, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->deviceInfoGattReadRunnable:Ljava/lang/Runnable;
    invoke-static {v9}, Lcom/navdy/hud/app/device/dial/DialManager;->access$3600(Lcom/navdy/hud/app/device/dial/DialManager;)Ljava/lang/Runnable;

    move-result-object v9

    const-wide/16 v10, 0x1388

    invoke-virtual {v8, v9, v10, v11}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_1

    .line 762
    :cond_e
    sget-object v8, Lcom/navdy/hud/app/device/dial/DialConstants;->OTA_SERVICE_UUID:Ljava/util/UUID;

    invoke-virtual {v8, v6}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_10

    .line 763
    iget-object v8, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    sget-object v9, Lcom/navdy/hud/app/device/dial/DialConstants;->DIAL_FORGET_KEYS_UUID:Ljava/util/UUID;

    invoke-virtual {v2, v9}, Landroid/bluetooth/BluetoothGattService;->getCharacteristic(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v9

    # setter for: Lcom/navdy/hud/app/device/dial/DialManager;->dialForgetKeysCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;
    invoke-static {v8, v9}, Lcom/navdy/hud/app/device/dial/DialManager;->access$2802(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothGattCharacteristic;)Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 764
    iget-object v8, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->dialForgetKeysCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;
    invoke-static {v8}, Lcom/navdy/hud/app/device/dial/DialManager;->access$2800(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v8

    if-nez v8, :cond_f

    .line 765
    sget-object v8, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "Dial key forget is null"

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 767
    :cond_f
    iget-object v8, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    sget-object v9, Lcom/navdy/hud/app/device/dial/DialConstants;->DIAL_REBOOT_UUID:Ljava/util/UUID;

    invoke-virtual {v2, v9}, Landroid/bluetooth/BluetoothGattService;->getCharacteristic(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v9

    # setter for: Lcom/navdy/hud/app/device/dial/DialManager;->dialRebootCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;
    invoke-static {v8, v9}, Lcom/navdy/hud/app/device/dial/DialManager;->access$2902(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothGattCharacteristic;)Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 768
    iget-object v8, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->dialRebootCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;
    invoke-static {v8}, Lcom/navdy/hud/app/device/dial/DialManager;->access$2900(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v8

    if-nez v8, :cond_2

    .line 769
    sget-object v8, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "Dial reboot is null"

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 772
    :cond_10
    sget-object v8, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[Dial]Ignoring service:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 777
    .end local v2    # "service":Landroid/bluetooth/BluetoothGattService;
    .end local v4    # "services":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothGattService;>;"
    .end local v6    # "uuid":Ljava/util/UUID;
    :cond_11
    sget-object v7, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[Dial]onServicesDiscovered error:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method print(Landroid/bluetooth/BluetoothGattCharacteristic;)V
    .locals 7
    .param p1, "characteristic"    # Landroid/bluetooth/BluetoothGattCharacteristic;

    .prologue
    const/16 v6, 0x22

    const/4 v5, 0x0

    .line 836
    sget-object v3, Lcom/navdy/hud/app/device/dial/DialConstants;->HID_HOST_READY_UUID:Ljava/util/UUID;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 837
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getValue()[B

    move-result-object v1

    .line 838
    .local v1, "val":[B
    sget-object v3, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[Dial]onCharacteristicRead hid host ready, length "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 884
    .end local v1    # "val":[B
    :cond_0
    :goto_0
    return-void

    .line 839
    :cond_1
    sget-object v3, Lcom/navdy/hud/app/device/dial/DialConstants;->BATTERY_LEVEL_UUID:Ljava/util/UUID;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 840
    sget-object v3, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "[Dial]onCharacteristicRead battery"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 841
    const/16 v3, 0x21

    invoke-virtual {p1, v3, v5}, Landroid/bluetooth/BluetoothGattCharacteristic;->getIntValue(II)Ljava/lang/Integer;

    move-result-object v0

    .line 842
    .local v0, "level":Ljava/lang/Integer;
    if-eqz v0, :cond_2

    .line 843
    sget-object v3, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[Dial]battery level is ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] %"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 844
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    # invokes: Lcom/navdy/hud/app/device/dial/DialManager;->processBatteryLevel(I)V
    invoke-static {v3, v4}, Lcom/navdy/hud/app/device/dial/DialManager;->access$3700(Lcom/navdy/hud/app/device/dial/DialManager;I)V

    goto :goto_0

    .line 846
    :cond_2
    sget-object v3, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "[Dial]battery level no data"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 848
    .end local v0    # "level":Ljava/lang/Integer;
    :cond_3
    sget-object v3, Lcom/navdy/hud/app/device/dial/DialConstants;->RAW_BATTERY_LEVEL_UUID:Ljava/util/UUID;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 849
    sget-object v3, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "[Dial]onCharacteristicRead rawBattery"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 850
    invoke-virtual {p1, v6, v5}, Landroid/bluetooth/BluetoothGattCharacteristic;->getIntValue(II)Ljava/lang/Integer;

    move-result-object v0

    .line 851
    .restart local v0    # "level":Ljava/lang/Integer;
    if-eqz v0, :cond_4

    .line 852
    sget-object v3, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[Dial]raw battery level is ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 853
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # invokes: Lcom/navdy/hud/app/device/dial/DialManager;->processRawBatteryLevel(Ljava/lang/Integer;)V
    invoke-static {v3, v0}, Lcom/navdy/hud/app/device/dial/DialManager;->access$3800(Lcom/navdy/hud/app/device/dial/DialManager;Ljava/lang/Integer;)V

    goto/16 :goto_0

    .line 855
    :cond_4
    sget-object v3, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "[Dial]raw battery level no data"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 857
    .end local v0    # "level":Ljava/lang/Integer;
    :cond_5
    sget-object v3, Lcom/navdy/hud/app/device/dial/DialConstants;->SYSTEM_TEMPERATURE_UUID:Ljava/util/UUID;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 858
    sget-object v3, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "[Dial]onCharacteristicRead temperature"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 859
    invoke-virtual {p1, v6, v5}, Landroid/bluetooth/BluetoothGattCharacteristic;->getIntValue(II)Ljava/lang/Integer;

    move-result-object v0

    .line 860
    .restart local v0    # "level":Ljava/lang/Integer;
    if-eqz v0, :cond_6

    .line 861
    sget-object v3, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[Dial]system temperature is ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 862
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # invokes: Lcom/navdy/hud/app/device/dial/DialManager;->processSystemTemperature(Ljava/lang/Integer;)V
    invoke-static {v3, v0}, Lcom/navdy/hud/app/device/dial/DialManager;->access$3900(Lcom/navdy/hud/app/device/dial/DialManager;Ljava/lang/Integer;)V

    goto/16 :goto_0

    .line 864
    :cond_6
    sget-object v3, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "[Dial]system temperature no data"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 866
    .end local v0    # "level":Ljava/lang/Integer;
    :cond_7
    sget-object v3, Lcom/navdy/hud/app/device/dial/DialConstants;->DEVICE_INFO_HARDWARE_VERSION_UUID:Ljava/util/UUID;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 867
    sget-object v3, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "[Dial]onCharacteristicRead Hardware version"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 868
    invoke-virtual {p1, v5}, Landroid/bluetooth/BluetoothGattCharacteristic;->getStringValue(I)Ljava/lang/String;

    move-result-object v2

    .line 869
    .local v2, "value":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 870
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    # setter for: Lcom/navdy/hud/app/device/dial/DialManager;->hardwareVersion:Ljava/lang/String;
    invoke-static {v3, v4}, Lcom/navdy/hud/app/device/dial/DialManager;->access$4002(Lcom/navdy/hud/app/device/dial/DialManager;Ljava/lang/String;)Ljava/lang/String;

    .line 871
    sget-object v3, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Device Info hardware version received: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->hardwareVersion:Ljava/lang/String;
    invoke-static {v5}, Lcom/navdy/hud/app/device/dial/DialManager;->access$4000(Lcom/navdy/hud/app/device/dial/DialManager;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 873
    .end local v2    # "value":Ljava/lang/String;
    :cond_8
    sget-object v3, Lcom/navdy/hud/app/device/dial/DialConstants;->DEVICE_INFO_FIRMWARE_VERSION_UUID:Ljava/util/UUID;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 874
    sget-object v3, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "[Dial]onCharacteristicRead Firmware version"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 875
    invoke-virtual {p1, v5}, Landroid/bluetooth/BluetoothGattCharacteristic;->getStringValue(I)Ljava/lang/String;

    move-result-object v2

    .line 876
    .restart local v2    # "value":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 877
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    # setter for: Lcom/navdy/hud/app/device/dial/DialManager;->firmWareVersion:Ljava/lang/String;
    invoke-static {v3, v4}, Lcom/navdy/hud/app/device/dial/DialManager;->access$4102(Lcom/navdy/hud/app/device/dial/DialManager;Ljava/lang/String;)Ljava/lang/String;

    .line 878
    sget-object v3, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Device Info firmware version received: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/device/dial/DialManager$13;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->firmWareVersion:Ljava/lang/String;
    invoke-static {v5}, Lcom/navdy/hud/app/device/dial/DialManager;->access$4100(Lcom/navdy/hud/app/device/dial/DialManager;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 882
    .end local v2    # "value":Ljava/lang/String;
    :cond_9
    sget-object v3, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[Dial]onCharacteristicRead:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
