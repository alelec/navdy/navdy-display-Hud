.class Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor$1;
.super Ljava/lang/Object;
.source "DialManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;->submitNext()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;

.field final synthetic val$command:Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor$Command;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor$Command;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;

    .prologue
    .line 608
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;

    iput-object p2, p0, Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor$1;->val$command:Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor$Command;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 612
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;->bluetoothGatt:Landroid/bluetooth/BluetoothGatt;
    invoke-static {v1}, Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;->access$2500(Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 613
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor$1;->val$command:Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor$Command;

    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;->bluetoothGatt:Landroid/bluetooth/BluetoothGatt;
    invoke-static {v2}, Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;->access$2500(Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/device/dial/DialManager$CharacteristicCommandProcessor$Command;->process(Landroid/bluetooth/BluetoothGatt;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 618
    :cond_0
    :goto_0
    return-void

    .line 615
    :catch_0
    move-exception v0

    .line 616
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Error while executing GATT command"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
