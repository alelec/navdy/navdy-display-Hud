.class Lcom/navdy/hud/app/device/dial/DialManager$9;
.super Landroid/content/BroadcastReceiver;
.source "DialManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/device/dial/DialManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/device/dial/DialManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/dial/DialManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 398
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager$9;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 402
    if-nez p2, :cond_1

    .line 484
    :cond_0
    :goto_0
    return-void

    .line 406
    :cond_1
    :try_start_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 407
    .local v0, "action":Ljava/lang/String;
    const-string v6, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    .line 408
    .local v1, "device":Landroid/bluetooth/BluetoothDevice;
    if-eqz v1, :cond_0

    .line 412
    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager$9;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    invoke-virtual {v6, v1}, Lcom/navdy/hud/app/device/dial/DialManager;->isDialDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 413
    sget-object v6, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[Dial]btConnRecvr: notification not for dial:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " addr:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 414
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 413
    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 481
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "device":Landroid/bluetooth/BluetoothDevice;
    :catch_0
    move-exception v5

    .line 482
    .local v5, "t":Ljava/lang/Throwable;
    sget-object v6, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "[Dial]"

    invoke-virtual {v6, v7, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 418
    .end local v5    # "t":Ljava/lang/Throwable;
    .restart local v0    # "action":Ljava/lang/String;
    .restart local v1    # "device":Landroid/bluetooth/BluetoothDevice;
    :cond_2
    :try_start_1
    sget-object v6, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[Dial]btConnRecvr ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] action ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 420
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isNavdyDevice()Z

    move-result v6

    if-nez v6, :cond_0

    .line 425
    const/4 v2, 0x0

    .line 427
    .local v2, "isConnected":Z
    const-string v6, "android.bluetooth.input.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 428
    const-string v6, "android.bluetooth.profile.extra.PREVIOUS_STATE"

    const/4 v7, -0x1

    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 429
    .local v4, "oldState":I
    const-string v6, "android.bluetooth.profile.extra.STATE"

    const/4 v7, -0x1

    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 430
    .local v3, "newState":I
    sget-object v6, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[Dial]btConnRecvr: dial connection state changed old["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] new["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 431
    const/4 v6, 0x2

    if-ne v3, v6, :cond_3

    .line 433
    const/4 v2, 0x1

    .line 437
    .end local v3    # "newState":I
    .end local v4    # "oldState":I
    :cond_3
    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager$9;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->connectedDial:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v6}, Lcom/navdy/hud/app/device/dial/DialManager;->access$300(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v6

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager$9;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->connectedDial:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v6}, Lcom/navdy/hud/app/device/dial/DialManager;->access$300(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v6

    invoke-virtual {v6}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 438
    sget-object v6, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "btConnRecvr: notification for not current connected dial"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 439
    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager$9;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->handler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/navdy/hud/app/device/dial/DialManager;->access$400(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/os/Handler;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/device/dial/DialManager$9;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->bondHangRunnable:Ljava/lang/Runnable;
    invoke-static {v7}, Lcom/navdy/hud/app/device/dial/DialManager;->access$600(Lcom/navdy/hud/app/device/dial/DialManager;)Ljava/lang/Runnable;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 443
    :cond_4
    const-string v6, "android.bluetooth.device.action.ACL_CONNECTED"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    if-eqz v2, :cond_7

    .line 444
    :cond_5
    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager$9;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->handler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/navdy/hud/app/device/dial/DialManager;->access$400(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/os/Handler;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/device/dial/DialManager$9;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->bondHangRunnable:Ljava/lang/Runnable;
    invoke-static {v7}, Lcom/navdy/hud/app/device/dial/DialManager;->access$600(Lcom/navdy/hud/app/device/dial/DialManager;)Ljava/lang/Runnable;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 445
    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager$9;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # setter for: Lcom/navdy/hud/app/device/dial/DialManager;->connectedDial:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v6, v1}, Lcom/navdy/hud/app/device/dial/DialManager;->access$302(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;

    .line 446
    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager$9;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # invokes: Lcom/navdy/hud/app/device/dial/DialManager;->addtoBondList(Landroid/bluetooth/BluetoothDevice;)Z
    invoke-static {v6, v1}, Lcom/navdy/hud/app/device/dial/DialManager;->access$700(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothDevice;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 448
    sget-object v6, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "attempt connection reqd"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 449
    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager$9;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v6}, Lcom/navdy/hud/app/device/dial/DialManager;->access$800(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v6

    new-instance v7, Lcom/navdy/hud/app/device/dial/DialManager$9$1;

    invoke-direct {v7, p0, v1}, Lcom/navdy/hud/app/device/dial/DialManager$9$1;-><init>(Lcom/navdy/hud/app/device/dial/DialManager$9;Landroid/bluetooth/BluetoothDevice;)V

    invoke-static {v6, v1, v7}, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->connectToDial(Landroid/bluetooth/BluetoothAdapter;Landroid/bluetooth/BluetoothDevice;Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;)V

    goto/16 :goto_0

    .line 463
    :cond_6
    sget-object v6, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "attempt connection not read"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 464
    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager$9;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # invokes: Lcom/navdy/hud/app/device/dial/DialManager;->handleDialConnection()V
    invoke-static {v6}, Lcom/navdy/hud/app/device/dial/DialManager;->access$1500(Lcom/navdy/hud/app/device/dial/DialManager;)V

    goto/16 :goto_0

    .line 466
    :cond_7
    const-string v6, "android.bluetooth.device.action.ACL_DISCONNECTED"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 467
    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager$9;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->handler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/navdy/hud/app/device/dial/DialManager;->access$400(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/os/Handler;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/device/dial/DialManager$9;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->bondHangRunnable:Ljava/lang/Runnable;
    invoke-static {v7}, Lcom/navdy/hud/app/device/dial/DialManager;->access$600(Lcom/navdy/hud/app/device/dial/DialManager;)Ljava/lang/Runnable;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 468
    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager$9;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->connectedDial:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v6}, Lcom/navdy/hud/app/device/dial/DialManager;->access$300(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v6

    if-eqz v6, :cond_8

    .line 469
    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager$9;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    iget-object v7, p0, Lcom/navdy/hud/app/device/dial/DialManager$9;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->connectedDial:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v7}, Lcom/navdy/hud/app/device/dial/DialManager;->access$300(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v7

    invoke-virtual {v7}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v7

    # setter for: Lcom/navdy/hud/app/device/dial/DialManager;->disconnectedDial:Ljava/lang/String;
    invoke-static {v6, v7}, Lcom/navdy/hud/app/device/dial/DialManager;->access$1602(Lcom/navdy/hud/app/device/dial/DialManager;Ljava/lang/String;)Ljava/lang/String;

    .line 471
    :cond_8
    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->DIAL_NOT_CONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->access$100()Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/device/dial/DialManager$9;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->disconnectedDial:Ljava/lang/String;
    invoke-static {v7}, Lcom/navdy/hud/app/device/dial/DialManager;->access$1600(Lcom/navdy/hud/app/device/dial/DialManager;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;->dialName:Ljava/lang/String;

    .line 472
    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager$9;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v6}, Lcom/navdy/hud/app/device/dial/DialManager;->access$200(Lcom/navdy/hud/app/device/dial/DialManager;)Lcom/squareup/otto/Bus;

    move-result-object v6

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->DIAL_NOT_CONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->access$100()Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 473
    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager$9;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    invoke-virtual {v6}, Lcom/navdy/hud/app/device/dial/DialManager;->getBondedDialCount()I

    move-result v6

    if-lez v6, :cond_9

    .line 477
    :cond_9
    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager$9;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # invokes: Lcom/navdy/hud/app/device/dial/DialManager;->clearConnectedDial()V
    invoke-static {v6}, Lcom/navdy/hud/app/device/dial/DialManager;->access$1700(Lcom/navdy/hud/app/device/dial/DialManager;)V

    .line 478
    iget-object v6, p0, Lcom/navdy/hud/app/device/dial/DialManager$9;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # invokes: Lcom/navdy/hud/app/device/dial/DialManager;->stopGATTClient()V
    invoke-static {v6}, Lcom/navdy/hud/app/device/dial/DialManager;->access$1800(Lcom/navdy/hud/app/device/dial/DialManager;)V

    .line 479
    sget-object v6, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[Dial]btConnRecvr: dial NOT connected ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] addr:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
