.class Lcom/navdy/hud/app/device/dial/DialManager$23$1;
.super Ljava/lang/Object;
.source "DialManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/device/dial/DialManager$23;->onAttemptDisconnection(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/device/dial/DialManager$23;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/dial/DialManager$23;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/device/dial/DialManager$23;

    .prologue
    .line 1864
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager$23$1;->this$1:Lcom/navdy/hud/app/device/dial/DialManager$23;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1867
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager$23$1;->this$1:Lcom/navdy/hud/app/device/dial/DialManager$23;

    iget-object v0, v0, Lcom/navdy/hud/app/device/dial/DialManager$23;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager$23$1;->this$1:Lcom/navdy/hud/app/device/dial/DialManager$23;

    iget-object v2, v2, Lcom/navdy/hud/app/device/dial/DialManager$23;->val$device:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/device/dial/DialManager;->forgetDial(Landroid/bluetooth/BluetoothDevice;)V

    .line 1868
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[DialEvent-encryptfail] bond removed :-/ connected["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManager$23$1;->this$1:Lcom/navdy/hud/app/device/dial/DialManager$23;

    iget-object v3, v3, Lcom/navdy/hud/app/device/dial/DialManager$23;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->connectedDial:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v3}, Lcom/navdy/hud/app/device/dial/DialManager;->access$300(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] event["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManager$23$1;->this$1:Lcom/navdy/hud/app/device/dial/DialManager$23;

    iget-object v3, v3, Lcom/navdy/hud/app/device/dial/DialManager$23;->val$device:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1870
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager$23$1;->this$1:Lcom/navdy/hud/app/device/dial/DialManager$23;

    iget-object v0, v0, Lcom/navdy/hud/app/device/dial/DialManager$23;->val$device:Landroid/bluetooth/BluetoothDevice;

    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager$23$1;->this$1:Lcom/navdy/hud/app/device/dial/DialManager$23;

    iget-object v2, v2, Lcom/navdy/hud/app/device/dial/DialManager$23;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->connectedDial:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v2}, Lcom/navdy/hud/app/device/dial/DialManager;->access$300(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1871
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager$23$1;->this$1:Lcom/navdy/hud/app/device/dial/DialManager$23;

    iget-object v0, v0, Lcom/navdy/hud/app/device/dial/DialManager$23;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager$23$1;->this$1:Lcom/navdy/hud/app/device/dial/DialManager$23;

    iget-object v2, v2, Lcom/navdy/hud/app/device/dial/DialManager$23;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->connectedDial:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v2}, Lcom/navdy/hud/app/device/dial/DialManager;->access$300(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/navdy/hud/app/device/dial/DialManager;->disconnectedDial:Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/navdy/hud/app/device/dial/DialManager;->access$1602(Lcom/navdy/hud/app/device/dial/DialManager;Ljava/lang/String;)Ljava/lang/String;

    .line 1872
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager$23$1;->this$1:Lcom/navdy/hud/app/device/dial/DialManager$23;

    iget-object v0, v0, Lcom/navdy/hud/app/device/dial/DialManager$23;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # invokes: Lcom/navdy/hud/app/device/dial/DialManager;->clearConnectedDial()V
    invoke-static {v0}, Lcom/navdy/hud/app/device/dial/DialManager;->access$1700(Lcom/navdy/hud/app/device/dial/DialManager;)V

    .line 1873
    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->DIAL_CONNECTION_FAILED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->access$1300()Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

    move-result-object v0

    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager$23$1;->this$1:Lcom/navdy/hud/app/device/dial/DialManager$23;

    iget-object v2, v2, Lcom/navdy/hud/app/device/dial/DialManager$23;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->disconnectedDial:Ljava/lang/String;
    invoke-static {v2}, Lcom/navdy/hud/app/device/dial/DialManager;->access$1600(Lcom/navdy/hud/app/device/dial/DialManager;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;->dialName:Ljava/lang/String;

    .line 1874
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager$23$1;->this$1:Lcom/navdy/hud/app/device/dial/DialManager$23;

    iget-object v0, v0, Lcom/navdy/hud/app/device/dial/DialManager$23;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v0}, Lcom/navdy/hud/app/device/dial/DialManager;->access$200(Lcom/navdy/hud/app/device/dial/DialManager;)Lcom/squareup/otto/Bus;

    move-result-object v0

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->DIAL_CONNECTION_FAILED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->access$1300()Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 1875
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "[DialEvent-encryptfail] current dial is disconnected"

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1878
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager$23$1;->this$1:Lcom/navdy/hud/app/device/dial/DialManager$23;

    iget-object v0, v0, Lcom/navdy/hud/app/device/dial/DialManager$23;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/device/dial/DialManager;->access$400(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/os/Handler;

    move-result-object v0

    const-string v5, "Encryption failed, deleting bond"

    move v2, v1

    move v3, v1

    move v4, v1

    invoke-static/range {v0 .. v5}, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->sendLocalyticsEvent(Landroid/os/Handler;ZZIZLjava/lang/String;)V

    .line 1879
    return-void
.end method
