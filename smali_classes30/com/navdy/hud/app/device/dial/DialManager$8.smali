.class Lcom/navdy/hud/app/device/dial/DialManager$8;
.super Ljava/lang/Object;
.source "DialManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/device/dial/DialManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/device/dial/DialManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/dial/DialManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 369
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager$8;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 372
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "connectionErrorRunnable no input descriptor event:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManager$8;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->tempConnectedDial:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v3}, Lcom/navdy/hud/app/device/dial/DialManager;->access$1200(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 373
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager$8;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->tempConnectedDial:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v1}, Lcom/navdy/hud/app/device/dial/DialManager;->access$1200(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 374
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager$8;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->tempConnectedDial:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v1}, Lcom/navdy/hud/app/device/dial/DialManager;->access$1200(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v0

    .line 375
    .local v0, "dialName":Ljava/lang/String;
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager$8;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager$8;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->tempConnectedDial:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v2}, Lcom/navdy/hud/app/device/dial/DialManager;->access$1200(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    new-instance v3, Lcom/navdy/hud/app/device/dial/DialManager$8$1;

    invoke-direct {v3, p0, v0}, Lcom/navdy/hud/app/device/dial/DialManager$8$1;-><init>(Lcom/navdy/hud/app/device/dial/DialManager$8;Ljava/lang/String;)V

    const/16 v4, 0x7d0

    # invokes: Lcom/navdy/hud/app/device/dial/DialManager;->disconectAndRemoveBond(Landroid/bluetooth/BluetoothDevice;Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialForgotten;I)V
    invoke-static {v1, v2, v3, v4}, Lcom/navdy/hud/app/device/dial/DialManager;->access$1400(Lcom/navdy/hud/app/device/dial/DialManager;Landroid/bluetooth/BluetoothDevice;Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialForgotten;I)V

    .line 394
    .end local v0    # "dialName":Ljava/lang/String;
    :goto_0
    return-void

    .line 392
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "connectionErrorRunnable no temp connected dial"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method
