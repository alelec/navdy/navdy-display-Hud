.class Lcom/navdy/hud/app/device/dial/DialManager$22;
.super Ljava/lang/Object;
.source "DialManager.java"

# interfaces
.implements Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/device/dial/DialManager;->encryptionFailedEvent(Landroid/bluetooth/BluetoothDevice;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/device/dial/DialManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/dial/DialManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 1837
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager$22;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAttemptConnection(Z)V
    .locals 0
    .param p1, "success"    # Z

    .prologue
    .line 1840
    return-void
.end method

.method public onAttemptDisconnection(Z)V
    .locals 6
    .param p1, "success"    # Z

    .prologue
    const/4 v1, 0x0

    .line 1845
    iget-object v0, p0, Lcom/navdy/hud/app/device/dial/DialManager$22;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/device/dial/DialManager;->access$400(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/os/Handler;

    move-result-object v0

    const-string v5, "Encryption failed, retrying"

    move v2, v1

    move v3, v1

    move v4, v1

    invoke-static/range {v0 .. v5}, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->sendLocalyticsEvent(Landroid/os/Handler;ZZIZLjava/lang/String;)V

    .line 1846
    return-void
.end method
