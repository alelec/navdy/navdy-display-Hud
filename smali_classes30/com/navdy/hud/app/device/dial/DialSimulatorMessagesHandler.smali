.class public Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler;
.super Ljava/lang/Object;
.source "DialSimulatorMessagesHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler$SimulatedKeyEventRunnable;
    }
.end annotation


# static fields
.field private static final LONG_PRESS_KEY_UP_FLAGS_SIMULATION:I = 0x280

.field private static final SIMULATION_TO_KEY_EVENT:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private longPressTimeThreshold:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler$1;

    invoke-direct {v0}, Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler$1;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler;->SIMULATION_TO_KEY_EVENT:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler;->longPressTimeThreshold:I

    .line 36
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler;

    .prologue
    .line 19
    iget v0, p0, Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler;->longPressTimeThreshold:I

    return v0
.end method

.method static synthetic access$100()Ljava/util/Map;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler;->SIMULATION_TO_KEY_EVENT:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public onDialSimulationEvent(Lcom/navdy/service/library/events/input/DialSimulationEvent;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/service/library/events/input/DialSimulationEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 68
    if-eqz p1, :cond_0

    .line 69
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler$SimulatedKeyEventRunnable;

    iget-object v2, p1, Lcom/navdy/service/library/events/input/DialSimulationEvent;->dialAction:Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

    invoke-direct {v1, p0, v2}, Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler$SimulatedKeyEventRunnable;-><init>(Lcom/navdy/hud/app/device/dial/DialSimulatorMessagesHandler;Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 72
    :cond_0
    return-void
.end method
