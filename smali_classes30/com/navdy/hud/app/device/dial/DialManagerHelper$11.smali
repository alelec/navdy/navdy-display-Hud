.class final Lcom/navdy/hud/app/device/dial/DialManagerHelper$11;
.super Ljava/lang/Object;
.source "DialManagerHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/device/dial/DialManagerHelper;->sendLocalyticsEvent(Landroid/os/Handler;ZZIZLjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$device:Landroid/bluetooth/BluetoothDevice;

.field final synthetic val$firstTime:Z

.field final synthetic val$foundDial:Z

.field final synthetic val$pair:Z

.field final synthetic val$result:Ljava/lang/String;


# direct methods
.method constructor <init>(ZZZLjava/lang/String;Landroid/bluetooth/BluetoothDevice;)V
    .locals 0

    .prologue
    .line 369
    iput-boolean p1, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$11;->val$firstTime:Z

    iput-boolean p2, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$11;->val$foundDial:Z

    iput-boolean p3, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$11;->val$pair:Z

    iput-object p4, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$11;->val$result:Ljava/lang/String;

    iput-object p5, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$11;->val$device:Landroid/bluetooth/BluetoothDevice;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    .line 373
    :try_start_0
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 374
    .local v6, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v12, "First_Time"

    iget-boolean v11, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$11;->val$firstTime:Z

    if-eqz v11, :cond_1

    const-string v11, "true"

    :goto_0
    invoke-interface {v6, v12, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 375
    const-string v12, "Success"

    iget-boolean v11, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$11;->val$foundDial:Z

    if-eqz v11, :cond_2

    const-string v11, "true"

    :goto_1
    invoke-interface {v6, v12, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 376
    iget-boolean v11, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$11;->val$pair:Z

    if-eqz v11, :cond_3

    const-string v10, "Pair_Attempt"

    .line 377
    .local v10, "type":Ljava/lang/String;
    :goto_2
    const-string v11, "Type"

    invoke-interface {v6, v11, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 378
    const-string v11, "Result"

    iget-object v12, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$11;->val$result:Ljava/lang/String;

    invoke-interface {v6, v11, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 379
    const/4 v0, 0x0

    .line 380
    .local v0, "address":Ljava/lang/String;
    iget-object v11, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$11;->val$device:Landroid/bluetooth/BluetoothDevice;

    if-eqz v11, :cond_0

    .line 381
    iget-object v11, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$11;->val$device:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v11}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    .line 383
    :cond_0
    const-string v11, "Dial_Address"

    invoke-interface {v6, v11, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 384
    iget-boolean v11, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$11;->val$foundDial:Z

    if-eqz v11, :cond_4

    .line 385
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->getInstance()Lcom/navdy/hud/app/device/dial/DialManager;

    move-result-object v2

    .line 386
    .local v2, "dialManager":Lcom/navdy/hud/app/device/dial/DialManager;
    invoke-virtual {v2}, Lcom/navdy/hud/app/device/dial/DialManager;->getLastKnownBatteryLevel()I

    move-result v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 387
    .local v1, "battery":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/navdy/hud/app/device/dial/DialManager;->getLastKnownRawBatteryLevel()Ljava/lang/Integer;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 388
    .local v7, "rawBattery":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/navdy/hud/app/device/dial/DialManager;->getLastKnownSystemTemperature()Ljava/lang/Integer;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 389
    .local v9, "temperature":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/navdy/hud/app/device/dial/DialManager;->getFirmWareVersion()Ljava/lang/String;

    move-result-object v3

    .line 390
    .local v3, "fw":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/navdy/hud/app/device/dial/DialManager;->getHardwareVersion()Ljava/lang/String;

    move-result-object v4

    .line 391
    .local v4, "hw":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/navdy/hud/app/device/dial/DialManager;->getDialFirmwareUpdater()Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;

    move-result-object v11

    invoke-virtual {v11}, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater;->getVersions()Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;

    move-result-object v11

    iget-object v11, v11, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions;->dial:Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;

    iget v5, v11, Lcom/navdy/hud/app/device/dial/DialFirmwareUpdater$Versions$Version;->incrementalVersion:I

    .line 392
    .local v5, "incremental":I
    const-string v11, "Battery_Level"

    invoke-interface {v6, v11, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 393
    const-string v11, "Raw_Battery_Level"

    invoke-interface {v6, v11, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 394
    const-string v11, "System_Temperature"

    invoke-interface {v6, v11, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 395
    const-string v11, "FW_Version"

    invoke-interface {v6, v11, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 396
    const-string v11, "HW_Version"

    invoke-interface {v6, v11, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 397
    const-string v11, "Incremental_Version"

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v12

    invoke-interface {v6, v11, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 398
    # getter for: Lcom/navdy/hud/app/device/dial/DialManagerHelper;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "sending firstTime ["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-boolean v13, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$11;->val$firstTime:Z

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "] foundDial["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-boolean v13, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$11;->val$foundDial:Z

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "] battery["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "] rawBattery["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "] temperature["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "]"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " fw["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "] hw["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "] incremental["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "] type["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "] result ["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$11;->val$result:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "] address ["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "]"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 404
    .end local v1    # "battery":Ljava/lang/String;
    .end local v2    # "dialManager":Lcom/navdy/hud/app/device/dial/DialManager;
    .end local v3    # "fw":Ljava/lang/String;
    .end local v4    # "hw":Ljava/lang/String;
    .end local v5    # "incremental":I
    .end local v7    # "rawBattery":Ljava/lang/String;
    .end local v9    # "temperature":Ljava/lang/String;
    :goto_3
    const-string v11, "Dial_Pairing"

    invoke-static {v11, v6}, Lcom/localytics/android/Localytics;->tagEvent(Ljava/lang/String;Ljava/util/Map;)V

    .line 408
    .end local v0    # "address":Ljava/lang/String;
    .end local v6    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v10    # "type":Ljava/lang/String;
    :goto_4
    return-void

    .line 374
    .restart local v6    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_1
    const-string v11, "false"

    goto/16 :goto_0

    .line 375
    :cond_2
    const-string v11, "false"

    goto/16 :goto_1

    .line 376
    :cond_3
    const-string v10, "Connect"

    goto/16 :goto_2

    .line 402
    .restart local v0    # "address":Ljava/lang/String;
    .restart local v10    # "type":Ljava/lang/String;
    :cond_4
    # getter for: Lcom/navdy/hud/app/device/dial/DialManagerHelper;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "sending firstTime ["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-boolean v13, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$11;->val$firstTime:Z

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "] foundDial["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-boolean v13, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$11;->val$foundDial:Z

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "] type["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "] result ["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$11;->val$result:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "] address ["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "]"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 405
    .end local v0    # "address":Ljava/lang/String;
    .end local v6    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v10    # "type":Ljava/lang/String;
    :catch_0
    move-exception v8

    .line 406
    .local v8, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/device/dial/DialManagerHelper;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v11

    invoke-virtual {v11, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_4
.end method
