.class Lcom/navdy/hud/app/device/dial/DialManager$20;
.super Ljava/lang/Object;
.source "DialManager.java"

# interfaces
.implements Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialForgotten;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/device/dial/DialManager;->forgetAllDials(Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialForgotten;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/device/dial/DialManager;

.field final synthetic val$callback:Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialForgotten;

.field final synthetic val$counter:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/dial/DialManager;Ljava/util/concurrent/atomic/AtomicInteger;Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialForgotten;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 1598
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager$20;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    iput-object p2, p0, Lcom/navdy/hud/app/device/dial/DialManager$20;->val$counter:Ljava/util/concurrent/atomic/AtomicInteger;

    iput-object p3, p0, Lcom/navdy/hud/app/device/dial/DialManager$20;->val$callback:Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialForgotten;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onForgotten()V
    .locals 4

    .prologue
    .line 1601
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager$20;->val$counter:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    .line 1602
    .local v0, "val":I
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "forgetAllDials counter:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1603
    if-nez v0, :cond_0

    .line 1604
    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->DIAL_NOT_CONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->access$100()Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

    move-result-object v1

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;->dialName:Ljava/lang/String;

    .line 1605
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager$20;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v1}, Lcom/navdy/hud/app/device/dial/DialManager;->access$200(Lcom/navdy/hud/app/device/dial/DialManager;)Lcom/squareup/otto/Bus;

    move-result-object v1

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->DIAL_NOT_CONNECTED:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->access$100()Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 1606
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager$20;->val$callback:Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialForgotten;

    if-eqz v1, :cond_0

    .line 1607
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager$20;->val$callback:Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialForgotten;

    invoke-interface {v1}, Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialForgotten;->onForgotten()V

    .line 1610
    :cond_0
    return-void
.end method
