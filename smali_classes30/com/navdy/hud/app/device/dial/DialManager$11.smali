.class Lcom/navdy/hud/app/device/dial/DialManager$11;
.super Ljava/lang/Object;
.source "DialManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/device/dial/DialManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/device/dial/DialManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/dial/DialManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/device/dial/DialManager;

    .prologue
    .line 512
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManager$11;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 516
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager$11;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->firmwareVersionCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;
    invoke-static {v1}, Lcom/navdy/hud/app/device/dial/DialManager;->access$2200(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 517
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager$11;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager$11;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->firmwareVersionCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;
    invoke-static {v2}, Lcom/navdy/hud/app/device/dial/DialManager;->access$2200(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/device/dial/DialManager;->queueRead(Landroid/bluetooth/BluetoothGattCharacteristic;)V

    .line 519
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager$11;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->hardwareVersionCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;
    invoke-static {v1}, Lcom/navdy/hud/app/device/dial/DialManager;->access$2300(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 520
    iget-object v1, p0, Lcom/navdy/hud/app/device/dial/DialManager$11;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    iget-object v2, p0, Lcom/navdy/hud/app/device/dial/DialManager$11;->this$0:Lcom/navdy/hud/app/device/dial/DialManager;

    # getter for: Lcom/navdy/hud/app/device/dial/DialManager;->hardwareVersionCharacteristic:Landroid/bluetooth/BluetoothGattCharacteristic;
    invoke-static {v2}, Lcom/navdy/hud/app/device/dial/DialManager;->access$2300(Lcom/navdy/hud/app/device/dial/DialManager;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/device/dial/DialManager;->queueRead(Landroid/bluetooth/BluetoothGattCharacteristic;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 525
    :cond_1
    :goto_0
    return-void

    .line 522
    :catch_0
    move-exception v0

    .line 523
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/hud/app/device/dial/DialManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "[Dial]"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
