.class Lcom/navdy/hud/app/device/dial/DialManagerHelper$7$1;
.super Ljava/lang/Object;
.source "DialManagerHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;->onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;

.field final synthetic val$proxy:Landroid/bluetooth/BluetoothProfile;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;Landroid/bluetooth/BluetoothProfile;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;

    .prologue
    .line 209
    iput-object p1, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;

    iput-object p2, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7$1;->val$proxy:Landroid/bluetooth/BluetoothProfile;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 213
    :try_start_0
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7$1;->val$proxy:Landroid/bluetooth/BluetoothProfile;

    iget-object v4, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;

    iget-object v4, v4, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;->val$device:Landroid/bluetooth/BluetoothDevice;

    invoke-interface {v3, v4}, Landroid/bluetooth/BluetoothProfile;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    move-result v1

    .line 214
    .local v1, "state":I
    # getter for: Lcom/navdy/hud/app/device/dial/DialManagerHelper;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[Dial]disconnectFromDial Device "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;

    iget-object v5, v5, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;->val$device:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v5}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " state is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 215
    invoke-static {v1}, Lcom/navdy/hud/app/bluetooth/utils/BluetoothUtils;->getConnectedState(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 214
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 217
    const/4 v3, 0x2

    if-ne v1, v3, :cond_2

    .line 218
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7$1;->val$proxy:Landroid/bluetooth/BluetoothProfile;

    iget-object v4, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;

    iget-object v4, v4, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;->val$device:Landroid/bluetooth/BluetoothDevice;

    # invokes: Lcom/navdy/hud/app/device/dial/DialManagerHelper;->forceDisconnect(Landroid/bluetooth/BluetoothProfile;Landroid/bluetooth/BluetoothDevice;)Z
    invoke-static {v3, v4}, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->access$200(Landroid/bluetooth/BluetoothProfile;Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    .line 219
    .local v0, "ret":Z
    if-nez v0, :cond_1

    .line 220
    # getter for: Lcom/navdy/hud/app/device/dial/DialManagerHelper;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "[Dial]disconnectFromDial proxy-disconnect from hid device failed"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 221
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;

    iget-boolean v3, v3, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;->eventSent:Z

    if-nez v3, :cond_0

    .line 222
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;

    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;->eventSent:Z

    .line 223
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;

    iget-object v3, v3, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;->val$callBack:Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;->onAttemptDisconnection(Z)V

    .line 246
    .end local v0    # "ret":Z
    .end local v1    # "state":I
    :cond_0
    :goto_0
    return-void

    .line 226
    .restart local v0    # "ret":Z
    .restart local v1    # "state":I
    :cond_1
    # getter for: Lcom/navdy/hud/app/device/dial/DialManagerHelper;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "[Dial]disconnectFromDial proxy-disconnect from hid device successful"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 227
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;

    iget-boolean v3, v3, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;->eventSent:Z

    if-nez v3, :cond_0

    .line 228
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;

    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;->eventSent:Z

    .line 229
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;

    iget-object v3, v3, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;->val$callBack:Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;

    const/4 v4, 0x1

    invoke-interface {v3, v4}, Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;->onAttemptDisconnection(Z)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 239
    .end local v0    # "ret":Z
    .end local v1    # "state":I
    :catch_0
    move-exception v2

    .line 240
    .local v2, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/device/dial/DialManagerHelper;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "[Dial] disconnectFromDial"

    invoke-virtual {v3, v4, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 241
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;

    iget-boolean v3, v3, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;->eventSent:Z

    if-nez v3, :cond_0

    .line 242
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;

    iput-boolean v6, v3, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;->eventSent:Z

    .line 243
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;

    iget-object v3, v3, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;->val$callBack:Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;

    invoke-interface {v3, v7}, Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;->onAttemptDisconnection(Z)V

    goto :goto_0

    .line 233
    .end local v2    # "t":Ljava/lang/Throwable;
    .restart local v1    # "state":I
    :cond_2
    :try_start_1
    # getter for: Lcom/navdy/hud/app/device/dial/DialManagerHelper;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManagerHelper;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[Dial]disconnectFromDial already disconnected:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;

    iget-object v5, v5, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;->val$device:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 234
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;

    iget-boolean v3, v3, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;->eventSent:Z

    if-nez v3, :cond_0

    .line 235
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;

    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;->eventSent:Z

    .line 236
    iget-object v3, p0, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7$1;->this$0:Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;

    iget-object v3, v3, Lcom/navdy/hud/app/device/dial/DialManagerHelper$7;->val$callBack:Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;

    const/4 v4, 0x1

    invoke-interface {v3, v4}, Lcom/navdy/hud/app/device/dial/DialManagerHelper$IDialConnection;->onAttemptDisconnection(Z)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
