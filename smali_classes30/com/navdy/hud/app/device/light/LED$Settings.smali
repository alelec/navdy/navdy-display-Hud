.class public Lcom/navdy/hud/app/device/light/LED$Settings;
.super Lcom/navdy/hud/app/device/light/LightSettings;
.source "LED.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/device/light/LED;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Settings"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/device/light/LED$Settings$Builder;
    }
.end annotation


# instance fields
.field private activityBlink:Z

.field private blinkFrequency:Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;

.field private blinkInfinite:Z

.field private blinkPulseCount:I

.field private name:Ljava/lang/String;


# direct methods
.method public constructor <init>(IZZLcom/navdy/hud/app/device/light/LED$BlinkFrequency;ZIZLjava/lang/String;)V
    .locals 0
    .param p1, "color"    # I
    .param p2, "isTurnedOn"    # Z
    .param p3, "isBlinking"    # Z
    .param p4, "blinkFrequency"    # Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;
    .param p5, "blinkInfinite"    # Z
    .param p6, "blinkPulseCount"    # I
    .param p7, "activityBlink"    # Z
    .param p8, "name"    # Ljava/lang/String;

    .prologue
    .line 94
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/hud/app/device/light/LightSettings;-><init>(IZZ)V

    .line 95
    iput-object p4, p0, Lcom/navdy/hud/app/device/light/LED$Settings;->blinkFrequency:Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;

    .line 96
    iput-boolean p5, p0, Lcom/navdy/hud/app/device/light/LED$Settings;->blinkInfinite:Z

    .line 97
    iput p6, p0, Lcom/navdy/hud/app/device/light/LED$Settings;->blinkPulseCount:I

    .line 98
    iput-boolean p7, p0, Lcom/navdy/hud/app/device/light/LED$Settings;->activityBlink:Z

    .line 99
    iput-object p8, p0, Lcom/navdy/hud/app/device/light/LED$Settings;->name:Ljava/lang/String;

    .line 100
    return-void
.end method


# virtual methods
.method public getActivityBlink()Z
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Lcom/navdy/hud/app/device/light/LED$Settings;->activityBlink:Z

    return v0
.end method

.method public getBlinkFrequency()Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/navdy/hud/app/device/light/LED$Settings;->blinkFrequency:Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;

    return-object v0
.end method

.method public getBlinkPulseCount()I
    .locals 1

    .prologue
    .line 111
    iget v0, p0, Lcom/navdy/hud/app/device/light/LED$Settings;->blinkPulseCount:I

    return v0
.end method

.method public isBlinkInfinite()Z
    .locals 1

    .prologue
    .line 107
    iget-boolean v0, p0, Lcom/navdy/hud/app/device/light/LED$Settings;->blinkInfinite:Z

    return v0
.end method
