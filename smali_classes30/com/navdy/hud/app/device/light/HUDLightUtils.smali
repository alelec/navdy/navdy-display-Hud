.class public Lcom/navdy/hud/app/device/light/HUDLightUtils;
.super Ljava/lang/Object;
.source "HUDLightUtils.java"


# static fields
.field public static gestureDetectedLedSettings:Lcom/navdy/hud/app/device/light/LED$Settings;

.field public static pairingLedSettings:Lcom/navdy/hud/app/device/light/LED$Settings;

.field public static final sLogger:Lcom/navdy/service/library/log/Logger;

.field public static snapShotCollectionEnd:Lcom/navdy/hud/app/device/light/LED$Settings;

.field public static snapshotCollectionStart:Lcom/navdy/hud/app/device/light/LED$Settings;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 14
    new-instance v1, Lcom/navdy/service/library/log/Logger;

    const-class v2, Lcom/navdy/hud/app/device/light/HUDLightUtils;

    invoke-direct {v1, v2}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v1, Lcom/navdy/hud/app/device/light/HUDLightUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 21
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0050

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 22
    .local v0, "color":I
    new-instance v1, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    invoke-direct {v1}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;-><init>()V

    const-string v2, "SnapshotStart"

    .line 23
    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setName(Ljava/lang/String;)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v1

    .line 24
    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setColor(I)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v1

    .line 25
    invoke-virtual {v1, v4}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setIsBlinking(Z)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v1

    .line 26
    invoke-virtual {v1}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->build()Lcom/navdy/hud/app/device/light/LED$Settings;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/device/light/HUDLightUtils;->snapshotCollectionStart:Lcom/navdy/hud/app/device/light/LED$Settings;

    .line 27
    new-instance v1, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    invoke-direct {v1}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;-><init>()V

    const-string v2, "SnapshotEnd"

    .line 28
    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setName(Ljava/lang/String;)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v1

    .line 29
    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setColor(I)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v1

    .line 30
    invoke-virtual {v1, v3}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setIsBlinking(Z)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v1

    .line 31
    invoke-virtual {v1, v4}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setBlinkInfinite(Z)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v1

    const/4 v2, 0x3

    .line 32
    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setBlinkPulseCount(I)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;->HIGH:Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;

    .line 33
    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setBlinkFrequency(Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v1

    .line 34
    invoke-virtual {v1}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->build()Lcom/navdy/hud/app/device/light/LED$Settings;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/device/light/HUDLightUtils;->snapShotCollectionEnd:Lcom/navdy/hud/app/device/light/LED$Settings;

    .line 35
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d004d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 36
    new-instance v1, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    invoke-direct {v1}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;-><init>()V

    const-string v2, "GestureDetected"

    .line 37
    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setName(Ljava/lang/String;)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v1

    .line 38
    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setColor(I)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v1

    .line 39
    invoke-virtual {v1, v3}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setIsBlinking(Z)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v1

    .line 40
    invoke-virtual {v1, v4}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setBlinkInfinite(Z)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v1

    const/4 v2, 0x2

    .line 41
    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setBlinkPulseCount(I)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;->HIGH:Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;

    .line 42
    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setBlinkFrequency(Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v1

    .line 43
    invoke-virtual {v1}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->build()Lcom/navdy/hud/app/device/light/LED$Settings;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/device/light/HUDLightUtils;->gestureDetectedLedSettings:Lcom/navdy/hud/app/device/light/LED$Settings;

    .line 44
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d004b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 45
    new-instance v1, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    invoke-direct {v1}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;-><init>()V

    const-string v2, "Pairing"

    .line 46
    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setName(Ljava/lang/String;)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v1

    .line 47
    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setColor(I)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v1

    .line 48
    invoke-virtual {v1, v3}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setIsBlinking(Z)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v1

    .line 49
    invoke-virtual {v1, v3}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setBlinkInfinite(Z)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;->LOW:Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;

    .line 50
    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setBlinkFrequency(Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v1

    .line 51
    invoke-virtual {v1}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->build()Lcom/navdy/hud/app/device/light/LED$Settings;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/device/light/HUDLightUtils;->pairingLedSettings:Lcom/navdy/hud/app/device/light/LED$Settings;

    .line 52
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static dialActionDetected(Landroid/content/Context;Lcom/navdy/hud/app/device/light/LightManager;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "manager"    # Lcom/navdy/hud/app/device/light/LightManager;

    .prologue
    .line 128
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/navdy/hud/app/device/light/LightManager;->getLight(I)Lcom/navdy/hud/app/device/light/ILight;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/device/light/LED;

    .line 129
    .local v0, "frontLed":Lcom/navdy/hud/app/device/light/LED;
    if-eqz v0, :cond_0

    .line 130
    sget-object v1, Lcom/navdy/hud/app/device/light/HUDLightUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Front LED, showing dial action detected"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 131
    invoke-virtual {v0}, Lcom/navdy/hud/app/device/light/LED;->startActivityBlink()V

    .line 133
    :cond_0
    return-void
.end method

.method public static removeSettings(Lcom/navdy/hud/app/device/light/LED$Settings;)V
    .locals 3
    .param p0, "settings"    # Lcom/navdy/hud/app/device/light/LED$Settings;

    .prologue
    .line 104
    if-eqz p0, :cond_0

    .line 105
    invoke-static {}, Lcom/navdy/hud/app/device/light/LightManager;->getInstance()Lcom/navdy/hud/app/device/light/LightManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/device/light/LightManager;->getLight(I)Lcom/navdy/hud/app/device/light/ILight;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/device/light/LED;

    .line 106
    .local v0, "frontLed":Lcom/navdy/hud/app/device/light/LED;
    if-eqz v0, :cond_0

    .line 107
    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/device/light/LED;->removeSetting(Lcom/navdy/hud/app/device/light/LightSettings;)V

    .line 110
    .end local v0    # "frontLed":Lcom/navdy/hud/app/device/light/LED;
    :cond_0
    return-void
.end method

.method public static resetFrontLED(Lcom/navdy/hud/app/device/light/LightManager;)V
    .locals 3
    .param p0, "manager"    # Lcom/navdy/hud/app/device/light/LightManager;

    .prologue
    .line 142
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/device/light/LightManager;->getLight(I)Lcom/navdy/hud/app/device/light/ILight;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/device/light/LED;

    .line 143
    .local v0, "frontLed":Lcom/navdy/hud/app/device/light/LED;
    if-eqz v0, :cond_0

    .line 144
    sget-object v1, Lcom/navdy/hud/app/device/light/HUDLightUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "resetFrontLED called"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 145
    invoke-virtual {v0}, Lcom/navdy/hud/app/device/light/LED;->reset()V

    .line 147
    :cond_0
    return-void
.end method

.method public static showError(Landroid/content/Context;Lcom/navdy/hud/app/device/light/LightManager;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "manager"    # Lcom/navdy/hud/app/device/light/LightManager;

    .prologue
    const/4 v5, 0x0

    .line 226
    invoke-virtual {p1, v5}, Lcom/navdy/hud/app/device/light/LightManager;->getLight(I)Lcom/navdy/hud/app/device/light/ILight;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/device/light/LED;

    .line 227
    .local v1, "frontLed":Lcom/navdy/hud/app/device/light/LED;
    if-eqz v1, :cond_0

    .line 228
    sget-object v3, Lcom/navdy/hud/app/device/light/HUDLightUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Front LED, showing error, not blinking"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 229
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d004c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 230
    .local v0, "color":I
    new-instance v3, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    invoke-direct {v3}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;-><init>()V

    .line 231
    invoke-virtual {v3, v0}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setColor(I)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v3

    .line 232
    invoke-virtual {v3, v5}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setIsBlinking(Z)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v3

    .line 233
    invoke-virtual {v3}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->build()Lcom/navdy/hud/app/device/light/LED$Settings;

    move-result-object v2

    .line 234
    .local v2, "settings":Lcom/navdy/hud/app/device/light/LED$Settings;
    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/device/light/LED;->pushSetting(Lcom/navdy/hud/app/device/light/LightSettings;)V

    .line 236
    .end local v0    # "color":I
    .end local v2    # "settings":Lcom/navdy/hud/app/device/light/LED$Settings;
    :cond_0
    return-void
.end method

.method public static showGestureDetected(Landroid/content/Context;Lcom/navdy/hud/app/device/light/LightManager;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "manager"    # Lcom/navdy/hud/app/device/light/LightManager;

    .prologue
    .line 119
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/navdy/hud/app/device/light/LightManager;->getLight(I)Lcom/navdy/hud/app/device/light/ILight;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/device/light/LED;

    .line 120
    .local v0, "frontLed":Lcom/navdy/hud/app/device/light/LED;
    if-eqz v0, :cond_0

    .line 121
    sget-object v1, Lcom/navdy/hud/app/device/light/HUDLightUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Front LED, showing gesture detected"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 122
    sget-object v1, Lcom/navdy/hud/app/device/light/HUDLightUtils;->gestureDetectedLedSettings:Lcom/navdy/hud/app/device/light/LED$Settings;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/device/light/LED;->pushSetting(Lcom/navdy/hud/app/device/light/LightSettings;)V

    .line 123
    sget-object v1, Lcom/navdy/hud/app/device/light/HUDLightUtils;->gestureDetectedLedSettings:Lcom/navdy/hud/app/device/light/LED$Settings;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/device/light/LED;->removeSetting(Lcom/navdy/hud/app/device/light/LightSettings;)V

    .line 125
    :cond_0
    return-void
.end method

.method public static showGestureDetectionEnabled(Landroid/content/Context;Lcom/navdy/hud/app/device/light/LightManager;Ljava/lang/String;)Lcom/navdy/hud/app/device/light/LED$Settings;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "manager"    # Lcom/navdy/hud/app/device/light/LightManager;
    .param p2, "logName"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    .line 73
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Lcom/navdy/hud/app/device/light/LightManager;->getLight(I)Lcom/navdy/hud/app/device/light/ILight;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/device/light/LED;

    .line 74
    .local v1, "frontLed":Lcom/navdy/hud/app/device/light/LED;
    if-eqz v1, :cond_0

    .line 75
    sget-object v3, Lcom/navdy/hud/app/device/light/HUDLightUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Front LED, showing gesture detection enabled"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 76
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d004d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 77
    .local v0, "color":I
    new-instance v3, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    invoke-direct {v3}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;-><init>()V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GestureDetectionEnabled-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 78
    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setName(Ljava/lang/String;)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v3

    .line 79
    invoke-virtual {v3, v0}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setColor(I)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v3

    .line 80
    invoke-virtual {v3, v6}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setIsBlinking(Z)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v3

    .line 81
    invoke-virtual {v3, v6}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setBlinkInfinite(Z)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;->LOW:Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;

    .line 82
    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setBlinkFrequency(Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v3

    .line 83
    invoke-virtual {v3}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->build()Lcom/navdy/hud/app/device/light/LED$Settings;

    move-result-object v2

    .line 84
    .local v2, "settings":Lcom/navdy/hud/app/device/light/LED$Settings;
    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/device/light/LED;->pushSetting(Lcom/navdy/hud/app/device/light/LightSettings;)V

    .line 87
    .end local v0    # "color":I
    .end local v2    # "settings":Lcom/navdy/hud/app/device/light/LED$Settings;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static showGestureNotRecognized(Landroid/content/Context;Lcom/navdy/hud/app/device/light/LightManager;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "manager"    # Lcom/navdy/hud/app/device/light/LightManager;

    .prologue
    const/4 v5, 0x0

    .line 239
    invoke-virtual {p1, v5}, Lcom/navdy/hud/app/device/light/LightManager;->getLight(I)Lcom/navdy/hud/app/device/light/ILight;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/device/light/LED;

    .line 240
    .local v1, "frontLed":Lcom/navdy/hud/app/device/light/LED;
    if-eqz v1, :cond_0

    .line 241
    sget-object v3, Lcom/navdy/hud/app/device/light/HUDLightUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Front LED, showing gesture not recognized"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 242
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d004e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 243
    .local v0, "color":I
    new-instance v3, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    invoke-direct {v3}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;-><init>()V

    .line 244
    invoke-virtual {v3, v0}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setColor(I)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v3

    .line 245
    invoke-virtual {v3, v5}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setIsBlinking(Z)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;->LOW:Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;

    .line 246
    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setBlinkFrequency(Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v3

    .line 247
    invoke-virtual {v3}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->build()Lcom/navdy/hud/app/device/light/LED$Settings;

    move-result-object v2

    .line 248
    .local v2, "settings":Lcom/navdy/hud/app/device/light/LED$Settings;
    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/device/light/LED;->pushSetting(Lcom/navdy/hud/app/device/light/LightSettings;)V

    .line 250
    .end local v0    # "color":I
    .end local v2    # "settings":Lcom/navdy/hud/app/device/light/LED$Settings;
    :cond_0
    return-void
.end method

.method public static showPairing(Landroid/content/Context;Lcom/navdy/hud/app/device/light/LightManager;Z)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "manager"    # Lcom/navdy/hud/app/device/light/LightManager;
    .param p2, "show"    # Z

    .prologue
    .line 60
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/navdy/hud/app/device/light/LightManager;->getLight(I)Lcom/navdy/hud/app/device/light/ILight;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/device/light/LED;

    .line 61
    .local v0, "frontLed":Lcom/navdy/hud/app/device/light/LED;
    if-eqz v0, :cond_0

    .line 62
    if-eqz p2, :cond_1

    .line 63
    sget-object v1, Lcom/navdy/hud/app/device/light/HUDLightUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Front LED, showing dial pairing"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 64
    sget-object v1, Lcom/navdy/hud/app/device/light/HUDLightUtils;->pairingLedSettings:Lcom/navdy/hud/app/device/light/LED$Settings;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/device/light/LED;->pushSetting(Lcom/navdy/hud/app/device/light/LightSettings;)V

    .line 70
    :cond_0
    :goto_0
    return-void

    .line 66
    :cond_1
    sget-object v1, Lcom/navdy/hud/app/device/light/HUDLightUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Front LED, stop showing dial pairing"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 67
    sget-object v1, Lcom/navdy/hud/app/device/light/HUDLightUtils;->pairingLedSettings:Lcom/navdy/hud/app/device/light/LED$Settings;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/device/light/LED;->removeSetting(Lcom/navdy/hud/app/device/light/LightSettings;)V

    goto :goto_0
.end method

.method public static showShutDown(Lcom/navdy/hud/app/device/light/LightManager;)V
    .locals 6
    .param p0, "manager"    # Lcom/navdy/hud/app/device/light/LightManager;

    .prologue
    const/4 v5, 0x1

    .line 168
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/navdy/hud/app/device/light/LightManager;->getLight(I)Lcom/navdy/hud/app/device/light/ILight;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/device/light/LED;

    .line 169
    .local v1, "frontLed":Lcom/navdy/hud/app/device/light/LED;
    if-eqz v1, :cond_0

    .line 170
    sget-object v3, Lcom/navdy/hud/app/device/light/HUDLightUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Front LED, showing shutdown"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 171
    sget v0, Lcom/navdy/hud/app/device/light/LED;->DEFAULT_COLOR:I

    .line 172
    .local v0, "color":I
    new-instance v3, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    invoke-direct {v3}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;-><init>()V

    .line 173
    invoke-virtual {v3, v0}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setColor(I)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v3

    .line 174
    invoke-virtual {v3, v5}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setIsBlinking(Z)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v3

    .line 175
    invoke-virtual {v3, v5}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setBlinkInfinite(Z)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;->LOW:Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;

    .line 176
    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setBlinkFrequency(Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v3

    .line 177
    invoke-virtual {v3}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->build()Lcom/navdy/hud/app/device/light/LED$Settings;

    move-result-object v2

    .line 178
    .local v2, "settings":Lcom/navdy/hud/app/device/light/LED$Settings;
    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/device/light/LED;->pushSetting(Lcom/navdy/hud/app/device/light/LightSettings;)V

    .line 180
    .end local v0    # "color":I
    .end local v2    # "settings":Lcom/navdy/hud/app/device/light/LED$Settings;
    :cond_0
    return-void
.end method

.method public static showSnapshotCollection(Landroid/content/Context;Lcom/navdy/hud/app/device/light/LightManager;Z)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "manager"    # Lcom/navdy/hud/app/device/light/LightManager;
    .param p2, "finished"    # Z

    .prologue
    .line 92
    if-eqz p2, :cond_1

    sget-object v1, Lcom/navdy/hud/app/device/light/HUDLightUtils;->snapShotCollectionEnd:Lcom/navdy/hud/app/device/light/LED$Settings;

    .line 93
    .local v1, "settings":Lcom/navdy/hud/app/device/light/LED$Settings;
    :goto_0
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Lcom/navdy/hud/app/device/light/LightManager;->getLight(I)Lcom/navdy/hud/app/device/light/ILight;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/device/light/LED;

    .line 94
    .local v0, "frontLed":Lcom/navdy/hud/app/device/light/LED;
    if-eqz v0, :cond_0

    .line 95
    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/device/light/LED;->pushSetting(Lcom/navdy/hud/app/device/light/LightSettings;)V

    .line 96
    if-eqz p2, :cond_0

    .line 97
    sget-object v2, Lcom/navdy/hud/app/device/light/HUDLightUtils;->snapShotCollectionEnd:Lcom/navdy/hud/app/device/light/LED$Settings;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/device/light/LED;->removeSetting(Lcom/navdy/hud/app/device/light/LightSettings;)V

    .line 98
    sget-object v2, Lcom/navdy/hud/app/device/light/HUDLightUtils;->snapshotCollectionStart:Lcom/navdy/hud/app/device/light/LED$Settings;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/device/light/LED;->removeSetting(Lcom/navdy/hud/app/device/light/LightSettings;)V

    .line 101
    :cond_0
    return-void

    .line 92
    .end local v0    # "frontLed":Lcom/navdy/hud/app/device/light/LED;
    .end local v1    # "settings":Lcom/navdy/hud/app/device/light/LED$Settings;
    :cond_1
    sget-object v1, Lcom/navdy/hud/app/device/light/HUDLightUtils;->snapshotCollectionStart:Lcom/navdy/hud/app/device/light/LED$Settings;

    goto :goto_0
.end method

.method public static showUSBPowerOn(Landroid/content/Context;Lcom/navdy/hud/app/device/light/LightManager;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "manager"    # Lcom/navdy/hud/app/device/light/LightManager;

    .prologue
    const/4 v5, 0x0

    .line 198
    invoke-virtual {p1, v5}, Lcom/navdy/hud/app/device/light/LightManager;->getLight(I)Lcom/navdy/hud/app/device/light/ILight;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/device/light/LED;

    .line 199
    .local v1, "frontLed":Lcom/navdy/hud/app/device/light/LED;
    if-eqz v1, :cond_0

    .line 200
    sget-object v3, Lcom/navdy/hud/app/device/light/HUDLightUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Front LED, showing USB power on, not blinking"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 201
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d0050

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 202
    .local v0, "color":I
    new-instance v3, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    invoke-direct {v3}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;-><init>()V

    .line 203
    invoke-virtual {v3, v0}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setColor(I)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v3

    .line 204
    invoke-virtual {v3, v5}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setIsBlinking(Z)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v3

    .line 205
    invoke-virtual {v3}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->build()Lcom/navdy/hud/app/device/light/LED$Settings;

    move-result-object v2

    .line 206
    .local v2, "settings":Lcom/navdy/hud/app/device/light/LED$Settings;
    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/device/light/LED;->pushSetting(Lcom/navdy/hud/app/device/light/LightSettings;)V

    .line 208
    .end local v0    # "color":I
    .end local v2    # "settings":Lcom/navdy/hud/app/device/light/LED$Settings;
    :cond_0
    return-void
.end method

.method public static showUSBPowerShutDown(Landroid/content/Context;Lcom/navdy/hud/app/device/light/LightManager;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "manager"    # Lcom/navdy/hud/app/device/light/LightManager;

    .prologue
    const/4 v5, 0x1

    .line 183
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Lcom/navdy/hud/app/device/light/LightManager;->getLight(I)Lcom/navdy/hud/app/device/light/ILight;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/device/light/LED;

    .line 184
    .local v1, "frontLed":Lcom/navdy/hud/app/device/light/LED;
    if-eqz v1, :cond_0

    .line 185
    sget-object v3, Lcom/navdy/hud/app/device/light/HUDLightUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Front LED, showing USB power shutdown"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 186
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d0050

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 187
    .local v0, "color":I
    new-instance v3, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    invoke-direct {v3}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;-><init>()V

    .line 188
    invoke-virtual {v3, v0}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setColor(I)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v3

    .line 189
    invoke-virtual {v3, v5}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setIsBlinking(Z)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v3

    .line 190
    invoke-virtual {v3, v5}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setBlinkInfinite(Z)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;->LOW:Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;

    .line 191
    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setBlinkFrequency(Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v3

    .line 192
    invoke-virtual {v3}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->build()Lcom/navdy/hud/app/device/light/LED$Settings;

    move-result-object v2

    .line 193
    .local v2, "settings":Lcom/navdy/hud/app/device/light/LED$Settings;
    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/device/light/LED;->pushSetting(Lcom/navdy/hud/app/device/light/LightSettings;)V

    .line 195
    .end local v0    # "color":I
    .end local v2    # "settings":Lcom/navdy/hud/app/device/light/LED$Settings;
    :cond_0
    return-void
.end method

.method public static showUSBTransfer(Landroid/content/Context;Lcom/navdy/hud/app/device/light/LightManager;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "manager"    # Lcom/navdy/hud/app/device/light/LightManager;

    .prologue
    const/4 v5, 0x1

    .line 211
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Lcom/navdy/hud/app/device/light/LightManager;->getLight(I)Lcom/navdy/hud/app/device/light/ILight;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/device/light/LED;

    .line 212
    .local v1, "frontLed":Lcom/navdy/hud/app/device/light/LED;
    if-eqz v1, :cond_0

    .line 213
    sget-object v3, Lcom/navdy/hud/app/device/light/HUDLightUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Front LED, showing USB transfer"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 214
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d0050

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 215
    .local v0, "color":I
    new-instance v3, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    invoke-direct {v3}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;-><init>()V

    .line 216
    invoke-virtual {v3, v0}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setColor(I)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v3

    .line 217
    invoke-virtual {v3, v5}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setIsBlinking(Z)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v3

    .line 218
    invoke-virtual {v3, v5}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setBlinkInfinite(Z)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;->HIGH:Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;

    .line 219
    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setBlinkFrequency(Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v3

    .line 220
    invoke-virtual {v3}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->build()Lcom/navdy/hud/app/device/light/LED$Settings;

    move-result-object v2

    .line 221
    .local v2, "settings":Lcom/navdy/hud/app/device/light/LED$Settings;
    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/device/light/LED;->pushSetting(Lcom/navdy/hud/app/device/light/LightSettings;)V

    .line 223
    .end local v0    # "color":I
    .end local v2    # "settings":Lcom/navdy/hud/app/device/light/LED$Settings;
    :cond_0
    return-void
.end method

.method public static turnOffFrontLED(Lcom/navdy/hud/app/device/light/LightManager;)V
    .locals 3
    .param p0, "manager"    # Lcom/navdy/hud/app/device/light/LightManager;

    .prologue
    .line 150
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/device/light/LightManager;->getLight(I)Lcom/navdy/hud/app/device/light/ILight;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/device/light/LED;

    .line 151
    .local v0, "frontLed":Lcom/navdy/hud/app/device/light/LED;
    if-eqz v0, :cond_0

    .line 152
    sget-object v1, Lcom/navdy/hud/app/device/light/HUDLightUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "turnOffFrontLED called"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 153
    invoke-virtual {v0}, Lcom/navdy/hud/app/device/light/LED;->turnOff()V

    .line 155
    :cond_0
    return-void
.end method
