.class public Lcom/navdy/hud/app/device/light/LED;
.super Ljava/lang/Object;
.source "LED.java"

# interfaces
.implements Lcom/navdy/hud/app/device/light/ILight;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/device/light/LED$Settings;,
        Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;
    }
.end annotation


# static fields
.field private static final BLINK_ATTRIBUTE:Ljava/lang/String; = "blink"

.field public static final CLEAR_COLOR:I = 0x0

.field private static final COLOR_ATTRIBUTE:Ljava/lang/String; = "color"

.field public static final DEFAULT_COLOR:I

.field private static final DEFAULT_SETTINGS:Lcom/navdy/hud/app/device/light/LED$Settings;

.field private static final SLOPE_DOWN_ATTRUTE:Ljava/lang/String; = "slope_down"

.field private static final SLOPE_UP_ATTRIBUTE:Ljava/lang/String; = "slope_up"

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private activityBlinkRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private blinkPath:Ljava/lang/String;

.field private colorPath:Ljava/lang/String;

.field private mLightSettingsStack:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/navdy/hud/app/device/light/LightSettings;",
            ">;"
        }
    .end annotation
.end field

.field private mSerialExecutor:Ljava/util/concurrent/ExecutorService;

.field private slopeDownPath:Ljava/lang/String;

.field private slopeUpPath:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/device/light/LED;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/device/light/LED;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 23
    const-string v0, "#ff000000"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/device/light/LED;->DEFAULT_COLOR:I

    .line 34
    new-instance v0, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    invoke-direct {v0}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;-><init>()V

    const-string v1, "DEFAULT"

    .line 35
    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setName(Ljava/lang/String;)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v0

    sget v1, Lcom/navdy/hud/app/device/light/LED;->DEFAULT_COLOR:I

    .line 36
    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setColor(I)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 37
    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setIsBlinking(Z)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v0

    .line 38
    invoke-virtual {v0}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->build()Lcom/navdy/hud/app/device/light/LED$Settings;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/device/light/LED;->DEFAULT_SETTINGS:Lcom/navdy/hud/app/device/light/LED$Settings;

    .line 34
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "ledDevice"    # Ljava/lang/String;

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/navdy/hud/app/device/light/LED;->activityBlinkRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 60
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "color"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/device/light/LED;->colorPath:Ljava/lang/String;

    .line 62
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "slope_up"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/device/light/LED;->slopeUpPath:Ljava/lang/String;

    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "slope_down"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/device/light/LED;->slopeDownPath:Ljava/lang/String;

    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "blink"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/device/light/LED;->blinkPath:Ljava/lang/String;

    .line 66
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/device/light/LED;->mLightSettingsStack:Ljava/util/LinkedList;

    .line 67
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/device/light/LED;->mSerialExecutor:Ljava/util/concurrent/ExecutorService;

    .line 68
    iget-object v0, p0, Lcom/navdy/hud/app/device/light/LED;->mLightSettingsStack:Ljava/util/LinkedList;

    sget-object v1, Lcom/navdy/hud/app/device/light/LED;->DEFAULT_SETTINGS:Lcom/navdy/hud/app/device/light/LED$Settings;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->push(Ljava/lang/Object;)V

    .line 72
    :goto_0
    return-void

    .line 70
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/device/light/LED;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to open led at "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/device/light/LED;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/light/LED;

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/navdy/hud/app/device/light/LED;->activityBlink()V

    return-void
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/device/light/LED;I)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/light/LED;
    .param p1, "x1"    # I

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/device/light/LED;->setColor(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/device/light/LED;Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/light/LED;
    .param p1, "x1"    # Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/device/light/LED;->setBlinkFrequency(Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;)V

    return-void
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/device/light/LED;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/light/LED;

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/navdy/hud/app/device/light/LED;->startBlinking()V

    return-void
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/device/light/LED;IILcom/navdy/hud/app/device/light/LED$BlinkFrequency;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/light/LED;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;

    .prologue
    .line 19
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/hud/app/device/light/LED;->startBlinking(IILcom/navdy/hud/app/device/light/LED$BlinkFrequency;)V

    return-void
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/device/light/LED;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/light/LED;

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/navdy/hud/app/device/light/LED;->stopBlinking()V

    return-void
.end method

.method private activityBlink()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 233
    :try_start_0
    invoke-direct {p0}, Lcom/navdy/hud/app/device/light/LED;->stopBlinking()V

    .line 234
    sget-object v1, Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;->HIGH:Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/device/light/LED;->setBlinkFrequency(Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;)V

    .line 235
    invoke-direct {p0}, Lcom/navdy/hud/app/device/light/LED;->startBlinking()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 237
    :try_start_1
    sget-object v1, Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;->HIGH:Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;

    # getter for: Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;->blinkDelay:I
    invoke-static {v1}, Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;->access$000(Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-long v2, v1

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 241
    :goto_0
    :try_start_2
    invoke-direct {p0}, Lcom/navdy/hud/app/device/light/LED;->stopBlinking()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 243
    iget-object v1, p0, Lcom/navdy/hud/app/device/light/LED;->activityBlinkRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 245
    return-void

    .line 238
    :catch_0
    move-exception v0

    .line 239
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_3
    sget-object v1, Lcom/navdy/hud/app/device/light/LED;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Interrupted exception while activity blinking"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 243
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/navdy/hud/app/device/light/LED;->activityBlinkRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    throw v1
.end method

.method private applySettings(Lcom/navdy/hud/app/device/light/LightSettings;)V
    .locals 2
    .param p1, "settings"    # Lcom/navdy/hud/app/device/light/LightSettings;

    .prologue
    .line 291
    iget-object v0, p0, Lcom/navdy/hud/app/device/light/LED;->mSerialExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/navdy/hud/app/device/light/LED$1;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/device/light/LED$1;-><init>(Lcom/navdy/hud/app/device/light/LED;Lcom/navdy/hud/app/device/light/LightSettings;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 316
    return-void
.end method

.method private setBlinkFrequency(Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;)V
    .locals 2
    .param p1, "frequency"    # Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;

    .prologue
    .line 284
    if-eqz p1, :cond_0

    .line 285
    invoke-virtual {p1}, Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;->getBlinkDelay()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/device/light/LED;->slopeUpPath:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/navdy/hud/app/device/light/LED;->writeToKernelDevice(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    invoke-virtual {p1}, Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;->getBlinkDelay()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/device/light/LED;->slopeDownPath:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/navdy/hud/app/device/light/LED;->writeToKernelDevice(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    :cond_0
    return-void
.end method

.method private setColor(I)V
    .locals 5
    .param p1, "color"    # I

    .prologue
    .line 191
    const-string v1, "%08x"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 192
    .local v0, "colorString":Ljava/lang/String;
    iget-object v1, p0, Lcom/navdy/hud/app/device/light/LED;->colorPath:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/navdy/hud/app/device/light/LED;->writeToKernelDevice(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    return-void
.end method

.method private startBlinking()V
    .locals 2

    .prologue
    .line 196
    const-string v0, "1"

    iget-object v1, p0, Lcom/navdy/hud/app/device/light/LED;->blinkPath:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/navdy/hud/app/device/light/LED;->writeToKernelDevice(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    return-void
.end method

.method private startBlinking(IILcom/navdy/hud/app/device/light/LED$BlinkFrequency;)V
    .locals 6
    .param p1, "pulseCount"    # I
    .param p2, "color"    # I
    .param p3, "frequency"    # Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;

    .prologue
    .line 208
    invoke-direct {p0}, Lcom/navdy/hud/app/device/light/LED;->stopBlinking()V

    .line 209
    # getter for: Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;->blinkDelay:I
    invoke-static {p3}, Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;->access$000(Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;)I

    move-result v0

    .line 210
    .local v0, "blinkDelay":I
    if-lez p1, :cond_0

    .line 211
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, p1, :cond_0

    .line 213
    :try_start_0
    invoke-direct {p0, p2}, Lcom/navdy/hud/app/device/light/LED;->setColor(I)V

    .line 214
    div-int/lit8 v3, v0, 0x2

    int-to-long v4, v3

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V

    .line 215
    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lcom/navdy/hud/app/device/light/LED;->setColor(I)V

    .line 216
    div-int/lit8 v3, v0, 0x2

    int-to-long v4, v3

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 211
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 217
    :catch_0
    move-exception v1

    .line 218
    .local v1, "e":Ljava/lang/InterruptedException;
    sget-object v3, Lcom/navdy/hud/app/device/light/LED;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Interrupted exception while blinking"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_1

    .line 222
    .end local v1    # "e":Ljava/lang/InterruptedException;
    .end local v2    # "i":I
    :cond_0
    return-void
.end method

.method private stopBlinking()V
    .locals 2

    .prologue
    .line 225
    sget-object v0, Lcom/navdy/hud/app/device/light/LED;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 226
    sget-object v0, Lcom/navdy/hud/app/device/light/LED;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Stop blinking"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 228
    :cond_0
    const-string v0, "0"

    iget-object v1, p0, Lcom/navdy/hud/app/device/light/LED;->blinkPath:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/navdy/hud/app/device/light/LED;->writeToKernelDevice(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    return-void
.end method

.method private static writeToKernelDevice(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p0, "data"    # Ljava/lang/String;
    .param p1, "kernelFilePath"    # Ljava/lang/String;

    .prologue
    .line 320
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 322
    .local v1, "file":Ljava/io/File;
    :try_start_1
    new-instance v2, Ljava/io/FileWriter;

    invoke-direct {v2, v1}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V

    .line 323
    .local v2, "writer":Ljava/io/FileWriter;
    invoke-virtual {v2, p0}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 324
    invoke-virtual {v2}, Ljava/io/FileWriter;->flush()V

    .line 325
    invoke-virtual {v2}, Ljava/io/FileWriter;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 332
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "writer":Ljava/io/FileWriter;
    :goto_0
    return-void

    .line 326
    .restart local v1    # "file":Ljava/io/File;
    :catch_0
    move-exception v0

    .line 327
    .local v0, "e":Ljava/io/IOException;
    :try_start_2
    sget-object v3, Lcom/navdy/hud/app/device/light/LED;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Error writing to the sysfsfile "

    invoke-virtual {v3, v4, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 329
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "file":Ljava/io/File;
    :catch_1
    move-exception v0

    .line 330
    .local v0, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/navdy/hud/app/device/light/LED;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Exception while trying to write to the sysfs "

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static writeToSysfs(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "data"    # Ljava/lang/String;
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 186
    sget-object v0, Lcom/navdy/hud/app/device/light/LED;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "writing \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' to sysfs file \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 187
    invoke-static {p0, p1}, Lcom/navdy/hud/app/device/light/LED;->writeToKernelDevice(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    return-void
.end method


# virtual methods
.method public isAvailable()Z
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/navdy/hud/app/device/light/LED;->mLightSettingsStack:Ljava/util/LinkedList;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized popSetting()V
    .locals 3

    .prologue
    .line 256
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/device/light/LED;->mLightSettingsStack:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    .line 257
    iget-object v1, p0, Lcom/navdy/hud/app/device/light/LED;->mLightSettingsStack:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/device/light/LightSettings;

    .line 258
    .local v0, "lightSettings":Lcom/navdy/hud/app/device/light/LightSettings;
    if-eqz v0, :cond_0

    .line 259
    check-cast v0, Lcom/navdy/hud/app/device/light/LED$Settings;

    .line 263
    .end local v0    # "lightSettings":Lcom/navdy/hud/app/device/light/LightSettings;
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/device/light/LED;->mLightSettingsStack:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/device/light/LightSettings;

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/device/light/LED;->applySettings(Lcom/navdy/hud/app/device/light/LightSettings;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 264
    monitor-exit p0

    return-void

    .line 256
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized pushSetting(Lcom/navdy/hud/app/device/light/LightSettings;)V
    .locals 1
    .param p1, "settings"    # Lcom/navdy/hud/app/device/light/LightSettings;

    .prologue
    .line 250
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/device/light/LED;->mLightSettingsStack:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->push(Ljava/lang/Object;)V

    .line 251
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/device/light/LED;->applySettings(Lcom/navdy/hud/app/device/light/LightSettings;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 252
    monitor-exit p0

    return-void

    .line 250
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized removeSetting(Lcom/navdy/hud/app/device/light/LightSettings;)V
    .locals 1
    .param p1, "settings"    # Lcom/navdy/hud/app/device/light/LightSettings;

    .prologue
    .line 268
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/device/light/LED;->mLightSettingsStack:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->peek()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 269
    invoke-virtual {p0}, Lcom/navdy/hud/app/device/light/LED;->popSetting()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 274
    :goto_0
    monitor-exit p0

    return-void

    .line 271
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/navdy/hud/app/device/light/LED;->mLightSettingsStack:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 268
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized reset()V
    .locals 1

    .prologue
    .line 278
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/device/light/LED;->mLightSettingsStack:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 279
    sget-object v0, Lcom/navdy/hud/app/device/light/LED;->DEFAULT_SETTINGS:Lcom/navdy/hud/app/device/light/LED$Settings;

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/device/light/LED;->pushSetting(Lcom/navdy/hud/app/device/light/LightSettings;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 281
    monitor-exit p0

    return-void

    .line 278
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public startActivityBlink()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 335
    iget-object v1, p0, Lcom/navdy/hud/app/device/light/LED;->activityBlinkRunning:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 336
    new-instance v1, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    invoke-direct {v1}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;-><init>()V

    const-string v2, "ActivityBlink"

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setName(Ljava/lang/String;)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->setActivityBlink(Z)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->build()Lcom/navdy/hud/app/device/light/LED$Settings;

    move-result-object v0

    .line 337
    .local v0, "settings":Lcom/navdy/hud/app/device/light/LED$Settings;
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/device/light/LED;->pushSetting(Lcom/navdy/hud/app/device/light/LightSettings;)V

    .line 338
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/device/light/LED;->removeSetting(Lcom/navdy/hud/app/device/light/LightSettings;)V

    .line 340
    .end local v0    # "settings":Lcom/navdy/hud/app/device/light/LED$Settings;
    :cond_0
    return-void
.end method

.method public declared-synchronized turnOff()V
    .locals 1

    .prologue
    .line 181
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/navdy/hud/app/device/light/LED;->stopBlinking()V

    .line 182
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/device/light/LED;->setColor(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 183
    monitor-exit p0

    return-void

    .line 181
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized turnOn()V
    .locals 1

    .prologue
    .line 176
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/navdy/hud/app/device/light/LED;->stopBlinking()V

    .line 177
    sget v0, Lcom/navdy/hud/app/device/light/LED;->DEFAULT_COLOR:I

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/device/light/LED;->setColor(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 178
    monitor-exit p0

    return-void

    .line 176
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
