.class public Lcom/navdy/hud/app/device/light/LED$Settings$Builder;
.super Ljava/lang/Object;
.source "LED.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/device/light/LED$Settings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private activityBlink:Z

.field private blinkFrequency:Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;

.field private blinkInfinite:Z

.field private blinkPulseCount:I

.field private color:I

.field private isBlinking:Z

.field private isTurnedOn:Z

.field private name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    sget v0, Lcom/navdy/hud/app/device/light/LED;->DEFAULT_COLOR:I

    iput v0, p0, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->color:I

    .line 121
    iput-boolean v1, p0, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->isTurnedOn:Z

    .line 122
    iput-boolean v1, p0, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->isBlinking:Z

    .line 123
    sget-object v0, Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;->LOW:Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;

    iput-object v0, p0, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->blinkFrequency:Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;

    .line 124
    iput-boolean v1, p0, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->blinkInfinite:Z

    .line 125
    iput v2, p0, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->blinkPulseCount:I

    .line 126
    iput-boolean v2, p0, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->activityBlink:Z

    .line 127
    const-string v0, "UNKNOWN"

    iput-object v0, p0, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->name:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public build()Lcom/navdy/hud/app/device/light/LED$Settings;
    .locals 9

    .prologue
    .line 170
    new-instance v0, Lcom/navdy/hud/app/device/light/LED$Settings;

    iget v1, p0, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->color:I

    iget-boolean v2, p0, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->isTurnedOn:Z

    iget-boolean v3, p0, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->isBlinking:Z

    iget-object v4, p0, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->blinkFrequency:Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;

    iget-boolean v5, p0, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->blinkInfinite:Z

    iget v6, p0, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->blinkPulseCount:I

    iget-boolean v7, p0, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->activityBlink:Z

    iget-object v8, p0, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->name:Ljava/lang/String;

    invoke-direct/range {v0 .. v8}, Lcom/navdy/hud/app/device/light/LED$Settings;-><init>(IZZLcom/navdy/hud/app/device/light/LED$BlinkFrequency;ZIZLjava/lang/String;)V

    return-object v0
.end method

.method public setActivityBlink(Z)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;
    .locals 0
    .param p1, "activityBlink"    # Z

    .prologue
    .line 160
    iput-boolean p1, p0, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->activityBlink:Z

    .line 161
    return-object p0
.end method

.method public setBlinkFrequency(Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;
    .locals 0
    .param p1, "blinkFrequency"    # Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;

    .prologue
    .line 145
    iput-object p1, p0, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->blinkFrequency:Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;

    .line 146
    return-object p0
.end method

.method public setBlinkInfinite(Z)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;
    .locals 0
    .param p1, "blinkInfinite"    # Z

    .prologue
    .line 150
    iput-boolean p1, p0, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->blinkInfinite:Z

    .line 151
    return-object p0
.end method

.method public setBlinkPulseCount(I)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;
    .locals 0
    .param p1, "blinkPulseCount"    # I

    .prologue
    .line 155
    iput p1, p0, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->blinkPulseCount:I

    .line 156
    return-object p0
.end method

.method public setColor(I)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 130
    iput p1, p0, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->color:I

    .line 131
    return-object p0
.end method

.method public setIsBlinking(Z)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;
    .locals 0
    .param p1, "isBlinking"    # Z

    .prologue
    .line 140
    iput-boolean p1, p0, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->isBlinking:Z

    .line 141
    return-object p0
.end method

.method public setIsTurnedOn(Z)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;
    .locals 0
    .param p1, "isTurnedOn"    # Z

    .prologue
    .line 135
    iput-boolean p1, p0, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->isTurnedOn:Z

    .line 136
    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/navdy/hud/app/device/light/LED$Settings$Builder;
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 165
    iput-object p1, p0, Lcom/navdy/hud/app/device/light/LED$Settings$Builder;->name:Ljava/lang/String;

    .line 166
    return-object p0
.end method
