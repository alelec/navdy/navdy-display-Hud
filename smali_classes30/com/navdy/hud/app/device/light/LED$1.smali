.class Lcom/navdy/hud/app/device/light/LED$1;
.super Ljava/lang/Object;
.source "LED.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/device/light/LED;->applySettings(Lcom/navdy/hud/app/device/light/LightSettings;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/device/light/LED;

.field final synthetic val$settings:Lcom/navdy/hud/app/device/light/LightSettings;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/light/LED;Lcom/navdy/hud/app/device/light/LightSettings;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/device/light/LED;

    .prologue
    .line 291
    iput-object p1, p0, Lcom/navdy/hud/app/device/light/LED$1;->this$0:Lcom/navdy/hud/app/device/light/LED;

    iput-object p2, p0, Lcom/navdy/hud/app/device/light/LED$1;->val$settings:Lcom/navdy/hud/app/device/light/LightSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 294
    iget-object v1, p0, Lcom/navdy/hud/app/device/light/LED$1;->val$settings:Lcom/navdy/hud/app/device/light/LightSettings;

    instance-of v1, v1, Lcom/navdy/hud/app/device/light/LED$Settings;

    if-eqz v1, :cond_0

    .line 295
    iget-object v0, p0, Lcom/navdy/hud/app/device/light/LED$1;->val$settings:Lcom/navdy/hud/app/device/light/LightSettings;

    check-cast v0, Lcom/navdy/hud/app/device/light/LED$Settings;

    .line 296
    .local v0, "ledSettings":Lcom/navdy/hud/app/device/light/LED$Settings;
    invoke-virtual {v0}, Lcom/navdy/hud/app/device/light/LED$Settings;->getActivityBlink()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 297
    iget-object v1, p0, Lcom/navdy/hud/app/device/light/LED$1;->this$0:Lcom/navdy/hud/app/device/light/LED;

    # invokes: Lcom/navdy/hud/app/device/light/LED;->activityBlink()V
    invoke-static {v1}, Lcom/navdy/hud/app/device/light/LED;->access$100(Lcom/navdy/hud/app/device/light/LED;)V

    .line 314
    .end local v0    # "ledSettings":Lcom/navdy/hud/app/device/light/LED$Settings;
    :cond_0
    :goto_0
    return-void

    .line 298
    .restart local v0    # "ledSettings":Lcom/navdy/hud/app/device/light/LED$Settings;
    :cond_1
    invoke-virtual {v0}, Lcom/navdy/hud/app/device/light/LED$Settings;->isTurnedOn()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 299
    iget-object v1, p0, Lcom/navdy/hud/app/device/light/LED$1;->this$0:Lcom/navdy/hud/app/device/light/LED;

    invoke-virtual {v0}, Lcom/navdy/hud/app/device/light/LED$Settings;->getColor()I

    move-result v2

    # invokes: Lcom/navdy/hud/app/device/light/LED;->setColor(I)V
    invoke-static {v1, v2}, Lcom/navdy/hud/app/device/light/LED;->access$200(Lcom/navdy/hud/app/device/light/LED;I)V

    .line 300
    invoke-virtual {v0}, Lcom/navdy/hud/app/device/light/LED$Settings;->isBlinking()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 301
    invoke-virtual {v0}, Lcom/navdy/hud/app/device/light/LED$Settings;->isBlinkInfinite()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 302
    iget-object v1, p0, Lcom/navdy/hud/app/device/light/LED$1;->this$0:Lcom/navdy/hud/app/device/light/LED;

    invoke-virtual {v0}, Lcom/navdy/hud/app/device/light/LED$Settings;->getBlinkFrequency()Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;

    move-result-object v2

    # invokes: Lcom/navdy/hud/app/device/light/LED;->setBlinkFrequency(Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;)V
    invoke-static {v1, v2}, Lcom/navdy/hud/app/device/light/LED;->access$300(Lcom/navdy/hud/app/device/light/LED;Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;)V

    .line 303
    iget-object v1, p0, Lcom/navdy/hud/app/device/light/LED$1;->this$0:Lcom/navdy/hud/app/device/light/LED;

    # invokes: Lcom/navdy/hud/app/device/light/LED;->startBlinking()V
    invoke-static {v1}, Lcom/navdy/hud/app/device/light/LED;->access$400(Lcom/navdy/hud/app/device/light/LED;)V

    goto :goto_0

    .line 305
    :cond_2
    iget-object v1, p0, Lcom/navdy/hud/app/device/light/LED$1;->this$0:Lcom/navdy/hud/app/device/light/LED;

    invoke-virtual {v0}, Lcom/navdy/hud/app/device/light/LED$Settings;->getBlinkPulseCount()I

    move-result v2

    invoke-virtual {v0}, Lcom/navdy/hud/app/device/light/LED$Settings;->getColor()I

    move-result v3

    invoke-virtual {v0}, Lcom/navdy/hud/app/device/light/LED$Settings;->getBlinkFrequency()Lcom/navdy/hud/app/device/light/LED$BlinkFrequency;

    move-result-object v4

    # invokes: Lcom/navdy/hud/app/device/light/LED;->startBlinking(IILcom/navdy/hud/app/device/light/LED$BlinkFrequency;)V
    invoke-static {v1, v2, v3, v4}, Lcom/navdy/hud/app/device/light/LED;->access$500(Lcom/navdy/hud/app/device/light/LED;IILcom/navdy/hud/app/device/light/LED$BlinkFrequency;)V

    goto :goto_0

    .line 308
    :cond_3
    iget-object v1, p0, Lcom/navdy/hud/app/device/light/LED$1;->this$0:Lcom/navdy/hud/app/device/light/LED;

    # invokes: Lcom/navdy/hud/app/device/light/LED;->stopBlinking()V
    invoke-static {v1}, Lcom/navdy/hud/app/device/light/LED;->access$600(Lcom/navdy/hud/app/device/light/LED;)V

    goto :goto_0

    .line 311
    :cond_4
    iget-object v1, p0, Lcom/navdy/hud/app/device/light/LED$1;->this$0:Lcom/navdy/hud/app/device/light/LED;

    invoke-virtual {v1}, Lcom/navdy/hud/app/device/light/LED;->turnOff()V

    goto :goto_0
.end method
