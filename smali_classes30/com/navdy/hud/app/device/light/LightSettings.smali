.class public Lcom/navdy/hud/app/device/light/LightSettings;
.super Ljava/lang/Object;
.source "LightSettings.java"


# instance fields
.field protected color:I

.field protected isBlinking:Z

.field protected isTurnedOn:Z


# direct methods
.method public constructor <init>(IZZ)V
    .locals 0
    .param p1, "color"    # I
    .param p2, "isTurnedOn"    # Z
    .param p3, "isBlinking"    # Z

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput p1, p0, Lcom/navdy/hud/app/device/light/LightSettings;->color:I

    .line 13
    iput-boolean p2, p0, Lcom/navdy/hud/app/device/light/LightSettings;->isTurnedOn:Z

    .line 14
    iput-boolean p3, p0, Lcom/navdy/hud/app/device/light/LightSettings;->isBlinking:Z

    .line 15
    return-void
.end method


# virtual methods
.method public getColor()I
    .locals 1

    .prologue
    .line 18
    iget v0, p0, Lcom/navdy/hud/app/device/light/LightSettings;->color:I

    return v0
.end method

.method public isBlinking()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/navdy/hud/app/device/light/LightSettings;->isBlinking:Z

    return v0
.end method

.method public isTurnedOn()Z
    .locals 1

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/navdy/hud/app/device/light/LightSettings;->isTurnedOn:Z

    return v0
.end method
