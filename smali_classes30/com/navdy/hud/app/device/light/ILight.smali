.class public interface abstract Lcom/navdy/hud/app/device/light/ILight;
.super Ljava/lang/Object;
.source "ILight.java"


# virtual methods
.method public abstract popSetting()V
.end method

.method public abstract pushSetting(Lcom/navdy/hud/app/device/light/LightSettings;)V
.end method

.method public abstract removeSetting(Lcom/navdy/hud/app/device/light/LightSettings;)V
.end method

.method public abstract reset()V
.end method

.method public abstract turnOff()V
.end method

.method public abstract turnOn()V
.end method
