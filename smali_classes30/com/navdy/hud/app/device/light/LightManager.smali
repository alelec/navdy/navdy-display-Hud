.class public Lcom/navdy/hud/app/device/light/LightManager;
.super Ljava/lang/Object;
.source "LightManager.java"


# static fields
.field public static final FRONT_LED:I = 0x0

.field private static final LED_DEVICE:Ljava/lang/String; = "/sys/class/leds/as3668"

.field private static final sInstance:Lcom/navdy/hud/app/device/light/LightManager;


# instance fields
.field private mLights:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/navdy/hud/app/device/light/ILight;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    new-instance v0, Lcom/navdy/hud/app/device/light/LightManager;

    invoke-direct {v0}, Lcom/navdy/hud/app/device/light/LightManager;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/device/light/LightManager;->sInstance:Lcom/navdy/hud/app/device/light/LightManager;

    .line 17
    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/device/light/LightManager;->mLights:Landroid/util/SparseArray;

    .line 27
    new-instance v0, Lcom/navdy/hud/app/device/light/LED;

    const-string v1, "/sys/class/leds/as3668"

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/device/light/LED;-><init>(Ljava/lang/String;)V

    .line 28
    .local v0, "frontLED":Lcom/navdy/hud/app/device/light/LED;
    invoke-virtual {v0}, Lcom/navdy/hud/app/device/light/LED;->isAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 29
    iget-object v1, p0, Lcom/navdy/hud/app/device/light/LightManager;->mLights:Landroid/util/SparseArray;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 31
    :cond_0
    return-void
.end method

.method public static getInstance()Lcom/navdy/hud/app/device/light/LightManager;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/navdy/hud/app/device/light/LightManager;->sInstance:Lcom/navdy/hud/app/device/light/LightManager;

    return-object v0
.end method


# virtual methods
.method public getLight(I)Lcom/navdy/hud/app/device/light/ILight;
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 34
    iget-object v0, p0, Lcom/navdy/hud/app/device/light/LightManager;->mLights:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/device/light/ILight;

    return-object v0
.end method
