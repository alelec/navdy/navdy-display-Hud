.class Lcom/navdy/hud/app/device/PowerManager$2;
.super Ljava/lang/Object;
.source "PowerManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/device/PowerManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/device/PowerManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/PowerManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/device/PowerManager;

    .prologue
    .line 204
    iput-object p1, p0, Lcom/navdy/hud/app/device/PowerManager$2;->this$0:Lcom/navdy/hud/app/device/PowerManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 207
    # getter for: Lcom/navdy/hud/app/device/PowerManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/PowerManager;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "Quiet mode has timed out - forcing full shutdown"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 208
    iget-object v0, p0, Lcom/navdy/hud/app/device/PowerManager$2;->this$0:Lcom/navdy/hud/app/device/PowerManager;

    # getter for: Lcom/navdy/hud/app/device/PowerManager;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v0}, Lcom/navdy/hud/app/device/PowerManager;->access$500(Lcom/navdy/hud/app/device/PowerManager;)Lcom/squareup/otto/Bus;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/event/Shutdown;

    sget-object v2, Lcom/navdy/hud/app/event/Shutdown$Reason;->TIMEOUT:Lcom/navdy/hud/app/event/Shutdown$Reason;

    invoke-direct {v1, v2}, Lcom/navdy/hud/app/event/Shutdown;-><init>(Lcom/navdy/hud/app/event/Shutdown$Reason;)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 209
    return-void
.end method
