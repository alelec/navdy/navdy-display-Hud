.class final enum Lcom/navdy/hud/app/device/PowerManager$RunState;
.super Ljava/lang/Enum;
.source "PowerManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/device/PowerManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "RunState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/device/PowerManager$RunState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/device/PowerManager$RunState;

.field public static final enum Booting:Lcom/navdy/hud/app/device/PowerManager$RunState;

.field public static final enum Running:Lcom/navdy/hud/app/device/PowerManager$RunState;

.field public static final enum Unknown:Lcom/navdy/hud/app/device/PowerManager$RunState;

.field public static final enum Waking:Lcom/navdy/hud/app/device/PowerManager$RunState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 106
    new-instance v0, Lcom/navdy/hud/app/device/PowerManager$RunState;

    const-string v1, "Unknown"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/device/PowerManager$RunState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/device/PowerManager$RunState;->Unknown:Lcom/navdy/hud/app/device/PowerManager$RunState;

    new-instance v0, Lcom/navdy/hud/app/device/PowerManager$RunState;

    const-string v1, "Booting"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/device/PowerManager$RunState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/device/PowerManager$RunState;->Booting:Lcom/navdy/hud/app/device/PowerManager$RunState;

    new-instance v0, Lcom/navdy/hud/app/device/PowerManager$RunState;

    const-string v1, "Waking"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/device/PowerManager$RunState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/device/PowerManager$RunState;->Waking:Lcom/navdy/hud/app/device/PowerManager$RunState;

    new-instance v0, Lcom/navdy/hud/app/device/PowerManager$RunState;

    const-string v1, "Running"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/app/device/PowerManager$RunState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/device/PowerManager$RunState;->Running:Lcom/navdy/hud/app/device/PowerManager$RunState;

    .line 105
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/navdy/hud/app/device/PowerManager$RunState;

    sget-object v1, Lcom/navdy/hud/app/device/PowerManager$RunState;->Unknown:Lcom/navdy/hud/app/device/PowerManager$RunState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/device/PowerManager$RunState;->Booting:Lcom/navdy/hud/app/device/PowerManager$RunState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/device/PowerManager$RunState;->Waking:Lcom/navdy/hud/app/device/PowerManager$RunState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/device/PowerManager$RunState;->Running:Lcom/navdy/hud/app/device/PowerManager$RunState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/navdy/hud/app/device/PowerManager$RunState;->$VALUES:[Lcom/navdy/hud/app/device/PowerManager$RunState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 105
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/device/PowerManager$RunState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 105
    const-class v0, Lcom/navdy/hud/app/device/PowerManager$RunState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/device/PowerManager$RunState;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/device/PowerManager$RunState;
    .locals 1

    .prologue
    .line 105
    sget-object v0, Lcom/navdy/hud/app/device/PowerManager$RunState;->$VALUES:[Lcom/navdy/hud/app/device/PowerManager$RunState;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/device/PowerManager$RunState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/device/PowerManager$RunState;

    return-object v0
.end method
