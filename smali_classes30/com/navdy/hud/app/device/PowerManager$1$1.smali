.class Lcom/navdy/hud/app/device/PowerManager$1$1;
.super Ljava/lang/Object;
.source "PowerManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/device/PowerManager$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/device/PowerManager$1;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/PowerManager$1;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/device/PowerManager$1;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/navdy/hud/app/device/PowerManager$1$1;->this$1:Lcom/navdy/hud/app/device/PowerManager$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 144
    iget-object v0, p0, Lcom/navdy/hud/app/device/PowerManager$1$1;->this$1:Lcom/navdy/hud/app/device/PowerManager$1;

    iget-object v0, v0, Lcom/navdy/hud/app/device/PowerManager$1;->this$0:Lcom/navdy/hud/app/device/PowerManager;

    # invokes: Lcom/navdy/hud/app/device/PowerManager;->updateCoolingState()Z
    invoke-static {v0}, Lcom/navdy/hud/app/device/PowerManager;->access$000(Lcom/navdy/hud/app/device/PowerManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/navdy/hud/app/device/PowerManager$1$1;->this$1:Lcom/navdy/hud/app/device/PowerManager$1;

    iget-object v0, v0, Lcom/navdy/hud/app/device/PowerManager$1;->this$0:Lcom/navdy/hud/app/device/PowerManager;

    # getter for: Lcom/navdy/hud/app/device/PowerManager;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/device/PowerManager;->access$300(Lcom/navdy/hud/app/device/PowerManager;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/device/PowerManager$1$1;->this$1:Lcom/navdy/hud/app/device/PowerManager$1;

    iget-object v1, v1, Lcom/navdy/hud/app/device/PowerManager$1;->this$0:Lcom/navdy/hud/app/device/PowerManager;

    # getter for: Lcom/navdy/hud/app/device/PowerManager;->checkCoolingState:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/navdy/hud/app/device/PowerManager;->access$100(Lcom/navdy/hud/app/device/PowerManager;)Ljava/lang/Runnable;

    move-result-object v1

    # getter for: Lcom/navdy/hud/app/device/PowerManager;->COOLING_MONITOR_INTERVAL:J
    invoke-static {}, Lcom/navdy/hud/app/device/PowerManager;->access$200()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 147
    :cond_0
    return-void
.end method
