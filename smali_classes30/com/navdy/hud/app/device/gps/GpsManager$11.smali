.class Lcom/navdy/hud/app/device/gps/GpsManager$11;
.super Landroid/content/BroadcastReceiver;
.source "GpsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/device/gps/GpsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/device/gps/GpsManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/gps/GpsManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/device/gps/GpsManager;

    .prologue
    .line 508
    iput-object p1, p0, Lcom/navdy/hud/app/device/gps/GpsManager$11;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x0

    .line 512
    :try_start_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 513
    .local v0, "action":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 549
    .end local v0    # "action":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 516
    .restart local v0    # "action":Ljava/lang/String;
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/debug/RouteRecorder;->getInstance()Lcom/navdy/hud/app/debug/RouteRecorder;

    move-result-object v2

    .line 517
    .local v2, "routeRecorder":Lcom/navdy/hud/app/debug/RouteRecorder;
    const/4 v5, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    :cond_2
    move v4, v5

    :goto_1
    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 519
    :pswitch_0
    invoke-virtual {v2}, Lcom/navdy/hud/app/debug/RouteRecorder;->isRecording()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 522
    const-string v4, "drtype"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 523
    .local v1, "marker":Ljava/lang/String;
    if-nez v1, :cond_3

    .line 524
    const-string v1, ""

    .line 526
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GPS_DR_STARTED "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/navdy/hud/app/debug/RouteRecorder;->injectMarker(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 546
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "marker":Ljava/lang/String;
    .end local v2    # "routeRecorder":Lcom/navdy/hud/app/debug/RouteRecorder;
    :catch_0
    move-exception v3

    .line 547
    .local v3, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 517
    .end local v3    # "t":Ljava/lang/Throwable;
    .restart local v0    # "action":Ljava/lang/String;
    .restart local v2    # "routeRecorder":Lcom/navdy/hud/app/debug/RouteRecorder;
    :sswitch_0
    :try_start_1
    const-string v6, "GPS_DR_STARTED"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    goto :goto_1

    :sswitch_1
    const-string v4, "GPS_DR_STOPPED"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    goto :goto_1

    :sswitch_2
    const-string v4, "EXTRAPOLATION"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x2

    goto :goto_1

    .line 530
    :pswitch_1
    invoke-virtual {v2}, Lcom/navdy/hud/app/debug/RouteRecorder;->isRecording()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 533
    const-string v4, "GPS_DR_STOPPED"

    invoke-virtual {v2, v4}, Lcom/navdy/hud/app/debug/RouteRecorder;->injectMarker(Ljava/lang/String;)V

    goto :goto_0

    .line 537
    :pswitch_2
    iget-object v4, p0, Lcom/navdy/hud/app/device/gps/GpsManager$11;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    const-string v5, "EXTRAPOLATION_FLAG"

    const/4 v6, 0x0

    invoke-virtual {p2, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    # setter for: Lcom/navdy/hud/app/device/gps/GpsManager;->extrapolationOn:Z
    invoke-static {v4, v5}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$2402(Lcom/navdy/hud/app/device/gps/GpsManager;Z)Z

    .line 538
    iget-object v4, p0, Lcom/navdy/hud/app/device/gps/GpsManager$11;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->extrapolationOn:Z
    invoke-static {v4}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$2400(Lcom/navdy/hud/app/device/gps/GpsManager;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 539
    iget-object v4, p0, Lcom/navdy/hud/app/device/gps/GpsManager$11;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    # setter for: Lcom/navdy/hud/app/device/gps/GpsManager;->extrapolationStartTime:J
    invoke-static {v4, v6, v7}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$2502(Lcom/navdy/hud/app/device/gps/GpsManager;J)J

    .line 543
    :goto_2
    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "extrapolation is:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/device/gps/GpsManager$11;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->extrapolationOn:Z
    invoke-static {v6}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$2400(Lcom/navdy/hud/app/device/gps/GpsManager;)Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " time:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/device/gps/GpsManager$11;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->extrapolationStartTime:J
    invoke-static {v6}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$2500(Lcom/navdy/hud/app/device/gps/GpsManager;)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 541
    :cond_4
    iget-object v4, p0, Lcom/navdy/hud/app/device/gps/GpsManager$11;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    const-wide/16 v6, 0x0

    # setter for: Lcom/navdy/hud/app/device/gps/GpsManager;->extrapolationStartTime:J
    invoke-static {v4, v6, v7}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$2502(Lcom/navdy/hud/app/device/gps/GpsManager;J)J
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 517
    nop

    :sswitch_data_0
    .sparse-switch
        -0x6b600b7b -> :sswitch_0
        -0x6a9bba2f -> :sswitch_1
        -0x2d77348 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
