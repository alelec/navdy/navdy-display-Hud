.class Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$2;
.super Ljava/lang/Object;
.source "GpsDeadReckoningManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    .prologue
    .line 251
    iput-object p1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$2;->this$0:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 254
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$2;->this$0:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$2;->this$0:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->injectSpeed:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$CommandWriter;
    invoke-static {v1}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->access$100(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$CommandWriter;

    move-result-object v1

    # invokes: Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->invokeUblox(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$CommandWriter;)Z
    invoke-static {v0, v1}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->access$200(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$CommandWriter;)Z

    .line 255
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$2;->this$0:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->deadReckoningInjectionStarted:Z
    invoke-static {v0}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->access$300(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$2;->this$0:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->lastConnectionFailure:J
    invoke-static {v0}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->access$400(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 256
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$2;->this$0:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->access$500(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0x64

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 260
    :goto_0
    return-void

    .line 258
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$2;->this$0:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->access$500(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
