.class Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$7;
.super Ljava/lang/Object;
.source "GpsDeadReckoningManager.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    .prologue
    .line 310
    iput-object p1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$7;->this$0:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x1

    .line 313
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 328
    :cond_0
    :goto_0
    const/4 v2, 0x0

    return v2

    .line 315
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$Alignment;

    .line 317
    .local v0, "alignment":Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$Alignment;
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$7;->this$0:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    # invokes: Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->handleAutoAlignmentResult(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$Alignment;)V
    invoke-static {v2, v0}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->access$1600(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$Alignment;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 318
    :catch_0
    move-exception v1

    .line 319
    .local v1, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->access$1000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 320
    iget-object v2, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$7;->this$0:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->waitForAutoAlignment:Z
    invoke-static {v2}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->access$1700(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 321
    iget-object v2, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$7;->this$0:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    # invokes: Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->postAlignmentRunnable(Z)V
    invoke-static {v2, v3}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->access$1400(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;Z)V

    goto :goto_0

    .line 322
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$7;->this$0:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->waitForFusionStatus:Z
    invoke-static {v2}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->access$1800(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 323
    iget-object v2, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$7;->this$0:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    # invokes: Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->postFusionRunnable(Z)V
    invoke-static {v2, v3}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->access$900(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;Z)V

    goto :goto_0

    .line 313
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
