.class Lcom/navdy/hud/app/device/gps/GpsManager$9;
.super Ljava/lang/Object;
.source "GpsManager.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/device/gps/GpsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/device/gps/GpsManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/gps/GpsManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/device/gps/GpsManager;

    .prologue
    .line 460
    iput-object p1, p0, Lcom/navdy/hud/app/device/gps/GpsManager$9;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v8, 0x1

    .line 464
    :try_start_0
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 483
    :cond_0
    :goto_0
    return v8

    .line 466
    :pswitch_0
    iget-object v3, p0, Lcom/navdy/hud/app/device/gps/GpsManager$9;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->nmeaParser:Lcom/navdy/hud/app/device/gps/GpsNmeaParser;
    invoke-static {v3}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$2600(Lcom/navdy/hud/app/device/gps/GpsManager;)Lcom/navdy/hud/app/device/gps/GpsNmeaParser;

    move-result-object v4

    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->parseNmeaMessage(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 480
    :catch_0
    move-exception v2

    .line 481
    .local v2, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 472
    .end local v2    # "t":Ljava/lang/Throwable;
    :pswitch_1
    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-object v3, p0, Lcom/navdy/hud/app/device/gps/GpsManager$9;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->gpsManagerStartTime:J
    invoke-static {v3}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$2700(Lcom/navdy/hud/app/device/gps/GpsManager;)J

    move-result-wide v6

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x3a98

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    .line 475
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/os/Bundle;

    .line 476
    .local v1, "dataBundle":Landroid/os/Bundle;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 477
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v3, "satellite_data"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 478
    iget-object v3, p0, Lcom/navdy/hud/app/device/gps/GpsManager$9;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    const-string v4, "GPS_SATELLITE_STATUS"

    # invokes: Lcom/navdy/hud/app/device/gps/GpsManager;->sendGpsEventBroadcast(Ljava/lang/String;Landroid/os/Bundle;)V
    invoke-static {v3, v4, v0}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$2300(Lcom/navdy/hud/app/device/gps/GpsManager;Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 464
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
