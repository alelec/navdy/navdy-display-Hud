.class public Lcom/navdy/hud/app/device/gps/GpsUtils;
.super Ljava/lang/Object;
.source "GpsUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;,
        Lcom/navdy/hud/app/device/gps/GpsUtils$GpsSwitch;,
        Lcom/navdy/hud/app/device/gps/GpsUtils$GpsSatelliteData;
    }
.end annotation


# static fields
.field public static SHOW_RAW_GPS:Lcom/navdy/hud/app/config/BooleanSetting;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 22
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/device/gps/GpsUtils;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/device/gps/GpsUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 24
    new-instance v0, Lcom/navdy/hud/app/config/BooleanSetting;

    const-string v1, "Show Raw GPS"

    sget-object v2, Lcom/navdy/hud/app/config/BooleanSetting$Scope;->NEVER:Lcom/navdy/hud/app/config/BooleanSetting$Scope;

    const-string v3, "map.raw_gps"

    const-string v4, "Add map indicators showing raw and map matched GPS"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/config/BooleanSetting;-><init>(Ljava/lang/String;Lcom/navdy/hud/app/config/BooleanSetting$Scope;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/device/gps/GpsUtils;->SHOW_RAW_GPS:Lcom/navdy/hud/app/config/BooleanSetting;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getHeadingDirection(D)Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;
    .locals 8
    .param p0, "heading"    # D

    .prologue
    const-wide v6, 0x4076800000000000L    # 360.0

    .line 74
    sget-object v0, Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;->N:Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

    .line 75
    .local v0, "headingDirection":Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;
    const-wide/16 v4, 0x0

    cmpg-double v3, p0, v4

    if-ltz v3, :cond_0

    cmpl-double v3, p0, v6

    if-lez v3, :cond_1

    .line 80
    .end local v0    # "headingDirection":Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;
    :cond_0
    :goto_0
    return-object v0

    .line 78
    .restart local v0    # "headingDirection":Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;
    :cond_1
    const-wide v4, 0x4036800000000000L    # 22.5

    add-double/2addr v4, p0

    rem-double/2addr v4, v6

    double-to-float v1, v4

    .line 79
    .local v1, "normalized":F
    const/high16 v3, 0x42340000    # 45.0f

    div-float v3, v1, v3

    float-to-int v2, v3

    .line 80
    .local v2, "ordinal":I
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;->values()[Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

    move-result-object v3

    aget-object v0, v3, v2

    goto :goto_0
.end method

.method public static isDebugRawGpsPosEnabled()Z
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/navdy/hud/app/device/gps/GpsUtils;->SHOW_RAW_GPS:Lcom/navdy/hud/app/config/BooleanSetting;

    invoke-virtual {v0}, Lcom/navdy/hud/app/config/BooleanSetting;->isEnabled()Z

    move-result v0

    return v0
.end method

.method public static sendEventBroadcast(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 5
    .param p0, "eventName"    # Ljava/lang/String;
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 85
    :try_start_0
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, p0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 86
    .local v2, "intent":Landroid/content/Intent;
    if-eqz p1, :cond_0

    .line 87
    invoke-virtual {v2, p1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 89
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 90
    .local v0, "context":Landroid/content/Context;
    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v1

    .line 91
    .local v1, "handle":Landroid/os/UserHandle;
    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "handle":Landroid/os/UserHandle;
    .end local v2    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 92
    :catch_0
    move-exception v3

    .line 93
    .local v3, "t":Ljava/lang/Throwable;
    sget-object v4, Lcom/navdy/hud/app/device/gps/GpsUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v4, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
