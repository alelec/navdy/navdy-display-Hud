.class public final Lcom/navdy/hud/app/device/gps/CalibratedGForceData;
.super Ljava/lang/Object;
.source "CalibratedGForceData.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0007\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\'\u0010\u000e\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\u0008R\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u0008\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/navdy/hud/app/device/gps/CalibratedGForceData;",
        "",
        "xAccel",
        "",
        "yAccel",
        "zAccel",
        "(FFF)V",
        "getXAccel",
        "()F",
        "getYAccel",
        "getZAccel",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# instance fields
.field private final xAccel:F

.field private final yAccel:F

.field private final zAccel:F


# direct methods
.method public constructor <init>(FFF)V
    .locals 0
    .param p1, "xAccel"    # F
    .param p2, "yAccel"    # F
    .param p3, "zAccel"    # F

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;->xAccel:F

    iput p2, p0, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;->yAccel:F

    iput p3, p0, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;->zAccel:F

    return-void
.end method

.method public static bridge synthetic copy$default(Lcom/navdy/hud/app/device/gps/CalibratedGForceData;FFFILjava/lang/Object;)Lcom/navdy/hud/app/device/gps/CalibratedGForceData;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    and-int/lit8 v0, p4, 0x1

    if-eqz v0, :cond_0

    iget p1, p0, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;->xAccel:F

    :cond_0
    and-int/lit8 v0, p4, 0x2

    if-eqz v0, :cond_1

    iget p2, p0, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;->yAccel:F

    :cond_1
    and-int/lit8 v0, p4, 0x4

    if-eqz v0, :cond_2

    iget p3, p0, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;->zAccel:F

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;->copy(FFF)Lcom/navdy/hud/app/device/gps/CalibratedGForceData;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()F
    .locals 1

    .prologue
    iget v0, p0, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;->xAccel:F

    return v0
.end method

.method public final component2()F
    .locals 1

    .prologue
    iget v0, p0, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;->yAccel:F

    return v0
.end method

.method public final component3()F
    .locals 1

    .prologue
    iget v0, p0, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;->zAccel:F

    return v0
.end method

.method public final copy(FFF)Lcom/navdy/hud/app/device/gps/CalibratedGForceData;
    .locals 1
    .param p1, "xAccel"    # F
    .param p2, "yAccel"    # F
    .param p3, "zAccel"    # F
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .prologue
    new-instance v0, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;

    invoke-direct {v0, p1, p2, p3}, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;-><init>(FFF)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_0

    instance-of v0, p1, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;

    iget v0, p0, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;->xAccel:F

    iget v1, p1, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;->xAccel:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;->yAccel:F

    iget v1, p1, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;->yAccel:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;->zAccel:F

    iget v1, p1, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;->zAccel:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getXAccel()F
    .locals 1

    .prologue
    .line 7
    iget v0, p0, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;->xAccel:F

    return v0
.end method

.method public final getYAccel()F
    .locals 1

    .prologue
    .line 7
    iget v0, p0, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;->yAccel:F

    return v0
.end method

.method public final getZAccel()F
    .locals 1

    .prologue
    .line 7
    iget v0, p0, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;->zAccel:F

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;->xAccel:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;->yAccel:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;->zAccel:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CalibratedGForceData(xAccel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;->xAccel:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", yAccel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;->yAccel:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", zAccel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;->zAccel:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
