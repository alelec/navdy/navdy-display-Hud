.class public Lcom/navdy/hud/app/device/gps/GpsManager;
.super Ljava/lang/Object;
.source "GpsManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;
    }
.end annotation


# static fields
.field private static final ACCEPTABLE_THRESHOLD:I = 0x2

.field private static final DISCARD_TIME:Ljava/lang/String; = "DISCARD_TIME"

.field private static final GPS_SYSTEM_PROPERTY:Ljava/lang/String; = "persist.sys.hud_gps"

.field public static final INACCURATE_GPS_REPORT_INTERVAL:J

.field private static final INITIAL_INTERVAL:I = 0x3a98

.field private static final KEEP_PHONE_GPS_ON:I = -0x1

.field private static final LOCATION_ACCURACY_THROW_AWAY_THRESHOLD:I = 0x1e

.field private static final LOCATION_TIME_THROW_AWAY_THRESHOLD:I = 0x7d0

.field public static final MAXIMUM_STOPPED_SPEED:D = 0.22353333333333333

.field private static final METERS_PER_MILE:D = 1609.44

.field public static final MINIMUM_DRIVE_TIME:J = 0xbb8L

.field public static final MINIMUM_DRIVING_SPEED:D = 2.2353333333333336

.field static final MSG_NMEA:I = 0x1

.field static final MSG_SATELLITE_STATUS:I = 0x2

.field private static final SECONDS_PER_HOUR:I = 0xe10

.field private static final SET_LOCATION_DISCARD_TIME_THRESHOLD:Ljava/lang/String; = "SET_LOCATION_DISCARD_TIME_THRESHOLD"

.field private static final SET_UBLOX_ACCURACY:Ljava/lang/String; = "SET_UBLOX_ACCURACY"

.field private static final START_SENDING_LOCATION:Lcom/navdy/service/library/events/location/TransmitLocation;

.field private static final START_UBLOX:Ljava/lang/String; = "START_REPORTING_UBLOX_LOCATION"

.field private static final STOP_SENDING_LOCATION:Lcom/navdy/service/library/events/location/TransmitLocation;

.field private static final STOP_UBLOX:Ljava/lang/String; = "STOP_REPORTING_UBLOX_LOCATION"

.field private static final TAG_GPS:Ljava/lang/String; = "[Gps-i] "

.field private static final TAG_GPS_LOC:Ljava/lang/String; = "[Gps-loc] "

.field private static final TAG_GPS_LOC_NAVDY:Ljava/lang/String; = "[Gps-loc-navdy] "

.field private static final TAG_GPS_NMEA:Ljava/lang/String; = "[Gps-nmea] "

.field private static final TAG_GPS_STATUS:Ljava/lang/String; = "[Gps-stat] "

.field private static final TAG_PHONE_LOC:Ljava/lang/String; = "[Phone-loc] "

.field private static final UBLOX_ACCURACY:Ljava/lang/String; = "UBLOX_ACCURACY"

.field private static final UBLOX_MIN_ACCEPTABLE_THRESHOLD:F = 5.0f

.field private static final WARM_RESET_INTERVAL:J

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static final singleton:Lcom/navdy/hud/app/device/gps/GpsManager;


# instance fields
.field private activeLocationSource:Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

.field private connectionService:Lcom/navdy/hud/app/service/HudConnectionService;

.field private driving:Z

.field private eventReceiver:Landroid/content/BroadcastReceiver;

.field private volatile extrapolationOn:Z

.field private volatile extrapolationStartTime:J

.field private firstSwitchToUbloxFromPhone:Z

.field private gpsAccuracy:F

.field private gpsLastEventTime:J

.field private gpsManagerStartTime:J

.field private handler:Landroid/os/Handler;

.field private inaccurateGpsCount:J

.field private isDebugTTSEnabled:Z

.field private keepPhoneGpsOn:Z

.field private lastInaccurateGpsTime:J

.field private lastPhoneCoordinate:Lcom/navdy/service/library/events/location/Coordinate;

.field private lastPhoneCoordinateTime:J

.field private lastUbloxCoordinate:Landroid/location/Location;

.field private lastUbloxCoordinateTime:J

.field private locationFixRunnable:Ljava/lang/Runnable;

.field private locationManager:Landroid/location/LocationManager;

.field private navdyGpsLocationListener:Landroid/location/LocationListener;

.field private nmeaCallback:Landroid/os/Handler$Callback;

.field private nmeaHandler:Landroid/os/Handler;

.field private nmeaHandlerThread:Landroid/os/HandlerThread;

.field private nmeaParser:Lcom/navdy/hud/app/device/gps/GpsNmeaParser;

.field private noLocationUpdatesRunnable:Ljava/lang/Runnable;

.field private noPhoneLocationFixRunnable:Ljava/lang/Runnable;

.field private queue:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field

.field private queueCallback:Ljava/lang/Runnable;

.field private volatile startUbloxCalled:Z

.field private switchBetweenPhoneUbloxAllowed:Z

.field private transitionStartTime:J

.field private transitioning:Z

.field private ubloxGpsLocationListener:Landroid/location/LocationListener;

.field private ubloxGpsNmeaListener:Landroid/location/GpsStatus$NmeaListener;

.field private ubloxGpsStatusListener:Landroid/location/GpsStatus$Listener;

.field private warmResetCancelled:Z

.field private warmResetRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 44
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/device/gps/GpsManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 76
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/device/gps/GpsManager;->INACCURATE_GPS_REPORT_INTERVAL:J

    .line 91
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa0

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/device/gps/GpsManager;->WARM_RESET_INTERVAL:J

    .line 93
    new-instance v0, Lcom/navdy/service/library/events/location/TransmitLocation;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/navdy/service/library/events/location/TransmitLocation;-><init>(Ljava/lang/Boolean;)V

    sput-object v0, Lcom/navdy/hud/app/device/gps/GpsManager;->START_SENDING_LOCATION:Lcom/navdy/service/library/events/location/TransmitLocation;

    .line 94
    new-instance v0, Lcom/navdy/service/library/events/location/TransmitLocation;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/navdy/service/library/events/location/TransmitLocation;-><init>(Ljava/lang/Boolean;)V

    sput-object v0, Lcom/navdy/hud/app/device/gps/GpsManager;->STOP_SENDING_LOCATION:Lcom/navdy/service/library/events/location/TransmitLocation;

    .line 96
    new-instance v0, Lcom/navdy/hud/app/device/gps/GpsManager;

    invoke-direct {v0}, Lcom/navdy/hud/app/device/gps/GpsManager;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/device/gps/GpsManager;->singleton:Lcom/navdy/hud/app/device/gps/GpsManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 18

    .prologue
    .line 552
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 98
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->queue:Ljava/util/ArrayList;

    .line 105
    new-instance v2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->handler:Landroid/os/Handler;

    .line 165
    new-instance v2, Lcom/navdy/hud/app/device/gps/GpsManager$1;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/navdy/hud/app/device/gps/GpsManager$1;-><init>(Lcom/navdy/hud/app/device/gps/GpsManager;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->ubloxGpsNmeaListener:Landroid/location/GpsStatus$NmeaListener;

    .line 181
    new-instance v2, Lcom/navdy/hud/app/device/gps/GpsManager$2;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/navdy/hud/app/device/gps/GpsManager$2;-><init>(Lcom/navdy/hud/app/device/gps/GpsManager;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->ubloxGpsLocationListener:Landroid/location/LocationListener;

    .line 244
    new-instance v2, Lcom/navdy/hud/app/device/gps/GpsManager$3;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/navdy/hud/app/device/gps/GpsManager$3;-><init>(Lcom/navdy/hud/app/device/gps/GpsManager;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->navdyGpsLocationListener:Landroid/location/LocationListener;

    .line 331
    new-instance v2, Lcom/navdy/hud/app/device/gps/GpsManager$4;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/navdy/hud/app/device/gps/GpsManager$4;-><init>(Lcom/navdy/hud/app/device/gps/GpsManager;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->ubloxGpsStatusListener:Landroid/location/GpsStatus$Listener;

    .line 356
    new-instance v2, Lcom/navdy/hud/app/device/gps/GpsManager$5;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/navdy/hud/app/device/gps/GpsManager$5;-><init>(Lcom/navdy/hud/app/device/gps/GpsManager;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->noPhoneLocationFixRunnable:Ljava/lang/Runnable;

    .line 379
    new-instance v2, Lcom/navdy/hud/app/device/gps/GpsManager$6;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/navdy/hud/app/device/gps/GpsManager$6;-><init>(Lcom/navdy/hud/app/device/gps/GpsManager;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->noLocationUpdatesRunnable:Ljava/lang/Runnable;

    .line 392
    new-instance v2, Lcom/navdy/hud/app/device/gps/GpsManager$7;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/navdy/hud/app/device/gps/GpsManager$7;-><init>(Lcom/navdy/hud/app/device/gps/GpsManager;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->warmResetRunnable:Ljava/lang/Runnable;

    .line 406
    new-instance v2, Lcom/navdy/hud/app/device/gps/GpsManager$8;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/navdy/hud/app/device/gps/GpsManager$8;-><init>(Lcom/navdy/hud/app/device/gps/GpsManager;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->locationFixRunnable:Ljava/lang/Runnable;

    .line 460
    new-instance v2, Lcom/navdy/hud/app/device/gps/GpsManager$9;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/navdy/hud/app/device/gps/GpsManager$9;-><init>(Lcom/navdy/hud/app/device/gps/GpsManager;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->nmeaCallback:Landroid/os/Handler$Callback;

    .line 487
    new-instance v2, Lcom/navdy/hud/app/device/gps/GpsManager$10;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/navdy/hud/app/device/gps/GpsManager$10;-><init>(Lcom/navdy/hud/app/device/gps/GpsManager;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->queueCallback:Ljava/lang/Runnable;

    .line 508
    new-instance v2, Lcom/navdy/hud/app/device/gps/GpsManager$11;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/navdy/hud/app/device/gps/GpsManager$11;-><init>(Lcom/navdy/hud/app/device/gps/GpsManager;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->eventReceiver:Landroid/content/BroadcastReceiver;

    .line 553
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v13

    .line 556
    .local v13, "context":Landroid/content/Context;
    :try_start_0
    const-string v2, "persist.sys.hud_gps"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/navdy/hud/app/util/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v16

    .line 557
    .local v16, "n":I
    const/4 v2, -0x1

    move/from16 v0, v16

    if-ne v0, v2, :cond_0

    .line 558
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->keepPhoneGpsOn:Z

    .line 559
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->switchBetweenPhoneUbloxAllowed:Z

    .line 560
    sget-object v2, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "switch between phone and ublox allowed"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 562
    :cond_0
    sget-object v2, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "keepPhoneGpsOn["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->keepPhoneGpsOn:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] val["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 564
    new-instance v2, Ljava/io/File;

    const-string v3, "/sdcard/debug_tts"

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->isDebugTTSEnabled:Z

    .line 566
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/device/gps/GpsManager;->setUbloxAccuracyThreshold()V

    .line 567
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/device/gps/GpsManager;->setUbloxTimeDiscardThreshold()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 573
    .end local v16    # "n":I
    :goto_0
    const/4 v15, 0x0

    .line 574
    .local v15, "gpsProvider":Landroid/location/LocationProvider;
    const-string v2, "location"

    invoke-virtual {v13, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/location/LocationManager;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->locationManager:Landroid/location/LocationManager;

    .line 577
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->locationManager:Landroid/location/LocationManager;

    const-string v3, "gps"

    invoke-virtual {v2, v3}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 578
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->locationManager:Landroid/location/LocationManager;

    const-string v3, "gps"

    invoke-virtual {v2, v3}, Landroid/location/LocationManager;->getProvider(Ljava/lang/String;)Landroid/location/LocationProvider;

    move-result-object v15

    .line 579
    if-eqz v15, :cond_1

    .line 580
    new-instance v2, Landroid/os/HandlerThread;

    const-string v3, "navdynmea"

    invoke-direct {v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->nmeaHandlerThread:Landroid/os/HandlerThread;

    .line 581
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->nmeaHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->start()V

    .line 582
    new-instance v2, Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->nmeaHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->nmeaCallback:Landroid/os/Handler$Callback;

    invoke-direct {v2, v3, v4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->nmeaHandler:Landroid/os/Handler;

    .line 583
    new-instance v2, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;

    sget-object v3, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->nmeaHandler:Landroid/os/Handler;

    invoke-direct {v2, v3, v4}, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;-><init>(Lcom/navdy/service/library/log/Logger;Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->nmeaParser:Lcom/navdy/hud/app/device/gps/GpsNmeaParser;

    .line 584
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->locationManager:Landroid/location/LocationManager;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->ubloxGpsStatusListener:Landroid/location/GpsStatus$Listener;

    invoke-virtual {v2, v3}, Landroid/location/LocationManager;->addGpsStatusListener(Landroid/location/GpsStatus$Listener;)Z

    .line 585
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->locationManager:Landroid/location/LocationManager;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->ubloxGpsNmeaListener:Landroid/location/GpsStatus$NmeaListener;

    invoke-virtual {v2, v3}, Landroid/location/LocationManager;->addNmeaListener(Landroid/location/GpsStatus$NmeaListener;)Z

    .line 588
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isNavdyDevice()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 589
    sget-object v2, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "[Gps-i] setting up ublox gps"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 590
    if-eqz v15, :cond_3

    .line 591
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->locationManager:Landroid/location/LocationManager;

    const-string v3, "gps"

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->ubloxGpsLocationListener:Landroid/location/LocationListener;

    invoke-virtual/range {v2 .. v7}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 595
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->locationManager:Landroid/location/LocationManager;

    const-string v3, "NAVDY_GPS_PROVIDER"

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->navdyGpsLocationListener:Landroid/location/LocationListener;

    invoke-virtual/range {v2 .. v7}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 596
    sget-object v2, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "requestLocationUpdates successful for NAVDY_GPS_PROVIDER"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 600
    :goto_1
    sget-object v2, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "[Gps-i] ublox gps listeners installed"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 609
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->locationManager:Landroid/location/LocationManager;

    const-string v3, "network"

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x1

    const/4 v10, 0x1

    const/4 v11, 0x0

    const/4 v12, 0x3

    invoke-virtual/range {v2 .. v12}, Landroid/location/LocationManager;->addTestProvider(Ljava/lang/String;ZZZZZZZII)V

    .line 610
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->locationManager:Landroid/location/LocationManager;

    const-string v3, "network"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/location/LocationManager;->setTestProviderEnabled(Ljava/lang/String;Z)V

    .line 611
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->locationManager:Landroid/location/LocationManager;

    const-string v3, "network"

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual/range {v2 .. v7}, Landroid/location/LocationManager;->setTestProviderStatus(Ljava/lang/String;ILandroid/os/Bundle;J)V

    .line 612
    sget-object v2, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "[Gps-i] added mock network provider"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 614
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->gpsManagerStartTime:J

    .line 615
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->gpsManagerStartTime:J

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->gpsLastEventTime:J

    .line 616
    invoke-static {}, Lcom/navdy/hud/app/service/ConnectionServiceAnalyticsSupport;->recordGpsAttemptAcquireLocation()V

    .line 617
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isNavdyDevice()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 618
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/device/gps/GpsManager;->setUbloxResetRunnable()V

    .line 622
    :cond_2
    new-instance v14, Landroid/content/IntentFilter;

    invoke-direct {v14}, Landroid/content/IntentFilter;-><init>()V

    .line 623
    .local v14, "filter":Landroid/content/IntentFilter;
    const-string v2, "GPS_DR_STARTED"

    invoke-virtual {v14, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 624
    const-string v2, "GPS_DR_STOPPED"

    invoke-virtual {v14, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 625
    const-string v2, "EXTRAPOLATION"

    invoke-virtual {v14, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 626
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->eventReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v13, v2, v14}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 627
    return-void

    .line 568
    .end local v14    # "filter":Landroid/content/IntentFilter;
    .end local v15    # "gpsProvider":Landroid/location/LocationProvider;
    :catch_0
    move-exception v17

    .line 569
    .local v17, "t":Ljava/lang/Throwable;
    sget-object v2, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 597
    .end local v17    # "t":Ljava/lang/Throwable;
    .restart local v15    # "gpsProvider":Landroid/location/LocationProvider;
    :catch_1
    move-exception v17

    .line 598
    .restart local v17    # "t":Ljava/lang/Throwable;
    sget-object v2, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "requestLocationUpdates"

    move-object/from16 v0, v17

    invoke-virtual {v2, v3, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 602
    .end local v17    # "t":Ljava/lang/Throwable;
    :cond_3
    sget-object v2, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "[Gps-i] gps provider not found"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 605
    :cond_4
    sget-object v2, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "[Gps-i] not a Hud device,not setting up ublox gps"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/device/gps/GpsManager;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/device/gps/GpsManager;->processNmea(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/navdy/hud/app/device/gps/GpsManager;)Lcom/navdy/service/library/events/location/Coordinate;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsManager;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->lastPhoneCoordinate:Lcom/navdy/service/library/events/location/Coordinate;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/navdy/hud/app/device/gps/GpsManager;)Landroid/location/Location;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsManager;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->lastUbloxCoordinate:Landroid/location/Location;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/navdy/hud/app/device/gps/GpsManager;Landroid/location/Location;)Landroid/location/Location;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsManager;
    .param p1, "x1"    # Landroid/location/Location;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->lastUbloxCoordinate:Landroid/location/Location;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/navdy/hud/app/device/gps/GpsManager;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsManager;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Z
    .param p4, "x4"    # Z

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/navdy/hud/app/device/gps/GpsManager;->markLocationFix(Ljava/lang/String;Ljava/lang/String;ZZ)V

    return-void
.end method

.method static synthetic access$1300(Lcom/navdy/hud/app/device/gps/GpsManager;)J
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsManager;

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->lastUbloxCoordinateTime:J

    return-wide v0
.end method

.method static synthetic access$1302(Lcom/navdy/hud/app/device/gps/GpsManager;J)J
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsManager;
    .param p1, "x1"    # J

    .prologue
    .line 43
    iput-wide p1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->lastUbloxCoordinateTime:J

    return-wide p1
.end method

.method static synthetic access$1400(Lcom/navdy/hud/app/device/gps/GpsManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsManager;

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->warmResetCancelled:Z

    return v0
.end method

.method static synthetic access$1500(Lcom/navdy/hud/app/device/gps/GpsManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsManager;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/navdy/hud/app/device/gps/GpsManager;->cancelUbloxResetRunnable()V

    return-void
.end method

.method static synthetic access$1600(Lcom/navdy/hud/app/device/gps/GpsManager;Landroid/location/Location;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsManager;
    .param p1, "x1"    # Landroid/location/Location;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/device/gps/GpsManager;->updateDrivingState(Landroid/location/Location;)V

    return-void
.end method

.method static synthetic access$1700(Lcom/navdy/hud/app/device/gps/GpsManager;)Lcom/navdy/hud/app/service/HudConnectionService;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsManager;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->connectionService:Lcom/navdy/hud/app/service/HudConnectionService;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/navdy/hud/app/device/gps/GpsManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsManager;

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->firstSwitchToUbloxFromPhone:Z

    return v0
.end method

.method static synthetic access$1900(Lcom/navdy/hud/app/device/gps/GpsManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsManager;

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->switchBetweenPhoneUbloxAllowed:Z

    return v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/device/gps/GpsManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsManager;

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->startUbloxCalled:Z

    return v0
.end method

.method static synthetic access$2000(Lcom/navdy/hud/app/device/gps/GpsManager;Landroid/location/Location;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsManager;
    .param p1, "x1"    # Landroid/location/Location;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/device/gps/GpsManager;->checkGpsToPhoneAccuracy(Landroid/location/Location;)V

    return-void
.end method

.method static synthetic access$2100(Lcom/navdy/hud/app/device/gps/GpsManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsManager;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/navdy/hud/app/device/gps/GpsManager;->startSendingLocation()V

    return-void
.end method

.method static synthetic access$2200()J
    .locals 2

    .prologue
    .line 43
    sget-wide v0, Lcom/navdy/hud/app/device/gps/GpsManager;->WARM_RESET_INTERVAL:J

    return-wide v0
.end method

.method static synthetic access$2300(Lcom/navdy/hud/app/device/gps/GpsManager;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsManager;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Landroid/os/Bundle;

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/device/gps/GpsManager;->sendGpsEventBroadcast(Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$2400(Lcom/navdy/hud/app/device/gps/GpsManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsManager;

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->extrapolationOn:Z

    return v0
.end method

.method static synthetic access$2402(Lcom/navdy/hud/app/device/gps/GpsManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsManager;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->extrapolationOn:Z

    return p1
.end method

.method static synthetic access$2500(Lcom/navdy/hud/app/device/gps/GpsManager;)J
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsManager;

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->extrapolationStartTime:J

    return-wide v0
.end method

.method static synthetic access$2502(Lcom/navdy/hud/app/device/gps/GpsManager;J)J
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsManager;
    .param p1, "x1"    # J

    .prologue
    .line 43
    iput-wide p1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->extrapolationStartTime:J

    return-wide p1
.end method

.method static synthetic access$2600(Lcom/navdy/hud/app/device/gps/GpsManager;)Lcom/navdy/hud/app/device/gps/GpsNmeaParser;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsManager;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->nmeaParser:Lcom/navdy/hud/app/device/gps/GpsNmeaParser;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/navdy/hud/app/device/gps/GpsManager;)J
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsManager;

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->gpsManagerStartTime:J

    return-wide v0
.end method

.method static synthetic access$2800(Lcom/navdy/hud/app/device/gps/GpsManager;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsManager;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->queue:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/device/gps/GpsManager;)Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsManager;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->activeLocationSource:Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    return-object v0
.end method

.method static synthetic access$302(Lcom/navdy/hud/app/device/gps/GpsManager;Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;)Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsManager;
    .param p1, "x1"    # Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->activeLocationSource:Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    return-object p1
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/device/gps/GpsManager;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsManager;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->noPhoneLocationFixRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/device/gps/GpsManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsManager;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/device/gps/GpsManager;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsManager;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->locationFixRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/device/gps/GpsManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsManager;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/navdy/hud/app/device/gps/GpsManager;->stopSendingLocation()V

    return-void
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/device/gps/GpsManager;)J
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsManager;

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->gpsLastEventTime:J

    return-wide v0
.end method

.method static synthetic access$802(Lcom/navdy/hud/app/device/gps/GpsManager;J)J
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsManager;
    .param p1, "x1"    # J

    .prologue
    .line 43
    iput-wide p1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->gpsLastEventTime:J

    return-wide p1
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/device/gps/GpsManager;)J
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsManager;

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->lastPhoneCoordinateTime:J

    return-wide v0
.end method

.method private static androidLocationFromCoordinate(Lcom/navdy/service/library/events/location/Coordinate;)Landroid/location/Location;
    .locals 4
    .param p0, "c"    # Lcom/navdy/service/library/events/location/Coordinate;

    .prologue
    .line 746
    new-instance v0, Landroid/location/Location;

    const-string v1, "network"

    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 747
    .local v0, "location":Landroid/location/Location;
    iget-object v1, p0, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLatitude(D)V

    .line 748
    iget-object v1, p0, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLongitude(D)V

    .line 749
    iget-object v1, p0, Lcom/navdy/service/library/events/location/Coordinate;->accuracy:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/location/Location;->setAccuracy(F)V

    .line 750
    iget-object v1, p0, Lcom/navdy/service/library/events/location/Coordinate;->altitude:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setAltitude(D)V

    .line 751
    iget-object v1, p0, Lcom/navdy/service/library/events/location/Coordinate;->bearing:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/location/Location;->setBearing(F)V

    .line 752
    iget-object v1, p0, Lcom/navdy/service/library/events/location/Coordinate;->speed:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/location/Location;->setSpeed(F)V

    .line 753
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setTime(J)V

    .line 754
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setElapsedRealtimeNanos(J)V

    .line 755
    return-object v0
.end method

.method private cancelUbloxResetRunnable()V
    .locals 2

    .prologue
    .line 1014
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->warmResetRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1015
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->warmResetCancelled:Z

    .line 1016
    return-void
.end method

.method private checkGpsToPhoneAccuracy(Landroid/location/Location;)V
    .locals 14
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    const/4 v13, 0x1

    const/high16 v12, 0x40000000    # 2.0f

    const/4 v11, 0x0

    const/4 v10, 0x3

    .line 928
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    .line 929
    .local v0, "accuracy":F
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 930
    .local v2, "now":J
    iget-wide v6, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->lastPhoneCoordinateTime:J

    sub-long v4, v2, v6

    .line 932
    .local v4, "time":J
    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v10}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 933
    sget-object v6, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[Gps-loc-navdy] checkGpsToPhoneAccuracy: ublox["

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, "] phone["

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->lastPhoneCoordinate:Lcom/navdy/service/library/events/location/Coordinate;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->lastPhoneCoordinate:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v1, v1, Lcom/navdy/service/library/events/location/Coordinate;->accuracy:Ljava/lang/Float;

    :goto_0
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, "] lastPhoneTime ["

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->lastPhoneCoordinate:Lcom/navdy/service/library/events/location/Coordinate;

    if-eqz v1, :cond_3

    .line 935
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    :goto_1
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, "]"

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 933
    invoke-virtual {v6, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 939
    :cond_0
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-nez v1, :cond_4

    .line 994
    :cond_1
    :goto_2
    return-void

    .line 933
    :cond_2
    const-string v1, "n/a"

    goto :goto_0

    .line 935
    :cond_3
    const-string v1, "n/a"

    goto :goto_1

    .line 944
    :cond_4
    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->activeLocationSource:Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    sget-object v6, Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;->UBLOX:Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    if-ne v1, v6, :cond_8

    .line 946
    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->lastPhoneCoordinate:Lcom/navdy/service/library/events/location/Coordinate;

    if-eqz v1, :cond_1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->lastPhoneCoordinateTime:J

    sub-long/2addr v6, v8

    const-wide/16 v8, 0xbb8

    cmp-long v1, v6, v8

    if-gtz v1, :cond_1

    .line 950
    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->lastPhoneCoordinate:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v1, v1, Lcom/navdy/service/library/events/location/Coordinate;->accuracy:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    add-float/2addr v1, v12

    cmpg-float v1, v1, v0

    if-gez v1, :cond_7

    .line 951
    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v10}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 952
    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[Gps-loc-navdy]  ublox accuracy ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] > phone accuracy["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->lastPhoneCoordinate:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v7, v7, Lcom/navdy/service/library/events/location/Coordinate;->accuracy:Ljava/lang/Float;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 955
    :cond_5
    iget-boolean v1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->startUbloxCalled:Z

    if-eqz v1, :cond_1

    .line 960
    const/high16 v1, 0x40a00000    # 5.0f

    cmpg-float v1, v0, v1

    if-gtz v1, :cond_6

    .line 961
    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ublox accuracy ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] > phone accuracy["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->lastPhoneCoordinate:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v7, v7, Lcom/navdy/service/library/events/location/Coordinate;->accuracy:Ljava/lang/Float;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "], but not above threshold"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 962
    invoke-direct {p0}, Lcom/navdy/hud/app/device/gps/GpsManager;->stopSendingLocation()V

    goto/16 :goto_2

    .line 966
    :cond_6
    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ublox accuracy ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] > phone accuracy["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->lastPhoneCoordinate:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v7, v7, Lcom/navdy/service/library/events/location/Coordinate;->accuracy:Ljava/lang/Float;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "], stop uBloxReporting"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 967
    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->handler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->locationFixRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 968
    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->handler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->noPhoneLocationFixRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 969
    invoke-virtual {p0}, Lcom/navdy/hud/app/device/gps/GpsManager;->stopUbloxReporting()V

    .line 970
    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[Gps-loc] [LocationProvider] switched from ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->activeLocationSource:Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] to ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;->PHONE:Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 971
    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;->PHONE:Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    iput-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->activeLocationSource:Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    .line 972
    const-string v1, "Switched to Phone Gps"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Phone ="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->lastPhoneCoordinate:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v7, v7, Lcom/navdy/service/library/events/location/Coordinate;->accuracy:Ljava/lang/Float;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ublox ="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v1, v6, v13, v11}, Lcom/navdy/hud/app/device/gps/GpsManager;->markLocationFix(Ljava/lang/String;Ljava/lang/String;ZZ)V

    goto/16 :goto_2

    .line 974
    :cond_7
    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v10}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 975
    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ublox accuracy ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] < phone accuracy["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->lastPhoneCoordinate:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v7, v7, Lcom/navdy/service/library/events/location/Coordinate;->accuracy:Ljava/lang/Float;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] no action"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 980
    :cond_8
    add-float v1, v0, v12

    iget-object v6, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->lastPhoneCoordinate:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v6, v6, Lcom/navdy/service/library/events/location/Coordinate;->accuracy:Ljava/lang/Float;

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    cmpg-float v1, v1, v6

    if-gez v1, :cond_9

    .line 982
    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ublox accuracy ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] < phone accuracy["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->lastPhoneCoordinate:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v7, v7, Lcom/navdy/service/library/events/location/Coordinate;->accuracy:Ljava/lang/Float;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "], start uBloxReporting, switch complete"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 984
    iput-boolean v13, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->firstSwitchToUbloxFromPhone:Z

    .line 985
    const-string v1, "Switched to Ublox Gps"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Phone ="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->lastPhoneCoordinate:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v7, v7, Lcom/navdy/service/library/events/location/Coordinate;->accuracy:Ljava/lang/Float;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ublox ="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v1, v6, v11, v11}, Lcom/navdy/hud/app/device/gps/GpsManager;->markLocationFix(Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 987
    invoke-virtual {p0}, Lcom/navdy/hud/app/device/gps/GpsManager;->startUbloxReporting()V

    goto/16 :goto_2

    .line 989
    :cond_9
    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v10}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 990
    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ublox accuracy ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] > phone accuracy["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->lastPhoneCoordinate:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v7, v7, Lcom/navdy/service/library/events/location/Coordinate;->accuracy:Ljava/lang/Float;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] no action"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method private feedLocationToProvider(Landroid/location/Location;Lcom/navdy/service/library/events/location/Coordinate;)V
    .locals 4
    .param p1, "androidLocation"    # Landroid/location/Location;
    .param p2, "coordinate"    # Lcom/navdy/service/library/events/location/Coordinate;

    .prologue
    .line 731
    :try_start_0
    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 732
    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[Phone-loc] "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 734
    :cond_0
    if-nez p1, :cond_1

    .line 735
    invoke-static {p2}, Lcom/navdy/hud/app/device/gps/GpsManager;->androidLocationFromCoordinate(Lcom/navdy/service/library/events/location/Coordinate;)Landroid/location/Location;

    move-result-object p1

    .line 737
    :cond_1
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/device/gps/GpsManager;->updateDrivingState(Landroid/location/Location;)V

    .line 738
    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->locationManager:Landroid/location/LocationManager;

    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Landroid/location/LocationManager;->setTestProviderLocation(Ljava/lang/String;Landroid/location/Location;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 742
    :goto_0
    return-void

    .line 739
    :catch_0
    move-exception v0

    .line 740
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "feedLocation"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static getInstance()Lcom/navdy/hud/app/device/gps/GpsManager;
    .locals 1

    .prologue
    .line 102
    sget-object v0, Lcom/navdy/hud/app/device/gps/GpsManager;->singleton:Lcom/navdy/hud/app/device/gps/GpsManager;

    return-object v0
.end method

.method private markLocationFix(Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 2
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "info"    # Ljava/lang/String;
    .param p3, "usingPhone"    # Z
    .param p4, "usingUblox"    # Z

    .prologue
    .line 1000
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1001
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "title"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1002
    const-string v1, "info"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1003
    const-string v1, "phone"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1004
    const-string v1, "ublox"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1005
    const-string v1, "GPS_Switch"

    invoke-direct {p0, v1, v0}, Lcom/navdy/hud/app/device/gps/GpsManager;->sendGpsEventBroadcast(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1006
    return-void
.end method

.method private processNmea(Ljava/lang/String;)V
    .locals 3
    .param p1, "nmea"    # Ljava/lang/String;

    .prologue
    .line 846
    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->nmeaHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 847
    .local v0, "msg":Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 848
    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->nmeaHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 849
    return-void
.end method

.method private sendGpsEventBroadcast(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 12
    .param p1, "eventName"    # Ljava/lang/String;
    .param p2, "extras"    # Landroid/os/Bundle;

    .prologue
    const-wide/16 v10, 0x3a98

    .line 853
    :try_start_0
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 854
    .local v2, "intent":Landroid/content/Intent;
    if-eqz p2, :cond_0

    .line 855
    invoke-virtual {v2, p2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 857
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->gpsManagerStartTime:J

    sub-long/2addr v6, v8

    cmp-long v5, v6, v10

    if-gtz v5, :cond_1

    .line 858
    iget-object v5, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->queue:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 859
    iget-object v5, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->handler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->queueCallback:Ljava/lang/Runnable;

    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 860
    iget-object v5, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->handler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->queueCallback:Ljava/lang/Runnable;

    const-wide/16 v8, 0x3a98

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 873
    .end local v2    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 863
    .restart local v2    # "intent":Landroid/content/Intent;
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 864
    .local v0, "context":Landroid/content/Context;
    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v1

    .line 865
    .local v1, "handle":Landroid/os/UserHandle;
    iget-object v5, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->queue:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 866
    .local v3, "len":I
    if-lez v3, :cond_2

    .line 867
    iget-object v5, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->queueCallback:Ljava/lang/Runnable;

    invoke-interface {v5}, Ljava/lang/Runnable;->run()V

    .line 869
    :cond_2
    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 870
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "handle":Landroid/os/UserHandle;
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v3    # "len":I
    :catch_0
    move-exception v4

    .line 871
    .local v4, "t":Ljava/lang/Throwable;
    sget-object v5, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v5, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private sendSpeechRequest(Lcom/navdy/service/library/events/audio/SpeechRequest;)V
    .locals 3
    .param p1, "speechRequest"    # Lcom/navdy/service/library/events/audio/SpeechRequest;

    .prologue
    .line 835
    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->connectionService:Lcom/navdy/hud/app/service/HudConnectionService;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->connectionService:Lcom/navdy/hud/app/service/HudConnectionService;

    invoke-virtual {v1}, Lcom/navdy/hud/app/service/HudConnectionService;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 837
    :try_start_0
    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "[Gps-loc] send speech request"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 838
    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->connectionService:Lcom/navdy/hud/app/service/HudConnectionService;

    invoke-virtual {v1, p1}, Lcom/navdy/hud/app/service/HudConnectionService;->sendMessage(Lcom/squareup/wire/Message;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 843
    :cond_0
    :goto_0
    return-void

    .line 839
    :catch_0
    move-exception v0

    .line 840
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private setDriving(Z)V
    .locals 2
    .param p1, "driving"    # Z

    .prologue
    .line 770
    iget-boolean v0, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->driving:Z

    if-eq v0, p1, :cond_0

    .line 771
    iput-boolean p1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->driving:Z

    .line 772
    if-eqz p1, :cond_1

    const-string v0, "driving_started"

    :goto_0
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/device/gps/GpsManager;->sendGpsEventBroadcast(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 775
    :cond_0
    return-void

    .line 772
    :cond_1
    const-string v0, "driving_stopped"

    goto :goto_0
.end method

.method private setUbloxAccuracyThreshold()V
    .locals 6

    .prologue
    .line 903
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 904
    .local v0, "context":Landroid/content/Context;
    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v1

    .line 905
    .local v1, "handle":Landroid/os/UserHandle;
    new-instance v2, Landroid/content/Intent;

    const-string v4, "SET_UBLOX_ACCURACY"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 906
    .local v2, "intent":Landroid/content/Intent;
    const-string v4, "UBLOX_ACCURACY"

    const/16 v5, 0x1e

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 907
    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 908
    sget-object v4, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "setUbloxAccuracyThreshold:30"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 912
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "handle":Landroid/os/UserHandle;
    .end local v2    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 909
    :catch_0
    move-exception v3

    .line 910
    .local v3, "t":Ljava/lang/Throwable;
    sget-object v4, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "setUbloxAccuracyThreshold"

    invoke-virtual {v4, v5, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private setUbloxResetRunnable()V
    .locals 4

    .prologue
    .line 1009
    sget-object v0, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "setUbloxResetRunnable"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1010
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->warmResetRunnable:Ljava/lang/Runnable;

    sget-wide v2, Lcom/navdy/hud/app/device/gps/GpsManager;->WARM_RESET_INTERVAL:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1011
    return-void
.end method

.method private setUbloxTimeDiscardThreshold()V
    .locals 6

    .prologue
    .line 916
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 917
    .local v0, "context":Landroid/content/Context;
    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v1

    .line 918
    .local v1, "handle":Landroid/os/UserHandle;
    new-instance v2, Landroid/content/Intent;

    const-string v4, "SET_LOCATION_DISCARD_TIME_THRESHOLD"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 919
    .local v2, "intent":Landroid/content/Intent;
    const-string v4, "DISCARD_TIME"

    const/16 v5, 0x7d0

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 920
    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 921
    sget-object v4, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "setUbloxTimeDiscardThreshold:2000"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 925
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "handle":Landroid/os/UserHandle;
    .end local v2    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 922
    :catch_0
    move-exception v3

    .line 923
    .local v3, "t":Ljava/lang/Throwable;
    sget-object v4, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "setUbloxTimeDiscardThreshold"

    invoke-virtual {v4, v5, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private startSendingLocation()V
    .locals 3

    .prologue
    .line 759
    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->connectionService:Lcom/navdy/hud/app/service/HudConnectionService;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->connectionService:Lcom/navdy/hud/app/service/HudConnectionService;

    invoke-virtual {v1}, Lcom/navdy/hud/app/service/HudConnectionService;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 761
    :try_start_0
    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "[Gps-loc] phone start sending location"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 762
    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->connectionService:Lcom/navdy/hud/app/service/HudConnectionService;

    sget-object v2, Lcom/navdy/hud/app/device/gps/GpsManager;->START_SENDING_LOCATION:Lcom/navdy/service/library/events/location/TransmitLocation;

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/service/HudConnectionService;->sendMessage(Lcom/squareup/wire/Message;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 767
    :cond_0
    :goto_0
    return-void

    .line 763
    :catch_0
    move-exception v0

    .line 764
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private stopSendingLocation()V
    .locals 3

    .prologue
    .line 816
    iget-boolean v1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->keepPhoneGpsOn:Z

    if-eqz v1, :cond_1

    .line 817
    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "stopSendingLocation: keeping phone gps on"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 832
    :cond_0
    :goto_0
    return-void

    .line 820
    :cond_1
    iget-boolean v1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->isDebugTTSEnabled:Z

    if-eqz v1, :cond_2

    .line 821
    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "stopSendingLocation: not stopping, debug_tts enabled"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 824
    :cond_2
    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->connectionService:Lcom/navdy/hud/app/service/HudConnectionService;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->connectionService:Lcom/navdy/hud/app/service/HudConnectionService;

    invoke-virtual {v1}, Lcom/navdy/hud/app/service/HudConnectionService;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 826
    :try_start_0
    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "[Gps-loc] phone stop sending location"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 827
    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->connectionService:Lcom/navdy/hud/app/service/HudConnectionService;

    sget-object v2, Lcom/navdy/hud/app/device/gps/GpsManager;->STOP_SENDING_LOCATION:Lcom/navdy/service/library/events/location/TransmitLocation;

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/service/HudConnectionService;->sendMessage(Lcom/squareup/wire/Message;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 828
    :catch_0
    move-exception v0

    .line 829
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private updateDrivingState(Landroid/location/Location;)V
    .locals 8
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    const-wide/16 v6, 0xbb8

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 778
    iget-boolean v0, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->driving:Z

    if-eqz v0, :cond_5

    .line 780
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v0

    float-to-double v0, v0

    const-wide v2, 0x3fcc9cbd821dc3a7L    # 0.22353333333333333

    cmpg-double v0, v0, v2

    if-gez v0, :cond_4

    .line 781
    :cond_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->transitioning:Z

    if-eqz v0, :cond_3

    .line 782
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->transitionStartTime:J

    sub-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-lez v0, :cond_1

    .line 783
    invoke-direct {p0, v4}, Lcom/navdy/hud/app/device/gps/GpsManager;->setDriving(Z)V

    .line 784
    iput-boolean v4, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->transitioning:Z

    .line 809
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->noLocationUpdatesRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 810
    iget-boolean v0, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->driving:Z

    if-eqz v0, :cond_2

    .line 811
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->noLocationUpdatesRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 813
    :cond_2
    return-void

    .line 787
    :cond_3
    iput-boolean v5, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->transitioning:Z

    .line 788
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->transitionStartTime:J

    goto :goto_0

    .line 791
    :cond_4
    iput-boolean v4, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->transitioning:Z

    goto :goto_0

    .line 795
    :cond_5
    if-eqz p1, :cond_7

    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v0

    float-to-double v0, v0

    const-wide v2, 0x4001e1f671529a49L    # 2.2353333333333336

    cmpl-double v0, v0, v2

    if-lez v0, :cond_7

    .line 796
    iget-boolean v0, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->transitioning:Z

    if-eqz v0, :cond_6

    .line 797
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->transitionStartTime:J

    sub-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-lez v0, :cond_1

    .line 798
    invoke-direct {p0, v5}, Lcom/navdy/hud/app/device/gps/GpsManager;->setDriving(Z)V

    .line 799
    iput-boolean v4, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->transitioning:Z

    goto :goto_0

    .line 802
    :cond_6
    iput-boolean v5, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->transitioning:Z

    .line 803
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->transitionStartTime:J

    goto :goto_0

    .line 806
    :cond_7
    iput-boolean v4, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->transitioning:Z

    goto :goto_0
.end method


# virtual methods
.method public feedLocation(Lcom/navdy/service/library/events/location/Coordinate;)V
    .locals 20
    .param p1, "coordinate"    # Lcom/navdy/service/library/events/location/Coordinate;

    .prologue
    .line 636
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->lastPhoneCoordinateTime:J

    const-wide/16 v16, 0x0

    cmp-long v9, v14, v16

    if-gtz v9, :cond_0

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->lastUbloxCoordinateTime:J

    const-wide/16 v16, 0x0

    cmp-long v9, v14, v16

    if-lez v9, :cond_3

    :cond_0
    const/4 v4, 0x1

    .line 638
    .local v4, "hasAtleastOneLocation":Z
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 642
    .local v6, "now":J
    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/navdy/service/library/events/location/Coordinate;->accuracy:Ljava/lang/Float;

    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    move-result v9

    const/high16 v14, 0x41f00000    # 30.0f

    cmpl-float v9, v9, v14

    if-ltz v9, :cond_4

    if-eqz v4, :cond_4

    .line 643
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->inaccurateGpsCount:J

    const-wide/16 v16, 0x1

    add-long v14, v14, v16

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->inaccurateGpsCount:J

    .line 644
    move-object/from16 v0, p0

    iget v9, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->gpsAccuracy:F

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/navdy/service/library/events/location/Coordinate;->accuracy:Ljava/lang/Float;

    invoke-virtual {v14}, Ljava/lang/Float;->floatValue()F

    move-result v14

    add-float/2addr v9, v14

    move-object/from16 v0, p0

    iput v9, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->gpsAccuracy:F

    .line 645
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->lastInaccurateGpsTime:J

    const-wide/16 v16, 0x0

    cmp-long v9, v14, v16

    if-eqz v9, :cond_1

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->lastInaccurateGpsTime:J

    sub-long v14, v6, v14

    sget-wide v16, Lcom/navdy/hud/app/device/gps/GpsManager;->INACCURATE_GPS_REPORT_INTERVAL:J

    cmp-long v9, v14, v16

    if-ltz v9, :cond_2

    .line 646
    :cond_1
    move-object/from16 v0, p0

    iget v9, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->gpsAccuracy:F

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->inaccurateGpsCount:J

    long-to-float v14, v14

    div-float v3, v9, v14

    .line 647
    .local v3, "avgAccuracy":F
    sget-object v9, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "BAD gps accuracy (discarding) avg:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", count="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->inaccurateGpsCount:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", last coord: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v9, v14}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 649
    const-wide/16 v14, 0x0

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->inaccurateGpsCount:J

    .line 650
    const/4 v9, 0x0

    move-object/from16 v0, p0

    iput v9, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->gpsAccuracy:F

    .line 651
    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->lastInaccurateGpsTime:J

    .line 727
    .end local v3    # "avgAccuracy":F
    :cond_2
    :goto_1
    return-void

    .line 636
    .end local v4    # "hasAtleastOneLocation":Z
    .end local v6    # "now":J
    :cond_3
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 656
    .restart local v4    # "hasAtleastOneLocation":Z
    .restart local v6    # "now":J
    :cond_4
    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/navdy/service/library/events/location/Coordinate;->timestamp:Ljava/lang/Long;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    sub-long v12, v6, v14

    .line 657
    .local v12, "timeDiff":J
    const-wide/16 v14, 0x7d0

    cmp-long v9, v12, v14

    if-ltz v9, :cond_5

    if-eqz v4, :cond_5

    .line 658
    sget-object v9, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "OLD location from phone(discard) time:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", timestamp:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/navdy/service/library/events/location/Coordinate;->timestamp:Ljava/lang/Long;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " now:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " lat:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " lng:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v9, v14}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_1

    .line 672
    :cond_5
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->lastPhoneCoordinate:Lcom/navdy/service/library/events/location/Coordinate;

    if-eqz v9, :cond_6

    .line 673
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->connectionService:Lcom/navdy/hud/app/service/HudConnectionService;

    invoke-virtual {v9}, Lcom/navdy/hud/app/service/HudConnectionService;->getDevicePlatform()Lcom/navdy/service/library/events/DeviceInfo$Platform;

    move-result-object v5

    .line 674
    .local v5, "platform":Lcom/navdy/service/library/events/DeviceInfo$Platform;
    sget-object v9, Lcom/navdy/service/library/events/DeviceInfo$Platform;->PLATFORM_iOS:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    if-ne v5, v9, :cond_6

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->lastPhoneCoordinate:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v9, v9, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    if-ne v9, v14, :cond_6

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->lastPhoneCoordinate:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v9, v9, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    if-ne v9, v14, :cond_6

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->lastPhoneCoordinate:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v9, v9, Lcom/navdy/service/library/events/location/Coordinate;->accuracy:Ljava/lang/Float;

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/navdy/service/library/events/location/Coordinate;->accuracy:Ljava/lang/Float;

    if-ne v9, v14, :cond_6

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->lastPhoneCoordinate:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v9, v9, Lcom/navdy/service/library/events/location/Coordinate;->altitude:Ljava/lang/Double;

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/navdy/service/library/events/location/Coordinate;->altitude:Ljava/lang/Double;

    if-ne v9, v14, :cond_6

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->lastPhoneCoordinate:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v9, v9, Lcom/navdy/service/library/events/location/Coordinate;->bearing:Ljava/lang/Float;

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/navdy/service/library/events/location/Coordinate;->bearing:Ljava/lang/Float;

    if-ne v9, v14, :cond_6

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->lastPhoneCoordinate:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v9, v9, Lcom/navdy/service/library/events/location/Coordinate;->speed:Ljava/lang/Float;

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/navdy/service/library/events/location/Coordinate;->speed:Ljava/lang/Float;

    if-ne v9, v14, :cond_6

    .line 681
    sget-object v9, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "same location(discard) diff="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/navdy/service/library/events/location/Coordinate;->timestamp:Ljava/lang/Long;

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->lastPhoneCoordinate:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v15, v15, Lcom/navdy/service/library/events/location/Coordinate;->timestamp:Ljava/lang/Long;

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    sub-long v16, v16, v18

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v9, v14}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 686
    .end local v5    # "platform":Lcom/navdy/service/library/events/DeviceInfo$Platform;
    :cond_6
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/device/gps/GpsManager;->lastPhoneCoordinate:Lcom/navdy/service/library/events/location/Coordinate;

    .line 687
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->lastPhoneCoordinateTime:J

    .line 688
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->handler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->noPhoneLocationFixRunnable:Ljava/lang/Runnable;

    invoke-virtual {v9, v14}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 690
    const/4 v2, 0x0

    .line 692
    .local v2, "androidLocation":Landroid/location/Location;
    invoke-static {}, Lcom/navdy/hud/app/debug/RouteRecorder;->getInstance()Lcom/navdy/hud/app/debug/RouteRecorder;

    move-result-object v8

    .line 693
    .local v8, "routeRecorder":Lcom/navdy/hud/app/debug/RouteRecorder;
    invoke-virtual {v8}, Lcom/navdy/hud/app/debug/RouteRecorder;->isRecording()Z

    move-result v9

    if-eqz v9, :cond_7

    .line 694
    invoke-static/range {p1 .. p1}, Lcom/navdy/hud/app/device/gps/GpsManager;->androidLocationFromCoordinate(Lcom/navdy/service/library/events/location/Coordinate;)Landroid/location/Location;

    move-result-object v2

    .line 695
    const/4 v9, 0x1

    invoke-virtual {v8, v2, v9}, Lcom/navdy/hud/app/debug/RouteRecorder;->injectLocation(Landroid/location/Location;Z)V

    .line 699
    :cond_7
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->activeLocationSource:Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    if-nez v9, :cond_8

    .line 700
    sget-object v9, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "[Gps-loc] [LocationProvider] switched from ["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->activeLocationSource:Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "] to ["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    sget-object v15, Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;->PHONE:Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "]"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v9, v14}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 701
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/device/gps/GpsManager;->stopUbloxReporting()V

    .line 702
    sget-object v9, Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;->PHONE:Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->activeLocationSource:Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    .line 703
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v2, v1}, Lcom/navdy/hud/app/device/gps/GpsManager;->feedLocationToProvider(Landroid/location/Location;Lcom/navdy/service/library/events/location/Coordinate;)V

    .line 704
    const-string v9, "Switched to Phone Gps"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Phone ="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->lastPhoneCoordinate:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v15, v15, Lcom/navdy/service/library/events/location/Coordinate;->accuracy:Ljava/lang/Float;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " ublox=n/a"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x1

    const/16 v16, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v9, v14, v15, v1}, Lcom/navdy/hud/app/device/gps/GpsManager;->markLocationFix(Ljava/lang/String;Ljava/lang/String;ZZ)V

    goto/16 :goto_1

    .line 709
    :cond_8
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->activeLocationSource:Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    sget-object v14, Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;->PHONE:Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    if-ne v9, v14, :cond_9

    .line 710
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v2, v1}, Lcom/navdy/hud/app/device/gps/GpsManager;->feedLocationToProvider(Landroid/location/Location;Lcom/navdy/service/library/events/location/Coordinate;)V

    goto/16 :goto_1

    .line 715
    :cond_9
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->lastUbloxCoordinateTime:J

    move-wide/from16 v16, v0

    sub-long v10, v14, v16

    .line 716
    .local v10, "time":J
    const-wide/16 v14, 0xbb8

    cmp-long v9, v10, v14

    if-lez v9, :cond_2

    .line 718
    sget-object v9, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "[Gps-loc] [LocationProvider] switched from ["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->activeLocationSource:Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "] to ["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    sget-object v15, Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;->PHONE:Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "]"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v9, v14}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 719
    sget-object v9, Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;->PHONE:Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->activeLocationSource:Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    .line 720
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/device/gps/GpsManager;->stopUbloxReporting()V

    .line 721
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v2, v1}, Lcom/navdy/hud/app/device/gps/GpsManager;->feedLocationToProvider(Landroid/location/Location;Lcom/navdy/service/library/events/location/Coordinate;)V

    .line 722
    const-string v9, "Switched to Phone Gps"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Phone ="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/hud/app/device/gps/GpsManager;->lastPhoneCoordinate:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v15, v15, Lcom/navdy/service/library/events/location/Coordinate;->accuracy:Ljava/lang/Float;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " ublox =lost"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x1

    const/16 v16, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v9, v14, v15, v1}, Lcom/navdy/hud/app/device/gps/GpsManager;->markLocationFix(Ljava/lang/String;Ljava/lang/String;ZZ)V

    goto/16 :goto_1
.end method

.method public setConnectionService(Lcom/navdy/hud/app/service/HudConnectionService;)V
    .locals 0
    .param p1, "connectionService"    # Lcom/navdy/hud/app/service/HudConnectionService;

    .prologue
    .line 630
    iput-object p1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->connectionService:Lcom/navdy/hud/app/service/HudConnectionService;

    .line 631
    return-void
.end method

.method public shutdown()V
    .locals 2

    .prologue
    .line 1019
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->locationManager:Landroid/location/LocationManager;

    if-eqz v0, :cond_0

    .line 1020
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->locationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->ubloxGpsLocationListener:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 1021
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->locationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->navdyGpsLocationListener:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 1022
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->locationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->ubloxGpsStatusListener:Landroid/location/GpsStatus$Listener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeGpsStatusListener(Landroid/location/GpsStatus$Listener;)V

    .line 1023
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->locationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->ubloxGpsNmeaListener:Landroid/location/GpsStatus$NmeaListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeNmeaListener(Landroid/location/GpsStatus$NmeaListener;)V

    .line 1025
    :cond_0
    return-void
.end method

.method public startUbloxReporting()V
    .locals 6

    .prologue
    .line 877
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 878
    .local v0, "context":Landroid/content/Context;
    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v1

    .line 879
    .local v1, "handle":Landroid/os/UserHandle;
    new-instance v2, Landroid/content/Intent;

    const-string v4, "START_REPORTING_UBLOX_LOCATION"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 880
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 881
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->startUbloxCalled:Z

    .line 882
    sget-object v4, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "started ublox reporting"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 886
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "handle":Landroid/os/UserHandle;
    .end local v2    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 883
    :catch_0
    move-exception v3

    .line 884
    .local v3, "t":Ljava/lang/Throwable;
    sget-object v4, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "startUbloxReporting"

    invoke-virtual {v4, v5, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public stopUbloxReporting()V
    .locals 6

    .prologue
    .line 890
    const/4 v4, 0x0

    :try_start_0
    iput-boolean v4, p0, Lcom/navdy/hud/app/device/gps/GpsManager;->startUbloxCalled:Z

    .line 891
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 892
    .local v0, "context":Landroid/content/Context;
    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v1

    .line 893
    .local v1, "handle":Landroid/os/UserHandle;
    new-instance v2, Landroid/content/Intent;

    const-string v4, "STOP_REPORTING_UBLOX_LOCATION"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 894
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 895
    sget-object v4, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "stopped ublox reporting"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 899
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "handle":Landroid/os/UserHandle;
    .end local v2    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 896
    :catch_0
    move-exception v3

    .line 897
    .local v3, "t":Ljava/lang/Throwable;
    sget-object v4, Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "stopUbloxReporting"

    invoke-virtual {v4, v5, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
