.class Lcom/navdy/hud/app/device/gps/GpsManager$3;
.super Ljava/lang/Object;
.source "GpsManager.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/device/gps/GpsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private accuracyMax:F

.field private accuracyMin:F

.field private accuracySampleCount:I

.field private accuracySampleStartTime:J

.field private accuracySum:F

.field final synthetic this$0:Lcom/navdy/hud/app/device/gps/GpsManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/gps/GpsManager;)V
    .locals 3
    .param p1, "this$0"    # Lcom/navdy/hud/app/device/gps/GpsManager;

    .prologue
    const/4 v2, 0x0

    .line 244
    iput-object p1, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 245
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->accuracySampleStartTime:J

    .line 246
    iput v2, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->accuracySum:F

    .line 247
    iput v2, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->accuracyMin:F

    .line 248
    iput v2, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->accuracyMax:F

    .line 249
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->accuracySampleCount:I

    return-void
.end method

.method private collectAccuracyStats(F)V
    .locals 9
    .param p1, "accuracy"    # F

    .prologue
    const/4 v8, 0x0

    .line 252
    cmpl-float v3, p1, v8

    if-eqz v3, :cond_2

    .line 253
    iget v3, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->accuracySum:F

    add-float/2addr v3, p1

    iput v3, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->accuracySum:F

    .line 254
    iget v3, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->accuracySampleCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->accuracySampleCount:I

    .line 255
    iget v3, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->accuracyMin:F

    cmpl-float v3, v3, v8

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->accuracyMin:F

    cmpg-float v3, p1, v3

    if-gez v3, :cond_1

    .line 256
    :cond_0
    iput p1, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->accuracyMin:F

    .line 258
    :cond_1
    iget v3, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->accuracyMax:F

    cmpg-float v3, v3, p1

    if-gez v3, :cond_2

    .line 259
    iput p1, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->accuracyMax:F

    .line 262
    :cond_2
    iget-object v3, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->lastUbloxCoordinateTime:J
    invoke-static {v3}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$1300(Lcom/navdy/hud/app/device/gps/GpsManager;)J

    move-result-wide v4

    iget-wide v6, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->accuracySampleStartTime:J

    sub-long/2addr v4, v6

    const-wide/32 v6, 0x493e0

    cmp-long v3, v4, v6

    if-lez v3, :cond_4

    .line 263
    iget v3, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->accuracySampleCount:I

    if-lez v3, :cond_3

    .line 264
    iget v3, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->accuracyMin:F

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 265
    .local v2, "minimum":Ljava/lang/String;
    iget v3, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->accuracyMax:F

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 266
    .local v1, "maximum":Ljava/lang/String;
    iget v3, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->accuracySum:F

    iget v4, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->accuracySampleCount:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 267
    .local v0, "average":Ljava/lang/String;
    invoke-static {v2, v1, v0}, Lcom/navdy/hud/app/service/ConnectionServiceAnalyticsSupport;->recordGpsAccuracy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    iput v8, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->accuracySum:F

    iput v8, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->accuracyMax:F

    iput v8, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->accuracyMin:F

    .line 269
    const/4 v3, 0x0

    iput v3, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->accuracySampleCount:I

    .line 271
    .end local v0    # "average":Ljava/lang/String;
    .end local v1    # "maximum":Ljava/lang/String;
    .end local v2    # "minimum":Ljava/lang/String;
    :cond_3
    iget-object v3, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->lastUbloxCoordinateTime:J
    invoke-static {v3}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$1300(Lcom/navdy/hud/app/device/gps/GpsManager;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->accuracySampleStartTime:J

    .line 273
    :cond_4
    return-void
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .locals 8
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 278
    :try_start_0
    iget-object v4, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->warmResetCancelled:Z
    invoke-static {v4}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$1400(Lcom/navdy/hud/app/device/gps/GpsManager;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 279
    iget-object v4, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # invokes: Lcom/navdy/hud/app/device/gps/GpsManager;->cancelUbloxResetRunnable()V
    invoke-static {v4}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$1500(Lcom/navdy/hud/app/device/gps/GpsManager;)V

    .line 281
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/debug/RouteRecorder;->getInstance()Lcom/navdy/hud/app/debug/RouteRecorder;

    move-result-object v0

    .line 282
    .local v0, "routeRecorder":Lcom/navdy/hud/app/debug/RouteRecorder;
    invoke-virtual {v0}, Lcom/navdy/hud/app/debug/RouteRecorder;->isPlaying()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 319
    .end local v0    # "routeRecorder":Lcom/navdy/hud/app/debug/RouteRecorder;
    :cond_1
    :goto_0
    return-void

    .line 285
    .restart local v0    # "routeRecorder":Lcom/navdy/hud/app/debug/RouteRecorder;
    :cond_2
    iget-object v4, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # invokes: Lcom/navdy/hud/app/device/gps/GpsManager;->updateDrivingState(Landroid/location/Location;)V
    invoke-static {v4, p1}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$1600(Lcom/navdy/hud/app/device/gps/GpsManager;Landroid/location/Location;)V

    .line 286
    iget-object v4, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # setter for: Lcom/navdy/hud/app/device/gps/GpsManager;->lastUbloxCoordinate:Landroid/location/Location;
    invoke-static {v4, p1}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$1102(Lcom/navdy/hud/app/device/gps/GpsManager;Landroid/location/Location;)Landroid/location/Location;

    .line 287
    iget-object v4, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    # setter for: Lcom/navdy/hud/app/device/gps/GpsManager;->lastUbloxCoordinateTime:J
    invoke-static {v4, v6, v7}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$1302(Lcom/navdy/hud/app/device/gps/GpsManager;J)J

    .line 288
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v4

    invoke-direct {p0, v4}, Lcom/navdy/hud/app/device/gps/GpsManager$3;->collectAccuracyStats(F)V

    .line 290
    iget-object v4, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->connectionService:Lcom/navdy/hud/app/service/HudConnectionService;
    invoke-static {v4}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$1700(Lcom/navdy/hud/app/device/gps/GpsManager;)Lcom/navdy/hud/app/service/HudConnectionService;

    move-result-object v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->connectionService:Lcom/navdy/hud/app/service/HudConnectionService;
    invoke-static {v4}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$1700(Lcom/navdy/hud/app/device/gps/GpsManager;)Lcom/navdy/hud/app/service/HudConnectionService;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/service/HudConnectionService;->isConnected()Z

    move-result v4

    if-nez v4, :cond_4

    .line 291
    :cond_3
    iget-object v4, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->activeLocationSource:Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;
    invoke-static {v4}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$300(Lcom/navdy/hud/app/device/gps/GpsManager;)Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    move-result-object v4

    sget-object v5, Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;->UBLOX:Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    if-eq v4, v5, :cond_1

    .line 292
    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "not connected with phone, start uBloxReporting"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 293
    iget-object v4, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    invoke-virtual {v4}, Lcom/navdy/hud/app/device/gps/GpsManager;->startUbloxReporting()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 316
    .end local v0    # "routeRecorder":Lcom/navdy/hud/app/debug/RouteRecorder;
    :catch_0
    move-exception v1

    .line 317
    .local v1, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 299
    .end local v1    # "t":Ljava/lang/Throwable;
    .restart local v0    # "routeRecorder":Lcom/navdy/hud/app/debug/RouteRecorder;
    :cond_4
    :try_start_1
    iget-object v4, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->activeLocationSource:Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;
    invoke-static {v4}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$300(Lcom/navdy/hud/app/device/gps/GpsManager;)Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    move-result-object v4

    sget-object v5, Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;->UBLOX:Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    if-eq v4, v5, :cond_5

    .line 300
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-object v6, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->lastPhoneCoordinateTime:J
    invoke-static {v6}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$900(Lcom/navdy/hud/app/device/gps/GpsManager;)J

    move-result-wide v6

    sub-long v2, v4, v6

    .line 301
    .local v2, "time":J
    const-wide/16 v4, 0xbb8

    cmp-long v4, v2, v4

    if-lez v4, :cond_5

    .line 303
    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "phone threshold exceeded= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", start uBloxReporting"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 304
    iget-object v4, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    invoke-virtual {v4}, Lcom/navdy/hud/app/device/gps/GpsManager;->startUbloxReporting()V

    goto/16 :goto_0

    .line 309
    .end local v2    # "time":J
    :cond_5
    iget-object v4, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->firstSwitchToUbloxFromPhone:Z
    invoke-static {v4}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$1800(Lcom/navdy/hud/app/device/gps/GpsManager;)Z

    move-result v4

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->switchBetweenPhoneUbloxAllowed:Z
    invoke-static {v4}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$1900(Lcom/navdy/hud/app/device/gps/GpsManager;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 315
    :cond_6
    iget-object v4, p0, Lcom/navdy/hud/app/device/gps/GpsManager$3;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # invokes: Lcom/navdy/hud/app/device/gps/GpsManager;->checkGpsToPhoneAccuracy(Landroid/location/Location;)V
    invoke-static {v4, p1}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$2000(Lcom/navdy/hud/app/device/gps/GpsManager;Landroid/location/Location;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 328
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 325
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;
    .param p2, "status"    # I
    .param p3, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 322
    return-void
.end method
