.class Lcom/navdy/hud/app/device/gps/GpsManager$5;
.super Ljava/lang/Object;
.source "GpsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/device/gps/GpsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/device/gps/GpsManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/gps/GpsManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/device/gps/GpsManager;

    .prologue
    .line 356
    iput-object p1, p0, Lcom/navdy/hud/app/device/gps/GpsManager$5;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x1388

    .line 359
    const/4 v0, 0x1

    .line 361
    .local v0, "resetRunnable":Z
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/device/gps/GpsManager$5;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->activeLocationSource:Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;
    invoke-static {v2}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$300(Lcom/navdy/hud/app/device/gps/GpsManager;)Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    move-result-object v2

    sget-object v3, Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;->UBLOX:Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    if-ne v2, v3, :cond_1

    .line 362
    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "[Gps-loc] noPhoneLocationFixRunnable: has location fix now"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 363
    const/4 v0, 0x0

    .line 372
    :goto_0
    if-eqz v0, :cond_0

    .line 373
    iget-object v2, p0, Lcom/navdy/hud/app/device/gps/GpsManager$5;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$500(Lcom/navdy/hud/app/device/gps/GpsManager;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/device/gps/GpsManager$5;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->noPhoneLocationFixRunnable:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$400(Lcom/navdy/hud/app/device/gps/GpsManager;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 376
    :cond_0
    :goto_1
    return-void

    .line 365
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/navdy/hud/app/device/gps/GpsManager$5;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    const/4 v3, 0x0

    # invokes: Lcom/navdy/hud/app/device/gps/GpsManager;->updateDrivingState(Landroid/location/Location;)V
    invoke-static {v2, v3}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$1600(Lcom/navdy/hud/app/device/gps/GpsManager;Landroid/location/Location;)V

    .line 367
    iget-object v2, p0, Lcom/navdy/hud/app/device/gps/GpsManager$5;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # invokes: Lcom/navdy/hud/app/device/gps/GpsManager;->startSendingLocation()V
    invoke-static {v2}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$2100(Lcom/navdy/hud/app/device/gps/GpsManager;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 369
    :catch_0
    move-exception v1

    .line 370
    .local v1, "t":Ljava/lang/Throwable;
    :try_start_2
    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 372
    if-eqz v0, :cond_0

    .line 373
    iget-object v2, p0, Lcom/navdy/hud/app/device/gps/GpsManager$5;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$500(Lcom/navdy/hud/app/device/gps/GpsManager;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/device/gps/GpsManager$5;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->noPhoneLocationFixRunnable:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$400(Lcom/navdy/hud/app/device/gps/GpsManager;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 372
    .end local v1    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_2

    .line 373
    iget-object v3, p0, Lcom/navdy/hud/app/device/gps/GpsManager$5;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->handler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$500(Lcom/navdy/hud/app/device/gps/GpsManager;)Landroid/os/Handler;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/device/gps/GpsManager$5;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->noPhoneLocationFixRunnable:Ljava/lang/Runnable;
    invoke-static {v4}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$400(Lcom/navdy/hud/app/device/gps/GpsManager;)Ljava/lang/Runnable;

    move-result-object v4

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_2
    throw v2
.end method
