.class final enum Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;
.super Ljava/lang/Enum;
.source "GpsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/device/gps/GpsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "LocationSource"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

.field public static final enum PHONE:Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

.field public static final enum UBLOX:Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 47
    new-instance v0, Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    const-string v1, "UBLOX"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;->UBLOX:Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    .line 48
    new-instance v0, Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    const-string v1, "PHONE"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;->PHONE:Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    .line 46
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;->UBLOX:Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;->PHONE:Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;->$VALUES:[Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 46
    const-class v0, Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;->$VALUES:[Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    return-object v0
.end method
