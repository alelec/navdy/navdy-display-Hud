.class public Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;
.super Ljava/lang/Object;
.source "GpsDeadReckoningManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$CommandWriter;,
        Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$Alignment;
    }
.end annotation


# static fields
.field private static final ALIGNMENT_DONE:I = 0x3

.field private static final ALIGNMENT_PACKET_LENGTH:I = 0x18

.field private static final ALIGNMENT_PACKET_PAYLOAD_LENGTH:I = 0x10

.field private static final ALIGNMENT_PATTERN:[B

.field private static final ALIGNMENT_POLL_FREQUENCY:J = 0x7530L

.field private static final ALIGNMENT_TIME:Ljava/lang/String; = "alignment_time"

.field private static final AUTO_ALIGNMENT:[B

.field private static final CFG_ESF_RAW:[B

.field private static final CFG_ESF_RAW_OFF:[B

.field private static final CFG_RST_COLD:[B

.field private static final CFG_RST_WARM:[B

.field private static final DEAD_RECKONING_ONLY:B = 0x1t

.field private static final ESF_PACKET_LENGTH:I = 0x13

.field private static final ESF_RAW_PATTERN:[B

.field private static final ESF_STATUS_PATTERN:[B

.field private static final FAILURE_RETRY_INTERVAL:I = 0x2710

.field private static final FIX_2D:B = 0x2t

.field private static final FIX_3D:B = 0x3t

.field private static final FUSION_DONE:I = 0x1

.field private static final GET_ALIGNMENT:[B

.field private static final GET_ESF_STATUS:[B

.field private static final GPS:Ljava/lang/String; = "gps"

.field private static final GPS_DEAD_RECKONING_COMBINED:B = 0x4t

.field private static final GPS_LOG:Ljava/lang/String; = "gps.log"

.field private static final HEX:[C

.field private static final INJECT_OBD_SPEED:[B

.field private static final MSG_ALIGNMENT_VALUE:I = 0x1

.field private static final NAV_STATUS_PATTERN:[B

.field private static final NO_FIX:B = 0x0t

.field private static final PASSPHRASE:[B

.field private static final PITCH:Ljava/lang/String; = "pitch"

.field private static final POLL_FREQUENCY:J = 0x2710L

.field private static final READ_BUF:[B

.field private static final ROLL:Ljava/lang/String; = "roll"

.field private static final SPEED_INJECTION_FREQUENCY:I = 0x64

.field private static final SPEED_TIME_TAG_COUNTER:I = 0x64

.field private static final TCP_SERVER_HOST:Ljava/lang/String; = "127.0.0.1"

.field private static final TCP_SERVER_PORT:I = 0xa5c2

.field private static final TEMP_BUF:[B

.field private static final THREAD_COUNTER:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static final TIME_ONLY:B = 0x5t

.field private static final YAW:Ljava/lang/String; = "yaw"

.field private static final sInstance:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private volatile alignmentChecked:Z

.field private alignmentInfo:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$Alignment;

.field private final bus:Lcom/squareup/otto/Bus;

.field private volatile deadReckoningInjectionStarted:Z

.field private deadReckoningOn:Z

.field private deadReckoningType:B

.field private enableEsfRunnable:Ljava/lang/Runnable;

.field private final getAlignmentRunnable:Ljava/lang/Runnable;

.field private final getAlignmentRunnableRetry:Ljava/lang/Runnable;

.field private final getFusionStatusRunnable:Ljava/lang/Runnable;

.field private final getFusionStatusRunnableRetry:Ljava/lang/Runnable;

.field private handler:Landroid/os/Handler;

.field private handlerThread:Landroid/os/HandlerThread;

.field private final injectRunnable:Ljava/lang/Runnable;

.field private final injectSpeed:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$CommandWriter;

.field private inputStream:Ljava/io/InputStream;

.field private lastConnectionFailure:J

.field private outputStream:Ljava/io/OutputStream;

.field private resetRunnable:Ljava/lang/Runnable;

.field private rootInfo:Lorg/json/JSONObject;

.field private volatile runThread:Z

.field private sensorDataProcessor:Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;

.field private socket:Ljava/net/Socket;

.field private final speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

.field private thread:Ljava/lang/Thread;

.field private timeStampCounter:I

.field private waitForAutoAlignment:Z

.field private waitForFusionStatus:Z


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/16 v6, 0x14

    const/16 v5, 0xc

    const/16 v4, 0xb

    const/16 v3, 0x8

    const/4 v2, 0x4

    .line 48
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 50
    new-instance v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    invoke-direct {v0}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sInstance:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    .line 97
    const/16 v0, 0x10

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->HEX:[C

    .line 110
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->THREAD_COUNTER:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 121
    new-array v0, v6, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->INJECT_OBD_SPEED:[B

    .line 132
    new-array v0, v5, [B

    fill-array-data v0, :array_2

    sput-object v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->CFG_RST_WARM:[B

    .line 136
    new-array v0, v4, [B

    fill-array-data v0, :array_3

    sput-object v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->CFG_ESF_RAW:[B

    .line 140
    new-array v0, v4, [B

    fill-array-data v0, :array_4

    sput-object v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->CFG_ESF_RAW_OFF:[B

    .line 145
    sget-object v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->CFG_ESF_RAW:[B

    invoke-static {v0}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->calculateChecksum([B)V

    .line 146
    sget-object v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->CFG_ESF_RAW_OFF:[B

    invoke-static {v0}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->calculateChecksum([B)V

    .line 150
    new-array v0, v5, [B

    fill-array-data v0, :array_5

    sput-object v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->CFG_RST_COLD:[B

    .line 156
    new-array v0, v2, [B

    fill-array-data v0, :array_6

    sput-object v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->NAV_STATUS_PATTERN:[B

    .line 160
    new-array v0, v2, [B

    fill-array-data v0, :array_7

    sput-object v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->ESF_RAW_PATTERN:[B

    .line 165
    new-array v0, v2, [B

    fill-array-data v0, :array_8

    sput-object v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->ALIGNMENT_PATTERN:[B

    .line 170
    new-array v0, v3, [B

    fill-array-data v0, :array_9

    sput-object v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->GET_ALIGNMENT:[B

    .line 175
    new-array v0, v6, [B

    fill-array-data v0, :array_a

    sput-object v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->AUTO_ALIGNMENT:[B

    .line 191
    new-array v0, v3, [B

    fill-array-data v0, :array_b

    sput-object v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->GET_ESF_STATUS:[B

    .line 196
    new-array v0, v2, [B

    fill-array-data v0, :array_c

    sput-object v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->ESF_STATUS_PATTERN:[B

    .line 203
    const/16 v0, 0x80

    new-array v0, v0, [B

    sput-object v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->TEMP_BUF:[B

    .line 208
    const/16 v0, 0x2000

    new-array v0, v0, [B

    sput-object v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->READ_BUF:[B

    .line 211
    const/4 v0, 0x5

    new-array v0, v0, [B

    fill-array-data v0, :array_d

    sput-object v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->PASSPHRASE:[B

    return-void

    .line 97
    nop

    :array_0
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x41s
        0x42s
        0x43s
        0x44s
        0x45s
        0x46s
    .end array-data

    .line 121
    :array_1
    .array-data 1
        -0x4bt
        0x62t
        0x10t
        0x2t
        0xct
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0xbt
        0x0t
        0x0t
    .end array-data

    .line 132
    :array_2
    .array-data 1
        -0x4bt
        0x62t
        0x6t
        0x4t
        0x4t
        0x0t
        0x1t
        0x0t
        0x2t
        0x0t
        0x11t
        0x6ct
    .end array-data

    .line 136
    :array_3
    .array-data 1
        -0x4bt
        0x62t
        0x6t
        0x1t
        0x3t
        0x0t
        0x10t
        0x3t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 140
    :array_4
    .array-data 1
        -0x4bt
        0x62t
        0x6t
        0x1t
        0x3t
        0x0t
        0x10t
        0x3t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 150
    :array_5
    .array-data 1
        -0x4bt
        0x62t
        0x6t
        0x4t
        0x4t
        0x0t
        -0x1t
        -0x1t
        0x2t
        0x0t
        0xet
        0x61t
    .end array-data

    .line 156
    :array_6
    .array-data 1
        -0x4bt
        0x62t
        0x1t
        0x3t
    .end array-data

    .line 160
    :array_7
    .array-data 1
        -0x4bt
        0x62t
        0x10t
        0x3t
    .end array-data

    .line 165
    :array_8
    .array-data 1
        -0x4bt
        0x62t
        0x10t
        0x14t
    .end array-data

    .line 170
    :array_9
    .array-data 1
        -0x4bt
        0x62t
        0x10t
        0x14t
        0x0t
        0x0t
        0x24t
        0x7ct
    .end array-data

    .line 175
    :array_a
    .array-data 1
        -0x4bt
        0x62t
        0x6t
        0x56t
        0xct
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x69t
        0x1dt
    .end array-data

    .line 191
    :array_b
    .array-data 1
        -0x4bt
        0x62t
        0x10t
        0x10t
        0x0t
        0x0t
        0x20t
        0x70t
    .end array-data

    .line 196
    :array_c
    .array-data 1
        -0x4bt
        0x62t
        0x10t
        0x10t
    .end array-data

    .line 211
    :array_d
    .array-data 1
        0x6et
        0x61t
        0x76t
        0x64t
        0x79t
    .end array-data
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    .line 305
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 214
    invoke-static {}, Lcom/navdy/hud/app/manager/SpeedManager;->getInstance()Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    .line 229
    const/4 v1, 0x0

    iput v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->timeStampCounter:I

    .line 232
    const/4 v1, -0x1

    iput-byte v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->deadReckoningType:B

    .line 244
    new-instance v1, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$1;-><init>(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)V

    iput-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->injectSpeed:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$CommandWriter;

    .line 251
    new-instance v1, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$2;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$2;-><init>(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)V

    iput-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->injectRunnable:Ljava/lang/Runnable;

    .line 264
    new-instance v1, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$3;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$3;-><init>(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)V

    iput-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->getFusionStatusRunnable:Ljava/lang/Runnable;

    .line 276
    new-instance v1, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$4;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$4;-><init>(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)V

    iput-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->getFusionStatusRunnableRetry:Ljava/lang/Runnable;

    .line 284
    new-instance v1, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$5;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$5;-><init>(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)V

    iput-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->getAlignmentRunnable:Ljava/lang/Runnable;

    .line 297
    new-instance v1, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$6;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$6;-><init>(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)V

    iput-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->getAlignmentRunnableRetry:Ljava/lang/Runnable;

    .line 647
    new-instance v1, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$9;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$9;-><init>(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)V

    iput-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->enableEsfRunnable:Ljava/lang/Runnable;

    .line 657
    new-instance v1, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$10;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$10;-><init>(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)V

    iput-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->resetRunnable:Ljava/lang/Runnable;

    .line 306
    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "ctor()"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 307
    new-instance v1, Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;

    invoke-direct {v1}, Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sensorDataProcessor:Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;

    .line 308
    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "gpsDeadReckoningHandler"

    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->handlerThread:Landroid/os/HandlerThread;

    .line 309
    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->handlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 310
    new-instance v1, Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->handlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    new-instance v3, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$7;

    invoke-direct {v3, p0}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$7;-><init>(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)V

    invoke-direct {v1, v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->handler:Landroid/os/Handler;

    .line 332
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v0

    .line 333
    .local v0, "obdManager":Lcom/navdy/hud/app/obd/ObdManager;
    invoke-virtual {v0}, Lcom/navdy/hud/app/obd/ObdManager;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 334
    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "ctor() obd is connected"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 335
    invoke-virtual {v0}, Lcom/navdy/hud/app/obd/ObdManager;->isSpeedPidAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 336
    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "ctor() speed pid is available"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 337
    invoke-direct {p0}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->checkAlignment()V

    .line 338
    invoke-direct {p0}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->startDeadReckoning()V

    .line 345
    :goto_0
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->bus:Lcom/squareup/otto/Bus;

    .line 346
    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v1, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 347
    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "ctor() initialized"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 349
    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->enableEsfRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 350
    return-void

    .line 340
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "ctor() speed pid is not available"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 343
    :cond_1
    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "ctor() obd is not connected, waiting for obd to connect"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sendObdSpeed()V

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$CommandWriter;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->injectSpeed:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$CommandWriter;

    return-object v0
.end method

.method static synthetic access$1000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->getFusionStatusRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1200()[B
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->GET_ALIGNMENT:[B

    return-object v0
.end method

.method static synthetic access$1300(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->getAlignmentRunnableRetry:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->postAlignmentRunnable(Z)V

    return-void
.end method

.method static synthetic access$1500(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->getAlignmentRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$Alignment;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;
    .param p1, "x1"    # Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$Alignment;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->handleAutoAlignmentResult(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$Alignment;)V

    return-void
.end method

.method static synthetic access$1700(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->waitForAutoAlignment:Z

    return v0
.end method

.method static synthetic access$1800(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->waitForFusionStatus:Z

    return v0
.end method

.method static synthetic access$1900(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)Ljava/io/OutputStream;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->outputStream:Ljava/io/OutputStream;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$CommandWriter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;
    .param p1, "x1"    # Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$CommandWriter;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->invokeUblox(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$CommandWriter;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2000([BII)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # [B
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 47
    invoke-static {p0, p1, p2}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->bytesToHex([BII)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2100()[B
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->CFG_ESF_RAW:[B

    return-object v0
.end method

.method static synthetic access$2200(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sensorDataProcessor:Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)Lcom/squareup/otto/Bus;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->bus:Lcom/squareup/otto/Bus;

    return-object v0
.end method

.method static synthetic access$2400()[B
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->CFG_RST_WARM:[B

    return-object v0
.end method

.method static synthetic access$2500(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sendAutoAlignment()V

    return-void
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->deadReckoningInjectionStarted:Z

    return v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)J
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    .prologue
    .line 47
    iget-wide v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->lastConnectionFailure:J

    return-wide v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$600()[B
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->GET_ESF_STATUS:[B

    return-object v0
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;[BLjava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;
    .param p1, "x1"    # [B
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->invokeUblox([BLjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->getFusionStatusRunnableRetry:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->postFusionRunnable(Z)V

    return-void
.end method

.method private static bytesToHex([BII)Ljava/lang/String;
    .locals 6
    .param p0, "bytes"    # [B
    .param p1, "offset"    # I
    .param p2, "count"    # I

    .prologue
    .line 537
    mul-int/lit8 v3, p2, 0x2

    new-array v0, v3, [C

    .line 538
    .local v0, "hexChars":[C
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p2, :cond_0

    .line 539
    add-int v3, v1, p1

    aget-byte v3, p0, v3

    and-int/lit16 v2, v3, 0xff

    .line 540
    .local v2, "v":I
    mul-int/lit8 v3, v1, 0x2

    sget-object v4, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->HEX:[C

    ushr-int/lit8 v5, v2, 0x4

    aget-char v4, v4, v5

    aput-char v4, v0, v3

    .line 541
    mul-int/lit8 v3, v1, 0x2

    add-int/lit8 v3, v3, 0x1

    sget-object v4, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->HEX:[C

    and-int/lit8 v5, v2, 0xf

    aget-char v4, v4, v5

    aput-char v4, v0, v3

    .line 538
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 543
    .end local v2    # "v":I
    :cond_0
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>([C)V

    return-object v3
.end method

.method private static calculateChecksum([B)V
    .locals 5
    .param p0, "message"    # [B

    .prologue
    .line 523
    array-length v3, p0

    .line 525
    .local v3, "length":I
    const/4 v0, 0x0

    .line 526
    .local v0, "ck_a":B
    const/4 v1, 0x0

    .line 528
    .local v1, "ck_b":B
    const/4 v2, 0x2

    .local v2, "i":I
    :goto_0
    add-int/lit8 v4, v3, -0x2

    if-ge v2, v4, :cond_0

    .line 529
    aget-byte v4, p0, v2

    add-int/2addr v4, v0

    int-to-byte v0, v4

    .line 530
    add-int v4, v1, v0

    int-to-byte v1, v4

    .line 528
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 532
    :cond_0
    add-int/lit8 v4, v3, -0x2

    aput-byte v0, p0, v4

    .line 533
    add-int/lit8 v4, v3, -0x1

    aput-byte v1, p0, v4

    .line 534
    return-void
.end method

.method private checkAlignment()V
    .locals 2

    .prologue
    .line 699
    iget-boolean v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->alignmentChecked:Z

    if-nez v0, :cond_0

    .line 700
    sget-object v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "checking alignment"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 701
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->postAlignmentRunnable(Z)V

    .line 705
    :goto_0
    return-void

    .line 703
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "alignment already checked"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private closeSocket()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 426
    iput-boolean v4, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->runThread:Z

    .line 427
    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->inputStream:Ljava/io/InputStream;

    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 428
    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->outputStream:Ljava/io/OutputStream;

    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 429
    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->socket:Ljava/net/Socket;

    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 430
    iput-object v3, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->inputStream:Ljava/io/InputStream;

    .line 431
    iput-object v3, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->outputStream:Ljava/io/OutputStream;

    .line 432
    iput-object v3, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->socket:Ljava/net/Socket;

    .line 433
    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->thread:Ljava/lang/Thread;

    if-eqz v1, :cond_0

    .line 435
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->thread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 436
    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->thread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 437
    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "waiting for thread"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 438
    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->thread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->join()V

    .line 439
    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "waited"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 446
    :goto_0
    iput-object v3, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->thread:Ljava/lang/Thread;

    .line 450
    :cond_0
    :goto_1
    iput-boolean v4, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->deadReckoningOn:Z

    .line 451
    const/4 v1, -0x1

    iput-byte v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->deadReckoningType:B

    .line 452
    return-void

    .line 441
    :cond_1
    :try_start_1
    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "thread is not alive"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 443
    :catch_0
    move-exception v0

    .line 444
    .local v0, "t":Ljava/lang/Throwable;
    :try_start_2
    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 446
    iput-object v3, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->thread:Ljava/lang/Thread;

    goto :goto_1

    .end local v0    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v1

    iput-object v3, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->thread:Ljava/lang/Thread;

    throw v1
.end method

.method private connectSocket()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 456
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->socket:Ljava/net/Socket;

    if-nez v2, :cond_0

    .line 457
    new-instance v2, Ljava/net/Socket;

    const-string v3, "127.0.0.1"

    const v4, 0xa5c2

    invoke-direct {v2, v3, v4}, Ljava/net/Socket;-><init>(Ljava/lang/String;I)V

    iput-object v2, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->socket:Ljava/net/Socket;

    .line 458
    iget-object v2, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->socket:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->inputStream:Ljava/io/InputStream;

    .line 459
    iget-object v2, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->socket:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->outputStream:Ljava/io/OutputStream;

    .line 460
    sget-object v2, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "connected to 42434"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 461
    iget-object v2, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->outputStream:Ljava/io/OutputStream;

    sget-object v3, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->PASSPHRASE:[B

    invoke-virtual {v2, v3}, Ljava/io/OutputStream;->write([B)V

    .line 462
    sget-object v2, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "sent passphrase"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 463
    new-instance v2, Ljava/lang/Thread;

    invoke-direct {v2, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v2, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->thread:Ljava/lang/Thread;

    .line 464
    iget-object v2, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->thread:Ljava/lang/Thread;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "GpsDeadReckoningManager-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->THREAD_COUNTER:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 465
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->runThread:Z

    .line 466
    iget-object v2, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->thread:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 469
    const/16 v2, 0x7d0

    invoke-static {v2}, Lcom/navdy/hud/app/util/GenericUtil;->sleep(I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 475
    :cond_0
    :goto_0
    return v1

    .line 472
    :catch_0
    move-exception v0

    .line 473
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 474
    invoke-direct {p0}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->closeSocket()V

    .line 475
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static getDRType(B)Ljava/lang/String;
    .locals 1
    .param p0, "b"    # B

    .prologue
    .line 674
    packed-switch p0, :pswitch_data_0

    .line 694
    const-string v0, "UNKNOWN"

    :goto_0
    return-object v0

    .line 676
    :pswitch_0
    const-string v0, "NO_FIX"

    goto :goto_0

    .line 679
    :pswitch_1
    const-string v0, "DEAD_RECKONING_ONLY"

    goto :goto_0

    .line 682
    :pswitch_2
    const-string v0, "FIX_2D"

    goto :goto_0

    .line 685
    :pswitch_3
    const-string v0, "FIX_3D"

    goto :goto_0

    .line 688
    :pswitch_4
    const-string v0, "GPS_DEAD_RECKONING_COMBINED"

    goto :goto_0

    .line 691
    :pswitch_5
    const-string v0, "TIME_ONLY"

    goto :goto_0

    .line 674
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static getInstance()Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sInstance:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    return-object v0
.end method

.method private handleAlignment(II)V
    .locals 22
    .param p1, "index"    # I
    .param p2, "nread"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 788
    add-int/lit8 v8, p1, 0x18

    .line 790
    .local v8, "endIndex":I
    move/from16 v0, p2

    if-gt v8, v0, :cond_3

    .line 791
    sget-object v19, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->READ_BUF:[B

    const/16 v20, 0x18

    move-object/from16 v0, v19

    move/from16 v1, p1

    move/from16 v2, v20

    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->bytesToHex([BII)Ljava/lang/String;

    move-result-object v16

    .line 792
    .local v16, "str":Ljava/lang/String;
    sget-object v19, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "alignment raw data["

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "]"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 794
    sget-object v19, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->READ_BUF:[B

    const/16 v20, 0x18

    move-object/from16 v0, v19

    move/from16 v1, p1

    move/from16 v2, v20

    invoke-static {v0, v1, v2}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 795
    .local v6, "buffer":Ljava/nio/ByteBuffer;
    sget-object v19, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 796
    sget-object v19, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->TEMP_BUF:[B

    const/16 v20, 0x0

    sget-object v21, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->ALIGNMENT_PATTERN:[B

    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v21, v0

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v6, v0, v1, v2}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 799
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v11

    .line 800
    .local v11, "len":S
    const/16 v19, 0x10

    move/from16 v0, v19

    if-eq v11, v0, :cond_0

    .line 801
    new-instance v19, Ljava/lang/RuntimeException;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "len["

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "] expected ["

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const/16 v21, 0x10

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "]"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v19

    .line 805
    :cond_0
    sget-object v19, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->TEMP_BUF:[B

    const/16 v20, 0x0

    const/16 v21, 0x4

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v6, v0, v1, v2}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 808
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->get()B

    move-result v17

    .line 811
    .local v17, "version":I
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->get()B

    move-result v10

    .line 812
    .local v10, "flags":B
    and-int/lit8 v13, v10, 0x1

    .line 813
    .local v13, "n":I
    and-int/lit8 v19, v10, 0xe

    shr-int/lit8 v4, v19, 0x1

    .line 815
    .local v4, "alignmentStatus":I
    const/16 v19, 0x1

    move/from16 v0, v19

    if-ne v13, v0, :cond_1

    const/4 v5, 0x1

    .line 816
    .local v5, "autoAlignment":Z
    :goto_0
    const/16 v19, 0x3

    move/from16 v0, v19

    if-ne v4, v0, :cond_2

    const/4 v7, 0x1

    .line 819
    .local v7, "done":Z
    :goto_1
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->get()B

    move-result v9

    .line 822
    .local v9, "error":I
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->get()B

    .line 825
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v18, v0

    .line 826
    .local v18, "yaw":F
    const/high16 v19, 0x42c80000    # 100.0f

    div-float v18, v18, v19

    .line 829
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v19

    move/from16 v0, v19

    int-to-float v14, v0

    .line 830
    .local v14, "pitch":F
    const/high16 v19, 0x42c80000    # 100.0f

    div-float v14, v14, v19

    .line 833
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v19

    move/from16 v0, v19

    int-to-float v15, v0

    .line 834
    .local v15, "roll":F
    const/high16 v19, 0x42c80000    # 100.0f

    div-float v15, v15, v19

    .line 836
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v12

    .line 837
    .local v12, "msg":Landroid/os/Message;
    const/16 v19, 0x1

    move/from16 v0, v19

    iput v0, v12, Landroid/os/Message;->what:I

    .line 838
    new-instance v3, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$Alignment;

    move/from16 v0, v18

    invoke-direct {v3, v0, v14, v15, v7}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$Alignment;-><init>(FFFZ)V

    .line 839
    .local v3, "alignment":Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$Alignment;
    iput-object v3, v12, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 841
    sget-object v19, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Alignment bitField["

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "] alignment["

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "] autoAlignment["

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "] version["

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "] "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 843
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->handler:Landroid/os/Handler;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->getAlignmentRunnableRetry:Ljava/lang/Runnable;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 844
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->handler:Landroid/os/Handler;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 849
    .end local v3    # "alignment":Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$Alignment;
    .end local v4    # "alignmentStatus":I
    .end local v5    # "autoAlignment":Z
    .end local v6    # "buffer":Ljava/nio/ByteBuffer;
    .end local v7    # "done":Z
    .end local v9    # "error":I
    .end local v10    # "flags":B
    .end local v11    # "len":S
    .end local v12    # "msg":Landroid/os/Message;
    .end local v13    # "n":I
    .end local v14    # "pitch":F
    .end local v15    # "roll":F
    .end local v16    # "str":Ljava/lang/String;
    .end local v17    # "version":I
    .end local v18    # "yaw":F
    :goto_2
    return-void

    .line 815
    .restart local v4    # "alignmentStatus":I
    .restart local v6    # "buffer":Ljava/nio/ByteBuffer;
    .restart local v10    # "flags":B
    .restart local v11    # "len":S
    .restart local v13    # "n":I
    .restart local v16    # "str":Ljava/lang/String;
    .restart local v17    # "version":I
    :cond_1
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 816
    .restart local v5    # "autoAlignment":Z
    :cond_2
    const/4 v7, 0x0

    goto/16 :goto_1

    .line 847
    .end local v4    # "alignmentStatus":I
    .end local v5    # "autoAlignment":Z
    .end local v6    # "buffer":Ljava/nio/ByteBuffer;
    .end local v10    # "flags":B
    .end local v11    # "len":S
    .end local v13    # "n":I
    .end local v16    # "str":Ljava/lang/String;
    .end local v17    # "version":I
    :cond_3
    const/16 v19, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->postAlignmentRunnable(Z)V

    goto :goto_2
.end method

.method private handleAutoAlignmentResult(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$Alignment;)V
    .locals 18
    .param p1, "alignment"    # Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$Alignment;

    .prologue
    .line 852
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->waitForAutoAlignment:Z

    if-nez v15, :cond_6

    .line 853
    sget-object v15, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v16, "waiting for align:false"

    invoke-virtual/range {v15 .. v16}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 854
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v15

    invoke-virtual {v15}, Lcom/navdy/hud/app/obd/ObdManager;->getVin()Ljava/lang/String;

    move-result-object v14

    .line 855
    .local v14, "vin":Ljava/lang/String;
    if-nez v14, :cond_0

    .line 856
    const-string v14, "UNKNOWN_VIN"

    .line 859
    :cond_0
    sget-object v15, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Vin is "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 860
    invoke-static {v14}, Lcom/navdy/hud/app/storage/db/helper/VinInformationHelper;->getVinInfo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 861
    .local v3, "info":Ljava/lang/String;
    const/4 v11, 0x0

    .line 862
    .local v11, "triggerAlignment":Z
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 863
    sget-object v15, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v16, "no info found for vin,  need auto alignment"

    invoke-virtual/range {v15 .. v16}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 864
    const/4 v11, 0x1

    .line 892
    :goto_0
    invoke-static {}, Lcom/navdy/hud/app/storage/db/helper/VinInformationHelper;->getVinPreference()Landroid/content/SharedPreferences;

    move-result-object v8

    .line 893
    .local v8, "pref":Landroid/content/SharedPreferences;
    const-string v15, "vin"

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-interface {v8, v15, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 894
    .local v7, "lastActiveVin":Ljava/lang/String;
    sget-object v15, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "last vin:"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " current vin:"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 895
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_4

    invoke-static {v7, v14}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_4

    .line 896
    sget-object v15, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v16, "vin has switched: trigger auto alignment"

    invoke-virtual/range {v15 .. v16}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 897
    const/4 v11, 0x1

    .line 898
    const-string v15, "GPS_Calibration_VinSwitch"

    const/16 v16, 0x0

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-static/range {v15 .. v16}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 903
    :goto_1
    if-eqz v11, :cond_5

    .line 904
    sget-object v15, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v16, "alignment required"

    invoke-virtual/range {v15 .. v16}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 907
    invoke-static {}, Lcom/navdy/hud/app/storage/db/helper/VinInformationHelper;->getVinPreference()Landroid/content/SharedPreferences;

    move-result-object v15

    invoke-interface {v15}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v15

    const-string v16, "vin"

    invoke-interface/range {v15 .. v16}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v15

    invoke-interface {v15}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 908
    sget-object v15, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v16, "last pref removed"

    invoke-virtual/range {v15 .. v16}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 909
    new-instance v15, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$11;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$11;-><init>(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->invokeUblox(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$CommandWriter;)Z

    move-result v9

    .line 915
    .local v9, "succeeded":Z
    if-nez v9, :cond_1

    .line 916
    const/4 v15, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->postAlignmentRunnable(Z)V

    .line 941
    .end local v3    # "info":Ljava/lang/String;
    .end local v7    # "lastActiveVin":Ljava/lang/String;
    .end local v8    # "pref":Landroid/content/SharedPreferences;
    .end local v9    # "succeeded":Z
    .end local v11    # "triggerAlignment":Z
    .end local v14    # "vin":Ljava/lang/String;
    :cond_1
    :goto_2
    return-void

    .line 867
    .restart local v3    # "info":Ljava/lang/String;
    .restart local v11    # "triggerAlignment":Z
    .restart local v14    # "vin":Ljava/lang/String;
    :cond_2
    :try_start_0
    sget-object v15, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "VinInfo is "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 868
    move-object/from16 v0, p1

    iget v15, v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$Alignment;->yaw:F

    const/16 v16, 0x0

    cmpl-float v15, v15, v16

    if-nez v15, :cond_3

    move-object/from16 v0, p1

    iget v15, v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$Alignment;->pitch:F

    const/16 v16, 0x0

    cmpl-float v15, v15, v16

    if-nez v15, :cond_3

    move-object/from16 v0, p1

    iget v15, v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$Alignment;->roll:F

    const/16 v16, 0x0

    cmpl-float v15, v15, v16

    if-nez v15, :cond_3

    .line 872
    sget-object v15, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v16, "VinInfo exists but u-blox alignment is lost, trigger alignment"

    invoke-virtual/range {v15 .. v16}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 873
    const-string v15, "GPS_Calibration_Lost"

    const/16 v16, 0x0

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-static/range {v15 .. v16}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 874
    const/4 v11, 0x1

    .line 875
    invoke-static {v14}, Lcom/navdy/hud/app/storage/db/helper/VinInformationHelper;->deleteVinInfo(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 886
    :catch_0
    move-exception v10

    .line 887
    .local v10, "t":Ljava/lang/Throwable;
    sget-object v15, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v15, v10}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 878
    .end local v10    # "t":Ljava/lang/Throwable;
    :cond_3
    :try_start_1
    new-instance v15, Lorg/json/JSONObject;

    invoke-direct {v15, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->rootInfo:Lorg/json/JSONObject;

    .line 879
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->rootInfo:Lorg/json/JSONObject;

    const-string v16, "gps"

    invoke-virtual/range {v15 .. v16}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    .line 880
    .local v6, "jsonObject":Lorg/json/JSONObject;
    const-string v15, "alignment_time"

    invoke-virtual {v6, v15}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 882
    .local v2, "alignmentTime":Ljava/lang/String;
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v12

    .line 883
    .local v12, "time":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    sub-long v4, v16, v12

    .line 884
    .local v4, "elapsed":J
    sget-object v15, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "elapsed="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 900
    .end local v2    # "alignmentTime":Ljava/lang/String;
    .end local v4    # "elapsed":J
    .end local v6    # "jsonObject":Lorg/json/JSONObject;
    .end local v12    # "time":J
    .restart local v7    # "lastActiveVin":Ljava/lang/String;
    .restart local v8    # "pref":Landroid/content/SharedPreferences;
    :cond_4
    sget-object v15, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v16, "vin switch not detected"

    invoke-virtual/range {v15 .. v16}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 919
    :cond_5
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->alignmentChecked:Z

    .line 920
    sget-object v15, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v16, "alignment not reqd"

    invoke-virtual/range {v15 .. v16}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 921
    invoke-static {}, Lcom/navdy/hud/app/framework/voice/TTSUtils;->debugShowGpsSensorCalibrationNotNeeded()V

    goto/16 :goto_2

    .line 925
    .end local v3    # "info":Ljava/lang/String;
    .end local v7    # "lastActiveVin":Ljava/lang/String;
    .end local v8    # "pref":Landroid/content/SharedPreferences;
    .end local v11    # "triggerAlignment":Z
    .end local v14    # "vin":Ljava/lang/String;
    :cond_6
    move-object/from16 v0, p1

    iget-boolean v15, v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$Alignment;->done:Z

    if-nez v15, :cond_7

    .line 926
    sget-object v15, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v16, "alignment not done yet, try again"

    invoke-virtual/range {v15 .. v16}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 927
    const/4 v15, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->postAlignmentRunnable(Z)V

    goto/16 :goto_2

    .line 931
    :cond_7
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->alignmentInfo:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$Alignment;

    .line 932
    const-string v15, "GPS_Calibration_IMU_Done"

    const/16 v16, 0x0

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-static/range {v15 .. v16}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 933
    invoke-static {}, Lcom/navdy/hud/app/framework/voice/TTSUtils;->debugShowGpsImuCalibrationDone()V

    .line 935
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->waitForAutoAlignment:Z

    .line 936
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->waitForFusionStatus:Z

    .line 938
    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->postFusionRunnable(Z)V

    .line 939
    sget-object v15, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v16, "got alignment, wait for fusion status"

    invoke-virtual/range {v15 .. v16}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method private handleEsfRaw(II)V
    .locals 6
    .param p1, "index"    # I
    .param p2, "nread"    # I

    .prologue
    .line 723
    new-instance v0, Lcom/navdy/hud/app/device/gps/RawSensorData;

    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->READ_BUF:[B

    invoke-direct {v0, v1, p1}, Lcom/navdy/hud/app/device/gps/RawSensorData;-><init>([BI)V

    .line 724
    .local v0, "rawSensorData":Lcom/navdy/hud/app/device/gps/RawSensorData;
    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sensorDataProcessor:Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;->onRawData(Lcom/navdy/hud/app/device/gps/RawSensorData;)V

    .line 725
    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sensorDataProcessor:Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;

    invoke-virtual {v1}, Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;->isCalibrated()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 726
    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v2, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;

    iget-object v3, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sensorDataProcessor:Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;

    iget v3, v3, Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;->xAccel:F

    iget-object v4, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sensorDataProcessor:Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;

    iget v4, v4, Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;->yAccel:F

    iget-object v5, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sensorDataProcessor:Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;

    iget v5, v5, Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;->zAccel:F

    invoke-direct {v2, v3, v4, v5}, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;-><init>(FFF)V

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 728
    :cond_0
    return-void
.end method

.method private handleFusion(II)V
    .locals 11
    .param p1, "index"    # I
    .param p2, "nread"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 945
    add-int/lit8 v1, p1, 0x13

    .line 947
    .local v1, "endIndex":I
    if-gt v1, p2, :cond_1

    .line 948
    sget-object v8, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->READ_BUF:[B

    const/16 v9, 0x13

    invoke-static {v8, p1, v9}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->bytesToHex([BII)Ljava/lang/String;

    move-result-object v6

    .line 949
    .local v6, "str":Ljava/lang/String;
    sget-object v8, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "esf raw data["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 951
    sget-object v8, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->READ_BUF:[B

    const/16 v9, 0x13

    invoke-static {v8, p1, v9}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 952
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    sget-object v8, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 953
    sget-object v8, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->TEMP_BUF:[B

    const/4 v9, 0x0

    sget-object v10, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->ALIGNMENT_PATTERN:[B

    array-length v10, v10

    invoke-virtual {v0, v8, v9, v10}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 956
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v5

    .line 959
    .local v5, "len":S
    sget-object v8, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->TEMP_BUF:[B

    const/4 v9, 0x0

    const/4 v10, 0x4

    invoke-virtual {v0, v8, v9, v10}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 962
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v7

    .line 965
    .local v7, "version":I
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v3

    .line 968
    .local v3, "initStatus1":I
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v4

    .line 971
    .local v4, "initStatus2":I
    sget-object v8, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->TEMP_BUF:[B

    const/4 v9, 0x0

    const/4 v10, 0x5

    invoke-virtual {v0, v8, v9, v10}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 973
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v2

    .line 975
    .local v2, "fusionMode":I
    iget-object v8, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->handler:Landroid/os/Handler;

    iget-object v9, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->getFusionStatusRunnableRetry:Ljava/lang/Runnable;

    invoke-virtual {v8, v9}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 977
    const/4 v8, 0x1

    if-eq v2, v8, :cond_0

    .line 978
    sget-object v8, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Fusion not done retry len["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] fusionMode["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] initStatus1["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] initStatus2["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] version["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 979
    const/4 v8, 0x1

    invoke-direct {p0, v8}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->postFusionRunnable(Z)V

    .line 988
    .end local v0    # "buffer":Ljava/nio/ByteBuffer;
    .end local v2    # "fusionMode":I
    .end local v3    # "initStatus1":I
    .end local v4    # "initStatus2":I
    .end local v5    # "len":S
    .end local v6    # "str":Ljava/lang/String;
    .end local v7    # "version":I
    :goto_0
    return-void

    .line 981
    .restart local v0    # "buffer":Ljava/nio/ByteBuffer;
    .restart local v2    # "fusionMode":I
    .restart local v3    # "initStatus1":I
    .restart local v4    # "initStatus2":I
    .restart local v5    # "len":S
    .restart local v6    # "str":Ljava/lang/String;
    .restart local v7    # "version":I
    :cond_0
    sget-object v8, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Fusion Done len["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] fusionMode["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] initStatus1["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] initStatus2["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] version["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 982
    invoke-direct {p0}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->storeVinInfoInDb()V

    goto :goto_0

    .line 986
    .end local v0    # "buffer":Ljava/nio/ByteBuffer;
    .end local v2    # "fusionMode":I
    .end local v3    # "initStatus1":I
    .end local v4    # "initStatus2":I
    .end local v5    # "len":S
    .end local v6    # "str":Ljava/lang/String;
    .end local v7    # "version":I
    :cond_1
    const/4 v8, 0x1

    invoke-direct {p0, v8}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->postFusionRunnable(Z)V

    goto :goto_0
.end method

.method private handleNavStatus(II)V
    .locals 8
    .param p1, "index"    # I
    .param p2, "nread"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    .line 740
    add-int/lit8 v2, p1, 0xa

    .line 742
    .local v2, "gpsFixIndex":I
    add-int/lit8 v4, p2, -0x1

    if-gt v2, v4, :cond_0

    .line 743
    sget-object v4, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->READ_BUF:[B

    aget-byte v3, v4, v2

    .line 745
    .local v3, "val":B
    packed-switch v3, :pswitch_data_0

    .line 773
    :pswitch_0
    iget-boolean v4, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->deadReckoningOn:Z

    if-eqz v4, :cond_0

    .line 774
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->deadReckoningOn:Z

    .line 775
    const/4 v4, -0x1

    iput-byte v4, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->deadReckoningType:B

    .line 776
    invoke-static {}, Lcom/navdy/hud/app/framework/voice/TTSUtils;->debugShowDREnded()V

    .line 777
    sget-object v4, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "dead reckoning stopped:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 778
    const-string v4, "GPS_DR_STOPPED"

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/navdy/hud/app/device/gps/GpsUtils;->sendEventBroadcast(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 783
    .end local v3    # "val":B
    :cond_0
    :goto_0
    return-void

    .line 748
    .restart local v3    # "val":B
    :pswitch_1
    iget-boolean v4, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->deadReckoningOn:Z

    if-eqz v4, :cond_2

    .line 750
    iget-byte v4, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->deadReckoningType:B

    if-eq v4, v3, :cond_0

    .line 751
    invoke-static {v3}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->getDRType(B)Ljava/lang/String;

    move-result-object v1

    .line 752
    .local v1, "drType":Ljava/lang/String;
    sget-object v4, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "dead reckoning type changed from ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-byte v6, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->deadReckoningType:B

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] to ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 753
    iput-byte v3, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->deadReckoningType:B

    .line 754
    invoke-static {v1}, Lcom/navdy/hud/app/framework/voice/TTSUtils;->debugShowDRStarted(Ljava/lang/String;)V

    .line 755
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 756
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v5, "drtype"

    if-ne v3, v7, :cond_1

    const-string v4, "DEAD_RECKONING_ONLY"

    :goto_1
    invoke-virtual {v0, v5, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 757
    const-string v4, "GPS_DR_STARTED"

    invoke-static {v4, v0}, Lcom/navdy/hud/app/device/gps/GpsUtils;->sendEventBroadcast(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 756
    :cond_1
    const-string v4, "GPS_DEAD_RECKONING_COMBINED"

    goto :goto_1

    .line 761
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "drType":Ljava/lang/String;
    :cond_2
    iput-boolean v7, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->deadReckoningOn:Z

    .line 762
    iput-byte v3, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->deadReckoningType:B

    .line 763
    invoke-static {v3}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->getDRType(B)Ljava/lang/String;

    move-result-object v1

    .line 764
    .restart local v1    # "drType":Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/hud/app/framework/voice/TTSUtils;->debugShowDRStarted(Ljava/lang/String;)V

    .line 765
    sget-object v4, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "dead reckoning on["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 766
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 767
    .restart local v0    # "bundle":Landroid/os/Bundle;
    const-string v5, "drtype"

    if-ne v3, v7, :cond_3

    const-string v4, "DEAD_RECKONING_ONLY"

    :goto_2
    invoke-virtual {v0, v5, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 768
    const-string v4, "GPS_DR_STARTED"

    invoke-static {v4, v0}, Lcom/navdy/hud/app/device/gps/GpsUtils;->sendEventBroadcast(Ljava/lang/String;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 767
    :cond_3
    const-string v4, "GPS_DEAD_RECKONING_COMBINED"

    goto :goto_2

    .line 745
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private initDeadReckoning()V
    .locals 2

    .prologue
    .line 416
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/obd/ObdManager;->isSpeedPidAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 417
    sget-object v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "speed pid is available"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 418
    invoke-direct {p0}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->checkAlignment()V

    .line 419
    invoke-direct {p0}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->startDeadReckoning()V

    .line 423
    :goto_0
    return-void

    .line 421
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "speed pid is not available"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private invokeUblox(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$CommandWriter;)Z
    .locals 8
    .param p1, "callback"    # Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$CommandWriter;

    .prologue
    const-wide/16 v6, 0x0

    .line 371
    const/4 v2, 0x0

    .line 373
    .local v2, "succeeded":Z
    :try_start_0
    iget-wide v4, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->lastConnectionFailure:J

    cmp-long v4, v4, v6

    if-lez v4, :cond_1

    .line 374
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->lastConnectionFailure:J

    sub-long v0, v4, v6

    .line 375
    .local v0, "diff":J
    const-wide/16 v4, 0x2710

    cmp-long v4, v0, v4

    if-gez v4, :cond_1

    .line 376
    sget-object v4, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 377
    sget-object v4, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "would retry time["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 379
    :cond_0
    const/4 v4, 0x0

    .line 394
    .end local v0    # "diff":J
    :goto_0
    return v4

    .line 382
    :cond_1
    invoke-direct {p0}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->connectSocket()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 383
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->lastConnectionFailure:J

    .line 384
    invoke-interface {p1}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$CommandWriter;->run()V

    .line 385
    const/4 v2, 0x1

    :goto_1
    move v4, v2

    .line 394
    goto :goto_0

    .line 387
    :cond_2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->lastConnectionFailure:J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 389
    :catch_0
    move-exception v3

    .line 390
    .local v3, "t":Ljava/io/IOException;
    sget-object v4, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Failed to "

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 391
    invoke-direct {p0}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->closeSocket()V

    .line 392
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->lastConnectionFailure:J

    goto :goto_1
.end method

.method private invokeUblox([BLjava/lang/String;)Z
    .locals 1
    .param p1, "message"    # [B
    .param p2, "description"    # Ljava/lang/String;

    .prologue
    .line 353
    new-instance v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$8;

    invoke-direct {v0, p0, p2, p1}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$8;-><init>(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;Ljava/lang/String;[B)V

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->invokeUblox(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$CommandWriter;)Z

    move-result v0

    return v0
.end method

.method private postAlignmentRunnable(Z)V
    .locals 4
    .param p1, "delayed"    # Z

    .prologue
    .line 1080
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->getAlignmentRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1081
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->getAlignmentRunnableRetry:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1082
    if-eqz p1, :cond_0

    .line 1083
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->getAlignmentRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1087
    :goto_0
    return-void

    .line 1085
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->getAlignmentRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private postFusionRunnable(Z)V
    .locals 4
    .param p1, "delayed"    # Z

    .prologue
    .line 1090
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->getFusionStatusRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1091
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->getFusionStatusRunnableRetry:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1092
    if-eqz p1, :cond_0

    .line 1093
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->getFusionStatusRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1097
    :goto_0
    return-void

    .line 1095
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->getFusionStatusRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private sendAutoAlignment()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 708
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/obd/ObdManager;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 709
    sget-object v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "not connected to obd manager any more"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 720
    :goto_0
    return-void

    .line 713
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "send auto alignment ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->AUTO_ALIGNMENT:[B

    sget-object v3, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->AUTO_ALIGNMENT:[B

    array-length v3, v3

    invoke-static {v2, v4, v3}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->bytesToHex([BII)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 714
    iput-boolean v5, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->waitForAutoAlignment:Z

    .line 715
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->outputStream:Ljava/io/OutputStream;

    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->AUTO_ALIGNMENT:[B

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    .line 716
    invoke-direct {p0, v5}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->postAlignmentRunnable(Z)V

    .line 718
    const-string v0, "GPS_Calibration_Start"

    new-array v1, v4, [Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 719
    invoke-static {}, Lcom/navdy/hud/app/framework/voice/TTSUtils;->debugShowGpsCalibrationStarted()V

    goto :goto_0
.end method

.method private sendObdSpeed()V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v10, 0x10

    const/16 v9, 0x8

    const/4 v8, 0x0

    .line 480
    sget-object v4, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v3

    .line 481
    .local v3, "verbose":Z
    iget-object v4, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    invoke-virtual {v4}, Lcom/navdy/hud/app/manager/SpeedManager;->getRawObdSpeed()I

    move-result v4

    int-to-long v0, v4

    .line 482
    .local v0, "speed":J
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-gez v4, :cond_1

    .line 483
    if-eqz v3, :cond_0

    .line 484
    sget-object v4, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "invalid obd speed:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 520
    :cond_0
    :goto_0
    return-void

    .line 490
    :cond_1
    iget v4, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->timeStampCounter:I

    add-int/lit8 v4, v4, 0x64

    iput v4, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->timeStampCounter:I

    .line 492
    iget v4, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->timeStampCounter:I

    if-gez v4, :cond_2

    .line 493
    const/16 v4, 0x64

    iput v4, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->timeStampCounter:I

    .line 496
    :cond_2
    sget-object v4, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->INJECT_OBD_SPEED:[B

    const/4 v5, 0x6

    iget v6, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->timeStampCounter:I

    shr-int/lit8 v6, v6, 0x0

    int-to-byte v6, v6

    aput-byte v6, v4, v5

    .line 497
    sget-object v4, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->INJECT_OBD_SPEED:[B

    const/4 v5, 0x7

    iget v6, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->timeStampCounter:I

    shr-int/lit8 v6, v6, 0x8

    int-to-byte v6, v6

    aput-byte v6, v4, v5

    .line 498
    sget-object v4, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->INJECT_OBD_SPEED:[B

    iget v5, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->timeStampCounter:I

    shr-int/lit8 v5, v5, 0x10

    int-to-byte v5, v5

    aput-byte v5, v4, v9

    .line 499
    sget-object v4, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->INJECT_OBD_SPEED:[B

    const/16 v5, 0x9

    iget v6, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->timeStampCounter:I

    shr-int/lit8 v6, v6, 0x18

    int-to-byte v6, v6

    aput-byte v6, v4, v5

    .line 502
    long-to-double v4, v0

    sget-object v6, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->KILOMETERS_PER_HOUR:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    sget-object v7, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->METERS_PER_SECOND:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    invoke-static {v4, v5, v6, v7}, Lcom/navdy/hud/app/manager/SpeedManager;->convertWithPrecision(DLcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;)F

    move-result v2

    .line 506
    .local v2, "speedF":F
    const/high16 v4, 0x447a0000    # 1000.0f

    mul-float/2addr v2, v4

    .line 507
    float-to-int v4, v2

    int-to-long v0, v4

    .line 510
    sget-object v4, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->INJECT_OBD_SPEED:[B

    const/16 v5, 0xe

    shr-long v6, v0, v8

    long-to-int v6, v6

    int-to-byte v6, v6

    aput-byte v6, v4, v5

    .line 511
    sget-object v4, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->INJECT_OBD_SPEED:[B

    const/16 v5, 0xf

    shr-long v6, v0, v9

    long-to-int v6, v6

    int-to-byte v6, v6

    aput-byte v6, v4, v5

    .line 512
    sget-object v4, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->INJECT_OBD_SPEED:[B

    shr-long v6, v0, v10

    long-to-int v5, v6

    int-to-byte v5, v5

    aput-byte v5, v4, v10

    .line 514
    sget-object v4, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->INJECT_OBD_SPEED:[B

    invoke-static {v4}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->calculateChecksum([B)V

    .line 516
    if-eqz v3, :cond_3

    .line 517
    sget-object v4, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "inject obd speed ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->INJECT_OBD_SPEED:[B

    sget-object v7, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->INJECT_OBD_SPEED:[B

    array-length v7, v7

    invoke-static {v6, v8, v7}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->bytesToHex([BII)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 519
    :cond_3
    iget-object v4, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->outputStream:Ljava/io/OutputStream;

    sget-object v5, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->INJECT_OBD_SPEED:[B

    invoke-virtual {v4, v5}, Ljava/io/OutputStream;->write([B)V

    goto/16 :goto_0
.end method

.method private startDeadReckoning()V
    .locals 4

    .prologue
    .line 618
    iget-boolean v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->deadReckoningInjectionStarted:Z

    if-eqz v0, :cond_0

    .line 625
    :goto_0
    return-void

    .line 621
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "starting dead reckoning injection"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 622
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->deadReckoningInjectionStarted:Z

    .line 623
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->injectRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 624
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->injectRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private stopDeadReckoning()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 628
    iget-boolean v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->deadReckoningInjectionStarted:Z

    if-nez v0, :cond_0

    .line 645
    :goto_0
    return-void

    .line 632
    :cond_0
    iput-boolean v2, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->waitForAutoAlignment:Z

    .line 633
    iput-boolean v2, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->waitForFusionStatus:Z

    .line 634
    iput-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->alignmentInfo:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$Alignment;

    .line 635
    iput-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->rootInfo:Lorg/json/JSONObject;

    .line 636
    iput-boolean v2, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->alignmentChecked:Z

    .line 638
    sget-object v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "stopping dead rekoning injection"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 639
    iput-boolean v2, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->deadReckoningInjectionStarted:Z

    .line 640
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->injectRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 641
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->getAlignmentRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 642
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->getAlignmentRunnableRetry:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 643
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->getFusionStatusRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 644
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->getFusionStatusRunnableRetry:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private storeVinInfoInDb()V
    .locals 8

    .prologue
    .line 993
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/obd/ObdManager;->getVin()Ljava/lang/String;

    move-result-object v3

    .line 994
    .local v3, "vin":Ljava/lang/String;
    if-nez v3, :cond_0

    .line 995
    const-string v3, "UNKNOWN_VIN"

    .line 997
    :cond_0
    sget-object v4, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "store alignment info in db for vin["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 998
    const/4 v0, 0x0

    .line 999
    .local v0, "gps":Lorg/json/JSONObject;
    iget-object v4, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->rootInfo:Lorg/json/JSONObject;

    if-nez v4, :cond_1

    .line 1000
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    iput-object v4, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->rootInfo:Lorg/json/JSONObject;

    .line 1001
    new-instance v0, Lorg/json/JSONObject;

    .end local v0    # "gps":Lorg/json/JSONObject;
    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1002
    .restart local v0    # "gps":Lorg/json/JSONObject;
    iget-object v4, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->rootInfo:Lorg/json/JSONObject;

    const-string v5, "gps"

    invoke-virtual {v4, v5, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1007
    :goto_0
    const-string v4, "yaw"

    iget-object v5, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->alignmentInfo:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$Alignment;

    iget v5, v5, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$Alignment;->yaw:F

    invoke-static {v5}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1008
    const-string v4, "pitch"

    iget-object v5, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->alignmentInfo:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$Alignment;

    iget v5, v5, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$Alignment;->pitch:F

    invoke-static {v5}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1009
    const-string v4, "roll"

    iget-object v5, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->alignmentInfo:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$Alignment;

    iget v5, v5, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$Alignment;->roll:F

    invoke-static {v5}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1010
    const-string v4, "alignment_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1012
    iget-object v4, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->rootInfo:Lorg/json/JSONObject;

    invoke-virtual {v4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1013
    .local v1, "str":Ljava/lang/String;
    invoke-static {v3, v1}, Lcom/navdy/hud/app/storage/db/helper/VinInformationHelper;->storeVinInfo(Ljava/lang/String;Ljava/lang/String;)V

    .line 1016
    invoke-static {}, Lcom/navdy/hud/app/storage/db/helper/VinInformationHelper;->getVinPreference()Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "vin"

    invoke-interface {v4, v5, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1017
    sget-object v4, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "store last vin pref ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1019
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->waitForAutoAlignment:Z

    .line 1020
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->waitForFusionStatus:Z

    .line 1021
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->alignmentChecked:Z

    .line 1023
    const-string v4, "GPS_Calibration_Sensor_Done"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->localyticsSendEvent(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1024
    invoke-static {}, Lcom/navdy/hud/app/framework/voice/TTSUtils;->debugShowGpsSensorCalibrationDone()V

    .line 1028
    .end local v0    # "gps":Lorg/json/JSONObject;
    .end local v1    # "str":Ljava/lang/String;
    .end local v3    # "vin":Ljava/lang/String;
    :goto_1
    return-void

    .line 1004
    .restart local v0    # "gps":Lorg/json/JSONObject;
    .restart local v3    # "vin":Ljava/lang/String;
    :cond_1
    iget-object v4, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->rootInfo:Lorg/json/JSONObject;

    const-string v5, "gps"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto/16 :goto_0

    .line 1025
    .end local v0    # "gps":Lorg/json/JSONObject;
    .end local v3    # "vin":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 1026
    .local v2, "t":Ljava/lang/Throwable;
    sget-object v4, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v4, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_1
.end method


# virtual methods
.method public ObdConnectionStatusEvent(Lcom/navdy/hud/app/obd/ObdManager$ObdConnectionStatusEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/obd/ObdManager$ObdConnectionStatusEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 404
    iget-boolean v0, p1, Lcom/navdy/hud/app/obd/ObdManager$ObdConnectionStatusEvent;->connected:Z

    if-eqz v0, :cond_0

    .line 406
    sget-object v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "got obd connected"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 407
    invoke-direct {p0}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->initDeadReckoning()V

    .line 413
    :goto_0
    return-void

    .line 410
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "got obd dis-connected"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 411
    invoke-direct {p0}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->stopDeadReckoning()V

    goto :goto_0
.end method

.method public dumpGpsInfo(Ljava/lang/String;)V
    .locals 9
    .param p1, "stagingPath"    # Ljava/lang/String;

    .prologue
    .line 1031
    const/4 v0, 0x0

    .line 1033
    .local v0, "fout":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "gps.log"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1034
    .end local v0    # "fout":Ljava/io/FileOutputStream;
    .local v1, "fout":Ljava/io/FileOutputStream;
    :try_start_1
    new-instance v5, Ljava/io/PrintWriter;

    new-instance v6, Ljava/io/OutputStreamWriter;

    invoke-direct {v6, v1}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v5, v6}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 1036
    .local v5, "writer":Ljava/io/PrintWriter;
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v2

    .line 1037
    .local v2, "obdManager":Lcom/navdy/hud/app/obd/ObdManager;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "obd_connected="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Lcom/navdy/hud/app/obd/ObdManager;->isConnected()Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V

    .line 1038
    iget-boolean v6, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->deadReckoningInjectionStarted:Z

    if-eqz v6, :cond_1

    .line 1039
    invoke-virtual {v2}, Lcom/navdy/hud/app/obd/ObdManager;->getVin()Ljava/lang/String;

    move-result-object v4

    .line 1040
    .local v4, "vin":Ljava/lang/String;
    if-nez v4, :cond_0

    .line 1041
    const-string v4, "UNKNOWN_VIN"

    .line 1043
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "vin="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V

    .line 1044
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "calibrated="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v6, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->rootInfo:Lorg/json/JSONObject;

    if-nez v6, :cond_2

    const-string v6, "no"

    :goto_0
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V

    .line 1045
    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V

    .line 1047
    .end local v4    # "vin":Ljava/lang/String;
    :cond_1
    invoke-virtual {v5}, Ljava/io/PrintWriter;->flush()V

    .line 1048
    sget-object v6, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "gps log written:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1052
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->fileSync(Ljava/io/FileOutputStream;)V

    .line 1053
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    move-object v0, v1

    .line 1055
    .end local v1    # "fout":Ljava/io/FileOutputStream;
    .end local v2    # "obdManager":Lcom/navdy/hud/app/obd/ObdManager;
    .end local v5    # "writer":Ljava/io/PrintWriter;
    .restart local v0    # "fout":Ljava/io/FileOutputStream;
    :goto_1
    return-void

    .line 1044
    .end local v0    # "fout":Ljava/io/FileOutputStream;
    .restart local v1    # "fout":Ljava/io/FileOutputStream;
    .restart local v2    # "obdManager":Lcom/navdy/hud/app/obd/ObdManager;
    .restart local v4    # "vin":Ljava/lang/String;
    .restart local v5    # "writer":Ljava/io/PrintWriter;
    :cond_2
    :try_start_2
    iget-object v6, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->rootInfo:Lorg/json/JSONObject;

    invoke-virtual {v6}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    goto :goto_0

    .line 1049
    .end local v1    # "fout":Ljava/io/FileOutputStream;
    .end local v2    # "obdManager":Lcom/navdy/hud/app/obd/ObdManager;
    .end local v4    # "vin":Ljava/lang/String;
    .end local v5    # "writer":Ljava/io/PrintWriter;
    .restart local v0    # "fout":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v3

    .line 1050
    .local v3, "t":Ljava/lang/Throwable;
    :goto_2
    :try_start_3
    sget-object v6, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v6, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1052
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->fileSync(Ljava/io/FileOutputStream;)V

    .line 1053
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_1

    .line 1052
    .end local v3    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v6

    :goto_3
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->fileSync(Ljava/io/FileOutputStream;)V

    .line 1053
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v6

    .line 1052
    .end local v0    # "fout":Ljava/io/FileOutputStream;
    .restart local v1    # "fout":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v6

    move-object v0, v1

    .end local v1    # "fout":Ljava/io/FileOutputStream;
    .restart local v0    # "fout":Ljava/io/FileOutputStream;
    goto :goto_3

    .line 1049
    .end local v0    # "fout":Ljava/io/FileOutputStream;
    .restart local v1    # "fout":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v3

    move-object v0, v1

    .end local v1    # "fout":Ljava/io/FileOutputStream;
    .restart local v0    # "fout":Ljava/io/FileOutputStream;
    goto :goto_2
.end method

.method public enableEsfRaw()V
    .locals 2

    .prologue
    .line 670
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->enableEsfRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 671
    return-void
.end method

.method public getDeadReckoningStatus()Ljava/lang/String;
    .locals 4

    .prologue
    const v3, 0x7f090122

    .line 1058
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1059
    .local v1, "resources":Landroid/content/res/Resources;
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdManager;->getInstance()Lcom/navdy/hud/app/obd/ObdManager;

    move-result-object v0

    .line 1060
    .local v0, "obdManager":Lcom/navdy/hud/app/obd/ObdManager;
    invoke-virtual {v0}, Lcom/navdy/hud/app/obd/ObdManager;->isConnected()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1061
    const v2, 0x7f0901e7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1075
    :goto_0
    return-object v2

    .line 1062
    :cond_0
    invoke-virtual {v0}, Lcom/navdy/hud/app/obd/ObdManager;->isSpeedPidAvailable()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1063
    const v2, 0x7f0901e6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 1064
    :cond_1
    iget-boolean v2, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->deadReckoningInjectionStarted:Z

    if-eqz v2, :cond_5

    .line 1065
    iget-object v2, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->rootInfo:Lorg/json/JSONObject;

    if-eqz v2, :cond_2

    .line 1066
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f09011f

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->rootInfo:Lorg/json/JSONObject;

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 1067
    :cond_2
    iget-boolean v2, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->waitForAutoAlignment:Z

    if-eqz v2, :cond_3

    .line 1068
    const v2, 0x7f090121

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 1069
    :cond_3
    iget-boolean v2, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->waitForFusionStatus:Z

    if-eqz v2, :cond_4

    .line 1070
    const v2, 0x7f090120

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 1072
    :cond_4
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 1075
    :cond_5
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public onDrivingStateChange(Lcom/navdy/hud/app/event/DrivingStateChange;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/event/DrivingStateChange;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 732
    iget-boolean v0, p1, Lcom/navdy/hud/app/event/DrivingStateChange;->driving:Z

    if-nez v0, :cond_0

    .line 734
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sensorDataProcessor:Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;->setCalibrated(Z)V

    .line 736
    :cond_0
    return-void
.end method

.method public onSupportedPidEventsChange(Lcom/navdy/hud/app/obd/ObdManager$ObdSupportedPidsChangedEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/hud/app/obd/ObdManager$ObdSupportedPidsChangedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 399
    invoke-direct {p0}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->initDeadReckoning()V

    .line 400
    return-void
.end method

.method public run()V
    .locals 9

    .prologue
    const/4 v8, -0x1

    const/4 v7, 0x0

    .line 548
    sget-object v3, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "start thread"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 549
    :cond_0
    :goto_0
    iget-boolean v3, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->runThread:Z

    if-eqz v3, :cond_6

    .line 551
    :try_start_0
    iget-object v3, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->inputStream:Ljava/io/InputStream;

    sget-object v4, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->READ_BUF:[B

    invoke-virtual {v3, v4}, Ljava/io/InputStream;->read([B)I

    move-result v1

    .line 552
    .local v1, "nread":I
    if-ne v1, v8, :cond_2

    .line 553
    sget-object v3, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "eof"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 554
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->runThread:Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 607
    .end local v1    # "nread":I
    :catch_0
    move-exception v2

    .line 608
    .local v2, "t":Ljava/lang/Throwable;
    instance-of v3, v2, Ljava/lang/InterruptedException;

    if-nez v3, :cond_1

    .line 609
    sget-object v3, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 611
    :cond_1
    iput-boolean v7, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->runThread:Z

    goto :goto_0

    .line 560
    .end local v2    # "t":Ljava/lang/Throwable;
    .restart local v1    # "nread":I
    :cond_2
    if-lez v1, :cond_0

    .line 561
    :try_start_1
    sget-object v3, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->READ_BUF:[B

    sget-object v4, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->ESF_RAW_PATTERN:[B

    const/4 v5, 0x0

    add-int/lit8 v6, v1, -0x1

    invoke-static {v3, v4, v5, v6}, Lcom/navdy/hud/app/util/GenericUtil;->indexOf([B[BII)I

    move-result v0

    .line 562
    .local v0, "index":I
    if-eq v0, v8, :cond_3

    .line 563
    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->handleEsfRaw(II)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 604
    .end local v0    # "index":I
    :catch_1
    move-exception v2

    .line 605
    .restart local v2    # "t":Ljava/lang/Throwable;
    :try_start_2
    sget-object v3, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 568
    .end local v2    # "t":Ljava/lang/Throwable;
    .restart local v0    # "index":I
    :cond_3
    :try_start_3
    sget-object v3, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->READ_BUF:[B

    sget-object v4, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->NAV_STATUS_PATTERN:[B

    const/4 v5, 0x0

    add-int/lit8 v6, v1, -0x1

    invoke-static {v3, v4, v5, v6}, Lcom/navdy/hud/app/util/GenericUtil;->indexOf([B[BII)I

    move-result v0

    .line 569
    if-eq v0, v8, :cond_4

    .line 570
    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->handleNavStatus(II)V

    goto :goto_0

    .line 574
    :cond_4
    iget-boolean v3, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->alignmentChecked:Z

    if-nez v3, :cond_5

    .line 576
    sget-object v3, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->READ_BUF:[B

    sget-object v4, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->ALIGNMENT_PATTERN:[B

    const/4 v5, 0x0

    add-int/lit8 v6, v1, -0x1

    invoke-static {v3, v4, v5, v6}, Lcom/navdy/hud/app/util/GenericUtil;->indexOf([B[BII)I
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    move-result v0

    .line 577
    if-eq v0, v8, :cond_5

    .line 579
    :try_start_4
    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->handleAlignment(II)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 580
    :catch_2
    move-exception v2

    .line 582
    .restart local v2    # "t":Ljava/lang/Throwable;
    :try_start_5
    sget-object v3, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 583
    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->postAlignmentRunnable(Z)V

    goto :goto_0

    .line 589
    .end local v2    # "t":Ljava/lang/Throwable;
    :cond_5
    iget-boolean v3, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->waitForFusionStatus:Z

    if-eqz v3, :cond_0

    .line 591
    sget-object v3, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->READ_BUF:[B

    sget-object v4, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->ESF_STATUS_PATTERN:[B

    const/4 v5, 0x0

    add-int/lit8 v6, v1, -0x1

    invoke-static {v3, v4, v5, v6}, Lcom/navdy/hud/app/util/GenericUtil;->indexOf([B[BII)I
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1

    move-result v0

    .line 592
    if-eq v0, v8, :cond_0

    .line 594
    :try_start_6
    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->handleFusion(II)V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_3

    goto/16 :goto_0

    .line 595
    :catch_3
    move-exception v2

    .line 597
    .restart local v2    # "t":Ljava/lang/Throwable;
    :try_start_7
    sget-object v3, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 598
    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->postFusionRunnable(Z)V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1

    goto/16 :goto_0

    .line 614
    .end local v0    # "index":I
    .end local v1    # "nread":I
    .end local v2    # "t":Ljava/lang/Throwable;
    :cond_6
    sget-object v3, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "end thread"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 615
    return-void
.end method

.method public sendWarmReset()V
    .locals 4

    .prologue
    .line 665
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->resetRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    .line 666
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->enableEsfRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 667
    return-void
.end method
