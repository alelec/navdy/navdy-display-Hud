.class Lcom/navdy/hud/app/device/gps/GpsManager$10;
.super Ljava/lang/Object;
.source "GpsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/device/gps/GpsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/device/gps/GpsManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/gps/GpsManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/device/gps/GpsManager;

    .prologue
    .line 487
    iput-object p1, p0, Lcom/navdy/hud/app/device/gps/GpsManager$10;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 491
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 492
    .local v0, "context":Landroid/content/Context;
    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v1

    .line 493
    .local v1, "handle":Landroid/os/UserHandle;
    iget-object v6, p0, Lcom/navdy/hud/app/device/gps/GpsManager$10;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->queue:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$2800(Lcom/navdy/hud/app/device/gps/GpsManager;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 494
    .local v4, "len":I
    if-lez v4, :cond_1

    .line 495
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v4, :cond_0

    .line 496
    iget-object v6, p0, Lcom/navdy/hud/app/device/gps/GpsManager$10;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->queue:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$2800(Lcom/navdy/hud/app/device/gps/GpsManager;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Intent;

    .line 497
    .local v3, "intent":Landroid/content/Intent;
    invoke-virtual {v0, v3, v1}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 498
    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "sent-queue gps event user:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 495
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 500
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_0
    iget-object v6, p0, Lcom/navdy/hud/app/device/gps/GpsManager$10;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->queue:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$2800(Lcom/navdy/hud/app/device/gps/GpsManager;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 505
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "handle":Landroid/os/UserHandle;
    .end local v2    # "i":I
    .end local v4    # "len":I
    :cond_1
    :goto_1
    return-void

    .line 502
    :catch_0
    move-exception v5

    .line 503
    .local v5, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_1
.end method
