.class Lcom/navdy/hud/app/device/gps/GpsManager$4;
.super Ljava/lang/Object;
.source "GpsManager.java"

# interfaces
.implements Landroid/location/GpsStatus$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/device/gps/GpsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/device/gps/GpsManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/gps/GpsManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/device/gps/GpsManager;

    .prologue
    .line 331
    iput-object p1, p0, Lcom/navdy/hud/app/device/gps/GpsManager$4;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGpsStatusChanged(I)V
    .locals 2
    .param p1, "event"    # I

    .prologue
    .line 334
    packed-switch p1, :pswitch_data_0

    .line 350
    :goto_0
    return-void

    .line 336
    :pswitch_0
    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "[Gps-stat] gps searching..."

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 340
    :pswitch_1
    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "[Gps-stat] gps stopped searching"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 344
    :pswitch_2
    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "[Gps-stat] gps got first fix"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 334
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
