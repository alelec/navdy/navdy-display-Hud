.class public Lcom/navdy/hud/app/device/gps/GpsNmeaParser;
.super Ljava/lang/Object;
.source "GpsNmeaParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/device/gps/GpsNmeaParser$NmeaMessage;
    }
.end annotation


# static fields
.field private static final COLON:Ljava/lang/String; = ":"

.field private static final COMMA:Ljava/lang/String; = ","

.field private static final COMMA_CHAR:C = ','

.field private static final DB:Ljava/lang/String; = "db"

.field private static final EQUAL:Ljava/lang/String; = "="

.field public static final FIX_TYPE_2D_3D:I = 0x1

.field public static final FIX_TYPE_DIFFERENTIAL:I = 0x2

.field public static final FIX_TYPE_DR:I = 0x6

.field public static final FIX_TYPE_FRTK:I = 0x5

.field public static final FIX_TYPE_NONE:I = 0x0

.field public static final FIX_TYPE_PPS:I = 0x3

.field public static final FIX_TYPE_RTK:I = 0x4

.field private static final GGA:Ljava/lang/String; = "GGA"

.field private static final GLL:Ljava/lang/String; = "GLL"

.field private static final GNS:Ljava/lang/String; = "GNS"

.field private static final GSV:Ljava/lang/String; = "GSV"

.field private static final RMC:Ljava/lang/String; = "RMC"

.field private static final SATELLITE_REPORT_INTERVAL:I = 0x1388

.field private static final SPACE:Ljava/lang/String; = " "

.field private static final SVID:Ljava/lang/String; = "SV-"

.field private static final VTG:Ljava/lang/String; = "VTG"


# instance fields
.field private fixType:I

.field private handler:Landroid/os/Handler;

.field private lastSatelliteReportTime:J

.field private messageCount:I

.field private messages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private nmeaResult:[Ljava/lang/String;

.field private sLogger:Lcom/navdy/service/library/log/Logger;

.field private satellitesSeen:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private satellitesUsed:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/navdy/service/library/log/Logger;Landroid/os/Handler;)V
    .locals 1
    .param p1, "logger"    # Lcom/navdy/service/library/log/Logger;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->satellitesSeen:Ljava/util/HashMap;

    .line 60
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->satellitesUsed:Ljava/util/HashMap;

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->messages:Ljava/util/ArrayList;

    .line 66
    const/16 v0, 0xc8

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->nmeaResult:[Ljava/lang/String;

    .line 69
    iput-object p1, p0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 70
    iput-object p2, p0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->handler:Landroid/os/Handler;

    .line 71
    return-void
.end method

.method private getGnssProviderName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 351
    const/4 v0, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 368
    const-string v0, "UNK"

    :goto_1
    return-object v0

    .line 351
    :sswitch_0
    const-string v1, "GP"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v1, "GL"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v1, "GA"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string v1, "GB"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :sswitch_4
    const-string v1, "GN"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x4

    goto :goto_0

    .line 353
    :pswitch_0
    const-string v0, "GPS"

    goto :goto_1

    .line 356
    :pswitch_1
    const-string v0, "GLONASS"

    goto :goto_1

    .line 359
    :pswitch_2
    const-string v0, "Galileo"

    goto :goto_1

    .line 362
    :pswitch_3
    const-string v0, "BeiDou"

    goto :goto_1

    .line 365
    :pswitch_4
    const-string v0, "MultiGNSS"

    goto :goto_1

    .line 351
    nop

    :sswitch_data_0
    .sparse-switch
        0x8da -> :sswitch_2
        0x8db -> :sswitch_3
        0x8e5 -> :sswitch_1
        0x8e7 -> :sswitch_4
        0x8e9 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method static getMessageType(CCC)Lcom/navdy/hud/app/device/gps/GpsNmeaParser$NmeaMessage;
    .locals 4
    .param p0, "ch1"    # C
    .param p1, "ch2"    # C
    .param p2, "ch3"    # C

    .prologue
    const/16 v3, 0x53

    const/16 v2, 0x4c

    const/16 v1, 0x47

    .line 122
    sparse-switch p0, :sswitch_data_0

    .line 154
    sget-object v0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser$NmeaMessage;->NOT_SUPPORTED:Lcom/navdy/hud/app/device/gps/GpsNmeaParser$NmeaMessage;

    :goto_0
    return-object v0

    .line 124
    :sswitch_0
    if-ne p1, v3, :cond_0

    const/16 v0, 0x56

    if-ne p2, v0, :cond_0

    .line 125
    sget-object v0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser$NmeaMessage;->GSV:Lcom/navdy/hud/app/device/gps/GpsNmeaParser$NmeaMessage;

    goto :goto_0

    .line 127
    :cond_0
    if-ne p1, v1, :cond_1

    const/16 v0, 0x41

    if-ne p2, v0, :cond_1

    .line 128
    sget-object v0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser$NmeaMessage;->GGA:Lcom/navdy/hud/app/device/gps/GpsNmeaParser$NmeaMessage;

    goto :goto_0

    .line 130
    :cond_1
    if-ne p1, v2, :cond_2

    if-ne p2, v2, :cond_2

    .line 131
    sget-object v0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser$NmeaMessage;->GLL:Lcom/navdy/hud/app/device/gps/GpsNmeaParser$NmeaMessage;

    goto :goto_0

    .line 133
    :cond_2
    const/16 v0, 0x4e

    if-ne p1, v0, :cond_3

    if-ne p2, v3, :cond_3

    .line 134
    sget-object v0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser$NmeaMessage;->GNS:Lcom/navdy/hud/app/device/gps/GpsNmeaParser$NmeaMessage;

    goto :goto_0

    .line 136
    :cond_3
    sget-object v0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser$NmeaMessage;->NOT_SUPPORTED:Lcom/navdy/hud/app/device/gps/GpsNmeaParser$NmeaMessage;

    goto :goto_0

    .line 140
    :sswitch_1
    const/16 v0, 0x4d

    if-ne p1, v0, :cond_4

    const/16 v0, 0x43

    if-ne p2, v0, :cond_4

    .line 141
    sget-object v0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser$NmeaMessage;->RMC:Lcom/navdy/hud/app/device/gps/GpsNmeaParser$NmeaMessage;

    goto :goto_0

    .line 143
    :cond_4
    sget-object v0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser$NmeaMessage;->NOT_SUPPORTED:Lcom/navdy/hud/app/device/gps/GpsNmeaParser$NmeaMessage;

    goto :goto_0

    .line 147
    :sswitch_2
    const/16 v0, 0x54

    if-ne p1, v0, :cond_5

    if-ne p2, v1, :cond_5

    .line 148
    sget-object v0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser$NmeaMessage;->VTG:Lcom/navdy/hud/app/device/gps/GpsNmeaParser$NmeaMessage;

    goto :goto_0

    .line 150
    :cond_5
    sget-object v0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser$NmeaMessage;->NOT_SUPPORTED:Lcom/navdy/hud/app/device/gps/GpsNmeaParser$NmeaMessage;

    goto :goto_0

    .line 122
    nop

    :sswitch_data_0
    .sparse-switch
        0x47 -> :sswitch_0
        0x52 -> :sswitch_1
        0x56 -> :sswitch_2
    .end sparse-switch
.end method

.method static parseData(Ljava/lang/String;[Ljava/lang/String;)I
    .locals 7
    .param p0, "nmea"    # Ljava/lang/String;
    .param p1, "result"    # [Ljava/lang/String;

    .prologue
    .line 292
    const/4 v2, 0x0

    .line 293
    .local v2, "resultIndex":I
    const/4 v4, 0x0

    .line 294
    .local v4, "start":I
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .line 296
    .local v0, "ch":[C
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v5, v0

    if-ge v1, v5, :cond_1

    .line 297
    aget-char v5, v0, v1

    const/16 v6, 0x2c

    if-ne v5, v6, :cond_0

    .line 298
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "resultIndex":I
    .local v3, "resultIndex":I
    new-instance v5, Ljava/lang/String;

    sub-int v6, v1, v4

    invoke-direct {v5, v0, v4, v6}, Ljava/lang/String;-><init>([CII)V

    aput-object v5, p1, v2

    .line 299
    add-int/lit8 v4, v1, 0x1

    move v2, v3

    .line 296
    .end local v3    # "resultIndex":I
    .restart local v2    # "resultIndex":I
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 303
    :cond_1
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "resultIndex":I
    .restart local v3    # "resultIndex":I
    new-instance v5, Ljava/lang/String;

    sub-int v6, v1, v4

    add-int/lit8 v6, v6, -0x2

    invoke-direct {v5, v0, v4, v6}, Ljava/lang/String;-><init>([CII)V

    aput-object v5, p1, v2

    .line 304
    return v3
.end method

.method private processGGA(Ljava/lang/String;)V
    .locals 4
    .param p1, "nmea"    # Ljava/lang/String;

    .prologue
    .line 165
    iget-object v2, p0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->nmeaResult:[Ljava/lang/String;

    invoke-static {p1, v2}, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->parseData(Ljava/lang/String;[Ljava/lang/String;)I

    .line 166
    iget-object v2, p0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->nmeaResult:[Ljava/lang/String;

    const/4 v3, 0x6

    aget-object v0, v2, v3

    .line 168
    .local v0, "quality":Ljava/lang/String;
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->fixType:I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 172
    :goto_0
    return-void

    .line 169
    :catch_0
    move-exception v1

    .line 170
    .local v1, "t":Ljava/lang/Throwable;
    const/4 v2, 0x0

    iput v2, p0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->fixType:I

    goto :goto_0
.end method

.method private processGLL(Ljava/lang/String;)V
    .locals 0
    .param p1, "nmea"    # Ljava/lang/String;

    .prologue
    .line 179
    return-void
.end method

.method private processGNS(Ljava/lang/String;)V
    .locals 8
    .param p1, "nmea"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 184
    iget-object v4, p0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->nmeaResult:[Ljava/lang/String;

    invoke-static {p1, v4}, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->parseData(Ljava/lang/String;[Ljava/lang/String;)I

    .line 185
    iget-object v4, p0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->nmeaResult:[Ljava/lang/String;

    aget-object v4, v4, v7

    const/4 v5, 0x1

    const/4 v6, 0x3

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 186
    .local v0, "gnsType":Ljava/lang/String;
    iget-object v4, p0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->nmeaResult:[Ljava/lang/String;

    const/4 v5, 0x2

    aget-object v1, v4, v5

    .line 187
    .local v1, "lat":Ljava/lang/String;
    iget-object v4, p0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->nmeaResult:[Ljava/lang/String;

    const/4 v5, 0x4

    aget-object v2, v4, v5

    .line 188
    .local v2, "lng":Ljava/lang/String;
    iget-object v4, p0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->nmeaResult:[Ljava/lang/String;

    const/4 v5, 0x7

    aget-object v3, v4, v5

    .line 190
    .local v3, "satellites":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 191
    :cond_0
    iget-object v4, p0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->satellitesUsed:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    :goto_0
    return-void

    .line 193
    :cond_1
    iget-object v4, p0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->satellitesUsed:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private processGSV(Ljava/lang/String;)V
    .locals 26
    .param p1, "nmea"    # Ljava/lang/String;

    .prologue
    .line 209
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->nmeaResult:[Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->parseData(Ljava/lang/String;[Ljava/lang/String;)I

    move-result v10

    .line 210
    .local v10, "elements":I
    const/16 v23, 0x8

    move/from16 v0, v23

    if-ge v10, v0, :cond_1

    .line 289
    :cond_0
    :goto_0
    return-void

    .line 213
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->nmeaResult:[Ljava/lang/String;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aget-object v23, v23, v24

    const/16 v24, 0x1

    const/16 v25, 0x3

    invoke-virtual/range {v23 .. v25}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    .line 214
    .local v13, "gnsType":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->nmeaResult:[Ljava/lang/String;

    move-object/from16 v23, v0

    const/16 v24, 0x1

    aget-object v23, v23, v24

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v18

    .line 215
    .local v18, "messageLen":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->nmeaResult:[Ljava/lang/String;

    move-object/from16 v23, v0

    const/16 v24, 0x2

    aget-object v23, v23, v24

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v17

    .line 217
    .local v17, "messageId":I
    const/16 v23, 0x1

    move/from16 v0, v17

    move/from16 v1, v23

    if-ne v0, v1, :cond_3

    .line 219
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->messages:Ljava/util/ArrayList;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->clear()V

    .line 220
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->messageCount:I

    .line 228
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->messages:Ljava/util/ArrayList;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 230
    move-object/from16 v0, p0

    iget v0, v0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->messageCount:I

    move/from16 v23, v0

    move/from16 v0, v17

    move/from16 v1, v23

    if-ne v0, v1, :cond_0

    .line 234
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->satellitesSeen:Ljava/util/HashMap;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/HashMap;

    .line 235
    .local v7, "data":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    if-nez v7, :cond_4

    .line 236
    new-instance v7, Ljava/util/HashMap;

    .end local v7    # "data":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 237
    .restart local v7    # "data":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->satellitesSeen:Ljava/util/HashMap;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v13, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->messages:Ljava/util/ArrayList;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    move-result v16

    .line 243
    .local v16, "len":I
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_2
    move/from16 v0, v16

    if-ge v14, v0, :cond_9

    .line 244
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->messages:Ljava/util/ArrayList;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->nmeaResult:[Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-static/range {v23 .. v24}, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->parseData(Ljava/lang/String;[Ljava/lang/String;)I

    move-result v10

    .line 246
    const/4 v15, 0x4

    .local v15, "j":I
    :goto_3
    add-int/lit8 v23, v15, 0x4

    move/from16 v0, v23

    if-ge v0, v10, :cond_8

    .line 247
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->nmeaResult:[Ljava/lang/String;

    move-object/from16 v23, v0

    aget-object v22, v23, v15

    .line 248
    .local v22, "satelliteId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->nmeaResult:[Ljava/lang/String;

    move-object/from16 v23, v0

    add-int/lit8 v24, v15, 0x1

    aget-object v11, v23, v24

    .line 249
    .local v11, "elevation":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->nmeaResult:[Ljava/lang/String;

    move-object/from16 v23, v0

    add-int/lit8 v24, v15, 0x2

    aget-object v4, v23, v24

    .line 250
    .local v4, "azmuth":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->nmeaResult:[Ljava/lang/String;

    move-object/from16 v23, v0

    add-int/lit8 v24, v15, 0x3

    aget-object v5, v23, v24

    .line 252
    .local v5, "cNO":Ljava/lang/String;
    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_5

    .line 246
    :goto_4
    add-int/lit8 v15, v15, 0x4

    goto :goto_3

    .line 222
    .end local v4    # "azmuth":Ljava/lang/String;
    .end local v5    # "cNO":Ljava/lang/String;
    .end local v7    # "data":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .end local v11    # "elevation":Ljava/lang/String;
    .end local v14    # "i":I
    .end local v15    # "j":I
    .end local v16    # "len":I
    .end local v22    # "satelliteId":Ljava/lang/String;
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->messages:Ljava/util/ArrayList;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    move-result v23

    if-nez v23, :cond_2

    goto/16 :goto_0

    .line 239
    .restart local v7    # "data":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :cond_4
    invoke-virtual {v7}, Ljava/util/HashMap;->clear()V

    goto :goto_1

    .line 256
    .restart local v4    # "azmuth":Ljava/lang/String;
    .restart local v5    # "cNO":Ljava/lang/String;
    .restart local v11    # "elevation":Ljava/lang/String;
    .restart local v14    # "i":I
    .restart local v15    # "j":I
    .restart local v16    # "len":I
    .restart local v22    # "satelliteId":Ljava/lang/String;
    :cond_5
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_6

    .line 257
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_6

    .line 258
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_7

    .line 259
    :cond_6
    move-object/from16 v0, v22

    invoke-virtual {v7, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 265
    :cond_7
    :try_start_0
    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v19

    .line 272
    .local v19, "nSatelliteId":I
    :try_start_1
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v6

    .line 276
    .local v6, "cNOVal":I
    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v7, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 266
    .end local v6    # "cNOVal":I
    .end local v19    # "nSatelliteId":I
    :catch_0
    move-exception v12

    .line 267
    .local v12, "ex":Ljava/lang/NumberFormatException;
    goto :goto_4

    .line 273
    .end local v12    # "ex":Ljava/lang/NumberFormatException;
    .restart local v19    # "nSatelliteId":I
    :catch_1
    move-exception v12

    .line 274
    .restart local v12    # "ex":Ljava/lang/NumberFormatException;
    goto :goto_4

    .line 243
    .end local v4    # "azmuth":Ljava/lang/String;
    .end local v5    # "cNO":Ljava/lang/String;
    .end local v11    # "elevation":Ljava/lang/String;
    .end local v12    # "ex":Ljava/lang/NumberFormatException;
    .end local v19    # "nSatelliteId":I
    .end local v22    # "satelliteId":Ljava/lang/String;
    :cond_8
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_2

    .line 280
    .end local v15    # "j":I
    :cond_9
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->messageCount:I

    .line 281
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->messages:Ljava/util/ArrayList;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->clear()V

    .line 283
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v20

    .line 284
    .local v20, "now":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->lastSatelliteReportTime:J

    move-wide/from16 v24, v0

    sub-long v8, v20, v24

    .line 285
    .local v8, "diff":J
    const-wide/16 v24, 0x1388

    cmp-long v23, v8, v24

    if-ltz v23, :cond_0

    .line 286
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->sendGpsSatelliteStatus()V

    .line 287
    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->lastSatelliteReportTime:J

    goto/16 :goto_0
.end method

.method private processRMC(Ljava/lang/String;)V
    .locals 0
    .param p1, "nmea"    # Ljava/lang/String;

    .prologue
    .line 201
    return-void
.end method

.method private processVTG(Ljava/lang/String;)V
    .locals 0
    .param p1, "nmea"    # Ljava/lang/String;

    .prologue
    .line 205
    return-void
.end method

.method private sendGpsSatelliteStatus()V
    .locals 19

    .prologue
    .line 308
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 309
    .local v2, "bundle":Landroid/os/Bundle;
    const/4 v4, 0x0

    .line 310
    .local v4, "counter":I
    const/4 v10, 0x0

    .line 312
    .local v10, "maxDB":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->satellitesSeen:Ljava/util/HashMap;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .line 313
    .local v8, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Integer;>;>;>;"
    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_2

    .line 314
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    .line 315
    .local v6, "data":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Integer;>;>;"
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 316
    .local v9, "key":Ljava/lang/String;
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/util/HashMap;

    .line 317
    .local v15, "val":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-virtual {v15}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .line 318
    .local v16, "valIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;>;"
    :cond_1
    :goto_0
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_0

    .line 319
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/util/Map$Entry;

    .line 320
    .local v13, "sData":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-interface {v13}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Integer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 321
    .local v7, "id":I
    invoke-interface {v13}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Integer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 322
    .local v3, "cno":I
    add-int/lit8 v4, v4, 0x1

    .line 323
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "SAT_PROVIDER_"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v2, v0, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "SAT_ID_"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v2, v0, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 325
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "SAT_DB_"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 326
    if-le v3, v10, :cond_1

    .line 327
    move v10, v3

    goto :goto_0

    .line 332
    .end local v3    # "cno":I
    .end local v6    # "data":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Integer;>;>;"
    .end local v7    # "id":I
    .end local v9    # "key":Ljava/lang/String;
    .end local v13    # "sData":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .end local v15    # "val":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .end local v16    # "valIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;>;"
    :cond_2
    const-string v17, "SAT_SEEN"

    move-object/from16 v0, v17

    invoke-virtual {v2, v0, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 333
    const-string v17, "SAT_MAX_DB"

    move-object/from16 v0, v17

    invoke-virtual {v2, v0, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 335
    const/4 v12, 0x0

    .line 336
    .local v12, "nSatellitesUsed":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->satellitesUsed:Ljava/util/HashMap;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .line 337
    .local v14, "usedIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_3

    .line 338
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 339
    .local v5, "data":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Integer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v17

    add-int v12, v12, v17

    .line 340
    goto :goto_1

    .line 342
    .end local v5    # "data":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    :cond_3
    const-string v17, "SAT_USED"

    move-object/from16 v0, v17

    invoke-virtual {v2, v0, v12}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 343
    const-string v17, "SAT_FIX_TYPE"

    move-object/from16 v0, p0

    iget v0, v0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->fixType:I

    move/from16 v18, v0

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 345
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->handler:Landroid/os/Handler;

    move-object/from16 v17, v0

    const/16 v18, 0x2

    invoke-virtual/range {v17 .. v18}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v11

    .line 346
    .local v11, "msg":Landroid/os/Message;
    iput-object v2, v11, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 347
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->handler:Landroid/os/Handler;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 348
    return-void
.end method


# virtual methods
.method public parseNmeaMessage(Ljava/lang/String;)V
    .locals 6
    .param p1, "nmea"    # Ljava/lang/String;

    .prologue
    .line 74
    if-nez p1, :cond_1

    .line 118
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 77
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    .line 82
    .local v1, "nmeaChars":[C
    array-length v3, v1

    const/4 v4, 0x7

    if-lt v3, v4, :cond_0

    const/4 v3, 0x0

    aget-char v3, v1, v3

    const/16 v4, 0x24

    if-ne v3, v4, :cond_0

    .line 86
    const/4 v3, 0x3

    aget-char v3, v1, v3

    const/4 v4, 0x4

    aget-char v4, v1, v4

    const/4 v5, 0x5

    aget-char v5, v1, v5

    invoke-static {v3, v4, v5}, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->getMessageType(CCC)Lcom/navdy/hud/app/device/gps/GpsNmeaParser$NmeaMessage;

    move-result-object v2

    .line 87
    .local v2, "nmeaType":Lcom/navdy/hud/app/device/gps/GpsNmeaParser$NmeaMessage;
    sget-object v3, Lcom/navdy/hud/app/device/gps/GpsNmeaParser$NmeaMessage;->NOT_SUPPORTED:Lcom/navdy/hud/app/device/gps/GpsNmeaParser$NmeaMessage;

    if-eq v2, v3, :cond_0

    .line 90
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v3, p0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->nmeaResult:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 91
    iget-object v3, p0, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->nmeaResult:[Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v4, v3, v0

    .line 90
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 93
    :cond_2
    sget-object v3, Lcom/navdy/hud/app/device/gps/GpsNmeaParser$1;->$SwitchMap$com$navdy$hud$app$device$gps$GpsNmeaParser$NmeaMessage:[I

    invoke-virtual {v2}, Lcom/navdy/hud/app/device/gps/GpsNmeaParser$NmeaMessage;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 95
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->processGGA(Ljava/lang/String;)V

    goto :goto_0

    .line 103
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->processGNS(Ljava/lang/String;)V

    goto :goto_0

    .line 115
    :pswitch_3
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/device/gps/GpsNmeaParser;->processGSV(Ljava/lang/String;)V

    goto :goto_0

    .line 93
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
