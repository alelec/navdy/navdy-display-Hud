.class public Lcom/navdy/hud/app/device/gps/RawSensorData;
.super Ljava/lang/Object;
.source "RawSensorData.java"


# static fields
.field private static final ACCEL_SCALE_FACTOR:F

.field public static final ACCEL_X:I = 0x10

.field public static final ACCEL_Y:I = 0x11

.field public static final ACCEL_Z:I = 0x12

.field private static final GYRO_SCALE_FACTOR:F

.field public static final GYRO_TEMP:I = 0xc

.field public static final GYRO_X:I = 0xd

.field public static final GYRO_Y:I = 0xe

.field public static final GYRO_Z:I = 0x5

.field public static final MAX_ACCEL:F = 2.0f


# instance fields
.field public final x:F

.field public final y:F

.field public final z:F


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    .line 22
    const-wide/high16 v0, -0x3fdc000000000000L    # -10.0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, Lcom/navdy/hud/app/device/gps/RawSensorData;->ACCEL_SCALE_FACTOR:F

    .line 23
    const-wide/high16 v0, -0x3fd8000000000000L    # -12.0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, Lcom/navdy/hud/app/device/gps/RawSensorData;->GYRO_SCALE_FACTOR:F

    return-void
.end method

.method public constructor <init>([BI)V
    .locals 8
    .param p1, "rawMessage"    # [B
    .param p2, "offset"    # I

    .prologue
    const v7, 0x411ccccd    # 9.8f

    const/high16 v6, 0x40000000    # 2.0f

    const/high16 v5, -0x40000000    # -2.0f

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    add-int/lit8 v3, p2, 0x4

    array-length v4, p1

    sub-int/2addr v4, p2

    add-int/lit8 v4, v4, -0x4

    invoke-static {p1, v3, v4}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 33
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    sget-object v3, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 34
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v3

    add-int/lit8 v3, v3, -0x4

    div-int/lit8 v1, v3, 0x8

    .line 35
    .local v1, "samples":I
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->mark()Ljava/nio/Buffer;

    .line 37
    const/16 v3, 0x10

    sget v4, Lcom/navdy/hud/app/device/gps/RawSensorData;->ACCEL_SCALE_FACTOR:F

    invoke-direct {p0, v0, v1, v3, v4}, Lcom/navdy/hud/app/device/gps/RawSensorData;->average(Ljava/nio/ByteBuffer;IIF)F

    move-result v3

    div-float v2, v3, v7

    .line 38
    .local v2, "tmp":F
    invoke-direct {p0, v2, v5, v6}, Lcom/navdy/hud/app/device/gps/RawSensorData;->clamp(FFF)F

    move-result v3

    iput v3, p0, Lcom/navdy/hud/app/device/gps/RawSensorData;->x:F

    .line 40
    const/16 v3, 0x11

    sget v4, Lcom/navdy/hud/app/device/gps/RawSensorData;->ACCEL_SCALE_FACTOR:F

    invoke-direct {p0, v0, v1, v3, v4}, Lcom/navdy/hud/app/device/gps/RawSensorData;->average(Ljava/nio/ByteBuffer;IIF)F

    move-result v3

    div-float v2, v3, v7

    .line 41
    invoke-direct {p0, v2, v5, v6}, Lcom/navdy/hud/app/device/gps/RawSensorData;->clamp(FFF)F

    move-result v3

    iput v3, p0, Lcom/navdy/hud/app/device/gps/RawSensorData;->y:F

    .line 43
    const/16 v3, 0x12

    sget v4, Lcom/navdy/hud/app/device/gps/RawSensorData;->ACCEL_SCALE_FACTOR:F

    invoke-direct {p0, v0, v1, v3, v4}, Lcom/navdy/hud/app/device/gps/RawSensorData;->average(Ljava/nio/ByteBuffer;IIF)F

    move-result v3

    div-float v2, v3, v7

    .line 44
    invoke-direct {p0, v2, v5, v6}, Lcom/navdy/hud/app/device/gps/RawSensorData;->clamp(FFF)F

    move-result v3

    iput v3, p0, Lcom/navdy/hud/app/device/gps/RawSensorData;->z:F

    .line 45
    return-void
.end method

.method private average(Ljava/nio/ByteBuffer;IIF)F
    .locals 9
    .param p1, "buffer"    # Ljava/nio/ByteBuffer;
    .param p2, "samples"    # I
    .param p3, "type"    # I
    .param p4, "scaleFactor"    # F

    .prologue
    .line 57
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->reset()Ljava/nio/Buffer;

    .line 58
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    .line 59
    .local v3, "reserved":I
    const/4 v0, 0x0

    .line 60
    .local v0, "count":I
    const/4 v5, 0x0

    .line 61
    .local v5, "sum":F
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, p2, :cond_2

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v7

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v8

    if-ge v7, v8, :cond_2

    .line 62
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    .line 63
    .local v1, "data":I
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v6

    .line 64
    .local v6, "timeStamp":I
    shr-int/lit8 v4, v1, 0x18

    .line 65
    .local v4, "sampleType":I
    if-ne v4, p3, :cond_1

    .line 66
    const v7, 0xffffff

    and-int/2addr v1, v7

    .line 67
    const/high16 v7, 0x800000

    and-int/2addr v7, v1

    if-eqz v7, :cond_0

    .line 69
    const/high16 v7, -0x1000000

    or-int/2addr v1, v7

    .line 71
    :cond_0
    int-to-float v7, v1

    mul-float/2addr v7, p4

    add-float/2addr v5, v7

    .line 72
    add-int/lit8 v0, v0, 0x1

    .line 61
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 75
    .end local v1    # "data":I
    .end local v4    # "sampleType":I
    .end local v6    # "timeStamp":I
    :cond_2
    if-lez v0, :cond_3

    int-to-float v7, v0

    div-float v7, v5, v7

    :goto_1
    return v7

    :cond_3
    const/4 v7, 0x0

    goto :goto_1
.end method

.method private clamp(FFF)F
    .locals 1
    .param p1, "val"    # F
    .param p2, "min"    # F
    .param p3, "max"    # F

    .prologue
    .line 48
    cmpg-float v0, p1, p2

    if-gez v0, :cond_1

    .line 49
    move p1, p2

    .line 53
    :cond_0
    :goto_0
    return p1

    .line 50
    :cond_1
    cmpl-float v0, p1, p3

    if-lez v0, :cond_0

    .line 51
    move p1, p3

    goto :goto_0
.end method
