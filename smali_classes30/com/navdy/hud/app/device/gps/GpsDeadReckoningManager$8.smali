.class Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$8;
.super Ljava/lang/Object;
.source "GpsDeadReckoningManager.java"

# interfaces
.implements Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$CommandWriter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->invokeUblox([BLjava/lang/String;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

.field final synthetic val$description:Ljava/lang/String;

.field final synthetic val$message:[B


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;Ljava/lang/String;[B)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    .prologue
    .line 353
    iput-object p1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$8;->this$0:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    iput-object p2, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$8;->val$description:Ljava/lang/String;

    iput-object p3, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$8;->val$message:[B

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 356
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$8;->this$0:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->outputStream:Ljava/io/OutputStream;
    invoke-static {v0}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->access$1900(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)Ljava/io/OutputStream;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 357
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$8;->val$description:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 358
    # getter for: Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->access$1000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$8;->val$description:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$8;->val$message:[B

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$8;->val$message:[B

    array-length v4, v4

    # invokes: Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->bytesToHex([BII)Ljava/lang/String;
    invoke-static {v2, v3, v4}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->access$2000([BII)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 360
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$8;->this$0:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->outputStream:Ljava/io/OutputStream;
    invoke-static {v0}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->access$1900(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)Ljava/io/OutputStream;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$8;->val$message:[B

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    .line 364
    return-void

    .line 362
    :cond_1
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Disconnected socket - failed to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$8;->val$description:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
