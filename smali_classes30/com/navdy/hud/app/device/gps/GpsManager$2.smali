.class Lcom/navdy/hud/app/device/gps/GpsManager$2;
.super Ljava/lang/Object;
.source "GpsManager.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/device/gps/GpsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/device/gps/GpsManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/gps/GpsManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/device/gps/GpsManager;

    .prologue
    .line 181
    iput-object p1, p0, Lcom/navdy/hud/app/device/gps/GpsManager$2;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .locals 14
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    const/high16 v8, -0x40800000    # -1.0f

    .line 185
    :try_start_0
    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v7

    const/4 v9, 0x2

    invoke-virtual {v7, v9}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 186
    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v9

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[Gps-loc] "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p1}, Landroid/location/Location;->isFromMockProvider()Z

    move-result v7

    if-eqz v7, :cond_2

    const-string v7, "[Mock] "

    :goto_0
    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v9, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 189
    :cond_0
    iget-object v7, p0, Lcom/navdy/hud/app/device/gps/GpsManager$2;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->startUbloxCalled:Z
    invoke-static {v7}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$200(Lcom/navdy/hud/app/device/gps/GpsManager;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 223
    :cond_1
    :goto_1
    return-void

    .line 186
    :cond_2
    const-string v7, ""

    goto :goto_0

    .line 194
    :cond_3
    invoke-virtual {p1}, Landroid/location/Location;->isFromMockProvider()Z

    move-result v7

    if-nez v7, :cond_1

    .line 195
    iget-object v7, p0, Lcom/navdy/hud/app/device/gps/GpsManager$2;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->activeLocationSource:Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;
    invoke-static {v7}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$300(Lcom/navdy/hud/app/device/gps/GpsManager;)Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    move-result-object v7

    sget-object v9, Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;->UBLOX:Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    if-eq v7, v9, :cond_1

    .line 196
    iget-object v7, p0, Lcom/navdy/hud/app/device/gps/GpsManager$2;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->activeLocationSource:Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;
    invoke-static {v7}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$300(Lcom/navdy/hud/app/device/gps/GpsManager;)Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    move-result-object v1

    .line 197
    .local v1, "last":Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;
    iget-object v7, p0, Lcom/navdy/hud/app/device/gps/GpsManager$2;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    sget-object v9, Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;->UBLOX:Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    # setter for: Lcom/navdy/hud/app/device/gps/GpsManager;->activeLocationSource:Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;
    invoke-static {v7, v9}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$302(Lcom/navdy/hud/app/device/gps/GpsManager;Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;)Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    .line 198
    iget-object v7, p0, Lcom/navdy/hud/app/device/gps/GpsManager$2;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->handler:Landroid/os/Handler;
    invoke-static {v7}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$500(Lcom/navdy/hud/app/device/gps/GpsManager;)Landroid/os/Handler;

    move-result-object v7

    iget-object v9, p0, Lcom/navdy/hud/app/device/gps/GpsManager$2;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->noPhoneLocationFixRunnable:Ljava/lang/Runnable;
    invoke-static {v9}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$400(Lcom/navdy/hud/app/device/gps/GpsManager;)Ljava/lang/Runnable;

    move-result-object v9

    invoke-virtual {v7, v9}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 199
    iget-object v7, p0, Lcom/navdy/hud/app/device/gps/GpsManager$2;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->handler:Landroid/os/Handler;
    invoke-static {v7}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$500(Lcom/navdy/hud/app/device/gps/GpsManager;)Landroid/os/Handler;

    move-result-object v7

    iget-object v9, p0, Lcom/navdy/hud/app/device/gps/GpsManager$2;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->locationFixRunnable:Ljava/lang/Runnable;
    invoke-static {v9}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$600(Lcom/navdy/hud/app/device/gps/GpsManager;)Ljava/lang/Runnable;

    move-result-object v9

    invoke-virtual {v7, v9}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 200
    iget-object v7, p0, Lcom/navdy/hud/app/device/gps/GpsManager$2;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->handler:Landroid/os/Handler;
    invoke-static {v7}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$500(Lcom/navdy/hud/app/device/gps/GpsManager;)Landroid/os/Handler;

    move-result-object v7

    iget-object v9, p0, Lcom/navdy/hud/app/device/gps/GpsManager$2;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->locationFixRunnable:Ljava/lang/Runnable;
    invoke-static {v9}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$600(Lcom/navdy/hud/app/device/gps/GpsManager;)Ljava/lang/Runnable;

    move-result-object v9

    const-wide/16 v10, 0x3e8

    invoke-virtual {v7, v9, v10, v11}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 201
    iget-object v7, p0, Lcom/navdy/hud/app/device/gps/GpsManager$2;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # invokes: Lcom/navdy/hud/app/device/gps/GpsManager;->stopSendingLocation()V
    invoke-static {v7}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$700(Lcom/navdy/hud/app/device/gps/GpsManager;)V

    .line 203
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    iget-object v7, p0, Lcom/navdy/hud/app/device/gps/GpsManager$2;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->gpsLastEventTime:J
    invoke-static {v7}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$800(Lcom/navdy/hud/app/device/gps/GpsManager;)J

    move-result-wide v12

    sub-long/2addr v10, v12

    long-to-int v7, v10

    div-int/lit16 v5, v7, 0x3e8

    .line 204
    .local v5, "second":I
    const/high16 v4, -0x40800000    # -1.0f

    .line 205
    .local v4, "phoneAccuracy":F
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    iget-object v7, p0, Lcom/navdy/hud/app/device/gps/GpsManager$2;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->lastPhoneCoordinateTime:J
    invoke-static {v7}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$900(Lcom/navdy/hud/app/device/gps/GpsManager;)J

    move-result-wide v12

    sub-long v2, v10, v12

    .line 206
    .local v2, "elapsedTime":J
    iget-object v7, p0, Lcom/navdy/hud/app/device/gps/GpsManager$2;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->lastPhoneCoordinate:Lcom/navdy/service/library/events/location/Coordinate;
    invoke-static {v7}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$1000(Lcom/navdy/hud/app/device/gps/GpsManager;)Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v7

    if-eqz v7, :cond_4

    const-wide/16 v10, 0xbb8

    cmp-long v7, v2, v10

    if-gez v7, :cond_4

    .line 207
    iget-object v7, p0, Lcom/navdy/hud/app/device/gps/GpsManager$2;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->lastPhoneCoordinate:Lcom/navdy/service/library/events/location/Coordinate;
    invoke-static {v7}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$1000(Lcom/navdy/hud/app/device/gps/GpsManager;)Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v7

    iget-object v7, v7, Lcom/navdy/service/library/events/location/Coordinate;->accuracy:Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v4

    .line 209
    :cond_4
    iget-object v7, p0, Lcom/navdy/hud/app/device/gps/GpsManager$2;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->lastUbloxCoordinate:Landroid/location/Location;
    invoke-static {v7}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$1100(Lcom/navdy/hud/app/device/gps/GpsManager;)Landroid/location/Location;

    move-result-object v7

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/navdy/hud/app/device/gps/GpsManager$2;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->lastUbloxCoordinate:Landroid/location/Location;
    invoke-static {v7}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$1100(Lcom/navdy/hud/app/device/gps/GpsManager;)Landroid/location/Location;

    move-result-object v7

    invoke-virtual {v7}, Landroid/location/Location;->getAccuracy()F

    move-result v7

    :goto_2
    float-to-int v0, v7

    .line 210
    .local v0, "accuracy":I
    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    .line 211
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    .line 210
    invoke-static {v7, v9}, Lcom/navdy/hud/app/service/ConnectionServiceAnalyticsSupport;->recordGpsAcquireLocation(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    iget-object v7, p0, Lcom/navdy/hud/app/device/gps/GpsManager$2;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    # setter for: Lcom/navdy/hud/app/device/gps/GpsManager;->gpsLastEventTime:J
    invoke-static {v7, v10, v11}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$802(Lcom/navdy/hud/app/device/gps/GpsManager;J)J

    .line 214
    iget-object v9, p0, Lcom/navdy/hud/app/device/gps/GpsManager$2;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    const-string v10, "Switched to Ublox Gps"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Phone ="

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    cmpl-float v7, v4, v8

    if-nez v7, :cond_6

    const-string v7, "n/a"

    :goto_3
    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v11, " ublox ="

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v11, 0x0

    const/4 v12, 0x1

    # invokes: Lcom/navdy/hud/app/device/gps/GpsManager;->markLocationFix(Ljava/lang/String;Ljava/lang/String;ZZ)V
    invoke-static {v9, v10, v7, v11, v12}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$1200(Lcom/navdy/hud/app/device/gps/GpsManager;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 216
    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v9

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[Gps-loc] [LocationProvider] switched from ["

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, "] to ["

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v10, Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;->UBLOX:Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, "] "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, "Phone ="

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    cmpl-float v7, v4, v8

    if-nez v7, :cond_7

    const-string v7, "n/a"

    .line 217
    :goto_4
    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " ublox ="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " time="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 216
    invoke-virtual {v9, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 220
    .end local v0    # "accuracy":I
    .end local v1    # "last":Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;
    .end local v2    # "elapsedTime":J
    .end local v4    # "phoneAccuracy":F
    .end local v5    # "second":I
    :catch_0
    move-exception v6

    .line 221
    .local v6, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .end local v6    # "t":Ljava/lang/Throwable;
    .restart local v1    # "last":Lcom/navdy/hud/app/device/gps/GpsManager$LocationSource;
    .restart local v2    # "elapsedTime":J
    .restart local v4    # "phoneAccuracy":F
    .restart local v5    # "second":I
    :cond_5
    move v7, v8

    .line 209
    goto/16 :goto_2

    .line 214
    .restart local v0    # "accuracy":I
    :cond_6
    :try_start_1
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    goto :goto_3

    .line 217
    :cond_7
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v7

    goto :goto_4
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 3
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 239
    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Gps-stat] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "[disabled]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 240
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 3
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 234
    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Gps-stat] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "[enabled]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 235
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 3
    .param p1, "provider"    # Ljava/lang/String;
    .param p2, "status"    # I
    .param p3, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 227
    invoke-static {}, Lcom/navdy/hud/app/device/PowerManager;->isAwake()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 228
    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Gps-stat] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 230
    :cond_0
    return-void
.end method
