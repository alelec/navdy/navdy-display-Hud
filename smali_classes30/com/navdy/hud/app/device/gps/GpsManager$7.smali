.class Lcom/navdy/hud/app/device/gps/GpsManager$7;
.super Ljava/lang/Object;
.source "GpsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/device/gps/GpsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/device/gps/GpsManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/gps/GpsManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/device/gps/GpsManager;

    .prologue
    .line 392
    iput-object p1, p0, Lcom/navdy/hud/app/device/gps/GpsManager$7;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 396
    :try_start_0
    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "issuing warm reset to ublox, no location report in last "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->WARM_RESET_INTERVAL:J
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$2200()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 397
    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsManager$7;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    const-string v2, "GPS_WARM_RESET_UBLOX"

    const/4 v3, 0x0

    # invokes: Lcom/navdy/hud/app/device/gps/GpsManager;->sendGpsEventBroadcast(Ljava/lang/String;Landroid/os/Bundle;)V
    invoke-static {v1, v2, v3}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$2300(Lcom/navdy/hud/app/device/gps/GpsManager;Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 401
    :goto_0
    return-void

    .line 398
    :catch_0
    move-exception v0

    .line 399
    .local v0, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
