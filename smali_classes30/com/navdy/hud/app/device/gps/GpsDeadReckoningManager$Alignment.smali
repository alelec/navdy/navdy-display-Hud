.class Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$Alignment;
.super Ljava/lang/Object;
.source "GpsDeadReckoningManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Alignment"
.end annotation


# instance fields
.field done:Z

.field pitch:F

.field roll:F

.field yaw:F


# direct methods
.method constructor <init>(FFFZ)V
    .locals 0
    .param p1, "yaw"    # F
    .param p2, "pitch"    # F
    .param p3, "roll"    # F
    .param p4, "done"    # Z

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput p1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$Alignment;->yaw:F

    .line 62
    iput p2, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$Alignment;->pitch:F

    .line 63
    iput p3, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$Alignment;->roll:F

    .line 64
    iput-boolean p4, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$Alignment;->done:Z

    .line 65
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Alignment{"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 75
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "yaw="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$Alignment;->yaw:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 76
    const-string v1, ", pitch="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$Alignment;->pitch:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 77
    const-string v1, ", roll="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$Alignment;->roll:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 78
    const-string v1, ", done="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$Alignment;->done:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 79
    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 80
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
