.class public Lcom/navdy/hud/app/device/gps/GpsConstants;
.super Ljava/lang/Object;
.source "GpsConstants.java"


# static fields
.field public static final ACCURACY_REPORTING_INTERVAL:I = 0x493e0

.field public static final DEAD_RECKONING_ONLY:Ljava/lang/String; = "DEAD_RECKONING_ONLY"

.field public static final DEBUG_TTS_PHONE_SWITCH:Ljava/lang/String; = "Switched to Phone Gps"

.field public static final DEBUG_TTS_UBLOX_SWITCH:Ljava/lang/String; = "Switched to Ublox Gps"

.field public static final DRIVE_LOGS_FOLDER:Ljava/lang/String; = "drive_logs"

.field public static final EMPTY:Ljava/lang/String; = ""

.field public static final EXTRAPOLATION:Ljava/lang/String; = "EXTRAPOLATION"

.field public static final EXTRAPOLATION_EXPIRY_INTERVAL:I = 0x1388

.field public static final EXTRAPOLATION_FLAG:Ljava/lang/String; = "EXTRAPOLATION_FLAG"

.field public static final GPS_COLLECT_LOGS:Ljava/lang/String; = "GPS_COLLECT_LOGS"

.field public static final GPS_DEAD_RECKONING_COMBINED:Ljava/lang/String; = "GPS_DEAD_RECKONING_COMBINED"

.field public static final GPS_EVENT_ACCURACY:Ljava/lang/String; = "accuracy"

.field public static final GPS_EVENT_ACCURACY_AVERAGE:Ljava/lang/String; = "Average_Accuracy"

.field public static final GPS_EVENT_ACCURACY_MAX:Ljava/lang/String; = "Max_Accuracy"

.field public static final GPS_EVENT_ACCURACY_MIN:Ljava/lang/String; = "Min_Accuracy"

.field public static final GPS_EVENT_DEAD_RECKONING_STARTED:Ljava/lang/String; = "GPS_DR_STARTED"

.field public static final GPS_EVENT_DEAD_RECKONING_STOPPED:Ljava/lang/String; = "GPS_DR_STOPPED"

.field public static final GPS_EVENT_DRIVING_STARTED:Ljava/lang/String; = "driving_started"

.field public static final GPS_EVENT_DRIVING_STOPPED:Ljava/lang/String; = "driving_stopped"

.field public static final GPS_EVENT_ENABLE_ESF_RAW:Ljava/lang/String; = "GPS_ENABLE_ESF_RAW"

.field public static final GPS_EVENT_SATELLITE_DATA:Ljava/lang/String; = "satellite_data"

.field public static final GPS_EVENT_SWITCH:Ljava/lang/String; = "GPS_Switch"

.field public static final GPS_EVENT_TIME:Ljava/lang/String; = "time"

.field public static final GPS_EVENT_WARM_RESET_UBLOX:Ljava/lang/String; = "GPS_WARM_RESET_UBLOX"

.field public static final GPS_EXTRA_DR_TYPE:Ljava/lang/String; = "drtype"

.field public static final GPS_EXTRA_LOG_PATH:Ljava/lang/String; = "logPath"

.field public static final GPS_FIX_TYPE:Ljava/lang/String; = "SAT_FIX_TYPE"

.field public static final GPS_PHONE_MARKER:Ljava/lang/String; = "P"

.field public static final GPS_SATELLITE_DB:Ljava/lang/String; = "SAT_DB_"

.field public static final GPS_SATELLITE_ID:Ljava/lang/String; = "SAT_ID_"

.field public static final GPS_SATELLITE_MAX_DB:Ljava/lang/String; = "SAT_MAX_DB"

.field public static final GPS_SATELLITE_PROVIDER:Ljava/lang/String; = "SAT_PROVIDER_"

.field public static final GPS_SATELLITE_SEEN:Ljava/lang/String; = "SAT_SEEN"

.field public static final GPS_SATELLITE_STATUS:Ljava/lang/String; = "GPS_SATELLITE_STATUS"

.field public static final GPS_SATELLITE_USED:Ljava/lang/String; = "SAT_USED"

.field public static final GPS_UBLOX_MARKER:Ljava/lang/String; = "U"

.field public static final INFO:Ljava/lang/String; = "info"

.field public static final LOCATION_FIX_CHECK_INTERVAL:I = 0x3e8

.field public static final LOCATION_FIX_THRESHOLD:I = 0xbb8

.field public static final NAVDY_GPS_PROVIDER:Ljava/lang/String; = "NAVDY_GPS_PROVIDER"

.field public static final NO_LOCATION_FIX_CHECK_INTERVAL:I = 0x1388

.field public static final SPACE:Ljava/lang/String; = " "

.field public static final TITLE:Ljava/lang/String; = "title"

.field public static final USING_PHONE_LOCATION:Ljava/lang/String; = "phone"

.field public static final USING_UBLOX_LOCATION:Ljava/lang/String; = "ublox"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
