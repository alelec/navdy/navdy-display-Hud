.class Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$9;
.super Ljava/lang/Object;
.source "GpsDeadReckoningManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    .prologue
    .line 647
    iput-object p1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$9;->this$0:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 650
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$9;->this$0:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->CFG_ESF_RAW:[B
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->access$2100()[B

    move-result-object v1

    const-string v2, "ESF-RAW"

    # invokes: Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->invokeUblox([BLjava/lang/String;)Z
    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->access$700(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;[BLjava/lang/String;)Z

    .line 652
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$9;->this$0:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sensorDataProcessor:Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;
    invoke-static {v0}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->access$2200(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;->setCalibrated(Z)V

    .line 653
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$9;->this$0:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v0}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->access$2300(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)Lcom/squareup/otto/Bus;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;

    invoke-direct {v1, v3, v3, v3}, Lcom/navdy/hud/app/device/gps/CalibratedGForceData;-><init>(FFF)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 654
    return-void
.end method
