.class public final enum Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;
.super Ljava/lang/Enum;
.source "GpsUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/device/gps/GpsUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "HeadingDirection"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

.field public static final enum E:Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

.field public static final enum N:Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

.field public static final enum NE:Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

.field public static final enum NW:Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

.field public static final enum S:Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

.field public static final enum SE:Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

.field public static final enum SW:Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

.field public static final enum W:Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 57
    new-instance v0, Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

    const-string v1, "N"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;->N:Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

    .line 58
    new-instance v0, Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

    const-string v1, "NE"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;->NE:Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

    .line 59
    new-instance v0, Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

    const-string v1, "E"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;->E:Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

    .line 60
    new-instance v0, Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

    const-string v1, "SE"

    invoke-direct {v0, v1, v6}, Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;->SE:Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

    .line 61
    new-instance v0, Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

    const-string v1, "S"

    invoke-direct {v0, v1, v7}, Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;->S:Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

    .line 62
    new-instance v0, Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

    const-string v1, "SW"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;->SW:Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

    .line 63
    new-instance v0, Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

    const-string v1, "W"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;->W:Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

    .line 64
    new-instance v0, Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

    const-string v1, "NW"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;->NW:Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

    .line 56
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;->N:Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;->NE:Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;->E:Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;->SE:Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;->S:Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;->SW:Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;->W:Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;->NW:Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;->$VALUES:[Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 56
    const-class v0, Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;->$VALUES:[Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/device/gps/GpsUtils$HeadingDirection;

    return-object v0
.end method
