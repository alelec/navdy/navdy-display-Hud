.class Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$3;
.super Ljava/lang/Object;
.source "GpsDeadReckoningManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    .prologue
    .line 264
    iput-object p1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$3;->this$0:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 267
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$3;->this$0:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->GET_ESF_STATUS:[B
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->access$600()[B

    move-result-object v1

    const-string v2, "get fusion status"

    # invokes: Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->invokeUblox([BLjava/lang/String;)Z
    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->access$700(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;[BLjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 268
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$3;->this$0:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->access$500(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$3;->this$0:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->getFusionStatusRunnableRetry:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->access$800(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 269
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$3;->this$0:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->access$500(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$3;->this$0:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->getFusionStatusRunnableRetry:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->access$800(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 273
    :goto_0
    return-void

    .line 271
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager$3;->this$0:Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    const/4 v1, 0x1

    # invokes: Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->postFusionRunnable(Z)V
    invoke-static {v0, v1}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->access$900(Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;Z)V

    goto :goto_0
.end method
