.class Lcom/navdy/hud/app/device/gps/GpsManager$8;
.super Ljava/lang/Object;
.source "GpsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/device/gps/GpsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/device/gps/GpsManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/device/gps/GpsManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/device/gps/GpsManager;

    .prologue
    .line 406
    iput-object p1, p0, Lcom/navdy/hud/app/device/gps/GpsManager$8;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    .line 409
    const/4 v4, 0x1

    .line 411
    .local v4, "resetRunnable":Z
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 412
    .local v2, "now":J
    iget-object v8, p0, Lcom/navdy/hud/app/device/gps/GpsManager$8;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->lastUbloxCoordinateTime:J
    invoke-static {v8}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$1300(Lcom/navdy/hud/app/device/gps/GpsManager;)J

    move-result-wide v8

    sub-long v6, v2, v8

    .line 413
    .local v6, "time":J
    const-wide/16 v8, 0xbb8

    cmp-long v8, v6, v8

    if-ltz v8, :cond_3

    .line 414
    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[Gps-loc] locationFixRunnable: lost location fix["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 418
    iget-object v8, p0, Lcom/navdy/hud/app/device/gps/GpsManager$8;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->extrapolationOn:Z
    invoke-static {v8}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$2400(Lcom/navdy/hud/app/device/gps/GpsManager;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 419
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    iget-object v10, p0, Lcom/navdy/hud/app/device/gps/GpsManager$8;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->extrapolationStartTime:J
    invoke-static {v10}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$2500(Lcom/navdy/hud/app/device/gps/GpsManager;)J

    move-result-wide v10

    sub-long v0, v8, v10

    .line 420
    .local v0, "diff":J
    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[Gps-loc] locationFixRunnable: extrapolation on time:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 421
    const-wide/16 v8, 0x1388

    cmp-long v8, v0, v8

    if-gez v8, :cond_1

    .line 422
    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v8

    const-string v9, "[Gps-loc] locationFixRunnable: reset"

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 447
    if-eqz v4, :cond_0

    .line 448
    iget-object v8, p0, Lcom/navdy/hud/app/device/gps/GpsManager$8;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->handler:Landroid/os/Handler;
    invoke-static {v8}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$500(Lcom/navdy/hud/app/device/gps/GpsManager;)Landroid/os/Handler;

    move-result-object v8

    iget-object v9, p0, Lcom/navdy/hud/app/device/gps/GpsManager$8;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->locationFixRunnable:Ljava/lang/Runnable;
    invoke-static {v9}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$600(Lcom/navdy/hud/app/device/gps/GpsManager;)Ljava/lang/Runnable;

    move-result-object v9

    const-wide/16 v10, 0x3e8

    invoke-virtual {v8, v9, v10, v11}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 451
    .end local v0    # "diff":J
    .end local v2    # "now":J
    .end local v6    # "time":J
    :cond_0
    :goto_0
    return-void

    .line 425
    .restart local v0    # "diff":J
    .restart local v2    # "now":J
    .restart local v6    # "time":J
    :cond_1
    :try_start_1
    iget-object v8, p0, Lcom/navdy/hud/app/device/gps/GpsManager$8;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    const/4 v9, 0x0

    # setter for: Lcom/navdy/hud/app/device/gps/GpsManager;->extrapolationOn:Z
    invoke-static {v8, v9}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$2402(Lcom/navdy/hud/app/device/gps/GpsManager;Z)Z

    .line 426
    iget-object v8, p0, Lcom/navdy/hud/app/device/gps/GpsManager$8;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    const-wide/16 v10, 0x0

    # setter for: Lcom/navdy/hud/app/device/gps/GpsManager;->extrapolationStartTime:J
    invoke-static {v8, v10, v11}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$2502(Lcom/navdy/hud/app/device/gps/GpsManager;J)J

    .line 427
    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v8

    const-string v9, "[Gps-loc] locationFixRunnable: expired"

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 431
    .end local v0    # "diff":J
    :cond_2
    const/4 v4, 0x0

    .line 433
    iget-object v8, p0, Lcom/navdy/hud/app/device/gps/GpsManager$8;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    const/4 v9, 0x0

    # invokes: Lcom/navdy/hud/app/device/gps/GpsManager;->updateDrivingState(Landroid/location/Location;)V
    invoke-static {v8, v9}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$1600(Lcom/navdy/hud/app/device/gps/GpsManager;Landroid/location/Location;)V

    .line 435
    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v8

    const-string v9, "[Gps-loc] lost gps fix"

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 436
    iget-object v8, p0, Lcom/navdy/hud/app/device/gps/GpsManager$8;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->handler:Landroid/os/Handler;
    invoke-static {v8}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$500(Lcom/navdy/hud/app/device/gps/GpsManager;)Landroid/os/Handler;

    move-result-object v8

    iget-object v9, p0, Lcom/navdy/hud/app/device/gps/GpsManager$8;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->locationFixRunnable:Ljava/lang/Runnable;
    invoke-static {v9}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$600(Lcom/navdy/hud/app/device/gps/GpsManager;)Ljava/lang/Runnable;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 437
    iget-object v8, p0, Lcom/navdy/hud/app/device/gps/GpsManager$8;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->handler:Landroid/os/Handler;
    invoke-static {v8}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$500(Lcom/navdy/hud/app/device/gps/GpsManager;)Landroid/os/Handler;

    move-result-object v8

    iget-object v9, p0, Lcom/navdy/hud/app/device/gps/GpsManager$8;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->noPhoneLocationFixRunnable:Ljava/lang/Runnable;
    invoke-static {v9}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$400(Lcom/navdy/hud/app/device/gps/GpsManager;)Ljava/lang/Runnable;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 438
    iget-object v8, p0, Lcom/navdy/hud/app/device/gps/GpsManager$8;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->handler:Landroid/os/Handler;
    invoke-static {v8}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$500(Lcom/navdy/hud/app/device/gps/GpsManager;)Landroid/os/Handler;

    move-result-object v8

    iget-object v9, p0, Lcom/navdy/hud/app/device/gps/GpsManager$8;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->noPhoneLocationFixRunnable:Ljava/lang/Runnable;
    invoke-static {v9}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$400(Lcom/navdy/hud/app/device/gps/GpsManager;)Ljava/lang/Runnable;

    move-result-object v9

    const-wide/16 v10, 0x1388

    invoke-virtual {v8, v9, v10, v11}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 439
    iget-object v8, p0, Lcom/navdy/hud/app/device/gps/GpsManager$8;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # invokes: Lcom/navdy/hud/app/device/gps/GpsManager;->startSendingLocation()V
    invoke-static {v8}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$2100(Lcom/navdy/hud/app/device/gps/GpsManager;)V

    .line 441
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    iget-object v10, p0, Lcom/navdy/hud/app/device/gps/GpsManager$8;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->gpsLastEventTime:J
    invoke-static {v10}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$800(Lcom/navdy/hud/app/device/gps/GpsManager;)J

    move-result-wide v10

    sub-long/2addr v8, v10

    const-wide/16 v10, 0x3e8

    div-long/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/navdy/hud/app/service/ConnectionServiceAnalyticsSupport;->recordGpsLostLocation(Ljava/lang/String;)V

    .line 442
    iget-object v8, p0, Lcom/navdy/hud/app/device/gps/GpsManager$8;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    # setter for: Lcom/navdy/hud/app/device/gps/GpsManager;->gpsLastEventTime:J
    invoke-static {v8, v10, v11}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$802(Lcom/navdy/hud/app/device/gps/GpsManager;J)J
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 447
    :cond_3
    if-eqz v4, :cond_0

    .line 448
    iget-object v8, p0, Lcom/navdy/hud/app/device/gps/GpsManager$8;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->handler:Landroid/os/Handler;
    invoke-static {v8}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$500(Lcom/navdy/hud/app/device/gps/GpsManager;)Landroid/os/Handler;

    move-result-object v8

    iget-object v9, p0, Lcom/navdy/hud/app/device/gps/GpsManager$8;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->locationFixRunnable:Ljava/lang/Runnable;
    invoke-static {v9}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$600(Lcom/navdy/hud/app/device/gps/GpsManager;)Ljava/lang/Runnable;

    move-result-object v9

    const-wide/16 v10, 0x3e8

    invoke-virtual {v8, v9, v10, v11}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 444
    .end local v2    # "now":J
    .end local v6    # "time":J
    :catch_0
    move-exception v5

    .line 445
    .local v5, "t":Ljava/lang/Throwable;
    :try_start_2
    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v8

    invoke-virtual {v8, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 447
    if-eqz v4, :cond_0

    .line 448
    iget-object v8, p0, Lcom/navdy/hud/app/device/gps/GpsManager$8;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->handler:Landroid/os/Handler;
    invoke-static {v8}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$500(Lcom/navdy/hud/app/device/gps/GpsManager;)Landroid/os/Handler;

    move-result-object v8

    iget-object v9, p0, Lcom/navdy/hud/app/device/gps/GpsManager$8;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->locationFixRunnable:Ljava/lang/Runnable;
    invoke-static {v9}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$600(Lcom/navdy/hud/app/device/gps/GpsManager;)Ljava/lang/Runnable;

    move-result-object v9

    const-wide/16 v10, 0x3e8

    invoke-virtual {v8, v9, v10, v11}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 447
    .end local v5    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v8

    if-eqz v4, :cond_4

    .line 448
    iget-object v9, p0, Lcom/navdy/hud/app/device/gps/GpsManager$8;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->handler:Landroid/os/Handler;
    invoke-static {v9}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$500(Lcom/navdy/hud/app/device/gps/GpsManager;)Landroid/os/Handler;

    move-result-object v9

    iget-object v10, p0, Lcom/navdy/hud/app/device/gps/GpsManager$8;->this$0:Lcom/navdy/hud/app/device/gps/GpsManager;

    # getter for: Lcom/navdy/hud/app/device/gps/GpsManager;->locationFixRunnable:Ljava/lang/Runnable;
    invoke-static {v10}, Lcom/navdy/hud/app/device/gps/GpsManager;->access$600(Lcom/navdy/hud/app/device/gps/GpsManager;)Ljava/lang/Runnable;

    move-result-object v10

    const-wide/16 v12, 0x3e8

    invoke-virtual {v9, v10, v12, v13}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_4
    throw v8
.end method
