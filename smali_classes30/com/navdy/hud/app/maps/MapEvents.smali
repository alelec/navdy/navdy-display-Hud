.class public Lcom/navdy/hud/app/maps/MapEvents;
.super Ljava/lang/Object;
.source "MapEvents.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/maps/MapEvents$HideTrafficLaneInfo;,
        Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;,
        Lcom/navdy/hud/app/maps/MapEvents$LaneData;,
        Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;,
        Lcom/navdy/hud/app/maps/MapEvents$HideSignPostJunction;,
        Lcom/navdy/hud/app/maps/MapEvents$DisplayJunction;,
        Lcom/navdy/hud/app/maps/MapEvents$MapUIReady;,
        Lcom/navdy/hud/app/maps/MapEvents$NewRouteAdded;,
        Lcom/navdy/hud/app/maps/MapEvents$ArrivalEvent;,
        Lcom/navdy/hud/app/maps/MapEvents$LocationFix;,
        Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;,
        Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;,
        Lcom/navdy/hud/app/maps/MapEvents$ManeuverSoonEvent;,
        Lcom/navdy/hud/app/maps/MapEvents$ManeuverEvent;,
        Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;,
        Lcom/navdy/hud/app/maps/MapEvents$TrafficJamDismissEvent;,
        Lcom/navdy/hud/app/maps/MapEvents$TrafficJamProgressEvent;,
        Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom;,
        Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficDismissEvent;,
        Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteDismissEvent;,
        Lcom/navdy/hud/app/maps/MapEvents$TrafficDelayDismissEvent;,
        Lcom/navdy/hud/app/maps/MapEvents$TrafficDelayEvent;,
        Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteAction;,
        Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;,
        Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent;,
        Lcom/navdy/hud/app/maps/MapEvents$RerouteEvent;,
        Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;,
        Lcom/navdy/hud/app/maps/MapEvents$SpeedWarning;,
        Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;,
        Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;,
        Lcom/navdy/hud/app/maps/MapEvents$NavigationModeChange;,
        Lcom/navdy/hud/app/maps/MapEvents$MapEngineReady;,
        Lcom/navdy/hud/app/maps/MapEvents$MapEngineInitialize;,
        Lcom/navdy/hud/app/maps/MapEvents$GPSSpeedEvent;,
        Lcom/navdy/hud/app/maps/MapEvents$GpsStatusChange;
    }
.end annotation


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/maps/MapEvents;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/maps/MapEvents;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/navdy/hud/app/maps/MapEvents;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method
