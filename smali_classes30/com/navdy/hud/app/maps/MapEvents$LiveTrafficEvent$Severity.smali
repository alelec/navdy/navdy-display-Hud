.class public final enum Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;
.super Ljava/lang/Enum;
.source "MapEvents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Severity"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;

.field public static final enum HIGH:Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;

.field public static final enum VERY_HIGH:Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;


# instance fields
.field public final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 193
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;

    const-string v1, "HIGH"

    invoke-direct {v0, v1, v2, v2}, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;->HIGH:Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;

    .line 194
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;

    const-string v1, "VERY_HIGH"

    invoke-direct {v0, v1, v3, v3}, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;->VERY_HIGH:Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;

    .line 192
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;->HIGH:Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;->VERY_HIGH:Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;->$VALUES:[Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 198
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 199
    iput p3, p0, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;->value:I

    .line 200
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 192
    const-class v0, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;
    .locals 1

    .prologue
    .line 192
    sget-object v0, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;->$VALUES:[Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;

    return-object v0
.end method
