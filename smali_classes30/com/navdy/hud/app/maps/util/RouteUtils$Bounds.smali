.class public final Lcom/navdy/hud/app/maps/util/RouteUtils$Bounds;
.super Ljava/lang/Object;
.source "RouteUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/util/RouteUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Bounds"
.end annotation


# instance fields
.field public height:D

.field public width:D


# direct methods
.method constructor <init>(DD)V
    .locals 1
    .param p1, "width"    # D
    .param p3, "height"    # D

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-wide p1, p0, Lcom/navdy/hud/app/maps/util/RouteUtils$Bounds;->width:D

    .line 50
    iput-wide p3, p0, Lcom/navdy/hud/app/maps/util/RouteUtils$Bounds;->height:D

    .line 51
    return-void
.end method
