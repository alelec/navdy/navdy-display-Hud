.class public Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;
.super Ljava/lang/Object;
.source "DistanceConverter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/util/DistanceConverter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Distance"
.end annotation


# instance fields
.field public unit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

.field public value:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;->value:F

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;->unit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    .line 23
    return-void
.end method

.method public getFormattedExtendedDistanceUnit()Ljava/lang/String;
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 26
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 28
    .local v0, "resources":Landroid/content/res/Resources;
    sget-object v1, Lcom/navdy/hud/app/maps/util/DistanceConverter$1;->$SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit:[I

    iget-object v2, p0, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;->unit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/navigation/DistanceUnit;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 55
    const-string v1, ""

    :goto_0
    return-object v1

    .line 30
    :pswitch_0
    iget v1, p0, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;->value:F

    cmpl-float v1, v1, v3

    if-nez v1, :cond_0

    .line 31
    const v1, 0x7f0902d4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 33
    :cond_0
    const v1, 0x7f0902d3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 36
    :pswitch_1
    iget v1, p0, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;->value:F

    cmpl-float v1, v1, v3

    if-nez v1, :cond_1

    .line 37
    const v1, 0x7f0902ce

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 39
    :cond_1
    const v1, 0x7f0902cd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 42
    :pswitch_2
    iget v1, p0, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;->value:F

    cmpl-float v1, v1, v3

    if-nez v1, :cond_2

    .line 43
    const v1, 0x7f0902d1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 45
    :cond_2
    const v1, 0x7f0902d0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 48
    :pswitch_3
    iget v1, p0, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;->value:F

    cmpl-float v1, v1, v3

    if-nez v1, :cond_3

    .line 49
    const v1, 0x7f0902d7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 51
    :cond_3
    const v1, 0x7f0902d6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 28
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
