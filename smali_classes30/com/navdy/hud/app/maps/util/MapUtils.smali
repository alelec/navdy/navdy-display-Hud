.class public final Lcom/navdy/hud/app/maps/util/MapUtils;
.super Ljava/lang/Object;
.source "MapUtils.java"


# static fields
.field public static final INVALID_DISTANCE:F = -1.0f

.field private static final MAX_ALTITUDE:D = 10000.0

.field private static final MAX_LATITUDE:D = 90.0

.field private static final MAX_LONGITUDE:D = 180.0

.field private static final MIN_ALTITUDE:D = -10000.0

.field private static final MIN_LATITUDE:D = -90.0

.field private static final MIN_LONGITUDE:D = -180.0

.field public static final SECONDS_PER_HOUR:I = 0xe10

.field public static final SECONDS_PER_MINUTE:I = 0x3c


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static distanceBetween(Lcom/navdy/service/library/events/location/LatLong;Lcom/navdy/service/library/events/location/LatLong;)F
    .locals 9
    .param p0, "latLong"    # Lcom/navdy/service/library/events/location/LatLong;
    .param p1, "other"    # Lcom/navdy/service/library/events/location/LatLong;

    .prologue
    .line 69
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 70
    :cond_0
    const/high16 v0, -0x40800000    # -1.0f

    .line 76
    :goto_0
    return v0

    .line 73
    :cond_1
    const/4 v0, 0x1

    new-array v8, v0, [F

    .line 74
    .local v8, "results":[F
    iget-object v0, p0, Lcom/navdy/service/library/events/location/LatLong;->latitude:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    iget-object v2, p0, Lcom/navdy/service/library/events/location/LatLong;->longitude:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    iget-object v4, p1, Lcom/navdy/service/library/events/location/LatLong;->latitude:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    iget-object v6, p1, Lcom/navdy/service/library/events/location/LatLong;->longitude:Ljava/lang/Double;

    invoke-virtual {v6}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    invoke-static/range {v0 .. v8}, Landroid/location/Location;->distanceBetween(DDDD[F)V

    .line 76
    const/4 v0, 0x0

    aget v0, v8, v0

    goto :goto_0
.end method

.method public static formatTime(I)Ljava/lang/String;
    .locals 6
    .param p0, "etaDifference"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 30
    const/16 v1, 0xe10

    if-lt p0, v1, :cond_0

    .line 31
    const-string v1, "%d:%02d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    div-int/lit16 v3, p0, 0xe10

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    div-int/lit8 v3, p0, 0x3c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 35
    .local v0, "formatted":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 33
    .end local v0    # "formatted":Ljava/lang/String;
    :cond_0
    const-string v1, "%d"

    new-array v2, v5, [Ljava/lang/Object;

    div-int/lit8 v3, p0, 0x3c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "formatted":Ljava/lang/String;
    goto :goto_0
.end method

.method public static getIconForDestination(Lcom/navdy/service/library/events/destination/Destination$FavoriteType;)I
    .locals 1
    .param p0, "type"    # Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    .prologue
    .line 65
    const v0, 0x7f0201b0

    return v0
.end method

.method public static getMinuteCeiling(I)I
    .locals 2
    .param p0, "time"    # I

    .prologue
    .line 58
    rem-int/lit8 v0, p0, 0x3c

    if-nez v0, :cond_0

    .line 61
    .end local p0    # "time":I
    :goto_0
    return p0

    .restart local p0    # "time":I
    :cond_0
    add-int/lit8 v0, p0, 0x3c

    rem-int/lit8 v1, p0, 0x3c

    sub-int p0, v0, v1

    goto :goto_0
.end method

.method public static sanitizeCoords(Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/here/android/mpa/common/GeoCoordinate;
    .locals 8
    .param p0, "geoCoordinate"    # Lcom/here/android/mpa/common/GeoCoordinate;

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v2

    .line 45
    .local v2, "latitude":D
    invoke-virtual {p0}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v4

    .line 46
    .local v4, "longitude":D
    invoke-virtual {p0}, Lcom/here/android/mpa/common/GeoCoordinate;->getAltitude()D

    move-result-wide v0

    .line 48
    .local v0, "altitude":D
    invoke-static {v2, v3, v4, v5}, Lcom/navdy/hud/app/maps/util/MapUtils;->sanitizeCoords(DD)Z

    move-result v6

    if-nez v6, :cond_1

    .line 49
    const/4 p0, 0x0

    .line 54
    .end local p0    # "geoCoordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    :cond_0
    :goto_0
    return-object p0

    .line 50
    .restart local p0    # "geoCoordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    :cond_1
    const-wide v6, -0x3f3c780000000000L    # -10000.0

    cmpg-double v6, v0, v6

    if-ltz v6, :cond_2

    const-wide v6, 0x40c3880000000000L    # 10000.0

    cmpl-double v6, v0, v6

    if-lez v6, :cond_0

    .line 51
    :cond_2
    const-wide/16 v6, 0x0

    invoke-virtual {p0, v6, v7}, Lcom/here/android/mpa/common/GeoCoordinate;->setAltitude(D)V

    goto :goto_0
.end method

.method public static sanitizeCoords(DD)Z
    .locals 2
    .param p0, "latitude"    # D
    .param p2, "longitude"    # D

    .prologue
    .line 39
    const-wide v0, 0x4056800000000000L    # 90.0

    cmpg-double v0, p0, v0

    if-gtz v0, :cond_0

    const-wide v0, -0x3fa9800000000000L    # -90.0

    cmpl-double v0, p0, v0

    if-ltz v0, :cond_0

    const-wide v0, 0x4066800000000000L    # 180.0

    cmpg-double v0, p2, v0

    if-gtz v0, :cond_0

    const-wide v0, -0x3f99800000000000L    # -180.0

    cmpl-double v0, p2, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
