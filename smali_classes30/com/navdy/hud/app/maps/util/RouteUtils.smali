.class public Lcom/navdy/hud/app/maps/util/RouteUtils;
.super Ljava/lang/Object;
.source "RouteUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/maps/util/RouteUtils$Bounds;
    }
.end annotation


# static fields
.field private static final SCALE_FACTOR:D = 1048576.0

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 53
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/maps/util/RouteUtils;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/maps/util/RouteUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static formatEtaMinutes(Landroid/content/res/Resources;I)Ljava/lang/String;
    .locals 7
    .param p0, "resources"    # Landroid/content/res/Resources;
    .param p1, "minutes"    # I

    .prologue
    const/4 v3, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 146
    const/16 v2, 0x3c

    if-ge p1, v2, :cond_0

    .line 148
    const v2, 0x7f0900f9

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {p0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 165
    :goto_0
    return-object v2

    .line 149
    :cond_0
    const/16 v2, 0x5a0

    if-ge p1, v2, :cond_2

    .line 151
    div-int/lit8 v1, p1, 0x3c

    .line 152
    .local v1, "hours":I
    mul-int/lit8 v2, v1, 0x3c

    sub-int/2addr p1, v2

    .line 153
    if-lez p1, :cond_1

    .line 154
    const v2, 0x7f0900f7

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {p0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 156
    :cond_1
    const v2, 0x7f0900f8

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {p0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 160
    .end local v1    # "hours":I
    :cond_2
    div-int/lit16 v0, p1, 0x5a0

    .line 161
    .local v0, "days":I
    mul-int/lit16 v2, v0, 0x5a0

    sub-int v2, p1, v2

    div-int/lit8 v1, v2, 0x3c

    .line 162
    .restart local v1    # "hours":I
    if-lez v1, :cond_3

    .line 163
    const v2, 0x7f0900f5

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {p0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 165
    :cond_3
    const v2, 0x7f0900f6

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {p0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method static getLatLngBounds(Ljava/util/List;)Lcom/navdy/hud/app/maps/util/RouteUtils$Bounds;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/common/GeoCoordinate;",
            ">;)",
            "Lcom/navdy/hud/app/maps/util/RouteUtils$Bounds;"
        }
    .end annotation

    .prologue
    .line 126
    .local p0, "points":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/common/GeoCoordinate;>;"
    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/here/android/mpa/common/GeoCoordinate;

    invoke-virtual {v3}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v14

    .line 127
    .local v14, "lowestLat":D
    move-wide v6, v14

    .line 128
    .local v6, "highestLat":D
    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/here/android/mpa/common/GeoCoordinate;

    invoke-virtual {v3}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v16

    .line 129
    .local v16, "lowestLong":D
    move-wide/from16 v8, v16

    .line 131
    .local v8, "highestLong":D
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/here/android/mpa/common/GeoCoordinate;

    .line 132
    .local v2, "geo":Lcom/here/android/mpa/common/GeoCoordinate;
    invoke-virtual {v2}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v10

    .line 133
    .local v10, "latitude":D
    invoke-virtual {v2}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v12

    .line 134
    .local v12, "longitude":D
    invoke-static {v10, v11, v14, v15}, Ljava/lang/Math;->min(DD)D

    move-result-wide v14

    .line 135
    invoke-static {v10, v11, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v6

    .line 136
    move-wide/from16 v0, v16

    invoke-static {v12, v13, v0, v1}, Ljava/lang/Math;->min(DD)D

    move-result-wide v16

    .line 137
    invoke-static {v12, v13, v8, v9}, Ljava/lang/Math;->max(DD)D

    move-result-wide v8

    .line 138
    goto :goto_0

    .line 139
    .end local v2    # "geo":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v10    # "latitude":D
    .end local v12    # "longitude":D
    :cond_0
    sub-double v18, v8, v16

    .line 140
    .local v18, "width":D
    sub-double v4, v6, v14

    .line 142
    .local v4, "height":D
    new-instance v3, Lcom/navdy/hud/app/maps/util/RouteUtils$Bounds;

    move-wide/from16 v0, v18

    invoke-direct {v3, v0, v1, v4, v5}, Lcom/navdy/hud/app/maps/util/RouteUtils$Bounds;-><init>(DD)V

    return-object v3
.end method

.method public static simplify(Ljava/util/List;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/common/GeoCoordinate;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    .local p0, "points":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/common/GeoCoordinate;>;"
    invoke-static {p0}, Lcom/navdy/hud/app/maps/util/RouteUtils;->getLatLngBounds(Ljava/util/List;)Lcom/navdy/hud/app/maps/util/RouteUtils$Bounds;

    move-result-object v0

    .line 59
    .local v0, "bounds":Lcom/navdy/hud/app/maps/util/RouteUtils$Bounds;
    iget-wide v4, v0, Lcom/navdy/hud/app/maps/util/RouteUtils$Bounds;->width:D

    iget-wide v6, v0, Lcom/navdy/hud/app/maps/util/RouteUtils$Bounds;->height:D

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    .line 60
    .local v2, "size":D
    const-wide v4, 0x409f400000000000L    # 2000.0

    mul-double/2addr v4, v2

    invoke-static {p0, v4, v5}, Lcom/navdy/hud/app/maps/util/RouteUtils;->simplify(Ljava/util/List;D)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public static simplify(Ljava/util/List;D)Ljava/util/List;
    .locals 3
    .param p1, "tolerance"    # D
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/common/GeoCoordinate;",
            ">;D)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    .local p0, "points":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/common/GeoCoordinate;>;"
    mul-double v0, p1, p1

    .line 67
    .local v0, "sqTolerance":D
    invoke-static {p0, v0, v1}, Lcom/navdy/hud/app/maps/util/RouteUtils;->simplifyRadialDistance(Ljava/util/List;D)Ljava/util/List;

    move-result-object v2

    return-object v2
.end method

.method static simplifyRadialDistance(Ljava/util/List;D)Ljava/util/List;
    .locals 21
    .param p1, "sqTolerance"    # D
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/common/GeoCoordinate;",
            ">;D)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    .local p0, "points":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/common/GeoCoordinate;>;"
    if-eqz p0, :cond_0

    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v16

    if-nez v16, :cond_1

    .line 73
    :cond_0
    const/4 v11, 0x0

    .line 122
    :goto_0
    return-object v11

    .line 75
    :cond_1
    const/4 v12, 0x0

    .line 76
    .local v12, "pointLatitude":F
    const/4 v13, 0x0

    .line 78
    .local v13, "pointLongitude":F
    const/16 v16, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/here/android/mpa/common/GeoCoordinate;

    invoke-virtual/range {v16 .. v16}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v14, v0

    .line 79
    .local v14, "prevLatitude":F
    const/16 v16, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/here/android/mpa/common/GeoCoordinate;

    invoke-virtual/range {v16 .. v16}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v15, v0

    .line 81
    .local v15, "prevLongitude":F
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 83
    .local v11, "newLatLongs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Float;>;"
    invoke-static {v14}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 84
    invoke-static {v15}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 86
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v10

    .line 89
    .local v10, "len":I
    const/16 v16, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/here/android/mpa/common/GeoCoordinate;

    .line 90
    .local v8, "geo":Lcom/here/android/mpa/common/GeoCoordinate;
    invoke-virtual {v8}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v12, v0

    .line 91
    invoke-virtual {v8}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v13, v0

    .line 92
    invoke-static {v12}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 93
    invoke-static {v13}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 96
    const/4 v9, 0x1

    .local v9, "i":I
    :goto_1
    add-int/lit8 v16, v10, -0x1

    move/from16 v0, v16

    if-ge v9, v0, :cond_3

    .line 97
    move-object/from16 v0, p0

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "geo":Lcom/here/android/mpa/common/GeoCoordinate;
    check-cast v8, Lcom/here/android/mpa/common/GeoCoordinate;

    .line 98
    .restart local v8    # "geo":Lcom/here/android/mpa/common/GeoCoordinate;
    invoke-virtual {v8}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v12, v0

    .line 99
    invoke-virtual {v8}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v13, v0

    .line 101
    sub-float v16, v12, v14

    move/from16 v0, v16

    float-to-double v0, v0

    move-wide/from16 v16, v0

    const-wide/high16 v18, 0x4130000000000000L    # 1048576.0

    mul-double v4, v16, v18

    .line 102
    .local v4, "dx":D
    sub-float v16, v13, v15

    move/from16 v0, v16

    float-to-double v0, v0

    move-wide/from16 v16, v0

    const-wide/high16 v18, 0x4130000000000000L    # 1048576.0

    mul-double v6, v16, v18

    .line 104
    .local v6, "dy":D
    mul-double v16, v4, v4

    mul-double v18, v6, v6

    add-double v2, v16, v18

    .line 107
    .local v2, "distanceSquared":D
    cmpl-double v16, v2, p1

    if-lez v16, :cond_2

    .line 108
    invoke-static {v12}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 109
    invoke-static {v13}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110
    move v14, v12

    .line 111
    move v15, v13

    .line 96
    :cond_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 116
    .end local v2    # "distanceSquared":D
    .end local v4    # "dx":D
    .end local v6    # "dy":D
    :cond_3
    add-int/lit8 v16, v10, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "geo":Lcom/here/android/mpa/common/GeoCoordinate;
    check-cast v8, Lcom/here/android/mpa/common/GeoCoordinate;

    .line 117
    .restart local v8    # "geo":Lcom/here/android/mpa/common/GeoCoordinate;
    invoke-virtual {v8}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v12, v0

    .line 118
    invoke-virtual {v8}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v13, v0

    .line 119
    invoke-static {v12}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 120
    invoke-static {v13}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method
