.class public Lcom/navdy/hud/app/maps/util/DistanceConverter;
.super Ljava/lang/Object;
.source "DistanceConverter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static convertToDistance(Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;FLcom/navdy/hud/app/maps/util/DistanceConverter$Distance;)V
    .locals 3
    .param p0, "unit"    # Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;
    .param p1, "value"    # F
    .param p2, "distance"    # Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;

    .prologue
    const/16 v2, 0xa

    .line 60
    invoke-virtual {p2}, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;->clear()V

    .line 62
    sget-object v0, Lcom/navdy/hud/app/maps/util/DistanceConverter$1;->$SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit:[I

    invoke-virtual {p0}, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 77
    const v0, 0x4320ef1b

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_1

    .line 78
    const v0, 0x44c92ae1

    div-float v0, p1, v0

    iput v0, p2, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;->value:F

    .line 79
    iget v0, p2, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;->value:F

    float-to-double v0, v0

    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->roundToN(DI)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p2, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;->value:F

    .line 80
    sget-object v0, Lcom/navdy/service/library/events/navigation/DistanceUnit;->DISTANCE_MILES:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    iput-object v0, p2, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;->unit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    .line 88
    :goto_0
    return-void

    .line 64
    :pswitch_0
    const/high16 v0, 0x43c80000    # 400.0f

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    .line 65
    const/high16 v0, 0x447a0000    # 1000.0f

    div-float v0, p1, v0

    iput v0, p2, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;->value:F

    .line 66
    iget v0, p2, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;->value:F

    float-to-double v0, v0

    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->roundToN(DI)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p2, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;->value:F

    .line 67
    sget-object v0, Lcom/navdy/service/library/events/navigation/DistanceUnit;->DISTANCE_KMS:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    iput-object v0, p2, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;->unit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    goto :goto_0

    .line 69
    :cond_0
    iput p1, p2, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;->value:F

    .line 70
    sget-object v0, Lcom/navdy/service/library/events/navigation/DistanceUnit;->DISTANCE_METERS:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    iput-object v0, p2, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;->unit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    goto :goto_0

    .line 82
    :cond_1
    const v0, 0x4051f948

    mul-float/2addr v0, p1

    float-to-int v0, v0

    int-to-float v0, v0

    iput v0, p2, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;->value:F

    .line 83
    sget-object v0, Lcom/navdy/service/library/events/navigation/DistanceUnit;->DISTANCE_FEET:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    iput-object v0, p2, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;->unit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    goto :goto_0

    .line 62
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static convertToMeters(FLcom/navdy/service/library/events/navigation/DistanceUnit;)F
    .locals 2
    .param p0, "value"    # F
    .param p1, "unit"    # Lcom/navdy/service/library/events/navigation/DistanceUnit;

    .prologue
    .line 91
    sget-object v0, Lcom/navdy/hud/app/maps/util/DistanceConverter$1;->$SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit:[I

    invoke-virtual {p1}, Lcom/navdy/service/library/events/navigation/DistanceUnit;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 101
    .end local p0    # "value":F
    :goto_0
    :pswitch_0
    return p0

    .line 95
    .restart local p0    # "value":F
    :pswitch_1
    const/high16 v0, 0x447a0000    # 1000.0f

    mul-float/2addr p0, v0

    goto :goto_0

    .line 97
    :pswitch_2
    const v0, 0x44c92ae1

    mul-float/2addr p0, v0

    goto :goto_0

    .line 99
    :pswitch_3
    const v0, 0x4051f948

    div-float/2addr p0, v0

    goto :goto_0

    .line 91
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
