.class public Lcom/navdy/hud/app/maps/util/DestinationUtil;
.super Ljava/lang/Object;
.source "DestinationUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static convert(Landroid/content/Context;Ljava/util/List;IIZ)Ljava/util/List;
    .locals 35
    .param p0, "context"    # Landroid/content/Context;
    .param p2, "defaultColor"    # I
    .param p3, "deselectedColor"    # I
    .param p4, "extendedUnits"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/destination/Destination;",
            ">;IIZ)",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 31
    .local p1, "destinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/destination/Destination;>;"
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v30

    .line 32
    .local v30, "hereLocationFixManager":Lcom/navdy/hud/app/maps/here/HereLocationFixManager;
    const/16 v24, 0x0

    .line 33
    .local v24, "coordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    const/16 v25, 0x0

    .line 34
    .local v25, "currentPosition":Lcom/navdy/service/library/events/location/LatLong;
    if-eqz v30, :cond_0

    .line 35
    invoke-virtual/range {v30 .. v30}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v24

    .line 37
    :cond_0
    if-eqz v24, :cond_1

    .line 38
    new-instance v25, Lcom/navdy/service/library/events/location/LatLong;

    .end local v25    # "currentPosition":Lcom/navdy/service/library/events/location/LatLong;
    invoke-virtual/range {v24 .. v24}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual/range {v24 .. v24}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    move-object/from16 v0, v25

    invoke-direct {v0, v3, v4}, Lcom/navdy/service/library/events/location/LatLong;-><init>(Ljava/lang/Double;Ljava/lang/Double;)V

    .line 41
    .restart local v25    # "currentPosition":Lcom/navdy/service/library/events/location/LatLong;
    :cond_1
    new-instance v34, Ljava/util/ArrayList;

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v3

    move-object/from16 v0, v34

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 46
    .local v34, "result":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;>;"
    const/16 v31, 0x0

    .local v31, "i":I
    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v3

    move/from16 v0, v31

    if-ge v0, v3, :cond_7

    .line 47
    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/navdy/service/library/events/destination/Destination;

    .line 48
    .local v26, "destination":Lcom/navdy/service/library/events/destination/Destination;
    const/high16 v27, -0x40800000    # -1.0f

    .line 50
    .local v27, "distance":F
    const-wide/16 v14, 0x0

    .line 51
    .local v14, "displayLat":D
    const-wide/16 v16, 0x0

    .line 52
    .local v16, "displayLng":D
    const-wide/16 v10, 0x0

    .line 53
    .local v10, "navLat":D
    const-wide/16 v12, 0x0

    .line 54
    .local v12, "navLng":D
    const/4 v7, 0x0

    .line 56
    .local v7, "distanceSubTitle":Ljava/lang/String;
    move-object/from16 v0, v26

    iget-object v3, v0, Lcom/navdy/service/library/events/destination/Destination;->navigation_position:Lcom/navdy/service/library/events/location/LatLong;

    if-eqz v3, :cond_2

    .line 57
    move-object/from16 v0, v26

    iget-object v3, v0, Lcom/navdy/service/library/events/destination/Destination;->navigation_position:Lcom/navdy/service/library/events/location/LatLong;

    iget-object v3, v3, Lcom/navdy/service/library/events/location/LatLong;->latitude:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v10

    .line 58
    move-object/from16 v0, v26

    iget-object v3, v0, Lcom/navdy/service/library/events/destination/Destination;->navigation_position:Lcom/navdy/service/library/events/location/LatLong;

    iget-object v3, v3, Lcom/navdy/service/library/events/location/LatLong;->longitude:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v12

    .line 60
    move-object/from16 v0, v26

    iget-object v3, v0, Lcom/navdy/service/library/events/destination/Destination;->navigation_position:Lcom/navdy/service/library/events/location/LatLong;

    iget-object v3, v3, Lcom/navdy/service/library/events/location/LatLong;->latitude:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide/16 v8, 0x0

    cmpl-double v3, v4, v8

    if-eqz v3, :cond_5

    move-object/from16 v0, v26

    iget-object v3, v0, Lcom/navdy/service/library/events/destination/Destination;->navigation_position:Lcom/navdy/service/library/events/location/LatLong;

    iget-object v3, v3, Lcom/navdy/service/library/events/location/LatLong;->longitude:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide/16 v8, 0x0

    cmpl-double v3, v4, v8

    if-eqz v3, :cond_5

    .line 61
    move-object/from16 v0, v26

    iget-object v3, v0, Lcom/navdy/service/library/events/destination/Destination;->navigation_position:Lcom/navdy/service/library/events/location/LatLong;

    move-object/from16 v0, v25

    invoke-static {v3, v0}, Lcom/navdy/hud/app/maps/util/MapUtils;->distanceBetween(Lcom/navdy/service/library/events/location/LatLong;Lcom/navdy/service/library/events/location/LatLong;)F

    move-result v27

    .line 67
    :cond_2
    :goto_1
    const/16 v28, 0x0

    .line 68
    .local v28, "distanceStr":Ljava/lang/String;
    const/high16 v3, -0x40800000    # -1.0f

    cmpl-float v3, v27, v3

    if-eqz v3, :cond_3

    .line 69
    new-instance v29, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;

    invoke-direct/range {v29 .. v29}, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;-><init>()V

    .line 70
    .local v29, "formattedDistance":Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;
    invoke-static {}, Lcom/navdy/hud/app/manager/SpeedManager;->getInstance()Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/manager/SpeedManager;->getSpeedUnit()Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    move-result-object v3

    move/from16 v0, v27

    move-object/from16 v1, v29

    invoke-static {v3, v0, v1}, Lcom/navdy/hud/app/maps/util/DistanceConverter;->convertToDistance(Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;FLcom/navdy/hud/app/maps/util/DistanceConverter$Distance;)V

    .line 71
    move-object/from16 v0, v29

    iget v3, v0, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;->value:F

    move-object/from16 v0, v29

    iget-object v4, v0, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;->unit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    move/from16 v0, p4

    invoke-static {v3, v4, v0}, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->getFormattedDistance(FLcom/navdy/service/library/events/navigation/DistanceUnit;Z)Ljava/lang/String;

    move-result-object v28

    .line 72
    const-string v3, " "

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v32

    .line 73
    .local v32, "index":I
    const/4 v3, -0x1

    move/from16 v0, v32

    if-eq v0, v3, :cond_3

    .line 74
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<b>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, v28

    move/from16 v1, v32

    invoke-virtual {v0, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "</b>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v28

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 78
    .end local v29    # "formattedDistance":Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;
    .end local v32    # "index":I
    :cond_3
    move-object/from16 v0, v26

    iget-object v3, v0, Lcom/navdy/service/library/events/destination/Destination;->display_position:Lcom/navdy/service/library/events/location/LatLong;

    if-eqz v3, :cond_4

    .line 79
    move-object/from16 v0, v26

    iget-object v3, v0, Lcom/navdy/service/library/events/destination/Destination;->display_position:Lcom/navdy/service/library/events/location/LatLong;

    iget-object v3, v3, Lcom/navdy/service/library/events/location/LatLong;->latitude:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v14

    .line 80
    move-object/from16 v0, v26

    iget-object v3, v0, Lcom/navdy/service/library/events/destination/Destination;->display_position:Lcom/navdy/service/library/events/location/LatLong;

    iget-object v3, v3, Lcom/navdy/service/library/events/location/LatLong;->longitude:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v16

    .line 83
    :cond_4
    move-object/from16 v0, v26

    iget-object v3, v0, Lcom/navdy/service/library/events/destination/Destination;->place_type:Lcom/navdy/service/library/events/places/PlaceType;

    invoke-static {v3}, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen;->getPlaceTypeHolder(Lcom/navdy/service/library/events/places/PlaceType;)Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;

    move-result-object v33

    .line 84
    .local v33, "resourceHolder":Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;
    if-eqz v33, :cond_6

    .line 85
    move-object/from16 v0, v33

    iget v0, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;->iconRes:I

    move/from16 v18, v0

    .line 86
    .local v18, "iconRes":I
    move-object/from16 v0, v33

    iget v3, v0, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;->colorRes:I

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v20

    .line 92
    .local v20, "color":I
    :goto_2
    new-instance v2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    const/4 v3, 0x0

    move-object/from16 v0, v26

    iget-object v4, v0, Lcom/navdy/service/library/events/destination/Destination;->destination_title:Ljava/lang/String;

    move-object/from16 v0, v26

    iget-object v5, v0, Lcom/navdy/service/library/events/destination/Destination;->destination_subtitle:Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v8, 0x1

    move-object/from16 v0, v26

    iget-object v9, v0, Lcom/navdy/service/library/events/destination/Destination;->full_address:Ljava/lang/String;

    const/16 v19, 0x0

    sget-object v22, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;->DESTINATION:Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/navdy/service/library/events/destination/Destination;->place_type:Lcom/navdy/service/library/events/places/PlaceType;

    move-object/from16 v23, v0

    move/from16 v21, p3

    invoke-direct/range {v2 .. v23}, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;-><init>(ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZLjava/lang/String;DDDDIIIILcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;Lcom/navdy/service/library/events/places/PlaceType;)V

    .line 109
    .local v2, "parcelable":Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;
    move-object/from16 v0, v26

    iget-object v3, v0, Lcom/navdy/service/library/events/destination/Destination;->identifier:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->setIdentifier(Ljava/lang/String;)V

    .line 110
    move-object/from16 v0, v26

    iget-object v3, v0, Lcom/navdy/service/library/events/destination/Destination;->place_id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->setPlaceId(Ljava/lang/String;)V

    .line 111
    move-object/from16 v0, v28

    iput-object v0, v2, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->distanceStr:Ljava/lang/String;

    .line 112
    move-object/from16 v0, v26

    iget-object v3, v0, Lcom/navdy/service/library/events/destination/Destination;->contacts:Ljava/util/List;

    invoke-static {v3}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->fromContacts(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->setContacts(Ljava/util/List;)V

    .line 113
    move-object/from16 v0, v26

    iget-object v3, v0, Lcom/navdy/service/library/events/destination/Destination;->phoneNumbers:Ljava/util/List;

    invoke-static {v3}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->fromPhoneNumbers(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->setPhoneNumbers(Ljava/util/List;)V

    .line 114
    move-object/from16 v0, v34

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    add-int/lit8 v31, v31, 0x1

    goto/16 :goto_0

    .line 62
    .end local v2    # "parcelable":Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;
    .end local v18    # "iconRes":I
    .end local v20    # "color":I
    .end local v28    # "distanceStr":Ljava/lang/String;
    .end local v33    # "resourceHolder":Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;
    :cond_5
    move-object/from16 v0, v26

    iget-object v3, v0, Lcom/navdy/service/library/events/destination/Destination;->display_position:Lcom/navdy/service/library/events/location/LatLong;

    if-eqz v3, :cond_2

    move-object/from16 v0, v26

    iget-object v3, v0, Lcom/navdy/service/library/events/destination/Destination;->display_position:Lcom/navdy/service/library/events/location/LatLong;

    iget-object v3, v3, Lcom/navdy/service/library/events/location/LatLong;->latitude:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide/16 v8, 0x0

    cmpl-double v3, v4, v8

    if-eqz v3, :cond_2

    move-object/from16 v0, v26

    iget-object v3, v0, Lcom/navdy/service/library/events/destination/Destination;->display_position:Lcom/navdy/service/library/events/location/LatLong;

    iget-object v3, v3, Lcom/navdy/service/library/events/location/LatLong;->longitude:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide/16 v8, 0x0

    cmpl-double v3, v4, v8

    if-eqz v3, :cond_2

    .line 63
    move-object/from16 v0, v26

    iget-object v3, v0, Lcom/navdy/service/library/events/destination/Destination;->display_position:Lcom/navdy/service/library/events/location/LatLong;

    move-object/from16 v0, v25

    invoke-static {v3, v0}, Lcom/navdy/hud/app/maps/util/MapUtils;->distanceBetween(Lcom/navdy/service/library/events/location/LatLong;Lcom/navdy/service/library/events/location/LatLong;)F

    move-result v27

    goto/16 :goto_1

    .line 88
    .restart local v28    # "distanceStr":Ljava/lang/String;
    .restart local v33    # "resourceHolder":Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;
    :cond_6
    const v18, 0x7f020184

    .line 89
    .restart local v18    # "iconRes":I
    move/from16 v20, p2

    .restart local v20    # "color":I
    goto/16 :goto_2

    .line 116
    .end local v7    # "distanceSubTitle":Ljava/lang/String;
    .end local v10    # "navLat":D
    .end local v12    # "navLng":D
    .end local v14    # "displayLat":D
    .end local v16    # "displayLng":D
    .end local v18    # "iconRes":I
    .end local v20    # "color":I
    .end local v26    # "destination":Lcom/navdy/service/library/events/destination/Destination;
    .end local v27    # "distance":F
    .end local v28    # "distanceStr":Ljava/lang/String;
    .end local v33    # "resourceHolder":Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$PlaceTypeResourceHolder;
    :cond_7
    return-object v34
.end method
