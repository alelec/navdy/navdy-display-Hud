.class public Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;
.super Lcom/navdy/hud/app/view/DashboardWidgetPresenter;
.source "TrafficIncidentWidgetPresenter.java"


# static fields
.field private static handler:Landroid/os/Handler;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private bus:Lcom/squareup/otto/Bus;

.field private registered:Z

.field private textView:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 38
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-string v1, "TrafficIncidentWidget"

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 40
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;->handler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;-><init>()V

    .line 47
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;->bus:Lcom/squareup/otto/Bus;

    .line 48
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;->textView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$100()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method private setText()V
    .locals 4

    .prologue
    .line 203
    iget-object v1, p0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;->textView:Landroid/widget/TextView;

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0c001d

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 204
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v0

    .line 205
    .local v0, "hereMapsManager":Lcom/navdy/hud/app/maps/here/HereMapsManager;
    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isTrafficUpdaterRunning()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 206
    iget-object v1, p0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;->textView:Landroid/widget/TextView;

    const-string v2, "NORMAL"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 210
    :goto_0
    return-void

    .line 208
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;->textView:Landroid/widget/TextView;

    const-string v2, "INACTIVE"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private unregister()V
    .locals 2

    .prologue
    .line 213
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;->registered:Z

    if-eqz v0, :cond_0

    .line 214
    sget-object v0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "unregister"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 215
    iget-object v0, p0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->unregister(Ljava/lang/Object;)V

    .line 216
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;->registered:Z

    .line 218
    :cond_0
    return-void
.end method


# virtual methods
.method public getDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    return-object v0
.end method

.method public getWidgetIdentifier()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    const-string v0, "TRAFFIC_INCIDENT_GAUGE_ID"

    return-object v0
.end method

.method public getWidgetName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    return-object v0
.end method

.method public onDisplayTrafficIncident(Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;)V
    .locals 6
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 104
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;->textView:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 105
    sget-object v1, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DisplayTrafficIncident type:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;->type:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", category:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;->category:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", title:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;->title:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", description:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;->description:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", affectedStreet:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;->affectedStreet:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", distanceToIncident:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p1, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;->distanceToIncident:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", reported:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;->reported:Ljava/util/Date;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", updated:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;->updated:Ljava/util/Date;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", icon:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;->icon:Lcom/here/android/mpa/common/Image;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 116
    sget-object v1, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter$2;->$SwitchMap$com$navdy$hud$app$maps$MapEvents$DisplayTrafficIncident$Type:[I

    iget-object v2, p1, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;->type:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 133
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter$1;

    invoke-direct {v2, p0, p1}, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter$1;-><init>(Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;)V

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 200
    :cond_0
    :goto_0
    return-void

    .line 118
    :pswitch_0
    iget-object v1, p0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;->textView:Landroid/widget/TextView;

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0c001d

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 119
    iget-object v1, p0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;->textView:Landroid/widget/TextView;

    const-string v2, "NORMAL"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 197
    :catch_0
    move-exception v0

    .line 198
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 123
    .end local v0    # "t":Ljava/lang/Throwable;
    :pswitch_1
    :try_start_1
    iget-object v1, p0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;->textView:Landroid/widget/TextView;

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0c001d

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 124
    iget-object v1, p0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;->textView:Landroid/widget/TextView;

    const-string v2, "INACTIVE"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 128
    :pswitch_2
    iget-object v1, p0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;->textView:Landroid/widget/TextView;

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0c001d

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 129
    iget-object v1, p0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;->textView:Landroid/widget/TextView;

    const-string v2, "FAILED"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 116
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setView(Lcom/navdy/hud/app/view/DashboardWidgetView;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "dashboardWidgetView"    # Lcom/navdy/hud/app/view/DashboardWidgetView;
    .param p2, "arguments"    # Landroid/os/Bundle;

    .prologue
    .line 70
    if-eqz p1, :cond_3

    .line 71
    const/4 v0, 0x0

    .line 72
    .local v0, "isActive":Z
    if-eqz p2, :cond_0

    .line 73
    const-string v1, "EXTRA_IS_ACTIVE"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 76
    :cond_0
    const v1, 0x7f030032

    invoke-virtual {p1, v1}, Lcom/navdy/hud/app/view/DashboardWidgetView;->setContentView(I)Z

    .line 77
    const v1, 0x7f0e013e

    invoke-virtual {p1, v1}, Lcom/navdy/hud/app/view/DashboardWidgetView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;->textView:Landroid/widget/TextView;

    .line 78
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;->setText()V

    .line 79
    invoke-super {p0, p1, p2}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->setView(Lcom/navdy/hud/app/view/DashboardWidgetView;Landroid/os/Bundle;)V

    .line 81
    if-nez v0, :cond_1

    .line 82
    sget-object v1, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "not active"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 83
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;->unregister()V

    .line 99
    .end local v0    # "isActive":Z
    :goto_0
    return-void

    .line 87
    .restart local v0    # "isActive":Z
    :cond_1
    iget-boolean v1, p0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;->registered:Z

    if-nez v1, :cond_2

    .line 88
    iget-object v1, p0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v1, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 89
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;->registered:Z

    .line 90
    sget-object v1, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "register"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 92
    :cond_2
    sget-object v1, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "already register"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 95
    .end local v0    # "isActive":Z
    :cond_3
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;->unregister()V

    .line 96
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;->textView:Landroid/widget/TextView;

    .line 97
    invoke-super {p0, p1, p2}, Lcom/navdy/hud/app/view/DashboardWidgetPresenter;->setView(Lcom/navdy/hud/app/view/DashboardWidgetView;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method protected updateGauge()V
    .locals 0

    .prologue
    .line 56
    return-void
.end method
