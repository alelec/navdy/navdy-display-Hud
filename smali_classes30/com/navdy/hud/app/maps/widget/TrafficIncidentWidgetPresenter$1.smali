.class Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter$1;
.super Ljava/lang/Object;
.source "TrafficIncidentWidgetPresenter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;->onDisplayTrafficIncident(Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;

.field final synthetic val$event:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter$1;->this$0:Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;

    iput-object p2, p0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter$1;->val$event:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/16 v7, 0x28

    const/16 v8, 0x21

    .line 136
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 138
    .local v1, "builder":Landroid/text/SpannableStringBuilder;
    const/4 v0, 0x0

    .line 139
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v6, p0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter$1;->val$event:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;

    iget-object v6, v6, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;->icon:Lcom/here/android/mpa/common/Image;

    if-eqz v6, :cond_0

    .line 140
    iget-object v6, p0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter$1;->val$event:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;

    iget-object v6, v6, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;->icon:Lcom/here/android/mpa/common/Image;

    invoke-virtual {v6, v7, v7}, Lcom/here/android/mpa/common/Image;->getBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 143
    :cond_0
    const/4 v5, 0x0

    .line 144
    .local v5, "start":I
    iget-object v6, p0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter$1;->val$event:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;

    iget-object v6, v6, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;->type:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

    invoke-virtual {v6}, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 145
    new-instance v6, Landroid/text/style/StyleSpan;

    const/4 v7, 0x1

    invoke-direct {v6, v7}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    invoke-virtual {v1, v6, v5, v7, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 146
    const-string v6, " "

    invoke-virtual {v1, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 147
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    .line 149
    if-eqz v0, :cond_1

    .line 150
    new-instance v4, Landroid/text/style/ImageSpan;

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v4, v6, v0}, Landroid/text/style/ImageSpan;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;)V

    .line 151
    .local v4, "imageSpan":Landroid/text/style/ImageSpan;
    const-string v6, "IMG"

    invoke-virtual {v1, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 152
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v6

    invoke-virtual {v1, v4, v5, v6, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 153
    const-string v6, " "

    invoke-virtual {v1, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 156
    .end local v4    # "imageSpan":Landroid/text/style/ImageSpan;
    :cond_1
    iget-object v6, p0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter$1;->val$event:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;

    iget-object v6, v6, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;->title:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 157
    const-string v6, "("

    invoke-virtual {v1, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 158
    iget-object v6, p0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter$1;->val$event:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;

    iget-object v6, v6, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;->title:Ljava/lang/String;

    invoke-virtual {v1, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 159
    const-string v6, ") ,"

    invoke-virtual {v1, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 162
    :cond_2
    iget-object v6, p0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter$1;->val$event:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;

    iget-object v6, v6, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;->description:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 163
    iget-object v6, p0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter$1;->val$event:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;

    iget-object v6, v6, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;->description:Ljava/lang/String;

    invoke-virtual {v1, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 164
    const-string v6, " , "

    invoke-virtual {v1, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 167
    :cond_3
    iget-object v6, p0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter$1;->val$event:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;

    iget-object v6, v6, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;->affectedStreet:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 168
    const-string v6, "street="

    invoke-virtual {v1, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 169
    iget-object v6, p0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter$1;->val$event:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;

    iget-object v6, v6, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;->affectedStreet:Ljava/lang/String;

    invoke-virtual {v1, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 170
    const-string v6, " , "

    invoke-virtual {v1, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 173
    :cond_4
    iget-object v6, p0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter$1;->val$event:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;

    iget-wide v6, v6, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;->distanceToIncident:J

    const-wide/16 v8, -0x1

    cmp-long v6, v6, v8

    if-eqz v6, :cond_5

    .line 174
    const-string v6, "dist="

    invoke-virtual {v1, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 175
    iget-object v6, p0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter$1;->val$event:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;

    iget-wide v6, v6, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;->distanceToIncident:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 176
    const-string v6, " , "

    invoke-virtual {v1, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 179
    :cond_5
    iget-object v6, p0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter$1;->val$event:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;

    iget-object v6, v6, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;->updated:Ljava/util/Date;

    if-eqz v6, :cond_6

    .line 180
    new-instance v6, Ljava/util/Date;

    invoke-direct {v6}, Ljava/util/Date;-><init>()V

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    iget-object v8, p0, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter$1;->val$event:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;

    iget-object v8, v8, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;->updated:Ljava/util/Date;

    invoke-virtual {v8}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    sub-long v2, v6, v8

    .line 181
    .local v2, "duration":J
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v7, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " minutes ago"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 184
    .end local v2    # "duration":J
    :cond_6
    # getter for: Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;->handler:Landroid/os/Handler;
    invoke-static {}, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter;->access$100()Landroid/os/Handler;

    move-result-object v6

    new-instance v7, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter$1$1;

    invoke-direct {v7, p0, v1}, Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter$1$1;-><init>(Lcom/navdy/hud/app/maps/widget/TrafficIncidentWidgetPresenter$1;Landroid/text/SpannableStringBuilder;)V

    invoke-virtual {v6, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 193
    return-void
.end method
