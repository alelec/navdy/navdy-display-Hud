.class public Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;
.super Ljava/lang/Object;
.source "MapEvents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/MapEvents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TrafficRerouteEvent"
.end annotation


# instance fields
.field public additionalVia:Ljava/lang/String;

.field public currentEta:J

.field public distanceDifference:J

.field public etaDifference:J

.field public newEta:J

.field public via:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;JJJJ)V
    .locals 1
    .param p1, "via"    # Ljava/lang/String;
    .param p2, "additionalVia"    # Ljava/lang/String;
    .param p3, "etaDifference"    # J
    .param p5, "currentEta"    # J
    .param p7, "distanceDifference"    # J
    .param p9, "newEta"    # J

    .prologue
    .line 246
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 247
    iput-object p1, p0, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;->via:Ljava/lang/String;

    .line 248
    iput-object p2, p0, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;->additionalVia:Ljava/lang/String;

    .line 249
    iput-wide p3, p0, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;->etaDifference:J

    .line 250
    iput-wide p5, p0, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;->currentEta:J

    .line 251
    iput-wide p7, p0, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;->distanceDifference:J

    .line 252
    iput-wide p9, p0, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;->newEta:J

    .line 253
    return-void
.end method
