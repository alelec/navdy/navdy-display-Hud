.class Lcom/navdy/hud/app/maps/MapSchemeController$1;
.super Landroid/os/Handler;
.source "MapSchemeController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/MapSchemeController;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/MapSchemeController;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/MapSchemeController;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/MapSchemeController;
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/navdy/hud/app/maps/MapSchemeController$1;->this$0:Lcom/navdy/hud/app/maps/MapSchemeController;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 91
    iget v0, p1, Landroid/os/Message;->what:I

    if-nez v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/navdy/hud/app/maps/MapSchemeController$1;->this$0:Lcom/navdy/hud/app/maps/MapSchemeController;

    # invokes: Lcom/navdy/hud/app/maps/MapSchemeController;->collectLightSensorSample()V
    invoke-static {v0}, Lcom/navdy/hud/app/maps/MapSchemeController;->access$000(Lcom/navdy/hud/app/maps/MapSchemeController;)V

    .line 93
    iget-object v0, p0, Lcom/navdy/hud/app/maps/MapSchemeController$1;->this$0:Lcom/navdy/hud/app/maps/MapSchemeController;

    # invokes: Lcom/navdy/hud/app/maps/MapSchemeController;->evaluateCollectedSamples()V
    invoke-static {v0}, Lcom/navdy/hud/app/maps/MapSchemeController;->access$100(Lcom/navdy/hud/app/maps/MapSchemeController;)V

    .line 94
    const/4 v0, 0x0

    const-wide/16 v2, 0x2710

    invoke-virtual {p0, v0, v2, v3}, Lcom/navdy/hud/app/maps/MapSchemeController$1;->sendEmptyMessageDelayed(IJ)Z

    .line 96
    :cond_0
    return-void
.end method
