.class public final Lcom/navdy/hud/app/maps/MapsEventHandler$$InjectAdapter;
.super Ldagger/internal/Binding;
.source "MapsEventHandler$$InjectAdapter.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldagger/internal/Binding",
        "<",
        "Lcom/navdy/hud/app/maps/MapsEventHandler;",
        ">;",
        "Ldagger/MembersInjector",
        "<",
        "Lcom/navdy/hud/app/maps/MapsEventHandler;",
        ">;"
    }
.end annotation


# instance fields
.field private bus:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/squareup/otto/Bus;",
            ">;"
        }
    .end annotation
.end field

.field private connectionHandler:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/service/ConnectionHandler;",
            ">;"
        }
    .end annotation
.end field

.field private mDriverProfileManager:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/profile/DriverProfileManager;",
            ">;"
        }
    .end annotation
.end field

.field private sharedPreferences:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Landroid/content/SharedPreferences;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 30
    const/4 v0, 0x0

    const-string v1, "members/com.navdy.hud.app.maps.MapsEventHandler"

    const/4 v2, 0x0

    const-class v3, Lcom/navdy/hud/app/maps/MapsEventHandler;

    invoke-direct {p0, v0, v1, v2, v3}, Ldagger/internal/Binding;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/Object;)V

    .line 31
    return-void
.end method


# virtual methods
.method public attach(Ldagger/internal/Linker;)V
    .locals 3
    .param p1, "linker"    # Ldagger/internal/Linker;

    .prologue
    .line 40
    const-string v0, "com.navdy.hud.app.service.ConnectionHandler"

    const-class v1, Lcom/navdy/hud/app/maps/MapsEventHandler;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/maps/MapsEventHandler$$InjectAdapter;->connectionHandler:Ldagger/internal/Binding;

    .line 41
    const-string v0, "com.squareup.otto.Bus"

    const-class v1, Lcom/navdy/hud/app/maps/MapsEventHandler;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/maps/MapsEventHandler$$InjectAdapter;->bus:Ldagger/internal/Binding;

    .line 42
    const-string v0, "android.content.SharedPreferences"

    const-class v1, Lcom/navdy/hud/app/maps/MapsEventHandler;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/maps/MapsEventHandler$$InjectAdapter;->sharedPreferences:Ldagger/internal/Binding;

    .line 43
    const-string v0, "com.navdy.hud.app.profile.DriverProfileManager"

    const-class v1, Lcom/navdy/hud/app/maps/MapsEventHandler;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/maps/MapsEventHandler$$InjectAdapter;->mDriverProfileManager:Ldagger/internal/Binding;

    .line 44
    return-void
.end method

.method public getDependencies(Ljava/util/Set;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ldagger/internal/Binding",
            "<*>;>;",
            "Ljava/util/Set",
            "<",
            "Ldagger/internal/Binding",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 52
    .local p1, "getBindings":Ljava/util/Set;, "Ljava/util/Set<Ldagger/internal/Binding<*>;>;"
    .local p2, "injectMembersBindings":Ljava/util/Set;, "Ljava/util/Set<Ldagger/internal/Binding<*>;>;"
    iget-object v0, p0, Lcom/navdy/hud/app/maps/MapsEventHandler$$InjectAdapter;->connectionHandler:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 53
    iget-object v0, p0, Lcom/navdy/hud/app/maps/MapsEventHandler$$InjectAdapter;->bus:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 54
    iget-object v0, p0, Lcom/navdy/hud/app/maps/MapsEventHandler$$InjectAdapter;->sharedPreferences:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 55
    iget-object v0, p0, Lcom/navdy/hud/app/maps/MapsEventHandler$$InjectAdapter;->mDriverProfileManager:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 56
    return-void
.end method

.method public injectMembers(Lcom/navdy/hud/app/maps/MapsEventHandler;)V
    .locals 1
    .param p1, "object"    # Lcom/navdy/hud/app/maps/MapsEventHandler;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/navdy/hud/app/maps/MapsEventHandler$$InjectAdapter;->connectionHandler:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/service/ConnectionHandler;

    iput-object v0, p1, Lcom/navdy/hud/app/maps/MapsEventHandler;->connectionHandler:Lcom/navdy/hud/app/service/ConnectionHandler;

    .line 65
    iget-object v0, p0, Lcom/navdy/hud/app/maps/MapsEventHandler$$InjectAdapter;->bus:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/otto/Bus;

    iput-object v0, p1, Lcom/navdy/hud/app/maps/MapsEventHandler;->bus:Lcom/squareup/otto/Bus;

    .line 66
    iget-object v0, p0, Lcom/navdy/hud/app/maps/MapsEventHandler$$InjectAdapter;->sharedPreferences:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p1, Lcom/navdy/hud/app/maps/MapsEventHandler;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 67
    iget-object v0, p0, Lcom/navdy/hud/app/maps/MapsEventHandler$$InjectAdapter;->mDriverProfileManager:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/profile/DriverProfileManager;

    iput-object v0, p1, Lcom/navdy/hud/app/maps/MapsEventHandler;->mDriverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

    .line 68
    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lcom/navdy/hud/app/maps/MapsEventHandler;

    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/maps/MapsEventHandler$$InjectAdapter;->injectMembers(Lcom/navdy/hud/app/maps/MapsEventHandler;)V

    return-void
.end method
