.class public Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;
.super Ljava/lang/Object;
.source "MapEvents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/MapEvents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RegionEvent"
.end annotation


# instance fields
.field public final country:Ljava/lang/String;

.field public final state:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "state"    # Ljava/lang/String;
    .param p2, "country"    # Ljava/lang/String;

    .prologue
    .line 309
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 310
    iput-object p1, p0, Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;->state:Ljava/lang/String;

    .line 311
    iput-object p2, p0, Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;->country:Ljava/lang/String;

    .line 312
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 316
    if-ne p1, p0, :cond_1

    .line 324
    :cond_0
    :goto_0
    return v2

    .line 318
    :cond_1
    instance-of v4, p1, Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;

    if-nez v4, :cond_2

    move v2, v3

    .line 319
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 321
    check-cast v0, Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;

    .line 323
    .local v0, "r":Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;
    iget-object v4, p0, Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;->state:Ljava/lang/String;

    if-nez v4, :cond_5

    iget-object v4, v0, Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;->state:Ljava/lang/String;

    if-nez v4, :cond_4

    move v1, v2

    .line 324
    .local v1, "stateEquals":Z
    :goto_1
    if-eqz v1, :cond_3

    iget-object v4, p0, Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;->country:Ljava/lang/String;

    if-nez v4, :cond_6

    iget-object v4, v0, Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;->country:Ljava/lang/String;

    if-eqz v4, :cond_0

    :cond_3
    move v2, v3

    goto :goto_0

    .end local v1    # "stateEquals":Z
    :cond_4
    move v1, v3

    .line 323
    goto :goto_1

    :cond_5
    iget-object v4, p0, Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;->state:Ljava/lang/String;

    iget-object v5, v0, Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;->state:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_1

    .line 324
    .restart local v1    # "stateEquals":Z
    :cond_6
    iget-object v4, p0, Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;->country:Ljava/lang/String;

    iget-object v5, v0, Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;->country:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 330
    const/16 v1, 0x11

    .line 331
    .local v1, "result":I
    iget-object v4, p0, Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;->state:Ljava/lang/String;

    if-nez v4, :cond_0

    move v2, v3

    .line 332
    .local v2, "stateHash":I
    :goto_0
    iget-object v4, p0, Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;->country:Ljava/lang/String;

    if-nez v4, :cond_1

    move v0, v3

    .line 334
    .local v0, "countryHash":I
    :goto_1
    add-int/lit16 v1, v2, 0x20f

    .line 335
    mul-int/lit8 v3, v1, 0x1f

    add-int v1, v3, v0

    .line 337
    return v1

    .line 331
    .end local v0    # "countryHash":I
    .end local v2    # "stateHash":I
    :cond_0
    iget-object v4, p0, Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;->state:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 332
    .restart local v2    # "stateHash":I
    :cond_1
    iget-object v3, p0, Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;->country:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 342
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RegionEvent: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;->state:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;->country:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
