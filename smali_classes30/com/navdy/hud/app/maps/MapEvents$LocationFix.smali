.class public Lcom/navdy/hud/app/maps/MapEvents$LocationFix;
.super Ljava/lang/Object;
.source "MapEvents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/MapEvents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LocationFix"
.end annotation


# instance fields
.field public locationAvailable:Z

.field public usingLocalGpsLocation:Z

.field public usingPhoneLocation:Z


# direct methods
.method public constructor <init>(ZZZ)V
    .locals 0
    .param p1, "locationAvailable"    # Z
    .param p2, "usingPhoneLocation"    # Z
    .param p3, "usingLocalGpsLocation"    # Z

    .prologue
    .line 408
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 409
    iput-boolean p1, p0, Lcom/navdy/hud/app/maps/MapEvents$LocationFix;->locationAvailable:Z

    .line 410
    iput-boolean p2, p0, Lcom/navdy/hud/app/maps/MapEvents$LocationFix;->usingPhoneLocation:Z

    .line 411
    iput-boolean p3, p0, Lcom/navdy/hud/app/maps/MapEvents$LocationFix;->usingLocalGpsLocation:Z

    .line 412
    return-void
.end method
