.class public final enum Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteAction;
.super Ljava/lang/Enum;
.source "MapEvents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/MapEvents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TrafficRerouteAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteAction;

.field public static final enum DISMISS:Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteAction;

.field public static final enum REROUTE:Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteAction;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 257
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteAction;

    const-string v1, "REROUTE"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteAction;->REROUTE:Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteAction;

    .line 258
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteAction;

    const-string v1, "DISMISS"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteAction;->DISMISS:Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteAction;

    .line 256
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteAction;

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteAction;->REROUTE:Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteAction;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteAction;->DISMISS:Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteAction;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteAction;->$VALUES:[Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 256
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 256
    const-class v0, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteAction;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteAction;
    .locals 1

    .prologue
    .line 256
    sget-object v0, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteAction;->$VALUES:[Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteAction;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteAction;

    return-object v0
.end method
