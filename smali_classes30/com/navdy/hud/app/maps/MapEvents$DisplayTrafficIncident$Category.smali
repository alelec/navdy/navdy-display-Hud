.class public final enum Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;
.super Ljava/lang/Enum;
.source "MapEvents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Category"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

.field public static final enum ACCIDENT:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

.field public static final enum CLOSURE:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

.field public static final enum CONGESTION:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

.field public static final enum FLOW:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

.field public static final enum OTHER:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

.field public static final enum ROADWORKS:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

.field public static final enum UNDEFINED:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 468
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    const-string v1, "ACCIDENT"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;->ACCIDENT:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    .line 469
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    const-string v1, "CLOSURE"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;->CLOSURE:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    .line 470
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    const-string v1, "ROADWORKS"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;->ROADWORKS:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    .line 471
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    const-string v1, "CONGESTION"

    invoke-direct {v0, v1, v6}, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;->CONGESTION:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    .line 472
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    const-string v1, "FLOW"

    invoke-direct {v0, v1, v7}, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;->FLOW:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    .line 473
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    const-string v1, "UNDEFINED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;->UNDEFINED:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    .line 474
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    const-string v1, "OTHER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;->OTHER:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    .line 467
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;->ACCIDENT:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;->CLOSURE:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;->ROADWORKS:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;->CONGESTION:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;->FLOW:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;->UNDEFINED:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;->OTHER:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;->$VALUES:[Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 467
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 467
    const-class v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;
    .locals 1

    .prologue
    .line 467
    sget-object v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;->$VALUES:[Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    return-object v0
.end method
