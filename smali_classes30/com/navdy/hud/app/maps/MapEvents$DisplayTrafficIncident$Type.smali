.class public final enum Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;
.super Ljava/lang/Enum;
.source "MapEvents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

.field public static final enum BLOCKING:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

.field public static final enum FAILED:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

.field public static final enum HIGH:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

.field public static final enum INACTIVE:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

.field public static final enum NORMAL:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

.field public static final enum VERY_HIGH:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 458
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

    const-string v1, "BLOCKING"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;->BLOCKING:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

    .line 459
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

    const-string v1, "VERY_HIGH"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;->VERY_HIGH:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

    .line 460
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

    const-string v1, "HIGH"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;->HIGH:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

    .line 461
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v6}, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;->NORMAL:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

    .line 463
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v7}, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;->FAILED:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

    .line 464
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

    const-string v1, "INACTIVE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;->INACTIVE:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

    .line 457
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;->BLOCKING:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;->VERY_HIGH:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;->HIGH:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;->NORMAL:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;->FAILED:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;->INACTIVE:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;->$VALUES:[Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 457
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 457
    const-class v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;
    .locals 1

    .prologue
    .line 457
    sget-object v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;->$VALUES:[Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

    return-object v0
.end method
