.class public Lcom/navdy/hud/app/maps/MapSettings;
.super Ljava/lang/Object;
.source "MapSettings.java"


# static fields
.field private static final CUSTOM_ANIMATION:Ljava/lang/String; = "persist.sys.custom_animation"

.field private static final DEBUG_HERE_LOC:Ljava/lang/String; = "persist.sys.dbg_here_loc"

.field private static final DEFAULT_MAP_FPS:I = 0x5

.field private static final FULL_CUSTOM_ANIMATION:Ljava/lang/String; = "persist.sys.full_animation"

.field private static final GENERATE_ROUTE_ICONS:Ljava/lang/String; = "persist.sys.route_icons"

.field private static final GLYMPSE_DURATION:Ljava/lang/String; = "persist.sys.glympse_dur"

.field private static final LANE_GUIDANCE_ENABLED:Ljava/lang/String; = "persist.sys.lane_info_enabled"

.field private static final MAP_FPS:Ljava/lang/String; = "persist.sys.map_fps"

.field private static final NO_TURN_TEXT_IN_TBT:Ljava/lang/String; = "persist.sys.no_turn_text"

.field private static final TBT_ONTO_DISABLED:Ljava/lang/String; = "persist.sys.map.tbt.disableonto"

.field private static final TRAFFIC_WIDGETS_ENABLED:Ljava/lang/String; = "persist.sys.map.traffic.widgets"

.field private static final TTS_RECALCULATION_DISABLED:Ljava/lang/String; = "persist.sys.map.tts.norecalc"

.field private static final customAnimationEnabled:Z

.field private static final debugHereLocation:Z

.field private static final dontShowTurnText:Z

.field private static final fps:I

.field private static final fullCustomAnimatonEnabled:Z

.field private static final generateRouteIcons:Z

.field private static final glympseDuration:I

.field private static final laneGuidanceEnabled:Z

.field private static simulationSpeed:I

.field private static final tbtOntoDisabled:Z

.field private static final trafficDashWidgetsEnabled:Z

.field private static final ttsDisableRecalculating:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 89
    const-string v1, "persist.sys.map.traffic.widgets"

    invoke-static {v1, v3}, Lcom/navdy/hud/app/util/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/navdy/hud/app/maps/MapSettings;->trafficDashWidgetsEnabled:Z

    .line 90
    const-string v1, "persist.sys.map.tbt.disableonto"

    invoke-static {v1, v3}, Lcom/navdy/hud/app/util/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/navdy/hud/app/maps/MapSettings;->tbtOntoDisabled:Z

    .line 91
    const-string v1, "persist.sys.map.tts.norecalc"

    invoke-static {v1, v3}, Lcom/navdy/hud/app/util/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/navdy/hud/app/maps/MapSettings;->ttsDisableRecalculating:Z

    .line 92
    const-string v1, "persist.sys.custom_animation"

    invoke-static {v1, v3}, Lcom/navdy/hud/app/util/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/navdy/hud/app/maps/MapSettings;->customAnimationEnabled:Z

    .line 93
    const-string v1, "persist.sys.full_animation"

    invoke-static {v1, v3}, Lcom/navdy/hud/app/util/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/navdy/hud/app/maps/MapSettings;->fullCustomAnimatonEnabled:Z

    .line 94
    const-string v1, "persist.sys.map_fps"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lcom/navdy/hud/app/util/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v1

    sput v1, Lcom/navdy/hud/app/maps/MapSettings;->fps:I

    .line 95
    const-string v1, "persist.sys.lane_info_enabled"

    invoke-static {v1, v3}, Lcom/navdy/hud/app/util/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/navdy/hud/app/maps/MapSettings;->laneGuidanceEnabled:Z

    .line 96
    const-string v1, "persist.sys.route_icons"

    invoke-static {v1, v3}, Lcom/navdy/hud/app/util/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/navdy/hud/app/maps/MapSettings;->generateRouteIcons:Z

    .line 97
    const-string v1, "persist.sys.dbg_here_loc"

    invoke-static {v1, v3}, Lcom/navdy/hud/app/util/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/navdy/hud/app/maps/MapSettings;->debugHereLocation:Z

    .line 98
    const-string v1, "persist.sys.no_turn_text"

    invoke-static {v1, v3}, Lcom/navdy/hud/app/util/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/navdy/hud/app/maps/MapSettings;->dontShowTurnText:Z

    .line 100
    const-string v1, "persist.sys.glympse_dur"

    invoke-static {v1, v3}, Lcom/navdy/hud/app/util/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 101
    .local v0, "minutes":I
    if-lez v0, :cond_0

    const/16 v1, 0x5a0

    if-gt v0, v1, :cond_0

    .line 102
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    long-to-int v1, v2

    sput v1, Lcom/navdy/hud/app/maps/MapSettings;->glympseDuration:I

    .line 106
    :goto_0
    return-void

    .line 104
    :cond_0
    sput v3, Lcom/navdy/hud/app/maps/MapSettings;->glympseDuration:I

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static doNotShowTurnTextInTBT()Z
    .locals 1

    .prologue
    .line 153
    const/4 v0, 0x1

    return v0
.end method

.method public static getGlympseDuration()I
    .locals 1

    .prologue
    .line 158
    sget v0, Lcom/navdy/hud/app/maps/MapSettings;->glympseDuration:I

    if-lez v0, :cond_0

    .line 159
    sget v0, Lcom/navdy/hud/app/maps/MapSettings;->glympseDuration:I

    .line 161
    :goto_0
    return v0

    :cond_0
    sget v0, Lcom/navdy/hud/app/framework/glympse/GlympseManager;->GLYMPSE_DURATION:I

    goto :goto_0
.end method

.method public static getMapFps()I
    .locals 1

    .prologue
    .line 129
    sget v0, Lcom/navdy/hud/app/maps/MapSettings;->fps:I

    return v0
.end method

.method public static getSimulationSpeed()I
    .locals 1

    .prologue
    .line 141
    sget v0, Lcom/navdy/hud/app/maps/MapSettings;->simulationSpeed:I

    return v0
.end method

.method public static isCustomAnimationEnabled()Z
    .locals 1

    .prologue
    .line 121
    sget-boolean v0, Lcom/navdy/hud/app/maps/MapSettings;->customAnimationEnabled:Z

    return v0
.end method

.method public static isDebugHereLocation()Z
    .locals 1

    .prologue
    .line 149
    sget-boolean v0, Lcom/navdy/hud/app/maps/MapSettings;->debugHereLocation:Z

    return v0
.end method

.method public static isFullCustomAnimatonEnabled()Z
    .locals 1

    .prologue
    .line 125
    sget-boolean v0, Lcom/navdy/hud/app/maps/MapSettings;->fullCustomAnimatonEnabled:Z

    return v0
.end method

.method public static isGenerateRouteIcons()Z
    .locals 1

    .prologue
    .line 145
    sget-boolean v0, Lcom/navdy/hud/app/maps/MapSettings;->generateRouteIcons:Z

    return v0
.end method

.method public static isLaneGuidanceEnabled()Z
    .locals 1

    .prologue
    .line 133
    sget-boolean v0, Lcom/navdy/hud/app/maps/MapSettings;->laneGuidanceEnabled:Z

    return v0
.end method

.method public static isTbtOntoDisabled()Z
    .locals 1

    .prologue
    .line 113
    sget-boolean v0, Lcom/navdy/hud/app/maps/MapSettings;->tbtOntoDisabled:Z

    if-nez v0, :cond_0

    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isUserBuild()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isTrafficDashWidgetsEnabled()Z
    .locals 1

    .prologue
    .line 109
    sget-boolean v0, Lcom/navdy/hud/app/maps/MapSettings;->trafficDashWidgetsEnabled:Z

    return v0
.end method

.method public static isTtsRecalculationDisabled()Z
    .locals 1

    .prologue
    .line 117
    sget-boolean v0, Lcom/navdy/hud/app/maps/MapSettings;->ttsDisableRecalculating:Z

    return v0
.end method

.method public static setSimulationSpeed(I)V
    .locals 0
    .param p0, "n"    # I

    .prologue
    .line 137
    sput p0, Lcom/navdy/hud/app/maps/MapSettings;->simulationSpeed:I

    .line 138
    return-void
.end method
