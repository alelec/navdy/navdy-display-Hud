.class public Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;
.super Lcom/navdy/hud/app/maps/notification/BaseTrafficNotification;
.source "RouteTimeUpdateNotification.java"


# static fields
.field private static final TAG_DISMISS:I = 0x66

.field private static final TAG_GO:I = 0x65

.field private static final TAG_READ:I = 0x67

.field private static ampmMarker:Ljava/lang/StringBuilder;

.field private static badTrafficColor:I

.field private static delay:Ljava/lang/String;

.field private static delayChoices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;",
            ">;"
        }
    .end annotation
.end field

.field private static done:Ljava/lang/String;

.field private static fasterRoute:Ljava/lang/String;

.field private static fasterRouteAvailable:Ljava/lang/String;

.field private static go:Ljava/lang/String;

.field private static hour:Ljava/lang/String;

.field private static hours:Ljava/lang/String;

.field private static hr:Ljava/lang/String;

.field private static min:Ljava/lang/String;

.field private static minute:Ljava/lang/String;

.field private static minutes:Ljava/lang/String;

.field private static normalTrafficColor:I

.field private static reRouteChoices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;",
            ">;"
        }
    .end annotation
.end field

.field private static resources:Landroid/content/res/Resources;

.field private static save:Ljava/lang/String;

.field private static trafficRerouteColor:I

.field private static trafficUpdate:Ljava/lang/String;

.field private static via:Ljava/lang/String;


# instance fields
.field private bus:Lcom/squareup/otto/Bus;

.field private cancelSpeechRequest:Lcom/navdy/service/library/events/audio/CancelSpeechRequest;

.field private choiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

.field private container:Landroid/view/ViewGroup;

.field private distanceDiffStr:Ljava/lang/String;

.field private distanceStr:Ljava/lang/String;

.field private etaStr:Ljava/lang/String;

.field private etaUnitStr:Ljava/lang/String;

.field private extendedContainer:Landroid/view/ViewGroup;

.field private fasterRouteEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;

.field private id:Ljava/lang/String;

.field private mainImage:Lcom/navdy/hud/app/ui/component/image/ColorImageView;

.field private mainTitle:Landroid/widget/TextView;

.field private sideImage:Landroid/widget/ImageView;

.field private subTitle:Landroid/widget/TextView;

.field private text1:Landroid/widget/TextView;

.field private text2:Landroid/widget/TextView;

.field private text3:Landroid/widget/TextView;

.field private timeStr:Ljava/lang/String;

.field private timeStrExpanded:Ljava/lang/String;

.field private timeUnitStr:Ljava/lang/String;

.field private trafficDelayEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficDelayEvent;

.field private ttsMessage:Ljava/lang/String;

.field private type:Lcom/navdy/hud/app/framework/notifications/NotificationType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 72
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->delayChoices:Ljava/util/List;

    .line 73
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->reRouteChoices:Ljava/util/List;

    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->ampmMarker:Ljava/lang/StringBuilder;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/navdy/hud/app/framework/notifications/NotificationType;Lcom/squareup/otto/Bus;)V
    .locals 22
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "type"    # Lcom/navdy/hud/app/framework/notifications/NotificationType;
    .param p3, "bus"    # Lcom/squareup/otto/Bus;

    .prologue
    .line 157
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/maps/notification/BaseTrafficNotification;-><init>()V

    .line 98
    const-string v2, ""

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->etaStr:Ljava/lang/String;

    .line 99
    const-string v2, ""

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->etaUnitStr:Ljava/lang/String;

    .line 100
    const-string v2, ""

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->timeStr:Ljava/lang/String;

    .line 101
    const-string v2, ""

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->timeUnitStr:Ljava/lang/String;

    .line 102
    const-string v2, ""

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->timeStrExpanded:Ljava/lang/String;

    .line 103
    const-string v2, ""

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->distanceStr:Ljava/lang/String;

    .line 104
    const-string v2, ""

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->distanceDiffStr:Ljava/lang/String;

    .line 110
    new-instance v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification$1;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification$1;-><init>(Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->choiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

    .line 158
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->id:Ljava/lang/String;

    .line 159
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->type:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    .line 160
    new-instance v2, Lcom/navdy/service/library/events/audio/CancelSpeechRequest;

    move-object/from16 v0, p1

    invoke-direct {v2, v0}, Lcom/navdy/service/library/events/audio/CancelSpeechRequest;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->cancelSpeechRequest:Lcom/navdy/service/library/events/audio/CancelSpeechRequest;

    .line 162
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->trafficUpdate:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 163
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sput-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->resources:Landroid/content/res/Resources;

    .line 165
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f09029c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->trafficUpdate:Ljava/lang/String;

    .line 166
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0902a3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->fasterRoute:Ljava/lang/String;

    .line 167
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f09029b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->delay:Ljava/lang/String;

    .line 168
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0900df

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->done:Ljava/lang/String;

    .line 169
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0901ab

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->min:Ljava/lang/String;

    .line 170
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f09017e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->hr:Ljava/lang/String;

    .line 171
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f090295

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->fasterRouteAvailable:Ljava/lang/String;

    .line 172
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f09017c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->hour:Ljava/lang/String;

    .line 173
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f09017d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->hours:Ljava/lang/String;

    .line 174
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0901ad

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->minute:Ljava/lang/String;

    .line 175
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0901ae

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->minutes:Ljava/lang/String;

    .line 177
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f09029e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->save:Ljava/lang/String;

    .line 178
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0902a4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->go:Ljava/lang/String;

    .line 179
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0902a7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->via:Ljava/lang/String;

    .line 181
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0d00b8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sput v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->normalTrafficColor:I

    .line 182
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0d00b6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sput v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->badTrafficColor:I

    .line 183
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0d002e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sput v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->trafficRerouteColor:I

    .line 185
    const/high16 v7, -0x1000000

    .line 186
    .local v7, "unselectedColor":I
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0d0021

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    .line 187
    .local v5, "dismissColor":I
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0d0024

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v12

    .line 188
    .local v12, "readColor":I
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0d0025

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v21

    .line 189
    .local v21, "goColor":I
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0900dc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 191
    .local v8, "dismiss":Ljava/lang/String;
    sget-object v10, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->delayChoices:Ljava/util/List;

    new-instance v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const/16 v3, 0x66

    const v4, 0x7f020125

    const v6, 0x7f020125

    move v9, v5

    invoke-direct/range {v2 .. v9}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-interface {v10, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 193
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->reRouteChoices:Ljava/util/List;

    new-instance v9, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const/16 v10, 0x67

    const v11, 0x7f02012a

    const v13, 0x7f02012a

    sget-object v15, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->read:Ljava/lang/String;

    move v14, v7

    move/from16 v16, v12

    invoke-direct/range {v9 .. v16}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-interface {v2, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 194
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->reRouteChoices:Ljava/util/List;

    new-instance v13, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const/16 v14, 0x65

    const v15, 0x7f020129

    const v17, 0x7f020129

    sget-object v19, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->go:Ljava/lang/String;

    move/from16 v16, v21

    move/from16 v18, v7

    move/from16 v20, v21

    invoke-direct/range {v13 .. v20}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-interface {v2, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 195
    sget-object v10, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->reRouteChoices:Ljava/util/List;

    new-instance v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const/16 v3, 0x66

    const v4, 0x7f020125

    const v6, 0x7f020125

    move v9, v5

    invoke-direct/range {v2 .. v9}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-interface {v10, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 197
    .end local v5    # "dismissColor":I
    .end local v7    # "unselectedColor":I
    .end local v8    # "dismiss":Ljava/lang/String;
    .end local v12    # "readColor":I
    .end local v21    # "goColor":I
    :cond_0
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->bus:Lcom/squareup/otto/Bus;

    .line 198
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 199
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;)Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->fasterRouteEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;

    return-object v0
.end method

.method static synthetic access$002(Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;)Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;
    .param p1, "x1"    # Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->fasterRouteEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;

    return-object p1
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;)Lcom/squareup/otto/Bus;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->bus:Lcom/squareup/otto/Bus;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;)Lcom/navdy/hud/app/maps/MapEvents$TrafficDelayEvent;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->trafficDelayEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficDelayEvent;

    return-object v0
.end method

.method static synthetic access$202(Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;Lcom/navdy/hud/app/maps/MapEvents$TrafficDelayEvent;)Lcom/navdy/hud/app/maps/MapEvents$TrafficDelayEvent;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;
    .param p1, "x1"    # Lcom/navdy/hud/app/maps/MapEvents$TrafficDelayEvent;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->trafficDelayEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficDelayEvent;

    return-object p1
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->switchToExpandedMode()V

    return-void
.end method

.method private buildData()V
    .locals 14

    .prologue
    .line 471
    iget-object v5, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->fasterRouteEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;

    if-eqz v5, :cond_3

    .line 472
    iget-object v5, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->fasterRouteEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;

    iget-wide v8, v5, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;->etaDifference:J

    const-wide/16 v10, 0x0

    cmp-long v5, v8, v10

    if-eqz v5, :cond_0

    .line 473
    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v8, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->fasterRouteEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;

    iget-wide v8, v8, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;->etaDifference:J

    invoke-virtual {v5, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v6

    .line 474
    .local v6, "val":J
    const-wide/16 v8, 0x63

    cmp-long v5, v6, v8

    if-lez v5, :cond_5

    .line 475
    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v8, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->fasterRouteEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;

    iget-wide v8, v8, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;->etaDifference:J

    invoke-virtual {v5, v8, v9}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v6

    .line 476
    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->timeStr:Ljava/lang/String;

    .line 477
    sget-object v5, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->hr:Ljava/lang/String;

    iput-object v5, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->timeUnitStr:Ljava/lang/String;

    .line 478
    const-wide/16 v8, 0x1

    cmp-long v5, v6, v8

    if-lez v5, :cond_4

    .line 479
    sget-object v5, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->hours:Ljava/lang/String;

    iput-object v5, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->timeStrExpanded:Ljava/lang/String;

    .line 494
    .end local v6    # "val":J
    :cond_0
    :goto_0
    const/4 v3, 0x0

    .line 495
    .local v3, "etaDate":Ljava/util/Date;
    iget-object v5, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->fasterRouteEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;

    iget-wide v8, v5, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;->newEta:J

    const-wide/16 v10, 0x0

    cmp-long v5, v8, v10

    if-eqz v5, :cond_7

    .line 496
    new-instance v3, Ljava/util/Date;

    .end local v3    # "etaDate":Ljava/util/Date;
    iget-object v5, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->fasterRouteEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;

    iget-wide v8, v5, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;->newEta:J

    invoke-direct {v3, v8, v9}, Ljava/util/Date;-><init>(J)V

    .line 501
    .restart local v3    # "etaDate":Ljava/util/Date;
    :cond_1
    :goto_1
    if-eqz v3, :cond_2

    .line 502
    sget-object v5, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->ampmMarker:Ljava/lang/StringBuilder;

    const/4 v8, 0x0

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 503
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getTimeHelper()Lcom/navdy/hud/app/common/TimeHelper;

    move-result-object v4

    .line 504
    .local v4, "timeHelper":Lcom/navdy/hud/app/common/TimeHelper;
    sget-object v5, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->ampmMarker:Ljava/lang/StringBuilder;

    const/4 v8, 0x0

    invoke-virtual {v4, v3, v5, v8}, Lcom/navdy/hud/app/common/TimeHelper;->formatTime12Hour(Ljava/util/Date;Ljava/lang/StringBuilder;Z)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->etaStr:Ljava/lang/String;

    .line 505
    sget-object v5, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->ampmMarker:Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->etaUnitStr:Ljava/lang/String;

    .line 508
    .end local v4    # "timeHelper":Lcom/navdy/hud/app/common/TimeHelper;
    :cond_2
    iget-object v5, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->fasterRouteEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;

    iget-wide v0, v5, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;->distanceDifference:J

    .line 510
    .local v0, "diff":J
    const-wide/16 v8, 0x0

    cmp-long v5, v0, v8

    if-lez v5, :cond_8

    .line 511
    sget-object v5, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->resources:Landroid/content/res/Resources;

    const v8, 0x7f0901a0

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->distanceDiffStr:Ljava/lang/String;

    .line 517
    :goto_2
    new-instance v2, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    invoke-direct {v2}, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;-><init>()V

    .line 518
    .local v2, "display":Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    const/4 v5, 0x1

    const/4 v8, 0x1

    invoke-static {v0, v1, v2, v5, v8}, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->setNavigationDistance(JLcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;ZZ)V

    .line 519
    iget-object v5, v2, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->distanceToPendingRoadText:Ljava/lang/String;

    iput-object v5, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->distanceStr:Ljava/lang/String;

    .line 521
    iget-object v5, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->fasterRouteEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;

    iget-wide v8, v5, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;->etaDifference:J

    const-wide/16 v10, 0x0

    cmp-long v5, v8, v10

    if-eqz v5, :cond_9

    .line 522
    sget-object v5, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->resources:Landroid/content/res/Resources;

    const v8, 0x7f090296

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->timeStr:Ljava/lang/String;

    aput-object v11, v9, v10

    const/4 v10, 0x1

    iget-object v11, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->timeStrExpanded:Ljava/lang/String;

    aput-object v11, v9, v10

    invoke-virtual {v5, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->ttsMessage:Ljava/lang/String;

    .line 527
    .end local v0    # "diff":J
    .end local v2    # "display":Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    .end local v3    # "etaDate":Ljava/util/Date;
    :cond_3
    :goto_3
    return-void

    .line 481
    .restart local v6    # "val":J
    :cond_4
    sget-object v5, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->hour:Ljava/lang/String;

    iput-object v5, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->timeStrExpanded:Ljava/lang/String;

    goto :goto_0

    .line 484
    :cond_5
    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->timeStr:Ljava/lang/String;

    .line 485
    sget-object v5, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->min:Ljava/lang/String;

    iput-object v5, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->timeUnitStr:Ljava/lang/String;

    .line 486
    const-wide/16 v8, 0x1

    cmp-long v5, v6, v8

    if-lez v5, :cond_6

    .line 487
    sget-object v5, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->minutes:Ljava/lang/String;

    iput-object v5, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->timeStrExpanded:Ljava/lang/String;

    goto/16 :goto_0

    .line 489
    :cond_6
    sget-object v5, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->minute:Ljava/lang/String;

    iput-object v5, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->timeStrExpanded:Ljava/lang/String;

    goto/16 :goto_0

    .line 497
    .end local v6    # "val":J
    .restart local v3    # "etaDate":Ljava/util/Date;
    :cond_7
    iget-object v5, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->fasterRouteEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;

    iget-wide v8, v5, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;->currentEta:J

    const-wide/16 v10, 0x0

    cmp-long v5, v8, v10

    if-eqz v5, :cond_1

    .line 498
    new-instance v3, Ljava/util/Date;

    .end local v3    # "etaDate":Ljava/util/Date;
    iget-object v5, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->fasterRouteEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;

    iget-wide v8, v5, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;->currentEta:J

    iget-object v5, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->fasterRouteEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;

    iget-wide v10, v5, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;->etaDifference:J

    const-wide/16 v12, 0x3e8

    mul-long/2addr v10, v12

    sub-long/2addr v8, v10

    invoke-direct {v3, v8, v9}, Ljava/util/Date;-><init>(J)V

    .restart local v3    # "etaDate":Ljava/util/Date;
    goto/16 :goto_1

    .line 513
    .restart local v0    # "diff":J
    :cond_8
    sget-object v5, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->resources:Landroid/content/res/Resources;

    const v8, 0x7f09025f

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->distanceDiffStr:Ljava/lang/String;

    .line 514
    neg-long v0, v0

    goto :goto_2

    .line 524
    .restart local v2    # "display":Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    :cond_9
    sget-object v5, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->resources:Landroid/content/res/Resources;

    const v8, 0x7f090297

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->ttsMessage:Ljava/lang/String;

    goto :goto_3
.end method

.method private cancelTts()V
    .locals 3

    .prologue
    .line 543
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->isTtsOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 544
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "tts-cancelled ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 545
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/event/RemoteEvent;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->cancelSpeechRequest:Lcom/navdy/service/library/events/audio/CancelSpeechRequest;

    invoke-direct {v1, v2}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 547
    :cond_0
    return-void
.end method

.method private cleanupView(Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;Landroid/view/ViewGroup;)V
    .locals 5
    .param p1, "viewType"    # Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;
    .param p2, "viewGroup"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v4, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    .line 550
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 551
    .local v0, "parent":Landroid/view/ViewGroup;
    if-eqz v0, :cond_0

    .line 552
    invoke-virtual {v0, p2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 555
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification$2;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceViewCache$ViewType:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 576
    :goto_0
    invoke-static {p1, p2}, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->putView(Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;Landroid/view/View;)V

    .line 577
    return-void

    .line 557
    :pswitch_0
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->mainTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setAlpha(F)V

    .line 558
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->mainTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 560
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->subTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setAlpha(F)V

    .line 561
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->subTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 563
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v1, v3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setAlpha(F)V

    .line 564
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v1, v4}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setVisibility(I)V

    .line 565
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setTag(Ljava/lang/Object;)V

    .line 567
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->sideImage:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 568
    invoke-virtual {p2, v3}, Landroid/view/ViewGroup;->setAlpha(F)V

    goto :goto_0

    .line 572
    :pswitch_1
    invoke-virtual {p2, v3}, Landroid/view/ViewGroup;->setAlpha(F)V

    goto :goto_0

    .line 555
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private sendtts()V
    .locals 3

    .prologue
    .line 537
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->isTtsOn()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->ttsMessage:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 538
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->ttsMessage:Ljava/lang/String;

    sget-object v1, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->SPEECH_REROUTE:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->id:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/framework/voice/TTSUtils;->sendSpeechRequest(Ljava/lang/String;Lcom/navdy/service/library/events/audio/SpeechRequest$Category;Ljava/lang/String;)V

    .line 540
    :cond_0
    return-void
.end method

.method private switchToExpandedMode()V
    .locals 5

    .prologue
    .line 530
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->backChoice2:Ljava/util/List;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->choiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

    const/high16 v4, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;F)V

    .line 531
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->isExpandedWithStack()Z

    move-result v0

    if-nez v0, :cond_0

    .line 532
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->expandNotification(Z)V

    .line 534
    :cond_0
    return-void
.end method

.method private updateState()V
    .locals 7

    .prologue
    const/high16 v6, 0x3f000000    # 0.5f

    .line 401
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-nez v1, :cond_1

    .line 460
    :cond_0
    :goto_0
    return-void

    .line 404
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "updateState"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 406
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->trafficDelayEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficDelayEvent;

    if-nez v1, :cond_0

    .line 427
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->fasterRouteEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;

    if-eqz v1, :cond_0

    .line 428
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->mainTitle:Landroid/widget/TextView;

    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->fasterRoute:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 430
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->fasterRouteEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;

    iget-object v1, v1, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;->via:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 431
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->subTitle:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->via:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->fasterRouteEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;

    iget-object v3, v3, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;->via:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 436
    :goto_1
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->mainImage:Lcom/navdy/hud/app/ui/component/image/ColorImageView;

    sget v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->trafficRerouteColor:I

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/image/ColorImageView;->setColor(I)V

    .line 437
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->sideImage:Landroid/widget/ImageView;

    const v2, 0x7f0200e4

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 438
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->buildData()V

    .line 440
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->fasterRouteEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;

    iget-wide v2, v1, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;->etaDifference:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 441
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->text1:Landroid/widget/TextView;

    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->save:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 442
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->text2:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 443
    .local v0, "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    sget v1, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->calendarNormalMargin:I

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 444
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->text2:Landroid/widget/TextView;

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0c0025

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 445
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->text2:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->timeStr:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 446
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->text3:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->timeUnitStr:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 449
    .end local v0    # "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_2
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v1}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->isExpandedWithStack()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 450
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->backChoice2:Ljava/util/List;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->choiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

    invoke-virtual {v1, v2, v3, v4, v6}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;F)V

    .line 451
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    sget-object v2, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->backChoice:Ljava/util/List;

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setTag(Ljava/lang/Object;)V

    .line 456
    :goto_2
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->extendedContainer:Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v1}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->isExpandedWithStack()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 457
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->setExpandedContent(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 433
    :cond_3
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->subTitle:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 453
    :cond_4
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->reRouteChoices:Ljava/util/List;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->choiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

    invoke-virtual {v1, v2, v3, v4, v6}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;F)V

    goto :goto_2
.end method


# virtual methods
.method public canAddToStackIfCurrentExists()Z
    .locals 1

    .prologue
    .line 334
    const/4 v0, 0x1

    return v0
.end method

.method public expandNotification()Z
    .locals 1

    .prologue
    .line 392
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-nez v0, :cond_0

    .line 393
    const/4 v0, 0x0

    .line 397
    :goto_0
    return v0

    .line 396
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->switchToExpandedMode()V

    .line 397
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getColor()I
    .locals 1

    .prologue
    .line 339
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->fasterRouteEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;

    if-eqz v0, :cond_0

    .line 340
    sget v0, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorFasterRoute:I

    .line 342
    :goto_0
    return v0

    :cond_0
    sget v0, Lcom/navdy/hud/app/framework/glance/GlanceConstants;->colorTrafficDelay:I

    goto :goto_0
.end method

.method public getExpandedView(Landroid/content/Context;Ljava/lang/Object;)Landroid/view/View;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 228
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->BIG_MULTI_TEXT:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    invoke-static {v0, p1}, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->getView(Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->extendedContainer:Landroid/view/ViewGroup;

    .line 229
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->setExpandedContent(Landroid/content/Context;)V

    .line 230
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->extendedContainer:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public getExpandedViewIndicatorColor()I
    .locals 1

    .prologue
    .line 274
    const/4 v0, 0x0

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getTimeout()I
    .locals 1

    .prologue
    .line 319
    const/4 v0, 0x0

    return v0
.end method

.method public getType()Lcom/navdy/hud/app/framework/notifications/NotificationType;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->type:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    return-object v0
.end method

.method public getView(Landroid/content/Context;)Landroid/view/View;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 212
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->container:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    .line 213
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->SMALL_SIGN:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    invoke-static {v0, p1}, Lcom/navdy/hud/app/framework/glance/GlanceViewCache;->getView(Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->container:Landroid/view/ViewGroup;

    .line 214
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->container:Landroid/view/ViewGroup;

    const v1, 0x7f0e00cc

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->mainTitle:Landroid/widget/TextView;

    .line 215
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->container:Landroid/view/ViewGroup;

    const v1, 0x7f0e0111

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->subTitle:Landroid/widget/TextView;

    .line 216
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->container:Landroid/view/ViewGroup;

    const v1, 0x7f0e0112

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/image/ColorImageView;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->mainImage:Lcom/navdy/hud/app/ui/component/image/ColorImageView;

    .line 217
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->container:Landroid/view/ViewGroup;

    const v1, 0x7f0e00d1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->sideImage:Landroid/widget/ImageView;

    .line 218
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->container:Landroid/view/ViewGroup;

    const v1, 0x7f0e0105

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->text1:Landroid/widget/TextView;

    .line 219
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->container:Landroid/view/ViewGroup;

    const v1, 0x7f0e0106

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->text2:Landroid/widget/TextView;

    .line 220
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->container:Landroid/view/ViewGroup;

    const v1, 0x7f0e0108

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->text3:Landroid/widget/TextView;

    .line 221
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->container:Landroid/view/ViewGroup;

    const v1, 0x7f0e00d2

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    .line 223
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->container:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public getViewSwitchAnimation(Z)Landroid/animation/AnimatorSet;
    .locals 11
    .param p1, "viewIn"    # Z

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x2

    .line 375
    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    .line 376
    .local v3, "set":Landroid/animation/AnimatorSet;
    if-nez p1, :cond_0

    .line 377
    iget-object v4, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->mainTitle:Landroid/widget/TextView;

    sget-object v5, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v6, v7, [F

    fill-array-data v6, :array_0

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 378
    .local v0, "o1":Landroid/animation/ObjectAnimator;
    iget-object v4, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->subTitle:Landroid/widget/TextView;

    sget-object v5, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v6, v7, [F

    fill-array-data v6, :array_1

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 379
    .local v1, "o2":Landroid/animation/ObjectAnimator;
    iget-object v4, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    sget-object v5, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v6, v7, [F

    fill-array-data v6, :array_2

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 380
    .local v2, "o3":Landroid/animation/ObjectAnimator;
    new-array v4, v10, [Landroid/animation/Animator;

    aput-object v0, v4, v8

    aput-object v1, v4, v9

    aput-object v2, v4, v7

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 387
    :goto_0
    return-object v3

    .line 382
    .end local v0    # "o1":Landroid/animation/ObjectAnimator;
    .end local v1    # "o2":Landroid/animation/ObjectAnimator;
    .end local v2    # "o3":Landroid/animation/ObjectAnimator;
    :cond_0
    iget-object v4, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->mainTitle:Landroid/widget/TextView;

    sget-object v5, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v6, v7, [F

    fill-array-data v6, :array_3

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 383
    .restart local v0    # "o1":Landroid/animation/ObjectAnimator;
    iget-object v4, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->subTitle:Landroid/widget/TextView;

    sget-object v5, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v6, v7, [F

    fill-array-data v6, :array_4

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 384
    .restart local v1    # "o2":Landroid/animation/ObjectAnimator;
    iget-object v4, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    sget-object v5, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v6, v7, [F

    fill-array-data v6, :array_5

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 385
    .restart local v2    # "o3":Landroid/animation/ObjectAnimator;
    new-array v4, v10, [Landroid/animation/Animator;

    aput-object v0, v4, v8

    aput-object v1, v4, v9

    aput-object v2, v4, v7

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    goto :goto_0

    .line 377
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 378
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 379
    :array_2
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 382
    :array_3
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 383
    :array_4
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 384
    :array_5
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public isAlive()Z
    .locals 1

    .prologue
    .line 324
    const/4 v0, 0x1

    return v0
.end method

.method public isPurgeable()Z
    .locals 1

    .prologue
    .line 329
    const/4 v0, 0x0

    return v0
.end method

.method public onExpandedNotificationEvent(Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V
    .locals 2
    .param p1, "mode"    # Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    .prologue
    .line 351
    sget-object v0, Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;->COLLAPSE:Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    if-ne p1, v0, :cond_2

    .line 352
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-eqz v0, :cond_0

    .line 353
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->cancelTts()V

    .line 355
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isNotificationMarkedForRemoval(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 356
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->dismissNotification()V

    .line 364
    :cond_0
    :goto_0
    return-void

    .line 359
    :cond_1
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->updateState()V

    goto :goto_0

    .line 362
    :cond_2
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->sendtts()V

    goto :goto_0
.end method

.method public onExpandedNotificationSwitched()V
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->isExpandedWithStack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 369
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->sendtts()V

    .line 371
    :cond_0
    return-void
.end method

.method public onNotificationEvent(Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V
    .locals 0
    .param p1, "mode"    # Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    .prologue
    .line 347
    return-void
.end method

.method public onStart(Lcom/navdy/hud/app/framework/notifications/INotificationController;)V
    .locals 3
    .param p1, "controller"    # Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 279
    invoke-super {p0, p1}, Lcom/navdy/hud/app/maps/notification/BaseTrafficNotification;->onStart(Lcom/navdy/hud/app/framework/notifications/INotificationController;)V

    .line 280
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->updateState()V

    .line 282
    invoke-interface {p1}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->isExpandedWithStack()Z

    move-result v0

    if-nez v0, :cond_0

    .line 283
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->container:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setTranslationX(F)V

    .line 284
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->container:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setTranslationY(F)V

    .line 285
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->container:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 286
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->mainTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setAlpha(F)V

    .line 287
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->subTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setAlpha(F)V

    .line 288
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setAlpha(F)V

    .line 294
    :goto_0
    return-void

    .line 290
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->mainTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 291
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->subTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 292
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setAlpha(F)V

    goto :goto_0
.end method

.method public onStop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 303
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->cancelTts()V

    .line 304
    invoke-super {p0}, Lcom/navdy/hud/app/maps/notification/BaseTrafficNotification;->onStop()V

    .line 306
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->container:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 307
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->SMALL_SIGN:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->container:Landroid/view/ViewGroup;

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->cleanupView(Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;Landroid/view/ViewGroup;)V

    .line 308
    iput-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->container:Landroid/view/ViewGroup;

    .line 311
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->extendedContainer:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    .line 312
    sget-object v0, Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;->BIG_MULTI_TEXT:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->extendedContainer:Landroid/view/ViewGroup;

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->cleanupView(Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;Landroid/view/ViewGroup;)V

    .line 313
    iput-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->extendedContainer:Landroid/view/ViewGroup;

    .line 315
    :cond_1
    return-void
.end method

.method public onUpdate()V
    .locals 0

    .prologue
    .line 298
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->updateState()V

    .line 299
    return-void
.end method

.method public setDelayEvent(Lcom/navdy/hud/app/maps/MapEvents$TrafficDelayEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$TrafficDelayEvent;

    .prologue
    .line 463
    iput-object p1, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->trafficDelayEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficDelayEvent;

    .line 464
    return-void
.end method

.method setExpandedContent(Landroid/content/Context;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 234
    iget-object v3, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->extendedContainer:Landroid/view/ViewGroup;

    const v4, 0x7f0e0088

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 235
    .local v1, "textView1":Landroid/widget/TextView;
    const v3, 0x7f0c0019

    invoke-virtual {v1, p1, v3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 237
    iget-object v3, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->extendedContainer:Landroid/view/ViewGroup;

    const v4, 0x7f0e0089

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 238
    .local v2, "textView2":Landroid/widget/TextView;
    const v3, 0x7f0c001a

    invoke-virtual {v2, p1, v3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 240
    iget-object v3, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->fasterRouteEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;

    if-eqz v3, :cond_1

    .line 241
    sget-object v3, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->fasterRouteAvailable:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 243
    iget-object v3, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->timeStr:Ljava/lang/String;

    if-nez v3, :cond_0

    .line 244
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->buildData()V

    .line 248
    :cond_0
    iget-object v3, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->fasterRouteEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;

    iget-wide v4, v3, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;->etaDifference:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_2

    .line 249
    sget-object v3, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->resources:Landroid/content/res/Resources;

    const v4, 0x7f0902a5

    const/4 v5, 0x7

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->timeStr:Ljava/lang/String;

    aput-object v6, v5, v8

    iget-object v6, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->timeStrExpanded:Ljava/lang/String;

    aput-object v6, v5, v9

    iget-object v6, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->fasterRouteEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;

    iget-object v6, v6, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;->additionalVia:Ljava/lang/String;

    aput-object v6, v5, v10

    iget-object v6, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->etaStr:Ljava/lang/String;

    aput-object v6, v5, v11

    iget-object v6, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->etaUnitStr:Ljava/lang/String;

    aput-object v6, v5, v12

    const/4 v6, 0x5

    iget-object v7, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->distanceStr:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x6

    iget-object v7, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->distanceDiffStr:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 265
    .local v0, "str":Ljava/lang/String;
    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 268
    .end local v0    # "str":Ljava/lang/String;
    :cond_1
    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 269
    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 270
    return-void

    .line 258
    :cond_2
    sget-object v3, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->resources:Landroid/content/res/Resources;

    const v4, 0x7f0902a6

    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->fasterRouteEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;

    iget-object v6, v6, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;->additionalVia:Ljava/lang/String;

    aput-object v6, v5, v8

    iget-object v6, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->etaStr:Ljava/lang/String;

    aput-object v6, v5, v9

    iget-object v6, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->etaUnitStr:Ljava/lang/String;

    aput-object v6, v5, v10

    iget-object v6, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->distanceStr:Ljava/lang/String;

    aput-object v6, v5, v11

    iget-object v6, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->distanceDiffStr:Ljava/lang/String;

    aput-object v6, v5, v12

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "str":Ljava/lang/String;
    goto :goto_0
.end method

.method public setFasterRouteEvent(Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;

    .prologue
    .line 467
    iput-object p1, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->fasterRouteEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;

    .line 468
    return-void
.end method
