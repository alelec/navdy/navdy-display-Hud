.class Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1$1$1;
.super Ljava/lang/Object;
.source "TrafficNotificationManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1$1;->result(Lcom/here/android/mpa/common/Image;Landroid/graphics/Bitmap;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1$1;

.field final synthetic val$combined:Landroid/graphics/Bitmap;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1$1;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "this$2"    # Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1$1;

    .prologue
    .line 265
    iput-object p1, p0, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1$1$1;->this$2:Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1$1;

    iput-object p2, p0, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1$1$1;->val$combined:Landroid/graphics/Bitmap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 269
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v5

    .line 270
    .local v5, "uiStateManager":Lcom/navdy/hud/app/ui/framework/UIStateManager;
    invoke-virtual {v5}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getHomescreenView()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    move-result-object v0

    .line 271
    .local v0, "homeScreenView":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 272
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v7, 0x7f030030

    const/4 v8, 0x0

    invoke-virtual {v2, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 273
    .local v6, "view":Landroid/view/View;
    const v7, 0x7f0e00cf

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 274
    .local v1, "imageView":Landroid/widget/ImageView;
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    iget-object v7, p0, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1$1$1;->this$2:Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1$1;

    iget-object v7, v7, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1$1;->this$1:Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1;

    iget-object v7, v7, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1;->this$0:Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;

    # getter for: Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->junctionViewW:I
    invoke-static {v7}, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->access$300(Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;)I

    move-result v7

    iget-object v8, p0, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1$1$1;->this$2:Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1$1;

    iget-object v8, v8, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1$1;->this$1:Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1;

    iget-object v8, v8, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1;->this$0:Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;

    # getter for: Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->junctionViewH:I
    invoke-static {v8}, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->access$400(Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;)I

    move-result v8

    invoke-direct {v3, v7, v8}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 275
    .local v3, "params":Landroid/widget/FrameLayout$LayoutParams;
    const/16 v7, 0x53

    iput v7, v3, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 276
    invoke-virtual {v6, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 277
    iget-object v7, p0, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1$1$1;->val$combined:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 278
    invoke-virtual {v0, v6}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->injectRightSection(Landroid/view/View;)V

    .line 279
    sget-object v7, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "junction view mode injected"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 283
    .end local v0    # "homeScreenView":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    .end local v1    # "imageView":Landroid/widget/ImageView;
    .end local v2    # "inflater":Landroid/view/LayoutInflater;
    .end local v3    # "params":Landroid/widget/FrameLayout$LayoutParams;
    .end local v5    # "uiStateManager":Lcom/navdy/hud/app/ui/framework/UIStateManager;
    .end local v6    # "view":Landroid/view/View;
    :goto_0
    return-void

    .line 280
    :catch_0
    move-exception v4

    .line 281
    .local v4, "t":Ljava/lang/Throwable;
    sget-object v7, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "junction view mode"

    invoke-virtual {v7, v8, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
