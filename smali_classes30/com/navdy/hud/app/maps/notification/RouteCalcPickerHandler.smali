.class public Lcom/navdy/hud/app/maps/notification/RouteCalcPickerHandler;
.super Ljava/lang/Object;
.source "RouteCalcPickerHandler.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/destination/IDestinationPicker;


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private event:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 25
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/maps/notification/RouteCalcPickerHandler;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/maps/notification/RouteCalcPickerHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalcPickerHandler;->event:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    .line 31
    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/navdy/hud/app/maps/notification/RouteCalcPickerHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method


# virtual methods
.method public onDestinationPickerClosed()V
    .locals 0

    .prologue
    .line 85
    return-void
.end method

.method public onItemClicked(IILcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$DestinationPickerState;)Z
    .locals 8
    .param p1, "id"    # I
    .param p2, "pos"    # I
    .param p3, "state"    # Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$DestinationPickerState;

    .prologue
    const/4 v7, 0x1

    .line 36
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getNavigationView()Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    move-result-object v1

    .line 37
    .local v1, "navigationView":Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;
    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->cleanupMapOverviewRoutes()V

    .line 38
    if-nez p2, :cond_0

    .line 39
    sget-object v4, Lcom/navdy/hud/app/maps/notification/RouteCalcPickerHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "map-route- onItemClicked current route"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 74
    :goto_0
    return v7

    .line 41
    :cond_0
    sget-object v4, Lcom/navdy/hud/app/maps/notification/RouteCalcPickerHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "map-route- onItemClicked new route"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 42
    iget-object v4, p0, Lcom/navdy/hud/app/maps/notification/RouteCalcPickerHandler;->event:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->response:Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

    iget-object v4, v4, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->results:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    .line 43
    .local v0, "len":I
    if-lt p2, v0, :cond_1

    .line 44
    sget-object v4, Lcom/navdy/hud/app/maps/notification/RouteCalcPickerHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "map-route- invalid pos:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " len="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :cond_1
    iget-object v4, p0, Lcom/navdy/hud/app/maps/notification/RouteCalcPickerHandler;->event:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->response:Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

    iget-object v4, v4, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->results:Ljava/util/List;

    invoke-interface {v4, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    .line 48
    .local v2, "result":Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getInstance()Lcom/navdy/hud/app/maps/here/HereRouteCache;

    move-result-object v4

    iget-object v5, v2, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getRoute(Ljava/lang/String;)Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;

    move-result-object v3

    .line 49
    .local v3, "routeInfo":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    if-nez v3, :cond_2

    .line 50
    sget-object v4, Lcom/navdy/hud/app/maps/notification/RouteCalcPickerHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "map-route- route not found in cache:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v2, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 53
    :cond_2
    iput-boolean v7, p3, Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$DestinationPickerState;->doNotAddOriginalRoute:Z

    .line 55
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v4

    new-instance v5, Lcom/navdy/hud/app/maps/notification/RouteCalcPickerHandler$1;

    invoke-direct {v5, p0, v2, v3}, Lcom/navdy/hud/app/maps/notification/RouteCalcPickerHandler$1;-><init>(Lcom/navdy/hud/app/maps/notification/RouteCalcPickerHandler;Lcom/navdy/service/library/events/navigation/NavigationRouteResult;Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;)V

    const/16 v6, 0x14

    invoke-virtual {v4, v5, v6}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public onItemSelected(IILcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$DestinationPickerState;)Z
    .locals 1
    .param p1, "id"    # I
    .param p2, "pos"    # I
    .param p3, "state"    # Lcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$DestinationPickerState;

    .prologue
    .line 79
    const/4 v0, 0x0

    return v0
.end method
