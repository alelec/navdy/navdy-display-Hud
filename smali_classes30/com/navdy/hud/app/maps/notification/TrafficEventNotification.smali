.class public Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;
.super Lcom/navdy/hud/app/maps/notification/BaseTrafficNotification;
.source "TrafficEventNotification.java"


# static fields
.field private static final TAG_DISMISS:I = 0x1

.field private static ahead:Ljava/lang/String;

.field private static dismiss:Ljava/lang/String;

.field private static dismissChoices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;",
            ">;"
        }
    .end annotation
.end field

.field private static enableInternet:Ljava/lang/String;

.field private static noUpdatedTraffic:Ljava/lang/String;

.field private static slowTraffic:Ljava/lang/String;

.field private static stoppedTraffic:Ljava/lang/String;

.field private static trafficIncident:Ljava/lang/String;

.field private static unknownIncident:Ljava/lang/String;


# instance fields
.field private bus:Lcom/squareup/otto/Bus;

.field private choiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

.field private container:Landroid/view/ViewGroup;

.field private image:Landroid/widget/ImageView;

.field private notifColor:I

.field private subTitle:Landroid/widget/TextView;

.field private title:Landroid/widget/TextView;

.field private trafficEvent:Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 42
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->dismissChoices:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/otto/Bus;)V
    .locals 10
    .param p1, "bus"    # Lcom/squareup/otto/Bus;

    .prologue
    const v2, 0x7f020125

    .line 70
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/BaseTrafficNotification;-><init>()V

    .line 56
    new-instance v0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification$1;-><init>(Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->choiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

    .line 71
    sget-object v0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->stoppedTraffic:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 72
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 73
    .local v8, "resources":Landroid/content/res/Resources;
    const v0, 0x7f0902a2

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->stoppedTraffic:Ljava/lang/String;

    .line 74
    const v0, 0x7f0902a1

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->slowTraffic:Ljava/lang/String;

    .line 75
    const v0, 0x7f0902a0

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->trafficIncident:Ljava/lang/String;

    .line 76
    const v0, 0x7f090299

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->noUpdatedTraffic:Ljava/lang/String;

    .line 77
    const v0, 0x7f0900eb

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->enableInternet:Ljava/lang/String;

    .line 78
    const v0, 0x7f09029a

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->ahead:Ljava/lang/String;

    .line 79
    const v0, 0x7f09029d

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->dismiss:Ljava/lang/String;

    .line 80
    const v0, 0x7f09029f

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->unknownIncident:Ljava/lang/String;

    .line 82
    const v0, 0x7f0d0021

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 83
    .local v3, "dismissColor":I
    const/high16 v5, -0x1000000

    .line 84
    .local v5, "unselectedColor":I
    sget-object v9, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->dismissChoices:Ljava/util/List;

    new-instance v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const/4 v1, 0x1

    sget-object v6, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->dismiss:Ljava/lang/String;

    move v4, v2

    move v7, v3

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    const v0, 0x7f0d00b6

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->notifColor:I

    .line 87
    .end local v3    # "dismissColor":I
    .end local v5    # "unselectedColor":I
    .end local v8    # "resources":Landroid/content/res/Resources;
    :cond_0
    iput-object p1, p0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->bus:Lcom/squareup/otto/Bus;

    .line 88
    return-void
.end method

.method static synthetic access$002(Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent;)Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;
    .param p1, "x1"    # Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->trafficEvent:Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent;

    return-object p1
.end method

.method private getLiveTrafficEventImage(Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent;)I
    .locals 2
    .param p1, "trafficEvent"    # Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent;

    .prologue
    .line 193
    iget-object v0, p1, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent;->type:Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Type;

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Type;->CONGESTION:Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Type;

    if-ne v0, v1, :cond_0

    iget-object v0, p1, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent;->severity:Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;

    iget v0, v0, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;->value:I

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;->VERY_HIGH:Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;

    iget v1, v1, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;->value:I

    if-le v0, v1, :cond_0

    .line 195
    const v0, 0x7f02018a

    .line 199
    :goto_0
    return v0

    .line 196
    :cond_0
    iget-object v0, p1, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent;->type:Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Type;

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Type;->INCIDENT:Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Type;

    if-ne v0, v1, :cond_1

    .line 197
    const v0, 0x7f02017c

    goto :goto_0

    .line 199
    :cond_1
    const v0, 0x7f020189

    goto :goto_0
.end method

.method private getLiveTrafficEventText(Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent;)Ljava/lang/String;
    .locals 2
    .param p1, "trafficEvent"    # Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent;

    .prologue
    .line 204
    iget-object v0, p1, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent;->type:Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Type;

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Type;->CONGESTION:Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Type;

    if-ne v0, v1, :cond_1

    .line 205
    iget-object v0, p1, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent;->severity:Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;

    iget v0, v0, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;->value:I

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;->HIGH:Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;

    iget v1, v1, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;->value:I

    if-le v0, v1, :cond_0

    .line 206
    sget-object v0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->stoppedTraffic:Ljava/lang/String;

    .line 213
    :goto_0
    return-object v0

    .line 208
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->slowTraffic:Ljava/lang/String;

    goto :goto_0

    .line 210
    :cond_1
    iget-object v0, p1, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent;->type:Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Type;

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Type;->INCIDENT:Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Type;

    if-ne v0, v1, :cond_2

    .line 211
    sget-object v0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->trafficIncident:Ljava/lang/String;

    goto :goto_0

    .line 213
    :cond_2
    sget-object v0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->unknownIncident:Ljava/lang/String;

    goto :goto_0
.end method

.method private updateState()V
    .locals 5

    .prologue
    .line 184
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->trafficEvent:Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent;

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->title:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->trafficEvent:Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent;

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->getLiveTrafficEventText(Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 186
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->subTitle:Landroid/widget/TextView;

    sget-object v1, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->ahead:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 187
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->image:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->trafficEvent:Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent;

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->getLiveTrafficEventImage(Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 188
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    sget-object v1, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->dismissChoices:Ljava/util/List;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->choiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

    const/high16 v4, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;F)V

    .line 190
    :cond_0
    return-void
.end method


# virtual methods
.method public canAddToStackIfCurrentExists()Z
    .locals 1

    .prologue
    .line 155
    const/4 v0, 0x1

    return v0
.end method

.method public expandNotification()Z
    .locals 1

    .prologue
    .line 180
    const/4 v0, 0x0

    return v0
.end method

.method public getColor()I
    .locals 1

    .prologue
    .line 160
    iget v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->notifColor:I

    return v0
.end method

.method public getExpandedView(Landroid/content/Context;Ljava/lang/Object;)Landroid/view/View;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 114
    const/4 v0, 0x0

    return-object v0
.end method

.method public getExpandedViewIndicatorColor()I
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x0

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    const-string v0, "navdy#traffic#event#notif"

    return-object v0
.end method

.method public getTimeout()I
    .locals 1

    .prologue
    .line 140
    const/4 v0, 0x0

    return v0
.end method

.method public getType()Lcom/navdy/hud/app/framework/notifications/NotificationType;
    .locals 1

    .prologue
    .line 92
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;->TRAFFIC_EVENT:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    return-object v0
.end method

.method public getView(Landroid/content/Context;)Landroid/view/View;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 102
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->container:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    .line 103
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030041

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->container:Landroid/view/ViewGroup;

    .line 104
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->container:Landroid/view/ViewGroup;

    const v1, 0x7f0e00c3

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->title:Landroid/widget/TextView;

    .line 105
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->container:Landroid/view/ViewGroup;

    const v1, 0x7f0e0111

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->subTitle:Landroid/widget/TextView;

    .line 106
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->container:Landroid/view/ViewGroup;

    const v1, 0x7f0e00cf

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->image:Landroid/widget/ImageView;

    .line 107
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->container:Landroid/view/ViewGroup;

    const v1, 0x7f0e00d2

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->container:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public getViewSwitchAnimation(Z)Landroid/animation/AnimatorSet;
    .locals 1
    .param p1, "viewIn"    # Z

    .prologue
    .line 175
    const/4 v0, 0x0

    return-object v0
.end method

.method public isAlive()Z
    .locals 1

    .prologue
    .line 145
    const/4 v0, 0x1

    return v0
.end method

.method public isPurgeable()Z
    .locals 1

    .prologue
    .line 150
    const/4 v0, 0x0

    return v0
.end method

.method public onExpandedNotificationEvent(Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V
    .locals 0
    .param p1, "mode"    # Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    .prologue
    .line 168
    return-void
.end method

.method public onExpandedNotificationSwitched()V
    .locals 0

    .prologue
    .line 171
    return-void
.end method

.method public onNotificationEvent(Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V
    .locals 0
    .param p1, "mode"    # Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    .prologue
    .line 164
    return-void
.end method

.method public onStart(Lcom/navdy/hud/app/framework/notifications/INotificationController;)V
    .locals 0
    .param p1, "controller"    # Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .prologue
    .line 124
    invoke-super {p0, p1}, Lcom/navdy/hud/app/maps/notification/BaseTrafficNotification;->onStart(Lcom/navdy/hud/app/framework/notifications/INotificationController;)V

    .line 125
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->updateState()V

    .line 126
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 135
    invoke-super {p0}, Lcom/navdy/hud/app/maps/notification/BaseTrafficNotification;->onStop()V

    .line 136
    return-void
.end method

.method public onUpdate()V
    .locals 0

    .prologue
    .line 130
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->updateState()V

    .line 131
    return-void
.end method

.method public setTrafficEvent(Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent;

    .prologue
    .line 218
    iput-object p1, p0, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->trafficEvent:Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent;

    .line 219
    return-void
.end method
