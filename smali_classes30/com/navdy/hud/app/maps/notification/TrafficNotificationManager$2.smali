.class Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$2;
.super Ljava/lang/Object;
.source "TrafficNotificationManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->removeJunctionView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;

    .prologue
    .line 305
    iput-object p1, p0, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$2;->this$0:Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 309
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v2

    .line 310
    .local v2, "uiStateManager":Lcom/navdy/hud/app/ui/framework/UIStateManager;
    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getHomescreenView()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    move-result-object v0

    .line 311
    .local v0, "homeScreenView":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->ejectRightSection()V

    .line 312
    sget-object v3, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "junction view mode Ejected"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 316
    .end local v0    # "homeScreenView":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    .end local v2    # "uiStateManager":Lcom/navdy/hud/app/ui/framework/UIStateManager;
    :goto_0
    return-void

    .line 313
    :catch_0
    move-exception v1

    .line 314
    .local v1, "t":Ljava/lang/Throwable;
    sget-object v3, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "junction view mode"

    invoke-virtual {v3, v4, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
