.class Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1$1;
.super Ljava/lang/Object;
.source "TrafficNotificationManager.java"

# interfaces
.implements Lcom/navdy/hud/app/maps/here/HereMapUtil$IImageLoadCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1;

    .prologue
    .line 211
    iput-object p1, p0, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1$1;->this$1:Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public result(Lcom/here/android/mpa/common/Image;Landroid/graphics/Bitmap;)V
    .locals 18
    .param p1, "image"    # Lcom/here/android/mpa/common/Image;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 215
    if-nez p2, :cond_1

    .line 216
    :try_start_0
    sget-object v14, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v15, "junction view bitmap not loaded"

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 288
    :cond_0
    :goto_0
    return-void

    .line 220
    :cond_1
    sget-object v14, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "junction view bitmap loaded w="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " h="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " orig w="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1$1;->this$1:Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1;->val$event:Lcom/navdy/hud/app/maps/MapEvents$DisplayJunction;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayJunction;->junction:Lcom/here/android/mpa/common/Image;

    move-object/from16 v16, v0

    .line 221
    invoke-virtual/range {v16 .. v16}, Lcom/here/android/mpa/common/Image;->getWidth()J

    move-result-wide v16

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " h="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1$1;->this$1:Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1;->val$event:Lcom/navdy/hud/app/maps/MapEvents$DisplayJunction;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayJunction;->junction:Lcom/here/android/mpa/common/Image;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/here/android/mpa/common/Image;->getHeight()J

    move-result-wide v16

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 220
    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 222
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1$1;->this$1:Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1;

    iget-object v14, v14, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1;->this$0:Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;

    # getter for: Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->lastJunctionEvent:Lcom/navdy/hud/app/maps/MapEvents$DisplayJunction;
    invoke-static {v14}, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->access$200(Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;)Lcom/navdy/hud/app/maps/MapEvents$DisplayJunction;

    move-result-object v14

    if-eqz v14, :cond_0

    .line 227
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 230
    .local v4, "l1":J
    const/high16 v14, 0x42b40000    # 90.0f

    move-object/from16 v0, p2

    invoke-static {v0, v14}, Lcom/navdy/hud/app/util/ImageUtil;->hueShift(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 231
    .local v3, "hue":Landroid/graphics/Bitmap;
    if-nez v3, :cond_2

    .line 232
    sget-object v14, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v15, "hue shift not performed"

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 233
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 285
    .end local v3    # "hue":Landroid/graphics/Bitmap;
    .end local v4    # "l1":J
    :catch_0
    move-exception v13

    .line 286
    .local v13, "t":Ljava/lang/Throwable;
    sget-object v14, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v15, "junction view mode"

    invoke-virtual {v14, v15, v13}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 237
    .end local v13    # "t":Ljava/lang/Throwable;
    .restart local v3    # "hue":Landroid/graphics/Bitmap;
    .restart local v4    # "l1":J
    :cond_2
    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    .line 240
    .local v6, "l2":J
    const/4 v14, 0x0

    move-object/from16 v0, p2

    invoke-static {v0, v14}, Lcom/navdy/hud/app/util/ImageUtil;->applySaturation(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 241
    .local v12, "saturation":Landroid/graphics/Bitmap;
    if-nez v12, :cond_3

    .line 242
    sget-object v14, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v15, "saturation not performed"

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 243
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->recycle()V

    .line 244
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    goto/16 :goto_0

    .line 248
    :cond_3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    .line 249
    .local v8, "l3":J
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->recycle()V

    .line 251
    invoke-static {v3, v12}, Lcom/navdy/hud/app/util/ImageUtil;->blend(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 252
    .local v2, "combined":Landroid/graphics/Bitmap;
    invoke-virtual {v12}, Landroid/graphics/Bitmap;->recycle()V

    .line 253
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 255
    if-nez v2, :cond_4

    .line 256
    sget-object v14, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v15, "combine not performed"

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 260
    :cond_4
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    .line 262
    .local v10, "l4":J
    sget-object v14, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "junction view bitmap took["

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sub-long v16, v10, v4

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "] hue ["

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sub-long v16, v6, v4

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "] sat["

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sub-long v16, v8, v6

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "] blend["

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sub-long v16, v10, v8

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "]"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 265
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1$1;->this$1:Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1;

    iget-object v14, v14, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1;->this$0:Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;

    # getter for: Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->handler:Landroid/os/Handler;
    invoke-static {v14}, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->access$500(Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;)Landroid/os/Handler;

    move-result-object v14

    new-instance v15, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1$1$1;

    move-object/from16 v0, p0

    invoke-direct {v15, v0, v2}, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1$1$1;-><init>(Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1$1;Landroid/graphics/Bitmap;)V

    invoke-virtual {v14, v15}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
