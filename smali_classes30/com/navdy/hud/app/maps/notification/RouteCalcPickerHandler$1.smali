.class Lcom/navdy/hud/app/maps/notification/RouteCalcPickerHandler$1;
.super Ljava/lang/Object;
.source "RouteCalcPickerHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/notification/RouteCalcPickerHandler;->onItemClicked(IILcom/navdy/hud/app/ui/component/destination/DestinationPickerScreen$DestinationPickerState;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/notification/RouteCalcPickerHandler;

.field final synthetic val$result:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

.field final synthetic val$routeInfo:Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/notification/RouteCalcPickerHandler;Lcom/navdy/service/library/events/navigation/NavigationRouteResult;Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/notification/RouteCalcPickerHandler;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalcPickerHandler$1;->this$0:Lcom/navdy/hud/app/maps/notification/RouteCalcPickerHandler;

    iput-object p2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalcPickerHandler$1;->val$result:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    iput-object p3, p0, Lcom/navdy/hud/app/maps/notification/RouteCalcPickerHandler$1;->val$routeInfo:Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 59
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v0

    .line 60
    .local v0, "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalcPickerHandler$1;->val$result:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    iget-object v1, v1, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isTrafficConsidered(Ljava/lang/String;)Z

    move-result v6

    .line 61
    .local v6, "trafficConsidered":Z
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalcPickerHandler$1;->val$routeInfo:Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;

    iget-object v1, v1, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->route:Lcom/here/android/mpa/routing/Route;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;->NAV_SESSION_ROUTE_PICKER:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    iget-object v3, p0, Lcom/navdy/hud/app/maps/notification/RouteCalcPickerHandler$1;->val$result:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    iget-object v3, v3, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeId:Ljava/lang/String;

    iget-object v4, p0, Lcom/navdy/hud/app/maps/notification/RouteCalcPickerHandler$1;->val$result:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    iget-object v4, v4, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->via:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-virtual/range {v0 .. v6}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->setReroute(Lcom/here/android/mpa/routing/Route;Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 62
    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getNavigationSessionPreference()Lcom/navdy/hud/app/maps/NavSessionPreferences;

    move-result-object v1

    iget-boolean v1, v1, Lcom/navdy/hud/app/maps/NavSessionPreferences;->spokenTurnByTurn:Z

    if-eqz v1, :cond_0

    .line 63
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalcPickerHandler$1;->val$result:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    iget-object v1, v1, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeId:Ljava/lang/String;

    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->buildChangeRouteTTS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 64
    .local v8, "tts":Ljava/lang/String;
    # getter for: Lcom/navdy/hud/app/maps/notification/RouteCalcPickerHandler;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/notification/RouteCalcPickerHandler;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "tts["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 65
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v7

    .line 66
    .local v7, "bus":Lcom/squareup/otto/Bus;
    sget-object v1, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->CANCEL_TBT_TTS:Lcom/navdy/hud/app/event/RemoteEvent;

    invoke-virtual {v7, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 67
    sget-object v1, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->CANCEL_CALC_TTS:Lcom/navdy/hud/app/event/RemoteEvent;

    invoke-virtual {v7, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 68
    sget-object v1, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->SPEECH_TURN_BY_TURN:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->ROUTE_CALC_TTS_ID:Ljava/lang/String;

    invoke-static {v8, v1, v2}, Lcom/navdy/hud/app/framework/voice/TTSUtils;->sendSpeechRequest(Ljava/lang/String;Lcom/navdy/service/library/events/audio/SpeechRequest$Category;Ljava/lang/String;)V

    .line 70
    .end local v7    # "bus":Lcom/squareup/otto/Bus;
    .end local v8    # "tts":Ljava/lang/String;
    :cond_0
    # getter for: Lcom/navdy/hud/app/maps/notification/RouteCalcPickerHandler;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/notification/RouteCalcPickerHandler;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "new route set: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/maps/notification/RouteCalcPickerHandler$1;->val$routeInfo:Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;

    iget-object v3, v3, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->routeRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 71
    return-void
.end method
