.class synthetic Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$5;
.super Ljava/lang/Object;
.source "RouteCalculationNotification.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$here$android$mpa$routing$RouteOptions$Type:[I

.field static final synthetic $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

.field static final synthetic $SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

.field static final synthetic $SwitchMap$com$navdy$service$library$events$input$Gesture:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 826
    invoke-static {}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->values()[Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$5;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

    :try_start_0
    sget-object v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$5;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_DISCONNECTED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_6

    .line 645
    :goto_0
    invoke-static {}, Lcom/here/android/mpa/routing/RouteOptions$Type;->values()[Lcom/here/android/mpa/routing/RouteOptions$Type;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$5;->$SwitchMap$com$here$android$mpa$routing$RouteOptions$Type:[I

    :try_start_1
    sget-object v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$5;->$SwitchMap$com$here$android$mpa$routing$RouteOptions$Type:[I

    sget-object v1, Lcom/here/android/mpa/routing/RouteOptions$Type;->FASTEST:Lcom/here/android/mpa/routing/RouteOptions$Type;

    invoke-virtual {v1}, Lcom/here/android/mpa/routing/RouteOptions$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_5

    :goto_1
    :try_start_2
    sget-object v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$5;->$SwitchMap$com$here$android$mpa$routing$RouteOptions$Type:[I

    sget-object v1, Lcom/here/android/mpa/routing/RouteOptions$Type;->SHORTEST:Lcom/here/android/mpa/routing/RouteOptions$Type;

    invoke-virtual {v1}, Lcom/here/android/mpa/routing/RouteOptions$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_4

    .line 477
    :goto_2
    invoke-static {}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->values()[Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$5;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    :try_start_3
    sget-object v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$5;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    sget-object v1, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->LEFT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :goto_3
    :try_start_4
    sget-object v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$5;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    sget-object v1, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->RIGHT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_2

    :goto_4
    :try_start_5
    sget-object v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$5;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    sget-object v1, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->SELECT:Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_1

    .line 462
    :goto_5
    invoke-static {}, Lcom/navdy/service/library/events/input/Gesture;->values()[Lcom/navdy/service/library/events/input/Gesture;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$5;->$SwitchMap$com$navdy$service$library$events$input$Gesture:[I

    :try_start_6
    sget-object v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$5;->$SwitchMap$com$navdy$service$library$events$input$Gesture:[I

    sget-object v1, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_SWIPE_LEFT:Lcom/navdy/service/library/events/input/Gesture;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/input/Gesture;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_0

    :goto_6
    return-void

    :catch_0
    move-exception v0

    goto :goto_6

    .line 477
    :catch_1
    move-exception v0

    goto :goto_5

    :catch_2
    move-exception v0

    goto :goto_4

    :catch_3
    move-exception v0

    goto :goto_3

    .line 645
    :catch_4
    move-exception v0

    goto :goto_2

    :catch_5
    move-exception v0

    goto :goto_1

    .line 826
    :catch_6
    move-exception v0

    goto :goto_0
.end method
