.class public Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;
.super Ljava/lang/Object;
.source "TrafficNotificationManager.java"


# static fields
.field private static final sInstance:Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;

.field public static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private bus:Lcom/squareup/otto/Bus;

.field private handler:Landroid/os/Handler;

.field private final junctionViewH:I

.field private final junctionViewInflateH:I

.field private final junctionViewInflateW:I

.field private final junctionViewW:I

.field private lastJunctionEvent:Lcom/navdy/hud/app/maps/MapEvents$DisplayJunction;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 40
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 42
    new-instance v0, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;

    invoke-direct {v0}, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->sInstance:Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->handler:Landroid/os/Handler;

    .line 60
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->bus:Lcom/squareup/otto/Bus;

    .line 61
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 62
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x7f0b019f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->junctionViewW:I

    .line 63
    const v1, 0x7f0b019c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->junctionViewH:I

    .line 64
    const v1, 0x7f0b019e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->junctionViewInflateW:I

    .line 65
    const v1, 0x7f0b019d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->junctionViewInflateH:I

    .line 66
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v1, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 67
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;

    .prologue
    .line 39
    iget v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->junctionViewInflateW:I

    return v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;

    .prologue
    .line 39
    iget v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->junctionViewInflateH:I

    return v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;)Lcom/navdy/hud/app/maps/MapEvents$DisplayJunction;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->lastJunctionEvent:Lcom/navdy/hud/app/maps/MapEvents$DisplayJunction;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;

    .prologue
    .line 39
    iget v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->junctionViewW:I

    return v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;

    .prologue
    .line 39
    iget v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->junctionViewH:I

    return v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method public static getInstance()Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->sInstance:Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;

    return-object v0
.end method

.method private removeJunctionView()V
    .locals 2

    .prologue
    .line 305
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$2;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$2;-><init>(Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 318
    return-void
.end method


# virtual methods
.method public onDisplayJunction(Lcom/navdy/hud/app/maps/MapEvents$DisplayJunction;)V
    .locals 4
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$DisplayJunction;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 201
    :try_start_0
    sget-object v1, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "onDisplayJunction"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 202
    iget-object v1, p1, Lcom/navdy/hud/app/maps/MapEvents$DisplayJunction;->junction:Lcom/here/android/mpa/common/Image;

    if-nez v1, :cond_0

    .line 203
    sget-object v1, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "no junction image"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 295
    :goto_0
    return-void

    .line 206
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->removeJunctionView()V

    .line 207
    iput-object p1, p0, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->lastJunctionEvent:Lcom/navdy/hud/app/maps/MapEvents$DisplayJunction;

    .line 208
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1;

    invoke-direct {v2, p0, p1}, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager$1;-><init>(Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;Lcom/navdy/hud/app/maps/MapEvents$DisplayJunction;)V

    const/16 v3, 0xa

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 292
    :catch_0
    move-exception v0

    .line 293
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onHideJunctionSignPost(Lcom/navdy/hud/app/maps/MapEvents$HideSignPostJunction;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$HideSignPostJunction;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 299
    sget-object v0, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onHideJunctionSignPost"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 300
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->lastJunctionEvent:Lcom/navdy/hud/app/maps/MapEvents$DisplayJunction;

    .line 301
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->removeJunctionView()V

    .line 302
    return-void
.end method

.method public onLiveTrafficDismiss(Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficDismissEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficDismissEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 98
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    const-string v1, "navdy#traffic#event#notif"

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;)V

    .line 99
    return-void
.end method

.method public onLiveTrafficEvent(Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent;)V
    .locals 6
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 71
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->isTrafficNotificationEnabled()Z

    move-result v3

    if-nez v3, :cond_0

    .line 72
    sget-object v3, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "traffic notification disabled:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 94
    :goto_0
    return-void

    .line 77
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v2

    .line 78
    .local v2, "notificationManager":Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    const-string v3, "navdy#traffic#reroute#notif"

    .line 79
    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isNotificationPresent(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "navdy#traffic#delay#notif"

    .line 80
    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isNotificationPresent(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "navdy#traffic#jam#notif"

    .line 81
    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isNotificationPresent(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 83
    .local v0, "isHigherPriorityActive":Z
    :goto_1
    if-eqz v0, :cond_3

    .line 84
    sget-object v3, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "higher priority notification active ignore:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent;->type:Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Type;

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Type;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 81
    .end local v0    # "isHigherPriorityActive":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 88
    .restart local v0    # "isHigherPriorityActive":Z
    :cond_3
    const-string v3, "navdy#traffic#event#notif"

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotification(Ljava/lang/String;)Lcom/navdy/hud/app/framework/notifications/INotification;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;

    .line 89
    .local v1, "notification":Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;
    if-nez v1, :cond_4

    .line 90
    new-instance v1, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;

    .end local v1    # "notification":Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;
    iget-object v3, p0, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->bus:Lcom/squareup/otto/Bus;

    invoke-direct {v1, v3}, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;-><init>(Lcom/squareup/otto/Bus;)V

    .line 92
    .restart local v1    # "notification":Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;
    :cond_4
    invoke-virtual {v1, p1}, Lcom/navdy/hud/app/maps/notification/TrafficEventNotification;->setTrafficEvent(Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent;)V

    .line 93
    invoke-virtual {v2, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->addNotification(Lcom/navdy/hud/app/framework/notifications/INotification;)V

    goto :goto_0
.end method

.method public onTrafficDelayDismiss(Lcom/navdy/hud/app/maps/MapEvents$TrafficDelayDismissEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$TrafficDelayDismissEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 133
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    const-string v1, "navdy#traffic#delay#notif"

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;)V

    .line 134
    return-void
.end method

.method public onTrafficDelayEvent(Lcom/navdy/hud/app/maps/MapEvents$TrafficDelayEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$TrafficDelayEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 104
    return-void
.end method

.method public onTrafficJamProgress(Lcom/navdy/hud/app/maps/MapEvents$TrafficJamProgressEvent;)V
    .locals 6
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$TrafficJamProgressEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 164
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->isTrafficNotificationEnabled()Z

    move-result v3

    if-nez v3, :cond_0

    .line 165
    sget-object v3, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "traffic notification disabled:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 186
    :goto_0
    return-void

    .line 169
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v2

    .line 170
    .local v2, "notificationManager":Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    const-string v3, "navdy#traffic#reroute#notif"

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isNotificationPresent(Ljava/lang/String;)Z

    move-result v0

    .line 172
    .local v0, "isHigherPriorityActive":Z
    if-eqz v0, :cond_1

    .line 173
    sget-object v3, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "reroute notification active ignore jam event:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Lcom/navdy/hud/app/maps/MapEvents$TrafficJamProgressEvent;->remainingTime:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 176
    :cond_1
    const-string v3, "navdy#traffic#event#notif"

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;)V

    .line 177
    const-string v3, "navdy#traffic#delay#notif"

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;)V

    .line 180
    const-string v3, "navdy#traffic#jam#notif"

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotification(Ljava/lang/String;)Lcom/navdy/hud/app/framework/notifications/INotification;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;

    .line 181
    .local v1, "notification":Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;
    if-nez v1, :cond_2

    .line 182
    new-instance v1, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;

    .end local v1    # "notification":Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;
    iget-object v3, p0, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->bus:Lcom/squareup/otto/Bus;

    invoke-direct {v1, v3, p1}, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/maps/MapEvents$TrafficJamProgressEvent;)V

    .line 184
    .restart local v1    # "notification":Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;
    :cond_2
    invoke-virtual {v1, p1}, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->setTrafficEvent(Lcom/navdy/hud/app/maps/MapEvents$TrafficJamProgressEvent;)V

    .line 185
    invoke-virtual {v2, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->addNotification(Lcom/navdy/hud/app/framework/notifications/INotification;)V

    goto :goto_0
.end method

.method public onTrafficJamProgressDismiss(Lcom/navdy/hud/app/maps/MapEvents$TrafficJamDismissEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$TrafficJamDismissEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 190
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    const-string v1, "navdy#traffic#jam#notif"

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;)V

    .line 191
    return-void
.end method

.method public onTrafficReroutePrompt(Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;)V
    .locals 5
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 138
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->isTrafficNotificationEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 139
    sget-object v2, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "traffic notification disabled:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 155
    :goto_0
    return-void

    .line 143
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v1

    .line 145
    .local v1, "notificationManager":Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    const-string v2, "navdy#traffic#event#notif"

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;)V

    .line 146
    const-string v2, "navdy#traffic#delay#notif"

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;)V

    .line 147
    const-string v2, "navdy#traffic#jam#notif"

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;)V

    .line 149
    const-string v2, "navdy#traffic#reroute#notif"

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotification(Ljava/lang/String;)Lcom/navdy/hud/app/framework/notifications/INotification;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;

    .line 150
    .local v0, "notification":Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;
    if-nez v0, :cond_1

    .line 151
    new-instance v0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;

    .end local v0    # "notification":Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;
    const-string v2, "navdy#traffic#reroute#notif"

    sget-object v3, Lcom/navdy/hud/app/framework/notifications/NotificationType;->FASTER_ROUTE:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    iget-object v4, p0, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->bus:Lcom/squareup/otto/Bus;

    invoke-direct {v0, v2, v3, v4}, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;-><init>(Ljava/lang/String;Lcom/navdy/hud/app/framework/notifications/NotificationType;Lcom/squareup/otto/Bus;)V

    .line 153
    .restart local v0    # "notification":Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;
    :cond_1
    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->setFasterRouteEvent(Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;)V

    .line 154
    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->addNotification(Lcom/navdy/hud/app/framework/notifications/INotification;)V

    goto :goto_0
.end method

.method public onTrafficReroutePromptDismiss(Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteDismissEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteDismissEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 159
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->removeFasterRouteNotifiation()V

    .line 160
    return-void
.end method

.method public removeFasterRouteNotifiation()V
    .locals 2

    .prologue
    .line 194
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    .line 195
    .local v0, "notificationManager":Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    const-string v1, "navdy#traffic#reroute#notif"

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;)V

    .line 196
    return-void
.end method
