.class Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$3;
.super Ljava/lang/Object;
.source "RouteCalculationNotification.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    .prologue
    .line 222
    iput-object p1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$3;->this$0:Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public executeItem(Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;)V
    .locals 8
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x1

    .line 225
    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$3;->this$0:Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    # getter for: Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->access$000(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;)Lcom/navdy/hud/app/framework/notifications/INotificationController;

    move-result-object v2

    if-nez v2, :cond_0

    .line 279
    :goto_0
    return-void

    .line 228
    :cond_0
    iget v2, p1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;->id:I

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 230
    :pswitch_0
    # getter for: Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "cancel"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 231
    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$3;->this$0:Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    # setter for: Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->dismissedbyUser:Z
    invoke-static {v2, v4}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->access$502(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;Z)Z

    .line 232
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordRouteSelectionCancelled()V

    .line 233
    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$3;->this$0:Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    # getter for: Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->lookupRequest:Lcom/navdy/service/library/events/places/DestinationSelectedRequest;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->access$100(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;)Lcom/navdy/service/library/events/places/DestinationSelectedRequest;

    move-result-object v2

    if-nez v2, :cond_1

    .line 234
    # getter for: Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "cancel route"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 235
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->cancelActiveRouteRequest()Z

    move-result v2

    if-nez v2, :cond_3

    .line 236
    # getter for: Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "route calc was not cancelled, may be it is not running"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 237
    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$3;->this$0:Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    # getter for: Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->routeId:Ljava/lang/String;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->access$600(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 238
    new-instance v2, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;-><init>()V

    iget-object v3, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$3;->this$0:Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    .line 239
    # getter for: Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->routeId:Ljava/lang/String;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->access$600(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->requestId(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;

    move-result-object v2

    sget-object v3, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_CANCELLED:Lcom/navdy/service/library/events/RequestStatus;

    .line 240
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->status(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;

    move-result-object v2

    new-instance v3, Lcom/navdy/service/library/events/location/Coordinate$Builder;

    invoke-direct {v3}, Lcom/navdy/service/library/events/location/Coordinate$Builder;-><init>()V

    .line 241
    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->latitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v3

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->longitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->build()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->destination(Lcom/navdy/service/library/events/location/Coordinate;)Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;

    move-result-object v2

    .line 242
    invoke-virtual {v2}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->build()Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

    move-result-object v1

    .line 243
    .local v1, "response":Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;
    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$3;->this$0:Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    # getter for: Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->access$700(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;)Lcom/squareup/otto/Bus;

    move-result-object v2

    new-instance v3, Lcom/navdy/hud/app/event/RemoteEvent;

    invoke-direct {v3, v1}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 244
    # getter for: Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "route calc event sent:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$3;->this$0:Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    # getter for: Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->routeId:Ljava/lang/String;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->access$600(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 252
    .end local v1    # "response":Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$3;->this$0:Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    # invokes: Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->dismissNotification()V
    invoke-static {v2}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->access$400(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;)V

    goto/16 :goto_0

    .line 246
    :cond_2
    # getter for: Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "route calc was not cancelled,no route id"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_1

    .line 249
    :cond_3
    # getter for: Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "route calc was cancelled"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_1

    .line 256
    :pswitch_1
    # getter for: Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "show route picker: choice"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 257
    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$3;->this$0:Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    # setter for: Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->dismissedbyUser:Z
    invoke-static {v2, v4}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->access$502(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;Z)Z

    .line 258
    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$3;->this$0:Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    # invokes: Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->showRoutePicker()V
    invoke-static {v2}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->access$800(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;)V

    goto/16 :goto_0

    .line 262
    :pswitch_2
    # getter for: Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "dismiss route"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 263
    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$3;->this$0:Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    # setter for: Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->dismissedbyUser:Z
    invoke-static {v2, v4}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->access$502(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;Z)Z

    .line 264
    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$3;->this$0:Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    # invokes: Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->dismissNotification()V
    invoke-static {v2}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->access$400(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;)V

    goto/16 :goto_0

    .line 268
    :pswitch_3
    # getter for: Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "retry"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 269
    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$3;->this$0:Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    # getter for: Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->routeCalculationEventHandler:Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->access$900(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;)Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->getCurrentRouteCalcEvent()Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    move-result-object v0

    .line 270
    .local v0, "event":Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;
    if-eqz v0, :cond_4

    iget-object v2, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    if-eqz v2, :cond_4

    .line 271
    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$3;->this$0:Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    # getter for: Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->access$700(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;)Lcom/squareup/otto/Bus;

    move-result-object v2

    iget-object v3, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 272
    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$3;->this$0:Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    # getter for: Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->access$700(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;)Lcom/squareup/otto/Bus;

    move-result-object v2

    new-instance v3, Lcom/navdy/hud/app/event/RemoteEvent;

    iget-object v4, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    invoke-direct {v3, v4}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 274
    :cond_4
    # getter for: Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "retry: request not found"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 275
    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$3;->this$0:Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    # invokes: Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->dismissNotification()V
    invoke-static {v2}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->access$400(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;)V

    goto/16 :goto_0

    .line 228
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public itemSelected(Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;)V
    .locals 0
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;

    .prologue
    .line 282
    return-void
.end method
