.class public abstract Lcom/navdy/hud/app/maps/notification/BaseTrafficNotification;
.super Ljava/lang/Object;
.source "BaseTrafficNotification.java"

# interfaces
.implements Lcom/navdy/hud/app/framework/notifications/INotification;


# static fields
.field protected static final EMPTY:Ljava/lang/String; = ""

.field public static final IMAGE_SCALE:F = 0.5f


# instance fields
.field protected choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

.field protected controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

.field protected logger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/BaseTrafficNotification;->logger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method


# virtual methods
.method protected dismissNotification()V
    .locals 2

    .prologue
    .line 65
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/notification/BaseTrafficNotification;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;)V

    .line 66
    return-void
.end method

.method public getTimeout()I
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    return v0
.end method

.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    return-object v0
.end method

.method public onClick()V
    .locals 0

    .prologue
    .line 69
    return-void
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 1
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    .line 75
    const/4 v0, 0x0

    return v0
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 4
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 45
    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/BaseTrafficNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    if-nez v2, :cond_0

    .line 61
    :goto_0
    return v0

    .line 48
    :cond_0
    sget-object v2, Lcom/navdy/hud/app/maps/notification/BaseTrafficNotification$1;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 50
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/BaseTrafficNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->moveSelectionLeft()Z

    move v0, v1

    .line 51
    goto :goto_0

    .line 54
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/BaseTrafficNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->moveSelectionRight()Z

    move v0, v1

    .line 55
    goto :goto_0

    .line 58
    :pswitch_2
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/BaseTrafficNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->executeSelectedItem()V

    move v0, v1

    .line 59
    goto :goto_0

    .line 48
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onStart(Lcom/navdy/hud/app/framework/notifications/INotificationController;)V
    .locals 0
    .param p1, "controller"    # Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/navdy/hud/app/maps/notification/BaseTrafficNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .line 28
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/BaseTrafficNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .line 33
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/BaseTrafficNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/BaseTrafficNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->clear()V

    .line 36
    :cond_0
    return-void
.end method

.method public onTrackHand(F)V
    .locals 0
    .param p1, "normalized"    # F

    .prologue
    .line 72
    return-void
.end method

.method public supportScroll()Z
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    return v0
.end method
