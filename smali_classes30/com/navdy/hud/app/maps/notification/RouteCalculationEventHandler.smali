.class public Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;
.super Ljava/lang/Object;
.source "RouteCalculationEventHandler.java"


# static fields
.field private static final MIN_ROUTE_CALC_DISPLAY_TIME:I

.field private static final ROUTE_CALC_DELAY_TIME:I

.field private static final logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private animationListener:Lcom/navdy/hud/app/ui/framework/INotificationAnimationListener;

.field private bus:Lcom/squareup/otto/Bus;

.field private currentRouteCalcEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

.field private handler:Landroid/os/Handler;

.field private routeCalcStartTime:J

.field private showRoutesCalcEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

.field private startRouteRunnable:Ljava/lang/Runnable;

.field private final startTrip:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 52
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->logger:Lcom/navdy/service/library/log/Logger;

    .line 56
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->MIN_ROUTE_CALC_DISPLAY_TIME:I

    .line 57
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->ROUTE_CALC_DELAY_TIME:I

    return-void
.end method

.method public constructor <init>(Lcom/squareup/otto/Bus;)V
    .locals 3
    .param p1, "bus"    # Lcom/squareup/otto/Bus;

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->handler:Landroid/os/Handler;

    .line 70
    new-instance v1, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler$1;-><init>(Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;)V

    iput-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->animationListener:Lcom/navdy/hud/app/ui/framework/INotificationAnimationListener;

    .line 95
    new-instance v1, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler$2;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler$2;-><init>(Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;)V

    iput-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->startRouteRunnable:Ljava/lang/Runnable;

    .line 103
    sget-object v1, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "ctor"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 104
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 105
    .local v0, "resources":Landroid/content/res/Resources;
    iput-object p1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->bus:Lcom/squareup/otto/Bus;

    .line 106
    const v1, 0x7f09027b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->startTrip:Ljava/lang/String;

    .line 107
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->animationListener:Lcom/navdy/hud/app/ui/framework/INotificationAnimationListener;

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->addNotificationAnimationListener(Lcom/navdy/hud/app/ui/framework/INotificationAnimationListener;)V

    .line 108
    invoke-virtual {p1, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 109
    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->startRoute()V

    return-void
.end method

.method private dismissNotification()V
    .locals 2

    .prologue
    .line 345
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    const-string v1, "navdy#route#calc#notif"

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;)V

    .line 346
    return-void
.end method

.method private navigateToRoute(Lcom/navdy/service/library/events/navigation/NavigationRouteResult;)V
    .locals 4
    .param p1, "result"    # Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    .prologue
    .line 323
    new-instance v2, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;-><init>()V

    sget-object v3, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_STARTED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 324
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;->newState(Lcom/navdy/service/library/events/navigation/NavigationSessionState;)Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->currentRouteCalcEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    iget-object v3, v3, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->response:Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

    iget-object v3, v3, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->label:Ljava/lang/String;

    .line 325
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;->label(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeId:Ljava/lang/String;

    .line 326
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;->routeId(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;

    move-result-object v2

    const/4 v3, 0x0

    .line 327
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;->simulationSpeed(Ljava/lang/Integer;)Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;

    move-result-object v2

    const/4 v3, 0x1

    .line 328
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;->originDisplay(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;

    move-result-object v1

    .line 330
    .local v1, "navigationSessionRequestBuilder":Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;
    invoke-virtual {v1}, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;->build()Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;

    move-result-object v0

    .line 331
    .local v0, "navigationSessionRequest":Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;
    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v2, v0}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 332
    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->bus:Lcom/squareup/otto/Bus;

    new-instance v3, Lcom/navdy/hud/app/event/RemoteEvent;

    invoke-direct {v3, v0}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 333
    return-void
.end method

.method private showMoreRoutes()V
    .locals 4

    .prologue
    .line 377
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "showMoreRoutes"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 378
    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->showRoutesCalcEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    if-nez v2, :cond_0

    .line 390
    :goto_0
    return-void

    .line 381
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v1

    .line 382
    .local v1, "notificationManager":Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    const-string v2, "navdy#route#calc#notif"

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotification(Ljava/lang/String;)Lcom/navdy/hud/app/framework/notifications/INotification;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    .line 383
    .local v0, "notif":Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;
    if-nez v0, :cond_1

    .line 384
    new-instance v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    .end local v0    # "notif":Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;
    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->bus:Lcom/squareup/otto/Bus;

    invoke-direct {v0, p0, v2}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;-><init>(Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;Lcom/squareup/otto/Bus;)V

    .line 386
    .restart local v0    # "notif":Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->showRoutesCalcEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->showRoutes(Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;)V

    .line 387
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->showRoutesCalcEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    .line 388
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "showMoreRoutes: posted notif"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 389
    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->addNotification(Lcom/navdy/hud/app/framework/notifications/INotification;)V

    goto :goto_0
.end method

.method private startRoute()V
    .locals 5

    .prologue
    .line 353
    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->currentRouteCalcEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->currentRouteCalcEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->currentRouteCalcEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->response:Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->currentRouteCalcEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->response:Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

    iget-object v2, v2, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->results:Ljava/util/List;

    if-nez v2, :cond_1

    .line 357
    :cond_0
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "invalid state"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 358
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->dismissNotification()V

    .line 374
    :goto_0
    return-void

    .line 361
    :cond_1
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "put user on route"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 362
    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->currentRouteCalcEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->response:Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

    iget-object v2, v2, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->results:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    .line 363
    .local v0, "result":Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->navigateToRoute(Lcom/navdy/service/library/events/navigation/NavigationRouteResult;)V

    .line 364
    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->currentRouteCalcEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->response:Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

    iget-object v2, v2, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->results:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    .line 365
    .local v1, "routes":I
    const/4 v2, 0x1

    if-le v1, v2, :cond_2

    .line 366
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "more route available:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 367
    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->currentRouteCalcEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    iput-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->showRoutesCalcEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    .line 368
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->showMoreRoutes()V

    goto :goto_0

    .line 370
    :cond_2
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "more route NOT available:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 371
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->currentRouteCalcEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    .line 372
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->dismissNotification()V

    goto :goto_0
.end method


# virtual methods
.method public clearCurrentRouteCalcEvent()V
    .locals 2

    .prologue
    .line 340
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->currentRouteCalcEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    .line 341
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->startRouteRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 342
    return-void
.end method

.method public getCurrentRouteCalcEvent()Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->currentRouteCalcEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    return-object v0
.end method

.method public hasMoreRoutes()Z
    .locals 1

    .prologue
    .line 349
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->showRoutesCalcEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onFirstManeuver(Lcom/navdy/hud/app/maps/MapEvents$ManeuverEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$ManeuverEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 113
    sget-object v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onFirstManeuver"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 114
    return-void
.end method

.method public onNavigationModeChanged(Lcom/navdy/hud/app/maps/MapEvents$NavigationModeChange;)V
    .locals 4
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$NavigationModeChange;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 118
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isSwitchingToNewRoute()Z

    move-result v0

    .line 119
    .local v0, "switching":Z
    sget-object v1, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onNavigationModeChanged event["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/hud/app/maps/MapEvents$NavigationModeChange;->navigationMode:Lcom/navdy/hud/app/maps/NavigationMode;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] switching="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 120
    iget-object v1, p1, Lcom/navdy/hud/app/maps/MapEvents$NavigationModeChange;->navigationMode:Lcom/navdy/hud/app/maps/NavigationMode;

    sget-object v2, Lcom/navdy/hud/app/maps/NavigationMode;->MAP:Lcom/navdy/hud/app/maps/NavigationMode;

    if-ne v1, v2, :cond_0

    if-nez v0, :cond_0

    .line 122
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->showRoutesCalcEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    .line 123
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->dismissNotification()V

    .line 125
    :cond_0
    return-void
.end method

.method public onRouteCalculation(Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;)V
    .locals 32
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 129
    sget-object v5, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "onRouteCalculation event["

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->state:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    move-object/from16 v29, v0

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "]"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v5, v0}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 130
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v21

    .line 131
    .local v21, "notificationManager":Lcom/navdy/hud/app/framework/notifications/NotificationManager;
    sget-object v5, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler$3;->$SwitchMap$com$navdy$hud$app$maps$MapEvents$RouteCalculationState:[I

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->state:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;->ordinal()I

    move-result v28

    aget v5, v5, v28

    packed-switch v5, :pswitch_data_0

    .line 320
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 135
    :pswitch_1
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v27

    .line 136
    .local v27, "uiStateManager":Lcom/navdy/hud/app/ui/framework/UIStateManager;
    invoke-virtual/range {v27 .. v27}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getCurrentScreen()Lcom/navdy/hud/app/screen/BaseScreen;

    move-result-object v25

    .line 137
    .local v25, "screen":Lcom/navdy/hud/app/screen/BaseScreen;
    if-eqz v25, :cond_1

    invoke-virtual/range {v25 .. v25}, Lcom/navdy/hud/app/screen/BaseScreen;->getScreen()Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v5

    sget-object v28, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DESTINATION_PICKER:Lcom/navdy/service/library/events/ui/Screen;

    move-object/from16 v0, v28

    if-ne v5, v0, :cond_1

    .line 138
    sget-object v5, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v28, "onRouteCalculation: destination picker on"

    move-object/from16 v0, v28

    invoke-virtual {v5, v0}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 139
    const/4 v5, 0x1

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->enableNotifications(Z)V

    .line 141
    :cond_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->handler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->startRouteRunnable:Ljava/lang/Runnable;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-virtual {v5, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 142
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->showRoutesCalcEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    .line 143
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->currentRouteCalcEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    .line 144
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v28

    move-wide/from16 v0, v28

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->routeCalcStartTime:J

    .line 145
    invoke-static {}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->getInstance()Lcom/navdy/hud/app/framework/destinations/DestinationsManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->clearSuggestedDestination()V

    .line 146
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getHomescreenView()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    move-result-object v17

    .line 147
    .local v17, "homeScreenView":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    if-eqz v17, :cond_2

    .line 148
    const/4 v5, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->setShowCollapsedNotification(Z)V

    .line 150
    :cond_2
    const-string v5, "navdy#route#calc#notif"

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotification(Ljava/lang/String;)Lcom/navdy/hud/app/framework/notifications/INotification;

    move-result-object v4

    check-cast v4, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    .line 151
    .local v4, "notif":Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;
    if-nez v4, :cond_3

    .line 152
    new-instance v4, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    .end local v4    # "notif":Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->bus:Lcom/squareup/otto/Bus;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v5}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;-><init>(Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;Lcom/squareup/otto/Bus;)V

    .line 156
    .restart local v4    # "notif":Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;
    :cond_3
    const/4 v14, 0x0

    .line 157
    .local v14, "destinationLabel":Ljava/lang/String;
    const/4 v15, 0x0

    .line 158
    .local v15, "destinationAddress":Ljava/lang/String;
    const/16 v16, 0x0

    .line 161
    .local v16, "destinationDistance":Ljava/lang/String;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->state:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    sget-object v28, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;->STARTED:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    move-object/from16 v0, v28

    if-ne v5, v0, :cond_c

    .line 163
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v5, v5, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->label:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_a

    .line 164
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v6, v5, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->label:Ljava/lang/String;

    .line 165
    .local v6, "label":Ljava/lang/String;
    move-object v14, v6

    .line 166
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v15, v5, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->streetAddress:Ljava/lang/String;

    .line 174
    :goto_1
    invoke-static {}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->getInstance()Lcom/navdy/hud/app/framework/destinations/DestinationsManager;

    move-result-object v5

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestDestination:Lcom/navdy/service/library/events/destination/Destination;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-virtual {v5, v0}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->transformToInternalDestination(Lcom/navdy/service/library/events/destination/Destination;)Lcom/navdy/hud/app/framework/destinations/Destination;

    move-result-object v26

    .line 191
    .local v26, "transformedDestination":Lcom/navdy/hud/app/framework/destinations/Destination;
    :goto_2
    const/4 v10, 0x0

    .line 192
    .local v10, "initials":Ljava/lang/String;
    const/4 v5, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    move-object/from16 v0, v26

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-static {v0, v5, v1, v2}, Lcom/navdy/hud/app/ui/component/mainmenu/PlacesMenu;->getPlaceModel(Lcom/navdy/hud/app/framework/destinations/Destination;ILjava/lang/String;Ljava/lang/String;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v20

    .line 194
    .local v20, "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->type:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    sget-object v28, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;->TWO_ICONS:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelType;

    move-object/from16 v0, v28

    if-ne v5, v0, :cond_f

    .line 195
    const/4 v5, 0x0

    move-object/from16 v0, v20

    iput v5, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconSelectedColor:I

    .line 205
    :cond_4
    :goto_3
    move-object/from16 v0, v20

    iget v7, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->icon:I

    .line 206
    .local v7, "icon":I
    move-object/from16 v0, v20

    iget v8, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconSelectedColor:I

    .line 208
    .local v8, "color":I
    if-eqz v26, :cond_6

    .line 209
    move-object/from16 v0, v26

    iget-object v5, v0, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationTitle:Ljava/lang/String;

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/navdy/hud/app/framework/destinations/Destination;->favoriteDestinationType:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-static {v5, v0}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->getInitials(Ljava/lang/String;Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;)Ljava/lang/String;

    move-result-object v10

    .line 210
    move-object/from16 v0, v26

    iget v5, v0, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationIcon:I

    if-eqz v5, :cond_5

    move-object/from16 v0, v26

    iget v5, v0, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationIconBkColor:I

    if-eqz v5, :cond_5

    .line 211
    move-object/from16 v0, v26

    iget v7, v0, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationIcon:I

    .line 212
    move-object/from16 v0, v26

    iget v8, v0, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationIconBkColor:I

    .line 214
    :cond_5
    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/navdy/hud/app/framework/destinations/Destination;->distanceStr:Ljava/lang/String;

    move-object/from16 v16, v0

    .line 217
    :cond_6
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    .line 218
    .local v22, "res":Landroid/content/res/Resources;
    const/4 v11, 0x0

    .line 219
    .local v11, "tts":Ljava/lang/String;
    sget-object v5, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler$3;->$SwitchMap$com$here$android$mpa$routing$RouteOptions$Type:[I

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->routeOptions:Lcom/here/android/mpa/routing/RouteOptions;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/here/android/mpa/routing/RouteOptions;->getRouteType()Lcom/here/android/mpa/routing/RouteOptions$Type;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/here/android/mpa/routing/RouteOptions$Type;->ordinal()I

    move-result v28

    aget v5, v5, v28

    packed-switch v5, :pswitch_data_1

    .line 229
    :goto_4
    const/4 v12, 0x0

    .line 230
    .local v12, "destination":Lcom/navdy/service/library/events/destination/Destination;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->lookupDestination:Lcom/navdy/hud/app/framework/destinations/Destination;

    if-eqz v5, :cond_7

    .line 231
    invoke-static {}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->getInstance()Lcom/navdy/hud/app/framework/destinations/DestinationsManager;

    move-result-object v5

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->lookupDestination:Lcom/navdy/hud/app/framework/destinations/Destination;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-virtual {v5, v0}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->transformToProtoDestination(Lcom/navdy/hud/app/framework/destinations/Destination;)Lcom/navdy/service/library/events/destination/Destination;

    move-result-object v12

    .line 233
    :cond_7
    const/4 v13, 0x0

    .line 234
    .local v13, "requestRouteId":Ljava/lang/String;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    if-eqz v5, :cond_8

    .line 235
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v13, v5, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestId:Ljava/lang/String;

    .line 237
    :cond_8
    const/4 v9, 0x0

    .line 238
    .local v9, "notifColor":I
    if-nez v8, :cond_9

    move-object/from16 v0, v20

    iget v5, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconFluctuatorColor:I

    const/16 v28, -0x1

    move/from16 v0, v28

    if-eq v5, v0, :cond_9

    .line 239
    move-object/from16 v0, v20

    iget v9, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconFluctuatorColor:I

    .line 241
    :cond_9
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->startTrip:Ljava/lang/String;

    invoke-virtual/range {v4 .. v16}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->showRouteSearch(Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/destination/Destination;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->addNotification(Lcom/navdy/hud/app/framework/notifications/INotification;)V

    goto/16 :goto_0

    .line 167
    .end local v6    # "label":Ljava/lang/String;
    .end local v7    # "icon":I
    .end local v8    # "color":I
    .end local v9    # "notifColor":I
    .end local v10    # "initials":Ljava/lang/String;
    .end local v11    # "tts":Ljava/lang/String;
    .end local v12    # "destination":Lcom/navdy/service/library/events/destination/Destination;
    .end local v13    # "requestRouteId":Ljava/lang/String;
    .end local v20    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .end local v22    # "res":Landroid/content/res/Resources;
    .end local v26    # "transformedDestination":Lcom/navdy/hud/app/framework/destinations/Destination;
    :cond_a
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v5, v5, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->streetAddress:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_b

    .line 168
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v6, v5, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->streetAddress:Ljava/lang/String;

    .line 169
    .restart local v6    # "label":Ljava/lang/String;
    move-object v14, v6

    goto/16 :goto_1

    .line 171
    .end local v6    # "label":Ljava/lang/String;
    :cond_b
    const-string v6, ""

    .line 172
    .restart local v6    # "label":Ljava/lang/String;
    move-object v14, v6

    goto/16 :goto_1

    .line 177
    .end local v6    # "label":Ljava/lang/String;
    :cond_c
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->lookupDestination:Lcom/navdy/hud/app/framework/destinations/Destination;

    if-eqz v5, :cond_d

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->lookupDestination:Lcom/navdy/hud/app/framework/destinations/Destination;

    iget-object v5, v5, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationTitle:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_d

    .line 178
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->lookupDestination:Lcom/navdy/hud/app/framework/destinations/Destination;

    iget-object v6, v5, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationTitle:Ljava/lang/String;

    .line 179
    .restart local v6    # "label":Ljava/lang/String;
    move-object v14, v6

    .line 180
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->lookupDestination:Lcom/navdy/hud/app/framework/destinations/Destination;

    iget-object v15, v5, Lcom/navdy/hud/app/framework/destinations/Destination;->fullAddress:Ljava/lang/String;

    .line 188
    :goto_5
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->lookupDestination:Lcom/navdy/hud/app/framework/destinations/Destination;

    move-object/from16 v26, v0

    .restart local v26    # "transformedDestination":Lcom/navdy/hud/app/framework/destinations/Destination;
    goto/16 :goto_2

    .line 181
    .end local v6    # "label":Ljava/lang/String;
    .end local v26    # "transformedDestination":Lcom/navdy/hud/app/framework/destinations/Destination;
    :cond_d
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->lookupDestination:Lcom/navdy/hud/app/framework/destinations/Destination;

    if-eqz v5, :cond_e

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->lookupDestination:Lcom/navdy/hud/app/framework/destinations/Destination;

    iget-object v5, v5, Lcom/navdy/hud/app/framework/destinations/Destination;->fullAddress:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_e

    .line 182
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->lookupDestination:Lcom/navdy/hud/app/framework/destinations/Destination;

    iget-object v6, v5, Lcom/navdy/hud/app/framework/destinations/Destination;->fullAddress:Ljava/lang/String;

    .line 183
    .restart local v6    # "label":Ljava/lang/String;
    move-object v14, v6

    goto :goto_5

    .line 185
    .end local v6    # "label":Ljava/lang/String;
    :cond_e
    const-string v6, ""

    .line 186
    .restart local v6    # "label":Ljava/lang/String;
    move-object v14, v6

    goto :goto_5

    .line 196
    .restart local v10    # "initials":Ljava/lang/String;
    .restart local v20    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .restart local v26    # "transformedDestination":Lcom/navdy/hud/app/framework/destinations/Destination;
    :cond_f
    if-nez v26, :cond_10

    .line 197
    sget v5, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->notifBkColor:I

    move-object/from16 v0, v20

    iput v5, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconSelectedColor:I

    goto/16 :goto_3

    .line 198
    :cond_10
    if-eqz v26, :cond_4

    move-object/from16 v0, v26

    iget-object v5, v0, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationType:Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;

    if-eqz v5, :cond_4

    move-object/from16 v0, v26

    iget-object v5, v0, Lcom/navdy/hud/app/framework/destinations/Destination;->destinationType:Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;

    sget-object v28, Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;->DEFAULT:Lcom/navdy/hud/app/framework/destinations/Destination$DestinationType;

    move-object/from16 v0, v28

    if-ne v5, v0, :cond_4

    move-object/from16 v0, v26

    iget-object v5, v0, Lcom/navdy/hud/app/framework/destinations/Destination;->favoriteDestinationType:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    if-eqz v5, :cond_4

    move-object/from16 v0, v26

    iget-object v5, v0, Lcom/navdy/hud/app/framework/destinations/Destination;->favoriteDestinationType:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    sget-object v28, Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;->FAVORITE_NONE:Lcom/navdy/hud/app/framework/destinations/Destination$FavoriteDestinationType;

    move-object/from16 v0, v28

    if-ne v5, v0, :cond_4

    .line 201
    const v5, 0x7f020184

    move-object/from16 v0, v20

    iput v5, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->icon:I

    .line 202
    sget v5, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->notifBkColor:I

    move-object/from16 v0, v20

    iput v5, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->iconSelectedColor:I

    goto/16 :goto_3

    .line 221
    .restart local v7    # "icon":I
    .restart local v8    # "color":I
    .restart local v11    # "tts":Ljava/lang/String;
    .restart local v22    # "res":Landroid/content/res/Resources;
    :pswitch_2
    const v5, 0x7f090105

    const/16 v28, 0x2

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    sget-object v30, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->shortestRoute:Ljava/lang/String;

    aput-object v30, v28, v29

    const/16 v29, 0x1

    aput-object v6, v28, v29

    move-object/from16 v0, v22

    move-object/from16 v1, v28

    invoke-virtual {v0, v5, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 222
    goto/16 :goto_4

    .line 225
    :pswitch_3
    const v5, 0x7f090105

    const/16 v28, 0x2

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    sget-object v30, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->fastestRoute:Ljava/lang/String;

    aput-object v30, v28, v29

    const/16 v29, 0x1

    aput-object v6, v28, v29

    move-object/from16 v0, v22

    move-object/from16 v1, v28

    invoke-virtual {v0, v5, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_4

    .line 248
    .end local v4    # "notif":Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;
    .end local v6    # "label":Ljava/lang/String;
    .end local v7    # "icon":I
    .end local v8    # "color":I
    .end local v10    # "initials":Ljava/lang/String;
    .end local v11    # "tts":Ljava/lang/String;
    .end local v14    # "destinationLabel":Ljava/lang/String;
    .end local v15    # "destinationAddress":Ljava/lang/String;
    .end local v16    # "destinationDistance":Ljava/lang/String;
    .end local v17    # "homeScreenView":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    .end local v20    # "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    .end local v22    # "res":Landroid/content/res/Resources;
    .end local v25    # "screen":Lcom/navdy/hud/app/screen/BaseScreen;
    .end local v26    # "transformedDestination":Lcom/navdy/hud/app/framework/destinations/Destination;
    .end local v27    # "uiStateManager":Lcom/navdy/hud/app/ui/framework/UIStateManager;
    :pswitch_4
    move-object/from16 v0, p1

    iget-boolean v5, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->stopped:Z

    if-nez v5, :cond_0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->currentRouteCalcEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    if-eqz v5, :cond_0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->currentRouteCalcEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->response:Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

    if-eqz v5, :cond_0

    .line 253
    const/4 v5, 0x1

    move-object/from16 v0, p1

    iput-boolean v5, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->stopped:Z

    .line 254
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->currentRouteCalcEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->response:Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

    iget-object v5, v5, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    sget-object v28, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    move-object/from16 v0, v28

    if-ne v5, v0, :cond_13

    .line 256
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v5

    const-string v28, "navdy#route#calc#notif"

    move-object/from16 v0, v28

    invoke-virtual {v5, v0}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotification(Ljava/lang/String;)Lcom/navdy/hud/app/framework/notifications/INotification;

    move-result-object v24

    check-cast v24, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    .line 257
    .local v24, "routeCNotif":Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;
    if-eqz v24, :cond_11

    .line 258
    sget-object v5, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v28, "hideStartTrip"

    move-object/from16 v0, v28

    invoke-virtual {v5, v0}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 259
    invoke-virtual/range {v24 .. v24}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->hideStartTrip()V

    .line 262
    :cond_11
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v28

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->routeCalcStartTime:J

    move-wide/from16 v30, v0

    sub-long v18, v28, v30

    .line 263
    .local v18, "diff":J
    sget v5, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->MIN_ROUTE_CALC_DISPLAY_TIME:I

    int-to-long v0, v5

    move-wide/from16 v28, v0

    cmp-long v5, v18, v28

    if-gez v5, :cond_12

    .line 264
    sget-object v5, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "route calc too fast["

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "] wait"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v5, v0}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 265
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->handler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->startRouteRunnable:Ljava/lang/Runnable;

    move-object/from16 v28, v0

    sget v29, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->ROUTE_CALC_DELAY_TIME:I

    move/from16 v0, v29

    int-to-long v0, v0

    move-wide/from16 v30, v0

    move-object/from16 v0, v28

    move-wide/from16 v1, v30

    invoke-virtual {v5, v0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 267
    :cond_12
    sget-object v5, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "route calc took["

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "]"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v5, v0}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 268
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->startRoute()V

    goto/16 :goto_0

    .line 271
    .end local v18    # "diff":J
    .end local v24    # "routeCNotif":Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;
    :cond_13
    sget-object v5, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "routing calculation not successful:"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->currentRouteCalcEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    iget-object v0, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->response:Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    move-object/from16 v29, v0

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v5, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 272
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->currentRouteCalcEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->response:Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

    iget-object v5, v5, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    sget-object v28, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_CANCELLED:Lcom/navdy/service/library/events/RequestStatus;

    move-object/from16 v0, v28

    if-ne v5, v0, :cond_14

    .line 273
    sget-object v5, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v28, "request cancelled"

    move-object/from16 v0, v28

    invoke-virtual {v5, v0}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 274
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->currentRouteCalcEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    .line 275
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->dismissNotification()V

    goto/16 :goto_0

    .line 278
    :cond_14
    sget-object v5, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v28, "show error"

    move-object/from16 v0, v28

    invoke-virtual {v5, v0}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 279
    const-string v5, "navdy#route#calc#notif"

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotification(Ljava/lang/String;)Lcom/navdy/hud/app/framework/notifications/INotification;

    move-result-object v4

    check-cast v4, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    .line 280
    .restart local v4    # "notif":Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;
    if-nez v4, :cond_15

    .line 281
    new-instance v4, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    .end local v4    # "notif":Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->bus:Lcom/squareup/otto/Bus;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v5}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;-><init>(Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;Lcom/squareup/otto/Bus;)V

    .line 283
    .restart local v4    # "notif":Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;
    :cond_15
    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->showError()V

    .line 284
    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->addNotification(Lcom/navdy/hud/app/framework/notifications/INotification;)V

    goto/16 :goto_0

    .line 292
    .end local v4    # "notif":Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;
    :pswitch_5
    sget-object v5, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "abort-nav routeCalc event[ABORT_NAVIGATION] "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->pendingNavigationRequestId:Ljava/lang/String;

    move-object/from16 v29, v0

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v5, v0}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 293
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->currentRouteCalcEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    if-eqz v5, :cond_16

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->currentRouteCalcEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    if-nez v5, :cond_17

    .line 294
    :cond_16
    sget-object v5, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v28, "abort-nav:no current route calc event"

    move-object/from16 v0, v28

    invoke-virtual {v5, v0}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 297
    :cond_17
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->currentRouteCalcEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v5, v5, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestId:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->pendingNavigationRequestId:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-static {v5, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_18

    .line 298
    sget-object v5, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "abort-nav:does not match current request:"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->currentRouteCalcEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    iget-object v0, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestId:Ljava/lang/String;

    move-object/from16 v29, v0

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v5, v0}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 301
    :cond_18
    sget-object v5, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v28, "abort-nav: found current route id, cancel"

    move-object/from16 v0, v28

    invoke-virtual {v5, v0}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 302
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->currentRouteCalcEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    .line 303
    move-object/from16 v0, p1

    iget-boolean v5, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->abortOriginDisplay:Z

    if-eqz v5, :cond_19

    .line 304
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->bus:Lcom/squareup/otto/Bus;

    new-instance v28, Lcom/navdy/hud/app/event/RemoteEvent;

    new-instance v29, Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->pendingNavigationRequestId:Ljava/lang/String;

    move-object/from16 v30, v0

    invoke-direct/range {v29 .. v30}, Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;-><init>(Ljava/lang/String;)V

    invoke-direct/range {v28 .. v29}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    move-object/from16 v0, v28

    invoke-virtual {v5, v0}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 305
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->dismissNotification()V

    goto/16 :goto_0

    .line 308
    :cond_19
    sget-object v5, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "abort-nav: sent navrouteresponse cancel ["

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->pendingNavigationRequestId:Ljava/lang/String;

    move-object/from16 v29, v0

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "]"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v5, v0}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 309
    new-instance v5, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;

    invoke-direct {v5}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;-><init>()V

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->pendingNavigationRequestId:Ljava/lang/String;

    move-object/from16 v28, v0

    .line 310
    move-object/from16 v0, v28

    invoke-virtual {v5, v0}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->requestId(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;

    move-result-object v5

    sget-object v28, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_CANCELLED:Lcom/navdy/service/library/events/RequestStatus;

    .line 311
    move-object/from16 v0, v28

    invoke-virtual {v5, v0}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->status(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;

    move-result-object v5

    new-instance v28, Lcom/navdy/service/library/events/location/Coordinate$Builder;

    invoke-direct/range {v28 .. v28}, Lcom/navdy/service/library/events/location/Coordinate$Builder;-><init>()V

    const-wide/16 v30, 0x0

    .line 312
    invoke-static/range {v30 .. v31}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->latitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v28

    const-wide/16 v30, 0x0

    invoke-static/range {v30 .. v31}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->longitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->build()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v5, v0}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->destination(Lcom/navdy/service/library/events/location/Coordinate;)Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;

    move-result-object v5

    .line 313
    invoke-virtual {v5}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->build()Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

    move-result-object v23

    .line 314
    .local v23, "response":Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->bus:Lcom/squareup/otto/Bus;

    new-instance v28, Lcom/navdy/hud/app/event/RemoteEvent;

    move-object/from16 v0, v28

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    move-object/from16 v0, v28

    invoke-virtual {v5, v0}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 315
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->pendingNavigationRequestId:Ljava/lang/String;

    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->clearActiveRouteCalc(Ljava/lang/String;)V

    .line 316
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->dismissNotification()V

    goto/16 :goto_0

    .line 131
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_5
    .end packed-switch

    .line 219
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
