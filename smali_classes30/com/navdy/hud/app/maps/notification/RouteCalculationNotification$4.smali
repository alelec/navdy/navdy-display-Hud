.class Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$4;
.super Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;
.source "RouteCalculationNotification.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->startLoadingAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    .prologue
    .line 558
    iput-object p1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$4;->this$0:Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    invoke-direct {p0}, Lcom/navdy/hud/app/ui/framework/DefaultAnimationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 4
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 561
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$4;->this$0:Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    # getter for: Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->loadingAnimator:Landroid/animation/ObjectAnimator;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->access$1000(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 562
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$4;->this$0:Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    # getter for: Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->loadingAnimator:Landroid/animation/ObjectAnimator;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->access$1000(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v2, 0x21

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 563
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$4;->this$0:Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    # getter for: Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->loadingAnimator:Landroid/animation/ObjectAnimator;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->access$1000(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 567
    :goto_0
    return-void

    .line 565
    :cond_0
    # getter for: Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "abandon loading animation"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method
