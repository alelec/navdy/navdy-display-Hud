.class public Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;
.super Ljava/lang/Object;
.source "RouteCalculationNotification.java"

# interfaces
.implements Lcom/navdy/hud/app/framework/notifications/INotification;


# static fields
.field public static final CANCEL_CALC_TTS:Lcom/navdy/hud/app/event/RemoteEvent;

.field public static final CANCEL_TBT_TTS:Lcom/navdy/hud/app/event/RemoteEvent;

.field private static final ICON_SCALE_FACTOR:F = 1.38f

.field private static final NAV_LOOK_UP_TIMEOUT:I

.field public static final ROUTE_CALC_TTS_ID:Ljava/lang/String;

.field public static final ROUTE_PICKER:Ljava/lang/String; = "ROUTE_PICKER"

.field public static final ROUTE_TBT_TTS_ID:Ljava/lang/String;

.field private static final TAG_CANCEL:I = 0x1

.field private static final TAG_DISMISS:I = 0x3

.field private static final TAG_RETRY:I = 0x4

.field private static final TAG_ROUTES:I = 0x2

.field private static final TIMEOUT:I

.field private static cancelRouteCalc:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;",
            ">;"
        }
    .end annotation
.end field

.field public static final fastestRoute:Ljava/lang/String;

.field public static final notifBkColor:I

.field private static final resources:Landroid/content/res/Resources;

.field private static routeError:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;",
            ">;"
        }
    .end annotation
.end field

.field private static final routeFailed:Ljava/lang/String;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field public static final shortestRoute:Ljava/lang/String;

.field private static final showRouteBkColor:I

.field private static showRoutes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;",
            ">;"
        }
    .end annotation
.end field

.field private static final startTripImageContainerSize:I

.field private static final startTripImageSize:I

.field private static final startTripLeftMargin:I

.field private static final startTripTextLeftMargin:I


# instance fields
.field private bus:Lcom/squareup/otto/Bus;

.field private choiceLayoutView:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

.field private choices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;",
            ">;"
        }
    .end annotation
.end field

.field private containerView:Landroid/view/ViewGroup;

.field private controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

.field private destinationAddress:Ljava/lang/String;

.field private destinationDistance:Ljava/lang/String;

.field private destinationLabel:Ljava/lang/String;

.field private dismissedbyUser:Z

.field private event:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

.field private handler:Landroid/os/Handler;

.field private icon:I

.field private iconBkColor:I

.field private iconColorImageView:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

.field private iconInitialsImageView:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

.field private iconSide:I

.field private iconSpinnerView:Landroid/widget/ImageView;

.field private initials:Ljava/lang/String;

.field private listener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

.field private loadingAnimator:Landroid/animation/ObjectAnimator;

.field private lookupDestination:Lcom/navdy/service/library/events/destination/Destination;

.field private lookupRequest:Lcom/navdy/service/library/events/places/DestinationSelectedRequest;

.field private notifColor:I

.field private registered:Z

.field private routeCalculationEventHandler:Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;

.field private routeId:Ljava/lang/String;

.field private routeTtaView:Landroid/widget/TextView;

.field private startTime:J

.field private subTitle:Ljava/lang/String;

.field private subTitleView:Landroid/widget/TextView;

.field private timeoutRunnable:Ljava/lang/Runnable;

.field private title:Ljava/lang/String;

.field private titleView:Landroid/widget/TextView;

.field private tripDuration:Landroid/text/SpannableStringBuilder;

.field private tts:Ljava/lang/String;

.field private ttsPlayed:Z

.field private uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

.field private waitForNavLookupRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 29

    .prologue
    .line 84
    new-instance v2, Lcom/navdy/service/library/log/Logger;

    const-class v3, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    invoke-direct {v2, v3}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 88
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v8, 0x1e

    invoke-virtual {v2, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    long-to-int v2, v2

    sput v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->TIMEOUT:I

    .line 90
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v8, 0xa

    invoke-virtual {v2, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    long-to-int v2, v2

    sput v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->NAV_LOOK_UP_TIMEOUT:I

    .line 94
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "route_calc_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->ROUTE_CALC_TTS_ID:Ljava/lang/String;

    .line 95
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "route_tbt_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->ROUTE_TBT_TTS_ID:Ljava/lang/String;

    .line 115
    new-instance v2, Lcom/navdy/hud/app/event/RemoteEvent;

    new-instance v3, Lcom/navdy/service/library/events/audio/CancelSpeechRequest;

    sget-object v4, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->ROUTE_CALC_TTS_ID:Ljava/lang/String;

    invoke-direct {v3, v4}, Lcom/navdy/service/library/events/audio/CancelSpeechRequest;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    sput-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->CANCEL_CALC_TTS:Lcom/navdy/hud/app/event/RemoteEvent;

    .line 117
    new-instance v2, Lcom/navdy/hud/app/event/RemoteEvent;

    new-instance v3, Lcom/navdy/service/library/events/audio/CancelSpeechRequest;

    sget-object v4, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->ROUTE_TBT_TTS_ID:Ljava/lang/String;

    invoke-direct {v3, v4}, Lcom/navdy/service/library/events/audio/CancelSpeechRequest;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    sput-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->CANCEL_TBT_TTS:Lcom/navdy/hud/app/event/RemoteEvent;

    .line 125
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v27

    .line 126
    .local v27, "context":Landroid/content/Context;
    invoke-virtual/range {v27 .. v27}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sput-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->resources:Landroid/content/res/Resources;

    .line 128
    const/high16 v7, -0x1000000

    .line 129
    .local v7, "unselectedColor":I
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0d0021

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    .line 130
    .local v5, "dismissColor":I
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0d0024

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v28

    .line 131
    .local v28, "retryColor":I
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0d0024

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    .line 132
    .local v11, "moreColor":I
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0d0025

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v18

    .line 133
    .local v18, "goColor":I
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0901c0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 135
    .local v14, "moreRoutes":Ljava/lang/String;
    new-instance v2, Ljava/util/ArrayList;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->cancelRouteCalc:Ljava/util/List;

    .line 136
    sget-object v10, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->cancelRouteCalc:Ljava/util/List;

    new-instance v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const/4 v3, 0x1

    const v4, 0x7f020125

    const v6, 0x7f020125

    sget-object v8, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->resources:Landroid/content/res/Resources;

    const v9, 0x7f09004f

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    move v9, v5

    invoke-direct/range {v2 .. v9}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-interface {v10, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 138
    new-instance v2, Ljava/util/ArrayList;

    const/4 v3, 0x2

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->showRoutes:Ljava/util/List;

    .line 139
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->showRoutes:Ljava/util/List;

    new-instance v8, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const/4 v9, 0x2

    const v10, 0x7f02012a

    const v12, 0x7f02012a

    move v13, v7

    move v15, v11

    invoke-direct/range {v8 .. v15}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-interface {v2, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 140
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->showRoutes:Ljava/util/List;

    new-instance v15, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const/16 v16, 0x3

    const v17, 0x7f020129

    const v19, 0x7f020129

    sget-object v3, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->resources:Landroid/content/res/Resources;

    const v4, 0x7f090243

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v21

    move/from16 v20, v7

    move/from16 v22, v18

    invoke-direct/range {v15 .. v22}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-interface {v2, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 142
    new-instance v2, Ljava/util/ArrayList;

    const/4 v3, 0x2

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->routeError:Ljava/util/List;

    .line 143
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->routeError:Ljava/util/List;

    new-instance v19, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const/16 v20, 0x4

    const v21, 0x7f02012c

    const v23, 0x7f02012c

    sget-object v3, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->resources:Landroid/content/res/Resources;

    const v4, 0x7f09022d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v25

    move/from16 v22, v28

    move/from16 v24, v7

    move/from16 v26, v28

    invoke-direct/range {v19 .. v26}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    move-object/from16 v0, v19

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 144
    sget-object v10, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->routeError:Ljava/util/List;

    new-instance v2, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const/4 v3, 0x3

    const v4, 0x7f020125

    const v6, 0x7f020125

    sget-object v8, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->resources:Landroid/content/res/Resources;

    const v9, 0x7f0900dc

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    move v9, v5

    invoke-direct/range {v2 .. v9}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-interface {v10, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 146
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f090242

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->routeFailed:Ljava/lang/String;

    .line 147
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f090103

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->fastestRoute:Ljava/lang/String;

    .line 148
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f090260

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->shortestRoute:Ljava/lang/String;

    .line 150
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0d009e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sput v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->showRouteBkColor:I

    .line 151
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0d009d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sput v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->notifBkColor:I

    .line 153
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0b0154

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->startTripLeftMargin:I

    .line 154
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0b0152

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->startTripImageContainerSize:I

    .line 155
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0b0153

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->startTripImageSize:I

    .line 156
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0b0155

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->startTripTextLeftMargin:I

    .line 157
    return-void
.end method

.method public constructor <init>(Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;Lcom/squareup/otto/Bus;)V
    .locals 2
    .param p1, "routeCalculationEventHandler"    # Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;
    .param p2, "bus"    # Lcom/squareup/otto/Bus;

    .prologue
    const/4 v0, 0x0

    .line 291
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 163
    iput v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->iconBkColor:I

    .line 164
    iput v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->notifColor:I

    .line 198
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->handler:Landroid/os/Handler;

    .line 200
    new-instance v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$1;-><init>(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->waitForNavLookupRunnable:Ljava/lang/Runnable;

    .line 214
    new-instance v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$2;-><init>(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->timeoutRunnable:Ljava/lang/Runnable;

    .line 222
    new-instance v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$3;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$3;-><init>(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->listener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

    .line 285
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    .line 292
    iput-object p1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->routeCalculationEventHandler:Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;

    .line 293
    iput-object p2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->bus:Lcom/squareup/otto/Bus;

    .line 294
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;)Lcom/navdy/hud/app/framework/notifications/INotificationController;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;)Lcom/navdy/service/library/events/places/DestinationSelectedRequest;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->lookupRequest:Lcom/navdy/service/library/events/places/DestinationSelectedRequest;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;)Landroid/animation/ObjectAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->loadingAnimator:Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method static synthetic access$102(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;Lcom/navdy/service/library/events/places/DestinationSelectedRequest;)Lcom/navdy/service/library/events/places/DestinationSelectedRequest;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;
    .param p1, "x1"    # Lcom/navdy/service/library/events/places/DestinationSelectedRequest;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->lookupRequest:Lcom/navdy/service/library/events/places/DestinationSelectedRequest;

    return-object p1
.end method

.method static synthetic access$200()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->launchOriginalDestination()V

    return-void
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->dismissNotification()V

    return-void
.end method

.method static synthetic access$502(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;
    .param p1, "x1"    # Z

    .prologue
    .line 83
    iput-boolean p1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->dismissedbyUser:Z

    return p1
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->routeId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;)Lcom/squareup/otto/Bus;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->bus:Lcom/squareup/otto/Bus;

    return-object v0
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->showRoutePicker()V

    return-void
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;)Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->routeCalculationEventHandler:Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;

    return-object v0
.end method

.method private dismissNotification()V
    .locals 2

    .prologue
    .line 499
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->routeCalculationEventHandler:Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->clearCurrentRouteCalcEvent()V

    .line 500
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v0

    const-string v1, "navdy#route#calc#notif"

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;)V

    .line 501
    return-void
.end method

.method private launchOriginalDestination()V
    .locals 4

    .prologue
    .line 838
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->lookupDestination:Lcom/navdy/service/library/events/destination/Destination;

    if-eqz v1, :cond_0

    .line 839
    invoke-static {}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->getInstance()Lcom/navdy/hud/app/framework/destinations/DestinationsManager;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->lookupDestination:Lcom/navdy/service/library/events/destination/Destination;

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->transformToInternalDestination(Lcom/navdy/service/library/events/destination/Destination;)Lcom/navdy/hud/app/framework/destinations/Destination;

    move-result-object v0

    .line 840
    .local v0, "d":Lcom/navdy/hud/app/framework/destinations/Destination;
    invoke-static {}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->getInstance()Lcom/navdy/hud/app/framework/destinations/DestinationsManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->requestNavigation(Lcom/navdy/hud/app/framework/destinations/Destination;)V

    .line 841
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->lookupDestination:Lcom/navdy/service/library/events/destination/Destination;

    .line 842
    sget-object v1, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "original destination launched:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 846
    .end local v0    # "d":Lcom/navdy/hud/app/framework/destinations/Destination;
    :goto_0
    return-void

    .line 844
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "original destination not launched"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private playTts(Ljava/lang/String;)V
    .locals 3
    .param p1, "tts"    # Ljava/lang/String;

    .prologue
    .line 782
    sget-object v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "tts["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 783
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getNavigationSessionPreference()Lcom/navdy/hud/app/maps/NavSessionPreferences;

    move-result-object v0

    iget-boolean v0, v0, Lcom/navdy/hud/app/maps/NavSessionPreferences;->spokenTurnByTurn:Z

    if-eqz v0, :cond_0

    .line 784
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->bus:Lcom/squareup/otto/Bus;

    sget-object v1, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->CANCEL_CALC_TTS:Lcom/navdy/hud/app/event/RemoteEvent;

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 785
    sget-object v0, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->SPEECH_TURN_BY_TURN:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    sget-object v1, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->ROUTE_CALC_TTS_ID:Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lcom/navdy/hud/app/framework/voice/TTSUtils;->sendSpeechRequest(Ljava/lang/String;Lcom/navdy/service/library/events/audio/SpeechRequest$Category;Ljava/lang/String;)V

    .line 787
    :cond_0
    return-void
.end method

.method private setIcon()V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 542
    iget v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->iconBkColor:I

    if-eqz v0, :cond_0

    .line 543
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->iconColorImageView:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    iget v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->icon:I

    iget v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->iconBkColor:I

    const/4 v3, 0x0

    const v4, 0x3fb0a3d7    # 1.38f

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setIcon(IILandroid/graphics/Shader;F)V

    .line 544
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->iconColorImageView:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    invoke-virtual {v0, v5}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setVisibility(I)V

    .line 545
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->iconInitialsImageView:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    invoke-virtual {v0, v6}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setVisibility(I)V

    .line 551
    :goto_0
    return-void

    .line 547
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->iconInitialsImageView:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    iget v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->icon:I

    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->initials:Ljava/lang/String;

    sget-object v3, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->LARGE:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    invoke-virtual {v0, v1, v2, v3}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setImage(ILjava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    .line 548
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->iconInitialsImageView:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    invoke-virtual {v0, v5}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setVisibility(I)V

    .line 549
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->iconColorImageView:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    invoke-virtual {v0, v6}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private setUI()V
    .locals 6

    .prologue
    const/16 v4, 0x8

    .line 504
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->startTime:J

    .line 505
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->titleView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 506
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->subTitleView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->subTitle:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 507
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->iconSpinnerView:Landroid/widget/ImageView;

    iget v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->iconSide:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 509
    const/4 v0, 0x0

    .line 510
    .local v0, "selection":I
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->choices:Ljava/util/List;

    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->cancelRouteCalc:Ljava/util/List;

    if-ne v1, v2, :cond_2

    .line 511
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->routeTtaView:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 512
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->setIcon()V

    .line 513
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->startLoadingAnimation()V

    .line 514
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->tts:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->ttsPlayed:Z

    if-nez v1, :cond_0

    .line 515
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->tts:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->playTts(Ljava/lang/String;)V

    .line 516
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->tts:Ljava/lang/String;

    .line 517
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->ttsPlayed:Z

    .line 519
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->lookupDestination:Lcom/navdy/service/library/events/destination/Destination;

    if-eqz v1, :cond_1

    .line 520
    new-instance v1, Lcom/navdy/service/library/events/places/DestinationSelectedRequest$Builder;

    invoke-direct {v1}, Lcom/navdy/service/library/events/places/DestinationSelectedRequest$Builder;-><init>()V

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/places/DestinationSelectedRequest$Builder;->request_id(Ljava/lang/String;)Lcom/navdy/service/library/events/places/DestinationSelectedRequest$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->lookupDestination:Lcom/navdy/service/library/events/destination/Destination;

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/places/DestinationSelectedRequest$Builder;->destination(Lcom/navdy/service/library/events/destination/Destination;)Lcom/navdy/service/library/events/places/DestinationSelectedRequest$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/service/library/events/places/DestinationSelectedRequest$Builder;->build()Lcom/navdy/service/library/events/places/DestinationSelectedRequest;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->lookupRequest:Lcom/navdy/service/library/events/places/DestinationSelectedRequest;

    .line 521
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->waitForNavLookupRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 522
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->waitForNavLookupRunnable:Ljava/lang/Runnable;

    sget v3, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->NAV_LOOK_UP_TIMEOUT:I

    int-to-long v4, v3

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 523
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->bus:Lcom/squareup/otto/Bus;

    new-instance v2, Lcom/navdy/hud/app/event/RemoteEvent;

    iget-object v3, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->lookupRequest:Lcom/navdy/service/library/events/places/DestinationSelectedRequest;

    invoke-direct {v2, v3}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 524
    sget-object v1, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "launched nav lookup request:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->lookupRequest:Lcom/navdy/service/library/events/places/DestinationSelectedRequest;

    iget-object v3, v3, Lcom/navdy/service/library/events/places/DestinationSelectedRequest;->request_id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " dest:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->lookupDestination:Lcom/navdy/service/library/events/destination/Destination;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " placeid:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->lookupDestination:Lcom/navdy/service/library/events/destination/Destination;

    iget-object v3, v3, Lcom/navdy/service/library/events/destination/Destination;->place_id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 537
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->choiceLayoutView:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->choices:Ljava/util/List;

    iget-object v3, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->listener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

    invoke-virtual {v1, v2, v0, v3}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;)V

    .line 538
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->getTimeout()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->startTimeout(I)V

    .line 539
    return-void

    .line 526
    :cond_2
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->choices:Ljava/util/List;

    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->showRoutes:Ljava/util/List;

    if-ne v1, v2, :cond_3

    .line 527
    const/4 v0, 0x1

    .line 528
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->setIcon()V

    .line 529
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->stopLoadingAnimation()V

    .line 530
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->routeTtaView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->tripDuration:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 531
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->routeTtaView:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 532
    :cond_3
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->choices:Ljava/util/List;

    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->routeError:Ljava/util/List;

    if-ne v1, v2, :cond_1

    .line 533
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->routeTtaView:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 534
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->setIcon()V

    .line 535
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->stopLoadingAnimation()V

    goto :goto_0
.end method

.method private showRoutePicker()V
    .locals 43

    .prologue
    .line 679
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->event:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->event:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->response:Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->event:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    if-nez v4, :cond_1

    .line 680
    :cond_0
    sget-object v4, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "showRoutePicker invalid state"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 681
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->dismissNotification()V

    .line 779
    :goto_0
    return-void

    .line 684
    :cond_1
    new-instance v26, Landroid/os/Bundle;

    invoke-direct/range {v26 .. v26}, Landroid/os/Bundle;-><init>()V

    .line 685
    .local v26, "args":Landroid/os/Bundle;
    const-string v4, "PICKER_LEFT_TITLE"

    sget-object v5, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->resources:Landroid/content/res/Resources;

    const v8, 0x7f09024d

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 686
    const-string v4, "PICKER_LEFT_ICON"

    const v5, 0x7f0201d2

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 687
    const-string v4, "PICKER_LEFT_ICON_BKCOLOR"

    const/4 v5, 0x0

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 688
    const-string v4, "PICKER_TITLE"

    sget-object v5, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->resources:Landroid/content/res/Resources;

    const v8, 0x7f090202

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 689
    const-string v4, "PICKER_HIDE"

    const/4 v5, 0x1

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 690
    const-string v4, "ROUTE_PICKER"

    const/4 v5, 0x1

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 691
    const-string v4, "PICKER_SHOW_ROUTE_MAP"

    const/4 v5, 0x1

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 692
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v32

    .line 693
    .local v32, "geoCoordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    const-string v4, "PICKER_MAP_START_LAT"

    invoke-virtual/range {v32 .. v32}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v8

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v8, v9}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 694
    const-string v4, "PICKER_MAP_START_LNG"

    invoke-virtual/range {v32 .. v32}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v8

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v8, v9}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 695
    const-string v4, "PICKER_MAP_END_LAT"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->event:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v5, v5, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v5, v5, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v8, v9}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 696
    const-string v4, "PICKER_MAP_END_LNG"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->event:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v5, v5, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v5, v5, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v8, v9}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 698
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->event:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->response:Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

    iget-object v4, v4, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->results:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v35

    .line 699
    .local v35, "len":I
    move/from16 v0, v35

    new-array v0, v0, [Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    move-object/from16 v28, v0

    .line 700
    .local v28, "destinationParcelables":[Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;
    const-wide/16 v16, 0x0

    .line 701
    .local v16, "displayLat":D
    const-wide/16 v18, 0x0

    .line 702
    .local v18, "displayLng":D
    const-wide/16 v12, 0x0

    .line 703
    .local v12, "navLat":D
    const-wide/16 v14, 0x0

    .line 704
    .local v14, "navLng":D
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->event:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v4, v4, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    if-eqz v4, :cond_2

    .line 705
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->event:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v4, v4, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v4, v4, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v12

    .line 706
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->event:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v4, v4, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v4, v4, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v14

    .line 708
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->event:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v4, v4, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationDisplay:Lcom/navdy/service/library/events/location/Coordinate;

    if-eqz v4, :cond_3

    .line 709
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->event:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v4, v4, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationDisplay:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v4, v4, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v16

    .line 710
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->event:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v4, v4, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationDisplay:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v4, v4, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v18

    .line 713
    :cond_3
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    .line 714
    .local v27, "builder":Ljava/lang/StringBuilder;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getInstance()Lcom/navdy/hud/app/maps/here/HereRouteCache;

    move-result-object v33

    .line 715
    .local v33, "hereRouteCache":Lcom/navdy/hud/app/maps/here/HereRouteCache;
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getTimeHelper()Lcom/navdy/hud/app/common/TimeHelper;

    move-result-object v41

    .line 716
    .local v41, "timeHelper":Lcom/navdy/hud/app/common/TimeHelper;
    new-instance v36, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    invoke-direct/range {v36 .. v36}, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;-><init>()V

    .line 718
    .local v36, "maneuverDisplay":Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->event:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v0, v4, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestDestination:Lcom/navdy/service/library/events/destination/Destination;

    move-object/from16 v37, v0

    .line 719
    .local v37, "requestDestination":Lcom/navdy/service/library/events/destination/Destination;
    const/16 v34, 0x0

    .local v34, "i":I
    :goto_1
    move/from16 v0, v34

    move/from16 v1, v35

    if-ge v0, v1, :cond_8

    .line 720
    const-string v40, ""

    .line 721
    .local v40, "time":Ljava/lang/String;
    const-string v29, ""

    .line 722
    .local v29, "etaStr":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->event:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->response:Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

    iget-object v4, v4, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->results:Ljava/util/List;

    move/from16 v0, v34

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v38

    check-cast v38, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    .line 724
    .local v38, "result":Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    move-object/from16 v0, v38

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->length:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-long v4, v4

    const/4 v8, 0x0

    const/4 v9, 0x1

    move-object/from16 v0, v36

    invoke-static {v4, v5, v0, v8, v9}, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->setNavigationDistance(JLcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;ZZ)V

    .line 725
    move-object/from16 v0, v38

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeId:Ljava/lang/String;

    move-object/from16 v0, v33

    invoke-virtual {v0, v4}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getRoute(Ljava/lang/String;)Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;

    move-result-object v39

    .line 726
    .local v39, "routeInfo":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    if-eqz v39, :cond_4

    .line 728
    move-object/from16 v0, v39

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->route:Lcom/here/android/mpa/routing/Route;

    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getRouteTtaDate(Lcom/here/android/mpa/routing/Route;)Ljava/util/Date;

    move-result-object v42

    .line 729
    .local v42, "ttaDate":Ljava/util/Date;
    if-eqz v42, :cond_4

    .line 730
    const/4 v4, 0x0

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2, v4}, Lcom/navdy/hud/app/common/TimeHelper;->formatTime12Hour(Ljava/util/Date;Ljava/lang/StringBuilder;Z)Ljava/lang/String;

    move-result-object v29

    .line 731
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v29

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    .line 734
    invoke-virtual/range {v42 .. v42}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long v30, v4, v8

    .line 735
    .local v30, "duration":J
    const-wide/16 v4, 0x0

    cmp-long v4, v30, v4

    if-gez v4, :cond_6

    .line 736
    const-wide/16 v30, 0x0

    .line 740
    :goto_2
    sget-object v4, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->resources:Landroid/content/res/Resources;

    move-wide/from16 v0, v30

    long-to-int v5, v0

    invoke-static {v4, v5}, Lcom/navdy/hud/app/maps/util/RouteUtils;->formatEtaMinutes(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v40

    .line 744
    .end local v30    # "duration":J
    .end local v42    # "ttaDate":Ljava/util/Date;
    :cond_4
    sget-object v4, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->resources:Landroid/content/res/Resources;

    const v5, 0x7f09024c

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v40, v8, v9

    const/4 v9, 0x1

    move-object/from16 v0, v38

    iget-object v10, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->via:Ljava/lang/String;

    aput-object v10, v8, v9

    invoke-virtual {v4, v5, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 745
    .local v6, "title":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v36

    iget-object v5, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->distanceToPendingRoadText:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v29

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 746
    .local v7, "subTitle":Ljava/lang/String;
    new-instance v4, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;

    const/4 v5, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v0, v38

    iget-object v11, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->address:Ljava/lang/String;

    const v20, 0x7f0201d2

    const v21, 0x7f0201d3

    sget v22, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->notifBkColor:I

    const/16 v23, 0x0

    sget-object v24, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;->DESTINATION:Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;

    if-eqz v37, :cond_7

    move-object/from16 v0, v37

    iget-object v0, v0, Lcom/navdy/service/library/events/destination/Destination;->place_type:Lcom/navdy/service/library/events/places/PlaceType;

    move-object/from16 v25, v0

    :goto_3
    invoke-direct/range {v4 .. v25}, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;-><init>(ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZLjava/lang/String;DDDDIIIILcom/navdy/hud/app/ui/component/destination/DestinationParcelable$DestinationType;Lcom/navdy/service/library/events/places/PlaceType;)V

    aput-object v4, v28, v34

    .line 766
    aget-object v5, v28, v34

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->event:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->response:Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

    iget-object v4, v4, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->results:Ljava/util/List;

    move/from16 v0, v34

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    iget-object v4, v4, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeId:Ljava/lang/String;

    invoke-virtual {v5, v4}, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->setRouteId(Ljava/lang/String;)V

    .line 767
    if-eqz v37, :cond_5

    .line 768
    aget-object v4, v28, v34

    move-object/from16 v0, v37

    iget-object v5, v0, Lcom/navdy/service/library/events/destination/Destination;->identifier:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->setIdentifier(Ljava/lang/String;)V

    .line 769
    aget-object v4, v28, v34

    move-object/from16 v0, v37

    iget-object v5, v0, Lcom/navdy/service/library/events/destination/Destination;->place_id:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/ui/component/destination/DestinationParcelable;->setPlaceId(Ljava/lang/String;)V

    .line 772
    :cond_5
    const/4 v4, 0x0

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 719
    add-int/lit8 v34, v34, 0x1

    goto/16 :goto_1

    .line 738
    .end local v6    # "title":Ljava/lang/String;
    .end local v7    # "subTitle":Ljava/lang/String;
    .restart local v30    # "duration":J
    .restart local v42    # "ttaDate":Ljava/util/Date;
    :cond_6
    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    move-wide/from16 v0, v30

    invoke-virtual {v4, v0, v1}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v30

    goto/16 :goto_2

    .line 746
    .end local v30    # "duration":J
    .end local v42    # "ttaDate":Ljava/util/Date;
    .restart local v6    # "title":Ljava/lang/String;
    .restart local v7    # "subTitle":Ljava/lang/String;
    :cond_7
    const/16 v25, 0x0

    goto :goto_3

    .line 775
    .end local v6    # "title":Ljava/lang/String;
    .end local v7    # "subTitle":Ljava/lang/String;
    .end local v29    # "etaStr":Ljava/lang/String;
    .end local v38    # "result":Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    .end local v39    # "routeInfo":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    .end local v40    # "time":Ljava/lang/String;
    :cond_8
    const-string v4, "PICKER_DESTINATIONS"

    move-object/from16 v0, v26

    move-object/from16 v1, v28

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 776
    new-instance v25, Lcom/navdy/hud/app/maps/notification/RouteCalcPickerHandler;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->event:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    move-object/from16 v0, v25

    invoke-direct {v0, v4}, Lcom/navdy/hud/app/maps/notification/RouteCalcPickerHandler;-><init>(Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;)V

    .line 777
    .local v25, "pickerHandler":Lcom/navdy/hud/app/maps/notification/RouteCalcPickerHandler;
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v20

    const-string v21, "navdy#route#calc#notif"

    const/16 v22, 0x1

    sget-object v23, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DESTINATION_PICKER:Lcom/navdy/service/library/events/ui/Screen;

    move-object/from16 v24, v26

    invoke-virtual/range {v20 .. v25}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->removeNotification(Ljava/lang/String;ZLcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Ljava/lang/Object;)V

    .line 778
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->event:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    goto/16 :goto_0
.end method

.method private startLoadingAnimation()V
    .locals 5

    .prologue
    .line 554
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->loadingAnimator:Landroid/animation/ObjectAnimator;

    if-nez v0, :cond_0

    .line 555
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->iconSpinnerView:Landroid/widget/ImageView;

    sget-object v1, Landroid/view/View;->ROTATION:Landroid/util/Property;

    const/4 v2, 0x1

    new-array v2, v2, [F

    const/4 v3, 0x0

    const/high16 v4, 0x43b40000    # 360.0f

    aput v4, v2, v3

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->loadingAnimator:Landroid/animation/ObjectAnimator;

    .line 556
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->loadingAnimator:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 557
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->loadingAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 558
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->loadingAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$4;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$4;-><init>(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 570
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->loadingAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_1

    .line 571
    sget-object v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "started loading animation"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 572
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->loadingAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 574
    :cond_1
    return-void
.end method

.method private stopLoadingAnimation()V
    .locals 2

    .prologue
    .line 577
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->loadingAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 578
    sget-object v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "cancelled loading animation"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 579
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->loadingAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->removeAllListeners()V

    .line 580
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->loadingAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 581
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->iconSpinnerView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setRotation(F)V

    .line 582
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->loadingAnimator:Landroid/animation/ObjectAnimator;

    .line 584
    :cond_0
    return-void
.end method


# virtual methods
.method public canAddToStackIfCurrentExists()Z
    .locals 1

    .prologue
    .line 419
    const/4 v0, 0x1

    return v0
.end method

.method public expandNotification()Z
    .locals 1

    .prologue
    .line 440
    const/4 v0, 0x0

    return v0
.end method

.method public getColor()I
    .locals 1

    .prologue
    .line 424
    sget v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->notifBkColor:I

    return v0
.end method

.method public getExpandedView(Landroid/content/Context;Ljava/lang/Object;)Landroid/view/View;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 323
    const/4 v0, 0x0

    return-object v0
.end method

.method public getExpandedViewIndicatorColor()I
    .locals 1

    .prologue
    .line 327
    const/4 v0, 0x0

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 303
    const-string v0, "navdy#route#calc#notif"

    return-object v0
.end method

.method public getTimeout()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 371
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->choices:Ljava/util/List;

    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->cancelRouteCalc:Ljava/util/List;

    if-ne v1, v2, :cond_1

    .line 379
    :cond_0
    :goto_0
    return v0

    .line 373
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->choices:Ljava/util/List;

    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->routeError:Ljava/util/List;

    if-ne v1, v2, :cond_2

    .line 374
    sget v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->TIMEOUT:I

    goto :goto_0

    .line 375
    :cond_2
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->choices:Ljava/util/List;

    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->showRoutes:Ljava/util/List;

    if-ne v1, v2, :cond_0

    .line 376
    sget v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->TIMEOUT:I

    goto :goto_0
.end method

.method public getType()Lcom/navdy/hud/app/framework/notifications/NotificationType;
    .locals 1

    .prologue
    .line 298
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;->ROUTE_CALC:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    return-object v0
.end method

.method public getView(Landroid/content/Context;)Landroid/view/View;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 308
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->containerView:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    .line 309
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->containerView:Landroid/view/ViewGroup;

    .line 310
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->containerView:Landroid/view/ViewGroup;

    const v1, 0x7f0e00c3

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->titleView:Landroid/widget/TextView;

    .line 311
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->containerView:Landroid/view/ViewGroup;

    const v1, 0x7f0e0111

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->subTitleView:Landroid/widget/TextView;

    .line 312
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->containerView:Landroid/view/ViewGroup;

    const v1, 0x7f0e015e

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->iconColorImageView:Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    .line 313
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->containerView:Landroid/view/ViewGroup;

    const v1, 0x7f0e0164

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->iconInitialsImageView:Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    .line 314
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->containerView:Landroid/view/ViewGroup;

    const v1, 0x7f0e0162

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->iconSpinnerView:Landroid/widget/ImageView;

    .line 315
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->containerView:Landroid/view/ViewGroup;

    const v1, 0x7f0e00d2

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->choiceLayoutView:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    .line 316
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->containerView:Landroid/view/ViewGroup;

    const v1, 0x7f0e0165

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->routeTtaView:Landroid/widget/TextView;

    .line 319
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->containerView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public getViewSwitchAnimation(Z)Landroid/animation/AnimatorSet;
    .locals 1
    .param p1, "viewIn"    # Z

    .prologue
    .line 437
    const/4 v0, 0x0

    return-object v0
.end method

.method public hideStartTrip()V
    .locals 2

    .prologue
    .line 928
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getRootScreen()Lcom/navdy/hud/app/ui/activity/Main;

    move-result-object v0

    .line 929
    .local v0, "main":Lcom/navdy/hud/app/ui/activity/Main;
    if-eqz v0, :cond_0

    .line 930
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/activity/Main;->ejectMainLowerView()V

    .line 932
    :cond_0
    return-void
.end method

.method public isAlive()Z
    .locals 12

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 384
    const/4 v0, 0x0

    .line 385
    .local v0, "alive":Z
    const/4 v1, 0x0

    .line 386
    .local v1, "reason":Ljava/lang/String;
    iget-object v7, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->choices:Ljava/util/List;

    sget-object v8, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->cancelRouteCalc:Ljava/util/List;

    if-ne v7, v8, :cond_3

    .line 387
    iget-boolean v7, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->dismissedbyUser:Z

    if-eqz v7, :cond_1

    .line 388
    const/4 v0, 0x0

    .line 392
    :goto_0
    const-string v1, "cancelRouteCalc"

    .line 408
    :cond_0
    :goto_1
    sget-object v5, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "alive="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " type="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 409
    return v0

    .line 390
    :cond_1
    iget-object v7, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->routeCalculationEventHandler:Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;

    invoke-virtual {v7}, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->getCurrentRouteCalcEvent()Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    move-result-object v7

    if-eqz v7, :cond_2

    move v0, v5

    :goto_2
    goto :goto_0

    :cond_2
    move v0, v6

    goto :goto_2

    .line 393
    :cond_3
    iget-object v7, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->choices:Ljava/util/List;

    sget-object v8, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->routeError:Ljava/util/List;

    if-ne v7, v8, :cond_4

    .line 394
    const/4 v0, 0x0

    .line 395
    const-string v1, "routeError"

    goto :goto_1

    .line 396
    :cond_4
    iget-object v7, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->choices:Ljava/util/List;

    sget-object v8, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->showRoutes:Ljava/util/List;

    if-ne v7, v8, :cond_0

    .line 397
    iget-boolean v7, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->dismissedbyUser:Z

    if-eqz v7, :cond_5

    .line 398
    const/4 v0, 0x0

    .line 405
    :goto_3
    const-string v1, "showRoutes"

    goto :goto_1

    .line 400
    :cond_5
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    iget-wide v10, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->startTime:J

    sub-long v2, v8, v10

    .line 401
    .local v2, "diff":J
    sget v7, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->TIMEOUT:I

    int-to-long v8, v7

    cmp-long v7, v2, v8

    if-ltz v7, :cond_6

    move v4, v5

    .line 402
    .local v4, "timedOut":Z
    :goto_4
    iget-object v7, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->routeCalculationEventHandler:Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;

    invoke-virtual {v7}, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->getCurrentRouteCalcEvent()Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    move-result-object v7

    if-eqz v7, :cond_7

    if-nez v4, :cond_7

    move v0, v5

    .line 403
    :goto_5
    sget-object v5, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "timedOut="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_3

    .end local v4    # "timedOut":Z
    :cond_6
    move v4, v6

    .line 401
    goto :goto_4

    .restart local v4    # "timedOut":Z
    :cond_7
    move v0, v6

    .line 402
    goto :goto_5
.end method

.method public isPurgeable()Z
    .locals 1

    .prologue
    .line 414
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->isAlive()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isStarting()Z
    .locals 2

    .prologue
    .line 935
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->choices:Ljava/util/List;

    sget-object v1, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->cancelRouteCalc:Ljava/util/List;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public nextHandler()Lcom/navdy/hud/app/manager/InputManager$IInputHandler;
    .locals 1

    .prologue
    .line 496
    const/4 v0, 0x0

    return-object v0
.end method

.method public onClick()V
    .locals 0

    .prologue
    .line 448
    return-void
.end method

.method public onConnectionStateChange(Lcom/navdy/service/library/events/connection/ConnectionStateChange;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/service/library/events/connection/ConnectionStateChange;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 822
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-nez v0, :cond_1

    .line 835
    :cond_0
    :goto_0
    return-void

    .line 826
    :cond_1
    sget-object v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$5;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

    iget-object v1, p1, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->state:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 828
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->lookupRequest:Lcom/navdy/service/library/events/places/DestinationSelectedRequest;

    if-eqz v0, :cond_0

    .line 829
    sget-object v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "nav lookup device disconnected"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 830
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->lookupRequest:Lcom/navdy/service/library/events/places/DestinationSelectedRequest;

    .line 831
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->launchOriginalDestination()V

    goto :goto_0

    .line 826
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onDestinationLookup(Lcom/navdy/service/library/events/places/DestinationSelectedResponse;)V
    .locals 4
    .param p1, "response"    # Lcom/navdy/service/library/events/places/DestinationSelectedResponse;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 791
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-nez v1, :cond_0

    .line 818
    :goto_0
    return-void

    .line 794
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->lookupRequest:Lcom/navdy/service/library/events/places/DestinationSelectedRequest;

    if-nez v1, :cond_1

    .line 795
    sget-object v1, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "receive destination lookup:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;->request_id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " , not in lookup mode, ignore"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 799
    :cond_1
    iget-object v1, p1, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;->request_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->lookupRequest:Lcom/navdy/service/library/events/places/DestinationSelectedRequest;

    iget-object v2, v2, Lcom/navdy/service/library/events/places/DestinationSelectedRequest;->request_id:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 801
    sget-object v1, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "receive destination lookup, mismatch id recv["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;->request_id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] lookup["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->lookupRequest:Lcom/navdy/service/library/events/places/DestinationSelectedRequest;

    iget-object v3, v3, Lcom/navdy/service/library/events/places/DestinationSelectedRequest;->request_id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 806
    :cond_2
    iget-object v1, p1, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;->request_status:Lcom/navdy/service/library/events/RequestStatus;

    if-eqz v1, :cond_3

    iget-object v1, p1, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;->request_status:Lcom/navdy/service/library/events/RequestStatus;

    sget-object v2, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    if-ne v1, v2, :cond_3

    .line 808
    sget-object v1, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "nav lookup suceeded:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;->destination:Lcom/navdy/service/library/events/destination/Destination;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 809
    invoke-static {}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->getInstance()Lcom/navdy/hud/app/framework/destinations/DestinationsManager;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;->destination:Lcom/navdy/service/library/events/destination/Destination;

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->transformToInternalDestination(Lcom/navdy/service/library/events/destination/Destination;)Lcom/navdy/hud/app/framework/destinations/Destination;

    move-result-object v0

    .line 810
    .local v0, "d":Lcom/navdy/hud/app/framework/destinations/Destination;
    invoke-static {}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->getInstance()Lcom/navdy/hud/app/framework/destinations/DestinationsManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/framework/destinations/DestinationsManager;->requestNavigation(Lcom/navdy/hud/app/framework/destinations/Destination;)V

    .line 817
    .end local v0    # "d":Lcom/navdy/hud/app/framework/destinations/Destination;
    :goto_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->lookupRequest:Lcom/navdy/service/library/events/places/DestinationSelectedRequest;

    goto/16 :goto_0

    .line 813
    :cond_3
    sget-object v1, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "nav lookup failed:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;->request_status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 814
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->launchOriginalDestination()V

    goto :goto_1
.end method

.method public onExpandedNotificationEvent(Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V
    .locals 0
    .param p1, "mode"    # Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    .prologue
    .line 431
    return-void
.end method

.method public onExpandedNotificationSwitched()V
    .locals 0

    .prologue
    .line 434
    return-void
.end method

.method public onGesture(Lcom/navdy/service/library/events/input/GestureEvent;)Z
    .locals 4
    .param p1, "event"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 455
    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-nez v2, :cond_1

    .line 469
    :cond_0
    :goto_0
    return v0

    .line 459
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->choices:Ljava/util/List;

    sget-object v3, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->showRoutes:Ljava/util/List;

    if-ne v2, v3, :cond_0

    .line 462
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$5;->$SwitchMap$com$navdy$service$library$events$input$Gesture:[I

    iget-object v3, p1, Lcom/navdy/service/library/events/input/GestureEvent;->gesture:Lcom/navdy/service/library/events/input/Gesture;

    invoke-virtual {v3}, Lcom/navdy/service/library/events/input/Gesture;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 464
    :pswitch_0
    sget-object v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "show route picker:gesture"

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 465
    iput-boolean v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->dismissedbyUser:Z

    .line 466
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->showRoutePicker()V

    move v0, v1

    .line 467
    goto :goto_0

    .line 462
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onKey(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
    .locals 4
    .param p1, "event"    # Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 474
    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-nez v2, :cond_0

    .line 492
    :goto_0
    return v0

    .line 477
    :cond_0
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$5;->$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I

    invoke-virtual {p1}, Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 479
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->resetTimeout()V

    .line 480
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->choiceLayoutView:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->moveSelectionLeft()Z

    move v0, v1

    .line 481
    goto :goto_0

    .line 484
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->resetTimeout()V

    .line 485
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->choiceLayoutView:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->moveSelectionRight()Z

    move v0, v1

    .line 486
    goto :goto_0

    .line 489
    :pswitch_2
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->choiceLayoutView:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->executeSelectedItem()V

    move v0, v1

    .line 490
    goto :goto_0

    .line 477
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onNotificationEvent(Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V
    .locals 0
    .param p1, "mode"    # Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    .prologue
    .line 428
    return-void
.end method

.method public onStart(Lcom/navdy/hud/app/framework/notifications/INotificationController;)V
    .locals 2
    .param p1, "controller"    # Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .prologue
    .line 332
    sget-object v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onStart"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 333
    iput-object p1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .line 334
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->timeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 335
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->registered:Z

    if-nez v0, :cond_0

    .line 336
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 337
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->registered:Z

    .line 339
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->setUI()V

    .line 340
    return-void
.end method

.method public onStop()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 347
    sget-object v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onStop"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 348
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->hideStartTrip()V

    .line 349
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->stopLoadingAnimation()V

    .line 350
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->waitForNavLookupRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 351
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .line 352
    iput-boolean v4, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->ttsPlayed:Z

    .line 353
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->choices:Ljava/util/List;

    sget-object v1, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->cancelRouteCalc:Ljava/util/List;

    if-ne v0, v1, :cond_0

    .line 354
    sget-object v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "cancel tts"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 355
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->bus:Lcom/squareup/otto/Bus;

    sget-object v1, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->CANCEL_CALC_TTS:Lcom/navdy/hud/app/event/RemoteEvent;

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 357
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->choices:Ljava/util/List;

    sget-object v1, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->showRoutes:Ljava/util/List;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->choices:Ljava/util/List;

    sget-object v1, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->routeError:Ljava/util/List;

    if-ne v0, v1, :cond_2

    :cond_1
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 358
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->timeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 359
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->timeoutRunnable:Ljava/lang/Runnable;

    sget v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->TIMEOUT:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 360
    sget-object v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "expire notif in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->TIMEOUT:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 363
    :cond_2
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->registered:Z

    if-eqz v0, :cond_3

    .line 364
    iput-boolean v4, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->registered:Z

    .line 365
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->unregister(Ljava/lang/Object;)V

    .line 367
    :cond_3
    return-void
.end method

.method public onTrackHand(F)V
    .locals 0
    .param p1, "normalized"    # F

    .prologue
    .line 451
    return-void
.end method

.method public onUpdate()V
    .locals 0

    .prologue
    .line 343
    return-void
.end method

.method public showError()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 633
    sget-object v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->routeFailed:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->title:Ljava/lang/String;

    .line 634
    sget-object v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->routeError:Ljava/util/List;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->choices:Ljava/util/List;

    .line 635
    const v0, 0x7f0200d9

    iput v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->iconSide:I

    .line 636
    iput-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->event:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    .line 637
    iput-object v1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->tts:Ljava/lang/String;

    .line 639
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-eqz v0, :cond_0

    .line 640
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->setUI()V

    .line 642
    :cond_0
    return-void
.end method

.method public showRouteSearch(Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/destination/Destination;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "subTitle"    # Ljava/lang/String;
    .param p3, "icon"    # I
    .param p4, "iconBkColor"    # I
    .param p5, "notifColor"    # I
    .param p6, "initials"    # Ljava/lang/String;
    .param p7, "tts"    # Ljava/lang/String;
    .param p8, "lookupDestination"    # Lcom/navdy/service/library/events/destination/Destination;
    .param p9, "routeId"    # Ljava/lang/String;
    .param p10, "destinationLabel"    # Ljava/lang/String;
    .param p11, "destinationAddress"    # Ljava/lang/String;
    .param p12, "destinationDistance"    # Ljava/lang/String;

    .prologue
    .line 598
    sget-object v3, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "showRouteSearch: navlookup:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz p8, :cond_3

    const/4 v2, 0x1

    :goto_0
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 599
    const/4 v1, 0x0

    .line 600
    .local v1, "navLookupTransition":Z
    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->lookupDestination:Lcom/navdy/service/library/events/destination/Destination;

    if-eqz v2, :cond_0

    .line 602
    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->title:Ljava/lang/String;

    invoke-static {v2, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 603
    const/4 v1, 0x1

    .line 604
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "showRouteSearch: nav lookup transition"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 607
    :cond_0
    iput-object p1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->title:Ljava/lang/String;

    .line 608
    iput-object p2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->subTitle:Ljava/lang/String;

    .line 609
    iput-object p9, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->routeId:Ljava/lang/String;

    .line 610
    if-nez v1, :cond_1

    .line 611
    iput p3, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->icon:I

    .line 612
    iput p4, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->iconBkColor:I

    .line 613
    iput p5, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->notifColor:I

    .line 614
    iput-object p6, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->initials:Ljava/lang/String;

    .line 617
    :cond_1
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->cancelRouteCalc:Ljava/util/List;

    iput-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->choices:Ljava/util/List;

    .line 618
    const v2, 0x7f02022e

    iput v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->iconSide:I

    .line 619
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->event:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    .line 620
    iput-object p7, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->tts:Ljava/lang/String;

    .line 621
    iput-object p8, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->lookupDestination:Lcom/navdy/service/library/events/destination/Destination;

    .line 623
    iput-object p10, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->destinationLabel:Ljava/lang/String;

    .line 624
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->destinationAddress:Ljava/lang/String;

    .line 625
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->destinationDistance:Ljava/lang/String;

    .line 627
    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-eqz v2, :cond_2

    .line 628
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->setUI()V

    .line 630
    :cond_2
    return-void

    .line 598
    .end local v1    # "navLookupTransition":Z
    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public showRoutes(Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;)V
    .locals 8
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 645
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$5;->$SwitchMap$com$here$android$mpa$routing$RouteOptions$Type:[I

    iget-object v3, p1, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->routeOptions:Lcom/here/android/mpa/routing/RouteOptions;

    invoke-virtual {v3}, Lcom/here/android/mpa/routing/RouteOptions;->getRouteType()Lcom/here/android/mpa/routing/RouteOptions$Type;

    move-result-object v3

    invoke-virtual {v3}, Lcom/here/android/mpa/routing/RouteOptions$Type;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 655
    :goto_0
    sget-object v3, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->resources:Landroid/content/res/Resources;

    const v4, 0x7f0902e9

    const/4 v2, 0x1

    new-array v5, v2, [Ljava/lang/Object;

    iget-object v2, p1, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->response:Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

    iget-object v2, v2, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->results:Ljava/util/List;

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    iget-object v2, v2, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->via:Ljava/lang/String;

    aput-object v2, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->subTitle:Ljava/lang/String;

    .line 656
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->showRoutes:Ljava/util/List;

    iput-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->choices:Ljava/util/List;

    .line 657
    iput v6, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->icon:I

    .line 658
    sget v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->showRouteBkColor:I

    iput v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->iconBkColor:I

    .line 659
    const v2, 0x7f0201d2

    iput v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->iconSide:I

    .line 660
    iput-object p1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->event:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    .line 661
    iput-object v7, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->tripDuration:Landroid/text/SpannableStringBuilder;

    .line 662
    iput-object v7, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->tts:Ljava/lang/String;

    .line 665
    iget-object v2, p1, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->response:Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

    iget-object v2, v2, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->results:Ljava/util/List;

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    .line 666
    .local v0, "result":Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getInstance()Lcom/navdy/hud/app/maps/here/HereRouteCache;

    move-result-object v2

    iget-object v3, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getRoute(Ljava/lang/String;)Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;

    move-result-object v1

    .line 667
    .local v1, "routeInfo":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    if-eqz v1, :cond_1

    .line 668
    iget-object v2, v1, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->route:Lcom/here/android/mpa/routing/Route;

    iget-object v3, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeId:Ljava/lang/String;

    invoke-static {v2, v3, v6}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getEtaString(Lcom/here/android/mpa/routing/Route;Ljava/lang/String;Z)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->tripDuration:Landroid/text/SpannableStringBuilder;

    .line 673
    :goto_1
    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-eqz v2, :cond_0

    .line 674
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->setUI()V

    .line 676
    :cond_0
    return-void

    .line 647
    .end local v0    # "result":Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    .end local v1    # "routeInfo":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    :pswitch_0
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->fastestRoute:Ljava/lang/String;

    iput-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->title:Ljava/lang/String;

    goto :goto_0

    .line 651
    :pswitch_1
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->shortestRoute:Ljava/lang/String;

    iput-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->title:Ljava/lang/String;

    goto :goto_0

    .line 670
    .restart local v0    # "result":Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    .restart local v1    # "routeInfo":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    :cond_1
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v2, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->tripDuration:Landroid/text/SpannableStringBuilder;

    goto :goto_1

    .line 645
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public showStartTrip()V
    .locals 26

    .prologue
    .line 849
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-nez v2, :cond_1

    .line 850
    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "showStartTrip:null"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 925
    :cond_0
    :goto_0
    return-void

    .line 853
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v24

    .line 854
    .local v24, "uiStateManager":Lcom/navdy/hud/app/ui/framework/UIStateManager;
    invoke-virtual/range {v24 .. v24}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getRootScreen()Lcom/navdy/hud/app/ui/activity/Main;

    move-result-object v19

    .line 855
    .local v19, "main":Lcom/navdy/hud/app/ui/activity/Main;
    if-eqz v19, :cond_0

    .line 856
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v2}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->getUIContext()Landroid/content/Context;

    move-result-object v11

    .line 857
    .local v11, "context":Landroid/content/Context;
    invoke-static {v11}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f030087

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/view/ViewGroup;

    .line 858
    .local v17, "lyt":Landroid/view/ViewGroup;
    const v2, 0x7f0e00c7

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Lcom/navdy/hud/app/ui/component/HaloView;

    .line 859
    .local v13, "haloView":Lcom/navdy/hud/app/ui/component/HaloView;
    const/16 v2, 0x8

    invoke-virtual {v13, v2}, Lcom/navdy/hud/app/ui/component/HaloView;->setVisibility(I)V

    .line 861
    const v2, 0x7f0e00c6

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v25

    check-cast v25, Landroid/view/ViewGroup;

    .line 862
    .local v25, "viewGroup":Landroid/view/ViewGroup;
    invoke-virtual/range {v25 .. v25}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v18

    check-cast v18, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 863
    .local v18, "lytParams":Landroid/view/ViewGroup$MarginLayoutParams;
    sget v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->startTripImageContainerSize:I

    move-object/from16 v0, v18

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 864
    sget v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->startTripImageContainerSize:I

    move-object/from16 v0, v18

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 865
    sget v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->startTripLeftMargin:I

    move-object/from16 v0, v18

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 867
    const v2, 0x7f0e00c8

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/view/ViewGroup;

    .line 868
    .local v14, "iconContainer":Landroid/view/ViewGroup;
    invoke-virtual {v14}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v18

    .end local v18    # "lytParams":Landroid/view/ViewGroup$MarginLayoutParams;
    check-cast v18, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 869
    .restart local v18    # "lytParams":Landroid/view/ViewGroup$MarginLayoutParams;
    sget v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->startTripImageSize:I

    move-object/from16 v0, v18

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 870
    sget v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->startTripImageSize:I

    move-object/from16 v0, v18

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 872
    const v2, 0x7f0e0228

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v25

    .end local v25    # "viewGroup":Landroid/view/ViewGroup;
    check-cast v25, Landroid/view/ViewGroup;

    .line 873
    .restart local v25    # "viewGroup":Landroid/view/ViewGroup;
    invoke-virtual/range {v25 .. v25}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v18

    .end local v18    # "lytParams":Landroid/view/ViewGroup$MarginLayoutParams;
    check-cast v18, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 874
    .restart local v18    # "lytParams":Landroid/view/ViewGroup$MarginLayoutParams;
    sget v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->startTripTextLeftMargin:I

    move-object/from16 v0, v18

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 877
    const v2, 0x7f0e00c3

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v23

    check-cast v23, Landroid/widget/TextView;

    .line 878
    .local v23, "startTripTitle":Landroid/widget/TextView;
    const v2, 0x7f0e0111

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/TextView;

    .line 879
    .local v21, "startTripSubTitle":Landroid/widget/TextView;
    const v2, 0x7f0e0229

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/TextView;

    .line 880
    .local v22, "startTripSubTitle2":Landroid/widget/TextView;
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->destinationLabel:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->destinationAddress:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->destinationDistance:Ljava/lang/String;

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/navdy/hud/app/ui/component/vlist/viewholder/IconBkColorViewHolder;->buildModel(IIIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/hud/app/ui/component/image/IconColorImageView$IconShape;)Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;

    move-result-object v20

    .line 881
    .local v20, "model":Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;
    const/4 v2, 0x0

    move-object/from16 v0, v20

    invoke-static {v0, v2}, Lcom/navdy/hud/app/ui/component/vlist/VerticalList;->setFontSize(Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;Z)V

    .line 883
    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v16

    check-cast v16, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 884
    .local v16, "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, v20

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    iget v2, v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontTopMargin:F

    float-to-int v2, v2

    move-object/from16 v0, v16

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 885
    invoke-virtual/range {v21 .. v21}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v16

    .end local v16    # "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    check-cast v16, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 886
    .restart local v16    # "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, v20

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    iget v2, v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontTopMargin:F

    float-to-int v2, v2

    move-object/from16 v0, v16

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 887
    invoke-virtual/range {v22 .. v22}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v16

    .end local v16    # "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    check-cast v16, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 888
    .restart local v16    # "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, v20

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    iget v2, v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitle2FontTopMargin:F

    float-to-int v2, v2

    move-object/from16 v0, v16

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 890
    move-object/from16 v0, v20

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    iget v2, v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->titleFontSize:F

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 891
    const/4 v2, 0x1

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 892
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->destinationLabel:Ljava/lang/String;

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 894
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->destinationAddress:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 895
    move-object/from16 v0, v20

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    iget v2, v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitleFontSize:F

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 896
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->destinationAddress:Ljava/lang/String;

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 901
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->destinationDistance:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 902
    move-object/from16 v0, v20

    iget-object v2, v0, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;->fontInfo:Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;

    iget v2, v2, Lcom/navdy/hud/app/ui/component/vlist/VerticalList$FontInfo;->subTitle2FontSize:F

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 903
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->destinationDistance:Ljava/lang/String;

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 904
    const/4 v2, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 909
    :goto_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->iconBkColor:I

    if-eqz v2, :cond_4

    .line 910
    new-instance v15, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;

    invoke-direct {v15, v11}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;-><init>(Landroid/content/Context;)V

    .line 911
    .local v15, "imageView":Lcom/navdy/hud/app/ui/component/image/IconColorImageView;
    invoke-virtual {v14, v15}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 912
    const/16 v2, 0x99

    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->iconBkColor:I

    invoke-static {v3}, Landroid/graphics/Color;->red(I)I

    move-result v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->iconBkColor:I

    invoke-static {v4}, Landroid/graphics/Color;->green(I)I

    move-result v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->iconBkColor:I

    invoke-static {v5}, Landroid/graphics/Color;->blue(I)I

    move-result v5

    invoke-static {v2, v3, v4, v5}, Landroid/graphics/Color;->argb(IIII)I

    move-result v12

    .line 913
    .local v12, "fluctuatorColor":I
    invoke-virtual {v13, v12}, Lcom/navdy/hud/app/ui/component/HaloView;->setStrokeColor(I)V

    .line 914
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->icon:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->iconBkColor:I

    const/4 v4, 0x0

    const v5, 0x3f547ae1    # 0.83f

    invoke-virtual {v15, v2, v3, v4, v5}, Lcom/navdy/hud/app/ui/component/image/IconColorImageView;->setIcon(IILandroid/graphics/Shader;F)V

    .line 923
    .end local v15    # "imageView":Lcom/navdy/hud/app/ui/component/image/IconColorImageView;
    :goto_3
    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/activity/Main;->injectMainLowerView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 898
    .end local v12    # "fluctuatorColor":I
    :cond_2
    const/16 v2, 0x8

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 906
    :cond_3
    const/16 v2, 0x8

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 916
    :cond_4
    new-instance v15, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;

    invoke-direct {v15, v11}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;-><init>(Landroid/content/Context;)V

    .line 917
    .local v15, "imageView":Lcom/navdy/hud/app/ui/component/image/InitialsImageView;
    invoke-virtual {v14, v15}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 918
    const/16 v2, 0x99

    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->notifColor:I

    invoke-static {v3}, Landroid/graphics/Color;->red(I)I

    move-result v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->notifColor:I

    invoke-static {v4}, Landroid/graphics/Color;->green(I)I

    move-result v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->notifColor:I

    invoke-static {v5}, Landroid/graphics/Color;->blue(I)I

    move-result v5

    invoke-static {v2, v3, v4, v5}, Landroid/graphics/Color;->argb(IIII)I

    move-result v12

    .line 919
    .restart local v12    # "fluctuatorColor":I
    invoke-virtual {v13, v12}, Lcom/navdy/hud/app/ui/component/HaloView;->setStrokeColor(I)V

    .line 920
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->icon:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->initials:Ljava/lang/String;

    sget-object v4, Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;->MEDIUM:Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;

    invoke-virtual {v15, v2, v3, v4}, Lcom/navdy/hud/app/ui/component/image/InitialsImageView;->setImage(ILjava/lang/String;Lcom/navdy/hud/app/ui/component/image/InitialsImageView$Style;)V

    goto :goto_3
.end method

.method public supportScroll()Z
    .locals 1

    .prologue
    .line 444
    const/4 v0, 0x0

    return v0
.end method
