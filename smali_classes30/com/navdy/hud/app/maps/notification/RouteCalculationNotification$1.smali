.class Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$1;
.super Ljava/lang/Object;
.source "RouteCalculationNotification.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    .prologue
    .line 200
    iput-object p1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$1;->this$0:Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 203
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$1;->this$0:Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    # getter for: Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->access$000(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;)Lcom/navdy/hud/app/framework/notifications/INotificationController;

    move-result-object v0

    if-nez v0, :cond_1

    .line 211
    :cond_0
    :goto_0
    return-void

    .line 206
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$1;->this$0:Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    # getter for: Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->lookupRequest:Lcom/navdy/service/library/events/places/DestinationSelectedRequest;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->access$100(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;)Lcom/navdy/service/library/events/places/DestinationSelectedRequest;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 207
    # getter for: Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "nav lookup expired"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 208
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$1;->this$0:Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    const/4 v1, 0x0

    # setter for: Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->lookupRequest:Lcom/navdy/service/library/events/places/DestinationSelectedRequest;
    invoke-static {v0, v1}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->access$102(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;Lcom/navdy/service/library/events/places/DestinationSelectedRequest;)Lcom/navdy/service/library/events/places/DestinationSelectedRequest;

    .line 209
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification$1;->this$0:Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    # invokes: Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->launchOriginalDestination()V
    invoke-static {v0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->access$300(Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;)V

    goto :goto_0
.end method
