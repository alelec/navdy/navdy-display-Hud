.class public Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;
.super Lcom/navdy/hud/app/maps/notification/BaseTrafficNotification;
.source "TrafficJamNotification.java"


# static fields
.field public static final REGULAR_SIZE:F = 64.0f

.field public static final SMALL_SIZE:F = 46.0f

.field private static final TAG_DISMISS:I = 0x1

.field private static delay:Ljava/lang/String;

.field private static dismiss:Ljava/lang/String;

.field private static dismissChoices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;",
            ">;"
        }
    .end annotation
.end field

.field private static hr:Ljava/lang/String;

.field private static min:Ljava/lang/String;

.field private static trafficJam:Ljava/lang/String;


# instance fields
.field private bus:Lcom/squareup/otto/Bus;

.field private choiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

.field private container:Landroid/view/ViewGroup;

.field private gauge:Lcom/navdy/hud/app/view/Gauge;

.field private initialRemainingTime:I

.field private notifColor:I

.field private title:Landroid/widget/TextView;

.field private title1:Landroid/widget/TextView;

.field private title2:Landroid/widget/TextView;

.field private title3:Landroid/widget/TextView;

.field private trafficJamEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficJamProgressEvent;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 43
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->dismissChoices:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/maps/MapEvents$TrafficJamProgressEvent;)V
    .locals 10
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .param p2, "initialEvent"    # Lcom/navdy/hud/app/maps/MapEvents$TrafficJamProgressEvent;

    .prologue
    const v2, 0x7f020125

    .line 75
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/BaseTrafficNotification;-><init>()V

    .line 60
    new-instance v0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification$1;-><init>(Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->choiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

    .line 76
    sget-object v0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->trafficJam:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 77
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 78
    .local v8, "resources":Landroid/content/res/Resources;
    const v0, 0x7f090298

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->trafficJam:Ljava/lang/String;

    .line 79
    const v0, 0x7f09029b

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->delay:Ljava/lang/String;

    .line 80
    const v0, 0x7f0901ab

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->min:Ljava/lang/String;

    .line 81
    const v0, 0x7f09017e

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->hr:Ljava/lang/String;

    .line 82
    const v0, 0x7f09029d

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->dismiss:Ljava/lang/String;

    .line 84
    const v0, 0x7f0d0021

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 85
    .local v3, "dismissColor":I
    const/high16 v5, -0x1000000

    .line 86
    .local v5, "unselectedColor":I
    sget-object v9, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->dismissChoices:Ljava/util/List;

    new-instance v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;

    const/4 v1, 0x1

    sget-object v6, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->dismiss:Ljava/lang/String;

    move v4, v2

    move v7, v3

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Choice;-><init>(IIIIILjava/lang/String;I)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 87
    const v0, 0x7f0d00b6

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->notifColor:I

    .line 90
    .end local v3    # "dismissColor":I
    .end local v5    # "unselectedColor":I
    .end local v8    # "resources":Landroid/content/res/Resources;
    :cond_0
    iput-object p1, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->bus:Lcom/squareup/otto/Bus;

    .line 91
    iget v0, p2, Lcom/navdy/hud/app/maps/MapEvents$TrafficJamProgressEvent;->remainingTime:I

    iput v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->initialRemainingTime:I

    .line 93
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TrafficJamNotification: initialRemainingTime="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Lcom/navdy/hud/app/maps/MapEvents$TrafficJamProgressEvent;->remainingTime:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 94
    return-void
.end method

.method static synthetic access$002(Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;Lcom/navdy/hud/app/maps/MapEvents$TrafficJamProgressEvent;)Lcom/navdy/hud/app/maps/MapEvents$TrafficJamProgressEvent;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;
    .param p1, "x1"    # Lcom/navdy/hud/app/maps/MapEvents$TrafficJamProgressEvent;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->trafficJamEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficJamProgressEvent;

    return-object p1
.end method

.method private updateState()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    .line 195
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->trafficJamEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficJamProgressEvent;

    if-eqz v1, :cond_0

    .line 196
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->title:Landroid/widget/TextView;

    sget-object v2, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->trafficJam:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 197
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->title1:Landroid/widget/TextView;

    sget-object v2, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->delay:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 200
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->trafficJamEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficJamProgressEvent;

    iget v1, v1, Lcom/navdy/hud/app/maps/MapEvents$TrafficJamProgressEvent;->remainingTime:I

    const/16 v2, 0xe10

    if-lt v1, v2, :cond_1

    .line 201
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->title2:Landroid/widget/TextView;

    const/high16 v2, 0x42380000    # 46.0f

    invoke-virtual {v1, v3, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 202
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->title3:Landroid/widget/TextView;

    sget-object v2, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->hr:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 208
    :goto_0
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->trafficJamEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficJamProgressEvent;

    iget v1, v1, Lcom/navdy/hud/app/maps/MapEvents$TrafficJamProgressEvent;->remainingTime:I

    invoke-static {v1}, Lcom/navdy/hud/app/maps/util/MapUtils;->formatTime(I)Ljava/lang/String;

    move-result-object v0

    .line 209
    .local v0, "timeFormatted":Ljava/lang/String;
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->title2:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 211
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->gauge:Lcom/navdy/hud/app/view/Gauge;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->trafficJamEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficJamProgressEvent;

    iget v2, v2, Lcom/navdy/hud/app/maps/MapEvents$TrafficJamProgressEvent;->remainingTime:I

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/view/Gauge;->setValue(I)V

    .line 212
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TrafficJamNotification: trafficJamEvent.remainingTime="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->trafficJamEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficJamProgressEvent;

    iget v3, v3, Lcom/navdy/hud/app/maps/MapEvents$TrafficJamProgressEvent;->remainingTime:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 214
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    sget-object v2, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->dismissChoices:Ljava/util/List;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->choiceListener:Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;

    const/high16 v5, 0x3f000000    # 0.5f

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;->setChoices(Ljava/util/List;ILcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;F)V

    .line 216
    .end local v0    # "timeFormatted":Ljava/lang/String;
    :cond_0
    return-void

    .line 204
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->title2:Landroid/widget/TextView;

    const/high16 v2, 0x42800000    # 64.0f

    invoke-virtual {v1, v3, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 205
    iget-object v1, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->title3:Landroid/widget/TextView;

    sget-object v2, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->min:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public canAddToStackIfCurrentExists()Z
    .locals 1

    .prologue
    .line 166
    const/4 v0, 0x1

    return v0
.end method

.method public expandNotification()Z
    .locals 1

    .prologue
    .line 191
    const/4 v0, 0x0

    return v0
.end method

.method public getColor()I
    .locals 1

    .prologue
    .line 171
    iget v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->notifColor:I

    return v0
.end method

.method public getExpandedView(Landroid/content/Context;Ljava/lang/Object;)Landroid/view/View;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 125
    const/4 v0, 0x0

    return-object v0
.end method

.method public getExpandedViewIndicatorColor()I
    .locals 1

    .prologue
    .line 130
    const/4 v0, 0x0

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    const-string v0, "navdy#traffic#jam#notif"

    return-object v0
.end method

.method public getTimeout()I
    .locals 1

    .prologue
    .line 151
    const/4 v0, 0x0

    return v0
.end method

.method public getType()Lcom/navdy/hud/app/framework/notifications/NotificationType;
    .locals 1

    .prologue
    .line 98
    sget-object v0, Lcom/navdy/hud/app/framework/notifications/NotificationType;->TRAFFIC_JAM:Lcom/navdy/hud/app/framework/notifications/NotificationType;

    return-object v0
.end method

.method public getView(Landroid/content/Context;)Landroid/view/View;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->container:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    .line 109
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030042

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->container:Landroid/view/ViewGroup;

    .line 111
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->container:Landroid/view/ViewGroup;

    const v1, 0x7f0e00c3

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->title:Landroid/widget/TextView;

    .line 112
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->container:Landroid/view/ViewGroup;

    const v1, 0x7f0e0088

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->title1:Landroid/widget/TextView;

    .line 113
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->container:Landroid/view/ViewGroup;

    const v1, 0x7f0e0089

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->title2:Landroid/widget/TextView;

    .line 114
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->container:Landroid/view/ViewGroup;

    const v1, 0x7f0e008a

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->title3:Landroid/widget/TextView;

    .line 116
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->container:Landroid/view/ViewGroup;

    const v1, 0x7f0e0167

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/view/Gauge;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->gauge:Lcom/navdy/hud/app/view/Gauge;

    .line 117
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->gauge:Lcom/navdy/hud/app/view/Gauge;

    iget v1, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->initialRemainingTime:I

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/view/Gauge;->setMaxValue(I)V

    .line 118
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->container:Landroid/view/ViewGroup;

    const v1, 0x7f0e00d2

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->choiceLayout:Lcom/navdy/hud/app/ui/component/ChoiceLayout2;

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->container:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public getViewSwitchAnimation(Z)Landroid/animation/AnimatorSet;
    .locals 1
    .param p1, "viewIn"    # Z

    .prologue
    .line 186
    const/4 v0, 0x0

    return-object v0
.end method

.method public isAlive()Z
    .locals 1

    .prologue
    .line 156
    const/4 v0, 0x1

    return v0
.end method

.method public isPurgeable()Z
    .locals 1

    .prologue
    .line 161
    const/4 v0, 0x0

    return v0
.end method

.method public onExpandedNotificationEvent(Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V
    .locals 0
    .param p1, "mode"    # Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    .prologue
    .line 179
    return-void
.end method

.method public onExpandedNotificationSwitched()V
    .locals 0

    .prologue
    .line 182
    return-void
.end method

.method public onNotificationEvent(Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V
    .locals 0
    .param p1, "mode"    # Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    .prologue
    .line 175
    return-void
.end method

.method public onStart(Lcom/navdy/hud/app/framework/notifications/INotificationController;)V
    .locals 0
    .param p1, "controller"    # Lcom/navdy/hud/app/framework/notifications/INotificationController;

    .prologue
    .line 135
    invoke-super {p0, p1}, Lcom/navdy/hud/app/maps/notification/BaseTrafficNotification;->onStart(Lcom/navdy/hud/app/framework/notifications/INotificationController;)V

    .line 136
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->updateState()V

    .line 137
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 146
    invoke-super {p0}, Lcom/navdy/hud/app/maps/notification/BaseTrafficNotification;->onStop()V

    .line 147
    return-void
.end method

.method public onUpdate()V
    .locals 0

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->updateState()V

    .line 142
    return-void
.end method

.method public setTrafficEvent(Lcom/navdy/hud/app/maps/MapEvents$TrafficJamProgressEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$TrafficJamProgressEvent;

    .prologue
    .line 219
    iput-object p1, p0, Lcom/navdy/hud/app/maps/notification/TrafficJamNotification;->trafficJamEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficJamProgressEvent;

    .line 220
    return-void
.end method
