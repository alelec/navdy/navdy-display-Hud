.class Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification$1;
.super Ljava/lang/Object;
.source "RouteTimeUpdateNotification.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/component/ChoiceLayout2$IListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification$1;->this$0:Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public executeItem(Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;)V
    .locals 3
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 113
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification$1;->this$0:Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;

    iget-object v0, v0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-nez v0, :cond_1

    .line 149
    :cond_0
    :goto_0
    return-void

    .line 117
    :cond_1
    iget v0, p1, Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;->id:I

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 144
    :sswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification$1;->this$0:Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;

    iget-object v0, v0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification$1;->this$0:Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;

    iget-object v0, v0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v0}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->isExpandedWithStack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification$1;->this$0:Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;

    iget-object v0, v0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->controller:Lcom/navdy/hud/app/framework/notifications/INotificationController;

    invoke-interface {v0, v1, v1}, Lcom/navdy/hud/app/framework/notifications/INotificationController;->collapseNotification(ZZ)V

    goto :goto_0

    .line 119
    :sswitch_1
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification$1;->this$0:Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;

    # getter for: Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->fasterRouteEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->access$000(Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;)Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 120
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification$1;->this$0:Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;

    # getter for: Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->access$100(Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;)Lcom/squareup/otto/Bus;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteAction;->REROUTE:Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteAction;

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 121
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification$1;->this$0:Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;

    # setter for: Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->fasterRouteEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;
    invoke-static {v0, v2}, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->access$002(Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;)Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;

    .line 123
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification$1;->this$0:Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->dismissNotification()V

    goto :goto_0

    .line 127
    :sswitch_2
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification$1;->this$0:Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;

    # getter for: Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->fasterRouteEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->access$000(Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;)Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 128
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification$1;->this$0:Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;

    # getter for: Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->access$100(Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;)Lcom/squareup/otto/Bus;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteAction;->DISMISS:Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteAction;

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 129
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification$1;->this$0:Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;

    # setter for: Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->fasterRouteEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;
    invoke-static {v0, v2}, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->access$002(Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;)Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;

    .line 134
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification$1;->this$0:Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->dismissNotification()V

    goto :goto_0

    .line 130
    :cond_4
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification$1;->this$0:Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;

    # getter for: Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->trafficDelayEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficDelayEvent;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->access$200(Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;)Lcom/navdy/hud/app/maps/MapEvents$TrafficDelayEvent;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 131
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification$1;->this$0:Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;

    # setter for: Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->trafficDelayEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficDelayEvent;
    invoke-static {v0, v2}, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->access$202(Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;Lcom/navdy/hud/app/maps/MapEvents$TrafficDelayEvent;)Lcom/navdy/hud/app/maps/MapEvents$TrafficDelayEvent;

    goto :goto_1

    .line 138
    :sswitch_3
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification$1;->this$0:Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;

    # getter for: Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->fasterRouteEvent:Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->access$000(Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;)Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification$1;->this$0:Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;

    # invokes: Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->switchToExpandedMode()V
    invoke-static {v0}, Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;->access$300(Lcom/navdy/hud/app/maps/notification/RouteTimeUpdateNotification;)V

    goto :goto_0

    .line 117
    nop

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0x65 -> :sswitch_1
        0x66 -> :sswitch_2
        0x67 -> :sswitch_3
    .end sparse-switch
.end method

.method public itemSelected(Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;)V
    .locals 0
    .param p1, "selection"    # Lcom/navdy/hud/app/ui/component/ChoiceLayout2$Selection;

    .prologue
    .line 152
    return-void
.end method
