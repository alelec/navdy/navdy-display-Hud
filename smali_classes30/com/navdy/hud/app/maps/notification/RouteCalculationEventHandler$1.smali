.class Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler$1;
.super Ljava/lang/Object;
.source "RouteCalculationEventHandler.java"

# interfaces
.implements Lcom/navdy/hud/app/ui/framework/INotificationAnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler$1;->this$0:Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onStart(Ljava/lang/String;Lcom/navdy/hud/app/framework/notifications/NotificationType;Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V
    .locals 3
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "type"    # Lcom/navdy/hud/app/framework/notifications/NotificationType;
    .param p3, "mode"    # Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    .prologue
    .line 73
    sget-object v1, Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;->COLLAPSE:Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    if-ne p3, v1, :cond_0

    const-string v1, "navdy#route#calc#notif"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 74
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v1

    const-string v2, "navdy#route#calc#notif"

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotification(Ljava/lang/String;)Lcom/navdy/hud/app/framework/notifications/INotification;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    .line 75
    .local v0, "notif":Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;
    if-eqz v0, :cond_0

    .line 76
    # getter for: Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "hideStartTrip"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 77
    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->hideStartTrip()V

    .line 80
    .end local v0    # "notif":Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;
    :cond_0
    return-void
.end method

.method public onStop(Ljava/lang/String;Lcom/navdy/hud/app/framework/notifications/NotificationType;Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;)V
    .locals 3
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "type"    # Lcom/navdy/hud/app/framework/notifications/NotificationType;
    .param p3, "mode"    # Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    .prologue
    .line 84
    sget-object v1, Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;->EXPAND:Lcom/navdy/hud/app/ui/framework/UIStateManager$Mode;

    if-ne p3, v1, :cond_0

    const-string v1, "navdy#route#calc#notif"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 85
    # getter for: Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "showStartTrip: check"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 86
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v1

    const-string v2, "navdy#route#calc#notif"

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotification(Ljava/lang/String;)Lcom/navdy/hud/app/framework/notifications/INotification;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;

    .line 87
    .local v0, "notif":Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->isStarting()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 88
    # getter for: Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "showStartTrip"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 89
    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->showStartTrip()V

    .line 92
    .end local v0    # "notif":Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;
    :cond_0
    return-void
.end method
