.class public Lcom/navdy/hud/app/maps/MapsNotification;
.super Ljava/lang/Object;
.source "MapsNotification.java"


# static fields
.field private static final MAP_ENGINE_NOT_INIT_ID:Ljava/lang/String; = "maps-no-init"

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 19
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/maps/MapsNotification;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/maps/MapsNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static showMapsEngineNotInitializedToast()V
    .locals 10

    .prologue
    .line 24
    sget-object v0, Lcom/navdy/hud/app/maps/MapsNotification;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "maps engine not init toast"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 25
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 26
    .local v7, "resources":Landroid/content/res/Resources;
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 27
    .local v2, "bundle":Landroid/os/Bundle;
    const-string v0, "13"

    const/16 v1, 0x7d0

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 28
    const-string v0, "8"

    const v1, 0x7f02017d

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 29
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getError()Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    move-result-object v6

    .line 31
    .local v6, "error":Lcom/here/android/mpa/common/OnEngineInitListener$Error;
    if-nez v6, :cond_0

    .line 32
    const-string v8, ""

    .line 37
    .local v8, "str":Ljava/lang/String;
    :goto_0
    const-string v0, "4"

    const v1, 0x7f0901a2

    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    const-string v0, "6"

    invoke-virtual {v2, v0, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    invoke-static {}, Lcom/navdy/hud/app/framework/toast/ToastManager;->getInstance()Lcom/navdy/hud/app/framework/toast/ToastManager;

    move-result-object v9

    new-instance v0, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;

    const-string v1, "maps-no-init"

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;-><init>(Ljava/lang/String;Landroid/os/Bundle;Lcom/navdy/hud/app/framework/toast/IToastCallback;ZZ)V

    invoke-virtual {v9, v0}, Lcom/navdy/hud/app/framework/toast/ToastManager;->addToast(Lcom/navdy/hud/app/framework/toast/ToastManager$ToastParams;)V

    .line 40
    return-void

    .line 34
    .end local v8    # "str":Ljava/lang/String;
    :cond_0
    invoke-virtual {v6}, Lcom/here/android/mpa/common/OnEngineInitListener$Error;->name()Ljava/lang/String;

    move-result-object v8

    .restart local v8    # "str":Ljava/lang/String;
    goto :goto_0
.end method
