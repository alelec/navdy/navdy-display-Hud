.class final enum Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;
.super Ljava/lang/Enum;
.source "MapSchemeController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/MapSchemeController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "MapScheme"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;

.field public static final enum DAY:Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;

.field public static final enum NIGHT:Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 56
    new-instance v0, Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;

    const-string v1, "DAY"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;->DAY:Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;

    new-instance v0, Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;

    const-string v1, "NIGHT"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;->NIGHT:Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;

    .line 55
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;

    sget-object v1, Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;->DAY:Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;->NIGHT:Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;->$VALUES:[Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 55
    const-class v0, Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;->$VALUES:[Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;

    return-object v0
.end method
