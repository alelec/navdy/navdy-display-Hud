.class public final enum Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;
.super Ljava/lang/Enum;
.source "MapEvents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/MapEvents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RouteEventType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;

.field public static final enum FAILED:Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;

.field public static final enum FINISHED:Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;

.field public static final enum STARTED:Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 168
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;

    const-string v1, "STARTED"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;->STARTED:Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;

    .line 169
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;

    const-string v1, "FINISHED"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;->FINISHED:Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;

    .line 170
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;->FAILED:Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;

    .line 167
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;->STARTED:Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;->FINISHED:Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;->FAILED:Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;->$VALUES:[Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 167
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 167
    const-class v0, Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;
    .locals 1

    .prologue
    .line 167
    sget-object v0, Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;->$VALUES:[Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;

    return-object v0
.end method
