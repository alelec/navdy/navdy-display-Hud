.class public Lcom/navdy/hud/app/maps/GpsEventsReceiver;
.super Landroid/content/BroadcastReceiver;
.source "GpsEventsReceiver.java"


# static fields
.field private static final DRIVING_STARTED:Lcom/navdy/hud/app/event/DrivingStateChange;

.field private static final DRIVING_STOPPED:Lcom/navdy/hud/app/event/DrivingStateChange;


# instance fields
.field private sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 25
    new-instance v0, Lcom/navdy/hud/app/event/DrivingStateChange;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/event/DrivingStateChange;-><init>(Z)V

    sput-object v0, Lcom/navdy/hud/app/maps/GpsEventsReceiver;->DRIVING_STARTED:Lcom/navdy/hud/app/event/DrivingStateChange;

    .line 26
    new-instance v0, Lcom/navdy/hud/app/event/DrivingStateChange;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/event/DrivingStateChange;-><init>(Z)V

    sput-object v0, Lcom/navdy/hud/app/maps/GpsEventsReceiver;->DRIVING_STOPPED:Lcom/navdy/hud/app/event/DrivingStateChange;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 23
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/maps/GpsEventsReceiver;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/GpsEventsReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method private reportGpsEvent(Landroid/content/Intent;)V
    .locals 12
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 35
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 36
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 37
    .local v1, "b":Landroid/os/Bundle;
    const/4 v9, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v9, :pswitch_data_0

    .line 95
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "b":Landroid/os/Bundle;
    :cond_1
    :goto_1
    return-void

    .line 37
    .restart local v0    # "action":Ljava/lang/String;
    .restart local v1    # "b":Landroid/os/Bundle;
    :sswitch_0
    const-string v10, "GPS_DR_STARTED"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    const/4 v9, 0x0

    goto :goto_0

    :sswitch_1
    const-string v10, "GPS_DR_STOPPED"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    const/4 v9, 0x1

    goto :goto_0

    :sswitch_2
    const-string v10, "GPS_Switch"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    const/4 v9, 0x2

    goto :goto_0

    :sswitch_3
    const-string v10, "driving_started"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    const/4 v9, 0x3

    goto :goto_0

    :sswitch_4
    const-string v10, "driving_stopped"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    const/4 v9, 0x4

    goto :goto_0

    :sswitch_5
    const-string v10, "GPS_WARM_RESET_UBLOX"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    const/4 v9, 0x5

    goto :goto_0

    :sswitch_6
    const-string v10, "GPS_ENABLE_ESF_RAW"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    const/4 v9, 0x6

    goto :goto_0

    :sswitch_7
    const-string v10, "GPS_SATELLITE_STATUS"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    const/4 v9, 0x7

    goto :goto_0

    :sswitch_8
    const-string v10, "GPS_COLLECT_LOGS"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    const/16 v9, 0x8

    goto :goto_0

    .line 39
    :pswitch_0
    const-string v9, ""

    invoke-static {v9}, Lcom/navdy/hud/app/framework/voice/TTSUtils;->debugShowDRStarted(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 92
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "b":Landroid/os/Bundle;
    :catch_0
    move-exception v5

    .line 93
    .local v5, "t":Ljava/lang/Throwable;
    iget-object v9, p0, Lcom/navdy/hud/app/maps/GpsEventsReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v9, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 43
    .end local v5    # "t":Ljava/lang/Throwable;
    .restart local v0    # "action":Ljava/lang/String;
    .restart local v1    # "b":Landroid/os/Bundle;
    :pswitch_1
    :try_start_1
    invoke-static {}, Lcom/navdy/hud/app/framework/voice/TTSUtils;->debugShowDREnded()V

    goto :goto_1

    .line 47
    :pswitch_2
    const-string v9, "title"

    invoke-virtual {v1, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 48
    .local v6, "title":Ljava/lang/String;
    const-string v9, "info"

    invoke-virtual {v1, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 49
    .local v3, "info":Ljava/lang/String;
    invoke-static {v6, v3}, Lcom/navdy/hud/app/framework/voice/TTSUtils;->debugShowGpsSwitch(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    const-string v9, "phone"

    invoke-virtual {v1, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    .line 51
    .local v7, "usingPhoneGps":Z
    const-string v9, "ublox"

    invoke-virtual {v1, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    .line 52
    .local v8, "usingUblox":Z
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v9

    new-instance v10, Lcom/navdy/hud/app/device/gps/GpsUtils$GpsSwitch;

    invoke-direct {v10, v7, v8}, Lcom/navdy/hud/app/device/gps/GpsUtils$GpsSwitch;-><init>(ZZ)V

    invoke-virtual {v9, v10}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 56
    .end local v3    # "info":Ljava/lang/String;
    .end local v6    # "title":Ljava/lang/String;
    .end local v7    # "usingPhoneGps":Z
    .end local v8    # "usingUblox":Z
    :pswitch_3
    iget-object v9, p0, Lcom/navdy/hud/app/maps/GpsEventsReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "Driving started"

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 57
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v9

    sget-object v10, Lcom/navdy/hud/app/maps/GpsEventsReceiver;->DRIVING_STARTED:Lcom/navdy/hud/app/event/DrivingStateChange;

    invoke-virtual {v9, v10}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 61
    :pswitch_4
    iget-object v9, p0, Lcom/navdy/hud/app/maps/GpsEventsReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "Driving stopped"

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 62
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v9

    sget-object v10, Lcom/navdy/hud/app/maps/GpsEventsReceiver;->DRIVING_STOPPED:Lcom/navdy/hud/app/event/DrivingStateChange;

    invoke-virtual {v9, v10}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 66
    :pswitch_5
    iget-object v9, p0, Lcom/navdy/hud/app/maps/GpsEventsReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "warm reset ublox"

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 67
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->getInstance()Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->sendWarmReset()V

    .line 68
    const-string v9, "Warm Reset Ublox"

    invoke-static {v9}, Lcom/navdy/hud/app/framework/voice/TTSUtils;->debugShowGpsReset(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 72
    :pswitch_6
    iget-object v9, p0, Lcom/navdy/hud/app/maps/GpsEventsReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "esf-raw"

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 73
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->getInstance()Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->enableEsfRaw()V

    goto/16 :goto_1

    .line 77
    :pswitch_7
    const-string v9, "satellite_data"

    invoke-virtual {p1, v9}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .line 78
    .local v2, "data":Landroid/os/Bundle;
    if-eqz v2, :cond_1

    .line 79
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v9

    new-instance v10, Lcom/navdy/hud/app/device/gps/GpsUtils$GpsSatelliteData;

    invoke-direct {v10, v2}, Lcom/navdy/hud/app/device/gps/GpsUtils$GpsSatelliteData;-><init>(Landroid/os/Bundle;)V

    invoke-virtual {v9, v10}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 84
    .end local v2    # "data":Landroid/os/Bundle;
    :pswitch_8
    const-string v9, "logPath"

    invoke-virtual {p1, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 85
    .local v4, "path":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 86
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->getInstance()Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    move-result-object v9

    invoke-virtual {v9, v4}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->dumpGpsInfo(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 88
    :cond_2
    iget-object v9, p0, Lcom/navdy/hud/app/maps/GpsEventsReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Invalid gps log path:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 37
    nop

    :sswitch_data_0
    .sparse-switch
        -0x79e7c955 -> :sswitch_7
        -0x6b600b7b -> :sswitch_0
        -0x6a9bba2f -> :sswitch_1
        -0x258753f7 -> :sswitch_3
        -0x24c302ab -> :sswitch_4
        -0xb72f1c6 -> :sswitch_6
        -0x66cc357 -> :sswitch_2
        0x35819ad3 -> :sswitch_5
        0x4e65a559 -> :sswitch_8
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 30
    invoke-direct {p0, p2}, Lcom/navdy/hud/app/maps/GpsEventsReceiver;->reportGpsEvent(Landroid/content/Intent;)V

    .line 31
    return-void
.end method
