.class Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;
.super Ljava/lang/Object;
.source "HereTrafficUpdater2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "HereTrafficEvent"
.end annotation


# instance fields
.field public distance:J

.field public trafficEvent:Lcom/here/android/mpa/mapping/TrafficEvent;


# direct methods
.method public constructor <init>(Lcom/here/android/mpa/mapping/TrafficEvent;J)V
    .locals 0
    .param p1, "trafficEvent"    # Lcom/here/android/mpa/mapping/TrafficEvent;
    .param p2, "distance"    # J

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;->trafficEvent:Lcom/here/android/mpa/mapping/TrafficEvent;

    .line 77
    iput-wide p2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;->distance:J

    .line 78
    return-void
.end method
