.class Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;
.super Ljava/lang/Object;
.source "HereRouteCalculator.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->onCalculateRouteFinished(Ljava/util/List;Lcom/here/android/mpa/routing/RoutingError;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

.field final synthetic val$results:Ljava/util/List;

.field final synthetic val$routingError:Lcom/here/android/mpa/routing/RoutingError;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;Lcom/here/android/mpa/routing/RoutingError;Ljava/util/List;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    .prologue
    .line 120
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->val$routingError:Lcom/here/android/mpa/routing/RoutingError;

    iput-object p3, p0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->val$results:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 28

    .prologue
    .line 124
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    .line 125
    .local v14, "l2":J
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->val$routingError:Lcom/here/android/mpa/routing/RoutingError;

    sget-object v7, Lcom/here/android/mpa/routing/RoutingError;->NONE:Lcom/here/android/mpa/routing/RoutingError;

    if-eq v2, v7, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->val$routingError:Lcom/here/android/mpa/routing/RoutingError;

    sget-object v7, Lcom/here/android/mpa/routing/RoutingError;->VIOLATES_OPTIONS:Lcom/here/android/mpa/routing/RoutingError;

    if-ne v2, v7, :cond_d

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->val$results:Ljava/util/List;

    .line 126
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_d

    .line 127
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->val$listener:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$RouteCalculatorListener;

    invoke-interface {v2}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$RouteCalculatorListener;->preSuccess()V

    .line 128
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->val$results:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v18

    .line 129
    .local v18, "len":I
    new-instance v19, Ljava/util/ArrayList;

    move-object/from16 v0, v19

    move/from16 v1, v18

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 130
    .local v19, "outgoingResults":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/navigation/NavigationRouteResult;>;"
    const/16 v20, 0x0

    .line 131
    .local v20, "requestId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    iget-boolean v2, v2, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->val$allowCancellation:Z

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->val$request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    if-eqz v2, :cond_1

    .line 132
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->val$request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v0, v2, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestId:Ljava/lang/String;

    move-object/from16 v20, v0

    .line 134
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->val$results:Ljava/util/List;

    move-object/from16 v0, v20

    invoke-static {v2, v0}, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator;->generateVia(Ljava/util/List;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v24

    .line 135
    .local v24, "viaList":[Ljava/lang/String;
    const/4 v12, 0x0

    .line 137
    .local v12, "index":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->val$results:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v27

    move v13, v12

    .end local v12    # "index":I
    .local v13, "index":I
    :goto_0
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/here/android/mpa/routing/RouteResult;

    .line 138
    .local v22, "routeResult":Lcom/here/android/mpa/routing/RouteResult;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    iget-boolean v2, v2, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->val$allowCancellation:Z

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->this$0:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    iget-object v7, v7, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->val$request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    # invokes: Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->isRouteCancelled(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)Z
    invoke-static {v2, v7}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->access$000(Lcom/navdy/hud/app/maps/here/HereRouteCalculator;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 198
    .end local v13    # "index":I
    .end local v14    # "l2":J
    .end local v18    # "len":I
    .end local v19    # "outgoingResults":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/navigation/NavigationRouteResult;>;"
    .end local v20    # "requestId":Ljava/lang/String;
    .end local v22    # "routeResult":Lcom/here/android/mpa/routing/RouteResult;
    .end local v24    # "viaList":[Ljava/lang/String;
    :cond_2
    :goto_1
    return-void

    .line 142
    .restart local v13    # "index":I
    .restart local v14    # "l2":J
    .restart local v18    # "len":I
    .restart local v19    # "outgoingResults":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/navigation/NavigationRouteResult;>;"
    .restart local v20    # "requestId":Ljava/lang/String;
    .restart local v22    # "routeResult":Lcom/here/android/mpa/routing/RouteResult;
    .restart local v24    # "viaList":[Ljava/lang/String;
    :cond_3
    invoke-virtual/range {v22 .. v22}, Lcom/here/android/mpa/routing/RouteResult;->getRoute()Lcom/here/android/mpa/routing/Route;

    move-result-object v4

    .line 143
    .local v4, "route":Lcom/here/android/mpa/routing/Route;
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    .line 144
    .local v3, "id":Ljava/lang/String;
    add-int/lit8 v12, v13, 0x1

    .end local v13    # "index":I
    .restart local v12    # "index":I
    aget-object v5, v24, v13

    .line 145
    .local v5, "via":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 146
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v7, 0x7f090245

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v8, v10

    invoke-virtual {v2, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 149
    :cond_4
    invoke-virtual/range {v22 .. v22}, Lcom/here/android/mpa/routing/RouteResult;->getViolatedOptions()Ljava/util/EnumSet;

    move-result-object v26

    .line 150
    .local v26, "violatedOptions":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/here/android/mpa/routing/RouteResult$ViolatedOption;>;"
    if-eqz v26, :cond_5

    invoke-virtual/range {v26 .. v26}, Ljava/util/EnumSet;->size()I

    move-result v2

    if-lez v2, :cond_5

    .line 151
    invoke-virtual/range {v26 .. v26}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/here/android/mpa/routing/RouteResult$ViolatedOption;

    .line 152
    .local v25, "violatedOption":Lcom/here/android/mpa/routing/RouteResult$ViolatedOption;
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    iget-object v7, v7, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->this$0:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v7}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->access$200(Lcom/navdy/hud/app/maps/here/HereRouteCalculator;)Lcom/navdy/service/library/log/Logger;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    iget-object v10, v10, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->this$0:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->tag:Ljava/lang/String;
    invoke-static {v10}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->access$100(Lcom/navdy/hud/app/maps/here/HereRouteCalculator;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, " violated options ["

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual/range {v25 .. v25}, Lcom/here/android/mpa/routing/RouteResult$ViolatedOption;->name()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, "]"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 194
    .end local v3    # "id":Ljava/lang/String;
    .end local v4    # "route":Lcom/here/android/mpa/routing/Route;
    .end local v5    # "via":Ljava/lang/String;
    .end local v12    # "index":I
    .end local v14    # "l2":J
    .end local v18    # "len":I
    .end local v19    # "outgoingResults":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/navigation/NavigationRouteResult;>;"
    .end local v20    # "requestId":Ljava/lang/String;
    .end local v22    # "routeResult":Lcom/here/android/mpa/routing/RouteResult;
    .end local v24    # "viaList":[Ljava/lang/String;
    .end local v25    # "violatedOption":Lcom/here/android/mpa/routing/RouteResult$ViolatedOption;
    .end local v26    # "violatedOptions":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/here/android/mpa/routing/RouteResult$ViolatedOption;>;"
    :catch_0
    move-exception v23

    .line 195
    .local v23, "t":Ljava/lang/Throwable;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->this$0:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->access$200(Lcom/navdy/hud/app/maps/here/HereRouteCalculator;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    iget-object v7, v7, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->this$0:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->tag:Ljava/lang/String;
    invoke-static {v7}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->access$100(Lcom/navdy/hud/app/maps/here/HereRouteCalculator;)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v23

    invoke-virtual {v2, v7, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 196
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->val$listener:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$RouteCalculatorListener;

    const/4 v7, 0x0

    move-object/from16 v0, v23

    invoke-interface {v2, v7, v0}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$RouteCalculatorListener;->error(Lcom/here/android/mpa/routing/RoutingError;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 158
    .end local v23    # "t":Ljava/lang/Throwable;
    .restart local v3    # "id":Ljava/lang/String;
    .restart local v4    # "route":Lcom/here/android/mpa/routing/Route;
    .restart local v5    # "via":Ljava/lang/String;
    .restart local v12    # "index":I
    .restart local v14    # "l2":J
    .restart local v18    # "len":I
    .restart local v19    # "outgoingResults":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/navigation/NavigationRouteResult;>;"
    .restart local v20    # "requestId":Ljava/lang/String;
    .restart local v22    # "routeResult":Lcom/here/android/mpa/routing/RouteResult;
    .restart local v24    # "viaList":[Ljava/lang/String;
    .restart local v26    # "violatedOptions":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/here/android/mpa/routing/RouteResult$ViolatedOption;>;"
    :cond_5
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->val$request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    if-eqz v2, :cond_9

    .line 159
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->this$0:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    iget-object v7, v7, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->val$request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v6, v7, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->label:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    iget-object v7, v7, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->val$request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v7, v7, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->streetAddress:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    iget-boolean v8, v8, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->val$calculatePolyline:Z

    invoke-virtual/range {v2 .. v8}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->getRouteResultFromRoute(Ljava/lang/String;Lcom/here/android/mpa/routing/Route;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    move-result-object v9

    .line 164
    .local v9, "outgoingRouteResult":Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    :goto_3
    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 166
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    iget-boolean v2, v2, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->val$cache:Z

    if-eqz v2, :cond_6

    .line 167
    new-instance v6, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    iget-object v8, v2, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->val$request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    iget-boolean v10, v2, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->val$factoringInTraffic:Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    iget-object v11, v2, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->val$startPoint:Lcom/here/android/mpa/common/GeoCoordinate;

    move-object v7, v4

    invoke-direct/range {v6 .. v11}, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;-><init>(Lcom/here/android/mpa/routing/Route;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/navdy/service/library/events/navigation/NavigationRouteResult;ZLcom/here/android/mpa/common/GeoCoordinate;)V

    .line 168
    .local v6, "routeInfo":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getInstance()Lcom/navdy/hud/app/maps/here/HereRouteCache;

    move-result-object v2

    invoke-virtual {v2, v3, v6}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->addRoute(Ljava/lang/String;Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;)V

    .line 171
    .end local v6    # "routeInfo":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->this$0:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->access$200(Lcom/navdy/hud/app/maps/here/HereRouteCalculator;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const/4 v7, 0x2

    invoke-virtual {v2, v7}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 172
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->val$request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    if-nez v2, :cond_a

    const-string v2, ""

    :goto_4
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    iget-object v7, v7, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->val$request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    const/4 v8, 0x0

    invoke-static {v4, v2, v7, v8}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->printRouteDetails(Lcom/here/android/mpa/routing/Route;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/StringBuilder;)V

    .line 175
    :cond_7
    invoke-static {}, Lcom/navdy/hud/app/maps/MapSettings;->isGenerateRouteIcons()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 176
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->val$request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v2, v2, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->label:Ljava/lang/String;

    invoke-static {v3, v2, v4}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->generateRouteIcons(Ljava/lang/String;Ljava/lang/String;Lcom/here/android/mpa/routing/Route;)V

    :cond_8
    move v13, v12

    .line 178
    .end local v12    # "index":I
    .restart local v13    # "index":I
    goto/16 :goto_0

    .line 161
    .end local v9    # "outgoingRouteResult":Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    .end local v13    # "index":I
    .restart local v12    # "index":I
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->this$0:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    iget-boolean v8, v8, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->val$calculatePolyline:Z

    invoke-virtual/range {v2 .. v8}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->getRouteResultFromRoute(Ljava/lang/String;Lcom/here/android/mpa/routing/Route;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    move-result-object v9

    .restart local v9    # "outgoingRouteResult":Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    goto :goto_3

    .line 172
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->val$request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v2, v2, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->streetAddress:Ljava/lang/String;

    goto :goto_4

    .line 180
    .end local v3    # "id":Ljava/lang/String;
    .end local v4    # "route":Lcom/here/android/mpa/routing/Route;
    .end local v5    # "via":Ljava/lang/String;
    .end local v9    # "outgoingRouteResult":Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    .end local v12    # "index":I
    .end local v22    # "routeResult":Lcom/here/android/mpa/routing/RouteResult;
    .end local v26    # "violatedOptions":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/here/android/mpa/routing/RouteResult$ViolatedOption;>;"
    .restart local v13    # "index":I
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    iget-boolean v2, v2, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->val$allowCancellation:Z

    if-eqz v2, :cond_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->this$0:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    iget-object v7, v7, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->val$request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    # invokes: Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->isRouteCancelled(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)Z
    invoke-static {v2, v7}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->access$000(Lcom/navdy/hud/app/maps/here/HereRouteCalculator;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 183
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->val$listener:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$RouteCalculatorListener;

    move-object/from16 v0, v19

    invoke-interface {v2, v0}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$RouteCalculatorListener;->postSuccess(Ljava/util/ArrayList;)V

    .line 192
    .end local v13    # "index":I
    .end local v18    # "len":I
    .end local v19    # "outgoingResults":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/navigation/NavigationRouteResult;>;"
    .end local v20    # "requestId":Ljava/lang/String;
    .end local v24    # "viaList":[Ljava/lang/String;
    :goto_5
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v16

    .line 193
    .local v16, "l3":J
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->this$0:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->access$200(Lcom/navdy/hud/app/maps/here/HereRouteCalculator;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    iget-object v8, v8, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->this$0:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->tag:Ljava/lang/String;
    invoke-static {v8}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->access$100(Lcom/navdy/hud/app/maps/here/HereRouteCalculator;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "handleSearchRequest- time-2="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sub-long v10, v16, v14

    invoke-virtual {v7, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 185
    .end local v16    # "l3":J
    :cond_d
    const/16 v21, -0x1

    .line 186
    .local v21, "resultsSize":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->val$results:Ljava/util/List;

    if-eqz v2, :cond_e

    .line 187
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->val$results:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v21

    .line 189
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->this$0:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->access$200(Lcom/navdy/hud/app/maps/here/HereRouteCalculator;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    iget-object v8, v8, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->this$0:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->tag:Ljava/lang/String;
    invoke-static {v8}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->access$100(Lcom/navdy/hud/app/maps/here/HereRouteCalculator;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "got error:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->val$routingError:Lcom/here/android/mpa/routing/RoutingError;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " results="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 190
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->val$listener:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$RouteCalculatorListener;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;->val$routingError:Lcom/here/android/mpa/routing/RoutingError;

    const/4 v8, 0x0

    invoke-interface {v2, v7, v8}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$RouteCalculatorListener;->error(Lcom/here/android/mpa/routing/RoutingError;Ljava/lang/Throwable;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_5
.end method
