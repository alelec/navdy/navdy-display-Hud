.class public Lcom/navdy/hud/app/maps/here/HereMapUtil$SavedRouteData;
.super Ljava/lang/Object;
.source "HereMapUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/here/HereMapUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SavedRouteData"
.end annotation


# instance fields
.field public final isGasRoute:Z

.field public final navigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;


# direct methods
.method public constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Z)V
    .locals 0
    .param p1, "navigationRouteRequest"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .param p2, "isGasRoute"    # Z

    .prologue
    .line 1074
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1075
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapUtil$SavedRouteData;->navigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .line 1076
    iput-boolean p2, p0, Lcom/navdy/hud/app/maps/here/HereMapUtil$SavedRouteData;->isGasRoute:Z

    .line 1077
    return-void
.end method
