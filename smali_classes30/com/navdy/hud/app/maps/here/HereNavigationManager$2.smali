.class Lcom/navdy/hud/app/maps/here/HereNavigationManager$2;
.super Ljava/lang/Object;
.source "HereNavigationManager.java"

# interfaces
.implements Lcom/here/android/mpa/guidance/AudioPlayerDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereNavigationManager;->setupNavigationTTS()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    .prologue
    .line 561
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public playFiles([Ljava/lang/String;)Z
    .locals 1
    .param p1, "strings"    # [Ljava/lang/String;

    .prologue
    .line 603
    const/4 v0, 0x1

    return v0
.end method

.method public playText(Ljava/lang/String;)Z
    .locals 8
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    .line 564
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[CB-TTS] ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 565
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationMode:Lcom/navdy/hud/app/maps/NavigationMode;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$000(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Lcom/navdy/hud/app/maps/NavigationMode;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/maps/NavigationMode;->MAP:Lcom/navdy/hud/app/maps/NavigationMode;

    if-ne v1, v2, :cond_1

    .line 566
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "[CB-TTS] not navigating"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 598
    :cond_0
    :goto_0
    return v7

    .line 569
    :cond_1
    const/4 v0, 0x0

    .line 570
    .local v0, "hasArrivedPattern":Z
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->HERE_TBT_AUDIO_DESTINATION_REACHED_PATTERN:Ljava/lang/String;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$300(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 571
    const/4 v0, 0x1

    .line 573
    :cond_2
    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$100(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    move-result-object v1

    iget-boolean v1, v1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->hasArrived:Z

    if-nez v1, :cond_3

    .line 576
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "last maneuver marked arrived tts:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$100(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    move-result-object v3

    iget-boolean v3, v3, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->lastManeuver:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 577
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$100(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    move-result-object v1

    iget-boolean v1, v1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->lastManeuver:Z

    if-nez v1, :cond_3

    .line 578
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->setLastManeuver()V

    .line 581
    :cond_3
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getNavigationSessionPreference()Lcom/navdy/hud/app/maps/NavSessionPreferences;

    move-result-object v1

    iget-boolean v1, v1, Lcom/navdy/hud/app/maps/NavSessionPreferences;->spokenTurnByTurn:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$100(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    move-result-object v1

    iget-boolean v1, v1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->hasArrived:Z

    if-nez v1, :cond_0

    .line 584
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$100(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    move-result-object v1

    iget-boolean v1, v1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->lastManeuver:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    .line 585
    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$100(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    move-result-object v1

    iget-object v1, v1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destinationDirection:Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    .line 586
    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$100(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    move-result-object v1

    iget-object v1, v1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destinationDirection:Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;

    sget-object v2, Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;->UNKNOWN:Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;

    if-eq v1, v2, :cond_4

    .line 588
    if-eqz v0, :cond_4

    .line 589
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Found destination reached tts["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 590
    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->context:Landroid/content/Context;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$400()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f090289

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    .line 591
    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$100(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    move-result-object v1

    iget-object v1, v1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destinationDirection:Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;

    sget-object v6, Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;->LEFT:Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;

    if-ne v1, v6, :cond_5

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->DESTINATION_LEFT:Ljava/lang/String;

    :goto_1
    aput-object v1, v4, v5

    .line 590
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 592
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Converted destination reached tts["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 595
    :cond_4
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$500(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Lcom/squareup/otto/Bus;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->CANCEL_TBT_TTS:Lcom/navdy/hud/app/event/RemoteEvent;

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 596
    sget-object v1, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->SPEECH_TURN_BY_TURN:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    sget-object v2, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->ROUTE_TBT_TTS_ID:Ljava/lang/String;

    invoke-static {p1, v1, v2}, Lcom/navdy/hud/app/framework/voice/TTSUtils;->sendSpeechRequest(Ljava/lang/String;Lcom/navdy/service/library/events/audio/SpeechRequest$Category;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 591
    :cond_5
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->DESTINATION_RIGHT:Ljava/lang/String;

    goto :goto_1
.end method
