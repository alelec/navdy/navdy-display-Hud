.class Lcom/navdy/hud/app/maps/here/HereNavigationManager$6;
.super Ljava/lang/Object;
.source "HereNavigationManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereNavigationManager;->handleConnectionState(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

.field final synthetic val$deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereNavigationManager;Lcom/navdy/service/library/device/NavdyDeviceId;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    .prologue
    .line 1134
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$6;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$6;->val$deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1137
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stop navigation, device id has changed from["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$6;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$100(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    move-result-object v2

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] to ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$6;->val$deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 1138
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$6;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    sget-object v1, Lcom/navdy/hud/app/maps/NavigationMode;->MAP:Lcom/navdy/hud/app/maps/NavigationMode;

    invoke-virtual {v0, v1, v3, v3}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->setNavigationMode(Lcom/navdy/hud/app/maps/NavigationMode;Lcom/navdy/hud/app/maps/here/HereNavigationInfo;Ljava/lang/StringBuilder;)Z

    .line 1139
    return-void
.end method
