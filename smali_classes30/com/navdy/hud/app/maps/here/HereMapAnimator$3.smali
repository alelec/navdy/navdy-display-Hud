.class Lcom/navdy/hud/app/maps/here/HereMapAnimator$3;
.super Ljava/lang/Object;
.source "HereMapAnimator.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereMapAnimator;->setZoom(D)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

.field final synthetic val$zoom:D


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereMapAnimator;D)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    .prologue
    .line 272
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    iput-wide p2, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$3;->val$zoom:D

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 275
    const-wide/16 v0, 0x0

    .line 277
    .local v0, "interval":J
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapAnimator;->lastZoomUpdateTime:J
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->access$1000(Lcom/navdy/hud/app/maps/here/HereMapAnimator;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 278
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapAnimator;->lastZoomUpdateTime:J
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->access$1000(Lcom/navdy/hud/app/maps/here/HereMapAnimator;)J

    move-result-wide v4

    sub-long v0, v2, v4

    .line 281
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    iget-wide v4, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$3;->val$zoom:D

    # invokes: Lcom/navdy/hud/app/maps/here/HereMapAnimator;->isValidZoom(D)Z
    invoke-static {v2, v4, v5}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->access$1100(Lcom/navdy/hud/app/maps/here/HereMapAnimator;D)Z

    move-result v2

    if-nez v2, :cond_1

    .line 282
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapAnimator;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "filtering out spurious zoom: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$3;->val$zoom:D

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 291
    :goto_0
    return-void

    .line 285
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    # setter for: Lcom/navdy/hud/app/maps/here/HereMapAnimator;->lastZoomUpdateTime:J
    invoke-static {v2, v4, v5}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->access$1002(Lcom/navdy/hud/app/maps/here/HereMapAnimator;J)J

    .line 287
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapAnimator;->zoomLock:Ljava/lang/Object;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->access$1200()Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 288
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    # setter for: Lcom/navdy/hud/app/maps/here/HereMapAnimator;->zoomUpdateInterval:J
    invoke-static {v2, v0, v1}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->access$1302(Lcom/navdy/hud/app/maps/here/HereMapAnimator;J)J

    .line 289
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    iget-wide v4, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$3;->val$zoom:D

    # setter for: Lcom/navdy/hud/app/maps/here/HereMapAnimator;->currentZoom:D
    invoke-static {v2, v4, v5}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->access$1402(Lcom/navdy/hud/app/maps/here/HereMapAnimator;D)D

    .line 290
    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method
