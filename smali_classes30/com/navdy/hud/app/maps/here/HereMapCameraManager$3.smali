.class Lcom/navdy/hud/app/maps/here/HereMapCameraManager$3;
.super Ljava/lang/Object;
.source "HereMapCameraManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/here/HereMapCameraManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    .prologue
    .line 155
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 158
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->zoomActionOn:Z
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$200(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 161
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "easeout user zoom action is on"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 179
    :goto_0
    return-void

    .line 165
    :cond_0
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$300(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereMapController;->getZoomLevel()D

    move-result-wide v4

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentDynamicZoom:D
    invoke-static {v6}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$900(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)D

    move-result-wide v6

    sub-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    const-wide/high16 v6, 0x3fd0000000000000L    # 0.25

    cmpl-double v4, v4, v6

    if-ltz v4, :cond_2

    move v1, v2

    .line 166
    .local v1, "isSignificantZoomDiff":Z
    :goto_1
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$300(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereMapController;->getTilt()F

    move-result v4

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentDynamicTilt:F
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$1000(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)F

    move-result v5

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    const/high16 v5, 0x40a00000    # 5.0f

    cmpl-float v4, v4, v5

    if-lez v4, :cond_3

    move v0, v2

    .line 168
    .local v0, "isSignificantTiltDiff":Z
    :goto_2
    if-nez v1, :cond_1

    if-eqz v0, :cond_4

    .line 169
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    # setter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->lastDynamicZoomTime:J
    invoke-static {v2, v4, v5}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$1102(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;J)J

    .line 170
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # invokes: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->animateCamera()V
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$1200(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)V

    .line 171
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "easeout zoom="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " tilt="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " current-zoom="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    .line 173
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentDynamicZoom:D
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$900(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)D

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " current-tilt="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    .line 174
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentDynamicTilt:F
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$1000(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 171
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .end local v0    # "isSignificantTiltDiff":Z
    .end local v1    # "isSignificantZoomDiff":Z
    :cond_2
    move v1, v3

    .line 165
    goto :goto_1

    .restart local v1    # "isSignificantZoomDiff":Z
    :cond_3
    move v0, v3

    .line 166
    goto :goto_2

    .line 176
    .restart local v0    # "isSignificantTiltDiff":Z
    :cond_4
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "easeout not significant current-zoom="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentDynamicZoom:D
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$900(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)D

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " current-tilt="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    .line 177
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentDynamicTilt:F
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$1000(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 176
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
