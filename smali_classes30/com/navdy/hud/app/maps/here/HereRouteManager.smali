.class public Lcom/navdy/hud/app/maps/here/HereRouteManager;
.super Ljava/lang/Object;
.source "HereRouteManager.java"


# static fields
.field private static final EMPTY_STR:Ljava/lang/String; = ""

.field public static final MAX_ALLOWED_NAV_DISPLAY_POSITION_DIFF:J = 0x274a6L

.field private static final MAX_DISTANCE:D = 1.5E7

.field private static final MAX_ONLINE_ROUTE_CALCULATION_TIME:I

.field private static final MAX_ROUTES:I

.field private static final MAX_ROUTES_PROP:Ljava/lang/String; = "persist.sys.routes.max"

.field private static final VERBOSE:Z

.field private static volatile activeGeoCalcId:Ljava/lang/String;

.field private static activeGeoRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

.field private static volatile activeRouteCalcId:Ljava/lang/String;

.field private static activeRouteCalculator:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

.field private static activeRouteCancelledByUser:Z

.field private static activeRouteGeocodeRequest:Lcom/here/android/mpa/search/GeocodeRequest;

.field private static activeRouteProgress:I

.field private static activeRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

.field private static final bus:Lcom/squareup/otto/Bus;

.field private static final context:Landroid/content/Context;

.field private static handler:Landroid/os/Handler;

.field private static final hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

.field private static final lockObj:Ljava/lang/Object;

.field private static final mapsEventHandler:Lcom/navdy/hud/app/maps/MapsEventHandler;

.field private static final pendingRouteRequestIds:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final pendingRouteRequestQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;",
            ">;"
        }
    .end annotation
.end field

.field private static routeCalculationEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 66
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 69
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .line 70
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->context:Landroid/content/Context;

    .line 74
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->handler:Landroid/os/Handler;

    .line 82
    const-string v0, "persist.sys.routes.max"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/navdy/hud/app/util/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->MAX_ROUTES:I

    .line 85
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x14

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->MAX_ONLINE_ROUTE_CALCULATION_TIME:I

    .line 87
    invoke-static {}, Lcom/navdy/hud/app/maps/MapsEventHandler;->getInstance()Lcom/navdy/hud/app/maps/MapsEventHandler;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->mapsEventHandler:Lcom/navdy/hud/app/maps/MapsEventHandler;

    .line 89
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->lockObj:Ljava/lang/Object;

    .line 104
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->pendingRouteRequestQueue:Ljava/util/Queue;

    .line 105
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->pendingRouteRequestIds:Ljava/util/HashSet;

    .line 107
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->bus:Lcom/squareup/otto/Bus;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/navdy/hud/app/maps/here/HereMapsManager;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    return-object v0
.end method

.method static synthetic access$100()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .prologue
    .line 65
    sput-object p0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeGeoRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/common/GeoCoordinate;Ljava/util/List;Lcom/here/android/mpa/common/GeoCoordinate;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .param p1, "x1"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p2, "x2"    # Ljava/util/List;
    .param p3, "x3"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p4, "x4"    # Z

    .prologue
    .line 65
    invoke-static {p0, p1, p2, p3, p4}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeSearch(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/common/GeoCoordinate;Ljava/util/List;Lcom/here/android/mpa/common/GeoCoordinate;Z)V

    return-void
.end method

.method static synthetic access$1200()I
    .locals 1

    .prologue
    .line 65
    sget v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->MAX_ONLINE_ROUTE_CALCULATION_TIME:I

    return v0
.end method

.method static synthetic access$1300(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/util/ArrayList;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .param p1, "x1"    # Ljava/util/ArrayList;
    .param p2, "x2"    # Z

    .prologue
    .line 65
    invoke-static {p0, p1, p2}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->returnSuccessResponse(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/util/ArrayList;Z)V

    return-void
.end method

.method static synthetic access$1400()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1500()Z
    .locals 1

    .prologue
    .line 65
    sget-boolean v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCancelledByUser:Z

    return v0
.end method

.method static synthetic access$1600(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/common/GeoCoordinate;Ljava/util/List;Lcom/here/android/mpa/common/GeoCoordinate;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .param p1, "x1"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p2, "x2"    # Ljava/util/List;
    .param p3, "x3"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p4, "x4"    # Z

    .prologue
    .line 65
    invoke-static {p0, p1, p2, p3, p4}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeSearchNoTraffic(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/common/GeoCoordinate;Ljava/util/List;Lcom/here/android/mpa/common/GeoCoordinate;Z)V

    return-void
.end method

.method static synthetic access$1702(Lcom/navdy/hud/app/maps/here/HereRouteCalculator;)Lcom/navdy/hud/app/maps/here/HereRouteCalculator;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    .prologue
    .line 65
    sput-object p0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCalculator:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    return-object p0
.end method

.method static synthetic access$1800()Lcom/navdy/hud/app/maps/MapsEventHandler;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->mapsEventHandler:Lcom/navdy/hud/app/maps/MapsEventHandler;

    return-object v0
.end method

.method static synthetic access$200()Landroid/content/Context;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/service/library/events/RequestStatus;
    .param p1, "x1"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 65
    invoke-static {p0, p1, p2}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->returnErrorResponse(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->lockObj:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$500()Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    return-object v0
.end method

.method static synthetic access$600()Ljava/util/HashSet;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->pendingRouteRequestIds:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic access$700(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/common/GeoCoordinate;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .param p1, "x1"    # Lcom/here/android/mpa/common/GeoCoordinate;

    .prologue
    .line 65
    invoke-static {p0, p1}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->handleRouteRequest(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/common/GeoCoordinate;)V

    return-void
.end method

.method static synthetic access$800()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeGeoCalcId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$802(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 65
    sput-object p0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeGeoCalcId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$902(Lcom/here/android/mpa/search/GeocodeRequest;)Lcom/here/android/mpa/search/GeocodeRequest;
    .locals 0
    .param p0, "x0"    # Lcom/here/android/mpa/search/GeocodeRequest;

    .prologue
    .line 65
    sput-object p0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteGeocodeRequest:Lcom/here/android/mpa/search/GeocodeRequest;

    return-object p0
.end method

.method public static buildChangeRouteTTS(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "routeId"    # Ljava/lang/String;

    .prologue
    .line 954
    const v0, 0x7f090240

    invoke-static {v0, p0}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->buildRouteTTS(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static buildRouteTTS(ILjava/lang/String;)Ljava/lang/String;
    .locals 14
    .param p0, "id"    # I
    .param p1, "routeId"    # Ljava/lang/String;

    .prologue
    const/4 v13, 0x2

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 958
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v9

    if-nez v9, :cond_0

    .line 959
    const-string v9, ""

    .line 1000
    .end local p1    # "routeId":Ljava/lang/String;
    :goto_0
    return-object v9

    .line 961
    .restart local p1    # "routeId":Ljava/lang/String;
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v2

    .line 962
    .local v2, "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 963
    .local v4, "resources":Landroid/content/res/Resources;
    const-string v6, ""

    .line 964
    .local v6, "routeType":Ljava/lang/String;
    const-string v1, ""

    .line 965
    .local v1, "eta":Ljava/lang/String;
    const-string v8, ""

    .line 967
    .local v8, "via":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getInstance()Lcom/navdy/hud/app/maps/here/HereRouteCache;

    move-result-object v9

    if-nez p1, :cond_1

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentRouteId()Ljava/lang/String;

    move-result-object p1

    .end local p1    # "routeId":Ljava/lang/String;
    :cond_1
    invoke-virtual {v9, p1}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getRoute(Ljava/lang/String;)Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;

    move-result-object v3

    .line 968
    .local v3, "info":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    if-eqz v3, :cond_3

    .line 969
    iget-object v9, v3, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->routeResult:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    iget-object v8, v9, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->via:Ljava/lang/String;

    .line 970
    sget-object v9, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getGeneratePhoneticTTS()Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 971
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "\u001b\\tn=address\\"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\u001b\\tn=normal\\"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 974
    :cond_2
    iget-object v9, v3, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->route:Lcom/here/android/mpa/routing/Route;

    invoke-static {v9}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getRouteTtaDate(Lcom/here/android/mpa/routing/Route;)Ljava/util/Date;

    move-result-object v7

    .line 975
    .local v7, "ttaDate":Ljava/util/Date;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 976
    .local v0, "builder":Ljava/lang/StringBuilder;
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getTimeHelper()Lcom/navdy/hud/app/common/TimeHelper;

    move-result-object v9

    invoke-virtual {v9, v7, v0, v11}, Lcom/navdy/hud/app/common/TimeHelper;->formatTime12Hour(Ljava/util/Date;Ljava/lang/StringBuilder;Z)Ljava/lang/String;

    move-result-object v1

    .line 977
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 980
    .end local v0    # "builder":Ljava/lang/StringBuilder;
    .end local v7    # "ttaDate":Ljava/util/Date;
    :cond_3
    sparse-switch p0, :sswitch_data_0

    .line 1000
    const-string v9, ""

    goto/16 :goto_0

    .line 982
    :sswitch_0
    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getRouteOptions()Lcom/here/android/mpa/routing/RouteOptions;

    move-result-object v5

    .line 983
    .local v5, "routeOptions":Lcom/here/android/mpa/routing/RouteOptions;
    if-eqz v5, :cond_4

    .line 984
    sget-object v9, Lcom/navdy/hud/app/maps/here/HereRouteManager$8;->$SwitchMap$com$here$android$mpa$routing$RouteOptions$Type:[I

    invoke-virtual {v5}, Lcom/here/android/mpa/routing/RouteOptions;->getRouteType()Lcom/here/android/mpa/routing/RouteOptions$Type;

    move-result-object v10

    invoke-virtual {v10}, Lcom/here/android/mpa/routing/RouteOptions$Type;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    .line 994
    :cond_4
    :goto_1
    const v9, 0x7f090247

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    aput-object v6, v10, v11

    aput-object v8, v10, v12

    aput-object v1, v10, v13

    invoke-virtual {v4, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_0

    .line 986
    :pswitch_0
    sget-object v6, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->shortestRoute:Ljava/lang/String;

    .line 987
    goto :goto_1

    .line 990
    :pswitch_1
    sget-object v6, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->fastestRoute:Ljava/lang/String;

    goto :goto_1

    .line 997
    .end local v5    # "routeOptions":Lcom/here/android/mpa/routing/RouteOptions;
    :sswitch_1
    const v9, 0x7f090240

    new-array v10, v13, [Ljava/lang/Object;

    aput-object v8, v10, v11

    aput-object v1, v10, v12

    invoke-virtual {v4, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_0

    .line 980
    :sswitch_data_0
    .sparse-switch
        0x7f090240 -> :sswitch_1
        0x7f090247 -> :sswitch_0
    .end sparse-switch

    .line 984
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static buildStartRouteTTS()Ljava/lang/String;
    .locals 2

    .prologue
    .line 950
    const v0, 0x7f090247

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->buildRouteTTS(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static cancelActiveRouteRequest()Z
    .locals 2

    .prologue
    .line 882
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCalcId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 883
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "no active route id"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 884
    const/4 v0, 0x0

    .line 886
    :goto_0
    return v0

    :cond_0
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCalcId:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->handleRouteCancelRequest(Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;Z)Z

    move-result v0

    goto :goto_0
.end method

.method private static cancelCurrentGeocodeRequest()V
    .locals 5

    .prologue
    .line 564
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereRouteManager;->lockObj:Ljava/lang/Object;

    monitor-enter v2

    .line 565
    :try_start_0
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteGeocodeRequest:Lcom/here/android/mpa/search/GeocodeRequest;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    .line 567
    :try_start_1
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteGeocodeRequest:Lcom/here/android/mpa/search/GeocodeRequest;

    invoke-virtual {v1}, Lcom/here/android/mpa/search/GeocodeRequest;->cancel()Z

    .line 568
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "geo: geocode request cancelled:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeGeoCalcId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 569
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeGeoRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    if-eqz v1, :cond_0

    .line 570
    sget-object v1, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_CANCELLED:Lcom/navdy/service/library/events/RequestStatus;

    sget-object v3, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeGeoRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    const/4 v4, 0x0

    invoke-static {v1, v3, v4}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->returnErrorResponse(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 575
    .local v0, "t":Ljava/lang/Throwable;
    :cond_0
    :goto_0
    const/4 v1, 0x0

    :try_start_2
    sput-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteGeocodeRequest:Lcom/here/android/mpa/search/GeocodeRequest;

    .line 576
    const/4 v1, 0x0

    sput-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeGeoCalcId:Ljava/lang/String;

    .line 577
    const/4 v1, 0x0

    sput-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeGeoRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .line 579
    :cond_1
    monitor-exit v2

    .line 580
    return-void

    .line 572
    .end local v0    # "t":Ljava/lang/Throwable;
    :catch_0
    move-exception v0

    .line 573
    .restart local v0    # "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 579
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public static clearActiveRouteCalc()V
    .locals 6

    .prologue
    .line 763
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereRouteManager;->lockObj:Ljava/lang/Object;

    monitor-enter v3

    .line 764
    :try_start_0
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "clearActiveRouteCalc"

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 765
    const/4 v2, 0x0

    sput-object v2, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCalculator:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    .line 766
    const/4 v2, 0x0

    sput-object v2, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCalcId:Ljava/lang/String;

    .line 767
    const/4 v2, 0x0

    sput-object v2, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .line 768
    const/4 v2, 0x0

    sput v2, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteProgress:I

    .line 769
    const/4 v2, 0x0

    sput-boolean v2, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCancelledByUser:Z

    .line 770
    const/4 v2, 0x0

    sput-object v2, Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeCalculationEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    .line 771
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereRouteManager;->pendingRouteRequestQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->size()I

    move-result v0

    .line 772
    .local v0, "len":I
    if-lez v0, :cond_0

    .line 773
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "pending route queue size:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 774
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereRouteManager;->pendingRouteRequestQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .line 775
    .local v1, "pendingRequest":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereRouteManager;->pendingRouteRequestIds:Ljava/util/HashSet;

    iget-object v4, v1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestId:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 776
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereRouteManager;->handler:Landroid/os/Handler;

    new-instance v4, Lcom/navdy/hud/app/maps/here/HereRouteManager$7;

    invoke-direct {v4, v1}, Lcom/navdy/hud/app/maps/here/HereRouteManager$7;-><init>(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V

    invoke-virtual {v2, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 784
    .end local v1    # "pendingRequest":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    :cond_0
    monitor-exit v3

    .line 785
    return-void

    .line 784
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public static clearActiveRouteCalc(Ljava/lang/String;)V
    .locals 2
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 755
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->lockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 756
    :try_start_0
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCalcId:Ljava/lang/String;

    invoke-static {p0, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 757
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->clearActiveRouteCalc()V

    .line 759
    :cond_0
    monitor-exit v1

    .line 760
    return-void

    .line 759
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getActiveRouteCalcId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 930
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCalcId:Ljava/lang/String;

    return-object v0
.end method

.method public static handleRouteCancelRequest(Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;Z)Z
    .locals 12
    .param p0, "routeCancelRequest"    # Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;
    .param p1, "originDisplay"    # Z

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 810
    :try_start_0
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "routeCancelRequest:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;->handle:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 811
    iget-object v6, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;->handle:Ljava/lang/String;

    if-nez v6, :cond_0

    .line 812
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "routeCancelRequest:null handle"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 877
    :goto_0
    return v4

    .line 815
    :cond_0
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereRouteManager;->lockObj:Ljava/lang/Object;

    monitor-enter v6
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 818
    const/4 v2, 0x0

    .line 820
    .local v2, "routeCalcCancellable":Z
    :try_start_1
    sget-object v7, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteGeocodeRequest:Lcom/here/android/mpa/search/GeocodeRequest;

    if-eqz v7, :cond_2

    .line 822
    iget-object v7, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;->handle:Ljava/lang/String;

    sget-object v8, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeGeoCalcId:Ljava/lang/String;

    invoke-static {v7, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 823
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->cancelCurrentGeocodeRequest()V

    .line 824
    sget-object v7, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "routeCancelRequest: sent navrouteresponse-geocode cancel ["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;->handle:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 825
    new-instance v7, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;

    invoke-direct {v7}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;-><init>()V

    iget-object v8, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;->handle:Ljava/lang/String;

    .line 826
    invoke-virtual {v7, v8}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->requestId(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;

    move-result-object v7

    sget-object v8, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_CANCELLED:Lcom/navdy/service/library/events/RequestStatus;

    .line 827
    invoke-virtual {v7, v8}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->status(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;

    move-result-object v7

    new-instance v8, Lcom/navdy/service/library/events/location/Coordinate$Builder;

    invoke-direct {v8}, Lcom/navdy/service/library/events/location/Coordinate$Builder;-><init>()V

    const-wide/16 v10, 0x0

    .line 828
    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->latitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v8

    const-wide/16 v10, 0x0

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->longitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v8

    invoke-virtual {v8}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->build()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->destination(Lcom/navdy/service/library/events/location/Coordinate;)Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;

    move-result-object v7

    .line 829
    invoke-virtual {v7}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->build()Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

    move-result-object v1

    .line 830
    .local v1, "response":Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;
    sget-object v7, Lcom/navdy/hud/app/maps/here/HereRouteManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v8, Lcom/navdy/hud/app/event/RemoteEvent;

    invoke-direct {v8, v1}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v7, v8}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 865
    if-nez v2, :cond_1

    .line 866
    :try_start_2
    sget-object v7, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "may be pending route exists: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;->handle:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 867
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    invoke-direct {v0}, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;-><init>()V

    .line 868
    .local v0, "event":Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;
    iget-object v7, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;->handle:Ljava/lang/String;

    iput-object v7, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->pendingNavigationRequestId:Ljava/lang/String;

    .line 869
    iput-boolean p1, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->abortOriginDisplay:Z

    .line 870
    sget-object v7, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;->ABORT_NAVIGATION:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    iput-object v7, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->state:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    .line 871
    sget-object v7, Lcom/navdy/hud/app/maps/here/HereRouteManager;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v7, v0}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 872
    .end local v0    # "event":Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;
    :cond_1
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v4, v5

    goto/16 :goto_0

    .line 835
    .end local v1    # "response":Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;
    :cond_2
    :try_start_3
    sget-object v7, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCalculator:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    if-nez v7, :cond_4

    .line 836
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "no active route calculation:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;->handle:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 865
    if-nez v2, :cond_3

    .line 866
    :try_start_4
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "may be pending route exists: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;->handle:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 867
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    invoke-direct {v0}, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;-><init>()V

    .line 868
    .restart local v0    # "event":Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;
    iget-object v5, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;->handle:Ljava/lang/String;

    iput-object v5, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->pendingNavigationRequestId:Ljava/lang/String;

    .line 869
    iput-boolean p1, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->abortOriginDisplay:Z

    .line 870
    sget-object v5, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;->ABORT_NAVIGATION:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    iput-object v5, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->state:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    .line 871
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereRouteManager;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v5, v0}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 872
    .end local v0    # "event":Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;
    :cond_3
    monitor-exit v6

    goto/16 :goto_0

    .line 874
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v5
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0

    .line 875
    .end local v2    # "routeCalcCancellable":Z
    :catch_0
    move-exception v3

    .line 876
    .local v3, "t":Ljava/lang/Throwable;
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v5, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 839
    .end local v3    # "t":Ljava/lang/Throwable;
    .restart local v2    # "routeCalcCancellable":Z
    :cond_4
    :try_start_6
    iget-object v7, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;->handle:Ljava/lang/String;

    sget-object v8, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCalcId:Ljava/lang/String;

    invoke-static {v7, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_6

    .line 840
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "invalid active route id:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;->handle:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 865
    if-nez v2, :cond_5

    .line 866
    :try_start_7
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "may be pending route exists: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;->handle:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 867
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    invoke-direct {v0}, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;-><init>()V

    .line 868
    .restart local v0    # "event":Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;
    iget-object v5, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;->handle:Ljava/lang/String;

    iput-object v5, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->pendingNavigationRequestId:Ljava/lang/String;

    .line 869
    iput-boolean p1, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->abortOriginDisplay:Z

    .line 870
    sget-object v5, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;->ABORT_NAVIGATION:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    iput-object v5, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->state:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    .line 871
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereRouteManager;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v5, v0}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 872
    .end local v0    # "event":Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;
    :cond_5
    monitor-exit v6
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_0

    .line 843
    :cond_6
    :try_start_8
    sget-boolean v7, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCancelledByUser:Z

    if-eqz v7, :cond_8

    .line 844
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "request already cancelled:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;->handle:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 865
    if-nez v2, :cond_7

    .line 866
    :try_start_9
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "may be pending route exists: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;->handle:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 867
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    invoke-direct {v0}, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;-><init>()V

    .line 868
    .restart local v0    # "event":Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;
    iget-object v5, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;->handle:Ljava/lang/String;

    iput-object v5, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->pendingNavigationRequestId:Ljava/lang/String;

    .line 869
    iput-boolean p1, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->abortOriginDisplay:Z

    .line 870
    sget-object v5, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;->ABORT_NAVIGATION:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    iput-object v5, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->state:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    .line 871
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereRouteManager;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v5, v0}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 872
    .end local v0    # "event":Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;
    :cond_7
    monitor-exit v6
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_0

    .line 847
    :cond_8
    if-eqz p1, :cond_9

    .line 848
    :try_start_a
    sget-object v7, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "send cancel to client"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 849
    sget-object v7, Lcom/navdy/hud/app/maps/here/HereRouteManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v8, Lcom/navdy/hud/app/event/RemoteEvent;

    invoke-direct {v8, p0}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v7, v8}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 851
    :cond_9
    const/4 v2, 0x1

    .line 852
    const/4 v7, 0x1

    sput-boolean v7, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCancelledByUser:Z

    .line 853
    sget-object v7, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCalculator:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    invoke-virtual {v7}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->cancel()V

    .line 854
    sget-object v7, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "called cancel"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 856
    sget-object v7, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "routeCancelRequest: sent navrouteresponse cancel ["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;->handle:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 857
    new-instance v7, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;

    invoke-direct {v7}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;-><init>()V

    iget-object v8, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;->handle:Ljava/lang/String;

    .line 858
    invoke-virtual {v7, v8}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->requestId(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;

    move-result-object v7

    sget-object v8, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_CANCELLED:Lcom/navdy/service/library/events/RequestStatus;

    .line 859
    invoke-virtual {v7, v8}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->status(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;

    move-result-object v7

    new-instance v8, Lcom/navdy/service/library/events/location/Coordinate$Builder;

    invoke-direct {v8}, Lcom/navdy/service/library/events/location/Coordinate$Builder;-><init>()V

    const-wide/16 v10, 0x0

    .line 860
    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->latitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v8

    const-wide/16 v10, 0x0

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->longitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v8

    invoke-virtual {v8}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->build()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->destination(Lcom/navdy/service/library/events/location/Coordinate;)Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;

    move-result-object v7

    .line 861
    invoke-virtual {v7}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->build()Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

    move-result-object v1

    .line 862
    .restart local v1    # "response":Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;
    sget-object v7, Lcom/navdy/hud/app/maps/here/HereRouteManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v8, Lcom/navdy/hud/app/event/RemoteEvent;

    invoke-direct {v8, v1}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v7, v8}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 865
    if-nez v2, :cond_a

    .line 866
    :try_start_b
    sget-object v7, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "may be pending route exists: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;->handle:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 867
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    invoke-direct {v0}, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;-><init>()V

    .line 868
    .restart local v0    # "event":Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;
    iget-object v7, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;->handle:Ljava/lang/String;

    iput-object v7, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->pendingNavigationRequestId:Ljava/lang/String;

    .line 869
    iput-boolean p1, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->abortOriginDisplay:Z

    .line 870
    sget-object v7, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;->ABORT_NAVIGATION:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    iput-object v7, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->state:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    .line 871
    sget-object v7, Lcom/navdy/hud/app/maps/here/HereRouteManager;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v7, v0}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 872
    .end local v0    # "event":Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;
    :cond_a
    monitor-exit v6

    move v4, v5

    goto/16 :goto_0

    .line 865
    .end local v1    # "response":Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;
    :catchall_1
    move-exception v5

    if-nez v2, :cond_b

    .line 866
    sget-object v7, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "may be pending route exists: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;->handle:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 867
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    invoke-direct {v0}, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;-><init>()V

    .line 868
    .restart local v0    # "event":Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;
    iget-object v7, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;->handle:Ljava/lang/String;

    iput-object v7, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->pendingNavigationRequestId:Ljava/lang/String;

    .line 869
    iput-boolean p1, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->abortOriginDisplay:Z

    .line 870
    sget-object v7, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;->ABORT_NAVIGATION:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    iput-object v7, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->state:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    .line 871
    sget-object v7, Lcom/navdy/hud/app/maps/here/HereRouteManager;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v7, v0}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 872
    .end local v0    # "event":Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;
    :cond_b
    throw v5
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0
.end method

.method public static handleRouteManeuverRequest(Lcom/navdy/service/library/events/navigation/RouteManeuverRequest;)V
    .locals 3
    .param p0, "routeManeuverRequest"    # Lcom/navdy/service/library/events/navigation/RouteManeuverRequest;

    .prologue
    .line 661
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereRouteManager$6;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/maps/here/HereRouteManager$6;-><init>(Lcom/navdy/service/library/events/navigation/RouteManeuverRequest;)V

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 730
    return-void
.end method

.method public static handleRouteRequest(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V
    .locals 3
    .param p0, "routeRequest"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .prologue
    .line 110
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereRouteManager$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/maps/here/HereRouteManager$1;-><init>(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V

    const/16 v2, 0x13

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 200
    return-void
.end method

.method private static handleRouteRequest(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/common/GeoCoordinate;)V
    .locals 30
    .param p0, "request"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .param p1, "startPoint"    # Lcom/here/android/mpa/common/GeoCoordinate;

    .prologue
    .line 203
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v18

    .line 205
    .local v18, "l1":J
    new-instance v22, Ljava/util/ArrayList;

    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V

    .line 207
    .local v22, "waypoints":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/common/GeoCoordinate;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->waypoints:Ljava/util/List;

    move-object/from16 v23, v0

    if-eqz v23, :cond_0

    .line 208
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->waypoints:Ljava/util/List;

    move-object/from16 v23, v0

    invoke-interface/range {v23 .. v23}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v23

    :goto_0
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v24

    if-eqz v24, :cond_0

    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/navdy/service/library/events/location/Coordinate;

    .line 209
    .local v17, "waypoint":Lcom/navdy/service/library/events/location/Coordinate;
    new-instance v24, Lcom/here/android/mpa/common/GeoCoordinate;

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v26

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v28

    move-object/from16 v0, v24

    move-wide/from16 v1, v26

    move-wide/from16 v3, v28

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 219
    .end local v17    # "waypoint":Lcom/navdy/service/library/events/location/Coordinate;
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationDisplay:Lcom/navdy/service/library/events/location/Coordinate;

    move-object/from16 v23, v0

    if-eqz v23, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationDisplay:Lcom/navdy/service/library/events/location/Coordinate;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    move-object/from16 v23, v0

    .line 220
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v24

    const-wide/16 v26, 0x0

    cmpl-double v23, v24, v26

    if-eqz v23, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationDisplay:Lcom/navdy/service/library/events/location/Coordinate;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v24

    const-wide/16 v26, 0x0

    cmpl-double v23, v24, v26

    if-eqz v23, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationDisplay:Lcom/navdy/service/library/events/location/Coordinate;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    move-object/from16 v24, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-eq v0, v1, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationDisplay:Lcom/navdy/service/library/events/location/Coordinate;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    move-object/from16 v24, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-eq v0, v1, :cond_2

    .line 223
    new-instance v13, Lcom/here/android/mpa/common/GeoCoordinate;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationDisplay:Lcom/navdy/service/library/events/location/Coordinate;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationDisplay:Lcom/navdy/service/library/events/location/Coordinate;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v26

    move-wide/from16 v0, v24

    move-wide/from16 v2, v26

    invoke-direct {v13, v0, v1, v2, v3}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    .line 224
    .local v13, "g1":Lcom/here/android/mpa/common/GeoCoordinate;
    new-instance v14, Lcom/here/android/mpa/common/GeoCoordinate;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v26

    move-wide/from16 v0, v24

    move-wide/from16 v2, v26

    invoke-direct {v14, v0, v1, v2, v3}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    .line 225
    .local v14, "g2":Lcom/here/android/mpa/common/GeoCoordinate;
    invoke-virtual {v13, v14}, Lcom/here/android/mpa/common/GeoCoordinate;->distanceTo(Lcom/here/android/mpa/common/GeoCoordinate;)D

    move-result-wide v24

    move-wide/from16 v0, v24

    double-to-long v8, v0

    .line 226
    .local v8, "d":J
    const-wide/32 v24, 0x274a6

    cmp-long v23, v8, v24

    if-ltz v23, :cond_1

    .line 227
    new-instance v15, Lcom/here/android/mpa/common/GeoCoordinate;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationDisplay:Lcom/navdy/service/library/events/location/Coordinate;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationDisplay:Lcom/navdy/service/library/events/location/Coordinate;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v26

    move-wide/from16 v0, v24

    move-wide/from16 v2, v26

    invoke-direct {v15, v0, v1, v2, v3}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    .line 228
    .local v15, "geoCheck":Lcom/here/android/mpa/common/GeoCoordinate;
    sget-object v23, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "display/nav distance check failed:"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 229
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationDisplay:Lcom/navdy/service/library/events/location/Coordinate;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ","

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationDisplay:Lcom/navdy/service/library/events/location/Coordinate;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, ","

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->streetAddress:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-static/range {v23 .. v25}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordBadRoutePosition(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    .end local v8    # "d":J
    .end local v13    # "g1":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v14    # "g2":Lcom/here/android/mpa/common/GeoCoordinate;
    :goto_1
    move-object v7, v15

    .line 241
    .local v7, "endPoint":Lcom/here/android/mpa/common/GeoCoordinate;
    sget-object v23, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "handleRouteRequest start="

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, ", end="

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 243
    const/16 v16, 0x0

    .line 245
    .local v16, "geoCode":Z
    sget-object v23, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->geoCodeStreetAddress:Ljava/lang/Boolean;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_3

    .line 247
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Lcom/here/android/mpa/common/GeoCoordinate;->distanceTo(Lcom/here/android/mpa/common/GeoCoordinate;)D

    move-result-wide v10

    .line 248
    .local v10, "distance":D
    const-wide v24, 0x416c9c3800000000L    # 1.5E7

    cmpl-double v23, v10, v24

    if-lez v23, :cond_5

    .line 249
    sget-object v23, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "distance between start and endpoint:"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, " max:"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-wide v26, 0x416c9c3800000000L    # 1.5E7

    move-object/from16 v0, v24

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 250
    sget-object v23, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_INVALID_REQUEST:Lcom/navdy/service/library/events/RequestStatus;

    sget-object v24, Lcom/navdy/hud/app/maps/here/HereRouteManager;->context:Landroid/content/Context;

    const v25, 0x7f09019f

    invoke-virtual/range {v24 .. v25}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    move-object/from16 v2, v24

    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->returnErrorResponse(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;)V

    .line 361
    .end local v10    # "distance":D
    :goto_2
    return-void

    .line 232
    .end local v7    # "endPoint":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v15    # "geoCheck":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v16    # "geoCode":Z
    .restart local v8    # "d":J
    .restart local v13    # "g1":Lcom/here/android/mpa/common/GeoCoordinate;
    .restart local v14    # "g2":Lcom/here/android/mpa/common/GeoCoordinate;
    :cond_1
    sget-object v23, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "display/nav distance check pass:"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 233
    new-instance v15, Lcom/here/android/mpa/common/GeoCoordinate;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v26

    move-wide/from16 v0, v24

    move-wide/from16 v2, v26

    invoke-direct {v15, v0, v1, v2, v3}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    .restart local v15    # "geoCheck":Lcom/here/android/mpa/common/GeoCoordinate;
    goto/16 :goto_1

    .line 236
    .end local v8    # "d":J
    .end local v13    # "g1":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v14    # "g2":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v15    # "geoCheck":Lcom/here/android/mpa/common/GeoCoordinate;
    :cond_2
    sget-object v23, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v24, "display/nav distance check pass: no display pos"

    invoke-virtual/range {v23 .. v24}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 237
    new-instance v15, Lcom/here/android/mpa/common/GeoCoordinate;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v26

    move-wide/from16 v0, v24

    move-wide/from16 v2, v26

    invoke-direct {v15, v0, v1, v2, v3}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    .restart local v15    # "geoCheck":Lcom/here/android/mpa/common/GeoCoordinate;
    goto/16 :goto_1

    .line 254
    .restart local v7    # "endPoint":Lcom/here/android/mpa/common/GeoCoordinate;
    .restart local v16    # "geoCode":Z
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->streetAddress:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-static/range {v23 .. v23}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_4

    .line 256
    sget-object v23, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_INVALID_REQUEST:Lcom/navdy/service/library/events/RequestStatus;

    sget-object v24, Lcom/navdy/hud/app/maps/here/HereRouteManager;->context:Landroid/content/Context;

    const v25, 0x7f0902d8

    invoke-virtual/range {v24 .. v25}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    move-object/from16 v2, v24

    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->returnErrorResponse(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 259
    :cond_4
    const/16 v16, 0x1

    .line 263
    :cond_5
    sget-object v23, Lcom/navdy/hud/app/maps/here/HereRouteManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-virtual/range {v23 .. v23}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isEngineOnline()Z

    move-result v23

    if-eqz v23, :cond_8

    .line 264
    const/4 v6, 0x1

    .line 269
    .local v6, "b":Z
    :goto_3
    move v12, v6

    .line 270
    .local v12, "factoringInTraffic":Z
    sget-object v23, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "maps engine online:"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 272
    if-eqz v16, :cond_c

    .line 274
    sget-object v23, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "Street Address:"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->streetAddress:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 277
    sget-object v24, Lcom/navdy/hud/app/maps/here/HereRouteManager;->lockObj:Ljava/lang/Object;

    monitor-enter v24

    .line 279
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->isRouteCalcInProgress()Z

    move-result v23

    if-eqz v23, :cond_6

    .line 280
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->cancelCurrent:Ljava/lang/Boolean;

    move-object/from16 v23, v0

    if-eqz v23, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->cancelCurrent:Ljava/lang/Boolean;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v23

    if-eqz v23, :cond_9

    .line 281
    sget-object v23, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCalculator:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    if-eqz v23, :cond_6

    .line 282
    sget-object v23, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "geo:route cancelling current request :"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    sget-object v26, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCalcId:Ljava/lang/String;

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 283
    sget-object v23, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCalculator:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    invoke-virtual/range {v23 .. v23}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->cancel()V

    .line 284
    const/16 v23, 0x0

    sput-object v23, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCalculator:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    .line 285
    sget-object v23, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_CANCELLED:Lcom/navdy/service/library/events/RequestStatus;

    sget-object v25, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    sget-object v26, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCalcId:Ljava/lang/String;

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->returnErrorResponse(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;)V

    .line 294
    :cond_6
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->isRouteCalcGeocodeRequestInProgress()Z

    move-result v23

    if-eqz v23, :cond_7

    .line 295
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->cancelCurrent:Ljava/lang/Boolean;

    move-object/from16 v23, v0

    if-eqz v23, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->cancelCurrent:Ljava/lang/Boolean;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v23

    if-eqz v23, :cond_a

    .line 296
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->cancelCurrentGeocodeRequest()V

    .line 303
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestId:Ljava/lang/String;

    move-object/from16 v23, v0

    sput-object v23, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeGeoCalcId:Ljava/lang/String;

    .line 304
    sput-object p0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeGeoRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .line 305
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->streetAddress:Ljava/lang/String;

    move-object/from16 v23, v0

    const v25, 0x186a0

    new-instance v26, Lcom/navdy/hud/app/maps/here/HereRouteManager$2;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, v22

    invoke-direct {v0, v1, v2, v3, v12}, Lcom/navdy/hud/app/maps/here/HereRouteManager$2;-><init>(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/common/GeoCoordinate;Ljava/util/List;Z)V

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    move/from16 v2, v25

    move-object/from16 v3, v26

    invoke-static {v0, v1, v2, v3}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->geoCodeStreetAddress(Lcom/here/android/mpa/common/GeoCoordinate;Ljava/lang/String;ILcom/navdy/hud/app/maps/here/HerePlacesManager$GeoCodeCallback;)Lcom/here/android/mpa/search/GeocodeRequest;

    move-result-object v23

    sput-object v23, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteGeocodeRequest:Lcom/here/android/mpa/search/GeocodeRequest;

    .line 347
    sget-object v23, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteGeocodeRequest:Lcom/here/android/mpa/search/GeocodeRequest;

    if-nez v23, :cond_b

    .line 348
    const/16 v23, 0x0

    sput-object v23, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteGeocodeRequest:Lcom/here/android/mpa/search/GeocodeRequest;

    .line 349
    const/16 v23, 0x0

    sput-object v23, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeGeoCalcId:Ljava/lang/String;

    .line 350
    sget-object v23, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v25, "geocode request failed"

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 354
    :goto_4
    monitor-exit v24
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 359
    :goto_5
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v20

    .line 360
    .local v20, "l2":J
    sget-object v23, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "handleRouteRequest- time-1 ="

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    sub-long v26, v20, v18

    move-object/from16 v0, v24

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 266
    .end local v6    # "b":Z
    .end local v12    # "factoringInTraffic":Z
    .end local v20    # "l2":J
    :cond_8
    const/4 v6, 0x0

    .restart local v6    # "b":Z
    goto/16 :goto_3

    .line 288
    .restart local v12    # "factoringInTraffic":Z
    :cond_9
    :try_start_1
    sget-object v23, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_ALREADY_IN_PROGRESS:Lcom/navdy/service/library/events/RequestStatus;

    sget-object v25, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCalcId:Ljava/lang/String;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    move-object/from16 v2, v25

    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->returnErrorResponse(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;)V

    .line 289
    monitor-exit v24

    goto/16 :goto_2

    .line 354
    :catchall_0
    move-exception v23

    monitor-exit v24
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v23

    .line 298
    :cond_a
    :try_start_2
    sget-object v23, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_ALREADY_IN_PROGRESS:Lcom/navdy/service/library/events/RequestStatus;

    sget-object v25, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeGeoCalcId:Ljava/lang/String;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    move-object/from16 v2, v25

    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->returnErrorResponse(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;)V

    .line 299
    monitor-exit v24

    goto/16 :goto_2

    .line 352
    :cond_b
    sget-object v23, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v25, "geocode request triggered"

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4

    .line 356
    :cond_c
    sget-object v23, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v24, "geocoding not set"

    invoke-virtual/range {v23 .. v24}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 357
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v22

    invoke-static {v0, v1, v2, v7, v12}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeSearch(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/common/GeoCoordinate;Ljava/util/List;Lcom/here/android/mpa/common/GeoCoordinate;Z)V

    goto :goto_5
.end method

.method private static isRouteCalcGeocodeRequestInProgress()Z
    .locals 4

    .prologue
    .line 744
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->lockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 745
    :try_start_0
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteGeocodeRequest:Lcom/here/android/mpa/search/GeocodeRequest;

    if-eqz v0, :cond_0

    .line 746
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "route calculation geocode already in progress:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCalcId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 747
    const/4 v0, 0x1

    monitor-exit v1

    .line 749
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    .line 751
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static isRouteCalcInProgress()Z
    .locals 4

    .prologue
    .line 733
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->lockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 734
    :try_start_0
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCalculator:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    if-eqz v0, :cond_0

    .line 735
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "route calculation already in progress:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCalcId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 736
    const/4 v0, 0x1

    monitor-exit v1

    .line 738
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    .line 740
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static isUIShowingRouteCalculation()Z
    .locals 3

    .prologue
    .line 921
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v1

    const-string v2, "navdy#route#calc#notif"

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getNotification(Ljava/lang/String;)Lcom/navdy/hud/app/framework/notifications/INotification;

    move-result-object v0

    .line 922
    .local v0, "notif":Lcom/navdy/hud/app/framework/notifications/INotification;
    if-eqz v0, :cond_0

    .line 923
    const/4 v1, 0x1

    .line 925
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static printRouteRequest(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V
    .locals 8
    .param p0, "request"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .prologue
    .line 891
    const/4 v3, 0x0

    .line 892
    .local v3, "routeAttributesStr":Ljava/lang/String;
    :try_start_0
    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->routeAttributes:Ljava/util/List;

    .line 893
    .local v2, "routeAttributes":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;>;"
    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_1

    .line 894
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 895
    .local v1, "builder":Ljava/lang/StringBuilder;
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;

    .line 896
    .local v0, "attribute":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;
    invoke-virtual {v0}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 897
    const-string v6, ","

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 912
    .end local v0    # "attribute":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;
    .end local v1    # "builder":Ljava/lang/StringBuilder;
    .end local v2    # "routeAttributes":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;>;"
    :catch_0
    move-exception v4

    .line 913
    .local v4, "t":Ljava/lang/Throwable;
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v5, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 915
    .end local v4    # "t":Ljava/lang/Throwable;
    :goto_1
    return-void

    .line 899
    .restart local v1    # "builder":Ljava/lang/StringBuilder;
    .restart local v2    # "routeAttributes":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;>;"
    :cond_0
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 901
    .end local v1    # "builder":Ljava/lang/StringBuilder;
    :cond_1
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "NavigationRouteRequest label["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->label:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] local["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->originDisplay:Ljava/lang/Boolean;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] streetAddress["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->streetAddress:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] destination_id["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination_identifier:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] autonav["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->autoNavigate:Ljava/lang/Boolean;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] geoCode["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->geoCodeStreetAddress:Ljava/lang/Boolean;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] dest_coordinate["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] favType["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationType:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] requestId["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestId:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] display_coordinate["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationDisplay:Lcom/navdy/service/library/events/location/Coordinate;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] route attrs["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method

.method private static returnErrorResponse(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;)V
    .locals 10
    .param p0, "status"    # Lcom/navdy/service/library/events/RequestStatus;
    .param p1, "request"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .param p2, "errorText"    # Ljava/lang/String;

    .prologue
    .line 583
    const/4 v8, 0x1

    .line 585
    .local v8, "clearRouteVars":Z
    :try_start_0
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 586
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

    iget-object v3, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iget-object v7, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestId:Ljava/lang/String;

    move-object v1, p0

    move-object v2, p2

    invoke-direct/range {v0 .. v7}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;-><init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 587
    .local v0, "response":Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;
    invoke-static {}, Lcom/navdy/hud/app/maps/MapsEventHandler;->getInstance()Lcom/navdy/hud/app/maps/MapsEventHandler;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/maps/MapsEventHandler;->sendEventToClient(Lcom/squareup/wire/Message;)V

    .line 588
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereRouteManager;->lockObj:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 589
    :try_start_1
    sget-object v1, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_ALREADY_IN_PROGRESS:Lcom/navdy/service/library/events/RequestStatus;

    if-ne p0, v1, :cond_1

    .line 591
    const/4 v8, 0x0

    .line 592
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 604
    if-eqz v8, :cond_0

    .line 605
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->clearActiveRouteCalc()V

    .line 608
    .end local v0    # "response":Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;
    :cond_0
    :goto_0
    return-void

    .line 595
    .restart local v0    # "response":Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;
    :cond_1
    :try_start_2
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeCalculationEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    if-eqz v1, :cond_2

    .line 596
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeCalculationEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    iput-object v0, v1, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->response:Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

    .line 597
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeCalculationEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    sget-object v3, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;->STOPPED:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    iput-object v3, v1, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->state:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    .line 598
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->bus:Lcom/squareup/otto/Bus;

    sget-object v3, Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeCalculationEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    invoke-virtual {v1, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 600
    :cond_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 604
    if-eqz v8, :cond_0

    .line 605
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->clearActiveRouteCalc()V

    goto :goto_0

    .line 600
    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 601
    .end local v0    # "response":Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;
    :catch_0
    move-exception v9

    .line 602
    .local v9, "t":Ljava/lang/Throwable;
    :try_start_5
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v9}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 604
    if-eqz v8, :cond_0

    .line 605
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->clearActiveRouteCalc()V

    goto :goto_0

    .line 604
    .end local v9    # "t":Ljava/lang/Throwable;
    :catchall_1
    move-exception v1

    if-eqz v8, :cond_3

    .line 605
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->clearActiveRouteCalc()V

    :cond_3
    throw v1
.end method

.method private static returnStatusResponse(Ljava/lang/String;ILjava/lang/String;)V
    .locals 4
    .param p0, "handle"    # Ljava/lang/String;
    .param p1, "progress"    # I
    .param p2, "requestId"    # Ljava/lang/String;

    .prologue
    .line 612
    :try_start_0
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handle["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] progress ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 613
    invoke-static {}, Lcom/navdy/hud/app/maps/MapsEventHandler;->getInstance()Lcom/navdy/hud/app/maps/MapsEventHandler;

    move-result-object v1

    new-instance v2, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v2, p0, v3, p2}, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/maps/MapsEventHandler;->sendEventToClient(Lcom/squareup/wire/Message;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 617
    :goto_0
    return-void

    .line 614
    :catch_0
    move-exception v0

    .line 615
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static returnSuccessResponse(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/util/ArrayList;Z)V
    .locals 12
    .param p0, "request"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .param p2, "factoringInTraffic"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteResult;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .local p1, "outgoingResults":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/navigation/NavigationRouteResult;>;"
    const/4 v11, 0x0

    .line 621
    :try_start_0
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

    sget-object v1, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    const-string v2, ""

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v4, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->label:Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestId:Ljava/lang/String;

    move-object v5, p1

    invoke-direct/range {v0 .. v7}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;-><init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 623
    .local v0, "navigationRouteResponse":Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;
    if-nez p2, :cond_0

    .line 624
    invoke-static {}, Lcom/navdy/hud/app/framework/voice/TTSUtils;->debugShowNoTrafficToast()V

    .line 627
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/maps/MapsEventHandler;->getInstance()Lcom/navdy/hud/app/maps/MapsEventHandler;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/maps/MapsEventHandler;->sendEventToClient(Lcom/squareup/wire/Message;)V

    .line 628
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereRouteManager;->lockObj:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 629
    :try_start_1
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeCalculationEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    if-eqz v1, :cond_1

    .line 630
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeCalculationEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    iput-object v0, v1, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->response:Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

    .line 631
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeCalculationEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    sget-object v3, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;->STOPPED:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    iput-object v3, v1, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->state:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    .line 632
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->bus:Lcom/squareup/otto/Bus;

    sget-object v3, Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeCalculationEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    invoke-virtual {v1, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 634
    :cond_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 635
    :try_start_2
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "sent route response"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 637
    iget-object v1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->autoNavigate:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->autoNavigate:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 638
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "start auto-navigation to route "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->label:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 640
    new-instance v1, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;

    invoke-direct {v1}, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;-><init>()V

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_STARTED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 641
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;->newState(Lcom/navdy/service/library/events/navigation/NavigationSessionState;)Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->label:Ljava/lang/String;

    .line 642
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;->label(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;

    move-result-object v2

    const/4 v1, 0x0

    .line 643
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    iget-object v1, v1, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeId:Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;->routeId(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;

    move-result-object v1

    const/4 v2, 0x0

    .line 644
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;->simulationSpeed(Ljava/lang/Integer;)Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;

    move-result-object v2

    iget-object v1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->originDisplay:Ljava/lang/Boolean;

    if-nez v1, :cond_3

    move v1, v11

    .line 645
    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;->originDisplay(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;

    move-result-object v9

    .line 647
    .local v9, "navigationSessionRequestBuilder":Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;
    invoke-virtual {v9}, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;->build()Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;

    move-result-object v8

    .line 648
    .local v8, "navigationSessionRequest":Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v1, v8}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 649
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v2, Lcom/navdy/hud/app/event/RemoteEvent;

    invoke-direct {v2, v8}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 652
    .end local v8    # "navigationSessionRequest":Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;
    .end local v9    # "navigationSessionRequestBuilder":Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;
    :cond_2
    invoke-static {}, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->getInstance()Lcom/navdy/hud/app/analytics/NavigationQualityTracker;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->trackCalculationWithTraffic(Z)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 656
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->clearActiveRouteCalc()V

    .line 658
    .end local v0    # "navigationRouteResponse":Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;
    :goto_1
    return-void

    .line 634
    .restart local v0    # "navigationRouteResponse":Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;
    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 653
    .end local v0    # "navigationRouteResponse":Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;
    :catch_0
    move-exception v10

    .line 654
    .local v10, "t":Ljava/lang/Throwable;
    :try_start_5
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v10}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 656
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->clearActiveRouteCalc()V

    goto :goto_1

    .line 644
    .end local v10    # "t":Ljava/lang/Throwable;
    .restart local v0    # "navigationRouteResponse":Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;
    :cond_3
    :try_start_6
    iget-object v1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->originDisplay:Ljava/lang/Boolean;

    .line 645
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result v1

    goto :goto_0

    .line 656
    .end local v0    # "navigationRouteResponse":Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;
    :catchall_1
    move-exception v1

    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->clearActiveRouteCalc()V

    throw v1
.end method

.method private static routeSearch(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/common/GeoCoordinate;Ljava/util/List;Lcom/here/android/mpa/common/GeoCoordinate;Z)V
    .locals 4
    .param p0, "request"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .param p1, "startPoint"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p3, "endPoint"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p4, "factoringInTraffic"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;",
            "Lcom/here/android/mpa/common/GeoCoordinate;",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/common/GeoCoordinate;",
            ">;",
            "Lcom/here/android/mpa/common/GeoCoordinate;",
            "Z)V"
        }
    .end annotation

    .prologue
    .local p2, "waypoints":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/common/GeoCoordinate;>;"
    const/4 v2, 0x1

    .line 368
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->lockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 370
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->isRouteCalcGeocodeRequestInProgress()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 371
    iget-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->cancelCurrent:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->cancelCurrent:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 372
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->cancelCurrentGeocodeRequest()V

    .line 379
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->isRouteCalcInProgress()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 380
    iget-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->cancelCurrent:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->cancelCurrent:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 381
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->pendingRouteRequestQueue:Ljava/util/Queue;

    invoke-interface {v0, p0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 382
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->pendingRouteRequestIds:Ljava/util/HashSet;

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 383
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "add to pending Queue"

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 384
    sget-boolean v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCancelledByUser:Z

    if-eqz v0, :cond_2

    .line 385
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "cancellation in progress"

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 386
    monitor-exit v1

    .line 402
    :goto_0
    return-void

    .line 374
    :cond_1
    sget-object v0, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_ALREADY_IN_PROGRESS:Lcom/navdy/service/library/events/RequestStatus;

    sget-object v2, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeGeoCalcId:Ljava/lang/String;

    invoke-static {v0, p0, v2}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->returnErrorResponse(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;)V

    .line 375
    monitor-exit v1

    goto :goto_0

    .line 396
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 388
    :cond_2
    :try_start_1
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cancelling current request :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCalcId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 389
    const/4 v0, 0x1

    sput-boolean v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCancelledByUser:Z

    .line 390
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCalculator:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->cancel()V

    .line 394
    :goto_1
    monitor-exit v1

    goto :goto_0

    .line 392
    :cond_3
    sget-object v0, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_ALREADY_IN_PROGRESS:Lcom/navdy/service/library/events/RequestStatus;

    sget-object v2, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCalcId:Ljava/lang/String;

    invoke-static {v0, p0, v2}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->returnErrorResponse(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;)V

    goto :goto_1

    .line 396
    :cond_4
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 397
    if-eqz p4, :cond_5

    .line 398
    invoke-static {p0, p1, p2, p3, v2}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeSearchTraffic(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/common/GeoCoordinate;Ljava/util/List;Lcom/here/android/mpa/common/GeoCoordinate;Z)V

    goto :goto_0

    .line 400
    :cond_5
    invoke-static {p0, p1, p2, p3, v2}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeSearchNoTraffic(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/common/GeoCoordinate;Ljava/util/List;Lcom/here/android/mpa/common/GeoCoordinate;Z)V

    goto :goto_0
.end method

.method private static routeSearchNoTraffic(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/common/GeoCoordinate;Ljava/util/List;Lcom/here/android/mpa/common/GeoCoordinate;Z)V
    .locals 10
    .param p0, "request"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .param p1, "startPoint"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p3, "endPoint"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p4, "newRequest"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;",
            "Lcom/here/android/mpa/common/GeoCoordinate;",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/common/GeoCoordinate;",
            ">;",
            "Lcom/here/android/mpa/common/GeoCoordinate;",
            "Z)V"
        }
    .end annotation

    .prologue
    .local p2, "waypoints":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/common/GeoCoordinate;>;"
    const/4 v5, 0x0

    .line 495
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;-><init>(Lcom/navdy/service/library/log/Logger;Z)V

    .line 496
    .local v0, "noTraffic":Lcom/navdy/hud/app/maps/here/HereRouteCalculator;
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getRouteOptions()Lcom/here/android/mpa/routing/RouteOptions;

    move-result-object v8

    .line 497
    .local v8, "routeOptions":Lcom/here/android/mpa/routing/RouteOptions;
    if-eqz p4, :cond_0

    .line 498
    iget-object v1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestId:Ljava/lang/String;

    sput-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCalcId:Ljava/lang/String;

    .line 499
    sput-object p0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .line 500
    sput v5, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteProgress:I

    .line 501
    sput-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCalculator:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    .line 502
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCalcId:Ljava/lang/String;

    sget v2, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteProgress:I

    sget-object v3, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v3, v3, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestId:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->returnStatusResponse(Ljava/lang/String;ILjava/lang/String;)V

    .line 504
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereRouteManager;->lockObj:Ljava/lang/Object;

    monitor-enter v2

    .line 505
    :try_start_0
    new-instance v1, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    invoke-direct {v1}, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;-><init>()V

    sput-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeCalculationEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    .line 506
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeCalculationEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    sget-object v3, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;->STARTED:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    iput-object v3, v1, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->state:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    .line 507
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeCalculationEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    iput-object p1, v1, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->start:Ljava/lang/Object;

    .line 508
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeCalculationEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    iput-object p2, v1, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->waypoints:Ljava/util/List;

    .line 509
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeCalculationEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    iput-object p3, v1, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->end:Ljava/lang/Object;

    .line 510
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeCalculationEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    iput-object p0, v1, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .line 511
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeCalculationEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    iput-object v8, v1, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->routeOptions:Lcom/here/android/mpa/routing/RouteOptions;

    .line 512
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->bus:Lcom/squareup/otto/Bus;

    sget-object v3, Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeCalculationEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    invoke-virtual {v1, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 513
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 522
    :goto_0
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "no-traffic route calc started"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 523
    new-instance v6, Lcom/navdy/hud/app/maps/here/HereRouteManager$5;

    invoke-direct {v6, p0}, Lcom/navdy/hud/app/maps/here/HereRouteManager$5;-><init>(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V

    sget v7, Lcom/navdy/hud/app/maps/here/HereRouteManager;->MAX_ROUTES:I

    const/4 v9, 0x1

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v9}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->calculateRoute(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/common/GeoCoordinate;Ljava/util/List;Lcom/here/android/mpa/common/GeoCoordinate;ZLcom/navdy/hud/app/maps/here/HereRouteCalculator$RouteCalculatorListener;ILcom/here/android/mpa/routing/RouteOptions;Z)V

    .line 561
    return-void

    .line 513
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 515
    :cond_0
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereRouteManager;->lockObj:Ljava/lang/Object;

    monitor-enter v2

    .line 516
    :try_start_2
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCalculator:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    if-eqz v1, :cond_1

    .line 517
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "replaced active route calc with no traffic"

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 518
    sput-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCalculator:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    .line 520
    :cond_1
    monitor-exit v2

    goto :goto_0

    :catchall_1
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v1
.end method

.method private static routeSearchTraffic(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/common/GeoCoordinate;Ljava/util/List;Lcom/here/android/mpa/common/GeoCoordinate;Z)V
    .locals 16
    .param p0, "request"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .param p1, "startPoint"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p3, "endPoint"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p4, "inProgressRouteCheck"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;",
            "Lcom/here/android/mpa/common/GeoCoordinate;",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/common/GeoCoordinate;",
            ">;",
            "Lcom/here/android/mpa/common/GeoCoordinate;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 411
    .local p2, "waypoints":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/common/GeoCoordinate;>;"
    new-instance v15, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    sget-object v2, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v3, 0x0

    invoke-direct {v15, v2, v3}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;-><init>(Lcom/navdy/service/library/log/Logger;Z)V

    .line 413
    .local v15, "traffic":Lcom/navdy/hud/app/maps/here/HereRouteCalculator;
    if-eqz p4, :cond_0

    .line 414
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereRouteManager;->lockObj:Ljava/lang/Object;

    monitor-enter v3

    .line 415
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestId:Ljava/lang/String;

    sput-object v2, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCalcId:Ljava/lang/String;

    .line 416
    sput-object p0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .line 417
    const/4 v2, 0x0

    sput v2, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteProgress:I

    .line 418
    sput-object v15, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCalculator:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    .line 419
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCalcId:Ljava/lang/String;

    sget v5, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteProgress:I

    sget-object v6, Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v6, v6, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestId:Ljava/lang/String;

    invoke-static {v2, v5, v6}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->returnStatusResponse(Ljava/lang/String;ILjava/lang/String;)V

    .line 420
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 422
    :cond_0
    new-instance v4, Lcom/navdy/hud/app/maps/here/HereRouteManager$3;

    invoke-direct {v4, v15}, Lcom/navdy/hud/app/maps/here/HereRouteManager$3;-><init>(Lcom/navdy/hud/app/maps/here/HereRouteCalculator;)V

    .line 430
    .local v4, "runnable":Ljava/lang/Runnable;
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "traffic route calc started"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 431
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereRouteManager;->handler:Landroid/os/Handler;

    sget v3, Lcom/navdy/hud/app/maps/here/HereRouteManager;->MAX_ONLINE_ROUTE_CALCULATION_TIME:I

    int-to-long v6, v3

    invoke-virtual {v2, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 433
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereRouteManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getRouteOptions()Lcom/here/android/mpa/routing/RouteOptions;

    move-result-object v13

    .line 435
    .local v13, "routeOptions":Lcom/here/android/mpa/routing/RouteOptions;
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereRouteManager;->lockObj:Ljava/lang/Object;

    monitor-enter v3

    .line 436
    :try_start_1
    new-instance v2, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    invoke-direct {v2}, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;-><init>()V

    sput-object v2, Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeCalculationEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    .line 437
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeCalculationEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    sget-object v5, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;->STARTED:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    iput-object v5, v2, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->state:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    .line 438
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeCalculationEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    move-object/from16 v0, p1

    iput-object v0, v2, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->start:Ljava/lang/Object;

    .line 439
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeCalculationEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    move-object/from16 v0, p2

    iput-object v0, v2, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->waypoints:Ljava/util/List;

    .line 440
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeCalculationEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    move-object/from16 v0, p3

    iput-object v0, v2, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->end:Ljava/lang/Object;

    .line 441
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeCalculationEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    move-object/from16 v0, p0

    iput-object v0, v2, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .line 442
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeCalculationEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    iput-object v13, v2, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->routeOptions:Lcom/here/android/mpa/routing/RouteOptions;

    .line 443
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereRouteManager;->bus:Lcom/squareup/otto/Bus;

    sget-object v5, Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeCalculationEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    invoke-virtual {v2, v5}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 444
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 446
    const/4 v10, 0x1

    new-instance v2, Lcom/navdy/hud/app/maps/here/HereRouteManager$4;

    move-object/from16 v3, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    invoke-direct/range {v2 .. v7}, Lcom/navdy/hud/app/maps/here/HereRouteManager$4;-><init>(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/Runnable;Lcom/here/android/mpa/common/GeoCoordinate;Ljava/util/List;Lcom/here/android/mpa/common/GeoCoordinate;)V

    sget v12, Lcom/navdy/hud/app/maps/here/HereRouteManager;->MAX_ROUTES:I

    const/4 v14, 0x1

    move-object v5, v15

    move-object/from16 v6, p0

    move-object/from16 v7, p1

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    move-object v11, v2

    invoke-virtual/range {v5 .. v14}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->calculateRoute(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/common/GeoCoordinate;Ljava/util/List;Lcom/here/android/mpa/common/GeoCoordinate;ZLcom/navdy/hud/app/maps/here/HereRouteCalculator$RouteCalculatorListener;ILcom/here/android/mpa/routing/RouteOptions;Z)V

    .line 488
    return-void

    .line 420
    .end local v4    # "runnable":Ljava/lang/Runnable;
    .end local v13    # "routeOptions":Lcom/here/android/mpa/routing/RouteOptions;
    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 444
    .restart local v4    # "runnable":Ljava/lang/Runnable;
    .restart local v13    # "routeOptions":Lcom/here/android/mpa/routing/RouteOptions;
    :catchall_1
    move-exception v2

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v2
.end method

.method public static startNavigationLookup(Lcom/navdy/hud/app/framework/destinations/Destination;)V
    .locals 3
    .param p0, "lookupDestination"    # Lcom/navdy/hud/app/framework/destinations/Destination;

    .prologue
    .line 934
    if-nez p0, :cond_0

    .line 935
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "startNavigationLookup null destination"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 946
    :goto_0
    return-void

    .line 938
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteManager;->lockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 939
    :try_start_0
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    invoke-direct {v0}, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeCalculationEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    .line 940
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeCalculationEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    sget-object v2, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;->FINDING_NAV_COORDINATES:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    iput-object v2, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->state:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    .line 941
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeCalculationEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    iput-object p0, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->lookupDestination:Lcom/navdy/hud/app/framework/destinations/Destination;

    .line 942
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeCalculationEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getRouteOptions()Lcom/here/android/mpa/routing/RouteOptions;

    move-result-object v2

    iput-object v2, v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;->routeOptions:Lcom/here/android/mpa/routing/RouteOptions;

    .line 943
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "startNavigationLookup sent nav_coordinate_lookup "

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 944
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereRouteManager;->bus:Lcom/squareup/otto/Bus;

    sget-object v2, Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeCalculationEvent:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;

    invoke-virtual {v0, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 945
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
