.class Lcom/navdy/hud/app/maps/here/HereMapsManager$3$2;
.super Ljava/lang/Object;
.source "HereMapsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereMapsManager$3;->onPositionFixChanged(Lcom/here/android/mpa/common/PositioningManager$LocationMethod;Lcom/here/android/mpa/common/PositioningManager$LocationStatus;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$3;

.field final synthetic val$locationMethod:Lcom/here/android/mpa/common/PositioningManager$LocationMethod;

.field final synthetic val$locationStatus:Lcom/here/android/mpa/common/PositioningManager$LocationStatus;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereMapsManager$3;Lcom/here/android/mpa/common/PositioningManager$LocationMethod;Lcom/here/android/mpa/common/PositioningManager$LocationStatus;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/maps/here/HereMapsManager$3;

    .prologue
    .line 577
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$2;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$3;

    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$2;->val$locationMethod:Lcom/here/android/mpa/common/PositioningManager$LocationMethod;

    iput-object p3, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$2;->val$locationStatus:Lcom/here/android/mpa/common/PositioningManager$LocationStatus;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 581
    :try_start_0
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 582
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onPositionFixChanged: method:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$2;->val$locationMethod:Lcom/here/android/mpa/common/PositioningManager$LocationMethod;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " status: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$2;->val$locationStatus:Lcom/here/android/mpa/common/PositioningManager$LocationStatus;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 585
    :cond_0
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$2;->val$locationMethod:Lcom/here/android/mpa/common/PositioningManager$LocationMethod;

    sget-object v4, Lcom/here/android/mpa/common/PositioningManager$LocationMethod;->GPS:Lcom/here/android/mpa/common/PositioningManager$LocationMethod;

    if-ne v3, v4, :cond_1

    .line 586
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$2;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$3;

    iget-object v3, v3, Lcom/navdy/hud/app/maps/here/HereMapsManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->positioningManager:Lcom/here/android/mpa/common/PositioningManager;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$900(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Lcom/here/android/mpa/common/PositioningManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/here/android/mpa/common/PositioningManager;->getRoadElement()Lcom/here/android/mpa/common/RoadElement;

    move-result-object v0

    .line 587
    .local v0, "roadElement":Lcom/here/android/mpa/common/RoadElement;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->isInTunnel(Lcom/here/android/mpa/common/RoadElement;)Z

    move-result v2

    .line 588
    .local v2, "tunnel":Z
    if-eqz v2, :cond_1

    .line 590
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$2;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$3;

    iget-object v3, v3, Lcom/navdy/hud/app/maps/here/HereMapsManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->extrapolationOn:Z
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$2800(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 591
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "TUNNEL extrapolation on"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 592
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$2;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$3;

    iget-object v3, v3, Lcom/navdy/hud/app/maps/here/HereMapsManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    const/4 v4, 0x1

    # setter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->extrapolationOn:Z
    invoke-static {v3, v4}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$2802(Lcom/navdy/hud/app/maps/here/HereMapsManager;Z)Z

    .line 593
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$2;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$3;

    iget-object v3, v3, Lcom/navdy/hud/app/maps/here/HereMapsManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # invokes: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sendExtrapolationEvent()V
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$2900(Lcom/navdy/hud/app/maps/here/HereMapsManager;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 600
    .end local v0    # "roadElement":Lcom/here/android/mpa/common/RoadElement;
    .end local v2    # "tunnel":Z
    :cond_1
    :goto_0
    return-void

    .line 597
    :catch_0
    move-exception v1

    .line 598
    .local v1, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
