.class Lcom/navdy/hud/app/maps/here/HerePlacesManager$8$1;
.super Ljava/lang/Object;
.source "HerePlacesManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HerePlacesManager$8;->onCompleted(Lcom/here/android/mpa/search/Place;Lcom/here/android/mpa/search/ErrorCode;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HerePlacesManager$8;

.field final synthetic val$error:Lcom/here/android/mpa/search/ErrorCode;

.field final synthetic val$place:Lcom/here/android/mpa/search/Place;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HerePlacesManager$8;Lcom/here/android/mpa/search/ErrorCode;Lcom/here/android/mpa/search/Place;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HerePlacesManager$8;

    .prologue
    .line 520
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$8$1;->this$0:Lcom/navdy/hud/app/maps/here/HerePlacesManager$8;

    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$8$1;->val$error:Lcom/here/android/mpa/search/ErrorCode;

    iput-object p3, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$8$1;->val$place:Lcom/here/android/mpa/search/Place;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 524
    :try_start_0
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$8$1;->this$0:Lcom/navdy/hud/app/maps/here/HerePlacesManager$8;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/HerePlacesManager$8;->val$counter:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v2

    .line 526
    .local v2, "left":I
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$8$1;->val$error:Lcom/here/android/mpa/search/ErrorCode;

    sget-object v5, Lcom/here/android/mpa/search/ErrorCode;->NONE:Lcom/here/android/mpa/search/ErrorCode;

    if-ne v4, v5, :cond_2

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$8$1;->val$place:Lcom/here/android/mpa/search/Place;

    if-eqz v4, :cond_2

    .line 528
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Found a Place: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$8$1;->val$place:Lcom/here/android/mpa/search/Place;

    invoke-virtual {v6}, Lcom/here/android/mpa/search/Place;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 529
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$8$1;->val$place:Lcom/here/android/mpa/search/Place;

    invoke-virtual {v4}, Lcom/here/android/mpa/search/Place;->getCategories()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/here/android/mpa/search/Category;

    .line 530
    .local v0, "category":Lcom/here/android/mpa/search/Category;
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "place category: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/here/android/mpa/search/Category;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 542
    .end local v0    # "category":Lcom/here/android/mpa/search/Category;
    .end local v2    # "left":I
    :catch_0
    move-exception v3

    .line 543
    .local v3, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "HERE internal PlaceLink.getDetailsRequest callback exception: "

    invoke-virtual {v4, v5, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 544
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$8$1;->this$0:Lcom/navdy/hud/app/maps/here/HerePlacesManager$8;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/HerePlacesManager$8;->val$counter:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    .line 545
    .local v1, "counterLeft":I
    if-nez v1, :cond_0

    .line 546
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$8$1;->this$0:Lcom/navdy/hud/app/maps/here/HerePlacesManager$8;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/HerePlacesManager$8;->val$places:Ljava/util/List;

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$8$1;->this$0:Lcom/navdy/hud/app/maps/here/HerePlacesManager$8;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/here/HerePlacesManager$8;->val$listener:Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;

    # invokes: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->invokeCategorySearchListener(Ljava/util/List;Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;)V
    invoke-static {v4, v5}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$1200(Ljava/util/List;Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;)V

    .line 549
    .end local v1    # "counterLeft":I
    .end local v3    # "t":Ljava/lang/Throwable;
    :cond_0
    :goto_1
    return-void

    .line 532
    .restart local v2    # "left":I
    :cond_1
    :try_start_1
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$8$1;->this$0:Lcom/navdy/hud/app/maps/here/HerePlacesManager$8;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/HerePlacesManager$8;->val$places:Ljava/util/List;

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$8$1;->val$place:Lcom/here/android/mpa/search/Place;

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 538
    :goto_2
    if-nez v2, :cond_0

    .line 540
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$8$1;->this$0:Lcom/navdy/hud/app/maps/here/HerePlacesManager$8;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/HerePlacesManager$8;->val$places:Ljava/util/List;

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$8$1;->this$0:Lcom/navdy/hud/app/maps/here/HerePlacesManager$8;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/here/HerePlacesManager$8;->val$listener:Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;

    # invokes: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->invokeCategorySearchListener(Ljava/util/List;Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;)V
    invoke-static {v4, v5}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$1200(Ljava/util/List;Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;)V

    goto :goto_1

    .line 535
    :cond_2
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error in place detail response: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$8$1;->val$error:Lcom/here/android/mpa/search/ErrorCode;

    invoke-virtual {v6}, Lcom/here/android/mpa/search/ErrorCode;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method
