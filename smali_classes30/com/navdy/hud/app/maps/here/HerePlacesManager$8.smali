.class final Lcom/navdy/hud/app/maps/here/HerePlacesManager$8;
.super Ljava/lang/Object;
.source "HerePlacesManager.java"

# interfaces
.implements Lcom/here/android/mpa/search/ResultListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HerePlacesManager;->handlePlaceLinksRequest(Ljava/util/List;Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/here/android/mpa/search/ResultListener",
        "<",
        "Lcom/here/android/mpa/search/Place;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$counter:Ljava/util/concurrent/atomic/AtomicInteger;

.field final synthetic val$listener:Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;

.field final synthetic val$places:Ljava/util/List;


# direct methods
.method constructor <init>(Ljava/util/concurrent/atomic/AtomicInteger;Ljava/util/List;Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;)V
    .locals 0

    .prologue
    .line 517
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$8;->val$counter:Ljava/util/concurrent/atomic/AtomicInteger;

    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$8;->val$places:Ljava/util/List;

    iput-object p3, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$8;->val$listener:Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted(Lcom/here/android/mpa/search/Place;Lcom/here/android/mpa/search/ErrorCode;)V
    .locals 3
    .param p1, "place"    # Lcom/here/android/mpa/search/Place;
    .param p2, "error"    # Lcom/here/android/mpa/search/ErrorCode;

    .prologue
    .line 520
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HerePlacesManager$8$1;

    invoke-direct {v1, p0, p2, p1}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$8$1;-><init>(Lcom/navdy/hud/app/maps/here/HerePlacesManager$8;Lcom/here/android/mpa/search/ErrorCode;Lcom/here/android/mpa/search/Place;)V

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 551
    return-void
.end method

.method public bridge synthetic onCompleted(Ljava/lang/Object;Lcom/here/android/mpa/search/ErrorCode;)V
    .locals 0

    .prologue
    .line 517
    check-cast p1, Lcom/here/android/mpa/search/Place;

    invoke-virtual {p0, p1, p2}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$8;->onCompleted(Lcom/here/android/mpa/search/Place;Lcom/here/android/mpa/search/ErrorCode;)V

    return-void
.end method
