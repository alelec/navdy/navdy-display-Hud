.class Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$1;
.super Ljava/lang/Object;
.source "HereMapImageGenerator.java"

# interfaces
.implements Lcom/here/android/mpa/common/OnScreenCaptureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->generateMapSnapshotInternal(Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$MapGeneratorParams;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;

.field final synthetic val$mapGeneratorParams:Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$MapGeneratorParams;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$MapGeneratorParams;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;

    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$1;->val$mapGeneratorParams:Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$MapGeneratorParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScreenCaptured(Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 118
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;

    # invokes: Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->stopOffscreenRenderer()V
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->access$000(Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;)V

    .line 119
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onScreenCaptured bitmap:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 120
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$1;->val$mapGeneratorParams:Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$MapGeneratorParams;

    # invokes: Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->saveFileToDisk(Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$MapGeneratorParams;Landroid/graphics/Bitmap;)V
    invoke-static {v1, v2, p1}, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->access$200(Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$MapGeneratorParams;Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 124
    :goto_0
    return-void

    .line 121
    :catch_0
    move-exception v0

    .line 122
    .local v0, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "onScreenCaptured"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
