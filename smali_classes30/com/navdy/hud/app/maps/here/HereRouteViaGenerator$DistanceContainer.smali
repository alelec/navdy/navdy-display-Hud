.class Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;
.super Ljava/lang/Object;
.source "HereRouteViaGenerator.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DistanceContainer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;",
        ">;"
    }
.end annotation


# instance fields
.field index:I

.field order:J

.field val:D

.field via:Ljava/lang/String;


# direct methods
.method constructor <init>(DIJLjava/lang/String;)V
    .locals 0
    .param p1, "val"    # D
    .param p3, "index"    # I
    .param p4, "order"    # J
    .param p6, "via"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-wide p1, p0, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;->val:D

    .line 35
    iput p3, p0, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;->index:I

    .line 36
    iput-wide p4, p0, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;->order:J

    .line 37
    iput-object p6, p0, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;->via:Ljava/lang/String;

    .line 38
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;)I
    .locals 6
    .param p1, "another"    # Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;

    .prologue
    .line 48
    iget v1, p0, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;->index:I

    iget v2, p1, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;->index:I

    sub-int v0, v1, v2

    .line 49
    .local v0, "diff":I
    if-eqz v0, :cond_0

    move v1, v0

    .line 60
    :goto_0
    return v1

    .line 54
    :cond_0
    iget-wide v2, p1, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;->val:D

    double-to-int v1, v2

    iget-wide v2, p0, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;->val:D

    double-to-int v2, v2

    sub-int v0, v1, v2

    .line 55
    if-eqz v0, :cond_1

    move v1, v0

    .line 56
    goto :goto_0

    .line 60
    :cond_1
    iget-wide v2, p0, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;->order:J

    iget-wide v4, p1, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;->order:J

    sub-long/2addr v2, v4

    long-to-int v1, v2

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 32
    check-cast p1, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;

    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;->compareTo(Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;)I

    move-result v0

    return v0
.end method
