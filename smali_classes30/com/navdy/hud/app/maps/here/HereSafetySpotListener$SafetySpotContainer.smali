.class Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;
.super Ljava/lang/Object;
.source "HereSafetySpotListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SafetySpotContainer"
.end annotation


# instance fields
.field private mapMarker:Lcom/here/android/mpa/mapping/MapMarker;

.field private safetySpotNotificationInfo:Lcom/here/android/mpa/guidance/SafetySpotNotificationInfo;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$1;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;-><init>()V

    return-void
.end method

.method static synthetic access$1000(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;)Lcom/here/android/mpa/mapping/MapMarker;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;->mapMarker:Lcom/here/android/mpa/mapping/MapMarker;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;Lcom/here/android/mpa/mapping/MapMarker;)Lcom/here/android/mpa/mapping/MapMarker;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;
    .param p1, "x1"    # Lcom/here/android/mpa/mapping/MapMarker;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;->mapMarker:Lcom/here/android/mpa/mapping/MapMarker;

    return-object p1
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;)Lcom/here/android/mpa/guidance/SafetySpotNotificationInfo;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;->safetySpotNotificationInfo:Lcom/here/android/mpa/guidance/SafetySpotNotificationInfo;

    return-object v0
.end method

.method static synthetic access$602(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;Lcom/here/android/mpa/guidance/SafetySpotNotificationInfo;)Lcom/here/android/mpa/guidance/SafetySpotNotificationInfo;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;
    .param p1, "x1"    # Lcom/here/android/mpa/guidance/SafetySpotNotificationInfo;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;->safetySpotNotificationInfo:Lcom/here/android/mpa/guidance/SafetySpotNotificationInfo;

    return-object p1
.end method
