.class final Lcom/navdy/hud/app/maps/here/HereRouteManager$4;
.super Ljava/lang/Object;
.source "HereRouteManager.java"

# interfaces
.implements Lcom/navdy/hud/app/maps/here/HereRouteCalculator$RouteCalculatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeSearchTraffic(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/common/GeoCoordinate;Ljava/util/List;Lcom/here/android/mpa/common/GeoCoordinate;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$endPoint:Lcom/here/android/mpa/common/GeoCoordinate;

.field final synthetic val$request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

.field final synthetic val$runnable:Ljava/lang/Runnable;

.field final synthetic val$startPoint:Lcom/here/android/mpa/common/GeoCoordinate;

.field final synthetic val$waypoints:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/Runnable;Lcom/here/android/mpa/common/GeoCoordinate;Ljava/util/List;Lcom/here/android/mpa/common/GeoCoordinate;)V
    .locals 0

    .prologue
    .line 446
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$4;->val$request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$4;->val$runnable:Ljava/lang/Runnable;

    iput-object p3, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$4;->val$startPoint:Lcom/here/android/mpa/common/GeoCoordinate;

    iput-object p4, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$4;->val$waypoints:Ljava/util/List;

    iput-object p5, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$4;->val$endPoint:Lcom/here/android/mpa/common/GeoCoordinate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public error(Lcom/here/android/mpa/routing/RoutingError;Ljava/lang/Throwable;)V
    .locals 5
    .param p1, "error"    # Lcom/here/android/mpa/routing/RoutingError;
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 455
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->handler:Landroid/os/Handler;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$1400()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$4;->val$runnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 456
    if-eqz p1, :cond_0

    .line 457
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "got error for traffic ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/here/android/mpa/routing/RoutingError;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 461
    :goto_0
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->lockObj:Ljava/lang/Object;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$400()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 462
    :try_start_0
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCancelledByUser:Z
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$1500()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 463
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v2, "user cancelled routing"

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 464
    sget-object v0, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_CANCELLED:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$4;->val$request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    const/4 v3, 0x0

    # invokes: Lcom/navdy/hud/app/maps/here/HereRouteManager;->returnErrorResponse(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;)V
    invoke-static {v0, v2, v3}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$300(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;)V

    .line 465
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 470
    :goto_1
    return-void

    .line 459
    :cond_0
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "got error for traffic"

    invoke-virtual {v0, v1, p2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 467
    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 468
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "launching no traffic"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 469
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$4;->val$request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$4;->val$startPoint:Lcom/here/android/mpa/common/GeoCoordinate;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$4;->val$waypoints:Ljava/util/List;

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$4;->val$endPoint:Lcom/here/android/mpa/common/GeoCoordinate;

    const/4 v4, 0x0

    # invokes: Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeSearchNoTraffic(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/common/GeoCoordinate;Ljava/util/List;Lcom/here/android/mpa/common/GeoCoordinate;Z)V
    invoke-static {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$1600(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/common/GeoCoordinate;Ljava/util/List;Lcom/here/android/mpa/common/GeoCoordinate;Z)V

    goto :goto_1

    .line 467
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method public postSuccess(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 449
    .local p1, "outgoingResults":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/navigation/NavigationRouteResult;>;"
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "got result for traffic"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 450
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$4;->val$request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    const/4 v1, 0x1

    # invokes: Lcom/navdy/hud/app/maps/here/HereRouteManager;->returnSuccessResponse(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/util/ArrayList;Z)V
    invoke-static {v0, p1, v1}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$1300(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/util/ArrayList;Z)V

    .line 451
    return-void
.end method

.method public preSuccess()V
    .locals 2

    .prologue
    .line 481
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->lockObj:Ljava/lang/Object;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$400()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 482
    const/4 v0, 0x0

    :try_start_0
    # setter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCalculator:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$1702(Lcom/navdy/hud/app/maps/here/HereRouteCalculator;)Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    .line 483
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 484
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->handler:Landroid/os/Handler;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$1400()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$4;->val$runnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 485
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "route successfully calculated, post calc in progress"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 486
    return-void

    .line 483
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public progress(I)V
    .locals 0
    .param p1, "progress"    # I

    .prologue
    .line 476
    return-void
.end method
