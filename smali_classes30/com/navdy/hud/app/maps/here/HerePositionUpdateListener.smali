.class public Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener;
.super Lcom/here/android/mpa/guidance/NavigationManager$PositionListener;
.source "HerePositionUpdateListener.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener$MapMatchEvent;
    }
.end annotation


# instance fields
.field private final bus:Lcom/squareup/otto/Bus;

.field private hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

.field private lastGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

.field private lastGeoPositionTime:J

.field private logger:Lcom/navdy/service/library/log/Logger;

.field private navController:Lcom/navdy/hud/app/maps/here/HereNavController;

.field private tag:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/navdy/hud/app/maps/here/HereNavController;Lcom/navdy/hud/app/maps/here/HereNavigationManager;Lcom/squareup/otto/Bus;)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "navController"    # Lcom/navdy/hud/app/maps/here/HereNavController;
    .param p3, "hereNavigationManager"    # Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    .param p4, "bus"    # Lcom/squareup/otto/Bus;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/here/android/mpa/guidance/NavigationManager$PositionListener;-><init>()V

    .line 25
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-string v1, "HerePositionListener"

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener;->logger:Lcom/navdy/service/library/log/Logger;

    .line 40
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener;->tag:Ljava/lang/String;

    .line 41
    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener;->navController:Lcom/navdy/hud/app/maps/here/HereNavController;

    .line 42
    iput-object p3, p0, Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    .line 43
    iput-object p4, p0, Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener;->bus:Lcom/squareup/otto/Bus;

    .line 45
    invoke-virtual {p4, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 46
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener;Lcom/here/android/mpa/common/GeoPosition;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener;
    .param p1, "x1"    # Lcom/here/android/mpa/common/GeoPosition;

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener;->handlePositionUpdate(Lcom/here/android/mpa/common/GeoPosition;)V

    return-void
.end method

.method private handlePositionUpdate(Lcom/here/android/mpa/common/GeoPosition;)V
    .locals 8
    .param p1, "geoPosition"    # Lcom/here/android/mpa/common/GeoPosition;

    .prologue
    .line 61
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener;->navController:Lcom/navdy/hud/app/maps/here/HereNavController;

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereNavController;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_1

    .line 91
    :cond_0
    :goto_0
    return-void

    .line 64
    :cond_1
    if-eqz p1, :cond_0

    .line 67
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener;->lastGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

    if-eqz v1, :cond_2

    .line 68
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener;->lastGeoPositionTime:J

    sub-long v2, v4, v6

    .line 69
    .local v2, "t":J
    const-wide/16 v4, 0x1f4

    cmp-long v1, v2, v4

    if-gez v1, :cond_2

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener;->lastGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

    invoke-virtual {v1}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v1

    invoke-virtual {p1}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->isCoordinateEqual(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoCoordinate;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 70
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener;->logger:Lcom/navdy/service/library/log/Logger;

    const/4 v4, 0x2

    invoke-virtual {v1, v4}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 71
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener;->tag:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "GEO-Here-Nav same pos as last:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 87
    .end local v2    # "t":J
    :catch_0
    move-exception v2

    .line 88
    .local v2, "t":Ljava/lang/Throwable;
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 89
    invoke-static {}, Lcom/navdy/hud/app/util/CrashReporter;->getInstance()Lcom/navdy/hud/app/util/CrashReporter;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/util/CrashReporter;->reportNonFatalException(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 77
    .end local v2    # "t":Ljava/lang/Throwable;
    :cond_2
    :try_start_1
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener;->lastGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

    .line 78
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener;->lastGeoPositionTime:J

    .line 80
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener;->logger:Lcom/navdy/service/library/log/Logger;

    const/4 v4, 0x2

    invoke-virtual {v1, v4}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 81
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getCurrentRoadName()Ljava/lang/String;

    move-result-object v0

    .line 82
    .local v0, "currentRoad":Ljava/lang/String;
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener;->tag:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " onPosUpdated: road name ="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 85
    .end local v0    # "currentRoad":Ljava/lang/String;
    :cond_3
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener;->postTrackingEvent(Lcom/here/android/mpa/common/GeoPosition;)V

    .line 86
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->refreshNavigationInfo()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method private postTrackingEvent(Lcom/here/android/mpa/common/GeoPosition;)V
    .locals 8
    .param p1, "geoPosition"    # Lcom/here/android/mpa/common/GeoPosition;

    .prologue
    .line 94
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isNavigationModeOn()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 95
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener;->navController:Lcom/navdy/hud/app/maps/here/HereNavController;

    sget-object v2, Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;->OPTIMAL:Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/navdy/hud/app/maps/here/HereNavController;->getTta(Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;Z)Lcom/here/android/mpa/routing/RouteTta;

    move-result-object v0

    .line 96
    .local v0, "routeTta":Lcom/here/android/mpa/routing/RouteTta;
    if-eqz v0, :cond_0

    .line 97
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener;->bus:Lcom/squareup/otto/Bus;

    new-instance v2, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    .line 99
    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentDestinationIdentifier()Ljava/lang/String;

    move-result-object v3

    .line 100
    invoke-virtual {v0}, Lcom/here/android/mpa/routing/RouteTta;->getDuration()I

    move-result v4

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener;->navController:Lcom/navdy/hud/app/maps/here/HereNavController;

    .line 101
    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/here/HereNavController;->getDestinationDistance()J

    move-result-wide v6

    long-to-int v5, v6

    invoke-direct {v2, p1, v3, v4, v5}, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;-><init>(Lcom/here/android/mpa/common/GeoPosition;Ljava/lang/String;II)V

    .line 97
    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 109
    .end local v0    # "routeTta":Lcom/here/android/mpa/routing/RouteTta;
    :goto_0
    return-void

    .line 104
    .restart local v0    # "routeTta":Lcom/here/android/mpa/routing/RouteTta;
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener;->bus:Lcom/squareup/otto/Bus;

    new-instance v2, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;

    invoke-direct {v2, p1}, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;-><init>(Lcom/here/android/mpa/common/GeoPosition;)V

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 107
    .end local v0    # "routeTta":Lcom/here/android/mpa/routing/RouteTta;
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener;->bus:Lcom/squareup/otto/Bus;

    new-instance v2, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;

    invoke-direct {v2, p1}, Lcom/navdy/hud/app/framework/trips/TripManager$TrackingEvent;-><init>(Lcom/here/android/mpa/common/GeoPosition;)V

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public onPositionUpdated(Lcom/here/android/mpa/common/GeoPosition;)V
    .locals 3
    .param p1, "geoPosition"    # Lcom/here/android/mpa/common/GeoPosition;

    .prologue
    .line 50
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener$1;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener$1;-><init>(Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener;Lcom/here/android/mpa/common/GeoPosition;)V

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 56
    return-void
.end method
