.class Lcom/navdy/hud/app/maps/here/HereNavigationManager$1;
.super Ljava/lang/Object;
.source "HereNavigationManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/here/HereNavigationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    .prologue
    .line 275
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const-wide/16 v6, 0x7530

    .line 278
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationMode:Lcom/navdy/hud/app/maps/NavigationMode;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$000(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Lcom/navdy/hud/app/maps/NavigationMode;

    move-result-object v2

    sget-object v3, Lcom/navdy/hud/app/maps/NavigationMode;->MAP:Lcom/navdy/hud/app/maps/NavigationMode;

    if-ne v2, v3, :cond_0

    .line 302
    :goto_0
    return-void

    .line 283
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$100(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    move-result-object v4

    iget-wide v4, v4, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->lastManeuverPostTime:J

    sub-long v0, v2, v4

    .line 284
    .local v0, "time":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_1

    .line 286
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$200(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, p0, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 289
    :cond_1
    const-wide/32 v2, 0xea60

    cmp-long v2, v0, v2

    if-gez v2, :cond_3

    .line 290
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2, v8}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 291
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "etaCalc threshold not met:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 293
    :cond_2
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$200(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, p0, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 297
    :cond_3
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->refreshNavigationInfo()V

    .line 298
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2, v8}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 299
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "etaCalc updated maneuver:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 301
    :cond_4
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$200(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, p0, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
