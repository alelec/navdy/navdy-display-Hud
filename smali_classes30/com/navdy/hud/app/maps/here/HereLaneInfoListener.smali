.class public Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;
.super Lcom/here/android/mpa/guidance/NavigationManager$LaneInformationListener;
.source "HereLaneInfoListener.java"


# static fields
.field private static final EMPTY_LANE_DATA:Lcom/navdy/hud/app/maps/MapEvents$LaneData;

.field private static final HIDE_LANE_INFO:Lcom/navdy/hud/app/maps/MapEvents$HideTrafficLaneInfo;

.field private static sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private bus:Lcom/squareup/otto/Bus;

.field private lastLanesData:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;

.field private navController:Lcom/navdy/hud/app/maps/here/HereNavController;

.field private volatile running:Z

.field private startRunnable:Ljava/lang/Runnable;

.field private stopRunnable:Ljava/lang/Runnable;

.field private taskManager:Lcom/navdy/service/library/task/TaskManager;

.field private tempDirections:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/here/android/mpa/guidance/LaneInformation$Direction;",
            ">;"
        }
    .end annotation
.end field

.field private tempDirections2:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/here/android/mpa/guidance/LaneInformation$Direction;",
            ">;"
        }
    .end annotation
.end field

.field private tempLanes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/hud/app/maps/MapEvents$LaneData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 28
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 30
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$HideTrafficLaneInfo;

    invoke-direct {v0}, Lcom/navdy/hud/app/maps/MapEvents$HideTrafficLaneInfo;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->HIDE_LANE_INFO:Lcom/navdy/hud/app/maps/MapEvents$HideTrafficLaneInfo;

    .line 32
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$LaneData;

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;->OFF_ROUTE:Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/maps/MapEvents$LaneData;-><init>(Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;[Landroid/graphics/drawable/Drawable;)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->EMPTY_LANE_DATA:Lcom/navdy/hud/app/maps/MapEvents$LaneData;

    return-void
.end method

.method constructor <init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/maps/here/HereNavController;)V
    .locals 2
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .param p2, "navController"    # Lcom/navdy/hud/app/maps/here/HereNavController;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/here/android/mpa/guidance/NavigationManager$LaneInformationListener;-><init>()V

    .line 35
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->taskManager:Lcom/navdy/service/library/task/TaskManager;

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempDirections:Ljava/util/ArrayList;

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempDirections2:Ljava/util/ArrayList;

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempLanes:Ljava/util/ArrayList;

    .line 45
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$1;-><init>(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->startRunnable:Ljava/lang/Runnable;

    .line 53
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$2;-><init>(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->stopRunnable:Ljava/lang/Runnable;

    .line 62
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "ctor"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 63
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->bus:Lcom/squareup/otto/Bus;

    .line 64
    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->navController:Lcom/navdy/hud/app/maps/here/HereNavController;

    .line 65
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Lcom/navdy/hud/app/maps/here/HereNavController;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->navController:Lcom/navdy/hud/app/maps/here/HereNavController;

    return-object v0
.end method

.method static synthetic access$100()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->running:Z

    return v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->lastLanesData:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;

    return-object v0
.end method

.method static synthetic access$202(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;)Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;
    .param p1, "x1"    # Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->lastLanesData:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;

    return-object p1
.end method

.method static synthetic access$300()Lcom/navdy/hud/app/maps/MapEvents$HideTrafficLaneInfo;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->HIDE_LANE_INFO:Lcom/navdy/hud/app/maps/MapEvents$HideTrafficLaneInfo;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Lcom/squareup/otto/Bus;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->bus:Lcom/squareup/otto/Bus;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;Ljava/util/EnumSet;)Lcom/here/android/mpa/guidance/LaneInformation$Direction;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;
    .param p1, "x1"    # Ljava/util/EnumSet;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->getOnRouteDirectionFromManeuver(Ljava/util/EnumSet;)Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempLanes:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$700()Lcom/navdy/hud/app/maps/MapEvents$LaneData;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->EMPTY_LANE_DATA:Lcom/navdy/hud/app/maps/MapEvents$LaneData;

    return-object v0
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempDirections:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempDirections2:Ljava/util/ArrayList;

    return-object v0
.end method

.method private getOnRouteDirectionFromManeuver(Ljava/util/EnumSet;)Lcom/here/android/mpa/guidance/LaneInformation$Direction;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/here/android/mpa/guidance/LaneInformation$Direction;",
            ">;)",
            "Lcom/here/android/mpa/guidance/LaneInformation$Direction;"
        }
    .end annotation

    .prologue
    .local p1, "directions":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/here/android/mpa/guidance/LaneInformation$Direction;>;"
    const/4 v5, 0x0

    const/4 v8, 0x1

    .line 419
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getNextManeuver()Lcom/here/android/mpa/routing/Maneuver;

    move-result-object v0

    .line 420
    .local v0, "maneuver":Lcom/here/android/mpa/routing/Maneuver;
    if-nez v0, :cond_1

    .line 512
    .end local v0    # "maneuver":Lcom/here/android/mpa/routing/Maneuver;
    :cond_0
    :goto_0
    return-object v5

    .line 424
    .restart local v0    # "maneuver":Lcom/here/android/mpa/routing/Maneuver;
    :cond_1
    invoke-virtual {v0}, Lcom/here/android/mpa/routing/Maneuver;->getTurn()Lcom/here/android/mpa/routing/Maneuver$Turn;

    move-result-object v4

    .line 425
    .local v4, "turn":Lcom/here/android/mpa/routing/Maneuver$Turn;
    if-eqz v4, :cond_0

    .line 432
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$4;->$SwitchMap$com$here$android$mpa$routing$Maneuver$Turn:[I

    invoke-virtual {v4}, Lcom/here/android/mpa/routing/Maneuver$Turn;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    goto :goto_0

    .line 435
    :pswitch_0
    sget-object v6, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->RIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    invoke-virtual {p1, v6}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 436
    sget-object v5, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->RIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    goto :goto_0

    .line 439
    :cond_2
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;->RIGHT:Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;

    invoke-static {p1, v6}, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->getNumDirections(Ljava/util/EnumSet;Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;)I

    move-result v2

    .line 440
    .local v2, "nRightDirections":I
    if-ne v2, v8, :cond_0

    .line 441
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;->RIGHT:Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;

    invoke-static {p1, v6}, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->getDirection(Ljava/util/EnumSet;Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;)Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    move-result-object v5

    goto :goto_0

    .line 447
    .end local v2    # "nRightDirections":I
    :pswitch_1
    sget-object v6, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SLIGHTLY_RIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    invoke-virtual {p1, v6}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 448
    sget-object v5, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SLIGHTLY_RIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    goto :goto_0

    .line 451
    :cond_3
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;->RIGHT:Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;

    invoke-static {p1, v6}, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->getNumDirections(Ljava/util/EnumSet;Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;)I

    move-result v2

    .line 452
    .restart local v2    # "nRightDirections":I
    if-ne v2, v8, :cond_0

    .line 453
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;->RIGHT:Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;

    invoke-static {p1, v6}, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->getDirection(Ljava/util/EnumSet;Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;)Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    move-result-object v5

    goto :goto_0

    .line 460
    .end local v2    # "nRightDirections":I
    :pswitch_2
    sget-object v6, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SHARP_RIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    invoke-virtual {p1, v6}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 461
    sget-object v5, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SHARP_RIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    goto :goto_0

    .line 464
    :cond_4
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;->RIGHT:Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;

    invoke-static {p1, v6}, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->getNumDirections(Ljava/util/EnumSet;Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;)I

    move-result v2

    .line 465
    .restart local v2    # "nRightDirections":I
    if-ne v2, v8, :cond_0

    .line 466
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;->RIGHT:Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;

    invoke-static {p1, v6}, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->getDirection(Ljava/util/EnumSet;Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;)Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    move-result-object v5

    goto :goto_0

    .line 473
    .end local v2    # "nRightDirections":I
    :pswitch_3
    sget-object v6, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->LEFT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    invoke-virtual {p1, v6}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 474
    sget-object v5, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->LEFT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    goto :goto_0

    .line 477
    :cond_5
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;->LEFT:Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;

    invoke-static {p1, v6}, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->getNumDirections(Ljava/util/EnumSet;Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;)I

    move-result v1

    .line 478
    .local v1, "nLeftDirections":I
    if-ne v1, v8, :cond_0

    .line 479
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;->LEFT:Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;

    invoke-static {p1, v6}, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->getDirection(Ljava/util/EnumSet;Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;)Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    move-result-object v5

    goto :goto_0

    .line 485
    .end local v1    # "nLeftDirections":I
    :pswitch_4
    sget-object v6, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SLIGHTLY_LEFT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    invoke-virtual {p1, v6}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 486
    sget-object v5, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SLIGHTLY_LEFT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    goto/16 :goto_0

    .line 489
    :cond_6
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;->LEFT:Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;

    invoke-static {p1, v6}, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->getNumDirections(Ljava/util/EnumSet;Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;)I

    move-result v1

    .line 490
    .restart local v1    # "nLeftDirections":I
    if-ne v1, v8, :cond_0

    .line 491
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;->LEFT:Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;

    invoke-static {p1, v6}, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->getDirection(Ljava/util/EnumSet;Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;)Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    move-result-object v5

    goto/16 :goto_0

    .line 498
    .end local v1    # "nLeftDirections":I
    :pswitch_5
    sget-object v6, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SHARP_LEFT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    invoke-virtual {p1, v6}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 499
    sget-object v5, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SHARP_LEFT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    goto/16 :goto_0

    .line 502
    :cond_7
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;->LEFT:Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;

    invoke-static {p1, v6}, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->getNumDirections(Ljava/util/EnumSet;Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;)I

    move-result v1

    .line 503
    .restart local v1    # "nLeftDirections":I
    if-ne v1, v8, :cond_0

    .line 504
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;->LEFT:Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;

    invoke-static {p1, v6}, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->getDirection(Ljava/util/EnumSet;Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;)Lcom/here/android/mpa/guidance/LaneInformation$Direction;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    goto/16 :goto_0

    .line 510
    .end local v0    # "maneuver":Lcom/here/android/mpa/routing/Maneuver;
    .end local v1    # "nLeftDirections":I
    .end local v4    # "turn":Lcom/here/android/mpa/routing/Maneuver$Turn;
    :catch_0
    move-exception v3

    .line 511
    .local v3, "t":Ljava/lang/Throwable;
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v6, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 432
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public hideLaneInfo()V
    .locals 2

    .prologue
    .line 388
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "hideLaneInfo::"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 389
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->bus:Lcom/squareup/otto/Bus;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->HIDE_LANE_INFO:Lcom/navdy/hud/app/maps/MapEvents$HideTrafficLaneInfo;

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 390
    return-void
.end method

.method public onLaneInformation(Ljava/util/List;Lcom/here/android/mpa/common/RoadElement;)V
    .locals 3
    .param p2, "roadElement"    # Lcom/here/android/mpa/common/RoadElement;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/guidance/LaneInformation;",
            ">;",
            "Lcom/here/android/mpa/common/RoadElement;",
            ")V"
        }
    .end annotation

    .prologue
    .line 69
    .local p1, "laneInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/guidance/LaneInformation;>;"
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->running:Z

    if-nez v0, :cond_0

    .line 385
    :goto_0
    return-void

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->taskManager:Lcom/navdy/service/library/task/TaskManager;

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;-><init>(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;Ljava/util/List;)V

    const/16 v2, 0xf

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method declared-synchronized start()V
    .locals 3

    .prologue
    .line 393
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->running:Z

    if-eqz v0, :cond_0

    .line 394
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "already running"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 400
    :goto_0
    monitor-exit p0

    return-void

    .line 397
    :cond_0
    :try_start_1
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "start"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 398
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->taskManager:Lcom/navdy/service/library/task/TaskManager;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->startRunnable:Ljava/lang/Runnable;

    const/16 v2, 0xf

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 399
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->running:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 393
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized stop()V
    .locals 3

    .prologue
    .line 403
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->running:Z

    if-nez v0, :cond_0

    .line 404
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "not running"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 413
    :goto_0
    monitor-exit p0

    return-void

    .line 408
    :cond_0
    :try_start_1
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "stop"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 409
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->taskManager:Lcom/navdy/service/library/task/TaskManager;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->stopRunnable:Ljava/lang/Runnable;

    const/16 v2, 0xf

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 410
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->running:Z

    .line 411
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->bus:Lcom/squareup/otto/Bus;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->HIDE_LANE_INFO:Lcom/navdy/hud/app/maps/MapEvents$HideTrafficLaneInfo;

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 412
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->lastLanesData:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 403
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
