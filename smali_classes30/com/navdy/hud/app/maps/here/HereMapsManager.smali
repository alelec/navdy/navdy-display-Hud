.class public Lcom/navdy/hud/app/maps/here/HereMapsManager;
.super Ljava/lang/Object;
.source "HereMapsManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/maps/here/HereMapsManager$MWConfigCorruptException;
    }
.end annotation


# static fields
.field public static final DEFAULT_LATITUDE:D = 37.802086

.field public static final DEFAULT_LONGITUDE:D = -122.419015

.field public static final DEFAULT_TILT:F = 60.0f

.field public static final DEFAULT_ZOOM_LEVEL:F = 16.5f

.field private static final DISABLE_MAPS_PROPERTY:Ljava/lang/String; = "persist.sys.map.disable"

.field private static final ENABLE_MAPS:Z

.field private static final ENROUTE_MAP_SCHEME:Ljava/lang/String; = "hybrid.night"

.field private static final GPS_SPEED_LAST_LOCATION_THRESHOLD:I = 0x7d0

.field static final MIN_SAME_POSITION_UPDATE_THRESHOLD:I = 0x1f4

.field private static final MW_CONFIG_EXCEPTION_MSG:Ljava/lang/String; = "Invalid configuration file. Check MWConfig!"

.field private static final NAVDY_HERE_MAP_SERVICE_NAME:Ljava/lang/String; = "com.navdy.HereMapService"

.field public static final ROUTE_CALC_START_ZOOM_LEVEL:F = 15.5f

.field private static final TRACKING_MAP_SCHEME:Ljava/lang/String; = "carnav.night"

.field private static final TRAFFIC_REROUTE_PROPERTY:Ljava/lang/String; = "persist.sys.hud_traffic_reroute"

.field private static recalcCurrentRouteForTraffic:Z

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static final singleton:Lcom/navdy/hud/app/maps/here/HereMapsManager;


# instance fields
.field private final bkLocationReceiverHandlerThread:Landroid/os/HandlerThread;

.field bus:Lcom/squareup/otto/Bus;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private context:Landroid/content/Context;

.field private engineInitListener:Lcom/here/android/mpa/common/OnEngineInitListener;

.field private volatile extrapolationOn:Z

.field private handler:Landroid/os/Handler;

.field private hereLocationFixManager:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

.field private hereMapAnimator:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

.field private initLock:Ljava/lang/Object;

.field private initialMapRendering:Z

.field private lastGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

.field private lastGeoPositionTime:J

.field private listener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private volatile lowBandwidthDetected:Z

.field mDriverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private map:Lcom/here/android/mpa/mapping/Map;

.field private mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

.field private mapDataVerified:Z

.field private mapEngine:Lcom/here/android/mpa/common/MapEngine;

.field private mapEngineStartTime:J

.field private mapError:Lcom/here/android/mpa/common/OnEngineInitListener$Error;

.field private mapInitLoaderComplete:Z

.field private mapInitialized:Z

.field private mapInitializing:Z

.field private mapLoader:Lcom/here/android/mpa/odml/MapLoader;

.field private mapPackageCount:I

.field private mapView:Lcom/here/android/mpa/mapping/MapView;

.field private navigationView:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

.field private offlineMapsVersion:Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;

.field private positionChangedListener:Lcom/here/android/mpa/common/PositioningManager$OnPositionChangedListener;

.field private positioningManager:Lcom/here/android/mpa/common/PositioningManager;

.field private positioningManagerInstalled:Z

.field powerManager:Lcom/navdy/hud/app/device/PowerManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private routeCalcEventHandler:Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;

.field private routeStartPoint:Lcom/here/android/mpa/common/GeoCoordinate;

.field sharedPreferences:Landroid/content/SharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

.field private volatile turnEngineOn:Z

.field private voiceSkinsLoaded:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 94
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v2, Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-direct {v0, v2}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 119
    const-string v0, "persist.sys.map.disable"

    invoke-static {v0, v1}, Lcom/navdy/hud/app/util/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->ENABLE_MAPS:Z

    .line 121
    sput-boolean v1, Lcom/navdy/hud/app/maps/here/HereMapsManager;->recalcCurrentRouteForTraffic:Z

    .line 145
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-direct {v0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->singleton:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    return-void

    :cond_0
    move v0, v1

    .line 119
    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 668
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 143
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->initLock:Ljava/lang/Object;

    .line 162
    iput-boolean v2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->initialMapRendering:Z

    .line 164
    iput-boolean v1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->lowBandwidthDetected:Z

    .line 165
    iput-boolean v1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->turnEngineOn:Z

    .line 193
    iput-boolean v2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapInitializing:Z

    .line 210
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->handler:Landroid/os/Handler;

    .line 218
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;-><init>(Lcom/navdy/hud/app/maps/here/HereMapsManager;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->engineInitListener:Lcom/here/android/mpa/common/OnEngineInitListener;

    .line 491
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager$3;-><init>(Lcom/navdy/hud/app/maps/here/HereMapsManager;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->positionChangedListener:Lcom/here/android/mpa/common/PositioningManager$OnPositionChangedListener;

    .line 605
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereMapsManager$4;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager$4;-><init>(Lcom/navdy/hud/app/maps/here/HereMapsManager;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->listener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 669
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->context:Landroid/content/Context;

    .line 670
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->context:Landroid/content/Context;

    invoke-static {v0, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 671
    invoke-static {}, Lcom/navdy/hud/app/manager/SpeedManager;->getInstance()Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    .line 673
    sget-boolean v0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->ENABLE_MAPS:Z

    if-eqz v0, :cond_0

    .line 675
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->checkforNetworkProvider()V

    .line 679
    :cond_0
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "here_bk_location"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->bkLocationReceiverHandlerThread:Landroid/os/HandlerThread;

    .line 680
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->bkLocationReceiverHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 681
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 682
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/maps/here/HereMapsManager;)J
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .prologue
    .line 93
    iget-wide v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapEngineStartTime:J

    return-wide v0
.end method

.method static synthetic access$100()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 93
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/navdy/hud/app/maps/here/HereMapsManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->registerConnectivityReceiver()V

    return-void
.end method

.method static synthetic access$1100(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Lcom/navdy/hud/app/maps/here/HereMapAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->hereMapAnimator:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/navdy/hud/app/maps/here/HereMapsManager;Lcom/navdy/hud/app/maps/here/HereMapAnimator;)Lcom/navdy/hud/app/maps/here/HereMapAnimator;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;
    .param p1, "x1"    # Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->hereMapAnimator:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->initialMapRendering:Z

    return v0
.end method

.method static synthetic access$1300(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Lcom/navdy/hud/app/maps/here/HereLocationFixManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->hereLocationFixManager:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/navdy/hud/app/maps/here/HereMapsManager;Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Lcom/navdy/hud/app/maps/here/HereLocationFixManager;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;
    .param p1, "x1"    # Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->hereLocationFixManager:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    return-object p1
.end method

.method static synthetic access$1402(Lcom/navdy/hud/app/maps/here/HereMapsManager;Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;)Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;
    .param p1, "x1"    # Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->routeCalcEventHandler:Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/navdy/hud/app/maps/here/HereMapsManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->initMapLoader()V

    return-void
.end method

.method static synthetic access$1602(Lcom/navdy/hud/app/maps/here/HereMapsManager;Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;)Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;
    .param p1, "x1"    # Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->offlineMapsVersion:Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->initLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1802(Lcom/navdy/hud/app/maps/here/HereMapsManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;
    .param p1, "x1"    # Z

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapInitializing:Z

    return p1
.end method

.method static synthetic access$1900(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapInitialized:Z

    return v0
.end method

.method static synthetic access$1902(Lcom/navdy/hud/app/maps/here/HereMapsManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;
    .param p1, "x1"    # Z

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapInitialized:Z

    return p1
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Lcom/here/android/mpa/common/MapEngine;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapEngine:Lcom/here/android/mpa/common/MapEngine;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/navdy/hud/app/maps/here/HereMapsManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->setEngineOnlineState()V

    return-void
.end method

.method static synthetic access$2100(Lcom/navdy/hud/app/maps/here/HereMapsManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->startTrafficUpdater()V

    return-void
.end method

.method static synthetic access$2202(Lcom/navdy/hud/app/maps/here/HereMapsManager;Lcom/here/android/mpa/common/OnEngineInitListener$Error;)Lcom/here/android/mpa/common/OnEngineInitListener$Error;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;
    .param p1, "x1"    # Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapError:Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    return-object p1
.end method

.method static synthetic access$2300(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Lcom/here/android/mpa/odml/MapLoader;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapLoader:Lcom/here/android/mpa/odml/MapLoader;

    return-object v0
.end method

.method static synthetic access$2302(Lcom/navdy/hud/app/maps/here/HereMapsManager;Lcom/here/android/mpa/odml/MapLoader;)Lcom/here/android/mpa/odml/MapLoader;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;
    .param p1, "x1"    # Lcom/here/android/mpa/odml/MapLoader;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapLoader:Lcom/here/android/mpa/odml/MapLoader;

    return-object p1
.end method

.method static synthetic access$2400(Lcom/navdy/hud/app/maps/here/HereMapsManager;Lcom/here/android/mpa/odml/MapPackage;Ljava/util/EnumSet;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;
    .param p1, "x1"    # Lcom/here/android/mpa/odml/MapPackage;
    .param p2, "x2"    # Ljava/util/EnumSet;

    .prologue
    .line 93
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->printMapPackages(Lcom/here/android/mpa/odml/MapPackage;Ljava/util/EnumSet;)V

    return-void
.end method

.method static synthetic access$2500(Lcom/navdy/hud/app/maps/here/HereMapsManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .prologue
    .line 93
    iget v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapPackageCount:I

    return v0
.end method

.method static synthetic access$2502(Lcom/navdy/hud/app/maps/here/HereMapsManager;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;
    .param p1, "x1"    # I

    .prologue
    .line 93
    iput p1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapPackageCount:I

    return p1
.end method

.method static synthetic access$2602(Lcom/navdy/hud/app/maps/here/HereMapsManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;
    .param p1, "x1"    # Z

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapDataVerified:Z

    return p1
.end method

.method static synthetic access$2700(Lcom/navdy/hud/app/maps/here/HereMapsManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->invokeMapLoader()V

    return-void
.end method

.method static synthetic access$2800(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->extrapolationOn:Z

    return v0
.end method

.method static synthetic access$2802(Lcom/navdy/hud/app/maps/here/HereMapsManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;
    .param p1, "x1"    # Z

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->extrapolationOn:Z

    return p1
.end method

.method static synthetic access$2900(Lcom/navdy/hud/app/maps/here/HereMapsManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sendExtrapolationEvent()V

    return-void
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Lcom/here/android/mpa/mapping/Map;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->map:Lcom/here/android/mpa/mapping/Map;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Lcom/here/android/mpa/common/GeoPosition;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->lastGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

    return-object v0
.end method

.method static synthetic access$3002(Lcom/navdy/hud/app/maps/here/HereMapsManager;Lcom/here/android/mpa/common/GeoPosition;)Lcom/here/android/mpa/common/GeoPosition;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;
    .param p1, "x1"    # Lcom/here/android/mpa/common/GeoPosition;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->lastGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

    return-object p1
.end method

.method static synthetic access$302(Lcom/navdy/hud/app/maps/here/HereMapsManager;Lcom/here/android/mpa/mapping/Map;)Lcom/here/android/mpa/mapping/Map;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;
    .param p1, "x1"    # Lcom/here/android/mpa/mapping/Map;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->map:Lcom/here/android/mpa/mapping/Map;

    return-object p1
.end method

.method static synthetic access$3100(Lcom/navdy/hud/app/maps/here/HereMapsManager;)J
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .prologue
    .line 93
    iget-wide v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->lastGeoPositionTime:J

    return-wide v0
.end method

.method static synthetic access$3102(Lcom/navdy/hud/app/maps/here/HereMapsManager;J)J
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;
    .param p1, "x1"    # J

    .prologue
    .line 93
    iput-wide p1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->lastGeoPositionTime:J

    return-wide p1
.end method

.method static synthetic access$3200(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Lcom/navdy/hud/app/manager/SpeedManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/navdy/hud/app/maps/here/HereMapsManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->initialize()V

    return-void
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Lcom/navdy/hud/app/maps/here/HereMapController;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    return-object v0
.end method

.method static synthetic access$502(Lcom/navdy/hud/app/maps/here/HereMapsManager;Lcom/navdy/hud/app/maps/here/HereMapController;)Lcom/navdy/hud/app/maps/here/HereMapController;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;
    .param p1, "x1"    # Lcom/navdy/hud/app/maps/here/HereMapController;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    return-object p1
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/maps/here/HereMapsManager;Lcom/here/android/mpa/common/GeoCoordinate;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;
    .param p1, "x1"    # Lcom/here/android/mpa/common/GeoCoordinate;

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->setMapAttributes(Lcom/here/android/mpa/common/GeoCoordinate;)V

    return-void
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/maps/here/HereMapsManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->setMapTraffic()V

    return-void
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/maps/here/HereMapsManager;Lcom/here/android/mpa/mapping/Map;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;
    .param p1, "x1"    # Lcom/here/android/mpa/mapping/Map;

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->setMapPoiLayer(Lcom/here/android/mpa/mapping/Map;)V

    return-void
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Lcom/here/android/mpa/common/PositioningManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->positioningManager:Lcom/here/android/mpa/common/PositioningManager;

    return-object v0
.end method

.method static synthetic access$902(Lcom/navdy/hud/app/maps/here/HereMapsManager;Lcom/here/android/mpa/common/PositioningManager;)Lcom/here/android/mpa/common/PositioningManager;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;
    .param p1, "x1"    # Lcom/here/android/mpa/common/PositioningManager;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->positioningManager:Lcom/here/android/mpa/common/PositioningManager;

    return-object p1
.end method

.method private checkforNetworkProvider()V
    .locals 2

    .prologue
    .line 685
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereMapsManager$5;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager$5;-><init>(Lcom/navdy/hud/app/maps/here/HereMapsManager;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 714
    return-void
.end method

.method private getDefaultTiltLevel()F
    .locals 1

    .prologue
    .line 850
    const/high16 v0, 0x42700000    # 60.0f

    return v0
.end method

.method private getDefaultZoomLevel()D
    .locals 2

    .prologue
    .line 846
    const-wide v0, 0x4030800000000000L    # 16.5

    return-wide v0
.end method

.method public static getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;
    .locals 1

    .prologue
    .line 148
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->singleton:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    return-object v0
.end method

.method private initMapLoader()V
    .locals 3

    .prologue
    .line 402
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereMapsManager$2;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager$2;-><init>(Lcom/navdy/hud/app/maps/here/HereMapsManager;)V

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 462
    return-void
.end method

.method private initialize()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 717
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, ":initializing voice skins"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 718
    invoke-static {}, Lcom/navdy/hud/app/maps/here/VoiceSkinsConfigurator;->updateVoiceSkins()V

    .line 720
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, ":initializing event handlers"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 721
    invoke-static {}, Lcom/navdy/hud/app/maps/MapsEventHandler;->getInstance()Lcom/navdy/hud/app/maps/MapsEventHandler;

    .line 723
    invoke-static {}, Lcom/navdy/hud/app/maps/MapSettings;->isDebugHereLocation()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 724
    invoke-static {v7}, Lcom/navdy/hud/app/debug/DebugReceiver;->setHereDebugLocation(Z)V

    .line 728
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/maps/MapSettings;->getMapFps()I

    move-result v5

    invoke-static {v5}, Lcom/here/android/mpa/mapping/Map;->setMaximumFps(I)V

    .line 729
    invoke-static {v7}, Lcom/here/android/mpa/mapping/Map;->enableMaximumFpsLimit(Z)V

    .line 730
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "map-fps ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Lcom/here/android/mpa/mapping/Map;->getMaximumFps()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] enabled["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Lcom/here/android/mpa/mapping/Map;->isMaximumFpsLimited()Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 736
    :try_start_0
    const-class v5, Lcom/nokia/maps/MapSettings;

    const-string v6, "g"

    invoke-virtual {v5, v6}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 737
    .local v1, "f":Ljava/lang/reflect/Field;
    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 738
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "enable worker thread before is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v1, v7}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 739
    const/4 v5, 0x0

    sget-object v6, Lcom/nokia/maps/MapSettings$b;->a:Lcom/nokia/maps/MapSettings$b;

    invoke-virtual {v1, v5, v6}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 740
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "enable worker thread after is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v1, v7}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 745
    .end local v1    # "f":Ljava/lang/reflect/Field;
    :goto_0
    invoke-static {}, Lcom/here/android/mpa/common/MapEngine;->getInstance()Lcom/here/android/mpa/common/MapEngine;

    move-result-object v5

    iput-object v5, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapEngine:Lcom/here/android/mpa/common/MapEngine;

    .line 746
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "calling maps engine init"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 748
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sharedPreferences:Landroid/content/SharedPreferences;

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->listener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v5, v6}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 750
    const-string v5, "persist.sys.hud_traffic_reroute"

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/navdy/hud/app/util/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    sput-boolean v5, Lcom/navdy/hud/app/maps/here/HereMapsManager;->recalcCurrentRouteForTraffic:Z

    .line 751
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "persist.sys.hud_traffic_reroute="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-boolean v7, Lcom/navdy/hud/app/maps/here/HereMapsManager;->recalcCurrentRouteForTraffic:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 754
    invoke-static {}, Lcom/navdy/hud/app/storage/PathManager;->getInstance()Lcom/navdy/hud/app/storage/PathManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/storage/PathManager;->getMapsPartitionPath()Ljava/lang/String;

    move-result-object v2

    .line 755
    .local v2, "mapsPartition":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    .line 757
    :try_start_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".here-maps"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "com.navdy.HereMapService"

    invoke-static {v5, v6}, Lcom/here/android/mpa/common/MapSettings;->setIsolatedDiskCacheRootPath(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    .line 758
    .local v3, "success":Z
    if-nez v3, :cond_1

    .line 759
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "setIsolatedDiskCacheRootPath() failed"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 766
    .end local v3    # "success":Z
    :cond_1
    :goto_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapEngineStartTime:J

    .line 767
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapEngine:Lcom/here/android/mpa/common/MapEngine;

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->context:Landroid/content/Context;

    iget-object v7, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->engineInitListener:Lcom/here/android/mpa/common/OnEngineInitListener;

    invoke-virtual {v5, v6, v7}, Lcom/here/android/mpa/common/MapEngine;->init(Landroid/content/Context;Lcom/here/android/mpa/common/OnEngineInitListener;)V

    .line 768
    return-void

    .line 741
    .end local v2    # "mapsPartition":Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 742
    .local v4, "t":Ljava/lang/Throwable;
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "enable worker thread"

    invoke-virtual {v5, v6, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 761
    .end local v4    # "t":Ljava/lang/Throwable;
    .restart local v2    # "mapsPartition":Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 762
    .local v0, "e":Ljava/lang/Exception;
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "exception in setIsolatedDiskCacheRootPath()"

    invoke-virtual {v5, v6, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private invokeMapLoader()V
    .locals 4

    .prologue
    .line 465
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapLoader:Lcom/here/android/mpa/odml/MapLoader;

    if-nez v1, :cond_0

    .line 466
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "invokeMapLoader: no maploader"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 480
    :goto_0
    return-void

    .line 469
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/navdy/service/library/util/SystemUtils;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 470
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "invokeMapLoader: not connected to n/w"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 473
    :cond_1
    iget-boolean v1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapInitLoaderComplete:Z

    if-nez v1, :cond_2

    .line 474
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapInitLoaderComplete:Z

    .line 475
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapLoader:Lcom/here/android/mpa/odml/MapLoader;

    invoke-virtual {v1}, Lcom/here/android/mpa/odml/MapLoader;->getMapPackages()Z

    move-result v0

    .line 476
    .local v0, "ret":Z
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initMapLoader called getMapPackages:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 478
    .end local v0    # "ret":Z
    :cond_2
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "invokeMapLoader: already complete"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private isTrafficOverlayEnabled(Lcom/navdy/hud/app/maps/NavigationMode;)Z
    .locals 3
    .param p1, "navigationMode"    # Lcom/navdy/hud/app/maps/NavigationMode;

    .prologue
    const/4 v1, 0x0

    .line 994
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->map:Lcom/here/android/mpa/mapping/Map;

    invoke-virtual {v2}, Lcom/here/android/mpa/mapping/Map;->getMapTrafficLayer()Lcom/here/android/mpa/mapping/MapTrafficLayer;

    move-result-object v0

    .line 995
    .local v0, "trafficLayer":Lcom/here/android/mpa/mapping/MapTrafficLayer;
    sget-object v2, Lcom/navdy/hud/app/maps/NavigationMode;->MAP:Lcom/navdy/hud/app/maps/NavigationMode;

    if-ne p1, v2, :cond_1

    .line 996
    sget-object v2, Lcom/here/android/mpa/mapping/MapTrafficLayer$RenderLayer;->FLOW:Lcom/here/android/mpa/mapping/MapTrafficLayer$RenderLayer;

    invoke-virtual {v0, v2}, Lcom/here/android/mpa/mapping/MapTrafficLayer;->isEnabled(Lcom/here/android/mpa/mapping/MapTrafficLayer$RenderLayer;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lcom/here/android/mpa/mapping/MapTrafficLayer$RenderLayer;->INCIDENT:Lcom/here/android/mpa/mapping/MapTrafficLayer$RenderLayer;

    .line 997
    invoke-virtual {v0, v2}, Lcom/here/android/mpa/mapping/MapTrafficLayer;->isEnabled(Lcom/here/android/mpa/mapping/MapTrafficLayer$RenderLayer;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 1001
    :cond_0
    :goto_0
    return v1

    .line 998
    :cond_1
    sget-object v2, Lcom/navdy/hud/app/maps/NavigationMode;->MAP_ON_ROUTE:Lcom/navdy/hud/app/maps/NavigationMode;

    if-ne p1, v2, :cond_0

    .line 999
    sget-object v1, Lcom/here/android/mpa/mapping/MapTrafficLayer$RenderLayer;->ONROUTE:Lcom/here/android/mpa/mapping/MapTrafficLayer$RenderLayer;

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/mapping/MapTrafficLayer;->isEnabled(Lcom/here/android/mpa/mapping/MapTrafficLayer$RenderLayer;)Z

    move-result v1

    goto :goto_0
.end method

.method private isTrafficOverlayVisible()Z
    .locals 2

    .prologue
    .line 989
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentNavigationMode()Lcom/navdy/hud/app/maps/NavigationMode;

    move-result-object v0

    .line 990
    .local v0, "navigationMode":Lcom/navdy/hud/app/maps/NavigationMode;
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isTrafficOverlayEnabled(Lcom/navdy/hud/app/maps/NavigationMode;)Z

    move-result v1

    return v1
.end method

.method private printMapPackages(Lcom/here/android/mpa/odml/MapPackage;Ljava/util/EnumSet;)V
    .locals 4
    .param p1, "mapPackage"    # Lcom/here/android/mpa/odml/MapPackage;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/here/android/mpa/odml/MapPackage;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/here/android/mpa/odml/MapPackage$InstallationState;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1263
    .local p2, "state":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/here/android/mpa/odml/MapPackage$InstallationState;>;"
    if-nez p1, :cond_1

    .line 1277
    :cond_0
    return-void

    .line 1266
    :cond_1
    invoke-virtual {p1}, Lcom/here/android/mpa/odml/MapPackage;->getInstallationState()Lcom/here/android/mpa/odml/MapPackage$InstallationState;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1268
    iget v2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapPackageCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapPackageCount:I

    .line 1270
    :cond_2
    invoke-virtual {p1}, Lcom/here/android/mpa/odml/MapPackage;->getChildren()Ljava/util/List;

    move-result-object v1

    .line 1271
    .local v1, "children":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/odml/MapPackage;>;"
    if-eqz v1, :cond_0

    .line 1274
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/here/android/mpa/odml/MapPackage;

    .line 1275
    .local v0, "child":Lcom/here/android/mpa/odml/MapPackage;
    invoke-direct {p0, v0, p2}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->printMapPackages(Lcom/here/android/mpa/odml/MapPackage;Ljava/util/EnumSet;)V

    goto :goto_0
.end method

.method private registerConnectivityReceiver()V
    .locals 3

    .prologue
    .line 1009
    new-instance v0, Landroid/content/IntentFilter;

    const-string v2, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 1010
    .local v0, "filter":Landroid/content/IntentFilter;
    new-instance v1, Lcom/navdy/hud/app/maps/here/HereMapsManager$11;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager$11;-><init>(Lcom/navdy/hud/app/maps/here/HereMapsManager;)V

    .line 1019
    .local v1, "receiver":Landroid/content/BroadcastReceiver;
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->context:Landroid/content/Context;

    invoke-virtual {v2, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1020
    return-void
.end method

.method private sendExtrapolationEvent()V
    .locals 3

    .prologue
    .line 1108
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1109
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "EXTRAPOLATION_FLAG"

    iget-boolean v2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->extrapolationOn:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1110
    const-string v1, "EXTRAPOLATION"

    invoke-static {v1, v0}, Lcom/navdy/hud/app/device/gps/GpsUtils;->sendEventBroadcast(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1111
    return-void
.end method

.method private setEngineOnlineState()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 885
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v0

    .line 886
    .local v0, "online":Z
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->lowBandwidthDetected:Z

    .line 887
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setEngineOnlineState:setting maps engine online state:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 888
    if-eqz v0, :cond_0

    .line 891
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$6;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager$6;-><init>(Lcom/navdy/hud/app/maps/here/HereMapsManager;)V

    invoke-virtual {v1, v2, v4}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 905
    :goto_0
    return-void

    .line 898
    :cond_0
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$7;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager$7;-><init>(Lcom/navdy/hud/app/maps/here/HereMapsManager;)V

    invoke-virtual {v1, v2, v4}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method private setMapAttributes(Lcom/here/android/mpa/common/GeoCoordinate;)V
    .locals 8
    .param p1, "geoCoordinate"    # Lcom/here/android/mpa/common/GeoCoordinate;

    .prologue
    const/4 v1, 0x0

    .line 854
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->map:Lcom/here/android/mpa/mapping/Map;

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/mapping/Map;->setExtrudedBuildingsVisible(Z)Z

    .line 855
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->map:Lcom/here/android/mpa/mapping/Map;

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/mapping/Map;->setLandmarksVisible(Z)Lcom/here/android/mpa/mapping/Map;

    .line 856
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->map:Lcom/here/android/mpa/mapping/Map;

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/mapping/Map;->setStreetLevelCoverageVisible(Z)Lcom/here/android/mpa/mapping/Map;

    .line 858
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->map:Lcom/here/android/mpa/mapping/Map;

    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getTrackingMapScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/mapping/Map;->setMapScheme(Ljava/lang/String;)Lcom/here/android/mpa/mapping/Map;

    .line 860
    if-eqz p1, :cond_0

    .line 861
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->map:Lcom/here/android/mpa/mapping/Map;

    sget-object v3, Lcom/here/android/mpa/mapping/Map$Animation;->NONE:Lcom/here/android/mpa/mapping/Map$Animation;

    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getDefaultZoomLevel()D

    move-result-wide v4

    const/high16 v6, -0x40800000    # -1.0f

    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getDefaultTiltLevel()F

    move-result v7

    move-object v2, p1

    invoke-virtual/range {v1 .. v7}, Lcom/here/android/mpa/mapping/Map;->setCenter(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;DFF)V

    .line 862
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setcenterDefault:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 866
    :goto_0
    return-void

    .line 864
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "geoCoordinate not available"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setMapOnRouteTraffic()V
    .locals 2

    .prologue
    .line 961
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereMapsManager$9;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager$9;-><init>(Lcom/navdy/hud/app/maps/here/HereMapsManager;)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereMapController;->execute(Ljava/lang/Runnable;)V

    .line 972
    return-void
.end method

.method private setMapPoiLayer(Lcom/here/android/mpa/mapping/Map;)V
    .locals 10
    .param p1, "map"    # Lcom/here/android/mpa/mapping/Map;

    .prologue
    const/4 v9, 0x0

    .line 908
    invoke-virtual {p1, v9}, Lcom/here/android/mpa/mapping/Map;->setCartoMarkersVisible(Z)Lcom/here/android/mpa/mapping/Map;

    .line 909
    invoke-virtual {p1}, Lcom/here/android/mpa/mapping/Map;->getMapTransitLayer()Lcom/here/android/mpa/mapping/MapTransitLayer;

    move-result-object v5

    sget-object v6, Lcom/here/android/mpa/mapping/MapTransitLayer$Mode;->NOTHING:Lcom/here/android/mpa/mapping/MapTransitLayer$Mode;

    invoke-virtual {v5, v6}, Lcom/here/android/mpa/mapping/MapTransitLayer;->setMode(Lcom/here/android/mpa/mapping/MapTransitLayer$Mode;)V

    .line 911
    sget-object v5, Lcom/here/android/mpa/mapping/Map$LayerCategory;->ICON_PUBLIC_TRANSIT_STATION:Lcom/here/android/mpa/mapping/Map$LayerCategory;

    const/16 v6, 0xf

    new-array v6, v6, [Lcom/here/android/mpa/mapping/Map$LayerCategory;

    sget-object v7, Lcom/here/android/mpa/mapping/Map$LayerCategory;->PUBLIC_TRANSIT_LINE:Lcom/here/android/mpa/mapping/Map$LayerCategory;

    aput-object v7, v6, v9

    const/4 v7, 0x1

    sget-object v8, Lcom/here/android/mpa/mapping/Map$LayerCategory;->LABEL_PUBLIC_TRANSIT_STATION:Lcom/here/android/mpa/mapping/Map$LayerCategory;

    aput-object v8, v6, v7

    const/4 v7, 0x2

    sget-object v8, Lcom/here/android/mpa/mapping/Map$LayerCategory;->LABEL_PUBLIC_TRANSIT_LINE:Lcom/here/android/mpa/mapping/Map$LayerCategory;

    aput-object v8, v6, v7

    const/4 v7, 0x3

    sget-object v8, Lcom/here/android/mpa/mapping/Map$LayerCategory;->BEACH:Lcom/here/android/mpa/mapping/Map$LayerCategory;

    aput-object v8, v6, v7

    const/4 v7, 0x4

    sget-object v8, Lcom/here/android/mpa/mapping/Map$LayerCategory;->WOODLAND:Lcom/here/android/mpa/mapping/Map$LayerCategory;

    aput-object v8, v6, v7

    const/4 v7, 0x5

    sget-object v8, Lcom/here/android/mpa/mapping/Map$LayerCategory;->DESERT:Lcom/here/android/mpa/mapping/Map$LayerCategory;

    aput-object v8, v6, v7

    const/4 v7, 0x6

    sget-object v8, Lcom/here/android/mpa/mapping/Map$LayerCategory;->GLACIER:Lcom/here/android/mpa/mapping/Map$LayerCategory;

    aput-object v8, v6, v7

    const/4 v7, 0x7

    sget-object v8, Lcom/here/android/mpa/mapping/Map$LayerCategory;->AMUSEMENT_PARK:Lcom/here/android/mpa/mapping/Map$LayerCategory;

    aput-object v8, v6, v7

    const/16 v7, 0x8

    sget-object v8, Lcom/here/android/mpa/mapping/Map$LayerCategory;->ANIMAL_PARK:Lcom/here/android/mpa/mapping/Map$LayerCategory;

    aput-object v8, v6, v7

    const/16 v7, 0x9

    sget-object v8, Lcom/here/android/mpa/mapping/Map$LayerCategory;->BUILTUP:Lcom/here/android/mpa/mapping/Map$LayerCategory;

    aput-object v8, v6, v7

    const/16 v7, 0xa

    sget-object v8, Lcom/here/android/mpa/mapping/Map$LayerCategory;->CEMETERY:Lcom/here/android/mpa/mapping/Map$LayerCategory;

    aput-object v8, v6, v7

    const/16 v7, 0xb

    sget-object v8, Lcom/here/android/mpa/mapping/Map$LayerCategory;->BUILDING:Lcom/here/android/mpa/mapping/Map$LayerCategory;

    aput-object v8, v6, v7

    const/16 v7, 0xc

    sget-object v8, Lcom/here/android/mpa/mapping/Map$LayerCategory;->NEIGHBORHOOD_AREA:Lcom/here/android/mpa/mapping/Map$LayerCategory;

    aput-object v8, v6, v7

    const/16 v7, 0xd

    sget-object v8, Lcom/here/android/mpa/mapping/Map$LayerCategory;->NATIONAL_PARK:Lcom/here/android/mpa/mapping/Map$LayerCategory;

    aput-object v8, v6, v7

    const/16 v7, 0xe

    sget-object v8, Lcom/here/android/mpa/mapping/Map$LayerCategory;->NATIVE_RESERVATION:Lcom/here/android/mpa/mapping/Map$LayerCategory;

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;[Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v3

    .line 930
    .local v3, "transit":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/here/android/mpa/mapping/Map$LayerCategory;>;"
    invoke-virtual {p1, v3, v9}, Lcom/here/android/mpa/mapping/Map;->setVisibleLayers(Ljava/util/EnumSet;Z)Lcom/here/android/mpa/mapping/Map;

    .line 932
    invoke-virtual {p1}, Lcom/here/android/mpa/mapping/Map;->getVisibleLayers()Ljava/util/EnumSet;

    move-result-object v4

    .line 933
    .local v4, "visibleLayers":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/here/android/mpa/mapping/Map$LayerCategory;>;"
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "==== visible layers == ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Ljava/util/EnumSet;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 934
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 935
    .local v2, "layers":Ljava/lang/StringBuilder;
    const/4 v0, 0x1

    .line 936
    .local v0, "first":Z
    invoke-virtual {v4}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/here/android/mpa/mapping/Map$LayerCategory;

    .line 937
    .local v1, "layerCategory":Lcom/here/android/mpa/mapping/Map$LayerCategory;
    if-nez v0, :cond_0

    .line 938
    const-string v6, ", "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 940
    :cond_0
    invoke-virtual {v1}, Lcom/here/android/mpa/mapping/Map$LayerCategory;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 941
    const/4 v0, 0x0

    .line 942
    goto :goto_0

    .line 943
    .end local v1    # "layerCategory":Lcom/here/android/mpa/mapping/Map$LayerCategory;
    :cond_1
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 944
    return-void
.end method

.method private setMapTraffic()V
    .locals 2

    .prologue
    .line 947
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereMapsManager$8;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager$8;-><init>(Lcom/navdy/hud/app/maps/here/HereMapsManager;)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereMapController;->execute(Ljava/lang/Runnable;)V

    .line 958
    return-void
.end method

.method private startTrafficUpdater()V
    .locals 2

    .prologue
    .line 389
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->startTrafficUpdater()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 393
    :goto_0
    return-void

    .line 390
    :catch_0
    move-exception v0

    .line 391
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public checkForMapDataUpdate()V
    .locals 5

    .prologue
    .line 1281
    :try_start_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1282
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "checkForMapDataUpdate MapLoader: engine not initialized"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1298
    :goto_0
    return-void

    .line 1285
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapLoader:Lcom/here/android/mpa/odml/MapLoader;

    if-nez v2, :cond_1

    .line 1286
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "checkForMapDataUpdate MapLoader: not available"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1295
    :catch_0
    move-exception v1

    .line 1296
    .local v1, "t":Ljava/lang/Throwable;
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1289
    .end local v1    # "t":Ljava/lang/Throwable;
    :cond_1
    :try_start_1
    iget-boolean v2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapInitLoaderComplete:Z

    if-nez v2, :cond_2

    .line 1290
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "checkForMapDataUpdate MapLoader: loader not initialized"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 1293
    :cond_2
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapLoader:Lcom/here/android/mpa/odml/MapLoader;

    invoke-virtual {v2}, Lcom/here/android/mpa/odml/MapLoader;->checkForMapDataUpdate()Z

    move-result v0

    .line 1294
    .local v0, "ret":Z
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "checkForMapDataUpdate MapLoader: called checkForMapDataUpdate:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method clearMapTraffic()V
    .locals 2

    .prologue
    .line 975
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereMapsManager$10;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager$10;-><init>(Lcom/navdy/hud/app/maps/here/HereMapsManager;)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereMapController;->execute(Ljava/lang/Runnable;)V

    .line 986
    return-void
.end method

.method public clearTrafficOverlay()V
    .locals 0

    .prologue
    .line 877
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->clearMapTraffic()V

    .line 878
    return-void
.end method

.method public getBkLocationReceiverLooper()Landroid/os/Looper;
    .locals 1

    .prologue
    .line 1125
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->bkLocationReceiverHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    return-object v0
.end method

.method getEnrouteMapScheme()Ljava/lang/String;
    .locals 1

    .prologue
    .line 842
    const-string v0, "hybrid.night"

    return-object v0
.end method

.method public getError()Lcom/here/android/mpa/common/OnEngineInitListener$Error;
    .locals 1

    .prologue
    .line 783
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapError:Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    return-object v0
.end method

.method public getLastGeoPosition()Lcom/here/android/mpa/common/GeoPosition;
    .locals 1

    .prologue
    .line 834
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->lastGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

    return-object v0
.end method

.method public getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;
    .locals 1

    .prologue
    .line 1023
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->hereLocationFixManager:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    return-object v0
.end method

.method public getMapAnimator()Lcom/navdy/hud/app/maps/here/HereMapAnimator;
    .locals 1

    .prologue
    .line 1203
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->hereMapAnimator:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    return-object v0
.end method

.method public getMapController()Lcom/navdy/hud/app/maps/here/HereMapController;
    .locals 1

    .prologue
    .line 789
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    return-object v0
.end method

.method public getMapPackageCount()I
    .locals 1

    .prologue
    .line 1259
    iget v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapPackageCount:I

    return v0
.end method

.method public getMapView()Lcom/here/android/mpa/mapping/MapView;
    .locals 1

    .prologue
    .line 793
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapView:Lcom/here/android/mpa/mapping/MapView;

    return-object v0
.end method

.method public getOfflineMapsData()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 1130
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/storage/PathManager;->getInstance()Lcom/navdy/hud/app/storage/PathManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/storage/PathManager;->getHereMapsDataDirectory()Ljava/lang/String;

    move-result-object v1

    .line 1131
    .local v1, "hereMapsDataDirectory":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1132
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_1

    .line 1142
    .end local v0    # "dir":Ljava/io/File;
    .end local v1    # "hereMapsDataDirectory":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v4

    .line 1135
    .restart local v0    # "dir":Ljava/io/File;
    .restart local v1    # "hereMapsDataDirectory":Ljava/lang/String;
    :cond_1
    new-instance v2, Ljava/io/File;

    const-string v5, "meta.json"

    invoke-direct {v2, v0, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1136
    .local v2, "metaData":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1139
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/navdy/service/library/util/IOUtils;->convertFileToString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    goto :goto_0

    .line 1140
    .end local v0    # "dir":Ljava/io/File;
    .end local v1    # "hereMapsDataDirectory":Ljava/lang/String;
    .end local v2    # "metaData":Ljava/io/File;
    :catch_0
    move-exception v3

    .line 1141
    .local v3, "t":Ljava/lang/Throwable;
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v5, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public getOfflineMapsVersion()Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;
    .locals 1

    .prologue
    .line 398
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->offlineMapsVersion:Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;

    return-object v0
.end method

.method public getPositioningManager()Lcom/here/android/mpa/common/PositioningManager;
    .locals 1

    .prologue
    .line 881
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->positioningManager:Lcom/here/android/mpa/common/PositioningManager;

    return-object v0
.end method

.method public getRouteCalcEventHandler()Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;
    .locals 1

    .prologue
    .line 1026
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->routeCalcEventHandler:Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;

    return-object v0
.end method

.method public getRouteOptions()Lcom/here/android/mpa/routing/RouteOptions;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 810
    new-instance v1, Lcom/here/android/mpa/routing/RouteOptions;

    invoke-direct {v1}, Lcom/here/android/mpa/routing/RouteOptions;-><init>()V

    .line 811
    .local v1, "routeOptions":Lcom/here/android/mpa/routing/RouteOptions;
    sget-object v2, Lcom/here/android/mpa/routing/RouteOptions$TransportMode;->CAR:Lcom/here/android/mpa/routing/RouteOptions$TransportMode;

    invoke-virtual {v1, v2}, Lcom/here/android/mpa/routing/RouteOptions;->setTransportMode(Lcom/here/android/mpa/routing/RouteOptions$TransportMode;)Lcom/here/android/mpa/routing/RouteOptions;

    .line 813
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mDriverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/profile/DriverProfile;->getNavigationPreferences()Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    move-result-object v0

    .line 815
    .local v0, "preferences":Lcom/navdy/service/library/events/preferences/NavigationPreferences;
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$14;->$SwitchMap$com$navdy$service$library$events$preferences$NavigationPreferences$RoutingType:[I

    iget-object v3, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->routingType:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;

    invoke-virtual {v3}, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 822
    :goto_0
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v3, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowHighways:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/here/android/mpa/routing/RouteOptions;->setHighwaysAllowed(Z)Lcom/here/android/mpa/routing/RouteOptions;

    .line 823
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v3, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowTollRoads:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/here/android/mpa/routing/RouteOptions;->setTollRoadsAllowed(Z)Lcom/here/android/mpa/routing/RouteOptions;

    .line 824
    invoke-virtual {v1, v4}, Lcom/here/android/mpa/routing/RouteOptions;->setFerriesAllowed(Z)Lcom/here/android/mpa/routing/RouteOptions;

    .line 825
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v3, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowTunnels:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/here/android/mpa/routing/RouteOptions;->setTunnelsAllowed(Z)Lcom/here/android/mpa/routing/RouteOptions;

    .line 826
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v3, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowUnpavedRoads:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/here/android/mpa/routing/RouteOptions;->setDirtRoadsAllowed(Z)Lcom/here/android/mpa/routing/RouteOptions;

    .line 827
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v3, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowAutoTrains:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/here/android/mpa/routing/RouteOptions;->setCarShuttleTrainsAllowed(Z)Lcom/here/android/mpa/routing/RouteOptions;

    .line 828
    invoke-virtual {v1, v4}, Lcom/here/android/mpa/routing/RouteOptions;->setCarpoolAllowed(Z)Lcom/here/android/mpa/routing/RouteOptions;

    .line 830
    return-object v1

    .line 817
    :pswitch_0
    sget-object v2, Lcom/here/android/mpa/routing/RouteOptions$Type;->FASTEST:Lcom/here/android/mpa/routing/RouteOptions$Type;

    invoke-virtual {v1, v2}, Lcom/here/android/mpa/routing/RouteOptions;->setRouteType(Lcom/here/android/mpa/routing/RouteOptions$Type;)Lcom/here/android/mpa/routing/RouteOptions;

    goto :goto_0

    .line 820
    :pswitch_1
    sget-object v2, Lcom/here/android/mpa/routing/RouteOptions$Type;->SHORTEST:Lcom/here/android/mpa/routing/RouteOptions$Type;

    invoke-virtual {v1, v2}, Lcom/here/android/mpa/routing/RouteOptions;->setRouteType(Lcom/here/android/mpa/routing/RouteOptions$Type;)Lcom/here/android/mpa/routing/RouteOptions;

    goto :goto_0

    .line 815
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getRouteStartPoint()Lcom/here/android/mpa/common/GeoCoordinate;
    .locals 1

    .prologue
    .line 1096
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->routeStartPoint:Lcom/here/android/mpa/common/GeoCoordinate;

    return-object v0
.end method

.method getTrackingMapScheme()Ljava/lang/String;
    .locals 1

    .prologue
    .line 838
    const-string v0, "carnav.night"

    return-object v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1239
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1240
    invoke-static {}, Lcom/here/android/mpa/common/Version;->getSdkVersion()Ljava/lang/String;

    move-result-object v0

    .line 1242
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public handleBandwidthPreferenceChange()V
    .locals 1

    .prologue
    .line 1213
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1214
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->setBandwidthPreferences()V

    .line 1216
    :cond_0
    return-void
.end method

.method public hideTraffic()V
    .locals 3

    .prologue
    .line 1163
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1164
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getDriverProfileManager()Lcom/navdy/hud/app/profile/DriverProfileManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfileManager;->isTrafficEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1165
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereMapsManager$12;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager$12;-><init>(Lcom/navdy/hud/app/maps/here/HereMapsManager;)V

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 1183
    :cond_0
    :goto_0
    return-void

    .line 1180
    :cond_1
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "hidetraffic: traffic is not enabled"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public initNavigation()V
    .locals 1

    .prologue
    .line 1207
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1208
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getNavController()Lcom/navdy/hud/app/maps/here/HereNavController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavController;->initialize()V

    .line 1210
    :cond_0
    return-void
.end method

.method public declared-synchronized installPositionListener()V
    .locals 3

    .prologue
    .line 484
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->positioningManagerInstalled:Z

    if-nez v0, :cond_0

    .line 485
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->positioningManager:Lcom/here/android/mpa/common/PositioningManager;

    new-instance v1, Ljava/lang/ref/WeakReference;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->positionChangedListener:Lcom/here/android/mpa/common/PositioningManager$OnPositionChangedListener;

    invoke-direct {v1, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/common/PositioningManager;->addListener(Ljava/lang/ref/WeakReference;)V

    .line 486
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "position manager listener installed"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 487
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->positioningManagerInstalled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 489
    :cond_0
    monitor-exit p0

    return-void

    .line 484
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isEngineOnline()Z
    .locals 1

    .prologue
    .line 1005
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapEngine:Lcom/here/android/mpa/common/MapEngine;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/here/android/mpa/common/MapEngine;->isOnlineEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInitialized()Z
    .locals 2

    .prologue
    .line 771
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->initLock:Ljava/lang/Object;

    monitor-enter v1

    .line 772
    :try_start_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapInitialized:Z

    monitor-exit v1

    return v0

    .line 773
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public isInitializing()Z
    .locals 2

    .prologue
    .line 777
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->initLock:Ljava/lang/Object;

    monitor-enter v1

    .line 778
    :try_start_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapInitializing:Z

    monitor-exit v1

    return v0

    .line 779
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public isMapDataVerified()Z
    .locals 1

    .prologue
    .line 1255
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapDataVerified:Z

    return v0
.end method

.method public isNavigationModeOn()Z
    .locals 1

    .prologue
    .line 1219
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1220
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isNavigationModeOn()Z

    move-result v0

    .line 1222
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNavigationPossible()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1227
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1228
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->hereLocationFixManager:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1229
    const/4 v0, 0x1

    .line 1234
    :cond_0
    return v0
.end method

.method public isRecalcCurrentRouteForTrafficEnabled()Z
    .locals 1

    .prologue
    .line 1104
    sget-boolean v0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->recalcCurrentRouteForTraffic:Z

    return v0
.end method

.method public isRenderingAllowed()Z
    .locals 1

    .prologue
    .line 786
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->powerManager:Lcom/navdy/hud/app/device/PowerManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/device/PowerManager;->inQuietMode()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isVoiceSkinsLoaded()Z
    .locals 1

    .prologue
    .line 1247
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->voiceSkinsLoaded:Z

    return v0
.end method

.method public onLinkPropertiesChanged(Lcom/navdy/service/library/events/connection/LinkPropertiesChanged;)V
    .locals 3
    .param p1, "propertiesChanged"    # Lcom/navdy/service/library/events/connection/LinkPropertiesChanged;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1080
    if-eqz p1, :cond_0

    iget-object v1, p1, Lcom/navdy/service/library/events/connection/LinkPropertiesChanged;->bandwidthLevel:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 1081
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Link Properties changed"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1082
    iget-object v1, p1, Lcom/navdy/service/library/events/connection/LinkPropertiesChanged;->bandwidthLevel:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1083
    .local v0, "newLevel":I
    if-gtz v0, :cond_1

    .line 1084
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Switching to low bandwidth mode"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1085
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->lowBandwidthDetected:Z

    .line 1086
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->updateEngineOnlineState()V

    .line 1093
    .end local v0    # "newLevel":I
    :cond_0
    :goto_0
    return-void

    .line 1088
    .restart local v0    # "newLevel":I
    :cond_1
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Switching back to normal bandwidth mode"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1089
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->lowBandwidthDetected:Z

    .line 1090
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->updateEngineOnlineState()V

    goto :goto_0
.end method

.method public onWakeup(Lcom/navdy/hud/app/event/Wakeup;)V
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/event/Wakeup;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1073
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1074
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->startTrafficUpdater()V

    .line 1076
    :cond_0
    return-void
.end method

.method public postManeuverDisplay()V
    .locals 1

    .prologue
    .line 1119
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1120
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->postManeuverDisplay()V

    .line 1122
    :cond_0
    return-void
.end method

.method public requestMapsEngineOnlineStateChange(Z)V
    .locals 3
    .param p1, "turnOn"    # Z

    .prologue
    .line 1058
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Request to set online state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1059
    iput-boolean p1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->turnEngineOn:Z

    .line 1060
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->updateEngineOnlineState()V

    .line 1061
    return-void
.end method

.method public setMapView(Lcom/here/android/mpa/mapping/MapView;Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)V
    .locals 4
    .param p1, "mapView"    # Lcom/here/android/mpa/mapping/MapView;
    .param p2, "navigationView"    # Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    .prologue
    .line 797
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapView:Lcom/here/android/mpa/mapping/MapView;

    .line 798
    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->navigationView:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    .line 799
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->setMapView(Lcom/here/android/mpa/mapping/MapView;Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)V

    .line 800
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->hereMapAnimator:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->getAnimationMode()Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;

    move-result-object v0

    .line 801
    .local v0, "mode":Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;->NONE:Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;

    if-eq v0, v1, :cond_0

    .line 802
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "installing map render listener:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 803
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapView:Lcom/here/android/mpa/mapping/MapView;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->hereMapAnimator:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->getMapRenderListener()Lcom/here/android/mpa/mapping/OnMapRenderListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/here/android/mpa/mapping/MapView;->addOnMapRenderListener(Lcom/here/android/mpa/mapping/OnMapRenderListener;)V

    .line 807
    :goto_0
    return-void

    .line 805
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "not installing map render listener:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setRouteStartPoint(Lcom/here/android/mpa/common/GeoCoordinate;)V
    .locals 0
    .param p1, "geoCoordinate"    # Lcom/here/android/mpa/common/GeoCoordinate;

    .prologue
    .line 1100
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->routeStartPoint:Lcom/here/android/mpa/common/GeoCoordinate;

    .line 1101
    return-void
.end method

.method public setTrafficOverlay(Lcom/navdy/hud/app/maps/NavigationMode;)V
    .locals 1
    .param p1, "navigationMode"    # Lcom/navdy/hud/app/maps/NavigationMode;

    .prologue
    .line 869
    sget-object v0, Lcom/navdy/hud/app/maps/NavigationMode;->MAP:Lcom/navdy/hud/app/maps/NavigationMode;

    if-ne p1, v0, :cond_1

    .line 870
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->setMapTraffic()V

    .line 874
    :cond_0
    :goto_0
    return-void

    .line 871
    :cond_1
    sget-object v0, Lcom/navdy/hud/app/maps/NavigationMode;->MAP_ON_ROUTE:Lcom/navdy/hud/app/maps/NavigationMode;

    if-ne p1, v0, :cond_0

    .line 872
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->setMapOnRouteTraffic()V

    goto :goto_0
.end method

.method public setVoiceSkinsLoaded()V
    .locals 1

    .prologue
    .line 1251
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->voiceSkinsLoaded:Z

    .line 1252
    return-void
.end method

.method public showTraffic()V
    .locals 3

    .prologue
    .line 1186
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1187
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getDriverProfileManager()Lcom/navdy/hud/app/profile/DriverProfileManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfileManager;->isTrafficEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1188
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereMapsManager$13;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager$13;-><init>(Lcom/navdy/hud/app/maps/here/HereMapsManager;)V

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 1200
    :cond_0
    :goto_0
    return-void

    .line 1197
    :cond_1
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "showTraffic: traffic is not enabled"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public startMapRendering()V
    .locals 1

    .prologue
    .line 1147
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->hereMapAnimator:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    if-eqz v0, :cond_0

    .line 1148
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->hereMapAnimator:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->startMapRendering()V

    .line 1152
    :goto_0
    return-void

    .line 1150
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->initialMapRendering:Z

    goto :goto_0
.end method

.method public stopMapRendering()V
    .locals 1

    .prologue
    .line 1155
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->hereMapAnimator:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    if-eqz v0, :cond_0

    .line 1156
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->hereMapAnimator:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->stopMapRendering()V

    .line 1160
    :goto_0
    return-void

    .line 1158
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->initialMapRendering:Z

    goto :goto_0
.end method

.method public declared-synchronized turnOffline()V
    .locals 3

    .prologue
    .line 1048
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1049
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "turnOffline: engine not yet initialized"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1055
    :goto_0
    monitor-exit p0

    return-void

    .line 1052
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->requestMapsEngineOnlineStateChange(Z)V

    .line 1053
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->stopTrafficRerouteListener()V

    .line 1054
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isOnline:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/here/android/mpa/common/MapEngine;->isOnlineEnabled()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1048
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized turnOnline()V
    .locals 3

    .prologue
    .line 1029
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1030
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "turnOnline: engine not yet initialized"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1044
    :goto_0
    monitor-exit p0

    return-void

    .line 1033
    :cond_0
    :try_start_1
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "turnOnline: enabling traffic info"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 1034
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->requestMapsEngineOnlineStateChange(Z)V

    .line 1035
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->isTrafficNotificationEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1036
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "turnOnline: enabling traffic notifications"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 1037
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->startTrafficRerouteListener()V

    .line 1041
    :goto_1
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "turnOnline invokeMapLoader"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1042
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->invokeMapLoader()V

    .line 1043
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isOnline:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/here/android/mpa/common/MapEngine;->isOnlineEnabled()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1029
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1039
    :cond_1
    :try_start_2
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "turnOnline: not enabling traffic notifications, glance off"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public declared-synchronized updateEngineOnlineState()V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1064
    monitor-enter p0

    :try_start_0
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Updating the engine online state LowBandwidthDetected :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->lowBandwidthDetected:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", TurnEngineOn :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->turnEngineOn:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1065
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1066
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Turning engine on :"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->lowBandwidthDetected:Z

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->turnEngineOn:Z

    if-eqz v2, :cond_1

    move v2, v0

    :goto_0
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1067
    iget-boolean v2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->lowBandwidthDetected:Z

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager;->turnEngineOn:Z

    if-eqz v2, :cond_2

    :goto_1
    invoke-static {v0}, Lcom/here/android/mpa/common/MapEngine;->setOnline(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1069
    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    move v2, v1

    .line 1066
    goto :goto_0

    :cond_2
    move v0, v1

    .line 1067
    goto :goto_1

    .line 1064
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
