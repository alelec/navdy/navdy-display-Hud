.class final Lcom/navdy/hud/app/maps/here/HerePlacesManager$6;
.super Ljava/lang/Object;
.source "HerePlacesManager.java"

# interfaces
.implements Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HerePlacesManager;->handleCategoriesRequest(Lcom/here/android/mpa/search/CategoryFilter;ILcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$filter:Lcom/here/android/mpa/search/CategoryFilter;

.field final synthetic val$listener:Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;

.field final synthetic val$nResults:I


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;Lcom/here/android/mpa/search/CategoryFilter;I)V
    .locals 0

    .prologue
    .line 366
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$6;->val$listener:Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;

    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$6;->val$filter:Lcom/here/android/mpa/search/CategoryFilter;

    iput p3, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$6;->val$nResults:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/search/Place;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 369
    .local p1, "places":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/search/Place;>;"
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$6;->val$listener:Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;

    invoke-interface {v0, p1}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;->onCompleted(Ljava/util/List;)V

    .line 370
    return-void
.end method

.method public onError(Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;)V
    .locals 4
    .param p1, "error"    # Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

    .prologue
    .line 374
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$6;->val$filter:Lcom/here/android/mpa/search/CategoryFilter;

    iget v1, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$6;->val$nResults:I

    new-instance v2, Lcom/navdy/hud/app/maps/here/HerePlacesManager$6$1;

    invoke-direct {v2, p0}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$6$1;-><init>(Lcom/navdy/hud/app/maps/here/HerePlacesManager$6;)V

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->handleCategoriesRequest(Lcom/here/android/mpa/search/CategoryFilter;ILcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;Z)V

    .line 385
    return-void
.end method
