.class public final enum Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;
.super Ljava/lang/Enum;
.source "HerePlacesManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/here/HerePlacesManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Error"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

.field public static final enum BAD_REQUEST:Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

.field public static final enum NO_MAP_ENGINE:Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

.field public static final enum NO_ROUTES:Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

.field public static final enum NO_USER_LOCATION:Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

.field public static final enum RESPONSE_ERROR:Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

.field public static final enum UNKNOWN_ERROR:Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 95
    new-instance v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

    const-string v1, "BAD_REQUEST"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;->BAD_REQUEST:Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

    new-instance v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

    const-string v1, "RESPONSE_ERROR"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;->RESPONSE_ERROR:Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

    new-instance v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

    const-string v1, "NO_USER_LOCATION"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;->NO_USER_LOCATION:Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

    new-instance v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

    const-string v1, "NO_MAP_ENGINE"

    invoke-direct {v0, v1, v6}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;->NO_MAP_ENGINE:Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

    new-instance v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

    const-string v1, "NO_ROUTES"

    invoke-direct {v0, v1, v7}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;->NO_ROUTES:Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

    new-instance v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

    const-string v1, "UNKNOWN_ERROR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;->UNKNOWN_ERROR:Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

    .line 94
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;->BAD_REQUEST:Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;->RESPONSE_ERROR:Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;->NO_USER_LOCATION:Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;->NO_MAP_ENGINE:Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;->NO_ROUTES:Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;->UNKNOWN_ERROR:Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;->$VALUES:[Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 94
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 94
    const-class v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;
    .locals 1

    .prologue
    .line 94
    sget-object v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;->$VALUES:[Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

    return-object v0
.end method
