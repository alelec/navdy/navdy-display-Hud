.class Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;
.super Ljava/lang/Object;
.source "HerePlacesManager.java"

# interfaces
.implements Lcom/navdy/hud/app/maps/here/HerePlacesManager$PlacesSearchCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HerePlacesManager$4;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HerePlacesManager$4;

.field final synthetic val$geoPosition:Lcom/here/android/mpa/common/GeoCoordinate;

.field final synthetic val$l1:J

.field final synthetic val$maxResults:I

.field final synthetic val$searchArea:I

.field final synthetic val$searchQuery:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HerePlacesManager$4;Ljava/lang/String;Lcom/here/android/mpa/common/GeoCoordinate;IIJ)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HerePlacesManager$4;

    .prologue
    .line 225
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;->this$0:Lcom/navdy/hud/app/maps/here/HerePlacesManager$4;

    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;->val$searchQuery:Ljava/lang/String;

    iput-object p3, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;->val$geoPosition:Lcom/here/android/mpa/common/GeoCoordinate;

    iput p4, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;->val$maxResults:I

    iput p5, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;->val$searchArea:I

    iput-wide p6, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;->val$l1:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public result(Lcom/here/android/mpa/search/ErrorCode;Lcom/here/android/mpa/search/DiscoveryResultPage;)V
    .locals 3
    .param p1, "errorCode"    # Lcom/here/android/mpa/search/ErrorCode;
    .param p2, "result"    # Lcom/here/android/mpa/search/DiscoveryResultPage;

    .prologue
    .line 229
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1$1;-><init>(Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;Lcom/here/android/mpa/search/ErrorCode;Lcom/here/android/mpa/search/DiscoveryResultPage;)V

    const/16 v2, 0x13

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 261
    return-void
.end method
