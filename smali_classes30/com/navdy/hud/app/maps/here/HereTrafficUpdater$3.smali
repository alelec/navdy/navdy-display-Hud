.class Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$3;
.super Ljava/lang/Object;
.source "HereTrafficUpdater.java"

# interfaces
.implements Lcom/here/android/mpa/guidance/TrafficUpdater$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$3;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onStatusChanged(Lcom/here/android/mpa/guidance/TrafficUpdater$RequestState;)V
    .locals 3
    .param p1, "requestState"    # Lcom/here/android/mpa/guidance/TrafficUpdater$RequestState;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$3;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentRoute:Lcom/here/android/mpa/routing/Route;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->access$200(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;)Lcom/here/android/mpa/routing/Route;

    move-result-object v0

    if-nez v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$3;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->access$400(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$3;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->tag:Ljava/lang/String;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->access$300(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "onRequestListener triggered and currentRoute is null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 87
    :goto_0
    return-void

    .line 81
    :cond_0
    sget-object v0, Lcom/here/android/mpa/guidance/TrafficUpdater$RequestState;->DONE:Lcom/here/android/mpa/guidance/TrafficUpdater$RequestState;

    if-ne p1, v0, :cond_1

    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$3;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->trafficUpdater:Lcom/here/android/mpa/guidance/TrafficUpdater;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->access$600(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;)Lcom/here/android/mpa/guidance/TrafficUpdater;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$3;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentRoute:Lcom/here/android/mpa/routing/Route;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->access$200(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;)Lcom/here/android/mpa/routing/Route;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$3;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->onGetEventsListener:Lcom/here/android/mpa/guidance/TrafficUpdater$GetEventsListener;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->access$500(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;)Lcom/here/android/mpa/guidance/TrafficUpdater$GetEventsListener;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/here/android/mpa/guidance/TrafficUpdater;->getEvents(Lcom/here/android/mpa/routing/Route;Lcom/here/android/mpa/guidance/TrafficUpdater$GetEventsListener;)V

    goto :goto_0

    .line 84
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$3;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->access$400(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$3;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->tag:Ljava/lang/String;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->access$300(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " error on completion of TrafficUpdater.request"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 85
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$3;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;

    # invokes: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->refresh()V
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->access$700(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;)V

    goto :goto_0
.end method
