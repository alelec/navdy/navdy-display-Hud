.class Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;
.super Ljava/lang/Object;
.source "HereMapsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->onEngineInitializationCompleted(Lcom/here/android/mpa/common/OnEngineInitListener$Error;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

.field final synthetic val$t1:J


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereMapsManager$1;J)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    .prologue
    .line 228
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    iput-wide p2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->val$t1:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    const-wide v12, 0x4042e6aac1094a2cL    # 37.802086

    const-wide v10, -0x3fa1652edbb59ddcL    # -122.419015

    .line 232
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    iget-object v6, v6, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapEngine:Lcom/here/android/mpa/common/MapEngine;
    invoke-static {v6}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$200(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Lcom/here/android/mpa/common/MapEngine;

    move-result-object v6

    invoke-virtual {v6}, Lcom/here/android/mpa/common/MapEngine;->onResume()V

    .line 235
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    const-string v7, ":initializing maps config"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 236
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsConfigurator;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsConfigurator;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/hud/app/maps/here/HereMapsConfigurator;->updateMapsConfig()V

    .line 239
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    const-string v7, "creating geopos"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 240
    new-instance v2, Lcom/here/android/mpa/common/GeoPosition;

    new-instance v6, Lcom/here/android/mpa/common/GeoCoordinate;

    invoke-direct {v6, v12, v13, v10, v11}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    invoke-direct {v2, v6}, Lcom/here/android/mpa/common/GeoPosition;-><init>(Lcom/here/android/mpa/common/GeoCoordinate;)V

    .line 241
    .local v2, "pos":Lcom/here/android/mpa/common/GeoPosition;
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    const-string v7, "created geopos"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 244
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isUserBuild()Z

    move-result v6

    if-nez v6, :cond_0

    .line 245
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 246
    .local v0, "l1":J
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->init()V

    .line 247
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "time to init HereLaneInfoBuilder ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    sub-long/2addr v8, v0

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 250
    .end local v0    # "l1":J
    :cond_0
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "engine initialized refcount:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    iget-object v8, v8, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapEngine:Lcom/here/android/mpa/common/MapEngine;
    invoke-static {v8}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$200(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Lcom/here/android/mpa/common/MapEngine;

    move-result-object v8

    invoke-virtual {v8}, Lcom/here/android/mpa/common/MapEngine;->getResourceReferenceCount()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 255
    :try_start_0
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    iget-object v6, v6, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    new-instance v7, Lcom/here/android/mpa/mapping/Map;

    invoke-direct {v7}, Lcom/here/android/mpa/mapping/Map;-><init>()V

    # setter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->map:Lcom/here/android/mpa/mapping/Map;
    invoke-static {v6, v7}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$302(Lcom/navdy/hud/app/maps/here/HereMapsManager;Lcom/here/android/mpa/mapping/Map;)Lcom/here/android/mpa/mapping/Map;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 271
    :goto_0
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    iget-object v6, v6, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->map:Lcom/here/android/mpa/mapping/Map;
    invoke-static {v6}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$300(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Lcom/here/android/mpa/mapping/Map;

    move-result-object v6

    sget-object v7, Lcom/here/android/mpa/mapping/Map$Projection;->MERCATOR:Lcom/here/android/mpa/mapping/Map$Projection;

    invoke-virtual {v6, v7}, Lcom/here/android/mpa/mapping/Map;->setProjectionMode(Lcom/here/android/mpa/mapping/Map$Projection;)Lcom/here/android/mpa/mapping/Map;

    .line 272
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    iget-object v6, v6, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->map:Lcom/here/android/mpa/mapping/Map;
    invoke-static {v6}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$300(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Lcom/here/android/mpa/mapping/Map;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/here/android/mpa/mapping/Map;->setTrafficInfoVisible(Z)Lcom/here/android/mpa/mapping/Map;

    .line 273
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    iget-object v6, v6, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    new-instance v7, Lcom/navdy/hud/app/maps/here/HereMapController;

    iget-object v8, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    iget-object v8, v8, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->map:Lcom/here/android/mpa/mapping/Map;
    invoke-static {v8}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$300(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Lcom/here/android/mpa/mapping/Map;

    move-result-object v8

    sget-object v9, Lcom/navdy/hud/app/maps/here/HereMapController$State;->NONE:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    invoke-direct {v7, v8, v9}, Lcom/navdy/hud/app/maps/here/HereMapController;-><init>(Lcom/here/android/mpa/mapping/Map;Lcom/navdy/hud/app/maps/here/HereMapController$State;)V

    # setter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v6, v7}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$502(Lcom/navdy/hud/app/maps/here/HereMapsManager;Lcom/navdy/hud/app/maps/here/HereMapController;)Lcom/navdy/hud/app/maps/here/HereMapController;

    .line 275
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    iget-object v6, v6, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    new-instance v7, Lcom/here/android/mpa/common/GeoCoordinate;

    invoke-direct {v7, v12, v13, v10, v11}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    # invokes: Lcom/navdy/hud/app/maps/here/HereMapsManager;->setMapAttributes(Lcom/here/android/mpa/common/GeoCoordinate;)V
    invoke-static {v6, v7}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$600(Lcom/navdy/hud/app/maps/here/HereMapsManager;Lcom/here/android/mpa/common/GeoCoordinate;)V

    .line 276
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    const-string v7, "setting default map attributes"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 279
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    iget-object v6, v6, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # invokes: Lcom/navdy/hud/app/maps/here/HereMapsManager;->setMapTraffic()V
    invoke-static {v6}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$700(Lcom/navdy/hud/app/maps/here/HereMapsManager;)V

    .line 282
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    iget-object v6, v6, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    iget-object v7, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    iget-object v7, v7, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->map:Lcom/here/android/mpa/mapping/Map;
    invoke-static {v7}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$300(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Lcom/here/android/mpa/mapping/Map;

    move-result-object v7

    # invokes: Lcom/navdy/hud/app/maps/here/HereMapsManager;->setMapPoiLayer(Lcom/here/android/mpa/mapping/Map;)V
    invoke-static {v6, v7}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$800(Lcom/navdy/hud/app/maps/here/HereMapsManager;Lcom/here/android/mpa/mapping/Map;)V

    .line 285
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    iget-object v6, v6, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-static {}, Lcom/here/android/mpa/common/PositioningManager;->getInstance()Lcom/here/android/mpa/common/PositioningManager;

    move-result-object v7

    # setter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->positioningManager:Lcom/here/android/mpa/common/PositioningManager;
    invoke-static {v6, v7}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$902(Lcom/navdy/hud/app/maps/here/HereMapsManager;Lcom/here/android/mpa/common/PositioningManager;)Lcom/here/android/mpa/common/PositioningManager;

    .line 287
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "MAP-ENGINE-INIT-2 took ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    iget-wide v10, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->val$t1:J

    sub-long/2addr v8, v10

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 288
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 290
    .local v4, "t2":J
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    iget-object v6, v6, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->handler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$400(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Landroid/os/Handler;

    move-result-object v6

    new-instance v7, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;

    invoke-direct {v7, p0, v4, v5}, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;-><init>(Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;J)V

    invoke-virtual {v6, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 368
    return-void

    .line 256
    .end local v4    # "t2":J
    :catch_0
    move-exception v3

    .line 257
    .local v3, "t":Ljava/lang/Throwable;
    invoke-virtual {v3}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Invalid configuration file. Check MWConfig!"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 258
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    const-string v7, "MWConfig is corrupted! Cleaning up and restarting HUD app."

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 259
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsConfigurator;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsConfigurator;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/hud/app/maps/here/HereMapsConfigurator;->removeMWConfigFolder()V

    .line 262
    :cond_1
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    iget-object v6, v6, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->handler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$400(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Landroid/os/Handler;

    move-result-object v6

    new-instance v7, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$1;

    invoke-direct {v7, p0, v3}, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$1;-><init>(Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;Ljava/lang/Throwable;)V

    invoke-virtual {v6, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0
.end method
