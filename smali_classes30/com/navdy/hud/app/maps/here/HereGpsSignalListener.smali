.class public Lcom/navdy/hud/app/maps/here/HereGpsSignalListener;
.super Lcom/here/android/mpa/guidance/NavigationManager$GpsSignalListener;
.source "HereGpsSignalListener.java"


# instance fields
.field private bus:Lcom/squareup/otto/Bus;

.field private hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

.field private logger:Lcom/navdy/service/library/log/Logger;

.field private mapsEventHandler:Lcom/navdy/hud/app/maps/MapsEventHandler;

.field private tag:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/service/library/log/Logger;Ljava/lang/String;Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/maps/MapsEventHandler;Lcom/navdy/hud/app/maps/here/HereNavigationManager;)V
    .locals 0
    .param p1, "logger"    # Lcom/navdy/service/library/log/Logger;
    .param p2, "tag"    # Ljava/lang/String;
    .param p3, "bus"    # Lcom/squareup/otto/Bus;
    .param p4, "mapsEventHandler"    # Lcom/navdy/hud/app/maps/MapsEventHandler;
    .param p5, "hereNavigationManager"    # Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/here/android/mpa/guidance/NavigationManager$GpsSignalListener;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereGpsSignalListener;->logger:Lcom/navdy/service/library/log/Logger;

    .line 23
    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereGpsSignalListener;->tag:Ljava/lang/String;

    .line 24
    iput-object p3, p0, Lcom/navdy/hud/app/maps/here/HereGpsSignalListener;->bus:Lcom/squareup/otto/Bus;

    .line 25
    iput-object p4, p0, Lcom/navdy/hud/app/maps/here/HereGpsSignalListener;->mapsEventHandler:Lcom/navdy/hud/app/maps/MapsEventHandler;

    .line 26
    iput-object p5, p0, Lcom/navdy/hud/app/maps/here/HereGpsSignalListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    .line 27
    return-void
.end method


# virtual methods
.method public onGpsLost()V
    .locals 3

    .prologue
    .line 31
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereGpsSignalListener;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereGpsSignalListener;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Gps signal lost"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 32
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereGpsSignalListener;->bus:Lcom/squareup/otto/Bus;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereGpsSignalListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    iget-object v1, v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->BUS_GPS_SIGNAL_LOST:Lcom/navdy/hud/app/maps/MapEvents$GpsStatusChange;

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 33
    return-void
.end method

.method public onGpsRestored()V
    .locals 3

    .prologue
    .line 37
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereGpsSignalListener;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereGpsSignalListener;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Gps signal restored"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 38
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereGpsSignalListener;->bus:Lcom/squareup/otto/Bus;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereGpsSignalListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    iget-object v1, v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->BUS_GPS_SIGNAL_RESTORED:Lcom/navdy/hud/app/maps/MapEvents$GpsStatusChange;

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 39
    return-void
.end method
