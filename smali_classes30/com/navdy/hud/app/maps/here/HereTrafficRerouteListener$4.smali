.class Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;
.super Ljava/lang/Object;
.source "HereTrafficRerouteListener.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->handleFasterRoute(Lcom/here/android/mpa/routing/Route;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

.field final synthetic val$fasterRoute:Lcom/here/android/mpa/routing/Route;

.field final synthetic val$notifFromHereTraffic:Z


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;ZLcom/here/android/mpa/routing/Route;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    .prologue
    .line 290
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    iput-boolean p2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->val$notifFromHereTraffic:Z

    iput-object p3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->val$fasterRoute:Lcom/here/android/mpa/routing/Route;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 52

    .prologue
    .line 294
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->val$notifFromHereTraffic:Z

    if-eqz v4, :cond_0

    const-string v33, "[Here Traffic]"

    .line 295
    .local v33, "fromTag":Ljava/lang/String;
    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$100(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->tag:Ljava/lang/String;
    invoke-static {v7}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$500(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v33

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " onTrafficRerouted"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 296
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->dismissReroute()V

    .line 298
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$300(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isNavigationModeOn()Z

    move-result v4

    if-nez v4, :cond_1

    .line 299
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$100(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->tag:Ljava/lang/String;
    invoke-static {v7}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$500(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " onTrafficRerouted, not currently navigating"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 463
    .end local v33    # "fromTag":Ljava/lang/String;
    :goto_1
    return-void

    .line 294
    :cond_0
    const-string v33, "[Navdy Calc]"

    goto :goto_0

    .line 303
    .restart local v33    # "fromTag":Ljava/lang/String;
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$300(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hasArrived()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 304
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$100(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v6, "onTrafficRerouted, arrival mode on"

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 459
    .end local v33    # "fromTag":Ljava/lang/String;
    :catch_0
    move-exception v50

    .line 460
    .local v50, "t":Ljava/lang/Throwable;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$100(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    move-object/from16 v0, v50

    invoke-virtual {v4, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 461
    invoke-static {}, Lcom/navdy/hud/app/util/CrashReporter;->getInstance()Lcom/navdy/hud/app/util/CrashReporter;

    move-result-object v4

    move-object/from16 v0, v50

    invoke-virtual {v4, v0}, Lcom/navdy/hud/app/util/CrashReporter;->reportNonFatalException(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 308
    .end local v50    # "t":Ljava/lang/Throwable;
    .restart local v33    # "fromTag":Ljava/lang/String;
    :cond_2
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$300(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isOnGasRoute()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 309
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$100(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v6, "onTrafficRerouted, on fas route"

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_1

    .line 313
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$300(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentRoute()Lcom/here/android/mpa/routing/Route;

    move-result-object v5

    .line 314
    .local v5, "currentRoute":Lcom/here/android/mpa/routing/Route;
    if-nez v5, :cond_4

    .line 315
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$100(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->tag:Ljava/lang/String;
    invoke-static {v7}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$500(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " onTrafficRerouted, not current route"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 319
    :cond_4
    invoke-virtual {v5}, Lcom/here/android/mpa/routing/Route;->getManeuvers()Ljava/util/List;

    move-result-object v24

    .line 321
    .local v24, "currentManeuvers":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->val$notifFromHereTraffic:Z

    if-eqz v4, :cond_5

    .line 322
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->val$fasterRoute:Lcom/here/android/mpa/routing/Route;

    invoke-virtual {v4}, Lcom/here/android/mpa/routing/Route;->getManeuvers()Ljava/util/List;

    move-result-object v4

    const/4 v6, 0x0

    invoke-static {v4, v6}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getAllRoadNames(Ljava/util/List;I)Ljava/lang/String;

    move-result-object v25

    .line 323
    .local v25, "currentRoadNames":Ljava/lang/String;
    const/4 v4, 0x0

    move-object/from16 v0, v24

    invoke-static {v0, v4}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getAllRoadNames(Ljava/util/List;I)Ljava/lang/String;

    move-result-object v36

    .line 324
    .local v36, "newRoadNames":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$100(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v6, "[HereNotif-RoadNames-current]"

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 325
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$100(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    move-object/from16 v0, v25

    invoke-virtual {v4, v0}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 326
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$100(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v6, "[HereNotif-RoadNames-faster]"

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 327
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$100(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    move-object/from16 v0, v36

    invoke-virtual {v4, v0}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 331
    .end local v25    # "currentRoadNames":Ljava/lang/String;
    .end local v36    # "newRoadNames":Ljava/lang/String;
    :cond_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->val$fasterRoute:Lcom/here/android/mpa/routing/Route;

    sget-object v6, Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;->OPTIMAL:Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    const v7, 0xfffffff

    invoke-virtual {v4, v6, v7}, Lcom/here/android/mpa/routing/Route;->getTta(Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;I)Lcom/here/android/mpa/routing/RouteTta;

    move-result-object v17

    .line 332
    .local v17, "fasterRouteTta":Lcom/here/android/mpa/routing/RouteTta;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->val$fasterRoute:Lcom/here/android/mpa/routing/Route;

    sget-object v6, Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;->DISABLED:Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    const v7, 0xfffffff

    invoke-virtual {v4, v6, v7}, Lcom/here/android/mpa/routing/Route;->getTta(Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;I)Lcom/here/android/mpa/routing/RouteTta;

    move-result-object v32

    .line 333
    .local v32, "fasterRouteTtaNoTraffic":Lcom/here/android/mpa/routing/RouteTta;
    if-eqz v17, :cond_6

    if-nez v32, :cond_7

    .line 334
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$100(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->tag:Ljava/lang/String;
    invoke-static {v7}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$500(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "error retrieving navigation and TTA"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 338
    :cond_7
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->val$notifFromHereTraffic:Z

    if-nez v4, :cond_8

    .line 341
    invoke-virtual/range {v17 .. v17}, Lcom/here/android/mpa/routing/RouteTta;->getDuration()I

    move-result v4

    int-to-long v0, v4

    move-wide/from16 v28, v0

    .line 342
    .local v28, "d1":J
    invoke-virtual/range {v32 .. v32}, Lcom/here/android/mpa/routing/RouteTta;->getDuration()I

    move-result v4

    int-to-long v0, v4

    move-wide/from16 v30, v0

    .line 344
    .local v30, "d2":J
    cmp-long v4, v28, v30

    if-nez v4, :cond_8

    .line 345
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$100(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->tag:Ljava/lang/String;
    invoke-static {v7}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$500(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "faster route durations is same="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v28

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 350
    .end local v28    # "d1":J
    .end local v30    # "d2":J
    :cond_8
    const-wide/16 v46, 0x0

    .line 351
    .local v46, "oldTtaDuration":J
    sget-object v4, Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;->OPTIMAL:Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    const v6, 0xfffffff

    invoke-virtual {v5, v4, v6}, Lcom/here/android/mpa/routing/Route;->getTta(Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;I)Lcom/here/android/mpa/routing/RouteTta;

    move-result-object v26

    .line 352
    .local v26, "currentRouteTta":Lcom/here/android/mpa/routing/RouteTta;
    if-eqz v26, :cond_9

    .line 353
    invoke-virtual/range {v26 .. v26}, Lcom/here/android/mpa/routing/RouteTta;->getDuration()I

    move-result v4

    int-to-long v0, v4

    move-wide/from16 v46, v0

    .line 357
    :cond_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->navController:Lcom/navdy/hud/app/maps/here/HereNavController;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$000(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/navdy/hud/app/maps/here/HereNavController;

    move-result-object v4

    const/4 v6, 0x1

    sget-object v7, Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;->OPTIMAL:Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    invoke-virtual {v4, v6, v7}, Lcom/navdy/hud/app/maps/here/HereNavController;->getEta(ZLcom/here/android/mpa/routing/Route$TrafficPenaltyMode;)Ljava/util/Date;

    move-result-object v27

    .line 358
    .local v27, "etaDate":Ljava/util/Date;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->isValidEtaDate(Ljava/util/Date;)Z

    move-result v4

    if-nez v4, :cond_a

    .line 359
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$100(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->tag:Ljava/lang/String;
    invoke-static {v7}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$500(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "error retrieving eta for current"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 364
    :cond_a
    invoke-virtual/range {v27 .. v27}, Ljava/util/Date;->getTime()J

    move-result-wide v12

    .line 365
    .local v12, "etaUtc":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v40

    .line 366
    .local v40, "now":J
    sub-long v6, v12, v40

    const-wide/16 v18, 0x3e8

    div-long v44, v6, v18

    .line 367
    .local v44, "oldTta":J
    invoke-virtual/range {v17 .. v17}, Lcom/here/android/mpa/routing/RouteTta;->getDuration()I

    move-result v4

    int-to-long v0, v4

    move-wide/from16 v38, v0

    .line 368
    .local v38, "newTta":J
    sub-long v9, v44, v38

    .line 371
    .local v9, "diff":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$100(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "etaDate["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v27

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] etaUtc["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] currentTta["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v44

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] now["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v40

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] newTta["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v38

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] diff["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] oldTtaDuration["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v46

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 380
    new-instance v48, Ljava/util/HashSet;

    invoke-direct/range {v48 .. v48}, Ljava/util/HashSet;-><init>()V

    .line 381
    .local v48, "seenVia":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    new-instance v42, Ljava/util/ArrayList;

    const/4 v4, 0x1

    move-object/from16 v0, v42

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 382
    .local v42, "oldRouteDifferentManeuver":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->val$fasterRoute:Lcom/here/android/mpa/routing/Route;

    const/4 v6, 0x2

    move-object/from16 v0, v42

    invoke-static {v4, v5, v6, v0}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getDifferentManeuver(Lcom/here/android/mpa/routing/Route;Lcom/here/android/mpa/routing/Route;ILjava/util/List;)Ljava/util/List;

    move-result-object v37

    .line 383
    .local v37, "newRouteDifferentManeuver":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    invoke-interface/range {v37 .. v37}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_b

    .line 384
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$100(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v6, "could not find a difference in maneuvers"

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 389
    :cond_b
    const/4 v8, 0x0

    .line 390
    .local v8, "via":Ljava/lang/String;
    new-instance v51, Ljava/lang/StringBuilder;

    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    .line 391
    .local v51, "viaBuilder":Ljava/lang/StringBuilder;
    invoke-interface/range {v37 .. v37}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_c
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_e

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/here/android/mpa/routing/Maneuver;

    .line 392
    .local v34, "m":Lcom/here/android/mpa/routing/Maneuver;
    invoke-static/range {v34 .. v34}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getNextRoadName(Lcom/here/android/mpa/routing/Maneuver;)Ljava/lang/String;

    move-result-object v49

    .line 393
    .local v49, "str":Ljava/lang/String;
    if-nez v8, :cond_d

    .line 394
    move-object/from16 v8, v49

    .line 395
    move-object/from16 v0, v51

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 396
    move-object/from16 v0, v48

    invoke-virtual {v0, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 398
    :cond_d
    invoke-virtual/range {v48 .. v49}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_c

    .line 399
    const-string v6, ","

    move-object/from16 v0, v51

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 400
    const-string v6, " "

    move-object/from16 v0, v51

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 401
    move-object/from16 v0, v51

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 402
    invoke-virtual/range {v48 .. v49}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 408
    .end local v34    # "m":Lcom/here/android/mpa/routing/Maneuver;
    .end local v49    # "str":Ljava/lang/String;
    :cond_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->val$fasterRoute:Lcom/here/android/mpa/routing/Route;

    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator;->getViaString(Lcom/here/android/mpa/routing/Route;)Ljava/lang/String;

    move-result-object v49

    .line 409
    .restart local v49    # "str":Ljava/lang/String;
    invoke-virtual/range {v48 .. v49}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_f

    .line 410
    const-string v4, ","

    move-object/from16 v0, v51

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 411
    const-string v4, " "

    move-object/from16 v0, v51

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 412
    move-object/from16 v0, v51

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 415
    :cond_f
    const/16 v16, 0x0

    .line 416
    .local v16, "currentVia":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getInstance()Lcom/navdy/hud/app/maps/here/HereRouteCache;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v6}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$300(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentRouteId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getRoute(Ljava/lang/String;)Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;

    move-result-object v43

    .line 417
    .local v43, "routeInfo":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    if-eqz v43, :cond_10

    .line 418
    move-object/from16 v0, v43

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->routeResult:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    iget-object v0, v4, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->via:Ljava/lang/String;

    move-object/from16 v16, v0

    .line 421
    :cond_10
    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 422
    .local v11, "additionalVia":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->val$fasterRoute:Lcom/here/android/mpa/routing/Route;

    invoke-virtual {v4}, Lcom/here/android/mpa/routing/Route;->getLength()I

    move-result v4

    int-to-long v6, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->navController:Lcom/navdy/hud/app/maps/here/HereNavController;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$000(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/navdy/hud/app/maps/here/HereNavController;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereNavController;->getDestinationDistance()J

    move-result-wide v18

    sub-long v14, v6, v18

    .line 424
    .local v14, "distanceDiff":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->getRerouteMinDuration()I

    move-result v35

    .line 426
    .local v35, "minDuration":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$100(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->tag:Ljava/lang/String;
    invoke-static {v7}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$500(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v33

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " new route via["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] additionalVia["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] is faster by["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] seconds than current route via["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] distanceDiff ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] minDur["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v35

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 431
    move/from16 v0, v35

    int-to-long v6, v0

    cmp-long v4, v9, v6

    if-ltz v4, :cond_12

    .line 433
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$100(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "faster route is above threshold:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 436
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    move-object/from16 v0, v24

    move-object/from16 v1, v42

    move-object/from16 v2, v37

    # invokes: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->isFasterRouteDivergePointCloseBy(Ljava/util/List;Ljava/util/List;Ljava/util/List;)Z
    invoke-static {v4, v0, v1, v2}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$600(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Z

    move-result v4

    if-nez v4, :cond_11

    .line 437
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$100(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v6, "faster route not near by, threshold not met"

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 440
    :cond_11
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    .end local v5    # "currentRoute":Lcom/here/android/mpa/routing/Route;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->val$fasterRoute:Lcom/here/android/mpa/routing/Route;

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->val$notifFromHereTraffic:Z

    const-wide/16 v16, 0x0

    invoke-virtual/range {v5 .. v17}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->setFasterRoute(Lcom/here/android/mpa/routing/Route;ZLjava/lang/String;JLjava/lang/String;JJJ)V

    goto/16 :goto_1

    .line 442
    .restart local v5    # "currentRoute":Lcom/here/android/mpa/routing/Route;
    :cond_12
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$100(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "faster route is below threshold:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 443
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isRecalcCurrentRouteForTrafficEnabled()Z

    move-result v4

    if-eqz v4, :cond_14

    .line 445
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    move-object/from16 v0, v24

    move-object/from16 v1, v42

    move-object/from16 v2, v37

    # invokes: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->isFasterRouteDivergePointCloseBy(Ljava/util/List;Ljava/util/List;Ljava/util/List;)Z
    invoke-static {v4, v0, v1, v2}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$600(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Z

    move-result v4

    if-nez v4, :cond_13

    .line 446
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$100(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v6, "faster route not near by, threshold not met"

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 453
    :cond_13
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->navdyTrafficRerouteManager:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$700(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->val$fasterRoute:Lcom/here/android/mpa/routing/Route;

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->val$notifFromHereTraffic:Z

    invoke-virtual/range {v4 .. v17}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->reCalculateCurrentRoute(Lcom/here/android/mpa/routing/Route;Lcom/here/android/mpa/routing/Route;ZLjava/lang/String;JLjava/lang/String;JJLjava/lang/String;Lcom/here/android/mpa/routing/RouteTta;)V

    goto/16 :goto_1

    .line 456
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;->val$notifFromHereTraffic:Z

    move/from16 v19, v0

    move-wide/from16 v20, v9

    move-object/from16 v22, v8

    move-object/from16 v23, v16

    invoke-virtual/range {v18 .. v23}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->fasterRouteNotFound(ZJLjava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method
