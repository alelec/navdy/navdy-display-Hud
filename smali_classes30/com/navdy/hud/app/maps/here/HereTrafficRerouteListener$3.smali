.class Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$3;
.super Ljava/lang/Object;
.source "HereTrafficRerouteListener.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->confirmReroute()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 113
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->currentProposedRoute:Lcom/here/android/mpa/routing/Route;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$200(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/here/android/mpa/routing/Route;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 114
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$300(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->clearNavManeuver()V

    .line 115
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->navController:Lcom/navdy/hud/app/maps/here/HereNavController;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$000(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/navdy/hud/app/maps/here/HereNavController;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->currentProposedRoute:Lcom/here/android/mpa/routing/Route;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$200(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/here/android/mpa/routing/Route;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereNavController;->setRoute(Lcom/here/android/mpa/routing/Route;)Lcom/here/android/mpa/guidance/NavigationManager$Error;

    move-result-object v7

    .line 116
    .local v7, "error":Lcom/here/android/mpa/guidance/NavigationManager$Error;
    sget-object v0, Lcom/here/android/mpa/guidance/NavigationManager$Error;->NONE:Lcom/here/android/mpa/guidance/NavigationManager$Error;

    if-ne v7, v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$300(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    .line 118
    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->currentProposedRoute:Lcom/here/android/mpa/routing/Route;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$200(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/here/android/mpa/routing/Route;

    move-result-object v1

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;->NAV_SESSION_TRAFFIC_REROUTE:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v4, v3

    .line 117
    invoke-virtual/range {v0 .. v6}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->updateMapRoute(Lcom/here/android/mpa/routing/Route;Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 124
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$100(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "confirmReroute: set"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 128
    :goto_0
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # invokes: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->clearProposedRoute()V
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$400(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)V

    .line 132
    .end local v7    # "error":Lcom/here/android/mpa/guidance/NavigationManager$Error;
    :goto_1
    return-void

    .line 126
    .restart local v7    # "error":Lcom/here/android/mpa/guidance/NavigationManager$Error;
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$100(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "confirmReroute: traffic route could not be set:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 130
    .end local v7    # "error":Lcom/here/android/mpa/guidance/NavigationManager$Error;
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->access$100(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "confirmReroute: no route available"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_1
.end method
