.class Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager$3;
.super Ljava/lang/Object;
.source "HereSpeedWarningManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 96
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 97
    .local v0, "now":J
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->lastUpdate:J
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->access$200(Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;)J

    move-result-wide v4

    sub-long v4, v0, v4

    const-wide/16 v6, 0x3e8

    cmp-long v3, v4, v6

    if-gez v3, :cond_0

    .line 105
    .end local v0    # "now":J
    :goto_0
    return-void

    .line 101
    .restart local v0    # "now":J
    :cond_0
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;

    # invokes: Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->checkSpeed()V
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->access$300(Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 102
    .end local v0    # "now":J
    :catch_0
    move-exception v2

    .line 103
    .local v2, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
