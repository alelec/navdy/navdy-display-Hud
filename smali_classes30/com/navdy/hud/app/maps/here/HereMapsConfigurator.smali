.class Lcom/navdy/hud/app/maps/here/HereMapsConfigurator;
.super Ljava/lang/Object;
.source "HereMapsConfigurator.java"


# static fields
.field private static final sInstance:Lcom/navdy/hud/app/maps/here/HereMapsConfigurator;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private volatile isAlreadyConfigured:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/maps/here/HereMapsConfigurator;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereMapsConfigurator;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 28
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereMapsConfigurator;

    invoke-direct {v0}, Lcom/navdy/hud/app/maps/here/HereMapsConfigurator;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereMapsConfigurator;->sInstance:Lcom/navdy/hud/app/maps/here/HereMapsConfigurator;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/navdy/hud/app/maps/here/HereMapsConfigurator;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapsConfigurator;->sInstance:Lcom/navdy/hud/app/maps/here/HereMapsConfigurator;

    return-object v0
.end method

.method private removeOldHereMapsFolders(Ljava/util/List;Ljava/lang/String;)V
    .locals 4
    .param p2, "latestHereMapConfigFolder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 73
    .local p1, "hereMapsConfigFolders":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 74
    .local v0, "hereMapsConfigFolder":Ljava/lang/String;
    invoke-static {v0, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 75
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v3}, Lcom/navdy/service/library/util/IOUtils;->deleteDirectory(Landroid/content/Context;Ljava/io/File;)V

    goto :goto_0

    .line 78
    .end local v0    # "hereMapsConfigFolder":Ljava/lang/String;
    :cond_1
    return-void
.end method


# virtual methods
.method removeMWConfigFolder()V
    .locals 3

    .prologue
    .line 68
    invoke-static {}, Lcom/navdy/hud/app/storage/PathManager;->getInstance()Lcom/navdy/hud/app/storage/PathManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/storage/PathManager;->getLatestHereMapsConfigPath()Ljava/lang/String;

    move-result-object v0

    .line 69
    .local v0, "latestHereMapConfigFolder":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/navdy/service/library/util/IOUtils;->deleteDirectory(Landroid/content/Context;Ljava/io/File;)V

    .line 70
    return-void
.end method

.method declared-synchronized updateMapsConfig()V
    .locals 14

    .prologue
    .line 37
    monitor-enter p0

    :try_start_0
    iget-boolean v9, p0, Lcom/navdy/hud/app/maps/here/HereMapsConfigurator;->isAlreadyConfigured:Z

    if-eqz v9, :cond_0

    .line 38
    sget-object v9, Lcom/navdy/hud/app/maps/here/HereMapsConfigurator;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "MWConfig is already configured"

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 65
    :goto_0
    monitor-exit p0

    return-void

    .line 42
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 43
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 44
    .local v0, "context":Landroid/content/Context;
    invoke-static {}, Lcom/navdy/hud/app/storage/PathManager;->getInstance()Lcom/navdy/hud/app/storage/PathManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/navdy/hud/app/storage/PathManager;->getLatestHereMapsConfigPath()Ljava/lang/String;

    move-result-object v6

    .line 46
    .local v6, "latestHereMapConfigFolder":Ljava/lang/String;
    sget-object v9, Lcom/navdy/hud/app/maps/here/HereMapsConfigurator;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "MWConfig: starting"

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 47
    invoke-static {}, Lcom/navdy/hud/app/storage/PathManager;->getInstance()Lcom/navdy/hud/app/storage/PathManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/navdy/hud/app/storage/PathManager;->getHereMapsConfigDirs()Ljava/util/List;

    move-result-object v1

    .line 48
    .local v1, "hereMapsConfigFolders":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v7, Lcom/navdy/hud/app/util/PackagedResource;

    const-string v9, "mwconfig_client"

    invoke-direct {v7, v9, v6}, Lcom/navdy/hud/app/util/PackagedResource;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 51
    .local v7, "mwConfigLatest":Lcom/navdy/hud/app/util/PackagedResource;
    :try_start_2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 53
    .local v2, "l1":J
    const v9, 0x7f070006

    const v10, 0x7f070007

    invoke-virtual {v7, v0, v9, v10}, Lcom/navdy/hud/app/util/PackagedResource;->updateFromResources(Landroid/content/Context;II)V

    .line 54
    invoke-direct {p0, v1, v6}, Lcom/navdy/hud/app/maps/here/HereMapsConfigurator;->removeOldHereMapsFolders(Ljava/util/List;Ljava/lang/String;)V

    .line 56
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 57
    .local v4, "l2":J
    sget-object v9, Lcom/navdy/hud/app/maps/here/HereMapsConfigurator;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "MWConfig: time took "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sub-long v12, v4, v2

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 59
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/navdy/hud/app/maps/here/HereMapsConfigurator;->isAlreadyConfigured:Z
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 64
    .end local v2    # "l1":J
    .end local v4    # "l2":J
    :goto_1
    const v9, 0x7f070002

    :try_start_3
    invoke-static {v0, v6, v9}, Lcom/navdy/service/library/util/IOUtils;->checkIntegrity(Landroid/content/Context;Ljava/lang/String;I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 37
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "hereMapsConfigFolders":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v6    # "latestHereMapConfigFolder":Ljava/lang/String;
    .end local v7    # "mwConfigLatest":Lcom/navdy/hud/app/util/PackagedResource;
    :catchall_0
    move-exception v9

    monitor-exit p0

    throw v9

    .line 60
    .restart local v0    # "context":Landroid/content/Context;
    .restart local v1    # "hereMapsConfigFolders":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v6    # "latestHereMapConfigFolder":Ljava/lang/String;
    .restart local v7    # "mwConfigLatest":Lcom/navdy/hud/app/util/PackagedResource;
    :catch_0
    move-exception v8

    .line 61
    .local v8, "throwable":Ljava/lang/Throwable;
    :try_start_4
    sget-object v9, Lcom/navdy/hud/app/maps/here/HereMapsConfigurator;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "MWConfig error"

    invoke-virtual {v9, v10, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method
