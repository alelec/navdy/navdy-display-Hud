.class public Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;
.super Lcom/here/android/mpa/guidance/NavigationManager$RealisticViewListener;
.source "HereRealisticViewListener.java"


# static fields
.field private static final HIDE_SIGN_POST_JUNCTION:Lcom/navdy/hud/app/maps/MapEvents$HideSignPostJunction;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private final bus:Lcom/squareup/otto/Bus;

.field private final navController:Lcom/navdy/hud/app/maps/here/HereNavController;

.field private running:Z

.field private start:Ljava/lang/Runnable;

.field private stop:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 17
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 19
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$HideSignPostJunction;

    invoke-direct {v0}, Lcom/navdy/hud/app/maps/MapEvents$HideSignPostJunction;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;->HIDE_SIGN_POST_JUNCTION:Lcom/navdy/hud/app/maps/MapEvents$HideSignPostJunction;

    return-void
.end method

.method constructor <init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/maps/here/HereNavController;)V
    .locals 1
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .param p2, "navController"    # Lcom/navdy/hud/app/maps/here/HereNavController;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/here/android/mpa/guidance/NavigationManager$RealisticViewListener;-><init>()V

    .line 26
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereRealisticViewListener$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/here/HereRealisticViewListener$1;-><init>(Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;->start:Ljava/lang/Runnable;

    .line 36
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereRealisticViewListener$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/here/HereRealisticViewListener$2;-><init>(Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;->stop:Ljava/lang/Runnable;

    .line 46
    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;->navController:Lcom/navdy/hud/app/maps/here/HereNavController;

    .line 47
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;->bus:Lcom/squareup/otto/Bus;

    .line 48
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;)Lcom/navdy/hud/app/maps/here/HereNavController;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;->navController:Lcom/navdy/hud/app/maps/here/HereNavController;

    return-object v0
.end method

.method static synthetic access$100()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method private clearDisplayEvent()V
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;->bus:Lcom/squareup/otto/Bus;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;->HIDE_SIGN_POST_JUNCTION:Lcom/navdy/hud/app/maps/MapEvents$HideSignPostJunction;

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 82
    return-void
.end method


# virtual methods
.method public onRealisticViewHide()V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;->clearDisplayEvent()V

    .line 78
    return-void
.end method

.method public onRealisticViewNextManeuver(Lcom/here/android/mpa/guidance/NavigationManager$AspectRatio;Lcom/here/android/mpa/common/Image;Lcom/here/android/mpa/common/Image;)V
    .locals 0
    .param p1, "ratio"    # Lcom/here/android/mpa/guidance/NavigationManager$AspectRatio;
    .param p2, "img1"    # Lcom/here/android/mpa/common/Image;
    .param p3, "img2"    # Lcom/here/android/mpa/common/Image;

    .prologue
    .line 68
    return-void
.end method

.method public onRealisticViewShow(Lcom/here/android/mpa/guidance/NavigationManager$AspectRatio;Lcom/here/android/mpa/common/Image;Lcom/here/android/mpa/common/Image;)V
    .locals 2
    .param p1, "ratio"    # Lcom/here/android/mpa/guidance/NavigationManager$AspectRatio;
    .param p2, "img1"    # Lcom/here/android/mpa/common/Image;
    .param p3, "img2"    # Lcom/here/android/mpa/common/Image;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/maps/MapEvents$DisplayJunction;

    invoke-direct {v1, p2, p3}, Lcom/navdy/hud/app/maps/MapEvents$DisplayJunction;-><init>(Lcom/here/android/mpa/common/Image;Lcom/here/android/mpa/common/Image;)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 73
    return-void
.end method

.method declared-synchronized start()V
    .locals 3

    .prologue
    .line 51
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;->running:Z

    if-nez v0, :cond_0

    .line 52
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;->start:Ljava/lang/Runnable;

    const/16 v2, 0xf

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 53
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;->running:Z

    .line 54
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "start:added realistic view listener"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56
    :cond_0
    monitor-exit p0

    return-void

    .line 51
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized stop()V
    .locals 3

    .prologue
    .line 59
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;->running:Z

    if-eqz v0, :cond_0

    .line 60
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;->stop:Ljava/lang/Runnable;

    const/16 v2, 0xf

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 61
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;->running:Z

    .line 62
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;->clearDisplayEvent()V

    .line 63
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "stop:remove realistic view listener"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 65
    :cond_0
    monitor-exit p0

    return-void

    .line 59
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
