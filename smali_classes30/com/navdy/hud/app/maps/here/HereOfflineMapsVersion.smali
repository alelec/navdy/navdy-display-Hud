.class public Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;
.super Ljava/lang/Object;
.source "HereOfflineMapsVersion.java"


# static fields
.field static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private date:Ljava/util/Date;

.field private packages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private rawDate:Ljava/lang/String;

.field private version:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 19
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 7
    .param p1, "offlineMapsData"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 30
    .local v2, "jsonObject":Lorg/json/JSONObject;
    const-string v5, "version"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;->rawDate:Ljava/lang/String;

    .line 31
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;->rawDate:Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;->parseMapDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v5

    iput-object v5, p0, Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;->date:Ljava/util/Date;

    .line 32
    const-string v5, "here_map_version"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;->version:Ljava/lang/String;

    .line 34
    const-string v5, "map_packages"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 35
    .local v4, "offlineMaps":Lorg/json/JSONArray;
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v3

    .line 36
    .local v3, "len":I
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v5, p0, Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;->packages:Ljava/util/List;

    .line 37
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v3, :cond_0

    .line 38
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;->packages:Ljava/util/List;

    invoke-virtual {v4, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 37
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 40
    .end local v1    # "i":I
    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    .end local v3    # "len":I
    .end local v4    # "offlineMaps":Lorg/json/JSONArray;
    :catch_0
    move-exception v0

    .line 41
    .local v0, "e":Ljava/lang/Exception;
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Failed to parse off-line maps version info"

    invoke-virtual {v5, v6, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 43
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    return-void
.end method

.method private parseMapDate(Ljava/lang/String;)Ljava/util/Date;
    .locals 8
    .param p1, "str"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v7, -0x1

    .line 63
    const/4 v5, -0x1

    .line 64
    .local v5, "y":I
    const/4 v2, -0x1

    .line 65
    .local v2, "m":I
    const/4 v1, -0x1

    .line 66
    .local v1, "d":I
    new-instance v4, Ljava/util/StringTokenizer;

    const-string v6, "."

    invoke-direct {v4, p1, v6}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    .local v4, "tokenizer":Ljava/util/StringTokenizer;
    :goto_0
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 68
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    .line 69
    .local v3, "token":Ljava/lang/String;
    if-ne v5, v7, :cond_0

    .line 70
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    goto :goto_0

    .line 71
    :cond_0
    if-ne v2, v7, :cond_1

    .line 72
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    goto :goto_0

    .line 74
    :cond_1
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    goto :goto_0

    .line 77
    .end local v3    # "token":Ljava/lang/String;
    :cond_2
    if-eq v5, v7, :cond_3

    if-eq v2, v7, :cond_3

    if-ne v1, v7, :cond_4

    .line 78
    :cond_3
    const/4 v6, 0x0

    .line 84
    :goto_1
    return-object v6

    .line 80
    :cond_4
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 81
    .local v0, "calendar":Ljava/util/Calendar;
    const/4 v6, 0x1

    invoke-virtual {v0, v6, v5}, Ljava/util/Calendar;->set(II)V

    .line 82
    const/4 v6, 0x2

    add-int/lit8 v7, v2, -0x1

    invoke-virtual {v0, v6, v7}, Ljava/util/Calendar;->set(II)V

    .line 83
    const/4 v6, 0x5

    invoke-virtual {v0, v6, v1}, Ljava/util/Calendar;->set(II)V

    .line 84
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v6

    goto :goto_1
.end method


# virtual methods
.method public getDate()Ljava/util/Date;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;->date:Ljava/util/Date;

    return-object v0
.end method

.method public getPackages()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;->packages:Ljava/util/List;

    return-object v0
.end method

.method public getRawDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;->rawDate:Ljava/lang/String;

    return-object v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;->version:Ljava/lang/String;

    return-object v0
.end method
