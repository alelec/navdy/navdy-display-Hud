.class public Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;
.super Ljava/lang/Object;
.source "HereTrafficUpdater2.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;
    }
.end annotation


# static fields
.field private static final FAILED_EVENT:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;

.field private static final INACTIVE_EVENT:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;

.field private static final NORMAL_EVENT:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;

.field private static final STALE_REQUEST_TIME:I

.field private static final TAG:Ljava/lang/String; = "HereTrafficUpdater2"

.field private static final TRAFFIC_DATA_CHECK_TIME_INTERVAL:I

.field private static final TRAFFIC_DATA_REFRESH_INTERVAL:I

.field private static final TRAFFIC_DATA_REFRESH_INTERVAL_LIMITED_BANDWIDTH:I

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private bus:Lcom/squareup/otto/Bus;

.field private checkRunnable:Ljava/lang/Runnable;

.field private currentRequestInfo:Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;

.field private handler:Landroid/os/Handler;

.field private lastRequestTime:J

.field private onGetEventsListener:Lcom/here/android/mpa/guidance/TrafficUpdater$GetEventsListener;

.field private onRequestListener:Lcom/here/android/mpa/guidance/TrafficUpdater$Listener;

.field private refreshRunnable:Ljava/lang/Runnable;

.field private route:Lcom/here/android/mpa/routing/Route;

.field private trafficUpdater:Lcom/here/android/mpa/guidance/TrafficUpdater;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 40
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-string v1, "HereTrafficUpdater2"

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 45
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->TRAFFIC_DATA_REFRESH_INTERVAL:I

    .line 46
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xf

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->TRAFFIC_DATA_REFRESH_INTERVAL_LIMITED_BANDWIDTH:I

    .line 51
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->TRAFFIC_DATA_CHECK_TIME_INTERVAL:I

    .line 54
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->STALE_REQUEST_TIME:I

    .line 57
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;->NORMAL:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

    sget-object v2, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;->UNDEFINED:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;-><init>(Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->NORMAL_EVENT:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;

    .line 60
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;->FAILED:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

    sget-object v2, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;->UNDEFINED:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;-><init>(Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->FAILED_EVENT:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;

    .line 63
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;->INACTIVE:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

    sget-object v2, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;->UNDEFINED:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;-><init>(Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->INACTIVE_EVENT:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/otto/Bus;)V
    .locals 2
    .param p1, "bus"    # Lcom/squareup/otto/Bus;

    .prologue
    .line 245
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->handler:Landroid/os/Handler;

    .line 85
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;-><init>(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->onRequestListener:Lcom/here/android/mpa/guidance/TrafficUpdater$Listener;

    .line 165
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$2;-><init>(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->onGetEventsListener:Lcom/here/android/mpa/guidance/TrafficUpdater$GetEventsListener;

    .line 213
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$3;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$3;-><init>(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->refreshRunnable:Ljava/lang/Runnable;

    .line 220
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$4;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$4;-><init>(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->checkRunnable:Ljava/lang/Runnable;

    .line 246
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "ctor"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 247
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->bus:Lcom/squareup/otto/Bus;

    .line 248
    invoke-static {}, Lcom/here/android/mpa/guidance/TrafficUpdater;->getInstance()Lcom/here/android/mpa/guidance/TrafficUpdater;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->trafficUpdater:Lcom/here/android/mpa/guidance/TrafficUpdater;

    .line 249
    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;)Lcom/here/android/mpa/routing/Route;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->route:Lcom/here/android/mpa/routing/Route;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->startRequest()V

    return-void
.end method

.method static synthetic access$1100()I
    .locals 1

    .prologue
    .line 38
    sget v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->STALE_REQUEST_TIME:I

    return v0
.end method

.method static synthetic access$1200()I
    .locals 1

    .prologue
    .line 38
    sget v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->TRAFFIC_DATA_CHECK_TIME_INTERVAL:I

    return v0
.end method

.method static synthetic access$1300(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;)Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->currentRequestInfo:Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;

    return-object v0
.end method

.method static synthetic access$202(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;)Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;
    .param p1, "x1"    # Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->currentRequestInfo:Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;

    return-object p1
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;)J
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    .prologue
    .line 38
    iget-wide v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->lastRequestTime:J

    return-wide v0
.end method

.method static synthetic access$302(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;J)J
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;
    .param p1, "x1"    # J

    .prologue
    .line 38
    iput-wide p1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->lastRequestTime:J

    return-wide p1
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;I)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;
    .param p1, "x1"    # I

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->reset(I)V

    return-void
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;)Lcom/here/android/mpa/guidance/TrafficUpdater$GetEventsListener;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->onGetEventsListener:Lcom/here/android/mpa/guidance/TrafficUpdater$GetEventsListener;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;)Lcom/here/android/mpa/guidance/TrafficUpdater;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->trafficUpdater:Lcom/here/android/mpa/guidance/TrafficUpdater;

    return-object v0
.end method

.method static synthetic access$700()Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->FAILED_EVENT:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;

    return-object v0
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;)Lcom/squareup/otto/Bus;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->bus:Lcom/squareup/otto/Bus;

    return-object v0
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;Lcom/here/android/mpa/routing/Route;Ljava/util/List;J)V
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;
    .param p1, "x1"    # Lcom/here/android/mpa/routing/Route;
    .param p2, "x2"    # Ljava/util/List;
    .param p3, "x3"    # J

    .prologue
    .line 38
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->processEvents(Lcom/here/android/mpa/routing/Route;Ljava/util/List;J)V

    return-void
.end method

.method private buildEvent(Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;Ljava/util/List;)Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;
    .locals 24
    .param p1, "type"    # Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;",
            ">;)",
            "Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;"
        }
    .end annotation

    .prologue
    .line 478
    .local p2, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;>;"
    const/4 v13, 0x0

    .line 479
    .local v13, "accidentEvent":Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;
    const/4 v14, 0x0

    .line 480
    .local v14, "closureEvent":Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;
    const/16 v19, 0x0

    .line 481
    .local v19, "roadworkEvent":Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;
    const/4 v15, 0x0

    .line 482
    .local v15, "congestionEvent":Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;
    const/16 v17, 0x0

    .line 483
    .local v17, "flowEvent":Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;
    const/16 v18, 0x0

    .line 484
    .local v18, "otherEvent":Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;
    const/16 v20, 0x0

    .line 486
    .local v20, "undefinedEvent":Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;

    .line 487
    .local v16, "event":Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;
    move-object/from16 v0, v16

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;->trafficEvent:Lcom/here/android/mpa/mapping/TrafficEvent;

    invoke-virtual {v3}, Lcom/here/android/mpa/mapping/TrafficEvent;->getShortText()Ljava/lang/String;

    move-result-object v9

    const/4 v3, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    :cond_1
    :goto_1
    packed-switch v3, :pswitch_data_0

    .line 539
    if-nez v20, :cond_8

    .line 540
    move-object/from16 v20, v16

    goto :goto_0

    .line 487
    :sswitch_0
    const-string v10, "ACCIDENT"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    const/4 v3, 0x0

    goto :goto_1

    :sswitch_1
    const-string v10, "CLOSURE"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :sswitch_2
    const-string v10, "ROADWORKS"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    const/4 v3, 0x2

    goto :goto_1

    :sswitch_3
    const-string v10, "CONGESTION"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    const/4 v3, 0x3

    goto :goto_1

    :sswitch_4
    const-string v10, "FLOW"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    const/4 v3, 0x4

    goto :goto_1

    :sswitch_5
    const-string v10, "OTHER"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    const/4 v3, 0x5

    goto :goto_1

    :sswitch_6
    const-string v10, "UNDEFINED"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    const/4 v3, 0x6

    goto :goto_1

    .line 489
    :pswitch_0
    if-nez v13, :cond_2

    .line 490
    move-object/from16 v13, v16

    goto :goto_0

    .line 491
    :cond_2
    iget-wide v10, v13, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;->distance:J

    move-object/from16 v0, v16

    iget-wide v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;->distance:J

    move-wide/from16 v22, v0

    cmp-long v3, v10, v22

    if-lez v3, :cond_0

    .line 492
    move-object/from16 v13, v16

    goto :goto_0

    .line 498
    :pswitch_1
    if-nez v14, :cond_3

    .line 499
    move-object/from16 v14, v16

    goto/16 :goto_0

    .line 500
    :cond_3
    iget-wide v10, v14, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;->distance:J

    move-object/from16 v0, v16

    iget-wide v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;->distance:J

    move-wide/from16 v22, v0

    cmp-long v3, v10, v22

    if-lez v3, :cond_0

    .line 501
    move-object/from16 v14, v16

    goto/16 :goto_0

    .line 506
    :pswitch_2
    if-nez v19, :cond_4

    .line 507
    move-object/from16 v19, v16

    goto/16 :goto_0

    .line 508
    :cond_4
    move-object/from16 v0, v19

    iget-wide v10, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;->distance:J

    move-object/from16 v0, v16

    iget-wide v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;->distance:J

    move-wide/from16 v22, v0

    cmp-long v3, v10, v22

    if-lez v3, :cond_0

    .line 509
    move-object/from16 v19, v16

    goto/16 :goto_0

    .line 514
    :pswitch_3
    if-nez v15, :cond_5

    .line 515
    move-object/from16 v15, v16

    goto/16 :goto_0

    .line 516
    :cond_5
    iget-wide v10, v15, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;->distance:J

    move-object/from16 v0, v16

    iget-wide v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;->distance:J

    move-wide/from16 v22, v0

    cmp-long v3, v10, v22

    if-lez v3, :cond_0

    .line 517
    move-object/from16 v15, v16

    goto/16 :goto_0

    .line 522
    :pswitch_4
    if-nez v17, :cond_6

    .line 523
    move-object/from16 v17, v16

    goto/16 :goto_0

    .line 524
    :cond_6
    move-object/from16 v0, v17

    iget-wide v10, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;->distance:J

    move-object/from16 v0, v16

    iget-wide v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;->distance:J

    move-wide/from16 v22, v0

    cmp-long v3, v10, v22

    if-lez v3, :cond_0

    .line 525
    move-object/from16 v17, v16

    goto/16 :goto_0

    .line 530
    :pswitch_5
    if-nez v18, :cond_7

    .line 531
    move-object/from16 v18, v16

    goto/16 :goto_0

    .line 532
    :cond_7
    move-object/from16 v0, v18

    iget-wide v10, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;->distance:J

    move-object/from16 v0, v16

    iget-wide v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;->distance:J

    move-wide/from16 v22, v0

    cmp-long v3, v10, v22

    if-lez v3, :cond_0

    .line 533
    move-object/from16 v18, v16

    goto/16 :goto_0

    .line 541
    :cond_8
    move-object/from16 v0, v20

    iget-wide v10, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;->distance:J

    move-object/from16 v0, v16

    iget-wide v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;->distance:J

    move-wide/from16 v22, v0

    cmp-long v3, v10, v22

    if-lez v3, :cond_0

    .line 542
    move-object/from16 v20, v16

    goto/16 :goto_0

    .line 548
    .end local v16    # "event":Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;
    :cond_9
    const/16 v21, 0x0

    .line 549
    .local v21, "userEvent":Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;
    if-eqz v13, :cond_c

    .line 550
    move-object/from16 v21, v13

    .line 567
    :cond_a
    :goto_2
    move-object/from16 v0, v21

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;->trafficEvent:Lcom/here/android/mpa/mapping/TrafficEvent;

    invoke-virtual {v3}, Lcom/here/android/mpa/mapping/TrafficEvent;->getFirstAffectedStreet()Ljava/lang/String;

    move-result-object v7

    .line 570
    .local v7, "affectedStreet":Ljava/lang/String;
    move-object/from16 v0, v21

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;->trafficEvent:Lcom/here/android/mpa/mapping/TrafficEvent;

    invoke-virtual {v3}, Lcom/here/android/mpa/mapping/TrafficEvent;->getShortText()Ljava/lang/String;

    move-result-object v8

    const/4 v3, -0x1

    invoke-virtual {v8}, Ljava/lang/String;->hashCode()I

    move-result v9

    sparse-switch v9, :sswitch_data_1

    :cond_b
    :goto_3
    packed-switch v3, :pswitch_data_1

    .line 610
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Undefined:"

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v21

    iget-object v8, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;->trafficEvent:Lcom/here/android/mpa/mapping/TrafficEvent;

    invoke-virtual {v8}, Lcom/here/android/mpa/mapping/TrafficEvent;->getShortText()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 611
    .local v5, "title":Ljava/lang/String;
    move-object/from16 v0, v21

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;->trafficEvent:Lcom/here/android/mpa/mapping/TrafficEvent;

    invoke-virtual {v3}, Lcom/here/android/mpa/mapping/TrafficEvent;->getEventText()Ljava/lang/String;

    move-result-object v6

    .line 612
    .local v6, "description":Ljava/lang/String;
    sget-object v4, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;->UNDEFINED:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    .line 616
    .local v4, "category":Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;
    :goto_4
    new-instance v2, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;

    move-object/from16 v0, v21

    iget-wide v8, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;->distance:J

    move-object/from16 v0, v21

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;->trafficEvent:Lcom/here/android/mpa/mapping/TrafficEvent;

    .line 623
    invoke-virtual {v3}, Lcom/here/android/mpa/mapping/TrafficEvent;->getActivationDate()Ljava/util/Date;

    move-result-object v10

    move-object/from16 v0, v21

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;->trafficEvent:Lcom/here/android/mpa/mapping/TrafficEvent;

    .line 624
    invoke-virtual {v3}, Lcom/here/android/mpa/mapping/TrafficEvent;->getActivationDate()Ljava/util/Date;

    move-result-object v11

    move-object/from16 v0, v21

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;->trafficEvent:Lcom/here/android/mpa/mapping/TrafficEvent;

    .line 625
    invoke-virtual {v3}, Lcom/here/android/mpa/mapping/TrafficEvent;->getIconOnRoute()Lcom/here/android/mpa/common/Image;

    move-result-object v12

    move-object/from16 v3, p1

    invoke-direct/range {v2 .. v12}, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;-><init>(Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/util/Date;Ljava/util/Date;Lcom/here/android/mpa/common/Image;)V

    .line 627
    .local v2, "incident":Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;
    return-object v2

    .line 551
    .end local v2    # "incident":Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;
    .end local v4    # "category":Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;
    .end local v5    # "title":Ljava/lang/String;
    .end local v6    # "description":Ljava/lang/String;
    .end local v7    # "affectedStreet":Ljava/lang/String;
    :cond_c
    if-eqz v14, :cond_d

    .line 552
    move-object/from16 v21, v14

    goto :goto_2

    .line 553
    :cond_d
    if-eqz v19, :cond_e

    .line 554
    move-object/from16 v21, v19

    goto :goto_2

    .line 555
    :cond_e
    if-eqz v15, :cond_f

    .line 556
    move-object/from16 v21, v15

    goto :goto_2

    .line 557
    :cond_f
    if-eqz v17, :cond_10

    .line 558
    move-object/from16 v21, v17

    goto :goto_2

    .line 559
    :cond_10
    if-eqz v18, :cond_11

    .line 560
    move-object/from16 v21, v18

    goto :goto_2

    .line 561
    :cond_11
    if-eqz v20, :cond_a

    .line 562
    move-object/from16 v21, v20

    goto/16 :goto_2

    .line 570
    .restart local v7    # "affectedStreet":Ljava/lang/String;
    :sswitch_7
    const-string v9, "ACCIDENT"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    const/4 v3, 0x0

    goto :goto_3

    :sswitch_8
    const-string v9, "CLOSURE"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    const/4 v3, 0x1

    goto :goto_3

    :sswitch_9
    const-string v9, "ROADWORKS"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    const/4 v3, 0x2

    goto/16 :goto_3

    :sswitch_a
    const-string v9, "CONGESTION"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    const/4 v3, 0x3

    goto/16 :goto_3

    :sswitch_b
    const-string v9, "FLOW"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    const/4 v3, 0x4

    goto/16 :goto_3

    :sswitch_c
    const-string v9, "OTHER"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    const/4 v3, 0x5

    goto/16 :goto_3

    :sswitch_d
    const-string v9, "UNDEFINED"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    const/4 v3, 0x6

    goto/16 :goto_3

    .line 572
    :pswitch_6
    const-string v5, "Accident"

    .line 573
    .restart local v5    # "title":Ljava/lang/String;
    move-object/from16 v0, v21

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;->trafficEvent:Lcom/here/android/mpa/mapping/TrafficEvent;

    invoke-virtual {v3}, Lcom/here/android/mpa/mapping/TrafficEvent;->getEventText()Ljava/lang/String;

    move-result-object v6

    .line 574
    .restart local v6    # "description":Ljava/lang/String;
    sget-object v4, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;->ACCIDENT:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    .line 575
    .restart local v4    # "category":Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;
    goto/16 :goto_4

    .line 578
    .end local v4    # "category":Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;
    .end local v5    # "title":Ljava/lang/String;
    .end local v6    # "description":Ljava/lang/String;
    :pswitch_7
    const-string v5, "Closure"

    .line 579
    .restart local v5    # "title":Ljava/lang/String;
    move-object/from16 v0, v21

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;->trafficEvent:Lcom/here/android/mpa/mapping/TrafficEvent;

    invoke-virtual {v3}, Lcom/here/android/mpa/mapping/TrafficEvent;->getEventText()Ljava/lang/String;

    move-result-object v6

    .line 580
    .restart local v6    # "description":Ljava/lang/String;
    sget-object v4, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;->CLOSURE:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    .line 581
    .restart local v4    # "category":Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;
    goto/16 :goto_4

    .line 584
    .end local v4    # "category":Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;
    .end local v5    # "title":Ljava/lang/String;
    .end local v6    # "description":Ljava/lang/String;
    :pswitch_8
    const-string v5, "RoadWork"

    .line 585
    .restart local v5    # "title":Ljava/lang/String;
    move-object/from16 v0, v21

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;->trafficEvent:Lcom/here/android/mpa/mapping/TrafficEvent;

    invoke-virtual {v3}, Lcom/here/android/mpa/mapping/TrafficEvent;->getEventText()Ljava/lang/String;

    move-result-object v6

    .line 586
    .restart local v6    # "description":Ljava/lang/String;
    sget-object v4, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;->ROADWORKS:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    .line 587
    .restart local v4    # "category":Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;
    goto/16 :goto_4

    .line 590
    .end local v4    # "category":Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;
    .end local v5    # "title":Ljava/lang/String;
    .end local v6    # "description":Ljava/lang/String;
    :pswitch_9
    const-string v5, "Congestion"

    .line 591
    .restart local v5    # "title":Ljava/lang/String;
    move-object/from16 v0, v21

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;->trafficEvent:Lcom/here/android/mpa/mapping/TrafficEvent;

    invoke-virtual {v3}, Lcom/here/android/mpa/mapping/TrafficEvent;->getEventText()Ljava/lang/String;

    move-result-object v6

    .line 592
    .restart local v6    # "description":Ljava/lang/String;
    sget-object v4, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;->CONGESTION:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    .line 593
    .restart local v4    # "category":Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;
    goto/16 :goto_4

    .line 596
    .end local v4    # "category":Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;
    .end local v5    # "title":Ljava/lang/String;
    .end local v6    # "description":Ljava/lang/String;
    :pswitch_a
    const-string v5, "Congestion"

    .line 597
    .restart local v5    # "title":Ljava/lang/String;
    const-string v6, ""

    .line 598
    .restart local v6    # "description":Ljava/lang/String;
    sget-object v4, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;->FLOW:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    .line 599
    .restart local v4    # "category":Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;
    goto/16 :goto_4

    .line 603
    .end local v4    # "category":Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;
    .end local v5    # "title":Ljava/lang/String;
    .end local v6    # "description":Ljava/lang/String;
    :pswitch_b
    const-string v5, "Other"

    .line 604
    .restart local v5    # "title":Ljava/lang/String;
    move-object/from16 v0, v21

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;->trafficEvent:Lcom/here/android/mpa/mapping/TrafficEvent;

    invoke-virtual {v3}, Lcom/here/android/mpa/mapping/TrafficEvent;->getEventText()Ljava/lang/String;

    move-result-object v6

    .line 605
    .restart local v6    # "description":Ljava/lang/String;
    sget-object v4, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;->OTHER:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    .line 606
    .restart local v4    # "category":Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;
    goto/16 :goto_4

    .line 487
    nop

    :sswitch_data_0
    .sparse-switch
        -0x5118bdf1 -> :sswitch_0
        0x20f92e -> :sswitch_4
        0x48086f0 -> :sswitch_5
        0x5e72161b -> :sswitch_1
        0x68377130 -> :sswitch_6
        0x73d62042 -> :sswitch_2
        0x7d444427 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 570
    :sswitch_data_1
    .sparse-switch
        -0x5118bdf1 -> :sswitch_7
        0x20f92e -> :sswitch_b
        0x48086f0 -> :sswitch_c
        0x5e72161b -> :sswitch_8
        0x68377130 -> :sswitch_d
        0x73d62042 -> :sswitch_9
        0x7d444427 -> :sswitch_a
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method private cancelRequest()V
    .locals 4

    .prologue
    .line 330
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->currentRequestInfo:Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;

    if-nez v1, :cond_0

    .line 344
    :goto_0
    return-void

    .line 334
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->currentRequestInfo:Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;

    invoke-virtual {v1}, Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;->getError()Lcom/here/android/mpa/guidance/TrafficUpdater$Error;

    move-result-object v1

    sget-object v2, Lcom/here/android/mpa/guidance/TrafficUpdater$Error;->NONE:Lcom/here/android/mpa/guidance/TrafficUpdater$Error;

    if-ne v1, v2, :cond_1

    .line 335
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->trafficUpdater:Lcom/here/android/mpa/guidance/TrafficUpdater;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->currentRequestInfo:Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;

    invoke-virtual {v2}, Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;->getRequestId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/here/android/mpa/guidance/TrafficUpdater;->cancelRequest(J)V

    .line 336
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->lastRequestTime:J

    .line 337
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "cancelRequest: called cancel"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 343
    :cond_1
    :goto_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->currentRequestInfo:Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;

    goto :goto_0

    .line 339
    :catch_0
    move-exception v0

    .line 340
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private processEvents(Lcom/here/android/mpa/routing/Route;Ljava/util/List;J)V
    .locals 39
    .param p1, "route"    # Lcom/here/android/mpa/routing/Route;
    .param p3, "distanceRemaining"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/here/android/mpa/routing/Route;",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/mapping/TrafficEvent;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 357
    .local p2, "events":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/mapping/TrafficEvent;>;"
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v20

    .line 358
    .local v20, "l1":J
    sget-object v33, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v34, Ljava/lang/StringBuilder;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuilder;-><init>()V

    const-string v35, "processEvents size:"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v35

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    invoke-virtual/range {v33 .. v34}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 359
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 361
    invoke-virtual/range {p1 .. p1}, Lcom/here/android/mpa/routing/Route;->getDestination()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v10

    .line 362
    .local v10, "destination":Lcom/here/android/mpa/common/GeoCoordinate;
    const/4 v5, 0x0

    .line 363
    .local v5, "blocking":I
    const/16 v29, 0x0

    .line 364
    .local v29, "veryHighCount":I
    const/16 v18, 0x0

    .line 366
    .local v18, "highCount":I
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 367
    .local v6, "blockingList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;>;"
    new-instance v32, Ljava/util/ArrayList;

    invoke-direct/range {v32 .. v32}, Ljava/util/ArrayList;-><init>()V

    .line 368
    .local v32, "veryHighList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;>;"
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 370
    .local v19, "highList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;>;"
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getCurrentRoadElement()Lcom/here/android/mpa/common/RoadElement;

    move-result-object v9

    .line 371
    .local v9, "currentRoadElement":Lcom/here/android/mpa/common/RoadElement;
    if-nez v9, :cond_0

    .line 372
    sget-object v33, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v34, "no current road element"

    invoke-virtual/range {v33 .. v34}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 474
    :goto_0
    return-void

    .line 376
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v8

    .line 377
    .local v8, "coordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    if-nez v8, :cond_1

    .line 378
    sget-object v33, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v34, "no current coordinate"

    invoke-virtual/range {v33 .. v34}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 382
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/here/android/mpa/routing/Route;->getManeuvers()Ljava/util/List;

    move-result-object v27

    .line 383
    .local v27, "maneuvers":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    const/16 v26, 0x0

    .line 384
    .local v26, "lastRoadElement":Lcom/here/android/mpa/common/RoadElement;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getLastManeuver(Ljava/util/List;)Lcom/here/android/mpa/routing/Maneuver;

    move-result-object v24

    .line 385
    .local v24, "lastManeuver":Lcom/here/android/mpa/routing/Maneuver;
    if-eqz v24, :cond_2

    .line 386
    invoke-virtual/range {v24 .. v24}, Lcom/here/android/mpa/routing/Maneuver;->getRoadElements()Ljava/util/List;

    move-result-object v25

    .line 387
    .local v25, "lastManeuverRoadElements":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/common/RoadElement;>;"
    if-eqz v25, :cond_2

    invoke-interface/range {v25 .. v25}, Ljava/util/List;->size()I

    move-result v33

    if-lez v33, :cond_2

    .line 388
    invoke-interface/range {v25 .. v25}, Ljava/util/List;->size()I

    move-result v33

    add-int/lit8 v33, v33, -0x1

    move-object/from16 v0, v25

    move/from16 v1, v33

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v26

    .end local v26    # "lastRoadElement":Lcom/here/android/mpa/common/RoadElement;
    check-cast v26, Lcom/here/android/mpa/common/RoadElement;

    .line 392
    .end local v25    # "lastManeuverRoadElements":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/common/RoadElement;>;"
    .restart local v26    # "lastRoadElement":Lcom/here/android/mpa/common/RoadElement;
    :cond_2
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v34

    :goto_1
    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->hasNext()Z

    move-result v33

    if-eqz v33, :cond_8

    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/here/android/mpa/mapping/TrafficEvent;

    .line 395
    .local v11, "event":Lcom/here/android/mpa/mapping/TrafficEvent;
    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Lcom/here/android/mpa/mapping/TrafficEvent;->isOnRoute(Lcom/here/android/mpa/routing/Route;)Z

    move-result v33

    if-nez v33, :cond_3

    .line 396
    sget-object v33, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v35, "[NOT_ON_ROUTE] event is not on route"

    move-object/from16 v0, v33

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_1

    .line 400
    :cond_3
    invoke-virtual {v11}, Lcom/here/android/mpa/mapping/TrafficEvent;->getAffectedRoadElements()Ljava/util/List;

    move-result-object v4

    .line 402
    .local v4, "affectedRoadElements":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/common/RoadElement;>;"
    if-eqz v4, :cond_4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v33

    if-nez v33, :cond_5

    .line 403
    :cond_4
    sget-object v33, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v35, "no road elements for event"

    move-object/from16 v0, v33

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_1

    .line 407
    :cond_5
    const/4 v7, 0x0

    .line 409
    .local v7, "calcDistance":Z
    invoke-virtual {v11}, Lcom/here/android/mpa/mapping/TrafficEvent;->getSeverity()Lcom/here/android/mpa/mapping/TrafficEvent$Severity;

    move-result-object v28

    .line 411
    .local v28, "severity":Lcom/here/android/mpa/mapping/TrafficEvent$Severity;
    sget-object v33, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$5;->$SwitchMap$com$here$android$mpa$mapping$TrafficEvent$Severity:[I

    invoke-virtual/range {v28 .. v28}, Lcom/here/android/mpa/mapping/TrafficEvent$Severity;->ordinal()I

    move-result v35

    aget v33, v33, v35

    packed-switch v33, :pswitch_data_0

    .line 419
    :goto_2
    const-wide/16 v12, -0x1

    .line 421
    .local v12, "distanceToEvent":J
    if-eqz v7, :cond_7

    .line 422
    const/16 v33, 0x0

    move/from16 v0, v33

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v33

    check-cast v33, Lcom/here/android/mpa/common/RoadElement;

    move-object/from16 v0, v33

    move-object/from16 v1, v27

    invoke-static {v9, v0, v1}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getDistanceToRoadElement(Lcom/here/android/mpa/common/RoadElement;Lcom/here/android/mpa/common/RoadElement;Ljava/util/List;)J

    move-result-wide v12

    .line 423
    const/16 v33, 0x0

    move/from16 v0, v33

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v33

    check-cast v33, Lcom/here/android/mpa/common/RoadElement;

    move-object/from16 v0, v33

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getDistanceToRoadElement(Lcom/here/android/mpa/common/RoadElement;Lcom/here/android/mpa/common/RoadElement;Ljava/util/List;)J

    move-result-wide v16

    .line 424
    .local v16, "eventToDestinationDistance":J
    const-wide/16 v36, -0x1

    cmp-long v33, v16, v36

    if-nez v33, :cond_6

    .line 425
    sget-object v33, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v35, "could not get distance"

    move-object/from16 v0, v33

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 415
    .end local v12    # "distanceToEvent":J
    .end local v16    # "eventToDestinationDistance":J
    :pswitch_0
    const/4 v7, 0x1

    goto :goto_2

    .line 429
    .restart local v12    # "distanceToEvent":J
    .restart local v16    # "eventToDestinationDistance":J
    :cond_6
    invoke-virtual {v11, v8}, Lcom/here/android/mpa/mapping/TrafficEvent;->getDistanceTo(Lcom/here/android/mpa/common/GeoCoordinate;)I

    move-result v33

    move/from16 v0, v33

    int-to-long v14, v0

    .line 430
    .local v14, "distanceToEventHaversine":J
    add-long v30, v12, v16

    .line 431
    .local v30, "total":J
    sget-object v33, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "[EVENT] distanceToEvent:"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v36, " eventToDestinationDistance:"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v36, " distanceToDest:"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-wide/from16 v1, p3

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v36, " total:"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v36, " distanceToEventHaversine:"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v33

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 440
    .end local v14    # "distanceToEventHaversine":J
    .end local v16    # "eventToDestinationDistance":J
    .end local v30    # "total":J
    :goto_3
    sget-object v33, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$5;->$SwitchMap$com$here$android$mpa$mapping$TrafficEvent$Severity:[I

    invoke-virtual/range {v28 .. v28}, Lcom/here/android/mpa/mapping/TrafficEvent$Severity;->ordinal()I

    move-result v35

    aget v33, v33, v35

    packed-switch v33, :pswitch_data_1

    goto/16 :goto_1

    .line 442
    :pswitch_1
    new-instance v33, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;

    move-object/from16 v0, v33

    invoke-direct {v0, v11, v12, v13}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;-><init>(Lcom/here/android/mpa/mapping/TrafficEvent;J)V

    move-object/from16 v0, v33

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 443
    add-int/lit8 v5, v5, 0x1

    .line 444
    goto/16 :goto_1

    .line 437
    :cond_7
    sget-object v33, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "[EVENT] n/a severity = "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v33

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_3

    .line 447
    :pswitch_2
    new-instance v33, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;

    move-object/from16 v0, v33

    invoke-direct {v0, v11, v12, v13}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;-><init>(Lcom/here/android/mpa/mapping/TrafficEvent;J)V

    invoke-interface/range {v32 .. v33}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 448
    add-int/lit8 v29, v29, 0x1

    .line 449
    goto/16 :goto_1

    .line 452
    :pswitch_3
    new-instance v33, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;

    move-object/from16 v0, v33

    invoke-direct {v0, v11, v12, v13}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$HereTrafficEvent;-><init>(Lcom/here/android/mpa/mapping/TrafficEvent;J)V

    move-object/from16 v0, v19

    move-object/from16 v1, v33

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 453
    add-int/lit8 v18, v18, 0x1

    goto/16 :goto_1

    .line 459
    .end local v4    # "affectedRoadElements":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/common/RoadElement;>;"
    .end local v7    # "calcDistance":Z
    .end local v11    # "event":Lcom/here/android/mpa/mapping/TrafficEvent;
    .end local v12    # "distanceToEvent":J
    .end local v28    # "severity":Lcom/here/android/mpa/mapping/TrafficEvent$Severity;
    :cond_8
    sget-object v33, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v34, Ljava/lang/StringBuilder;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuilder;-><init>()V

    const-string v35, "blocking:"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v34

    const-string v35, " very high:"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    move-object/from16 v0, v34

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v34

    const-string v35, " high:"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    move-object/from16 v0, v34

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    invoke-virtual/range {v33 .. v34}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 461
    if-lez v5, :cond_9

    .line 462
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->bus:Lcom/squareup/otto/Bus;

    move-object/from16 v33, v0

    sget-object v34, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;->BLOCKING:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    invoke-direct {v0, v1, v6}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->buildEvent(Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;Ljava/util/List;)Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;

    move-result-object v34

    invoke-virtual/range {v33 .. v34}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 472
    :goto_4
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v22

    .line 473
    .local v22, "l2":J
    sget-object v33, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v34, Ljava/lang/StringBuilder;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuilder;-><init>()V

    const-string v35, "processEvents took ["

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    sub-long v36, v22, v20

    move-object/from16 v0, v34

    move-wide/from16 v1, v36

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v34

    const-string v35, "]"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    invoke-virtual/range {v33 .. v34}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 463
    .end local v22    # "l2":J
    :cond_9
    if-lez v29, :cond_a

    .line 464
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->bus:Lcom/squareup/otto/Bus;

    move-object/from16 v33, v0

    sget-object v34, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;->VERY_HIGH:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    move-object/from16 v2, v32

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->buildEvent(Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;Ljava/util/List;)Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;

    move-result-object v34

    invoke-virtual/range {v33 .. v34}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_4

    .line 465
    :cond_a
    if-lez v18, :cond_b

    .line 466
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->bus:Lcom/squareup/otto/Bus;

    move-object/from16 v33, v0

    sget-object v34, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;->HIGH:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->buildEvent(Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;Ljava/util/List;)Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;

    move-result-object v34

    invoke-virtual/range {v33 .. v34}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_4

    .line 469
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->bus:Lcom/squareup/otto/Bus;

    move-object/from16 v33, v0

    sget-object v34, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->NORMAL_EVENT:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;

    invoke-virtual/range {v33 .. v34}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_4

    .line 411
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 440
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private reset(I)V
    .locals 4
    .param p1, "delay"    # I

    .prologue
    .line 347
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "reset"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 348
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->cancelRequest()V

    .line 349
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->refreshRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 350
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->refreshRunnable:Ljava/lang/Runnable;

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 351
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->checkRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 352
    return-void
.end method

.method private startRequest()V
    .locals 8

    .prologue
    .line 275
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/navdy/service/library/util/SystemUtils;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 276
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "startRequest: not connected to n/w, retry"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 277
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->getRefereshInterval()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    invoke-direct {p0, v3}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->reset(I)V

    .line 278
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->bus:Lcom/squareup/otto/Bus;

    sget-object v4, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->FAILED_EVENT:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;

    invoke-virtual {v3, v4}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 279
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->lastRequestTime:J

    .line 325
    :goto_0
    return-void

    .line 283
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hasArrived()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 284
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "startRequest: arrived, not making request"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 285
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->bus:Lcom/squareup/otto/Bus;

    sget-object v4, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->INACTIVE_EVENT:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;

    invoke-virtual {v3, v4}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 286
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->lastRequestTime:J
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 321
    :catch_0
    move-exception v2

    .line 322
    .local v2, "t":Ljava/lang/Throwable;
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 323
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->getRefereshInterval()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    invoke-direct {p0, v3}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->reset(I)V

    goto :goto_0

    .line 290
    .end local v2    # "t":Ljava/lang/Throwable;
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->route:Lcom/here/android/mpa/routing/Route;

    if-eqz v3, :cond_2

    .line 291
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "startRequest: getting route traffic"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 292
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->trafficUpdater:Lcom/here/android/mpa/guidance/TrafficUpdater;

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->route:Lcom/here/android/mpa/routing/Route;

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->onRequestListener:Lcom/here/android/mpa/guidance/TrafficUpdater$Listener;

    invoke-virtual {v3, v4, v5}, Lcom/here/android/mpa/guidance/TrafficUpdater;->request(Lcom/here/android/mpa/routing/Route;Lcom/here/android/mpa/guidance/TrafficUpdater$Listener;)Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->currentRequestInfo:Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;

    .line 305
    :goto_1
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->currentRequestInfo:Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;

    invoke-virtual {v3}, Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;->getError()Lcom/here/android/mpa/guidance/TrafficUpdater$Error;

    move-result-object v1

    .line 306
    .local v1, "error":Lcom/here/android/mpa/guidance/TrafficUpdater$Error;
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "startRequest: returned:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 308
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$5;->$SwitchMap$com$here$android$mpa$guidance$TrafficUpdater$Error:[I

    invoke-virtual {v1}, Lcom/here/android/mpa/guidance/TrafficUpdater$Error;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 316
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->lastRequestTime:J

    .line 317
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->bus:Lcom/squareup/otto/Bus;

    sget-object v4, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->FAILED_EVENT:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;

    invoke-virtual {v3, v4}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 318
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->getRefereshInterval()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    invoke-direct {p0, v3}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->reset(I)V

    goto/16 :goto_0

    .line 294
    .end local v1    # "error":Lcom/here/android/mpa/guidance/TrafficUpdater$Error;
    :cond_2
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v0

    .line 295
    .local v0, "coordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    if-nez v0, :cond_3

    .line 296
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "startRequest: getting area traffic, no current coordinate"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 297
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->lastRequestTime:J

    .line 298
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->bus:Lcom/squareup/otto/Bus;

    sget-object v4, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->FAILED_EVENT:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;

    invoke-virtual {v3, v4}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 299
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->getRefereshInterval()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    invoke-direct {p0, v3}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->reset(I)V

    goto/16 :goto_0

    .line 302
    :cond_3
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "startRequest: getting area traffic"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 303
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->trafficUpdater:Lcom/here/android/mpa/guidance/TrafficUpdater;

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->onRequestListener:Lcom/here/android/mpa/guidance/TrafficUpdater$Listener;

    invoke-virtual {v3, v0, v4}, Lcom/here/android/mpa/guidance/TrafficUpdater;->request(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/guidance/TrafficUpdater$Listener;)Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->currentRequestInfo:Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;

    goto :goto_1

    .line 310
    .end local v0    # "coordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    .restart local v1    # "error":Lcom/here/android/mpa/guidance/TrafficUpdater$Error;
    :pswitch_0
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->handler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->checkRunnable:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 311
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->handler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->checkRunnable:Ljava/lang/Runnable;

    sget v5, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->TRAFFIC_DATA_CHECK_TIME_INTERVAL:I

    int-to-long v6, v5

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 312
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->lastRequestTime:J
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 308
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method getRefereshInterval()I
    .locals 1

    .prologue
    .line 635
    invoke-static {}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->getInstance()Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->isLimitBandwidthModeOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 636
    sget v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->TRAFFIC_DATA_REFRESH_INTERVAL_LIMITED_BANDWIDTH:I

    .line 638
    :goto_0
    return v0

    :cond_0
    sget v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->TRAFFIC_DATA_REFRESH_INTERVAL:I

    goto :goto_0
.end method

.method isRunning()Z
    .locals 1

    .prologue
    .line 631
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->route:Lcom/here/android/mpa/routing/Route;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setRoute(Lcom/here/android/mpa/routing/Route;)V
    .locals 3
    .param p1, "route"    # Lcom/here/android/mpa/routing/Route;

    .prologue
    .line 257
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setRoute:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 258
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->cancelRequest()V

    .line 259
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->route:Lcom/here/android/mpa/routing/Route;

    .line 260
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->startRequest()V

    .line 261
    return-void
.end method

.method public start()V
    .locals 2

    .prologue
    .line 252
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "start"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 253
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->startRequest()V

    .line 254
    return-void
.end method
