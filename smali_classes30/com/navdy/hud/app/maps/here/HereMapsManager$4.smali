.class Lcom/navdy/hud/app/maps/here/HereMapsManager$4;
.super Ljava/lang/Object;
.source "HereMapsManager.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/here/HereMapsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereMapsManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .prologue
    .line 605
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 9
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    const/4 v5, 0x2

    .line 608
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onSharedPreferenceChanged: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 609
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v4

    if-nez v4, :cond_0

    .line 610
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onSharedPreferenceChanged: map engine not intialized:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 665
    :goto_0
    return-void

    .line 616
    :cond_0
    const/4 v4, -0x1

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    :cond_1
    :goto_1
    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 618
    :pswitch_0
    invoke-interface {p1, p2, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 619
    .local v2, "val":Ljava/lang/String;
    if-nez v2, :cond_2

    .line 620
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "onSharedPreferenceChanged:no tilt"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 616
    .end local v2    # "val":Ljava/lang/String;
    :sswitch_0
    const-string v6, "map.tilt"

    invoke-virtual {p2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v4, 0x0

    goto :goto_1

    :sswitch_1
    const-string v6, "map.zoom"

    invoke-virtual {p2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v4, 0x1

    goto :goto_1

    :sswitch_2
    const-string v6, "map.scheme"

    invoke-virtual {p2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    move v4, v5

    goto :goto_1

    :sswitch_3
    const-string v6, "map.animation.mode"

    invoke-virtual {p2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v4, 0x3

    goto :goto_1

    .line 625
    .restart local v2    # "val":Ljava/lang/String;
    :cond_2
    :try_start_0
    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 630
    .local v1, "tilt":F
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onSharedPreferenceChanged:tilt:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 631
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$500(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/navdy/hud/app/maps/here/HereMapController;->setTilt(F)V

    goto :goto_0

    .line 626
    .end local v1    # "tilt":F
    :catch_0
    move-exception v0

    .line 627
    .local v0, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "onSharedPreferenceChanged:tilt"

    invoke-virtual {v4, v5, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 635
    .end local v0    # "t":Ljava/lang/Throwable;
    .end local v2    # "val":Ljava/lang/String;
    :pswitch_1
    invoke-interface {p1, p2, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 636
    .restart local v2    # "val":Ljava/lang/String;
    if-nez v2, :cond_3

    .line 637
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "onSharedPreferenceChanged:no zoom"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 642
    :cond_3
    :try_start_1
    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    move-result v3

    .line 647
    .local v3, "zoom":F
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onSharedPreferenceChanged:zoom:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 648
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$500(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v4

    float-to-double v6, v3

    invoke-virtual {v4, v6, v7}, Lcom/navdy/hud/app/maps/here/HereMapController;->setZoomLevel(D)V

    goto/16 :goto_0

    .line 643
    .end local v3    # "zoom":F
    :catch_1
    move-exception v0

    .line 644
    .restart local v0    # "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "onSharedPreferenceChanged:zoom"

    invoke-virtual {v4, v5, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 652
    .end local v0    # "t":Ljava/lang/Throwable;
    .end local v2    # "val":Ljava/lang/String;
    :pswitch_2
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v4

    new-instance v6, Lcom/navdy/hud/app/maps/here/HereMapsManager$4$1;

    invoke-direct {v6, p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager$4$1;-><init>(Lcom/navdy/hud/app/maps/here/HereMapsManager$4;)V

    invoke-virtual {v4, v6, v5}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto/16 :goto_0

    .line 616
    nop

    :sswitch_data_0
    .sparse-switch
        -0x413e8761 -> :sswitch_3
        -0x11094989 -> :sswitch_2
        0x7f9e00f -> :sswitch_0
        0x7fcb125 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
