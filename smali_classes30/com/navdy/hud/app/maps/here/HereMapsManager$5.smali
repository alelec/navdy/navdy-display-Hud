.class Lcom/navdy/hud/app/maps/here/HereMapsManager$5;
.super Ljava/lang/Object;
.source "HereMapsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereMapsManager;->checkforNetworkProvider()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereMapsManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .prologue
    .line 685
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$5;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x3e8

    .line 688
    const/4 v0, 0x0

    .line 690
    .local v0, "checkAgain":Z
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "location"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/LocationManager;

    .line 691
    .local v1, "locationManager":Landroid/location/LocationManager;
    const-string v4, "network"

    invoke-virtual {v1, v4}, Landroid/location/LocationManager;->getProvider(Ljava/lang/String;)Landroid/location/LocationProvider;

    move-result-object v2

    .line 692
    .local v2, "provider":Landroid/location/LocationProvider;
    if-nez v2, :cond_1

    .line 693
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "n/w provider not found yet, check again"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 694
    const/4 v0, 0x1

    .line 708
    :goto_0
    if-eqz v0, :cond_0

    .line 709
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$5;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->handler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$400(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, p0, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 712
    .end local v1    # "locationManager":Landroid/location/LocationManager;
    .end local v2    # "provider":Landroid/location/LocationProvider;
    :cond_0
    :goto_1
    return-void

    .line 696
    .restart local v1    # "locationManager":Landroid/location/LocationManager;
    .restart local v2    # "provider":Landroid/location/LocationProvider;
    :cond_1
    :try_start_1
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "n/w provider found, initialize here"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 697
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v4

    new-instance v5, Lcom/navdy/hud/app/maps/here/HereMapsManager$5$1;

    invoke-direct {v5, p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager$5$1;-><init>(Lcom/navdy/hud/app/maps/here/HereMapsManager$5;)V

    const/4 v6, 0x2

    invoke-virtual {v4, v5, v6}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 704
    .end local v1    # "locationManager":Landroid/location/LocationManager;
    .end local v2    # "provider":Landroid/location/LocationProvider;
    :catch_0
    move-exception v3

    .line 705
    .local v3, "t":Ljava/lang/Throwable;
    :try_start_2
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 706
    const/4 v0, 0x1

    .line 708
    if-eqz v0, :cond_0

    .line 709
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$5;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->handler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$400(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, p0, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 708
    .end local v3    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v4

    if-eqz v0, :cond_2

    .line 709
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$5;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->handler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$400(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Landroid/os/Handler;

    move-result-object v5

    invoke-virtual {v5, p0, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_2
    throw v4
.end method
