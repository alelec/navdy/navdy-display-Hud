.class Lcom/navdy/hud/app/maps/here/HereMapController$12;
.super Ljava/lang/Object;
.source "HereMapController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereMapController;->zoomTo(Lcom/here/android/mpa/common/GeoBoundingBox;Lcom/here/android/mpa/common/ViewRect;Lcom/here/android/mpa/mapping/Map$Animation;F)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereMapController;

.field final synthetic val$animation:Lcom/here/android/mpa/mapping/Map$Animation;

.field final synthetic val$boundingBox:Lcom/here/android/mpa/common/GeoBoundingBox;

.field final synthetic val$orientation:F

.field final synthetic val$viewRect:Lcom/here/android/mpa/common/ViewRect;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereMapController;Lcom/here/android/mpa/common/GeoBoundingBox;Lcom/here/android/mpa/common/ViewRect;Lcom/here/android/mpa/mapping/Map$Animation;F)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereMapController;

    .prologue
    .line 273
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapController$12;->this$0:Lcom/navdy/hud/app/maps/here/HereMapController;

    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereMapController$12;->val$boundingBox:Lcom/here/android/mpa/common/GeoBoundingBox;

    iput-object p3, p0, Lcom/navdy/hud/app/maps/here/HereMapController$12;->val$viewRect:Lcom/here/android/mpa/common/ViewRect;

    iput-object p4, p0, Lcom/navdy/hud/app/maps/here/HereMapController$12;->val$animation:Lcom/here/android/mpa/mapping/Map$Animation;

    iput p5, p0, Lcom/navdy/hud/app/maps/here/HereMapController$12;->val$orientation:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 277
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapController$12;->this$0:Lcom/navdy/hud/app/maps/here/HereMapController;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapController;->map:Lcom/here/android/mpa/mapping/Map;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereMapController;->access$000(Lcom/navdy/hud/app/maps/here/HereMapController;)Lcom/here/android/mpa/mapping/Map;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapController$12;->val$boundingBox:Lcom/here/android/mpa/common/GeoBoundingBox;

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereMapController$12;->val$viewRect:Lcom/here/android/mpa/common/ViewRect;

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereMapController$12;->val$animation:Lcom/here/android/mpa/mapping/Map$Animation;

    iget v5, p0, Lcom/navdy/hud/app/maps/here/HereMapController$12;->val$orientation:F

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/here/android/mpa/mapping/Map;->zoomTo(Lcom/here/android/mpa/common/GeoBoundingBox;Lcom/here/android/mpa/common/ViewRect;Lcom/here/android/mpa/mapping/Map$Animation;F)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 282
    :goto_0
    return-void

    .line 278
    :catch_0
    move-exception v0

    .line 280
    .local v0, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapController;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapController;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
