.class Lcom/navdy/hud/app/maps/here/HereNavigationManager$10;
.super Ljava/lang/Object;
.source "HereNavigationManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereNavigationManager;->setBandwidthPreferences()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    .prologue
    .line 2028
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$10;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 2032
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "limitbandwidth: turn off HERE traffic avoidance mode"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2033
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$10;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereNavController:Lcom/navdy/hud/app/maps/here/HereNavController;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$700(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Lcom/navdy/hud/app/maps/here/HereNavController;

    move-result-object v0

    sget-object v1, Lcom/here/android/mpa/guidance/NavigationManager$TrafficAvoidanceMode;->DISABLE:Lcom/here/android/mpa/guidance/NavigationManager$TrafficAvoidanceMode;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereNavController;->setTrafficAvoidanceMode(Lcom/here/android/mpa/guidance/NavigationManager$TrafficAvoidanceMode;)V

    .line 2034
    return-void
.end method
