.class Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$2;
.super Ljava/lang/Object;
.source "NavdyTrafficRerouteManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$2;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 64
    :try_start_0
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "stopping route calc"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 65
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$2;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->routeCalculator:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$200(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->cancel()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 69
    :goto_0
    return-void

    .line 66
    :catch_0
    move-exception v0

    .line 67
    .local v0, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
