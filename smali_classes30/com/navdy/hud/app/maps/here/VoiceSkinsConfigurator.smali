.class Lcom/navdy/hud/app/maps/here/VoiceSkinsConfigurator;
.super Ljava/lang/Object;
.source "VoiceSkinsConfigurator.java"


# static fields
.field private static final resource:Lcom/navdy/hud/app/util/PackagedResource;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 22
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/maps/here/VoiceSkinsConfigurator;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/VoiceSkinsConfigurator;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 23
    new-instance v0, Lcom/navdy/hud/app/util/PackagedResource;

    const-string v1, "voiceskins"

    invoke-static {}, Lcom/navdy/hud/app/storage/PathManager;->getInstance()Lcom/navdy/hud/app/storage/PathManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/storage/PathManager;->getHereVoiceSkinsPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/util/PackagedResource;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/VoiceSkinsConfigurator;->resource:Lcom/navdy/hud/app/util/PackagedResource;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static updateVoiceSkins()V
    .locals 10

    .prologue
    .line 27
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 28
    .local v0, "context":Landroid/content/Context;
    invoke-static {}, Lcom/navdy/hud/app/storage/PathManager;->getInstance()Lcom/navdy/hud/app/storage/PathManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/hud/app/storage/PathManager;->getHereVoiceSkinsPath()Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f070004

    invoke-static {v0, v6, v7}, Lcom/navdy/service/library/util/IOUtils;->checkIntegrity(Landroid/content/Context;Ljava/lang/String;I)V

    .line 30
    sget-object v6, Lcom/navdy/hud/app/maps/here/VoiceSkinsConfigurator;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "Voice skins update: starting"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 31
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 32
    .local v2, "l1":J
    sget-object v6, Lcom/navdy/hud/app/maps/here/VoiceSkinsConfigurator;->resource:Lcom/navdy/hud/app/util/PackagedResource;

    const v7, 0x7f070003

    const v8, 0x7f070005

    const/4 v9, 0x1

    invoke-virtual {v6, v0, v7, v8, v9}, Lcom/navdy/hud/app/util/PackagedResource;->updateFromResources(Landroid/content/Context;IIZ)V

    .line 33
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 34
    .local v4, "l2":J
    sget-object v6, Lcom/navdy/hud/app/maps/here/VoiceSkinsConfigurator;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Voice skins update: time took "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sub-long v8, v4, v2

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " ms"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 38
    .end local v2    # "l1":J
    .end local v4    # "l2":J
    :goto_0
    return-void

    .line 35
    :catch_0
    move-exception v1

    .line 36
    .local v1, "throwable":Ljava/lang/Throwable;
    sget-object v6, Lcom/navdy/hud/app/maps/here/VoiceSkinsConfigurator;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "Voice skins error"

    invoke-virtual {v6, v7, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
