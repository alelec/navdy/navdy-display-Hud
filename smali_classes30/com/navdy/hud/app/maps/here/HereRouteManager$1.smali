.class final Lcom/navdy/hud/app/maps/here/HereRouteManager$1;
.super Ljava/lang/Object;
.source "HereRouteManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereRouteManager;->handleRouteRequest(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$routeRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;


# direct methods
.method constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$1;->val$routeRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 114
    :try_start_0
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$1;->val$routeRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->printRouteRequest(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V

    .line 115
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$000()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v5

    if-nez v5, :cond_0

    .line 116
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    const-string v6, "engine not ready"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 117
    sget-object v5, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_NOT_READY:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$1;->val$routeRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->context:Landroid/content/Context;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$200()Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f0901a2

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    # invokes: Lcom/navdy/hud/app/maps/here/HereRouteManager;->returnErrorResponse(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;)V
    invoke-static {v5, v6, v7}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$300(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;)V

    .line 198
    :goto_0
    return-void

    .line 120
    :cond_0
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$000()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v2

    .line 121
    .local v2, "hereLocationFixManager":Lcom/navdy/hud/app/maps/here/HereLocationFixManager;
    if-nez v2, :cond_1

    .line 122
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    const-string v6, "engine not ready, location fix manager n/a"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 123
    sget-object v5, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_NOT_READY:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$1;->val$routeRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->context:Landroid/content/Context;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$200()Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f0901a2

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    # invokes: Lcom/navdy/hud/app/maps/here/HereRouteManager;->returnErrorResponse(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;)V
    invoke-static {v5, v6, v7}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$300(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 194
    .end local v2    # "hereLocationFixManager":Lcom/navdy/hud/app/maps/here/HereLocationFixManager;
    :catch_0
    move-exception v4

    .line 195
    .local v4, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 196
    sget-object v5, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_UNKNOWN_ERROR:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$1;->val$routeRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->context:Landroid/content/Context;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$200()Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f0902da

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    # invokes: Lcom/navdy/hud/app/maps/here/HereRouteManager;->returnErrorResponse(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;)V
    invoke-static {v5, v6, v7}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$300(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;)V

    goto :goto_0

    .line 128
    .end local v4    # "t":Ljava/lang/Throwable;
    .restart local v2    # "hereLocationFixManager":Lcom/navdy/hud/app/maps/here/HereLocationFixManager;
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->getInstance()Lcom/navdy/hud/app/framework/network/NetworkStateManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->isNetworkStateInitialized()Z

    move-result v5

    if-nez v5, :cond_2

    .line 129
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    const-string v6, "network state is not initialized yet"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 130
    sget-object v5, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_NOT_READY:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$1;->val$routeRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->context:Landroid/content/Context;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$200()Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f0901d6

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    # invokes: Lcom/navdy/hud/app/maps/here/HereRouteManager;->returnErrorResponse(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;)V
    invoke-static {v5, v6, v7}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$300(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;)V

    goto :goto_0

    .line 136
    :cond_2
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$000()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getRouteStartPoint()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v1

    .line 137
    .local v1, "geoPosition":Lcom/here/android/mpa/common/GeoCoordinate;
    if-nez v1, :cond_3

    .line 138
    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v1

    .line 143
    :goto_1
    if-nez v1, :cond_4

    .line 144
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    const-string v6, "start location n/a"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 145
    sget-object v5, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_NO_LOCATION_SERVICE:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$1;->val$routeRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->context:Landroid/content/Context;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$200()Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f0901d9

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    # invokes: Lcom/navdy/hud/app/maps/here/HereRouteManager;->returnErrorResponse(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;)V
    invoke-static {v5, v6, v7}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$300(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 140
    :cond_3
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "using debug start point:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_1

    .line 149
    :cond_4
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$1;->val$routeRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v5, v5, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestId:Ljava/lang/String;

    if-nez v5, :cond_5

    .line 150
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    const-string v6, "no request id specified"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 151
    sget-object v5, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_INVALID_REQUEST:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$1;->val$routeRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    const-string v7, "no route id"

    # invokes: Lcom/navdy/hud/app/maps/here/HereRouteManager;->returnErrorResponse(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;)V
    invoke-static {v5, v6, v7}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$300(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 156
    :cond_5
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    const-string v6, "reset traffic reroute listener"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 157
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->resetTrafficRerouteListener()V

    .line 159
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->lockObj:Ljava/lang/Object;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$400()Ljava/lang/Object;

    move-result-object v6

    monitor-enter v6
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 160
    const/4 v0, 0x0

    .line 161
    .local v0, "activeRouteId":Ljava/lang/String;
    :try_start_2
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$500()Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    move-result-object v5

    if-nez v5, :cond_6

    .line 162
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->isUIShowingRouteCalculation()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 165
    :cond_6
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$1;->val$routeRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->isSavedRoute(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 168
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    const-string v7, "saved route cannot override an existing route calculation"

    invoke-virtual {v5, v7}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 169
    sget-object v5, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_ALREADY_IN_PROGRESS:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v7, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$1;->val$routeRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    const/4 v8, 0x0

    # invokes: Lcom/navdy/hud/app/maps/here/HereRouteManager;->returnErrorResponse(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;)V
    invoke-static {v5, v7, v8}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$300(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;)V

    .line 170
    monitor-exit v6

    goto/16 :goto_0

    .line 183
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v5
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0

    .line 173
    :cond_7
    :try_start_4
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$500()Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    move-result-object v5

    if-eqz v5, :cond_8

    .line 174
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$500()Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    move-result-object v5

    iget-object v0, v5, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestId:Ljava/lang/String;

    .line 178
    :cond_8
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->pendingRouteRequestIds:Ljava/util/HashSet;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$600()Ljava/util/HashSet;

    move-result-object v5

    iget-object v7, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$1;->val$routeRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v7, v7, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestId:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_9

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$1;->val$routeRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v5, v5, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestId:Ljava/lang/String;

    .line 179
    invoke-static {v5, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 180
    :cond_9
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "route request["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$1;->val$routeRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v8, v8, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestId:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] already received"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 181
    monitor-exit v6

    goto/16 :goto_0

    .line 183
    :cond_a
    monitor-exit v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 186
    :try_start_5
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v3

    .line 187
    .local v3, "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hasArrived()Z

    move-result v5

    if-eqz v5, :cond_b

    .line 188
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    const-string v6, "stop arrival mode"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 189
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->setIgnoreArrived(Z)V

    .line 190
    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->stopNavigation()V

    .line 193
    :cond_b
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$1;->val$routeRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    # invokes: Lcom/navdy/hud/app/maps/here/HereRouteManager;->handleRouteRequest(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/common/GeoCoordinate;)V
    invoke-static {v5, v1}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$700(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/common/GeoCoordinate;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0

    goto/16 :goto_0
.end method
