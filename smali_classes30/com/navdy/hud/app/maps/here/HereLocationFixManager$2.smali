.class Lcom/navdy/hud/app/maps/here/HereLocationFixManager$2;
.super Ljava/lang/Object;
.source "HereLocationFixManager.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/here/HereLocationFixManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .locals 8
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 136
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->locationFix:Z
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$100(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 137
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    invoke-virtual {v1, p1}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->setMaptoLocation(Landroid/location/Location;)V

    .line 140
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsUtils;->isDebugRawGpsPosEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 143
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->rawLocationMarker:Lcom/here/android/mpa/mapping/MapMarker;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$800(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Lcom/here/android/mpa/mapping/MapMarker;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->rawLocationMarkerAdded:Z
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$900(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 144
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->rawLocationMarker:Lcom/here/android/mpa/mapping/MapMarker;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$800(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Lcom/here/android/mpa/mapping/MapMarker;

    move-result-object v1

    new-instance v2, Lcom/here/android/mpa/common/GeoCoordinate;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    invoke-virtual {v1, v2}, Lcom/here/android/mpa/mapping/MapMarker;->setCoordinate(Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/here/android/mpa/mapping/MapMarker;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 151
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->isRecording:Z
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$1000(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 153
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # invokes: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->recordRawLocation(Landroid/location/Location;)V
    invoke-static {v1, p1}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$1100(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;Landroid/location/Location;)V

    .line 156
    :cond_2
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    # setter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->lastAndroidLocationTime:J
    invoke-static {v1, v2, v3}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$002(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;J)J

    .line 157
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # setter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->lastAndroidLocation:Landroid/location/Location;
    invoke-static {v1, p1}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$402(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;Landroid/location/Location;)Landroid/location/Location;

    .line 158
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->lastAndroidLocationTime:J
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$000(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)J

    move-result-wide v2

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->lastAndroidLocationPostTime:J
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$1200(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    cmp-long v1, v2, v4

    if-gez v1, :cond_3

    .line 169
    :goto_1
    return-void

    .line 146
    :catch_0
    move-exception v0

    .line 147
    .local v0, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 165
    .end local v0    # "t":Ljava/lang/Throwable;
    :cond_3
    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v1

    const-string v2, "NAVDY_GPS_PROVIDER"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 166
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # invokes: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->sendLocation(Landroid/location/Location;)V
    invoke-static {v1, p1}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$1300(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;Landroid/location/Location;)V

    .line 168
    :cond_4
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->lastAndroidLocationTime:J
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$000(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)J

    move-result-wide v2

    # setter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->lastAndroidLocationPostTime:J
    invoke-static {v1, v2, v3}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$1202(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;J)J

    goto :goto_1
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 181
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 177
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;
    .param p2, "status"    # I
    .param p3, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 173
    return-void
.end method
