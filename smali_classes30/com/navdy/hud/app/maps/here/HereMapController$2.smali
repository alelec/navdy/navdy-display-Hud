.class Lcom/navdy/hud/app/maps/here/HereMapController$2;
.super Ljava/lang/Object;
.source "HereMapController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereMapController;->setZoomLevel(D)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereMapController;

.field final synthetic val$zoomLevel:D


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereMapController;D)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereMapController;

    .prologue
    .line 127
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapController$2;->this$0:Lcom/navdy/hud/app/maps/here/HereMapController;

    iput-wide p2, p0, Lcom/navdy/hud/app/maps/here/HereMapController$2;->val$zoomLevel:D

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 131
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapController$2;->this$0:Lcom/navdy/hud/app/maps/here/HereMapController;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapController;->map:Lcom/here/android/mpa/mapping/Map;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereMapController;->access$000(Lcom/navdy/hud/app/maps/here/HereMapController;)Lcom/here/android/mpa/mapping/Map;

    move-result-object v1

    iget-wide v2, p0, Lcom/navdy/hud/app/maps/here/HereMapController$2;->val$zoomLevel:D

    invoke-virtual {v1, v2, v3}, Lcom/here/android/mpa/mapping/Map;->setZoomLevel(D)Lcom/here/android/mpa/mapping/Map;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    :goto_0
    return-void

    .line 132
    :catch_0
    move-exception v0

    .line 134
    .local v0, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapController;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapController;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
