.class Lcom/navdy/hud/app/maps/here/HereMapAnimator$5;
.super Ljava/lang/Object;
.source "HereMapAnimator.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereMapAnimator;->startMapRendering()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereMapAnimator;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereMapAnimator;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    .prologue
    .line 622
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$5;->this$0:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 625
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$5;->this$0:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapAnimator;->currentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->access$700(Lcom/navdy/hud/app/maps/here/HereMapAnimator;)Lcom/here/android/mpa/common/GeoPosition;

    move-result-object v0

    .line 626
    .local v0, "pos":Lcom/here/android/mpa/common/GeoPosition;
    if-eqz v0, :cond_0

    .line 627
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$5;->this$0:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapAnimator;->mode:Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->access$2000(Lcom/navdy/hud/app/maps/here/HereMapAnimator;)Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;->NONE:Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;

    if-ne v1, v2, :cond_1

    .line 628
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$5;->this$0:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->setGeoPosition(Lcom/here/android/mpa/common/GeoPosition;)V

    .line 629
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapAnimator;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "rendering: set last pos"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 635
    :cond_0
    :goto_0
    return-void

    .line 631
    :cond_1
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapAnimator;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "rendering: set center"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 632
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$5;->this$0:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapAnimator;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->access$900(Lcom/navdy/hud/app/maps/here/HereMapAnimator;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v1

    invoke-virtual {v0}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v2

    sget-object v3, Lcom/here/android/mpa/mapping/Map$Animation;->NONE:Lcom/here/android/mpa/mapping/Map$Animation;

    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    invoke-virtual {v0}, Lcom/here/android/mpa/common/GeoPosition;->getHeading()D

    move-result-wide v6

    double-to-float v6, v6

    const/high16 v7, -0x40800000    # -1.0f

    invoke-virtual/range {v1 .. v7}, Lcom/navdy/hud/app/maps/here/HereMapController;->setCenter(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;DFF)V

    goto :goto_0
.end method
