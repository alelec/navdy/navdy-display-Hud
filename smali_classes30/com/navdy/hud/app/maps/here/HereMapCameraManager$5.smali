.class Lcom/navdy/hud/app/maps/here/HereMapCameraManager$5;
.super Ljava/lang/Object;
.source "HereMapCameraManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->animateCamera()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    .prologue
    .line 514
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$5;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 517
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$5;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->isFullAnimationSettingEnabled:Z
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$1900(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 518
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$5;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->hereMapAnimator:Lcom/navdy/hud/app/maps/here/HereMapAnimator;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$2000(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    move-result-object v1

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$5;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentDynamicZoom:D
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$900(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)D

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->setZoom(D)V

    .line 519
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$5;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->hereMapAnimator:Lcom/navdy/hud/app/maps/here/HereMapAnimator;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$2000(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    move-result-object v1

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$5;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentDynamicTilt:F
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$1000(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)F

    move-result v4

    invoke-virtual {v1, v4}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->setTilt(F)V

    .line 527
    :goto_0
    return-void

    .line 521
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$5;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    const/4 v4, 0x0

    # setter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->nTimesAnimated:I
    invoke-static {v1, v4}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$2202(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;I)I

    .line 522
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$5;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    const/4 v4, 0x1

    # setter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->animationOn:Z
    invoke-static {v1, v4}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$1402(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;Z)Z

    .line 523
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$5;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentDynamicZoom:D
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$900(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)D

    move-result-wide v4

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$5;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$300(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereMapController;->getZoomLevel()D

    move-result-wide v6

    sub-double/2addr v4, v6

    const-wide/high16 v6, 0x4014000000000000L    # 5.0

    div-double v2, v4, v6

    .line 524
    .local v2, "zoomStep":D
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$5;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentDynamicTilt:F
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$1000(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)F

    move-result v1

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$5;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$300(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereMapController;->getTilt()F

    move-result v4

    sub-float/2addr v1, v4

    const/high16 v4, 0x40a00000    # 5.0f

    div-float v0, v1, v4

    .line 525
    .local v0, "tiltStep":F
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$5;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$100(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Landroid/os/Handler;

    move-result-object v1

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$5;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # invokes: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->getAnimateCameraRunnable(DF)Ljava/lang/Runnable;
    invoke-static {v4, v2, v3, v0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$2300(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;DF)Ljava/lang/Runnable;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
