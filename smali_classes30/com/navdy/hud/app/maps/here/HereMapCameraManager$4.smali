.class Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4;
.super Ljava/lang/Object;
.source "HereMapCameraManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->onDialZoom(Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

.field final synthetic val$event:Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    .prologue
    .line 369
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4;->val$event:Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 22

    .prologue
    .line 373
    :try_start_0
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$9;->$SwitchMap$com$navdy$hud$app$maps$here$HereMapController$State:[I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$300(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/here/HereMapController;->getState()Lcom/navdy/hud/app/maps/here/HereMapController$State;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/here/HereMapController$State;->ordinal()I

    move-result v5

    aget v3, v3, v5

    packed-switch v3, :pswitch_data_0

    .line 493
    :cond_0
    :goto_0
    return-void

    .line 383
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$1300(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Lcom/here/android/mpa/common/GeoPosition;

    move-result-object v3

    if-nez v3, :cond_1

    .line 384
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v2

    .line 389
    .local v2, "c":Lcom/here/android/mpa/common/GeoCoordinate;
    :goto_1
    if-nez v2, :cond_2

    .line 390
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v5, "no location"

    invoke-virtual {v3, v5}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 490
    .end local v2    # "c":Lcom/here/android/mpa/common/GeoCoordinate;
    :catch_0
    move-exception v16

    .line 491
    .local v16, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v5, "onDialZoom"

    move-object/from16 v0, v16

    invoke-virtual {v3, v5, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 386
    .end local v16    # "t":Ljava/lang/Throwable;
    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$1300(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Lcom/here/android/mpa/common/GeoPosition;

    move-result-object v3

    invoke-virtual {v3}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v2

    .restart local v2    # "c":Lcom/here/android/mpa/common/GeoCoordinate;
    goto :goto_1

    .line 394
    :cond_2
    move-object v4, v2

    .line 396
    .local v4, "coordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->animationOn:Z
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$1400(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Z

    move-result v3

    if-nez v3, :cond_3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    .line 397
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->goBackfromOverviewOn:Z
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$1500(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Z

    move-result v3

    if-nez v3, :cond_3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    .line 398
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->goToOverviewOn:Z
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$1600(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Z

    move-result v3

    if-nez v3, :cond_3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    .line 399
    # invokes: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->isOverViewMapVisible()Z
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$600(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Z

    move-result v3

    if-eqz v3, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4;->val$event:Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom;

    iget-object v3, v3, Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom;->type:Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;

    sget-object v5, Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;->OUT:Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;

    if-ne v3, v5, :cond_5

    .line 404
    :cond_3
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const/4 v5, 0x2

    invoke-virtual {v3, v5}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 405
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onDialZoom no-op, animationOn: "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->animationOn:Z
    invoke-static {v8}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$1400(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Z

    move-result v8

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, " goBackfromOverviewOn:"

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    .line 406
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->goBackfromOverviewOn:Z
    invoke-static {v8}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$1500(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Z

    move-result v8

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, " goToOverviewOn:"

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    .line 407
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->goToOverviewOn:Z
    invoke-static {v8}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$1600(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Z

    move-result v8

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, " overviewMap on-out:"

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    .line 408
    # invokes: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->isOverViewMapVisible()Z
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$600(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Z

    move-result v3

    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4;->val$event:Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom;

    iget-object v3, v3, Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom;->type:Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;

    sget-object v9, Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;->OUT:Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;

    if-ne v3, v9, :cond_4

    const/4 v3, 0x1

    :goto_2
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 405
    invoke-virtual {v5, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 408
    :cond_4
    const/4 v3, 0x0

    goto :goto_2

    .line 414
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$300(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereMapController;->getZoomLevel()D

    move-result-wide v10

    .line 415
    .local v10, "currentMapZoom":D
    const-wide/16 v18, 0x0

    .line 417
    .local v18, "zoomStep":D
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    const/4 v5, 0x1

    # setter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->zoomActionOn:Z
    invoke-static {v3, v5}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$202(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;Z)Z

    .line 418
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->handler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$100(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Landroid/os/Handler;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->unlockZoom:Ljava/lang/Runnable;
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$1700(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Ljava/lang/Runnable;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 420
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$9;->$SwitchMap$com$navdy$hud$app$maps$MapEvents$DialMapZoom$Type:[I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4;->val$event:Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom;->type:Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;->ordinal()I

    move-result v5

    aget v3, v3, v5

    packed-switch v3, :pswitch_data_1

    .line 455
    :goto_3
    add-double v6, v10, v18

    .line 456
    .local v6, "newZoomLevel":D
    const-wide v8, 0x4030800000000000L    # 16.5

    cmpg-double v3, v10, v8

    if-gez v3, :cond_6

    const-wide v8, 0x4030800000000000L    # 16.5

    cmpl-double v3, v6, v8

    if-ltz v3, :cond_6

    .line 457
    const-wide v6, 0x4030800000000000L    # 16.5

    .line 460
    :cond_6
    const-wide v8, 0x4030800000000000L    # 16.5

    cmpg-double v3, v6, v8

    if-gtz v3, :cond_9

    const-wide/high16 v8, 0x4028000000000000L    # 12.0

    cmpl-double v3, v6, v8

    if-ltz v3, :cond_9

    const/4 v13, 0x1

    .line 461
    .local v13, "isInZoomRange":Z
    :goto_4
    add-double v8, v10, v18

    const-wide/high16 v20, 0x4028000000000000L    # 12.0

    cmpg-double v3, v8, v20

    if-gez v3, :cond_a

    const/4 v12, 0x1

    .line 463
    .local v12, "enteredOverviewRange":Z
    :goto_5
    if-eqz v13, :cond_c

    .line 464
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    .line 465
    .local v14, "l1":J
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->isFullAnimationSettingEnabled:Z
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$1900(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 466
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->hereMapAnimator:Lcom/navdy/hud/app/maps/here/HereMapAnimator;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$2000(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    move-result-object v3

    invoke-virtual {v3, v6, v7}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->setZoom(D)V

    .line 470
    :goto_6
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "zoomLevel:"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, " time:"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    sub-long/2addr v8, v14

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 489
    .end local v14    # "l1":J
    :cond_7
    :goto_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->handler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$100(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Landroid/os/Handler;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->unlockZoom:Ljava/lang/Runnable;
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$1700(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Ljava/lang/Runnable;

    move-result-object v5

    const-wide/16 v8, 0x2710

    invoke-virtual {v3, v5, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 422
    .end local v6    # "newZoomLevel":D
    .end local v12    # "enteredOverviewRange":Z
    .end local v13    # "isInZoomRange":Z
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # invokes: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->isOverViewMapVisible()Z
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$600(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 423
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    const/4 v5, 0x0

    # setter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->goToOverviewOn:Z
    invoke-static {v3, v5}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$1602(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;Z)Z

    .line 424
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->goBackfromOverviewOn:Z
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$1500(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 425
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v5, "goBackfromOverview:on"

    invoke-virtual {v3, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 426
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    const/4 v5, 0x1

    # setter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->goBackfromOverviewOn:Z
    invoke-static {v3, v5}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$1502(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;Z)Z

    .line 427
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->handler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$100(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Landroid/os/Handler;

    move-result-object v3

    new-instance v5, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4$1;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4$1;-><init>(Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4;)V

    invoke-virtual {v3, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 443
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->goBackfromOverviewOn:Z
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$1500(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 447
    const-wide/high16 v18, 0x3fd0000000000000L    # 0.25

    .line 448
    goto/16 :goto_3

    .line 451
    :pswitch_2
    const-wide/high16 v18, -0x4030000000000000L    # -0.25

    goto/16 :goto_3

    .line 460
    .restart local v6    # "newZoomLevel":D
    :cond_9
    const/4 v13, 0x0

    goto/16 :goto_4

    .line 461
    .restart local v13    # "isInZoomRange":Z
    :cond_a
    const/4 v12, 0x0

    goto/16 :goto_5

    .line 468
    .restart local v12    # "enteredOverviewRange":Z
    .restart local v14    # "l1":J
    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$300(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v3

    sget-object v5, Lcom/here/android/mpa/mapping/Map$Animation;->NONE:Lcom/here/android/mpa/mapping/Map$Animation;

    const/high16 v8, -0x40800000    # -1.0f

    const/high16 v9, -0x40800000    # -1.0f

    invoke-virtual/range {v3 .. v9}, Lcom/navdy/hud/app/maps/here/HereMapController;->setCenter(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;DFF)V

    goto/16 :goto_6

    .line 471
    .end local v14    # "l1":J
    :cond_c
    if-eqz v12, :cond_7

    .line 472
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    const/4 v5, 0x0

    # setter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->goBackfromOverviewOn:Z
    invoke-static {v3, v5}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$1502(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;Z)Z

    .line 473
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    const/4 v5, 0x1

    # setter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->goToOverviewOn:Z
    invoke-static {v3, v5}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$1602(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;Z)Z

    .line 474
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v5, "goToOverviewOn:on"

    invoke-virtual {v3, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 475
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->handler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$100(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Landroid/os/Handler;

    move-result-object v3

    new-instance v5, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4$2;

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v4}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4$2;-><init>(Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4;Lcom/here/android/mpa/common/GeoCoordinate;)V

    invoke-virtual {v3, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 487
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v5, "zoomLevel: overview"

    invoke-virtual {v3, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_7

    .line 373
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 420
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
