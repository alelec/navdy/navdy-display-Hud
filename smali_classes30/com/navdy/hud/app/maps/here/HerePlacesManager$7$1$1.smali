.class Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1$1;
.super Ljava/lang/Object;
.source "HerePlacesManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1;->onCompleted(Lcom/here/android/mpa/search/DiscoveryResultPage;Lcom/here/android/mpa/search/ErrorCode;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1;

.field final synthetic val$error:Lcom/here/android/mpa/search/ErrorCode;

.field final synthetic val$results:Lcom/here/android/mpa/search/DiscoveryResultPage;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1;Lcom/here/android/mpa/search/ErrorCode;Lcom/here/android/mpa/search/DiscoveryResultPage;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1;

    .prologue
    .line 445
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1;

    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1$1;->val$error:Lcom/here/android/mpa/search/ErrorCode;

    iput-object p3, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1$1;->val$results:Lcom/here/android/mpa/search/DiscoveryResultPage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    .line 449
    :try_start_0
    iget-object v9, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1$1;->val$error:Lcom/here/android/mpa/search/ErrorCode;

    sget-object v10, Lcom/here/android/mpa/search/ErrorCode;->NONE:Lcom/here/android/mpa/search/ErrorCode;

    if-eq v9, v10, :cond_0

    .line 450
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Error in nearby categories response: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1$1;->val$error:Lcom/here/android/mpa/search/ErrorCode;

    invoke-virtual {v11}, Lcom/here/android/mpa/search/ErrorCode;->name()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 451
    # invokes: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->tryRestablishOnline()V
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$1000()V

    .line 452
    iget-object v9, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1;

    iget-object v9, v9, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1;->this$0:Lcom/navdy/hud/app/maps/here/HerePlacesManager$7;

    iget-object v9, v9, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7;->val$listener:Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;

    sget-object v10, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;->RESPONSE_ERROR:Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

    invoke-interface {v9, v10}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;->onError(Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 485
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    .line 486
    .local v6, "l2":J
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "handleCategoriesRequest- time-2 ="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1;

    iget-wide v12, v11, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1;->val$l1:J

    sub-long v12, v6, v12

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 488
    :goto_0
    return-void

    .line 456
    .end local v6    # "l2":J
    :cond_0
    :try_start_1
    iget-object v9, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1$1;->val$results:Lcom/here/android/mpa/search/DiscoveryResultPage;

    invoke-virtual {v9}, Lcom/here/android/mpa/search/DiscoveryResultPage;->getItems()Ljava/util/List;

    move-result-object v3

    .line 457
    .local v3, "discoveryResults":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/search/DiscoveryResult;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 459
    .local v5, "placeLinks":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/search/PlaceLink;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_1
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/here/android/mpa/search/DiscoveryResult;

    .line 460
    .local v2, "discoveryResult":Lcom/here/android/mpa/search/DiscoveryResult;
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v10

    const/4 v11, 0x2

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 461
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Found a nearby category item: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v2}, Lcom/here/android/mpa/search/DiscoveryResult;->getTitle()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 464
    :cond_2
    invoke-virtual {v2}, Lcom/here/android/mpa/search/DiscoveryResult;->getResultType()Lcom/here/android/mpa/search/DiscoveryResult$ResultType;

    move-result-object v10

    sget-object v11, Lcom/here/android/mpa/search/DiscoveryResult$ResultType;->PLACE:Lcom/here/android/mpa/search/DiscoveryResult$ResultType;

    if-ne v10, v11, :cond_1

    .line 465
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Discovered a place: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v2}, Lcom/here/android/mpa/search/DiscoveryResult;->getTitle()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 466
    move-object v0, v2

    check-cast v0, Lcom/here/android/mpa/search/PlaceLink;

    move-object v4, v0

    .line 467
    .local v4, "gasStationPlaceLink":Lcom/here/android/mpa/search/PlaceLink;
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 480
    .end local v2    # "discoveryResult":Lcom/here/android/mpa/search/DiscoveryResult;
    .end local v3    # "discoveryResults":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/search/DiscoveryResult;>;"
    .end local v4    # "gasStationPlaceLink":Lcom/here/android/mpa/search/PlaceLink;
    .end local v5    # "placeLinks":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/search/PlaceLink;>;"
    :catch_0
    move-exception v8

    .line 481
    .local v8, "t":Ljava/lang/Throwable;
    :try_start_2
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v9

    const-string v10, "HERE internal DiscoveryRequest.execute callback exception: "

    invoke-virtual {v9, v10, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 482
    # invokes: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->tryRestablishOnline()V
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$1000()V

    .line 483
    iget-object v9, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1;

    iget-object v9, v9, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1;->this$0:Lcom/navdy/hud/app/maps/here/HerePlacesManager$7;

    iget-object v9, v9, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7;->val$listener:Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;

    sget-object v10, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;->UNKNOWN_ERROR:Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

    invoke-interface {v9, v10}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;->onError(Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 485
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    .line 486
    .restart local v6    # "l2":J
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "handleCategoriesRequest- time-2 ="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1;

    iget-wide v12, v11, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1;->val$l1:J

    sub-long v12, v6, v12

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 471
    .end local v6    # "l2":J
    .end local v8    # "t":Ljava/lang/Throwable;
    .restart local v3    # "discoveryResults":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/search/DiscoveryResult;>;"
    .restart local v5    # "placeLinks":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/search/PlaceLink;>;"
    :cond_3
    :try_start_3
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v9

    if-nez v9, :cond_4

    .line 472
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v9

    const-string v10, "no places links found"

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 473
    # invokes: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->tryRestablishOnline()V
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$1000()V

    .line 474
    iget-object v9, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1;

    iget-object v9, v9, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1;->this$0:Lcom/navdy/hud/app/maps/here/HerePlacesManager$7;

    iget-object v9, v9, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7;->val$listener:Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;

    sget-object v10, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;->RESPONSE_ERROR:Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

    invoke-interface {v9, v10}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;->onError(Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 485
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    .line 486
    .restart local v6    # "l2":J
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "handleCategoriesRequest- time-2 ="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1;

    iget-wide v12, v11, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1;->val$l1:J

    sub-long v12, v6, v12

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 479
    .end local v6    # "l2":J
    :cond_4
    :try_start_4
    iget-object v9, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1;

    iget-object v9, v9, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1;->this$0:Lcom/navdy/hud/app/maps/here/HerePlacesManager$7;

    iget-object v9, v9, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7;->val$listener:Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;

    # invokes: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->handlePlaceLinksRequest(Ljava/util/List;Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;)V
    invoke-static {v5, v9}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$1100(Ljava/util/List;Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 485
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    .line 486
    .restart local v6    # "l2":J
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "handleCategoriesRequest- time-2 ="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1;

    iget-wide v12, v11, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1;->val$l1:J

    sub-long v12, v6, v12

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 485
    .end local v3    # "discoveryResults":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/search/DiscoveryResult;>;"
    .end local v5    # "placeLinks":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/search/PlaceLink;>;"
    .end local v6    # "l2":J
    :catchall_0
    move-exception v9

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    .line 486
    .restart local v6    # "l2":J
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "handleCategoriesRequest- time-2 ="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1;

    iget-wide v12, v12, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1;->val$l1:J

    sub-long v12, v6, v12

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 487
    throw v9
.end method
