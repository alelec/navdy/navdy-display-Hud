.class Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;
.super Lcom/here/android/mpa/guidance/NavigationManager$TrafficRerouteListener;
.source "HereTrafficRerouteListener.java"


# static fields
.field static final MIN_INTERVAL_REROUTE_FIRST:I = 0x5a

.field static final NOTIF_FROM_HERE_THRESHOLD:J

.field static final REROUTE_POINT_THRESHOLD:J

.field static final ROUTE_CHECK_INITIAL_INTERVAL:I

.field static final ROUTE_CHECK_INTERVAL:I

.field static final ROUTE_CHECK_INTERVAL_LIMIT_BANDWIDTH:I

.field static final WAIT_INTERVAL_AFTER_FIRST_MANEUVER_SHOWN:I


# instance fields
.field private final bus:Lcom/squareup/otto/Bus;

.field private currentProposedRoute:Lcom/here/android/mpa/routing/Route;

.field private fasterBy:J

.field private final hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

.field private lastTrafficNotifFromHere:J

.field private final logger:Lcom/navdy/service/library/log/Logger;

.field private final navController:Lcom/navdy/hud/app/maps/here/HereNavController;

.field private navdyTrafficRerouteManager:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

.field private notifFromHere:Z

.field private placeCalculated:Lcom/here/android/mpa/common/GeoCoordinate;

.field private startRunnable:Ljava/lang/Runnable;

.field private stopRunnable:Ljava/lang/Runnable;

.field private final tag:Ljava/lang/String;

.field private timeCalculated:J

.field private trafficRerouteListenerRunning:Z

.field private final verbose:Z

.field private viaString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x1

    .line 48
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3c

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->WAIT_INTERVAL_AFTER_FIRST_MANEUVER_SHOWN:I

    .line 50
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->ROUTE_CHECK_INITIAL_INTERVAL:I

    .line 51
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->ROUTE_CHECK_INTERVAL:I

    .line 52
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->ROUTE_CHECK_INTERVAL_LIMIT_BANDWIDTH:I

    .line 56
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x14

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->REROUTE_POINT_THRESHOLD:J

    .line 58
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->NOTIF_FROM_HERE_THRESHOLD:J

    return-void
.end method

.method constructor <init>(Ljava/lang/String;ZLcom/navdy/hud/app/maps/here/HereNavController;Lcom/navdy/hud/app/maps/here/HereNavigationManager;Lcom/squareup/otto/Bus;)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "verbose"    # Z
    .param p3, "navController"    # Lcom/navdy/hud/app/maps/here/HereNavController;
    .param p4, "hereNavigationManager"    # Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    .param p5, "bus"    # Lcom/squareup/otto/Bus;

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/here/android/mpa/guidance/NavigationManager$TrafficRerouteListener;-><init>()V

    .line 60
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-string v1, "HereTrafficReroute"

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;

    .line 80
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$1;-><init>(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->startRunnable:Ljava/lang/Runnable;

    .line 88
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$2;-><init>(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->stopRunnable:Ljava/lang/Runnable;

    .line 101
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "HereTrafficRerouteListener:ctor"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 102
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->tag:Ljava/lang/String;

    .line 103
    iput-boolean p2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->verbose:Z

    .line 104
    iput-object p3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->navController:Lcom/navdy/hud/app/maps/here/HereNavController;

    .line 105
    iput-object p4, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    .line 106
    iput-object p5, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->bus:Lcom/squareup/otto/Bus;

    .line 107
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/navdy/hud/app/maps/here/HereNavController;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->navController:Lcom/navdy/hud/app/maps/here/HereNavController;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/here/android/mpa/routing/Route;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->currentProposedRoute:Lcom/here/android/mpa/routing/Route;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->clearProposedRoute()V

    return-void
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->tag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;
    .param p1, "x1"    # Ljava/util/List;
    .param p2, "x2"    # Ljava/util/List;
    .param p3, "x3"    # Ljava/util/List;

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->isFasterRouteDivergePointCloseBy(Ljava/util/List;Ljava/util/List;Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->navdyTrafficRerouteManager:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    return-object v0
.end method

.method private clearProposedRoute()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 141
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "clearProposedRoute"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 142
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->currentProposedRoute:Lcom/here/android/mpa/routing/Route;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "clearProposedRoute:cleared"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 144
    invoke-static {}, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->getInstance()Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->removeFasterRouteNotifiation()V

    .line 145
    iput-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->currentProposedRoute:Lcom/here/android/mpa/routing/Route;

    .line 146
    iput-wide v4, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->timeCalculated:J

    .line 147
    iput-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->placeCalculated:Lcom/here/android/mpa/common/GeoCoordinate;

    .line 148
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->notifFromHere:Z

    .line 149
    iput-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->viaString:Ljava/lang/String;

    .line 150
    iput-wide v4, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->fasterBy:J

    .line 152
    :cond_0
    return-void
.end method

.method private isFasterRouteDivergePointCloseBy(Ljava/util/List;Ljava/util/List;Ljava/util/List;)Z
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/routing/Maneuver;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/routing/Maneuver;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/routing/Maneuver;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 472
    .local p1, "currentManeuvers":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    .local p2, "oldRouteDifferentManeuver":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    .local p3, "newRouteDifferentManeuver":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->navController:Lcom/navdy/hud/app/maps/here/HereNavController;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/navdy/hud/app/maps/here/HereNavController;->getNextManeuver()Lcom/here/android/mpa/routing/Maneuver;

    move-result-object v2

    .line 473
    .local v2, "currentManeuver":Lcom/here/android/mpa/routing/Maneuver;
    const/16 v16, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/here/android/mpa/routing/Maneuver;

    .line 475
    .local v12, "slowChange":Lcom/here/android/mpa/routing/Maneuver;
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v16

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-le v0, v1, :cond_2

    .line 476
    const/16 v16, 0x1

    move-object/from16 v0, p3

    move/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/here/android/mpa/routing/Maneuver;

    .line 481
    .local v3, "fasterChange":Lcom/here/android/mpa/routing/Maneuver;
    :goto_0
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v16

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-le v0, v1, :cond_3

    .line 482
    const/16 v16, 0x1

    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/here/android/mpa/routing/Maneuver;

    .line 486
    .local v10, "nextSlowChange":Lcom/here/android/mpa/routing/Maneuver;
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v16, v0

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->tag:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " diverge point road faster ["

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getRoadName(Lcom/here/android/mpa/routing/Maneuver;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, ","

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getNextRoadName(Lcom/here/android/mpa/routing/Maneuver;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "]  slower ["

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    .line 487
    invoke-static {v10}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getRoadName(Lcom/here/android/mpa/routing/Maneuver;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, ","

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-static {v10}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getNextRoadName(Lcom/here/android/mpa/routing/Maneuver;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "]"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 486
    invoke-virtual/range {v16 .. v17}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 488
    invoke-static {v2, v12}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->areManeuverEqual(Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;)Z

    move-result v16

    if-nez v16, :cond_a

    .line 489
    const/4 v11, -0x1

    .line 490
    .local v11, "offset":I
    if-nez v2, :cond_4

    .line 491
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v16, v0

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->tag:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "no current maneuver"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 503
    :cond_0
    :goto_2
    const/16 v16, -0x1

    move/from16 v0, v16

    if-eq v11, v0, :cond_9

    .line 504
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v16, v0

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->tag:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "reroute offset="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 505
    const-wide/16 v14, 0x0

    .line 506
    .local v14, "timeToManeuver":J
    const-wide/16 v4, 0x0

    .line 507
    .local v4, "distToManeuver":J
    new-instance v8, Lcom/navdy/hud/app/maps/here/HereMapUtil$LongContainer;

    invoke-direct {v8}, Lcom/navdy/hud/app/maps/here/HereMapUtil$LongContainer;-><init>()V

    .line 508
    .local v8, "longContainer":Lcom/navdy/hud/app/maps/here/HereMapUtil$LongContainer;
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v7

    .line 509
    .local v7, "len":I
    const/4 v13, 0x0

    .line 510
    .local v13, "slowManeuverFound":Z
    move v6, v11

    .local v6, "i":I
    :goto_3
    if-ge v6, v7, :cond_1

    .line 511
    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/here/android/mpa/routing/Maneuver;

    .line 512
    .local v9, "m":Lcom/here/android/mpa/routing/Maneuver;
    invoke-static {v9, v12}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->areManeuverEqual(Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;)Z

    move-result v16

    if-eqz v16, :cond_6

    .line 513
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v16, v0

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->tag:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "reroute diverge:"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, ","

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 514
    const/4 v13, 0x1

    .line 520
    .end local v9    # "m":Lcom/here/android/mpa/routing/Maneuver;
    :cond_1
    if-nez v13, :cond_7

    .line 521
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v16, v0

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->tag:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "reroute threshold slow maneuver not found"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 522
    const/16 v16, 0x1

    .line 537
    .end local v4    # "distToManeuver":J
    .end local v6    # "i":I
    .end local v7    # "len":I
    .end local v8    # "longContainer":Lcom/navdy/hud/app/maps/here/HereMapUtil$LongContainer;
    .end local v11    # "offset":I
    .end local v13    # "slowManeuverFound":Z
    .end local v14    # "timeToManeuver":J
    :goto_4
    return v16

    .line 478
    .end local v3    # "fasterChange":Lcom/here/android/mpa/routing/Maneuver;
    .end local v10    # "nextSlowChange":Lcom/here/android/mpa/routing/Maneuver;
    :cond_2
    const/16 v16, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/here/android/mpa/routing/Maneuver;

    .restart local v3    # "fasterChange":Lcom/here/android/mpa/routing/Maneuver;
    goto/16 :goto_0

    .line 484
    :cond_3
    move-object v10, v12

    .restart local v10    # "nextSlowChange":Lcom/here/android/mpa/routing/Maneuver;
    goto/16 :goto_1

    .line 493
    .restart local v11    # "offset":I
    :cond_4
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v7

    .line 494
    .restart local v7    # "len":I
    const/4 v6, 0x0

    .restart local v6    # "i":I
    :goto_5
    if-ge v6, v7, :cond_0

    .line 495
    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/here/android/mpa/routing/Maneuver;

    move-object/from16 v0, v16

    invoke-static {v0, v2}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->areManeuverEqual(Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;)Z

    move-result v16

    if-eqz v16, :cond_5

    .line 497
    move v11, v6

    .line 498
    goto/16 :goto_2

    .line 494
    :cond_5
    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    .line 517
    .restart local v4    # "distToManeuver":J
    .restart local v8    # "longContainer":Lcom/navdy/hud/app/maps/here/HereMapUtil$LongContainer;
    .restart local v9    # "m":Lcom/here/android/mpa/routing/Maneuver;
    .restart local v13    # "slowManeuverFound":Z
    .restart local v14    # "timeToManeuver":J
    :cond_6
    invoke-static {v9, v8}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getManeuverDriveTime(Lcom/here/android/mpa/routing/Maneuver;Lcom/navdy/hud/app/maps/here/HereMapUtil$LongContainer;)J

    move-result-wide v16

    add-long v14, v14, v16

    .line 518
    iget-wide v0, v8, Lcom/navdy/hud/app/maps/here/HereMapUtil$LongContainer;->val:J

    move-wide/from16 v16, v0

    add-long v4, v4, v16

    .line 510
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_3

    .line 524
    .end local v9    # "m":Lcom/here/android/mpa/routing/Maneuver;
    :cond_7
    sget-wide v16, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->REROUTE_POINT_THRESHOLD:J

    cmp-long v16, v14, v16

    if-ltz v16, :cond_8

    .line 525
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v16, v0

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->tag:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "reroute threshold ["

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "] is > "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    sget-wide v18, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->REROUTE_POINT_THRESHOLD:J

    invoke-virtual/range {v17 .. v19}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " dist="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 526
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->dismissReroute()V

    .line 527
    const/16 v16, 0x0

    goto/16 :goto_4

    .line 529
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v16, v0

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->tag:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "reroute point threshold ["

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "] distance["

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "]"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 537
    .end local v4    # "distToManeuver":J
    .end local v6    # "i":I
    .end local v7    # "len":I
    .end local v8    # "longContainer":Lcom/navdy/hud/app/maps/here/HereMapUtil$LongContainer;
    .end local v11    # "offset":I
    .end local v13    # "slowManeuverFound":Z
    .end local v14    # "timeToManeuver":J
    :goto_6
    const/16 v16, 0x1

    goto/16 :goto_4

    .line 531
    .restart local v11    # "offset":I
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v16, v0

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->tag:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "offset not found"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto :goto_6

    .line 534
    .end local v11    # "offset":I
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v16, v0

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->tag:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "first change and current are same"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto :goto_6
.end method


# virtual methods
.method public confirmReroute()V
    .locals 3

    .prologue
    .line 110
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$3;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$3;-><init>(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;)V

    const/16 v2, 0x14

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 134
    return-void
.end method

.method public dismissReroute()V
    .locals 0

    .prologue
    .line 137
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->clearProposedRoute()V

    .line 138
    return-void
.end method

.method fasterRouteNotFound(ZJLjava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "notifFromHereTraffic"    # Z
    .param p2, "diff"    # J
    .param p4, "via"    # Ljava/lang/String;
    .param p5, "currentVia"    # Ljava/lang/String;

    .prologue
    .line 564
    if-eqz p1, :cond_1

    const-string v0, "[Here Traffic]"

    .line 565
    .local v0, "fromTag":Ljava/lang/String;
    :goto_0
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->tag:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " new route threshold not met:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->getRerouteMinDuration()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 566
    const-wide/16 v4, 0x0

    cmp-long v3, p2, v4

    if-lez v3, :cond_0

    invoke-static {}, Lcom/navdy/hud/app/framework/voice/TTSUtils;->isDebugTTSEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 567
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Faster route below threshold"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 568
    .local v2, "title":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "via["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] faster by["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] seconds current via["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 569
    .local v1, "info":Ljava/lang/String;
    invoke-static {v2, v1}, Lcom/navdy/hud/app/framework/voice/TTSUtils;->debugShowFasterRouteToast(Ljava/lang/String;Ljava/lang/String;)V

    .line 571
    .end local v1    # "info":Ljava/lang/String;
    .end local v2    # "title":Ljava/lang/String;
    :cond_0
    return-void

    .line 564
    .end local v0    # "fromTag":Ljava/lang/String;
    :cond_1
    const-string v0, "[Navdy Calc]"

    goto :goto_0
.end method

.method public getCurrentProposedRoute()Lcom/here/android/mpa/routing/Route;
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->currentProposedRoute:Lcom/here/android/mpa/routing/Route;

    return-object v0
.end method

.method public getCurrentProposedRouteTime()J
    .locals 2

    .prologue
    .line 273
    iget-wide v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->timeCalculated:J

    return-wide v0
.end method

.method public getFasterBy()J
    .locals 2

    .prologue
    .line 281
    iget-wide v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->fasterBy:J

    return-wide v0
.end method

.method public getRerouteMinDuration()I
    .locals 10

    .prologue
    .line 578
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentTrafficRerouteCount()I

    move-result v0

    .line 579
    .local v0, "count":I
    const/4 v3, 0x1

    if-ge v0, v3, :cond_0

    .line 580
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "getRerouteMinDuration:90"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 581
    const/16 v1, 0x5a

    .line 586
    :goto_0
    return v1

    .line 583
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getLastTrafficRerouteTime()J

    move-result-wide v6

    sub-long/2addr v4, v6

    long-to-int v3, v4

    div-int/lit16 v2, v3, 0x3e8

    .line 584
    .local v2, "secondsTillLastDuration":I
    const-wide v4, 0x4056800000000000L    # 90.0

    const-wide/high16 v6, 0x3ff8000000000000L    # 1.5

    rsub-int v3, v2, 0x168

    const/4 v8, 0x0

    invoke-static {v3, v8}, Ljava/lang/Math;->max(II)I

    move-result v3

    int-to-double v8, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    double-to-int v1, v4

    .line 585
    .local v1, "ret":I
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getRerouteMinDuration:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " secslast:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getViaString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->viaString:Ljava/lang/String;

    return-object v0
.end method

.method public handleFasterRoute(Lcom/here/android/mpa/routing/Route;Z)V
    .locals 3
    .param p1, "fasterRoute"    # Lcom/here/android/mpa/routing/Route;
    .param p2, "notifFromHereTraffic"    # Z

    .prologue
    .line 285
    if-nez p1, :cond_0

    .line 286
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "null faster route:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 465
    :goto_0
    return-void

    .line 290
    :cond_0
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;

    invoke-direct {v1, p0, p2, p1}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener$4;-><init>(Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;ZLcom/here/android/mpa/routing/Route;)V

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public isMoreRouteNotificationComplete()Z
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 231
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isShownFirstManeuver()Z

    move-result v5

    if-nez v5, :cond_0

    .line 232
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "isMoreRouteNotificationComplete:not shown first maneuver"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 265
    :goto_0
    return v4

    .line 237
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->getInstance()Lcom/navdy/hud/app/framework/notifications/NotificationManager;

    move-result-object v5

    const-string v6, "navdy#route#calc#notif"

    invoke-virtual {v5, v6}, Lcom/navdy/hud/app/framework/notifications/NotificationManager;->isCurrentNotificationId(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 238
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "isMoreRouteNotificationComplete:route calc notif on"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 243
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getCurrentScreen()Lcom/navdy/hud/app/screen/BaseScreen;

    move-result-object v1

    .line 244
    .local v1, "screen":Lcom/navdy/hud/app/screen/BaseScreen;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/navdy/hud/app/screen/BaseScreen;->getScreen()Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v5

    sget-object v6, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DESTINATION_PICKER:Lcom/navdy/service/library/events/ui/Screen;

    if-ne v5, v6, :cond_2

    .line 245
    invoke-virtual {v1}, Lcom/navdy/hud/app/screen/BaseScreen;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 246
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz v0, :cond_2

    const-string v5, "ROUTE_PICKER"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 247
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "isMoreRouteNotificationComplete:user looking at route picker"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 253
    .end local v0    # "bundle":Landroid/os/Bundle;
    :cond_2
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getRouteCalcEventHandler()Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;->hasMoreRoutes()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 254
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "isMoreRouteNotificationComplete:user has not seen more routes"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 259
    :cond_3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getFirstManeuverShowntime()J

    move-result-wide v8

    sub-long v2, v6, v8

    .line 260
    .local v2, "diff":J
    sget v5, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->WAIT_INTERVAL_AFTER_FIRST_MANEUVER_SHOWN:I

    int-to-long v6, v5

    cmp-long v5, v2, v6

    if-gez v5, :cond_4

    .line 261
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isMoreRouteNotificationComplete:first maneuver just shown:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 265
    :cond_4
    const/4 v4, 0x1

    goto/16 :goto_0
.end method

.method public onTrafficRerouteBegin(Lcom/here/android/mpa/guidance/TrafficNotification;)V
    .locals 3
    .param p1, "trafficNotification"    # Lcom/here/android/mpa/guidance/TrafficNotification;

    .prologue
    .line 156
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " reroute-begin:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 157
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->verbose:Z

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->tag:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->printTrafficNotification(Lcom/here/android/mpa/guidance/TrafficNotification;Ljava/lang/String;)V

    .line 160
    :cond_0
    return-void
.end method

.method public onTrafficRerouteFailed(Lcom/here/android/mpa/guidance/TrafficNotification;)V
    .locals 3
    .param p1, "trafficNotification"    # Lcom/here/android/mpa/guidance/TrafficNotification;

    .prologue
    .line 184
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " reroute-failed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 185
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->tag:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->printTrafficNotification(Lcom/here/android/mpa/guidance/TrafficNotification;Ljava/lang/String;)V

    .line 186
    return-void
.end method

.method public onTrafficRerouteState(Lcom/here/android/mpa/guidance/NavigationManager$TrafficRerouteListener$TrafficEnabledRoutingState;)V
    .locals 0
    .param p1, "trafficEnabledRoutingState"    # Lcom/here/android/mpa/guidance/NavigationManager$TrafficRerouteListener$TrafficEnabledRoutingState;

    .prologue
    .line 194
    return-void
.end method

.method public onTrafficRerouted(Lcom/here/android/mpa/routing/Route;)V
    .locals 7
    .param p1, "fasterRoute"    # Lcom/here/android/mpa/routing/Route;

    .prologue
    .line 164
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 165
    .local v2, "now":J
    iget-wide v4, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->lastTrafficNotifFromHere:J

    sub-long v0, v2, v4

    .line 166
    .local v0, "diff":J
    sget-wide v4, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->NOTIF_FROM_HERE_THRESHOLD:J

    cmp-long v4, v0, v4

    if-gez v4, :cond_0

    .line 169
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->tag:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "here traffic notif came in "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 180
    :goto_0
    return-void

    .line 173
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->isMoreRouteNotificationComplete()Z

    move-result v4

    if-nez v4, :cond_1

    .line 174
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "onTrafficRerouted: more route notif in progress"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 178
    :cond_1
    iput-wide v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->lastTrafficNotifFromHere:J

    .line 179
    const/4 v4, 0x1

    invoke-virtual {p0, p1, v4}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->handleFasterRoute(Lcom/here/android/mpa/routing/Route;Z)V

    goto :goto_0
.end method

.method setFasterRoute(Lcom/here/android/mpa/routing/Route;ZLjava/lang/String;JLjava/lang/String;JJJ)V
    .locals 15
    .param p1, "fasterRoute"    # Lcom/here/android/mpa/routing/Route;
    .param p2, "notifFromHereTraffic"    # Z
    .param p3, "via"    # Ljava/lang/String;
    .param p4, "diff"    # J
    .param p6, "additionalVia"    # Ljava/lang/String;
    .param p7, "etaUtc"    # J
    .param p9, "distanceDiff"    # J
    .param p11, "newEta"    # J

    .prologue
    .line 548
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->incrCurrentTrafficRerouteCount()V

    .line 549
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->setLastTrafficRerouteTime(J)V

    .line 550
    move-object/from16 v0, p1

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->currentProposedRoute:Lcom/here/android/mpa/routing/Route;

    .line 551
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->timeCalculated:J

    .line 552
    move/from16 v0, p2

    iput-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->notifFromHere:Z

    .line 553
    move-object/from16 v0, p3

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->viaString:Ljava/lang/String;

    .line 554
    move-wide/from16 v0, p4

    iput-wide v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->fasterBy:J

    .line 555
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->placeCalculated:Lcom/here/android/mpa/common/GeoCoordinate;

    .line 556
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->bus:Lcom/squareup/otto/Bus;

    new-instance v3, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;

    move-object/from16 v4, p6

    move-object/from16 v5, p6

    move-wide/from16 v6, p4

    move-wide/from16 v8, p7

    move-wide/from16 v10, p9

    move-wide/from16 v12, p11

    invoke-direct/range {v3 .. v13}, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteEvent;-><init>(Ljava/lang/String;Ljava/lang/String;JJJJ)V

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 557
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "faster reroute event sent"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 558
    return-void
.end method

.method public setNavdyTrafficRerouteManager(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)V
    .locals 0
    .param p1, "navdyTrafficRerouteManager"    # Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    .prologue
    .line 574
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->navdyTrafficRerouteManager:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    .line 575
    return-void
.end method

.method public shouldCalculateFasterRoute()Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 214
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->timeCalculated:J

    sub-long v0, v4, v6

    .line 215
    .local v0, "elapsed":J
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "elapsed="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 217
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getRerouteInterval()I

    move-result v3

    add-int/lit16 v3, v3, -0x3a98

    int-to-long v4, v3

    cmp-long v3, v0, v4

    if-gez v3, :cond_0

    .line 226
    :goto_0
    return v2

    .line 221
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->isMoreRouteNotificationComplete()Z

    move-result v3

    if-nez v3, :cond_1

    .line 222
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "shouldCalculateFasterRoute: more route notif in progress"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 226
    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public start()V
    .locals 3

    .prologue
    .line 197
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->trafficRerouteListenerRunning:Z

    if-nez v0, :cond_0

    .line 198
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->trafficRerouteListenerRunning:Z

    .line 199
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->startRunnable:Ljava/lang/Runnable;

    const/16 v2, 0xf

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 200
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "added traffic reroute listener"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 202
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 3

    .prologue
    .line 205
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->trafficRerouteListenerRunning:Z

    if-eqz v0, :cond_0

    .line 206
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->trafficRerouteListenerRunning:Z

    .line 207
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->stopRunnable:Ljava/lang/Runnable;

    const/16 v2, 0xf

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 208
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "removed traffic reroute listener"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 210
    :cond_0
    return-void
.end method
