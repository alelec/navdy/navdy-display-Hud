.class Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;
.super Ljava/lang/Object;
.source "HereMapsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereMapsManager$3;->onPositionUpdated(Lcom/here/android/mpa/common/PositioningManager$LocationMethod;Lcom/here/android/mpa/common/GeoPosition;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$3;

.field final synthetic val$geoPosition:Lcom/here/android/mpa/common/GeoPosition;

.field final synthetic val$isMapMatched:Z

.field final synthetic val$locationMethod:Lcom/here/android/mpa/common/PositioningManager$LocationMethod;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereMapsManager$3;Lcom/here/android/mpa/common/GeoPosition;Lcom/here/android/mpa/common/PositioningManager$LocationMethod;Z)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/maps/here/HereMapsManager$3;

    .prologue
    .line 500
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$3;

    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;->val$geoPosition:Lcom/here/android/mpa/common/GeoPosition;

    iput-object p3, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;->val$locationMethod:Lcom/here/android/mpa/common/PositioningManager$LocationMethod;

    iput-boolean p4, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;->val$isMapMatched:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    const/4 v12, 0x2

    .line 504
    :try_start_0
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;->val$geoPosition:Lcom/here/android/mpa/common/GeoPosition;

    instance-of v5, v5, Lcom/here/android/mpa/common/MatchedGeoPosition;

    if-nez v5, :cond_1

    .line 505
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;->val$geoPosition:Lcom/here/android/mpa/common/GeoPosition;

    invoke-virtual {v5}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v0

    .line 506
    .local v0, "coordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "position not map-matched lat:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " lng:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " speed:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;->val$geoPosition:Lcom/here/android/mpa/common/GeoPosition;

    invoke-virtual {v7}, Lcom/here/android/mpa/common/GeoPosition;->getSpeed()D

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 570
    .end local v0    # "coordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    :cond_0
    :goto_0
    return-void

    .line 511
    :cond_1
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$3;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/here/HereMapsManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->extrapolationOn:Z
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$2800(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 512
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$3;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/here/HereMapsManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->positioningManager:Lcom/here/android/mpa/common/PositioningManager;
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$900(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Lcom/here/android/mpa/common/PositioningManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/here/android/mpa/common/PositioningManager;->getRoadElement()Lcom/here/android/mpa/common/RoadElement;

    move-result-object v5

    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->isInTunnel(Lcom/here/android/mpa/common/RoadElement;)Z

    move-result v4

    .line 513
    .local v4, "tunnel":Z
    if-nez v4, :cond_3

    .line 515
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    const-string v6, "TUNNEL extrapolation off"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 516
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$3;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/here/HereMapsManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    const/4 v6, 0x0

    # setter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->extrapolationOn:Z
    invoke-static {v5, v6}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$2802(Lcom/navdy/hud/app/maps/here/HereMapsManager;Z)Z

    .line 517
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$3;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/here/HereMapsManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # invokes: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sendExtrapolationEvent()V
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$2900(Lcom/navdy/hud/app/maps/here/HereMapsManager;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 527
    .end local v4    # "tunnel":Z
    :cond_2
    :goto_1
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$3;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/here/HereMapsManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->lastGeoPosition:Lcom/here/android/mpa/common/GeoPosition;
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$3000(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Lcom/here/android/mpa/common/GeoPosition;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 528
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$3;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/here/HereMapsManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->lastGeoPositionTime:J
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$3100(Lcom/navdy/hud/app/maps/here/HereMapsManager;)J

    move-result-wide v8

    sub-long v2, v6, v8

    .line 529
    .local v2, "t":J
    const-wide/16 v6, 0x1f4

    cmp-long v5, v2, v6

    if-gez v5, :cond_4

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$3;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/here/HereMapsManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->lastGeoPosition:Lcom/here/android/mpa/common/GeoPosition;
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$3000(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Lcom/here/android/mpa/common/GeoPosition;

    move-result-object v5

    invoke-virtual {v5}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;->val$geoPosition:Lcom/here/android/mpa/common/GeoPosition;

    invoke-virtual {v6}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->isCoordinateEqual(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoCoordinate;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 530
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    invoke-virtual {v5, v12}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 531
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "GEO-Here same pos as last:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 520
    .end local v2    # "t":J
    .restart local v4    # "tunnel":Z
    :cond_3
    :try_start_1
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$3;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/here/HereMapsManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # invokes: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sendExtrapolationEvent()V
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$2900(Lcom/navdy/hud/app/maps/here/HereMapsManager;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 523
    .end local v4    # "tunnel":Z
    :catch_0
    move-exception v2

    .line 524
    .local v2, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 537
    .end local v2    # "t":Ljava/lang/Throwable;
    :cond_4
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$3;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/here/HereMapsManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;->val$geoPosition:Lcom/here/android/mpa/common/GeoPosition;

    # setter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->lastGeoPosition:Lcom/here/android/mpa/common/GeoPosition;
    invoke-static {v5, v6}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$3002(Lcom/navdy/hud/app/maps/here/HereMapsManager;Lcom/here/android/mpa/common/GeoPosition;)Lcom/here/android/mpa/common/GeoPosition;

    .line 538
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$3;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/here/HereMapsManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    # setter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->lastGeoPositionTime:J
    invoke-static {v5, v6, v7}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$3102(Lcom/navdy/hud/app/maps/here/HereMapsManager;J)J

    .line 541
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$3;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/here/HereMapsManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->hereMapAnimator:Lcom/navdy/hud/app/maps/here/HereMapAnimator;
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$1100(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;->val$geoPosition:Lcom/here/android/mpa/common/GeoPosition;

    invoke-virtual {v5, v6}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->setGeoPosition(Lcom/here/android/mpa/common/GeoPosition;)V

    .line 544
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$3;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/here/HereMapsManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->hereLocationFixManager:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$1300(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;->val$locationMethod:Lcom/here/android/mpa/common/PositioningManager$LocationMethod;

    iget-object v7, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;->val$geoPosition:Lcom/here/android/mpa/common/GeoPosition;

    iget-boolean v8, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;->val$isMapMatched:Z

    invoke-virtual {v5, v6, v7, v8}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->onHerePositionUpdated(Lcom/here/android/mpa/common/PositioningManager$LocationMethod;Lcom/here/android/mpa/common/GeoPosition;Z)V

    .line 547
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;->val$geoPosition:Lcom/here/android/mpa/common/GeoPosition;

    invoke-virtual {v5, v6}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->onGeoPositionChange(Lcom/here/android/mpa/common/GeoPosition;)V

    .line 553
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$3;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/here/HereMapsManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$3200(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/manager/SpeedManager;->getObdSpeed()I

    move-result v5

    const/4 v6, -0x1

    if-ne v5, v6, :cond_5

    .line 554
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$3;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/here/HereMapsManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->hereLocationFixManager:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$1300(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLastLocationTime()J

    move-result-wide v8

    sub-long/2addr v6, v8

    const-wide/16 v8, 0x7d0

    cmp-long v5, v6, v8

    if-ltz v5, :cond_5

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;->val$geoPosition:Lcom/here/android/mpa/common/GeoPosition;

    instance-of v5, v5, Lcom/here/android/mpa/common/MatchedGeoPosition;

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;->val$geoPosition:Lcom/here/android/mpa/common/GeoPosition;

    check-cast v5, Lcom/here/android/mpa/common/MatchedGeoPosition;

    .line 556
    invoke-virtual {v5}, Lcom/here/android/mpa/common/MatchedGeoPosition;->isExtrapolated()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 557
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$3;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/here/HereMapsManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$3200(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;->val$geoPosition:Lcom/here/android/mpa/common/GeoPosition;

    invoke-virtual {v6}, Lcom/here/android/mpa/common/GeoPosition;->getSpeed()D

    move-result-wide v6

    double-to-float v6, v6

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v8

    const-wide/32 v10, 0xf4240

    div-long/2addr v8, v10

    invoke-virtual {v5, v6, v8, v9}, Lcom/navdy/hud/app/manager/SpeedManager;->setGpsSpeed(FJ)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 558
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$3;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/here/HereMapsManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/here/HereMapsManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v6, Lcom/navdy/hud/app/maps/MapEvents$GPSSpeedEvent;

    invoke-direct {v6}, Lcom/navdy/hud/app/maps/MapEvents$GPSSpeedEvent;-><init>()V

    invoke-virtual {v5, v6}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 562
    :cond_5
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$3;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/here/HereMapsManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/here/HereMapsManager;->bus:Lcom/squareup/otto/Bus;

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;->val$geoPosition:Lcom/here/android/mpa/common/GeoPosition;

    invoke-virtual {v5, v6}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 564
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    invoke-virtual {v5, v12}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 565
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;->val$geoPosition:Lcom/here/android/mpa/common/GeoPosition;

    invoke-virtual {v5}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v1

    .line 566
    .local v1, "geoCoordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "GEO-Here speed-mps["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;->val$geoPosition:Lcom/here/android/mpa/common/GeoPosition;

    invoke-virtual {v7}, Lcom/here/android/mpa/common/GeoPosition;->getSpeed()D

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] lat=["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 567
    invoke-virtual {v1}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] lon=["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] provider=["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;->val$locationMethod:Lcom/here/android/mpa/common/PositioningManager$LocationMethod;

    .line 568
    invoke-virtual {v7}, Lcom/here/android/mpa/common/PositioningManager$LocationMethod;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] timestamp:["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;->val$geoPosition:Lcom/here/android/mpa/common/GeoPosition;

    invoke-virtual {v7}, Lcom/here/android/mpa/common/GeoPosition;->getTimestamp()Ljava/util/Date;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 566
    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
