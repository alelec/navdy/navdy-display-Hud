.class Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2$1;
.super Ljava/lang/Object;
.source "HereMapsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$3:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;

.field final synthetic val$t3:J


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;J)V
    .locals 0
    .param p1, "this$3"    # Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;

    .prologue
    .line 301
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2$1;->this$3:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;

    iput-wide p2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2$1;->val$t3:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 305
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v1

    .line 308
    .local v1, "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2$1;->this$3:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;->this$2:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # invokes: Lcom/navdy/hud/app/maps/here/HereMapsManager;->registerConnectivityReceiver()V
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$1000(Lcom/navdy/hud/app/maps/here/HereMapsManager;)V

    .line 309
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "map created"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 311
    invoke-static {}, Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;->getInstance()Lcom/navdy/hud/app/maps/notification/TrafficNotificationManager;

    .line 321
    invoke-static {}, Lcom/navdy/hud/app/maps/MapSettings;->isCustomAnimationEnabled()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 322
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;->ANIMATION:Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;

    .line 329
    .local v0, "animationMode":Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;
    :goto_0
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "HereMapAnimator mode is ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 330
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2$1;->this$3:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;->this$2:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    new-instance v3, Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2$1;->this$3:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;->this$2:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$500(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v4

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getNavController()Lcom/navdy/hud/app/maps/here/HereNavController;

    move-result-object v5

    invoke-direct {v3, v0, v4, v5}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;-><init>(Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;Lcom/navdy/hud/app/maps/here/HereMapController;Lcom/navdy/hud/app/maps/here/HereNavController;)V

    # setter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->hereMapAnimator:Lcom/navdy/hud/app/maps/here/HereMapAnimator;
    invoke-static {v2, v3}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$1102(Lcom/navdy/hud/app/maps/here/HereMapsManager;Lcom/navdy/hud/app/maps/here/HereMapAnimator;)Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    .line 331
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2$1;->this$3:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;->this$2:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->initialMapRendering:Z
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$1200(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 332
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "HereMapAnimator MapRendering initial disabled"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 333
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2$1;->this$3:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;->this$2:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->hereMapAnimator:Lcom/navdy/hud/app/maps/here/HereMapAnimator;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$1100(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->stopMapRendering()V

    .line 335
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2$1;->this$3:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;

    iget-object v3, v3, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;->this$2:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;

    iget-object v3, v3, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    iget-object v3, v3, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$500(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2$1;->this$3:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;->this$2:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/HereMapsManager;->bus:Lcom/squareup/otto/Bus;

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2$1;->this$3:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;->this$2:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->hereMapAnimator:Lcom/navdy/hud/app/maps/here/HereMapAnimator;
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$1100(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->initialize(Lcom/navdy/hud/app/maps/here/HereMapController;Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/maps/here/HereMapAnimator;)V

    .line 337
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2$1;->this$3:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;->this$2:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    new-instance v3, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2$1;->this$3:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;->this$2:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/HereMapsManager;->bus:Lcom/squareup/otto/Bus;

    invoke-direct {v3, v4}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;-><init>(Lcom/squareup/otto/Bus;)V

    # setter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->hereLocationFixManager:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;
    invoke-static {v2, v3}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$1302(Lcom/navdy/hud/app/maps/here/HereMapsManager;Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    .line 339
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MAP-ENGINE-INIT-4 took ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2$1;->val$t3:J

    sub-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 342
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2$1;->this$3:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;->this$2:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    new-instance v3, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2$1;->this$3:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;->this$2:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/HereMapsManager;->bus:Lcom/squareup/otto/Bus;

    invoke-direct {v3, v4}, Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;-><init>(Lcom/squareup/otto/Bus;)V

    # setter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->routeCalcEventHandler:Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;
    invoke-static {v2, v3}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$1402(Lcom/navdy/hud/app/maps/here/HereMapsManager;Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;)Lcom/navdy/hud/app/maps/notification/RouteCalculationEventHandler;

    .line 345
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2$1;->this$3:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;->this$2:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # invokes: Lcom/navdy/hud/app/maps/here/HereMapsManager;->initMapLoader()V
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$1500(Lcom/navdy/hud/app/maps/here/HereMapsManager;)V

    .line 347
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2$1;->this$3:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;->this$2:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    new-instance v3, Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2$1;->this$3:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;->this$2:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getOfflineMapsData()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;-><init>(Ljava/lang/String;)V

    # setter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->offlineMapsVersion:Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;
    invoke-static {v2, v3}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$1602(Lcom/navdy/hud/app/maps/here/HereMapsManager;Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;)Lcom/navdy/hud/app/maps/here/HereOfflineMapsVersion;

    .line 350
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2$1;->this$3:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;->this$2:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->initLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$1700(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 351
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2$1;->this$3:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;->this$2:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    const/4 v4, 0x0

    # setter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapInitializing:Z
    invoke-static {v2, v4}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$1802(Lcom/navdy/hud/app/maps/here/HereMapsManager;Z)Z

    .line 352
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2$1;->this$3:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;->this$2:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    const/4 v4, 0x1

    # setter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapInitialized:Z
    invoke-static {v2, v4}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$1902(Lcom/navdy/hud/app/maps/here/HereMapsManager;Z)Z

    .line 353
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2$1;->this$3:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;->this$2:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v4, Lcom/navdy/hud/app/maps/MapEvents$MapEngineInitialize;

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2$1;->this$3:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;->this$2:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapInitialized:Z
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$1900(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Z

    move-result v5

    invoke-direct {v4, v5}, Lcom/navdy/hud/app/maps/MapEvents$MapEngineInitialize;-><init>(Z)V

    invoke-virtual {v2, v4}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 354
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v4, "MAP-ENGINE-INIT event sent"

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 355
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 358
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "setEngineOnlineState initial"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 359
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2$1;->this$3:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;->this$2:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # invokes: Lcom/navdy/hud/app/maps/here/HereMapsManager;->setEngineOnlineState()V
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$2000(Lcom/navdy/hud/app/maps/here/HereMapsManager;)V

    .line 361
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2$1;->this$3:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;->this$2:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager;->powerManager:Lcom/navdy/hud/app/device/PowerManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/device/PowerManager;->inQuietMode()Z

    move-result v2

    if-nez v2, :cond_1

    .line 362
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2$1;->this$3:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;->this$2:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # invokes: Lcom/navdy/hud/app/maps/here/HereMapsManager;->startTrafficUpdater()V
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$2100(Lcom/navdy/hud/app/maps/here/HereMapsManager;)V

    .line 364
    :cond_1
    return-void

    .line 323
    .end local v0    # "animationMode":Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;
    :cond_2
    invoke-static {}, Lcom/navdy/hud/app/maps/MapSettings;->isFullCustomAnimatonEnabled()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 324
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;->ZOOM_TILT_ANIMATION:Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;

    .restart local v0    # "animationMode":Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;
    goto/16 :goto_0

    .line 326
    .end local v0    # "animationMode":Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;
    :cond_3
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;->NONE:Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;

    .restart local v0    # "animationMode":Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;
    goto/16 :goto_0

    .line 355
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method
