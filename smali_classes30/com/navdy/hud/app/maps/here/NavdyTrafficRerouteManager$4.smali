.class Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;
.super Ljava/lang/Object;
.source "NavdyTrafficRerouteManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 18

    .prologue
    .line 83
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$500(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Landroid/os/Handler;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->killRouteCalc:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$400(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 85
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 86
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # invokes: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->cleanupRouteIfStale()V
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$600(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)V

    .line 87
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "skip traffic check, no n/w"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 355
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->running:Z
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$800(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 356
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$500(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Landroid/os/Handler;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->fasterRouteTrigger:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$900(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Ljava/lang/Runnable;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$700(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getRerouteInterval()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-long v8, v5

    invoke-virtual {v2, v3, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 359
    :cond_0
    :goto_0
    return-void

    .line 91
    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$700(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isNavigationModeOn()Z

    move-result v2

    if-nez v2, :cond_2

    .line 92
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$000(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->dismissReroute()V

    .line 93
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "skip traffic check, not navigating"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 355
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->running:Z
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$800(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 356
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$500(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Landroid/os/Handler;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->fasterRouteTrigger:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$900(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Ljava/lang/Runnable;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$700(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getRerouteInterval()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-long v8, v5

    invoke-virtual {v2, v3, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 98
    :cond_2
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$700(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isRerouting()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 99
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "skip traffic check, rerouting"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 100
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$000(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->dismissReroute()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 355
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->running:Z
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$800(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 356
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$500(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Landroid/os/Handler;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->fasterRouteTrigger:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$900(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Ljava/lang/Runnable;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$700(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getRerouteInterval()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-long v8, v5

    invoke-virtual {v2, v3, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 104
    :cond_3
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$700(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hasArrived()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 105
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "skip traffic check, arrival mode on"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 106
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$000(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->dismissReroute()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 355
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->running:Z
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$800(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 356
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$500(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Landroid/os/Handler;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->fasterRouteTrigger:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$900(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Ljava/lang/Runnable;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$700(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getRerouteInterval()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-long v8, v5

    invoke-virtual {v2, v3, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 110
    :cond_4
    :try_start_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$700(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isOnGasRoute()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 111
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "skip traffic check, on gas route"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 112
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$000(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->dismissReroute()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 355
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->running:Z
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$800(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 356
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$500(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Landroid/os/Handler;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->fasterRouteTrigger:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$900(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Ljava/lang/Runnable;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$700(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getRerouteInterval()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-long v8, v5

    invoke-virtual {v2, v3, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 116
    :cond_5
    :try_start_5
    invoke-static {}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->getInstance()Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->isLimitBandwidthModeOn()Z

    move-result v2

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$700(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hasTrafficRerouteOnce()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 117
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "user has already selected faster route, no-op in limited b/w condition"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 355
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->running:Z
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$800(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 356
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$500(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Landroid/os/Handler;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->fasterRouteTrigger:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$900(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Ljava/lang/Runnable;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$700(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getRerouteInterval()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-long v8, v5

    invoke-virtual {v2, v3, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 122
    :cond_6
    :try_start_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$700(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentRoute()Lcom/here/android/mpa/routing/Route;

    move-result-object v14

    .line 123
    .local v14, "currentRoute":Lcom/here/android/mpa/routing/Route;
    if-nez v14, :cond_7

    .line 124
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$000(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->dismissReroute()V

    .line 125
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "skip traffic check, no current route"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 355
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->running:Z
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$800(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 356
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$500(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Landroid/os/Handler;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->fasterRouteTrigger:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$900(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Ljava/lang/Runnable;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$700(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getRerouteInterval()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-long v8, v5

    invoke-virtual {v2, v3, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 129
    :cond_7
    :try_start_7
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v16

    .line 130
    .local v16, "hereMapsManager":Lcom/navdy/hud/app/maps/here/HereMapsManager;
    invoke-virtual/range {v16 .. v16}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v15

    .line 131
    .local v15, "hereLocationFixManager":Lcom/navdy/hud/app/maps/here/HereLocationFixManager;
    invoke-virtual {v15}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v4

    .line 132
    .local v4, "startPoint":Lcom/here/android/mpa/common/GeoCoordinate;
    if-nez v4, :cond_8

    .line 133
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # invokes: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->cleanupRouteIfStale()V
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$600(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)V

    .line 134
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "skip traffic check, no start point"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 355
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->running:Z
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$800(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 356
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$500(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Landroid/os/Handler;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->fasterRouteTrigger:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$900(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Ljava/lang/Runnable;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$700(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getRerouteInterval()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-long v8, v5

    invoke-virtual {v2, v3, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 138
    :cond_8
    :try_start_8
    invoke-virtual {v14}, Lcom/here/android/mpa/routing/Route;->getDestination()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v6

    .line 139
    .local v6, "endPoint":Lcom/here/android/mpa/common/GeoCoordinate;
    if-nez v6, :cond_9

    .line 140
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # invokes: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->cleanupRouteIfStale()V
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$600(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)V

    .line 141
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "skip traffic check, no end point"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 355
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->running:Z
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$800(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 356
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$500(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Landroid/os/Handler;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->fasterRouteTrigger:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$900(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Ljava/lang/Runnable;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$700(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getRerouteInterval()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-long v8, v5

    invoke-virtual {v2, v3, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 145
    :cond_9
    :try_start_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$000(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->shouldCalculateFasterRoute()Z

    move-result v2

    if-nez v2, :cond_a

    .line 146
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "skip traffic check, time not right"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 355
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->running:Z
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$800(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 356
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$500(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Landroid/os/Handler;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->fasterRouteTrigger:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$900(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Ljava/lang/Runnable;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$700(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getRerouteInterval()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-long v8, v5

    invoke-virtual {v2, v3, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 150
    :cond_a
    :try_start_a
    invoke-virtual/range {v16 .. v16}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getRouteOptions()Lcom/here/android/mpa/routing/RouteOptions;

    move-result-object v10

    .line 151
    .local v10, "routeOptions":Lcom/here/android/mpa/routing/RouteOptions;
    sget-object v2, Lcom/here/android/mpa/routing/RouteOptions$Type;->FASTEST:Lcom/here/android/mpa/routing/RouteOptions$Type;

    invoke-virtual {v10, v2}, Lcom/here/android/mpa/routing/RouteOptions;->setRouteType(Lcom/here/android/mpa/routing/RouteOptions$Type;)Lcom/here/android/mpa/routing/RouteOptions;

    .line 152
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->routeCalculator:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$200(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->cancel()V

    .line 153
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$500(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Landroid/os/Handler;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->killRouteCalc:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$400(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Ljava/lang/Runnable;

    move-result-object v3

    const-wide/32 v8, 0xafc8

    invoke-virtual {v2, v3, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 154
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->routeCalculator:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$200(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x1

    new-instance v8, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;

    move-object/from16 v0, p0

    invoke-direct {v8, v0}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;-><init>(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;)V

    const/4 v9, 0x1

    const/4 v11, 0x1

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual/range {v2 .. v13}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->calculateRoute(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/common/GeoCoordinate;Ljava/util/List;Lcom/here/android/mpa/common/GeoCoordinate;ZLcom/navdy/hud/app/maps/here/HereRouteCalculator$RouteCalculatorListener;ILcom/here/android/mpa/routing/RouteOptions;ZZZ)V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 355
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->running:Z
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$800(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 356
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$500(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Landroid/os/Handler;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->fasterRouteTrigger:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$900(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Ljava/lang/Runnable;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$700(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getRerouteInterval()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-long v8, v5

    invoke-virtual {v2, v3, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 351
    .end local v4    # "startPoint":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v6    # "endPoint":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v10    # "routeOptions":Lcom/here/android/mpa/routing/RouteOptions;
    .end local v14    # "currentRoute":Lcom/here/android/mpa/routing/Route;
    .end local v15    # "hereLocationFixManager":Lcom/navdy/hud/app/maps/here/HereLocationFixManager;
    .end local v16    # "hereMapsManager":Lcom/navdy/hud/app/maps/here/HereMapsManager;
    :catch_0
    move-exception v17

    .line 352
    .local v17, "t":Ljava/lang/Throwable;
    :try_start_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # invokes: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->cleanupRouteIfStale()V
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$600(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)V

    .line 353
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 355
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->running:Z
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$800(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 356
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$500(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Landroid/os/Handler;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->fasterRouteTrigger:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$900(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Ljava/lang/Runnable;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$700(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getRerouteInterval()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-long v8, v5

    invoke-virtual {v2, v3, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 355
    .end local v17    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->running:Z
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$800(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 356
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->handler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$500(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Landroid/os/Handler;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->fasterRouteTrigger:Ljava/lang/Runnable;
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$900(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Ljava/lang/Runnable;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v7}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$700(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getRerouteInterval()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    int-to-long v8, v7

    invoke-virtual {v3, v5, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_b
    throw v2
.end method
