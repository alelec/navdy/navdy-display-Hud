.class final Lcom/navdy/hud/app/maps/here/HerePlacesManager$2;
.super Ljava/lang/Object;
.source "HerePlacesManager.java"

# interfaces
.implements Lcom/here/android/mpa/search/ResultListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HerePlacesManager;->geoCodeStreetAddress(Lcom/here/android/mpa/common/GeoCoordinate;Ljava/lang/String;ILcom/navdy/hud/app/maps/here/HerePlacesManager$GeoCodeCallback;)Lcom/here/android/mpa/search/GeocodeRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/here/android/mpa/search/ResultListener",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/here/android/mpa/search/Location;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic val$callBack:Lcom/navdy/hud/app/maps/here/HerePlacesManager$GeoCodeCallback;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HerePlacesManager$GeoCodeCallback;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$2;->val$callBack:Lcom/navdy/hud/app/maps/here/HerePlacesManager$GeoCodeCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onCompleted(Ljava/lang/Object;Lcom/here/android/mpa/search/ErrorCode;)V
    .locals 0

    .prologue
    .line 135
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1, p2}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$2;->onCompleted(Ljava/util/List;Lcom/here/android/mpa/search/ErrorCode;)V

    return-void
.end method

.method public onCompleted(Ljava/util/List;Lcom/here/android/mpa/search/ErrorCode;)V
    .locals 2
    .param p2, "errorCode"    # Lcom/here/android/mpa/search/ErrorCode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/search/Location;",
            ">;",
            "Lcom/here/android/mpa/search/ErrorCode;",
            ")V"
        }
    .end annotation

    .prologue
    .line 139
    .local p1, "locations":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/search/Location;>;"
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$2;->val$callBack:Lcom/navdy/hud/app/maps/here/HerePlacesManager$GeoCodeCallback;

    invoke-interface {v1, p2, p1}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$GeoCodeCallback;->result(Lcom/here/android/mpa/search/ErrorCode;Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 143
    :goto_0
    return-void

    .line 140
    :catch_0
    move-exception v0

    .line 141
    .local v0, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
