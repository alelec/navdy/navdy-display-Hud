.class Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;
.super Ljava/lang/Object;
.source "HereMapsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;

.field final synthetic val$t2:J


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;J)V
    .locals 0
    .param p1, "this$2"    # Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;

    .prologue
    .line 290
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;->this$2:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;

    iput-wide p2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;->val$t2:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 295
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;->this$2:Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->positioningManager:Lcom/here/android/mpa/common/PositioningManager;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$900(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Lcom/here/android/mpa/common/PositioningManager;

    move-result-object v2

    sget-object v3, Lcom/here/android/mpa/common/PositioningManager$LocationMethod;->GPS_NETWORK:Lcom/here/android/mpa/common/PositioningManager$LocationMethod;

    invoke-virtual {v2, v3}, Lcom/here/android/mpa/common/PositioningManager;->start(Lcom/here/android/mpa/common/PositioningManager$LocationMethod;)Z

    .line 296
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "position manager started"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 298
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MAP-ENGINE-INIT-3 took ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;->val$t2:J

    sub-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 299
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 301
    .local v0, "t3":J
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v2

    new-instance v3, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2$1;

    invoke-direct {v3, p0, v0, v1}, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2$1;-><init>(Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1$2;J)V

    const/4 v4, 0x3

    invoke-virtual {v2, v3, v4}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 366
    return-void
.end method
