.class Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$2;
.super Ljava/lang/Object;
.source "HereLaneInfoListener.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$2;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$2;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->navController:Lcom/navdy/hud/app/maps/here/HereNavController;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$000(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Lcom/navdy/hud/app/maps/here/HereNavController;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$2;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereNavController;->removeLaneInfoListener(Lcom/here/android/mpa/guidance/NavigationManager$LaneInformationListener;)V

    .line 57
    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "removed lane info listener"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 58
    return-void
.end method
