.class Lcom/navdy/hud/app/maps/here/HereRerouteListener$2;
.super Ljava/lang/Object;
.source "HereRerouteListener.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereRerouteListener;->onRerouteEnd(Lcom/here/android/mpa/routing/Route;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereRerouteListener;

.field final synthetic val$route:Lcom/here/android/mpa/routing/Route;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereRerouteListener;Lcom/here/android/mpa/routing/Route;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereRerouteListener;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener$2;->this$0:Lcom/navdy/hud/app/maps/here/HereRerouteListener;

    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener$2;->val$route:Lcom/here/android/mpa/routing/Route;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 74
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener$2;->this$0:Lcom/navdy/hud/app/maps/here/HereRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRerouteListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->access$100(Lcom/navdy/hud/app/maps/here/HereRerouteListener;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentRouteId()Ljava/lang/String;

    move-result-object v7

    .line 75
    .local v7, "currentRouteId":Ljava/lang/String;
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener$2;->this$0:Lcom/navdy/hud/app/maps/here/HereRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRerouteListener;->routeCalcId:Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->access$000(Lcom/navdy/hud/app/maps/here/HereRerouteListener;)Ljava/lang/String;

    move-result-object v8

    .line 76
    .local v8, "id":Ljava/lang/String;
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener$2;->this$0:Lcom/navdy/hud/app/maps/here/HereRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->access$300(Lcom/navdy/hud/app/maps/here/HereRerouteListener;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener$2;->this$0:Lcom/navdy/hud/app/maps/here/HereRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRerouteListener;->tag:Ljava/lang/String;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->access$200(Lcom/navdy/hud/app/maps/here/HereRerouteListener;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " reroute-end: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener$2;->val$route:Lcom/here/android/mpa/routing/Route;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener$2;->this$0:Lcom/navdy/hud/app/maps/here/HereRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRerouteListener;->routeCalcId:Ljava/lang/String;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->access$000(Lcom/navdy/hud/app/maps/here/HereRerouteListener;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " current="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener$2;->this$0:Lcom/navdy/hud/app/maps/here/HereRerouteListener;

    # setter for: Lcom/navdy/hud/app/maps/here/HereRerouteListener;->routeCalcId:Ljava/lang/String;
    invoke-static {v0, v3}, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->access$002(Lcom/navdy/hud/app/maps/here/HereRerouteListener;Ljava/lang/String;)Ljava/lang/String;

    .line 78
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener$2;->this$0:Lcom/navdy/hud/app/maps/here/HereRerouteListener;

    # setter for: Lcom/navdy/hud/app/maps/here/HereRerouteListener;->routeCalculationOn:Z
    invoke-static {v0, v5}, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->access$402(Lcom/navdy/hud/app/maps/here/HereRerouteListener;Z)Z

    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener$2;->this$0:Lcom/navdy/hud/app/maps/here/HereRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRerouteListener;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->access$500(Lcom/navdy/hud/app/maps/here/HereRerouteListener;)Lcom/squareup/otto/Bus;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->REROUTE_FINISHED:Lcom/navdy/hud/app/maps/MapEvents$RerouteEvent;

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 80
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener$2;->val$route:Lcom/here/android/mpa/routing/Route;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener$2;->this$0:Lcom/navdy/hud/app/maps/here/HereRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRerouteListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->access$100(Lcom/navdy/hud/app/maps/here/HereRerouteListener;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isNavigationModeOn()Z

    move-result v0

    if-nez v0, :cond_1

    .line 82
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener$2;->this$0:Lcom/navdy/hud/app/maps/here/HereRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->access$300(Lcom/navdy/hud/app/maps/here/HereRerouteListener;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener$2;->this$0:Lcom/navdy/hud/app/maps/here/HereRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRerouteListener;->tag:Ljava/lang/String;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->access$200(Lcom/navdy/hud/app/maps/here/HereRerouteListener;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "reroute-end: navigation already ended, not adding route"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 93
    :cond_0
    :goto_0
    return-void

    .line 85
    :cond_1
    invoke-static {v8, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 86
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener$2;->this$0:Lcom/navdy/hud/app/maps/here/HereRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->access$300(Lcom/navdy/hud/app/maps/here/HereRerouteListener;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "route change before recalc completed: cannot use"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 89
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener$2;->this$0:Lcom/navdy/hud/app/maps/here/HereRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRerouteListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->access$100(Lcom/navdy/hud/app/maps/here/HereRerouteListener;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isTrafficConsidered(Ljava/lang/String;)Z

    move-result v6

    .line 90
    .local v6, "trafficConsidered":Z
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener$2;->this$0:Lcom/navdy/hud/app/maps/here/HereRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRerouteListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->access$100(Lcom/navdy/hud/app/maps/here/HereRerouteListener;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener$2;->val$route:Lcom/here/android/mpa/routing/Route;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;->NAV_SESSION_OFF_REROUTE:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    move-object v4, v3

    invoke-virtual/range {v0 .. v6}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->setReroute(Lcom/here/android/mpa/routing/Route;Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 91
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener$2;->this$0:Lcom/navdy/hud/app/maps/here/HereRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRerouteListener;->navigationQualityTracker:Lcom/navdy/hud/app/analytics/NavigationQualityTracker;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->access$600(Lcom/navdy/hud/app/maps/here/HereRerouteListener;)Lcom/navdy/hud/app/analytics/NavigationQualityTracker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->trackTripRecalculated()V

    goto :goto_0
.end method
