.class Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;
.super Lcom/here/android/mpa/guidance/NavigationManager$NewInstructionEventListener;
.source "HereNewManeuverListener.java"


# instance fields
.field private final bus:Lcom/squareup/otto/Bus;

.field private hasNewRoute:Z

.field private final hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

.field private final logger:Lcom/navdy/service/library/log/Logger;

.field private final navController:Lcom/navdy/hud/app/maps/here/HereNavController;

.field private navManeuver:Lcom/here/android/mpa/routing/Maneuver;

.field private final tag:Ljava/lang/String;

.field private final verbose:Z


# direct methods
.method constructor <init>(Lcom/navdy/service/library/log/Logger;Ljava/lang/String;ZLcom/navdy/hud/app/maps/here/HereNavController;Lcom/navdy/hud/app/maps/here/HereNavigationManager;Lcom/squareup/otto/Bus;)V
    .locals 0
    .param p1, "logger"    # Lcom/navdy/service/library/log/Logger;
    .param p2, "tag"    # Ljava/lang/String;
    .param p3, "verbose"    # Z
    .param p4, "navController"    # Lcom/navdy/hud/app/maps/here/HereNavController;
    .param p5, "hereNavigationManager"    # Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    .param p6, "bus"    # Lcom/squareup/otto/Bus;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/here/android/mpa/guidance/NavigationManager$NewInstructionEventListener;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->logger:Lcom/navdy/service/library/log/Logger;

    .line 38
    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->tag:Ljava/lang/String;

    .line 39
    iput-boolean p3, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->verbose:Z

    .line 40
    iput-object p4, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->navController:Lcom/navdy/hud/app/maps/here/HereNavController;

    .line 41
    iput-object p5, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    .line 42
    iput-object p6, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->bus:Lcom/squareup/otto/Bus;

    .line 43
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;)Lcom/navdy/hud/app/maps/here/HereNavController;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->navController:Lcom/navdy/hud/app/maps/here/HereNavController;

    return-object v0
.end method

.method static synthetic access$102(Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;Lcom/here/android/mpa/routing/Maneuver;)Lcom/here/android/mpa/routing/Maneuver;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;
    .param p1, "x1"    # Lcom/here/android/mpa/routing/Maneuver;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->navManeuver:Lcom/here/android/mpa/routing/Maneuver;

    return-object p1
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->tag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->verbose:Z

    return v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->hasNewRoute:Z

    return v0
.end method

.method static synthetic access$502(Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->hasNewRoute:Z

    return p1
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    return-object v0
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;)Lcom/squareup/otto/Bus;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->bus:Lcom/squareup/otto/Bus;

    return-object v0
.end method


# virtual methods
.method clearNavManeuver()V
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->navManeuver:Lcom/here/android/mpa/routing/Maneuver;

    .line 111
    return-void
.end method

.method getNavManeuver()Lcom/here/android/mpa/routing/Maneuver;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->navManeuver:Lcom/here/android/mpa/routing/Maneuver;

    return-object v0
.end method

.method public onNewInstructionEvent()V
    .locals 3

    .prologue
    .line 47
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener$1;-><init>(Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;)V

    const/16 v2, 0x14

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 94
    return-void
.end method

.method setNewRoute()V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "setNewRoute"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 98
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->hasNewRoute:Z

    .line 99
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->logger:Lcom/navdy/service/library/log/Logger;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "setNewRoute: send empty maneuver"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->bus:Lcom/squareup/otto/Bus;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->EMPTY_MANEUVER_DISPLAY:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 103
    return-void
.end method
