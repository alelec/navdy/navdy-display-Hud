.class public Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
.super Ljava/lang/Object;
.source "HereRouteCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/here/HereRouteCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RouteInfo"
.end annotation


# instance fields
.field public route:Lcom/here/android/mpa/routing/Route;

.field public routeRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

.field public routeResult:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

.field public routeStartPoint:Lcom/here/android/mpa/common/GeoCoordinate;

.field public traffic:Z


# direct methods
.method public constructor <init>(Lcom/here/android/mpa/routing/Route;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/navdy/service/library/events/navigation/NavigationRouteResult;ZLcom/here/android/mpa/common/GeoCoordinate;)V
    .locals 0
    .param p1, "route"    # Lcom/here/android/mpa/routing/Route;
    .param p2, "routeRequest"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .param p3, "routeResult"    # Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    .param p4, "traffic"    # Z
    .param p5, "routeStartPoint"    # Lcom/here/android/mpa/common/GeoCoordinate;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->route:Lcom/here/android/mpa/routing/Route;

    .line 25
    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->routeRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .line 26
    iput-object p3, p0, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->routeResult:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    .line 27
    iput-boolean p4, p0, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->traffic:Z

    .line 28
    iput-object p5, p0, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->routeStartPoint:Lcom/here/android/mpa/common/GeoCoordinate;

    .line 29
    return-void
.end method
