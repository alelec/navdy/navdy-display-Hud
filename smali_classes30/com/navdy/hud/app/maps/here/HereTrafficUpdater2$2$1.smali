.class Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$2$1;
.super Ljava/lang/Object;
.source "HereTrafficUpdater2.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$2;->onComplete(Ljava/util/List;Lcom/here/android/mpa/guidance/TrafficUpdater$Error;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$2;

.field final synthetic val$list:Ljava/util/List;

.field final synthetic val$r:Lcom/here/android/mpa/routing/Route;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$2;Lcom/here/android/mpa/routing/Route;Ljava/util/List;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$2;

    .prologue
    .line 188
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$2$1;->this$1:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$2;

    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$2$1;->val$r:Lcom/here/android/mpa/routing/Route;

    iput-object p3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$2$1;->val$list:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 191
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$2$1;->this$1:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$2;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$2;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->route:Lcom/here/android/mpa/routing/Route;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$100(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;)Lcom/here/android/mpa/routing/Route;

    move-result-object v2

    if-nez v2, :cond_0

    .line 192
    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "no current route, cannot process traffic events"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 197
    :goto_0
    return-void

    .line 195
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getNavController()Lcom/navdy/hud/app/maps/here/HereNavController;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavController;->getDestinationDistance()J

    move-result-wide v0

    .line 196
    .local v0, "distanceRemaining":J
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$2$1;->this$1:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$2;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$2;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$2$1;->val$r:Lcom/here/android/mpa/routing/Route;

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$2$1;->val$list:Ljava/util/List;

    # invokes: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->processEvents(Lcom/here/android/mpa/routing/Route;Ljava/util/List;J)V
    invoke-static {v2, v3, v4, v0, v1}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$900(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;Lcom/here/android/mpa/routing/Route;Ljava/util/List;J)V

    goto :goto_0
.end method
