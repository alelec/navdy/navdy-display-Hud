.class Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$2;
.super Ljava/lang/Object;
.source "HereMapImageGenerator.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->saveFileToDisk(Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$MapGeneratorParams;Landroid/graphics/Bitmap;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;

.field final synthetic val$bitmap:Landroid/graphics/Bitmap;

.field final synthetic val$mapGeneratorParams:Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$MapGeneratorParams;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;Landroid/graphics/Bitmap;Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$MapGeneratorParams;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;

    .prologue
    .line 152
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$2;->this$0:Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;

    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$2;->val$bitmap:Landroid/graphics/Bitmap;

    iput-object p3, p0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$2;->val$mapGeneratorParams:Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$MapGeneratorParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 155
    const/4 v0, 0x0

    .line 157
    .local v0, "fout":Ljava/io/FileOutputStream;
    :try_start_0
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$2;->val$bitmap:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_1

    .line 158
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$2;->this$0:Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$2;->val$mapGeneratorParams:Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$MapGeneratorParams;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$MapGeneratorParams;->id:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->getMapImageFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 159
    .local v2, "path":Ljava/lang/String;
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 160
    .end local v0    # "fout":Ljava/io/FileOutputStream;
    .local v1, "fout":Ljava/io/FileOutputStream;
    :try_start_1
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$2;->val$bitmap:Landroid/graphics/Bitmap;

    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v6, 0x64

    invoke-virtual {v4, v5, v6, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 161
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->fileSync(Ljava/io/FileOutputStream;)V

    .line 162
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 163
    const/4 v0, 0x0

    .line 164
    .end local v1    # "fout":Ljava/io/FileOutputStream;
    .restart local v0    # "fout":Ljava/io/FileOutputStream;
    :try_start_2
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "bitmap saved:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 168
    .end local v2    # "path":Ljava/lang/String;
    :goto_0
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$2;->val$mapGeneratorParams:Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$MapGeneratorParams;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$MapGeneratorParams;->callback:Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$ICallback;

    if-eqz v4, :cond_0

    .line 169
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$2;->this$0:Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->handler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->access$300(Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;)Landroid/os/Handler;

    move-result-object v4

    new-instance v5, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$2$1;

    invoke-direct {v5, p0}, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$2$1;-><init>(Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$2;)V

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 179
    :cond_0
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->fileSync(Ljava/io/FileOutputStream;)V

    .line 180
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 182
    :goto_1
    return-void

    .line 166
    :cond_1
    :try_start_3
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "bitmap not saved:"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 176
    :catch_0
    move-exception v3

    .line 177
    .local v3, "t":Ljava/lang/Throwable;
    :goto_2
    :try_start_4
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 179
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->fileSync(Ljava/io/FileOutputStream;)V

    .line 180
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_1

    .line 179
    .end local v3    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v4

    :goto_3
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->fileSync(Ljava/io/FileOutputStream;)V

    .line 180
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v4

    .line 179
    .end local v0    # "fout":Ljava/io/FileOutputStream;
    .restart local v1    # "fout":Ljava/io/FileOutputStream;
    .restart local v2    # "path":Ljava/lang/String;
    :catchall_1
    move-exception v4

    move-object v0, v1

    .end local v1    # "fout":Ljava/io/FileOutputStream;
    .restart local v0    # "fout":Ljava/io/FileOutputStream;
    goto :goto_3

    .line 176
    .end local v0    # "fout":Ljava/io/FileOutputStream;
    .restart local v1    # "fout":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v3

    move-object v0, v1

    .end local v1    # "fout":Ljava/io/FileOutputStream;
    .restart local v0    # "fout":Ljava/io/FileOutputStream;
    goto :goto_2
.end method
