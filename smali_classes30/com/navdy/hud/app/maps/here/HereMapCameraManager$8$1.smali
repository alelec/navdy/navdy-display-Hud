.class Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8$1;
.super Ljava/lang/Object;
.source "HereMapCameraManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;

    .prologue
    .line 645
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 650
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$1300(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Lcom/here/android/mpa/common/GeoPosition;

    move-result-object v0

    if-nez v0, :cond_2

    .line 651
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$300(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapController;->getCenter()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v2

    .line 656
    .local v2, "coordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    :goto_0
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$300(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapController;->getZoomLevel()D

    move-result-wide v0

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;

    iget-wide v8, v3, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;->val$zoomStep:D

    add-double v4, v0, v8

    .line 657
    .local v4, "newZoom":D
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$300(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapController;->getTilt()F

    move-result v0

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;

    iget v1, v1, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;->val$tiltStep:F

    add-float v7, v0, v1

    .line 659
    .local v7, "newTilt":F
    const-wide v0, 0x4030800000000000L    # 16.5

    cmpl-double v0, v4, v0

    if-lez v0, :cond_0

    .line 660
    const-wide v4, 0x4030800000000000L    # 16.5

    .line 663
    :cond_0
    const/high16 v0, 0x428c0000    # 70.0f

    cmpl-float v0, v7, v0

    if-lez v0, :cond_1

    .line 664
    const/high16 v7, 0x428c0000    # 70.0f

    .line 667
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$300(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v1

    sget-object v3, Lcom/here/android/mpa/mapping/Map$Animation;->NONE:Lcom/here/android/mpa/mapping/Map$Animation;

    const/high16 v6, -0x40800000    # -1.0f

    invoke-virtual/range {v1 .. v7}, Lcom/navdy/hud/app/maps/here/HereMapController;->setCenter(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;DFF)V

    .line 668
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # operator++ for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->nTimesAnimated:I
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$2208(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)I

    .line 670
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->nTimesAnimated:I
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$2200(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 671
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # setter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->animationOn:Z
    invoke-static {v0, v10}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$1402(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;Z)Z

    .line 672
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # setter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->nTimesAnimated:I
    invoke-static {v0, v10}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$2202(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;I)I

    .line 676
    :goto_1
    return-void

    .line 653
    .end local v2    # "coordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v4    # "newZoom":D
    .end local v7    # "newTilt":F
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$1300(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Lcom/here/android/mpa/common/GeoPosition;

    move-result-object v0

    invoke-virtual {v0}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v2

    .restart local v2    # "coordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    goto :goto_0

    .line 674
    .restart local v4    # "newZoom":D
    .restart local v7    # "newTilt":F
    :cond_3
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$100(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;

    iget-object v1, v1, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;

    iget-wide v8, v3, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;->val$zoomStep:D

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;

    iget v3, v3, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;->val$tiltStep:F

    # invokes: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->getAnimateCameraRunnable(DF)Ljava/lang/Runnable;
    invoke-static {v1, v8, v9, v3}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$2300(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;DF)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v8, 0xc8

    invoke-virtual {v0, v1, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1
.end method
