.class final Lcom/navdy/hud/app/maps/here/HereRouteManager$2;
.super Ljava/lang/Object;
.source "HereRouteManager.java"

# interfaces
.implements Lcom/navdy/hud/app/maps/here/HerePlacesManager$GeoCodeCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereRouteManager;->handleRouteRequest(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/common/GeoCoordinate;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$factoringInTraffic:Z

.field final synthetic val$request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

.field final synthetic val$startPoint:Lcom/here/android/mpa/common/GeoCoordinate;

.field final synthetic val$waypoints:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/common/GeoCoordinate;Ljava/util/List;Z)V
    .locals 0

    .prologue
    .line 305
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$2;->val$request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$2;->val$startPoint:Lcom/here/android/mpa/common/GeoCoordinate;

    iput-object p3, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$2;->val$waypoints:Ljava/util/List;

    iput-boolean p4, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$2;->val$factoringInTraffic:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public result(Lcom/here/android/mpa/search/ErrorCode;Ljava/util/List;)V
    .locals 12
    .param p1, "errorCode"    # Lcom/here/android/mpa/search/ErrorCode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/here/android/mpa/search/ErrorCode;",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/search/Location;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "locationList":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/search/Location;>;"
    const v11, 0x7f090111

    .line 311
    :try_start_0
    sget-object v1, Lcom/here/android/mpa/search/ErrorCode;->CANCELLED:Lcom/here/android/mpa/search/ErrorCode;

    if-ne p1, v1, :cond_0

    .line 312
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "geocode request has been cancelled ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$2;->val$request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v3, v3, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 345
    :goto_0
    return-void

    .line 316
    :cond_0
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->lockObj:Ljava/lang/Object;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$400()Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 317
    :try_start_1
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$2;->val$request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v1, v1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestId:Ljava/lang/String;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeGeoCalcId:Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$800()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    .line 318
    .local v8, "matchesCurrentRequestId":Z
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 319
    if-nez v8, :cond_1

    .line 320
    :try_start_2
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "geocode request is not valid active:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeGeoCalcId:Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$800()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " request:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$2;->val$request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v3, v3, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 321
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

    sget-object v1, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_CANCELLED:Lcom/navdy/service/library/events/RequestStatus;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$2;->val$request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v3, v3, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$2;->val$request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v7, v7, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestId:Ljava/lang/String;

    invoke-direct/range {v0 .. v7}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;-><init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 322
    .local v0, "response":Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;
    invoke-static {}, Lcom/navdy/hud/app/maps/MapsEventHandler;->getInstance()Lcom/navdy/hud/app/maps/MapsEventHandler;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/maps/MapsEventHandler;->sendEventToClient(Lcom/squareup/wire/Message;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 341
    .end local v0    # "response":Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;
    .end local v8    # "matchesCurrentRequestId":Z
    :catch_0
    move-exception v10

    .line 342
    .local v10, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "Street Address placesSearch exception"

    invoke-virtual {v1, v2, v10}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 343
    sget-object v1, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_UNKNOWN_ERROR:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$2;->val$request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->context:Landroid/content/Context;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$200()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/navdy/hud/app/maps/here/HereRouteManager;->returnErrorResponse(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;)V
    invoke-static {v1, v2, v3}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$300(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;)V

    goto :goto_0

    .line 318
    .end local v10    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1

    .line 324
    .restart local v8    # "matchesCurrentRequestId":Z
    :cond_1
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->lockObj:Ljava/lang/Object;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$400()Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0

    .line 325
    const/4 v1, 0x0

    :try_start_5
    # setter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteGeocodeRequest:Lcom/here/android/mpa/search/GeocodeRequest;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$902(Lcom/here/android/mpa/search/GeocodeRequest;)Lcom/here/android/mpa/search/GeocodeRequest;

    .line 326
    const/4 v1, 0x0

    # setter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeGeoCalcId:Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$802(Ljava/lang/String;)Ljava/lang/String;

    .line 327
    const/4 v1, 0x0

    # setter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeGeoRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$1002(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .line 328
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 329
    :try_start_6
    sget-object v1, Lcom/here/android/mpa/search/ErrorCode;->NONE:Lcom/here/android/mpa/search/ErrorCode;

    if-ne p1, v1, :cond_2

    if-eqz p2, :cond_2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 331
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Street Address results size="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 332
    const/4 v1, 0x0

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/here/android/mpa/search/Location;

    invoke-virtual {v1}, Lcom/here/android/mpa/search/Location;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v9

    .line 333
    .local v9, "newEndPoint":Lcom/here/android/mpa/common/GeoCoordinate;
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Street Address placesSearch success: new-end-geo:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 334
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$2;->val$request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$2;->val$startPoint:Lcom/here/android/mpa/common/GeoCoordinate;

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$2;->val$waypoints:Ljava/util/List;

    iget-boolean v4, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$2;->val$factoringInTraffic:Z

    # invokes: Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeSearch(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/common/GeoCoordinate;Ljava/util/List;Lcom/here/android/mpa/common/GeoCoordinate;Z)V
    invoke-static {v1, v2, v3, v9, v4}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$1100(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/common/GeoCoordinate;Ljava/util/List;Lcom/here/android/mpa/common/GeoCoordinate;Z)V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_0

    goto/16 :goto_0

    .line 328
    .end local v9    # "newEndPoint":Lcom/here/android/mpa/common/GeoCoordinate;
    :catchall_1
    move-exception v1

    :try_start_7
    monitor-exit v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v1

    .line 337
    :cond_2
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "could not geocode, Street Address placesSearch error:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/here/android/mpa/search/ErrorCode;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 338
    sget-object v1, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SERVICE_ERROR:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$2;->val$request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->context:Landroid/content/Context;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$200()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f090111

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/navdy/hud/app/maps/here/HereRouteManager;->returnErrorResponse(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;)V
    invoke-static {v1, v2, v3}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$300(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_0

    goto/16 :goto_0
.end method
