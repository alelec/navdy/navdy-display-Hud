.class Lcom/navdy/hud/app/maps/here/HereMapCameraManager$1;
.super Ljava/lang/Object;
.source "HereMapCameraManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/here/HereMapCameraManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 118
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$100(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->easeOutDynamicZoomTilt:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$000(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 119
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    const/4 v1, 0x0

    # setter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->zoomActionOn:Z
    invoke-static {v0, v1}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$202(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;Z)Z

    .line 120
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$300(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapController;->getState()Lcom/navdy/hud/app/maps/here/HereMapController$State;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapController$State;->ROUTE_PICKER:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    if-ne v0, v1, :cond_0

    .line 121
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "unlocking: in route search, no-op"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 145
    :goto_0
    return-void

    .line 124
    :cond_0
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$1$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$1$1;-><init>(Lcom/navdy/hud/app/maps/here/HereMapCameraManager$1;)V

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method
