.class public Lcom/navdy/hud/app/maps/here/HereMapUtil;
.super Ljava/lang/Object;
.source "HereMapUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/maps/here/HereMapUtil$SavedRouteData;,
        Lcom/navdy/hud/app/maps/here/HereMapUtil$IImageLoadCallback;,
        Lcom/navdy/hud/app/maps/here/HereMapUtil$LongContainer;,
        Lcom/navdy/hud/app/maps/here/HereMapUtil$SignPostInfo;
    }
.end annotation


# static fields
.field private static final ACTIVE_GAS_ROUTE_INFO_REQUEST:Ljava/lang/String; = "gasrouterequest.bin"

.field private static final ACTIVE_ROUTE_INFO_DEVICE_ID:Ljava/lang/String; = "routerequest.device"

.field private static final ACTIVE_ROUTE_INFO_REQUEST:Ljava/lang/String; = "routerequest.bin"

.field private static final EQUALITY_DISTANCE_VARIATION:I = 0x1f4

.field private static final INVALID_ETA_TIME_DIFF:J = 0x57e40L

.field public static final MILES_TO_METERS:D = 1609.34

.field public static final SHORT_DISTANCE_IN_METERS:I = 0x14

.field private static final STALE_ROUTE_TIME:J

.field public static final TBT_ISO3_LANG_CODE:Ljava/lang/String;

.field private static final VERBOSE:Z

.field private static final day:Ljava/lang/String;

.field private static final hr:Ljava/lang/String;

.field private static lastKnownDeviceId:Ljava/lang/String;

.field private static final min:Ljava/lang/String;

.field private static final min_short:Ljava/lang/String;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static final signPostBuilder:Ljava/lang/StringBuilder;

.field private static final signPostBuilder2:Ljava/lang/StringBuilder;

.field private static final signPostSet:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sp_20_b_1:Landroid/text/style/TextAppearanceSpan;

.field private static final sp_20_b_2:Landroid/text/style/TextAppearanceSpan;

.field private static final sp_24_1:Landroid/text/style/TextAppearanceSpan;

.field private static final sp_24_2:Landroid/text/style/TextAppearanceSpan;

.field private static final sp_26_1:Landroid/text/style/TextAppearanceSpan;

.field private static final sp_26_2:Landroid/text/style/TextAppearanceSpan;

.field private static final stringBuilder:Ljava/lang/StringBuilder;

.field private static final trafficEventBuilder:Ljava/lang/StringBuilder;

.field private static final wire:Lcom/squareup/wire/Wire;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const v8, 0x7f0c0024

    const v7, 0x7f0c0023

    const v6, 0x7f0c0022

    .line 66
    new-instance v2, Lcom/navdy/service/library/log/Logger;

    const-class v3, Lcom/navdy/hud/app/maps/here/HereMapUtil;

    invoke-direct {v2, v3}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 91
    sget-object v2, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x3

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    sput-wide v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->STALE_ROUTE_TIME:J

    .line 93
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sput-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->stringBuilder:Ljava/lang/StringBuilder;

    .line 94
    new-instance v2, Ljava/util/Locale;

    const-string v3, "en"

    const-string v4, "US"

    invoke-direct {v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->TBT_ISO3_LANG_CODE:Ljava/lang/String;

    .line 95
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sput-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->signPostBuilder:Ljava/lang/StringBuilder;

    .line 96
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sput-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->signPostBuilder2:Ljava/lang/StringBuilder;

    .line 97
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    sput-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->signPostSet:Ljava/util/HashSet;

    .line 99
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sput-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->trafficEventBuilder:Ljava/lang/StringBuilder;

    .line 121
    new-instance v2, Lcom/squareup/wire/Wire;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Lcom/navdy/service/library/events/Ext_NavdyEvent;

    aput-object v5, v3, v4

    invoke-direct {v2, v3}, Lcom/squareup/wire/Wire;-><init>([Ljava/lang/Class;)V

    sput-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->wire:Lcom/squareup/wire/Wire;

    .line 124
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 125
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 127
    .local v1, "resources":Landroid/content/res/Resources;
    new-instance v2, Landroid/text/style/TextAppearanceSpan;

    invoke-direct {v2, v0, v6}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    sput-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sp_20_b_1:Landroid/text/style/TextAppearanceSpan;

    .line 128
    new-instance v2, Landroid/text/style/TextAppearanceSpan;

    invoke-direct {v2, v0, v6}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    sput-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sp_20_b_2:Landroid/text/style/TextAppearanceSpan;

    .line 129
    new-instance v2, Landroid/text/style/TextAppearanceSpan;

    invoke-direct {v2, v0, v7}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    sput-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sp_24_1:Landroid/text/style/TextAppearanceSpan;

    .line 130
    new-instance v2, Landroid/text/style/TextAppearanceSpan;

    invoke-direct {v2, v0, v7}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    sput-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sp_24_2:Landroid/text/style/TextAppearanceSpan;

    .line 131
    new-instance v2, Landroid/text/style/TextAppearanceSpan;

    invoke-direct {v2, v0, v8}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    sput-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sp_26_1:Landroid/text/style/TextAppearanceSpan;

    .line 132
    new-instance v2, Landroid/text/style/TextAppearanceSpan;

    invoke-direct {v2, v0, v8}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    sput-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sp_26_2:Landroid/text/style/TextAppearanceSpan;

    .line 134
    const v2, 0x7f09024a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->min:Ljava/lang/String;

    .line 135
    const v2, 0x7f09024b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->min_short:Ljava/lang/String;

    .line 136
    const v2, 0x7f090249

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->hr:Ljava/lang/String;

    .line 137
    const v2, 0x7f090248

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->day:Ljava/lang/String;

    .line 138
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method public static areManeuverEqual(Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;)Z
    .locals 6
    .param p0, "maneuver"    # Lcom/here/android/mpa/routing/Maneuver;
    .param p1, "other"    # Lcom/here/android/mpa/routing/Maneuver;

    .prologue
    const/16 v5, 0x1f4

    .line 420
    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    .line 421
    :cond_0
    const/4 v1, 0x0

    .line 449
    :cond_1
    :goto_0
    return v1

    .line 424
    :cond_2
    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Maneuver;->getRoadElements()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p1}, Lcom/here/android/mpa/routing/Maneuver;->getRoadElements()Ljava/util/List;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 425
    .local v1, "ret":Z
    if-nez v1, :cond_1

    .line 430
    :try_start_0
    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Maneuver;->getTurn()Lcom/here/android/mpa/routing/Maneuver$Turn;

    move-result-object v3

    invoke-virtual {p1}, Lcom/here/android/mpa/routing/Maneuver;->getTurn()Lcom/here/android/mpa/routing/Maneuver$Turn;

    move-result-object v4

    if-ne v3, v4, :cond_1

    .line 431
    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Maneuver;->getNextRoadNumber()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/here/android/mpa/routing/Maneuver;->getNextRoadNumber()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 432
    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Maneuver;->getNextRoadName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/here/android/mpa/routing/Maneuver;->getNextRoadName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 433
    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Maneuver;->getRoadNumber()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/here/android/mpa/routing/Maneuver;->getRoadNumber()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 434
    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Maneuver;->getRoadName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/here/android/mpa/routing/Maneuver;->getRoadName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 435
    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Maneuver;->getAction()Lcom/here/android/mpa/routing/Maneuver$Action;

    move-result-object v3

    invoke-virtual {p1}, Lcom/here/android/mpa/routing/Maneuver;->getAction()Lcom/here/android/mpa/routing/Maneuver$Action;

    move-result-object v4

    if-ne v3, v4, :cond_1

    .line 436
    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Maneuver;->getDistanceToNextManeuver()I

    move-result v3

    invoke-virtual {p1}, Lcom/here/android/mpa/routing/Maneuver;->getDistanceToNextManeuver()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    if-gt v3, v5, :cond_1

    .line 437
    const/4 v1, 0x1

    .line 439
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 440
    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Maneuver;->getDistanceToNextManeuver()I

    move-result v3

    invoke-virtual {p1}, Lcom/here/android/mpa/routing/Maneuver;->getDistanceToNextManeuver()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 441
    .local v0, "distance":I
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "areManeuverEqual: distance diff="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " allowance="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x1f4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 444
    .end local v0    # "distance":I
    :catch_0
    move-exception v2

    .line 445
    .local v2, "t":Ljava/lang/Throwable;
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto/16 :goto_0
.end method

.method static areManeuversEqual(Ljava/util/List;ILjava/util/List;ILjava/util/List;)Z
    .locals 9
    .param p1, "firstOffset"    # I
    .param p3, "secondOffset"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/routing/Maneuver;",
            ">;I",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/routing/Maneuver;",
            ">;I",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/routing/Maneuver;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p0, "first":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    .local p2, "second":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    .local p4, "difference":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    const/4 v6, 0x0

    .line 507
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v7

    sub-int v1, v7, p1

    .line 508
    .local v1, "len1":I
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v7

    sub-int v2, v7, p3

    .local v2, "len2":I
    move v5, p3

    .end local p3    # "secondOffset":I
    .local v5, "secondOffset":I
    move v0, p1

    .line 510
    .end local p1    # "firstOffset":I
    .local v0, "firstOffset":I
    :goto_0
    if-lez v1, :cond_3

    if-lez v2, :cond_3

    .line 511
    add-int/lit8 p1, v0, 0x1

    .end local v0    # "firstOffset":I
    .restart local p1    # "firstOffset":I
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/here/android/mpa/routing/Maneuver;

    .line 512
    .local v3, "m1":Lcom/here/android/mpa/routing/Maneuver;
    add-int/lit8 p3, v5, 0x1

    .end local v5    # "secondOffset":I
    .restart local p3    # "secondOffset":I
    invoke-interface {p2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/here/android/mpa/routing/Maneuver;

    .line 513
    .local v4, "m2":Lcom/here/android/mpa/routing/Maneuver;
    invoke-static {v3, v4}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->areManeuverEqual(Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 514
    sget-object v7, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "maneuver are NOT equal"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 515
    const-string v7, "[maneuver-1]"

    invoke-static {v3, v7}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->printManeuverDetails(Lcom/here/android/mpa/routing/Maneuver;Ljava/lang/String;)V

    .line 516
    const-string v7, "[maneuver-2]"

    invoke-static {v4, v7}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->printManeuverDetails(Lcom/here/android/mpa/routing/Maneuver;Ljava/lang/String;)V

    .line 517
    if-eqz p4, :cond_0

    .line 518
    invoke-interface {p4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 538
    .end local v3    # "m1":Lcom/here/android/mpa/routing/Maneuver;
    .end local v4    # "m2":Lcom/here/android/mpa/routing/Maneuver;
    :cond_0
    :goto_1
    return v6

    .line 522
    .restart local v3    # "m1":Lcom/here/android/mpa/routing/Maneuver;
    .restart local v4    # "m2":Lcom/here/android/mpa/routing/Maneuver;
    :cond_1
    sget-object v7, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v8, 0x2

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 523
    sget-object v7, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "maneuver are equal"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 524
    const-string v7, "[maneuver-1]"

    invoke-static {v3, v7}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->printManeuverDetails(Lcom/here/android/mpa/routing/Maneuver;Ljava/lang/String;)V

    .line 525
    const-string v7, "[maneuver-2]"

    invoke-static {v4, v7}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->printManeuverDetails(Lcom/here/android/mpa/routing/Maneuver;Ljava/lang/String;)V

    .line 528
    :cond_2
    add-int/lit8 v1, v1, -0x1

    .line 529
    add-int/lit8 v2, v2, -0x1

    move v5, p3

    .end local p3    # "secondOffset":I
    .restart local v5    # "secondOffset":I
    move v0, p1

    .line 530
    .end local p1    # "firstOffset":I
    .restart local v0    # "firstOffset":I
    goto :goto_0

    .line 532
    .end local v3    # "m1":Lcom/here/android/mpa/routing/Maneuver;
    .end local v4    # "m2":Lcom/here/android/mpa/routing/Maneuver;
    :cond_3
    if-gtz v1, :cond_4

    if-lez v2, :cond_6

    .line 533
    :cond_4
    if-lez v1, :cond_5

    if-eqz p4, :cond_5

    .line 534
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-interface {p4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    move p3, v5

    .end local v5    # "secondOffset":I
    .restart local p3    # "secondOffset":I
    move p1, v0

    .line 536
    .end local v0    # "firstOffset":I
    .restart local p1    # "firstOffset":I
    goto :goto_1

    .line 538
    .end local p1    # "firstOffset":I
    .end local p3    # "secondOffset":I
    .restart local v0    # "firstOffset":I
    .restart local v5    # "secondOffset":I
    :cond_6
    const/4 v6, 0x1

    move p3, v5

    .end local v5    # "secondOffset":I
    .restart local p3    # "secondOffset":I
    move p1, v0

    .end local v0    # "firstOffset":I
    .restart local p1    # "firstOffset":I
    goto :goto_1
.end method

.method public static concatText(Ljava/lang/String;Ljava/lang/String;ZZ)Ljava/lang/String;
    .locals 4
    .param p0, "number"    # Ljava/lang/String;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "addParenthesis"    # Z
    .param p3, "isHighway"    # Z

    .prologue
    .line 869
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->stringBuilder:Ljava/lang/StringBuilder;

    monitor-enter v2

    .line 870
    :try_start_0
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapUtil;->stringBuilder:Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 871
    const/4 v0, 0x0

    .line 873
    .local v0, "hasNumber":Z
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 874
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapUtil;->stringBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 875
    const/4 v0, 0x1

    .line 881
    :goto_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 883
    if-nez p3, :cond_1

    .line 884
    if-eqz v0, :cond_3

    .line 885
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapUtil;->stringBuilder:Ljava/lang/StringBuilder;

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 886
    if-eqz p2, :cond_0

    .line 887
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapUtil;->stringBuilder:Ljava/lang/StringBuilder;

    const-string v3, "("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 892
    :cond_0
    :goto_1
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapUtil;->stringBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 894
    if-eqz p2, :cond_1

    .line 895
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapUtil;->stringBuilder:Ljava/lang/StringBuilder;

    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 899
    :cond_1
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapUtil;->stringBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    monitor-exit v2

    return-object v1

    .line 878
    :cond_2
    const/4 p3, 0x0

    goto :goto_0

    .line 890
    :cond_3
    const/4 p2, 0x0

    goto :goto_1

    .line 900
    .end local v0    # "hasNumber":Z
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static convertDateToEta(Ljava/util/Date;)Ljava/lang/String;
    .locals 14
    .param p0, "etaDate"    # Ljava/util/Date;

    .prologue
    .line 1517
    invoke-static {p0}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->isValidEtaDate(Ljava/util/Date;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1518
    const/4 v4, 0x0

    .line 1543
    :cond_0
    :goto_0
    return-object v4

    .line 1521
    :cond_1
    const/4 v4, 0x0

    .line 1522
    .local v4, "eta":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v10

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    sub-long v2, v10, v12

    .line 1523
    .local v2, "duration":J
    const-wide/16 v10, 0x0

    cmp-long v5, v2, v10

    if-lez v5, :cond_0

    .line 1524
    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v5, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v8

    .line 1525
    .local v8, "minutes":J
    const-wide/16 v10, 0x3c

    cmp-long v5, v8, v10

    if-gez v5, :cond_2

    .line 1527
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, " "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v10, Lcom/navdy/hud/app/maps/here/HereMapUtil;->min:Ljava/lang/String;

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 1529
    :cond_2
    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v5, v2, v3}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v6

    .line 1530
    .local v6, "hours":J
    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v10, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v10, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v10

    sub-long v10, v2, v10

    invoke-virtual {v5, v10, v11}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v8

    .line 1531
    const-wide/16 v10, 0x18

    cmp-long v5, v6, v10

    if-gez v5, :cond_3

    .line 1532
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, " "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v10, Lcom/navdy/hud/app/maps/here/HereMapUtil;->hr:Ljava/lang/String;

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, " "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 1533
    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, " "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v10, Lcom/navdy/hud/app/maps/here/HereMapUtil;->min_short:Ljava/lang/String;

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 1536
    :cond_3
    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v5, v2, v3}, Ljava/util/concurrent/TimeUnit;->toDays(J)J

    move-result-wide v0

    .line 1537
    .local v0, "days":J
    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v10, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v10, v0, v1}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v10

    sub-long v10, v2, v10

    invoke-virtual {v5, v10, v11}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v6

    .line 1538
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, " "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v10, Lcom/navdy/hud/app/maps/here/HereMapUtil;->day:Ljava/lang/String;

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, " "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 1539
    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, " "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v10, Lcom/navdy/hud/app/maps/here/HereMapUtil;->hr:Ljava/lang/String;

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0
.end method

.method public static findManeuverIndex(Ljava/util/List;Lcom/here/android/mpa/routing/Maneuver;)I
    .locals 4
    .param p1, "maneuver"    # Lcom/here/android/mpa/routing/Maneuver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/routing/Maneuver;",
            ">;",
            "Lcom/here/android/mpa/routing/Maneuver;",
            ")I"
        }
    .end annotation

    .prologue
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    const/4 v3, -0x1

    .line 1149
    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    :cond_0
    move v0, v3

    .line 1161
    :cond_1
    :goto_0
    return v0

    .line 1153
    :cond_2
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    .line 1154
    .local v1, "len":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_3

    .line 1155
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/here/android/mpa/routing/Maneuver;

    invoke-static {v2, p1}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->areManeuverEqual(Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1154
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    move v0, v3

    .line 1161
    goto :goto_0
.end method

.method private static formatString(Landroid/text/SpannableStringBuilder;Ljava/lang/String;Landroid/text/style/TextAppearanceSpan;Ljava/lang/String;Landroid/text/style/TextAppearanceSpan;Ljava/lang/String;Landroid/text/style/TextAppearanceSpan;Ljava/lang/String;Landroid/text/style/TextAppearanceSpan;Z)V
    .locals 4
    .param p0, "builder"    # Landroid/text/SpannableStringBuilder;
    .param p1, "s1"    # Ljava/lang/String;
    .param p2, "s1Span"    # Landroid/text/style/TextAppearanceSpan;
    .param p3, "s2"    # Ljava/lang/String;
    .param p4, "s2Span"    # Landroid/text/style/TextAppearanceSpan;
    .param p5, "s3"    # Ljava/lang/String;
    .param p6, "s3Span"    # Landroid/text/style/TextAppearanceSpan;
    .param p7, "s4"    # Ljava/lang/String;
    .param p8, "s4Span"    # Landroid/text/style/TextAppearanceSpan;
    .param p9, "addSpaceBetweenUnits"    # Z

    .prologue
    const/16 v3, 0x21

    .line 1418
    if-eqz p1, :cond_0

    .line 1419
    invoke-virtual {p0, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1420
    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    invoke-virtual {p0, p2, v1, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1423
    :cond_0
    if-eqz p3, :cond_2

    .line 1424
    if-eqz p9, :cond_1

    .line 1425
    const-string v1, " "

    invoke-virtual {p0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1427
    :cond_1
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    .line 1428
    .local v0, "offset":I
    invoke-virtual {p0, p3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1429
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    invoke-virtual {p0, p4, v0, v1, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1432
    .end local v0    # "offset":I
    :cond_2
    if-eqz p5, :cond_3

    .line 1433
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    .line 1434
    .restart local v0    # "offset":I
    const-string v1, " "

    invoke-virtual {p0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1435
    invoke-virtual {p0, p5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1436
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    invoke-virtual {p0, p6, v0, v1, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1439
    .end local v0    # "offset":I
    :cond_3
    if-eqz p7, :cond_5

    .line 1440
    if-eqz p9, :cond_4

    .line 1441
    const-string v1, " "

    invoke-virtual {p0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1443
    :cond_4
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    .line 1444
    .restart local v0    # "offset":I
    invoke-virtual {p0, p7}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1445
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    invoke-virtual {p0, p8, v0, v1, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1447
    .end local v0    # "offset":I
    :cond_5
    return-void
.end method

.method public static generateRouteIcons(Ljava/lang/String;Ljava/lang/String;Lcom/here/android/mpa/routing/Route;)V
    .locals 19
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "label"    # Ljava/lang/String;
    .param p2, "route"    # Lcom/here/android/mpa/routing/Route;

    .prologue
    .line 1328
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 1331
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v4

    .line 1332
    .local v4, "context":Landroid/content/Context;
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/navdy/hud/app/storage/PathManager;->getInstance()Lcom/navdy/hud/app/storage/PathManager;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/navdy/hud/app/storage/PathManager;->getMapsPartitionPath()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget-object v16, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "routeicons"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget-object v16, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "_"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-static/range {p1 .. p1}, Lcom/navdy/hud/app/util/GenericUtil;->normalizeToFilename(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 1333
    .local v12, "path":Ljava/lang/String;
    sget-object v15, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "generateRouteIcons: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1334
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1335
    .local v5, "f":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v15

    if-eqz v15, :cond_0

    .line 1336
    invoke-static {v4, v5}, Lcom/navdy/service/library/util/IOUtils;->deleteDirectory(Landroid/content/Context;Ljava/io/File;)V

    .line 1338
    :cond_0
    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    .line 1339
    invoke-virtual/range {p2 .. p2}, Lcom/here/android/mpa/routing/Route;->getManeuvers()Ljava/util/List;

    move-result-object v11

    .line 1340
    .local v11, "maneuvers":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    const/4 v9, 0x0

    .line 1341
    .local v9, "iconCount":I
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_1
    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_2

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/here/android/mpa/routing/Maneuver;

    .line 1342
    .local v10, "maneuver":Lcom/here/android/mpa/routing/Maneuver;
    invoke-virtual {v10}, Lcom/here/android/mpa/routing/Maneuver;->getNextRoadImage()Lcom/here/android/mpa/common/Image;

    move-result-object v8

    .line 1343
    .local v8, "icon":Lcom/here/android/mpa/common/Image;
    if-eqz v8, :cond_1

    .line 1344
    invoke-virtual {v8}, Lcom/here/android/mpa/common/Image;->getWidth()J

    move-result-wide v16

    move-wide/from16 v0, v16

    long-to-int v14, v0

    .line 1345
    .local v14, "w":I
    invoke-virtual {v8}, Lcom/here/android/mpa/common/Image;->getHeight()J

    move-result-wide v16

    move-wide/from16 v0, v16

    long-to-int v7, v0

    .line 1346
    .local v7, "h":I
    sget-object v16, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "generateRouteIcons: w="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " h="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1347
    mul-int/lit8 v16, v14, 0x3

    mul-int/lit8 v17, v7, 0x3

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v8, v0, v1}, Lcom/here/android/mpa/common/Image;->getBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1348
    .local v2, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v2, :cond_1

    .line 1349
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "man_"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-static {v10}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getNextRoadName(Lcom/here/android/mpa/routing/Maneuver;)Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/navdy/hud/app/util/GenericUtil;->normalizeToFilename(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "_"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "_"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ".png"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1350
    .local v6, "filename":Ljava/lang/String;
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 1351
    .local v3, "bout":Ljava/io/ByteArrayOutputStream;
    sget-object v16, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v17, 0x64

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v2, v0, v1, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 1352
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    sget-object v17, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/navdy/service/library/util/IOUtils;->copyFile(Ljava/lang/String;[B)I

    .line 1353
    add-int/lit8 v9, v9, 0x1

    .line 1354
    sget-object v16, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v17, "icon generated"

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1359
    .end local v2    # "bitmap":Landroid/graphics/Bitmap;
    .end local v3    # "bout":Ljava/io/ByteArrayOutputStream;
    .end local v4    # "context":Landroid/content/Context;
    .end local v5    # "f":Ljava/io/File;
    .end local v6    # "filename":Ljava/lang/String;
    .end local v7    # "h":I
    .end local v8    # "icon":Lcom/here/android/mpa/common/Image;
    .end local v9    # "iconCount":I
    .end local v10    # "maneuver":Lcom/here/android/mpa/routing/Maneuver;
    .end local v11    # "maneuvers":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    .end local v12    # "path":Ljava/lang/String;
    .end local v14    # "w":I
    :catch_0
    move-exception v13

    .line 1360
    .local v13, "t":Ljava/lang/Throwable;
    sget-object v15, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v15, v13}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 1362
    .end local v13    # "t":Ljava/lang/Throwable;
    :goto_1
    return-void

    .line 1358
    .restart local v4    # "context":Landroid/content/Context;
    .restart local v5    # "f":Ljava/io/File;
    .restart local v9    # "iconCount":I
    .restart local v11    # "maneuvers":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    .restart local v12    # "path":Ljava/lang/String;
    :cond_2
    :try_start_1
    sget-object v15, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "total icons generated:"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public static getAheadRouteElements(Lcom/here/android/mpa/routing/Route;Lcom/here/android/mpa/common/RoadElement;Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;)Ljava/util/List;
    .locals 14
    .param p0, "route"    # Lcom/here/android/mpa/routing/Route;
    .param p1, "currentRoadElement"    # Lcom/here/android/mpa/common/RoadElement;
    .param p2, "prev"    # Lcom/here/android/mpa/routing/Maneuver;
    .param p3, "next"    # Lcom/here/android/mpa/routing/Maneuver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/here/android/mpa/routing/Route;",
            "Lcom/here/android/mpa/common/RoadElement;",
            "Lcom/here/android/mpa/routing/Maneuver;",
            "Lcom/here/android/mpa/routing/Maneuver;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/routing/RouteElement;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1169
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    if-nez p3, :cond_2

    .line 1172
    :cond_0
    sget-object v11, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v12, "getAheadRouteElements:invalid arg"

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 1173
    const/4 v1, 0x0

    .line 1216
    :cond_1
    :goto_0
    return-object v1

    .line 1176
    :cond_2
    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Route;->getManeuvers()Ljava/util/List;

    move-result-object v7

    .line 1177
    .local v7, "maneuvers":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    move-object/from16 v0, p3

    invoke-static {v7, v0}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->findManeuverIndex(Ljava/util/List;Lcom/here/android/mpa/routing/Maneuver;)I

    move-result v4

    .line 1179
    .local v4, "index":I
    const/4 v11, -0x1

    if-ne v4, v11, :cond_3

    .line 1180
    sget-object v11, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v12, "getAheadRouteElements:maneuver index not found"

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 1181
    const/4 v1, 0x0

    goto :goto_0

    .line 1184
    :cond_3
    sget-object v11, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getAheadRouteElements:found maneuver index:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " total:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 1186
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1189
    .local v1, "aheadRouteElements":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/RouteElement;>;"
    if-eqz p2, :cond_6

    .line 1190
    invoke-virtual/range {p2 .. p2}, Lcom/here/android/mpa/routing/Maneuver;->getRouteElements()Ljava/util/List;

    move-result-object v10

    .line 1191
    .local v10, "routeElements":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/RouteElement;>;"
    if-eqz v10, :cond_6

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v11

    if-lez v11, :cond_6

    .line 1193
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v5

    .line 1194
    .local v5, "len":I
    const/4 v2, 0x0

    .line 1195
    .local v2, "found":Z
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v5, :cond_6

    .line 1196
    invoke-interface {v10, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/here/android/mpa/routing/RouteElement;

    .line 1197
    .local v9, "routeElement":Lcom/here/android/mpa/routing/RouteElement;
    if-nez v2, :cond_5

    .line 1198
    invoke-virtual {v9}, Lcom/here/android/mpa/routing/RouteElement;->getRoadElement()Lcom/here/android/mpa/common/RoadElement;

    move-result-object v8

    .line 1199
    .local v8, "re":Lcom/here/android/mpa/common/RoadElement;
    if-eqz v8, :cond_4

    invoke-virtual {v8, p1}, Lcom/here/android/mpa/common/RoadElement;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 1200
    const/4 v2, 0x1

    .line 1195
    .end local v8    # "re":Lcom/here/android/mpa/common/RoadElement;
    :cond_4
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1203
    :cond_5
    invoke-interface {v1, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1210
    .end local v2    # "found":Z
    .end local v3    # "i":I
    .end local v5    # "len":I
    .end local v9    # "routeElement":Lcom/here/android/mpa/routing/RouteElement;
    .end local v10    # "routeElements":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/RouteElement;>;"
    :cond_6
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v5

    .line 1211
    .restart local v5    # "len":I
    move v3, v4

    .restart local v3    # "i":I
    :goto_3
    if-ge v3, v5, :cond_1

    .line 1212
    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/here/android/mpa/routing/Maneuver;

    .line 1213
    .local v6, "maneuver":Lcom/here/android/mpa/routing/Maneuver;
    invoke-virtual {v6}, Lcom/here/android/mpa/routing/Maneuver;->getRouteElements()Ljava/util/List;

    move-result-object v11

    invoke-interface {v1, v11}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1211
    add-int/lit8 v3, v3, 0x1

    goto :goto_3
.end method

.method public static getAllRoadNames(Ljava/util/List;I)Ljava/lang/String;
    .locals 4
    .param p1, "startOffset"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/routing/Maneuver;",
            ">;I)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1081
    .local p0, "maneuvers":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    if-eqz p0, :cond_0

    if-gez p1, :cond_1

    .line 1082
    :cond_0
    const-string v3, ""

    .line 1099
    :goto_0
    return-object v3

    .line 1085
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    .line 1086
    .local v2, "len":I
    if-lt p1, v2, :cond_2

    .line 1087
    const-string v3, ""

    goto :goto_0

    .line 1090
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1092
    .local v0, "builder":Ljava/lang/StringBuilder;
    move v1, p1

    .local v1, "i":I
    :goto_1
    if-ge v1, v2, :cond_3

    .line 1093
    const-string v3, "["

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1094
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/here/android/mpa/routing/Maneuver;

    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getRoadName(Lcom/here/android/mpa/routing/Maneuver;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1095
    const-string v3, "]"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1096
    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1092
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1099
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public static getCurrentEtaString()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 1496
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v5

    if-nez v5, :cond_1

    .line 1513
    .local v0, "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    :cond_0
    :goto_0
    return-object v4

    .line 1499
    .end local v0    # "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v0

    .line 1500
    .restart local v0    # "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isNavigationModeOn()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1504
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v2

    .line 1505
    .local v2, "uiStateManager":Lcom/navdy/hud/app/ui/framework/UIStateManager;
    invoke-virtual {v2}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getHomescreenView()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    move-result-object v3

    .line 1506
    .local v3, "view":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    if-eqz v3, :cond_0

    .line 1509
    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->getEtaView()Lcom/navdy/hud/app/ui/component/homescreen/EtaView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/ui/component/homescreen/EtaView;->getLastEtaDate()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    goto :goto_0

    .line 1510
    .end local v2    # "uiStateManager":Lcom/navdy/hud/app/ui/framework/UIStateManager;
    .end local v3    # "view":Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;
    :catch_0
    move-exception v1

    .line 1511
    .local v1, "t":Ljava/lang/Throwable;
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v5, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static getCurrentEtaStringWithDestination()Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v6, 0x0

    .line 1451
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v7

    if-nez v7, :cond_1

    move-object v0, v6

    .line 1491
    .local v1, "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    :cond_0
    :goto_0
    return-object v0

    .line 1454
    .end local v1    # "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v1

    .line 1455
    .restart local v1    # "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isNavigationModeOn()Z

    move-result v7

    if-nez v7, :cond_2

    move-object v0, v6

    .line 1456
    goto :goto_0

    .line 1458
    :cond_2
    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getDestinationLabel()Ljava/lang/String;

    move-result-object v2

    .line 1459
    .local v2, "label":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1460
    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getDestinationStreetAddress()Ljava/lang/String;

    move-result-object v2

    .line 1461
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1462
    const/4 v2, 0x0

    .line 1466
    :cond_3
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1467
    .local v4, "resources":Landroid/content/res/Resources;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v3

    .line 1469
    .local v3, "navigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hasArrived()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1470
    if-eqz v2, :cond_4

    .line 1471
    const v7, 0x7f0901b1

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v2, v8, v9

    invoke-virtual {v4, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1473
    :cond_4
    const v7, 0x7f0901b3

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1478
    :cond_5
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getCurrentEtaString()Ljava/lang/String;

    move-result-object v0

    .line 1479
    .local v0, "eta":Ljava/lang/String;
    if-nez v0, :cond_6

    move-object v0, v6

    .line 1480
    goto :goto_0

    .line 1483
    :cond_6
    if-eqz v2, :cond_0

    .line 1484
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f0901b4

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v0, v9, v10

    const/4 v10, 0x1

    aput-object v2, v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 1488
    .end local v0    # "eta":Ljava/lang/String;
    .end local v2    # "label":Ljava/lang/String;
    .end local v3    # "navigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    .end local v4    # "resources":Landroid/content/res/Resources;
    :catch_0
    move-exception v5

    .line 1489
    .local v5, "t":Ljava/lang/Throwable;
    sget-object v7, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v7, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    move-object v0, v6

    .line 1491
    goto :goto_0
.end method

.method public static getCurrentRoadElement()Lcom/here/android/mpa/common/RoadElement;
    .locals 2

    .prologue
    .line 141
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getPositioningManager()Lcom/here/android/mpa/common/PositioningManager;

    move-result-object v0

    .line 142
    .local v0, "positioningManager":Lcom/here/android/mpa/common/PositioningManager;
    if-nez v0, :cond_0

    .line 143
    const/4 v1, 0x0

    .line 145
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/here/android/mpa/common/PositioningManager;->getRoadElement()Lcom/here/android/mpa/common/RoadElement;

    move-result-object v1

    goto :goto_0
.end method

.method public static getCurrentRoadName()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 149
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getPositioningManager()Lcom/here/android/mpa/common/PositioningManager;

    move-result-object v0

    .line 150
    .local v0, "positioningManager":Lcom/here/android/mpa/common/PositioningManager;
    if-nez v0, :cond_1

    .line 157
    :cond_0
    :goto_0
    return-object v2

    .line 153
    :cond_1
    invoke-virtual {v0}, Lcom/here/android/mpa/common/PositioningManager;->getRoadElement()Lcom/here/android/mpa/common/RoadElement;

    move-result-object v1

    .line 154
    .local v1, "roadElement":Lcom/here/android/mpa/common/RoadElement;
    if-eqz v1, :cond_0

    .line 155
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getRoadName(Lcom/here/android/mpa/common/RoadElement;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public static getCurrentSpeedLimit()F
    .locals 2

    .prologue
    .line 200
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getCurrentRoadElement()Lcom/here/android/mpa/common/RoadElement;

    move-result-object v0

    .line 201
    .local v0, "roadElement":Lcom/here/android/mpa/common/RoadElement;
    if-eqz v0, :cond_0

    .line 202
    invoke-virtual {v0}, Lcom/here/android/mpa/common/RoadElement;->getSpeedLimit()F

    move-result v1

    .line 204
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static getDifferentManeuver(Lcom/here/android/mpa/routing/Route;Lcom/here/android/mpa/routing/Route;ILjava/util/List;)Ljava/util/List;
    .locals 14
    .param p0, "route"    # Lcom/here/android/mpa/routing/Route;
    .param p1, "other"    # Lcom/here/android/mpa/routing/Route;
    .param p2, "n"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/here/android/mpa/routing/Route;",
            "Lcom/here/android/mpa/routing/Route;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/routing/Maneuver;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/routing/Maneuver;",
            ">;"
        }
    .end annotation

    .prologue
    .line 453
    .local p3, "otherManeuverDifference":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 455
    .local v5, "result":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Route;->getManeuvers()Ljava/util/List;

    move-result-object v7

    .line 456
    .local v7, "routeManeuvers":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    invoke-virtual {p1}, Lcom/here/android/mpa/routing/Route;->getManeuvers()Ljava/util/List;

    move-result-object v4

    .line 458
    .local v4, "otherManeuvers":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v9

    .line 459
    .local v9, "size1":I
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v10

    .line 461
    .local v10, "size2":I
    if-le v9, v10, :cond_1

    add-int/lit8 v2, v10, -0x1

    .line 462
    .local v2, "maxIndex":I
    :goto_0
    const/4 v1, 0x0

    .line 464
    .local v1, "i":I
    :goto_1
    if-gt v1, v2, :cond_4

    .line 465
    invoke-interface {v7, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/here/android/mpa/routing/Maneuver;

    .line 466
    .local v6, "routeManeuver":Lcom/here/android/mpa/routing/Maneuver;
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/here/android/mpa/routing/Maneuver;

    .line 468
    .local v3, "otherManeuver":Lcom/here/android/mpa/routing/Maneuver;
    sget-object v11, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v12, 0x2

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 469
    const-string v11, "getDifferentManeuver-1"

    invoke-static {v6, v11}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->printManeuverDetails(Lcom/here/android/mpa/routing/Maneuver;Ljava/lang/String;)V

    .line 470
    const-string v11, "getDifferentManeuver-2"

    invoke-static {v3, v11}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->printManeuverDetails(Lcom/here/android/mpa/routing/Maneuver;Ljava/lang/String;)V

    .line 473
    :cond_0
    invoke-static {v6, v3}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->areManeuverEqual(Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;)Z

    move-result v11

    if-nez v11, :cond_5

    .line 474
    invoke-static {v6}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getNextRoadName(Lcom/here/android/mpa/routing/Maneuver;)Ljava/lang/String;

    move-result-object v8

    .line 475
    .local v8, "s":Ljava/lang/String;
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 476
    add-int/lit8 v1, v1, 0x1

    .line 477
    goto :goto_1

    .line 461
    .end local v1    # "i":I
    .end local v2    # "maxIndex":I
    .end local v3    # "otherManeuver":Lcom/here/android/mpa/routing/Maneuver;
    .end local v6    # "routeManeuver":Lcom/here/android/mpa/routing/Maneuver;
    .end local v8    # "s":Ljava/lang/String;
    :cond_1
    add-int/lit8 v2, v9, -0x1

    goto :goto_0

    .line 480
    .restart local v1    # "i":I
    .restart local v2    # "maxIndex":I
    .restart local v3    # "otherManeuver":Lcom/here/android/mpa/routing/Maneuver;
    .restart local v6    # "routeManeuver":Lcom/here/android/mpa/routing/Maneuver;
    .restart local v8    # "s":Ljava/lang/String;
    :cond_2
    sget-object v11, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v12, 0x2

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 481
    sget-object v11, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getDifferentManeuver:diff:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 484
    :cond_3
    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 485
    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 486
    add-int/lit8 p2, p2, -0x1

    .line 487
    if-nez p2, :cond_5

    .line 495
    .end local v3    # "otherManeuver":Lcom/here/android/mpa/routing/Maneuver;
    .end local v6    # "routeManeuver":Lcom/here/android/mpa/routing/Maneuver;
    .end local v8    # "s":Ljava/lang/String;
    :cond_4
    return-object v5

    .line 492
    .restart local v3    # "otherManeuver":Lcom/here/android/mpa/routing/Maneuver;
    .restart local v6    # "routeManeuver":Lcom/here/android/mpa/routing/Maneuver;
    :cond_5
    add-int/lit8 v1, v1, 0x1

    .line 493
    goto :goto_1
.end method

.method public static getDistanceToRoadElement(Lcom/here/android/mpa/common/RoadElement;Lcom/here/android/mpa/common/RoadElement;Ljava/util/List;)J
    .locals 12
    .param p0, "from"    # Lcom/here/android/mpa/common/RoadElement;
    .param p1, "to"    # Lcom/here/android/mpa/common/RoadElement;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/here/android/mpa/common/RoadElement;",
            "Lcom/here/android/mpa/common/RoadElement;",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/routing/Maneuver;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 1231
    .local p2, "maneuvers":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    .line 1232
    :cond_0
    const-wide/16 v0, -0x1

    .line 1260
    :cond_1
    :goto_0
    return-wide v0

    .line 1235
    :cond_2
    const-wide/16 v0, -0x1

    .line 1236
    .local v0, "distance":J
    const/4 v3, 0x0

    .line 1237
    .local v3, "fromFound":Z
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v7

    .line 1239
    .local v7, "maneuverLen":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    if-ge v4, v7, :cond_6

    .line 1240
    invoke-interface {p2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/here/android/mpa/routing/Maneuver;

    .line 1241
    .local v6, "maneuver":Lcom/here/android/mpa/routing/Maneuver;
    invoke-virtual {v6}, Lcom/here/android/mpa/routing/Maneuver;->getRoadElements()Ljava/util/List;

    move-result-object v9

    .line 1242
    .local v9, "roadElements":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/common/RoadElement;>;"
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v2

    .line 1243
    .local v2, "elementLen":I
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_2
    if-ge v5, v2, :cond_5

    .line 1244
    invoke-interface {v9, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/here/android/mpa/common/RoadElement;

    .line 1245
    .local v8, "re":Lcom/here/android/mpa/common/RoadElement;
    if-nez v3, :cond_4

    .line 1246
    invoke-virtual {v8, p0}, Lcom/here/android/mpa/common/RoadElement;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 1247
    const/4 v3, 0x1

    .line 1248
    invoke-virtual {v8}, Lcom/here/android/mpa/common/RoadElement;->getGeometryLength()D

    move-result-wide v10

    double-to-long v0, v10

    .line 1243
    :cond_3
    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 1251
    :cond_4
    invoke-virtual {v8, p1}, Lcom/here/android/mpa/common/RoadElement;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1

    .line 1255
    invoke-virtual {v8}, Lcom/here/android/mpa/common/RoadElement;->getGeometryLength()D

    move-result-wide v10

    double-to-long v10, v10

    add-long/2addr v0, v10

    goto :goto_3

    .line 1239
    .end local v8    # "re":Lcom/here/android/mpa/common/RoadElement;
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1260
    .end local v2    # "elementLen":I
    .end local v5    # "j":I
    .end local v6    # "maneuver":Lcom/here/android/mpa/routing/Maneuver;
    .end local v9    # "roadElements":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/common/RoadElement;>;"
    :cond_6
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public static getEtaString(Lcom/here/android/mpa/routing/Route;Ljava/lang/String;Z)Landroid/text/SpannableStringBuilder;
    .locals 21
    .param p0, "route"    # Lcom/here/android/mpa/routing/Route;
    .param p1, "routeId"    # Ljava/lang/String;
    .param p2, "addSpaceBetweenUnits"    # Z

    .prologue
    .line 1365
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1366
    .local v2, "tripDuration":Landroid/text/SpannableStringBuilder;
    if-nez p0, :cond_1

    .line 1403
    :cond_0
    :goto_0
    return-object v2

    .line 1369
    :cond_1
    invoke-static/range {p0 .. p0}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getRouteTtaDate(Lcom/here/android/mpa/routing/Route;)Ljava/util/Date;

    move-result-object v20

    .line 1370
    .local v20, "ttaDate":Ljava/util/Date;
    const-wide/16 v14, 0x0

    .line 1371
    .local v14, "duration":J
    if-eqz v20, :cond_2

    .line 1372
    invoke-virtual/range {v20 .. v20}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v14, v4, v6

    .line 1376
    :goto_1
    const-wide/16 v4, 0x0

    cmp-long v3, v14, v4

    if-lez v3, :cond_0

    .line 1377
    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v14, v15}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v18

    .line 1378
    .local v18, "minutes":J
    const-wide/16 v4, 0x3c

    cmp-long v3, v18, v4

    if-gez v3, :cond_3

    .line 1380
    invoke-static/range {v18 .. v19}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sp_26_1:Landroid/text/style/TextAppearanceSpan;

    sget-object v5, Lcom/navdy/hud/app/maps/here/HereMapUtil;->min:Ljava/lang/String;

    sget-object v6, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sp_20_b_1:Landroid/text/style/TextAppearanceSpan;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x1

    invoke-static/range {v2 .. v11}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->formatString(Landroid/text/SpannableStringBuilder;Ljava/lang/String;Landroid/text/style/TextAppearanceSpan;Ljava/lang/String;Landroid/text/style/TextAppearanceSpan;Ljava/lang/String;Landroid/text/style/TextAppearanceSpan;Ljava/lang/String;Landroid/text/style/TextAppearanceSpan;Z)V

    goto :goto_0

    .line 1374
    .end local v18    # "minutes":J
    :cond_2
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "tta date is null for route:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto :goto_1

    .line 1382
    .restart local v18    # "minutes":J
    :cond_3
    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v14, v15}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v16

    .line 1383
    .local v16, "hours":J
    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v4, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    move-wide/from16 v0, v16

    invoke-virtual {v4, v0, v1}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    sub-long v4, v14, v4

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v18

    .line 1384
    const-wide/16 v4, 0xa

    cmp-long v3, v16, v4

    if-gez v3, :cond_4

    .line 1385
    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sp_26_1:Landroid/text/style/TextAppearanceSpan;

    sget-object v5, Lcom/navdy/hud/app/maps/here/HereMapUtil;->hr:Ljava/lang/String;

    sget-object v6, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sp_20_b_1:Landroid/text/style/TextAppearanceSpan;

    invoke-static/range {v18 .. v19}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sp_26_2:Landroid/text/style/TextAppearanceSpan;

    sget-object v9, Lcom/navdy/hud/app/maps/here/HereMapUtil;->min_short:Ljava/lang/String;

    sget-object v10, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sp_20_b_2:Landroid/text/style/TextAppearanceSpan;

    move/from16 v11, p2

    invoke-static/range {v2 .. v11}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->formatString(Landroid/text/SpannableStringBuilder;Ljava/lang/String;Landroid/text/style/TextAppearanceSpan;Ljava/lang/String;Landroid/text/style/TextAppearanceSpan;Ljava/lang/String;Landroid/text/style/TextAppearanceSpan;Ljava/lang/String;Landroid/text/style/TextAppearanceSpan;Z)V

    goto/16 :goto_0

    .line 1386
    :cond_4
    const-wide/16 v4, 0x18

    cmp-long v3, v16, v4

    if-gez v3, :cond_5

    .line 1388
    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sp_24_1:Landroid/text/style/TextAppearanceSpan;

    sget-object v5, Lcom/navdy/hud/app/maps/here/HereMapUtil;->hr:Ljava/lang/String;

    sget-object v6, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sp_20_b_1:Landroid/text/style/TextAppearanceSpan;

    invoke-static/range {v18 .. v19}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sp_24_2:Landroid/text/style/TextAppearanceSpan;

    sget-object v9, Lcom/navdy/hud/app/maps/here/HereMapUtil;->min_short:Ljava/lang/String;

    sget-object v10, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sp_20_b_2:Landroid/text/style/TextAppearanceSpan;

    move/from16 v11, p2

    invoke-static/range {v2 .. v11}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->formatString(Landroid/text/SpannableStringBuilder;Ljava/lang/String;Landroid/text/style/TextAppearanceSpan;Ljava/lang/String;Landroid/text/style/TextAppearanceSpan;Ljava/lang/String;Landroid/text/style/TextAppearanceSpan;Ljava/lang/String;Landroid/text/style/TextAppearanceSpan;Z)V

    goto/16 :goto_0

    .line 1391
    :cond_5
    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v14, v15}, Ljava/util/concurrent/TimeUnit;->toDays(J)J

    move-result-wide v12

    .line 1392
    .local v12, "days":J
    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v4, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v12, v13}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    sub-long v4, v14, v4

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v16

    .line 1393
    const-wide/16 v4, 0x1

    cmp-long v3, v12, v4

    if-nez v3, :cond_6

    .line 1395
    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sp_26_1:Landroid/text/style/TextAppearanceSpan;

    sget-object v5, Lcom/navdy/hud/app/maps/here/HereMapUtil;->day:Ljava/lang/String;

    sget-object v6, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sp_20_b_1:Landroid/text/style/TextAppearanceSpan;

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sp_26_2:Landroid/text/style/TextAppearanceSpan;

    sget-object v9, Lcom/navdy/hud/app/maps/here/HereMapUtil;->hr:Ljava/lang/String;

    sget-object v10, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sp_20_b_2:Landroid/text/style/TextAppearanceSpan;

    move/from16 v11, p2

    invoke-static/range {v2 .. v11}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->formatString(Landroid/text/SpannableStringBuilder;Ljava/lang/String;Landroid/text/style/TextAppearanceSpan;Ljava/lang/String;Landroid/text/style/TextAppearanceSpan;Ljava/lang/String;Landroid/text/style/TextAppearanceSpan;Ljava/lang/String;Landroid/text/style/TextAppearanceSpan;Z)V

    goto/16 :goto_0

    .line 1398
    :cond_6
    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sp_24_1:Landroid/text/style/TextAppearanceSpan;

    sget-object v5, Lcom/navdy/hud/app/maps/here/HereMapUtil;->day:Ljava/lang/String;

    sget-object v6, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sp_20_b_1:Landroid/text/style/TextAppearanceSpan;

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sp_24_2:Landroid/text/style/TextAppearanceSpan;

    sget-object v9, Lcom/navdy/hud/app/maps/here/HereMapUtil;->hr:Ljava/lang/String;

    sget-object v10, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sp_20_b_2:Landroid/text/style/TextAppearanceSpan;

    move/from16 v11, p2

    invoke-static/range {v2 .. v11}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->formatString(Landroid/text/SpannableStringBuilder;Ljava/lang/String;Landroid/text/style/TextAppearanceSpan;Ljava/lang/String;Landroid/text/style/TextAppearanceSpan;Ljava/lang/String;Landroid/text/style/TextAppearanceSpan;Ljava/lang/String;Landroid/text/style/TextAppearanceSpan;Z)V

    goto/16 :goto_0
.end method

.method public static getLastEqualManeuver(Lcom/here/android/mpa/routing/Route;Lcom/here/android/mpa/routing/Route;)Lcom/here/android/mpa/routing/Maneuver;
    .locals 10
    .param p0, "route"    # Lcom/here/android/mpa/routing/Route;
    .param p1, "other"    # Lcom/here/android/mpa/routing/Route;

    .prologue
    .line 591
    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Route;->getManeuvers()Ljava/util/List;

    move-result-object v7

    .line 592
    .local v7, "routeManeuvers":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    invoke-virtual {p1}, Lcom/here/android/mpa/routing/Route;->getManeuvers()Ljava/util/List;

    move-result-object v5

    .line 594
    .local v5, "otherManeuvers":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v9

    if-ge v8, v9, :cond_1

    .line 595
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    add-int/lit8 v3, v8, -0x1

    .line 600
    .local v3, "maxIndex":I
    :goto_0
    const/4 v2, 0x0

    .line 602
    .local v2, "lastEqual":Lcom/here/android/mpa/routing/Maneuver;
    const/4 v1, 0x0

    .line 604
    .local v1, "i":I
    :goto_1
    if-gt v1, v3, :cond_0

    .line 605
    invoke-interface {v7, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/here/android/mpa/routing/Maneuver;

    .line 606
    .local v6, "routeManeuver":Lcom/here/android/mpa/routing/Maneuver;
    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/here/android/mpa/routing/Maneuver;

    .line 608
    .local v4, "otherManeuver":Lcom/here/android/mpa/routing/Maneuver;
    invoke-static {v6, v4}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->areManeuverEqual(Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;)Z

    move-result v0

    .line 609
    .local v0, "equalManeuvers":Z
    if-eqz v0, :cond_2

    if-lez v1, :cond_2

    .line 610
    add-int/lit8 v8, v1, -0x1

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "lastEqual":Lcom/here/android/mpa/routing/Maneuver;
    check-cast v2, Lcom/here/android/mpa/routing/Maneuver;

    .line 616
    .end local v0    # "equalManeuvers":Z
    .end local v4    # "otherManeuver":Lcom/here/android/mpa/routing/Maneuver;
    .end local v6    # "routeManeuver":Lcom/here/android/mpa/routing/Maneuver;
    .restart local v2    # "lastEqual":Lcom/here/android/mpa/routing/Maneuver;
    :cond_0
    return-object v2

    .line 595
    .end local v1    # "i":I
    .end local v2    # "lastEqual":Lcom/here/android/mpa/routing/Maneuver;
    .end local v3    # "maxIndex":I
    :cond_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v8

    add-int/lit8 v3, v8, -0x1

    goto :goto_0

    .line 613
    .restart local v0    # "equalManeuvers":Z
    .restart local v1    # "i":I
    .restart local v2    # "lastEqual":Lcom/here/android/mpa/routing/Maneuver;
    .restart local v3    # "maxIndex":I
    .restart local v4    # "otherManeuver":Lcom/here/android/mpa/routing/Maneuver;
    .restart local v6    # "routeManeuver":Lcom/here/android/mpa/routing/Maneuver;
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static getLastKnownDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 841
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapUtil;->lastKnownDeviceId:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 843
    :try_start_0
    new-instance v0, Lcom/navdy/service/library/device/NavdyDeviceId;

    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapUtil;->lastKnownDeviceId:Ljava/lang/String;

    invoke-direct {v0, v3}, Lcom/navdy/service/library/device/NavdyDeviceId;-><init>(Ljava/lang/String;)V

    .line 844
    .local v0, "id":Lcom/navdy/service/library/device/NavdyDeviceId;
    const/4 v3, 0x0

    sput-object v3, Lcom/navdy/hud/app/maps/here/HereMapUtil;->lastKnownDeviceId:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 852
    .end local v0    # "id":Lcom/navdy/service/library/device/NavdyDeviceId;
    :goto_0
    return-object v0

    .line 846
    .restart local v0    # "id":Lcom/navdy/service/library/device/NavdyDeviceId;
    :catch_0
    move-exception v1

    .line 847
    .local v1, "t":Ljava/lang/Throwable;
    sput-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->lastKnownDeviceId:Ljava/lang/String;

    .line 848
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v3, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    move-object v0, v2

    .line 849
    goto :goto_0

    .end local v0    # "id":Lcom/navdy/service/library/device/NavdyDeviceId;
    .end local v1    # "t":Ljava/lang/Throwable;
    :cond_0
    move-object v0, v2

    .line 852
    goto :goto_0
.end method

.method public static getLastManeuver(Ljava/util/List;)Lcom/here/android/mpa/routing/Maneuver;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/routing/Maneuver;",
            ">;)",
            "Lcom/here/android/mpa/routing/Maneuver;"
        }
    .end annotation

    .prologue
    .line 1220
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_1

    .line 1221
    :cond_0
    const/4 v0, 0x0

    .line 1224
    :goto_0
    return-object v0

    :cond_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/here/android/mpa/routing/Maneuver;

    goto :goto_0
.end method

.method public static getManeuverDriveTime(Lcom/here/android/mpa/routing/Maneuver;Lcom/navdy/hud/app/maps/here/HereMapUtil$LongContainer;)J
    .locals 10
    .param p0, "maneuver"    # Lcom/here/android/mpa/routing/Maneuver;
    .param p1, "distance"    # Lcom/navdy/hud/app/maps/here/HereMapUtil$LongContainer;

    .prologue
    const-wide/16 v6, 0x0

    .line 1103
    if-nez p0, :cond_1

    move-wide v4, v6

    .line 1127
    :cond_0
    :goto_0
    return-wide v4

    .line 1107
    :cond_1
    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Maneuver;->getRoadElements()Ljava/util/List;

    move-result-object v3

    .line 1108
    .local v3, "roadElements":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/common/RoadElement;>;"
    if-eqz v3, :cond_2

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v8

    if-nez v8, :cond_3

    :cond_2
    move-wide v4, v6

    .line 1109
    goto :goto_0

    .line 1112
    :cond_3
    const-wide/16 v4, 0x0

    .line 1113
    .local v4, "time":J
    if-eqz p1, :cond_4

    .line 1114
    iput-wide v6, p1, Lcom/navdy/hud/app/maps/here/HereMapUtil$LongContainer;->val:J

    .line 1117
    :cond_4
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_5
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/here/android/mpa/common/RoadElement;

    .line 1118
    .local v2, "roadElement":Lcom/here/android/mpa/common/RoadElement;
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lcom/here/android/mpa/common/RoadElement;->getDefaultSpeed()F

    move-result v7

    const/4 v8, 0x0

    cmpl-float v7, v7, v8

    if-lez v7, :cond_5

    .line 1119
    invoke-virtual {v2}, Lcom/here/android/mpa/common/RoadElement;->getGeometryLength()D

    move-result-wide v0

    .line 1120
    .local v0, "length":D
    invoke-virtual {v2}, Lcom/here/android/mpa/common/RoadElement;->getDefaultSpeed()F

    move-result v7

    float-to-double v8, v7

    div-double v8, v0, v8

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    add-long/2addr v4, v8

    .line 1121
    if-eqz p1, :cond_5

    .line 1122
    iget-wide v8, p1, Lcom/navdy/hud/app/maps/here/HereMapUtil$LongContainer;->val:J

    long-to-double v8, v8

    add-double/2addr v8, v0

    double-to-long v8, v8

    iput-wide v8, p1, Lcom/navdy/hud/app/maps/here/HereMapUtil$LongContainer;->val:J

    goto :goto_1
.end method

.method public static getNextRoadName(Lcom/here/android/mpa/routing/Maneuver;)Ljava/lang/String;
    .locals 4
    .param p0, "maneuver"    # Lcom/here/android/mpa/routing/Maneuver;

    .prologue
    .line 187
    invoke-static {p0}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->isCurrentRoadHighway(Lcom/here/android/mpa/routing/Maneuver;)Z

    move-result v0

    .line 188
    .local v0, "isHighway":Z
    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Maneuver;->getNextRoadNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Maneuver;->getNextRoadName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v1, v2, v3, v0}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->concatText(Ljava/lang/String;Ljava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getRoadName(Lcom/here/android/mpa/common/RoadElement;)Ljava/lang/String;
    .locals 4
    .param p0, "roadElement"    # Lcom/here/android/mpa/common/RoadElement;

    .prologue
    .line 192
    if-nez p0, :cond_0

    .line 193
    const/4 v1, 0x0

    .line 196
    :goto_0
    return-object v1

    .line 195
    :cond_0
    invoke-static {p0}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->isCurrentRoadHighway(Lcom/here/android/mpa/common/RoadElement;)Z

    move-result v0

    .line 196
    .local v0, "isHighway":Z
    invoke-virtual {p0}, Lcom/here/android/mpa/common/RoadElement;->getRouteName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/here/android/mpa/common/RoadElement;->getRoadName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v1, v2, v3, v0}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->concatText(Ljava/lang/String;Ljava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static getRoadName(Lcom/here/android/mpa/routing/Maneuver;)Ljava/lang/String;
    .locals 4
    .param p0, "maneuver"    # Lcom/here/android/mpa/routing/Maneuver;

    .prologue
    .line 182
    invoke-static {p0}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->isCurrentRoadHighway(Lcom/here/android/mpa/routing/Maneuver;)Z

    move-result v0

    .line 183
    .local v0, "isHighway":Z
    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Maneuver;->getRoadNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Maneuver;->getRoadName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v1, v2, v3, v0}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->concatText(Ljava/lang/String;Ljava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getRoadName(Ljava/util/List;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/common/RoadElement;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 162
    .local p0, "roadElements":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/common/RoadElement;>;"
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_2

    .line 163
    :cond_0
    const-string v0, ""

    .line 178
    :cond_1
    :goto_0
    return-object v0

    .line 166
    :cond_2
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/here/android/mpa/common/RoadElement;

    .line 167
    .local v1, "roadElement":Lcom/here/android/mpa/common/RoadElement;
    invoke-virtual {v1}, Lcom/here/android/mpa/common/RoadElement;->getRoadName()Ljava/lang/String;

    move-result-object v0

    .line 168
    .local v0, "name":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 172
    invoke-virtual {v1}, Lcom/here/android/mpa/common/RoadElement;->getRouteName()Ljava/lang/String;

    move-result-object v0

    .line 173
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    goto :goto_0

    .line 178
    .end local v0    # "name":Ljava/lang/String;
    .end local v1    # "roadElement":Lcom/here/android/mpa/common/RoadElement;
    :cond_4
    const-string v0, ""

    goto :goto_0
.end method

.method public static getRouteTtaDate(Lcom/here/android/mpa/routing/Route;)Ljava/util/Date;
    .locals 10
    .param p0, "route"    # Lcom/here/android/mpa/routing/Route;

    .prologue
    const/4 v3, 0x0

    .line 644
    if-nez p0, :cond_1

    .line 663
    :cond_0
    :goto_0
    return-object v3

    .line 648
    :cond_1
    :try_start_0
    sget-object v4, Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;->OPTIMAL:Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    const v5, 0xfffffff

    invoke-virtual {p0, v4, v5}, Lcom/here/android/mpa/routing/Route;->getTta(Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;I)Lcom/here/android/mpa/routing/RouteTta;

    move-result-object v2

    .line 649
    .local v2, "tta":Lcom/here/android/mpa/routing/RouteTta;
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/here/android/mpa/routing/RouteTta;->getDuration()I

    move-result v4

    if-nez v4, :cond_2

    .line 650
    const/4 v2, 0x0

    .line 652
    :cond_2
    if-nez v2, :cond_3

    .line 653
    sget-object v4, Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;->DISABLED:Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    const v5, 0xfffffff

    invoke-virtual {p0, v4, v5}, Lcom/here/android/mpa/routing/Route;->getTta(Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;I)Lcom/here/android/mpa/routing/RouteTta;

    move-result-object v2

    .line 655
    :cond_3
    if-eqz v2, :cond_0

    .line 656
    invoke-virtual {v2}, Lcom/here/android/mpa/routing/RouteTta;->getDuration()I

    move-result v1

    .line 657
    .local v1, "trafficDuration":I
    new-instance v4, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    int-to-long v8, v1

    invoke-virtual {v5, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v8

    add-long/2addr v6, v8

    invoke-direct {v4, v6, v7}, Ljava/util/Date;-><init>(J)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-object v3, v4

    goto :goto_0

    .line 659
    .end local v1    # "trafficDuration":I
    .end local v2    # "tta":Lcom/here/android/mpa/routing/RouteTta;
    :catch_0
    move-exception v0

    .line 660
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v4, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v4, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static declared-synchronized getSavedRouteData(Lcom/navdy/service/library/log/Logger;)Lcom/navdy/hud/app/maps/here/HereMapUtil$SavedRouteData;
    .locals 5
    .param p0, "logger"    # Lcom/navdy/service/library/log/Logger;

    .prologue
    .line 757
    const-class v3, Lcom/navdy/hud/app/maps/here/HereMapUtil;

    monitor-enter v3

    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 758
    invoke-static {p0}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getSavedRouteRequest(Lcom/navdy/service/library/log/Logger;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    move-result-object v1

    .line 759
    .local v1, "request":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->routeAttributes:Ljava/util/List;

    sget-object v4, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;->ROUTE_ATTRIBUTE_GAS:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;

    .line 760
    invoke-interface {v2, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    .line 762
    .local v0, "isGasRoute":Z
    :goto_0
    new-instance v2, Lcom/navdy/hud/app/maps/here/HereMapUtil$SavedRouteData;

    invoke-direct {v2, v1, v0}, Lcom/navdy/hud/app/maps/here/HereMapUtil$SavedRouteData;-><init>(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v3

    return-object v2

    .line 760
    .end local v0    # "isGasRoute":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 757
    .end local v1    # "request":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method private static getSavedRouteRequest(Lcom/navdy/service/library/log/Logger;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .locals 18
    .param p0, "logger"    # Lcom/navdy/service/library/log/Logger;

    .prologue
    .line 766
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 767
    const-string v14, "getSavedRouteRequest"

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 768
    const/4 v4, 0x0

    .line 770
    .local v4, "file":Ljava/io/FileInputStream;
    const/4 v14, 0x0

    :try_start_0
    sput-object v14, Lcom/navdy/hud/app/maps/here/HereMapUtil;->lastKnownDeviceId:Ljava/lang/String;

    .line 771
    invoke-static {}, Lcom/navdy/hud/app/storage/PathManager;->getInstance()Lcom/navdy/hud/app/storage/PathManager;

    move-result-object v14

    invoke-virtual {v14}, Lcom/navdy/hud/app/storage/PathManager;->getActiveRouteInfoDir()Ljava/lang/String;

    move-result-object v10

    .line 774
    .local v10, "path":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    sget-object v15, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "gasrouterequest.bin"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v3, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 775
    .local v3, "f":Ljava/io/File;
    const/4 v6, 0x0

    .line 776
    .local v6, "gasRouteExists":Z
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v14

    if-eqz v14, :cond_0

    .line 778
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->isRouteFileStale(Ljava/io/File;)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 779
    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    move-result-wide v8

    .line 780
    .local v8, "modified":J
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v14

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v12

    .line 781
    .local v12, "ret":Z
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "getSavedRouteRequest: gas route is stale file-time ="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " now="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " deleted="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 788
    .end local v8    # "modified":J
    .end local v12    # "ret":Z
    :cond_0
    :goto_0
    if-nez v6, :cond_4

    .line 789
    const-string v14, "getSavedRouteRequest: no saved gas route"

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 790
    new-instance v3, Ljava/io/File;

    .end local v3    # "f":Ljava/io/File;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    sget-object v15, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "routerequest.bin"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v3, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 791
    .restart local v3    # "f":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v14

    if-nez v14, :cond_2

    .line 792
    const-string v14, "getSavedRouteRequest: no saved route"

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 793
    const/4 v11, 0x0

    .line 836
    .end local v3    # "f":Ljava/io/File;
    .end local v6    # "gasRouteExists":Z
    .end local v10    # "path":Ljava/lang/String;
    :goto_1
    return-object v11

    .line 783
    .restart local v3    # "f":Ljava/io/File;
    .restart local v6    # "gasRouteExists":Z
    .restart local v10    # "path":Ljava/lang/String;
    :cond_1
    const/4 v6, 0x1

    .line 784
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "getSavedRouteRequest: gas route is good, file-time ="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " now="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 831
    .end local v3    # "f":Ljava/io/File;
    .end local v6    # "gasRouteExists":Z
    .end local v10    # "path":Ljava/lang/String;
    :catch_0
    move-exception v13

    .line 832
    .local v13, "t":Ljava/lang/Throwable;
    :goto_2
    sget-object v14, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v14, v13}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 833
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 834
    const/4 v14, 0x1

    move-object/from16 v0, p0

    invoke-static {v0, v14}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->removeRouteInfo(Lcom/navdy/service/library/log/Logger;Z)V

    .line 835
    const/4 v14, 0x0

    sput-object v14, Lcom/navdy/hud/app/maps/here/HereMapUtil;->lastKnownDeviceId:Ljava/lang/String;

    .line 836
    const/4 v11, 0x0

    goto :goto_1

    .line 795
    .end local v13    # "t":Ljava/lang/Throwable;
    .restart local v3    # "f":Ljava/io/File;
    .restart local v6    # "gasRouteExists":Z
    .restart local v10    # "path":Ljava/lang/String;
    :cond_2
    :try_start_1
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->isRouteFileStale(Ljava/io/File;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 796
    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    move-result-wide v8

    .line 797
    .restart local v8    # "modified":J
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v14

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v12

    .line 798
    .restart local v12    # "ret":Z
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "getSavedRouteRequest: route is stale file-time ="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " now="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " deleted="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 799
    const/4 v11, 0x0

    goto/16 :goto_1

    .line 801
    .end local v8    # "modified":J
    .end local v12    # "ret":Z
    :cond_3
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "getSavedRouteRequest: route is good file-time ="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " now="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 806
    :cond_4
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 807
    .end local v4    # "file":Ljava/io/FileInputStream;
    .local v5, "file":Ljava/io/FileInputStream;
    :try_start_2
    sget-object v14, Lcom/navdy/hud/app/maps/here/HereMapUtil;->wire:Lcom/squareup/wire/Wire;

    const-class v15, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    invoke-virtual {v14, v5, v15}, Lcom/squareup/wire/Wire;->parseFrom(Ljava/io/InputStream;Ljava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v11

    check-cast v11, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .line 809
    .local v11, "request":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    new-instance v14, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    invoke-direct {v14, v11}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;-><init>(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v15

    invoke-virtual {v15}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->requestId(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v14

    invoke-virtual {v14}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->build()Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    move-result-object v11

    .line 811
    invoke-static {v5}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    .line 812
    const/4 v4, 0x0

    .line 815
    .end local v5    # "file":Ljava/io/FileInputStream;
    .restart local v4    # "file":Ljava/io/FileInputStream;
    :try_start_3
    new-instance v3, Ljava/io/File;

    .end local v3    # "f":Ljava/io/File;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    sget-object v15, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "routerequest.device"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v3, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 816
    .restart local v3    # "f":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v14

    if-nez v14, :cond_5

    .line 817
    const-string v14, "getSavedRouteRequest: no saved device id"

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 820
    :cond_5
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/navdy/service/library/util/IOUtils;->convertFileToString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 821
    .local v2, "deviceId":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v14

    invoke-virtual {v14}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v7

    .line 823
    .local v7, "id":Lcom/navdy/service/library/device/NavdyDeviceId;
    if-eqz v7, :cond_6

    new-instance v14, Lcom/navdy/service/library/device/NavdyDeviceId;

    invoke-direct {v14, v2}, Lcom/navdy/service/library/device/NavdyDeviceId;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v14}, Lcom/navdy/service/library/device/NavdyDeviceId;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_6

    .line 824
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "getSavedRouteRequest: device id is different now["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "] stored["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "]"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 825
    const/4 v14, 0x1

    move-object/from16 v0, p0

    invoke-static {v0, v14}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->removeRouteInfo(Lcom/navdy/service/library/log/Logger;Z)V

    .line 826
    const/4 v11, 0x0

    goto/16 :goto_1

    .line 828
    :cond_6
    sput-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->lastKnownDeviceId:Ljava/lang/String;

    .line 829
    const-string v14, "getSavedRouteRequest: returning route"

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_1

    .line 831
    .end local v2    # "deviceId":Ljava/lang/String;
    .end local v4    # "file":Ljava/io/FileInputStream;
    .end local v7    # "id":Lcom/navdy/service/library/device/NavdyDeviceId;
    .end local v11    # "request":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .restart local v5    # "file":Ljava/io/FileInputStream;
    :catch_1
    move-exception v13

    move-object v4, v5

    .end local v5    # "file":Ljava/io/FileInputStream;
    .restart local v4    # "file":Ljava/io/FileInputStream;
    goto/16 :goto_2
.end method

.method public static getSignpostText(Lcom/here/android/mpa/routing/Signpost;Lcom/here/android/mpa/routing/Maneuver;Lcom/navdy/hud/app/maps/here/HereMapUtil$SignPostInfo;Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 14
    .param p0, "signpost"    # Lcom/here/android/mpa/routing/Signpost;
    .param p1, "maneuver"    # Lcom/here/android/mpa/routing/Maneuver;
    .param p2, "info"    # Lcom/navdy/hud/app/maps/here/HereMapUtil$SignPostInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/here/android/mpa/routing/Signpost;",
            "Lcom/here/android/mpa/routing/Maneuver;",
            "Lcom/navdy/hud/app/maps/here/HereMapUtil$SignPostInfo;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 928
    .local p3, "parsedSignpost":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p0, :cond_12

    .line 929
    sget-object v11, Lcom/navdy/hud/app/maps/here/HereMapUtil;->signPostBuilder:Ljava/lang/StringBuilder;

    monitor-enter v11

    .line 930
    :try_start_0
    sget-object v10, Lcom/navdy/hud/app/maps/here/HereMapUtil;->signPostBuilder:Ljava/lang/StringBuilder;

    const/4 v12, 0x0

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 931
    sget-object v10, Lcom/navdy/hud/app/maps/here/HereMapUtil;->signPostBuilder2:Ljava/lang/StringBuilder;

    const/4 v12, 0x0

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 932
    sget-object v10, Lcom/navdy/hud/app/maps/here/HereMapUtil;->signPostSet:Ljava/util/HashSet;

    invoke-virtual {v10}, Ljava/util/HashSet;->clear()V

    .line 933
    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Signpost;->getExitNumber()Ljava/lang/String;

    move-result-object v3

    .line 934
    .local v3, "exitNumber":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Signpost;->getExitText()Ljava/lang/String;

    move-result-object v4

    .line 935
    .local v4, "exitText":Ljava/lang/String;
    const/4 v5, 0x0

    .line 936
    .local v5, "hasExitNumber":Z
    const/4 v7, 0x0

    .line 938
    .local v7, "leaveHighway":Z
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 939
    invoke-virtual {p1}, Lcom/here/android/mpa/routing/Maneuver;->getAction()Lcom/here/android/mpa/routing/Maneuver$Action;

    move-result-object v10

    sget-object v12, Lcom/here/android/mpa/routing/Maneuver$Action;->LEAVE_HIGHWAY:Lcom/here/android/mpa/routing/Maneuver$Action;

    if-eq v10, v12, :cond_e

    .line 940
    sget-object v10, Lcom/navdy/hud/app/maps/here/HereMapUtil;->signPostBuilder:Ljava/lang/StringBuilder;

    sget-object v12, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->EXIT_NUMBER:Ljava/lang/String;

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 941
    sget-object v10, Lcom/navdy/hud/app/maps/here/HereMapUtil;->signPostBuilder:Ljava/lang/StringBuilder;

    const-string v12, " "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 942
    const/4 v5, 0x0

    .line 943
    const/4 v7, 0x1

    .line 947
    :goto_0
    sget-object v10, Lcom/navdy/hud/app/maps/here/HereMapUtil;->signPostBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 948
    if-eqz p3, :cond_0

    .line 949
    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 953
    :cond_0
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 954
    sget-object v10, Lcom/navdy/hud/app/maps/here/HereMapUtil;->signPostBuilder:Ljava/lang/StringBuilder;

    invoke-static {v10}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->needSeparator(Ljava/lang/StringBuilder;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 955
    sget-object v10, Lcom/navdy/hud/app/maps/here/HereMapUtil;->signPostBuilder:Ljava/lang/StringBuilder;

    const-string v12, " "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 957
    :cond_1
    sget-object v10, Lcom/navdy/hud/app/maps/here/HereMapUtil;->signPostBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 958
    if-eqz p3, :cond_2

    .line 959
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 963
    :cond_2
    if-eqz v5, :cond_3

    .line 964
    sget-object v10, Lcom/navdy/hud/app/maps/here/HereMapUtil;->signPostBuilder:Ljava/lang/StringBuilder;

    const-string v12, " "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 967
    :cond_3
    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Signpost;->getExitDirections()Ljava/util/List;

    move-result-object v2

    .line 969
    .local v2, "exitDirections":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Signpost$LocalizedLabel;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_4
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_f

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/here/android/mpa/routing/Signpost$LocalizedLabel;

    .line 970
    .local v6, "label":Lcom/here/android/mpa/routing/Signpost$LocalizedLabel;
    invoke-virtual {v6}, Lcom/here/android/mpa/routing/Signpost$LocalizedLabel;->getLanguage()Ljava/lang/String;

    move-result-object v12

    sget-object v13, Lcom/navdy/hud/app/maps/here/HereMapUtil;->TBT_ISO3_LANG_CODE:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 971
    invoke-virtual {v6}, Lcom/here/android/mpa/routing/Signpost$LocalizedLabel;->getRouteName()Ljava/lang/String;

    move-result-object v8

    .line 972
    .local v8, "routeName":Ljava/lang/String;
    invoke-virtual {v6}, Lcom/here/android/mpa/routing/Signpost$LocalizedLabel;->getRouteDirection()Ljava/lang/String;

    move-result-object v1

    .line 973
    .local v1, "direction":Ljava/lang/String;
    invoke-virtual {v6}, Lcom/here/android/mpa/routing/Signpost$LocalizedLabel;->getText()Ljava/lang/String;

    move-result-object v9

    .line 975
    .local v9, "text":Ljava/lang/String;
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_8

    .line 976
    if-eqz p2, :cond_5

    .line 977
    const/4 v12, 0x1

    move-object/from16 v0, p2

    iput-boolean v12, v0, Lcom/navdy/hud/app/maps/here/HereMapUtil$SignPostInfo;->hasNumberInSignPost:Z

    .line 979
    :cond_5
    if-eqz v7, :cond_6

    .line 980
    const/4 v7, 0x0

    .line 981
    sget-object v12, Lcom/navdy/hud/app/maps/here/HereMapUtil;->signPostBuilder:Ljava/lang/StringBuilder;

    invoke-static {v12}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->needSeparator(Ljava/lang/StringBuilder;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 982
    sget-object v12, Lcom/navdy/hud/app/maps/here/HereMapUtil;->signPostBuilder:Ljava/lang/StringBuilder;

    const-string v13, "/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 986
    :cond_6
    sget-object v12, Lcom/navdy/hud/app/maps/here/HereMapUtil;->signPostBuilder:Ljava/lang/StringBuilder;

    invoke-static {v12}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->needSeparator(Ljava/lang/StringBuilder;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 987
    sget-object v12, Lcom/navdy/hud/app/maps/here/HereMapUtil;->signPostBuilder:Ljava/lang/StringBuilder;

    const-string v13, "/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 989
    :cond_7
    sget-object v12, Lcom/navdy/hud/app/maps/here/HereMapUtil;->signPostBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 992
    :cond_8
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_b

    .line 993
    if-eqz p2, :cond_9

    move-object/from16 v0, p2

    iget-boolean v12, v0, Lcom/navdy/hud/app/maps/here/HereMapUtil$SignPostInfo;->hasNumberInSignPost:Z

    if-eqz v12, :cond_9

    .line 994
    sget-object v12, Lcom/navdy/hud/app/maps/here/HereMapUtil;->signPostBuilder:Ljava/lang/StringBuilder;

    const-string v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 996
    :cond_9
    if-eqz p3, :cond_a

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_a

    .line 997
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 999
    :cond_a
    sget-object v12, Lcom/navdy/hud/app/maps/here/HereMapUtil;->signPostBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1002
    :cond_b
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_4

    .line 1003
    sget-object v12, Lcom/navdy/hud/app/maps/here/HereMapUtil;->signPostSet:Ljava/util/HashSet;

    invoke-virtual {v12, v9}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_4

    .line 1007
    if-eqz p2, :cond_c

    .line 1008
    const/4 v12, 0x1

    move-object/from16 v0, p2

    iput-boolean v12, v0, Lcom/navdy/hud/app/maps/here/HereMapUtil$SignPostInfo;->hasNameInSignPost:Z

    .line 1011
    :cond_c
    sget-object v12, Lcom/navdy/hud/app/maps/here/HereMapUtil;->signPostBuilder2:Ljava/lang/StringBuilder;

    invoke-static {v12}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->needSeparator(Ljava/lang/StringBuilder;)Z

    move-result v12

    if-eqz v12, :cond_d

    .line 1012
    sget-object v12, Lcom/navdy/hud/app/maps/here/HereMapUtil;->signPostBuilder2:Ljava/lang/StringBuilder;

    const-string v13, "/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1014
    :cond_d
    sget-object v12, Lcom/navdy/hud/app/maps/here/HereMapUtil;->signPostBuilder2:Ljava/lang/StringBuilder;

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1015
    sget-object v12, Lcom/navdy/hud/app/maps/here/HereMapUtil;->signPostSet:Ljava/util/HashSet;

    invoke-virtual {v12, v9}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 1027
    .end local v1    # "direction":Ljava/lang/String;
    .end local v2    # "exitDirections":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Signpost$LocalizedLabel;>;"
    .end local v3    # "exitNumber":Ljava/lang/String;
    .end local v4    # "exitText":Ljava/lang/String;
    .end local v5    # "hasExitNumber":Z
    .end local v6    # "label":Lcom/here/android/mpa/routing/Signpost$LocalizedLabel;
    .end local v7    # "leaveHighway":Z
    .end local v8    # "routeName":Ljava/lang/String;
    .end local v9    # "text":Ljava/lang/String;
    :catchall_0
    move-exception v10

    monitor-exit v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v10

    .line 945
    .restart local v3    # "exitNumber":Ljava/lang/String;
    .restart local v4    # "exitText":Ljava/lang/String;
    .restart local v5    # "hasExitNumber":Z
    .restart local v7    # "leaveHighway":Z
    :cond_e
    const/4 v5, 0x1

    goto/16 :goto_0

    .line 1020
    .restart local v2    # "exitDirections":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Signpost$LocalizedLabel;>;"
    :cond_f
    :try_start_1
    sget-object v10, Lcom/navdy/hud/app/maps/here/HereMapUtil;->signPostBuilder2:Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->length()I

    move-result v10

    if-lez v10, :cond_11

    .line 1021
    sget-object v10, Lcom/navdy/hud/app/maps/here/HereMapUtil;->signPostBuilder:Ljava/lang/StringBuilder;

    invoke-static {v10}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->needSeparator(Ljava/lang/StringBuilder;)Z

    move-result v10

    if-eqz v10, :cond_10

    .line 1022
    sget-object v10, Lcom/navdy/hud/app/maps/here/HereMapUtil;->signPostBuilder:Ljava/lang/StringBuilder;

    const-string v12, "/"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1024
    :cond_10
    sget-object v10, Lcom/navdy/hud/app/maps/here/HereMapUtil;->signPostBuilder:Ljava/lang/StringBuilder;

    sget-object v12, Lcom/navdy/hud/app/maps/here/HereMapUtil;->signPostBuilder2:Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1026
    :cond_11
    sget-object v10, Lcom/navdy/hud/app/maps/here/HereMapUtil;->signPostBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    monitor-exit v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1029
    .end local v2    # "exitDirections":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Signpost$LocalizedLabel;>;"
    .end local v3    # "exitNumber":Ljava/lang/String;
    .end local v4    # "exitText":Ljava/lang/String;
    .end local v5    # "hasExitNumber":Z
    .end local v7    # "leaveHighway":Z
    :goto_2
    return-object v10

    :cond_12
    const/4 v10, 0x0

    goto :goto_2
.end method

.method public static hasSameSignpostAndToRoad(Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;)Z
    .locals 9
    .param p0, "first"    # Lcom/here/android/mpa/routing/Maneuver;
    .param p1, "second"    # Lcom/here/android/mpa/routing/Maneuver;

    .prologue
    const/4 v4, 0x1

    const/4 v8, 0x0

    const/4 v5, 0x0

    .line 904
    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    :cond_0
    move v4, v5

    .line 924
    :cond_1
    :goto_0
    return v4

    .line 909
    :cond_2
    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Maneuver;->getNextRoadNumber()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Maneuver;->getNextRoadName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v4, v5}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->concatText(Ljava/lang/String;Ljava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v2

    .line 910
    .local v2, "toRoad1":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/here/android/mpa/routing/Maneuver;->getNextRoadNumber()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lcom/here/android/mpa/routing/Maneuver;->getNextRoadName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v4, v5}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->concatText(Ljava/lang/String;Ljava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v3

    .line 912
    .local v3, "toRoad2":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    :cond_3
    move v4, v5

    .line 913
    goto :goto_0

    .line 917
    :cond_4
    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Maneuver;->getSignpost()Lcom/here/android/mpa/routing/Signpost;

    move-result-object v6

    invoke-static {v6, p0, v8, v8}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getSignpostText(Lcom/here/android/mpa/routing/Signpost;Lcom/here/android/mpa/routing/Maneuver;Lcom/navdy/hud/app/maps/here/HereMapUtil$SignPostInfo;Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v0

    .line 918
    .local v0, "signPostText1":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/here/android/mpa/routing/Maneuver;->getSignpost()Lcom/here/android/mpa/routing/Signpost;

    move-result-object v6

    invoke-static {v6, p1, v8, v8}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getSignpostText(Lcom/here/android/mpa/routing/Signpost;Lcom/here/android/mpa/routing/Maneuver;Lcom/navdy/hud/app/maps/here/HereMapUtil$SignPostInfo;Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v1

    .line 920
    .local v1, "signPostText2":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_5

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_5

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    :cond_5
    move v4, v5

    .line 921
    goto :goto_0
.end method

.method public static isCoordinateEqual(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoCoordinate;)Z
    .locals 9
    .param p0, "g1"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p1, "g2"    # Lcom/here/android/mpa/common/GeoCoordinate;

    .prologue
    .line 857
    invoke-virtual {p0}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v0

    .line 858
    .local v0, "lat1":D
    invoke-virtual {p0}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v4

    .line 859
    .local v4, "lng1":D
    invoke-virtual {p1}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v2

    .line 860
    .local v2, "lat2":D
    invoke-virtual {p1}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v6

    .line 861
    .local v6, "lng2":D
    cmpl-double v8, v0, v2

    if-nez v8, :cond_0

    cmpl-double v8, v4, v6

    if-nez v8, :cond_0

    .line 862
    const/4 v8, 0x1

    .line 864
    :goto_0
    return v8

    :cond_0
    const/4 v8, 0x0

    goto :goto_0
.end method

.method public static isCurrentRoadHighway(Lcom/here/android/mpa/common/RoadElement;)Z
    .locals 3
    .param p0, "roadElement"    # Lcom/here/android/mpa/common/RoadElement;

    .prologue
    const/4 v1, 0x0

    .line 209
    if-nez p0, :cond_1

    .line 216
    :cond_0
    :goto_0
    return v1

    .line 212
    :cond_1
    invoke-virtual {p0}, Lcom/here/android/mpa/common/RoadElement;->getAttributes()Ljava/util/EnumSet;

    move-result-object v0

    .line 213
    .local v0, "attributes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/here/android/mpa/common/RoadElement$Attribute;>;"
    sget-object v2, Lcom/here/android/mpa/common/RoadElement$Attribute;->HIGHWAY:Lcom/here/android/mpa/common/RoadElement$Attribute;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 214
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static isCurrentRoadHighway(Lcom/here/android/mpa/routing/Maneuver;)Z
    .locals 3
    .param p0, "maneuver"    # Lcom/here/android/mpa/routing/Maneuver;

    .prologue
    const/4 v1, 0x0

    .line 221
    if-nez p0, :cond_1

    .line 231
    :cond_0
    :goto_0
    return v1

    .line 224
    :cond_1
    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Maneuver;->getAction()Lcom/here/android/mpa/routing/Maneuver$Action;

    move-result-object v2

    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->isManeuverActionHighway(Lcom/here/android/mpa/routing/Maneuver$Action;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 225
    const/4 v1, 0x1

    goto :goto_0

    .line 227
    :cond_2
    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Maneuver;->getRoadElements()Ljava/util/List;

    move-result-object v0

    .line 228
    .local v0, "elements":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/common/RoadElement;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-eqz v2, :cond_0

    .line 231
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/here/android/mpa/common/RoadElement;

    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->isCurrentRoadHighway(Lcom/here/android/mpa/common/RoadElement;)Z

    move-result v1

    goto :goto_0
.end method

.method public static isGeoCoordinateEquals(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoCoordinate;)Z
    .locals 4
    .param p0, "g1"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p1, "g2"    # Lcom/here/android/mpa/common/GeoCoordinate;

    .prologue
    .line 1296
    invoke-virtual {p0}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p1}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 1297
    invoke-virtual {p0}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v0

    invoke-virtual {p1}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 1298
    const/4 v0, 0x1

    .line 1300
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isHighwayAction(Lcom/here/android/mpa/routing/Maneuver;)Z
    .locals 4
    .param p0, "maneuver"    # Lcom/here/android/mpa/routing/Maneuver;

    .prologue
    const/4 v1, 0x0

    .line 1050
    if-nez p0, :cond_1

    .line 1066
    :cond_0
    :goto_0
    return v1

    .line 1054
    :cond_1
    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Maneuver;->getAction()Lcom/here/android/mpa/routing/Maneuver$Action;

    move-result-object v0

    .line 1055
    .local v0, "action":Lcom/here/android/mpa/routing/Maneuver$Action;
    if-eqz v0, :cond_0

    .line 1059
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil$2;->$SwitchMap$com$here$android$mpa$routing$Maneuver$Action:[I

    invoke-virtual {v0}, Lcom/here/android/mpa/routing/Maneuver$Action;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 1063
    :pswitch_0
    const/4 v1, 0x1

    goto :goto_0

    .line 1059
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static isInTunnel(Lcom/here/android/mpa/common/RoadElement;)Z
    .locals 3
    .param p0, "roadElement"    # Lcom/here/android/mpa/common/RoadElement;

    .prologue
    .line 1131
    if-eqz p0, :cond_0

    .line 1132
    invoke-virtual {p0}, Lcom/here/android/mpa/common/RoadElement;->getAttributes()Ljava/util/EnumSet;

    move-result-object v0

    .line 1133
    .local v0, "attributes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/here/android/mpa/common/RoadElement$Attribute;>;"
    if-eqz v0, :cond_0

    .line 1134
    sget-object v2, Lcom/here/android/mpa/common/RoadElement$Attribute;->TUNNEL:Lcom/here/android/mpa/common/RoadElement$Attribute;

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    .line 1139
    .end local v0    # "attributes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/here/android/mpa/common/RoadElement$Attribute;>;"
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isManeuverActionHighway(Lcom/here/android/mpa/routing/Maneuver$Action;)Z
    .locals 3
    .param p0, "action"    # Lcom/here/android/mpa/routing/Maneuver$Action;

    .prologue
    const/4 v0, 0x0

    .line 235
    if-nez p0, :cond_0

    .line 247
    :goto_0
    return v0

    .line 238
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapUtil$2;->$SwitchMap$com$here$android$mpa$routing$Maneuver$Action:[I

    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Maneuver$Action;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 244
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 238
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static isManeuverShort(Lcom/here/android/mpa/routing/Maneuver;)Z
    .locals 2
    .param p0, "maneuver"    # Lcom/here/android/mpa/routing/Maneuver;

    .prologue
    .line 252
    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Maneuver;->getDistanceToNextManeuver()I

    move-result v0

    .line 253
    .local v0, "distance":I
    const/16 v1, 0x14

    if-gt v0, v1, :cond_0

    .line 257
    const/4 v1, 0x1

    .line 262
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static isRouteFileStale(Ljava/io/File;)Z
    .locals 8
    .param p0, "f"    # Ljava/io/File;

    .prologue
    .line 1317
    invoke-virtual {p0}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    .line 1318
    .local v2, "modified":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 1319
    .local v4, "now":J
    sub-long v0, v4, v2

    .line 1320
    .local v0, "l":J
    sget-wide v6, Lcom/navdy/hud/app/maps/here/HereMapUtil;->STALE_ROUTE_TIME:J

    cmp-long v6, v0, v6

    if-gez v6, :cond_0

    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-gez v6, :cond_1

    .line 1321
    :cond_0
    const/4 v6, 0x1

    .line 1323
    :goto_0
    return v6

    :cond_1
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public static isSavedRoute(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)Z
    .locals 2
    .param p0, "request"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .prologue
    .line 1286
    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->routeAttributes:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->routeAttributes:Ljava/util/List;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;->ROUTE_ATTRIBUTE_SAVED_ROUTE:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;

    .line 1288
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1289
    const/4 v0, 0x1

    .line 1291
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isStartManeuver(Lcom/here/android/mpa/routing/Maneuver;)Z
    .locals 3
    .param p0, "maneuver"    # Lcom/here/android/mpa/routing/Maneuver;

    .prologue
    const/4 v0, 0x0

    .line 267
    if-nez p0, :cond_1

    .line 273
    :cond_0
    :goto_0
    return v0

    .line 270
    :cond_1
    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Maneuver;->getIcon()Lcom/here/android/mpa/routing/Maneuver$Icon;

    move-result-object v1

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Icon;->START:Lcom/here/android/mpa/routing/Maneuver$Icon;

    if-ne v1, v2, :cond_0

    .line 271
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isValidEtaDate(Ljava/util/Date;)Z
    .locals 8
    .param p0, "etaDate"    # Ljava/util/Date;

    .prologue
    const/4 v3, 0x0

    .line 623
    if-nez p0, :cond_0

    .line 638
    :goto_0
    return v3

    .line 626
    :cond_0
    invoke-virtual {p0}, Ljava/util/Date;->getYear()I

    move-result v2

    .line 627
    .local v2, "year":I
    if-eqz p0, :cond_1

    const/16 v4, 0x47

    if-gt v2, v4, :cond_2

    .line 628
    :cond_1
    sget-object v4, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "invalid eta date["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 631
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    sub-long v0, v4, v6

    .line 632
    .local v0, "timeDiff":J
    const-wide/32 v4, 0x57e40

    cmp-long v4, v0, v4

    if-ltz v4, :cond_3

    .line 633
    sget-object v4, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "invalid eta timediff["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 635
    :cond_3
    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-lez v3, :cond_4

    .line 636
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "eta is behind timediff["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 638
    :cond_4
    const/4 v3, 0x1

    goto :goto_0
.end method

.method static isValidNavigationState(Lcom/navdy/service/library/events/navigation/NavigationSessionState;)Z
    .locals 1
    .param p0, "navigationSessionState"    # Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .prologue
    .line 394
    if-eqz p0, :cond_1

    sget-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_STARTED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_PAUSED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_STOPPED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    if-ne p0, v0, :cond_1

    .line 398
    :cond_0
    const/4 v0, 0x1

    .line 400
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static loadImage(Lcom/here/android/mpa/common/Image;IILcom/navdy/hud/app/maps/here/HereMapUtil$IImageLoadCallback;)V
    .locals 3
    .param p0, "image"    # Lcom/here/android/mpa/common/Image;
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "callback"    # Lcom/navdy/hud/app/maps/here/HereMapUtil$IImageLoadCallback;

    .prologue
    .line 1267
    if-eqz p0, :cond_0

    if-eqz p3, :cond_0

    if-lez p1, :cond_0

    if-gtz p2, :cond_1

    .line 1268
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1271
    :cond_1
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereMapUtil$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/navdy/hud/app/maps/here/HereMapUtil$1;-><init>(Lcom/here/android/mpa/common/Image;IILcom/navdy/hud/app/maps/here/HereMapUtil$IImageLoadCallback;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 1283
    return-void
.end method

.method public static needSeparator(Ljava/lang/StringBuilder;)Z
    .locals 4
    .param p0, "builder"    # Ljava/lang/StringBuilder;

    .prologue
    const/4 v2, 0x0

    .line 1034
    if-nez p0, :cond_1

    .line 1045
    :cond_0
    :goto_0
    return v2

    .line 1037
    :cond_1
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    .line 1038
    .local v1, "len":I
    if-eqz v1, :cond_0

    .line 1041
    add-int/lit8 v3, v1, -0x1

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    .line 1042
    .local v0, "ch":C
    const/16 v3, 0x2f

    if-eq v0, v3, :cond_0

    const/16 v3, 0x20

    if-eq v0, v3, :cond_0

    .line 1045
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static parseStreetAddress(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "streetAddress"    # Ljava/lang/String;

    .prologue
    .line 405
    if-nez p0, :cond_0

    .line 406
    const/4 v1, 0x0

    .line 415
    :goto_0
    return-object v1

    .line 408
    :cond_0
    const-string v2, ","

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 409
    .local v0, "index":I
    const/4 v1, 0x0

    .line 410
    .local v1, "str":Ljava/lang/String;
    if-ltz v0, :cond_1

    .line 411
    const/4 v2, 0x0

    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 413
    :cond_1
    move-object v1, p0

    goto :goto_0
.end method

.method public static printManeuverDetails(Lcom/here/android/mpa/routing/Maneuver;Ljava/lang/String;)V
    .locals 3
    .param p0, "maneuver"    # Lcom/here/android/mpa/routing/Maneuver;
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 388
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " turn["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Maneuver;->getTurn()Lcom/here/android/mpa/routing/Maneuver$Turn;

    move-result-object v2

    invoke-virtual {v2}, Lcom/here/android/mpa/routing/Maneuver$Turn;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] to["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Maneuver;->getNextRoadNumber()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Maneuver;->getNextRoadName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] distance["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 389
    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Maneuver;->getDistanceToNextManeuver()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " meters] action["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Maneuver;->getAction()Lcom/here/android/mpa/routing/Maneuver$Action;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] current["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Maneuver;->getRoadNumber()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 390
    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Maneuver;->getRoadName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] distanceFromStart["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Maneuver;->getDistanceFromStart()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " meters]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 388
    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 391
    return-void
.end method

.method public static printRouteDetails(Lcom/here/android/mpa/routing/Route;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/StringBuilder;)V
    .locals 1
    .param p0, "route"    # Lcom/here/android/mpa/routing/Route;
    .param p1, "streetAddress"    # Ljava/lang/String;
    .param p2, "request"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .param p3, "buffer"    # Ljava/lang/StringBuilder;

    .prologue
    .line 278
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->printRouteDetails(Lcom/here/android/mpa/routing/Route;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/StringBuilder;Lcom/here/android/mpa/routing/Maneuver;)V

    .line 279
    return-void
.end method

.method public static printRouteDetails(Lcom/here/android/mpa/routing/Route;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/StringBuilder;Lcom/here/android/mpa/routing/Maneuver;)V
    .locals 35
    .param p0, "route"    # Lcom/here/android/mpa/routing/Route;
    .param p1, "streetAddress"    # Ljava/lang/String;
    .param p2, "request"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .param p3, "buffer"    # Ljava/lang/StringBuilder;
    .param p4, "printFromManeuver"    # Lcom/here/android/mpa/routing/Maneuver;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 282
    if-nez p0, :cond_1

    .line 385
    :cond_0
    :goto_0
    return-void

    .line 285
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v20

    .line 286
    .local v20, "l1":J
    sget-object v3, Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;->OPTIMAL:Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    const v4, 0xfffffff

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Lcom/here/android/mpa/routing/Route;->getTta(Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;I)Lcom/here/android/mpa/routing/RouteTta;

    move-result-object v30

    .line 287
    .local v30, "routeTta":Lcom/here/android/mpa/routing/RouteTta;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[Route] start["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/here/android/mpa/routing/Route;->getStart()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] end["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/here/android/mpa/routing/Route;->getDestination()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] length["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/here/android/mpa/routing/Route;->getLength()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " meters] tta["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v30 .. v30}, Lcom/here/android/mpa/routing/RouteTta;->getDuration()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " seconds]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    .line 288
    .local v29, "routeInfo":Ljava/lang/String;
    if-nez p3, :cond_7

    .line 289
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v0, v29

    invoke-virtual {v3, v0}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 293
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/here/android/mpa/routing/Route;->getManeuvers()Ljava/util/List;

    move-result-object v27

    .line 295
    .local v27, "maneuvers":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    if-eqz p4, :cond_2

    .line 296
    move-object/from16 v0, v27

    move-object/from16 v1, p4

    invoke-static {v0, v1}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->findManeuverIndex(Ljava/util/List;Lcom/here/android/mpa/routing/Maneuver;)I

    move-result v15

    .line 298
    .local v15, "fromManeuverIndex":I
    if-ltz v15, :cond_0

    .line 301
    invoke-interface/range {v27 .. v27}, Ljava/util/List;->size()I

    move-result v3

    move-object/from16 v0, v27

    invoke-interface {v0, v15, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v27

    .line 304
    .end local v15    # "fromManeuverIndex":I
    :cond_2
    invoke-interface/range {v27 .. v27}, Ljava/util/List;->size()I

    move-result v25

    .line 305
    .local v25, "len":I
    const/4 v7, 0x0

    .line 306
    .local v7, "previous":Lcom/here/android/mpa/routing/Maneuver;
    const/4 v2, 0x0

    .line 307
    .local v2, "current":Lcom/here/android/mpa/routing/Maneuver;
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_2
    move/from16 v0, v16

    move/from16 v1, v25

    if-ge v0, v1, :cond_f

    .line 308
    if-eqz v2, :cond_3

    .line 309
    move-object v7, v2

    .line 311
    :cond_3
    move-object/from16 v0, v27

    move/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "current":Lcom/here/android/mpa/routing/Maneuver;
    check-cast v2, Lcom/here/android/mpa/routing/Maneuver;

    .line 312
    .restart local v2    # "current":Lcom/here/android/mpa/routing/Maneuver;
    const/4 v9, 0x0

    .line 313
    .local v9, "after":Lcom/here/android/mpa/routing/Maneuver;
    add-int/lit8 v3, v25, -0x1

    move/from16 v0, v16

    if-ge v0, v3, :cond_4

    .line 314
    add-int/lit8 v3, v16, 0x1

    move-object/from16 v0, v27

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "after":Lcom/here/android/mpa/routing/Maneuver;
    check-cast v9, Lcom/here/android/mpa/routing/Maneuver;

    .line 316
    .restart local v9    # "after":Lcom/here/android/mpa/routing/Maneuver;
    :cond_4
    const-string v18, ""

    .line 317
    .local v18, "iconStr":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/here/android/mpa/routing/Maneuver;->getIcon()Lcom/here/android/mpa/routing/Maneuver$Icon;

    move-result-object v17

    .line 318
    .local v17, "icon":Lcom/here/android/mpa/routing/Maneuver$Icon;
    if-eqz v17, :cond_5

    .line 319
    invoke-virtual/range {v17 .. v17}, Lcom/here/android/mpa/routing/Maneuver$Icon;->name()Ljava/lang/String;

    move-result-object v18

    .line 321
    :cond_5
    const-string v13, ""

    .line 322
    .local v13, "directionStr":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/here/android/mpa/routing/Maneuver;->getTrafficDirection()Lcom/here/android/mpa/routing/Maneuver$TrafficDirection;

    move-result-object v34

    .line 323
    .local v34, "trafficDirection":Lcom/here/android/mpa/routing/Maneuver$TrafficDirection;
    if-eqz v34, :cond_6

    .line 324
    invoke-virtual/range {v34 .. v34}, Lcom/here/android/mpa/routing/Maneuver$TrafficDirection;->name()Ljava/lang/String;

    move-result-object v13

    .line 326
    :cond_6
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "    [Maneuver-Raw] turn["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/here/android/mpa/routing/Maneuver;->getTurn()Lcom/here/android/mpa/routing/Maneuver$Turn;

    move-result-object v4

    invoke-virtual {v4}, Lcom/here/android/mpa/routing/Maneuver$Turn;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] to["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/here/android/mpa/routing/Maneuver;->getNextRoadNumber()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/here/android/mpa/routing/Maneuver;->getNextRoadName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] distance["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 327
    invoke-virtual {v2}, Lcom/here/android/mpa/routing/Maneuver;->getDistanceToNextManeuver()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " meters] action["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/here/android/mpa/routing/Maneuver;->getAction()Lcom/here/android/mpa/routing/Maneuver$Action;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] current["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/here/android/mpa/routing/Maneuver;->getRoadNumber()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 328
    invoke-virtual {v2}, Lcom/here/android/mpa/routing/Maneuver;->getRoadName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] traffic-directon["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] angle["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/here/android/mpa/routing/Maneuver;->getAngle()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] orientation["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/here/android/mpa/routing/Maneuver;->getMapOrientation()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] distanceFromStart["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 329
    invoke-virtual {v2}, Lcom/here/android/mpa/routing/Maneuver;->getDistanceFromStart()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " meters] Icon["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    .line 330
    .local v26, "maneuverRaw":Ljava/lang/String;
    if-eqz p3, :cond_8

    .line 331
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 336
    :goto_3
    invoke-virtual {v2}, Lcom/here/android/mpa/routing/Maneuver;->getSignpost()Lcom/here/android/mpa/routing/Signpost;

    move-result-object v32

    .line 337
    .local v32, "signpost":Lcom/here/android/mpa/routing/Signpost;
    if-eqz v32, :cond_b

    .line 338
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "    [Maneuver-Raw-SignPost] exitNumber["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v32 .. v32}, Lcom/here/android/mpa/routing/Signpost;->getExitNumber()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] exitText["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v32 .. v32}, Lcom/here/android/mpa/routing/Signpost;->getExitText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    .line 339
    .local v28, "rawSignPost":Ljava/lang/String;
    if-eqz p3, :cond_9

    .line 340
    move-object/from16 v0, p3

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 344
    :goto_4
    invoke-virtual/range {v32 .. v32}, Lcom/here/android/mpa/routing/Signpost;->getExitDirections()Ljava/util/List;

    move-result-object v24

    .line 345
    .local v24, "labels":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Signpost$LocalizedLabel;>;"
    invoke-interface/range {v24 .. v24}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/here/android/mpa/routing/Signpost$LocalizedLabel;

    .line 346
    .local v19, "label":Lcom/here/android/mpa/routing/Signpost$LocalizedLabel;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "     [Maneuver-Raw-SignPost-Label]  text["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v19 .. v19}, Lcom/here/android/mpa/routing/Signpost$LocalizedLabel;->getText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] routeDirection["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 347
    invoke-virtual/range {v19 .. v19}, Lcom/here/android/mpa/routing/Signpost$LocalizedLabel;->getRouteDirection()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] routeName["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v19 .. v19}, Lcom/here/android/mpa/routing/Signpost$LocalizedLabel;->getRouteName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    .line 348
    .local v33, "signpostLabel":Ljava/lang/String;
    if-eqz p3, :cond_a

    .line 349
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v33

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    .line 291
    .end local v2    # "current":Lcom/here/android/mpa/routing/Maneuver;
    .end local v7    # "previous":Lcom/here/android/mpa/routing/Maneuver;
    .end local v9    # "after":Lcom/here/android/mpa/routing/Maneuver;
    .end local v13    # "directionStr":Ljava/lang/String;
    .end local v16    # "i":I
    .end local v17    # "icon":Lcom/here/android/mpa/routing/Maneuver$Icon;
    .end local v18    # "iconStr":Ljava/lang/String;
    .end local v19    # "label":Lcom/here/android/mpa/routing/Signpost$LocalizedLabel;
    .end local v24    # "labels":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Signpost$LocalizedLabel;>;"
    .end local v25    # "len":I
    .end local v26    # "maneuverRaw":Ljava/lang/String;
    .end local v27    # "maneuvers":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    .end local v28    # "rawSignPost":Ljava/lang/String;
    .end local v32    # "signpost":Lcom/here/android/mpa/routing/Signpost;
    .end local v33    # "signpostLabel":Ljava/lang/String;
    .end local v34    # "trafficDirection":Lcom/here/android/mpa/routing/Maneuver$TrafficDirection;
    :cond_7
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v29

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 333
    .restart local v2    # "current":Lcom/here/android/mpa/routing/Maneuver;
    .restart local v7    # "previous":Lcom/here/android/mpa/routing/Maneuver;
    .restart local v9    # "after":Lcom/here/android/mpa/routing/Maneuver;
    .restart local v13    # "directionStr":Ljava/lang/String;
    .restart local v16    # "i":I
    .restart local v17    # "icon":Lcom/here/android/mpa/routing/Maneuver$Icon;
    .restart local v18    # "iconStr":Ljava/lang/String;
    .restart local v25    # "len":I
    .restart local v26    # "maneuverRaw":Ljava/lang/String;
    .restart local v27    # "maneuvers":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    .restart local v34    # "trafficDirection":Lcom/here/android/mpa/routing/Maneuver$TrafficDirection;
    :cond_8
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v0, v26

    invoke-virtual {v3, v0}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 342
    .restart local v28    # "rawSignPost":Ljava/lang/String;
    .restart local v32    # "signpost":Lcom/here/android/mpa/routing/Signpost;
    :cond_9
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v0, v28

    invoke-virtual {v3, v0}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 351
    .restart local v19    # "label":Lcom/here/android/mpa/routing/Signpost$LocalizedLabel;
    .restart local v24    # "labels":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Signpost$LocalizedLabel;>;"
    .restart local v33    # "signpostLabel":Ljava/lang/String;
    :cond_a
    sget-object v4, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v0, v33

    invoke-virtual {v4, v0}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 357
    .end local v19    # "label":Lcom/here/android/mpa/routing/Signpost$LocalizedLabel;
    .end local v24    # "labels":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Signpost$LocalizedLabel;>;"
    .end local v28    # "rawSignPost":Ljava/lang/String;
    .end local v33    # "signpostLabel":Ljava/lang/String;
    :cond_b
    if-nez v16, :cond_c

    .line 358
    invoke-static {v2, v9}, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->getStartManeuverDisplay(Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;)Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    move-result-object v14

    .line 373
    .local v14, "display":Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    :goto_6
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[Maneuver-Display] "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v14}, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 375
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[Maneuver-Display-short] "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v14, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingTurn:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v14, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingRoad:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ( "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v14, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->distanceToPendingRoadText:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " )"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    .line 376
    .local v31, "shortTbt":Ljava/lang/String;
    if-eqz p3, :cond_e

    .line 377
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v31

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 307
    :goto_7
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_2

    .line 360
    .end local v14    # "display":Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    .end local v31    # "shortTbt":Ljava/lang/String;
    :cond_c
    if-nez v9, :cond_d

    const/4 v3, 0x1

    .line 363
    :goto_8
    invoke-virtual {v2}, Lcom/here/android/mpa/routing/Maneuver;->getDistanceToNextManeuver()I

    move-result v4

    int-to-long v4, v4

    const/4 v10, 0x1

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v6, p1

    move-object/from16 v8, p2

    .line 360
    invoke-static/range {v2 .. v12}, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->getManeuverDisplay(Lcom/here/android/mpa/routing/Maneuver;ZJLjava/lang/String;Lcom/here/android/mpa/routing/Maneuver;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/routing/Maneuver;ZZLcom/navdy/hud/app/maps/MapEvents$DestinationDirection;)Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    move-result-object v14

    .restart local v14    # "display":Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    goto/16 :goto_6

    .end local v14    # "display":Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    :cond_d
    const/4 v3, 0x0

    goto :goto_8

    .line 379
    .restart local v14    # "display":Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    .restart local v31    # "shortTbt":Ljava/lang/String;
    :cond_e
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v0, v31

    invoke-virtual {v3, v0}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_7

    .line 383
    .end local v9    # "after":Lcom/here/android/mpa/routing/Maneuver;
    .end local v13    # "directionStr":Ljava/lang/String;
    .end local v14    # "display":Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    .end local v17    # "icon":Lcom/here/android/mpa/routing/Maneuver$Icon;
    .end local v18    # "iconStr":Ljava/lang/String;
    .end local v26    # "maneuverRaw":Ljava/lang/String;
    .end local v31    # "shortTbt":Ljava/lang/String;
    .end local v32    # "signpost":Lcom/here/android/mpa/routing/Signpost;
    .end local v34    # "trafficDirection":Lcom/here/android/mpa/routing/Maneuver$TrafficDirection;
    :cond_f
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v22

    .line 384
    .local v22, "l2":J
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Time to print route:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sub-long v10, v22, v20

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method static printTrafficEvent(Lcom/here/android/mpa/mapping/TrafficEvent;Ljava/lang/String;Lcom/here/android/mpa/routing/Route;)V
    .locals 6
    .param p0, "event"    # Lcom/here/android/mpa/mapping/TrafficEvent;
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "route"    # Lcom/here/android/mpa/routing/Route;

    .prologue
    .line 555
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapUtil;->trafficEventBuilder:Ljava/lang/StringBuilder;

    monitor-enter v3

    .line 556
    :try_start_0
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->trafficEventBuilder:Ljava/lang/StringBuilder;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 557
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->trafficEventBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 558
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->trafficEventBuilder:Ljava/lang/StringBuilder;

    const-string v4, ":: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 559
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->trafficEventBuilder:Ljava/lang/StringBuilder;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "severity:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/here/android/mpa/mapping/TrafficEvent;->getSeverity()Lcom/here/android/mpa/mapping/TrafficEvent$Severity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/here/android/mpa/mapping/TrafficEvent$Severity;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 560
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->trafficEventBuilder:Ljava/lang/StringBuilder;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ", short-text:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/here/android/mpa/mapping/TrafficEvent;->getShortText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 561
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->trafficEventBuilder:Ljava/lang/StringBuilder;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ", text:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/here/android/mpa/mapping/TrafficEvent;->getEventText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 562
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->trafficEventBuilder:Ljava/lang/StringBuilder;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ", isActive:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/here/android/mpa/mapping/TrafficEvent;->isActive()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 563
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->trafficEventBuilder:Ljava/lang/StringBuilder;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ", isFlow:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/here/android/mpa/mapping/TrafficEvent;->isFlow()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 564
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->trafficEventBuilder:Ljava/lang/StringBuilder;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ", isIncident:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/here/android/mpa/mapping/TrafficEvent;->isIncident()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 565
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->trafficEventBuilder:Ljava/lang/StringBuilder;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ", isVisible:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/here/android/mpa/mapping/TrafficEvent;->isVisible()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 566
    sget-object v4, Lcom/navdy/hud/app/maps/here/HereMapUtil;->trafficEventBuilder:Ljava/lang/StringBuilder;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ", isOnRoute:"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-nez p2, :cond_0

    const-string v2, "n/a"

    :goto_0
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 567
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->trafficEventBuilder:Ljava/lang/StringBuilder;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ", isReroutable:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/here/android/mpa/mapping/TrafficEvent;->isReroutable()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 568
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->trafficEventBuilder:Ljava/lang/StringBuilder;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ", affected len:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/here/android/mpa/mapping/TrafficEvent;->getAffectedLength()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 569
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->trafficEventBuilder:Ljava/lang/StringBuilder;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ", offRoute icon:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/here/android/mpa/mapping/TrafficEvent;->getIconOffRoute()Lcom/here/android/mpa/common/Image;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 570
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->trafficEventBuilder:Ljava/lang/StringBuilder;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ", onRoute icon:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/here/android/mpa/mapping/TrafficEvent;->getIconOnRoute()Lcom/here/android/mpa/common/Image;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 571
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->trafficEventBuilder:Ljava/lang/StringBuilder;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ", firstAffectedStreet:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/here/android/mpa/mapping/TrafficEvent;->getFirstAffectedStreet()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 572
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->trafficEventBuilder:Ljava/lang/StringBuilder;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ", activateDate:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/here/android/mpa/mapping/TrafficEvent;->getActivationDate()Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 573
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->trafficEventBuilder:Ljava/lang/StringBuilder;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ", updateDate:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/here/android/mpa/mapping/TrafficEvent;->getUpdateDate()Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 574
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->trafficEventBuilder:Ljava/lang/StringBuilder;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ", now:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 576
    invoke-virtual {p0}, Lcom/here/android/mpa/mapping/TrafficEvent;->getAffectedStreets()Ljava/util/List;

    move-result-object v0

    .line 577
    .local v0, "affectedStreets":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->trafficEventBuilder:Ljava/lang/StringBuilder;

    const-string v4, ", affectedStreet: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 578
    if-eqz v0, :cond_1

    .line 579
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 580
    .local v1, "s":Ljava/lang/String;
    sget-object v4, Lcom/navdy/hud/app/maps/here/HereMapUtil;->trafficEventBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 581
    sget-object v4, Lcom/navdy/hud/app/maps/here/HereMapUtil;->trafficEventBuilder:Ljava/lang/StringBuilder;

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 587
    .end local v0    # "affectedStreets":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v1    # "s":Ljava/lang/String;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 566
    :cond_0
    :try_start_1
    invoke-virtual {p0, p2}, Lcom/here/android/mpa/mapping/TrafficEvent;->isOnRoute(Lcom/here/android/mpa/routing/Route;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0

    .line 585
    .restart local v0    # "affectedStreets":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    sget-object v4, Lcom/navdy/hud/app/maps/here/HereMapUtil;->trafficEventBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 586
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereMapUtil;->trafficEventBuilder:Ljava/lang/StringBuilder;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 587
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 588
    return-void
.end method

.method static printTrafficNotification(Lcom/here/android/mpa/guidance/TrafficNotification;Ljava/lang/String;)V
    .locals 8
    .param p0, "trafficNotification"    # Lcom/here/android/mpa/guidance/TrafficNotification;
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 543
    invoke-virtual {p0}, Lcom/here/android/mpa/guidance/TrafficNotification;->getInfoList()Ljava/util/List;

    move-result-object v1

    .line 544
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/guidance/TrafficNotificationInfo;>;"
    if-eqz v1, :cond_0

    .line 545
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/here/android/mpa/guidance/TrafficNotificationInfo;

    .line 546
    .local v2, "trafficNotificationInfo":Lcom/here/android/mpa/guidance/TrafficNotificationInfo;
    sget-object v4, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " type:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lcom/here/android/mpa/guidance/TrafficNotificationInfo;->getType()Lcom/here/android/mpa/guidance/TrafficNotificationInfo$Type;

    move-result-object v6

    invoke-virtual {v6}, Lcom/here/android/mpa/guidance/TrafficNotificationInfo$Type;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 547
    sget-object v4, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " distance:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lcom/here/android/mpa/guidance/TrafficNotificationInfo;->getDistanceInMeters()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 548
    invoke-virtual {v2}, Lcom/here/android/mpa/guidance/TrafficNotificationInfo;->getEvent()Lcom/here/android/mpa/mapping/TrafficEvent;

    move-result-object v0

    .line 549
    .local v0, "event":Lcom/here/android/mpa/mapping/TrafficEvent;
    const/4 v4, 0x0

    invoke-static {v0, p1, v4}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->printTrafficEvent(Lcom/here/android/mpa/mapping/TrafficEvent;Ljava/lang/String;Lcom/here/android/mpa/routing/Route;)V

    goto :goto_0

    .line 552
    .end local v0    # "event":Lcom/here/android/mpa/mapping/TrafficEvent;
    .end local v2    # "trafficNotificationInfo":Lcom/here/android/mpa/guidance/TrafficNotificationInfo;
    :cond_0
    return-void
.end method

.method public static declared-synchronized removeRouteInfo(Lcom/navdy/service/library/log/Logger;Z)V
    .locals 2
    .param p0, "logger"    # Lcom/navdy/service/library/log/Logger;
    .param p1, "clearAll"    # Z

    .prologue
    .line 735
    const-class v0, Lcom/navdy/hud/app/maps/here/HereMapUtil;

    monitor-enter v0

    :try_start_0
    invoke-static {p0, p1}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->removeRouteInfoFiles(Lcom/navdy/service/library/log/Logger;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 736
    monitor-exit v0

    return-void

    .line 735
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private static removeRouteInfoFiles(Lcom/navdy/service/library/log/Logger;Z)V
    .locals 8
    .param p0, "logger"    # Lcom/navdy/service/library/log/Logger;
    .param p1, "clearAll"    # Z

    .prologue
    .line 740
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    .line 741
    .local v3, "context":Landroid/content/Context;
    invoke-static {}, Lcom/navdy/hud/app/storage/PathManager;->getInstance()Lcom/navdy/hud/app/storage/PathManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/hud/app/storage/PathManager;->getActiveRouteInfoDir()Ljava/lang/String;

    move-result-object v4

    .line 742
    .local v4, "path":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "gasrouterequest.bin"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 744
    .local v0, "b1":Z
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "removeRouteInfo delete gasRoute["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 746
    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    .line 747
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "routerequest.bin"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    .line 748
    .local v1, "b2":Z
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "routerequest.device"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    .line 749
    .local v2, "b3":Z
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "removeRouteInfo delete route["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] delete id["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 754
    .end local v0    # "b1":Z
    .end local v1    # "b2":Z
    .end local v2    # "b3":Z
    .end local v3    # "context":Landroid/content/Context;
    .end local v4    # "path":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 751
    :catch_0
    move-exception v5

    .line 752
    .local v5, "t":Ljava/lang/Throwable;
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v6, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static roundToIntegerStep(IF)F
    .locals 2
    .param p0, "minStep"    # I
    .param p1, "value"    # F

    .prologue
    .line 1312
    int-to-float v1, p0

    div-float v1, p1, v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 1313
    .local v0, "nTimes":I
    mul-int v1, p0, v0

    int-to-float v1, v1

    return v1
.end method

.method public static roundToN(DI)D
    .locals 4
    .param p0, "val"    # D
    .param p2, "N"    # I

    .prologue
    .line 1143
    int-to-double v2, p2

    mul-double/2addr v2, p0

    double-to-long v2, v2

    long-to-double v0, v2

    .line 1144
    .local v0, "l":D
    int-to-double v2, p2

    div-double/2addr v0, v2

    .line 1145
    return-wide v0
.end method

.method public static saveGasRouteInfo(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/navdy/service/library/device/NavdyDeviceId;Lcom/navdy/service/library/log/Logger;)V
    .locals 1
    .param p0, "request"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .param p1, "deviceId"    # Lcom/navdy/service/library/device/NavdyDeviceId;
    .param p2, "sLogger"    # Lcom/navdy/service/library/log/Logger;

    .prologue
    .line 671
    const-string v0, "gasrouterequest.bin"

    invoke-static {p0, p1, p2, v0}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->saveRouteInfoInternal(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/navdy/service/library/device/NavdyDeviceId;Lcom/navdy/service/library/log/Logger;Ljava/lang/String;)V

    .line 672
    return-void
.end method

.method public static saveRouteInfo(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/navdy/service/library/device/NavdyDeviceId;Lcom/navdy/service/library/log/Logger;)V
    .locals 1
    .param p0, "request"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .param p1, "deviceId"    # Lcom/navdy/service/library/device/NavdyDeviceId;
    .param p2, "logger"    # Lcom/navdy/service/library/log/Logger;

    .prologue
    .line 667
    const-string v0, "routerequest.bin"

    invoke-static {p0, p1, p2, v0}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->saveRouteInfoInternal(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/navdy/service/library/device/NavdyDeviceId;Lcom/navdy/service/library/log/Logger;Ljava/lang/String;)V

    .line 668
    return-void
.end method

.method private static declared-synchronized saveRouteInfoInternal(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/navdy/service/library/device/NavdyDeviceId;Lcom/navdy/service/library/log/Logger;Ljava/lang/String;)V
    .locals 12
    .param p0, "r"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .param p1, "deviceId"    # Lcom/navdy/service/library/device/NavdyDeviceId;
    .param p2, "logger"    # Lcom/navdy/service/library/log/Logger;
    .param p3, "fileName"    # Ljava/lang/String;

    .prologue
    .line 675
    const-class v9, Lcom/navdy/hud/app/maps/here/HereMapUtil;

    monitor-enter v9

    const/4 v2, 0x0

    .line 678
    .local v2, "file":Ljava/io/FileOutputStream;
    const/4 v8, 0x0

    :try_start_0
    sput-object v8, Lcom/navdy/hud/app/maps/here/HereMapUtil;->lastKnownDeviceId:Ljava/lang/String;

    .line 679
    if-nez p1, :cond_0

    .line 681
    sget-object v8, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "saveRouteInfo: null deviceid"

    invoke-virtual {v8, v10}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 683
    :try_start_1
    new-instance v1, Lcom/navdy/service/library/device/NavdyDeviceId;

    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getConnectionHandler()Lcom/navdy/hud/app/service/ConnectionHandler;

    move-result-object v8

    invoke-virtual {v8}, Lcom/navdy/hud/app/service/ConnectionHandler;->getLastConnectedDeviceInfo()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v8

    iget-object v8, v8, Lcom/navdy/service/library/events/DeviceInfo;->deviceId:Ljava/lang/String;

    invoke-direct {v1, v8}, Lcom/navdy/service/library/device/NavdyDeviceId;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 684
    .end local p1    # "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    .local v1, "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    :try_start_2
    sget-object v8, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "saveRouteInfo: last deviceid found:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object p1, v1

    .line 689
    .end local v1    # "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    .restart local p1    # "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    :cond_0
    :goto_0
    :try_start_3
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "saveRouteInfo request["

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, "] deviceId["

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, "]"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p2, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 690
    iget-object v8, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    if-nez v8, :cond_1

    .line 691
    const-string v8, "route does not have destination"

    invoke-virtual {p2, v8}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 692
    sget-object v8, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v10, 0x1

    invoke-static {v8, v10}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->removeRouteInfoFiles(Lcom/navdy/service/library/log/Logger;Z)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 732
    :goto_1
    monitor-exit v9

    return-void

    .line 685
    :catch_0
    move-exception v7

    .line 686
    .local v7, "t":Ljava/lang/Throwable;
    :goto_2
    :try_start_4
    sget-object v8, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "saveRouteInfo"

    invoke-virtual {v8, v10, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 727
    .end local v7    # "t":Ljava/lang/Throwable;
    :catch_1
    move-exception v7

    .line 728
    .restart local v7    # "t":Ljava/lang/Throwable;
    :goto_3
    :try_start_5
    sget-object v8, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v8, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 729
    invoke-static {v2}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 730
    sget-object v8, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v10, 0x1

    invoke-static {v8, v10}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->removeRouteInfoFiles(Lcom/navdy/service/library/log/Logger;Z)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 675
    .end local v7    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v8

    :goto_4
    monitor-exit v9

    throw v8

    .line 696
    :cond_1
    :try_start_6
    new-instance v6, Ljava/util/ArrayList;

    iget-object v8, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->routeAttributes:Ljava/util/List;

    invoke-direct {v6, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 697
    .local v6, "routeAttributes":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;>;"
    sget-object v8, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;->ROUTE_ATTRIBUTE_SAVED_ROUTE:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;

    invoke-interface {v6, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 698
    sget-object v8, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;->ROUTE_ATTRIBUTE_SAVED_ROUTE:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 700
    :cond_2
    new-instance v8, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    invoke-direct {v8, p0}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;-><init>(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V

    invoke-virtual {v8, v6}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->routeAttributes(Ljava/util/List;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    move-result-object v8

    invoke-virtual {v8}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->build()Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    move-result-object v5

    .line 701
    .local v5, "request":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    sget-object v8, Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "saveRouteInfo added saved route attribute"

    invoke-virtual {v8, v10}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 703
    invoke-static {}, Lcom/navdy/hud/app/storage/PathManager;->getInstance()Lcom/navdy/hud/app/storage/PathManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/navdy/hud/app/storage/PathManager;->getActiveRouteInfoDir()Ljava/lang/String;

    move-result-object v4

    .line 706
    .local v4, "path":Ljava/lang/String;
    new-instance v3, Ljava/io/FileOutputStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v10, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v3, v8}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 707
    .end local v2    # "file":Ljava/io/FileOutputStream;
    .local v3, "file":Ljava/io/FileOutputStream;
    :try_start_7
    invoke-virtual {v5}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->toByteArray()[B

    move-result-object v0

    .line 708
    .local v0, "data":[B
    invoke-virtual {v3, v0}, Ljava/io/FileOutputStream;->write([B)V

    .line 709
    invoke-static {v3}, Lcom/navdy/service/library/util/IOUtils;->fileSync(Ljava/io/FileOutputStream;)V

    .line 710
    invoke-static {v3}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 711
    const/4 v2, 0x0

    .line 712
    .end local v3    # "file":Ljava/io/FileOutputStream;
    .restart local v2    # "file":Ljava/io/FileOutputStream;
    :try_start_8
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "saveRouteInfo: route info size["

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    array-length v10, v0

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, "]"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p2, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 715
    if-eqz p1, :cond_3

    .line 716
    new-instance v3, Ljava/io/FileOutputStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v10, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, "routerequest.device"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v3, v8}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 717
    .end local v2    # "file":Ljava/io/FileOutputStream;
    .restart local v3    # "file":Ljava/io/FileOutputStream;
    :try_start_9
    invoke-virtual {p1}, Lcom/navdy/service/library/device/NavdyDeviceId;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 718
    invoke-virtual {v3, v0}, Ljava/io/FileOutputStream;->write([B)V

    .line 719
    invoke-static {v3}, Lcom/navdy/service/library/util/IOUtils;->fileSync(Ljava/io/FileOutputStream;)V

    .line 720
    invoke-static {v3}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 721
    const/4 v2, 0x0

    .line 722
    .end local v3    # "file":Ljava/io/FileOutputStream;
    .restart local v2    # "file":Ljava/io/FileOutputStream;
    :try_start_a
    invoke-virtual {p1}, Lcom/navdy/service/library/device/NavdyDeviceId;->toString()Ljava/lang/String;

    move-result-object v8

    sput-object v8, Lcom/navdy/hud/app/maps/here/HereMapUtil;->lastKnownDeviceId:Ljava/lang/String;

    .line 723
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "saveRouteInfo: device id size["

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    array-length v10, v0

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, "]"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p2, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 725
    :cond_3
    const-string v8, "saveRouteInfo: device id not written:null"

    invoke-virtual {p2, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_1

    .line 727
    .end local v0    # "data":[B
    .end local v2    # "file":Ljava/io/FileOutputStream;
    .restart local v3    # "file":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v7

    move-object v2, v3

    .end local v3    # "file":Ljava/io/FileOutputStream;
    .restart local v2    # "file":Ljava/io/FileOutputStream;
    goto/16 :goto_3

    .line 685
    .end local v4    # "path":Ljava/lang/String;
    .end local v5    # "request":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .end local v6    # "routeAttributes":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;>;"
    .end local p1    # "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    .restart local v1    # "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    :catch_3
    move-exception v7

    move-object p1, v1

    .end local v1    # "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    .restart local p1    # "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    goto/16 :goto_2

    .line 675
    .end local p1    # "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    .restart local v1    # "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    :catchall_1
    move-exception v8

    move-object p1, v1

    .end local v1    # "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    .restart local p1    # "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    goto/16 :goto_4

    .end local v2    # "file":Ljava/io/FileOutputStream;
    .restart local v3    # "file":Ljava/io/FileOutputStream;
    .restart local v4    # "path":Ljava/lang/String;
    .restart local v5    # "request":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .restart local v6    # "routeAttributes":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;>;"
    :catchall_2
    move-exception v8

    move-object v2, v3

    .end local v3    # "file":Ljava/io/FileOutputStream;
    .restart local v2    # "file":Ljava/io/FileOutputStream;
    goto/16 :goto_4
.end method
