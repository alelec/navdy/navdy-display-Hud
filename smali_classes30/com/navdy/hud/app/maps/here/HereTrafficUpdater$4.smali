.class Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$4;
.super Ljava/lang/Object;
.source "HereTrafficUpdater.java"

# interfaces
.implements Lcom/here/android/mpa/guidance/TrafficUpdater$GetEventsListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(Ljava/util/List;Lcom/here/android/mpa/guidance/TrafficUpdater$Error;)V
    .locals 3
    .param p2, "error"    # Lcom/here/android/mpa/guidance/TrafficUpdater$Error;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/mapping/TrafficEvent;",
            ">;",
            "Lcom/here/android/mpa/guidance/TrafficUpdater$Error;",
            ")V"
        }
    .end annotation

    .prologue
    .line 93
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/mapping/TrafficEvent;>;"
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentRoute:Lcom/here/android/mpa/routing/Route;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->access$200(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;)Lcom/here/android/mpa/routing/Route;

    move-result-object v0

    if-nez v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->access$400(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->tag:Ljava/lang/String;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->access$300(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "onGetEventsListener triggered and currentRoute is null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 113
    :goto_0
    return-void

    .line 98
    :cond_0
    sget-object v0, Lcom/here/android/mpa/guidance/TrafficUpdater$Error;->NONE:Lcom/here/android/mpa/guidance/TrafficUpdater$Error;

    if-ne p2, v0, :cond_3

    .line 100
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentState:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->access$800(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;)Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->ACTIVE:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentState:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->access$800(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;)Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->TRACK_DISTANCE:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    if-ne v0, v1, :cond_2

    .line 101
    :cond_1
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$4$1;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$4$1;-><init>(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$4;Ljava/util/List;)V

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 112
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;

    # invokes: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->refresh()V
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->access$700(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;)V

    goto :goto_0

    .line 110
    :cond_3
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->access$400(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->tag:Ljava/lang/String;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->access$300(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " error "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/here/android/mpa/guidance/TrafficUpdater$Error;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " while getting TrafficEvents."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto :goto_1
.end method
