.class Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;
.super Ljava/lang/Object;
.source "NavdyTrafficRerouteManager.java"

# interfaces
.implements Lcom/navdy/hud/app/maps/here/HereRouteCalculator$RouteCalculatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->reCalculateCurrentRoute(Lcom/here/android/mpa/routing/Route;Lcom/here/android/mpa/routing/Route;ZLjava/lang/String;JLjava/lang/String;JJLjava/lang/String;Lcom/here/android/mpa/routing/RouteTta;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

.field final synthetic val$additionalVia:Ljava/lang/String;

.field final synthetic val$currentVia:Ljava/lang/String;

.field final synthetic val$diff:J

.field final synthetic val$distanceDiff:J

.field final synthetic val$etaUtc:J

.field final synthetic val$fasterRoute:Lcom/here/android/mpa/routing/Route;

.field final synthetic val$fasterRouteTta:Lcom/here/android/mpa/routing/RouteTta;

.field final synthetic val$notifFromHereTraffic:Z

.field final synthetic val$via:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;ZJLjava/lang/String;Ljava/lang/String;Lcom/here/android/mpa/routing/RouteTta;JLcom/here/android/mpa/routing/Route;Ljava/lang/String;J)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    .prologue
    .line 551
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    iput-boolean p2, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$notifFromHereTraffic:Z

    iput-wide p3, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$diff:J

    iput-object p5, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$via:Ljava/lang/String;

    iput-object p6, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$currentVia:Ljava/lang/String;

    iput-object p7, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$fasterRouteTta:Lcom/here/android/mpa/routing/RouteTta;

    iput-wide p8, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$etaUtc:J

    iput-object p10, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$fasterRoute:Lcom/here/android/mpa/routing/Route;

    iput-object p11, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$additionalVia:Ljava/lang/String;

    iput-wide p12, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$distanceDiff:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public error(Lcom/here/android/mpa/routing/RoutingError;Ljava/lang/Throwable;)V
    .locals 6
    .param p1, "error"    # Lcom/here/android/mpa/routing/RoutingError;
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 635
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$500(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->killRouteCalc:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$400(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 636
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # invokes: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->cleanupRouteIfStale()V
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$600(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)V

    .line 637
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$000(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    move-result-object v0

    iget-boolean v1, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$notifFromHereTraffic:Z

    iget-wide v2, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$diff:J

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$via:Ljava/lang/String;

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$currentVia:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->fasterRouteNotFound(ZJLjava/lang/String;Ljava/lang/String;)V

    .line 638
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "error occured:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 639
    return-void
.end method

.method public postSuccess(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 559
    .local p1, "outgoingResults":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/navigation/NavigationRouteResult;>;"
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;-><init>(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;Ljava/util/ArrayList;)V

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 631
    return-void
.end method

.method public preSuccess()V
    .locals 2

    .prologue
    .line 554
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$500(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->killRouteCalc:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$400(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 555
    return-void
.end method

.method public progress(I)V
    .locals 0
    .param p1, "progress"    # I

    .prologue
    .line 642
    return-void
.end method
