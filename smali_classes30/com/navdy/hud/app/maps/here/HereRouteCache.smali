.class public Lcom/navdy/hud/app/maps/here/HereRouteCache;
.super Landroid/util/LruCache;
.source "HereRouteCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/LruCache",
        "<",
        "Ljava/lang/String;",
        "Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;",
        ">;"
    }
.end annotation


# static fields
.field public static final MAX_CACHE_ROUTE_COUNT:I = 0x14

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static final sSingleton:Lcom/navdy/hud/app/maps/here/HereRouteCache;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 15
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/maps/here/HereRouteCache;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereRouteCache;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 38
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereRouteCache;

    invoke-direct {v0}, Lcom/navdy/hud/app/maps/here/HereRouteCache;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereRouteCache;->sSingleton:Lcom/navdy/hud/app/maps/here/HereRouteCache;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 45
    const/16 v0, 0x14

    invoke-direct {p0, v0}, Landroid/util/LruCache;-><init>(I)V

    .line 46
    return-void
.end method

.method public static getInstance()Lcom/navdy/hud/app/maps/here/HereRouteCache;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereRouteCache;->sSingleton:Lcom/navdy/hud/app/maps/here/HereRouteCache;

    return-object v0
.end method


# virtual methods
.method public addRoute(Ljava/lang/String;Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "routeInfo"    # Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;

    .prologue
    .line 53
    invoke-super {p0, p1, p2}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    return-void
.end method

.method protected entryRemoved(ZLjava/lang/String;Lcom/here/android/mpa/routing/Route;Lcom/here/android/mpa/routing/Route;)V
    .locals 3
    .param p1, "evicted"    # Z
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "oldValue"    # Lcom/here/android/mpa/routing/Route;
    .param p4, "newValue"    # Lcom/here/android/mpa/routing/Route;

    .prologue
    .line 69
    if-eqz p1, :cond_0

    if-eqz p3, :cond_0

    .line 70
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereRouteCache;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "route removed from cache:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 72
    :cond_0
    return-void
.end method

.method public getRoute(Ljava/lang/String;)Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 61
    if-eqz p1, :cond_0

    .line 62
    invoke-super {p0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;

    .line 64
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeRoute(Ljava/lang/String;)Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 57
    invoke-super {p0, p1}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;

    return-object v0
.end method

.method protected sizeOf(Ljava/lang/String;Lcom/here/android/mpa/routing/Route;)I
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Lcom/here/android/mpa/routing/Route;

    .prologue
    .line 49
    const/4 v0, 0x1

    return v0
.end method
