.class Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;
.super Ljava/lang/Object;
.source "HereMapCameraManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->getAnimateCameraRunnable(DF)Ljava/lang/Runnable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

.field final synthetic val$tiltStep:F

.field final synthetic val$zoomStep:D


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;DF)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    .prologue
    .line 642
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    iput-wide p2, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;->val$zoomStep:D

    iput p4, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;->val$tiltStep:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 645
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8$1;-><init>(Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;)V

    const/16 v2, 0x11

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 678
    return-void
.end method
