.class Lcom/navdy/hud/app/maps/here/HereRegionManager$2;
.super Ljava/lang/Object;
.source "HereRegionManager.java"

# interfaces
.implements Lcom/here/android/mpa/search/ResultListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereRegionManager;->checkRegion()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/here/android/mpa/search/ResultListener",
        "<",
        "Lcom/here/android/mpa/search/Location;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereRegionManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereRegionManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereRegionManager;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereRegionManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereRegionManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted(Lcom/here/android/mpa/search/Location;Lcom/here/android/mpa/search/ErrorCode;)V
    .locals 6
    .param p1, "location"    # Lcom/here/android/mpa/search/Location;
    .param p2, "errorCode"    # Lcom/here/android/mpa/search/ErrorCode;

    .prologue
    .line 76
    :try_start_0
    sget-object v3, Lcom/here/android/mpa/search/ErrorCode;->NONE:Lcom/here/android/mpa/search/ErrorCode;

    if-eq p2, v3, :cond_1

    .line 77
    # getter for: Lcom/navdy/hud/app/maps/here/HereRegionManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRegionManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "reverseGeoCode:onComplete: Error["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p2}, Lcom/here/android/mpa/search/ErrorCode;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 97
    :cond_0
    :goto_0
    return-void

    .line 79
    :cond_1
    if-nez p1, :cond_2

    .line 80
    # getter for: Lcom/navdy/hud/app/maps/here/HereRegionManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRegionManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "reverseGeoCode:onComplete: address is null"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 93
    :catch_0
    move-exception v2

    .line 94
    .local v2, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/maps/here/HereRegionManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRegionManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 95
    invoke-static {}, Lcom/navdy/hud/app/util/CrashReporter;->getInstance()Lcom/navdy/hud/app/util/CrashReporter;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/navdy/hud/app/util/CrashReporter;->reportNonFatalException(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 83
    .end local v2    # "t":Ljava/lang/Throwable;
    :cond_2
    :try_start_1
    invoke-virtual {p1}, Lcom/here/android/mpa/search/Location;->getAddress()Lcom/here/android/mpa/search/Address;

    move-result-object v0

    .line 84
    .local v0, "address":Lcom/here/android/mpa/search/Address;
    if-eqz v0, :cond_0

    .line 85
    new-instance v1, Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;

    invoke-virtual {v0}, Lcom/here/android/mpa/search/Address;->getState()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/here/android/mpa/search/Address;->getCountryName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    .local v1, "regionEvent":Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereRegionManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereRegionManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRegionManager;->currentRegionEvent:Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereRegionManager;->access$200(Lcom/navdy/hud/app/maps/here/HereRegionManager;)Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 87
    # getter for: Lcom/navdy/hud/app/maps/here/HereRegionManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRegionManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 88
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereRegionManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereRegionManager;

    # setter for: Lcom/navdy/hud/app/maps/here/HereRegionManager;->currentRegionEvent:Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;
    invoke-static {v3, v1}, Lcom/navdy/hud/app/maps/here/HereRegionManager;->access$202(Lcom/navdy/hud/app/maps/here/HereRegionManager;Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;)Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;

    .line 89
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereRegionManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereRegionManager;

    iget-object v3, v3, Lcom/navdy/hud/app/maps/here/HereRegionManager;->bus:Lcom/squareup/otto/Bus;

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereRegionManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereRegionManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRegionManager;->currentRegionEvent:Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereRegionManager;->access$200(Lcom/navdy/hud/app/maps/here/HereRegionManager;)Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public bridge synthetic onCompleted(Ljava/lang/Object;Lcom/here/android/mpa/search/ErrorCode;)V
    .locals 0

    .prologue
    .line 72
    check-cast p1, Lcom/here/android/mpa/search/Location;

    invoke-virtual {p0, p1, p2}, Lcom/navdy/hud/app/maps/here/HereRegionManager$2;->onCompleted(Lcom/here/android/mpa/search/Location;Lcom/here/android/mpa/search/ErrorCode;)V

    return-void
.end method
