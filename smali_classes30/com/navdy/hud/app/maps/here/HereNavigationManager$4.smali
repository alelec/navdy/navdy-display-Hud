.class Lcom/navdy/hud/app/maps/here/HereNavigationManager$4;
.super Ljava/lang/Object;
.source "HereNavigationManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereNavigationManager;->postNavigationSessionStatusEvent(Lcom/navdy/service/library/events/navigation/NavigationSessionState;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

.field final synthetic val$sendRouteResult:Z

.field final synthetic val$sessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereNavigationManager;ZLcom/navdy/service/library/events/navigation/NavigationSessionState;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    .prologue
    .line 773
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    iput-boolean p2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$4;->val$sendRouteResult:Z

    iput-object p3, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$4;->val$sessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 20

    .prologue
    .line 776
    const/4 v4, 0x0

    .line 777
    .local v4, "label":Ljava/lang/String;
    const/4 v6, 0x0

    .line 778
    .local v6, "result":Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    const/4 v5, 0x0

    .line 779
    .local v5, "routeId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$100(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    move-result-object v3

    iget-object v11, v3, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->navigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .line 780
    .local v11, "navRequest":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    if-eqz v11, :cond_2

    .line 781
    iget-object v4, v11, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->label:Ljava/lang/String;

    .line 782
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$100(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    move-result-object v3

    iget-object v5, v3, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->routeId:Ljava/lang/String;

    .line 783
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$4;->val$sendRouteResult:Z

    if-eqz v3, :cond_2

    .line 784
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getInstance()Lcom/navdy/hud/app/maps/here/HereRouteCache;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getRoute(Ljava/lang/String;)Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;

    move-result-object v13

    .line 785
    .local v13, "routeInfo":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    if-eqz v13, :cond_2

    .line 786
    iget-object v6, v13, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->routeResult:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    .line 789
    iget-object v3, v6, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->duration:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 790
    .local v8, "freeFlowDuration":I
    iget-object v3, v6, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->duration_traffic:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v17

    .line 792
    .local v17, "trafficDuration":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereNavController:Lcom/navdy/hud/app/maps/here/HereNavController;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$700(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Lcom/navdy/hud/app/maps/here/HereNavController;

    move-result-object v3

    const/4 v7, 0x1

    sget-object v18, Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;->DISABLED:Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    move-object/from16 v0, v18

    invoke-virtual {v3, v7, v0}, Lcom/navdy/hud/app/maps/here/HereNavController;->getEta(ZLcom/here/android/mpa/routing/Route$TrafficPenaltyMode;)Ljava/util/Date;

    move-result-object v12

    .line 793
    .local v12, "noTraffic":Ljava/util/Date;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereNavController:Lcom/navdy/hud/app/maps/here/HereNavController;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$700(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Lcom/navdy/hud/app/maps/here/HereNavController;

    move-result-object v3

    const/4 v7, 0x1

    sget-object v18, Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;->OPTIMAL:Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    move-object/from16 v0, v18

    invoke-virtual {v3, v7, v0}, Lcom/navdy/hud/app/maps/here/HereNavController;->getEta(ZLcom/here/android/mpa/routing/Route$TrafficPenaltyMode;)Ljava/util/Date;

    move-result-object v16

    .line 794
    .local v16, "traffic":Ljava/util/Date;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    .line 796
    .local v14, "now":J
    invoke-static {v12}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->isValidEtaDate(Ljava/util/Date;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 797
    invoke-virtual {v12}, Ljava/util/Date;->getTime()J

    move-result-wide v18

    sub-long v18, v18, v14

    move-wide/from16 v0, v18

    long-to-int v3, v0

    div-int/lit16 v8, v3, 0x3e8

    .line 800
    :cond_0
    invoke-static/range {v16 .. v16}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->isValidEtaDate(Ljava/util/Date;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 801
    invoke-virtual/range {v16 .. v16}, Ljava/util/Date;->getTime()J

    move-result-wide v18

    sub-long v18, v18, v14

    move-wide/from16 v0, v18

    long-to-int v3, v0

    div-int/lit16 v0, v3, 0x3e8

    move/from16 v17, v0

    .line 804
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereNavController:Lcom/navdy/hud/app/maps/here/HereNavController;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$700(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Lcom/navdy/hud/app/maps/here/HereNavController;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereNavController;->getDestinationDistance()J

    move-result-wide v18

    move-wide/from16 v0, v18

    long-to-int v10, v0

    .line 806
    .local v10, "length":I
    new-instance v3, Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;

    invoke-direct {v3, v6}, Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;-><init>(Lcom/navdy/service/library/events/navigation/NavigationRouteResult;)V

    .line 807
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;->duration(Ljava/lang/Integer;)Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;

    move-result-object v3

    .line 808
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;->duration_traffic(Ljava/lang/Integer;)Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;

    move-result-object v3

    .line 809
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;->length(Ljava/lang/Integer;)Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;

    move-result-object v3

    .line 810
    invoke-virtual {v3}, Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;->build()Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    move-result-object v6

    .line 812
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "NavigationSessionStatusEvent new data duration["

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v18, "] traffic["

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v18, "] length["

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v18, "]"

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 817
    .end local v8    # "freeFlowDuration":I
    .end local v10    # "length":I
    .end local v12    # "noTraffic":Ljava/util/Date;
    .end local v13    # "routeInfo":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    .end local v14    # "now":J
    .end local v16    # "traffic":Ljava/util/Date;
    .end local v17    # "trafficDuration":I
    :cond_2
    if-nez v5, :cond_3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$4;->val$sessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    sget-object v7, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_STOPPED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    if-ne v3, v7, :cond_3

    .line 818
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$100(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->getLastNavigationRequest()Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    move-result-object v9

    .line 819
    .local v9, "lastRequest":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    if-eqz v9, :cond_3

    .line 820
    iget-object v4, v9, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->label:Ljava/lang/String;

    .line 821
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$100(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->getLastRouteId()Ljava/lang/String;

    move-result-object v5

    .line 824
    .end local v9    # "lastRequest":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    :cond_3
    new-instance v2, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$4;->val$sessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;
    invoke-static {v7}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$100(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    move-result-object v7

    iget-object v7, v7, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destinationIdentifier:Ljava/lang/String;

    invoke-direct/range {v2 .. v7}, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;-><init>(Lcom/navdy/service/library/events/navigation/NavigationSessionState;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationRouteResult;Ljava/lang/String;)V

    .line 825
    .local v2, "navigationSessionState":Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;
    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->mapsEventHandler:Lcom/navdy/hud/app/maps/MapsEventHandler;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$800()Lcom/navdy/hud/app/maps/MapsEventHandler;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/navdy/hud/app/maps/MapsEventHandler;->sendEventToClient(Lcom/squareup/wire/Message;)V

    .line 826
    sget-object v7, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "posted NavigationSessionStatusEvent state["

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, v2, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->sessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v18, "] label["

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, v2, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->label:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v18, "] routeId["

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, v2, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->routeId:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v18, "]"

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v18, "] result["

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    if-nez v6, :cond_4

    const-string v3, "null"

    :goto_0
    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v18, "]"

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 831
    return-void

    .line 826
    :cond_4
    iget-object v3, v6, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->via:Ljava/lang/String;

    goto :goto_0
.end method
