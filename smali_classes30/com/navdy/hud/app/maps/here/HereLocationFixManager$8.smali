.class Lcom/navdy/hud/app/maps/here/HereLocationFixManager$8;
.super Ljava/lang/Object;
.source "HereLocationFixManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->recordHereLocation(Lcom/here/android/mpa/common/GeoPosition;Lcom/here/android/mpa/common/PositioningManager$LocationMethod;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

.field final synthetic val$geoPosition:Lcom/here/android/mpa/common/GeoPosition;

.field final synthetic val$isMapMatched:Z

.field final synthetic val$locationMethod:Lcom/here/android/mpa/common/PositioningManager$LocationMethod;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;Lcom/here/android/mpa/common/GeoPosition;Lcom/here/android/mpa/common/PositioningManager$LocationMethod;Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    .prologue
    .line 449
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$8;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$8;->val$geoPosition:Lcom/here/android/mpa/common/GeoPosition;

    iput-object p3, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$8;->val$locationMethod:Lcom/here/android/mpa/common/PositioningManager$LocationMethod;

    iput-boolean p4, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$8;->val$isMapMatched:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 453
    :try_start_0
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$8;->val$geoPosition:Lcom/here/android/mpa/common/GeoPosition;

    invoke-virtual {v5}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v1

    .line 454
    .local v1, "coordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    const-string v0, ""

    .line 455
    .local v0, "additional":Ljava/lang/String;
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$8;->val$geoPosition:Lcom/here/android/mpa/common/GeoPosition;

    instance-of v5, v5, Lcom/here/android/mpa/common/MatchedGeoPosition;

    if-eqz v5, :cond_0

    .line 456
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$8;->val$geoPosition:Lcom/here/android/mpa/common/GeoPosition;

    check-cast v3, Lcom/here/android/mpa/common/MatchedGeoPosition;

    .line 457
    .local v3, "matchedGeoPosition":Lcom/here/android/mpa/common/MatchedGeoPosition;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Lcom/here/android/mpa/common/MatchedGeoPosition;->getMatchQuality()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 458
    invoke-virtual {v3}, Lcom/here/android/mpa/common/MatchedGeoPosition;->isExtrapolated()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 459
    invoke-virtual {v3}, Lcom/here/android/mpa/common/MatchedGeoPosition;->isOnStreet()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 461
    .end local v3    # "matchedGeoPosition":Lcom/here/android/mpa/common/MatchedGeoPosition;
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 462
    invoke-virtual {v1}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$8;->val$geoPosition:Lcom/here/android/mpa/common/GeoPosition;

    .line 463
    invoke-virtual {v6}, Lcom/here/android/mpa/common/GeoPosition;->getHeading()D

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$8;->val$geoPosition:Lcom/here/android/mpa/common/GeoPosition;

    .line 464
    invoke-virtual {v6}, Lcom/here/android/mpa/common/GeoPosition;->getSpeed()D

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$8;->val$geoPosition:Lcom/here/android/mpa/common/GeoPosition;

    .line 465
    invoke-virtual {v6}, Lcom/here/android/mpa/common/GeoPosition;->getLatitudeAccuracy()F

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 466
    invoke-virtual {v1}, Lcom/here/android/mpa/common/GeoCoordinate;->getAltitude()D

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$8;->val$geoPosition:Lcom/here/android/mpa/common/GeoPosition;

    .line 467
    invoke-virtual {v6}, Lcom/here/android/mpa/common/GeoPosition;->getTimestamp()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "here"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$8;->val$locationMethod:Lcom/here/android/mpa/common/PositioningManager$LocationMethod;

    .line 469
    invoke-virtual {v6}, Lcom/here/android/mpa/common/PositioningManager$LocationMethod;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$8;->val$isMapMatched:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 474
    .local v2, "locationString":Ljava/lang/String;
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$8;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->recordingFile:Ljava/io/FileOutputStream;
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$1500(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Ljava/io/FileOutputStream;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 478
    .end local v0    # "additional":Ljava/lang/String;
    .end local v1    # "coordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v2    # "locationString":Ljava/lang/String;
    :goto_0
    return-void

    .line 475
    :catch_0
    move-exception v4

    .line 476
    .local v4, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
