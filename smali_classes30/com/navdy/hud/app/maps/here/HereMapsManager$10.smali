.class Lcom/navdy/hud/app/maps/here/HereMapsManager$10;
.super Ljava/lang/Object;
.source "HereMapsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereMapsManager;->clearMapTraffic()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereMapsManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .prologue
    .line 975
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$10;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 978
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$10;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->map:Lcom/here/android/mpa/mapping/Map;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$300(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Lcom/here/android/mpa/mapping/Map;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/here/android/mpa/mapping/Map;->setTrafficInfoVisible(Z)Lcom/here/android/mpa/mapping/Map;

    .line 979
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$10;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->map:Lcom/here/android/mpa/mapping/Map;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$300(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Lcom/here/android/mpa/mapping/Map;

    move-result-object v1

    invoke-virtual {v1}, Lcom/here/android/mpa/mapping/Map;->getMapTrafficLayer()Lcom/here/android/mpa/mapping/MapTrafficLayer;

    move-result-object v0

    .line 980
    .local v0, "trafficLayer":Lcom/here/android/mpa/mapping/MapTrafficLayer;
    sget-object v1, Lcom/here/android/mpa/mapping/TrafficEvent$Severity;->NORMAL:Lcom/here/android/mpa/mapping/TrafficEvent$Severity;

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/mapping/MapTrafficLayer;->setDisplayFilter(Lcom/here/android/mpa/mapping/TrafficEvent$Severity;)Z

    .line 981
    sget-object v1, Lcom/here/android/mpa/mapping/MapTrafficLayer$RenderLayer;->ONROUTE:Lcom/here/android/mpa/mapping/MapTrafficLayer$RenderLayer;

    invoke-virtual {v0, v1, v2}, Lcom/here/android/mpa/mapping/MapTrafficLayer;->setEnabled(Lcom/here/android/mpa/mapping/MapTrafficLayer$RenderLayer;Z)V

    .line 982
    sget-object v1, Lcom/here/android/mpa/mapping/MapTrafficLayer$RenderLayer;->FLOW:Lcom/here/android/mpa/mapping/MapTrafficLayer$RenderLayer;

    invoke-virtual {v0, v1, v2}, Lcom/here/android/mpa/mapping/MapTrafficLayer;->setEnabled(Lcom/here/android/mpa/mapping/MapTrafficLayer$RenderLayer;Z)V

    .line 983
    sget-object v1, Lcom/here/android/mpa/mapping/MapTrafficLayer$RenderLayer;->INCIDENT:Lcom/here/android/mpa/mapping/MapTrafficLayer$RenderLayer;

    invoke-virtual {v0, v1, v2}, Lcom/here/android/mpa/mapping/MapTrafficLayer;->setEnabled(Lcom/here/android/mpa/mapping/MapTrafficLayer$RenderLayer;Z)V

    .line 984
    return-void
.end method
