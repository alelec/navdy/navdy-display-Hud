.class Lcom/navdy/hud/app/maps/here/HereMapsManager$3;
.super Ljava/lang/Object;
.source "HereMapsManager.java"

# interfaces
.implements Lcom/here/android/mpa/common/PositioningManager$OnPositionChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/here/HereMapsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereMapsManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .prologue
    .line 491
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPositionFixChanged(Lcom/here/android/mpa/common/PositioningManager$LocationMethod;Lcom/here/android/mpa/common/PositioningManager$LocationStatus;)V
    .locals 3
    .param p1, "locationMethod"    # Lcom/here/android/mpa/common/PositioningManager$LocationMethod;
    .param p2, "locationStatus"    # Lcom/here/android/mpa/common/PositioningManager$LocationStatus;

    .prologue
    .line 577
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$2;-><init>(Lcom/navdy/hud/app/maps/here/HereMapsManager$3;Lcom/here/android/mpa/common/PositioningManager$LocationMethod;Lcom/here/android/mpa/common/PositioningManager$LocationStatus;)V

    const/16 v2, 0x12

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 602
    return-void
.end method

.method public onPositionUpdated(Lcom/here/android/mpa/common/PositioningManager$LocationMethod;Lcom/here/android/mpa/common/GeoPosition;Z)V
    .locals 3
    .param p1, "locationMethod"    # Lcom/here/android/mpa/common/PositioningManager$LocationMethod;
    .param p2, "geoPosition"    # Lcom/here/android/mpa/common/GeoPosition;
    .param p3, "isMapMatched"    # Z

    .prologue
    .line 496
    if-nez p2, :cond_0

    .line 572
    :goto_0
    return-void

    .line 500
    :cond_0
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;

    invoke-direct {v1, p0, p2, p1, p3}, Lcom/navdy/hud/app/maps/here/HereMapsManager$3$1;-><init>(Lcom/navdy/hud/app/maps/here/HereMapsManager$3;Lcom/here/android/mpa/common/GeoPosition;Lcom/here/android/mpa/common/PositioningManager$LocationMethod;Z)V

    const/16 v2, 0x12

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method
