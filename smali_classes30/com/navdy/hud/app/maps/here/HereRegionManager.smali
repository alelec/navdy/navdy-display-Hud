.class public Lcom/navdy/hud/app/maps/here/HereRegionManager;
.super Ljava/lang/Object;
.source "HereRegionManager.java"


# static fields
.field private static final REGION_CHECK_INTERVAL:J = 0xdbba0L

.field private static final sInstance:Lcom/navdy/hud/app/maps/here/HereRegionManager;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field bus:Lcom/squareup/otto/Bus;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final checkRegionRunnable:Ljava/lang/Runnable;

.field private currentRegionEvent:Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;

.field private final handler:Landroid/os/Handler;

.field private final hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 29
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/maps/here/HereRegionManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereRegionManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 33
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereRegionManager;

    invoke-direct {v0}, Lcom/navdy/hud/app/maps/here/HereRegionManager;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereRegionManager;->sInstance:Lcom/navdy/hud/app/maps/here/HereRegionManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereRegionManager$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/here/HereRegionManager$1;-><init>(Lcom/navdy/hud/app/maps/here/HereRegionManager;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRegionManager;->checkRegionRunnable:Ljava/lang/Runnable;

    .line 55
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 57
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRegionManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .line 58
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRegionManager;->handler:Landroid/os/Handler;

    .line 59
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRegionManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereRegionManager;->checkRegionRunnable:Ljava/lang/Runnable;

    const-wide/32 v2, 0xdbba0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 60
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/maps/here/HereRegionManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereRegionManager;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereRegionManager;->checkRegion()V

    return-void
.end method

.method static synthetic access$100()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereRegionManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/maps/here/HereRegionManager;)Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereRegionManager;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRegionManager;->currentRegionEvent:Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;

    return-object v0
.end method

.method static synthetic access$202(Lcom/navdy/hud/app/maps/here/HereRegionManager;Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;)Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereRegionManager;
    .param p1, "x1"    # Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereRegionManager;->currentRegionEvent:Lcom/navdy/hud/app/maps/MapEvents$RegionEvent;

    return-object p1
.end method

.method private checkRegion()V
    .locals 10

    .prologue
    const-wide/32 v8, 0xdbba0

    .line 64
    :try_start_0
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereRegionManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLastGeoPosition()Lcom/here/android/mpa/common/GeoPosition;

    move-result-object v1

    .line 65
    .local v1, "geoPosition":Lcom/here/android/mpa/common/GeoPosition;
    if-nez v1, :cond_0

    .line 66
    sget-object v4, Lcom/navdy/hud/app/maps/here/HereRegionManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "invalid checkRegion call with unknown geoPosition"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 107
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereRegionManager;->handler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereRegionManager;->checkRegionRunnable:Ljava/lang/Runnable;

    invoke-virtual {v4, v5, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 109
    .end local v1    # "geoPosition":Lcom/here/android/mpa/common/GeoPosition;
    :goto_0
    return-void

    .line 70
    .restart local v1    # "geoPosition":Lcom/here/android/mpa/common/GeoPosition;
    :cond_0
    :try_start_1
    new-instance v2, Lcom/here/android/mpa/search/ReverseGeocodeRequest2;

    invoke-virtual {v1}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v4

    invoke-direct {v2, v4}, Lcom/here/android/mpa/search/ReverseGeocodeRequest2;-><init>(Lcom/here/android/mpa/common/GeoCoordinate;)V

    .line 72
    .local v2, "rGeo":Lcom/here/android/mpa/search/ReverseGeocodeRequest2;
    new-instance v4, Lcom/navdy/hud/app/maps/here/HereRegionManager$2;

    invoke-direct {v4, p0}, Lcom/navdy/hud/app/maps/here/HereRegionManager$2;-><init>(Lcom/navdy/hud/app/maps/here/HereRegionManager;)V

    invoke-virtual {v2, v4}, Lcom/here/android/mpa/search/ReverseGeocodeRequest2;->execute(Lcom/here/android/mpa/search/ResultListener;)Lcom/here/android/mpa/search/ErrorCode;

    move-result-object v0

    .line 100
    .local v0, "error":Lcom/here/android/mpa/search/ErrorCode;
    sget-object v4, Lcom/here/android/mpa/search/ErrorCode;->NONE:Lcom/here/android/mpa/search/ErrorCode;

    if-eq v0, v4, :cond_1

    .line 101
    sget-object v4, Lcom/navdy/hud/app/maps/here/HereRegionManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "reverseGeoCode:execute: Error["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/here/android/mpa/search/ErrorCode;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 107
    :cond_1
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereRegionManager;->handler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereRegionManager;->checkRegionRunnable:Ljava/lang/Runnable;

    invoke-virtual {v4, v5, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 103
    .end local v0    # "error":Lcom/here/android/mpa/search/ErrorCode;
    .end local v1    # "geoPosition":Lcom/here/android/mpa/common/GeoPosition;
    .end local v2    # "rGeo":Lcom/here/android/mpa/search/ReverseGeocodeRequest2;
    :catch_0
    move-exception v3

    .line 104
    .local v3, "t":Ljava/lang/Throwable;
    :try_start_2
    sget-object v4, Lcom/navdy/hud/app/maps/here/HereRegionManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v4, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 105
    invoke-static {}, Lcom/navdy/hud/app/util/CrashReporter;->getInstance()Lcom/navdy/hud/app/util/CrashReporter;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/navdy/hud/app/util/CrashReporter;->reportNonFatalException(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 107
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereRegionManager;->handler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereRegionManager;->checkRegionRunnable:Ljava/lang/Runnable;

    invoke-virtual {v4, v5, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .end local v3    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v4

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereRegionManager;->handler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereRegionManager;->checkRegionRunnable:Ljava/lang/Runnable;

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    throw v4
.end method

.method public static getInstance()Lcom/navdy/hud/app/maps/here/HereRegionManager;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereRegionManager;->sInstance:Lcom/navdy/hud/app/maps/here/HereRegionManager;

    return-object v0
.end method
