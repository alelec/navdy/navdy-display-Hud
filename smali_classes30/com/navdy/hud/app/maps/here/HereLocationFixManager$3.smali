.class Lcom/navdy/hud/app/maps/here/HereLocationFixManager$3;
.super Ljava/lang/Object;
.source "HereLocationFixManager.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereLocationFixManager;-><init>(Lcom/squareup/otto/Bus;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

.field toastSent:Z

.field final synthetic val$locationManager:Landroid/location/LocationManager;

.field final synthetic val$t1:J


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;Landroid/location/LocationManager;J)V
    .locals 1
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    .prologue
    .line 199
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$3;->val$locationManager:Landroid/location/LocationManager;

    iput-wide p3, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$3;->val$t1:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 200
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$3;->toastSent:Z

    return-void
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .locals 8
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    const-wide/16 v6, 0x2710

    .line 205
    iget-boolean v2, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$3;->toastSent:Z

    if-eqz v2, :cond_0

    .line 206
    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "ublox first fix toast already posted"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 223
    :goto_0
    return-void

    .line 209
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$3;->val$locationManager:Landroid/location/LocationManager;

    invoke-virtual {v2, p0}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 210
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$3;->toastSent:Z

    .line 211
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$3;->val$t1:J

    sub-long v0, v2, v4

    .line 212
    .local v0, "t2":J
    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "got first u-blox fix, unregister ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 213
    cmp-long v2, v0, v6

    if-gez v2, :cond_1

    .line 214
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$700(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$3$1;

    invoke-direct {v3, p0}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$3$1;-><init>(Lcom/navdy/hud/app/maps/here/HereLocationFixManager$3;)V

    sub-long v4, v6, v0

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 221
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/framework/voice/TTSUtils;->debugShowGotUbloxFix()V

    goto :goto_0
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 235
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 231
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;
    .param p2, "status"    # I
    .param p3, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 227
    return-void
.end method
