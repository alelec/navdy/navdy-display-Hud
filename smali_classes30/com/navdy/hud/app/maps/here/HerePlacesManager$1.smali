.class final Lcom/navdy/hud/app/maps/here/HerePlacesManager$1;
.super Ljava/lang/Object;
.source "HerePlacesManager.java"

# interfaces
.implements Lcom/here/android/mpa/search/ResultListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HerePlacesManager;->searchPlaces(Lcom/here/android/mpa/common/GeoCoordinate;Ljava/lang/String;IILcom/navdy/hud/app/maps/here/HerePlacesManager$PlacesSearchCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/here/android/mpa/search/ResultListener",
        "<",
        "Lcom/here/android/mpa/search/DiscoveryResultPage;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$callBack:Lcom/navdy/hud/app/maps/here/HerePlacesManager$PlacesSearchCallback;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HerePlacesManager$PlacesSearchCallback;)V
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$1;->val$callBack:Lcom/navdy/hud/app/maps/here/HerePlacesManager$PlacesSearchCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted(Lcom/here/android/mpa/search/DiscoveryResultPage;Lcom/here/android/mpa/search/ErrorCode;)V
    .locals 2
    .param p1, "discoveryResultPage"    # Lcom/here/android/mpa/search/DiscoveryResultPage;
    .param p2, "errorCode"    # Lcom/here/android/mpa/search/ErrorCode;

    .prologue
    .line 115
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$1;->val$callBack:Lcom/navdy/hud/app/maps/here/HerePlacesManager$PlacesSearchCallback;

    invoke-interface {v1, p2, p1}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$PlacesSearchCallback;->result(Lcom/here/android/mpa/search/ErrorCode;Lcom/here/android/mpa/search/DiscoveryResultPage;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 119
    :goto_0
    return-void

    .line 116
    :catch_0
    move-exception v0

    .line 117
    .local v0, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public bridge synthetic onCompleted(Ljava/lang/Object;Lcom/here/android/mpa/search/ErrorCode;)V
    .locals 0

    .prologue
    .line 111
    check-cast p1, Lcom/here/android/mpa/search/DiscoveryResultPage;

    invoke-virtual {p0, p1, p2}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$1;->onCompleted(Lcom/here/android/mpa/search/DiscoveryResultPage;Lcom/here/android/mpa/search/ErrorCode;)V

    return-void
.end method
