.class public Lcom/navdy/hud/app/maps/here/HereMapAnimator;
.super Ljava/lang/Object;
.source "HereMapAnimator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;
    }
.end annotation


# static fields
.field private static final DRASTIC_HEADING_CHANGE:I = 0x2d

.field private static final INVALID_STEP:I = -0x1

.field private static final INVALID_TILT:I = -0x1

.field private static final INVALID_ZOOM:I = -0x1

.field private static final MAX_DIFFERENCE_HEADING_FILTER:I = 0x78

.field private static final MAX_HEADING:I = 0x168

.field private static final MAX_INTERVAL_HEADING_UPDATE:J = 0xbb8L

.field private static final MIN_HEADING:I = 0x0

.field private static final MIN_SPEED_FILTER:D = 0.44704

.field private static final MPH_TO_MS:D = 0.44704

.field private static final REFRESH_TIME_THROTTLE:J

.field private static final geoPositionLock:Ljava/lang/Object;

.field private static final logger:Lcom/navdy/service/library/log/Logger;

.field private static final tiltLock:Ljava/lang/Object;

.field private static final zoomLock:Ljava/lang/Object;


# instance fields
.field private currentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

.field private currentTilt:F

.field private currentZoom:D

.field private geoPositionUpdateInterval:J

.field private lastGeoPositionUpdateTime:J

.field private volatile lastHeading:F

.field private lastPreDrawTime:J

.field private lastTiltUpdateTime:J

.field private lastZoomUpdateTime:J

.field private mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

.field private mapRenderListener:Lcom/here/android/mpa/mapping/OnMapRenderListener;

.field private mode:Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;

.field private navController:Lcom/navdy/hud/app/maps/here/HereNavController;

.field private preDrawFinishTime:J

.field private volatile renderingEnabled:Z

.field private tiltUpdateInterval:J

.field private zoomUpdateInterval:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 44
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->logger:Lcom/navdy/service/library/log/Logger;

    .line 57
    const/16 v0, 0x3e8

    invoke-static {}, Lcom/navdy/hud/app/maps/MapSettings;->getMapFps()I

    move-result v1

    div-int/2addr v0, v1

    int-to-long v0, v0

    sput-wide v0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->REFRESH_TIME_THROTTLE:J

    .line 87
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->geoPositionLock:Ljava/lang/Object;

    .line 88
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->zoomLock:Ljava/lang/Object;

    .line 89
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->tiltLock:Ljava/lang/Object;

    return-void
.end method

.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;Lcom/navdy/hud/app/maps/here/HereMapController;Lcom/navdy/hud/app/maps/here/HereNavController;)V
    .locals 4
    .param p1, "mode"    # Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;
    .param p2, "mapController"    # Lcom/navdy/hud/app/maps/here/HereMapController;
    .param p3, "navController"    # Lcom/navdy/hud/app/maps/here/HereNavController;

    .prologue
    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;->NONE:Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->mode:Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;

    .line 102
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->lastHeading:F

    .line 148
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->renderingEnabled:Z

    .line 151
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->mode:Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;

    .line 152
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;->NONE:Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;

    if-eq p1, v0, :cond_0

    .line 153
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "animation throttle = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-wide v2, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->REFRESH_TIME_THROTTLE:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 154
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/here/HereMapAnimator$1;-><init>(Lcom/navdy/hud/app/maps/here/HereMapAnimator;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->mapRenderListener:Lcom/here/android/mpa/mapping/OnMapRenderListener;

    .line 176
    :cond_0
    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    .line 177
    iput-object p3, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->navController:Lcom/navdy/hud/app/maps/here/HereNavController;

    .line 178
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->currentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

    .line 179
    invoke-virtual {p2}, Lcom/navdy/hud/app/maps/here/HereMapController;->getZoomLevel()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->currentZoom:D

    .line 180
    invoke-virtual {p2}, Lcom/navdy/hud/app/maps/here/HereMapController;->getTilt()F

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->currentTilt:F

    .line 181
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/maps/here/HereMapAnimator;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->onPreDraw()V

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/maps/here/HereMapAnimator;ZJ)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapAnimator;
    .param p1, "x1"    # Z
    .param p2, "x2"    # J

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->onPostDraw(ZJ)V

    return-void
.end method

.method static synthetic access$1000(Lcom/navdy/hud/app/maps/here/HereMapAnimator;)J
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->lastZoomUpdateTime:J

    return-wide v0
.end method

.method static synthetic access$1002(Lcom/navdy/hud/app/maps/here/HereMapAnimator;J)J
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapAnimator;
    .param p1, "x1"    # J

    .prologue
    .line 43
    iput-wide p1, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->lastZoomUpdateTime:J

    return-wide p1
.end method

.method static synthetic access$1100(Lcom/navdy/hud/app/maps/here/HereMapAnimator;D)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapAnimator;
    .param p1, "x1"    # D

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->isValidZoom(D)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1200()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->zoomLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/navdy/hud/app/maps/here/HereMapAnimator;J)J
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapAnimator;
    .param p1, "x1"    # J

    .prologue
    .line 43
    iput-wide p1, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->zoomUpdateInterval:J

    return-wide p1
.end method

.method static synthetic access$1402(Lcom/navdy/hud/app/maps/here/HereMapAnimator;D)D
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapAnimator;
    .param p1, "x1"    # D

    .prologue
    .line 43
    iput-wide p1, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->currentZoom:D

    return-wide p1
.end method

.method static synthetic access$1500(Lcom/navdy/hud/app/maps/here/HereMapAnimator;)J
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->lastTiltUpdateTime:J

    return-wide v0
.end method

.method static synthetic access$1502(Lcom/navdy/hud/app/maps/here/HereMapAnimator;J)J
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapAnimator;
    .param p1, "x1"    # J

    .prologue
    .line 43
    iput-wide p1, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->lastTiltUpdateTime:J

    return-wide p1
.end method

.method static synthetic access$1600(Lcom/navdy/hud/app/maps/here/HereMapAnimator;F)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapAnimator;
    .param p1, "x1"    # F

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->isValidTilt(F)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1700()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->tiltLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1802(Lcom/navdy/hud/app/maps/here/HereMapAnimator;J)J
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapAnimator;
    .param p1, "x1"    # J

    .prologue
    .line 43
    iput-wide p1, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->tiltUpdateInterval:J

    return-wide p1
.end method

.method static synthetic access$1902(Lcom/navdy/hud/app/maps/here/HereMapAnimator;F)F
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapAnimator;
    .param p1, "x1"    # F

    .prologue
    .line 43
    iput p1, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->currentTilt:F

    return p1
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/maps/here/HereMapAnimator;)J
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->lastGeoPositionUpdateTime:J

    return-wide v0
.end method

.method static synthetic access$2000(Lcom/navdy/hud/app/maps/here/HereMapAnimator;)Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->mode:Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;

    return-object v0
.end method

.method static synthetic access$202(Lcom/navdy/hud/app/maps/here/HereMapAnimator;J)J
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapAnimator;
    .param p1, "x1"    # J

    .prologue
    .line 43
    iput-wide p1, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->lastGeoPositionUpdateTime:J

    return-wide p1
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/maps/here/HereMapAnimator;Lcom/here/android/mpa/common/GeoPosition;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapAnimator;
    .param p1, "x1"    # Lcom/here/android/mpa/common/GeoPosition;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->isValidGeoPosition(Lcom/here/android/mpa/common/GeoPosition;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$400()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$500()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->geoPositionLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$602(Lcom/navdy/hud/app/maps/here/HereMapAnimator;J)J
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapAnimator;
    .param p1, "x1"    # J

    .prologue
    .line 43
    iput-wide p1, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->geoPositionUpdateInterval:J

    return-wide p1
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/maps/here/HereMapAnimator;)Lcom/here/android/mpa/common/GeoPosition;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->currentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

    return-object v0
.end method

.method static synthetic access$702(Lcom/navdy/hud/app/maps/here/HereMapAnimator;Lcom/here/android/mpa/common/GeoPosition;)Lcom/here/android/mpa/common/GeoPosition;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapAnimator;
    .param p1, "x1"    # Lcom/here/android/mpa/common/GeoPosition;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->currentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

    return-object p1
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/maps/here/HereMapAnimator;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->renderingEnabled:Z

    return v0
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/maps/here/HereMapAnimator;)Lcom/navdy/hud/app/maps/here/HereMapController;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    return-object v0
.end method

.method private getNewCenter(Lcom/here/android/mpa/common/GeoPosition;D)Lcom/here/android/mpa/common/GeoCoordinate;
    .locals 14
    .param p1, "geoPosition"    # Lcom/here/android/mpa/common/GeoPosition;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "step"    # D

    .prologue
    .line 530
    iget-object v10, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {v10}, Lcom/navdy/hud/app/maps/here/HereMapController;->getCenter()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v0

    .line 532
    .local v0, "currentCenter":Lcom/here/android/mpa/common/GeoCoordinate;
    if-eqz p1, :cond_0

    const-wide/high16 v10, -0x4010000000000000L    # -1.0

    cmpl-double v10, p2, v10

    if-nez v10, :cond_1

    .line 544
    .end local v0    # "currentCenter":Lcom/here/android/mpa/common/GeoCoordinate;
    :cond_0
    :goto_0
    return-object v0

    .line 536
    .restart local v0    # "currentCenter":Lcom/here/android/mpa/common/GeoCoordinate;
    :cond_1
    invoke-virtual {p1}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v1

    .line 538
    .local v1, "newCoords":Lcom/here/android/mpa/common/GeoCoordinate;
    invoke-virtual {v1}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v10

    invoke-virtual {v0}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v12

    sub-double v6, v10, v12

    .line 539
    .local v6, "latVector":D
    invoke-virtual {v1}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v10

    invoke-virtual {v0}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v12

    sub-double v8, v10, v12

    .line 541
    .local v8, "lngVector":D
    invoke-virtual {v0}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v10

    mul-double v12, v6, p2

    add-double v2, v10, v12

    .line 542
    .local v2, "interpolatedLat":D
    invoke-virtual {v0}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v10

    mul-double v12, v8, p2

    add-double v4, v10, v12

    .line 544
    .local v4, "interpolatedLng":D
    new-instance v0, Lcom/here/android/mpa/common/GeoCoordinate;

    .end local v0    # "currentCenter":Lcom/here/android/mpa/common/GeoCoordinate;
    invoke-direct {v0, v2, v3, v4, v5}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    goto :goto_0
.end method

.method private getNewHeading(Lcom/here/android/mpa/common/GeoPosition;D)F
    .locals 12
    .param p1, "geoPosition"    # Lcom/here/android/mpa/common/GeoPosition;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "step"    # D

    .prologue
    const-wide v10, 0x4076800000000000L    # 360.0

    .line 557
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {v6}, Lcom/navdy/hud/app/maps/here/HereMapController;->getOrientation()F

    move-result v0

    .line 559
    .local v0, "currentOrientation":F
    if-eqz p1, :cond_0

    const-wide/high16 v6, -0x4010000000000000L    # -1.0

    cmpl-double v6, p2, v6

    if-nez v6, :cond_1

    .line 574
    .end local v0    # "currentOrientation":F
    :cond_0
    :goto_0
    return v0

    .line 563
    .restart local v0    # "currentOrientation":F
    :cond_1
    invoke-virtual {p1}, Lcom/here/android/mpa/common/GeoPosition;->getHeading()D

    move-result-wide v6

    double-to-float v1, v6

    .line 565
    .local v1, "newOrientation":F
    float-to-double v6, v1

    float-to-double v8, v0

    invoke-direct {p0, v6, v7, v8, v9}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->getOrientationDiff(DD)D

    move-result-wide v6

    mul-double v4, p2, v6

    .line 566
    .local v4, "orientationDiff":D
    float-to-double v6, v0

    add-double v2, v6, v4

    .line 568
    .local v2, "newHeading":D
    cmpl-double v6, v2, v10

    if-lez v6, :cond_3

    .line 569
    sub-double/2addr v2, v10

    .line 574
    :cond_2
    :goto_1
    double-to-float v0, v2

    goto :goto_0

    .line 570
    :cond_3
    const-wide/16 v6, 0x0

    cmpg-double v6, v2, v6

    if-gez v6, :cond_2

    .line 571
    add-double/2addr v2, v10

    goto :goto_1
.end method

.method private getNewTilt(FD)F
    .locals 6
    .param p1, "tilt"    # F
    .param p2, "step"    # D

    .prologue
    const/high16 v0, -0x40800000    # -1.0f

    .line 606
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->mode:Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;

    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;->ZOOM_TILT_ANIMATION:Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;

    if-ne v2, v3, :cond_0

    cmpl-float v2, p1, v0

    if-eqz v2, :cond_0

    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    cmpl-double v2, p2, v2

    if-nez v2, :cond_1

    .line 614
    :cond_0
    :goto_0
    return v0

    .line 610
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereMapController;->getTilt()F

    move-result v1

    .line 612
    .local v1, "currentTilt":F
    float-to-double v2, v1

    sub-float v4, p1, v1

    float-to-double v4, v4

    mul-double/2addr v4, p2

    add-double/2addr v2, v4

    double-to-float v0, v2

    .line 613
    .local v0, "animatorTilt":F
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "animatorTilt: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getNewZoomLevel(DD)D
    .locals 7
    .param p1, "zoom"    # D
    .param p3, "step"    # D

    .prologue
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    .line 586
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->mode:Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;

    sget-object v5, Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;->ZOOM_TILT_ANIMATION:Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;

    if-ne v4, v5, :cond_0

    cmpl-double v4, p1, v0

    if-eqz v4, :cond_0

    cmpl-double v4, p3, v0

    if-nez v4, :cond_1

    .line 594
    :cond_0
    :goto_0
    return-wide v0

    .line 590
    :cond_1
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereMapController;->getZoomLevel()D

    move-result-wide v2

    .line 592
    .local v2, "currentZoomLevel":D
    sub-double v4, p1, v2

    mul-double/2addr v4, p3

    add-double v0, v2, v4

    .line 593
    .local v0, "animatorZoom":D
    sget-object v4, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "animatorZoom: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getOrientationDiff(DD)D
    .locals 7
    .param p1, "firstOrientation"    # D
    .param p3, "secondOrientation"    # D

    .prologue
    const-wide v4, 0x4076800000000000L    # 360.0

    .line 510
    sub-double v0, p1, p3

    .line 512
    .local v0, "orientationDiff":D
    const-wide v2, 0x4066800000000000L    # 180.0

    cmpl-double v2, v0, v2

    if-lez v2, :cond_1

    .line 513
    sub-double/2addr v0, v4

    .line 517
    :cond_0
    :goto_0
    return-wide v0

    .line 514
    :cond_1
    const-wide v2, -0x3f99800000000000L    # -180.0

    cmpg-double v2, v0, v2

    if-gez v2, :cond_0

    .line 515
    add-double/2addr v0, v4

    goto :goto_0
.end method

.method private isValidGeoPosition(Lcom/here/android/mpa/common/GeoPosition;)Z
    .locals 10
    .param p1, "geoPosition"    # Lcom/here/android/mpa/common/GeoPosition;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    .line 464
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->geoPositionLock:Ljava/lang/Object;

    monitor-enter v6

    .line 465
    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->currentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

    .line 466
    .local v0, "currentGeoPositionCopy":Lcom/here/android/mpa/common/GeoPosition;
    iget-wide v2, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->geoPositionUpdateInterval:J

    .line 467
    .local v2, "geoPositionUpdateIntervalCopy":J
    monitor-exit v6

    .line 469
    if-nez v0, :cond_1

    .line 477
    :cond_0
    :goto_0
    return v1

    .line 467
    .end local v0    # "currentGeoPositionCopy":Lcom/here/android/mpa/common/GeoPosition;
    .end local v2    # "geoPositionUpdateIntervalCopy":J
    :catchall_0
    move-exception v1

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 474
    .restart local v0    # "currentGeoPositionCopy":Lcom/here/android/mpa/common/GeoPosition;
    .restart local v2    # "geoPositionUpdateIntervalCopy":J
    :cond_1
    invoke-virtual {p1}, Lcom/here/android/mpa/common/GeoPosition;->getHeading()D

    move-result-wide v6

    invoke-virtual {v0}, Lcom/here/android/mpa/common/GeoPosition;->getHeading()D

    move-result-wide v8

    invoke-direct {p0, v6, v7, v8, v9}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->getOrientationDiff(DD)D

    move-result-wide v4

    .line 476
    .local v4, "orientationDiff":D
    invoke-virtual {p1}, Lcom/here/android/mpa/common/GeoPosition;->getSpeed()D

    move-result-wide v6

    const-wide v8, 0x3fdc9c4da9003eeaL    # 0.44704

    cmpl-double v6, v6, v8

    if-gez v6, :cond_0

    .line 477
    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    const-wide/high16 v8, 0x405e000000000000L    # 120.0

    cmpg-double v6, v6, v8

    if-lez v6, :cond_0

    const-wide/16 v6, 0xbb8

    cmp-long v6, v2, v6

    if-gez v6, :cond_0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isValidTilt(F)Z
    .locals 1
    .param p1, "tilt"    # F

    .prologue
    .line 498
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    const/high16 v0, 0x42b40000    # 90.0f

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isValidZoom(D)Z
    .locals 3
    .param p1, "zoom"    # D

    .prologue
    .line 488
    const-wide/16 v0, 0x0

    cmpl-double v0, p1, v0

    if-ltz v0, :cond_0

    const-wide/high16 v0, 0x4034000000000000L    # 20.0

    cmpg-double v0, p1, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onPostDraw(ZJ)V
    .locals 11
    .param p1, "invalidated"    # Z
    .param p2, "renderTime"    # J

    .prologue
    const/4 v10, 0x2

    .line 429
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->preDrawFinishTime:J

    sub-long v2, v6, v8

    .line 430
    .local v2, "lastRenderTime":J
    sget-wide v6, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->REFRESH_TIME_THROTTLE:J

    sub-long v4, v6, v2

    .line 432
    .local v4, "sleepTime":J
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v10}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 433
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "last render took "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ms"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 436
    :cond_0
    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-lez v1, :cond_3

    .line 437
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v10}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 438
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "sleeping render thread for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ms"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 441
    :cond_1
    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 450
    :cond_2
    :goto_0
    return-void

    .line 442
    :catch_0
    move-exception v0

    .line 443
    .local v0, "e":Ljava/lang/InterruptedException;
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 446
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_3
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v10}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 447
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "render thread could not sleep"

    invoke-virtual {v1, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private onPreDraw()V
    .locals 46

    .prologue
    .line 326
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->currentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

    if-nez v7, :cond_1

    .line 425
    :cond_0
    :goto_0
    return-void

    .line 330
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v28

    .line 333
    .local v28, "preDrawStartTime":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->lastPreDrawTime:J

    move-wide/from16 v42, v0

    const-wide/16 v44, 0x0

    cmp-long v7, v42, v44

    if-lez v7, :cond_5

    .line 334
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->lastPreDrawTime:J

    move-wide/from16 v42, v0

    sub-long v26, v28, v42

    .line 335
    .local v26, "preDrawInterval":J
    move-wide/from16 v0, v28

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->lastPreDrawTime:J

    .line 341
    const/4 v15, 0x0

    .line 342
    .local v15, "geoPosition":Lcom/here/android/mpa/common/GeoPosition;
    const-wide/high16 v36, -0x4010000000000000L    # -1.0

    .line 343
    .local v36, "zoom":D
    const/high16 v25, -0x40800000    # -1.0f

    .line 345
    .local v25, "tilt":F
    const-wide/high16 v18, -0x4010000000000000L    # -1.0

    .line 346
    .local v18, "geoPositionStep":D
    const-wide/high16 v38, -0x4010000000000000L    # -1.0

    .line 347
    .local v38, "zoomStep":D
    const-wide/high16 v32, -0x4010000000000000L    # -1.0

    .line 358
    .local v32, "tiltStep":D
    sget-object v9, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->geoPositionLock:Ljava/lang/Object;

    monitor-enter v9

    .line 359
    :try_start_0
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->geoPositionUpdateInterval:J

    move-wide/from16 v20, v0

    .line 360
    .local v20, "geoPositionUpdateIntervalCopy":J
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->currentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

    .line 361
    .local v6, "currentGeoPositionCopy":Lcom/here/android/mpa/common/GeoPosition;
    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 363
    sget-object v9, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->zoomLock:Ljava/lang/Object;

    monitor-enter v9

    .line 364
    :try_start_1
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->zoomUpdateInterval:J

    move-wide/from16 v40, v0

    .line 365
    .local v40, "zoomUpdateIntervalCopy":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->currentZoom:D

    move-wide/from16 v16, v0

    .line 366
    .local v16, "currentZoomCopy":D
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 368
    sget-object v9, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->tiltLock:Ljava/lang/Object;

    monitor-enter v9

    .line 369
    :try_start_2
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->tiltUpdateInterval:J

    move-wide/from16 v34, v0

    .line 370
    .local v34, "tiltUpdateIntervalCopy":J
    move-object/from16 v0, p0

    iget v14, v0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->currentTilt:F

    .line 371
    .local v14, "currentTiltCopy":F
    monitor-exit v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 373
    if-eqz v6, :cond_6

    const-wide/16 v42, 0x0

    cmp-long v7, v20, v42

    if-lez v7, :cond_6

    const/16 v22, 0x1

    .line 374
    .local v22, "hasGeoPositionUpdate":Z
    :goto_1
    const-wide/16 v42, 0x0

    cmpl-double v7, v16, v42

    if-ltz v7, :cond_7

    const-wide/16 v42, 0x0

    cmp-long v7, v40, v42

    if-lez v7, :cond_7

    const/16 v24, 0x1

    .line 375
    .local v24, "hasZoomUpdate":Z
    :goto_2
    const/4 v7, 0x0

    cmpl-float v7, v14, v7

    if-ltz v7, :cond_8

    const-wide/16 v42, 0x0

    cmp-long v7, v34, v42

    if-lez v7, :cond_8

    const/16 v23, 0x1

    .line 377
    .local v23, "hasTiltUpdate":Z
    :goto_3
    if-eqz v22, :cond_2

    .line 378
    move-object v15, v6

    .line 379
    move-wide/from16 v0, v26

    long-to-double v0, v0

    move-wide/from16 v42, v0

    move-wide/from16 v0, v20

    long-to-double v0, v0

    move-wide/from16 v44, v0

    div-double v18, v42, v44

    .line 382
    :cond_2
    if-eqz v24, :cond_3

    .line 383
    move-wide/from16 v36, v16

    .line 384
    move-wide/from16 v0, v26

    long-to-double v0, v0

    move-wide/from16 v42, v0

    move-wide/from16 v0, v40

    long-to-double v0, v0

    move-wide/from16 v44, v0

    div-double v38, v42, v44

    .line 387
    :cond_3
    if-eqz v23, :cond_4

    .line 388
    move/from16 v25, v14

    .line 389
    move-wide/from16 v0, v26

    long-to-double v0, v0

    move-wide/from16 v42, v0

    move-wide/from16 v0, v34

    long-to-double v0, v0

    move-wide/from16 v44, v0

    div-double v32, v42, v44

    .line 392
    :cond_4
    if-nez v22, :cond_9

    if-nez v24, :cond_9

    if-nez v23, :cond_9

    .line 393
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v42

    move-wide/from16 v0, v42

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->preDrawFinishTime:J

    goto/16 :goto_0

    .line 337
    .end local v6    # "currentGeoPositionCopy":Lcom/here/android/mpa/common/GeoPosition;
    .end local v14    # "currentTiltCopy":F
    .end local v15    # "geoPosition":Lcom/here/android/mpa/common/GeoPosition;
    .end local v16    # "currentZoomCopy":D
    .end local v18    # "geoPositionStep":D
    .end local v20    # "geoPositionUpdateIntervalCopy":J
    .end local v22    # "hasGeoPositionUpdate":Z
    .end local v23    # "hasTiltUpdate":Z
    .end local v24    # "hasZoomUpdate":Z
    .end local v25    # "tilt":F
    .end local v26    # "preDrawInterval":J
    .end local v32    # "tiltStep":D
    .end local v34    # "tiltUpdateIntervalCopy":J
    .end local v36    # "zoom":D
    .end local v38    # "zoomStep":D
    .end local v40    # "zoomUpdateIntervalCopy":J
    :cond_5
    move-wide/from16 v0, v28

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->lastPreDrawTime:J

    goto/16 :goto_0

    .line 361
    .restart local v15    # "geoPosition":Lcom/here/android/mpa/common/GeoPosition;
    .restart local v18    # "geoPositionStep":D
    .restart local v25    # "tilt":F
    .restart local v26    # "preDrawInterval":J
    .restart local v32    # "tiltStep":D
    .restart local v36    # "zoom":D
    .restart local v38    # "zoomStep":D
    :catchall_0
    move-exception v7

    :try_start_3
    monitor-exit v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v7

    .line 366
    .restart local v6    # "currentGeoPositionCopy":Lcom/here/android/mpa/common/GeoPosition;
    .restart local v20    # "geoPositionUpdateIntervalCopy":J
    :catchall_1
    move-exception v7

    :try_start_4
    monitor-exit v9
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v7

    .line 371
    .restart local v16    # "currentZoomCopy":D
    .restart local v40    # "zoomUpdateIntervalCopy":J
    :catchall_2
    move-exception v7

    :try_start_5
    monitor-exit v9
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v7

    .line 373
    .restart local v14    # "currentTiltCopy":F
    .restart local v34    # "tiltUpdateIntervalCopy":J
    :cond_6
    const/16 v22, 0x0

    goto :goto_1

    .line 374
    .restart local v22    # "hasGeoPositionUpdate":Z
    :cond_7
    const/16 v24, 0x0

    goto :goto_2

    .line 375
    .restart local v24    # "hasZoomUpdate":Z
    :cond_8
    const/16 v23, 0x0

    goto :goto_3

    .line 398
    .restart local v23    # "hasTiltUpdate":Z
    :cond_9
    move-wide/from16 v0, v38

    move-wide/from16 v2, v32

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v42

    move-wide/from16 v0, v18

    move-wide/from16 v2, v42

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v30

    .line 400
    .local v30, "step":D
    const-wide/high16 v42, 0x3ff0000000000000L    # 1.0

    cmpl-double v7, v30, v42

    if-lez v7, :cond_a

    .line 401
    const-wide/high16 v30, 0x3ff0000000000000L    # 1.0

    .line 404
    :cond_a
    move-object/from16 v0, p0

    move-wide/from16 v1, v30

    invoke-direct {v0, v15, v1, v2}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->getNewCenter(Lcom/here/android/mpa/common/GeoPosition;D)Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v8

    .line 405
    .local v8, "newCenter":Lcom/here/android/mpa/common/GeoCoordinate;
    move-object/from16 v0, p0

    move-wide/from16 v1, v30

    invoke-direct {v0, v15, v1, v2}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->getNewHeading(Lcom/here/android/mpa/common/GeoPosition;D)F

    move-result v12

    .line 407
    .local v12, "newHeading":F
    move-object/from16 v0, p0

    move-wide/from16 v1, v36

    move-wide/from16 v3, v30

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->getNewZoomLevel(DD)D

    move-result-wide v10

    .line 408
    .local v10, "newZoomLevel":D
    move-object/from16 v0, p0

    move/from16 v1, v25

    move-wide/from16 v2, v30

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->getNewTilt(FD)F

    move-result v13

    .line 410
    .local v13, "newTilt":F
    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->renderingEnabled:Z

    if-eqz v7, :cond_b

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {v7}, Lcom/navdy/hud/app/maps/here/HereMapController;->getState()Lcom/navdy/hud/app/maps/here/HereMapController$State;

    move-result-object v7

    sget-object v9, Lcom/navdy/hud/app/maps/here/HereMapController$State;->AR_MODE:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    if-ne v7, v9, :cond_b

    .line 411
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    sget-object v9, Lcom/here/android/mpa/mapping/Map$Animation;->NONE:Lcom/here/android/mpa/mapping/Map$Animation;

    invoke-virtual/range {v7 .. v13}, Lcom/navdy/hud/app/maps/here/HereMapController;->setCenter(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;DFF)V

    .line 420
    :cond_b
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v42

    move-wide/from16 v0, v42

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->preDrawFinishTime:J

    .line 422
    sget-object v7, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->logger:Lcom/navdy/service/library/log/Logger;

    const/4 v9, 0x2

    invoke-virtual {v7, v9}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 423
    sget-object v7, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v42, "predraw logic took "

    move-object/from16 v0, v42

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->preDrawFinishTime:J

    move-wide/from16 v42, v0

    sub-long v42, v42, v28

    move-wide/from16 v0, v42

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v42, " ms"

    move-object/from16 v0, v42

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public clearState()V
    .locals 2

    .prologue
    .line 649
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "clearState"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 650
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->lastHeading:F

    .line 651
    return-void
.end method

.method public getAnimationMode()Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->mode:Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;

    return-object v0
.end method

.method public getMapRenderListener()Lcom/here/android/mpa/mapping/OnMapRenderListener;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->mapRenderListener:Lcom/here/android/mpa/mapping/OnMapRenderListener;

    return-object v0
.end method

.method public setGeoPosition(Lcom/here/android/mpa/common/GeoPosition;)V
    .locals 14
    .param p1, "geoPosition"    # Lcom/here/android/mpa/common/GeoPosition;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/high16 v7, -0x40800000    # -1.0f

    .line 204
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapAnimator$6;->$SwitchMap$com$navdy$hud$app$maps$here$HereMapAnimator$AnimationMode:[I

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->mode:Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 228
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/maps/here/HereMapAnimator$2;

    invoke-direct {v2, p0, p1}, Lcom/navdy/hud/app/maps/here/HereMapAnimator$2;-><init>(Lcom/navdy/hud/app/maps/here/HereMapAnimator;Lcom/here/android/mpa/common/GeoPosition;)V

    const/16 v3, 0x11

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 264
    :cond_0
    :goto_0
    return-void

    .line 206
    :pswitch_0
    invoke-virtual {p1}, Lcom/here/android/mpa/common/GeoPosition;->getHeading()D

    move-result-wide v8

    .line 207
    .local v8, "heading":D
    const/4 v0, 0x0

    .line 208
    .local v0, "animate":Z
    iget v1, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->lastHeading:F

    cmpl-float v1, v1, v7

    if-eqz v1, :cond_2

    .line 209
    iget v1, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->lastHeading:F

    float-to-double v2, v1

    invoke-direct {p0, v8, v9, v2, v3}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->getOrientationDiff(DD)D

    move-result-wide v10

    .line 210
    .local v10, "orientationDiff":D
    invoke-virtual {p1}, Lcom/here/android/mpa/common/GeoPosition;->getSpeed()D

    move-result-wide v12

    .line 211
    .local v12, "speed":D
    const-wide v2, 0x3fdc9c4da9003eeaL    # 0.44704

    cmpg-double v1, v12, v2

    if-gtz v1, :cond_1

    invoke-static {v10, v11}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    const-wide/high16 v4, 0x405e000000000000L    # 120.0

    cmpl-double v1, v2, v4

    if-ltz v1, :cond_1

    .line 212
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "filtering out spurious heading last: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->lastHeading:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " new:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " speed:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12, v13}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 216
    :cond_1
    invoke-static {v10, v11}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    const-wide v4, 0x4046800000000000L    # 45.0

    cmpl-double v1, v2, v4

    if-ltz v1, :cond_2

    .line 217
    const/4 v0, 0x1

    .line 220
    .end local v10    # "orientationDiff":D
    .end local v12    # "speed":D
    :cond_2
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->currentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

    .line 221
    double-to-float v1, v8

    iput v1, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->lastHeading:F

    .line 222
    iget-boolean v1, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->renderingEnabled:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereMapController;->getState()Lcom/navdy/hud/app/maps/here/HereMapController$State;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/maps/here/HereMapController$State;->AR_MODE:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    if-ne v1, v2, :cond_0

    .line 223
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {p1}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v2

    if-eqz v0, :cond_3

    sget-object v3, Lcom/here/android/mpa/mapping/Map$Animation;->BOW:Lcom/here/android/mpa/mapping/Map$Animation;

    :goto_1
    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    iget v6, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->lastHeading:F

    invoke-virtual/range {v1 .. v7}, Lcom/navdy/hud/app/maps/here/HereMapController;->setCenter(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;DFF)V

    goto/16 :goto_0

    :cond_3
    sget-object v3, Lcom/here/android/mpa/mapping/Map$Animation;->NONE:Lcom/here/android/mpa/mapping/Map$Animation;

    goto :goto_1

    .line 204
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method setTilt(F)V
    .locals 3
    .param p1, "tilt"    # F

    .prologue
    .line 301
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereMapAnimator$4;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/maps/here/HereMapAnimator$4;-><init>(Lcom/navdy/hud/app/maps/here/HereMapAnimator;F)V

    const/16 v2, 0x11

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 322
    return-void
.end method

.method setZoom(D)V
    .locals 3
    .param p1, "zoom"    # D

    .prologue
    .line 272
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereMapAnimator$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/navdy/hud/app/maps/here/HereMapAnimator$3;-><init>(Lcom/navdy/hud/app/maps/here/HereMapAnimator;D)V

    const/16 v2, 0x11

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 293
    return-void
.end method

.method public startMapRendering()V
    .locals 3

    .prologue
    .line 618
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "startMapRendering: rendering enabled"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 619
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->renderingEnabled:Z

    .line 622
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereMapAnimator$5;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/maps/here/HereMapAnimator$5;-><init>(Lcom/navdy/hud/app/maps/here/HereMapAnimator;)V

    const/16 v2, 0x11

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 637
    return-void
.end method

.method public stopMapRendering()V
    .locals 2

    .prologue
    .line 640
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "stopMapRendering: rendering disabled"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 641
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->renderingEnabled:Z

    .line 642
    return-void
.end method
