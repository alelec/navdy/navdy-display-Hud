.class Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager$2;
.super Ljava/lang/Object;
.source "HereSpeedWarningManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x1388

    .line 73
    :try_start_0
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->registered:Z
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->access$100(Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_1

    .line 85
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->registered:Z
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->access$100(Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 86
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->handler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->access$600(Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;)Landroid/os/Handler;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->periodicRunnable:Ljava/lang/Runnable;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->access$500(Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;)Ljava/lang/Runnable;

    move-result-object v4

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 89
    :cond_0
    :goto_0
    return-void

    .line 76
    :cond_1
    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 77
    .local v0, "now":J
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->lastUpdate:J
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->access$200(Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;)J
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v4

    sub-long v4, v0, v4

    cmp-long v3, v4, v6

    if-gez v3, :cond_2

    .line 85
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->registered:Z
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->access$100(Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 86
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->handler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->access$600(Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;)Landroid/os/Handler;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->periodicRunnable:Ljava/lang/Runnable;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->access$500(Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;)Ljava/lang/Runnable;

    move-result-object v4

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 81
    :cond_2
    :try_start_2
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;

    # invokes: Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->checkSpeed()V
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->access$300(Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 85
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->registered:Z
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->access$100(Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 86
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->handler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->access$600(Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;)Landroid/os/Handler;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->periodicRunnable:Ljava/lang/Runnable;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->access$500(Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;)Ljava/lang/Runnable;

    move-result-object v4

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 82
    .end local v0    # "now":J
    :catch_0
    move-exception v2

    .line 83
    .local v2, "t":Ljava/lang/Throwable;
    :try_start_3
    # getter for: Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 85
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->registered:Z
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->access$100(Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 86
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->handler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->access$600(Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;)Landroid/os/Handler;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->periodicRunnable:Ljava/lang/Runnable;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->access$500(Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;)Ljava/lang/Runnable;

    move-result-object v4

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 85
    .end local v2    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->registered:Z
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->access$100(Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 86
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->handler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->access$600(Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;)Landroid/os/Handler;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->periodicRunnable:Ljava/lang/Runnable;
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->access$500(Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;)Ljava/lang/Runnable;

    move-result-object v5

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_3
    throw v3
.end method
