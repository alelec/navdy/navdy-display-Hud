.class Lcom/navdy/hud/app/maps/here/HereLocationFixManager$4;
.super Ljava/lang/Object;
.source "HereLocationFixManager.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereLocationFixManager;-><init>(Lcom/squareup/otto/Bus;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    .prologue
    .line 254
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .locals 1
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 259
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->isRecording:Z
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$1000(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isNavdyDevice()Z

    move-result v0

    if-nez v0, :cond_0

    .line 261
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # invokes: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->recordRawLocation(Landroid/location/Location;)V
    invoke-static {v0, p1}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$1100(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;Landroid/location/Location;)V

    .line 264
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # invokes: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->sendLocation(Landroid/location/Location;)V
    invoke-static {v0, p1}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$1300(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;Landroid/location/Location;)V

    .line 265
    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 274
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 271
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;
    .param p2, "status"    # I
    .param p3, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 268
    return-void
.end method
