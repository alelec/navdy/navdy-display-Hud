.class Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$2;
.super Ljava/lang/Object;
.source "HereSafetySpotListener.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->onSafetySpot(Lcom/here/android/mpa/guidance/SafetySpotNotification;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

.field final synthetic val$safetySpotNotification:Lcom/here/android/mpa/guidance/SafetySpotNotification;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;Lcom/here/android/mpa/guidance/SafetySpotNotification;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$2;->this$0:Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$2;->val$safetySpotNotification:Lcom/here/android/mpa/guidance/SafetySpotNotification;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 124
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$2;->val$safetySpotNotification:Lcom/here/android/mpa/guidance/SafetySpotNotification;

    invoke-virtual {v6}, Lcom/here/android/mpa/guidance/SafetySpotNotification;->getSafetySpotNotificationInfos()Ljava/util/List;

    move-result-object v3

    .line 125
    .local v3, "safetySpotNotificationList":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/guidance/SafetySpotNotificationInfo;>;"
    if-eqz v3, :cond_0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    if-nez v6, :cond_1

    .line 126
    :cond_0
    # getter for: Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->access$300()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    const-string v7, "no safety spots"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 154
    :goto_0
    return-void

    .line 129
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v0

    .line 130
    .local v0, "currentLocation":Lcom/here/android/mpa/common/GeoCoordinate;
    if-nez v0, :cond_2

    .line 131
    # getter for: Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->access$300()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    const-string v7, "no current location"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 134
    :cond_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 135
    .local v4, "tmpList":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/guidance/SafetySpotNotificationInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/here/android/mpa/guidance/SafetySpotNotificationInfo;

    .line 136
    .local v1, "info":Lcom/here/android/mpa/guidance/SafetySpotNotificationInfo;
    invoke-virtual {v1}, Lcom/here/android/mpa/guidance/SafetySpotNotificationInfo;->getSafetySpot()Lcom/here/android/mpa/mapping/SafetySpotInfo;

    move-result-object v2

    .line 137
    .local v2, "safetySpotInfo":Lcom/here/android/mpa/mapping/SafetySpotInfo;
    if-eqz v2, :cond_3

    .line 140
    invoke-virtual {v2}, Lcom/here/android/mpa/mapping/SafetySpotInfo;->getType()Lcom/here/android/mpa/mapping/SafetySpotInfo$Type;

    move-result-object v5

    .line 141
    .local v5, "type":Lcom/here/android/mpa/mapping/SafetySpotInfo$Type;
    sget-object v7, Lcom/here/android/mpa/mapping/SafetySpotInfo$Type;->UNDEFINED:Lcom/here/android/mpa/mapping/SafetySpotInfo$Type;

    if-eq v5, v7, :cond_3

    .line 144
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 146
    .end local v1    # "info":Lcom/here/android/mpa/guidance/SafetySpotNotificationInfo;
    .end local v2    # "safetySpotInfo":Lcom/here/android/mpa/mapping/SafetySpotInfo;
    .end local v5    # "type":Lcom/here/android/mpa/mapping/SafetySpotInfo$Type;
    :cond_4
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    if-nez v6, :cond_5

    .line 147
    # getter for: Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->access$300()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    const-string v7, "no valid safety spots"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 151
    :cond_5
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$2;->this$0:Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->handler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->access$500(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;)Landroid/os/Handler;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$2;->this$0:Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->checkExpiredSpots:Ljava/lang/Runnable;
    invoke-static {v7}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->access$700(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;)Ljava/lang/Runnable;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 152
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$2;->this$0:Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

    # invokes: Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->addSafetySpotMarkers(Ljava/util/List;Lcom/here/android/mpa/common/GeoCoordinate;)V
    invoke-static {v6, v4, v0}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->access$800(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;Ljava/util/List;Lcom/here/android/mpa/common/GeoCoordinate;)V

    .line 153
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$2;->this$0:Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->handler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->access$500(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;)Landroid/os/Handler;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$2;->this$0:Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->checkExpiredSpots:Ljava/lang/Runnable;
    invoke-static {v7}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->access$700(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;)Ljava/lang/Runnable;

    move-result-object v7

    # getter for: Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->CHECK_INTERVAL:I
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->access$400()I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
