.class Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$2;
.super Ljava/lang/Object;
.source "HereTrafficUpdater2.java"

# interfaces
.implements Lcom/here/android/mpa/guidance/TrafficUpdater$GetEventsListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    .prologue
    .line 165
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$2;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(Ljava/util/List;Lcom/here/android/mpa/guidance/TrafficUpdater$Error;)V
    .locals 6
    .param p2, "error"    # Lcom/here/android/mpa/guidance/TrafficUpdater$Error;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/mapping/TrafficEvent;",
            ">;",
            "Lcom/here/android/mpa/guidance/TrafficUpdater$Error;",
            ")V"
        }
    .end annotation

    .prologue
    .line 168
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/mapping/TrafficEvent;>;"
    const/4 v1, 0x0

    .line 170
    .local v1, "reset":Z
    :try_start_0
    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onGetEventsListener::"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 172
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$2;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->route:Lcom/here/android/mpa/routing/Route;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$100(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;)Lcom/here/android/mpa/routing/Route;

    move-result-object v0

    .line 174
    .local v0, "r":Lcom/here/android/mpa/routing/Route;
    if-nez v0, :cond_1

    .line 175
    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "onGetEventsListener: route is null"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 206
    if-eqz v1, :cond_0

    .line 207
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$2;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$2;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->getRefereshInterval()I

    move-result v3

    # invokes: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->reset(I)V
    invoke-static {v2, v3}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$400(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;I)V

    .line 210
    :cond_0
    :goto_0
    return-void

    .line 179
    :cond_1
    const/4 v1, 0x1

    .line 180
    :try_start_1
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$2;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    const/4 v3, 0x0

    # setter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->currentRequestInfo:Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;
    invoke-static {v2, v3}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$202(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;)Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;

    .line 181
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$2;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    const-wide/16 v4, 0x0

    # setter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->lastRequestTime:J
    invoke-static {v2, v4, v5}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$302(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;J)J

    .line 183
    sget-object v2, Lcom/here/android/mpa/guidance/TrafficUpdater$Error;->NONE:Lcom/here/android/mpa/guidance/TrafficUpdater$Error;

    if-ne p2, v2, :cond_5

    .line 184
    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_3

    .line 185
    :cond_2
    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "onGetEventsListener: no events"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 186
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$2;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$800(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;)Lcom/squareup/otto/Bus;

    move-result-object v2

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->FAILED_EVENT:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$700()Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 206
    :goto_1
    if-eqz v1, :cond_0

    .line 207
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$2;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$2;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->getRefereshInterval()I

    move-result v3

    # invokes: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->reset(I)V
    invoke-static {v2, v3}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$400(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;I)V

    goto :goto_0

    .line 188
    :cond_3
    :try_start_2
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v2

    new-instance v3, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$2$1;

    invoke-direct {v3, p0, v0, p1}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$2$1;-><init>(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$2;Lcom/here/android/mpa/routing/Route;Ljava/util/List;)V

    const/4 v4, 0x2

    invoke-virtual {v2, v3, v4}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 206
    .end local v0    # "r":Lcom/here/android/mpa/routing/Route;
    :catchall_0
    move-exception v2

    if-eqz v1, :cond_4

    .line 207
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$2;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$2;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->getRefereshInterval()I

    move-result v4

    # invokes: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->reset(I)V
    invoke-static {v3, v4}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$400(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;I)V

    :cond_4
    throw v2

    .line 202
    .restart local v0    # "r":Lcom/here/android/mpa/routing/Route;
    :cond_5
    :try_start_3
    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onGetEventsListener: error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 203
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$2;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$800(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;)Lcom/squareup/otto/Bus;

    move-result-object v2

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->FAILED_EVENT:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$700()Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method
