.class Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1$1;
.super Ljava/lang/Object;
.source "NavdyTrafficRerouteManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;->postSuccess(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;

.field final synthetic val$outgoingResults:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;Ljava/util/ArrayList;)V
    .locals 0
    .param p1, "this$2"    # Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;

    .prologue
    .line 162
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1$1;->this$2:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;

    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1$1;->val$outgoingResults:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 42

    .prologue
    .line 166
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1$1;->this$2:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    move-object/from16 v38, v0

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->running:Z
    invoke-static/range {v38 .. v38}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$800(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Z

    move-result v38

    if-nez v38, :cond_0

    .line 167
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v38

    const-string v39, "success,route manager stopped"

    invoke-virtual/range {v38 .. v39}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 327
    :goto_0
    return-void

    .line 171
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1$1;->val$outgoingResults:Ljava/util/ArrayList;

    move-object/from16 v38, v0

    if-eqz v38, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1$1;->val$outgoingResults:Ljava/util/ArrayList;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Ljava/util/ArrayList;->size()I

    move-result v38

    if-nez v38, :cond_2

    .line 172
    :cond_1
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v38

    const-string v39, "success, but no result"

    invoke-virtual/range {v38 .. v39}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 173
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1$1;->this$2:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    move-object/from16 v38, v0

    # invokes: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->cleanupRouteIfStale()V
    invoke-static/range {v38 .. v38}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$600(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 324
    :catch_0
    move-exception v34

    .line 325
    .local v34, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v38

    move-object/from16 v0, v38

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 177
    .end local v34    # "t":Ljava/lang/Throwable;
    :cond_2
    :try_start_1
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v38

    const-string v39, "success"

    invoke-virtual/range {v38 .. v39}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 178
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1$1;->val$outgoingResults:Ljava/util/ArrayList;

    move-object/from16 v38, v0

    const/16 v39, 0x0

    invoke-virtual/range {v38 .. v39}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    .line 179
    .local v29, "result":Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getInstance()Lcom/navdy/hud/app/maps/here/HereRouteCache;

    move-result-object v18

    .line 180
    .local v18, "hereRouteCache":Lcom/navdy/hud/app/maps/here/HereRouteCache;
    move-object/from16 v0, v29

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeId:Ljava/lang/String;

    move-object/from16 v38, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getRoute(Ljava/lang/String;)Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;

    move-result-object v32

    .line 182
    .local v32, "routeInfo":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    if-nez v32, :cond_3

    .line 183
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v38

    const-string v39, "did not get routeinfo"

    invoke-virtual/range {v38 .. v39}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 184
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1$1;->this$2:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    move-object/from16 v38, v0

    # invokes: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->cleanupRouteIfStale()V
    invoke-static/range {v38 .. v38}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$600(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)V

    goto/16 :goto_0

    .line 188
    :cond_3
    move-object/from16 v0, v29

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeId:Ljava/lang/String;

    move-object/from16 v38, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->removeRoute(Ljava/lang/String;)Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;

    .line 190
    move-object/from16 v0, v29

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->duration_traffic:Ljava/lang/Integer;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Ljava/lang/Integer;->intValue()I

    move-result v38

    if-nez v38, :cond_4

    .line 191
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v38

    const-string v39, "did not get traffic duration"

    invoke-virtual/range {v38 .. v39}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 192
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1$1;->this$2:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    move-object/from16 v38, v0

    # invokes: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->cleanupRouteIfStale()V
    invoke-static/range {v38 .. v38}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$600(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)V

    goto/16 :goto_0

    .line 197
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1$1;->this$2:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    move-object/from16 v38, v0

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static/range {v38 .. v38}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$700(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentRoute()Lcom/here/android/mpa/routing/Route;

    move-result-object v10

    .line 198
    .local v10, "currentRoute":Lcom/here/android/mpa/routing/Route;
    if-nez v10, :cond_5

    .line 199
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v38

    const-string v39, "no current route"

    invoke-virtual/range {v38 .. v39}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 200
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1$1;->this$2:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    move-object/from16 v38, v0

    # invokes: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->cleanupRouteIfStale()V
    invoke-static/range {v38 .. v38}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$600(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)V

    goto/16 :goto_0

    .line 204
    :cond_5
    move-object/from16 v0, v32

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->route:Lcom/here/android/mpa/routing/Route;

    move-object/from16 v38, v0

    sget-object v39, Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;->OPTIMAL:Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    const v40, 0xfffffff

    invoke-virtual/range {v38 .. v40}, Lcom/here/android/mpa/routing/Route;->getTta(Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;I)Lcom/here/android/mpa/routing/RouteTta;

    move-result-object v36

    .line 205
    .local v36, "trafficTta":Lcom/here/android/mpa/routing/RouteTta;
    move-object/from16 v0, v32

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->route:Lcom/here/android/mpa/routing/Route;

    move-object/from16 v38, v0

    sget-object v39, Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;->DISABLED:Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    const v40, 0xfffffff

    invoke-virtual/range {v38 .. v40}, Lcom/here/android/mpa/routing/Route;->getTta(Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;I)Lcom/here/android/mpa/routing/RouteTta;

    move-result-object v25

    .line 206
    .local v25, "noTrafficTta":Lcom/here/android/mpa/routing/RouteTta;
    if-eqz v36, :cond_6

    if-eqz v25, :cond_6

    .line 207
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v38

    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string v40, "route duration traffic["

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v36 .. v36}, Lcom/here/android/mpa/routing/RouteTta;->getDuration()I

    move-result v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, "] no-traffic["

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v25 .. v25}, Lcom/here/android/mpa/routing/RouteTta;->getDuration()I

    move-result v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, "]"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    invoke-virtual/range {v38 .. v39}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 210
    :cond_6
    move-object/from16 v0, v32

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->route:Lcom/here/android/mpa/routing/Route;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Lcom/here/android/mpa/routing/Route;->getManeuvers()Ljava/util/List;

    move-result-object v13

    .line 211
    .local v13, "fasterManeuvers":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    invoke-virtual {v10}, Lcom/here/android/mpa/routing/Route;->getManeuvers()Ljava/util/List;

    move-result-object v6

    .line 212
    .local v6, "currentManeuvers":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1$1;->this$2:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    move-object/from16 v38, v0

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static/range {v38 .. v38}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$700(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getNavController()Lcom/navdy/hud/app/maps/here/HereNavController;

    move-result-object v23

    .line 213
    .local v23, "navController":Lcom/navdy/hud/app/maps/here/HereNavController;
    invoke-virtual/range {v23 .. v23}, Lcom/navdy/hud/app/maps/here/HereNavController;->getNextManeuver()Lcom/here/android/mpa/routing/Maneuver;

    move-result-object v5

    .line 214
    .local v5, "currentManeuver":Lcom/here/android/mpa/routing/Maneuver;
    const/16 v17, -0x1

    .line 215
    .local v17, "firstOffSet":I
    const/16 v33, 0x0

    .line 217
    .local v33, "secondOffset":I
    if-nez v5, :cond_a

    .line 218
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v38

    const-string v39, "no current maneuver"

    invoke-virtual/range {v38 .. v39}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 219
    const/16 v17, 0x0

    .line 247
    :cond_7
    :goto_1
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v38

    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string v40, "firstOff = "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, " secondOff = "

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    invoke-virtual/range {v38 .. v39}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 248
    new-instance v16, Ljava/util/ArrayList;

    const/16 v38, 0x2

    move-object/from16 v0, v16

    move/from16 v1, v38

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 249
    .local v16, "firstDifference":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    move/from16 v0, v17

    move/from16 v1, v33

    move-object/from16 v2, v16

    invoke-static {v6, v0, v13, v1, v2}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->areManeuversEqual(Ljava/util/List;ILjava/util/List;ILjava/util/List;)Z

    move-result v4

    .line 250
    .local v4, "areRoutesEqual":Z
    move-object/from16 v0, v32

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->route:Lcom/here/android/mpa/routing/Route;

    move-object/from16 v38, v0

    invoke-static/range {v38 .. v38}, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator;->getViaString(Lcom/here/android/mpa/routing/Route;)Ljava/lang/String;

    move-result-object v37

    .line 251
    .local v37, "via":Ljava/lang/String;
    const/4 v12, 0x0

    .line 252
    .local v12, "currentVia":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getInstance()Lcom/navdy/hud/app/maps/here/HereRouteCache;

    move-result-object v38

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1$1;->this$2:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    move-object/from16 v39, v0

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static/range {v39 .. v39}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$700(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentRouteId()Ljava/lang/String;

    move-result-object v39

    invoke-virtual/range {v38 .. v39}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getRoute(Ljava/lang/String;)Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;

    move-result-object v11

    .line 253
    .local v11, "currentRouteInfo":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    if-eqz v11, :cond_8

    .line 254
    move-object/from16 v0, v32

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->routeResult:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget-object v12, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->via:Ljava/lang/String;

    .line 256
    :cond_8
    if-eqz v4, :cond_f

    .line 257
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v38

    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string v40, "fast route same as current route faster-via["

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, "] current-via["

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, "]"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    invoke-virtual/range {v38 .. v39}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 259
    invoke-static {}, Lcom/navdy/hud/app/framework/voice/TTSUtils;->isDebugTTSEnabled()Z

    move-result v38

    if-eqz v38, :cond_9

    .line 260
    const-string v35, "Faster route not found"

    .line 261
    .local v35, "title":Ljava/lang/String;
    new-instance v38, Ljava/lang/StringBuilder;

    invoke-direct/range {v38 .. v38}, Ljava/lang/StringBuilder;-><init>()V

    const-string v39, " current route via["

    invoke-virtual/range {v38 .. v39}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v38

    move-object/from16 v0, v38

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v38

    const-string v39, "] is still fastest"

    invoke-virtual/range {v38 .. v39}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 262
    .local v20, "info":Ljava/lang/String;
    move-object/from16 v0, v35

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Lcom/navdy/hud/app/framework/voice/TTSUtils;->debugShowFasterRouteToast(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    .end local v20    # "info":Ljava/lang/String;
    .end local v35    # "title":Ljava/lang/String;
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1$1;->this$2:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    move-object/from16 v38, v0

    # invokes: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->cleanupRouteIfStale()V
    invoke-static/range {v38 .. v38}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$600(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)V

    goto/16 :goto_0

    .line 221
    .end local v4    # "areRoutesEqual":Z
    .end local v11    # "currentRouteInfo":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    .end local v12    # "currentVia":Ljava/lang/String;
    .end local v16    # "firstDifference":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    .end local v37    # "via":Ljava/lang/String;
    :cond_a
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v21

    .line 222
    .local v21, "len":I
    const/16 v19, 0x0

    .local v19, "i":I
    :goto_2
    move/from16 v0, v19

    move/from16 v1, v21

    if-ge v0, v1, :cond_b

    .line 223
    move/from16 v0, v19

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v38

    check-cast v38, Lcom/here/android/mpa/routing/Maneuver;

    move-object/from16 v0, v38

    invoke-static {v0, v5}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->areManeuverEqual(Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;)Z

    move-result v38

    if-eqz v38, :cond_c

    .line 225
    move/from16 v17, v19

    .line 230
    :cond_b
    const/16 v38, -0x1

    move/from16 v0, v17

    move/from16 v1, v38

    if-ne v0, v1, :cond_d

    .line 231
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v38

    const-string v39, "cannot find current maneuver in route"

    invoke-virtual/range {v38 .. v39}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 232
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1$1;->this$2:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    move-object/from16 v38, v0

    # invokes: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->cleanupRouteIfStale()V
    invoke-static/range {v38 .. v38}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$600(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)V

    goto/16 :goto_0

    .line 222
    :cond_c
    add-int/lit8 v19, v19, 0x1

    goto :goto_2

    .line 236
    :cond_d
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v21

    .line 237
    const/16 v19, 0x0

    :goto_3
    move/from16 v0, v19

    move/from16 v1, v21

    if-ge v0, v1, :cond_7

    .line 238
    move/from16 v0, v19

    invoke-interface {v13, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v38

    check-cast v38, Lcom/here/android/mpa/routing/Maneuver;

    move-object/from16 v0, v38

    invoke-static {v0, v5}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->areManeuverEqual(Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;)Z

    move-result v38

    if-eqz v38, :cond_e

    .line 240
    move/from16 v33, v19

    .line 241
    goto/16 :goto_1

    .line 237
    :cond_e
    add-int/lit8 v19, v19, 0x1

    goto :goto_3

    .line 267
    .end local v19    # "i":I
    .end local v21    # "len":I
    .restart local v4    # "areRoutesEqual":Z
    .restart local v11    # "currentRouteInfo":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    .restart local v12    # "currentVia":Ljava/lang/String;
    .restart local v16    # "firstDifference":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    .restart local v37    # "via":Ljava/lang/String;
    :cond_f
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v38

    const-string v39, "fast route is different than current route"

    invoke-virtual/range {v38 .. v39}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 268
    const/16 v38, 0x0

    move/from16 v0, v38

    invoke-static {v6, v0}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getAllRoadNames(Ljava/util/List;I)Ljava/lang/String;

    move-result-object v9

    .line 269
    .local v9, "currentRoadNames":Ljava/lang/String;
    const/16 v38, 0x0

    move/from16 v0, v38

    invoke-static {v13, v0}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getAllRoadNames(Ljava/util/List;I)Ljava/lang/String;

    move-result-object v24

    .line 270
    .local v24, "newRoadNames":Ljava/lang/String;
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v38

    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string v40, "[RoadNames-current] [offset="

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, "] maneuverCount="

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    invoke-virtual/range {v38 .. v39}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 271
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v38

    move-object/from16 v0, v38

    invoke-virtual {v0, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 272
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v38

    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string v40, "[RoadNames-faster] [offset="

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, "] maneuverCount="

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    invoke-virtual/range {v38 .. v39}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 273
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v38

    move-object/from16 v0, v38

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 278
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1$1;->this$2:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    move-object/from16 v38, v0

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;
    invoke-static/range {v38 .. v38}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$000(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->getCurrentProposedRoute()Lcom/here/android/mpa/routing/Route;

    move-result-object v7

    .line 279
    .local v7, "currentProposedRoute":Lcom/here/android/mpa/routing/Route;
    if-eqz v7, :cond_12

    .line 280
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v38

    const-string v39, "check if current proposed route is same as new one"

    invoke-virtual/range {v38 .. v39}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 281
    invoke-virtual {v7}, Lcom/here/android/mpa/routing/Route;->getManeuvers()Ljava/util/List;

    move-result-object v8

    .line 282
    .local v8, "currentProposedRouteManeuvers":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    const/16 v38, 0x0

    const/16 v39, 0x0

    const/16 v40, 0x0

    move/from16 v0, v38

    move/from16 v1, v39

    move-object/from16 v2, v40

    invoke-static {v8, v0, v13, v1, v2}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->areManeuversEqual(Ljava/util/List;ILjava/util/List;ILjava/util/List;)Z

    move-result v38

    if-eqz v38, :cond_13

    .line 287
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1$1;->this$2:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    move-object/from16 v38, v0

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static/range {v38 .. v38}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$700(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getNavController()Lcom/navdy/hud/app/maps/here/HereNavController;

    move-result-object v38

    const/16 v39, 0x1

    sget-object v40, Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;->OPTIMAL:Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    invoke-virtual/range {v38 .. v40}, Lcom/navdy/hud/app/maps/here/HereNavController;->getEta(ZLcom/here/android/mpa/routing/Route$TrafficPenaltyMode;)Ljava/util/Date;

    move-result-object v28

    .line 288
    .local v28, "oldEta":Ljava/util/Date;
    invoke-static/range {v28 .. v28}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->isValidEtaDate(Ljava/util/Date;)Z

    move-result v38

    if-nez v38, :cond_10

    .line 289
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v38

    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string v40, "current eta is invalid:"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    invoke-virtual/range {v38 .. v39}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 290
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1$1;->this$2:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    move-object/from16 v38, v0

    # invokes: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->cleanupRouteIfStale()V
    invoke-static/range {v38 .. v38}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$600(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)V

    goto/16 :goto_0

    .line 294
    :cond_10
    invoke-virtual/range {v36 .. v36}, Lcom/here/android/mpa/routing/RouteTta;->getDuration()I

    move-result v38

    move/from16 v0, v38

    int-to-long v0, v0

    move-wide/from16 v26, v0

    .line 295
    .local v26, "newTta":J
    invoke-virtual/range {v28 .. v28}, Ljava/util/Date;->getTime()J

    move-result-wide v38

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v40

    sub-long v38, v38, v40

    const-wide/16 v40, 0x3e8

    div-long v30, v38, v40

    .line 296
    .local v30, "oldTta":J
    sub-long v14, v30, v26

    .line 297
    .local v14, "diff":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1$1;->this$2:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    move-object/from16 v38, v0

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;
    invoke-static/range {v38 .. v38}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$000(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->getRerouteMinDuration()I

    move-result v22

    .line 298
    .local v22, "minDuration":I
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v38

    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string v40, "current proposed route["

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, "] is same as new one["

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, "] new-diff["

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, "] old-diff["

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1$1;->this$2:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    move-object/from16 v40, v0

    .line 300
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;
    invoke-static/range {v40 .. v40}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$000(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->getFasterBy()J

    move-result-wide v40

    invoke-virtual/range {v39 .. v41}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, "] minDur["

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, "]"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    .line 298
    invoke-virtual/range {v38 .. v39}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 303
    move/from16 v0, v22

    int-to-long v0, v0

    move-wide/from16 v38, v0

    cmp-long v38, v14, v38

    if-gez v38, :cond_11

    .line 305
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v38

    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string v40, "route has dropped below threshold:"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    invoke-virtual/range {v38 .. v39}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 306
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1$1;->this$2:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    move-object/from16 v38, v0

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;
    invoke-static/range {v38 .. v38}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$000(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->dismissReroute()V

    goto/16 :goto_0

    .line 309
    :cond_11
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v38

    const-string v39, "route still above threshold, update"

    invoke-virtual/range {v38 .. v39}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 322
    .end local v8    # "currentProposedRouteManeuvers":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    .end local v14    # "diff":J
    .end local v22    # "minDuration":I
    .end local v26    # "newTta":J
    .end local v28    # "oldEta":Ljava/util/Date;
    .end local v30    # "oldTta":J
    :cond_12
    :goto_4
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v38

    const-string v39, "calling here traffic reroute listener"

    invoke-virtual/range {v38 .. v39}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 323
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1$1;->this$2:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    move-object/from16 v38, v0

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;
    invoke-static/range {v38 .. v38}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$000(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    move-result-object v38

    move-object/from16 v0, v32

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->route:Lcom/here/android/mpa/routing/Route;

    move-object/from16 v39, v0

    const/16 v40, 0x0

    invoke-virtual/range {v38 .. v40}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->handleFasterRoute(Lcom/here/android/mpa/routing/Route;Z)V

    goto/16 :goto_0

    .line 312
    .restart local v8    # "currentProposedRouteManeuvers":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    :cond_13
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v38

    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string v40, "current proposed route["

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, "] is different from new one["

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v40, "]"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    invoke-virtual/range {v38 .. v39}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 313
    const/16 v38, 0x0

    move/from16 v0, v38

    invoke-static {v8, v0}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getAllRoadNames(Ljava/util/List;I)Ljava/lang/String;

    move-result-object v9

    .line 314
    const/16 v38, 0x0

    move/from16 v0, v38

    invoke-static {v13, v0}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getAllRoadNames(Ljava/util/List;I)Ljava/lang/String;

    move-result-object v24

    .line 315
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v38

    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string v40, "[RoadNames-proposed-current] maneuverCount="

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    invoke-virtual/range {v38 .. v39}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 316
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v38

    move-object/from16 v0, v38

    invoke-virtual {v0, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 317
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v38

    new-instance v39, Ljava/lang/StringBuilder;

    invoke-direct/range {v39 .. v39}, Ljava/lang/StringBuilder;-><init>()V

    const-string v40, "[RoadNames-proposed-faster] maneuverCount="

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    invoke-virtual/range {v38 .. v39}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 318
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v38

    move-object/from16 v0, v38

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_4
.end method
