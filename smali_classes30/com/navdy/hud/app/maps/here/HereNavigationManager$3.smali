.class Lcom/navdy/hud/app/maps/here/HereNavigationManager$3;
.super Ljava/lang/Object;
.source "HereNavigationManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereNavigationManager;->updateMapRoute(Lcom/here/android/mpa/routing/Route;Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;Ljava/lang/String;ZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

.field final synthetic val$id:Ljava/lang/String;

.field final synthetic val$includedTraffic:Z

.field final synthetic val$newRoute:Lcom/here/android/mpa/routing/Route;

.field final synthetic val$oldRouteId:Ljava/lang/String;

.field final synthetic val$reason:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

.field final synthetic val$routeRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

.field final synthetic val$viaStr:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereNavigationManager;Ljava/lang/String;Lcom/here/android/mpa/routing/Route;Ljava/lang/String;ZLjava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    .prologue
    .line 681
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$3;->val$viaStr:Ljava/lang/String;

    iput-object p3, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$3;->val$newRoute:Lcom/here/android/mpa/routing/Route;

    iput-object p4, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$3;->val$id:Ljava/lang/String;

    iput-boolean p5, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$3;->val$includedTraffic:Z

    iput-object p6, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$3;->val$oldRouteId:Ljava/lang/String;

    iput-object p7, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$3;->val$reason:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    iput-object p8, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$3;->val$routeRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    .prologue
    const/4 v14, 0x0

    .line 684
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-direct {v0, v1, v14}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;-><init>(Lcom/navdy/service/library/log/Logger;Z)V

    .line 685
    .local v0, "routeCalculator":Lcom/navdy/hud/app/maps/here/HereRouteCalculator;
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$3;->val$viaStr:Ljava/lang/String;

    .line 686
    .local v3, "via":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 687
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$3;->val$newRoute:Lcom/here/android/mpa/routing/Route;

    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator;->getViaString(Lcom/here/android/mpa/routing/Route;)Ljava/lang/String;

    move-result-object v3

    .line 688
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 689
    const-string v3, ""

    .line 693
    :cond_0
    const/4 v4, 0x0

    .line 694
    .local v4, "label":Ljava/lang/String;
    const/4 v5, 0x0

    .line 695
    .local v5, "streetAddress":Ljava/lang/String;
    const/4 v12, 0x0

    .line 697
    .local v12, "routeAttributes":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;>;"
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$100(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    move-result-object v1

    iget-object v8, v1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->navigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .line 699
    .local v8, "navRequest":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    if-eqz v8, :cond_1

    .line 700
    iget-object v4, v8, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->label:Ljava/lang/String;

    .line 701
    iget-object v5, v8, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->streetAddress:Ljava/lang/String;

    .line 702
    iget-object v12, v8, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->routeAttributes:Ljava/util/List;

    .line 705
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$3;->val$id:Ljava/lang/String;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$3;->val$newRoute:Lcom/here/android/mpa/routing/Route;

    const/4 v6, 0x1

    invoke-virtual/range {v0 .. v6}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->getRouteResultFromRoute(Ljava/lang/String;Lcom/here/android/mpa/routing/Route;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    move-result-object v9

    .line 707
    .local v9, "result":Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$100(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$3;->val$id:Ljava/lang/String;

    iput-object v2, v1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->routeId:Ljava/lang/String;

    .line 708
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$100(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$3;->val$newRoute:Lcom/here/android/mpa/routing/Route;

    iput-object v2, v1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->route:Lcom/here/android/mpa/routing/Route;

    .line 710
    new-instance v6, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;

    iget-object v7, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$3;->val$newRoute:Lcom/here/android/mpa/routing/Route;

    iget-boolean v10, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$3;->val$includedTraffic:Z

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$600()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v11

    invoke-direct/range {v6 .. v11}, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;-><init>(Lcom/here/android/mpa/routing/Route;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/navdy/service/library/events/navigation/NavigationRouteResult;ZLcom/here/android/mpa/common/GeoCoordinate;)V

    .line 711
    .local v6, "routeInfo":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getInstance()Lcom/navdy/hud/app/maps/here/HereRouteCache;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$3;->val$id:Ljava/lang/String;

    invoke-virtual {v1, v2, v6}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->addRoute(Ljava/lang/String;Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;)V

    .line 712
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "new route added to cache"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 715
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_REROUTED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    invoke-virtual {v1, v2, v14}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->postNavigationSessionStatusEvent(Lcom/navdy/service/library/events/navigation/NavigationSessionState;Z)V

    .line 717
    new-instance v13, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$3;->val$oldRouteId:Ljava/lang/String;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$3;->val$reason:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    invoke-direct {v13, v1, v9, v2}, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationRouteResult;Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;)V

    .line 722
    .local v13, "routeChange":Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;
    if-eqz v12, :cond_2

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;->ROUTE_ATTRIBUTE_GAS:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;

    invoke-interface {v12, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 723
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$100(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    move-result-object v1

    iget-object v1, v1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-static {v8, v1, v2}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->saveGasRouteInfo(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/navdy/service/library/device/NavdyDeviceId;Lcom/navdy/service/library/log/Logger;)V

    .line 726
    :cond_2
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$500(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Lcom/squareup/otto/Bus;

    move-result-object v1

    invoke-virtual {v1, v13}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 727
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$3;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$500(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Lcom/squareup/otto/Bus;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/event/RemoteEvent;

    invoke-direct {v2, v13}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 728
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "NavigationSessionRouteChange sent to client oldRoute["

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v7, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$3;->val$oldRouteId:Ljava/lang/String;

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "] newRoute["

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v7, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$3;->val$id:Ljava/lang/String;

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "] reason["

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v7, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$3;->val$reason:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "] via["

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "]"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 730
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 731
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$3;->val$newRoute:Lcom/here/android/mpa/routing/Route;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$3;->val$routeRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v2, v2, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->streetAddress:Ljava/lang/String;

    iget-object v7, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$3;->val$routeRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    const/4 v10, 0x0

    invoke-static {v1, v2, v7, v10}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->printRouteDetails(Lcom/here/android/mpa/routing/Route;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/StringBuilder;)V

    .line 733
    :cond_3
    return-void
.end method
