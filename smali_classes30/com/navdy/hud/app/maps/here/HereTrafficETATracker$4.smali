.class Lcom/navdy/hud/app/maps/here/HereTrafficETATracker$4;
.super Ljava/lang/Object;
.source "HereTrafficETATracker.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v4, 0x1

    .line 59
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->navController:Lcom/navdy/hud/app/maps/here/HereNavController;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->access$300(Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;)Lcom/navdy/hud/app/maps/here/HereNavController;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavController;->getState()Lcom/navdy/hud/app/maps/here/HereNavController$State;

    move-result-object v1

    .line 60
    .local v1, "state":Lcom/navdy/hud/app/maps/here/HereNavController$State;
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavController$State;->NAVIGATING:Lcom/navdy/hud/app/maps/here/HereNavController$State;

    if-ne v1, v2, :cond_2

    .line 61
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->navController:Lcom/navdy/hud/app/maps/here/HereNavController;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->access$300(Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;)Lcom/navdy/hud/app/maps/here/HereNavController;

    move-result-object v2

    sget-object v3, Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;->OPTIMAL:Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    invoke-virtual {v2, v4, v3}, Lcom/navdy/hud/app/maps/here/HereNavController;->getEta(ZLcom/here/android/mpa/routing/Route$TrafficPenaltyMode;)Ljava/util/Date;

    move-result-object v0

    .line 62
    .local v0, "etaDate":Ljava/util/Date;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->isValidEtaDate(Ljava/util/Date;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 63
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    # setter for: Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->baseEta:J
    invoke-static {v2, v4, v5}, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->access$402(Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;J)J

    .line 64
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->access$500(Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "got baseEta from eta:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->baseEta:J
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->access$400(Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 77
    :goto_0
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;

    # invokes: Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->refreshEta()V
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->access$600(Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;)V

    .line 85
    .end local v0    # "etaDate":Ljava/util/Date;
    :goto_1
    return-void

    .line 67
    .restart local v0    # "etaDate":Ljava/util/Date;
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->navController:Lcom/navdy/hud/app/maps/here/HereNavController;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->access$300(Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;)Lcom/navdy/hud/app/maps/here/HereNavController;

    move-result-object v2

    sget-object v3, Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;->OPTIMAL:Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    invoke-virtual {v2, v4, v3}, Lcom/navdy/hud/app/maps/here/HereNavController;->getTtaDate(ZLcom/here/android/mpa/routing/Route$TrafficPenaltyMode;)Ljava/util/Date;

    move-result-object v0

    .line 68
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->isValidEtaDate(Ljava/util/Date;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 69
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;

    # setter for: Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->baseEta:J
    invoke-static {v2, v6, v7}, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->access$402(Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;J)J

    .line 70
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;

    # invokes: Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->sendTrafficDelayDismissEvent()V
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->access$200(Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;)V

    .line 71
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->access$500(Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "no baseEta"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 73
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    # setter for: Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->baseEta:J
    invoke-static {v2, v4, v5}, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->access$402(Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;J)J

    .line 74
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->access$500(Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "got baseEta from eta--tta:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->baseEta:J
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->access$400(Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 79
    .end local v0    # "etaDate":Ljava/util/Date;
    :cond_2
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->access$500(Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "no baseEta, not navigation:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 80
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;

    # setter for: Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->baseEta:J
    invoke-static {v2, v6, v7}, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->access$402(Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;J)J

    .line 81
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->access$800(Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->refreshEtaRunnable:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->access$700(Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 82
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->access$800(Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->dismissEtaRunnable:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->access$900(Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 83
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;

    # invokes: Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->sendTrafficDelayDismissEvent()V
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->access$200(Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;)V

    goto/16 :goto_1
.end method
