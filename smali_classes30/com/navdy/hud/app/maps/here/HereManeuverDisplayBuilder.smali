.class public Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;
.super Ljava/lang/Object;
.source "HereManeuverDisplayBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;,
        Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$PendingRoadInfo;
    }
.end annotation


# static fields
.field public static final ARRIVED:Ljava/lang/String;

.field public static final CLOSE_BRACKET:Ljava/lang/String; = ")"

.field public static final COMBINE_HIGHWAY_MANEUVER:Z = true

.field public static final COMMA:Ljava/lang/String; = ","

.field static final DESTINATION_LEFT:Ljava/lang/String;

.field private static final DESTINATION_POINT_A_MIN_DISTANCE:I = 0x1e

.field static final DESTINATION_RIGHT:Ljava/lang/String;

.field private static final DISTANCE:Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;

.field public static final EMPTY:Ljava/lang/String; = ""

.field public static final EMPTY_MANEUVER_DISPLAY:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

.field private static final END_TURN:Ljava/lang/String;

.field private static final ENTER_HIGHWAY:Ljava/lang/String;

.field private static final EXIT:Ljava/lang/String;

.field public static final EXIT_NUMBER:Ljava/lang/String;

.field private static final FEET_METER_FORMAT:Ljava/lang/String; = "%.0f %s"

.field public static final FT_IN_METER:F = 3.28084f

.field private static final HYPHEN:Ljava/lang/String;

.field public static final KM_DISPLAY_THRESHOLD:F = 400.0f

.field private static final KM_FORMAT:Ljava/lang/String; = "%.1f %s"

.field private static final KM_FORMAT_SHORT:Ljava/lang/String; = "%.0f %s"

.field public static final METERS_IN_KM:F = 1000.0f

.field public static final METERS_IN_MI:F = 1609.34f

.field public static final MILES_DISPLAY_THRESHOLD:F = 160.934f

.field private static final MILES_FORMAT:Ljava/lang/String; = "%.1f %s"

.field private static final MILES_FORMAT_SHORT:Ljava/lang/String; = "%.0f %s"

.field private static final MIN_STEP_FEET:I = 0xa

.field private static final MIN_STEP_METERS:I = 0x5

.field private static final NEXT_DISTANCE_THRESHOLD:J = 0x1925L

.field private static final NEXT_DISTANCE_THRESHOLD_HIGHWAY:J = 0x3eddL

.field private static final NOW_DISTANCE_THRESHOLD:J = 0x1eL

.field private static final NOW_DISTANCE_THRESHOLD_HIGHWAY:J = 0x64L

.field private static final ONTO:Ljava/lang/String;

.field public static final OPEN_BRACKET:Ljava/lang/String; = "("

.field public static final SHORT_DISTANCE_DISPLAY_THRESHOLD:I = 0xa

.field public static final SHOW_DESTINATION_DIRECTION:Z = true

.field public static final SLASH:Ljava/lang/String; = "/"

.field public static final SLASH_CHAR:C = '/'

.field private static final SMALL_UNITS_THRESHOLD:F = 0.1f

.field public static final SMART_MANEUVER_CALCULATION:Z = true

.field private static final SOON_DISTANCE_THRESHOLD:J = 0x507L

.field private static final SOON_DISTANCE_THRESHOLD_HIGHWAY:J = 0xc92L

.field public static final SPACE:Ljava/lang/String; = " "

.field public static final SPACE_CHAR:C = ' '

.field public static final START_TURN:Ljava/lang/String;

.field private static final STAY_ON:Ljava/lang/String;

.field private static final THEN_MANEUVER_THRESHOLD:J = 0xfaL

.field private static final THEN_MANEUVER_THRESHOLD_HIGHWAY:J = 0x3e8L

.field public static final TOWARDS:Ljava/lang/String;

.field private static final UNIT_FEET:Ljava/lang/String;

.field private static final UNIT_FEET_EXTENDED:Ljava/lang/String;

.field private static final UNIT_FEET_EXTENDED_SINGULAR:Ljava/lang/String;

.field private static final UNIT_KILOMETERS:Ljava/lang/String;

.field private static final UNIT_KILOMETERS_EXTENDED:Ljava/lang/String;

.field private static final UNIT_KILOMETERS_EXTENDED_SINGULAR:Ljava/lang/String;

.field private static final UNIT_METERS:Ljava/lang/String;

.field private static final UNIT_METERS_EXTENDED:Ljava/lang/String;

.field private static final UNIT_METERS_EXTENDED_SINGULAR:Ljava/lang/String;

.field private static final UNIT_MILES:Ljava/lang/String;

.field private static final UNIT_MILES_EXTENDED:Ljava/lang/String;

.field private static final UNIT_MILES_EXTENDED_SINGULAR:Ljava/lang/String;

.field private static final UTURN:Ljava/lang/String;

.field private static final allowedTurnTextMap:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static lastStayManeuver:Lcom/here/android/mpa/routing/Maneuver;

.field private static lastStayManeuverRoadName:Ljava/lang/String;

.field private static final mainSignPostBuilder:Ljava/lang/StringBuilder;

.field private static final resources:Landroid/content/res/Resources;

.field private static final sHereIconToNavigationIconMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/here/android/mpa/routing/Maneuver$Icon;",
            "Lcom/navdy/service/library/events/navigation/NavigationTurn;",
            ">;"
        }
    .end annotation
.end field

.field private static final sHereTurnToNavigationTurnMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/here/android/mpa/routing/Maneuver$Turn;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static final sTurnIconImageMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/navdy/service/library/events/navigation/NavigationTurn;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final sTurnIconSoonImageMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/navdy/service/library/events/navigation/NavigationTurn;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final sTurnIconSoonImageMapGrey:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/navdy/service/library/events/navigation/NavigationTurn;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final speedManager:Lcom/navdy/hud/app/manager/SpeedManager;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const v7, 0x7f09021a

    const v6, 0x7f090219

    const v5, 0x7f020268

    const v4, 0x7f020261

    .line 33
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 119
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    .line 120
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    .line 121
    invoke-static {}, Lcom/navdy/hud/app/manager/SpeedManager;->getInstance()Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    .line 165
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->allowedTurnTextMap:Ljava/util/HashSet;

    .line 167
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    invoke-direct {v0}, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->EMPTY_MANEUVER_DISPLAY:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    .line 170
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    .line 171
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->UNDEFINED:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_STRAIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->GO_STRAIGHT:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_STRAIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->KEEP_MIDDLE:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_STRAIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->KEEP_RIGHT:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_KEEP_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->LIGHT_RIGHT:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_EASY_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->QUITE_RIGHT:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->HEAVY_RIGHT:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_SHARP_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->KEEP_LEFT:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_KEEP_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->LIGHT_LEFT:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_EASY_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->QUITE_LEFT:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->HEAVY_LEFT:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_SHARP_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->UTURN_RIGHT:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_UTURN_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->UTURN_LEFT:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_UTURN_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_1:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_S:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_2:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_S:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_3:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_S:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_4:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_S:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_5:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_S:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 189
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_6:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_S:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_7:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_S:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_8:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_S:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_9:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_S:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_10:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_S:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_11:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_S:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_12:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_S:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->ENTER_HIGHWAY_RIGHT_LANE:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_MERGE_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->ENTER_HIGHWAY_LEFT_LANE:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_MERGE_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->LEAVE_HIGHWAY_RIGHT_LANE:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_EXIT_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->LEAVE_HIGHWAY_LEFT_LANE:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_EXIT_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->HIGHWAY_KEEP_RIGHT:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_KEEP_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->HIGHWAY_KEEP_LEFT:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_KEEP_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_1_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_N:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_2_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_N:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_3_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_N:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_4_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_N:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_5_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_N:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_6_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_N:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_7_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_N:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_8_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_N:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_9_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_N:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_10_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_N:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_11_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_N:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->ROUNDABOUT_12_LH:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_N:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->START:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_START:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->END:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_END:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 216
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->FERRY:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_FERRY:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Icon;->HEAD_TO:Lcom/here/android/mpa/routing/Maneuver$Icon;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_STRAIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0900f2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->ENTER_HIGHWAY:Ljava/lang/String;

    .line 221
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0900fb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->EXIT:Ljava/lang/String;

    .line 222
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0902e8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->UTURN:Ljava/lang/String;

    .line 223
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0900fc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->EXIT_NUMBER:Ljava/lang/String;

    .line 224
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v1, 0x7f09027a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->START_TURN:Ljava/lang/String;

    .line 225
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0900f0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->END_TURN:Ljava/lang/String;

    .line 226
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090294

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->TOWARDS:Ljava/lang/String;

    .line 227
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0901ed

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->ONTO:Ljava/lang/String;

    .line 228
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v1, 0x7f09027d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->STAY_ON:Ljava/lang/String;

    .line 229
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v1, 0x7f090181

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->HYPHEN:Ljava/lang/String;

    .line 230
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v1, 0x7f09019a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->DESTINATION_LEFT:Ljava/lang/String;

    .line 231
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v1, 0x7f09022f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->DESTINATION_RIGHT:Ljava/lang/String;

    .line 232
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v1, 0x7f09001e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->ARRIVED:Ljava/lang/String;

    .line 234
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereTurnToNavigationTurnMap:Ljava/util/HashMap;

    .line 235
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Turn;->UNDEFINED:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v2, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0902cb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Turn;->NO_TURN:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v2, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0901db

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 237
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Turn;->KEEP_MIDDLE:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v2, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v3, 0x7f090194

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Turn;->KEEP_RIGHT:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v2, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v3, 0x7f090195

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Turn;->LIGHT_RIGHT:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v2, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v3, 0x7f09026d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Turn;->QUITE_RIGHT:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v2, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Turn;->HEAVY_RIGHT:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v2, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Turn;->KEEP_LEFT:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v2, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v3, 0x7f090193

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Turn;->LIGHT_LEFT:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v2, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v3, 0x7f09026c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Turn;->QUITE_LEFT:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v2, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Turn;->HEAVY_LEFT:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v2, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 246
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Turn;->RETURN:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v2, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->UTURN:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 247
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Turn;->ROUNDABOUT_1:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v2, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v3, 0x7f090233

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 248
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Turn;->ROUNDABOUT_2:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v2, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v3, 0x7f090237

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 249
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Turn;->ROUNDABOUT_3:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v2, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v3, 0x7f090238

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Turn;->ROUNDABOUT_4:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v2, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v3, 0x7f090239

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Turn;->ROUNDABOUT_5:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v2, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v3, 0x7f09023a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Turn;->ROUNDABOUT_6:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v2, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v3, 0x7f09023b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 253
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Turn;->ROUNDABOUT_7:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v2, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v3, 0x7f09023c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Turn;->ROUNDABOUT_8:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v2, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v3, 0x7f09023d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 255
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Turn;->ROUNDABOUT_9:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v2, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v3, 0x7f09023e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 256
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Turn;->ROUNDABOUT_10:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v2, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v3, 0x7f090234

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Turn;->ROUNDABOUT_11:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v2, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v3, 0x7f090235

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 258
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v1, Lcom/here/android/mpa/routing/Maneuver$Turn;->ROUNDABOUT_12:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v2, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v3, 0x7f090236

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 260
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0902d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->UNIT_MILES:Ljava/lang/String;

    .line 261
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0902cc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->UNIT_FEET:Ljava/lang/String;

    .line 262
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0902d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->UNIT_METERS:Ljava/lang/String;

    .line 263
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0902cf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->UNIT_KILOMETERS:Ljava/lang/String;

    .line 265
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0902d6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->UNIT_MILES_EXTENDED:Ljava/lang/String;

    .line 266
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0902cd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->UNIT_FEET_EXTENDED:Ljava/lang/String;

    .line 267
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0902d3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->UNIT_METERS_EXTENDED:Ljava/lang/String;

    .line 268
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0902d0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->UNIT_KILOMETERS_EXTENDED:Ljava/lang/String;

    .line 270
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0902d7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->UNIT_MILES_EXTENDED_SINGULAR:Ljava/lang/String;

    .line 271
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0902ce

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->UNIT_FEET_EXTENDED_SINGULAR:Ljava/lang/String;

    .line 272
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0902d4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->UNIT_METERS_EXTENDED_SINGULAR:Ljava/lang/String;

    .line 273
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0902d1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->UNIT_KILOMETERS_EXTENDED_SINGULAR:Ljava/lang/String;

    .line 275
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconImageMap:Ljava/util/Map;

    .line 276
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconImageMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_START:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f020271

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 277
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconImageMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f020273

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconImageMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f020275

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 279
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconImageMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_EASY_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f020255

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 280
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconImageMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_EASY_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f020253

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconImageMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_SHARP_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f020269

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 282
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconImageMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_SHARP_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f02026b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 283
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconImageMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_EXIT_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f02025b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconImageMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_EXIT_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f020259

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 285
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconImageMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_KEEP_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f02026d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconImageMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_KEEP_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f02026f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 287
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconImageMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_END:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f020257

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconImageMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_STRAIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f02025d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 289
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconImageMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_UTURN_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f020277

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 290
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconImageMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_UTURN_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f020279

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 291
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconImageMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_S:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f020260

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 292
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconImageMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_N:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f020267

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 293
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconImageMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_MERGE_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f020262

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 294
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconImageMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_MERGE_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f020264

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMap:Ljava/util/Map;

    .line 297
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_START:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f020272

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 298
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f020274

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f020276

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 300
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_EASY_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f020256

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 301
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_EASY_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f020254

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 302
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_SHARP_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f02026a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 303
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_SHARP_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f02026c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 304
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_EXIT_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f02025c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 305
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_EXIT_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f02025a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 306
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_KEEP_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f02026e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 307
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_KEEP_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f020270

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 308
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_END:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f020258

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_STRAIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f02025e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 310
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_UTURN_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f020278

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 311
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_UTURN_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f02027a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 312
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_S:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 313
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_N:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 314
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_MERGE_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f020263

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 315
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_MERGE_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f020265

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 317
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMapGrey:Ljava/util/Map;

    .line 318
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMapGrey:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_START:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f020209

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 319
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMapGrey:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f020207

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 320
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMapGrey:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f020208

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 321
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMapGrey:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_EASY_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f020202

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 322
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMapGrey:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_EASY_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f020201

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 323
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMapGrey:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_SHARP_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f0201fb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 324
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMapGrey:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_SHARP_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f0201fc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 325
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMapGrey:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_EXIT_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f0201ff

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 326
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMapGrey:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_EXIT_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f020200

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 327
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMapGrey:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_KEEP_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f0201fd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 328
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMapGrey:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_KEEP_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f0201fe

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 329
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMapGrey:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_END:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f0201f7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 330
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMapGrey:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_STRAIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f0201fa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 331
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMapGrey:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_UTURN_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f02025f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 332
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMapGrey:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_UTURN_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f02020a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 333
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMapGrey:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_S:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 334
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMapGrey:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_N:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 335
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMapGrey:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_MERGE_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f020204

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 336
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMapGrey:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_MERGE_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const v2, 0x7f020205

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 338
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->EMPTY_MANEUVER_DISPLAY:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->empty:Z

    .line 340
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->allowedTurnTextMap:Ljava/util/HashSet;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->EXIT:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 341
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->allowedTurnTextMap:Ljava/util/HashSet;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->UTURN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 342
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->allowedTurnTextMap:Ljava/util/HashSet;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->START_TURN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 343
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->allowedTurnTextMap:Ljava/util/HashSet;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->END_TURN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 344
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->allowedTurnTextMap:Ljava/util/HashSet;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->ARRIVED:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 345
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->allowedTurnTextMap:Ljava/util/HashSet;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Turn;->ROUNDABOUT_1:Lcom/here/android/mpa/routing/Maneuver$Turn;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 346
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->allowedTurnTextMap:Ljava/util/HashSet;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Turn;->ROUNDABOUT_2:Lcom/here/android/mpa/routing/Maneuver$Turn;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 347
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->allowedTurnTextMap:Ljava/util/HashSet;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Turn;->ROUNDABOUT_3:Lcom/here/android/mpa/routing/Maneuver$Turn;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 348
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->allowedTurnTextMap:Ljava/util/HashSet;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Turn;->ROUNDABOUT_4:Lcom/here/android/mpa/routing/Maneuver$Turn;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 349
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->allowedTurnTextMap:Ljava/util/HashSet;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Turn;->ROUNDABOUT_5:Lcom/here/android/mpa/routing/Maneuver$Turn;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 350
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->allowedTurnTextMap:Ljava/util/HashSet;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Turn;->ROUNDABOUT_6:Lcom/here/android/mpa/routing/Maneuver$Turn;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 351
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->allowedTurnTextMap:Ljava/util/HashSet;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Turn;->ROUNDABOUT_7:Lcom/here/android/mpa/routing/Maneuver$Turn;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 352
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->allowedTurnTextMap:Ljava/util/HashSet;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Turn;->ROUNDABOUT_8:Lcom/here/android/mpa/routing/Maneuver$Turn;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 353
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->allowedTurnTextMap:Ljava/util/HashSet;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Turn;->ROUNDABOUT_9:Lcom/here/android/mpa/routing/Maneuver$Turn;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 354
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->allowedTurnTextMap:Ljava/util/HashSet;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Turn;->ROUNDABOUT_10:Lcom/here/android/mpa/routing/Maneuver$Turn;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 355
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->allowedTurnTextMap:Ljava/util/HashSet;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Turn;->ROUNDABOUT_11:Lcom/here/android/mpa/routing/Maneuver$Turn;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 356
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->allowedTurnTextMap:Ljava/util/HashSet;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Turn;->ROUNDABOUT_12:Lcom/here/android/mpa/routing/Maneuver$Turn;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 363
    new-instance v0, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;

    invoke-direct {v0}, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->DISTANCE:Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static canShowTurnText(Ljava/lang/String;)Z
    .locals 1
    .param p0, "turnText"    # Ljava/lang/String;

    .prologue
    .line 1239
    invoke-static {}, Lcom/navdy/hud/app/maps/MapSettings;->doNotShowTurnTextInTBT()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->allowedTurnTextMap:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1240
    :cond_0
    const/4 v0, 0x1

    .line 1242
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getArrivedManeuverDisplay()Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    .locals 4

    .prologue
    .line 623
    new-instance v1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    invoke-direct {v1}, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;-><init>()V

    .line 624
    .local v1, "display":Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    const-string v3, "arrived"

    iput-object v3, v1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->maneuverId:Ljava/lang/String;

    .line 625
    const v3, 0x7f0201f6

    iput v3, v1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->turnIconId:I

    .line 626
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->ARRIVED:Ljava/lang/String;

    iput-object v3, v1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingTurn:Ljava/lang/String;

    .line 627
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v2

    .line 628
    .local v2, "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getDestinationLabel()Ljava/lang/String;

    move-result-object v0

    .line 629
    .local v0, "currentRoad":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 630
    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getDestinationStreetAddress()Ljava/lang/String;

    move-result-object v0

    .line 631
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 632
    const-string v0, ""

    .line 636
    :cond_0
    iput-object v0, v1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingRoad:Ljava/lang/String;

    .line 637
    return-object v1
.end method

.method public static getDestinationDirection(Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;)Ljava/lang/String;
    .locals 2
    .param p0, "direction"    # Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;

    .prologue
    .line 1152
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$1;->$SwitchMap$com$navdy$hud$app$maps$MapEvents$DestinationDirection:[I

    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1160
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1154
    :pswitch_0
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->DESTINATION_LEFT:Ljava/lang/String;

    goto :goto_0

    .line 1157
    :pswitch_1
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->DESTINATION_RIGHT:Ljava/lang/String;

    goto :goto_0

    .line 1152
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getDestinationIcon(Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;)I
    .locals 2
    .param p0, "direction"    # Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;

    .prologue
    .line 1165
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$1;->$SwitchMap$com$navdy$hud$app$maps$MapEvents$DestinationDirection:[I

    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1173
    const v0, 0x7f0201a4

    :goto_0
    return v0

    .line 1167
    :pswitch_0
    const v0, 0x7f0201aa

    goto :goto_0

    .line 1170
    :pswitch_1
    const v0, 0x7f0201ad

    goto :goto_0

    .line 1165
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static getEstimatedPointAfromManeuver(Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/here/android/mpa/common/GeoCoordinate;
    .locals 10
    .param p0, "maneuver"    # Lcom/here/android/mpa/routing/Maneuver;
    .param p1, "destination"    # Lcom/here/android/mpa/common/GeoCoordinate;

    .prologue
    .line 1178
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 1179
    :cond_0
    const/4 v1, 0x0

    .line 1199
    :goto_0
    return-object v1

    .line 1182
    :cond_1
    const/4 v4, 0x0

    .line 1183
    .local v4, "last":Lcom/here/android/mpa/common/GeoCoordinate;
    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Maneuver;->getManeuverGeometry()Ljava/util/List;

    move-result-object v2

    .line 1184
    .local v2, "geoCoordinates":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/common/GeoCoordinate;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    .line 1186
    .local v5, "len":I
    add-int/lit8 v3, v5, -0x1

    .local v3, "i":I
    :goto_1
    if-ltz v3, :cond_3

    .line 1187
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/here/android/mpa/common/GeoCoordinate;

    .line 1188
    .local v1, "geo":Lcom/here/android/mpa/common/GeoCoordinate;
    invoke-virtual {v1, p1}, Lcom/here/android/mpa/common/GeoCoordinate;->distanceTo(Lcom/here/android/mpa/common/GeoCoordinate;)D

    move-result-wide v6

    double-to-int v0, v6

    .line 1189
    .local v0, "distance":I
    const/16 v6, 0x1e

    if-lt v0, v6, :cond_2

    .line 1190
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "remaining="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 1193
    :cond_2
    move-object v4, v1

    .line 1186
    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    .line 1196
    .end local v0    # "distance":I
    .end local v1    # "geo":Lcom/here/android/mpa/common/GeoCoordinate;
    :cond_3
    if-eqz v4, :cond_4

    .line 1197
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "remaining="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4, p1}, Lcom/here/android/mpa/common/GeoCoordinate;->distanceTo(Lcom/here/android/mpa/common/GeoCoordinate;)D

    move-result-wide v8

    double-to-int v8, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    :cond_4
    move-object v1, v4

    .line 1199
    goto :goto_0
.end method

.method public static getFormattedDistance(FLcom/navdy/service/library/events/navigation/DistanceUnit;Z)Ljava/lang/String;
    .locals 8
    .param p0, "distance"    # F
    .param p1, "unit"    # Lcom/navdy/service/library/events/navigation/DistanceUnit;
    .param p2, "extendedUnit"    # Z

    .prologue
    const/high16 v7, 0x41200000    # 10.0f

    const/4 v3, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 1205
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$1;->$SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit:[I

    invoke-virtual {p1}, Lcom/navdy/service/library/events/navigation/DistanceUnit;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1235
    float-to-int v1, p0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    .line 1207
    :pswitch_0
    cmpg-float v1, p0, v7

    if-gez v1, :cond_0

    .line 1208
    const-string v0, "%.1f %s"

    .line 1213
    .local v0, "distanceFormat":Ljava/lang/String;
    :goto_1
    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v2, v5

    if-nez p2, :cond_1

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->UNIT_MILES:Ljava/lang/String;

    :goto_2
    aput-object v1, v2, v6

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1210
    .end local v0    # "distanceFormat":Ljava/lang/String;
    :cond_0
    const-string v0, "%.0f %s"

    .restart local v0    # "distanceFormat":Ljava/lang/String;
    goto :goto_1

    .line 1213
    :cond_1
    cmpg-float v1, p0, v4

    if-gtz v1, :cond_2

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->UNIT_MILES_EXTENDED_SINGULAR:Ljava/lang/String;

    goto :goto_2

    :cond_2
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->UNIT_MILES_EXTENDED:Ljava/lang/String;

    goto :goto_2

    .line 1217
    .end local v0    # "distanceFormat":Ljava/lang/String;
    :pswitch_1
    const-string v2, "%.0f %s"

    new-array v3, v3, [Ljava/lang/Object;

    const/16 v1, 0xa

    invoke-static {v1, p0}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->roundToIntegerStep(IF)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v3, v5

    if-nez p2, :cond_3

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->UNIT_FEET:Ljava/lang/String;

    :goto_3
    aput-object v1, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_3
    cmpg-float v1, p0, v4

    if-gtz v1, :cond_4

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->UNIT_FEET_EXTENDED_SINGULAR:Ljava/lang/String;

    goto :goto_3

    :cond_4
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->UNIT_FEET_EXTENDED:Ljava/lang/String;

    goto :goto_3

    .line 1221
    :pswitch_2
    cmpg-float v1, p0, v7

    if-gez v1, :cond_5

    .line 1222
    const-string v0, "%.1f %s"

    .line 1227
    .restart local v0    # "distanceFormat":Ljava/lang/String;
    :goto_4
    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v2, v5

    if-nez p2, :cond_6

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->UNIT_KILOMETERS:Ljava/lang/String;

    :goto_5
    aput-object v1, v2, v6

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1224
    .end local v0    # "distanceFormat":Ljava/lang/String;
    :cond_5
    const-string v0, "%.0f %s"

    .restart local v0    # "distanceFormat":Ljava/lang/String;
    goto :goto_4

    .line 1227
    :cond_6
    cmpg-float v1, p0, v4

    if-gtz v1, :cond_7

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->UNIT_KILOMETERS_EXTENDED_SINGULAR:Ljava/lang/String;

    goto :goto_5

    :cond_7
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->UNIT_KILOMETERS_EXTENDED:Ljava/lang/String;

    goto :goto_5

    .line 1231
    .end local v0    # "distanceFormat":Ljava/lang/String;
    :pswitch_3
    const-string v2, "%.0f %s"

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v1, 0x5

    invoke-static {v1, p0}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->roundToIntegerStep(IF)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v3, v5

    if-nez p2, :cond_8

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->UNIT_METERS:Ljava/lang/String;

    :goto_6
    aput-object v1, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    :cond_8
    cmpg-float v1, p0, v4

    if-gtz v1, :cond_9

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->UNIT_METERS_EXTENDED_SINGULAR:Ljava/lang/String;

    goto :goto_6

    :cond_9
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->UNIT_METERS_EXTENDED:Ljava/lang/String;

    goto :goto_6

    .line 1205
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static getManeuverDisplay(Lcom/here/android/mpa/routing/Maneuver;ZJLjava/lang/String;Lcom/here/android/mpa/routing/Maneuver;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/routing/Maneuver;ZZLcom/navdy/hud/app/maps/MapEvents$DestinationDirection;)Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    .locals 38
    .param p0, "current"    # Lcom/here/android/mpa/routing/Maneuver;
    .param p1, "last"    # Z
    .param p2, "distance"    # J
    .param p4, "streetAddress"    # Ljava/lang/String;
    .param p5, "previousManeuver"    # Lcom/here/android/mpa/routing/Maneuver;
    .param p6, "request"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .param p7, "nextManeuver"    # Lcom/here/android/mpa/routing/Maneuver;
    .param p8, "calculateEarlyManeuver"    # Z
    .param p9, "calcManeuverState"    # Z
    .param p10, "destinationDirection"    # Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;

    .prologue
    .line 378
    new-instance v24, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$PendingRoadInfo;

    invoke-direct/range {v24 .. v24}, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$PendingRoadInfo;-><init>()V

    .line 379
    .local v24, "info":Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$PendingRoadInfo;
    new-instance v9, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    invoke-direct {v9}, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;-><init>()V

    .line 380
    .local v9, "display":Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    if-eqz p0, :cond_c

    invoke-static/range {p0 .. p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    :goto_0
    iput-object v5, v9, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->maneuverId:Ljava/lang/String;

    .line 382
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getCurrentRoadElement()Lcom/here/android/mpa/common/RoadElement;

    move-result-object v18

    .line 383
    .local v18, "currentRoadElement":Lcom/here/android/mpa/common/RoadElement;
    invoke-static/range {p0 .. p0}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->isCurrentRoadHighway(Lcom/here/android/mpa/routing/Maneuver;)Z

    move-result v11

    .line 385
    .local v11, "isHighway":Z
    if-nez v11, :cond_0

    .line 386
    invoke-static/range {v18 .. v18}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->isCurrentRoadHighway(Lcom/here/android/mpa/common/RoadElement;)Z

    move-result v11

    .line 389
    :cond_0
    if-eqz p9, :cond_d

    .line 390
    const-wide/32 v6, 0x7fffffff

    cmp-long v5, p2, v6

    if-ltz v5, :cond_1

    .line 391
    const-wide/16 p2, 0x0

    .line 393
    :cond_1
    move-wide/from16 v0, p2

    invoke-static {v0, v1, v11}, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->getManeuverState(JZ)Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    move-result-object v5

    iput-object v5, v9, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->maneuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    .line 400
    :goto_1
    iget-object v8, v9, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->maneuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    if-eqz p8, :cond_e

    move-object/from16 v10, p7

    :goto_2
    move-object/from16 v5, p0

    move-wide/from16 v6, p2

    move-object/from16 v12, p10

    invoke-static/range {v5 .. v12}, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->setNavigationTurnIconId(Lcom/here/android/mpa/routing/Maneuver;JLcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;Lcom/here/android/mpa/routing/Maneuver;ZLcom/navdy/hud/app/maps/MapEvents$DestinationDirection;)V

    .line 408
    const/4 v6, 0x0

    if-nez p9, :cond_f

    const/4 v5, 0x1

    :goto_3
    move-wide/from16 v0, p2

    invoke-static {v0, v1, v9, v6, v5}, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->setNavigationDistance(JLcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;ZZ)V

    .line 410
    iget-object v5, v9, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->maneuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    sget-object v6, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->STAY:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    if-ne v5, v6, :cond_12

    .line 411
    if-nez v18, :cond_10

    .line 412
    const/4 v5, 0x0

    iput-object v5, v9, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->currentRoad:Ljava/lang/String;

    .line 421
    :cond_2
    :goto_4
    iget-object v5, v9, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->currentRoad:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 422
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->lastStayManeuver:Lcom/here/android/mpa/routing/Maneuver;

    move-object/from16 v0, p0

    if-ne v5, v0, :cond_11

    .line 423
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->lastStayManeuverRoadName:Ljava/lang/String;

    if-eqz v5, :cond_3

    .line 424
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->lastStayManeuverRoadName:Ljava/lang/String;

    iput-object v5, v9, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->currentRoad:Ljava/lang/String;

    .line 432
    :cond_3
    :goto_5
    iget-object v5, v9, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->currentRoad:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 434
    if-eqz p5, :cond_4

    .line 435
    invoke-virtual/range {p5 .. p5}, Lcom/here/android/mpa/routing/Maneuver;->getNextRoadNumber()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p5 .. p5}, Lcom/here/android/mpa/routing/Maneuver;->getNextRoadName()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    invoke-static {v5, v6, v7, v11}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->concatText(Ljava/lang/String;Ljava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v9, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->currentRoad:Ljava/lang/String;

    .line 438
    :cond_4
    iget-object v5, v9, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->currentRoad:Ljava/lang/String;

    if-nez v5, :cond_5

    .line 439
    const-string v5, ""

    iput-object v5, v9, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->currentRoad:Ljava/lang/String;

    .line 454
    :cond_5
    :goto_6
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getCurrentSpeedLimit()F

    move-result v5

    iput v5, v9, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->currentSpeedLimit:F

    .line 455
    if-eqz p1, :cond_1c

    .line 457
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->END_TURN:Ljava/lang/String;

    iput-object v5, v9, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingTurn:Ljava/lang/String;

    .line 458
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getDestinationLabel()Ljava/lang/String;

    move-result-object v17

    .line 459
    .local v17, "currentRoad":Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 460
    if-eqz p4, :cond_13

    .line 461
    invoke-static/range {p4 .. p4}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->parseStreetAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 465
    :goto_7
    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 466
    invoke-virtual/range {p0 .. p0}, Lcom/here/android/mpa/routing/Maneuver;->getRoadName()Ljava/lang/String;

    move-result-object v17

    .line 471
    :cond_6
    const/16 v20, 0x0

    .line 474
    .local v20, "direction":Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;
    if-eqz p6, :cond_a

    .line 475
    move-object/from16 v0, p6

    iget-object v15, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    .line 476
    .local v15, "c1":Lcom/navdy/service/library/events/location/Coordinate;
    move-object/from16 v0, p6

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationDisplay:Lcom/navdy/service/library/events/location/Coordinate;

    move-object/from16 v16, v0

    .line 478
    .local v16, "c2":Lcom/navdy/service/library/events/location/Coordinate;
    if-eqz v15, :cond_19

    if-eqz v16, :cond_19

    .line 485
    new-instance v14, Lcom/here/android/mpa/common/GeoCoordinate;

    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationDisplay:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v5, v5, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationDisplay:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v5, v5, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v32

    move-wide/from16 v0, v32

    invoke-direct {v14, v6, v7, v0, v1}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    .line 486
    .local v14, "M":Lcom/here/android/mpa/common/GeoCoordinate;
    new-instance v13, Lcom/here/android/mpa/common/GeoCoordinate;

    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v5, v5, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    move-object/from16 v0, p6

    iget-object v5, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v5, v5, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v32

    move-wide/from16 v0, v32

    invoke-direct {v13, v6, v7, v0, v1}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    .line 487
    .local v13, "B":Lcom/here/android/mpa/common/GeoCoordinate;
    if-nez p5, :cond_7

    .line 489
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentRoute()Lcom/here/android/mpa/routing/Route;

    move-result-object v29

    .line 490
    .local v29, "route":Lcom/here/android/mpa/routing/Route;
    if-eqz v29, :cond_7

    .line 491
    invoke-virtual/range {v29 .. v29}, Lcom/here/android/mpa/routing/Route;->getManeuvers()Ljava/util/List;

    move-result-object v25

    .line 492
    .local v25, "maneuvers":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    if-eqz v25, :cond_7

    invoke-interface/range {v25 .. v25}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_7

    .line 493
    const/4 v5, 0x0

    move-object/from16 v0, v25

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p5

    .end local p5    # "previousManeuver":Lcom/here/android/mpa/routing/Maneuver;
    check-cast p5, Lcom/here/android/mpa/routing/Maneuver;

    .line 497
    .end local v25    # "maneuvers":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    .end local v29    # "route":Lcom/here/android/mpa/routing/Route;
    .restart local p5    # "previousManeuver":Lcom/here/android/mpa/routing/Maneuver;
    :cond_7
    move-object/from16 v0, p5

    invoke-static {v0, v13}, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->getEstimatedPointAfromManeuver(Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v4

    .line 499
    .local v4, "A":Lcom/here/android/mpa/common/GeoCoordinate;
    if-eqz v4, :cond_18

    .line 500
    invoke-virtual {v13}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v6

    invoke-virtual {v4}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v32

    sub-double v6, v6, v32

    invoke-virtual {v14}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v32

    invoke-virtual {v4}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v34

    sub-double v32, v32, v34

    mul-double v6, v6, v32

    invoke-virtual {v13}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v32

    invoke-virtual {v4}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v34

    sub-double v32, v32, v34

    invoke-virtual {v14}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v34

    invoke-virtual {v4}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v36

    sub-double v34, v34, v36

    mul-double v32, v32, v34

    sub-double v6, v6, v32

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v30

    .line 502
    .local v30, "position":D
    const-wide/16 v6, 0x0

    cmpl-double v5, v30, v6

    if-ltz v5, :cond_14

    .line 503
    sget-object v20, Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;->RIGHT:Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;

    .line 510
    :goto_8
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "dest(1) position="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v30

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " DIRECTION = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v20

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 511
    move-object/from16 v0, v20

    iput-object v0, v9, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->direction:Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;

    .line 513
    sget-object v5, Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;->UNKNOWN:Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;

    move-object/from16 v0, v20

    if-eq v0, v5, :cond_8

    .line 514
    invoke-static/range {v20 .. v20}, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->getDestinationIcon(Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;)I

    move-result v5

    iput v5, v9, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->destinationIconId:I

    .line 518
    :cond_8
    invoke-virtual {v13, v14}, Lcom/here/android/mpa/common/GeoCoordinate;->getHeading(Lcom/here/android/mpa/common/GeoCoordinate;)D

    move-result-wide v26

    .line 519
    .local v26, "navToDisplayAngle":D
    invoke-virtual {v4, v13}, Lcom/here/android/mpa/common/GeoCoordinate;->getHeading(Lcom/here/android/mpa/common/GeoCoordinate;)D

    move-result-wide v22

    .line 520
    .local v22, "estimatedToNavAngle":D
    sub-double v6, v26, v22

    double-to-int v0, v6

    move/from16 v19, v0

    .line 521
    .local v19, "diffAngle":I
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "dest(2): navToDisplayAngle="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v26

    double-to-int v7, v0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " currentHereAngle="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v22

    double-to-int v7, v0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " diffAngle ="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 522
    if-gez v19, :cond_9

    .line 523
    move/from16 v0, v19

    add-int/lit16 v0, v0, 0x168

    move/from16 v19, v0

    .line 527
    :cond_9
    const/4 v5, 0x1

    move/from16 v0, v19

    if-lt v0, v5, :cond_16

    const/16 v5, 0xb3

    move/from16 v0, v19

    if-gt v0, v5, :cond_16

    .line 528
    sget-object v21, Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;->RIGHT:Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;

    .line 534
    .local v21, "direction2":Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;
    :goto_9
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "dest(2): diffAngle="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " direction="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v21

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 546
    .end local v4    # "A":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v13    # "B":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v14    # "M":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v15    # "c1":Lcom/navdy/service/library/events/location/Coordinate;
    .end local v16    # "c2":Lcom/navdy/service/library/events/location/Coordinate;
    .end local v19    # "diffAngle":I
    .end local v21    # "direction2":Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;
    .end local v22    # "estimatedToNavAngle":D
    .end local v26    # "navToDisplayAngle":D
    .end local v30    # "position":D
    :cond_a
    :goto_a
    if-eqz v20, :cond_1a

    .line 547
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v6, 0x7f0900ee

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    .line 548
    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_b

    sget-object v10, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v12, 0x7f0900ef

    invoke-virtual {v10, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    .end local v17    # "currentRoad":Ljava/lang/String;
    :cond_b
    aput-object v17, v7, v8

    const/4 v8, 0x1

    .line 549
    invoke-static/range {v20 .. v20}, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->getDestinationDirection(Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v7, v8

    .line 547
    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v9, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingRoad:Ljava/lang/String;

    .line 571
    .end local v20    # "direction":Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;
    :goto_b
    return-object v9

    .line 380
    .end local v11    # "isHighway":Z
    .end local v18    # "currentRoadElement":Lcom/here/android/mpa/common/RoadElement;
    :cond_c
    const-string v5, "unknown"

    goto/16 :goto_0

    .line 397
    .restart local v11    # "isHighway":Z
    .restart local v18    # "currentRoadElement":Lcom/here/android/mpa/common/RoadElement;
    :cond_d
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->NOW:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    iput-object v5, v9, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->maneuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    goto/16 :goto_1

    .line 400
    :cond_e
    const/4 v10, 0x0

    goto/16 :goto_2

    .line 408
    :cond_f
    const/4 v5, 0x0

    goto/16 :goto_3

    .line 414
    :cond_10
    invoke-virtual/range {v18 .. v18}, Lcom/here/android/mpa/common/RoadElement;->getRouteName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v18 .. v18}, Lcom/here/android/mpa/common/RoadElement;->getRoadName()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    invoke-static {v5, v6, v7, v11}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->concatText(Ljava/lang/String;Ljava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v9, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->currentRoad:Ljava/lang/String;

    .line 415
    iget-object v5, v9, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->currentRoad:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 416
    iget-object v5, v9, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->currentRoad:Ljava/lang/String;

    sput-object v5, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->lastStayManeuverRoadName:Ljava/lang/String;

    .line 417
    sput-object p0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->lastStayManeuver:Lcom/here/android/mpa/routing/Maneuver;

    goto/16 :goto_4

    .line 427
    :cond_11
    const/4 v5, 0x0

    sput-object v5, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->lastStayManeuver:Lcom/here/android/mpa/routing/Maneuver;

    .line 428
    const/4 v5, 0x0

    sput-object v5, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->lastStayManeuverRoadName:Ljava/lang/String;

    goto/16 :goto_5

    .line 443
    :cond_12
    const/4 v5, 0x0

    sput-object v5, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->lastStayManeuver:Lcom/here/android/mpa/routing/Maneuver;

    .line 444
    const/4 v5, 0x0

    sput-object v5, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->lastStayManeuverRoadName:Ljava/lang/String;

    .line 446
    invoke-virtual/range {p0 .. p0}, Lcom/here/android/mpa/routing/Maneuver;->getRoadNumber()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/here/android/mpa/routing/Maneuver;->getRoadName()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    invoke-static {v5, v6, v7, v11}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->concatText(Ljava/lang/String;Ljava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v9, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->currentRoad:Ljava/lang/String;

    .line 448
    iget-object v5, v9, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->currentRoad:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 450
    move-object/from16 v0, p0

    move-object/from16 v1, p5

    move-object/from16 v2, p7

    move-object/from16 v3, v24

    invoke-static {v0, v1, v2, v3}, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->getPendingRoadText(Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$PendingRoadInfo;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v9, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->currentRoad:Ljava/lang/String;

    goto/16 :goto_6

    .line 463
    .restart local v17    # "currentRoad":Ljava/lang/String;
    :cond_13
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getDestinationStreetAddress()Ljava/lang/String;

    move-result-object v17

    goto/16 :goto_7

    .line 504
    .restart local v4    # "A":Lcom/here/android/mpa/common/GeoCoordinate;
    .restart local v13    # "B":Lcom/here/android/mpa/common/GeoCoordinate;
    .restart local v14    # "M":Lcom/here/android/mpa/common/GeoCoordinate;
    .restart local v15    # "c1":Lcom/navdy/service/library/events/location/Coordinate;
    .restart local v16    # "c2":Lcom/navdy/service/library/events/location/Coordinate;
    .restart local v20    # "direction":Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;
    .restart local v30    # "position":D
    :cond_14
    const-wide/16 v6, 0x0

    cmpg-double v5, v30, v6

    if-gez v5, :cond_15

    .line 505
    sget-object v20, Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;->LEFT:Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;

    goto/16 :goto_8

    .line 507
    :cond_15
    sget-object v20, Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;->UNKNOWN:Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;

    goto/16 :goto_8

    .line 529
    .restart local v19    # "diffAngle":I
    .restart local v22    # "estimatedToNavAngle":D
    .restart local v26    # "navToDisplayAngle":D
    :cond_16
    const/16 v5, 0xb5

    move/from16 v0, v19

    if-lt v0, v5, :cond_17

    const/16 v5, 0x167

    move/from16 v0, v19

    if-gt v0, v5, :cond_17

    .line 530
    sget-object v21, Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;->LEFT:Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;

    .restart local v21    # "direction2":Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;
    goto/16 :goto_9

    .line 532
    .end local v21    # "direction2":Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;
    :cond_17
    sget-object v21, Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;->UNKNOWN:Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;

    .restart local v21    # "direction2":Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;
    goto/16 :goto_9

    .line 536
    .end local v19    # "diffAngle":I
    .end local v21    # "direction2":Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;
    .end local v22    # "estimatedToNavAngle":D
    .end local v26    # "navToDisplayAngle":D
    .end local v30    # "position":D
    :cond_18
    sget-object v5, Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;->UNKNOWN:Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;

    iput-object v5, v9, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->direction:Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;

    .line 537
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "dest(1) cannot calculate direction, A pos not available"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_a

    .line 540
    .end local v4    # "A":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v13    # "B":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v14    # "M":Lcom/here/android/mpa/common/GeoCoordinate;
    :cond_19
    sget-object v5, Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;->UNKNOWN:Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;

    iput-object v5, v9, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->direction:Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;

    .line 541
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "dest(1) cannot calculate direction pos not available"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_a

    .line 551
    .end local v15    # "c1":Lcom/navdy/service/library/events/location/Coordinate;
    .end local v16    # "c2":Lcom/navdy/service/library/events/location/Coordinate;
    :cond_1a
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v6, 0x7f0900ed

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    .line 552
    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_1b

    sget-object v10, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v12, 0x7f0900ef

    invoke-virtual {v10, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    .end local v17    # "currentRoad":Ljava/lang/String;
    :cond_1b
    aput-object v17, v7, v8

    .line 551
    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v9, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingRoad:Ljava/lang/String;

    goto/16 :goto_b

    .line 556
    .end local v20    # "direction":Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;
    :cond_1c
    iget-object v5, v9, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->maneuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move-object/from16 v2, p5

    move-object/from16 v3, p7

    invoke-static {v0, v1, v5, v2, v3}, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->getTurnText(Lcom/here/android/mpa/routing/Maneuver;Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$PendingRoadInfo;Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v9, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingTurn:Ljava/lang/String;

    .line 557
    iget-object v5, v9, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->maneuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    sget-object v6, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->STAY:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    if-ne v5, v6, :cond_1d

    .line 558
    iget-object v5, v9, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->currentRoad:Ljava/lang/String;

    iput-object v5, v9, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingRoad:Ljava/lang/String;

    goto/16 :goto_b

    .line 560
    :cond_1d
    move-object/from16 v0, p0

    move-object/from16 v1, p5

    move-object/from16 v2, p7

    move-object/from16 v3, v24

    invoke-static {v0, v1, v2, v3}, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->getPendingRoadText(Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$PendingRoadInfo;)Ljava/lang/String;

    move-result-object v28

    .line 561
    .local v28, "pendingRoadText":Ljava/lang/String;
    invoke-static/range {v28 .. v28}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1e

    .line 562
    invoke-virtual/range {p0 .. p0}, Lcom/here/android/mpa/routing/Maneuver;->getRoadElements()Ljava/util/List;

    move-result-object v5

    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getRoadName(Ljava/util/List;)Ljava/lang/String;

    move-result-object v28

    .line 564
    :cond_1e
    invoke-static/range {v28 .. v28}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1f

    .line 565
    move-object/from16 v0, v28

    iput-object v0, v9, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingRoad:Ljava/lang/String;

    goto/16 :goto_b

    .line 567
    :cond_1f
    const-string v5, ""

    iput-object v5, v9, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingRoad:Ljava/lang/String;

    goto/16 :goto_b
.end method

.method static getManeuverState(JZ)Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;
    .locals 10
    .param p0, "distance"    # J
    .param p2, "isHighway"    # Z

    .prologue
    .line 1109
    if-eqz p2, :cond_1

    .line 1110
    const-wide v2, 0x40cf6e8000000000L    # 16093.0

    .line 1111
    .local v2, "nextThreshold":D
    const-wide v6, 0x40a9240000000000L    # 3218.0

    .line 1112
    .local v6, "soonThreshold":D
    const-wide/16 v4, 0x64

    .line 1118
    .local v4, "nowThreshold":J
    :goto_0
    cmp-long v1, p0, v4

    if-gtz v1, :cond_2

    .line 1119
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->NOW:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    .line 1128
    .local v0, "maneuverState":Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;
    :goto_1
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v8, 0x2

    invoke-virtual {v1, v8}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1129
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "maneuverState="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " distance="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " isHighway="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1133
    :cond_0
    return-object v0

    .line 1114
    .end local v0    # "maneuverState":Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;
    .end local v2    # "nextThreshold":D
    .end local v4    # "nowThreshold":J
    .end local v6    # "soonThreshold":D
    :cond_1
    const-wide v2, 0x40b9250000000000L    # 6437.0

    .line 1115
    .restart local v2    # "nextThreshold":D
    const-wide v6, 0x40941c0000000000L    # 1287.0

    .line 1116
    .restart local v6    # "soonThreshold":D
    const-wide/16 v4, 0x1e

    .restart local v4    # "nowThreshold":J
    goto :goto_0

    .line 1120
    :cond_2
    long-to-double v8, p0

    cmpg-double v1, v8, v6

    if-gtz v1, :cond_3

    .line 1121
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->SOON:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    .restart local v0    # "maneuverState":Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;
    goto :goto_1

    .line 1122
    .end local v0    # "maneuverState":Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;
    :cond_3
    long-to-double v8, p0

    cmpg-double v1, v8, v2

    if-gtz v1, :cond_4

    .line 1123
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->NEXT:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    .restart local v0    # "maneuverState":Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;
    goto :goto_1

    .line 1125
    .end local v0    # "maneuverState":Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;
    :cond_4
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->STAY:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    .restart local v0    # "maneuverState":Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;
    goto :goto_1
.end method

.method private static getNavigationTurn(Lcom/here/android/mpa/routing/Maneuver;)Lcom/navdy/service/library/events/navigation/NavigationTurn;
    .locals 2
    .param p0, "maneuver"    # Lcom/here/android/mpa/routing/Maneuver;

    .prologue
    .line 939
    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Maneuver;->getIcon()Lcom/here/android/mpa/routing/Maneuver$Icon;

    move-result-object v0

    .line 940
    .local v0, "icon":Lcom/here/android/mpa/routing/Maneuver$Icon;
    if-nez v0, :cond_0

    .line 941
    const/4 v1, 0x0

    .line 943
    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereIconToNavigationIconMap:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;

    goto :goto_0
.end method

.method private static getPendingRoadText(Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$PendingRoadInfo;)Ljava/lang/String;
    .locals 25
    .param p0, "maneuver"    # Lcom/here/android/mpa/routing/Maneuver;
    .param p1, "before"    # Lcom/here/android/mpa/routing/Maneuver;
    .param p2, "after"    # Lcom/here/android/mpa/routing/Maneuver;
    .param p3, "pendingRoadInfo"    # Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$PendingRoadInfo;

    .prologue
    .line 724
    if-eqz p3, :cond_0

    move-object/from16 v0, p3

    iget-boolean v0, v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$PendingRoadInfo;->usePrevManeuverInfo:Z

    move/from16 v21, v0

    if-eqz v21, :cond_0

    if-eqz p1, :cond_0

    .line 725
    move-object/from16 p0, p1

    .line 726
    const/16 p1, 0x0

    .line 727
    const/16 p2, 0x0

    .line 730
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/here/android/mpa/routing/Maneuver;->getNextRoadNumber()Ljava/lang/String;

    move-result-object v11

    .line 731
    .local v11, "number":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/here/android/mpa/routing/Maneuver;->getNextRoadName()Ljava/lang/String;

    move-result-object v9

    .line 732
    .local v9, "name":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/here/android/mpa/routing/Maneuver;->getSignpost()Lcom/here/android/mpa/routing/Signpost;

    move-result-object v15

    .line 735
    .local v15, "signpost":Lcom/here/android/mpa/routing/Signpost;
    new-instance v7, Lcom/navdy/hud/app/maps/here/HereMapUtil$SignPostInfo;

    invoke-direct {v7}, Lcom/navdy/hud/app/maps/here/HereMapUtil$SignPostInfo;-><init>()V

    .line 736
    .local v7, "info":Lcom/navdy/hud/app/maps/here/HereMapUtil$SignPostInfo;
    const/16 v21, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-static {v15, v0, v7, v1}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getSignpostText(Lcom/here/android/mpa/routing/Signpost;Lcom/here/android/mpa/routing/Maneuver;Lcom/navdy/hud/app/maps/here/HereMapUtil$SignPostInfo;Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v14

    .line 738
    .local v14, "signPostText":Ljava/lang/String;
    if-eqz p3, :cond_2

    move-object/from16 v0, p3

    iget-boolean v0, v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$PendingRoadInfo;->dontUseSignpostText:Z

    move/from16 v21, v0

    if-eqz v21, :cond_2

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v21

    if-eqz v21, :cond_1

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v21

    if-nez v21, :cond_2

    .line 739
    :cond_1
    const/4 v14, 0x0

    .line 740
    const/16 v21, 0x0

    move/from16 v0, v21

    iput-boolean v0, v7, Lcom/navdy/hud/app/maps/here/HereMapUtil$SignPostInfo;->hasNameInSignPost:Z

    .line 741
    const/16 v21, 0x0

    move/from16 v0, v21

    iput-boolean v0, v7, Lcom/navdy/hud/app/maps/here/HereMapUtil$SignPostInfo;->hasNumberInSignPost:Z

    .line 745
    :cond_2
    const/4 v8, 0x0

    .line 746
    .local v8, "maneuverMatch":Z
    const/16 v20, 0x0

    .line 747
    .local v20, "useToRoad":Z
    const/16 v19, 0x0

    .line 748
    .local v19, "useSignpost":Z
    const/16 v18, 0x0

    .line 749
    .local v18, "toInstructionAdded":Z
    const/4 v13, 0x0

    .line 753
    .local v13, "ontoInstructionAdded":Z
    invoke-static/range {p0 .. p1}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->hasSameSignpostAndToRoad(Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;)Z

    move-result v8

    .line 754
    if-eqz v8, :cond_19

    .line 756
    const/16 v20, 0x1

    .line 767
    :cond_3
    :goto_0
    sget-object v22, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    monitor-enter v22

    .line 768
    :try_start_0
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    const/16 v23, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 769
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v21

    if-nez v21, :cond_4

    iget-boolean v0, v7, Lcom/navdy/hud/app/maps/here/HereMapUtil$SignPostInfo;->hasNumberInSignPost:Z

    move/from16 v21, v0

    if-nez v21, :cond_4

    .line 770
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    move-object/from16 v0, v21

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 773
    :cond_4
    const/4 v5, 0x0

    .line 774
    .local v5, "hasSignPost":Z
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v21

    if-nez v21, :cond_7

    .line 775
    const/4 v3, 0x1

    .line 777
    .local v3, "addSignpost":Z
    if-eqz v8, :cond_5

    if-nez v19, :cond_5

    .line 778
    const/4 v3, 0x0

    .line 779
    const-string v14, ""

    .line 781
    :cond_5
    const/4 v5, 0x1

    .line 782
    if-eqz v3, :cond_7

    .line 783
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    invoke-static/range {v21 .. v21}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->needSeparator(Ljava/lang/StringBuilder;)Z

    move-result v21

    if-eqz v21, :cond_6

    .line 784
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    const-string v23, "/"

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 786
    :cond_6
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 790
    .end local v3    # "addSignpost":Z
    :cond_7
    const/16 v17, 0x0

    .line 792
    .local v17, "toInstruction":Ljava/lang/String;
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v21

    if-nez v21, :cond_17

    .line 793
    if-eqz v5, :cond_e

    .line 795
    invoke-virtual {v14, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v21

    if-nez v21, :cond_e

    .line 797
    const/4 v2, 0x1

    .line 799
    .local v2, "addOnto":Z
    if-eqz v8, :cond_8

    if-nez v20, :cond_8

    .line 800
    const/4 v2, 0x0

    .line 803
    :cond_8
    if-eqz v2, :cond_d

    .line 804
    if-eqz p3, :cond_1a

    move-object/from16 v0, p3

    iget-boolean v0, v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$PendingRoadInfo;->hasTo:Z

    move/from16 v21, v0

    if-eqz v21, :cond_1a

    .line 807
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 808
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    const/16 v23, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 814
    :cond_9
    :goto_1
    if-nez v20, :cond_1b

    .line 815
    invoke-static {}, Lcom/navdy/hud/app/maps/MapSettings;->isTbtOntoDisabled()Z

    move-result v21

    if-nez v21, :cond_a

    .line 816
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    sget-object v23, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->ONTO:Ljava/lang/String;

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 817
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    const-string v23, " "

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 818
    const/4 v13, 0x1

    .line 826
    :cond_a
    :goto_2
    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v21

    if-eqz v21, :cond_c

    .line 827
    if-eqz p3, :cond_c

    move-object/from16 v0, p3

    iget-boolean v0, v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$PendingRoadInfo;->hasTo:Z

    move/from16 v21, v0

    if-eqz v21, :cond_c

    .line 828
    const/16 v21, 0x0

    move/from16 v0, v21

    move-object/from16 v1, p3

    iput-boolean v0, v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$PendingRoadInfo;->hasTo:Z

    .line 829
    invoke-static {}, Lcom/navdy/hud/app/maps/MapSettings;->doNotShowTurnTextInTBT()Z

    move-result v21

    if-nez v21, :cond_b

    .line 830
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    sget-object v23, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->TOWARDS:Ljava/lang/String;

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 831
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    const-string v23, " "

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 833
    :cond_b
    const/16 v18, 0x1

    .line 837
    :cond_c
    invoke-static {}, Lcom/navdy/hud/app/maps/MapSettings;->isTbtOntoDisabled()Z

    move-result v21

    if-nez v21, :cond_1c

    .line 838
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    const/16 v23, 0x1

    invoke-static/range {p0 .. p0}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->isCurrentRoadHighway(Lcom/here/android/mpa/routing/Maneuver;)Z

    move-result v24

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v11, v9, v0, v1}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->concatText(Ljava/lang/String;Ljava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 845
    :cond_d
    :goto_3
    const/16 v21, 0x1

    move/from16 v0, v21

    iput-boolean v0, v7, Lcom/navdy/hud/app/maps/here/HereMapUtil$SignPostInfo;->hasNameInSignPost:Z

    .line 850
    .end local v2    # "addOnto":Z
    :cond_e
    iget-boolean v0, v7, Lcom/navdy/hud/app/maps/here/HereMapUtil$SignPostInfo;->hasNameInSignPost:Z

    move/from16 v21, v0

    if-nez v21, :cond_12

    .line 851
    const/4 v10, 0x1

    .line 852
    .local v10, "needName":Z
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->length()I

    move-result v21

    if-lez v21, :cond_f

    invoke-static/range {p0 .. p0}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->isCurrentRoadHighway(Lcom/here/android/mpa/routing/Maneuver;)Z

    move-result v21

    if-eqz v21, :cond_f

    .line 853
    const/4 v10, 0x0

    .line 855
    :cond_f
    if-eqz v10, :cond_12

    .line 856
    const/4 v4, 0x0

    .line 857
    .local v4, "bracket":Z
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    invoke-static/range {v21 .. v21}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->needSeparator(Ljava/lang/StringBuilder;)Z

    move-result v21

    if-eqz v21, :cond_10

    .line 858
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    const-string v23, " "

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 860
    :cond_10
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v21

    if-nez v21, :cond_11

    .line 861
    const/4 v4, 0x1

    .line 862
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    const-string v23, "("

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 864
    :cond_11
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    move-object/from16 v0, v21

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 866
    if-eqz v4, :cond_12

    .line 867
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    const-string v23, ")"

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 872
    .end local v4    # "bracket":Z
    .end local v10    # "needName":Z
    :cond_12
    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v21

    if-nez v21, :cond_1e

    .line 874
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 875
    .local v12, "onto":Ljava/lang/String;
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v23, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->ONTO:Ljava/lang/String;

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v23, " "

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 876
    .local v6, "index":I
    const/16 v21, -0x1

    move/from16 v0, v21

    if-eq v6, v0, :cond_13

    .line 877
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->ONTO:Ljava/lang/String;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    add-int v21, v21, v6

    const-string v23, " "

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->length()I

    move-result v23

    add-int v21, v21, v23

    move/from16 v0, v21

    invoke-virtual {v12, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v12

    .line 879
    :cond_13
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v21

    if-nez v21, :cond_14

    .line 880
    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v21

    if-eqz v21, :cond_14

    .line 882
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    const/16 v23, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 886
    :cond_14
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    invoke-static/range {v21 .. v21}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->needSeparator(Ljava/lang/StringBuilder;)Z

    move-result v21

    if-eqz v21, :cond_15

    .line 887
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    const-string v23, " "

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 889
    :cond_15
    invoke-static {}, Lcom/navdy/hud/app/maps/MapSettings;->doNotShowTurnTextInTBT()Z

    move-result v21

    if-nez v21, :cond_16

    .line 890
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    sget-object v23, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->TOWARDS:Ljava/lang/String;

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 891
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    const-string v23, " "

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 893
    :cond_16
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 894
    const/16 v18, 0x1

    .line 895
    const/16 v21, 0x0

    move/from16 v0, v21

    move-object/from16 v1, p3

    iput-boolean v0, v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$PendingRoadInfo;->hasTo:Z

    .line 911
    .end local v6    # "index":I
    .end local v12    # "onto":Ljava/lang/String;
    :cond_17
    :goto_4
    if-eqz p3, :cond_18

    .line 913
    invoke-static {}, Lcom/navdy/hud/app/maps/MapSettings;->isTbtOntoDisabled()Z

    move-result v21

    if-nez v21, :cond_20

    move-object/from16 v0, p3

    iget-boolean v0, v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$PendingRoadInfo;->enterHighway:Z

    move/from16 v21, v0

    if-eqz v21, :cond_20

    if-nez v13, :cond_20

    if-nez v18, :cond_20

    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    .line 915
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->length()I

    move-result v21

    if-lez v21, :cond_20

    .line 916
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 917
    .local v16, "str":Ljava/lang/String;
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    const/16 v23, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 918
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    sget-object v23, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->ONTO:Ljava/lang/String;

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 919
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    const-string v23, " "

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 920
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 933
    .end local v16    # "str":Ljava/lang/String;
    :cond_18
    :goto_5
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    monitor-exit v22
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v21

    .line 758
    .end local v5    # "hasSignPost":Z
    .end local v17    # "toInstruction":Ljava/lang/String;
    :cond_19
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->hasSameSignpostAndToRoad(Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;)Z

    move-result v8

    .line 759
    if-eqz v8, :cond_3

    .line 761
    const/16 v19, 0x1

    goto/16 :goto_0

    .line 810
    .restart local v2    # "addOnto":Z
    .restart local v5    # "hasSignPost":Z
    .restart local v17    # "toInstruction":Ljava/lang/String;
    :cond_1a
    :try_start_1
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    invoke-static/range {v21 .. v21}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->needSeparator(Ljava/lang/StringBuilder;)Z

    move-result v21

    if-eqz v21, :cond_9

    .line 811
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    const-string v23, " "

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 934
    .end local v2    # "addOnto":Z
    .end local v5    # "hasSignPost":Z
    .end local v17    # "toInstruction":Ljava/lang/String;
    :catchall_0
    move-exception v21

    monitor-exit v22
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v21

    .line 821
    .restart local v2    # "addOnto":Z
    .restart local v5    # "hasSignPost":Z
    .restart local v17    # "toInstruction":Ljava/lang/String;
    :cond_1b
    :try_start_2
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    invoke-static/range {v21 .. v21}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->needSeparator(Ljava/lang/StringBuilder;)Z

    move-result v21

    if-eqz v21, :cond_a

    .line 822
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    const-string v23, " "

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 840
    :cond_1c
    if-eqz p3, :cond_1d

    move-object/from16 v0, p3

    iget-boolean v0, v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$PendingRoadInfo;->hasTo:Z

    move/from16 v21, v0

    if-nez v21, :cond_d

    :cond_1d
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v21

    if-eqz v21, :cond_d

    .line 841
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    const/16 v23, 0x1

    invoke-static/range {p0 .. p0}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->isCurrentRoadHighway(Lcom/here/android/mpa/routing/Maneuver;)Z

    move-result v24

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v11, v9, v0, v1}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->concatText(Ljava/lang/String;Ljava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 897
    .end local v2    # "addOnto":Z
    :cond_1e
    if-eqz p3, :cond_17

    move-object/from16 v0, p3

    iget-boolean v0, v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$PendingRoadInfo;->hasTo:Z

    move/from16 v21, v0

    if-eqz v21, :cond_17

    .line 898
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 899
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    const/16 v23, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 900
    invoke-static {}, Lcom/navdy/hud/app/maps/MapSettings;->doNotShowTurnTextInTBT()Z

    move-result v21

    if-nez v21, :cond_1f

    .line 901
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    sget-object v23, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->TOWARDS:Ljava/lang/String;

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 902
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    const-string v23, " "

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 904
    :cond_1f
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 905
    const/16 v18, 0x1

    .line 906
    const/16 v21, 0x0

    move/from16 v0, v21

    move-object/from16 v1, p3

    iput-boolean v0, v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$PendingRoadInfo;->hasTo:Z

    goto/16 :goto_4

    .line 921
    :cond_20
    move-object/from16 v0, p3

    iget-boolean v0, v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$PendingRoadInfo;->hasTo:Z

    move/from16 v21, v0

    if-eqz v21, :cond_18

    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->length()I

    move-result v21

    if-lez v21, :cond_18

    .line 923
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 924
    .restart local v16    # "str":Ljava/lang/String;
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    const/16 v23, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 925
    invoke-static {}, Lcom/navdy/hud/app/maps/MapSettings;->doNotShowTurnTextInTBT()Z

    move-result v21

    if-nez v21, :cond_21

    .line 926
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    sget-object v23, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->TOWARDS:Ljava/lang/String;

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 927
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    const-string v23, " "

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 929
    :cond_21
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->mainSignPostBuilder:Ljava/lang/StringBuilder;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_5
.end method

.method public static getStartManeuverDisplay(Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;)Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    .locals 11
    .param p0, "current"    # Lcom/here/android/mpa/routing/Maneuver;
    .param p1, "next"    # Lcom/here/android/mpa/routing/Maneuver;

    .prologue
    const/4 v8, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 576
    new-instance v1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    invoke-direct {v1}, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;-><init>()V

    .line 577
    .local v1, "display":Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    invoke-static {p0}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->isCurrentRoadHighway(Lcom/here/android/mpa/routing/Maneuver;)Z

    move-result v2

    .line 578
    .local v2, "isCurrentHighway":Z
    invoke-static {p1}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->isCurrentRoadHighway(Lcom/here/android/mpa/routing/Maneuver;)Z

    move-result v3

    .line 580
    .local v3, "isNextHighway":Z
    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Maneuver;->getRoadNumber()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Maneuver;->getRoadName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v9, v2}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->concatText(Ljava/lang/String;Ljava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v0

    .line 581
    .local v0, "currentRoad":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/here/android/mpa/routing/Maneuver;->getRoadNumber()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lcom/here/android/mpa/routing/Maneuver;->getRoadName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v9, v3}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->concatText(Ljava/lang/String;Ljava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v4

    .line 583
    .local v4, "nextRoad":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 584
    move-object v0, v4

    .line 585
    invoke-virtual {p1}, Lcom/here/android/mpa/routing/Maneuver;->getNextRoadNumber()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lcom/here/android/mpa/routing/Maneuver;->getNextRoadName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v3, v3}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->concatText(Ljava/lang/String;Ljava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v5

    .line 586
    .local v5, "str":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 587
    move-object v4, v5

    .line 591
    .end local v5    # "str":Ljava/lang/String;
    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 592
    invoke-static {p0, v8, v8, v8}, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->getPendingRoadText(Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$PendingRoadInfo;)Ljava/lang/String;

    move-result-object v0

    .line 595
    :cond_1
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconImageMap:Ljava/util/Map;

    sget-object v7, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_START:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, v1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->turnIconId:I

    .line 596
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->START_TURN:Ljava/lang/String;

    iput-object v6, v1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingTurn:Ljava/lang/String;

    .line 597
    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Maneuver;->getRoadNumber()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Maneuver;->getRoadName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v9, v2}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->concatText(Ljava/lang/String;Ljava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->currentRoad:Ljava/lang/String;

    .line 598
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getCurrentSpeedLimit()F

    move-result v6

    iput v6, v1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->currentSpeedLimit:F

    .line 599
    invoke-static {v0, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 600
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 601
    const-string v6, ""

    iput-object v6, v1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingRoad:Ljava/lang/String;

    .line 610
    :goto_0
    const-wide/16 v6, 0x0

    invoke-static {v6, v7, v1, v10, v10}, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->setNavigationDistance(JLcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;ZZ)V

    .line 611
    iget v6, v1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->totalDistance:F

    iput v6, v1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->totalDistanceRemaining:F

    .line 612
    iget-object v6, v1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->totalDistanceUnit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    iput-object v6, v1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->totalDistanceRemainingUnit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    .line 615
    const-string v6, ""

    iput-object v6, v1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->distanceToPendingRoadText:Ljava/lang/String;

    .line 616
    sget-object v6, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_START:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    iput-object v6, v1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->navigationTurn:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 617
    if-eqz p0, :cond_4

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    :goto_1
    iput-object v6, v1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->maneuverId:Ljava/lang/String;

    .line 618
    return-object v1

    .line 603
    :cond_2
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v7, 0x7f090279

    new-array v8, v9, [Ljava/lang/Object;

    aput-object v0, v8, v10

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingRoad:Ljava/lang/String;

    goto :goto_0

    .line 606
    :cond_3
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->resources:Landroid/content/res/Resources;

    const v7, 0x7f090278

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v0, v8, v10

    aput-object v4, v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingRoad:Ljava/lang/String;

    goto :goto_0

    .line 617
    :cond_4
    const-string v6, "start"

    goto :goto_1
.end method

.method private static getTurnText(Lcom/here/android/mpa/routing/Maneuver;Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$PendingRoadInfo;Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;)Ljava/lang/String;
    .locals 8
    .param p0, "current"    # Lcom/here/android/mpa/routing/Maneuver;
    .param p1, "roadInfo"    # Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$PendingRoadInfo;
    .param p2, "maneuverState"    # Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;
    .param p3, "prevManeuver"    # Lcom/here/android/mpa/routing/Maneuver;
    .param p4, "nextManeuver"    # Lcom/here/android/mpa/routing/Maneuver;

    .prologue
    const/4 v7, 0x1

    .line 646
    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Maneuver;->getAction()Lcom/here/android/mpa/routing/Maneuver$Action;

    move-result-object v0

    .line 647
    .local v0, "action":Lcom/here/android/mpa/routing/Maneuver$Action;
    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Maneuver;->getTurn()Lcom/here/android/mpa/routing/Maneuver$Turn;

    move-result-object v3

    .line 649
    .local v3, "turn":Lcom/here/android/mpa/routing/Maneuver$Turn;
    sget-object v4, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->STAY:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    if-ne p2, v4, :cond_0

    .line 650
    sget-object v4, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->STAY_ON:Ljava/lang/String;

    .line 696
    :goto_0
    return-object v4

    .line 651
    :cond_0
    invoke-static {p0}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->isHighwayAction(Lcom/here/android/mpa/routing/Maneuver;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 652
    sget-object v4, Lcom/here/android/mpa/routing/Maneuver$Action;->ENTER_HIGHWAY_FROM_LEFT:Lcom/here/android/mpa/routing/Maneuver$Action;

    if-eq v0, v4, :cond_1

    sget-object v4, Lcom/here/android/mpa/routing/Maneuver$Action;->ENTER_HIGHWAY_FROM_RIGHT:Lcom/here/android/mpa/routing/Maneuver$Action;

    if-ne v0, v4, :cond_2

    .line 653
    :cond_1
    sget-object v4, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->ENTER_HIGHWAY:Ljava/lang/String;

    goto :goto_0

    .line 655
    :cond_2
    if-eqz p3, :cond_3

    if-eqz p4, :cond_3

    .line 657
    invoke-static {p3}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->isHighwayAction(Lcom/here/android/mpa/routing/Maneuver;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 659
    iput-boolean v7, p1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$PendingRoadInfo;->enterHighway:Z

    .line 660
    iput-boolean v7, p1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$PendingRoadInfo;->usePrevManeuverInfo:Z

    .line 661
    invoke-virtual {p3}, Lcom/here/android/mpa/routing/Maneuver;->getTurn()Lcom/here/android/mpa/routing/Maneuver$Turn;

    move-result-object v3

    .line 681
    :cond_3
    :goto_1
    sget-object v4, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$1;->$SwitchMap$com$here$android$mpa$routing$Maneuver$Turn:[I

    invoke-virtual {v3}, Lcom/here/android/mpa/routing/Maneuver$Turn;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 696
    :goto_2
    sget-object v4, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sHereTurnToNavigationTurnMap:Ljava/util/HashMap;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    goto :goto_0

    .line 662
    :cond_4
    invoke-static {p4}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->isHighwayAction(Lcom/here/android/mpa/routing/Maneuver;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 664
    iput-boolean v7, p1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$PendingRoadInfo;->enterHighway:Z

    goto :goto_1

    .line 667
    :cond_5
    sget-object v4, Lcom/here/android/mpa/routing/Maneuver$Action;->LEAVE_HIGHWAY:Lcom/here/android/mpa/routing/Maneuver$Action;

    if-ne v0, v4, :cond_7

    .line 670
    invoke-static {p0}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getRoadName(Lcom/here/android/mpa/routing/Maneuver;)Ljava/lang/String;

    move-result-object v1

    .line 671
    .local v1, "from":Ljava/lang/String;
    invoke-static {p0}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getNextRoadName(Lcom/here/android/mpa/routing/Maneuver;)Ljava/lang/String;

    move-result-object v2

    .line 672
    .local v2, "to":Ljava/lang/String;
    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 673
    sget-object v4, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ignoring action["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] using["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] from["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] to["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_1

    .line 675
    :cond_6
    sget-object v4, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->EXIT:Ljava/lang/String;

    goto/16 :goto_0

    .line 677
    .end local v1    # "from":Ljava/lang/String;
    .end local v2    # "to":Ljava/lang/String;
    :cond_7
    sget-object v4, Lcom/here/android/mpa/routing/Maneuver$Action;->UTURN:Lcom/here/android/mpa/routing/Maneuver$Action;

    if-ne v0, v4, :cond_3

    .line 678
    sget-object v4, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->UTURN:Ljava/lang/String;

    goto/16 :goto_0

    .line 686
    :pswitch_0
    iput-boolean v7, p1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$PendingRoadInfo;->dontUseSignpostText:Z

    goto :goto_2

    .line 692
    :pswitch_1
    iput-boolean v7, p1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$PendingRoadInfo;->hasTo:Z

    goto :goto_2

    .line 681
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public static getUnitDisplayStr(Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;)Ljava/lang/String;
    .locals 2
    .param p0, "unit"    # Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    .prologue
    .line 1137
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$1;->$SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit:[I

    invoke-virtual {p0}, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1148
    const-string v0, ""

    :goto_0
    return-object v0

    .line 1139
    :pswitch_0
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->UNIT_MILES:Ljava/lang/String;

    goto :goto_0

    .line 1142
    :pswitch_1
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->UNIT_KILOMETERS:Ljava/lang/String;

    goto :goto_0

    .line 1145
    :pswitch_2
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->UNIT_METERS:Ljava/lang/String;

    goto :goto_0

    .line 1137
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static setNavigationDistance(JLcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;ZZ)V
    .locals 10
    .param p0, "meters"    # J
    .param p2, "maneuverDisplay"    # Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    .param p3, "extendedUnit"    # Z
    .param p4, "calculateDistanceUnitOnly"    # Z

    .prologue
    .line 1050
    sget-object v7, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->DISTANCE:Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;

    monitor-enter v7

    .line 1051
    :try_start_0
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    invoke-virtual {v6}, Lcom/navdy/hud/app/manager/SpeedManager;->getSpeedUnit()Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    move-result-object v4

    .line 1053
    .local v4, "speedUnit":Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;
    const-wide/32 v8, 0x7fffffff

    cmp-long v6, p0, v8

    if-ltz v6, :cond_0

    .line 1056
    const-wide/16 p0, 0x0

    .line 1059
    :cond_0
    iput-wide p0, p2, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->distanceInMeters:J

    .line 1062
    long-to-float v6, p0

    sget-object v8, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->DISTANCE:Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;

    invoke-static {v4, v6, v8}, Lcom/navdy/hud/app/maps/util/DistanceConverter;->convertToDistance(Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;FLcom/navdy/hud/app/maps/util/DistanceConverter$Distance;)V

    .line 1063
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->DISTANCE:Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;

    iget v6, v6, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;->value:F

    iput v6, p2, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->distance:F

    .line 1064
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->DISTANCE:Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;

    iget-object v6, v6, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;->unit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    iput-object v6, p2, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->distanceUnit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    .line 1065
    iget v6, p2, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->distance:F

    iget-object v8, p2, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->distanceUnit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    invoke-static {v6, v8, p3}, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->getFormattedDistance(FLcom/navdy/service/library/events/navigation/DistanceUnit;Z)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p2, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->distanceToPendingRoadText:Ljava/lang/String;

    .line 1067
    if-eqz p4, :cond_1

    .line 1068
    monitor-exit v7

    .line 1100
    :goto_0
    return-void

    .line 1072
    :cond_1
    const/high16 v5, -0x40800000    # -1.0f

    .line 1073
    .local v5, "totalDistance":F
    const/high16 v2, -0x40800000    # -1.0f

    .line 1075
    .local v2, "remainingDistance":F
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v0

    .line 1076
    .local v0, "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isNavigationModeOn()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1077
    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentRoute()Lcom/here/android/mpa/routing/Route;

    move-result-object v3

    .line 1078
    .local v3, "route":Lcom/here/android/mpa/routing/Route;
    if-eqz v3, :cond_3

    .line 1079
    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getNavController()Lcom/navdy/hud/app/maps/here/HereNavController;

    move-result-object v1

    .line 1080
    .local v1, "navController":Lcom/navdy/hud/app/maps/here/HereNavController;
    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereNavController;->getDestinationDistance()J

    move-result-wide v8

    long-to-float v2, v8

    .line 1081
    const/4 v6, 0x0

    cmpg-float v6, v2, v6

    if-gez v6, :cond_2

    .line 1082
    const/4 v2, 0x0

    .line 1084
    :cond_2
    invoke-virtual {v3}, Lcom/here/android/mpa/routing/Route;->getLength()I

    move-result v6

    int-to-float v5, v6

    .line 1088
    .end local v1    # "navController":Lcom/navdy/hud/app/maps/here/HereNavController;
    .end local v3    # "route":Lcom/here/android/mpa/routing/Route;
    :cond_3
    const/high16 v6, -0x40800000    # -1.0f

    cmpl-float v6, v5, v6

    if-eqz v6, :cond_4

    .line 1090
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->DISTANCE:Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;

    invoke-static {v4, v5, v6}, Lcom/navdy/hud/app/maps/util/DistanceConverter;->convertToDistance(Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;FLcom/navdy/hud/app/maps/util/DistanceConverter$Distance;)V

    .line 1091
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->DISTANCE:Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;

    iget v6, v6, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;->value:F

    iput v6, p2, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->totalDistance:F

    .line 1092
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->DISTANCE:Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;

    iget-object v6, v6, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;->unit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    iput-object v6, p2, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->totalDistanceUnit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    .line 1095
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->DISTANCE:Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;

    invoke-static {v4, v2, v6}, Lcom/navdy/hud/app/maps/util/DistanceConverter;->convertToDistance(Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;FLcom/navdy/hud/app/maps/util/DistanceConverter$Distance;)V

    .line 1096
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->DISTANCE:Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;

    iget v6, v6, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;->value:F

    iput v6, p2, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->totalDistanceRemaining:F

    .line 1097
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->DISTANCE:Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;

    iget-object v6, v6, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;->unit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    iput-object v6, p2, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->totalDistanceRemainingUnit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    .line 1099
    :cond_4
    monitor-exit v7

    goto :goto_0

    .end local v0    # "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    .end local v2    # "remainingDistance":F
    .end local v4    # "speedUnit":Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;
    .end local v5    # "totalDistance":F
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6
.end method

.method private static setNavigationTurnIconId(Lcom/here/android/mpa/routing/Maneuver;JLcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;Lcom/here/android/mpa/routing/Maneuver;ZLcom/navdy/hud/app/maps/MapEvents$DestinationDirection;)V
    .locals 15
    .param p0, "maneuver"    # Lcom/here/android/mpa/routing/Maneuver;
    .param p1, "distance"    # J
    .param p3, "maneuverState"    # Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;
    .param p4, "display"    # Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    .param p5, "nextManeuver"    # Lcom/here/android/mpa/routing/Maneuver;
    .param p6, "isHighway"    # Z
    .param p7, "destinationDirection"    # Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;

    .prologue
    .line 953
    const/4 v7, 0x0

    .line 954
    .local v7, "resourceId":Ljava/lang/Integer;
    const/4 v9, 0x0

    .line 955
    .local v9, "resourceSoonId":Ljava/lang/Integer;
    const/4 v8, 0x0

    .line 956
    .local v8, "resourceNowId":Ljava/lang/Integer;
    sget-object v12, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$1;->$SwitchMap$com$navdy$hud$app$maps$here$HereManeuverDisplayBuilder$ManeuverState:[I

    invoke-virtual/range {p3 .. p3}, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->ordinal()I

    move-result v13

    aget v12, v12, v13

    packed-switch v12, :pswitch_data_0

    .line 1035
    :cond_0
    :goto_0
    if-nez v7, :cond_a

    .line 1036
    const/4 v12, -0x1

    move-object/from16 v0, p4

    iput v12, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->turnIconId:I

    .line 1037
    const/4 v12, 0x0

    move-object/from16 v0, p4

    iput v12, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->turnIconNowId:I

    .line 1038
    const/4 v12, 0x0

    move-object/from16 v0, p4

    iput v12, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->turnIconSoonId:I

    .line 1044
    :goto_1
    return-void

    .line 958
    :pswitch_0
    const v12, 0x7f02025e

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    .line 959
    sget-object v12, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_STRAIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    move-object/from16 v0, p4

    iput-object v12, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->navigationTurn:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    goto :goto_0

    .line 965
    :pswitch_1
    invoke-static {p0}, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->getNavigationTurn(Lcom/here/android/mpa/routing/Maneuver;)Lcom/navdy/service/library/events/navigation/NavigationTurn;

    move-result-object v2

    .line 966
    .local v2, "navigationTurn":Lcom/navdy/service/library/events/navigation/NavigationTurn;
    move-object/from16 v0, p4

    iput-object v2, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->navigationTurn:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 967
    sget-object v12, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->NOW:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    move-object/from16 v0, p3

    if-eq v0, v12, :cond_1

    sget-object v12, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->SOON:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    move-object/from16 v0, p3

    if-ne v0, v12, :cond_3

    .line 968
    :cond_1
    sget-object v12, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconImageMap:Ljava/util/Map;

    invoke-interface {v12, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "resourceId":Ljava/lang/Integer;
    check-cast v7, Ljava/lang/Integer;

    .line 972
    .restart local v7    # "resourceId":Ljava/lang/Integer;
    :goto_2
    sget-object v12, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconImageMap:Ljava/util/Map;

    invoke-interface {v12, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "resourceNowId":Ljava/lang/Integer;
    check-cast v8, Ljava/lang/Integer;

    .line 973
    .restart local v8    # "resourceNowId":Ljava/lang/Integer;
    sget-object v12, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMapGrey:Ljava/util/Map;

    invoke-interface {v12, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "resourceSoonId":Ljava/lang/Integer;
    check-cast v9, Ljava/lang/Integer;

    .line 974
    .restart local v9    # "resourceSoonId":Ljava/lang/Integer;
    move-object/from16 v0, p4

    iget-object v12, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->navigationTurn:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    sget-object v13, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_END:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    if-ne v12, v13, :cond_2

    if-eqz p7, :cond_2

    sget-object v12, Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;->UNKNOWN:Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;

    move-object/from16 v0, p7

    if-eq v0, v12, :cond_2

    .line 975
    sget-object v12, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->NOW:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    move-object/from16 v0, p3

    if-ne v0, v12, :cond_4

    .line 976
    sget-object v12, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$1;->$SwitchMap$com$navdy$hud$app$maps$MapEvents$DestinationDirection:[I

    invoke-virtual/range {p7 .. p7}, Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;->ordinal()I

    move-result v13

    aget v12, v12, v13

    packed-switch v12, :pswitch_data_1

    .line 997
    :cond_2
    :goto_3
    if-eqz p5, :cond_9

    .line 1000
    if-eqz p6, :cond_5

    .line 1001
    const-wide/16 v10, 0xc92

    .line 1006
    .local v10, "threshold":J
    :goto_4
    cmp-long v12, p1, v10

    if-lez v12, :cond_6

    .line 1007
    const/4 v12, -0x1

    move-object/from16 v0, p4

    iput v12, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->nextTurnIconId:I

    goto :goto_0

    .line 970
    .end local v10    # "threshold":J
    :cond_3
    sget-object v12, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMap:Ljava/util/Map;

    invoke-interface {v12, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "resourceId":Ljava/lang/Integer;
    check-cast v7, Ljava/lang/Integer;

    .restart local v7    # "resourceId":Ljava/lang/Integer;
    goto :goto_2

    .line 978
    :pswitch_2
    const v12, 0x7f02024f

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    .line 979
    goto :goto_3

    .line 982
    :pswitch_3
    const v12, 0x7f020251

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    goto :goto_3

    .line 986
    :cond_4
    sget-object v12, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$1;->$SwitchMap$com$navdy$hud$app$maps$MapEvents$DestinationDirection:[I

    invoke-virtual/range {p7 .. p7}, Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;->ordinal()I

    move-result v13

    aget v12, v12, v13

    packed-switch v12, :pswitch_data_2

    goto :goto_3

    .line 988
    :pswitch_4
    const v12, 0x7f020250

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    .line 989
    goto :goto_3

    .line 992
    :pswitch_5
    const v12, 0x7f020252

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    goto :goto_3

    .line 1003
    :cond_5
    const-wide/16 v10, 0x507

    .restart local v10    # "threshold":J
    goto :goto_4

    .line 1009
    :cond_6
    invoke-virtual/range {p5 .. p5}, Lcom/here/android/mpa/routing/Maneuver;->getDistanceFromPreviousManeuver()I

    move-result v12

    int-to-long v4, v12

    .line 1011
    .local v4, "nextDistance":J
    if-eqz p6, :cond_7

    .line 1012
    const-wide/16 v10, 0x3e8

    .line 1016
    :goto_5
    cmp-long v12, v4, v10

    if-gtz v12, :cond_0

    .line 1017
    invoke-static/range {p5 .. p5}, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->getNavigationTurn(Lcom/here/android/mpa/routing/Maneuver;)Lcom/navdy/service/library/events/navigation/NavigationTurn;

    move-result-object v6

    .line 1018
    .local v6, "nextTurn":Lcom/navdy/service/library/events/navigation/NavigationTurn;
    sget-object v12, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sTurnIconSoonImageMap:Ljava/util/Map;

    invoke-interface {v12, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 1019
    .local v3, "nextResourceId":Ljava/lang/Integer;
    if-nez v7, :cond_8

    .line 1020
    const/4 v12, -0x1

    move-object/from16 v0, p4

    iput v12, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->nextTurnIconId:I

    .line 1024
    :goto_6
    sget-object v12, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "distance is less than threshold "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1014
    .end local v3    # "nextResourceId":Ljava/lang/Integer;
    .end local v6    # "nextTurn":Lcom/navdy/service/library/events/navigation/NavigationTurn;
    :cond_7
    const-wide/16 v10, 0xfa

    goto :goto_5

    .line 1022
    .restart local v3    # "nextResourceId":Ljava/lang/Integer;
    .restart local v6    # "nextTurn":Lcom/navdy/service/library/events/navigation/NavigationTurn;
    :cond_8
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v12

    move-object/from16 v0, p4

    iput v12, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->nextTurnIconId:I

    goto :goto_6

    .line 1028
    .end local v3    # "nextResourceId":Ljava/lang/Integer;
    .end local v4    # "nextDistance":J
    .end local v6    # "nextTurn":Lcom/navdy/service/library/events/navigation/NavigationTurn;
    .end local v10    # "threshold":J
    :cond_9
    const/4 v12, -0x1

    move-object/from16 v0, p4

    iput v12, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->turnIconId:I

    .line 1029
    const/4 v12, 0x0

    move-object/from16 v0, p4

    iput v12, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->turnIconNowId:I

    .line 1030
    const/4 v12, 0x0

    move-object/from16 v0, p4

    iput v12, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->turnIconSoonId:I

    goto/16 :goto_0

    .line 1040
    .end local v2    # "navigationTurn":Lcom/navdy/service/library/events/navigation/NavigationTurn;
    :cond_a
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v12

    move-object/from16 v0, p4

    iput v12, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->turnIconId:I

    .line 1041
    if-nez v9, :cond_b

    const/4 v12, 0x0

    :goto_7
    move-object/from16 v0, p4

    iput v12, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->turnIconSoonId:I

    .line 1042
    if-nez v8, :cond_c

    const/4 v12, 0x0

    :goto_8
    move-object/from16 v0, p4

    iput v12, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->turnIconNowId:I

    goto/16 :goto_1

    .line 1041
    :cond_b
    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v12

    goto :goto_7

    .line 1042
    :cond_c
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v12

    goto :goto_8

    .line 956
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 976
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 986
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static shouldShowTurnText(Ljava/lang/String;)Z
    .locals 1
    .param p0, "turnText"    # Ljava/lang/String;

    .prologue
    .line 1247
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->allowedTurnTextMap:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
