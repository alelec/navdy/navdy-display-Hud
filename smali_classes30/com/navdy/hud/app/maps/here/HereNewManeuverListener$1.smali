.class Lcom/navdy/hud/app/maps/here/HereNewManeuverListener$1;
.super Ljava/lang/Object;
.source "HereNewManeuverListener.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->onNewInstructionEvent()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener$1;->this$0:Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 52
    :try_start_0
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener$1;->this$0:Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->navController:Lcom/navdy/hud/app/maps/here/HereNavController;
    invoke-static {v6}, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->access$000(Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;)Lcom/navdy/hud/app/maps/here/HereNavController;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/hud/app/maps/here/HereNavController;->getNextManeuver()Lcom/here/android/mpa/routing/Maneuver;

    move-result-object v3

    .line 53
    .local v3, "nextManeuver":Lcom/here/android/mpa/routing/Maneuver;
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener$1;->this$0:Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;

    # setter for: Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->navManeuver:Lcom/here/android/mpa/routing/Maneuver;
    invoke-static {v6, v3}, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->access$102(Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;Lcom/here/android/mpa/routing/Maneuver;)Lcom/here/android/mpa/routing/Maneuver;

    .line 55
    if-nez v3, :cond_0

    .line 56
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener$1;->this$0:Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v6}, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->access$300(Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;)Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener$1;->this$0:Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->tag:Ljava/lang/String;
    invoke-static {v8}, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->access$200(Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "There is no next maneuver"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 92
    .end local v3    # "nextManeuver":Lcom/here/android/mpa/routing/Maneuver;
    :goto_0
    return-void

    .line 59
    .restart local v3    # "nextManeuver":Lcom/here/android/mpa/routing/Maneuver;
    :cond_0
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener$1;->this$0:Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->navController:Lcom/navdy/hud/app/maps/here/HereNavController;
    invoke-static {v6}, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->access$000(Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;)Lcom/navdy/hud/app/maps/here/HereNavController;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/hud/app/maps/here/HereNavController;->getAfterNextManeuver()Lcom/here/android/mpa/routing/Maneuver;

    move-result-object v2

    .line 61
    .local v2, "maneuverAfterNext":Lcom/here/android/mpa/routing/Maneuver;
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener$1;->this$0:Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->verbose:Z
    invoke-static {v6}, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->access$400(Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 62
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getCurrentRoadName()Ljava/lang/String;

    move-result-object v1

    .line 63
    .local v1, "currentRoad":Ljava/lang/String;
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener$1;->this$0:Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v6}, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->access$300(Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;)Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener$1;->this$0:Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->tag:Ljava/lang/String;
    invoke-static {v8}, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->access$200(Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener$1;->this$0:Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->navController:Lcom/navdy/hud/app/maps/here/HereNavController;
    invoke-static {v8}, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->access$000(Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;)Lcom/navdy/hud/app/maps/here/HereNavController;

    move-result-object v8

    invoke-virtual {v8}, Lcom/navdy/hud/app/maps/here/HereNavController;->getState()Lcom/navdy/hud/app/maps/here/HereNavController$State;

    move-result-object v8

    invoke-virtual {v8}, Lcom/navdy/hud/app/maps/here/HereNavController$State;->name()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " current road["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] maneuver road name["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 64
    invoke-virtual {v3}, Lcom/here/android/mpa/routing/Maneuver;->getRoadName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] maneuver next roadname["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Lcom/here/android/mpa/routing/Maneuver;->getNextRoadName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] turn["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 65
    invoke-virtual {v3}, Lcom/here/android/mpa/routing/Maneuver;->getTurn()Lcom/here/android/mpa/routing/Maneuver$Turn;

    move-result-object v8

    invoke-virtual {v8}, Lcom/here/android/mpa/routing/Maneuver$Turn;->name()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] action["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Lcom/here/android/mpa/routing/Maneuver;->getAction()Lcom/here/android/mpa/routing/Maneuver$Action;

    move-result-object v8

    invoke-virtual {v8}, Lcom/here/android/mpa/routing/Maneuver$Action;->name()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] icon["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Lcom/here/android/mpa/routing/Maneuver;->getIcon()Lcom/here/android/mpa/routing/Maneuver$Icon;

    move-result-object v8

    invoke-virtual {v8}, Lcom/here/android/mpa/routing/Maneuver$Icon;->name()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 63
    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 68
    .end local v1    # "currentRoad":Ljava/lang/String;
    :cond_1
    invoke-virtual {v3}, Lcom/here/android/mpa/routing/Maneuver;->getAction()Lcom/here/android/mpa/routing/Maneuver$Action;

    move-result-object v6

    sget-object v7, Lcom/here/android/mpa/routing/Maneuver$Action;->END:Lcom/here/android/mpa/routing/Maneuver$Action;

    if-eq v6, v7, :cond_2

    invoke-virtual {v3}, Lcom/here/android/mpa/routing/Maneuver;->getIcon()Lcom/here/android/mpa/routing/Maneuver$Icon;

    move-result-object v6

    sget-object v7, Lcom/here/android/mpa/routing/Maneuver$Icon;->END:Lcom/here/android/mpa/routing/Maneuver$Icon;

    if-ne v6, v7, :cond_3

    .line 69
    :cond_2
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener$1;->this$0:Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v6}, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->access$300(Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;)Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener$1;->this$0:Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->tag:Ljava/lang/String;
    invoke-static {v8}, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->access$200(Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener$1;->this$0:Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->navController:Lcom/navdy/hud/app/maps/here/HereNavController;
    invoke-static {v8}, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->access$000(Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;)Lcom/navdy/hud/app/maps/here/HereNavController;

    move-result-object v8

    invoke-virtual {v8}, Lcom/navdy/hud/app/maps/here/HereNavController;->getState()Lcom/navdy/hud/app/maps/here/HereNavController$State;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " LAST MANEUVER RECEIVED"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 70
    const/4 v2, 0x0

    .line 73
    :cond_3
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener$1;->this$0:Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->hasNewRoute:Z
    invoke-static {v6}, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->access$500(Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;)Z

    move-result v0

    .line 74
    .local v0, "clearPreviousRoute":Z
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener$1;->this$0:Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;

    const/4 v7, 0x0

    # setter for: Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->hasNewRoute:Z
    invoke-static {v6, v7}, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->access$502(Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;Z)Z

    .line 75
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener$1;->this$0:Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v6}, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->access$600(Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v3, v2, v7, v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->updateNavigationInfo(Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;Z)V

    .line 77
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener$1;->this$0:Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v6}, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->access$600(Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isShownFirstManeuver()Z

    move-result v6

    if-nez v6, :cond_5

    .line 78
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener$1;->this$0:Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v6}, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->access$600(Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->setShownFirstManeuver(Z)V

    .line 79
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener$1;->this$0:Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v6}, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->access$600(Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getNavigationSessionPreference()Lcom/navdy/hud/app/maps/NavSessionPreferences;

    move-result-object v6

    iget-boolean v6, v6, Lcom/navdy/hud/app/maps/NavSessionPreferences;->spokenTurnByTurn:Z

    if-eqz v6, :cond_4

    .line 80
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->buildStartRouteTTS()Ljava/lang/String;

    move-result-object v5

    .line 81
    .local v5, "tts":Ljava/lang/String;
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener$1;->this$0:Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v6}, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->access$300(Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;)Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "tts["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 82
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener$1;->this$0:Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v6}, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->access$700(Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;)Lcom/squareup/otto/Bus;

    move-result-object v6

    sget-object v7, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->CANCEL_CALC_TTS:Lcom/navdy/hud/app/event/RemoteEvent;

    invoke-virtual {v6, v7}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 83
    sget-object v6, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->SPEECH_TURN_BY_TURN:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    sget-object v7, Lcom/navdy/hud/app/maps/notification/RouteCalculationNotification;->ROUTE_CALC_TTS_ID:Ljava/lang/String;

    invoke-static {v5, v6, v7}, Lcom/navdy/hud/app/framework/voice/TTSUtils;->sendSpeechRequest(Ljava/lang/String;Lcom/navdy/service/library/events/audio/SpeechRequest$Category;Ljava/lang/String;)V

    .line 85
    .end local v5    # "tts":Ljava/lang/String;
    :cond_4
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener$1;->this$0:Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v6}, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->access$700(Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;)Lcom/squareup/otto/Bus;

    move-result-object v6

    new-instance v7, Lcom/navdy/hud/app/maps/MapEvents$ManeuverEvent;

    sget-object v8, Lcom/navdy/hud/app/maps/MapEvents$ManeuverEvent$Type;->FIRST:Lcom/navdy/hud/app/maps/MapEvents$ManeuverEvent$Type;

    invoke-direct {v7, v8, v3}, Lcom/navdy/hud/app/maps/MapEvents$ManeuverEvent;-><init>(Lcom/navdy/hud/app/maps/MapEvents$ManeuverEvent$Type;Lcom/here/android/mpa/routing/Maneuver;)V

    invoke-virtual {v6, v7}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 89
    .end local v0    # "clearPreviousRoute":Z
    .end local v2    # "maneuverAfterNext":Lcom/here/android/mpa/routing/Maneuver;
    .end local v3    # "nextManeuver":Lcom/here/android/mpa/routing/Maneuver;
    :catch_0
    move-exception v4

    .line 90
    .local v4, "t":Ljava/lang/Throwable;
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener$1;->this$0:Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v6}, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->access$300(Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;)Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    invoke-virtual {v6, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 87
    .end local v4    # "t":Ljava/lang/Throwable;
    .restart local v0    # "clearPreviousRoute":Z
    .restart local v2    # "maneuverAfterNext":Lcom/here/android/mpa/routing/Maneuver;
    .restart local v3    # "nextManeuver":Lcom/here/android/mpa/routing/Maneuver;
    :cond_5
    :try_start_1
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener$1;->this$0:Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v6}, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->access$700(Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;)Lcom/squareup/otto/Bus;

    move-result-object v6

    new-instance v7, Lcom/navdy/hud/app/maps/MapEvents$ManeuverEvent;

    sget-object v8, Lcom/navdy/hud/app/maps/MapEvents$ManeuverEvent$Type;->INTERMEDIATE:Lcom/navdy/hud/app/maps/MapEvents$ManeuverEvent$Type;

    invoke-direct {v7, v8, v3}, Lcom/navdy/hud/app/maps/MapEvents$ManeuverEvent;-><init>(Lcom/navdy/hud/app/maps/MapEvents$ManeuverEvent$Type;Lcom/here/android/mpa/routing/Maneuver;)V

    invoke-virtual {v6, v7}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
