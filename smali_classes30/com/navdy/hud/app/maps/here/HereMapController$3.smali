.class Lcom/navdy/hud/app/maps/here/HereMapController$3;
.super Ljava/lang/Object;
.source "HereMapController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereMapController;->setCenter(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;DFF)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereMapController;

.field final synthetic val$animation:Lcom/here/android/mpa/mapping/Map$Animation;

.field final synthetic val$center:Lcom/here/android/mpa/common/GeoCoordinate;

.field final synthetic val$orientation:F

.field final synthetic val$tilt:F

.field final synthetic val$zoom:D


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereMapController;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;DFF)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereMapController;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapController$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapController;

    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereMapController$3;->val$center:Lcom/here/android/mpa/common/GeoCoordinate;

    iput-object p3, p0, Lcom/navdy/hud/app/maps/here/HereMapController$3;->val$animation:Lcom/here/android/mpa/mapping/Map$Animation;

    iput-wide p4, p0, Lcom/navdy/hud/app/maps/here/HereMapController$3;->val$zoom:D

    iput p6, p0, Lcom/navdy/hud/app/maps/here/HereMapController$3;->val$orientation:F

    iput p7, p0, Lcom/navdy/hud/app/maps/here/HereMapController$3;->val$tilt:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 151
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapController$3;->this$0:Lcom/navdy/hud/app/maps/here/HereMapController;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapController;->map:Lcom/here/android/mpa/mapping/Map;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereMapController;->access$000(Lcom/navdy/hud/app/maps/here/HereMapController;)Lcom/here/android/mpa/mapping/Map;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapController$3;->val$center:Lcom/here/android/mpa/common/GeoCoordinate;

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereMapController$3;->val$animation:Lcom/here/android/mpa/mapping/Map$Animation;

    iget-wide v4, p0, Lcom/navdy/hud/app/maps/here/HereMapController$3;->val$zoom:D

    iget v6, p0, Lcom/navdy/hud/app/maps/here/HereMapController$3;->val$orientation:F

    iget v7, p0, Lcom/navdy/hud/app/maps/here/HereMapController$3;->val$tilt:F

    invoke-virtual/range {v1 .. v7}, Lcom/here/android/mpa/mapping/Map;->setCenter(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;DFF)V

    .line 152
    return-void
.end method
