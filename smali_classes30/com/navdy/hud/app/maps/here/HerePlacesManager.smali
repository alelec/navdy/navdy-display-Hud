.class public Lcom/navdy/hud/app/maps/here/HerePlacesManager;
.super Ljava/lang/Object;
.source "HerePlacesManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;,
        Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;,
        Lcom/navdy/hud/app/maps/here/HerePlacesManager$AutoCompleteCallback;,
        Lcom/navdy/hud/app/maps/here/HerePlacesManager$PlacesSearchCallback;,
        Lcom/navdy/hud/app/maps/here/HerePlacesManager$GeoCodeCallback;
    }
.end annotation


# static fields
.field private static final EMPTY_AUTO_COMPLETE_RESULT:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final EMPTY_RESULT:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/here/android/mpa/search/PlaceLink;",
            ">;"
        }
    .end annotation
.end field

.field public static final HERE_PLACE_TYPE_ATM:Ljava/lang/String; = "atm-bank-exchange"

.field public static final HERE_PLACE_TYPE_COFFEE:Ljava/lang/String; = "coffee-tea"

.field public static final HERE_PLACE_TYPE_GAS:Ljava/lang/String; = "petrol-station"

.field public static final HERE_PLACE_TYPE_HOSPITAL:Ljava/lang/String; = "hospital-health-care-facility"

.field public static final HERE_PLACE_TYPE_PARKING:Ljava/lang/String; = "parking-facility"

.field public static final HERE_PLACE_TYPE_RESTAURANT:[Ljava/lang/String;

.field public static final HERE_PLACE_TYPE_STORE:Ljava/lang/String; = "shopping"

.field private static final MAX_RESULTS_AUTOCOMPLETE:I = 0xa

.field private static final MAX_RESULTS_SEARCH:I = 0xf

.field private static final context:Landroid/content/Context;

.field private static final hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static volatile willRestablishOnline:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 49
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/maps/here/HerePlacesManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->EMPTY_RESULT:Ljava/util/ArrayList;

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->EMPTY_AUTO_COMPLETE_RESULT:Ljava/util/ArrayList;

    .line 53
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .line 54
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->context:Landroid/content/Context;

    .line 62
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "restaurant"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string v2, "snacks-fast-food"

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->HERE_PLACE_TYPE_RESTAURANT:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100()Lcom/navdy/hud/app/maps/here/HereMapsManager;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    return-object v0
.end method

.method static synthetic access$1000()V
    .locals 0

    .prologue
    .line 48
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->tryRestablishOnline()V

    return-void
.end method

.method static synthetic access$1100(Ljava/util/List;Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;)V
    .locals 0
    .param p0, "x0"    # Ljava/util/List;
    .param p1, "x1"    # Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;

    .prologue
    .line 48
    invoke-static {p0, p1}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->handlePlaceLinksRequest(Ljava/util/List;Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;)V

    return-void
.end method

.method static synthetic access$1200(Ljava/util/List;Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;)V
    .locals 0
    .param p0, "x0"    # Ljava/util/List;
    .param p1, "x1"    # Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;

    .prologue
    .line 48
    invoke-static {p0, p1}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->invokeCategorySearchListener(Ljava/util/List;Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;)V

    return-void
.end method

.method static synthetic access$200()Landroid/content/Context;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Lcom/navdy/service/library/events/RequestStatus;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 48
    invoke-static {p0, p1, p2}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->returnSearchErrorResponse(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->EMPTY_RESULT:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$500(Lcom/here/android/mpa/common/GeoCoordinate;Ljava/lang/String;Ljava/util/List;II)V
    .locals 0
    .param p0, "x0"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/util/List;
    .param p3, "x3"    # I
    .param p4, "x4"    # I

    .prologue
    .line 48
    invoke-static {p0, p1, p2, p3, p4}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->returnSearchResults(Lcom/here/android/mpa/common/GeoCoordinate;Ljava/lang/String;Ljava/util/List;II)V

    return-void
.end method

.method static synthetic access$600(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Lcom/navdy/service/library/events/RequestStatus;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 48
    invoke-static {p0, p1, p2}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->returnAutoCompleteErrorResponse(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$700()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->EMPTY_AUTO_COMPLETE_RESULT:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$800(Ljava/lang/String;ILjava/util/List;)V
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/util/List;

    .prologue
    .line 48
    invoke-static {p0, p1, p2}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->returnAutoCompleteResults(Ljava/lang/String;ILjava/util/List;)V

    return-void
.end method

.method static synthetic access$900()V
    .locals 0

    .prologue
    .line 48
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->establishOffline()V

    return-void
.end method

.method public static autoComplete(Lcom/here/android/mpa/common/GeoCoordinate;Ljava/lang/String;IILcom/navdy/hud/app/maps/here/HerePlacesManager$AutoCompleteCallback;)V
    .locals 4
    .param p0, "currentPosition"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p1, "queryText"    # Ljava/lang/String;
    .param p2, "searchArea"    # I
    .param p3, "maxResults"    # I
    .param p4, "callBack"    # Lcom/navdy/hud/app/maps/here/HerePlacesManager$AutoCompleteCallback;

    .prologue
    .line 158
    new-instance v1, Lcom/here/android/mpa/search/TextSuggestionRequest;

    invoke-direct {v1, p1}, Lcom/here/android/mpa/search/TextSuggestionRequest;-><init>(Ljava/lang/String;)V

    .line 159
    .local v1, "request":Lcom/here/android/mpa/search/TextSuggestionRequest;
    invoke-virtual {v1, p3}, Lcom/here/android/mpa/search/TextSuggestionRequest;->setCollectionSize(I)Lcom/here/android/mpa/search/TextSuggestionRequest;

    .line 160
    invoke-virtual {v1, p0}, Lcom/here/android/mpa/search/TextSuggestionRequest;->setSearchCenter(Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/here/android/mpa/search/TextSuggestionRequest;

    .line 161
    new-instance v3, Lcom/navdy/hud/app/maps/here/HerePlacesManager$3;

    invoke-direct {v3, p4}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$3;-><init>(Lcom/navdy/hud/app/maps/here/HerePlacesManager$AutoCompleteCallback;)V

    invoke-virtual {v1, v3}, Lcom/here/android/mpa/search/TextSuggestionRequest;->execute(Lcom/here/android/mpa/search/ResultListener;)Lcom/here/android/mpa/search/ErrorCode;

    move-result-object v0

    .line 171
    .local v0, "errorCode":Lcom/here/android/mpa/search/ErrorCode;
    sget-object v3, Lcom/here/android/mpa/search/ErrorCode;->NONE:Lcom/here/android/mpa/search/ErrorCode;

    if-eq v0, v3, :cond_0

    .line 173
    const/4 v3, 0x0

    :try_start_0
    invoke-interface {p4, v0, v3}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$AutoCompleteCallback;->result(Lcom/here/android/mpa/search/ErrorCode;Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 178
    :cond_0
    :goto_0
    return-void

    .line 174
    :catch_0
    move-exception v2

    .line 175
    .local v2, "t":Ljava/lang/Throwable;
    sget-object v3, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static establishOffline()V
    .locals 2

    .prologue
    .line 582
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isEngineOnline()Z

    move-result v0

    sput-boolean v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->willRestablishOnline:Z

    .line 584
    sget-boolean v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->willRestablishOnline:Z

    if-eqz v0, :cond_0

    .line 585
    sget-object v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "turn engine offline"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 586
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->turnOffline()V

    .line 588
    :cond_0
    return-void
.end method

.method public static geoCodeStreetAddress(Lcom/here/android/mpa/common/GeoCoordinate;Ljava/lang/String;ILcom/navdy/hud/app/maps/here/HerePlacesManager$GeoCodeCallback;)Lcom/here/android/mpa/search/GeocodeRequest;
    .locals 5
    .param p0, "currentPosition"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p1, "queryText"    # Ljava/lang/String;
    .param p2, "searchArea"    # I
    .param p3, "callBack"    # Lcom/navdy/hud/app/maps/here/HerePlacesManager$GeoCodeCallback;

    .prologue
    const/4 v3, 0x0

    .line 131
    new-instance v1, Lcom/here/android/mpa/search/GeocodeRequest;

    invoke-direct {v1, p1}, Lcom/here/android/mpa/search/GeocodeRequest;-><init>(Ljava/lang/String;)V

    .line 132
    .local v1, "geocodeRequest":Lcom/here/android/mpa/search/GeocodeRequest;
    if-eqz p0, :cond_0

    .line 133
    invoke-virtual {v1, p0, p2}, Lcom/here/android/mpa/search/GeocodeRequest;->setSearchArea(Lcom/here/android/mpa/common/GeoCoordinate;I)Lcom/here/android/mpa/search/GeocodeRequest;

    .line 135
    :cond_0
    new-instance v4, Lcom/navdy/hud/app/maps/here/HerePlacesManager$2;

    invoke-direct {v4, p3}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$2;-><init>(Lcom/navdy/hud/app/maps/here/HerePlacesManager$GeoCodeCallback;)V

    invoke-virtual {v1, v4}, Lcom/here/android/mpa/search/GeocodeRequest;->execute(Lcom/here/android/mpa/search/ResultListener;)Lcom/here/android/mpa/search/ErrorCode;

    move-result-object v0

    .line 145
    .local v0, "errorCode":Lcom/here/android/mpa/search/ErrorCode;
    sget-object v4, Lcom/here/android/mpa/search/ErrorCode;->NONE:Lcom/here/android/mpa/search/ErrorCode;

    if-eq v0, v4, :cond_1

    .line 147
    const/4 v4, 0x0

    :try_start_0
    invoke-interface {p3, v0, v4}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$GeoCodeCallback;->result(Lcom/here/android/mpa/search/ErrorCode;Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    move-object v1, v3

    .line 153
    .end local v1    # "geocodeRequest":Lcom/here/android/mpa/search/GeocodeRequest;
    :cond_1
    return-object v1

    .line 148
    .restart local v1    # "geocodeRequest":Lcom/here/android/mpa/search/GeocodeRequest;
    :catch_0
    move-exception v2

    .line 149
    .local v2, "t":Ljava/lang/Throwable;
    sget-object v4, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v4, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static getPlaceEntry(Lcom/here/android/mpa/search/Place;)Lcom/here/android/mpa/common/GeoCoordinate;
    .locals 3
    .param p0, "place"    # Lcom/here/android/mpa/search/Place;

    .prologue
    .line 740
    invoke-virtual {p0}, Lcom/here/android/mpa/search/Place;->getLocation()Lcom/here/android/mpa/search/Location;

    move-result-object v1

    invoke-virtual {v1}, Lcom/here/android/mpa/search/Location;->getAccessPoints()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 741
    invoke-virtual {p0}, Lcom/here/android/mpa/search/Place;->getLocation()Lcom/here/android/mpa/search/Location;

    move-result-object v1

    invoke-virtual {v1}, Lcom/here/android/mpa/search/Location;->getAccessPoints()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/here/android/mpa/search/NavigationPosition;

    invoke-virtual {v1}, Lcom/here/android/mpa/search/NavigationPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v0

    .line 746
    .local v0, "entry":Lcom/here/android/mpa/common/GeoCoordinate;
    :goto_0
    return-object v0

    .line 743
    .end local v0    # "entry":Lcom/here/android/mpa/common/GeoCoordinate;
    :cond_0
    invoke-virtual {p0}, Lcom/here/android/mpa/search/Place;->getLocation()Lcom/here/android/mpa/search/Location;

    move-result-object v1

    invoke-virtual {v1}, Lcom/here/android/mpa/search/Location;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v0

    .restart local v0    # "entry":Lcom/here/android/mpa/common/GeoCoordinate;
    goto :goto_0
.end method

.method public static handleAutoCompleteRequest(Lcom/navdy/service/library/events/places/AutoCompleteRequest;)V
    .locals 3
    .param p0, "request"    # Lcom/navdy/service/library/events/places/AutoCompleteRequest;

    .prologue
    .line 277
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5;-><init>(Lcom/navdy/service/library/events/places/AutoCompleteRequest;)V

    const/16 v2, 0x13

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 363
    return-void
.end method

.method public static handleCategoriesRequest(Lcom/here/android/mpa/search/CategoryFilter;ILcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;)V
    .locals 2
    .param p0, "filter"    # Lcom/here/android/mpa/search/CategoryFilter;
    .param p1, "nResults"    # I
    .param p2, "listener"    # Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;

    .prologue
    .line 366
    new-instance v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$6;

    invoke-direct {v0, p2, p0, p1}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$6;-><init>(Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;Lcom/here/android/mpa/search/CategoryFilter;I)V

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->handleCategoriesRequest(Lcom/here/android/mpa/search/CategoryFilter;ILcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;Z)V

    .line 387
    return-void
.end method

.method public static handleCategoriesRequest(Lcom/here/android/mpa/search/CategoryFilter;ILcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;Z)V
    .locals 3
    .param p0, "filter"    # Lcom/here/android/mpa/search/CategoryFilter;
    .param p1, "nResults"    # I
    .param p2, "listener"    # Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;
    .param p3, "offline"    # Z

    .prologue
    .line 393
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7;-><init>(Lcom/here/android/mpa/search/CategoryFilter;ILcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;Z)V

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 508
    return-void
.end method

.method private static handlePlaceLinksRequest(Ljava/util/List;Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;)V
    .locals 9
    .param p1, "listener"    # Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/search/PlaceLink;",
            ">;",
            "Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 511
    .local p0, "placeLinks":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/search/PlaceLink;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 512
    .local v4, "places":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/search/Place;>;"
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v5

    invoke-direct {v0, v5}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 516
    .local v0, "counter":Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/here/android/mpa/search/PlaceLink;

    .line 517
    .local v3, "placeLink":Lcom/here/android/mpa/search/PlaceLink;
    invoke-virtual {v3}, Lcom/here/android/mpa/search/PlaceLink;->getDetailsRequest()Lcom/here/android/mpa/search/PlaceRequest;

    move-result-object v6

    new-instance v7, Lcom/navdy/hud/app/maps/here/HerePlacesManager$8;

    invoke-direct {v7, v0, v4, p1}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$8;-><init>(Ljava/util/concurrent/atomic/AtomicInteger;Ljava/util/List;Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;)V

    invoke-virtual {v6, v7}, Lcom/here/android/mpa/search/PlaceRequest;->execute(Lcom/here/android/mpa/search/ResultListener;)Lcom/here/android/mpa/search/ErrorCode;

    move-result-object v1

    .line 554
    .local v1, "error":Lcom/here/android/mpa/search/ErrorCode;
    sget-object v6, Lcom/here/android/mpa/search/ErrorCode;->NONE:Lcom/here/android/mpa/search/ErrorCode;

    if-eq v1, v6, :cond_0

    .line 555
    sget-object v6, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Error in place detail request: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Lcom/here/android/mpa/search/ErrorCode;->name()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 556
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v2

    .line 557
    .local v2, "left":I
    if-nez v2, :cond_0

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    if-nez v6, :cond_0

    .line 559
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->tryRestablishOnline()V

    .line 560
    sget-object v6, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;->BAD_REQUEST:Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

    invoke-interface {p1, v6}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;->onError(Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;)V

    goto :goto_0

    .line 564
    .end local v1    # "error":Lcom/here/android/mpa/search/ErrorCode;
    .end local v2    # "left":I
    .end local v3    # "placeLink":Lcom/here/android/mpa/search/PlaceLink;
    :cond_1
    return-void
.end method

.method public static handlePlacesSearchRequest(Lcom/navdy/service/library/events/places/PlacesSearchRequest;)V
    .locals 3
    .param p0, "request"    # Lcom/navdy/service/library/events/places/PlacesSearchRequest;

    .prologue
    .line 182
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4;-><init>(Lcom/navdy/service/library/events/places/PlacesSearchRequest;)V

    const/16 v2, 0x13

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 274
    return-void
.end method

.method private static invokeCategorySearchListener(Ljava/util/List;Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/search/Place;",
            ">;",
            "Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 567
    .local p0, "places":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/search/Place;>;"
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    .line 568
    .local v0, "size":I
    if-nez v0, :cond_0

    .line 570
    sget-object v1, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "places query complete, no result"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 571
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->tryRestablishOnline()V

    .line 572
    sget-object v1, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;->RESPONSE_ERROR:Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

    invoke-interface {p1, v1}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;->onError(Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;)V

    .line 579
    :goto_0
    return-void

    .line 575
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "places query complete:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 576
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->tryRestablishOnline()V

    .line 577
    invoke-interface {p1, p0}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;->onCompleted(Ljava/util/List;)V

    goto :goto_0
.end method

.method public static printAutoCompleteRequest(Lcom/navdy/service/library/events/places/AutoCompleteRequest;)V
    .locals 4
    .param p0, "request"    # Lcom/navdy/service/library/events/places/AutoCompleteRequest;

    .prologue
    .line 730
    :try_start_0
    sget-object v1, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AutoCompleteRequest query["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/service/library/events/places/AutoCompleteRequest;->partialSearch:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] maxResults["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/service/library/events/places/AutoCompleteRequest;->maxResults:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 735
    :goto_0
    return-void

    .line 732
    :catch_0
    move-exception v0

    .line 733
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static printSearchRequest(Lcom/navdy/service/library/events/places/PlacesSearchRequest;)V
    .locals 4
    .param p0, "request"    # Lcom/navdy/service/library/events/places/PlacesSearchRequest;

    .prologue
    .line 720
    :try_start_0
    sget-object v1, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "PlacesSearchRequest query["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->searchQuery:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] area["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->searchArea:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] maxResults["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->maxResults:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 726
    :goto_0
    return-void

    .line 723
    :catch_0
    move-exception v0

    .line 724
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static returnAutoCompleteErrorResponse(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V
    .locals 4
    .param p0, "partialSearch"    # Ljava/lang/String;
    .param p1, "status"    # Lcom/navdy/service/library/events/RequestStatus;
    .param p2, "errorText"    # Ljava/lang/String;

    .prologue
    .line 686
    :try_start_0
    sget-object v1, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[autocomplete]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 687
    invoke-static {}, Lcom/navdy/hud/app/maps/MapsEventHandler;->getInstance()Lcom/navdy/hud/app/maps/MapsEventHandler;

    move-result-object v1

    new-instance v2, Lcom/navdy/service/library/events/places/AutoCompleteResponse;

    const/4 v3, 0x0

    invoke-direct {v2, p0, p1, p2, v3}, Lcom/navdy/service/library/events/places/AutoCompleteResponse;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/maps/MapsEventHandler;->sendEventToClient(Lcom/squareup/wire/Message;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 691
    :goto_0
    return-void

    .line 688
    :catch_0
    move-exception v0

    .line 689
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static returnAutoCompleteResults(Ljava/lang/String;ILjava/util/List;)V
    .locals 3
    .param p0, "autoCompleteSearch"    # Ljava/lang/String;
    .param p1, "maxResults"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 697
    .local p2, "results":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HerePlacesManager$10;

    invoke-direct {v1, p2, p1, p0}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$10;-><init>(Ljava/util/List;ILjava/lang/String;)V

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 716
    return-void
.end method

.method private static returnSearchErrorResponse(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V
    .locals 5
    .param p0, "searchQuery"    # Ljava/lang/String;
    .param p1, "status"    # Lcom/navdy/service/library/events/RequestStatus;
    .param p2, "errorText"    # Ljava/lang/String;

    .prologue
    .line 602
    :try_start_0
    sget-object v2, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[search]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 603
    new-instance v2, Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;-><init>()V

    .line 604
    invoke-virtual {v2, p0}, Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;->searchQuery(Ljava/lang/String;)Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;

    move-result-object v2

    .line 605
    invoke-virtual {v2, p1}, Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;->status(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;

    move-result-object v2

    .line 606
    invoke-virtual {v2, p2}, Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;->statusDetail(Ljava/lang/String;)Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;

    move-result-object v2

    const/4 v3, 0x0

    .line 607
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;->results(Ljava/util/List;)Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;

    move-result-object v2

    .line 608
    invoke-virtual {v2}, Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;->build()Lcom/navdy/service/library/events/places/PlacesSearchResponse;

    move-result-object v0

    .line 609
    .local v0, "response":Lcom/navdy/service/library/events/places/PlacesSearchResponse;
    invoke-static {}, Lcom/navdy/hud/app/maps/MapsEventHandler;->getInstance()Lcom/navdy/hud/app/maps/MapsEventHandler;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/navdy/hud/app/maps/MapsEventHandler;->sendEventToClient(Lcom/squareup/wire/Message;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 613
    .end local v0    # "response":Lcom/navdy/service/library/events/places/PlacesSearchResponse;
    :goto_0
    return-void

    .line 610
    :catch_0
    move-exception v1

    .line 611
    .local v1, "t":Ljava/lang/Throwable;
    sget-object v2, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static returnSearchResults(Lcom/here/android/mpa/common/GeoCoordinate;Ljava/lang/String;Ljava/util/List;II)V
    .locals 7
    .param p0, "currentLocation"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p1, "queryText"    # Ljava/lang/String;
    .param p3, "maxResults"    # I
    .param p4, "searchArea"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/here/android/mpa/common/GeoCoordinate;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/search/PlaceLink;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 620
    .local p2, "destinationLinks":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/search/PlaceLink;>;"
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v6

    new-instance v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$9;

    move v1, p3

    move-object v2, p2

    move-object v3, p0

    move v4, p4

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$9;-><init>(ILjava/util/List;Lcom/here/android/mpa/common/GeoCoordinate;ILjava/lang/String;)V

    const/4 v1, 0x2

    invoke-virtual {v6, v0, v1}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 681
    return-void
.end method

.method public static searchPlaces(Lcom/here/android/mpa/common/GeoCoordinate;Ljava/lang/String;IILcom/navdy/hud/app/maps/here/HerePlacesManager$PlacesSearchCallback;)V
    .locals 4
    .param p0, "currentPosition"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p1, "queryText"    # Ljava/lang/String;
    .param p2, "searchArea"    # I
    .param p3, "maxResults"    # I
    .param p4, "callBack"    # Lcom/navdy/hud/app/maps/here/HerePlacesManager$PlacesSearchCallback;

    .prologue
    .line 108
    new-instance v1, Lcom/here/android/mpa/search/SearchRequest;

    invoke-direct {v1, p1}, Lcom/here/android/mpa/search/SearchRequest;-><init>(Ljava/lang/String;)V

    .line 109
    .local v1, "request":Lcom/here/android/mpa/search/SearchRequest;
    invoke-virtual {v1, p0}, Lcom/here/android/mpa/search/SearchRequest;->setSearchCenter(Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/here/android/mpa/search/SearchRequest;

    .line 110
    invoke-virtual {v1, p3}, Lcom/here/android/mpa/search/SearchRequest;->setCollectionSize(I)Lcom/here/android/mpa/search/DiscoveryRequest;

    .line 111
    new-instance v3, Lcom/navdy/hud/app/maps/here/HerePlacesManager$1;

    invoke-direct {v3, p4}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$1;-><init>(Lcom/navdy/hud/app/maps/here/HerePlacesManager$PlacesSearchCallback;)V

    invoke-virtual {v1, v3}, Lcom/here/android/mpa/search/SearchRequest;->execute(Lcom/here/android/mpa/search/ResultListener;)Lcom/here/android/mpa/search/ErrorCode;

    move-result-object v0

    .line 121
    .local v0, "errorCode":Lcom/here/android/mpa/search/ErrorCode;
    sget-object v3, Lcom/here/android/mpa/search/ErrorCode;->NONE:Lcom/here/android/mpa/search/ErrorCode;

    if-eq v0, v3, :cond_0

    .line 123
    const/4 v3, 0x0

    :try_start_0
    invoke-interface {p4, v0, v3}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$PlacesSearchCallback;->result(Lcom/here/android/mpa/search/ErrorCode;Lcom/here/android/mpa/search/DiscoveryResultPage;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 128
    :cond_0
    :goto_0
    return-void

    .line 124
    :catch_0
    move-exception v2

    .line 125
    .local v2, "t":Ljava/lang/Throwable;
    sget-object v3, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static tryRestablishOnline()V
    .locals 2

    .prologue
    .line 591
    sget-boolean v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->willRestablishOnline:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isEngineOnline()Z

    move-result v0

    if-nez v0, :cond_0

    .line 592
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->isRemoteDeviceConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 593
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 594
    sget-object v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "turn engine online"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 595
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->turnOnline()V

    .line 597
    :cond_0
    const/4 v0, 0x0

    sput-boolean v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->willRestablishOnline:Z

    .line 598
    return-void
.end method
