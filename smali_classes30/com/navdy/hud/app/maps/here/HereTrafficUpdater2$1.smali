.class Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;
.super Ljava/lang/Object;
.source "HereTrafficUpdater2.java"

# interfaces
.implements Lcom/here/android/mpa/guidance/TrafficUpdater$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onStatusChanged(Lcom/here/android/mpa/guidance/TrafficUpdater$RequestState;)V
    .locals 10
    .param p1, "requestState"    # Lcom/here/android/mpa/guidance/TrafficUpdater$RequestState;

    .prologue
    const-wide/16 v8, 0x0

    const/4 v6, 0x0

    .line 88
    const/4 v0, 0x0

    .line 89
    .local v0, "reset":Z
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->getRefereshInterval()I

    move-result v1

    .line 91
    .local v1, "resetTime":I
    :try_start_0
    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onRequestListener::"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 93
    sget-object v3, Lcom/here/android/mpa/guidance/TrafficUpdater$RequestState;->DONE:Lcom/here/android/mpa/guidance/TrafficUpdater$RequestState;

    if-ne p1, v3, :cond_6

    .line 94
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->route:Lcom/here/android/mpa/routing/Route;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$100(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;)Lcom/here/android/mpa/routing/Route;

    move-result-object v3

    if-nez v3, :cond_2

    .line 95
    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "onRequestListener:traffic downloaded for open map"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 96
    const/4 v0, 0x1

    .line 154
    invoke-static {}, Lcom/navdy/hud/app/maps/MapSettings;->isTrafficDashWidgetsEnabled()Z

    move-result v3

    if-nez v3, :cond_0

    .line 155
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    # setter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->currentRequestInfo:Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;
    invoke-static {v3, v6}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$202(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;)Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;

    .line 156
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    # setter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->lastRequestTime:J
    invoke-static {v3, v8, v9}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$302(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;J)J

    .line 158
    :cond_0
    if-eqz v0, :cond_1

    .line 159
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    # invokes: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->reset(I)V
    invoke-static {v3, v1}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$400(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;I)V

    .line 162
    :cond_1
    :goto_0
    return-void

    .line 99
    :cond_2
    :try_start_1
    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "onRequestListener:traffic downloaded for route"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 102
    invoke-static {}, Lcom/navdy/hud/app/maps/MapSettings;->isTrafficDashWidgetsEnabled()Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    if-nez v3, :cond_4

    .line 103
    const/4 v0, 0x1

    .line 154
    invoke-static {}, Lcom/navdy/hud/app/maps/MapSettings;->isTrafficDashWidgetsEnabled()Z

    move-result v3

    if-nez v3, :cond_3

    .line 155
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    # setter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->currentRequestInfo:Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;
    invoke-static {v3, v6}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$202(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;)Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;

    .line 156
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    # setter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->lastRequestTime:J
    invoke-static {v3, v8, v9}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$302(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;J)J

    .line 158
    :cond_3
    if-eqz v0, :cond_1

    .line 159
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    # invokes: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->reset(I)V
    invoke-static {v3, v1}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$400(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;I)V

    goto :goto_0

    .line 106
    :cond_4
    :try_start_2
    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "onRequestListener:call getEvents"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 108
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v3

    new-instance v4, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1$1;

    invoke-direct {v4, p0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1$1;-><init>(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;)V

    const/4 v5, 0x2

    invoke-virtual {v3, v4, v5}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 154
    :goto_1
    invoke-static {}, Lcom/navdy/hud/app/maps/MapSettings;->isTrafficDashWidgetsEnabled()Z

    move-result v3

    if-nez v3, :cond_5

    .line 155
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    # setter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->currentRequestInfo:Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;
    invoke-static {v3, v6}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$202(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;)Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;

    .line 156
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    # setter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->lastRequestTime:J
    invoke-static {v3, v8, v9}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$302(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;J)J

    .line 158
    :cond_5
    if-eqz v0, :cond_1

    .line 159
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    # invokes: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->reset(I)V
    invoke-static {v3, v1}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$400(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;I)V

    goto :goto_0

    .line 144
    :cond_6
    :try_start_3
    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "onRequestListener:: traffic data download failed"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 145
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->getRefereshInterval()I

    move-result v3

    div-int/lit8 v1, v3, 0x2

    .line 146
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$800(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;)Lcom/squareup/otto/Bus;

    move-result-object v3

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->FAILED_EVENT:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$700()Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 147
    const/4 v0, 0x1

    goto :goto_1

    .line 149
    :catch_0
    move-exception v2

    .line 150
    .local v2, "t":Ljava/lang/Throwable;
    :try_start_4
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$800(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;)Lcom/squareup/otto/Bus;

    move-result-object v3

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->FAILED_EVENT:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$700()Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 151
    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "onRequestListener"

    invoke-virtual {v3, v4, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 152
    const/4 v0, 0x1

    .line 154
    invoke-static {}, Lcom/navdy/hud/app/maps/MapSettings;->isTrafficDashWidgetsEnabled()Z

    move-result v3

    if-nez v3, :cond_7

    .line 155
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    # setter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->currentRequestInfo:Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;
    invoke-static {v3, v6}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$202(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;)Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;

    .line 156
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    # setter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->lastRequestTime:J
    invoke-static {v3, v8, v9}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$302(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;J)J

    .line 158
    :cond_7
    if-eqz v0, :cond_1

    .line 159
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    # invokes: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->reset(I)V
    invoke-static {v3, v1}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$400(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;I)V

    goto/16 :goto_0

    .line 154
    .end local v2    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v3

    invoke-static {}, Lcom/navdy/hud/app/maps/MapSettings;->isTrafficDashWidgetsEnabled()Z

    move-result v4

    if-nez v4, :cond_8

    .line 155
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    # setter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->currentRequestInfo:Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;
    invoke-static {v4, v6}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$202(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;)Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;

    .line 156
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    # setter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->lastRequestTime:J
    invoke-static {v4, v8, v9}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$302(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;J)J

    .line 158
    :cond_8
    if-eqz v0, :cond_9

    .line 159
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    # invokes: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->reset(I)V
    invoke-static {v4, v1}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$400(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;I)V

    :cond_9
    throw v3
.end method
