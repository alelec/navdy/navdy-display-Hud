.class public Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;
.super Lcom/here/android/mpa/guidance/NavigationManager$NavigationManagerEventListener;
.source "HereTrafficETATracker.java"


# static fields
.field private static final ETA_THRESHOLD:J

.field private static final INVALID:J = -0x1L

.field private static final REFRESH_ETA_INTERVAL_MILLIS:J


# instance fields
.field private baseEta:J

.field private final bus:Lcom/squareup/otto/Bus;

.field private dismissEtaRunnable:Ljava/lang/Runnable;

.field private final handler:Landroid/os/Handler;

.field private final navController:Lcom/navdy/hud/app/maps/here/HereNavController;

.field private navigationModeChangeBkRunnable:Ljava/lang/Runnable;

.field private refreshEtaBkRunnable:Ljava/lang/Runnable;

.field private refreshEtaRunnable:Ljava/lang/Runnable;

.field private final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 25
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3c

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->REFRESH_ETA_INTERVAL_MILLIS:J

    .line 26
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->ETA_THRESHOLD:J

    return-void
.end method

.method public constructor <init>(Lcom/navdy/hud/app/maps/here/HereNavController;Lcom/squareup/otto/Bus;)V
    .locals 2
    .param p1, "navController"    # Lcom/navdy/hud/app/maps/here/HereNavController;
    .param p2, "bus"    # Lcom/squareup/otto/Bus;

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/here/android/mpa/guidance/NavigationManager$NavigationManagerEventListener;-><init>()V

    .line 23
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 35
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker$1;-><init>(Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->refreshEtaRunnable:Ljava/lang/Runnable;

    .line 42
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker$2;-><init>(Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->refreshEtaBkRunnable:Ljava/lang/Runnable;

    .line 49
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker$3;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker$3;-><init>(Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->dismissEtaRunnable:Ljava/lang/Runnable;

    .line 56
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker$4;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker$4;-><init>(Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->navigationModeChangeBkRunnable:Ljava/lang/Runnable;

    .line 89
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->navController:Lcom/navdy/hud/app/maps/here/HereNavController;

    .line 90
    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->bus:Lcom/squareup/otto/Bus;

    .line 91
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->handler:Landroid/os/Handler;

    .line 92
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "ctor"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 93
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->refreshEtaBkRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->etaUpdate()V

    return-void
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->sendTrafficDelayDismissEvent()V

    return-void
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;)Lcom/navdy/hud/app/maps/here/HereNavController;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->navController:Lcom/navdy/hud/app/maps/here/HereNavController;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;)J
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;

    .prologue
    .line 22
    iget-wide v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->baseEta:J

    return-wide v0
.end method

.method static synthetic access$402(Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;J)J
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;
    .param p1, "x1"    # J

    .prologue
    .line 22
    iput-wide p1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->baseEta:J

    return-wide p1
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->refreshEta()V

    return-void
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->refreshEtaRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->dismissEtaRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method private etaUpdate()V
    .locals 10

    .prologue
    .line 102
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "etaUpdate"

    invoke-virtual {v1, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 104
    iget-wide v6, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->baseEta:J

    const-wide/16 v8, -0x1

    cmp-long v1, v6, v8

    if-nez v1, :cond_0

    .line 105
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "base ETA is invalid"

    invoke-virtual {v1, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 128
    :goto_0
    return-void

    .line 109
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->navController:Lcom/navdy/hud/app/maps/here/HereNavController;

    const/4 v6, 0x1

    sget-object v7, Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;->OPTIMAL:Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    invoke-virtual {v1, v6, v7}, Lcom/navdy/hud/app/maps/here/HereNavController;->getEta(ZLcom/here/android/mpa/routing/Route$TrafficPenaltyMode;)Ljava/util/Date;

    move-result-object v0

    .line 111
    .local v0, "etaDate":Ljava/util/Date;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->isValidEtaDate(Ljava/util/Date;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 112
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    .line 113
    .local v4, "newEta":J
    iget-wide v6, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->baseEta:J

    sub-long v2, v4, v6

    .line 115
    .local v2, "etaDiff":J
    sget-wide v6, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->ETA_THRESHOLD:J

    cmp-long v1, v2, v6

    if-lez v1, :cond_2

    .line 116
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ETA threshold delay, triggering delay:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 117
    iput-wide v4, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->baseEta:J

    .line 118
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->bus:Lcom/squareup/otto/Bus;

    new-instance v6, Lcom/navdy/hud/app/maps/MapEvents$TrafficDelayEvent;

    const-wide/16 v8, 0x3e8

    div-long v8, v2, v8

    invoke-direct {v6, v8, v9}, Lcom/navdy/hud/app/maps/MapEvents$TrafficDelayEvent;-><init>(J)V

    invoke-virtual {v1, v6}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 120
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->handler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->dismissEtaRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 121
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->handler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->dismissEtaRunnable:Ljava/lang/Runnable;

    sget-wide v8, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->ETA_THRESHOLD:J

    invoke-virtual {v1, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 127
    .end local v2    # "etaDiff":J
    .end local v4    # "newEta":J
    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->refreshEta()V

    goto :goto_0

    .line 123
    .restart local v2    # "etaDiff":J
    .restart local v4    # "newEta":J
    :cond_2
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ETA threshold ok:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private refreshEta()V
    .locals 4

    .prologue
    .line 96
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "posting refresh ETA in 60 sec..."

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 97
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->refreshEtaRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 98
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->refreshEtaRunnable:Ljava/lang/Runnable;

    sget-wide v2, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->REFRESH_ETA_INTERVAL_MILLIS:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 99
    return-void
.end method

.method private sendTrafficDelayDismissEvent()V
    .locals 2

    .prologue
    .line 142
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/maps/MapEvents$TrafficDelayDismissEvent;

    invoke-direct {v1}, Lcom/navdy/hud/app/maps/MapEvents$TrafficDelayDismissEvent;-><init>()V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 143
    return-void
.end method


# virtual methods
.method public onNavigationModeChanged()V
    .locals 3

    .prologue
    .line 138
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->navigationModeChangeBkRunnable:Ljava/lang/Runnable;

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 139
    return-void
.end method

.method public onRouteUpdated(Lcom/here/android/mpa/routing/Route;)V
    .locals 0
    .param p1, "route"    # Lcom/here/android/mpa/routing/Route;

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;->sendTrafficDelayDismissEvent()V

    .line 134
    return-void
.end method
