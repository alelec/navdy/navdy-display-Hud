.class public Lcom/navdy/hud/app/maps/here/HereRouteCalculator;
.super Ljava/lang/Object;
.source "HereRouteCalculator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/maps/here/HereRouteCalculator$RouteCalculatorListener;
    }
.end annotation


# static fields
.field private static ID:Ljava/util/concurrent/atomic/AtomicLong; = null

.field private static final PROGRESS_INTERVAL_MS:I = 0xc8


# instance fields
.field private coreRouter:Lcom/here/android/mpa/routing/CoreRouter;

.field private logger:Lcom/navdy/service/library/log/Logger;

.field private tag:Ljava/lang/String;

.field private verbose:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 38
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x1

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->ID:Ljava/util/concurrent/atomic/AtomicLong;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/log/Logger;Z)V
    .locals 4
    .param p1, "logger"    # Lcom/navdy/service/library/log/Logger;
    .param p2, "verbose"    # Z

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->logger:Lcom/navdy/service/library/log/Logger;

    .line 57
    iput-boolean p2, p0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->verbose:Z

    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->ID:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicLong;->getAndIncrement()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->tag:Ljava/lang/String;

    .line 59
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/maps/here/HereRouteCalculator;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereRouteCalculator;
    .param p1, "x1"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->isRouteCancelled(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/maps/here/HereRouteCalculator;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->tag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/maps/here/HereRouteCalculator;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method private isRouteCancelled(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)Z
    .locals 4
    .param p1, "request"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .prologue
    const/4 v1, 0x0

    .line 265
    if-nez p1, :cond_1

    .line 273
    :cond_0
    :goto_0
    return v1

    .line 268
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->getActiveRouteCalcId()Ljava/lang/String;

    move-result-object v0

    .line 269
    .local v0, "activeRouteCalcId":Ljava/lang/String;
    iget-object v2, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestId:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 270
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "route request ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] is not active anymore, current ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 271
    const/4 v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public calculateRoute(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/common/GeoCoordinate;Ljava/util/List;Lcom/here/android/mpa/common/GeoCoordinate;ZLcom/navdy/hud/app/maps/here/HereRouteCalculator$RouteCalculatorListener;ILcom/here/android/mpa/routing/RouteOptions;Z)V
    .locals 12
    .param p1, "request"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .param p2, "startPoint"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p4, "endPoint"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p5, "factoringInTraffic"    # Z
    .param p6, "listener"    # Lcom/navdy/hud/app/maps/here/HereRouteCalculator$RouteCalculatorListener;
    .param p7, "maxRoutes"    # I
    .param p8, "routeOptions"    # Lcom/here/android/mpa/routing/RouteOptions;
    .param p9, "allowCancellation"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;",
            "Lcom/here/android/mpa/common/GeoCoordinate;",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/common/GeoCoordinate;",
            ">;",
            "Lcom/here/android/mpa/common/GeoCoordinate;",
            "Z",
            "Lcom/navdy/hud/app/maps/here/HereRouteCalculator$RouteCalculatorListener;",
            "I",
            "Lcom/here/android/mpa/routing/RouteOptions;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 70
    .local p3, "waypoints":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/common/GeoCoordinate;>;"
    const/4 v9, 0x1

    const/4 v10, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    move/from16 v11, p9

    invoke-virtual/range {v0 .. v11}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->calculateRoute(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/common/GeoCoordinate;Ljava/util/List;Lcom/here/android/mpa/common/GeoCoordinate;ZLcom/navdy/hud/app/maps/here/HereRouteCalculator$RouteCalculatorListener;ILcom/here/android/mpa/routing/RouteOptions;ZZZ)V

    .line 71
    return-void
.end method

.method public calculateRoute(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/common/GeoCoordinate;Ljava/util/List;Lcom/here/android/mpa/common/GeoCoordinate;ZLcom/navdy/hud/app/maps/here/HereRouteCalculator$RouteCalculatorListener;ILcom/here/android/mpa/routing/RouteOptions;ZZZ)V
    .locals 17
    .param p1, "request"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .param p2, "startPoint"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p4, "endPoint"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p5, "factoringInTraffic"    # Z
    .param p6, "listener"    # Lcom/navdy/hud/app/maps/here/HereRouteCalculator$RouteCalculatorListener;
    .param p7, "maxRoutes"    # I
    .param p8, "routeOptions"    # Lcom/here/android/mpa/routing/RouteOptions;
    .param p9, "cache"    # Z
    .param p10, "calculatePolyline"    # Z
    .param p11, "allowCancellation"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;",
            "Lcom/here/android/mpa/common/GeoCoordinate;",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/common/GeoCoordinate;",
            ">;",
            "Lcom/here/android/mpa/common/GeoCoordinate;",
            "Z",
            "Lcom/navdy/hud/app/maps/here/HereRouteCalculator$RouteCalculatorListener;",
            "I",
            "Lcom/here/android/mpa/routing/RouteOptions;",
            "ZZZ)V"
        }
    .end annotation

    .prologue
    .line 84
    .local p3, "waypoints":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/common/GeoCoordinate;>;"
    new-instance v3, Lcom/here/android/mpa/routing/CoreRouter;

    invoke-direct {v3}, Lcom/here/android/mpa/routing/CoreRouter;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->coreRouter:Lcom/here/android/mpa/routing/CoreRouter;

    .line 85
    new-instance v14, Lcom/here/android/mpa/routing/RoutePlan;

    invoke-direct {v14}, Lcom/here/android/mpa/routing/RoutePlan;-><init>()V

    .line 86
    .local v14, "routePlan":Lcom/here/android/mpa/routing/RoutePlan;
    move-object/from16 v0, p8

    move/from16 v1, p7

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/routing/RouteOptions;->setRouteCount(I)Lcom/here/android/mpa/routing/RouteOptions;

    .line 87
    move-object/from16 v0, p8

    invoke-virtual {v14, v0}, Lcom/here/android/mpa/routing/RoutePlan;->setRouteOptions(Lcom/here/android/mpa/routing/RouteOptions;)Lcom/here/android/mpa/routing/RoutePlan;

    .line 88
    new-instance v3, Lcom/here/android/mpa/routing/RouteWaypoint;

    move-object/from16 v0, p2

    invoke-direct {v3, v0}, Lcom/here/android/mpa/routing/RouteWaypoint;-><init>(Lcom/here/android/mpa/common/GeoCoordinate;)V

    invoke-virtual {v14, v3}, Lcom/here/android/mpa/routing/RoutePlan;->addWaypoint(Lcom/here/android/mpa/routing/RouteWaypoint;)Lcom/here/android/mpa/routing/RoutePlan;

    .line 90
    if-eqz p3, :cond_0

    .line 91
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/here/android/mpa/common/GeoCoordinate;

    .line 92
    .local v15, "waypoint":Lcom/here/android/mpa/common/GeoCoordinate;
    new-instance v4, Lcom/here/android/mpa/routing/RouteWaypoint;

    invoke-direct {v4, v15}, Lcom/here/android/mpa/routing/RouteWaypoint;-><init>(Lcom/here/android/mpa/common/GeoCoordinate;)V

    invoke-virtual {v14, v4}, Lcom/here/android/mpa/routing/RoutePlan;->addWaypoint(Lcom/here/android/mpa/routing/RouteWaypoint;)Lcom/here/android/mpa/routing/RoutePlan;

    goto :goto_0

    .line 97
    .end local v15    # "waypoint":Lcom/here/android/mpa/common/GeoCoordinate;
    :cond_0
    new-instance v3, Lcom/here/android/mpa/routing/RouteWaypoint;

    move-object/from16 v0, p4

    invoke-direct {v3, v0}, Lcom/here/android/mpa/routing/RouteWaypoint;-><init>(Lcom/here/android/mpa/common/GeoCoordinate;)V

    invoke-virtual {v14, v3}, Lcom/here/android/mpa/routing/RoutePlan;->addWaypoint(Lcom/here/android/mpa/routing/RouteWaypoint;)Lcom/here/android/mpa/routing/RoutePlan;

    .line 99
    new-instance v2, Lcom/here/android/mpa/routing/DynamicPenalty;

    invoke-direct {v2}, Lcom/here/android/mpa/routing/DynamicPenalty;-><init>()V

    .line 101
    .local v2, "dynamicPenalty":Lcom/here/android/mpa/routing/DynamicPenalty;
    if-eqz p5, :cond_1

    .line 102
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->tag:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "Factoring traffic in route calc[ONLINE]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 103
    sget-object v3, Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;->OPTIMAL:Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    invoke-virtual {v2, v3}, Lcom/here/android/mpa/routing/DynamicPenalty;->setTrafficPenaltyMode(Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;)V

    .line 104
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->coreRouter:Lcom/here/android/mpa/routing/CoreRouter;

    sget-object v4, Lcom/here/android/mpa/routing/CoreRouter$Connectivity;->ONLINE:Lcom/here/android/mpa/routing/CoreRouter$Connectivity;

    invoke-virtual {v3, v4}, Lcom/here/android/mpa/routing/CoreRouter;->setConnectivity(Lcom/here/android/mpa/routing/CoreRouter$Connectivity;)Lcom/here/android/mpa/routing/CoreRouter;

    .line 110
    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->coreRouter:Lcom/here/android/mpa/routing/CoreRouter;

    invoke-virtual {v3, v2}, Lcom/here/android/mpa/routing/CoreRouter;->setDynamicPenalty(Lcom/here/android/mpa/routing/DynamicPenalty;)Lcom/here/android/mpa/routing/CoreRouter;

    .line 112
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->tag:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "Generating route with "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v14}, Lcom/here/android/mpa/routing/RoutePlan;->getWaypointCount()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " waypoints"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 113
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v12

    .line 114
    .local v12, "startTime":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->coreRouter:Lcom/here/android/mpa/routing/CoreRouter;

    move-object/from16 v16, v0

    new-instance v3, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;

    move-object/from16 v4, p0

    move-object/from16 v5, p6

    move/from16 v6, p11

    move-object/from16 v7, p1

    move/from16 v8, p10

    move/from16 v9, p9

    move/from16 v10, p5

    move-object/from16 v11, p2

    invoke-direct/range {v3 .. v13}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;-><init>(Lcom/navdy/hud/app/maps/here/HereRouteCalculator;Lcom/navdy/hud/app/maps/here/HereRouteCalculator$RouteCalculatorListener;ZLcom/navdy/service/library/events/navigation/NavigationRouteRequest;ZZZLcom/here/android/mpa/common/GeoCoordinate;J)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v14, v3}, Lcom/here/android/mpa/routing/CoreRouter;->calculateRoute(Lcom/here/android/mpa/routing/RoutePlan;Lcom/here/android/mpa/routing/Router$Listener;)V

    .line 216
    return-void

    .line 106
    .end local v12    # "startTime":J
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->tag:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "Not factoring traffic in route calc[OFFLINE]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 107
    sget-object v3, Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;->DISABLED:Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    invoke-virtual {v2, v3}, Lcom/here/android/mpa/routing/DynamicPenalty;->setTrafficPenaltyMode(Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;)V

    .line 108
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->coreRouter:Lcom/here/android/mpa/routing/CoreRouter;

    sget-object v4, Lcom/here/android/mpa/routing/CoreRouter$Connectivity;->OFFLINE:Lcom/here/android/mpa/routing/CoreRouter$Connectivity;

    invoke-virtual {v3, v4}, Lcom/here/android/mpa/routing/CoreRouter;->setConnectivity(Lcom/here/android/mpa/routing/CoreRouter$Connectivity;)Lcom/here/android/mpa/routing/CoreRouter;

    goto/16 :goto_1
.end method

.method public cancel()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 254
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->coreRouter:Lcom/here/android/mpa/routing/CoreRouter;

    if-eqz v1, :cond_0

    .line 255
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->coreRouter:Lcom/here/android/mpa/routing/CoreRouter;

    invoke-virtual {v1}, Lcom/here/android/mpa/routing/CoreRouter;->cancel()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 260
    :cond_0
    iput-object v3, p0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->coreRouter:Lcom/here/android/mpa/routing/CoreRouter;

    .line 262
    :goto_0
    return-void

    .line 257
    :catch_0
    move-exception v0

    .line 258
    .local v0, "t":Ljava/lang/Throwable;
    :try_start_1
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->logger:Lcom/navdy/service/library/log/Logger;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 260
    iput-object v3, p0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->coreRouter:Lcom/here/android/mpa/routing/CoreRouter;

    goto :goto_0

    .end local v0    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v1

    iput-object v3, p0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->coreRouter:Lcom/here/android/mpa/routing/CoreRouter;

    throw v1
.end method

.method public getRouteResultFromRoute(Ljava/lang/String;Lcom/here/android/mpa/routing/Route;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    .locals 24
    .param p1, "routeId"    # Ljava/lang/String;
    .param p2, "route"    # Lcom/here/android/mpa/routing/Route;
    .param p3, "via"    # Ljava/lang/String;
    .param p4, "label"    # Ljava/lang/String;
    .param p5, "address"    # Ljava/lang/String;
    .param p6, "calculatePolyline"    # Z

    .prologue
    .line 219
    const/4 v10, 0x0

    .line 220
    .local v10, "simplified":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 222
    .local v6, "oldList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/location/Coordinate;>;"
    if-eqz p6, :cond_0

    .line 223
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v18

    .line 224
    .local v18, "t1":J
    invoke-virtual/range {p2 .. p2}, Lcom/here/android/mpa/routing/Route;->getRouteGeometry()Ljava/util/List;

    move-result-object v16

    .line 225
    .local v16, "routeGeometry":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/common/GeoCoordinate;>;"
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v20

    .line 226
    .local v20, "t2":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->tag:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "getting geometry took "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sub-long v8, v20, v18

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " ms"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 228
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->tag:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "points before simplification: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 229
    invoke-static/range {v16 .. v16}, Lcom/navdy/hud/app/maps/util/RouteUtils;->simplify(Ljava/util/List;)Ljava/util/List;

    move-result-object v17

    .line 230
    .end local v10    # "simplified":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    .local v17, "simplified":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->tag:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "points after simplification: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 233
    new-instance v2, Lcom/navdy/service/library/events/location/Coordinate;

    const/4 v4, 0x0

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/here/android/mpa/common/GeoCoordinate;

    invoke-virtual {v4}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/here/android/mpa/common/GeoCoordinate;

    invoke-virtual {v4}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-direct/range {v2 .. v10}, Lcom/navdy/service/library/events/location/Coordinate;-><init>(Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Float;Ljava/lang/Double;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Long;Ljava/lang/String;)V

    .line 234
    .end local v6    # "oldList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/location/Coordinate;>;"
    .local v2, "first":Lcom/navdy/service/library/events/location/Coordinate;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 235
    .restart local v6    # "oldList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/location/Coordinate;>;"
    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object/from16 v10, v17

    .line 240
    .end local v2    # "first":Lcom/navdy/service/library/events/location/Coordinate;
    .end local v16    # "routeGeometry":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/common/GeoCoordinate;>;"
    .end local v17    # "simplified":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    .end local v18    # "t1":J
    .end local v20    # "t2":J
    .restart local v10    # "simplified":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    :goto_0
    sget-object v4, Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;->OPTIMAL:Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    const v5, 0xfffffff

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v5}, Lcom/here/android/mpa/routing/Route;->getTta(Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;I)Lcom/here/android/mpa/routing/RouteTta;

    move-result-object v23

    .line 241
    .local v23, "tta":Lcom/here/android/mpa/routing/RouteTta;
    invoke-virtual/range {v23 .. v23}, Lcom/here/android/mpa/routing/RouteTta;->getDuration()I

    move-result v22

    .line 242
    .local v22, "trafficDuration":I
    invoke-virtual/range {p2 .. p2}, Lcom/here/android/mpa/routing/Route;->getLength()I

    move-result v15

    .line 244
    .local v15, "length":I
    sget-object v4, Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;->DISABLED:Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    const v5, 0xfffffff

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v5}, Lcom/here/android/mpa/routing/Route;->getTta(Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;I)Lcom/here/android/mpa/routing/RouteTta;

    move-result-object v14

    .line 245
    .local v14, "freeFlowTta":Lcom/here/android/mpa/routing/RouteTta;
    invoke-virtual {v14}, Lcom/here/android/mpa/routing/RouteTta;->getDuration()I

    move-result v13

    .line 247
    .local v13, "freeFlowDuration":I
    new-instance v3, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    move-object/from16 v4, p1

    move-object/from16 v5, p4

    move-object/from16 v11, p3

    move-object/from16 v12, p5

    invoke-direct/range {v3 .. v12}, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    .local v3, "outgoingRouteResult":Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->tag:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "route id["

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "] via["

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "] length["

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "] duration["

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v22

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "] freeFlowDuration["

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "]"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 249
    return-object v3

    .line 237
    .end local v3    # "outgoingRouteResult":Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    .end local v13    # "freeFlowDuration":I
    .end local v14    # "freeFlowTta":Lcom/here/android/mpa/routing/RouteTta;
    .end local v15    # "length":I
    .end local v22    # "trafficDuration":I
    .end local v23    # "tta":Lcom/here/android/mpa/routing/RouteTta;
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->tag:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " poly line calc off"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
