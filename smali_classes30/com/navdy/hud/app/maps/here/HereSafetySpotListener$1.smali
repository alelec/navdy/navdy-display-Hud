.class Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$1;
.super Ljava/lang/Object;
.source "HereSafetySpotListener.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$1;->this$0:Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 60
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$1;->this$0:Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

    # invokes: Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->removeExpiredSpots()Ljava/util/List;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->access$000(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;)Ljava/util/List;

    move-result-object v0

    .line 61
    .local v0, "invalid":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;>;"
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$1;->this$0:Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->canDrawOnTheMap:Z
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->access$100(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 62
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$1;->this$0:Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

    # invokes: Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->removeMapMarkers(Ljava/util/List;)V
    invoke-static {v2, v0}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->access$200(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$1;->this$0:Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->access$500(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;)Landroid/os/Handler;

    move-result-object v2

    # getter for: Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->CHECK_INTERVAL:I
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->access$400()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v2, p0, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 69
    .end local v0    # "invalid":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;>;"
    :goto_0
    return-void

    .line 64
    :catch_0
    move-exception v1

    .line 65
    .local v1, "t":Ljava/lang/Throwable;
    :try_start_1
    # getter for: Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->access$300()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "check"

    invoke-virtual {v2, v3, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 67
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$1;->this$0:Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->access$500(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;)Landroid/os/Handler;

    move-result-object v2

    # getter for: Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->CHECK_INTERVAL:I
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->access$400()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v2, p0, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .end local v1    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$1;->this$0:Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->handler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->access$500(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;)Landroid/os/Handler;

    move-result-object v3

    # getter for: Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->CHECK_INTERVAL:I
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->access$400()I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v3, p0, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    throw v2
.end method
