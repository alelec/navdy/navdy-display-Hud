.class Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;
.super Ljava/lang/Object;
.source "NavdyTrafficRerouteManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->postSuccess(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

.field final synthetic val$outgoingResults:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;Ljava/util/ArrayList;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    .prologue
    .line 559
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->val$outgoingResults:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 31

    .prologue
    .line 563
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->running:Z
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$800(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 564
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "reCalculateCurrentRoute: success,route manager stopped"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 565
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # invokes: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->cleanupRouteIfStale()V
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$600(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)V

    .line 566
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$000(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-boolean v5, v5, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$notifFromHereTraffic:Z

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-wide v6, v8, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$diff:J

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-object v8, v8, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$via:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-object v9, v9, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$currentVia:Ljava/lang/String;

    invoke-virtual/range {v4 .. v9}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->fasterRouteNotFound(ZJLjava/lang/String;Ljava/lang/String;)V

    .line 629
    :goto_0
    return-void

    .line 570
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->val$outgoingResults:Ljava/util/ArrayList;

    if-eqz v4, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->val$outgoingResults:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_2

    .line 571
    :cond_1
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "reCalculateCurrentRoute: success, but no result"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 572
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # invokes: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->cleanupRouteIfStale()V
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$600(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)V

    .line 573
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$000(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-boolean v5, v5, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$notifFromHereTraffic:Z

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-wide v6, v8, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$diff:J

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-object v8, v8, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$via:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-object v9, v9, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$currentVia:Ljava/lang/String;

    invoke-virtual/range {v4 .. v9}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->fasterRouteNotFound(ZJLjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 626
    :catch_0
    move-exception v30

    .line 627
    .local v30, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    move-object/from16 v0, v30

    invoke-virtual {v4, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 577
    .end local v30    # "t":Ljava/lang/Throwable;
    :cond_2
    :try_start_1
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "reCalculateCurrentRoute: success"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 578
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->val$outgoingResults:Ljava/util/ArrayList;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    .line 579
    .local v28, "result":Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getInstance()Lcom/navdy/hud/app/maps/here/HereRouteCache;

    move-result-object v24

    .line 580
    .local v24, "hereRouteCache":Lcom/navdy/hud/app/maps/here/HereRouteCache;
    move-object/from16 v0, v28

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeId:Ljava/lang/String;

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getRoute(Ljava/lang/String;)Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;

    move-result-object v29

    .line 582
    .local v29, "routeInfo":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    if-nez v29, :cond_3

    .line 583
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "reCalculateCurrentRoute: did not get routeinfo"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 584
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # invokes: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->cleanupRouteIfStale()V
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$600(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)V

    .line 585
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$000(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-boolean v5, v5, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$notifFromHereTraffic:Z

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-wide v6, v8, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$diff:J

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-object v8, v8, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$via:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-object v9, v9, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$currentVia:Ljava/lang/String;

    invoke-virtual/range {v4 .. v9}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->fasterRouteNotFound(ZJLjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 589
    :cond_3
    move-object/from16 v0, v28

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeId:Ljava/lang/String;

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->removeRoute(Ljava/lang/String;)Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;

    .line 591
    move-object/from16 v0, v28

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->duration_traffic:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-nez v4, :cond_4

    .line 592
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "reCalculateCurrentRoute: did not get traffic duration"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 593
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # invokes: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->cleanupRouteIfStale()V
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$600(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)V

    .line 594
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$000(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-boolean v5, v5, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$notifFromHereTraffic:Z

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-wide v6, v8, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$diff:J

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-object v8, v8, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$via:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-object v9, v9, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$currentVia:Ljava/lang/String;

    invoke-virtual/range {v4 .. v9}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->fasterRouteNotFound(ZJLjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 598
    :cond_4
    move-object/from16 v0, v29

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->route:Lcom/here/android/mpa/routing/Route;

    sget-object v5, Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;->OPTIMAL:Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    const v8, 0xfffffff

    invoke-virtual {v4, v5, v8}, Lcom/here/android/mpa/routing/Route;->getTta(Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;I)Lcom/here/android/mpa/routing/RouteTta;

    move-result-object v19

    .line 599
    .local v19, "currentTrafficTta":Lcom/here/android/mpa/routing/RouteTta;
    if-nez v19, :cond_5

    .line 600
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "reCalculateCurrentRoute: could not get traffic route tta"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 601
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # invokes: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->cleanupRouteIfStale()V
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$600(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)V

    .line 602
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$000(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-boolean v5, v5, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$notifFromHereTraffic:Z

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-wide v6, v8, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$diff:J

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-object v8, v8, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$via:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-object v9, v9, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$currentVia:Ljava/lang/String;

    invoke-virtual/range {v4 .. v9}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->fasterRouteNotFound(ZJLjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 606
    :cond_5
    invoke-virtual/range {v19 .. v19}, Lcom/here/android/mpa/routing/RouteTta;->getDuration()I

    move-result v18

    .line 607
    .local v18, "currentDuration":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$fasterRouteTta:Lcom/here/android/mpa/routing/RouteTta;

    invoke-virtual {v4}, Lcom/here/android/mpa/routing/RouteTta;->getDuration()I

    move-result v23

    .line 608
    .local v23, "fasterDuration":I
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "reCalculateCurrentRoute: currentDuration="

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, " fasterDuration="

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v23

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 610
    sub-int v4, v18, v23

    int-to-long v6, v4

    .line 611
    .local v6, "newDiff":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$000(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->getRerouteMinDuration()I

    move-result v25

    .line 612
    .local v25, "minDuration":I
    move/from16 v0, v25

    int-to-long v4, v0

    cmp-long v4, v6, v4

    if-ltz v4, :cond_6

    .line 613
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "reCalculateCurrentRoute: set faster route, duration:"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, " >= threshold:"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 614
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v26

    .line 615
    .local v26, "now":J
    move/from16 v0, v23

    mul-int/lit16 v4, v0, 0x3e8

    int-to-long v4, v4

    add-long v16, v26, v4

    .line 616
    .local v16, "newEtaUtc":J
    new-instance v20, Ljava/util/Date;

    move-object/from16 v0, v20

    move-wide/from16 v1, v16

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    .line 617
    .local v20, "d1":Ljava/util/Date;
    new-instance v21, Ljava/util/Date;

    move-object/from16 v0, v21

    move-wide/from16 v1, v26

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    .line 618
    .local v21, "d2":Ljava/util/Date;
    new-instance v22, Ljava/util/Date;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-wide v4, v4, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$etaUtc:J

    move-object/from16 v0, v22

    invoke-direct {v0, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 619
    .local v22, "d3":Ljava/util/Date;
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "reCalculateCurrentRoute: newEta:"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, " now="

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, " currentEta="

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 620
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$000(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-object v6, v4, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$fasterRoute:Lcom/here/android/mpa/routing/Route;

    .end local v6    # "newDiff":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-boolean v7, v4, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$notifFromHereTraffic:Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-object v8, v4, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$via:Ljava/lang/String;

    const-wide/16 v9, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-object v11, v4, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$additionalVia:Ljava/lang/String;

    const-wide/16 v12, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-wide v14, v4, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$distanceDiff:J

    invoke-virtual/range {v5 .. v17}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->setFasterRoute(Lcom/here/android/mpa/routing/Route;ZLjava/lang/String;JLjava/lang/String;JJJ)V

    goto/16 :goto_0

    .line 622
    .end local v16    # "newEtaUtc":J
    .end local v20    # "d1":Ljava/util/Date;
    .end local v21    # "d2":Ljava/util/Date;
    .end local v22    # "d3":Ljava/util/Date;
    .end local v26    # "now":J
    .restart local v6    # "newDiff":J
    :cond_6
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "reCalculateCurrentRoute: duration:"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, " < threshold:"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 623
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # invokes: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->cleanupRouteIfStale()V
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$600(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)V

    .line 624
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$000(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-boolean v5, v5, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$notifFromHereTraffic:Z

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-object v8, v8, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$via:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    iget-object v9, v9, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;->val$currentVia:Ljava/lang/String;

    invoke-virtual/range {v4 .. v9}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->fasterRouteNotFound(ZJLjava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
