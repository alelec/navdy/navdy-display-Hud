.class final Lcom/navdy/hud/app/maps/here/HerePlacesManager$10;
.super Ljava/lang/Object;
.source "HerePlacesManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HerePlacesManager;->returnAutoCompleteResults(Ljava/lang/String;ILjava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$autoCompleteSearch:Ljava/lang/String;

.field final synthetic val$maxResults:I

.field final synthetic val$results:Ljava/util/List;


# direct methods
.method constructor <init>(Ljava/util/List;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 697
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$10;->val$results:Ljava/util/List;

    iput p2, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$10;->val$maxResults:I

    iput-object p3, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$10;->val$autoCompleteSearch:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 701
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$10;->val$results:Ljava/util/List;

    .line 703
    .local v1, "ret":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$10;->val$results:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    iget v4, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$10;->val$maxResults:I

    if-le v3, v4, :cond_0

    .line 704
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$10;->val$results:Ljava/util/List;

    const/4 v4, 0x0

    iget v5, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$10;->val$maxResults:I

    invoke-interface {v3, v4, v5}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    .line 707
    :cond_0
    new-instance v0, Lcom/navdy/service/library/events/places/AutoCompleteResponse;

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$10;->val$autoCompleteSearch:Ljava/lang/String;

    sget-object v4, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    const/4 v5, 0x0

    invoke-direct {v0, v3, v4, v5, v1}, Lcom/navdy/service/library/events/places/AutoCompleteResponse;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Ljava/util/List;)V

    .line 708
    .local v0, "response":Lcom/navdy/service/library/events/places/AutoCompleteResponse;
    invoke-static {}, Lcom/navdy/hud/app/maps/MapsEventHandler;->getInstance()Lcom/navdy/hud/app/maps/MapsEventHandler;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/navdy/hud/app/maps/MapsEventHandler;->sendEventToClient(Lcom/squareup/wire/Message;)V

    .line 709
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[autocomplete] returned:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 714
    .end local v0    # "response":Lcom/navdy/service/library/events/places/AutoCompleteResponse;
    .end local v1    # "ret":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_0
    return-void

    .line 710
    :catch_0
    move-exception v2

    .line 711
    .local v2, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 712
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$10;->val$autoCompleteSearch:Ljava/lang/String;

    sget-object v4, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_UNKNOWN_ERROR:Lcom/navdy/service/library/events/RequestStatus;

    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->context:Landroid/content/Context;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$200()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0902da

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    # invokes: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->returnSearchErrorResponse(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V
    invoke-static {v3, v4, v5}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$300(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V

    goto :goto_0
.end method
