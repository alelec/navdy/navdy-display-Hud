.class Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;
.super Ljava/lang/Object;
.source "NavdyTrafficRerouteManager.java"

# interfaces
.implements Lcom/navdy/hud/app/maps/here/HereRouteCalculator$RouteCalculatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;

    .prologue
    .line 154
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public error(Lcom/here/android/mpa/routing/RoutingError;Ljava/lang/Throwable;)V
    .locals 5
    .param p1, "error"    # Lcom/here/android/mpa/routing/RoutingError;
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 333
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$500(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;

    iget-object v3, v3, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->killRouteCalc:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$400(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 334
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # invokes: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->cleanupRouteIfStale()V
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$600(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)V

    .line 335
    invoke-static {}, Lcom/navdy/hud/app/framework/voice/TTSUtils;->isDebugTTSEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 336
    const-string v1, "Faster route not found [Error]"

    .line 337
    .local v1, "title":Ljava/lang/String;
    const-string v0, ""

    .line 338
    .local v0, "info":Ljava/lang/String;
    if-eqz p2, :cond_2

    .line 339
    invoke-virtual {p2}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v0

    .line 343
    :cond_0
    :goto_0
    invoke-static {v1, v0}, Lcom/navdy/hud/app/framework/voice/TTSUtils;->debugShowFasterRouteToast(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    .end local v0    # "info":Ljava/lang/String;
    .end local v1    # "title":Ljava/lang/String;
    :cond_1
    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "error occured:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, p2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 346
    return-void

    .line 340
    .restart local v0    # "info":Ljava/lang/String;
    .restart local v1    # "title":Ljava/lang/String;
    :cond_2
    if-eqz p1, :cond_0

    .line 341
    invoke-virtual {p1}, Lcom/here/android/mpa/routing/RoutingError;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public postSuccess(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 162
    .local p1, "outgoingResults":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/navigation/NavigationRouteResult;>;"
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1$1;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1$1;-><init>(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;Ljava/util/ArrayList;)V

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 329
    return-void
.end method

.method public preSuccess()V
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$500(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4$1;->this$1:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;

    iget-object v1, v1, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;->this$0:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    # getter for: Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->killRouteCalc:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->access$400(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 158
    return-void
.end method

.method public progress(I)V
    .locals 0
    .param p1, "progress"    # I

    .prologue
    .line 349
    return-void
.end method
