.class Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$4;
.super Ljava/lang/Object;
.source "HereTrafficUpdater2.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    .prologue
    .line 220
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 223
    const/4 v0, 0x0

    .line 225
    .local v0, "check":Z
    :try_start_0
    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v4, "checkRunnable"

    invoke-virtual {v1, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 226
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->currentRequestInfo:Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$200(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;)Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->lastRequestTime:J
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$300(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-lez v1, :cond_0

    .line 227
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->lastRequestTime:J
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$300(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;)J

    move-result-wide v6

    sub-long v2, v4, v6

    .line 228
    .local v2, "diff":J
    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "checkRunnable:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 229
    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->STALE_REQUEST_TIME:I
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$1100()I

    move-result v1

    int-to-long v4, v1

    cmp-long v1, v2, v4

    if-ltz v1, :cond_2

    .line 230
    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v4, "checkRunnable: request stale"

    invoke-virtual {v1, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 231
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    const/16 v4, 0x1388

    # invokes: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->reset(I)V
    invoke-static {v1, v4}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$400(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 238
    .end local v2    # "diff":J
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 239
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$1300(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;)Landroid/os/Handler;

    move-result-object v1

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->TRAFFIC_DATA_CHECK_TIME_INTERVAL:I
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$1200()I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v1, p0, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 242
    :cond_1
    return-void

    .line 233
    .restart local v2    # "diff":J
    :cond_2
    const/4 v0, 0x1

    .line 234
    :try_start_1
    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v4, "checkRunnable: request not stale"

    invoke-virtual {v1, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 238
    .end local v2    # "diff":J
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_3

    .line 239
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$4;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->handler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$1300(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;)Landroid/os/Handler;

    move-result-object v4

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->TRAFFIC_DATA_CHECK_TIME_INTERVAL:I
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$1200()I

    move-result v5

    int-to-long v6, v5

    invoke-virtual {v4, p0, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_3
    throw v1
.end method
