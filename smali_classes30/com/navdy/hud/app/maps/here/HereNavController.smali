.class public Lcom/navdy/hud/app/maps/here/HereNavController;
.super Ljava/lang/Object;
.source "HereNavController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/maps/here/HereNavController$State;
    }
.end annotation


# static fields
.field private static final ANIMATE_STATE_CLEAR_DELAY:I = 0x7d0

.field private static final TRIP_END:Lcom/navdy/hud/app/framework/trips/TripManager$FinishedTripRouteEvent;

.field private static final TRIP_START:Lcom/navdy/hud/app/framework/trips/TripManager$NewTripEvent;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private final bus:Lcom/squareup/otto/Bus;

.field private clearAnimatorState:Ljava/lang/Runnable;

.field private clearAnimatorStateBk:Ljava/lang/Runnable;

.field private firstTrip:Z

.field private handler:Landroid/os/Handler;

.field private initialized:Z

.field private final navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

.field private final navigationQualityTracker:Lcom/navdy/hud/app/analytics/NavigationQualityTracker;

.field private route:Lcom/here/android/mpa/routing/Route;

.field private volatile state:Lcom/navdy/hud/app/maps/here/HereNavController$State;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 35
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/maps/here/HereNavController;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereNavController;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 57
    new-instance v0, Lcom/navdy/hud/app/framework/trips/TripManager$NewTripEvent;

    invoke-direct {v0}, Lcom/navdy/hud/app/framework/trips/TripManager$NewTripEvent;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereNavController;->TRIP_START:Lcom/navdy/hud/app/framework/trips/TripManager$NewTripEvent;

    .line 58
    new-instance v0, Lcom/navdy/hud/app/framework/trips/TripManager$FinishedTripRouteEvent;

    invoke-direct {v0}, Lcom/navdy/hud/app/framework/trips/TripManager$FinishedTripRouteEvent;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereNavController;->TRIP_END:Lcom/navdy/hud/app/framework/trips/TripManager$FinishedTripRouteEvent;

    return-void
.end method

.method constructor <init>(Lcom/here/android/mpa/guidance/NavigationManager;Lcom/squareup/otto/Bus;)V
    .locals 3
    .param p1, "navigationManager"    # Lcom/here/android/mpa/guidance/NavigationManager;
    .param p2, "bus"    # Lcom/squareup/otto/Bus;

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->handler:Landroid/os/Handler;

    .line 60
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->firstTrip:Z

    .line 63
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereNavController$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/here/HereNavController$1;-><init>(Lcom/navdy/hud/app/maps/here/HereNavController;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->clearAnimatorState:Ljava/lang/Runnable;

    .line 70
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereNavController$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/here/HereNavController$2;-><init>(Lcom/navdy/hud/app/maps/here/HereNavController;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->clearAnimatorStateBk:Ljava/lang/Runnable;

    .line 78
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    .line 79
    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->bus:Lcom/squareup/otto/Bus;

    .line 80
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavController;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ":ctor: state:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->state:Lcom/navdy/hud/app/maps/here/HereNavController$State;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 81
    invoke-static {}, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->getInstance()Lcom/navdy/hud/app/analytics/NavigationQualityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->navigationQualityTracker:Lcom/navdy/hud/app/analytics/NavigationQualityTracker;

    .line 82
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/maps/here/HereNavController;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereNavController;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->clearAnimatorStateBk:Ljava/lang/Runnable;

    return-object v0
.end method

.method private startTrip()V
    .locals 2

    .prologue
    .line 309
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->firstTrip:Z

    if-eqz v0, :cond_0

    .line 310
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->firstTrip:Z

    .line 311
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavController;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "first trip started"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 315
    :goto_0
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->bus:Lcom/squareup/otto/Bus;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavController;->TRIP_START:Lcom/navdy/hud/app/framework/trips/TripManager$NewTripEvent;

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 316
    return-void

    .line 313
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavController;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "trip started"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public addLaneInfoListener(Ljava/lang/ref/WeakReference;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/here/android/mpa/guidance/NavigationManager$LaneInformationListener;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 253
    .local p1, "listener":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/here/android/mpa/guidance/NavigationManager$LaneInformationListener;>;"
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 254
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v0, p1}, Lcom/here/android/mpa/guidance/NavigationManager;->addLaneInformationListener(Ljava/lang/ref/WeakReference;)V

    .line 255
    return-void
.end method

.method public addRealisticViewAspectRatio(Lcom/here/android/mpa/guidance/NavigationManager$AspectRatio;)V
    .locals 1
    .param p1, "aspectRatio"    # Lcom/here/android/mpa/guidance/NavigationManager$AspectRatio;

    .prologue
    .line 269
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 270
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v0, p1}, Lcom/here/android/mpa/guidance/NavigationManager;->addRealisticViewAspectRatio(Lcom/here/android/mpa/guidance/NavigationManager$AspectRatio;)V

    .line 271
    return-void
.end method

.method public addRealisticViewListener(Ljava/lang/ref/WeakReference;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/here/android/mpa/guidance/NavigationManager$RealisticViewListener;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 274
    .local p1, "listener":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/here/android/mpa/guidance/NavigationManager$RealisticViewListener;>;"
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 275
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v0, p1}, Lcom/here/android/mpa/guidance/NavigationManager;->addRealisticViewListener(Ljava/lang/ref/WeakReference;)V

    .line 276
    return-void
.end method

.method public addTrafficRerouteListener(Ljava/lang/ref/WeakReference;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/here/android/mpa/guidance/NavigationManager$TrafficRerouteListener;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 289
    .local p1, "listener":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/here/android/mpa/guidance/NavigationManager$TrafficRerouteListener;>;"
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 290
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v0, p1}, Lcom/here/android/mpa/guidance/NavigationManager;->addTrafficRerouteListener(Ljava/lang/ref/WeakReference;)V

    .line 291
    return-void
.end method

.method public declared-synchronized arrivedAtDestination()V
    .locals 4

    .prologue
    .line 179
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 180
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavController;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "arrivedAtDestination state["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->state:Lcom/navdy/hud/app/maps/here/HereNavController$State;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 182
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->state:Lcom/navdy/hud/app/maps/here/HereNavController$State;

    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavController$State;->NAVIGATING:Lcom/navdy/hud/app/maps/here/HereNavController$State;

    if-ne v1, v2, :cond_0

    .line 183
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->navigationQualityTracker:Lcom/navdy/hud/app/analytics/NavigationQualityTracker;

    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereNavController;->getElapsedDistance()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->trackTripEnded(J)V

    .line 184
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereNavController;->expireGlympseTickets()V

    .line 185
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v1}, Lcom/here/android/mpa/guidance/NavigationManager;->stop()V

    .line 186
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v1}, Lcom/here/android/mpa/guidance/NavigationManager;->startTracking()Lcom/here/android/mpa/guidance/NavigationManager$Error;

    move-result-object v0

    .line 187
    .local v0, "error":Lcom/here/android/mpa/guidance/NavigationManager$Error;
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavController;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "arrivedAtDestination: tracking error state ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 191
    .end local v0    # "error":Lcom/here/android/mpa/guidance/NavigationManager$Error;
    :goto_0
    monitor-exit p0

    return-void

    .line 189
    :cond_0
    :try_start_1
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavController;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "arrivedAtDestination: not navigating:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->state:Lcom/navdy/hud/app/maps/here/HereNavController$State;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 179
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public endTrip()V
    .locals 2

    .prologue
    .line 319
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavController;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "trip ended"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 320
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->bus:Lcom/squareup/otto/Bus;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavController;->TRIP_END:Lcom/navdy/hud/app/framework/trips/TripManager$FinishedTripRouteEvent;

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 321
    return-void
.end method

.method expireGlympseTickets()V
    .locals 2

    .prologue
    .line 329
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereNavController$3;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/maps/here/HereNavController$3;-><init>(Lcom/navdy/hud/app/maps/here/HereNavController;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 335
    return-void
.end method

.method public getAfterNextManeuver()Lcom/here/android/mpa/routing/Maneuver;
    .locals 1

    .prologue
    .line 243
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 244
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v0}, Lcom/here/android/mpa/guidance/NavigationManager;->getAfterNextManeuver()Lcom/here/android/mpa/routing/Maneuver;

    move-result-object v0

    return-object v0
.end method

.method public getDestinationDistance()J
    .locals 2

    .prologue
    .line 209
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 210
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->state:Lcom/navdy/hud/app/maps/here/HereNavController$State;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavController$State;->TRACKING:Lcom/navdy/hud/app/maps/here/HereNavController$State;

    if-ne v0, v1, :cond_0

    .line 211
    const-wide/16 v0, 0x0

    .line 213
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v0}, Lcom/here/android/mpa/guidance/NavigationManager;->getDestinationDistance()J

    move-result-wide v0

    goto :goto_0
.end method

.method public getElapsedDistance()J
    .locals 2

    .prologue
    .line 248
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 249
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v0}, Lcom/here/android/mpa/guidance/NavigationManager;->getElapsedDistance()J

    move-result-wide v0

    return-wide v0
.end method

.method public getEta(ZLcom/here/android/mpa/routing/Route$TrafficPenaltyMode;)Ljava/util/Date;
    .locals 1
    .param p1, "wholeRoute"    # Z
    .param p2, "trafficPenaltyMode"    # Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    .prologue
    .line 218
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 219
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v0, p1, p2}, Lcom/here/android/mpa/guidance/NavigationManager;->getEta(ZLcom/here/android/mpa/routing/Route$TrafficPenaltyMode;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getNextManeuver()Lcom/here/android/mpa/routing/Maneuver;
    .locals 1

    .prologue
    .line 238
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 239
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v0}, Lcom/here/android/mpa/guidance/NavigationManager;->getNextManeuver()Lcom/here/android/mpa/routing/Maneuver;

    move-result-object v0

    return-object v0
.end method

.method public getNextManeuverDistance()J
    .locals 2

    .prologue
    .line 304
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 305
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v0}, Lcom/here/android/mpa/guidance/NavigationManager;->getNextManeuverDistance()J

    move-result-wide v0

    return-wide v0
.end method

.method public getState()Lcom/navdy/hud/app/maps/here/HereNavController$State;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->state:Lcom/navdy/hud/app/maps/here/HereNavController$State;

    return-object v0
.end method

.method public getTta(Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;Z)Lcom/here/android/mpa/routing/RouteTta;
    .locals 1
    .param p1, "trafficPenaltyMode"    # Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;
    .param p2, "wholeRoute"    # Z

    .prologue
    .line 204
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 205
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v0, p1, p2}, Lcom/here/android/mpa/guidance/NavigationManager;->getTta(Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;Z)Lcom/here/android/mpa/routing/RouteTta;

    move-result-object v0

    return-object v0
.end method

.method public getTtaDate(ZLcom/here/android/mpa/routing/Route$TrafficPenaltyMode;)Ljava/util/Date;
    .locals 8
    .param p1, "wholeRoute"    # Z
    .param p2, "trafficPenaltyMode"    # Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    .prologue
    const/4 v2, 0x0

    .line 223
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 224
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v3, p2, p1}, Lcom/here/android/mpa/guidance/NavigationManager;->getTta(Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;Z)Lcom/here/android/mpa/routing/RouteTta;

    move-result-object v0

    .line 225
    .local v0, "routeTta":Lcom/here/android/mpa/routing/RouteTta;
    if-eqz v0, :cond_0

    .line 226
    invoke-virtual {v0}, Lcom/here/android/mpa/routing/RouteTta;->getDuration()I

    move-result v1

    .line 227
    .local v1, "trafficDuration":I
    if-lez v1, :cond_0

    .line 228
    new-instance v2, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    int-to-long v6, v1

    invoke-virtual {v3, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    add-long/2addr v4, v6

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 233
    .end local v1    # "trafficDuration":I
    :cond_0
    return-object v2
.end method

.method public declared-synchronized initialize()V
    .locals 4

    .prologue
    .line 89
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->initialized:Z

    if-nez v1, :cond_0

    .line 90
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->initialized:Z

    .line 91
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavController$State;->TRACKING:Lcom/navdy/hud/app/maps/here/HereNavController$State;

    iput-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->state:Lcom/navdy/hud/app/maps/here/HereNavController$State;

    .line 92
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v1}, Lcom/here/android/mpa/guidance/NavigationManager;->startTracking()Lcom/here/android/mpa/guidance/NavigationManager$Error;

    move-result-object v0

    .line 93
    .local v0, "error":Lcom/here/android/mpa/guidance/NavigationManager$Error;
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavController;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initialize state:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->state:Lcom/navdy/hud/app/maps/here/HereNavController$State;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " error = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 94
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereNavController;->startTrip()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 96
    .end local v0    # "error":Lcom/here/android/mpa/guidance/NavigationManager$Error;
    :cond_0
    monitor-exit p0

    return-void

    .line 89
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized isInitialized()Z
    .locals 1

    .prologue
    .line 99
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->initialized:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized pause()V
    .locals 1

    .prologue
    .line 194
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 195
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v0}, Lcom/here/android/mpa/guidance/NavigationManager;->pause()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 196
    monitor-exit p0

    return-void

    .line 194
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public removeLaneInfoListener(Lcom/here/android/mpa/guidance/NavigationManager$LaneInformationListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/here/android/mpa/guidance/NavigationManager$LaneInformationListener;

    .prologue
    .line 258
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 259
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v0, p1}, Lcom/here/android/mpa/guidance/NavigationManager;->removeLaneInformationListener(Lcom/here/android/mpa/guidance/NavigationManager$LaneInformationListener;)V

    .line 260
    return-void
.end method

.method public removeRealisticViewListener(Lcom/here/android/mpa/guidance/NavigationManager$RealisticViewListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/here/android/mpa/guidance/NavigationManager$RealisticViewListener;

    .prologue
    .line 279
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 280
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v0, p1}, Lcom/here/android/mpa/guidance/NavigationManager;->removeRealisticViewListener(Lcom/here/android/mpa/guidance/NavigationManager$RealisticViewListener;)V

    .line 281
    return-void
.end method

.method public removeTrafficRerouteListener(Lcom/here/android/mpa/guidance/NavigationManager$TrafficRerouteListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/here/android/mpa/guidance/NavigationManager$TrafficRerouteListener;

    .prologue
    .line 294
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 295
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v0, p1}, Lcom/here/android/mpa/guidance/NavigationManager;->removeTrafficRerouteListener(Lcom/here/android/mpa/guidance/NavigationManager$TrafficRerouteListener;)V

    .line 296
    return-void
.end method

.method public declared-synchronized resume()Lcom/here/android/mpa/guidance/NavigationManager$Error;
    .locals 1

    .prologue
    .line 199
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 200
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v0}, Lcom/here/android/mpa/guidance/NavigationManager;->resume()Lcom/here/android/mpa/guidance/NavigationManager$Error;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 199
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setDistanceUnit(Lcom/here/android/mpa/guidance/NavigationManager$UnitSystem;)V
    .locals 1
    .param p1, "unitSystem"    # Lcom/here/android/mpa/guidance/NavigationManager$UnitSystem;

    .prologue
    .line 299
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 300
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v0, p1}, Lcom/here/android/mpa/guidance/NavigationManager;->setDistanceUnit(Lcom/here/android/mpa/guidance/NavigationManager$UnitSystem;)Lcom/here/android/mpa/guidance/NavigationManager$Error;

    .line 301
    return-void
.end method

.method public setRealisticViewMode(Lcom/here/android/mpa/guidance/NavigationManager$RealisticViewMode;)V
    .locals 1
    .param p1, "mode"    # Lcom/here/android/mpa/guidance/NavigationManager$RealisticViewMode;

    .prologue
    .line 264
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 265
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v0, p1}, Lcom/here/android/mpa/guidance/NavigationManager;->setRealisticViewMode(Lcom/here/android/mpa/guidance/NavigationManager$RealisticViewMode;)V

    .line 266
    return-void
.end method

.method public setRoute(Lcom/here/android/mpa/routing/Route;)Lcom/here/android/mpa/guidance/NavigationManager$Error;
    .locals 1
    .param p1, "route"    # Lcom/here/android/mpa/routing/Route;

    .prologue
    .line 284
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 285
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v0, p1}, Lcom/here/android/mpa/guidance/NavigationManager;->setRoute(Lcom/here/android/mpa/routing/Route;)Lcom/here/android/mpa/guidance/NavigationManager$Error;

    move-result-object v0

    return-object v0
.end method

.method public setTrafficAvoidanceMode(Lcom/here/android/mpa/guidance/NavigationManager$TrafficAvoidanceMode;)V
    .locals 1
    .param p1, "mode"    # Lcom/here/android/mpa/guidance/NavigationManager$TrafficAvoidanceMode;

    .prologue
    .line 324
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 325
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v0, p1}, Lcom/here/android/mpa/guidance/NavigationManager;->setTrafficAvoidanceMode(Lcom/here/android/mpa/guidance/NavigationManager$TrafficAvoidanceMode;)Lcom/here/android/mpa/guidance/NavigationManager$Error;

    .line 326
    return-void
.end method

.method public declared-synchronized startNavigation(Lcom/here/android/mpa/routing/Route;I)Lcom/here/android/mpa/guidance/NavigationManager$Error;
    .locals 16
    .param p1, "route"    # Lcom/here/android/mpa/routing/Route;
    .param p2, "simulationSpeed"    # I

    .prologue
    .line 103
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 104
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereNavController;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "startNavigation state["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/maps/here/HereNavController;->state:Lcom/navdy/hud/app/maps/here/HereNavController$State;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "]"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v3, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 105
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereNavController;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "startNavigation simulationSpeed:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " route-id="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static/range {p1 .. p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v3, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 107
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereNavController;->state:Lcom/navdy/hud/app/maps/here/HereNavController$State;

    sget-object v12, Lcom/navdy/hud/app/maps/here/HereNavController$State;->NAVIGATING:Lcom/navdy/hud/app/maps/here/HereNavController$State;

    if-ne v3, v12, :cond_0

    .line 108
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereNavController;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v12, "startNavigation stop existing nav"

    invoke-virtual {v3, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 109
    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/maps/here/HereNavController;->stopNavigation(Z)V

    .line 112
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    .line 114
    .local v10, "l1":J
    if-nez p2, :cond_1

    .line 115
    invoke-static {}, Lcom/navdy/hud/app/maps/MapSettings;->getSimulationSpeed()I

    move-result p2

    .line 117
    :cond_1
    if-lez p2, :cond_2

    .line 118
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereNavController;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    move/from16 v0, p2

    int-to-long v12, v0

    move-object/from16 v0, p1

    invoke-virtual {v3, v0, v12, v13}, Lcom/here/android/mpa/guidance/NavigationManager;->simulate(Lcom/here/android/mpa/routing/Route;J)Lcom/here/android/mpa/guidance/NavigationManager$Error;

    move-result-object v2

    .line 122
    .local v2, "error":Lcom/here/android/mpa/guidance/NavigationManager$Error;
    :goto_0
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereNavController;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "startNavigation took ["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    sub-long/2addr v14, v10

    invoke-virtual {v12, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "]"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v3, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 124
    sget-object v3, Lcom/here/android/mpa/guidance/NavigationManager$Error;->NONE:Lcom/here/android/mpa/guidance/NavigationManager$Error;

    if-ne v2, v3, :cond_3

    .line 125
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereNavController$State;->NAVIGATING:Lcom/navdy/hud/app/maps/here/HereNavController$State;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/navdy/hud/app/maps/here/HereNavController;->state:Lcom/navdy/hud/app/maps/here/HereNavController$State;

    .line 126
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/maps/here/HereNavController;->route:Lcom/here/android/mpa/routing/Route;

    .line 127
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereNavController;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "startNavigation: success ["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/maps/here/HereNavController;->state:Lcom/navdy/hud/app/maps/here/HereNavController$State;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "] route-id="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static/range {p1 .. p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v3, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 128
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/maps/here/HereNavController;->endTrip()V

    .line 129
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/maps/here/HereNavController;->startTrip()V

    .line 131
    sget-object v3, Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;->OPTIMAL:Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    const v12, 0xfffffff

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, Lcom/here/android/mpa/routing/Route;->getTta(Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;I)Lcom/here/android/mpa/routing/RouteTta;

    move-result-object v3

    invoke-virtual {v3}, Lcom/here/android/mpa/routing/RouteTta;->getDuration()I

    move-result v3

    int-to-long v4, v3

    .line 132
    .local v4, "durationWithTraffic":J
    sget-object v3, Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;->DISABLED:Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    const v12, 0xfffffff

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, Lcom/here/android/mpa/routing/Route;->getTta(Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;I)Lcom/here/android/mpa/routing/RouteTta;

    move-result-object v3

    invoke-virtual {v3}, Lcom/here/android/mpa/routing/RouteTta;->getDuration()I

    move-result v3

    int-to-long v6, v3

    .line 133
    .local v6, "duration":J
    invoke-virtual/range {p1 .. p1}, Lcom/here/android/mpa/routing/Route;->getLength()I

    move-result v3

    int-to-long v8, v3

    .line 135
    .local v8, "distance":J
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereNavController;->navigationQualityTracker:Lcom/navdy/hud/app/analytics/NavigationQualityTracker;

    invoke-virtual/range {v3 .. v9}, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->trackTripStarted(JJJ)V

    .line 136
    const/4 v3, 0x1

    invoke-static {v3}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordNavigation(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 140
    .end local v4    # "durationWithTraffic":J
    .end local v6    # "duration":J
    .end local v8    # "distance":J
    :goto_1
    monitor-exit p0

    return-object v2

    .line 120
    .end local v2    # "error":Lcom/here/android/mpa/guidance/NavigationManager$Error;
    :cond_2
    :try_start_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereNavController;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/here/android/mpa/guidance/NavigationManager;->startNavigation(Lcom/here/android/mpa/routing/Route;)Lcom/here/android/mpa/guidance/NavigationManager$Error;

    move-result-object v2

    .restart local v2    # "error":Lcom/here/android/mpa/guidance/NavigationManager$Error;
    goto/16 :goto_0

    .line 138
    :cond_3
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereNavController;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "startNavigation error ["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/maps/here/HereNavController;->state:Lcom/navdy/hud/app/maps/here/HereNavController$State;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "] "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v2}, Lcom/here/android/mpa/guidance/NavigationManager$Error;->name()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v3, v12}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 103
    .end local v2    # "error":Lcom/here/android/mpa/guidance/NavigationManager$Error;
    .end local v10    # "l1":J
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public declared-synchronized stopNavigation(Z)V
    .locals 8
    .param p1, "sendTripEvent"    # Z

    .prologue
    const/4 v1, 0x0

    .line 144
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 145
    sget-object v4, Lcom/navdy/hud/app/maps/here/HereNavController;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "stopNavigation state["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->state:Lcom/navdy/hud/app/maps/here/HereNavController$State;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 147
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->state:Lcom/navdy/hud/app/maps/here/HereNavController$State;

    sget-object v5, Lcom/navdy/hud/app/maps/here/HereNavController$State;->NAVIGATING:Lcom/navdy/hud/app/maps/here/HereNavController$State;

    if-ne v4, v5, :cond_3

    .line 148
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 149
    .local v2, "l1":J
    sget-object v4, Lcom/navdy/hud/app/maps/here/HereNavController;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "stopNavigation:stopping nav state["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->state:Lcom/navdy/hud/app/maps/here/HereNavController$State;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ] route-id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->route:Lcom/here/android/mpa/routing/Route;

    invoke-static {v6}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 150
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v4}, Lcom/here/android/mpa/guidance/NavigationManager;->getNavigationMode()Lcom/here/android/mpa/guidance/NavigationManager$NavigationMode;

    move-result-object v4

    sget-object v5, Lcom/here/android/mpa/guidance/NavigationManager$NavigationMode;->SIMULATION:Lcom/here/android/mpa/guidance/NavigationManager$NavigationMode;

    if-ne v4, v5, :cond_0

    const/4 v1, 0x1

    .line 151
    .local v1, "resetMapAnimator":Z
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereNavController;->expireGlympseTickets()V

    .line 152
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v4}, Lcom/here/android/mpa/guidance/NavigationManager;->stop()V

    .line 153
    if-eqz v1, :cond_1

    .line 154
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->handler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->clearAnimatorState:Ljava/lang/Runnable;

    const-wide/16 v6, 0x7d0

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 156
    :cond_1
    sget-object v4, Lcom/navdy/hud/app/maps/here/HereNavController;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "stopNavigation:stopped nav state["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->state:Lcom/navdy/hud/app/maps/here/HereNavController$State;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ] took ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long/2addr v6, v2

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 158
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v4}, Lcom/here/android/mpa/guidance/NavigationManager;->startTracking()Lcom/here/android/mpa/guidance/NavigationManager$Error;

    move-result-object v0

    .line 159
    .local v0, "error":Lcom/here/android/mpa/guidance/NavigationManager$Error;
    sget-object v4, Lcom/navdy/hud/app/maps/here/HereNavController;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "stopNavigation: tracking error state ="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 160
    sget-object v4, Lcom/navdy/hud/app/maps/here/HereNavController$State;->TRACKING:Lcom/navdy/hud/app/maps/here/HereNavController$State;

    iput-object v4, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->state:Lcom/navdy/hud/app/maps/here/HereNavController$State;

    .line 162
    const/4 v4, 0x0

    invoke-static {v4}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordNavigation(Z)V

    .line 163
    if-eqz p1, :cond_2

    .line 164
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereNavController;->endTrip()V

    .line 165
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereNavController;->startTrip()V

    .line 168
    :cond_2
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->route:Lcom/here/android/mpa/routing/Route;

    .line 169
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->navigationQualityTracker:Lcom/navdy/hud/app/analytics/NavigationQualityTracker;

    invoke-virtual {v4}, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->cancelTrip()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 173
    .end local v0    # "error":Lcom/here/android/mpa/guidance/NavigationManager$Error;
    .end local v1    # "resetMapAnimator":Z
    .end local v2    # "l1":J
    :goto_0
    monitor-exit p0

    return-void

    .line 171
    :cond_3
    :try_start_1
    sget-object v4, Lcom/navdy/hud/app/maps/here/HereNavController;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "stopNavigation: already "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereNavController;->state:Lcom/navdy/hud/app/maps/here/HereNavController$State;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 144
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method
