.class final Lcom/navdy/hud/app/maps/here/HerePlacesManager$7;
.super Ljava/lang/Object;
.source "HerePlacesManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HerePlacesManager;->handleCategoriesRequest(Lcom/here/android/mpa/search/CategoryFilter;ILcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$filter:Lcom/here/android/mpa/search/CategoryFilter;

.field final synthetic val$listener:Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;

.field final synthetic val$nResults:I

.field final synthetic val$offline:Z


# direct methods
.method constructor <init>(Lcom/here/android/mpa/search/CategoryFilter;ILcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;Z)V
    .locals 0

    .prologue
    .line 393
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7;->val$filter:Lcom/here/android/mpa/search/CategoryFilter;

    iput p2, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7;->val$nResults:I

    iput-object p3, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7;->val$listener:Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;

    iput-boolean p4, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7;->val$offline:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    .line 396
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 398
    .local v4, "l1":J
    iget-object v9, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7;->val$filter:Lcom/here/android/mpa/search/CategoryFilter;

    if-eqz v9, :cond_0

    iget v9, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7;->val$nResults:I

    if-lez v9, :cond_0

    iget-object v9, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7;->val$listener:Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;

    if-nez v9, :cond_1

    .line 399
    :cond_0
    new-instance v9, Ljava/lang/IllegalArgumentException;

    invoke-direct {v9}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v9

    .line 403
    :cond_1
    :try_start_0
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$100()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v9

    if-nez v9, :cond_2

    .line 404
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v9

    const-string v10, "engine not ready"

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 405
    iget-object v9, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7;->val$listener:Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;

    sget-object v10, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;->NO_MAP_ENGINE:Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

    invoke-interface {v9, v10}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;->onError(Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 503
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    .line 504
    .local v6, "l2":J
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "handleCategoriesRequest time-1="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sub-long v12, v6, v4

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 506
    :goto_0
    return-void

    .line 409
    .end local v6    # "l2":J
    :cond_2
    :try_start_1
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$100()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v2

    .line 410
    .local v2, "hereLocationFixManager":Lcom/navdy/hud/app/maps/here/HereLocationFixManager;
    if-nez v2, :cond_3

    .line 411
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v9

    const-string v10, "engine not ready, location fix manager n/a"

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 412
    iget-object v9, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7;->val$listener:Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;

    sget-object v10, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;->NO_USER_LOCATION:Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

    invoke-interface {v9, v10}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;->onError(Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 503
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    .line 504
    .restart local v6    # "l2":J
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "handleCategoriesRequest time-1="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sub-long v12, v6, v4

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 416
    .end local v6    # "l2":J
    :cond_3
    :try_start_2
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$100()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getRouteStartPoint()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v1

    .line 417
    .local v1, "geoPosition":Lcom/here/android/mpa/common/GeoCoordinate;
    if-nez v1, :cond_4

    .line 418
    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v1

    .line 423
    :goto_1
    if-nez v1, :cond_5

    .line 424
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v9

    const-string v10, "start location n/a"

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 425
    iget-object v9, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7;->val$listener:Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;

    sget-object v10, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;->NO_USER_LOCATION:Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

    invoke-interface {v9, v10}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;->onError(Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 503
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    .line 504
    .restart local v6    # "l2":J
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "handleCategoriesRequest time-1="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sub-long v12, v6, v4

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 420
    .end local v6    # "l2":J
    :cond_4
    :try_start_3
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "using debug start point:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 498
    .end local v1    # "geoPosition":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v2    # "hereLocationFixManager":Lcom/navdy/hud/app/maps/here/HereLocationFixManager;
    :catch_0
    move-exception v8

    .line 499
    .local v8, "t":Ljava/lang/Throwable;
    :try_start_4
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v9

    const-string v10, "HERE internal DiscoveryRequest.execute exception: "

    invoke-virtual {v9, v10, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 500
    # invokes: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->tryRestablishOnline()V
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$1000()V

    .line 501
    iget-object v9, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7;->val$listener:Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;

    sget-object v10, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;->UNKNOWN_ERROR:Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

    invoke-interface {v9, v10}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;->onError(Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 503
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    .line 504
    .restart local v6    # "l2":J
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "handleCategoriesRequest time-1="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sub-long v12, v6, v4

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 429
    .end local v6    # "l2":J
    .end local v8    # "t":Ljava/lang/Throwable;
    .restart local v1    # "geoPosition":Lcom/here/android/mpa/common/GeoCoordinate;
    .restart local v2    # "hereLocationFixManager":Lcom/navdy/hud/app/maps/here/HereLocationFixManager;
    :cond_5
    :try_start_5
    iget-boolean v9, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7;->val$offline:Z

    if-eqz v9, :cond_8

    .line 430
    # invokes: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->establishOffline()V
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$900()V

    .line 436
    :cond_6
    new-instance v9, Lcom/here/android/mpa/search/ExploreRequest;

    invoke-direct {v9}, Lcom/here/android/mpa/search/ExploreRequest;-><init>()V

    iget-object v10, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7;->val$filter:Lcom/here/android/mpa/search/CategoryFilter;

    .line 437
    invoke-virtual {v9, v10}, Lcom/here/android/mpa/search/ExploreRequest;->setCategoryFilter(Lcom/here/android/mpa/search/CategoryFilter;)Lcom/here/android/mpa/search/ExploreRequest;

    move-result-object v9

    .line 438
    invoke-virtual {v9, v1}, Lcom/here/android/mpa/search/ExploreRequest;->setSearchCenter(Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/here/android/mpa/search/ExploreRequest;

    move-result-object v9

    iget v10, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7;->val$nResults:I

    .line 439
    invoke-virtual {v9, v10}, Lcom/here/android/mpa/search/ExploreRequest;->setCollectionSize(I)Lcom/here/android/mpa/search/DiscoveryRequest;

    move-result-object v3

    .line 441
    .local v3, "request":Lcom/here/android/mpa/search/DiscoveryRequest;
    new-instance v9, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1;

    invoke-direct {v9, p0, v4, v5}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7$1;-><init>(Lcom/navdy/hud/app/maps/here/HerePlacesManager$7;J)V

    invoke-virtual {v3, v9}, Lcom/here/android/mpa/search/DiscoveryRequest;->execute(Lcom/here/android/mpa/search/ResultListener;)Lcom/here/android/mpa/search/ErrorCode;

    move-result-object v0

    .line 493
    .local v0, "error":Lcom/here/android/mpa/search/ErrorCode;
    sget-object v9, Lcom/here/android/mpa/search/ErrorCode;->NONE:Lcom/here/android/mpa/search/ErrorCode;

    if-eq v0, v9, :cond_7

    .line 494
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Error while requesting nearby gas stations: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v0}, Lcom/here/android/mpa/search/ErrorCode;->name()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 495
    # invokes: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->tryRestablishOnline()V
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$1000()V

    .line 496
    iget-object v9, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7;->val$listener:Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;

    sget-object v10, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;->BAD_REQUEST:Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

    invoke-interface {v9, v10}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;->onError(Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 503
    :cond_7
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    .line 504
    .restart local v6    # "l2":J
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "handleCategoriesRequest time-1="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sub-long v12, v6, v4

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 431
    .end local v0    # "error":Lcom/here/android/mpa/search/ErrorCode;
    .end local v3    # "request":Lcom/here/android/mpa/search/DiscoveryRequest;
    .end local v6    # "l2":J
    :cond_8
    :try_start_6
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isEngineOnline()Z

    move-result v9

    if-nez v9, :cond_6

    .line 432
    iget-object v9, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$7;->val$listener:Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;

    sget-object v10, Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;->BAD_REQUEST:Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;

    invoke-interface {v9, v10}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$OnCategoriesSearchListener;->onError(Lcom/navdy/hud/app/maps/here/HerePlacesManager$Error;)V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 503
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    .line 504
    .restart local v6    # "l2":J
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "handleCategoriesRequest time-1="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sub-long v12, v6, v4

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 503
    .end local v1    # "geoPosition":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v2    # "hereLocationFixManager":Lcom/navdy/hud/app/maps/here/HereLocationFixManager;
    .end local v6    # "l2":J
    :catchall_0
    move-exception v9

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    .line 504
    .restart local v6    # "l2":J
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "handleCategoriesRequest time-1="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sub-long v12, v6, v4

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 505
    throw v9
.end method
