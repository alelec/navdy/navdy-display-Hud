.class public final Lcom/navdy/hud/app/maps/here/HereMapCameraManager;
.super Ljava/lang/Object;
.source "HereMapCameraManager.java"


# static fields
.field public static final DEFAULT_TILT:F = 60.0f

.field public static final DEFAULT_ZOOM_LEVEL:D = 16.5

.field private static final EASE_OUT_DYNAMIC_ZOOM_TILT_INTERVAL:I = 0x2710

.field private static final HIGH_SPEED_THRESHOLD:D = 29.0576

.field private static final HIGH_SPEED_TILT:F = 70.0f

.field private static final HIGH_SPEED_ZOOM:D = 15.25

.field private static final LOW_SPEED_THRESHOLD:D = 8.9408

.field private static final LOW_SPEED_TILT:F = 60.0f

.field private static final LOW_SPEED_ZOOM:D = 16.5

.field private static final MAX_ZOOM_LEVEL_DIAL:D = 16.5

.field private static final MEDIUM_SPEED_THRESHOLD:D = 20.1168

.field private static final MEDIUM_SPEED_ZOOM:D = 16.0

.field private static final MIN_SPEED_DIFFERENCE:D = 1.5

.field private static final MIN_TILT_DIFFERENCE_TRIGGER:F = 5.0f

.field private static final MIN_ZOOM_DIFFERENCE_TRIGGER:D = 0.25

.field public static final MIN_ZOOM_LEVEL_DIAL:D = 12.0

.field private static final MPH_TO_MS:D = 0.44704

.field private static final N_ZOOM_TILT_ANIMATION_STEPS:I = 0x5

.field private static final OVERVIEW_MAP_ZOOM_LEVEL:F = 10000.0f

.field private static final VERY_HIGH_SPEED_THRESHOLD:D = 33.528

.field private static final VERY_HIGH_SPEED_ZOOM:D = 14.75

.field private static final ZOOM_LOCK_DURATION:J = 0x2710L

.field private static final ZOOM_STEP:D = 0.25

.field private static final ZOOM_TILT_ANIMATION_INTERVAL:I = 0xc8

.field private static sInstance:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private volatile animationOn:Z

.field private currentDynamicTilt:F

.field private currentDynamicZoom:D

.field private currentGeoCoordinate:Lcom/here/android/mpa/common/GeoCoordinate;

.field private currentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

.field private easeOutDynamicZoomTilt:Ljava/lang/Runnable;

.field private easeOutDynamicZoomTiltBk:Ljava/lang/Runnable;

.field private volatile goBackfromOverviewOn:Z

.field private volatile goToOverviewOn:Z

.field private final handler:Landroid/os/Handler;

.field private hereMapAnimator:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

.field private final hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

.field private homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

.field private init:Z

.field private initialZoom:Z

.field private isFullAnimationSettingEnabled:Z

.field private lastDynamicZoomTime:J

.field private lastRecordedSpeed:D

.field private localPreferences:Lcom/navdy/service/library/events/preferences/LocalPreferences;

.field private mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

.field private mapUIReady:Z

.field private volatile nTimesAnimated:I

.field private navigationView:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

.field private speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

.field private unlockZoom:Ljava/lang/Runnable;

.field private volatile zoomActionOn:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 34
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 76
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    invoke-direct {v0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sInstance:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 182
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$1;-><init>(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->unlockZoom:Ljava/lang/Runnable;

    .line 148
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$2;-><init>(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->easeOutDynamicZoomTilt:Ljava/lang/Runnable;

    .line 155
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$3;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$3;-><init>(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->easeOutDynamicZoomTiltBk:Ljava/lang/Runnable;

    .line 183
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->handler:Landroid/os/Handler;

    .line 184
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    .line 185
    invoke-static {}, Lcom/navdy/hud/app/manager/SpeedManager;->getInstance()Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    .line 186
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->easeOutDynamicZoomTilt:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)F
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    .prologue
    .line 32
    iget v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentDynamicTilt:F

    return v0
.end method

.method static synthetic access$1102(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;J)J
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapCameraManager;
    .param p1, "x1"    # J

    .prologue
    .line 32
    iput-wide p1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->lastDynamicZoomTime:J

    return-wide p1
.end method

.method static synthetic access$1200(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->animateCamera()V

    return-void
.end method

.method static synthetic access$1300(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Lcom/here/android/mpa/common/GeoPosition;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->animationOn:Z

    return v0
.end method

.method static synthetic access$1402(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapCameraManager;
    .param p1, "x1"    # Z

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->animationOn:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->goBackfromOverviewOn:Z

    return v0
.end method

.method static synthetic access$1502(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapCameraManager;
    .param p1, "x1"    # Z

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->goBackfromOverviewOn:Z

    return p1
.end method

.method static synthetic access$1600(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->goToOverviewOn:Z

    return v0
.end method

.method static synthetic access$1602(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapCameraManager;
    .param p1, "x1"    # Z

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->goToOverviewOn:Z

    return p1
.end method

.method static synthetic access$1700(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->unlockZoom:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;Ljava/lang/Runnable;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapCameraManager;
    .param p1, "x1"    # Ljava/lang/Runnable;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->goBackFromOverview(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic access$1900(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->isFullAnimationSettingEnabled:Z

    return v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->zoomActionOn:Z

    return v0
.end method

.method static synthetic access$2000(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Lcom/navdy/hud/app/maps/here/HereMapAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->hereMapAnimator:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    return-object v0
.end method

.method static synthetic access$202(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapCameraManager;
    .param p1, "x1"    # Z

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->zoomActionOn:Z

    return p1
.end method

.method static synthetic access$2100(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;Lcom/here/android/mpa/common/GeoCoordinate;Ljava/lang/Runnable;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapCameraManager;
    .param p1, "x1"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p2, "x2"    # Ljava/lang/Runnable;

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->goToOverview(Lcom/here/android/mpa/common/GeoCoordinate;Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic access$2200(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    .prologue
    .line 32
    iget v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->nTimesAnimated:I

    return v0
.end method

.method static synthetic access$2202(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapCameraManager;
    .param p1, "x1"    # I

    .prologue
    .line 32
    iput p1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->nTimesAnimated:I

    return p1
.end method

.method static synthetic access$2208(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)I
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    .prologue
    .line 32
    iget v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->nTimesAnimated:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->nTimesAnimated:I

    return v0
.end method

.method static synthetic access$2300(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;DF)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapCameraManager;
    .param p1, "x1"    # D
    .param p3, "x2"    # F

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->getAnimateCameraRunnable(DF)Ljava/lang/Runnable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Lcom/navdy/hud/app/maps/here/HereMapController;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    return-object v0
.end method

.method static synthetic access$400()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->zoomToDefault()V

    return-void
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->isOverViewMapVisible()Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Lcom/navdy/service/library/events/preferences/LocalPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->localPreferences:Lcom/navdy/service/library/events/preferences/LocalPreferences;

    return-object v0
.end method

.method static synthetic access$702(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;Lcom/navdy/service/library/events/preferences/LocalPreferences;)Lcom/navdy/service/library/events/preferences/LocalPreferences;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapCameraManager;
    .param p1, "x1"    # Lcom/navdy/service/library/events/preferences/LocalPreferences;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->localPreferences:Lcom/navdy/service/library/events/preferences/LocalPreferences;

    return-object p1
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->easeOutDynamicZoomTiltBk:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)D
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    .prologue
    .line 32
    iget-wide v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentDynamicZoom:D

    return-wide v0
.end method

.method private animateCamera()V
    .locals 3

    .prologue
    .line 514
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$5;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$5;-><init>(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)V

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 529
    return-void
.end method

.method private getAnimateCameraRunnable(DF)Ljava/lang/Runnable;
    .locals 1
    .param p1, "zoomStep"    # D
    .param p3, "tiltStep"    # F

    .prologue
    .line 642
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$8;-><init>(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;DF)V

    return-object v0
.end method

.method private getDynamicTilt(D)F
    .locals 13
    .param p1, "speed"    # D

    .prologue
    const-wide v6, 0x403d0ebedfa43fe6L    # 29.0576

    const-wide v4, 0x4021e1b089a02752L    # 8.9408

    .line 576
    cmpg-double v1, p1, v4

    if-gtz v1, :cond_0

    .line 577
    const/high16 v0, 0x42700000    # 60.0f

    .line 584
    .local v0, "tilt":F
    :goto_0
    return v0

    .line 578
    .end local v0    # "tilt":F
    :cond_0
    cmpl-double v1, p1, v4

    if-lez v1, :cond_1

    cmpg-double v1, p1, v6

    if-gtz v1, :cond_1

    .line 579
    const-wide/high16 v8, 0x404e000000000000L    # 60.0

    const-wide v10, 0x4051800000000000L    # 70.0

    move-object v1, p0

    move-wide v2, p1

    invoke-direct/range {v1 .. v11}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->getLinearMapSection(DDDDD)D

    move-result-wide v2

    double-to-float v0, v2

    .restart local v0    # "tilt":F
    goto :goto_0

    .line 581
    .end local v0    # "tilt":F
    :cond_1
    const/high16 v0, 0x428c0000    # 70.0f

    .restart local v0    # "tilt":F
    goto :goto_0
.end method

.method private getDynamicZoom(D)D
    .locals 15
    .param p1, "speed"    # D

    .prologue
    .line 559
    const-wide v0, 0x4021e1b089a02752L    # 8.9408

    cmpg-double v0, p1, v0

    if-gtz v0, :cond_0

    .line 560
    const-wide v12, 0x4030800000000000L    # 16.5

    .line 571
    .local v12, "zoomLevel":D
    :goto_0
    return-wide v12

    .line 561
    .end local v12    # "zoomLevel":D
    :cond_0
    const-wide v0, 0x4021e1b089a02752L    # 8.9408

    cmpl-double v0, p1, v0

    if-lez v0, :cond_1

    const-wide v0, 0x40341de69ad42c3dL    # 20.1168

    cmpg-double v0, p1, v0

    if-gtz v0, :cond_1

    .line 562
    const-wide v4, 0x4021e1b089a02752L    # 8.9408

    const-wide v6, 0x40341de69ad42c3dL    # 20.1168

    const-wide v8, 0x4030800000000000L    # 16.5

    const-wide/high16 v10, 0x4030000000000000L    # 16.0

    move-object v1, p0

    move-wide/from16 v2, p1

    invoke-direct/range {v1 .. v11}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->getLinearMapSection(DDDDD)D

    move-result-wide v12

    .restart local v12    # "zoomLevel":D
    goto :goto_0

    .line 563
    .end local v12    # "zoomLevel":D
    :cond_1
    const-wide v0, 0x40341de69ad42c3dL    # 20.1168

    cmpl-double v0, p1, v0

    if-lez v0, :cond_2

    const-wide v0, 0x403d0ebedfa43fe6L    # 29.0576

    cmpg-double v0, p1, v0

    if-gtz v0, :cond_2

    .line 564
    const-wide v4, 0x40341de69ad42c3dL    # 20.1168

    const-wide v6, 0x403d0ebedfa43fe6L    # 29.0576

    const-wide/high16 v8, 0x4030000000000000L    # 16.0

    const-wide v10, 0x402e800000000000L    # 15.25

    move-object v1, p0

    move-wide/from16 v2, p1

    invoke-direct/range {v1 .. v11}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->getLinearMapSection(DDDDD)D

    move-result-wide v12

    .restart local v12    # "zoomLevel":D
    goto :goto_0

    .line 565
    .end local v12    # "zoomLevel":D
    :cond_2
    const-wide v0, 0x403d0ebedfa43fe6L    # 29.0576

    cmpl-double v0, p1, v0

    if-lez v0, :cond_3

    const-wide v0, 0x4040c395810624ddL    # 33.528

    cmpg-double v0, p1, v0

    if-gtz v0, :cond_3

    .line 566
    const-wide v4, 0x403d0ebedfa43fe6L    # 29.0576

    const-wide v6, 0x4040c395810624ddL    # 33.528

    const-wide v8, 0x402e800000000000L    # 15.25

    const-wide v10, 0x402d800000000000L    # 14.75

    move-object v1, p0

    move-wide/from16 v2, p1

    invoke-direct/range {v1 .. v11}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->getLinearMapSection(DDDDD)D

    move-result-wide v12

    .restart local v12    # "zoomLevel":D
    goto/16 :goto_0

    .line 568
    .end local v12    # "zoomLevel":D
    :cond_3
    const-wide v12, 0x402d800000000000L    # 14.75

    .restart local v12    # "zoomLevel":D
    goto/16 :goto_0
.end method

.method public static getInstance()Lcom/navdy/hud/app/maps/here/HereMapCameraManager;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sInstance:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    return-object v0
.end method

.method private getLinearMapSection(DDDDD)D
    .locals 5
    .param p1, "x"    # D
    .param p3, "xStart"    # D
    .param p5, "xEnd"    # D
    .param p7, "yStart"    # D
    .param p9, "yEnd"    # D

    .prologue
    .line 588
    sub-double v0, p1, p3

    sub-double v2, p9, p7

    mul-double/2addr v0, v2

    sub-double v2, p5, p3

    div-double/2addr v0, v2

    add-double/2addr v0, p7

    return-wide v0
.end method

.method private getNavigationView()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 626
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->navigationView:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    if-eqz v2, :cond_1

    .line 634
    :cond_0
    :goto_0
    return v1

    .line 630
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v0

    .line 631
    .local v0, "uiStateManager":Lcom/navdy/hud/app/ui/framework/UIStateManager;
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getNavigationView()Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->navigationView:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    .line 632
    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getHomescreenView()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    .line 634
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->navigationView:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    if-nez v2, :cond_0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method private goBackFromOverview(Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "endAction"    # Ljava/lang/Runnable;

    .prologue
    .line 543
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->getNavigationView()Z

    move-result v0

    if-nez v0, :cond_0

    .line 555
    :goto_0
    return-void

    .line 547
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->navigationView:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$6;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$6;-><init>(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->animateBackfromOverview(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private goToOverview(Lcom/here/android/mpa/common/GeoCoordinate;Ljava/lang/Runnable;)V
    .locals 6
    .param p1, "coordinate"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p2, "endAction"    # Ljava/lang/Runnable;

    .prologue
    const/4 v2, 0x0

    .line 532
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->getNavigationView()Z

    move-result v0

    if-nez v0, :cond_0

    .line 540
    :goto_0
    return-void

    .line 535
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentRoute()Lcom/here/android/mpa/routing/Route;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 536
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->navigationView:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentRoute()Lcom/here/android/mpa/routing/Route;

    move-result-object v2

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentMapRoute()Lcom/here/android/mpa/mapping/MapRoute;

    move-result-object v3

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hasArrived()Z

    move-result v5

    move-object v1, p1

    move-object v4, p2

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->animateToOverview(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/routing/Route;Lcom/here/android/mpa/mapping/MapRoute;Ljava/lang/Runnable;Z)V

    goto :goto_0

    .line 538
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->navigationView:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    const/4 v5, 0x0

    move-object v1, p1

    move-object v3, v2

    move-object v4, p2

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->animateToOverview(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/routing/Route;Lcom/here/android/mpa/mapping/MapRoute;Ljava/lang/Runnable;Z)V

    goto :goto_0
.end method

.method private isOverViewMapVisible()Z
    .locals 1

    .prologue
    .line 638
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->getNavigationView()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->navigationView:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->isOverviewMapMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setInitialZoom()V
    .locals 4

    .prologue
    .line 498
    iget-boolean v1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->initialZoom:Z

    if-nez v1, :cond_0

    .line 499
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setInitialZoom:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 500
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

    if-nez v1, :cond_2

    .line 501
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v0

    .line 502
    .local v0, "coordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    if-nez v0, :cond_1

    .line 511
    .end local v0    # "coordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    :cond_0
    :goto_0
    return-void

    .line 505
    .restart local v0    # "coordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    :cond_1
    new-instance v1, Lcom/here/android/mpa/common/GeoPosition;

    invoke-direct {v1, v0}, Lcom/here/android/mpa/common/GeoPosition;-><init>(Lcom/here/android/mpa/common/GeoCoordinate;)V

    iput-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

    .line 506
    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentGeoCoordinate:Lcom/here/android/mpa/common/GeoCoordinate;

    .line 508
    .end local v0    # "coordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    :cond_2
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->setZoom()Z

    move-result v1

    iput-boolean v1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->initialZoom:Z

    .line 509
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setInitialZoom:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->initialZoom:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private zoomToDefault()V
    .locals 2

    .prologue
    .line 592
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->init:Z

    if-nez v0, :cond_0

    .line 615
    :goto_0
    return-void

    .line 595
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->isManualZoom()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 596
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "zoomToDefault: manual zoom enabled"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 600
    :cond_1
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->getNavigationView()Z

    move-result v0

    if-nez v0, :cond_2

    .line 601
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "zoomToDefault: no view"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 605
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->navigationView:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    invoke-virtual {v0}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->isOverviewMapMode()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 606
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$7;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$7;-><init>(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)V

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->goBackFromOverview(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 613
    :cond_3
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->animateCamera()V

    goto :goto_0
.end method


# virtual methods
.method public getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;
    .locals 1

    .prologue
    .line 687
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentGeoCoordinate:Lcom/here/android/mpa/common/GeoCoordinate;

    return-object v0
.end method

.method public getLastGeoPosition()Lcom/here/android/mpa/common/GeoPosition;
    .locals 1

    .prologue
    .line 683
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

    return-object v0
.end method

.method public getLastTilt()F
    .locals 1

    .prologue
    .line 691
    iget v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentDynamicTilt:F

    return v0
.end method

.method public getLastZoom()D
    .locals 2

    .prologue
    .line 695
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->isManualZoom()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 696
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->localPreferences:Lcom/navdy/service/library/events/preferences/LocalPreferences;

    iget-object v0, v0, Lcom/navdy/service/library/events/preferences/LocalPreferences;->manualZoomLevel:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    const v1, 0x461c4000    # 10000.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 697
    const-wide/high16 v0, 0x4028000000000000L    # 12.0

    .line 702
    :goto_0
    return-wide v0

    .line 699
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->localPreferences:Lcom/navdy/service/library/events/preferences/LocalPreferences;

    iget-object v0, v0, Lcom/navdy/service/library/events/preferences/LocalPreferences;->manualZoomLevel:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    float-to-double v0, v0

    goto :goto_0

    .line 702
    :cond_1
    iget-wide v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentDynamicZoom:D

    goto :goto_0
.end method

.method public declared-synchronized initialize(Lcom/navdy/hud/app/maps/here/HereMapController;Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/maps/here/HereMapAnimator;)V
    .locals 4
    .param p1, "mapController"    # Lcom/navdy/hud/app/maps/here/HereMapController;
    .param p2, "bus"    # Lcom/squareup/otto/Bus;
    .param p3, "hereMapAnimator"    # Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    .prologue
    const/4 v1, 0x1

    .line 189
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->init:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_0

    .line 204
    :goto_0
    monitor-exit p0

    return-void

    .line 192
    :cond_0
    :try_start_1
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    .line 193
    iput-object p3, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->hereMapAnimator:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    .line 194
    invoke-virtual {p3}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->getAnimationMode()Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;

    move-result-object v2

    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;->ZOOM_TILT_ANIMATION:Lcom/navdy/hud/app/maps/here/HereMapAnimator$AnimationMode;

    if-ne v2, v3, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->isFullAnimationSettingEnabled:Z

    .line 196
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v0

    .line 197
    .local v0, "profile":Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->getLocalPreferences()Lcom/navdy/service/library/events/preferences/LocalPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->localPreferences:Lcom/navdy/service/library/events/preferences/LocalPreferences;

    .line 198
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "localPreferences ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->getProfileName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->localPreferences:Lcom/navdy/service/library/events/preferences/LocalPreferences;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 199
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->setZoom()Z

    .line 200
    const-wide v2, 0x4030800000000000L    # 16.5

    iput-wide v2, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentDynamicZoom:D

    .line 201
    const/high16 v1, 0x42700000    # 60.0f

    iput v1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentDynamicTilt:F

    .line 202
    invoke-virtual {p2, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 203
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->init:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 189
    .end local v0    # "profile":Lcom/navdy/hud/app/profile/DriverProfile;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 194
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public isManualZoom()Z
    .locals 2

    .prologue
    .line 618
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->localPreferences:Lcom/navdy/service/library/events/preferences/LocalPreferences;

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->localPreferences:Lcom/navdy/service/library/events/preferences/LocalPreferences;

    iget-object v1, v1, Lcom/navdy/service/library/events/preferences/LocalPreferences;->manualZoom:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isOverviewZoomLevel()Z
    .locals 2

    .prologue
    .line 622
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->isManualZoom()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->localPreferences:Lcom/navdy/service/library/events/preferences/LocalPreferences;

    iget-object v0, v0, Lcom/navdy/service/library/events/preferences/LocalPreferences;->manualZoomLevel:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    const v1, 0x461c4000    # 10000.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDialZoom(Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 369
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$4;-><init>(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom;)V

    const/16 v2, 0x10

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 495
    return-void
.end method

.method public onDriverProfileChanged(Lcom/navdy/hud/app/event/DriverProfileChanged;)V
    .locals 4
    .param p1, "event"    # Lcom/navdy/hud/app/event/DriverProfileChanged;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 346
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v0

    .line 347
    .local v0, "profile":Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->getLocalPreferences()Lcom/navdy/service/library/events/preferences/LocalPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->localPreferences:Lcom/navdy/service/library/events/preferences/LocalPreferences;

    .line 348
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "driver changed["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->getProfileName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->localPreferences:Lcom/navdy/service/library/events/preferences/LocalPreferences;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 349
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->setZoom()Z

    .line 350
    return-void
.end method

.method public onGeoPositionChange(Lcom/here/android/mpa/common/GeoPosition;)V
    .locals 18
    .param p1, "geoPosition"    # Lcom/here/android/mpa/common/GeoPosition;

    .prologue
    .line 271
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

    .line 272
    invoke-virtual/range {p1 .. p1}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentGeoCoordinate:Lcom/here/android/mpa/common/GeoCoordinate;

    .line 273
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->localPreferences:Lcom/navdy/service/library/events/preferences/LocalPreferences;

    iget-object v5, v5, Lcom/navdy/service/library/events/preferences/LocalPreferences;->manualZoom:Ljava/lang/Boolean;

    if-eqz v5, :cond_0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->localPreferences:Lcom/navdy/service/library/events/preferences/LocalPreferences;

    iget-object v5, v5, Lcom/navdy/service/library/events/preferences/LocalPreferences;->manualZoom:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-nez v5, :cond_1

    .line 275
    :cond_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    invoke-virtual {v5}, Lcom/navdy/hud/app/manager/SpeedManager;->getObdSpeed()I

    move-result v5

    int-to-double v10, v5

    .line 276
    .local v10, "speed":D
    const-wide/high16 v14, -0x4010000000000000L    # -1.0

    cmpl-double v5, v10, v14

    if-eqz v5, :cond_2

    .line 277
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    invoke-virtual {v5}, Lcom/navdy/hud/app/manager/SpeedManager;->getSpeedUnit()Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    move-result-object v5

    sget-object v14, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->METERS_PER_SECOND:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    invoke-static {v10, v11, v5, v14}, Lcom/navdy/hud/app/manager/SpeedManager;->convert(DLcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;)I

    move-result v5

    int-to-double v10, v5

    .line 282
    :goto_0
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->lastRecordedSpeed:D

    cmpl-double v5, v10, v14

    if-nez v5, :cond_3

    .line 284
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v14, 0x2

    invoke-virtual {v5, v14}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 285
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "speed not changed :"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v5, v14}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 331
    .end local v10    # "speed":D
    :cond_1
    :goto_1
    return-void

    .line 279
    .restart local v10    # "speed":D
    :cond_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

    invoke-virtual {v5}, Lcom/here/android/mpa/common/GeoPosition;->getSpeed()D

    move-result-wide v10

    goto :goto_0

    .line 290
    :cond_3
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->lastRecordedSpeed:D

    .line 291
    .local v6, "lastSpeed":D
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->lastRecordedSpeed:D

    sub-double v14, v10, v14

    invoke-static {v14, v15}, Ljava/lang/Math;->abs(D)D

    move-result-wide v14

    const-wide/high16 v16, 0x3ff8000000000000L    # 1.5

    cmpl-double v5, v14, v16

    if-ltz v5, :cond_5

    const/4 v2, 0x1

    .line 292
    .local v2, "isSignificantSpeedDiff":Z
    :goto_2
    move-object/from16 v0, p0

    iput-wide v10, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->lastRecordedSpeed:D

    .line 294
    if-eqz v2, :cond_1

    .line 295
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->handler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->easeOutDynamicZoomTilt:Ljava/lang/Runnable;

    invoke-virtual {v5, v14}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 296
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v11}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->getDynamicZoom(D)D

    move-result-wide v14

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentDynamicZoom:D

    .line 297
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v11}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->getDynamicTilt(D)F

    move-result v5

    move-object/from16 v0, p0

    iput v5, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentDynamicTilt:F

    .line 299
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->zoomActionOn:Z

    if-nez v5, :cond_1

    .line 304
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/here/HereMapController;->getZoomLevel()D

    move-result-wide v14

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentDynamicZoom:D

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    invoke-static {v14, v15}, Ljava/lang/Math;->abs(D)D

    move-result-wide v14

    const-wide/high16 v16, 0x3fd0000000000000L    # 0.25

    cmpl-double v5, v14, v16

    if-ltz v5, :cond_6

    const/4 v4, 0x1

    .line 305
    .local v4, "isSignificantZoomDiff":Z
    :goto_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/here/HereMapController;->getTilt()F

    move-result v5

    move-object/from16 v0, p0

    iget v14, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentDynamicTilt:F

    sub-float/2addr v5, v14

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const/high16 v14, 0x40a00000    # 5.0f

    cmpl-float v5, v5, v14

    if-lez v5, :cond_7

    const/4 v3, 0x1

    .line 307
    .local v3, "isSignificantTiltDiff":Z
    :goto_4
    if-nez v4, :cond_4

    if-eqz v3, :cond_1

    .line 308
    :cond_4
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->lastDynamicZoomTime:J

    move-wide/from16 v16, v0

    sub-long v8, v14, v16

    .line 310
    .local v8, "lastZoomInterval":J
    const-wide/16 v14, 0x2710

    cmp-long v5, v8, v14

    if-gez v5, :cond_8

    .line 311
    const-wide/16 v14, 0x2710

    sub-long v12, v14, v8

    .line 312
    .local v12, "timeLeft":J
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->handler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->easeOutDynamicZoomTilt:Ljava/lang/Runnable;

    invoke-virtual {v5, v14, v12, v13}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 313
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v14, 0x2

    invoke-virtual {v5, v14}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 314
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "significant too early ="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " scheduled ="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v5, v14}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 291
    .end local v2    # "isSignificantSpeedDiff":Z
    .end local v3    # "isSignificantTiltDiff":Z
    .end local v4    # "isSignificantZoomDiff":Z
    .end local v8    # "lastZoomInterval":J
    .end local v12    # "timeLeft":J
    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 304
    .restart local v2    # "isSignificantSpeedDiff":Z
    :cond_6
    const/4 v4, 0x0

    goto :goto_3

    .line 305
    .restart local v4    # "isSignificantZoomDiff":Z
    :cond_7
    const/4 v3, 0x0

    goto :goto_4

    .line 319
    .restart local v3    # "isSignificantTiltDiff":Z
    .restart local v8    # "lastZoomInterval":J
    :cond_8
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "significant zoom="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " tilt="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " current-zoom="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentDynamicZoom:D

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " current-tilt="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget v15, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentDynamicTilt:F

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " speed = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " lastSpeed="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v5, v14}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 326
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->lastDynamicZoomTime:J

    .line 327
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->animateCamera()V

    goto/16 :goto_1
.end method

.method public onLocalPreferencesChanged(Lcom/navdy/service/library/events/preferences/LocalPreferences;)V
    .locals 3
    .param p1, "preferences"    # Lcom/navdy/service/library/events/preferences/LocalPreferences;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 335
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->localPreferences:Lcom/navdy/service/library/events/preferences/LocalPreferences;

    invoke-virtual {p1, v0}, Lcom/navdy/service/library/events/preferences/LocalPreferences;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 336
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "localPref not changed:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 342
    :goto_0
    return-void

    .line 339
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "localPref changed:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 340
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->localPreferences:Lcom/navdy/service/library/events/preferences/LocalPreferences;

    .line 341
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->setZoom()Z

    goto :goto_0
.end method

.method public onLocationFixChangeEvent(Lcom/navdy/hud/app/maps/MapEvents$LocationFix;)V
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$LocationFix;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 354
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->init:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->mapUIReady:Z

    if-nez v0, :cond_1

    .line 358
    :cond_0
    :goto_0
    return-void

    .line 357
    :cond_1
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->setInitialZoom()V

    goto :goto_0
.end method

.method public onMapUIReady(Lcom/navdy/hud/app/maps/MapEvents$MapUIReady;)V
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$MapUIReady;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 362
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->mapUIReady:Z

    .line 363
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->setInitialZoom()V

    .line 364
    return-void
.end method

.method public setZoom()Z
    .locals 18

    .prologue
    .line 207
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->init:Z

    if-nez v3, :cond_0

    .line 208
    const/4 v3, 0x0

    .line 267
    :goto_0
    return v3

    .line 211
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

    if-eqz v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->localPreferences:Lcom/navdy/service/library/events/preferences/LocalPreferences;

    if-nez v3, :cond_2

    .line 212
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 215
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->getNavigationView()Z

    move-result v3

    if-nez v3, :cond_3

    .line 216
    const/4 v3, 0x0

    goto :goto_0

    .line 219
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereMapController;->getState()Lcom/navdy/hud/app/maps/here/HereMapController$State;

    move-result-object v2

    .line 220
    .local v2, "state":Lcom/navdy/hud/app/maps/here/HereMapController$State;
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->isManualZoom()Z

    move-result v3

    if-eqz v3, :cond_b

    .line 221
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setZoom: manual: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 222
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->localPreferences:Lcom/navdy/service/library/events/preferences/LocalPreferences;

    iget-object v3, v3, Lcom/navdy/service/library/events/preferences/LocalPreferences;->manualZoomLevel:Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    float-to-double v6, v3

    .line 223
    .local v6, "level":D
    const-wide v4, 0x40c3880000000000L    # 10000.0

    cmpl-double v3, v6, v4

    if-nez v3, :cond_5

    .line 224
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapController$State;->AR_MODE:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    if-ne v2, v3, :cond_4

    .line 225
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "setZoom: overview map zoom level, showOverviewMap"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 226
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->navigationView:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentRoute()Lcom/here/android/mpa/routing/Route;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentMapRoute()Lcom/here/android/mpa/mapping/MapRoute;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

    invoke-virtual {v8}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    invoke-virtual {v9}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hasArrived()Z

    move-result v9

    invoke-virtual {v3, v4, v5, v8, v9}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->showOverviewMap(Lcom/here/android/mpa/routing/Route;Lcom/here/android/mpa/mapping/MapRoute;Lcom/here/android/mpa/common/GeoCoordinate;Z)V

    .line 267
    .end local v6    # "level":D
    :goto_1
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 228
    .restart local v6    # "level":D
    :cond_4
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setZoom: overview map zoom level, not in ar mode:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", set to OVERVIEW"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 229
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    sget-object v4, Lcom/navdy/hud/app/maps/here/HereMapController$State;->OVERVIEW:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/maps/here/HereMapController;->setState(Lcom/navdy/hud/app/maps/here/HereMapController$State;)V

    .line 230
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->navigationView:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->showStartMarker()V

    goto :goto_1

    .line 235
    :cond_5
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setZoom: normal zoom level: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 236
    const-wide/high16 v4, 0x4028000000000000L    # 12.0

    cmpg-double v3, v6, v4

    if-ltz v3, :cond_6

    const-wide v4, 0x4030800000000000L    # 16.5

    cmpl-double v3, v6, v4

    if-lez v3, :cond_7

    .line 237
    :cond_6
    move-wide/from16 v16, v6

    .line 238
    .local v16, "prevLevel":D
    const-wide v6, 0x4030800000000000L    # 16.5

    .line 239
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setZoom: normal zoom level changed from ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v16

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] to ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 241
    .end local v16    # "prevLevel":D
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereMapController;->getState()Lcom/navdy/hud/app/maps/here/HereMapController$State;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/app/maps/here/HereMapController$State;->OVERVIEW:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    if-eq v3, v4, :cond_8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    .line 242
    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereMapController;->getState()Lcom/navdy/hud/app/maps/here/HereMapController$State;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/app/maps/here/HereMapController$State;->TRANSITION:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    if-ne v3, v4, :cond_9

    .line 244
    :cond_8
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "setZoom: normal zoom level, showRouteMap"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 245
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->navigationView:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentDynamicTilt:F

    invoke-virtual {v3, v4, v6, v7, v5}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->showRouteMap(Lcom/here/android/mpa/common/GeoPosition;DF)V

    goto/16 :goto_1

    .line 246
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereMapController;->getState()Lcom/navdy/hud/app/maps/here/HereMapController$State;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/app/maps/here/HereMapController$State;->AR_MODE:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    if-ne v3, v4, :cond_a

    .line 247
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

    invoke-virtual {v4}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v4

    sget-object v5, Lcom/here/android/mpa/mapping/Map$Animation;->NONE:Lcom/here/android/mpa/mapping/Map$Animation;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

    invoke-virtual {v8}, Lcom/here/android/mpa/common/GeoPosition;->getHeading()D

    move-result-wide v8

    double-to-float v8, v8

    move-object/from16 v0, p0

    iget v9, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentDynamicTilt:F

    invoke-virtual/range {v3 .. v9}, Lcom/navdy/hud/app/maps/here/HereMapController;->setCenter(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;DFF)V

    .line 248
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "setZoom: normal zoom level, already in route map, setcenter"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 250
    :cond_a
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setZoom: normal zoom level, incorrect state:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 254
    .end local v6    # "level":D
    :cond_b
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setZoom: automatic: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 255
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapController$State;->OVERVIEW:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    if-eq v2, v3, :cond_c

    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapController$State;->TRANSITION:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    if-ne v2, v3, :cond_d

    .line 258
    :cond_c
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "setZoom:automatic showRouteMap"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 259
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->navigationView:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentDynamicZoom:D

    move-object/from16 v0, p0

    iget v5, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentDynamicTilt:F

    invoke-virtual {v3, v4, v8, v9, v5}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->showRouteMap(Lcom/here/android/mpa/common/GeoPosition;DF)V

    goto/16 :goto_1

    .line 260
    :cond_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereMapController;->getState()Lcom/navdy/hud/app/maps/here/HereMapController$State;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/app/maps/here/HereMapController$State;->AR_MODE:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    if-ne v3, v4, :cond_e

    .line 261
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "setZoom:automatic already in route map, setCenter"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 262
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

    invoke-virtual {v3}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v10

    sget-object v11, Lcom/here/android/mpa/mapping/Map$Animation;->NONE:Lcom/here/android/mpa/mapping/Map$Animation;

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentDynamicZoom:D

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

    invoke-virtual {v3}, Lcom/here/android/mpa/common/GeoPosition;->getHeading()D

    move-result-wide v4

    double-to-float v14, v4

    move-object/from16 v0, p0

    iget v15, v0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->currentDynamicTilt:F

    invoke-virtual/range {v9 .. v15}, Lcom/navdy/hud/app/maps/here/HereMapController;->setCenter(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;DFF)V

    goto/16 :goto_1

    .line 264
    :cond_e
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setZoom:automatic incorrect state:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_1
.end method
