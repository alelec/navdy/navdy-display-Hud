.class public Lcom/navdy/hud/app/maps/here/HereLocationFixManager;
.super Ljava/lang/Object;
.source "HereLocationFixManager.java"


# static fields
.field private static final DATE_FORMAT:Ljava/text/SimpleDateFormat;

.field private static final RECORD_MARKER:[B

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private androidGpsLocationListener:Landroid/location/LocationListener;

.field private final bus:Lcom/squareup/otto/Bus;

.field private checkLocationFix:Ljava/lang/Runnable;

.field private counter:J

.field private debugTTSLocationListener:Landroid/location/LocationListener;

.field private firstLocationFixCheck:Z

.field private geoPosition:Lcom/here/android/mpa/common/GeoPosition;

.field private handler:Landroid/os/Handler;

.field private volatile hereGpsSignal:Z

.field private volatile isRecording:Z

.field private lastAndroidLocation:Landroid/location/Location;

.field private lastAndroidLocationPostTime:J

.field private lastAndroidLocationTime:J

.field private lastGpsSwitchEvent:Lcom/navdy/hud/app/device/gps/GpsUtils$GpsSwitch;

.field private lastHerelocationUpdateTime:J

.field private volatile lastLocationTime:J

.field private volatile locationFix:Z

.field private final locationListener:Landroid/location/LocationListener;

.field private locationMethod:Lcom/here/android/mpa/common/PositioningManager$LocationMethod;

.field private rawLocationMarker:Lcom/here/android/mpa/mapping/MapMarker;

.field private rawLocationMarkerAdded:Z

.field private recordingFile:Ljava/io/FileOutputStream;

.field private volatile recordingLabel:Ljava/lang/String;

.field private final speedManager:Lcom/navdy/hud/app/manager/SpeedManager;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 54
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 56
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd\'_\'HH_mm_ss.SSS"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->DATE_FORMAT:Ljava/text/SimpleDateFormat;

    .line 57
    const-string v0, "<< Marker >>\n"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->RECORD_MARKER:[B

    return-void
.end method

.method constructor <init>(Lcom/squareup/otto/Bus;)V
    .locals 20
    .param p1, "bus"    # Lcom/squareup/otto/Bus;

    .prologue
    .line 187
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 77
    new-instance v5, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->handler:Landroid/os/Handler;

    .line 87
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->firstLocationFixCheck:Z

    .line 89
    new-instance v5, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$1;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$1;-><init>(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->checkLocationFix:Ljava/lang/Runnable;

    .line 133
    new-instance v5, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$2;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$2;-><init>(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->locationListener:Landroid/location/LocationListener;

    .line 184
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->androidGpsLocationListener:Landroid/location/LocationListener;

    .line 185
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->debugTTSLocationListener:Landroid/location/LocationListener;

    .line 188
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->bus:Lcom/squareup/otto/Bus;

    .line 189
    invoke-static {}, Lcom/navdy/hud/app/manager/SpeedManager;->getInstance()Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    .line 190
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v12

    .line 191
    .local v12, "context":Landroid/content/Context;
    const-string v5, "location"

    invoke-virtual {v12, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/location/LocationManager;

    .line 192
    .local v4, "locationManager":Landroid/location/LocationManager;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getBkLocationReceiverLooper()Landroid/os/Looper;

    move-result-object v10

    .line 194
    .local v10, "locationReceiverLooper":Landroid/os/Looper;
    invoke-static {}, Lcom/navdy/hud/app/framework/voice/TTSUtils;->isDebugTTSEnabled()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isNavdyDevice()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 196
    const-string v5, "gps"

    invoke-virtual {v4, v5}, Landroid/location/LocationManager;->getProvider(Ljava/lang/String;)Landroid/location/LocationProvider;

    move-result-object v14

    .line 197
    .local v14, "gpsProvider":Landroid/location/LocationProvider;
    if-eqz v14, :cond_0

    .line 198
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v18

    .line 199
    .local v18, "t1":J
    new-instance v5, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$3;

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-direct {v5, v0, v4, v1, v2}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$3;-><init>(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;Landroid/location/LocationManager;J)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->debugTTSLocationListener:Landroid/location/LocationListener;

    .line 237
    const-string v5, "gps"

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->debugTTSLocationListener:Landroid/location/LocationListener;

    invoke-virtual/range {v4 .. v10}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;Landroid/os/Looper;)V

    .line 241
    .end local v14    # "gpsProvider":Landroid/location/LocationProvider;
    .end local v18    # "t1":J
    :cond_0
    const/4 v15, 0x0

    .line 244
    .local v15, "last":Landroid/location/Location;
    const-string v5, "NAVDY_GPS_PROVIDER"

    invoke-virtual {v4, v5}, Landroid/location/LocationManager;->getProvider(Ljava/lang/String;)Landroid/location/LocationProvider;

    move-result-object v14

    .line 245
    .restart local v14    # "gpsProvider":Landroid/location/LocationProvider;
    if-eqz v14, :cond_1

    .line 246
    const-string v5, "NAVDY_GPS_PROVIDER"

    invoke-virtual {v4, v5}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v15

    .line 247
    const-string v5, "NAVDY_GPS_PROVIDER"

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->locationListener:Landroid/location/LocationListener;

    invoke-virtual/range {v4 .. v10}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;Landroid/os/Looper;)V

    .line 248
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "installed gps listener"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 252
    :cond_1
    const-string v5, "gps"

    invoke-virtual {v4, v5}, Landroid/location/LocationManager;->getProvider(Ljava/lang/String;)Landroid/location/LocationProvider;

    move-result-object v14

    .line 253
    if-eqz v14, :cond_2

    .line 254
    new-instance v5, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$4;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$4;-><init>(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->androidGpsLocationListener:Landroid/location/LocationListener;

    .line 276
    const-string v5, "gps"

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->androidGpsLocationListener:Landroid/location/LocationListener;

    invoke-virtual/range {v4 .. v10}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;Landroid/os/Looper;)V

    .line 278
    :cond_2
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "installed gps listener"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 281
    const-string v5, "network"

    invoke-virtual {v4, v5}, Landroid/location/LocationManager;->getProvider(Ljava/lang/String;)Landroid/location/LocationProvider;

    move-result-object v16

    .line 282
    .local v16, "networkProvider":Landroid/location/LocationProvider;
    if-eqz v16, :cond_4

    .line 283
    if-nez v15, :cond_3

    .line 284
    const-string v5, "network"

    invoke-virtual {v4, v5}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v15

    .line 286
    :cond_3
    const-string v5, "network"

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->locationListener:Landroid/location/LocationListener;

    invoke-virtual/range {v4 .. v10}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;Landroid/os/Looper;)V

    .line 287
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "installed n/w listener"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 290
    :cond_4
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "got last location from location manager:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 291
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->locationFix:Z

    if-nez v5, :cond_5

    if-eqz v15, :cond_5

    .line 292
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "sending to map"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 293
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->setMaptoLocation(Landroid/location/Location;)V

    .line 296
    :cond_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->bus:Lcom/squareup/otto/Bus;

    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 297
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->handler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->checkLocationFix:Ljava/lang/Runnable;

    const-wide/16 v8, 0x3e8

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 300
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getConnectionHandler()Lcom/navdy/hud/app/service/ConnectionHandler;

    move-result-object v11

    .line 301
    .local v11, "connectionHandler":Lcom/navdy/hud/app/service/ConnectionHandler;
    invoke-virtual {v11}, Lcom/navdy/hud/app/service/ConnectionHandler;->getDriverRecordingEvent()Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;

    move-result-object v13

    .line 302
    .local v13, "event":Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;
    if-eqz v13, :cond_6

    .line 303
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->onStartDriveRecording(Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;)V

    .line 306
    :cond_6
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "initialized"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 307
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)J
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    .prologue
    .line 53
    iget-wide v0, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->lastAndroidLocationTime:J

    return-wide v0
.end method

.method static synthetic access$002(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;J)J
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereLocationFixManager;
    .param p1, "x1"    # J

    .prologue
    .line 53
    iput-wide p1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->lastAndroidLocationTime:J

    return-wide p1
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->locationFix:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->isRecording:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereLocationFixManager;
    .param p1, "x1"    # Z

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->isRecording:Z

    return p1
.end method

.method static synthetic access$102(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereLocationFixManager;
    .param p1, "x1"    # Z

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->locationFix:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;Landroid/location/Location;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereLocationFixManager;
    .param p1, "x1"    # Landroid/location/Location;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->recordRawLocation(Landroid/location/Location;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)J
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    .prologue
    .line 53
    iget-wide v0, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->lastAndroidLocationPostTime:J

    return-wide v0
.end method

.method static synthetic access$1202(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;J)J
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereLocationFixManager;
    .param p1, "x1"    # J

    .prologue
    .line 53
    iput-wide p1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->lastAndroidLocationPostTime:J

    return-wide p1
.end method

.method static synthetic access$1300(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;Landroid/location/Location;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereLocationFixManager;
    .param p1, "x1"    # Landroid/location/Location;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->sendLocation(Landroid/location/Location;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->recordingLabel:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereLocationFixManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->recordingLabel:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Ljava/io/FileOutputStream;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->recordingFile:Ljava/io/FileOutputStream;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;Ljava/io/FileOutputStream;)Ljava/io/FileOutputStream;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereLocationFixManager;
    .param p1, "x1"    # Ljava/io/FileOutputStream;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->recordingFile:Ljava/io/FileOutputStream;

    return-object p1
.end method

.method static synthetic access$1600()[B
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->RECORD_MARKER:[B

    return-object v0
.end method

.method static synthetic access$200()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Lcom/squareup/otto/Bus;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->bus:Lcom/squareup/otto/Bus;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Landroid/location/Location;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->lastAndroidLocation:Landroid/location/Location;

    return-object v0
.end method

.method static synthetic access$402(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;Landroid/location/Location;)Landroid/location/Location;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereLocationFixManager;
    .param p1, "x1"    # Landroid/location/Location;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->lastAndroidLocation:Landroid/location/Location;

    return-object p1
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->firstLocationFixCheck:Z

    return v0
.end method

.method static synthetic access$502(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereLocationFixManager;
    .param p1, "x1"    # Z

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->firstLocationFixCheck:Z

    return p1
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->checkLocationFix:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Lcom/here/android/mpa/mapping/MapMarker;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->rawLocationMarker:Lcom/here/android/mpa/mapping/MapMarker;

    return-object v0
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->rawLocationMarkerAdded:Z

    return v0
.end method

.method private recordHereLocation(Lcom/here/android/mpa/common/GeoPosition;Lcom/here/android/mpa/common/PositioningManager$LocationMethod;Z)V
    .locals 3
    .param p1, "geoPosition"    # Lcom/here/android/mpa/common/GeoPosition;
    .param p2, "locationMethod"    # Lcom/here/android/mpa/common/PositioningManager$LocationMethod;
    .param p3, "isMapMatched"    # Z

    .prologue
    .line 449
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$8;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$8;-><init>(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;Lcom/here/android/mpa/common/GeoPosition;Lcom/here/android/mpa/common/PositioningManager$LocationMethod;Z)V

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 480
    return-void
.end method

.method private recordRawLocation(Landroid/location/Location;)V
    .locals 3
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 420
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$7;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$7;-><init>(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;Landroid/location/Location;)V

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 446
    return-void
.end method

.method private sendLocation(Landroid/location/Location;)V
    .locals 6
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 566
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v1

    invoke-virtual {p1}, Landroid/location/Location;->getElapsedRealtimeNanos()J

    move-result-wide v2

    const-wide/32 v4, 0xf4240

    div-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/navdy/hud/app/manager/SpeedManager;->setGpsSpeed(FJ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 567
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/maps/MapEvents$GPSSpeedEvent;

    invoke-direct {v1}, Lcom/navdy/hud/app/maps/MapEvents$GPSSpeedEvent;-><init>()V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 569
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->lastLocationTime:J

    .line 570
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 571
    return-void
.end method


# virtual methods
.method public addMarkers(Lcom/navdy/hud/app/maps/here/HereMapController;)V
    .locals 4
    .param p1, "mapController"    # Lcom/navdy/hud/app/maps/here/HereMapController;

    .prologue
    .line 503
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsUtils;->isDebugRawGpsPosEnabled()Z

    move-result v2

    if-nez v2, :cond_1

    .line 523
    :cond_0
    :goto_0
    return-void

    .line 508
    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->rawLocationMarker:Lcom/here/android/mpa/mapping/MapMarker;

    if-nez v2, :cond_2

    .line 509
    new-instance v2, Lcom/here/android/mpa/mapping/MapMarker;

    invoke-direct {v2}, Lcom/here/android/mpa/mapping/MapMarker;-><init>()V

    iput-object v2, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->rawLocationMarker:Lcom/here/android/mpa/mapping/MapMarker;

    .line 510
    new-instance v0, Lcom/here/android/mpa/common/Image;

    invoke-direct {v0}, Lcom/here/android/mpa/common/Image;-><init>()V

    .line 511
    .local v0, "image":Lcom/here/android/mpa/common/Image;
    const v2, 0x7f0201c7

    invoke-virtual {v0, v2}, Lcom/here/android/mpa/common/Image;->setImageResource(I)V

    .line 512
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->rawLocationMarker:Lcom/here/android/mpa/mapping/MapMarker;

    invoke-virtual {v2, v0}, Lcom/here/android/mpa/mapping/MapMarker;->setIcon(Lcom/here/android/mpa/common/Image;)Lcom/here/android/mpa/mapping/MapMarker;

    .line 513
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "marker created"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 515
    .end local v0    # "image":Lcom/here/android/mpa/common/Image;
    :cond_2
    iget-boolean v2, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->rawLocationMarkerAdded:Z

    if-nez v2, :cond_0

    .line 516
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->rawLocationMarker:Lcom/here/android/mpa/mapping/MapMarker;

    invoke-virtual {p1, v2}, Lcom/navdy/hud/app/maps/here/HereMapController;->addMapObject(Lcom/here/android/mpa/mapping/MapObject;)V

    .line 517
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->rawLocationMarkerAdded:Z

    .line 518
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "marker added"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 520
    :catch_0
    move-exception v1

    .line 521
    .local v1, "t":Ljava/lang/Throwable;
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->geoPosition:Lcom/here/android/mpa/common/GeoPosition;

    if-nez v0, :cond_0

    .line 331
    const/4 v0, 0x0

    .line 333
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->geoPosition:Lcom/here/android/mpa/common/GeoPosition;

    invoke-virtual {v0}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v0

    goto :goto_0
.end method

.method public getLastLocationTime()J
    .locals 2

    .prologue
    .line 574
    iget-wide v0, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->lastLocationTime:J

    return-wide v0
.end method

.method public getLocationFixEvent()Lcom/navdy/hud/app/maps/MapEvents$LocationFix;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 548
    iget-boolean v3, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->locationFix:Z

    if-eqz v3, :cond_2

    .line 549
    const/4 v1, 0x0

    .line 550
    .local v1, "phone":Z
    const/4 v0, 0x0

    .line 551
    .local v0, "gps":Z
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->lastAndroidLocation:Landroid/location/Location;

    if-eqz v3, :cond_0

    .line 552
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->lastAndroidLocation:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v2

    .line 553
    .local v2, "provider":Ljava/lang/String;
    const-string v3, "NAVDY_GPS_PROVIDER"

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 554
    const/4 v0, 0x1

    .line 559
    .end local v2    # "provider":Ljava/lang/String;
    :cond_0
    :goto_0
    new-instance v3, Lcom/navdy/hud/app/maps/MapEvents$LocationFix;

    const/4 v4, 0x1

    invoke-direct {v3, v4, v1, v0}, Lcom/navdy/hud/app/maps/MapEvents$LocationFix;-><init>(ZZZ)V

    .line 561
    .end local v0    # "gps":Z
    .end local v1    # "phone":Z
    :goto_1
    return-object v3

    .line 556
    .restart local v0    # "gps":Z
    .restart local v1    # "phone":Z
    .restart local v2    # "provider":Ljava/lang/String;
    :cond_1
    const/4 v1, 0x1

    goto :goto_0

    .line 561
    .end local v0    # "gps":Z
    .end local v1    # "phone":Z
    .end local v2    # "provider":Ljava/lang/String;
    :cond_2
    new-instance v3, Lcom/navdy/hud/app/maps/MapEvents$LocationFix;

    invoke-direct {v3, v4, v4, v4}, Lcom/navdy/hud/app/maps/MapEvents$LocationFix;-><init>(ZZZ)V

    goto :goto_1
.end method

.method public hasLocationFix()Z
    .locals 1

    .prologue
    .line 326
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->locationFix:Z

    return v0
.end method

.method public isRecording()Z
    .locals 1

    .prologue
    .line 499
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->isRecording:Z

    return v0
.end method

.method public onGpsEventSwitch(Lcom/navdy/hud/app/device/gps/GpsUtils$GpsSwitch;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/hud/app/device/gps/GpsUtils$GpsSwitch;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 345
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Gps-Switch phone="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p1, Lcom/navdy/hud/app/device/gps/GpsUtils$GpsSwitch;->usingPhone:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ublox="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p1, Lcom/navdy/hud/app/device/gps/GpsUtils$GpsSwitch;->usingUblox:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 346
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->lastGpsSwitchEvent:Lcom/navdy/hud/app/device/gps/GpsUtils$GpsSwitch;

    .line 347
    return-void
.end method

.method public onHereGpsEvent(Lcom/navdy/hud/app/maps/MapEvents$GpsStatusChange;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/hud/app/maps/MapEvents$GpsStatusChange;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 339
    iget-boolean v0, p1, Lcom/navdy/hud/app/maps/MapEvents$GpsStatusChange;->connected:Z

    iput-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->hereGpsSignal:Z

    .line 340
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "here gps event connected ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->hereGpsSignal:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 341
    return-void
.end method

.method public onHerePositionUpdated(Lcom/here/android/mpa/common/PositioningManager$LocationMethod;Lcom/here/android/mpa/common/GeoPosition;Z)V
    .locals 6
    .param p1, "locationMethod"    # Lcom/here/android/mpa/common/PositioningManager$LocationMethod;
    .param p2, "geoPosition"    # Lcom/here/android/mpa/common/GeoPosition;
    .param p3, "isMapMatched"    # Z

    .prologue
    .line 310
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 311
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onHerePositionUpdated ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->counter:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->counter:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] geoPosition["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] method ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/here/android/mpa/common/PositioningManager$LocationMethod;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] valid["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/here/android/mpa/common/GeoPosition;->isValid()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 313
    :cond_0
    if-eqz p1, :cond_1

    if-nez p2, :cond_2

    .line 323
    :cond_1
    :goto_0
    return-void

    .line 316
    :cond_2
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->locationMethod:Lcom/here/android/mpa/common/PositioningManager$LocationMethod;

    .line 317
    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->geoPosition:Lcom/here/android/mpa/common/GeoPosition;

    .line 318
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->lastHerelocationUpdateTime:J

    .line 320
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->isRecording:Z

    if-eqz v0, :cond_1

    .line 321
    invoke-direct {p0, p2, p1, p3}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->recordHereLocation(Lcom/here/android/mpa/common/GeoPosition;Lcom/here/android/mpa/common/PositioningManager$LocationMethod;Z)V

    goto :goto_0
.end method

.method public onStartDriveRecording(Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 366
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$5;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$5;-><init>(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;)V

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 394
    return-void
.end method

.method public onStopDriveRecording(Lcom/navdy/service/library/events/debug/StopDriveRecordingEvent;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/service/library/events/debug/StopDriveRecordingEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 398
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$6;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$6;-><init>(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)V

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 417
    return-void
.end method

.method public recordMarker()V
    .locals 3

    .prologue
    .line 483
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->isRecording:Z

    if-nez v0, :cond_0

    .line 496
    :goto_0
    return-void

    .line 486
    :cond_0
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$9;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$9;-><init>(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)V

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public removeMarkers(Lcom/navdy/hud/app/maps/here/HereMapController;)V
    .locals 2
    .param p1, "mapController"    # Lcom/navdy/hud/app/maps/here/HereMapController;

    .prologue
    .line 526
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsUtils;->isDebugRawGpsPosEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 534
    :cond_0
    :goto_0
    return-void

    .line 529
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->rawLocationMarker:Lcom/here/android/mpa/mapping/MapMarker;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->rawLocationMarkerAdded:Z

    if-eqz v0, :cond_0

    .line 530
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->rawLocationMarker:Lcom/here/android/mpa/mapping/MapMarker;

    invoke-virtual {p1, v0}, Lcom/navdy/hud/app/maps/here/HereMapController;->removeMapObject(Lcom/here/android/mpa/mapping/MapObject;)V

    .line 531
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->rawLocationMarkerAdded:Z

    .line 532
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "marker removed"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setMaptoLocation(Landroid/location/Location;)V
    .locals 9
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 351
    :try_start_0
    iget-boolean v1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->locationFix:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->geoPosition:Lcom/here/android/mpa/common/GeoPosition;

    if-nez v1, :cond_0

    .line 352
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "marking location fix:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 353
    new-instance v8, Lcom/here/android/mpa/common/GeoPosition;

    new-instance v1, Lcom/here/android/mpa/common/GeoCoordinate;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-virtual {p1}, Landroid/location/Location;->getAltitude()D

    move-result-wide v6

    invoke-direct/range {v1 .. v7}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DDD)V

    invoke-direct {v8, v1}, Lcom/here/android/mpa/common/GeoPosition;-><init>(Lcom/here/android/mpa/common/GeoCoordinate;)V

    iput-object v8, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->geoPosition:Lcom/here/android/mpa/common/GeoPosition;

    .line 354
    sget-object v1, Lcom/here/android/mpa/common/PositioningManager$LocationMethod;->NETWORK:Lcom/here/android/mpa/common/PositioningManager$LocationMethod;

    iput-object v1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->locationMethod:Lcom/here/android/mpa/common/PositioningManager$LocationMethod;

    .line 355
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->lastHerelocationUpdateTime:J

    .line 356
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->locationFix:Z

    .line 357
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v2, Lcom/navdy/hud/app/maps/MapEvents$LocationFix;

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct {v2, v3, v4, v5}, Lcom/navdy/hud/app/maps/MapEvents$LocationFix;-><init>(ZZZ)V

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 362
    :cond_0
    :goto_0
    return-void

    .line 359
    :catch_0
    move-exception v0

    .line 360
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public shutdown()V
    .locals 3

    .prologue
    .line 537
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "location"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 538
    .local v0, "locationManager":Landroid/location/LocationManager;
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->locationListener:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 539
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->androidGpsLocationListener:Landroid/location/LocationListener;

    if-eqz v1, :cond_0

    .line 540
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->androidGpsLocationListener:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 542
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->debugTTSLocationListener:Landroid/location/LocationListener;

    if-eqz v1, :cond_1

    .line 543
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->debugTTSLocationListener:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 545
    :cond_1
    return-void
.end method
