.class final Lcom/navdy/hud/app/maps/here/HereRouteManager$5;
.super Ljava/lang/Object;
.source "HereRouteManager.java"

# interfaces
.implements Lcom/navdy/hud/app/maps/here/HereRouteCalculator$RouteCalculatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeSearchNoTraffic(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/common/GeoCoordinate;Ljava/util/List;Lcom/here/android/mpa/common/GeoCoordinate;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;


# direct methods
.method constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V
    .locals 0

    .prologue
    .line 523
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$5;->val$request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public error(Lcom/here/android/mpa/routing/RoutingError;Ljava/lang/Throwable;)V
    .locals 5
    .param p1, "error"    # Lcom/here/android/mpa/routing/RoutingError;
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 532
    if-eqz p1, :cond_0

    .line 533
    invoke-virtual {p1}, Lcom/here/android/mpa/routing/RoutingError;->name()Ljava/lang/String;

    move-result-object v0

    .line 537
    .local v0, "errorStr":Ljava/lang/String;
    :goto_0
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->lockObj:Ljava/lang/Object;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$400()Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 538
    :try_start_0
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCancelledByUser:Z
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$1500()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 539
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v3, "user cancelled routing"

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 540
    sget-object v1, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_CANCELLED:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$5;->val$request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    const/4 v4, 0x0

    # invokes: Lcom/navdy/hud/app/maps/here/HereRouteManager;->returnErrorResponse(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;)V
    invoke-static {v1, v3, v4}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$300(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;)V

    .line 544
    :goto_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 545
    return-void

    .line 535
    .end local v0    # "errorStr":Ljava/lang/String;
    :cond_0
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->context:Landroid/content/Context;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$200()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0902da

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "errorStr":Ljava/lang/String;
    goto :goto_0

    .line 542
    :cond_1
    :try_start_1
    sget-object v1, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SERVICE_ERROR:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$5;->val$request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    # invokes: Lcom/navdy/hud/app/maps/here/HereRouteManager;->returnErrorResponse(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;)V
    invoke-static {v1, v3, v0}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$300(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;)V

    goto :goto_1

    .line 544
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public postSuccess(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 526
    .local p1, "outgoingResults":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/navigation/NavigationRouteResult;>;"
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$5;->val$request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    const/4 v1, 0x0

    # invokes: Lcom/navdy/hud/app/maps/here/HereRouteManager;->returnSuccessResponse(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/util/ArrayList;Z)V
    invoke-static {v0, p1, v1}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$1300(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/util/ArrayList;Z)V

    .line 527
    return-void
.end method

.method public preSuccess()V
    .locals 2

    .prologue
    .line 556
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->lockObj:Ljava/lang/Object;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$400()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 557
    const/4 v0, 0x0

    :try_start_0
    # setter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->activeRouteCalculator:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$1702(Lcom/navdy/hud/app/maps/here/HereRouteCalculator;)Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    .line 558
    monitor-exit v1

    .line 559
    return-void

    .line 558
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public progress(I)V
    .locals 0
    .param p1, "progress"    # I

    .prologue
    .line 551
    return-void
.end method
