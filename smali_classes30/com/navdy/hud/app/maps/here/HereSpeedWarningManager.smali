.class public Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;
.super Ljava/lang/Object;
.source "HereSpeedWarningManager.java"


# static fields
.field private static final BUFFER_SPEED:I = 0x7

.field private static final DISCARD_INTERVAL:J

.field private static final PERIODIC_CHECK_INTERVAL:I = 0x1388

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private bus:Lcom/squareup/otto/Bus;

.field private checkSpeedRunnable:Ljava/lang/Runnable;

.field private context:Landroid/content/Context;

.field private handler:Landroid/os/Handler;

.field private hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

.field private lastSpeedWarningTime:J

.field private lastUpdate:J

.field private mapsEventHandler:Lcom/navdy/hud/app/maps/MapsEventHandler;

.field private periodicCheckRunnable:Ljava/lang/Runnable;

.field private periodicRunnable:Ljava/lang/Runnable;

.field private registered:Z

.field private speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

.field private volatile speedWarningOn:Z

.field private tag:Ljava/lang/String;

.field private warningSpeed:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 35
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 44
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->DISCARD_INTERVAL:J

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/maps/MapsEventHandler;Lcom/navdy/hud/app/maps/here/HereNavigationManager;)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "bus"    # Lcom/squareup/otto/Bus;
    .param p3, "mapsEventHandler"    # Lcom/navdy/hud/app/maps/MapsEventHandler;
    .param p4, "hereNavigationManager"    # Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->context:Landroid/content/Context;

    .line 51
    invoke-static {}, Lcom/navdy/hud/app/manager/SpeedManager;->getInstance()Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    .line 62
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager$1;-><init>(Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->periodicRunnable:Ljava/lang/Runnable;

    .line 69
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager$2;-><init>(Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->periodicCheckRunnable:Ljava/lang/Runnable;

    .line 92
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager$3;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager$3;-><init>(Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->checkSpeedRunnable:Ljava/lang/Runnable;

    .line 110
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->tag:Ljava/lang/String;

    .line 111
    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->bus:Lcom/squareup/otto/Bus;

    .line 112
    iput-object p3, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->mapsEventHandler:Lcom/navdy/hud/app/maps/MapsEventHandler;

    .line 113
    iput-object p4, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    .line 114
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->handler:Landroid/os/Handler;

    .line 115
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->periodicCheckRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->registered:Z

    return v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;)J
    .locals 2
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;

    .prologue
    .line 34
    iget-wide v0, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->lastUpdate:J

    return-wide v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->checkSpeed()V

    return-void
.end method

.method static synthetic access$400()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->periodicRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method private checkSpeed()V
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v7, -0x1

    .line 212
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    invoke-virtual {v3}, Lcom/navdy/hud/app/manager/SpeedManager;->getCurrentSpeed()I

    move-result v3

    int-to-float v0, v3

    .line 213
    .local v0, "currentSpeed":F
    const/high16 v3, -0x40800000    # -1.0f

    cmpl-float v3, v0, v3

    if-nez v3, :cond_1

    .line 214
    iget-boolean v3, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->speedWarningOn:Z

    if-eqz v3, :cond_0

    .line 216
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "lost speed info while warning on"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 217
    const/4 v3, 0x0

    invoke-direct {p0, v3, v5, v5}, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->onSpeedExceededEnd(Ljava/lang/String;II)V

    .line 249
    :cond_0
    :goto_0
    return-void

    .line 221
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getCurrentRoadElement()Lcom/here/android/mpa/common/RoadElement;

    move-result-object v1

    .line 222
    .local v1, "roadElement":Lcom/here/android/mpa/common/RoadElement;
    if-eqz v1, :cond_0

    .line 226
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->lastUpdate:J

    .line 228
    invoke-virtual {v1}, Lcom/here/android/mpa/common/RoadElement;->getSpeedLimit()F

    move-result v3

    float-to-double v4, v3

    sget-object v3, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->METERS_PER_SECOND:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    invoke-virtual {v6}, Lcom/navdy/hud/app/manager/SpeedManager;->getSpeedUnit()Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    move-result-object v6

    invoke-static {v4, v5, v3, v6}, Lcom/navdy/hud/app/manager/SpeedManager;->convert(DLcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;)I

    move-result v2

    .line 229
    .local v2, "speedAllowed":I
    if-gtz v2, :cond_2

    .line 231
    iget-boolean v3, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->speedWarningOn:Z

    if-eqz v3, :cond_0

    .line 233
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "clear speed warning, no speedlimit info avail"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 234
    invoke-virtual {v1}, Lcom/here/android/mpa/common/RoadElement;->getRoadName()Ljava/lang/String;

    move-result-object v3

    float-to-int v4, v0

    invoke-direct {p0, v3, v2, v4}, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->onSpeedExceededEnd(Ljava/lang/String;II)V

    goto :goto_0

    .line 238
    :cond_2
    int-to-float v3, v2

    cmpl-float v3, v0, v3

    if-lez v3, :cond_4

    .line 239
    iget-boolean v3, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->speedWarningOn:Z

    if-eqz v3, :cond_3

    iget v3, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->warningSpeed:I

    if-eq v3, v7, :cond_3

    iget v3, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->warningSpeed:I

    if-eq v3, v7, :cond_0

    iget v3, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->warningSpeed:I

    int-to-float v3, v3

    cmpl-float v3, v0, v3

    if-lez v3, :cond_0

    .line 242
    :cond_3
    invoke-virtual {v1}, Lcom/here/android/mpa/common/RoadElement;->getRoadName()Ljava/lang/String;

    move-result-object v3

    float-to-int v4, v0

    invoke-direct {p0, v3, v2, v4}, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->onSpeedExceeded(Ljava/lang/String;II)V

    goto :goto_0

    .line 245
    :cond_4
    iget-boolean v3, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->speedWarningOn:Z

    if-eqz v3, :cond_0

    .line 246
    invoke-virtual {v1}, Lcom/here/android/mpa/common/RoadElement;->getRoadName()Ljava/lang/String;

    move-result-object v3

    float-to-int v4, v0

    invoke-direct {p0, v3, v2, v4}, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->onSpeedExceededEnd(Ljava/lang/String;II)V

    goto :goto_0
.end method

.method private onSpeedExceeded(Ljava/lang/String;II)V
    .locals 17
    .param p1, "roadName"    # Ljava/lang/String;
    .param p2, "speedLimit"    # I
    .param p3, "currentSpeed"    # I

    .prologue
    .line 137
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->bus:Lcom/squareup/otto/Bus;

    sget-object v13, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->SPEED_EXCEEDED:Lcom/navdy/hud/app/maps/MapEvents$SpeedWarning;

    invoke-virtual {v12, v13}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 138
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->speedWarningOn:Z

    if-nez v12, :cond_0

    .line 139
    const/4 v12, -0x1

    move-object/from16 v0, p0

    iput v12, v0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->warningSpeed:I

    .line 141
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->speedWarningOn:Z

    .line 142
    .local v2, "alreadyOn":Z
    const/4 v12, 0x1

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->speedWarningOn:Z

    .line 143
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->mapsEventHandler:Lcom/navdy/hud/app/maps/MapsEventHandler;

    invoke-virtual {v12}, Lcom/navdy/hud/app/maps/MapsEventHandler;->getNavigationPreferences()Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    move-result-object v3

    .line 144
    .local v3, "prefs":Lcom/navdy/service/library/events/preferences/NavigationPreferences;
    sget-object v12, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v13, v3, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->spokenSpeedLimitWarnings:Ljava/lang/Boolean;

    invoke-virtual {v12, v13}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 145
    invoke-static {}, Lcom/navdy/hud/app/framework/phonecall/CallUtils;->isPhoneCallInProgress()Z

    move-result v12

    if-eqz v12, :cond_3

    .line 146
    :cond_1
    if-nez v2, :cond_2

    .line 147
    sget-object v12, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->tag:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " speed exceeded:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " limit="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " current="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p3

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 196
    :cond_2
    :goto_0
    return-void

    .line 151
    :cond_3
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    invoke-virtual {v12}, Lcom/navdy/hud/app/manager/SpeedManager;->getSpeedUnit()Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    move-result-object v9

    .line 153
    .local v9, "speedUnit":Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;
    move-object/from16 v0, p0

    iget v12, v0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->warningSpeed:I

    const/4 v13, -0x1

    if-ne v12, v13, :cond_4

    .line 154
    add-int/lit8 v8, p2, 0x7

    .line 158
    .local v8, "speedToConsider":I
    :goto_1
    move/from16 v0, p3

    if-lt v0, v8, :cond_5

    .line 159
    sget-object v12, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->tag:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " speed exceeded-notify current["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p3

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "] threshold["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "] allowed["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "] "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    .line 160
    invoke-virtual {v9}, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->name()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 159
    invoke-virtual {v12, v13}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 161
    move/from16 v0, p3

    move-object/from16 v1, p0

    iput v0, v1, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->warningSpeed:I

    .line 170
    sget-object v12, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager$4;->$SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit:[I

    invoke-virtual {v9}, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->ordinal()I

    move-result v13

    aget v12, v12, v13

    packed-switch v12, :pswitch_data_0

    .line 184
    const-string v11, ""

    .line 187
    .local v11, "unit":Ljava/lang/String;
    :goto_2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    .line 188
    .local v6, "now":J
    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->lastSpeedWarningTime:J

    sub-long v4, v6, v12

    .line 189
    .local v4, "diff":J
    sget-wide v12, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->DISCARD_INTERVAL:J

    cmp-long v12, v4, v12

    if-gtz v12, :cond_6

    .line 190
    sget-object v12, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "don\'t post speed tts:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 156
    .end local v4    # "diff":J
    .end local v6    # "now":J
    .end local v8    # "speedToConsider":I
    .end local v11    # "unit":Ljava/lang/String;
    :cond_4
    move-object/from16 v0, p0

    iget v12, v0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->warningSpeed:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->warningSpeed:I

    div-int/lit8 v13, v13, 0xa

    add-int v8, v12, v13

    .restart local v8    # "speedToConsider":I
    goto/16 :goto_1

    .line 163
    :cond_5
    if-nez v2, :cond_2

    .line 164
    sget-object v12, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->tag:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " speed exceeded current["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p3

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "] threshold["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "] allowed["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "] "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    .line 165
    invoke-virtual {v9}, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->name()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 164
    invoke-virtual {v12, v13}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 172
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    iget-object v11, v12, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->TTS_MILES:Ljava/lang/String;

    .line 173
    .restart local v11    # "unit":Ljava/lang/String;
    goto/16 :goto_2

    .line 176
    .end local v11    # "unit":Ljava/lang/String;
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    iget-object v11, v12, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->TTS_KMS:Ljava/lang/String;

    .line 177
    .restart local v11    # "unit":Ljava/lang/String;
    goto/16 :goto_2

    .line 180
    .end local v11    # "unit":Ljava/lang/String;
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    iget-object v11, v12, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->TTS_METERS:Ljava/lang/String;

    .line 181
    .restart local v11    # "unit":Ljava/lang/String;
    goto/16 :goto_2

    .line 193
    .restart local v4    # "diff":J
    .restart local v6    # "now":J
    :cond_6
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->context:Landroid/content/Context;

    const v13, 0x7f0902c4

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-virtual {v12, v13, v14}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 194
    .local v10, "speedWarning":Ljava/lang/String;
    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->lastSpeedWarningTime:J

    .line 195
    sget-object v12, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->SPEECH_SPEED_WARNING:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    const/4 v13, 0x0

    invoke-static {v10, v12, v13}, Lcom/navdy/hud/app/framework/voice/TTSUtils;->sendSpeechRequest(Ljava/lang/String;Lcom/navdy/service/library/events/audio/SpeechRequest$Category;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 170
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private onSpeedExceededEnd(Ljava/lang/String;II)V
    .locals 3
    .param p1, "roadName"    # Ljava/lang/String;
    .param p2, "speedLimit"    # I
    .param p3, "currentSpeed"    # I

    .prologue
    .line 199
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->speedWarningOn:Z

    .line 200
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->warningSpeed:I

    .line 201
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->bus:Lcom/squareup/otto/Bus;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->SPEED_NORMAL:Lcom/navdy/hud/app/maps/MapEvents$SpeedWarning;

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 202
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " speed normal:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " limit="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " current="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 203
    return-void
.end method


# virtual methods
.method public onPositionUpdated(Lcom/here/android/mpa/common/GeoPosition;)V
    .locals 3
    .param p1, "geoPosition"    # Lcom/here/android/mpa/common/GeoPosition;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 207
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->checkSpeedRunnable:Ljava/lang/Runnable;

    const/16 v2, 0xc

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 208
    return-void
.end method

.method public start()V
    .locals 4

    .prologue
    .line 118
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->registered:Z

    if-nez v0, :cond_0

    .line 119
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->registered:Z

    .line 120
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 121
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->periodicRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 122
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->periodicRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 123
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "started"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 125
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 128
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->registered:Z

    if-eqz v0, :cond_0

    .line 129
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->registered:Z

    .line 130
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->unregister(Ljava/lang/Object;)V

    .line 131
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->periodicRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 132
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "stopped"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 134
    :cond_0
    return-void
.end method
