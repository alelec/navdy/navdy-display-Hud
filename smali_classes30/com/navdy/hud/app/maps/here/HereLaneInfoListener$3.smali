.class Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;
.super Ljava/lang/Object;
.source "HereLaneInfoListener.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->onLaneInformation(Ljava/util/List;Lcom/here/android/mpa/common/RoadElement;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

.field final synthetic val$laneInfoList:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;Ljava/util/List;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->val$laneInfoList:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 33

    .prologue
    .line 76
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->val$laneInfoList:Ljava/util/List;

    move-object/from16 v27, v0

    if-eqz v27, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->val$laneInfoList:Ljava/util/List;

    move-object/from16 v27, v0

    invoke-interface/range {v27 .. v27}, Ljava/util/List;->size()I

    move-result v27

    if-nez v27, :cond_2

    .line 77
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->lastLanesData:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$200(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;

    move-result-object v27

    if-eqz v27, :cond_1

    .line 78
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    # setter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->lastLanesData:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;
    invoke-static/range {v27 .. v28}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$202(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;)Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;

    .line 79
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->hideLaneInfo()V

    .line 383
    :cond_1
    :goto_0
    return-void

    .line 85
    :cond_2
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->val$laneInfoList:Ljava/util/List;

    move-object/from16 v27, v0

    invoke-interface/range {v27 .. v27}, Ljava/util/List;->size()I

    move-result v19

    .line 102
    .local v19, "numLanes":I
    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v27

    const/16 v28, 0x2

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v27

    if-eqz v27, :cond_4

    .line 103
    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v27

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "onShowLaneInfo: size="

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 104
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->val$laneInfoList:Ljava/util/List;

    move-object/from16 v27, v0

    invoke-interface/range {v27 .. v27}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v27

    :cond_3
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    if-eqz v28, :cond_4

    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/here/android/mpa/guidance/LaneInformation;

    .line 105
    .local v12, "laneInfo":Lcom/here/android/mpa/guidance/LaneInformation;
    invoke-virtual {v12}, Lcom/here/android/mpa/guidance/LaneInformation;->getRecommendationState()Lcom/here/android/mpa/guidance/LaneInformation$RecommendationState;

    move-result-object v24

    .line 106
    .local v24, "recommendationState":Lcom/here/android/mpa/guidance/LaneInformation$RecommendationState;
    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v28

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "onShowLaneInfo:recommended state:"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v24 .. v24}, Lcom/here/android/mpa/guidance/LaneInformation$RecommendationState;->name()Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 107
    invoke-virtual {v12}, Lcom/here/android/mpa/guidance/LaneInformation;->getDirections()Ljava/util/EnumSet;

    move-result-object v5

    .line 108
    .local v5, "directions":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/here/android/mpa/guidance/LaneInformation$Direction;>;"
    if-eqz v5, :cond_3

    .line 109
    invoke-virtual {v5}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v28

    :goto_1
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v29

    if-eqz v29, :cond_3

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    .line 110
    .local v4, "direction":Lcom/here/android/mpa/guidance/LaneInformation$Direction;
    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v29

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "onShowLaneInfo:direction:"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual {v4}, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->name()Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 380
    .end local v4    # "direction":Lcom/here/android/mpa/guidance/LaneInformation$Direction;
    .end local v5    # "directions":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/here/android/mpa/guidance/LaneInformation$Direction;>;"
    .end local v12    # "laneInfo":Lcom/here/android/mpa/guidance/LaneInformation;
    .end local v19    # "numLanes":I
    .end local v24    # "recommendationState":Lcom/here/android/mpa/guidance/LaneInformation$RecommendationState;
    :catch_0
    move-exception v26

    .line 381
    .local v26, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 116
    .end local v26    # "t":Ljava/lang/Throwable;
    .restart local v19    # "numLanes":I
    :cond_4
    const/4 v6, 0x0

    .line 117
    .local v6, "hasHighlyRecommendedLanes":I
    const/4 v8, 0x0

    .line 118
    .local v8, "hasRecommendedLanes":I
    const/4 v7, 0x0

    .line 120
    .local v7, "hasNonRecommendedLanes":I
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_2
    move/from16 v0, v19

    if-ge v9, v0, :cond_5

    .line 121
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->val$laneInfoList:Ljava/util/List;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/here/android/mpa/guidance/LaneInformation;

    .line 122
    .restart local v12    # "laneInfo":Lcom/here/android/mpa/guidance/LaneInformation;
    invoke-virtual {v12}, Lcom/here/android/mpa/guidance/LaneInformation;->getRecommendationState()Lcom/here/android/mpa/guidance/LaneInformation$RecommendationState;

    move-result-object v23

    .line 123
    .local v23, "position":Lcom/here/android/mpa/guidance/LaneInformation$RecommendationState;
    sget-object v27, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$4;->$SwitchMap$com$here$android$mpa$guidance$LaneInformation$RecommendationState:[I

    invoke-virtual/range {v23 .. v23}, Lcom/here/android/mpa/guidance/LaneInformation$RecommendationState;->ordinal()I

    move-result v28

    aget v27, v27, v28

    packed-switch v27, :pswitch_data_0

    .line 120
    :goto_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 125
    :pswitch_0
    add-int/lit8 v7, v7, 0x1

    .line 126
    goto :goto_3

    .line 129
    :pswitch_1
    add-int/lit8 v6, v6, 0x1

    .line 130
    goto :goto_3

    .line 133
    :pswitch_2
    add-int/lit8 v8, v8, 0x1

    .line 134
    goto :goto_3

    .line 141
    .end local v12    # "laneInfo":Lcom/here/android/mpa/guidance/LaneInformation;
    .end local v23    # "position":Lcom/here/android/mpa/guidance/LaneInformation$RecommendationState;
    :cond_5
    if-nez v6, :cond_7

    if-nez v8, :cond_7

    .line 143
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->lastLanesData:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$200(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;

    move-result-object v27

    if-eqz v27, :cond_6

    .line 144
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    # setter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->lastLanesData:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;
    invoke-static/range {v27 .. v28}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$202(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;)Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;

    .line 145
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->bus:Lcom/squareup/otto/Bus;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$400(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Lcom/squareup/otto/Bus;

    move-result-object v27

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->HIDE_LANE_INFO:Lcom/navdy/hud/app/maps/MapEvents$HideTrafficLaneInfo;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$300()Lcom/navdy/hud/app/maps/MapEvents$HideTrafficLaneInfo;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 148
    :cond_6
    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v27

    const/16 v28, 0x2

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v27

    if-eqz v27, :cond_1

    .line 149
    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v27

    const-string v28, "Invalid lane data"

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 159
    :cond_7
    const/16 v20, 0x0

    .line 160
    .local v20, "onRouteDirection":Lcom/here/android/mpa/guidance/LaneInformation$Direction;
    const/4 v15, 0x0

    .line 161
    .local v15, "needOnRouteDirection":Z
    const/16 v16, 0x0

    .line 163
    .local v16, "needOnRouteDirectionList":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/here/android/mpa/guidance/LaneInformation$Direction;>;"
    const/16 v21, 0x0

    .line 164
    .local v21, "onRouteHighlyRecommendedDirection":Lcom/here/android/mpa/guidance/LaneInformation$Direction;
    const/16 v17, 0x0

    .line 166
    .local v17, "needOnRouteHighlyRecommendedDirectionList":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/here/android/mpa/guidance/LaneInformation$Direction;>;"
    const/16 v22, 0x0

    .line 167
    .local v22, "onRouteRecommendedDirection":Lcom/here/android/mpa/guidance/LaneInformation$Direction;
    const/16 v18, 0x0

    .line 170
    .local v18, "needOnRouteRecommendedDirectionList":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/here/android/mpa/guidance/LaneInformation$Direction;>;"
    const/4 v9, 0x0

    :goto_4
    move/from16 v0, v19

    if-ge v9, v0, :cond_b

    .line 171
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->val$laneInfoList:Ljava/util/List;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/here/android/mpa/guidance/LaneInformation;

    .line 172
    .restart local v12    # "laneInfo":Lcom/here/android/mpa/guidance/LaneInformation;
    invoke-virtual {v12}, Lcom/here/android/mpa/guidance/LaneInformation;->getRecommendationState()Lcom/here/android/mpa/guidance/LaneInformation$RecommendationState;

    move-result-object v23

    .line 173
    .restart local v23    # "position":Lcom/here/android/mpa/guidance/LaneInformation$RecommendationState;
    sget-object v27, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$4;->$SwitchMap$com$here$android$mpa$guidance$LaneInformation$RecommendationState:[I

    invoke-virtual/range {v23 .. v23}, Lcom/here/android/mpa/guidance/LaneInformation$RecommendationState;->ordinal()I

    move-result v28

    aget v27, v27, v28

    packed-switch v27, :pswitch_data_1

    .line 170
    :cond_8
    :goto_5
    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    .line 175
    :pswitch_3
    if-nez v21, :cond_8

    .line 176
    invoke-virtual {v12}, Lcom/here/android/mpa/guidance/LaneInformation;->getDirections()Ljava/util/EnumSet;

    move-result-object v5

    .line 177
    .restart local v5    # "directions":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/here/android/mpa/guidance/LaneInformation$Direction;>;"
    if-eqz v5, :cond_8

    invoke-virtual {v5}, Ljava/util/EnumSet;->size()I

    move-result v27

    if-lez v27, :cond_8

    .line 178
    invoke-virtual {v5}, Ljava/util/EnumSet;->size()I

    move-result v27

    const/16 v28, 0x1

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_9

    .line 179
    invoke-virtual {v5}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v21

    .end local v21    # "onRouteHighlyRecommendedDirection":Lcom/here/android/mpa/guidance/LaneInformation$Direction;
    check-cast v21, Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    .line 180
    .restart local v21    # "onRouteHighlyRecommendedDirection":Lcom/here/android/mpa/guidance/LaneInformation$Direction;
    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v27

    const/16 v28, 0x2

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v27

    if-eqz v27, :cond_8

    .line 181
    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v27

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "onRouteDirection:"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_5

    .line 184
    :cond_9
    const/4 v15, 0x1

    .line 185
    move-object/from16 v17, v5

    goto :goto_5

    .line 193
    .end local v5    # "directions":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/here/android/mpa/guidance/LaneInformation$Direction;>;"
    :pswitch_4
    if-nez v22, :cond_8

    .line 194
    invoke-virtual {v12}, Lcom/here/android/mpa/guidance/LaneInformation;->getDirections()Ljava/util/EnumSet;

    move-result-object v5

    .line 195
    .restart local v5    # "directions":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/here/android/mpa/guidance/LaneInformation$Direction;>;"
    if-eqz v5, :cond_8

    invoke-virtual {v5}, Ljava/util/EnumSet;->size()I

    move-result v27

    if-lez v27, :cond_8

    .line 196
    invoke-virtual {v5}, Ljava/util/EnumSet;->size()I

    move-result v27

    const/16 v28, 0x1

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_a

    .line 197
    invoke-virtual {v5}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    .end local v22    # "onRouteRecommendedDirection":Lcom/here/android/mpa/guidance/LaneInformation$Direction;
    check-cast v22, Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    .line 198
    .restart local v22    # "onRouteRecommendedDirection":Lcom/here/android/mpa/guidance/LaneInformation$Direction;
    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v27

    const/16 v28, 0x2

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v27

    if-eqz v27, :cond_8

    .line 199
    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v27

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "onRouteDirection:"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 202
    :cond_a
    const/4 v15, 0x1

    .line 203
    move-object/from16 v18, v5

    goto/16 :goto_5

    .line 212
    .end local v5    # "directions":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/here/android/mpa/guidance/LaneInformation$Direction;>;"
    .end local v12    # "laneInfo":Lcom/here/android/mpa/guidance/LaneInformation;
    .end local v23    # "position":Lcom/here/android/mpa/guidance/LaneInformation$RecommendationState;
    :cond_b
    if-lez v6, :cond_10

    .line 213
    if-eqz v21, :cond_e

    .line 215
    move-object/from16 v20, v21

    .line 236
    :goto_6
    if-nez v20, :cond_13

    if-eqz v15, :cond_13

    .line 237
    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v27

    const/16 v28, 0x2

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v27

    if-eqz v27, :cond_c

    .line 238
    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v27

    const-string v28, "onRouteDirection is needed, but not available"

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 243
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v16

    # invokes: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->getOnRouteDirectionFromManeuver(Ljava/util/EnumSet;)Lcom/here/android/mpa/guidance/LaneInformation$Direction;
    invoke-static {v0, v1}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$500(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;Ljava/util/EnumSet;)Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    move-result-object v20

    .line 245
    if-nez v20, :cond_12

    .line 246
    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v27

    const/16 v28, 0x2

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v27

    if-eqz v27, :cond_d

    .line 247
    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v27

    const-string v28, "onRouteDirection:could not get onroute direction from maneuver"

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 250
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->lastLanesData:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$200(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;

    move-result-object v27

    if-eqz v27, :cond_1

    .line 251
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    # setter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->lastLanesData:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;
    invoke-static/range {v27 .. v28}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$202(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;)Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;

    .line 252
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->bus:Lcom/squareup/otto/Bus;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$400(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Lcom/squareup/otto/Bus;

    move-result-object v27

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->HIDE_LANE_INFO:Lcom/navdy/hud/app/maps/MapEvents$HideTrafficLaneInfo;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$300()Lcom/navdy/hud/app/maps/MapEvents$HideTrafficLaneInfo;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 219
    :cond_e
    if-eqz v22, :cond_f

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_f

    .line 221
    move-object/from16 v20, v22

    goto :goto_6

    .line 223
    :cond_f
    move-object/from16 v16, v17

    goto :goto_6

    .line 227
    :cond_10
    if-eqz v22, :cond_11

    .line 229
    move-object/from16 v20, v22

    goto :goto_6

    .line 231
    :cond_11
    move-object/from16 v16, v18

    goto/16 :goto_6

    .line 257
    :cond_12
    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v27

    const/16 v28, 0x2

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v27

    if-eqz v27, :cond_13

    .line 258
    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v27

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "onRouteDirection:got on route direction:"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 264
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempLanes:Ljava/util/ArrayList;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$600(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Ljava/util/ArrayList;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->clear()V

    .line 267
    const/4 v9, 0x0

    :goto_7
    move/from16 v0, v19

    if-ge v9, v0, :cond_1f

    .line 268
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->val$laneInfoList:Ljava/util/List;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/here/android/mpa/guidance/LaneInformation;

    .line 269
    .restart local v12    # "laneInfo":Lcom/here/android/mpa/guidance/LaneInformation;
    invoke-virtual {v12}, Lcom/here/android/mpa/guidance/LaneInformation;->getDirections()Ljava/util/EnumSet;

    move-result-object v5

    .line 270
    .restart local v5    # "directions":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/here/android/mpa/guidance/LaneInformation$Direction;>;"
    if-eqz v5, :cond_14

    invoke-virtual {v5}, Ljava/util/EnumSet;->size()I

    move-result v27

    if-nez v27, :cond_15

    .line 271
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempLanes:Ljava/util/ArrayList;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$600(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Ljava/util/ArrayList;

    move-result-object v27

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->EMPTY_LANE_DATA:Lcom/navdy/hud/app/maps/MapEvents$LaneData;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$700()Lcom/navdy/hud/app/maps/MapEvents$LaneData;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 267
    :goto_8
    add-int/lit8 v9, v9, 0x1

    goto :goto_7

    .line 275
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempDirections:Ljava/util/ArrayList;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$800(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Ljava/util/ArrayList;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->clear()V

    .line 277
    invoke-virtual {v5}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v27

    :goto_9
    :pswitch_5
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    if-eqz v28, :cond_16

    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    .line 278
    .restart local v4    # "direction":Lcom/here/android/mpa/guidance/LaneInformation$Direction;
    sget-object v28, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$4;->$SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction:[I

    invoke-virtual {v4}, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->ordinal()I

    move-result v29

    aget v28, v28, v29

    packed-switch v28, :pswitch_data_2

    .line 283
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v28, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempDirections:Ljava/util/ArrayList;
    invoke-static/range {v28 .. v28}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$800(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Ljava/util/ArrayList;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 288
    .end local v4    # "direction":Lcom/here/android/mpa/guidance/LaneInformation$Direction;
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempDirections:Ljava/util/ArrayList;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$800(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Ljava/util/ArrayList;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    move-result v14

    .line 289
    .local v14, "nDirections":I
    if-nez v14, :cond_17

    .line 291
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempLanes:Ljava/util/ArrayList;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$600(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Ljava/util/ArrayList;

    move-result-object v27

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->EMPTY_LANE_DATA:Lcom/navdy/hud/app/maps/MapEvents$LaneData;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$700()Lcom/navdy/hud/app/maps/MapEvents$LaneData;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 295
    :cond_17
    invoke-virtual {v12}, Lcom/here/android/mpa/guidance/LaneInformation;->getRecommendationState()Lcom/here/android/mpa/guidance/LaneInformation$RecommendationState;

    move-result-object v25

    .line 296
    .local v25, "recommendedState":Lcom/here/android/mpa/guidance/LaneInformation$RecommendationState;
    sget-object v27, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$4;->$SwitchMap$com$here$android$mpa$guidance$LaneInformation$RecommendationState:[I

    invoke-virtual/range {v25 .. v25}, Lcom/here/android/mpa/guidance/LaneInformation$RecommendationState;->ordinal()I

    move-result v28

    aget v27, v27, v28

    packed-switch v27, :pswitch_data_3

    goto :goto_8

    .line 298
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempLanes:Ljava/util/ArrayList;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$600(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Ljava/util/ArrayList;

    move-result-object v27

    new-instance v28, Lcom/navdy/hud/app/maps/MapEvents$LaneData;

    sget-object v29, Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;->OFF_ROUTE:Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v30, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempDirections:Ljava/util/ArrayList;
    invoke-static/range {v30 .. v30}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$800(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Ljava/util/ArrayList;

    move-result-object v30

    sget-object v31, Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;->OFF_ROUTE:Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;

    const/16 v32, 0x0

    invoke-static/range {v30 .. v32}, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->getDrawable(Ljava/util/List;Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;Lcom/here/android/mpa/guidance/LaneInformation$Direction;)[Landroid/graphics/drawable/Drawable;

    move-result-object v30

    invoke-direct/range {v28 .. v30}, Lcom/navdy/hud/app/maps/MapEvents$LaneData;-><init>(Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;[Landroid/graphics/drawable/Drawable;)V

    invoke-virtual/range {v27 .. v28}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_8

    .line 302
    :pswitch_7
    if-eqz v20, :cond_1a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempDirections:Ljava/util/ArrayList;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$800(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Ljava/util/ArrayList;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    move-result v27

    const/16 v28, 0x1

    move/from16 v0, v27

    move/from16 v1, v28

    if-le v0, v1, :cond_1a

    .line 305
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempDirections2:Ljava/util/ArrayList;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$900(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Ljava/util/ArrayList;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->clear()V

    .line 306
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempDirections:Ljava/util/ArrayList;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$800(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Ljava/util/ArrayList;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .line 307
    .local v10, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/here/android/mpa/guidance/LaneInformation$Direction;>;"
    :cond_18
    :goto_a
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v27

    if-eqz v27, :cond_19

    .line 308
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    .line 309
    .local v3, "d":Lcom/here/android/mpa/guidance/LaneInformation$Direction;
    move-object/from16 v0, v20

    if-ne v3, v0, :cond_18

    .line 310
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempDirections2:Ljava/util/ArrayList;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$900(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Ljava/util/ArrayList;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 311
    invoke-interface {v10}, Ljava/util/Iterator;->remove()V

    goto :goto_a

    .line 314
    .end local v3    # "d":Lcom/here/android/mpa/guidance/LaneInformation$Direction;
    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempDirections:Ljava/util/ArrayList;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$800(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Ljava/util/ArrayList;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v28, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempDirections2:Ljava/util/ArrayList;
    invoke-static/range {v28 .. v28}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$900(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Ljava/util/ArrayList;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 316
    .end local v10    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/here/android/mpa/guidance/LaneInformation$Direction;>;"
    :cond_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempLanes:Ljava/util/ArrayList;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$600(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Ljava/util/ArrayList;

    move-result-object v27

    new-instance v28, Lcom/navdy/hud/app/maps/MapEvents$LaneData;

    sget-object v29, Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;->ON_ROUTE:Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v30, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempDirections:Ljava/util/ArrayList;
    invoke-static/range {v30 .. v30}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$800(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Ljava/util/ArrayList;

    move-result-object v30

    sget-object v31, Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;->ON_ROUTE:Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    move-object/from16 v2, v20

    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->getDrawable(Ljava/util/List;Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;Lcom/here/android/mpa/guidance/LaneInformation$Direction;)[Landroid/graphics/drawable/Drawable;

    move-result-object v30

    invoke-direct/range {v28 .. v30}, Lcom/navdy/hud/app/maps/MapEvents$LaneData;-><init>(Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;[Landroid/graphics/drawable/Drawable;)V

    invoke-virtual/range {v27 .. v28}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_8

    .line 320
    :pswitch_8
    if-eqz v20, :cond_1d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempDirections:Ljava/util/ArrayList;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$800(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Ljava/util/ArrayList;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    move-result v27

    const/16 v28, 0x1

    move/from16 v0, v27

    move/from16 v1, v28

    if-le v0, v1, :cond_1d

    .line 323
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempDirections2:Ljava/util/ArrayList;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$900(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Ljava/util/ArrayList;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->clear()V

    .line 324
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempDirections:Ljava/util/ArrayList;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$800(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Ljava/util/ArrayList;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .line 325
    .restart local v10    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/here/android/mpa/guidance/LaneInformation$Direction;>;"
    :cond_1b
    :goto_b
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v27

    if-eqz v27, :cond_1c

    .line 326
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    .line 327
    .restart local v3    # "d":Lcom/here/android/mpa/guidance/LaneInformation$Direction;
    move-object/from16 v0, v20

    if-ne v3, v0, :cond_1b

    .line 328
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempDirections2:Ljava/util/ArrayList;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$900(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Ljava/util/ArrayList;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 329
    invoke-interface {v10}, Ljava/util/Iterator;->remove()V

    goto :goto_b

    .line 333
    .end local v3    # "d":Lcom/here/android/mpa/guidance/LaneInformation$Direction;
    :cond_1c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempDirections:Ljava/util/ArrayList;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$800(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Ljava/util/ArrayList;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v28, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempDirections2:Ljava/util/ArrayList;
    invoke-static/range {v28 .. v28}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$900(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Ljava/util/ArrayList;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 335
    .end local v10    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/here/android/mpa/guidance/LaneInformation$Direction;>;"
    :cond_1d
    if-nez v6, :cond_1e

    .line 336
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempLanes:Ljava/util/ArrayList;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$600(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Ljava/util/ArrayList;

    move-result-object v27

    new-instance v28, Lcom/navdy/hud/app/maps/MapEvents$LaneData;

    sget-object v29, Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;->ON_ROUTE:Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v30, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempDirections:Ljava/util/ArrayList;
    invoke-static/range {v30 .. v30}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$800(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Ljava/util/ArrayList;

    move-result-object v30

    sget-object v31, Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;->ON_ROUTE:Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    move-object/from16 v2, v20

    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->getDrawable(Ljava/util/List;Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;Lcom/here/android/mpa/guidance/LaneInformation$Direction;)[Landroid/graphics/drawable/Drawable;

    move-result-object v30

    invoke-direct/range {v28 .. v30}, Lcom/navdy/hud/app/maps/MapEvents$LaneData;-><init>(Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;[Landroid/graphics/drawable/Drawable;)V

    invoke-virtual/range {v27 .. v28}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_8

    .line 338
    :cond_1e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempLanes:Ljava/util/ArrayList;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$600(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Ljava/util/ArrayList;

    move-result-object v27

    new-instance v28, Lcom/navdy/hud/app/maps/MapEvents$LaneData;

    sget-object v29, Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;->OFF_ROUTE:Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v30, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempDirections:Ljava/util/ArrayList;
    invoke-static/range {v30 .. v30}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$800(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Ljava/util/ArrayList;

    move-result-object v30

    sget-object v31, Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;->OFF_ROUTE:Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;

    const/16 v32, 0x0

    invoke-static/range {v30 .. v32}, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->getDrawable(Ljava/util/List;Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;Lcom/here/android/mpa/guidance/LaneInformation$Direction;)[Landroid/graphics/drawable/Drawable;

    move-result-object v30

    invoke-direct/range {v28 .. v30}, Lcom/navdy/hud/app/maps/MapEvents$LaneData;-><init>(Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;[Landroid/graphics/drawable/Drawable;)V

    invoke-virtual/range {v27 .. v28}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_8

    .line 343
    :pswitch_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempLanes:Ljava/util/ArrayList;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$600(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Ljava/util/ArrayList;

    move-result-object v27

    new-instance v28, Lcom/navdy/hud/app/maps/MapEvents$LaneData;

    sget-object v29, Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;->OFF_ROUTE:Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v30, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempDirections:Ljava/util/ArrayList;
    invoke-static/range {v30 .. v30}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$800(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Ljava/util/ArrayList;

    move-result-object v30

    sget-object v31, Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;->OFF_ROUTE:Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;

    const/16 v32, 0x0

    invoke-static/range {v30 .. v32}, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->getDrawable(Ljava/util/List;Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;Lcom/here/android/mpa/guidance/LaneInformation$Direction;)[Landroid/graphics/drawable/Drawable;

    move-result-object v30

    invoke-direct/range {v28 .. v30}, Lcom/navdy/hud/app/maps/MapEvents$LaneData;-><init>(Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;[Landroid/graphics/drawable/Drawable;)V

    invoke-virtual/range {v27 .. v28}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_8

    .line 348
    .end local v5    # "directions":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/here/android/mpa/guidance/LaneInformation$Direction;>;"
    .end local v12    # "laneInfo":Lcom/here/android/mpa/guidance/LaneInformation;
    .end local v14    # "nDirections":I
    .end local v25    # "recommendedState":Lcom/here/android/mpa/guidance/LaneInformation$RecommendationState;
    :cond_1f
    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v27

    const/16 v28, 0x2

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v27

    if-eqz v27, :cond_22

    .line 349
    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v27

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "LaneData: size="

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v29, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempLanes:Ljava/util/ArrayList;
    invoke-static/range {v29 .. v29}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$600(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Ljava/util/ArrayList;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->size()I

    move-result v29

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 350
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempLanes:Ljava/util/ArrayList;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$600(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Ljava/util/ArrayList;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v27

    :cond_20
    :goto_c
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    if-eqz v28, :cond_22

    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/navdy/hud/app/maps/MapEvents$LaneData;

    .line 351
    .local v11, "laneData":Lcom/navdy/hud/app/maps/MapEvents$LaneData;
    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v28

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "LaneData:position: "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    iget-object v0, v11, Lcom/navdy/hud/app/maps/MapEvents$LaneData;->position:Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;

    move-object/from16 v30, v0

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 352
    iget-object v0, v11, Lcom/navdy/hud/app/maps/MapEvents$LaneData;->icons:[Landroid/graphics/drawable/Drawable;

    move-object/from16 v28, v0

    if-eqz v28, :cond_21

    .line 353
    const/4 v9, 0x0

    :goto_d
    iget-object v0, v11, Lcom/navdy/hud/app/maps/MapEvents$LaneData;->icons:[Landroid/graphics/drawable/Drawable;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    array-length v0, v0

    move/from16 v28, v0

    move/from16 v0, v28

    if-ge v9, v0, :cond_20

    .line 354
    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v28

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "LaneData:icons"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    add-int/lit8 v30, v9, 0x1

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, " = "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    iget-object v0, v11, Lcom/navdy/hud/app/maps/MapEvents$LaneData;->icons:[Landroid/graphics/drawable/Drawable;

    move-object/from16 v30, v0

    aget-object v30, v30, v9

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 353
    add-int/lit8 v9, v9, 0x1

    goto :goto_d

    .line 357
    :cond_21
    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v28

    const-string v29, "LaneData:icons: null"

    invoke-virtual/range {v28 .. v29}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_c

    .line 365
    .end local v11    # "laneData":Lcom/navdy/hud/app/maps/MapEvents$LaneData;
    :cond_22
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->lastLanesData:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$200(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;

    move-result-object v27

    if-eqz v27, :cond_24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->lastLanesData:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$200(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;

    move-result-object v27

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;->laneData:Ljava/util/ArrayList;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v28, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempLanes:Ljava/util/ArrayList;
    invoke-static/range {v28 .. v28}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$600(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Ljava/util/ArrayList;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->compareLaneData(Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    move-result v27

    if-eqz v27, :cond_24

    .line 366
    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v27

    const/16 v28, 0x2

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v27

    if-eqz v27, :cond_23

    .line 367
    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v27

    const-string v28, "LaneData: lanedata is same as last"

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 369
    :cond_23
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempLanes:Ljava/util/ArrayList;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$600(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Ljava/util/ArrayList;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_0

    .line 373
    :cond_24
    new-instance v13, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempLanes:Ljava/util/ArrayList;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$600(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Ljava/util/ArrayList;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-direct {v13, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 374
    .local v13, "lanes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/hud/app/maps/MapEvents$LaneData;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v27, v0

    new-instance v28, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;

    move-object/from16 v0, v28

    invoke-direct {v0, v13}, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;-><init>(Ljava/util/ArrayList;)V

    # setter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->lastLanesData:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;
    invoke-static/range {v27 .. v28}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$202(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;)Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;

    .line 375
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->tempLanes:Ljava/util/ArrayList;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$600(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Ljava/util/ArrayList;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->clear()V

    .line 376
    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v27

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "onShowLaneInfo::"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v29

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 377
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->running:Z
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$1000(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Z

    move-result v27

    if-eqz v27, :cond_1

    .line 378
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v27, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->bus:Lcom/squareup/otto/Bus;
    invoke-static/range {v27 .. v27}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$400(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Lcom/squareup/otto/Bus;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    move-object/from16 v28, v0

    # getter for: Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->lastLanesData:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;
    invoke-static/range {v28 .. v28}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->access$200(Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;)Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficLaneInfo;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 123
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 173
    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 278
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_5
    .end packed-switch

    .line 296
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method
