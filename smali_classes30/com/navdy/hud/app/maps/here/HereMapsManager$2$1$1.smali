.class Lcom/navdy/hud/app/maps/here/HereMapsManager$2$1$1;
.super Ljava/lang/Object;
.source "HereMapsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereMapsManager$2$1;->onGetMapPackagesComplete(Lcom/here/android/mpa/odml/MapPackage;Lcom/here/android/mpa/odml/MapLoader$ResultCode;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/navdy/hud/app/maps/here/HereMapsManager$2$1;

.field final synthetic val$mapPackage:Lcom/here/android/mpa/odml/MapPackage;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereMapsManager$2$1;Lcom/here/android/mpa/odml/MapPackage;)V
    .locals 0
    .param p1, "this$2"    # Lcom/navdy/hud/app/maps/here/HereMapsManager$2$1;

    .prologue
    .line 427
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$2$1$1;->this$2:Lcom/navdy/hud/app/maps/here/HereMapsManager$2$1;

    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$2$1$1;->val$mapPackage:Lcom/here/android/mpa/odml/MapPackage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 430
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$2$1$1;->this$2:Lcom/navdy/hud/app/maps/here/HereMapsManager$2$1;

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereMapsManager$2$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$2;

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereMapsManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$2$1$1;->val$mapPackage:Lcom/here/android/mpa/odml/MapPackage;

    sget-object v2, Lcom/here/android/mpa/odml/MapPackage$InstallationState;->INSTALLED:Lcom/here/android/mpa/odml/MapPackage$InstallationState;

    sget-object v3, Lcom/here/android/mpa/odml/MapPackage$InstallationState;->PARTIALLY_INSTALLED:Lcom/here/android/mpa/odml/MapPackage$InstallationState;

    invoke-static {v2, v3}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v2

    # invokes: Lcom/navdy/hud/app/maps/here/HereMapsManager;->printMapPackages(Lcom/here/android/mpa/odml/MapPackage;Ljava/util/EnumSet;)V
    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$2400(Lcom/navdy/hud/app/maps/here/HereMapsManager;Lcom/here/android/mpa/odml/MapPackage;Ljava/util/EnumSet;)V

    .line 431
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initMapLoader: map package count:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$2$1$1;->this$2:Lcom/navdy/hud/app/maps/here/HereMapsManager$2$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$2$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$2;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapsManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapPackageCount:I
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$2500(Lcom/navdy/hud/app/maps/here/HereMapsManager;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 432
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$2$1$1;->this$2:Lcom/navdy/hud/app/maps/here/HereMapsManager$2$1;

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereMapsManager$2$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$2;

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereMapsManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    const/4 v1, 0x1

    # setter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapDataVerified:Z
    invoke-static {v0, v1}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$2602(Lcom/navdy/hud/app/maps/here/HereMapsManager;Z)Z

    .line 433
    return-void
.end method
