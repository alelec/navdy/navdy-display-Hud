.class Lcom/navdy/hud/app/maps/here/HereMapsManager$12;
.super Ljava/lang/Object;
.source "HereMapsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereMapsManager;->hideTraffic()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereMapsManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .prologue
    .line 1165
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$12;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1168
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$12;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$500(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/navdy/hud/app/maps/here/HereMapController;->setTrafficInfoVisible(Z)V

    .line 1169
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "hidetraffic: map off"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1170
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentMapRoute()Lcom/here/android/mpa/mapping/MapRoute;

    move-result-object v0

    .line 1171
    .local v0, "mapRoute":Lcom/here/android/mpa/mapping/MapRoute;
    if-eqz v0, :cond_0

    .line 1172
    invoke-virtual {v0, v3}, Lcom/here/android/mpa/mapping/MapRoute;->setTrafficEnabled(Z)Lcom/here/android/mpa/mapping/MapRoute;

    .line 1173
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "hidetraffic: route off"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1175
    :cond_0
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "hidetraffic: traffic is disabled"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1176
    return-void
.end method
