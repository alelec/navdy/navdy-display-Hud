.class Lcom/navdy/hud/app/maps/here/HereNavigationEventListener$1;
.super Ljava/lang/Object;
.source "HereNavigationEventListener.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;->onNavigationModeChanged()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationEventListener$1;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 33
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationEventListener$1;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;->access$000(Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getHereNavigationState()Lcom/here/android/mpa/guidance/NavigationManager$NavigationMode;

    move-result-object v0

    .line 34
    .local v0, "mode":Lcom/here/android/mpa/guidance/NavigationManager$NavigationMode;
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationEventListener$1;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;->access$000(Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getNavigationState()Lcom/navdy/hud/app/maps/here/HereNavController$State;

    move-result-object v1

    .line 35
    .local v1, "state":Lcom/navdy/hud/app/maps/here/HereNavController$State;
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationEventListener$1;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;->access$200(Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereNavigationEventListener$1;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;->tag:Ljava/lang/String;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;->access$100(Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " onNavigationModeChanged HERE mode="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Navdy="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 36
    return-void
.end method
