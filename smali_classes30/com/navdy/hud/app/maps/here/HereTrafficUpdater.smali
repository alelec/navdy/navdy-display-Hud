.class public Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;
.super Ljava/lang/Object;
.source "HereTrafficUpdater.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$TrackDismissEvent;,
        Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$JamTrackEvent;,
        Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$DistanceTrackEvent;,
        Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;
    }
.end annotation


# static fields
.field private static final MAX_DISTANCE_NOTIFICATION_METERS:I = 0x1388

.field private static final MIN_DISTANCE_NOTIFICATION_METERS:I = 0xc8

.field private static final MIN_JAM_DURATION_SECS:I = 0xb4

.field private static final ONE_MINUTE_SECS:I = 0x3c

.field private static final REFRESH_INTERVAL_MILLIS:J = 0x7530L

.field public static final TRAFFIC_DEBUG_ENABLED:Z


# instance fields
.field private final bus:Lcom/squareup/otto/Bus;

.field private currentEvent:Lcom/here/android/mpa/mapping/TrafficEvent;

.field private currentRemainingTimeInJam:I

.field private currentRequestInfo:Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;

.field private currentRoute:Lcom/here/android/mpa/routing/Route;

.field private currentState:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

.field private dismissEtaUpdate:Ljava/lang/Runnable;

.field private final handler:Landroid/os/Handler;

.field private final logger:Lcom/navdy/service/library/log/Logger;

.field private final navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

.field private onGetEventsListener:Lcom/here/android/mpa/guidance/TrafficUpdater$GetEventsListener;

.field private onRequestListener:Lcom/here/android/mpa/guidance/TrafficUpdater$Listener;

.field private refreshRunnable:Ljava/lang/Runnable;

.field private final tag:Ljava/lang/String;

.field private final trafficUpdater:Lcom/here/android/mpa/guidance/TrafficUpdater;

.field private final verbose:Z


# direct methods
.method public constructor <init>(Lcom/navdy/service/library/log/Logger;Ljava/lang/String;ZLcom/here/android/mpa/guidance/NavigationManager;Lcom/squareup/otto/Bus;)V
    .locals 2
    .param p1, "logger"    # Lcom/navdy/service/library/log/Logger;
    .param p2, "tag"    # Ljava/lang/String;
    .param p3, "verbose"    # Z
    .param p4, "navigationManager"    # Lcom/here/android/mpa/guidance/NavigationManager;
    .param p5, "bus"    # Lcom/squareup/otto/Bus;

    .prologue
    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$1;-><init>(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->refreshRunnable:Ljava/lang/Runnable;

    .line 66
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$2;-><init>(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->dismissEtaUpdate:Ljava/lang/Runnable;

    .line 73
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$3;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$3;-><init>(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->onRequestListener:Lcom/here/android/mpa/guidance/TrafficUpdater$Listener;

    .line 90
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$4;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$4;-><init>(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->onGetEventsListener:Lcom/here/android/mpa/guidance/TrafficUpdater$GetEventsListener;

    .line 164
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->logger:Lcom/navdy/service/library/log/Logger;

    .line 165
    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->tag:Ljava/lang/String;

    .line 166
    iput-boolean p3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->verbose:Z

    .line 167
    iput-object p4, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    .line 168
    iput-object p5, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->bus:Lcom/squareup/otto/Bus;

    .line 170
    invoke-static {}, Lcom/here/android/mpa/guidance/TrafficUpdater;->getInstance()Lcom/here/android/mpa/guidance/TrafficUpdater;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->trafficUpdater:Lcom/here/android/mpa/guidance/TrafficUpdater;

    .line 171
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->trafficUpdater:Lcom/here/android/mpa/guidance/TrafficUpdater;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/guidance/TrafficUpdater;->enableUpdate(Z)V

    .line 173
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->handler:Landroid/os/Handler;

    .line 175
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->STOPPED:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentState:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    .line 176
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 177
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->startRequest()V

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;)Lcom/squareup/otto/Bus;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->bus:Lcom/squareup/otto/Bus;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;)Lcom/here/android/mpa/routing/Route;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentRoute:Lcom/here/android/mpa/routing/Route;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->tag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;)Lcom/here/android/mpa/guidance/TrafficUpdater$GetEventsListener;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->onGetEventsListener:Lcom/here/android/mpa/guidance/TrafficUpdater$GetEventsListener;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;)Lcom/here/android/mpa/guidance/TrafficUpdater;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->trafficUpdater:Lcom/here/android/mpa/guidance/TrafficUpdater;

    return-object v0
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->refresh()V

    return-void
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;)Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentState:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    return-object v0
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;Lcom/here/android/mpa/routing/Route;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;
    .param p1, "x1"    # Lcom/here/android/mpa/routing/Route;
    .param p2, "x2"    # Ljava/util/List;

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->processEvents(Lcom/here/android/mpa/routing/Route;Ljava/util/List;)V

    return-void
.end method

.method private cancelRequest()V
    .locals 4

    .prologue
    .line 428
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentRequestInfo:Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;

    if-nez v0, :cond_0

    .line 429
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "cancelRequest: currentRequestInfo must not be null."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 438
    :goto_0
    return-void

    .line 433
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentRequestInfo:Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;

    invoke-virtual {v0}, Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;->getError()Lcom/here/android/mpa/guidance/TrafficUpdater$Error;

    move-result-object v0

    sget-object v1, Lcom/here/android/mpa/guidance/TrafficUpdater$Error;->NONE:Lcom/here/android/mpa/guidance/TrafficUpdater$Error;

    if-ne v0, v1, :cond_1

    .line 434
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->trafficUpdater:Lcom/here/android/mpa/guidance/TrafficUpdater;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentRequestInfo:Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;

    invoke-virtual {v1}, Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;->getRequestId()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/here/android/mpa/guidance/TrafficUpdater;->cancelRequest(J)V

    .line 437
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentRequestInfo:Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;

    goto :goto_0
.end method

.method private changeState(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;)V
    .locals 3
    .param p1, "newState"    # Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    .prologue
    .line 229
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentState:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    if-ne p1, v0, :cond_0

    .line 269
    :goto_0
    return-void

    .line 233
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->STOPPED:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    if-ne p1, v0, :cond_4

    .line 234
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentState:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->TRACK_DISTANCE:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    if-ne v0, v1, :cond_3

    .line 235
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->stopTrackingDistance()V

    .line 240
    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->stopUpdates()V

    .line 268
    :cond_2
    :goto_2
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentState:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    goto :goto_0

    .line 236
    :cond_3
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentState:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->TRACK_JAM:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    if-ne v0, v1, :cond_1

    .line 237
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->stopTrackingJamTime()V

    goto :goto_1

    .line 241
    :cond_4
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->ACTIVE:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    if-ne p1, v0, :cond_7

    .line 242
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentState:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->STOPPED:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    if-ne v0, v1, :cond_5

    .line 243
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->startUpdates()V

    goto :goto_2

    .line 244
    :cond_5
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentState:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->TRACK_DISTANCE:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    if-ne v0, v1, :cond_6

    .line 245
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->stopTrackingDistance()V

    goto :goto_2

    .line 246
    :cond_6
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentState:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->TRACK_JAM:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    if-ne v0, v1, :cond_2

    .line 247
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->stopTrackingJamTime()V

    goto :goto_2

    .line 249
    :cond_7
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->TRACK_DISTANCE:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    if-ne p1, v0, :cond_9

    .line 250
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentState:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->ACTIVE:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    if-ne v0, v1, :cond_8

    .line 251
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->startTrackingDistance()V

    goto :goto_2

    .line 253
    :cond_8
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " invalid state change from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentState:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 256
    :cond_9
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->TRACK_JAM:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    if-ne p1, v0, :cond_2

    .line 257
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentState:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->ACTIVE:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    if-ne v0, v1, :cond_a

    .line 258
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->startTrackingJamTime()V

    goto :goto_2

    .line 259
    :cond_a
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentState:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->TRACK_DISTANCE:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    if-ne v0, v1, :cond_b

    .line 260
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->stopTrackingDistance()V

    .line 261
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->startTrackingJamTime()V

    goto/16 :goto_2

    .line 263
    :cond_b
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " invalid state change from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentState:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private getRemainingTimeInJam(Lcom/here/android/mpa/routing/Route;Lcom/here/android/mpa/mapping/TrafficEvent;)I
    .locals 18
    .param p1, "route"    # Lcom/here/android/mpa/routing/Route;
    .param p2, "event"    # Lcom/here/android/mpa/mapping/TrafficEvent;

    .prologue
    .line 353
    const-wide/16 v6, 0x0

    .line 354
    .local v6, "remainingTime":D
    invoke-virtual/range {p2 .. p2}, Lcom/here/android/mpa/mapping/TrafficEvent;->getAffectedRoadElements()Ljava/util/List;

    move-result-object v10

    .line 355
    .local v10, "roadElements":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/common/RoadElement;>;"
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v12

    add-int/lit8 v12, v12, -0x1

    invoke-interface {v10, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/here/android/mpa/common/RoadElement;

    .line 357
    .local v3, "lastRoadElementInEvent":Lcom/here/android/mpa/common/RoadElement;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    .line 358
    invoke-virtual {v12}, Lcom/here/android/mpa/guidance/NavigationManager;->getElapsedDistance()J

    move-result-wide v12

    long-to-int v12, v12

    .line 359
    invoke-virtual/range {p1 .. p1}, Lcom/here/android/mpa/routing/Route;->getLength()I

    move-result v13

    .line 357
    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Lcom/here/android/mpa/routing/Route;->getRouteElementsFromLength(II)Lcom/here/android/mpa/routing/RouteElements;

    move-result-object v5

    .line 363
    .local v5, "remainingRouteElementsInRoute":Lcom/here/android/mpa/routing/RouteElements;
    invoke-virtual {v3}, Lcom/here/android/mpa/common/RoadElement;->getIdentifier()Lcom/here/android/mpa/common/Identifier;

    move-result-object v12

    invoke-virtual {v12}, Lcom/here/android/mpa/common/Identifier;->toString()Ljava/lang/String;

    move-result-object v4

    .line 364
    .local v4, "lastRoadElementInEventId":Ljava/lang/String;
    const/4 v2, 0x0

    .line 366
    .local v2, "foundRoadElement":Z
    invoke-virtual {v5}, Lcom/here/android/mpa/routing/RouteElements;->getElements()Ljava/util/List;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_0

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/here/android/mpa/routing/RouteElement;

    .line 367
    .local v11, "routeElement":Lcom/here/android/mpa/routing/RouteElement;
    invoke-virtual {v11}, Lcom/here/android/mpa/routing/RouteElement;->getRoadElement()Lcom/here/android/mpa/common/RoadElement;

    move-result-object v8

    .line 368
    .local v8, "roadElement":Lcom/here/android/mpa/common/RoadElement;
    invoke-virtual {v8}, Lcom/here/android/mpa/common/RoadElement;->getIdentifier()Lcom/here/android/mpa/common/Identifier;

    move-result-object v13

    invoke-virtual {v13}, Lcom/here/android/mpa/common/Identifier;->toString()Ljava/lang/String;

    move-result-object v9

    .line 371
    .local v9, "roadElementId":Ljava/lang/String;
    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 372
    const/4 v2, 0x1

    .line 379
    .end local v8    # "roadElement":Lcom/here/android/mpa/common/RoadElement;
    .end local v9    # "roadElementId":Ljava/lang/String;
    .end local v11    # "routeElement":Lcom/here/android/mpa/routing/RouteElement;
    :cond_0
    if-eqz v2, :cond_2

    .line 380
    double-to-int v12, v6

    .line 382
    :goto_1
    return v12

    .line 375
    .restart local v8    # "roadElement":Lcom/here/android/mpa/common/RoadElement;
    .restart local v9    # "roadElementId":Ljava/lang/String;
    .restart local v11    # "routeElement":Lcom/here/android/mpa/routing/RouteElement;
    :cond_1
    invoke-virtual {v8}, Lcom/here/android/mpa/common/RoadElement;->getGeometryLength()D

    move-result-wide v14

    invoke-virtual {v8}, Lcom/here/android/mpa/common/RoadElement;->getDefaultSpeed()F

    move-result v13

    float-to-double v0, v13

    move-wide/from16 v16, v0

    div-double v14, v14, v16

    add-double/2addr v6, v14

    .line 377
    goto :goto_0

    .line 382
    .end local v8    # "roadElement":Lcom/here/android/mpa/common/RoadElement;
    .end local v9    # "roadElementId":Ljava/lang/String;
    .end local v11    # "routeElement":Lcom/here/android/mpa/routing/RouteElement;
    :cond_2
    const/4 v12, 0x0

    goto :goto_1
.end method

.method private isCurrentRoute(Lcom/here/android/mpa/routing/Route;)Z
    .locals 1
    .param p1, "route"    # Lcom/here/android/mpa/routing/Route;

    .prologue
    .line 349
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentRoute:Lcom/here/android/mpa/routing/Route;

    invoke-virtual {p1, v0}, Lcom/here/android/mpa/routing/Route;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private onDistanceTrackingUpdate(Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener$MapMatchEvent;)V
    .locals 9
    .param p1, "mapMatchEvent"    # Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener$MapMatchEvent;

    .prologue
    const/4 v5, 0x0

    .line 448
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentEvent:Lcom/here/android/mpa/mapping/TrafficEvent;

    if-nez v6, :cond_1

    .line 449
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->tag:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "onDistanceTrackingUpdate triggered while currentEvent is null."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 479
    :cond_0
    :goto_0
    return-void

    .line 453
    :cond_1
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentEvent:Lcom/here/android/mpa/mapping/TrafficEvent;

    invoke-virtual {v6}, Lcom/here/android/mpa/mapping/TrafficEvent;->getSeverity()Lcom/here/android/mpa/mapping/TrafficEvent$Severity;

    move-result-object v6

    invoke-virtual {v6}, Lcom/here/android/mpa/mapping/TrafficEvent$Severity;->getValue()I

    move-result v6

    sget-object v7, Lcom/here/android/mpa/mapping/TrafficEvent$Severity;->VERY_HIGH:Lcom/here/android/mpa/mapping/TrafficEvent$Severity;

    invoke-virtual {v7}, Lcom/here/android/mpa/mapping/TrafficEvent$Severity;->getValue()I

    move-result v7

    if-lt v6, v7, :cond_4

    const/4 v2, 0x1

    .line 454
    .local v2, "isVerySevereEvent":Z
    :goto_1
    const/4 v0, 0x0

    .line 455
    .local v0, "currentRoadId":Ljava/lang/String;
    iget-object v6, p1, Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener$MapMatchEvent;->mapMatch:Lcom/here/android/mpa/common/MatchedGeoPosition;

    invoke-virtual {v6}, Lcom/here/android/mpa/common/MatchedGeoPosition;->getRoadElement()Lcom/here/android/mpa/common/RoadElement;

    move-result-object v4

    .line 456
    .local v4, "roadElement":Lcom/here/android/mpa/common/RoadElement;
    if-eqz v4, :cond_2

    .line 457
    invoke-virtual {v4}, Lcom/here/android/mpa/common/RoadElement;->getIdentifier()Lcom/here/android/mpa/common/Identifier;

    move-result-object v6

    invoke-virtual {v6}, Lcom/here/android/mpa/common/Identifier;->toString()Ljava/lang/String;

    move-result-object v0

    .line 459
    :cond_2
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentEvent:Lcom/here/android/mpa/mapping/TrafficEvent;

    invoke-virtual {v6}, Lcom/here/android/mpa/mapping/TrafficEvent;->getAffectedRoadElements()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/here/android/mpa/common/RoadElement;

    invoke-virtual {v5}, Lcom/here/android/mpa/common/RoadElement;->getIdentifier()Lcom/here/android/mpa/common/Identifier;

    move-result-object v5

    invoke-virtual {v5}, Lcom/here/android/mpa/common/Identifier;->toString()Ljava/lang/String;

    move-result-object v1

    .line 461
    .local v1, "eventFirstRoadId":Ljava/lang/String;
    iget-boolean v5, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->verbose:Z

    if-eqz v5, :cond_3

    .line 462
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->tag:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "Position update: currentRoadId="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "; eventFirstRoadId="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 465
    :cond_3
    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 466
    if-eqz v2, :cond_5

    .line 467
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentRoute:Lcom/here/android/mpa/routing/Route;

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentEvent:Lcom/here/android/mpa/mapping/TrafficEvent;

    invoke-direct {p0, v5, v6}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->getRemainingTimeInJam(Lcom/here/android/mpa/routing/Route;Lcom/here/android/mpa/mapping/TrafficEvent;)I

    move-result v3

    .line 468
    .local v3, "remainingTimeInJam":I
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->tag:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "triggering jam, remaining time="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " sec"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 470
    const/16 v5, 0xb4

    if-lt v3, v5, :cond_5

    .line 471
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->bus:Lcom/squareup/otto/Bus;

    new-instance v6, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$JamTrackEvent;

    iget-object v7, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentRoute:Lcom/here/android/mpa/routing/Route;

    iget-object v8, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentEvent:Lcom/here/android/mpa/mapping/TrafficEvent;

    invoke-direct {v6, p0, v7, v8, v3}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$JamTrackEvent;-><init>(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;Lcom/here/android/mpa/routing/Route;Lcom/here/android/mpa/mapping/TrafficEvent;I)V

    invoke-virtual {v5, v6}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_0

    .end local v0    # "currentRoadId":Ljava/lang/String;
    .end local v1    # "eventFirstRoadId":Ljava/lang/String;
    .end local v2    # "isVerySevereEvent":Z
    .end local v3    # "remainingTimeInJam":I
    .end local v4    # "roadElement":Lcom/here/android/mpa/common/RoadElement;
    :cond_4
    move v2, v5

    .line 453
    goto/16 :goto_1

    .line 476
    .restart local v0    # "currentRoadId":Ljava/lang/String;
    .restart local v1    # "eventFirstRoadId":Ljava/lang/String;
    .restart local v2    # "isVerySevereEvent":Z
    .restart local v4    # "roadElement":Lcom/here/android/mpa/common/RoadElement;
    :cond_5
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentEvent:Lcom/here/android/mpa/mapping/TrafficEvent;

    .line 477
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->ACTIVE:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    invoke-direct {p0, v5}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->changeState(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;)V

    goto/16 :goto_0
.end method

.method private onJamTrackingUpdate()V
    .locals 6

    .prologue
    .line 482
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentRoute:Lcom/here/android/mpa/routing/Route;

    if-nez v3, :cond_1

    .line 483
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->tag:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "onJamTrackingUpdate triggered while currentRoute is null."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 515
    :cond_0
    :goto_0
    return-void

    .line 486
    :cond_1
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentEvent:Lcom/here/android/mpa/mapping/TrafficEvent;

    if-nez v3, :cond_2

    .line 487
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->tag:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "onJamTrackingUpdate triggered while currentEvent is null."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 490
    :cond_2
    iget v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentRemainingTimeInJam:I

    if-nez v3, :cond_3

    .line 491
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->tag:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "onJamTrackingUpdate triggered while currentRemainingTimeInJam is zero."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 495
    :cond_3
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentRoute:Lcom/here/android/mpa/routing/Route;

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentEvent:Lcom/here/android/mpa/mapping/TrafficEvent;

    invoke-direct {p0, v3, v4}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->getRemainingTimeInJam(Lcom/here/android/mpa/routing/Route;Lcom/here/android/mpa/mapping/TrafficEvent;)I

    move-result v2

    .line 497
    .local v2, "remainingTimeInJam":I
    iget-boolean v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->verbose:Z

    if-eqz v3, :cond_4

    .line 498
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->tag:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "Jam time update: remainingTimeInJam="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 501
    :cond_4
    if-lez v2, :cond_5

    .line 502
    iget v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentRemainingTimeInJam:I

    invoke-static {v3}, Lcom/navdy/hud/app/maps/util/MapUtils;->getMinuteCeiling(I)I

    move-result v0

    .line 503
    .local v0, "currentMinuteCeiling":I
    invoke-static {v2}, Lcom/navdy/hud/app/maps/util/MapUtils;->getMinuteCeiling(I)I

    move-result v1

    .line 505
    .local v1, "minuteCeiling":I
    sub-int v3, v0, v1

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    const/16 v4, 0x3c

    if-lt v3, v4, :cond_0

    .line 506
    iput v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentRemainingTimeInJam:I

    .line 509
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->bus:Lcom/squareup/otto/Bus;

    new-instance v4, Lcom/navdy/hud/app/maps/MapEvents$TrafficJamProgressEvent;

    invoke-direct {v4, v1}, Lcom/navdy/hud/app/maps/MapEvents$TrafficJamProgressEvent;-><init>(I)V

    invoke-virtual {v3, v4}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 512
    .end local v0    # "currentMinuteCeiling":I
    .end local v1    # "minuteCeiling":I
    :cond_5
    const/4 v3, 0x0

    iput v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentRemainingTimeInJam:I

    .line 513
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->ACTIVE:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    invoke-direct {p0, v3}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->changeState(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;)V

    goto/16 :goto_0
.end method

.method private processEvents(Lcom/here/android/mpa/routing/Route;Ljava/util/List;)V
    .locals 22
    .param p1, "route"    # Lcom/here/android/mpa/routing/Route;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/here/android/mpa/routing/Route;",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/mapping/TrafficEvent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 272
    .local p2, "events":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/mapping/TrafficEvent;>;"
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 274
    const/4 v9, 0x0

    .line 275
    .local v9, "firstEvent":Lcom/here/android/mpa/mapping/TrafficEvent;
    const/4 v5, 0x0

    .line 277
    .local v5, "backupEvent":Lcom/here/android/mpa/mapping/TrafficEvent;
    new-instance v14, Ljava/util/HashMap;

    invoke-direct {v14}, Ljava/util/HashMap;-><init>()V

    .line 280
    .local v14, "roadElementIdToTrafficEventMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/here/android/mpa/mapping/TrafficEvent;>;"
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :cond_0
    :goto_0
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_1

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/here/android/mpa/mapping/TrafficEvent;

    .line 281
    .local v8, "event":Lcom/here/android/mpa/mapping/TrafficEvent;
    invoke-virtual {v8}, Lcom/here/android/mpa/mapping/TrafficEvent;->getAffectedRoadElements()Ljava/util/List;

    move-result-object v4

    .line 282
    .local v4, "affectedRoadElements":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/common/RoadElement;>;"
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v17

    if-lez v17, :cond_0

    .line 283
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/here/android/mpa/common/RoadElement;

    invoke-virtual/range {v17 .. v17}, Lcom/here/android/mpa/common/RoadElement;->getIdentifier()Lcom/here/android/mpa/common/Identifier;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/here/android/mpa/common/Identifier;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v14, v0, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 287
    .end local v4    # "affectedRoadElements":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/common/RoadElement;>;"
    .end local v8    # "event":Lcom/here/android/mpa/mapping/TrafficEvent;
    :cond_1
    const/4 v6, 0x0

    .line 294
    .local v6, "distanceToLocation":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    move-object/from16 v17, v0

    .line 295
    invoke-virtual/range {v17 .. v17}, Lcom/here/android/mpa/guidance/NavigationManager;->getElapsedDistance()J

    move-result-wide v18

    move-wide/from16 v0, v18

    long-to-int v0, v0

    move/from16 v17, v0

    .line 296
    invoke-virtual/range {p1 .. p1}, Lcom/here/android/mpa/routing/Route;->getLength()I

    move-result v18

    .line 294
    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/here/android/mpa/routing/Route;->getRouteElementsFromLength(II)Lcom/here/android/mpa/routing/RouteElements;

    move-result-object v17

    .line 297
    invoke-virtual/range {v17 .. v17}, Lcom/here/android/mpa/routing/RouteElements;->getElements()Ljava/util/List;

    move-result-object v16

    .line 299
    .local v16, "routeElementsRemaining":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/RouteElement;>;"
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :cond_2
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_6

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/here/android/mpa/routing/RouteElement;

    .line 300
    .local v7, "element":Lcom/here/android/mpa/routing/RouteElement;
    invoke-virtual {v7}, Lcom/here/android/mpa/routing/RouteElement;->getRoadElement()Lcom/here/android/mpa/common/RoadElement;

    move-result-object v13

    .line 301
    .local v13, "roadElement":Lcom/here/android/mpa/common/RoadElement;
    invoke-virtual {v13}, Lcom/here/android/mpa/common/RoadElement;->getIdentifier()Lcom/here/android/mpa/common/Identifier;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/here/android/mpa/common/Identifier;->toString()Ljava/lang/String;

    move-result-object v15

    .line 303
    .local v15, "roadId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->logger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->tag:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "route element id="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 305
    invoke-interface {v14, v15}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_8

    .line 306
    invoke-interface {v14, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/here/android/mpa/mapping/TrafficEvent;

    .line 308
    .restart local v8    # "event":Lcom/here/android/mpa/mapping/TrafficEvent;
    invoke-virtual {v8}, Lcom/here/android/mpa/mapping/TrafficEvent;->getSeverity()Lcom/here/android/mpa/mapping/TrafficEvent$Severity;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/here/android/mpa/mapping/TrafficEvent$Severity;->getValue()I

    move-result v18

    sget-object v19, Lcom/here/android/mpa/mapping/TrafficEvent$Severity;->HIGH:Lcom/here/android/mpa/mapping/TrafficEvent$Severity;

    invoke-virtual/range {v19 .. v19}, Lcom/here/android/mpa/mapping/TrafficEvent$Severity;->getValue()I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_3

    const/4 v11, 0x1

    .line 309
    .local v11, "isVerySevereEvent":Z
    :goto_1
    invoke-virtual {v8}, Lcom/here/android/mpa/mapping/TrafficEvent;->getSeverity()Lcom/here/android/mpa/mapping/TrafficEvent$Severity;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/here/android/mpa/mapping/TrafficEvent$Severity;->getValue()I

    move-result v18

    sget-object v19, Lcom/here/android/mpa/mapping/TrafficEvent$Severity;->NORMAL:Lcom/here/android/mpa/mapping/TrafficEvent$Severity;

    invoke-virtual/range {v19 .. v19}, Lcom/here/android/mpa/mapping/TrafficEvent$Severity;->getValue()I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_4

    const/4 v10, 0x1

    .line 311
    .local v10, "isSevereEvent":Z
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->logger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->tag:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "event inside route: isVerySevereEvent="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "; isSevereEvent="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "; distanceToLocation="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 314
    if-eqz v11, :cond_5

    const/16 v18, 0xc8

    move/from16 v0, v18

    if-ge v6, v0, :cond_5

    .line 316
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v8}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->getRemainingTimeInJam(Lcom/here/android/mpa/routing/Route;Lcom/here/android/mpa/mapping/TrafficEvent;)I

    move-result v12

    .line 318
    .local v12, "remainingTimeInJam":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->logger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->tag:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "triggering jam, remaining time="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " sec"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 320
    const/16 v18, 0xb4

    move/from16 v0, v18

    if-lt v12, v0, :cond_8

    .line 321
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->bus:Lcom/squareup/otto/Bus;

    move-object/from16 v17, v0

    new-instance v18, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$JamTrackEvent;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2, v8, v12}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$JamTrackEvent;-><init>(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;Lcom/here/android/mpa/routing/Route;Lcom/here/android/mpa/mapping/TrafficEvent;I)V

    invoke-virtual/range {v17 .. v18}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 346
    .end local v7    # "element":Lcom/here/android/mpa/routing/RouteElement;
    .end local v8    # "event":Lcom/here/android/mpa/mapping/TrafficEvent;
    .end local v10    # "isSevereEvent":Z
    .end local v11    # "isVerySevereEvent":Z
    .end local v12    # "remainingTimeInJam":I
    .end local v13    # "roadElement":Lcom/here/android/mpa/common/RoadElement;
    .end local v15    # "roadId":Ljava/lang/String;
    :goto_3
    return-void

    .line 308
    .restart local v7    # "element":Lcom/here/android/mpa/routing/RouteElement;
    .restart local v8    # "event":Lcom/here/android/mpa/mapping/TrafficEvent;
    .restart local v13    # "roadElement":Lcom/here/android/mpa/common/RoadElement;
    .restart local v15    # "roadId":Ljava/lang/String;
    :cond_3
    const/4 v11, 0x0

    goto/16 :goto_1

    .line 309
    .restart local v11    # "isVerySevereEvent":Z
    :cond_4
    const/4 v10, 0x0

    goto/16 :goto_2

    .line 324
    .restart local v10    # "isSevereEvent":Z
    :cond_5
    if-eqz v11, :cond_7

    const/16 v18, 0xc8

    move/from16 v0, v18

    if-le v6, v0, :cond_7

    .line 325
    move-object v9, v8

    .line 339
    .end local v7    # "element":Lcom/here/android/mpa/routing/RouteElement;
    .end local v8    # "event":Lcom/here/android/mpa/mapping/TrafficEvent;
    .end local v10    # "isSevereEvent":Z
    .end local v11    # "isVerySevereEvent":Z
    .end local v13    # "roadElement":Lcom/here/android/mpa/common/RoadElement;
    .end local v15    # "roadId":Ljava/lang/String;
    :cond_6
    :goto_4
    if-eqz v9, :cond_9

    .line 340
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->bus:Lcom/squareup/otto/Bus;

    move-object/from16 v17, v0

    new-instance v18, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$DistanceTrackEvent;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2, v9}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$DistanceTrackEvent;-><init>(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;Lcom/here/android/mpa/routing/Route;Lcom/here/android/mpa/mapping/TrafficEvent;)V

    invoke-virtual/range {v17 .. v18}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_3

    .line 327
    .restart local v7    # "element":Lcom/here/android/mpa/routing/RouteElement;
    .restart local v8    # "event":Lcom/here/android/mpa/mapping/TrafficEvent;
    .restart local v10    # "isSevereEvent":Z
    .restart local v11    # "isVerySevereEvent":Z
    .restart local v13    # "roadElement":Lcom/here/android/mpa/common/RoadElement;
    .restart local v15    # "roadId":Ljava/lang/String;
    :cond_7
    if-eqz v10, :cond_8

    if-nez v5, :cond_8

    const/16 v18, 0xc8

    move/from16 v0, v18

    if-le v6, v0, :cond_8

    .line 328
    move-object v5, v8

    .line 332
    .end local v8    # "event":Lcom/here/android/mpa/mapping/TrafficEvent;
    .end local v10    # "isSevereEvent":Z
    .end local v11    # "isVerySevereEvent":Z
    :cond_8
    int-to-double v0, v6

    move-wide/from16 v18, v0

    invoke-virtual {v13}, Lcom/here/android/mpa/common/RoadElement;->getGeometryLength()D

    move-result-wide v20

    add-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-int v6, v0

    .line 334
    const/16 v18, 0x1388

    move/from16 v0, v18

    if-le v6, v0, :cond_2

    goto :goto_4

    .line 341
    .end local v7    # "element":Lcom/here/android/mpa/routing/RouteElement;
    .end local v13    # "roadElement":Lcom/here/android/mpa/common/RoadElement;
    .end local v15    # "roadId":Ljava/lang/String;
    :cond_9
    if-eqz v5, :cond_a

    .line 342
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->bus:Lcom/squareup/otto/Bus;

    move-object/from16 v17, v0

    new-instance v18, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$DistanceTrackEvent;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2, v5}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$DistanceTrackEvent;-><init>(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;Lcom/here/android/mpa/routing/Route;Lcom/here/android/mpa/mapping/TrafficEvent;)V

    invoke-virtual/range {v17 .. v18}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_3

    .line 344
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->bus:Lcom/squareup/otto/Bus;

    move-object/from16 v17, v0

    new-instance v18, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$TrackDismissEvent;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$TrackDismissEvent;-><init>(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;Lcom/here/android/mpa/routing/Route;)V

    invoke-virtual/range {v17 .. v18}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_3
.end method

.method private refresh()V
    .locals 4

    .prologue
    .line 441
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "posting refresh in 30 sec..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 442
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->cancelRequest()V

    .line 443
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->refreshRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 444
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->refreshRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 445
    return-void
.end method

.method private startRequest()V
    .locals 4

    .prologue
    .line 402
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentRoute:Lcom/here/android/mpa/routing/Route;

    if-nez v1, :cond_0

    .line 403
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->tag:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "startRequest: currentRoute must not be null."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 425
    :goto_0
    :pswitch_0
    return-void

    .line 407
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->trafficUpdater:Lcom/here/android/mpa/guidance/TrafficUpdater;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentRoute:Lcom/here/android/mpa/routing/Route;

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->onRequestListener:Lcom/here/android/mpa/guidance/TrafficUpdater$Listener;

    invoke-virtual {v1, v2, v3}, Lcom/here/android/mpa/guidance/TrafficUpdater;->request(Lcom/here/android/mpa/routing/Route;Lcom/here/android/mpa/guidance/TrafficUpdater$Listener;)Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentRequestInfo:Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;

    .line 408
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentRequestInfo:Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;

    invoke-virtual {v1}, Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;->getError()Lcom/here/android/mpa/guidance/TrafficUpdater$Error;

    move-result-object v0

    .line 410
    .local v0, "error":Lcom/here/android/mpa/guidance/TrafficUpdater$Error;
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$5;->$SwitchMap$com$here$android$mpa$guidance$TrafficUpdater$Error:[I

    invoke-virtual {v0}, Lcom/here/android/mpa/guidance/TrafficUpdater$Error;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 421
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->tag:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " got error before TrafficUpdater.request. Invalid/malformed request."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 422
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->STOPPED:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->changeState(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;)V

    goto :goto_0

    .line 416
    :pswitch_1
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->tag:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " recovering from error before TrafficUpdater.request"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 417
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->refresh()V

    goto :goto_0

    .line 410
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private startTrackingDistance()V
    .locals 2

    .prologue
    .line 544
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->bus:Lcom/squareup/otto/Bus;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentEvent:Lcom/here/android/mpa/mapping/TrafficEvent;

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->transformToLiveTrafficEvent(Lcom/here/android/mpa/mapping/TrafficEvent;)Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 545
    return-void
.end method

.method private startTrackingJamTime()V
    .locals 3

    .prologue
    .line 553
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/maps/MapEvents$TrafficJamProgressEvent;

    iget v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentRemainingTimeInJam:I

    invoke-static {v2}, Lcom/navdy/hud/app/maps/util/MapUtils;->getMinuteCeiling(I)I

    move-result v2

    invoke-direct {v1, v2}, Lcom/navdy/hud/app/maps/MapEvents$TrafficJamProgressEvent;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 554
    return-void
.end method

.method private startUpdates()V
    .locals 3

    .prologue
    .line 520
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentRoute:Lcom/here/android/mpa/routing/Route;

    if-nez v0, :cond_0

    .line 521
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " route must not be null."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 528
    :cond_0
    return-void
.end method

.method private stopTrackingDistance()V
    .locals 2

    .prologue
    .line 548
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficDismissEvent;

    invoke-direct {v1}, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficDismissEvent;-><init>()V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 549
    return-void
.end method

.method private stopTrackingJamTime()V
    .locals 2

    .prologue
    .line 557
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/maps/MapEvents$TrafficJamDismissEvent;

    invoke-direct {v1}, Lcom/navdy/hud/app/maps/MapEvents$TrafficJamDismissEvent;-><init>()V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 558
    return-void
.end method

.method private stopUpdates()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 531
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->cancelRequest()V

    .line 532
    iput-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentEvent:Lcom/here/android/mpa/mapping/TrafficEvent;

    .line 534
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->refreshRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 535
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->dismissEtaUpdate:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 537
    iput-object v2, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentRoute:Lcom/here/android/mpa/routing/Route;

    .line 539
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/maps/MapEvents$TrafficJamDismissEvent;

    invoke-direct {v1}, Lcom/navdy/hud/app/maps/MapEvents$TrafficJamDismissEvent;-><init>()V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 540
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/maps/MapEvents$TrafficDelayDismissEvent;

    invoke-direct {v1}, Lcom/navdy/hud/app/maps/MapEvents$TrafficDelayDismissEvent;-><init>()V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 541
    return-void
.end method

.method private trackEvent(Lcom/here/android/mpa/mapping/TrafficEvent;)V
    .locals 1
    .param p1, "newTrafficEvent"    # Lcom/here/android/mpa/mapping/TrafficEvent;

    .prologue
    .line 199
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentEvent:Lcom/here/android/mpa/mapping/TrafficEvent;

    .line 200
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->TRACK_DISTANCE:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->changeState(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;)V

    .line 201
    return-void
.end method

.method private trackJam(I)V
    .locals 1
    .param p1, "newRemainingTimeInJam"    # I

    .prologue
    .line 209
    iput p1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentRemainingTimeInJam:I

    .line 210
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->TRACK_JAM:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->changeState(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;)V

    .line 211
    return-void
.end method

.method private transformToLiveTrafficEvent(Lcom/here/android/mpa/mapping/TrafficEvent;)Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent;
    .locals 4
    .param p1, "event"    # Lcom/here/android/mpa/mapping/TrafficEvent;

    .prologue
    .line 387
    invoke-virtual {p1}, Lcom/here/android/mpa/mapping/TrafficEvent;->isFlow()Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Type;->CONGESTION:Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Type;

    .line 392
    .local v1, "type":Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Type;
    :goto_0
    invoke-virtual {p1}, Lcom/here/android/mpa/mapping/TrafficEvent;->getSeverity()Lcom/here/android/mpa/mapping/TrafficEvent$Severity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/here/android/mpa/mapping/TrafficEvent$Severity;->getValue()I

    move-result v2

    sget-object v3, Lcom/here/android/mpa/mapping/TrafficEvent$Severity;->HIGH:Lcom/here/android/mpa/mapping/TrafficEvent$Severity;

    invoke-virtual {v3}, Lcom/here/android/mpa/mapping/TrafficEvent$Severity;->getValue()I

    move-result v3

    if-le v2, v3, :cond_1

    .line 393
    sget-object v0, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;->VERY_HIGH:Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;

    .line 398
    .local v0, "severity":Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;
    :goto_1
    new-instance v2, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent;

    invoke-direct {v2, v1, v0}, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent;-><init>(Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Type;Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;)V

    return-object v2

    .line 387
    .end local v0    # "severity":Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;
    .end local v1    # "type":Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Type;
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Type;->INCIDENT:Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Type;

    goto :goto_0

    .line 395
    .restart local v1    # "type":Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Type;
    :cond_1
    sget-object v0, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;->HIGH:Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;

    .restart local v0    # "severity":Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficEvent$Severity;
    goto :goto_1
.end method


# virtual methods
.method public activate(Lcom/here/android/mpa/routing/Route;)V
    .locals 1
    .param p1, "newRoute"    # Lcom/here/android/mpa/routing/Route;

    .prologue
    .line 185
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentRoute:Lcom/here/android/mpa/routing/Route;

    .line 186
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->ACTIVE:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->changeState(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;)V

    .line 187
    return-void
.end method

.method public onDistanceTrackEvent(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$DistanceTrackEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$DistanceTrackEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 134
    iget-object v0, p1, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$DistanceTrackEvent;->route:Lcom/here/android/mpa/routing/Route;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->isCurrentRoute(Lcom/here/android/mpa/routing/Route;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p1, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$DistanceTrackEvent;->trafficEvent:Lcom/here/android/mpa/mapping/TrafficEvent;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->trackEvent(Lcom/here/android/mpa/mapping/TrafficEvent;)V

    .line 137
    :cond_0
    return-void
.end method

.method public onJamTrackEvent(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$JamTrackEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$JamTrackEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 142
    iget-object v0, p1, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$JamTrackEvent;->route:Lcom/here/android/mpa/routing/Route;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->isCurrentRoute(Lcom/here/android/mpa/routing/Route;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p1, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$JamTrackEvent;->trafficEvent:Lcom/here/android/mpa/mapping/TrafficEvent;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentEvent:Lcom/here/android/mpa/mapping/TrafficEvent;

    .line 144
    iget v0, p1, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$JamTrackEvent;->remainingTimeInJam:I

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->trackJam(I)V

    .line 146
    :cond_0
    return-void
.end method

.method public onTrackDismissEvent(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$TrackDismissEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$TrackDismissEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 151
    iget-object v0, p1, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$TrackDismissEvent;->route:Lcom/here/android/mpa/routing/Route;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->isCurrentRoute(Lcom/here/android/mpa/routing/Route;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->ACTIVE:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->changeState(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;)V

    .line 154
    :cond_0
    return-void
.end method

.method public onTrackingUpdate(Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener$MapMatchEvent;)V
    .locals 3
    .param p1, "mapMatchEvent"    # Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener$MapMatchEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 120
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentState:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    sget-object v2, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->TRACK_DISTANCE:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    if-ne v1, v2, :cond_1

    .line 121
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->onDistanceTrackingUpdate(Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener$MapMatchEvent;)V

    .line 129
    :cond_0
    :goto_0
    return-void

    .line 122
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->currentState:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    sget-object v2, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->TRACK_JAM:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    if-ne v1, v2, :cond_0

    .line 123
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->onJamTrackingUpdate()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 125
    :catch_0
    move-exception v0

    .line 126
    .local v0, "t":Ljava/lang/Throwable;
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 127
    invoke-static {}, Lcom/navdy/hud/app/util/CrashReporter;->getInstance()Lcom/navdy/hud/app/util/CrashReporter;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/util/CrashReporter;->reportNonFatalException(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 190
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->STOPPED:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;->changeState(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;)V

    .line 191
    return-void
.end method
