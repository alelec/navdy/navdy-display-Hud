.class Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1$1;
.super Ljava/lang/Object;
.source "HerePlacesManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1;->result(Lcom/here/android/mpa/search/ErrorCode;Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1;

.field final synthetic val$errorCode:Lcom/here/android/mpa/search/ErrorCode;

.field final synthetic val$result:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1;Lcom/here/android/mpa/search/ErrorCode;Ljava/util/List;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1;

    .prologue
    .line 324
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1;

    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1$1;->val$errorCode:Lcom/here/android/mpa/search/ErrorCode;

    iput-object p3, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1$1;->val$result:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/4 v10, 0x2

    .line 328
    :try_start_0
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1$1;->val$errorCode:Lcom/here/android/mpa/search/ErrorCode;

    sget-object v4, Lcom/here/android/mpa/search/ErrorCode;->NONE:Lcom/here/android/mpa/search/ErrorCode;

    if-eq v3, v4, :cond_1

    .line 329
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1;

    iget-object v3, v3, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1;->val$autoCompleteSearch:Ljava/lang/String;

    sget-object v4, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SERVICE_ERROR:Lcom/navdy/service/library/events/RequestStatus;

    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->context:Landroid/content/Context;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$200()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f090255

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1$1;->val$errorCode:Lcom/here/android/mpa/search/ErrorCode;

    invoke-virtual {v9}, Lcom/here/android/mpa/search/ErrorCode;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    # invokes: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->returnAutoCompleteErrorResponse(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V
    invoke-static {v3, v4, v5}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$600(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 343
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    invoke-virtual {v3, v10}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 344
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 345
    .local v0, "l2":J
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "handleAutoCompleteRequest- time-2 ="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1;

    iget-wide v6, v5, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1;->val$l1:J

    sub-long v6, v0, v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 348
    .end local v0    # "l2":J
    :cond_0
    :goto_0
    return-void

    .line 332
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1$1;->val$result:Ljava/util/List;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1$1;->val$result:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_3

    .line 333
    :cond_2
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "no results"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 334
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1;

    iget-object v3, v3, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1;->val$autoCompleteSearch:Ljava/lang/String;

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1;

    iget v4, v4, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1;->val$maxResults:I

    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->EMPTY_AUTO_COMPLETE_RESULT:Ljava/util/ArrayList;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$700()Ljava/util/ArrayList;

    move-result-object v5

    # invokes: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->returnAutoCompleteResults(Ljava/lang/String;ILjava/util/List;)V
    invoke-static {v3, v4, v5}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$800(Ljava/lang/String;ILjava/util/List;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 343
    :goto_1
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    invoke-virtual {v3, v10}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 344
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 345
    .restart local v0    # "l2":J
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "handleAutoCompleteRequest- time-2 ="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1;

    iget-wide v6, v5, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1;->val$l1:J

    sub-long v6, v0, v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 336
    .end local v0    # "l2":J
    :cond_3
    :try_start_2
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "returned results:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1$1;->val$result:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 337
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1;

    iget-object v3, v3, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1;->val$autoCompleteSearch:Ljava/lang/String;

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1;

    iget v4, v4, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1;->val$maxResults:I

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1$1;->val$result:Ljava/util/List;

    # invokes: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->returnAutoCompleteResults(Ljava/lang/String;ILjava/util/List;)V
    invoke-static {v3, v4, v5}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$800(Ljava/lang/String;ILjava/util/List;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 339
    :catch_0
    move-exception v2

    .line 340
    .local v2, "t":Ljava/lang/Throwable;
    :try_start_3
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 341
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1;

    iget-object v3, v3, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1;->val$autoCompleteSearch:Ljava/lang/String;

    sget-object v4, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_UNKNOWN_ERROR:Lcom/navdy/service/library/events/RequestStatus;

    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->context:Landroid/content/Context;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$200()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0902da

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    # invokes: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->returnAutoCompleteErrorResponse(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V
    invoke-static {v3, v4, v5}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$600(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 343
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    invoke-virtual {v3, v10}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 344
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 345
    .restart local v0    # "l2":J
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "handleAutoCompleteRequest- time-2 ="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1;

    iget-wide v6, v5, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1;->val$l1:J

    sub-long v6, v0, v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 343
    .end local v0    # "l2":J
    .end local v2    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v3

    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    invoke-virtual {v4, v10}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 344
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 345
    .restart local v0    # "l2":J
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "handleAutoCompleteRequest- time-2 ="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1;

    iget-wide v6, v6, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1;->val$l1:J

    sub-long v6, v0, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 346
    .end local v0    # "l2":J
    :cond_4
    throw v3
.end method
