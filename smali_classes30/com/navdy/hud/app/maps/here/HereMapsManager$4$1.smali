.class Lcom/navdy/hud/app/maps/here/HereMapsManager$4$1;
.super Ljava/lang/Object;
.source "HereMapsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereMapsManager$4;->onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$4;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereMapsManager$4;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/maps/here/HereMapsManager$4;

    .prologue
    .line 652
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$4$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 655
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$4$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$4;

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereMapsManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$500(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapController;->getMapScheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "hybrid.night"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 656
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$4$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$4;

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereMapsManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$500(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$4$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapsManager$4;

    iget-object v1, v1, Lcom/navdy/hud/app/maps/here/HereMapsManager$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getTrackingMapScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereMapController;->setMapScheme(Ljava/lang/String;)V

    .line 658
    :cond_0
    return-void
.end method
