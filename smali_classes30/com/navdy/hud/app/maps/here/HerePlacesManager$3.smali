.class final Lcom/navdy/hud/app/maps/here/HerePlacesManager$3;
.super Ljava/lang/Object;
.source "HerePlacesManager.java"

# interfaces
.implements Lcom/here/android/mpa/search/ResultListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HerePlacesManager;->autoComplete(Lcom/here/android/mpa/common/GeoCoordinate;Ljava/lang/String;IILcom/navdy/hud/app/maps/here/HerePlacesManager$AutoCompleteCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/here/android/mpa/search/ResultListener",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic val$callBack:Lcom/navdy/hud/app/maps/here/HerePlacesManager$AutoCompleteCallback;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HerePlacesManager$AutoCompleteCallback;)V
    .locals 0

    .prologue
    .line 161
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$3;->val$callBack:Lcom/navdy/hud/app/maps/here/HerePlacesManager$AutoCompleteCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onCompleted(Ljava/lang/Object;Lcom/here/android/mpa/search/ErrorCode;)V
    .locals 0

    .prologue
    .line 161
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1, p2}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$3;->onCompleted(Ljava/util/List;Lcom/here/android/mpa/search/ErrorCode;)V

    return-void
.end method

.method public onCompleted(Ljava/util/List;Lcom/here/android/mpa/search/ErrorCode;)V
    .locals 2
    .param p2, "errorCode"    # Lcom/here/android/mpa/search/ErrorCode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/here/android/mpa/search/ErrorCode;",
            ")V"
        }
    .end annotation

    .prologue
    .line 165
    .local p1, "results":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$3;->val$callBack:Lcom/navdy/hud/app/maps/here/HerePlacesManager$AutoCompleteCallback;

    invoke-interface {v1, p2, p1}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$AutoCompleteCallback;->result(Lcom/here/android/mpa/search/ErrorCode;Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 169
    :goto_0
    return-void

    .line 166
    :catch_0
    move-exception v0

    .line 167
    .local v0, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
