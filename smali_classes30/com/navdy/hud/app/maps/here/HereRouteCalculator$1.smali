.class Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;
.super Ljava/lang/Object;
.source "HereRouteCalculator.java"

# interfaces
.implements Lcom/here/android/mpa/routing/CoreRouter$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->calculateRoute(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/common/GeoCoordinate;Ljava/util/List;Lcom/here/android/mpa/common/GeoCoordinate;ZLcom/navdy/hud/app/maps/here/HereRouteCalculator$RouteCalculatorListener;ILcom/here/android/mpa/routing/RouteOptions;ZZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private lastPercentage:I

.field private lastProgress:J

.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

.field final synthetic val$allowCancellation:Z

.field final synthetic val$cache:Z

.field final synthetic val$calculatePolyline:Z

.field final synthetic val$factoringInTraffic:Z

.field final synthetic val$listener:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$RouteCalculatorListener;

.field final synthetic val$request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

.field final synthetic val$startPoint:Lcom/here/android/mpa/common/GeoCoordinate;

.field final synthetic val$startTime:J


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereRouteCalculator;Lcom/navdy/hud/app/maps/here/HereRouteCalculator$RouteCalculatorListener;ZLcom/navdy/service/library/events/navigation/NavigationRouteRequest;ZZZLcom/here/android/mpa/common/GeoCoordinate;J)V
    .locals 3
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->this$0:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->val$listener:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$RouteCalculatorListener;

    iput-boolean p3, p0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->val$allowCancellation:Z

    iput-object p4, p0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->val$request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iput-boolean p5, p0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->val$calculatePolyline:Z

    iput-boolean p6, p0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->val$cache:Z

    iput-boolean p7, p0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->val$factoringInTraffic:Z

    iput-object p8, p0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->val$startPoint:Lcom/here/android/mpa/common/GeoCoordinate;

    iput-wide p9, p0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->val$startTime:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->lastPercentage:I

    .line 116
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->lastProgress:J

    return-void
.end method


# virtual methods
.method public bridge synthetic onCalculateRouteFinished(Ljava/lang/Object;Ljava/lang/Enum;)V
    .locals 0

    .prologue
    .line 114
    check-cast p1, Ljava/util/List;

    check-cast p2, Lcom/here/android/mpa/routing/RoutingError;

    invoke-virtual {p0, p1, p2}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->onCalculateRouteFinished(Ljava/util/List;Lcom/here/android/mpa/routing/RoutingError;)V

    return-void
.end method

.method public onCalculateRouteFinished(Ljava/util/List;Lcom/here/android/mpa/routing/RoutingError;)V
    .locals 3
    .param p2, "routingError"    # Lcom/here/android/mpa/routing/RoutingError;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/routing/RouteResult;",
            ">;",
            "Lcom/here/android/mpa/routing/RoutingError;",
            ")V"
        }
    .end annotation

    .prologue
    .line 120
    .local p1, "results":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/RouteResult;>;"
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;

    invoke-direct {v1, p0, p2, p1}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1$1;-><init>(Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;Lcom/here/android/mpa/routing/RoutingError;Ljava/util/List;)V

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 200
    return-void
.end method

.method public onProgress(I)V
    .locals 8
    .param p1, "percentage"    # I

    .prologue
    .line 206
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 207
    .local v0, "now":J
    iget v4, p0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->lastPercentage:I

    if-eq p1, v4, :cond_1

    iget-wide v4, p0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->lastProgress:J

    const-wide/16 v6, 0xc8

    add-long/2addr v4, v6

    cmp-long v4, v0, v4

    if-gtz v4, :cond_0

    const/16 v4, 0x64

    if-ne p1, v4, :cond_1

    .line 208
    :cond_0
    iput p1, p0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->lastPercentage:I

    .line 209
    iput-wide v0, p0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->lastProgress:J

    .line 210
    iget-wide v4, p0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->val$startTime:J

    sub-long v2, v0, v4

    .line 211
    .local v2, "timeElapsed":J
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->this$0:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->access$200(Lcom/navdy/hud/app/maps/here/HereRouteCalculator;)Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->this$0:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->tag:Ljava/lang/String;
    invoke-static {v6}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->access$100(Lcom/navdy/hud/app/maps/here/HereRouteCalculator;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "route calculation: ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%] done timeElapsed:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 212
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$1;->val$listener:Lcom/navdy/hud/app/maps/here/HereRouteCalculator$RouteCalculatorListener;

    invoke-interface {v4, p1}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator$RouteCalculatorListener;->progress(I)V

    .line 214
    .end local v2    # "timeElapsed":J
    :cond_1
    return-void
.end method
