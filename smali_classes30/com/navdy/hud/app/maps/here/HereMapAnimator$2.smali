.class Lcom/navdy/hud/app/maps/here/HereMapAnimator$2;
.super Ljava/lang/Object;
.source "HereMapAnimator.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereMapAnimator;->setGeoPosition(Lcom/here/android/mpa/common/GeoPosition;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

.field final synthetic val$geoPosition:Lcom/here/android/mpa/common/GeoPosition;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereMapAnimator;Lcom/here/android/mpa/common/GeoPosition;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    .prologue
    .line 228
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$2;->this$0:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$2;->val$geoPosition:Lcom/here/android/mpa/common/GeoPosition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 231
    const-wide/16 v8, 0x0

    .line 233
    .local v8, "interval":J
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$2;->this$0:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapAnimator;->lastGeoPositionUpdateTime:J
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->access$200(Lcom/navdy/hud/app/maps/here/HereMapAnimator;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 234
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$2;->this$0:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapAnimator;->lastGeoPositionUpdateTime:J
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->access$200(Lcom/navdy/hud/app/maps/here/HereMapAnimator;)J

    move-result-wide v4

    sub-long v8, v2, v4

    .line 237
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$2;->this$0:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$2;->val$geoPosition:Lcom/here/android/mpa/common/GeoPosition;

    # invokes: Lcom/navdy/hud/app/maps/here/HereMapAnimator;->isValidGeoPosition(Lcom/here/android/mpa/common/GeoPosition;)Z
    invoke-static {v1, v2}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->access$300(Lcom/navdy/hud/app/maps/here/HereMapAnimator;Lcom/here/android/mpa/common/GeoPosition;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 238
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapAnimator;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "filtering out spurious heading: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$2;->val$geoPosition:Lcom/here/android/mpa/common/GeoPosition;

    invoke-virtual {v3}, Lcom/here/android/mpa/common/GeoPosition;->getHeading()D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " speed:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$2;->val$geoPosition:Lcom/here/android/mpa/common/GeoPosition;

    .line 239
    invoke-virtual {v3}, Lcom/here/android/mpa/common/GeoPosition;->getSpeed()D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 238
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 261
    :cond_1
    :goto_0
    return-void

    .line 243
    :cond_2
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$2;->this$0:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    # setter for: Lcom/navdy/hud/app/maps/here/HereMapAnimator;->lastGeoPositionUpdateTime:J
    invoke-static {v1, v2, v3}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->access$202(Lcom/navdy/hud/app/maps/here/HereMapAnimator;J)J

    .line 246
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapAnimator;->geoPositionLock:Ljava/lang/Object;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->access$500()Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 247
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$2;->this$0:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    # setter for: Lcom/navdy/hud/app/maps/here/HereMapAnimator;->geoPositionUpdateInterval:J
    invoke-static {v1, v8, v9}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->access$602(Lcom/navdy/hud/app/maps/here/HereMapAnimator;J)J

    .line 248
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$2;->this$0:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapAnimator;->currentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->access$700(Lcom/navdy/hud/app/maps/here/HereMapAnimator;)Lcom/here/android/mpa/common/GeoPosition;

    move-result-object v0

    .line 249
    .local v0, "oldPosition":Lcom/here/android/mpa/common/GeoPosition;
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$2;->this$0:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$2;->val$geoPosition:Lcom/here/android/mpa/common/GeoPosition;

    # setter for: Lcom/navdy/hud/app/maps/here/HereMapAnimator;->currentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;
    invoke-static {v1, v3}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->access$702(Lcom/navdy/hud/app/maps/here/HereMapAnimator;Lcom/here/android/mpa/common/GeoPosition;)Lcom/here/android/mpa/common/GeoPosition;

    .line 250
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 252
    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$2;->this$0:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapAnimator;->renderingEnabled:Z
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->access$800(Lcom/navdy/hud/app/maps/here/HereMapAnimator;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 253
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapAnimator;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "initial setCenter"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 255
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$2;->this$0:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapAnimator;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->access$900(Lcom/navdy/hud/app/maps/here/HereMapAnimator;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$2;->this$0:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapAnimator;->currentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->access$700(Lcom/navdy/hud/app/maps/here/HereMapAnimator;)Lcom/here/android/mpa/common/GeoPosition;

    move-result-object v2

    invoke-virtual {v2}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v2

    sget-object v3, Lcom/here/android/mpa/mapping/Map$Animation;->NONE:Lcom/here/android/mpa/mapping/Map$Animation;

    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$2;->this$0:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    .line 258
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapAnimator;->currentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;
    invoke-static {v6}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->access$700(Lcom/navdy/hud/app/maps/here/HereMapAnimator;)Lcom/here/android/mpa/common/GeoPosition;

    move-result-object v6

    invoke-virtual {v6}, Lcom/here/android/mpa/common/GeoPosition;->getHeading()D

    move-result-wide v6

    double-to-float v6, v6

    const/high16 v7, -0x40800000    # -1.0f

    .line 255
    invoke-virtual/range {v1 .. v7}, Lcom/navdy/hud/app/maps/here/HereMapController;->setCenter(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;DFF)V

    goto :goto_0

    .line 250
    .end local v0    # "oldPosition":Lcom/here/android/mpa/common/GeoPosition;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
