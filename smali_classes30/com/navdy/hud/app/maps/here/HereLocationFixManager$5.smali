.class Lcom/navdy/hud/app/maps/here/HereLocationFixManager$5;
.super Ljava/lang/Object;
.source "HereLocationFixManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->onStartDriveRecording(Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

.field final synthetic val$event:Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    .prologue
    .line 366
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$5;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$5;->val$event:Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 370
    :try_start_0
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$5;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->isRecording:Z
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$1000(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 371
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$5;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$5;->val$event:Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;

    iget-object v4, v4, Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;->label:Ljava/lang/String;

    # setter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->recordingLabel:Ljava/lang/String;
    invoke-static {v3, v4}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$1402(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;Ljava/lang/String;)Ljava/lang/String;

    .line 372
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$5;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    const/4 v4, 0x1

    # setter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->isRecording:Z
    invoke-static {v3, v4}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$1002(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;Z)Z

    .line 373
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$5;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->recordingLabel:Ljava/lang/String;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$1400(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/navdy/hud/app/debug/DriveRecorder;->getDriveLogsDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 374
    .local v1, "driveLogsDir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 375
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 378
    :cond_0
    new-instance v0, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$5;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->recordingLabel:Ljava/lang/String;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$1400(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".here"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 380
    .local v0, "driveLogFile":Ljava/io/File;
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$5;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    # setter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->recordingFile:Ljava/io/FileOutputStream;
    invoke-static {v3, v4}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$1502(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;Ljava/io/FileOutputStream;)Ljava/io/FileOutputStream;

    .line 381
    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "created recording file ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$5;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->recordingLabel:Ljava/lang/String;
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$1400(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] path:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 392
    .end local v0    # "driveLogFile":Ljava/io/File;
    .end local v1    # "driveLogsDir":Ljava/io/File;
    :goto_0
    return-void

    .line 383
    :cond_1
    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "already recording"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 385
    :catch_0
    move-exception v2

    .line 386
    .local v2, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 387
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$5;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    const/4 v4, 0x0

    # setter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->isRecording:Z
    invoke-static {v3, v4}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$1002(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;Z)Z

    .line 388
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$5;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # setter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->recordingLabel:Ljava/lang/String;
    invoke-static {v3, v6}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$1402(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;Ljava/lang/String;)Ljava/lang/String;

    .line 389
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$5;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->recordingFile:Ljava/io/FileOutputStream;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$1500(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Ljava/io/FileOutputStream;

    move-result-object v3

    invoke-static {v3}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 390
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$5;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # setter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->recordingFile:Ljava/io/FileOutputStream;
    invoke-static {v3, v6}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$1502(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;Ljava/io/FileOutputStream;)Ljava/io/FileOutputStream;

    goto :goto_0
.end method
