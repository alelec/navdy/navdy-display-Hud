.class final Lcom/navdy/hud/app/maps/here/HereRouteManager$6;
.super Ljava/lang/Object;
.source "HereRouteManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereRouteManager;->handleRouteManeuverRequest(Lcom/navdy/service/library/events/navigation/RouteManeuverRequest;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$routeManeuverRequest:Lcom/navdy/service/library/events/navigation/RouteManeuverRequest;


# direct methods
.method constructor <init>(Lcom/navdy/service/library/events/navigation/RouteManeuverRequest;)V
    .locals 0

    .prologue
    .line 661
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$6;->val$routeManeuverRequest:Lcom/navdy/service/library/events/navigation/RouteManeuverRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 33

    .prologue
    .line 665
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereRouteManager$6;->val$routeManeuverRequest:Lcom/navdy/service/library/events/navigation/RouteManeuverRequest;

    iget-object v3, v3, Lcom/navdy/service/library/events/navigation/RouteManeuverRequest;->routeId:Ljava/lang/String;

    if-eqz v3, :cond_7

    .line 666
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v24

    .line 667
    .local v24, "l1":J
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getInstance()Lcom/navdy/hud/app/maps/here/HereRouteCache;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereRouteManager$6;->val$routeManeuverRequest:Lcom/navdy/service/library/events/navigation/RouteManeuverRequest;

    iget-object v4, v4, Lcom/navdy/service/library/events/navigation/RouteManeuverRequest;->routeId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getRoute(Ljava/lang/String;)Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;

    move-result-object v30

    .line 668
    .local v30, "routeInfo":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    if-eqz v30, :cond_7

    .line 669
    const/4 v6, 0x0

    .line 670
    .local v6, "streetAddress":Ljava/lang/String;
    move-object/from16 v0, v30

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->routeRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    if-eqz v3, :cond_0

    .line 671
    move-object/from16 v0, v30

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->routeRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v6, v3, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->streetAddress:Ljava/lang/String;

    .line 673
    :cond_0
    move-object/from16 v0, v30

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->route:Lcom/here/android/mpa/routing/Route;

    invoke-virtual {v3}, Lcom/here/android/mpa/routing/Route;->getManeuvers()Ljava/util/List;

    move-result-object v28

    .line 674
    .local v28, "list":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    new-instance v31, Ljava/util/ArrayList;

    const/16 v3, 0x14

    move-object/from16 v0, v31

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 675
    .local v31, "routeManeuvers":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/navigation/RouteManeuver;>;"
    invoke-interface/range {v28 .. v28}, Ljava/util/List;->size()I

    move-result v23

    .line 676
    .local v23, "len":I
    const/4 v7, 0x0

    .line 677
    .local v7, "previous":Lcom/here/android/mpa/routing/Maneuver;
    const/4 v2, 0x0

    .line 679
    .local v2, "current":Lcom/here/android/mpa/routing/Maneuver;
    const/16 v22, 0x0

    .local v22, "i":I
    :goto_0
    move/from16 v0, v22

    move/from16 v1, v23

    if-ge v0, v1, :cond_6

    .line 680
    if-eqz v2, :cond_1

    .line 681
    move-object v7, v2

    .line 683
    :cond_1
    move-object/from16 v0, v28

    move/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "current":Lcom/here/android/mpa/routing/Maneuver;
    check-cast v2, Lcom/here/android/mpa/routing/Maneuver;

    .line 684
    .restart local v2    # "current":Lcom/here/android/mpa/routing/Maneuver;
    const/4 v9, 0x0

    .line 685
    .local v9, "after":Lcom/here/android/mpa/routing/Maneuver;
    add-int/lit8 v3, v23, -0x1

    move/from16 v0, v22

    if-ge v0, v3, :cond_2

    .line 686
    add-int/lit8 v3, v22, 0x1

    move-object/from16 v0, v28

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "after":Lcom/here/android/mpa/routing/Maneuver;
    check-cast v9, Lcom/here/android/mpa/routing/Maneuver;

    .line 689
    .restart local v9    # "after":Lcom/here/android/mpa/routing/Maneuver;
    :cond_2
    const/16 v29, 0x0

    .line 690
    .local v29, "maneuverTime":I
    if-nez v22, :cond_4

    .line 691
    invoke-static {v2, v9}, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->getStartManeuverDisplay(Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;)Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    move-result-object v17

    .line 712
    .local v17, "display":Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    :cond_3
    :goto_1
    new-instance v10, Lcom/navdy/service/library/events/navigation/RouteManeuver;

    move-object/from16 v0, v17

    iget-object v11, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->currentRoad:Ljava/lang/String;

    move-object/from16 v0, v17

    iget-object v12, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->navigationTurn:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    move-object/from16 v0, v17

    iget-object v13, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingRoad:Ljava/lang/String;

    move-object/from16 v0, v17

    iget v3, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->distance:F

    .line 713
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v14

    move-object/from16 v0, v17

    iget-object v15, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->distanceUnit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    invoke-direct/range {v10 .. v16}, Lcom/navdy/service/library/events/navigation/RouteManeuver;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationTurn;Ljava/lang/String;Ljava/lang/Float;Lcom/navdy/service/library/events/navigation/DistanceUnit;Ljava/lang/Integer;)V

    .line 714
    .local v10, "maneuver":Lcom/navdy/service/library/events/navigation/RouteManeuver;
    move-object/from16 v0, v31

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 679
    add-int/lit8 v22, v22, 0x1

    goto :goto_0

    .line 693
    .end local v10    # "maneuver":Lcom/navdy/service/library/events/navigation/RouteManeuver;
    .end local v17    # "display":Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    :cond_4
    if-nez v9, :cond_5

    const/4 v3, 0x1

    .line 696
    :goto_2
    invoke-virtual {v2}, Lcom/here/android/mpa/routing/Maneuver;->getDistanceToNextManeuver()I

    move-result v4

    int-to-long v4, v4

    move-object/from16 v0, v30

    iget-object v8, v0, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->routeRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    const/4 v10, 0x1

    const/4 v11, 0x0

    const/4 v12, 0x0

    .line 693
    invoke-static/range {v2 .. v12}, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->getManeuverDisplay(Lcom/here/android/mpa/routing/Maneuver;ZJLjava/lang/String;Lcom/here/android/mpa/routing/Maneuver;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/routing/Maneuver;ZZLcom/navdy/hud/app/maps/MapEvents$DestinationDirection;)Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    move-result-object v17

    .line 706
    .restart local v17    # "display":Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    if-eqz v9, :cond_3

    .line 707
    invoke-virtual {v2}, Lcom/here/android/mpa/routing/Maneuver;->getStartTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v18

    .line 708
    .local v18, "c1":J
    invoke-virtual {v9}, Lcom/here/android/mpa/routing/Maneuver;->getStartTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v20

    .line 709
    .local v20, "c2":J
    sub-long v4, v20, v18

    const-wide/16 v12, 0x3e8

    div-long/2addr v4, v12

    long-to-int v0, v4

    move/from16 v29, v0

    goto :goto_1

    .line 693
    .end local v17    # "display":Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    .end local v18    # "c1":J
    .end local v20    # "c2":J
    :cond_5
    const/4 v3, 0x0

    goto :goto_2

    .line 716
    .end local v9    # "after":Lcom/here/android/mpa/routing/Maneuver;
    .end local v29    # "maneuverTime":I
    :cond_6
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v26

    .line 717
    .local v26, "l2":J
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Time to build maneuver list:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sub-long v12, v26, v24

    invoke-virtual {v4, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 718
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->mapsEventHandler:Lcom/navdy/hud/app/maps/MapsEventHandler;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$1800()Lcom/navdy/hud/app/maps/MapsEventHandler;

    move-result-object v3

    new-instance v4, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;

    sget-object v5, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    const/4 v8, 0x0

    move-object/from16 v0, v31

    invoke-direct {v4, v5, v8, v0}, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;-><init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/maps/MapsEventHandler;->sendEventToClient(Lcom/squareup/wire/Message;)V

    .line 728
    .end local v2    # "current":Lcom/here/android/mpa/routing/Maneuver;
    .end local v6    # "streetAddress":Ljava/lang/String;
    .end local v7    # "previous":Lcom/here/android/mpa/routing/Maneuver;
    .end local v22    # "i":I
    .end local v23    # "len":I
    .end local v24    # "l1":J
    .end local v26    # "l2":J
    .end local v28    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    .end local v30    # "routeInfo":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    .end local v31    # "routeManeuvers":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/navigation/RouteManeuver;>;"
    :goto_3
    return-void

    .line 723
    :cond_7
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->mapsEventHandler:Lcom/navdy/hud/app/maps/MapsEventHandler;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$1800()Lcom/navdy/hud/app/maps/MapsEventHandler;

    move-result-object v3

    new-instance v4, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;

    sget-object v5, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_INVALID_REQUEST:Lcom/navdy/service/library/events/RequestStatus;

    const/4 v8, 0x0

    const/4 v11, 0x0

    invoke-direct {v4, v5, v8, v11}, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;-><init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/maps/MapsEventHandler;->sendEventToClient(Lcom/squareup/wire/Message;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 724
    :catch_0
    move-exception v32

    .line 725
    .local v32, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->mapsEventHandler:Lcom/navdy/hud/app/maps/MapsEventHandler;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$1800()Lcom/navdy/hud/app/maps/MapsEventHandler;

    move-result-object v3

    new-instance v4, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;

    sget-object v5, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SERVICE_ERROR:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual/range {v32 .. v32}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v11, 0x0

    invoke-direct {v4, v5, v8, v11}, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;-><init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/maps/MapsEventHandler;->sendEventToClient(Lcom/squareup/wire/Message;)V

    .line 726
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    move-object/from16 v0, v32

    invoke-virtual {v3, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_3
.end method
