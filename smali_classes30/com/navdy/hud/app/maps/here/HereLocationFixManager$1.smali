.class Lcom/navdy/hud/app/maps/here/HereLocationFixManager$1;
.super Ljava/lang/Object;
.source "HereLocationFixManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/here/HereLocationFixManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    const-wide/16 v12, 0xbb8

    const-wide/16 v10, 0x3e8

    const/4 v9, 0x0

    .line 93
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->lastAndroidLocationTime:J
    invoke-static {v6}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$000(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)J

    move-result-wide v6

    sub-long v0, v4, v6

    .line 94
    .local v0, "diff":J
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->locationFix:Z
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$100(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 96
    cmp-long v4, v0, v12

    if-ltz v4, :cond_1

    .line 97
    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "lost location fix["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 98
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    const/4 v5, 0x0

    # setter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->locationFix:Z
    invoke-static {v4, v5}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$102(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;Z)Z

    .line 99
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$300(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Lcom/squareup/otto/Bus;

    move-result-object v4

    new-instance v5, Lcom/navdy/hud/app/maps/MapEvents$LocationFix;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct {v5, v6, v7, v8}, Lcom/navdy/hud/app/maps/MapEvents$LocationFix;-><init>(ZZZ)V

    invoke-virtual {v4, v5}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 126
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->handler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$700(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Landroid/os/Handler;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->checkLocationFix:Ljava/lang/Runnable;
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$600(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Ljava/lang/Runnable;

    move-result-object v5

    invoke-virtual {v4, v5, v10, v11}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 127
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # setter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->firstLocationFixCheck:Z
    invoke-static {v4, v9}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$502(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;Z)Z

    .line 129
    .end local v0    # "diff":J
    :goto_1
    return-void

    .line 101
    .restart local v0    # "diff":J
    :cond_1
    :try_start_1
    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 102
    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "still has location fix ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 123
    .end local v0    # "diff":J
    :catch_0
    move-exception v3

    .line 124
    .local v3, "t":Ljava/lang/Throwable;
    :try_start_2
    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 126
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->handler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$700(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Landroid/os/Handler;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->checkLocationFix:Ljava/lang/Runnable;
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$600(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Ljava/lang/Runnable;

    move-result-object v5

    invoke-virtual {v4, v5, v10, v11}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 127
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # setter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->firstLocationFixCheck:Z
    invoke-static {v4, v9}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$502(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;Z)Z

    goto :goto_1

    .line 107
    .end local v3    # "t":Ljava/lang/Throwable;
    .restart local v0    # "diff":J
    :cond_2
    :try_start_3
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->lastAndroidLocation:Landroid/location/Location;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$400(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Landroid/location/Location;

    move-result-object v4

    if-eqz v4, :cond_3

    cmp-long v4, v0, v12

    if-gtz v4, :cond_3

    .line 108
    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "got location fix ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->lastAndroidLocation:Landroid/location/Location;
    invoke-static {v6}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$400(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Landroid/location/Location;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 109
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    const/4 v5, 0x1

    # setter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->locationFix:Z
    invoke-static {v4, v5}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$102(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;Z)Z

    .line 110
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLocationFixEvent()Lcom/navdy/hud/app/maps/MapEvents$LocationFix;

    move-result-object v2

    .line 111
    .local v2, "obj":Lcom/navdy/hud/app/maps/MapEvents$LocationFix;
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$300(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Lcom/squareup/otto/Bus;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 126
    .end local v0    # "diff":J
    .end local v2    # "obj":Lcom/navdy/hud/app/maps/MapEvents$LocationFix;
    :catchall_0
    move-exception v4

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->handler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$700(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Landroid/os/Handler;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->checkLocationFix:Ljava/lang/Runnable;
    invoke-static {v6}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$600(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Ljava/lang/Runnable;

    move-result-object v6

    invoke-virtual {v5, v6, v10, v11}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 127
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # setter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->firstLocationFixCheck:Z
    invoke-static {v5, v9}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$502(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;Z)Z

    throw v4

    .line 113
    .restart local v0    # "diff":J
    :cond_3
    :try_start_4
    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 114
    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "still don\'t have location fix ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 118
    :cond_4
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->firstLocationFixCheck:Z
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$500(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 119
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$300(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Lcom/squareup/otto/Bus;

    move-result-object v4

    new-instance v5, Lcom/navdy/hud/app/maps/MapEvents$LocationFix;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct {v5, v6, v7, v8}, Lcom/navdy/hud/app/maps/MapEvents$LocationFix;-><init>(ZZZ)V

    invoke-virtual {v4, v5}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0
.end method
