.class Lcom/navdy/hud/app/maps/here/HereLocationFixManager$6;
.super Ljava/lang/Object;
.source "HereLocationFixManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->onStopDriveRecording(Lcom/navdy/service/library/events/debug/StopDriveRecordingEvent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    .prologue
    .line 398
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$6;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 402
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$6;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->isRecording:Z
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$1000(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 403
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$6;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    const/4 v2, 0x0

    # setter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->isRecording:Z
    invoke-static {v1, v2}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$1002(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;Z)Z

    .line 404
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$6;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->recordingFile:Ljava/io/FileOutputStream;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$1500(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Ljava/io/FileOutputStream;

    move-result-object v1

    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->fileSync(Ljava/io/FileOutputStream;)V

    .line 405
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$6;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->recordingFile:Ljava/io/FileOutputStream;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$1500(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Ljava/io/FileOutputStream;

    move-result-object v1

    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 406
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$6;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    const/4 v2, 0x0

    # setter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->recordingFile:Ljava/io/FileOutputStream;
    invoke-static {v1, v2}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$1502(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;Ljava/io/FileOutputStream;)Ljava/io/FileOutputStream;

    .line 407
    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stopped recording file ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$6;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->recordingLabel:Ljava/lang/String;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$1400(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 408
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereLocationFixManager$6;->this$0:Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    const/4 v2, 0x0

    # setter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->recordingLabel:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$1402(Lcom/navdy/hud/app/maps/here/HereLocationFixManager;Ljava/lang/String;)Ljava/lang/String;

    .line 415
    :goto_0
    return-void

    .line 410
    :cond_0
    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "not recording"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 412
    :catch_0
    move-exception v0

    .line 413
    .local v0, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
