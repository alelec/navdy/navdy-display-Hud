.class public Lcom/navdy/hud/app/maps/here/HereNavigationInfo;
.super Ljava/lang/Object;
.source "HereNavigationInfo.java"


# instance fields
.field arrivedManeuverDisplay:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

.field currentRerouteReason:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

.field destination:Lcom/here/android/mpa/common/GeoCoordinate;

.field destinationDirection:Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;

.field destinationDirectionStr:Ljava/lang/String;

.field destinationIconId:I

.field public destinationIdentifier:Ljava/lang/String;

.field destinationLabel:Ljava/lang/String;

.field deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

.field firstManeuverShown:Z

.field firstManeuverShownTime:J

.field hasArrived:Z

.field ignoreArrived:Z

.field lastManeuver:Z

.field lastManeuverPostTime:J

.field lastRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

.field lastRouteId:Ljava/lang/String;

.field lastTrafficRerouteTime:J

.field maneuverAfterCurrent:Lcom/here/android/mpa/routing/Maneuver;

.field maneuverAfterCurrentIconid:I

.field maneuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

.field mapDestinationMarker:Lcom/here/android/mpa/mapping/MapMarker;

.field mapRoute:Lcom/here/android/mpa/mapping/MapRoute;

.field navigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

.field route:Lcom/here/android/mpa/routing/Route;

.field routeId:Ljava/lang/String;

.field routeOptions:Lcom/here/android/mpa/routing/RouteOptions;

.field simulationSpeed:I

.field startLocation:Lcom/here/android/mpa/common/GeoCoordinate;

.field streetAddress:Ljava/lang/String;

.field trafficRerouteCount:I

.field trafficRerouteOnce:Z

.field waypoints:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/common/GeoCoordinate;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->maneuverAfterCurrentIconid:I

    return-void
.end method


# virtual methods
.method clear()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 104
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->navigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->navigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->lastRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .line 106
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->routeId:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->lastRouteId:Ljava/lang/String;

    .line 112
    :goto_0
    iput-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->navigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .line 113
    iput-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->routeId:Ljava/lang/String;

    .line 114
    iput-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->route:Lcom/here/android/mpa/routing/Route;

    .line 115
    iput v3, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->simulationSpeed:I

    .line 116
    iput-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->streetAddress:Ljava/lang/String;

    .line 117
    iput-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destinationLabel:Ljava/lang/String;

    .line 118
    iput-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destinationIdentifier:Ljava/lang/String;

    .line 119
    iput-wide v4, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->lastManeuverPostTime:J

    .line 120
    iput-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    .line 121
    iput-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destinationDirection:Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;

    .line 122
    iput-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destinationDirectionStr:Ljava/lang/String;

    .line 123
    iput v3, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destinationIconId:I

    .line 124
    iput-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->maneuverAfterCurrent:Lcom/here/android/mpa/routing/Maneuver;

    .line 125
    iput v3, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->maneuverAfterCurrentIconid:I

    .line 126
    iput-boolean v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->hasArrived:Z

    .line 127
    iput-boolean v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->ignoreArrived:Z

    .line 128
    iput-boolean v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->lastManeuver:Z

    .line 129
    iput-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->arrivedManeuverDisplay:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    .line 130
    iput-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destination:Lcom/here/android/mpa/common/GeoCoordinate;

    .line 131
    iput v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->trafficRerouteCount:I

    .line 132
    iput-wide v4, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->lastTrafficRerouteTime:J

    .line 133
    iput-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->currentRerouteReason:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    .line 134
    iput-boolean v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->trafficRerouteOnce:Z

    .line 135
    iput-boolean v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->firstManeuverShown:Z

    .line 136
    iput-wide v4, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->firstManeuverShownTime:J

    .line 137
    iput-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->routeOptions:Lcom/here/android/mpa/routing/RouteOptions;

    .line 138
    iput-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->startLocation:Lcom/here/android/mpa/common/GeoCoordinate;

    .line 139
    return-void

    .line 108
    :cond_0
    iput-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->lastRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .line 109
    iput-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->lastRouteId:Ljava/lang/String;

    goto :goto_0
.end method

.method copy(Lcom/navdy/hud/app/maps/here/HereNavigationInfo;)V
    .locals 7
    .param p1, "info"    # Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    .prologue
    const/4 v6, 0x0

    .line 142
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->clear()V

    .line 143
    if-eqz p1, :cond_1

    .line 144
    iget-object v0, p1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->navigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->navigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .line 145
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->navigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    if-eqz v0, :cond_0

    .line 146
    new-instance v0, Lcom/here/android/mpa/common/GeoCoordinate;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->navigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v1, v1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v1, v1, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->navigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v1, v1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v1, v1, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    .line 147
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destination:Lcom/here/android/mpa/common/GeoCoordinate;

    .line 149
    :cond_0
    iget-object v0, p1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->routeId:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->routeId:Ljava/lang/String;

    .line 150
    iget-object v0, p1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->routeOptions:Lcom/here/android/mpa/routing/RouteOptions;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->routeOptions:Lcom/here/android/mpa/routing/RouteOptions;

    .line 151
    iget-object v0, p1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->route:Lcom/here/android/mpa/routing/Route;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->route:Lcom/here/android/mpa/routing/Route;

    .line 152
    iget v0, p1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->simulationSpeed:I

    iput v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->simulationSpeed:I

    .line 153
    iget-object v0, p1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->streetAddress:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->streetAddress:Ljava/lang/String;

    .line 154
    iget-object v0, p1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destinationLabel:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destinationLabel:Ljava/lang/String;

    .line 155
    iget-object v0, p1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destinationIdentifier:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destinationIdentifier:Ljava/lang/String;

    .line 156
    iget-object v0, p1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    .line 157
    iget-object v0, p1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destinationDirection:Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destinationDirection:Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;

    .line 158
    iget-object v0, p1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destinationDirectionStr:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destinationDirectionStr:Ljava/lang/String;

    .line 159
    iget v0, p1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destinationIconId:I

    iput v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destinationIconId:I

    .line 160
    iget-object v0, p1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->maneuverAfterCurrent:Lcom/here/android/mpa/routing/Maneuver;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->maneuverAfterCurrent:Lcom/here/android/mpa/routing/Maneuver;

    .line 161
    iget v0, p1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->maneuverAfterCurrentIconid:I

    iput v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->maneuverAfterCurrentIconid:I

    .line 162
    iput-object v6, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->lastRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .line 163
    iput-object v6, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->lastRouteId:Ljava/lang/String;

    .line 164
    iput-object v6, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->currentRerouteReason:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    .line 165
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->trafficRerouteOnce:Z

    .line 166
    iget-object v0, p1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->startLocation:Lcom/here/android/mpa/common/GeoCoordinate;

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->startLocation:Lcom/here/android/mpa/common/GeoCoordinate;

    .line 168
    :cond_1
    return-void
.end method

.method getLastNavigationRequest()Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->lastRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .line 172
    .local v0, "ret":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->lastRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .line 173
    return-object v0
.end method

.method getLastRouteId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->lastRouteId:Ljava/lang/String;

    .line 178
    .local v0, "ret":Ljava/lang/String;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->lastRouteId:Ljava/lang/String;

    .line 179
    return-object v0
.end method

.method getLastTrafficRerouteTime()J
    .locals 2

    .prologue
    .line 187
    iget-wide v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->lastTrafficRerouteTime:J

    return-wide v0
.end method

.method getTrafficReRouteCount()I
    .locals 1

    .prologue
    .line 183
    iget v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->trafficRerouteCount:I

    return v0
.end method

.method incrTrafficRerouteCount()V
    .locals 1

    .prologue
    .line 194
    iget v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->trafficRerouteCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->trafficRerouteCount:I

    return-void
.end method

.method setLastTrafficRerouteTime(J)V
    .locals 1
    .param p1, "l"    # J

    .prologue
    .line 191
    iput-wide p1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->lastTrafficRerouteTime:J

    .line 192
    return-void
.end method
