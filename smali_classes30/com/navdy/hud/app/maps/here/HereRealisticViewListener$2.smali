.class Lcom/navdy/hud/app/maps/here/HereRealisticViewListener$2;
.super Ljava/lang/Object;
.source "HereRealisticViewListener.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereRealisticViewListener$2;->this$0:Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRealisticViewListener$2;->this$0:Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;->navController:Lcom/navdy/hud/app/maps/here/HereNavController;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;->access$000(Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;)Lcom/navdy/hud/app/maps/here/HereNavController;

    move-result-object v0

    sget-object v1, Lcom/here/android/mpa/guidance/NavigationManager$RealisticViewMode;->OFF:Lcom/here/android/mpa/guidance/NavigationManager$RealisticViewMode;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereNavController;->setRealisticViewMode(Lcom/here/android/mpa/guidance/NavigationManager$RealisticViewMode;)V

    .line 40
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRealisticViewListener$2;->this$0:Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;->navController:Lcom/navdy/hud/app/maps/here/HereNavController;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;->access$000(Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;)Lcom/navdy/hud/app/maps/here/HereNavController;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereRealisticViewListener$2;->this$0:Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereNavController;->removeRealisticViewListener(Lcom/here/android/mpa/guidance/NavigationManager$RealisticViewListener;)V

    .line 41
    # getter for: Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "removed realistic view listener"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 42
    return-void
.end method
