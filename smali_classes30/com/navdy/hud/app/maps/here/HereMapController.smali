.class public Lcom/navdy/hud/app/maps/here/HereMapController;
.super Ljava/lang/Object;
.source "HereMapController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/maps/here/HereMapController$Callback;,
        Lcom/navdy/hud/app/maps/here/HereMapController$State;
    }
.end annotation


# static fields
.field private static final counter:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private final map:Lcom/here/android/mpa/mapping/Map;

.field private final mapBkHandler:Landroid/os/Handler;

.field private mapRouteCount:I

.field private volatile state:Lcom/navdy/hud/app/maps/here/HereMapController$State;

.field private transformCenter:Landroid/graphics/PointF;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereMapController;->counter:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 35
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereMapController;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/here/android/mpa/mapping/Map;Lcom/navdy/hud/app/maps/here/HereMapController$State;)V
    .locals 3
    .param p1, "map"    # Lcom/here/android/mpa/mapping/Map;
    .param p2, "state"    # Lcom/navdy/hud/app/maps/here/HereMapController$State;

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 78
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->map:Lcom/here/android/mpa/mapping/Map;

    .line 79
    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->state:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    .line 80
    new-instance v0, Landroid/os/HandlerThread;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HereMapController-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/maps/here/HereMapController;->counter:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 81
    .local v0, "handlerThread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 82
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->mapBkHandler:Landroid/os/Handler;

    .line 83
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/maps/here/HereMapController;)Lcom/here/android/mpa/mapping/Map;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapController;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->map:Lcom/here/android/mpa/mapping/Map;

    return-object v0
.end method

.method static synthetic access$100()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapController;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/maps/here/HereMapController;Lcom/here/android/mpa/mapping/MapObject;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapController;
    .param p1, "x1"    # Lcom/here/android/mpa/mapping/MapObject;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/maps/here/HereMapController;->addMapObjectInternal(Lcom/here/android/mpa/mapping/MapObject;)V

    return-void
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/maps/here/HereMapController;Lcom/here/android/mpa/mapping/MapObject;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapController;
    .param p1, "x1"    # Lcom/here/android/mpa/mapping/MapObject;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/maps/here/HereMapController;->removeMapObjectInternal(Lcom/here/android/mpa/mapping/MapObject;)V

    return-void
.end method

.method private addMapObjectInternal(Lcom/here/android/mpa/mapping/MapObject;)V
    .locals 5
    .param p1, "mapObject"    # Lcom/here/android/mpa/mapping/MapObject;

    .prologue
    .line 183
    instance-of v2, p1, Lcom/here/android/mpa/mapping/MapRoute;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 184
    check-cast v0, Lcom/here/android/mpa/mapping/MapRoute;

    .line 185
    .local v0, "mapRoute":Lcom/here/android/mpa/mapping/MapRoute;
    invoke-virtual {v0}, Lcom/here/android/mpa/mapping/MapRoute;->getRoute()Lcom/here/android/mpa/routing/Route;

    move-result-object v1

    .line 186
    .local v1, "route":Lcom/here/android/mpa/routing/Route;
    iget v2, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->mapRouteCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->mapRouteCount:I

    .line 187
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereMapController;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[map-route-added] maproute="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " route="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " count="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->mapRouteCount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 189
    .end local v0    # "mapRoute":Lcom/here/android/mpa/mapping/MapRoute;
    .end local v1    # "route":Lcom/here/android/mpa/routing/Route;
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->map:Lcom/here/android/mpa/mapping/Map;

    invoke-virtual {v2, p1}, Lcom/here/android/mpa/mapping/Map;->addMapObject(Lcom/here/android/mpa/mapping/MapObject;)Z

    .line 190
    return-void
.end method

.method private removeMapObjectInternal(Lcom/here/android/mpa/mapping/MapObject;)V
    .locals 5
    .param p1, "mapObject"    # Lcom/here/android/mpa/mapping/MapObject;

    .prologue
    .line 224
    instance-of v2, p1, Lcom/here/android/mpa/mapping/MapRoute;

    if-eqz v2, :cond_0

    .line 225
    iget v2, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->mapRouteCount:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->mapRouteCount:I

    move-object v0, p1

    .line 226
    check-cast v0, Lcom/here/android/mpa/mapping/MapRoute;

    .line 227
    .local v0, "mapRoute":Lcom/here/android/mpa/mapping/MapRoute;
    invoke-virtual {v0}, Lcom/here/android/mpa/mapping/MapRoute;->getRoute()Lcom/here/android/mpa/routing/Route;

    move-result-object v1

    .line 228
    .local v1, "route":Lcom/here/android/mpa/routing/Route;
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereMapController;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[map-route-removed] maproute="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " route="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " count="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->mapRouteCount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 230
    .end local v0    # "mapRoute":Lcom/here/android/mpa/mapping/MapRoute;
    .end local v1    # "route":Lcom/here/android/mpa/routing/Route;
    :cond_0
    if-eqz p1, :cond_1

    .line 231
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->map:Lcom/here/android/mpa/mapping/Map;

    invoke-virtual {v2, p1}, Lcom/here/android/mpa/mapping/Map;->removeMapObject(Lcom/here/android/mpa/mapping/MapObject;)Z

    .line 233
    :cond_1
    return-void
.end method


# virtual methods
.method public addMapObject(Lcom/here/android/mpa/mapping/MapObject;)V
    .locals 2
    .param p1, "mapObject"    # Lcom/here/android/mpa/mapping/MapObject;

    .prologue
    .line 174
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->mapBkHandler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereMapController$5;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/maps/here/HereMapController$5;-><init>(Lcom/navdy/hud/app/maps/here/HereMapController;Lcom/here/android/mpa/mapping/MapObject;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 180
    return-void
.end method

.method public addMapObjects(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/mapping/MapObject;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 193
    .local p1, "mapObjects":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/mapping/MapObject;>;"
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->mapBkHandler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereMapController$6;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/maps/here/HereMapController$6;-><init>(Lcom/navdy/hud/app/maps/here/HereMapController;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 201
    return-void
.end method

.method public addTransformListener(Lcom/here/android/mpa/mapping/Map$OnTransformListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/here/android/mpa/mapping/Map$OnTransformListener;

    .prologue
    .line 252
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->mapBkHandler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereMapController$10;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/maps/here/HereMapController$10;-><init>(Lcom/navdy/hud/app/maps/here/HereMapController;Lcom/here/android/mpa/mapping/Map$OnTransformListener;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 258
    return-void
.end method

.method public execute(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 305
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->mapBkHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 306
    return-void
.end method

.method public execute(Ljava/lang/Runnable;Lcom/navdy/hud/app/maps/here/HereMapController$Callback;)V
    .locals 2
    .param p1, "runnable"    # Ljava/lang/Runnable;
    .param p2, "cb"    # Lcom/navdy/hud/app/maps/here/HereMapController$Callback;

    .prologue
    .line 309
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->mapBkHandler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereMapController$14;

    invoke-direct {v1, p0, p1, p2}, Lcom/navdy/hud/app/maps/here/HereMapController$14;-><init>(Lcom/navdy/hud/app/maps/here/HereMapController;Ljava/lang/Runnable;Lcom/navdy/hud/app/maps/here/HereMapController$Callback;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 318
    return-void
.end method

.method public getCenter()Lcom/here/android/mpa/common/GeoCoordinate;
    .locals 1

    .prologue
    .line 97
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 98
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->map:Lcom/here/android/mpa/mapping/Map;

    invoke-virtual {v0}, Lcom/here/android/mpa/mapping/Map;->getCenter()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v0

    return-object v0
.end method

.method public getMap()Lcom/here/android/mpa/mapping/Map;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->map:Lcom/here/android/mpa/mapping/Map;

    return-object v0
.end method

.method public getMapScheme()Ljava/lang/String;
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->map:Lcom/here/android/mpa/mapping/Map;

    invoke-virtual {v0}, Lcom/here/android/mpa/mapping/Map;->getMapScheme()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOrientation()F
    .locals 1

    .prologue
    .line 122
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 123
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->map:Lcom/here/android/mpa/mapping/Map;

    invoke-virtual {v0}, Lcom/here/android/mpa/mapping/Map;->getOrientation()F

    move-result v0

    return v0
.end method

.method public getPositionIndicator()Lcom/here/android/mpa/mapping/PositionIndicator;
    .locals 1

    .prologue
    .line 287
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 288
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->map:Lcom/here/android/mpa/mapping/Map;

    invoke-virtual {v0}, Lcom/here/android/mpa/mapping/Map;->getPositionIndicator()Lcom/here/android/mpa/mapping/PositionIndicator;

    move-result-object v0

    return-object v0
.end method

.method public getState()Lcom/navdy/hud/app/maps/here/HereMapController$State;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->state:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    return-object v0
.end method

.method public getTilt()F
    .locals 1

    .prologue
    .line 108
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 109
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->map:Lcom/here/android/mpa/mapping/Map;

    invoke-virtual {v0}, Lcom/here/android/mpa/mapping/Map;->getTilt()F

    move-result v0

    return v0
.end method

.method public getZoomLevel()D
    .locals 2

    .prologue
    .line 102
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 103
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->map:Lcom/here/android/mpa/mapping/Map;

    invoke-virtual {v0}, Lcom/here/android/mpa/mapping/Map;->getZoomLevel()D

    move-result-wide v0

    return-wide v0
.end method

.method public projectToPixel(Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/here/android/mpa/mapping/Map$PixelResult;
    .locals 1
    .param p1, "latlng"    # Lcom/here/android/mpa/common/GeoCoordinate;

    .prologue
    .line 247
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 248
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->map:Lcom/here/android/mpa/mapping/Map;

    invoke-virtual {v0, p1}, Lcom/here/android/mpa/mapping/Map;->projectToPixel(Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/here/android/mpa/mapping/Map$PixelResult;

    move-result-object v0

    return-object v0
.end method

.method public removeMapObject(Lcom/here/android/mpa/mapping/MapObject;)V
    .locals 2
    .param p1, "mapObject"    # Lcom/here/android/mpa/mapping/MapObject;

    .prologue
    .line 204
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->mapBkHandler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereMapController$7;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/maps/here/HereMapController$7;-><init>(Lcom/navdy/hud/app/maps/here/HereMapController;Lcom/here/android/mpa/mapping/MapObject;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 210
    return-void
.end method

.method public removeMapObjects(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/mapping/MapObject;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 213
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/mapping/MapObject;>;"
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->mapBkHandler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereMapController$8;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/maps/here/HereMapController$8;-><init>(Lcom/navdy/hud/app/maps/here/HereMapController;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 221
    return-void
.end method

.method public removeTransformListener(Lcom/here/android/mpa/mapping/Map$OnTransformListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/here/android/mpa/mapping/Map$OnTransformListener;

    .prologue
    .line 261
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->mapBkHandler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereMapController$11;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/maps/here/HereMapController$11;-><init>(Lcom/navdy/hud/app/maps/here/HereMapController;Lcom/here/android/mpa/mapping/Map$OnTransformListener;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 267
    return-void
.end method

.method public setCenter(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;DFF)V
    .locals 9
    .param p1, "center"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p2, "animation"    # Lcom/here/android/mpa/mapping/Map$Animation;
    .param p3, "zoom"    # D
    .param p5, "orientation"    # F
    .param p6, "tilt"    # F

    .prologue
    .line 145
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->state:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapController$State;->AR_MODE:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    if-eq v0, v1, :cond_0

    .line 154
    :goto_0
    return-void

    .line 148
    :cond_0
    iget-object v8, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->mapBkHandler:Landroid/os/Handler;

    new-instance v0, Lcom/navdy/hud/app/maps/here/HereMapController$3;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/maps/here/HereMapController$3;-><init>(Lcom/navdy/hud/app/maps/here/HereMapController;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;DFF)V

    invoke-virtual {v8, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public setCenterForState(Lcom/navdy/hud/app/maps/here/HereMapController$State;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;DFF)V
    .locals 10
    .param p1, "state"    # Lcom/navdy/hud/app/maps/here/HereMapController$State;
    .param p2, "center"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p3, "animation"    # Lcom/here/android/mpa/mapping/Map$Animation;
    .param p4, "zoom"    # D
    .param p6, "orientation"    # F
    .param p7, "tilt"    # F

    .prologue
    .line 162
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->state:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    if-eq v0, p1, :cond_0

    .line 171
    :goto_0
    return-void

    .line 165
    :cond_0
    iget-object v8, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->mapBkHandler:Landroid/os/Handler;

    new-instance v0, Lcom/navdy/hud/app/maps/here/HereMapController$4;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    move/from16 v6, p6

    move/from16 v7, p7

    invoke-direct/range {v0 .. v7}, Lcom/navdy/hud/app/maps/here/HereMapController$4;-><init>(Lcom/navdy/hud/app/maps/here/HereMapController;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;DFF)V

    invoke-virtual {v8, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public setMapScheme(Ljava/lang/String;)V
    .locals 2
    .param p1, "mapScheme"    # Ljava/lang/String;

    .prologue
    .line 296
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->mapBkHandler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereMapController$13;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/maps/here/HereMapController$13;-><init>(Lcom/navdy/hud/app/maps/here/HereMapController;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 302
    return-void
.end method

.method public setState(Lcom/navdy/hud/app/maps/here/HereMapController$State;)V
    .locals 3
    .param p1, "state"    # Lcom/navdy/hud/app/maps/here/HereMapController$State;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->state:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    .line 93
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapController;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "state:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 94
    return-void
.end method

.method public setTilt(F)V
    .locals 2
    .param p1, "tilt"    # F

    .prologue
    .line 113
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->mapBkHandler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereMapController$1;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/maps/here/HereMapController$1;-><init>(Lcom/navdy/hud/app/maps/here/HereMapController;F)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 119
    return-void
.end method

.method public setTrafficInfoVisible(Z)V
    .locals 2
    .param p1, "b"    # Z

    .prologue
    .line 321
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->mapBkHandler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereMapController$15;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/maps/here/HereMapController$15;-><init>(Lcom/navdy/hud/app/maps/here/HereMapController;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 327
    return-void
.end method

.method public setTransformCenter(Landroid/graphics/PointF;)V
    .locals 2
    .param p1, "pointF"    # Landroid/graphics/PointF;

    .prologue
    .line 237
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->transformCenter:Landroid/graphics/PointF;

    .line 238
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->mapBkHandler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereMapController$9;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/maps/here/HereMapController$9;-><init>(Lcom/navdy/hud/app/maps/here/HereMapController;Landroid/graphics/PointF;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 244
    return-void
.end method

.method public setZoomLevel(D)V
    .locals 3
    .param p1, "zoomLevel"    # D

    .prologue
    .line 127
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->mapBkHandler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereMapController$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/navdy/hud/app/maps/here/HereMapController$2;-><init>(Lcom/navdy/hud/app/maps/here/HereMapController;D)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 138
    return-void
.end method

.method public zoomTo(Lcom/here/android/mpa/common/GeoBoundingBox;Lcom/here/android/mpa/common/ViewRect;Lcom/here/android/mpa/mapping/Map$Animation;F)V
    .locals 7
    .param p1, "boundingBox"    # Lcom/here/android/mpa/common/GeoBoundingBox;
    .param p2, "viewRect"    # Lcom/here/android/mpa/common/ViewRect;
    .param p3, "animation"    # Lcom/here/android/mpa/mapping/Map$Animation;
    .param p4, "orientation"    # F

    .prologue
    .line 273
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereMapController;->mapBkHandler:Landroid/os/Handler;

    new-instance v0, Lcom/navdy/hud/app/maps/here/HereMapController$12;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/maps/here/HereMapController$12;-><init>(Lcom/navdy/hud/app/maps/here/HereMapController;Lcom/here/android/mpa/common/GeoBoundingBox;Lcom/here/android/mpa/common/ViewRect;Lcom/here/android/mpa/mapping/Map$Animation;F)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 284
    return-void
.end method
