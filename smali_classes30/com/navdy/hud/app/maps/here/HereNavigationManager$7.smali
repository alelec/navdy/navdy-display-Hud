.class Lcom/navdy/hud/app/maps/here/HereNavigationManager$7;
.super Ljava/lang/Object;
.source "HereNavigationManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereNavigationManager;->updateNavigationInfo(Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

.field final synthetic val$clearPrevious:Z

.field final synthetic val$maneuverAfterNext:Lcom/here/android/mpa/routing/Maneuver;

.field final synthetic val$nextManeuver:Lcom/here/android/mpa/routing/Maneuver;

.field final synthetic val$previous:Lcom/here/android/mpa/routing/Maneuver;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereNavigationManager;Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    .prologue
    .line 1309
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$7;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$7;->val$nextManeuver:Lcom/here/android/mpa/routing/Maneuver;

    iput-object p3, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$7;->val$maneuverAfterNext:Lcom/here/android/mpa/routing/Maneuver;

    iput-object p4, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$7;->val$previous:Lcom/here/android/mpa/routing/Maneuver;

    iput-boolean p5, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$7;->val$clearPrevious:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 1313
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$7;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$7;->val$nextManeuver:Lcom/here/android/mpa/routing/Maneuver;

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$7;->val$maneuverAfterNext:Lcom/here/android/mpa/routing/Maneuver;

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$7;->val$previous:Lcom/here/android/mpa/routing/Maneuver;

    iget-boolean v5, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$7;->val$clearPrevious:Z

    # invokes: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->updateNavigationInfoInternal(Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;Z)V
    invoke-static {v1, v2, v3, v4, v5}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$1000(Lcom/navdy/hud/app/maps/here/HereNavigationManager;Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;Z)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1317
    :goto_0
    return-void

    .line 1314
    :catch_0
    move-exception v0

    .line 1315
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "updateNavigationInfoInternal"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
