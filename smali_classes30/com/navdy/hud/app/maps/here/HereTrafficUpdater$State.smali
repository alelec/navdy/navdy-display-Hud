.class public final enum Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;
.super Ljava/lang/Enum;
.source "HereTrafficUpdater.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/here/HereTrafficUpdater;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

.field public static final enum ACTIVE:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

.field public static final enum STOPPED:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

.field public static final enum TRACK_DISTANCE:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

.field public static final enum TRACK_JAM:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 560
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    const-string v1, "STOPPED"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->STOPPED:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    new-instance v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    const-string v1, "ACTIVE"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->ACTIVE:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    new-instance v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    const-string v1, "TRACK_DISTANCE"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->TRACK_DISTANCE:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    new-instance v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    const-string v1, "TRACK_JAM"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->TRACK_JAM:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->STOPPED:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->ACTIVE:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->TRACK_DISTANCE:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->TRACK_JAM:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    aput-object v1, v0, v5

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->$VALUES:[Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 560
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 560
    const-class v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;
    .locals 1

    .prologue
    .line 560
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->$VALUES:[Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/maps/here/HereTrafficUpdater$State;

    return-object v0
.end method
