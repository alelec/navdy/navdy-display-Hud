.class public Lcom/navdy/hud/app/maps/here/HereSpeedWarningListener;
.super Lcom/here/android/mpa/guidance/NavigationManager$SpeedWarningListener;
.source "HereSpeedWarningListener.java"


# static fields
.field private static final BUFFER_SPEED:I = 0x7


# instance fields
.field private bus:Lcom/squareup/otto/Bus;

.field private context:Landroid/content/Context;

.field private hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

.field private logger:Lcom/navdy/service/library/log/Logger;

.field private mapsEventHandler:Lcom/navdy/hud/app/maps/MapsEventHandler;

.field private speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

.field private speedWarningOn:Z

.field private tag:Ljava/lang/String;

.field private verbose:Z

.field private warningSpeed:I


# direct methods
.method constructor <init>(Lcom/navdy/service/library/log/Logger;Ljava/lang/String;ZLcom/squareup/otto/Bus;Lcom/navdy/hud/app/maps/MapsEventHandler;Lcom/navdy/hud/app/maps/here/HereNavigationManager;)V
    .locals 1
    .param p1, "logger"    # Lcom/navdy/service/library/log/Logger;
    .param p2, "tag"    # Ljava/lang/String;
    .param p3, "verbose"    # Z
    .param p4, "bus"    # Lcom/squareup/otto/Bus;
    .param p5, "mapsEventHandler"    # Lcom/navdy/hud/app/maps/MapsEventHandler;
    .param p6, "hereNavigationManager"    # Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/here/android/mpa/guidance/NavigationManager$SpeedWarningListener;-><init>()V

    .line 34
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningListener;->context:Landroid/content/Context;

    .line 35
    invoke-static {}, Lcom/navdy/hud/app/manager/SpeedManager;->getInstance()Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningListener;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    .line 41
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningListener;->logger:Lcom/navdy/service/library/log/Logger;

    .line 42
    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningListener;->tag:Ljava/lang/String;

    .line 43
    iput-boolean p3, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningListener;->verbose:Z

    .line 44
    iput-object p4, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningListener;->bus:Lcom/squareup/otto/Bus;

    .line 45
    iput-object p5, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningListener;->mapsEventHandler:Lcom/navdy/hud/app/maps/MapsEventHandler;

    .line 46
    iput-object p6, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    .line 47
    return-void
.end method


# virtual methods
.method public onSpeedExceeded(Ljava/lang/String;F)V
    .locals 12
    .param p1, "roadName"    # Ljava/lang/String;
    .param p2, "speedLimit"    # F

    .prologue
    const/4 v11, 0x1

    const/4 v10, -0x1

    .line 51
    iget-object v7, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningListener;->bus:Lcom/squareup/otto/Bus;

    sget-object v8, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->SPEED_EXCEEDED:Lcom/navdy/hud/app/maps/MapEvents$SpeedWarning;

    invoke-virtual {v7, v8}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 52
    iget-object v7, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningListener;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningListener;->tag:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " speed exceeded:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 53
    iget-boolean v7, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningListener;->speedWarningOn:Z

    if-nez v7, :cond_0

    .line 54
    iput v10, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningListener;->warningSpeed:I

    .line 56
    :cond_0
    iput-boolean v11, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningListener;->speedWarningOn:Z

    .line 57
    invoke-static {}, Lcom/navdy/hud/app/maps/MapsEventHandler;->getInstance()Lcom/navdy/hud/app/maps/MapsEventHandler;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/hud/app/maps/MapsEventHandler;->getNavigationPreferences()Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    move-result-object v1

    .line 58
    .local v1, "prefs":Lcom/navdy/service/library/events/preferences/NavigationPreferences;
    sget-object v7, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v8, v1, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->spokenSpeedLimitWarnings:Ljava/lang/Boolean;

    invoke-virtual {v7, v8}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 59
    invoke-static {}, Lcom/navdy/hud/app/framework/phonecall/CallUtils;->isPhoneCallInProgress()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 105
    :cond_1
    :goto_0
    return-void

    .line 62
    :cond_2
    iget-object v7, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningListener;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    invoke-virtual {v7}, Lcom/navdy/hud/app/manager/SpeedManager;->getCurrentSpeed()I

    move-result v0

    .line 63
    .local v0, "currentSpeed":I
    if-gtz v0, :cond_3

    .line 65
    iget-object v7, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningListener;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningListener;->tag:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "no obd or gps speed available"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 68
    :cond_3
    iget-object v7, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningListener;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    invoke-virtual {v7}, Lcom/navdy/hud/app/manager/SpeedManager;->getSpeedUnit()Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    move-result-object v4

    .line 69
    .local v4, "speedUnit":Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;
    float-to-double v8, p2

    sget-object v7, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->METERS_PER_SECOND:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    invoke-static {v8, v9, v7, v4}, Lcom/navdy/hud/app/manager/SpeedManager;->convert(DLcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;)I

    move-result v2

    .line 71
    .local v2, "speedAllowed":I
    iget v7, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningListener;->warningSpeed:I

    if-ne v7, v10, :cond_4

    .line 72
    add-int/lit8 v3, v2, 0x7

    .line 76
    .local v3, "speedToConsider":I
    :goto_1
    if-lt v0, v3, :cond_5

    .line 77
    iget-object v7, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningListener;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningListener;->tag:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " speed exceeded current["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "] threshold["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "] allowed["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "] "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 78
    invoke-virtual {v4}, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->name()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 77
    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 79
    iput v0, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningListener;->warningSpeed:I

    .line 86
    sget-object v7, Lcom/navdy/hud/app/maps/here/HereSpeedWarningListener$1;->$SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit:[I

    invoke-virtual {v4}, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    .line 100
    const-string v6, ""

    .line 103
    .local v6, "unit":Ljava/lang/String;
    :goto_2
    iget-object v7, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningListener;->context:Landroid/content/Context;

    const v8, 0x7f0902c4

    new-array v9, v11, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 104
    .local v5, "speedWarning":Ljava/lang/String;
    sget-object v7, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->SPEECH_SPEED_WARNING:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    const/4 v8, 0x0

    invoke-static {v5, v7, v8}, Lcom/navdy/hud/app/framework/voice/TTSUtils;->sendSpeechRequest(Ljava/lang/String;Lcom/navdy/service/library/events/audio/SpeechRequest$Category;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 74
    .end local v3    # "speedToConsider":I
    .end local v5    # "speedWarning":Ljava/lang/String;
    .end local v6    # "unit":Ljava/lang/String;
    :cond_4
    iget v7, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningListener;->warningSpeed:I

    iget v8, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningListener;->warningSpeed:I

    div-int/lit8 v8, v8, 0xa

    add-int v3, v7, v8

    .restart local v3    # "speedToConsider":I
    goto :goto_1

    .line 81
    :cond_5
    iget-object v7, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningListener;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningListener;->tag:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " speed exceeded current["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "] threshold["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "] allowed["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "] "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 82
    invoke-virtual {v4}, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->name()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 81
    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 88
    :pswitch_0
    iget-object v7, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    iget-object v6, v7, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->TTS_MILES:Ljava/lang/String;

    .line 89
    .restart local v6    # "unit":Ljava/lang/String;
    goto :goto_2

    .line 92
    .end local v6    # "unit":Ljava/lang/String;
    :pswitch_1
    iget-object v7, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    iget-object v6, v7, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->TTS_KMS:Ljava/lang/String;

    .line 93
    .restart local v6    # "unit":Ljava/lang/String;
    goto :goto_2

    .line 96
    .end local v6    # "unit":Ljava/lang/String;
    :pswitch_2
    iget-object v7, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    iget-object v6, v7, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->TTS_METERS:Ljava/lang/String;

    .line 97
    .restart local v6    # "unit":Ljava/lang/String;
    goto :goto_2

    .line 86
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onSpeedExceededEnd(Ljava/lang/String;F)V
    .locals 3
    .param p1, "roadName"    # Ljava/lang/String;
    .param p2, "speedLimit"    # F

    .prologue
    .line 109
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningListener;->speedWarningOn:Z

    .line 110
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningListener;->warningSpeed:I

    .line 111
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningListener;->bus:Lcom/squareup/otto/Bus;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->SPEED_NORMAL:Lcom/navdy/hud/app/maps/MapEvents$SpeedWarning;

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 112
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningListener;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningListener;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "speed normal:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 113
    return-void
.end method
