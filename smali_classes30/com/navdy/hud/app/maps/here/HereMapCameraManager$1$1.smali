.class Lcom/navdy/hud/app/maps/here/HereMapCameraManager$1$1;
.super Ljava/lang/Object;
.source "HereMapCameraManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereMapCameraManager$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/maps/here/HereMapCameraManager$1;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereMapCameraManager$1;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/maps/here/HereMapCameraManager$1;

    .prologue
    .line 124
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapCameraManager$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 127
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapCameraManager$1;

    iget-object v1, v1, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->isManualZoom()Z

    move-result v1

    if-nez v1, :cond_0

    .line 128
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "unlocking dial zoom:auto"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 129
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapCameraManager$1;

    iget-object v1, v1, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # invokes: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->zoomToDefault()V
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$500(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)V

    .line 143
    :goto_0
    return-void

    .line 131
    :cond_0
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "unlocking dial zoom:manual"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 134
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapCameraManager$1;

    iget-object v1, v1, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # invokes: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->isOverViewMapVisible()Z
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$600(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 135
    const v0, 0x461c4000    # 10000.0f

    .line 139
    .local v0, "zoomLevel":F
    :goto_1
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapCameraManager$1;

    iget-object v1, v1, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    new-instance v2, Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapCameraManager$1;

    iget-object v3, v3, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->localPreferences:Lcom/navdy/service/library/events/preferences/LocalPreferences;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$700(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Lcom/navdy/service/library/events/preferences/LocalPreferences;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;-><init>(Lcom/navdy/service/library/events/preferences/LocalPreferences;)V

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;->manualZoomLevel(Ljava/lang/Float;)Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;->build()Lcom/navdy/service/library/events/preferences/LocalPreferences;

    move-result-object v2

    # setter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->localPreferences:Lcom/navdy/service/library/events/preferences/LocalPreferences;
    invoke-static {v1, v2}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$702(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;Lcom/navdy/service/library/events/preferences/LocalPreferences;)Lcom/navdy/service/library/events/preferences/LocalPreferences;

    .line 140
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getDriverProfileManager()Lcom/navdy/hud/app/profile/DriverProfileManager;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapCameraManager$1;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->localPreferences:Lcom/navdy/service/library/events/preferences/LocalPreferences;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$700(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Lcom/navdy/service/library/events/preferences/LocalPreferences;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/profile/DriverProfileManager;->updateLocalPreferences(Lcom/navdy/service/library/events/preferences/LocalPreferences;)V

    .line 141
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "unlocking dial zoom:manual pref saved"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 137
    .end local v0    # "zoomLevel":F
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereMapCameraManager$1;

    iget-object v1, v1, Lcom/navdy/hud/app/maps/here/HereMapCameraManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapCameraManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereMapCameraManager;->access$300(Lcom/navdy/hud/app/maps/here/HereMapCameraManager;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereMapController;->getZoomLevel()D

    move-result-wide v2

    double-to-float v0, v2

    .restart local v0    # "zoomLevel":F
    goto :goto_1
.end method
