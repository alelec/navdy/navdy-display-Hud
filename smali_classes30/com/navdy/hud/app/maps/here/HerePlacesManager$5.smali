.class final Lcom/navdy/hud/app/maps/here/HerePlacesManager$5;
.super Ljava/lang/Object;
.source "HerePlacesManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HerePlacesManager;->handleAutoCompleteRequest(Lcom/navdy/service/library/events/places/AutoCompleteRequest;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$request:Lcom/navdy/service/library/events/places/AutoCompleteRequest;


# direct methods
.method constructor <init>(Lcom/navdy/service/library/events/places/AutoCompleteRequest;)V
    .locals 0

    .prologue
    .line 277
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5;->val$request:Lcom/navdy/service/library/events/places/AutoCompleteRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 18

    .prologue
    .line 280
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    .line 281
    .local v6, "l1":J
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5;->val$request:Lcom/navdy/service/library/events/places/AutoCompleteRequest;

    iget-object v4, v2, Lcom/navdy/service/library/events/places/AutoCompleteRequest;->partialSearch:Ljava/lang/String;

    .line 282
    .local v4, "autoCompleteSearch":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5;->val$request:Lcom/navdy/service/library/events/places/AutoCompleteRequest;

    iget-object v2, v2, Lcom/navdy/service/library/events/places/AutoCompleteRequest;->searchArea:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5;->val$request:Lcom/navdy/service/library/events/places/AutoCompleteRequest;

    iget-object v2, v2, Lcom/navdy/service/library/events/places/AutoCompleteRequest;->searchArea:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v13

    .line 284
    .local v13, "searchArea":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5;->val$request:Lcom/navdy/service/library/events/places/AutoCompleteRequest;

    iget-object v2, v2, Lcom/navdy/service/library/events/places/AutoCompleteRequest;->maxResults:Ljava/lang/Integer;

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5;->val$request:Lcom/navdy/service/library/events/places/AutoCompleteRequest;

    iget-object v2, v2, Lcom/navdy/service/library/events/places/AutoCompleteRequest;->maxResults:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v12

    .line 285
    .local v12, "n":I
    :goto_1
    if-lez v12, :cond_0

    const/16 v2, 0xa

    if-le v12, v2, :cond_1

    .line 286
    :cond_0
    const/16 v12, 0xa

    .line 289
    :cond_1
    move v5, v12

    .line 292
    .local v5, "maxResults":I
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5;->val$request:Lcom/navdy/service/library/events/places/AutoCompleteRequest;

    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->printAutoCompleteRequest(Lcom/navdy/service/library/events/places/AutoCompleteRequest;)V

    .line 294
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$100()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v2

    if-nez v2, :cond_5

    .line 295
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "engine not ready"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 296
    sget-object v2, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_NOT_READY:Lcom/navdy/service/library/events/RequestStatus;

    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->context:Landroid/content/Context;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$200()Landroid/content/Context;

    move-result-object v3

    const v15, 0x7f0901a2

    invoke-virtual {v3, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->returnAutoCompleteErrorResponse(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V
    invoke-static {v4, v2, v3}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$600(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 356
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 357
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    .line 358
    .local v10, "l2":J
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "handleAutoCompleteRequest- time-1 ="

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sub-long v16, v10, v6

    move-wide/from16 v0, v16

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 361
    .end local v10    # "l2":J
    :cond_2
    :goto_2
    return-void

    .line 282
    .end local v5    # "maxResults":I
    .end local v12    # "n":I
    .end local v13    # "searchArea":I
    :cond_3
    const/4 v13, -0x1

    goto :goto_0

    .line 284
    .restart local v13    # "searchArea":I
    :cond_4
    const/16 v12, 0xa

    goto :goto_1

    .line 300
    .restart local v5    # "maxResults":I
    .restart local v12    # "n":I
    :cond_5
    :try_start_1
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 301
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "invalid autocomplete query"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 302
    sget-object v2, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_INVALID_REQUEST:Lcom/navdy/service/library/events/RequestStatus;

    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->context:Landroid/content/Context;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$200()Landroid/content/Context;

    move-result-object v3

    const v15, 0x7f0900ea

    invoke-virtual {v3, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->returnAutoCompleteErrorResponse(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V
    invoke-static {v4, v2, v3}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$600(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 356
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 357
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    .line 358
    .restart local v10    # "l2":J
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "handleAutoCompleteRequest- time-1 ="

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sub-long v16, v10, v6

    move-wide/from16 v0, v16

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_2

    .line 306
    .end local v10    # "l2":J
    :cond_6
    :try_start_2
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$100()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v9

    .line 307
    .local v9, "hereLocationFixManager":Lcom/navdy/hud/app/maps/here/HereLocationFixManager;
    if-nez v9, :cond_7

    .line 308
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "engine not ready, location fix manager n/a"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 309
    sget-object v2, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_NOT_READY:Lcom/navdy/service/library/events/RequestStatus;

    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->context:Landroid/content/Context;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$200()Landroid/content/Context;

    move-result-object v3

    const v15, 0x7f0901a2

    invoke-virtual {v3, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->returnAutoCompleteErrorResponse(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V
    invoke-static {v4, v2, v3}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$600(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 356
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 357
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    .line 358
    .restart local v10    # "l2":J
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "handleAutoCompleteRequest- time-1 ="

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sub-long v16, v10, v6

    move-wide/from16 v0, v16

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 313
    .end local v10    # "l2":J
    :cond_7
    :try_start_3
    invoke-virtual {v9}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v8

    .line 314
    .local v8, "geoPosition":Lcom/here/android/mpa/common/GeoCoordinate;
    if-nez v8, :cond_8

    .line 315
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "start location n/a"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 316
    sget-object v2, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_NO_LOCATION_SERVICE:Lcom/navdy/service/library/events/RequestStatus;

    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->context:Landroid/content/Context;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$200()Landroid/content/Context;

    move-result-object v3

    const v15, 0x7f0901d9

    invoke-virtual {v3, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->returnAutoCompleteErrorResponse(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V
    invoke-static {v4, v2, v3}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$600(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 356
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 357
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    .line 358
    .restart local v10    # "l2":J
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "handleAutoCompleteRequest- time-1 ="

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sub-long v16, v10, v6

    move-wide/from16 v0, v16

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 320
    .end local v10    # "l2":J
    :cond_8
    :try_start_4
    new-instance v2, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v7}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$5$1;-><init>(Lcom/navdy/hud/app/maps/here/HerePlacesManager$5;Ljava/lang/String;IJ)V

    invoke-static {v8, v4, v13, v5, v2}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->autoComplete(Lcom/here/android/mpa/common/GeoCoordinate;Ljava/lang/String;IILcom/navdy/hud/app/maps/here/HerePlacesManager$AutoCompleteCallback;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 356
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 357
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    .line 358
    .restart local v10    # "l2":J
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "handleAutoCompleteRequest- time-1 ="

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sub-long v16, v10, v6

    move-wide/from16 v0, v16

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 352
    .end local v8    # "geoPosition":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v9    # "hereLocationFixManager":Lcom/navdy/hud/app/maps/here/HereLocationFixManager;
    .end local v10    # "l2":J
    :catch_0
    move-exception v14

    .line 353
    .local v14, "t":Ljava/lang/Throwable;
    :try_start_5
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    invoke-virtual {v2, v14}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 354
    sget-object v2, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_UNKNOWN_ERROR:Lcom/navdy/service/library/events/RequestStatus;

    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->context:Landroid/content/Context;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$200()Landroid/content/Context;

    move-result-object v3

    const v15, 0x7f0902da

    invoke-virtual {v3, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->returnAutoCompleteErrorResponse(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V
    invoke-static {v4, v2, v3}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$600(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 356
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 357
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    .line 358
    .restart local v10    # "l2":J
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "handleAutoCompleteRequest- time-1 ="

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sub-long v16, v10, v6

    move-wide/from16 v0, v16

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 356
    .end local v10    # "l2":J
    .end local v14    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v2

    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const/4 v15, 0x2

    invoke-virtual {v3, v15}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 357
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    .line 358
    .restart local v10    # "l2":J
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "handleAutoCompleteRequest- time-1 ="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sub-long v16, v10, v6

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v3, v15}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 359
    .end local v10    # "l2":J
    :cond_9
    throw v2
.end method
