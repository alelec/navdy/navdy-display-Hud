.class Lcom/navdy/hud/app/maps/here/HereMapsManager$2;
.super Ljava/lang/Object;
.source "HereMapsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereMapsManager;->initMapLoader()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereMapsManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .prologue
    .line 402
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 405
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 406
    .local v2, "l1":J
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-static {}, Lcom/here/android/mpa/odml/MapLoader;->getInstance()Lcom/here/android/mpa/odml/MapLoader;

    move-result-object v5

    # setter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapLoader:Lcom/here/android/mpa/odml/MapLoader;
    invoke-static {v4, v5}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$2302(Lcom/navdy/hud/app/maps/here/HereMapsManager;Lcom/here/android/mpa/odml/MapLoader;)Lcom/here/android/mpa/odml/MapLoader;

    .line 407
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long v0, v4, v2

    .line 408
    .local v0, "diff":J
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "initMapLoader took ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 409
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapLoader:Lcom/here/android/mpa/odml/MapLoader;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$2300(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Lcom/here/android/mpa/odml/MapLoader;

    move-result-object v4

    new-instance v5, Lcom/navdy/hud/app/maps/here/HereMapsManager$2$1;

    invoke-direct {v5, p0}, Lcom/navdy/hud/app/maps/here/HereMapsManager$2$1;-><init>(Lcom/navdy/hud/app/maps/here/HereMapsManager$2;)V

    invoke-virtual {v4, v5}, Lcom/here/android/mpa/odml/MapLoader;->addListener(Lcom/here/android/mpa/odml/MapLoader$Listener;)V

    .line 458
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    const/4 v5, 0x0

    # setter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapPackageCount:I
    invoke-static {v4, v5}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$2502(Lcom/navdy/hud/app/maps/here/HereMapsManager;I)I

    .line 459
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$2;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # invokes: Lcom/navdy/hud/app/maps/here/HereMapsManager;->invokeMapLoader()V
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$2700(Lcom/navdy/hud/app/maps/here/HereMapsManager;)V

    .line 460
    return-void
.end method
