.class synthetic Lcom/navdy/hud/app/maps/here/HereNavigationManager$12;
.super Ljava/lang/Object;
.source "HereNavigationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/here/HereNavigationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit:[I

.field static final synthetic $SwitchMap$com$navdy$hud$app$maps$NavigationMode:[I

.field static final synthetic $SwitchMap$com$navdy$hud$app$maps$here$HereMapController$State:[I

.field static final synthetic $SwitchMap$com$navdy$service$library$events$navigation$NavigationSessionState:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1981
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapController$State;->values()[Lcom/navdy/hud/app/maps/here/HereMapController$State;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$12;->$SwitchMap$com$navdy$hud$app$maps$here$HereMapController$State:[I

    :try_start_0
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$12;->$SwitchMap$com$navdy$hud$app$maps$here$HereMapController$State:[I

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapController$State;->AR_MODE:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereMapController$State;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_8

    .line 1715
    :goto_0
    invoke-static {}, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->values()[Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$12;->$SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit:[I

    :try_start_1
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$12;->$SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit:[I

    sget-object v1, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->KILOMETERS_PER_HOUR:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_7

    :goto_1
    :try_start_2
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$12;->$SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit:[I

    sget-object v1, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->MILES_PER_HOUR:Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_6

    .line 1219
    :goto_2
    invoke-static {}, Lcom/navdy/hud/app/maps/NavigationMode;->values()[Lcom/navdy/hud/app/maps/NavigationMode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$12;->$SwitchMap$com$navdy$hud$app$maps$NavigationMode:[I

    :try_start_3
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$12;->$SwitchMap$com$navdy$hud$app$maps$NavigationMode:[I

    sget-object v1, Lcom/navdy/hud/app/maps/NavigationMode;->MAP:Lcom/navdy/hud/app/maps/NavigationMode;

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/NavigationMode;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_5

    :goto_3
    :try_start_4
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$12;->$SwitchMap$com$navdy$hud$app$maps$NavigationMode:[I

    sget-object v1, Lcom/navdy/hud/app/maps/NavigationMode;->MAP_ON_ROUTE:Lcom/navdy/hud/app/maps/NavigationMode;

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/NavigationMode;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :goto_4
    :try_start_5
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$12;->$SwitchMap$com$navdy$hud$app$maps$NavigationMode:[I

    sget-object v1, Lcom/navdy/hud/app/maps/NavigationMode;->TBT_ON_ROUTE:Lcom/navdy/hud/app/maps/NavigationMode;

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/NavigationMode;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_3

    .line 943
    :goto_5
    invoke-static {}, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->values()[Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$12;->$SwitchMap$com$navdy$service$library$events$navigation$NavigationSessionState:[I

    :try_start_6
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$12;->$SwitchMap$com$navdy$service$library$events$navigation$NavigationSessionState:[I

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_PAUSED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_2

    :goto_6
    :try_start_7
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$12;->$SwitchMap$com$navdy$service$library$events$navigation$NavigationSessionState:[I

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_STOPPED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_1

    :goto_7
    :try_start_8
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$12;->$SwitchMap$com$navdy$service$library$events$navigation$NavigationSessionState:[I

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_STARTED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_0

    :goto_8
    return-void

    :catch_0
    move-exception v0

    goto :goto_8

    :catch_1
    move-exception v0

    goto :goto_7

    :catch_2
    move-exception v0

    goto :goto_6

    .line 1219
    :catch_3
    move-exception v0

    goto :goto_5

    :catch_4
    move-exception v0

    goto :goto_4

    :catch_5
    move-exception v0

    goto :goto_3

    .line 1715
    :catch_6
    move-exception v0

    goto :goto_2

    :catch_7
    move-exception v0

    goto :goto_1

    .line 1981
    :catch_8
    move-exception v0

    goto/16 :goto_0
.end method
