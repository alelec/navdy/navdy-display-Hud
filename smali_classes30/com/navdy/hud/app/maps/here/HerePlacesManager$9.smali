.class final Lcom/navdy/hud/app/maps/here/HerePlacesManager$9;
.super Ljava/lang/Object;
.source "HerePlacesManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HerePlacesManager;->returnSearchResults(Lcom/here/android/mpa/common/GeoCoordinate;Ljava/lang/String;Ljava/util/List;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$currentLocation:Lcom/here/android/mpa/common/GeoCoordinate;

.field final synthetic val$destinationLinks:Ljava/util/List;

.field final synthetic val$maxResults:I

.field final synthetic val$queryText:Ljava/lang/String;

.field final synthetic val$searchArea:I


# direct methods
.method constructor <init>(ILjava/util/List;Lcom/here/android/mpa/common/GeoCoordinate;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 620
    iput p1, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$9;->val$maxResults:I

    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$9;->val$destinationLinks:Ljava/util/List;

    iput-object p3, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$9;->val$currentLocation:Lcom/here/android/mpa/common/GeoCoordinate;

    iput p4, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$9;->val$searchArea:I

    iput-object p5, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$9;->val$queryText:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 24

    .prologue
    .line 624
    const/4 v10, 0x0

    .line 625
    .local v10, "count":I
    :try_start_0
    new-instance v16, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$9;->val$maxResults:I

    move-object/from16 v0, v16

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 627
    .local v16, "searchResults":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/places/PlacesSearchResult;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$9;->val$destinationLinks:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :cond_0
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/here/android/mpa/search/PlaceLink;

    .line 628
    .local v11, "placeLink":Lcom/here/android/mpa/search/PlaceLink;
    invoke-virtual {v11}, Lcom/here/android/mpa/search/PlaceLink;->getPosition()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v14

    .line 630
    .local v14, "placesLocationCoordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    if-eqz v14, :cond_0

    .line 634
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$9;->val$currentLocation:Lcom/here/android/mpa/common/GeoCoordinate;

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$9;->val$searchArea:I

    if-lez v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$9;->val$currentLocation:Lcom/here/android/mpa/common/GeoCoordinate;

    invoke-virtual {v2, v14}, Lcom/here/android/mpa/common/GeoCoordinate;->distanceTo(Lcom/here/android/mpa/common/GeoCoordinate;)D

    move-result-wide v20

    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$9;->val$searchArea:I

    int-to-double v0, v2

    move-wide/from16 v22, v0

    cmpl-double v2, v20, v22

    if-gtz v2, :cond_0

    .line 638
    :cond_1
    new-instance v2, Lcom/navdy/service/library/events/location/Coordinate$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/location/Coordinate$Builder;-><init>()V

    .line 639
    invoke-virtual {v14}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->latitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v2

    .line 640
    invoke-virtual {v14}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->longitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v2

    .line 641
    invoke-virtual {v2}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->build()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v5

    .line 643
    .local v5, "coordinate":Lcom/navdy/service/library/events/location/Coordinate;
    invoke-virtual {v11}, Lcom/here/android/mpa/search/PlaceLink;->getVicinity()Ljava/lang/String;

    move-result-object v6

    .line 644
    .local v6, "address":Ljava/lang/String;
    invoke-virtual {v11}, Lcom/here/android/mpa/search/PlaceLink;->getTitle()Ljava/lang/String;

    move-result-object v3

    .line 645
    .local v3, "title":Ljava/lang/String;
    const/4 v7, 0x0

    .line 646
    .local v7, "categoryStr":Ljava/lang/String;
    invoke-virtual {v11}, Lcom/here/android/mpa/search/PlaceLink;->getDistance()D

    move-result-wide v12

    .line 649
    .local v12, "distance":D
    if-eqz v6, :cond_2

    .line 650
    const-string v2, "<br/>"

    const-string v4, ", "

    invoke-virtual {v6, v2, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    .line 653
    :cond_2
    invoke-virtual {v11}, Lcom/here/android/mpa/search/PlaceLink;->getCategory()Lcom/here/android/mpa/search/Category;

    move-result-object v9

    .line 654
    .local v9, "category":Lcom/here/android/mpa/search/Category;
    if-eqz v9, :cond_3

    .line 655
    invoke-virtual {v9}, Lcom/here/android/mpa/search/Category;->getName()Ljava/lang/String;

    move-result-object v7

    .line 658
    :cond_3
    new-instance v2, Lcom/navdy/service/library/events/places/PlacesSearchResult;

    const/4 v4, 0x0

    invoke-static {v12, v13}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v8

    invoke-direct/range {v2 .. v8}, Lcom/navdy/service/library/events/places/PlacesSearchResult;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/location/Coordinate;Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Double;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 660
    add-int/lit8 v10, v10, 0x1

    .line 661
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$9;->val$maxResults:I

    if-ne v10, v2, :cond_0

    .line 662
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[search] max results reached:"

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v8, v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$9;->val$maxResults:I

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 667
    .end local v3    # "title":Ljava/lang/String;
    .end local v5    # "coordinate":Lcom/navdy/service/library/events/location/Coordinate;
    .end local v6    # "address":Ljava/lang/String;
    .end local v7    # "categoryStr":Ljava/lang/String;
    .end local v9    # "category":Lcom/here/android/mpa/search/Category;
    .end local v11    # "placeLink":Lcom/here/android/mpa/search/PlaceLink;
    .end local v12    # "distance":D
    .end local v14    # "placesLocationCoordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    :cond_4
    new-instance v2, Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$9;->val$queryText:Ljava/lang/String;

    .line 668
    invoke-virtual {v2, v4}, Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;->searchQuery(Ljava/lang/String;)Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;

    move-result-object v2

    sget-object v4, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    .line 669
    invoke-virtual {v2, v4}, Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;->status(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;

    move-result-object v2

    const/4 v4, 0x0

    .line 670
    invoke-virtual {v2, v4}, Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;->statusDetail(Ljava/lang/String;)Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;

    move-result-object v2

    .line 671
    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;->results(Ljava/util/List;)Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;

    move-result-object v2

    .line 672
    invoke-virtual {v2}, Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;->build()Lcom/navdy/service/library/events/places/PlacesSearchResponse;

    move-result-object v15

    .line 673
    .local v15, "response":Lcom/navdy/service/library/events/places/PlacesSearchResponse;
    invoke-static {}, Lcom/navdy/hud/app/maps/MapsEventHandler;->getInstance()Lcom/navdy/hud/app/maps/MapsEventHandler;

    move-result-object v2

    invoke-virtual {v2, v15}, Lcom/navdy/hud/app/maps/MapsEventHandler;->sendEventToClient(Lcom/squareup/wire/Message;)V

    .line 674
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[search] returned:"

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 679
    .end local v15    # "response":Lcom/navdy/service/library/events/places/PlacesSearchResponse;
    .end local v16    # "searchResults":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/places/PlacesSearchResult;>;"
    :goto_0
    return-void

    .line 675
    :catch_0
    move-exception v17

    .line 676
    .local v17, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 677
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$9;->val$queryText:Ljava/lang/String;

    sget-object v4, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_UNKNOWN_ERROR:Lcom/navdy/service/library/events/RequestStatus;

    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->context:Landroid/content/Context;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$200()Landroid/content/Context;

    move-result-object v8

    const v18, 0x7f0902da

    move/from16 v0, v18

    invoke-virtual {v8, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    # invokes: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->returnSearchErrorResponse(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V
    invoke-static {v2, v4, v8}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$300(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V

    goto :goto_0
.end method
