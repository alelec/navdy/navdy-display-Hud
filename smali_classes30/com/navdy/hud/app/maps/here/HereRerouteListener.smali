.class public Lcom/navdy/hud/app/maps/here/HereRerouteListener;
.super Lcom/here/android/mpa/guidance/NavigationManager$RerouteListener;
.source "HereRerouteListener.java"


# static fields
.field public static final REROUTE_FAILED:Lcom/navdy/hud/app/maps/MapEvents$RerouteEvent;

.field public static final REROUTE_FINISHED:Lcom/navdy/hud/app/maps/MapEvents$RerouteEvent;

.field public static final REROUTE_STARTED:Lcom/navdy/hud/app/maps/MapEvents$RerouteEvent;


# instance fields
.field private bus:Lcom/squareup/otto/Bus;

.field private hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

.field private logger:Lcom/navdy/service/library/log/Logger;

.field private final navigationQualityTracker:Lcom/navdy/hud/app/analytics/NavigationQualityTracker;

.field private routeCalcId:Ljava/lang/String;

.field private volatile routeCalculationOn:Z

.field private tag:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 34
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$RerouteEvent;

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;->STARTED:Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/maps/MapEvents$RerouteEvent;-><init>(Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->REROUTE_STARTED:Lcom/navdy/hud/app/maps/MapEvents$RerouteEvent;

    .line 35
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$RerouteEvent;

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;->FINISHED:Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/maps/MapEvents$RerouteEvent;-><init>(Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->REROUTE_FINISHED:Lcom/navdy/hud/app/maps/MapEvents$RerouteEvent;

    .line 36
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$RerouteEvent;

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;->FAILED:Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/maps/MapEvents$RerouteEvent;-><init>(Lcom/navdy/hud/app/maps/MapEvents$RouteEventType;)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->REROUTE_FAILED:Lcom/navdy/hud/app/maps/MapEvents$RerouteEvent;

    return-void
.end method

.method constructor <init>(Lcom/navdy/service/library/log/Logger;Ljava/lang/String;Lcom/navdy/hud/app/maps/here/HereNavigationManager;Lcom/squareup/otto/Bus;)V
    .locals 2
    .param p1, "logger"    # Lcom/navdy/service/library/log/Logger;
    .param p2, "tag"    # Ljava/lang/String;
    .param p3, "hereNavigationManager"    # Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    .param p4, "bus"    # Lcom/squareup/otto/Bus;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/here/android/mpa/guidance/NavigationManager$RerouteListener;-><init>()V

    .line 42
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/maps/here/HereRerouteListener;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;

    .line 43
    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->tag:Ljava/lang/String;

    .line 44
    iput-object p3, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    .line 45
    iput-object p4, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->bus:Lcom/squareup/otto/Bus;

    .line 47
    invoke-static {}, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->getInstance()Lcom/navdy/hud/app/analytics/NavigationQualityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->navigationQualityTracker:Lcom/navdy/hud/app/analytics/NavigationQualityTracker;

    .line 48
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/maps/here/HereRerouteListener;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereRerouteListener;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->routeCalcId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Lcom/navdy/hud/app/maps/here/HereRerouteListener;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereRerouteListener;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->routeCalcId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/maps/here/HereRerouteListener;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereRerouteListener;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/maps/here/HereRerouteListener;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereRerouteListener;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->tag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/maps/here/HereRerouteListener;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereRerouteListener;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$402(Lcom/navdy/hud/app/maps/here/HereRerouteListener;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereRerouteListener;
    .param p1, "x1"    # Z

    .prologue
    .line 21
    iput-boolean p1, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->routeCalculationOn:Z

    return p1
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/maps/here/HereRerouteListener;)Lcom/squareup/otto/Bus;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereRerouteListener;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->bus:Lcom/squareup/otto/Bus;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/maps/here/HereRerouteListener;)Lcom/navdy/hud/app/analytics/NavigationQualityTracker;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereRerouteListener;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->navigationQualityTracker:Lcom/navdy/hud/app/analytics/NavigationQualityTracker;

    return-object v0
.end method


# virtual methods
.method public isRecalculating()Z
    .locals 1

    .prologue
    .line 116
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->routeCalculationOn:Z

    return v0
.end method

.method public onRerouteBegin()V
    .locals 3

    .prologue
    .line 52
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereRerouteListener$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/maps/here/HereRerouteListener$1;-><init>(Lcom/navdy/hud/app/maps/here/HereRerouteListener;)V

    const/16 v2, 0x14

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 67
    return-void
.end method

.method public onRerouteEnd(Lcom/here/android/mpa/routing/Route;)V
    .locals 3
    .param p1, "route"    # Lcom/here/android/mpa/routing/Route;

    .prologue
    .line 71
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereRerouteListener$2;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/maps/here/HereRerouteListener$2;-><init>(Lcom/navdy/hud/app/maps/here/HereRerouteListener;Lcom/here/android/mpa/routing/Route;)V

    const/16 v2, 0x14

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 95
    return-void
.end method

.method public onRerouteFailed()V
    .locals 3

    .prologue
    .line 99
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereRerouteListener$3;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/maps/here/HereRerouteListener$3;-><init>(Lcom/navdy/hud/app/maps/here/HereRerouteListener;)V

    const/16 v2, 0x14

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 113
    return-void
.end method
