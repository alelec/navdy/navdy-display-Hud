.class Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1$1;
.super Ljava/lang/Object;
.source "HerePlacesManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;->result(Lcom/here/android/mpa/search/ErrorCode;Lcom/here/android/mpa/search/DiscoveryResultPage;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;

.field final synthetic val$errorCode:Lcom/here/android/mpa/search/ErrorCode;

.field final synthetic val$result:Lcom/here/android/mpa/search/DiscoveryResultPage;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;Lcom/here/android/mpa/search/ErrorCode;Lcom/here/android/mpa/search/DiscoveryResultPage;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;

    .prologue
    .line 229
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;

    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1$1;->val$errorCode:Lcom/here/android/mpa/search/ErrorCode;

    iput-object p3, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1$1;->val$result:Lcom/here/android/mpa/search/DiscoveryResultPage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    const/4 v11, 0x2

    .line 233
    :try_start_0
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1$1;->val$errorCode:Lcom/here/android/mpa/search/ErrorCode;

    sget-object v5, Lcom/here/android/mpa/search/ErrorCode;->NONE:Lcom/here/android/mpa/search/ErrorCode;

    if-eq v4, v5, :cond_1

    .line 234
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;->val$searchQuery:Ljava/lang/String;

    sget-object v5, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SERVICE_ERROR:Lcom/navdy/service/library/events/RequestStatus;

    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->context:Landroid/content/Context;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$200()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f090255

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1$1;->val$errorCode:Lcom/here/android/mpa/search/ErrorCode;

    invoke-virtual {v10}, Lcom/here/android/mpa/search/ErrorCode;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    # invokes: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->returnSearchErrorResponse(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V
    invoke-static {v4, v5, v6}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$300(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 254
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    invoke-virtual {v4, v11}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 255
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 256
    .local v0, "l2":J
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[search] handleSearchRequest- time-2 ="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;

    iget-wide v6, v6, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;->val$l1:J

    sub-long v6, v0, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 259
    .end local v0    # "l2":J
    :cond_0
    :goto_0
    return-void

    .line 237
    :cond_1
    :try_start_1
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1$1;->val$result:Lcom/here/android/mpa/search/DiscoveryResultPage;

    if-eqz v4, :cond_4

    .line 238
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1$1;->val$result:Lcom/here/android/mpa/search/DiscoveryResultPage;

    invoke-virtual {v4}, Lcom/here/android/mpa/search/DiscoveryResultPage;->getPlaceLinks()Ljava/util/List;

    move-result-object v2

    .line 239
    .local v2, "links":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/search/PlaceLink;>;"
    if-eqz v2, :cond_2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_3

    .line 240
    :cond_2
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "[search] no results"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 241
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;->val$geoPosition:Lcom/here/android/mpa/common/GeoCoordinate;

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;->val$searchQuery:Ljava/lang/String;

    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->EMPTY_RESULT:Ljava/util/ArrayList;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$400()Ljava/util/ArrayList;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;

    iget v7, v7, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;->val$maxResults:I

    iget-object v8, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;

    iget v8, v8, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;->val$searchArea:I

    # invokes: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->returnSearchResults(Lcom/here/android/mpa/common/GeoCoordinate;Ljava/lang/String;Ljava/util/List;II)V
    invoke-static {v4, v5, v6, v7, v8}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$500(Lcom/here/android/mpa/common/GeoCoordinate;Ljava/lang/String;Ljava/util/List;II)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 254
    .end local v2    # "links":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/search/PlaceLink;>;"
    :goto_1
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    invoke-virtual {v4, v11}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 255
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 256
    .restart local v0    # "l2":J
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[search] handleSearchRequest- time-2 ="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;

    iget-wide v6, v6, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;->val$l1:J

    sub-long v6, v0, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 243
    .end local v0    # "l2":J
    .restart local v2    # "links":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/search/PlaceLink;>;"
    :cond_3
    :try_start_2
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[search] returned results:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 244
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;->val$geoPosition:Lcom/here/android/mpa/common/GeoCoordinate;

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;->val$searchQuery:Ljava/lang/String;

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;

    iget v6, v6, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;->val$maxResults:I

    iget-object v7, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;

    iget v7, v7, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;->val$searchArea:I

    # invokes: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->returnSearchResults(Lcom/here/android/mpa/common/GeoCoordinate;Ljava/lang/String;Ljava/util/List;II)V
    invoke-static {v4, v5, v2, v6, v7}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$500(Lcom/here/android/mpa/common/GeoCoordinate;Ljava/lang/String;Ljava/util/List;II)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 250
    .end local v2    # "links":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/search/PlaceLink;>;"
    :catch_0
    move-exception v3

    .line 251
    .local v3, "t":Ljava/lang/Throwable;
    :try_start_3
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 252
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;->val$searchQuery:Ljava/lang/String;

    sget-object v5, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_UNKNOWN_ERROR:Lcom/navdy/service/library/events/RequestStatus;

    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->context:Landroid/content/Context;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$200()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f0902da

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    # invokes: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->returnSearchErrorResponse(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V
    invoke-static {v4, v5, v6}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$300(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 254
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    invoke-virtual {v4, v11}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 255
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 256
    .restart local v0    # "l2":J
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[search] handleSearchRequest- time-2 ="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;

    iget-wide v6, v6, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;->val$l1:J

    sub-long v6, v0, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 247
    .end local v0    # "l2":J
    .end local v3    # "t":Ljava/lang/Throwable;
    :cond_4
    :try_start_4
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "[search] no results"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 248
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;

    iget-object v4, v4, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;->val$geoPosition:Lcom/here/android/mpa/common/GeoCoordinate;

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;->val$searchQuery:Ljava/lang/String;

    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->EMPTY_RESULT:Ljava/util/ArrayList;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$400()Ljava/util/ArrayList;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;

    iget v7, v7, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;->val$maxResults:I

    iget-object v8, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;

    iget v8, v8, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;->val$searchArea:I

    # invokes: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->returnSearchResults(Lcom/here/android/mpa/common/GeoCoordinate;Ljava/lang/String;Ljava/util/List;II)V
    invoke-static {v4, v5, v6, v7, v8}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$500(Lcom/here/android/mpa/common/GeoCoordinate;Ljava/lang/String;Ljava/util/List;II)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1

    .line 254
    :catchall_0
    move-exception v4

    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    invoke-virtual {v5, v11}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 255
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 256
    .restart local v0    # "l2":J
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[search] handleSearchRequest- time-2 ="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;

    iget-wide v8, v7, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;->val$l1:J

    sub-long v8, v0, v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 257
    .end local v0    # "l2":J
    :cond_5
    throw v4
.end method
