.class public Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;
.super Ljava/lang/Object;
.source "HereLaneInfoBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;
    }
.end annotation


# static fields
.field private static initialized:Z

.field private static sLaneDrawableCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private static sLeftOnlyNotRecommendedDirection:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/here/android/mpa/guidance/LaneInformation$Direction;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static sLeftOnlyRecommendedDirection:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/here/android/mpa/guidance/LaneInformation$Direction;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static sLeftRightNotRecommendedDirection:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/here/android/mpa/guidance/LaneInformation$Direction;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static sLeftRightRecommendedDirection:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/here/android/mpa/guidance/LaneInformation$Direction;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static sRightOnlyNotRecommendedDirection:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/here/android/mpa/guidance/LaneInformation$Direction;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static sRightOnlyRecommendedDirection:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/here/android/mpa/guidance/LaneInformation$Direction;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static sSingleNotRecommendedDirection:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/here/android/mpa/guidance/LaneInformation$Direction;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static sSingleRecommendedDirection:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/here/android/mpa/guidance/LaneInformation$Direction;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 23
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static compareLaneData(Ljava/util/ArrayList;Ljava/util/ArrayList;)Z
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/hud/app/maps/MapEvents$LaneData;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/hud/app/maps/MapEvents$LaneData;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p0, "l1":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/hud/app/maps/MapEvents$LaneData;>;"
    .local p1, "l2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/hud/app/maps/MapEvents$LaneData;>;"
    const/4 v6, 0x0

    .line 419
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 420
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 421
    .local v4, "size1":I
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 423
    .local v5, "size2":I
    if-eq v4, v5, :cond_1

    .line 455
    .end local v4    # "size1":I
    .end local v5    # "size2":I
    :cond_0
    :goto_0
    return v6

    .line 427
    .restart local v4    # "size1":I
    .restart local v5    # "size2":I
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v4, :cond_3

    .line 428
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/hud/app/maps/MapEvents$LaneData;

    .line 429
    .local v2, "laneData1":Lcom/navdy/hud/app/maps/MapEvents$LaneData;
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/navdy/hud/app/maps/MapEvents$LaneData;

    .line 431
    .local v3, "laneData2":Lcom/navdy/hud/app/maps/MapEvents$LaneData;
    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    .line 432
    iget-object v7, v2, Lcom/navdy/hud/app/maps/MapEvents$LaneData;->position:Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;

    iget-object v8, v3, Lcom/navdy/hud/app/maps/MapEvents$LaneData;->position:Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;

    if-ne v7, v8, :cond_0

    .line 436
    iget-object v7, v2, Lcom/navdy/hud/app/maps/MapEvents$LaneData;->icons:[Landroid/graphics/drawable/Drawable;

    if-eqz v7, :cond_0

    iget-object v7, v3, Lcom/navdy/hud/app/maps/MapEvents$LaneData;->icons:[Landroid/graphics/drawable/Drawable;

    if-eqz v7, :cond_0

    .line 437
    iget-object v7, v2, Lcom/navdy/hud/app/maps/MapEvents$LaneData;->icons:[Landroid/graphics/drawable/Drawable;

    array-length v7, v7

    iget-object v8, v3, Lcom/navdy/hud/app/maps/MapEvents$LaneData;->icons:[Landroid/graphics/drawable/Drawable;

    array-length v8, v8

    if-ne v7, v8, :cond_0

    .line 440
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_2
    iget-object v7, v2, Lcom/navdy/hud/app/maps/MapEvents$LaneData;->icons:[Landroid/graphics/drawable/Drawable;

    array-length v7, v7

    if-ge v1, v7, :cond_2

    .line 441
    iget-object v7, v2, Lcom/navdy/hud/app/maps/MapEvents$LaneData;->icons:[Landroid/graphics/drawable/Drawable;

    aget-object v7, v7, v1

    iget-object v8, v3, Lcom/navdy/hud/app/maps/MapEvents$LaneData;->icons:[Landroid/graphics/drawable/Drawable;

    aget-object v8, v8, v1

    if-ne v7, v8, :cond_0

    .line 440
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 427
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 453
    .end local v1    # "j":I
    .end local v2    # "laneData1":Lcom/navdy/hud/app/maps/MapEvents$LaneData;
    .end local v3    # "laneData2":Lcom/navdy/hud/app/maps/MapEvents$LaneData;
    :cond_3
    const/4 v6, 0x1

    goto :goto_0
.end method

.method public static getDirection(Ljava/util/EnumSet;Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;)Lcom/here/android/mpa/guidance/LaneInformation$Direction;
    .locals 4
    .param p1, "type"    # Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/here/android/mpa/guidance/LaneInformation$Direction;",
            ">;",
            "Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;",
            ")",
            "Lcom/here/android/mpa/guidance/LaneInformation$Direction;"
        }
    .end annotation

    .prologue
    .line 484
    .local p0, "directions":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/here/android/mpa/guidance/LaneInformation$Direction;>;"
    invoke-virtual {p0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    .line 485
    .local v0, "d":Lcom/here/android/mpa/guidance/LaneInformation$Direction;
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$1;->$SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction:[I

    invoke-virtual {v0}, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 489
    :pswitch_1
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;->RIGHT:Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;

    if-ne p1, v2, :cond_0

    .line 504
    .end local v0    # "d":Lcom/here/android/mpa/guidance/LaneInformation$Direction;
    :goto_1
    return-object v0

    .line 497
    .restart local v0    # "d":Lcom/here/android/mpa/guidance/LaneInformation$Direction;
    :pswitch_2
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;->LEFT:Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;

    if-ne p1, v2, :cond_0

    goto :goto_1

    .line 504
    .end local v0    # "d":Lcom/here/android/mpa/guidance/LaneInformation$Direction;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 485
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private static getDirectionType(Ljava/util/List;)Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/guidance/LaneInformation$Direction;",
            ">;)",
            "Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;"
        }
    .end annotation

    .prologue
    .line 367
    .local p0, "directions":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/guidance/LaneInformation$Direction;>;"
    const/4 v1, 0x0

    .line 368
    .local v1, "left":I
    const/4 v2, 0x0

    .line 370
    .local v2, "right":I
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    :pswitch_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    .line 371
    .local v0, "d":Lcom/here/android/mpa/guidance/LaneInformation$Direction;
    sget-object v4, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$1;->$SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction:[I

    invoke-virtual {v0}, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 376
    :pswitch_1
    add-int/lit8 v2, v2, 0x1

    .line 377
    goto :goto_0

    .line 380
    :pswitch_2
    add-int/lit8 v2, v2, 0x1

    .line 381
    goto :goto_0

    .line 384
    :pswitch_3
    add-int/lit8 v2, v2, 0x1

    .line 385
    goto :goto_0

    .line 388
    :pswitch_4
    add-int/lit8 v1, v1, 0x1

    .line 389
    goto :goto_0

    .line 392
    :pswitch_5
    add-int/lit8 v1, v1, 0x1

    .line 393
    goto :goto_0

    .line 396
    :pswitch_6
    add-int/lit8 v1, v1, 0x1

    .line 397
    goto :goto_0

    .line 400
    :pswitch_7
    add-int/lit8 v1, v1, 0x1

    .line 401
    goto :goto_0

    .line 404
    :pswitch_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 409
    .end local v0    # "d":Lcom/here/android/mpa/guidance/LaneInformation$Direction;
    :cond_0
    if-lez v1, :cond_1

    if-lez v2, :cond_1

    .line 410
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;->LEFT_RIGHT:Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;

    .line 414
    :goto_1
    return-object v3

    .line 411
    :cond_1
    if-lez v1, :cond_2

    .line 412
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;->LEFT:Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;

    goto :goto_1

    .line 414
    :cond_2
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;->RIGHT:Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;

    goto :goto_1

    .line 371
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static getDrawable(Ljava/util/List;Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;Lcom/here/android/mpa/guidance/LaneInformation$Direction;)[Landroid/graphics/drawable/Drawable;
    .locals 15
    .param p1, "position"    # Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;
    .param p2, "onRouteDirection"    # Lcom/here/android/mpa/guidance/LaneInformation$Direction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/guidance/LaneInformation$Direction;",
            ">;",
            "Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;",
            "Lcom/here/android/mpa/guidance/LaneInformation$Direction;",
            ")[",
            "Landroid/graphics/drawable/Drawable;"
        }
    .end annotation

    .prologue
    .line 275
    .local p0, "directions":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/guidance/LaneInformation$Direction;>;"
    if-nez p0, :cond_1

    .line 276
    const/4 v9, 0x0

    .line 345
    :cond_0
    :goto_0
    return-object v9

    .line 279
    :cond_1
    const/4 v9, 0x0

    .line 281
    .local v9, "ret":[Landroid/graphics/drawable/Drawable;
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v11

    const/4 v12, 0x1

    if-ne v11, v12, :cond_2

    .line 283
    const/4 v11, 0x1

    new-array v9, v11, [Landroid/graphics/drawable/Drawable;

    .end local v9    # "ret":[Landroid/graphics/drawable/Drawable;
    const/4 v12, 0x0

    const/4 v11, 0x0

    invoke-interface {p0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    sget-object v13, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sSingleRecommendedDirection:Ljava/util/HashMap;

    sget-object v14, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sSingleNotRecommendedDirection:Ljava/util/HashMap;

    move-object/from16 v0, p1

    invoke-static {v11, v0, v13, v14}, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->getDrawableForDirection(Lcom/here/android/mpa/guidance/LaneInformation$Direction;Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;Ljava/util/HashMap;Ljava/util/HashMap;)Landroid/graphics/drawable/Drawable;

    move-result-object v11

    aput-object v11, v9, v12

    .line 317
    .restart local v9    # "ret":[Landroid/graphics/drawable/Drawable;
    :goto_1
    if-eqz v9, :cond_0

    .line 318
    const/4 v7, 0x0

    .line 319
    .local v7, "nonNulls":I
    const/4 v8, 0x0

    .line 321
    .local v8, "nulls":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_2
    array-length v11, v9

    if-ge v6, v11, :cond_7

    .line 322
    aget-object v11, v9, v6

    if-nez v11, :cond_6

    .line 323
    add-int/lit8 v8, v8, 0x1

    .line 321
    :goto_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 287
    .end local v6    # "i":I
    .end local v7    # "nonNulls":I
    .end local v8    # "nulls":I
    :cond_2
    invoke-static {p0}, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->getDirectionType(Ljava/util/List;)Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;

    move-result-object v4

    .line 288
    .local v4, "directionType":Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v11

    new-array v5, v11, [Landroid/graphics/drawable/Drawable;

    .line 289
    .local v5, "drawables":[Landroid/graphics/drawable/Drawable;
    const/4 v1, 0x0

    .line 290
    .local v1, "counter":I
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_4
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_5

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    .line 291
    .local v3, "d":Lcom/here/android/mpa/guidance/LaneInformation$Direction;
    move-object/from16 v10, p1

    .line 292
    .local v10, "tempPosition":Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;
    sget-object v12, Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;->ON_ROUTE:Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;

    move-object/from16 v0, p1

    if-ne v0, v12, :cond_3

    if-eqz p2, :cond_3

    .line 293
    move-object/from16 v0, p2

    if-ne v3, v0, :cond_4

    .line 294
    sget-object v10, Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;->ON_ROUTE:Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;

    .line 299
    :cond_3
    :goto_5
    sget-object v12, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$1;->$SwitchMap$com$navdy$hud$app$maps$here$HereLaneInfoBuilder$DirectionType:[I

    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;->ordinal()I

    move-result v13

    aget v12, v12, v13

    packed-switch v12, :pswitch_data_0

    goto :goto_4

    .line 301
    :pswitch_0
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "counter":I
    .local v2, "counter":I
    sget-object v12, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftOnlyRecommendedDirection:Ljava/util/HashMap;

    sget-object v13, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftOnlyNotRecommendedDirection:Ljava/util/HashMap;

    invoke-static {v3, v10, v12, v13}, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->getDrawableForDirection(Lcom/here/android/mpa/guidance/LaneInformation$Direction;Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;Ljava/util/HashMap;Ljava/util/HashMap;)Landroid/graphics/drawable/Drawable;

    move-result-object v12

    aput-object v12, v5, v1

    move v1, v2

    .line 302
    .end local v2    # "counter":I
    .restart local v1    # "counter":I
    goto :goto_4

    .line 296
    :cond_4
    sget-object v10, Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;->OFF_ROUTE:Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;

    goto :goto_5

    .line 305
    :pswitch_1
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "counter":I
    .restart local v2    # "counter":I
    sget-object v12, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sRightOnlyRecommendedDirection:Ljava/util/HashMap;

    sget-object v13, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sRightOnlyNotRecommendedDirection:Ljava/util/HashMap;

    invoke-static {v3, v10, v12, v13}, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->getDrawableForDirection(Lcom/here/android/mpa/guidance/LaneInformation$Direction;Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;Ljava/util/HashMap;Ljava/util/HashMap;)Landroid/graphics/drawable/Drawable;

    move-result-object v12

    aput-object v12, v5, v1

    move v1, v2

    .line 306
    .end local v2    # "counter":I
    .restart local v1    # "counter":I
    goto :goto_4

    .line 309
    :pswitch_2
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "counter":I
    .restart local v2    # "counter":I
    sget-object v12, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftRightRecommendedDirection:Ljava/util/HashMap;

    sget-object v13, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftRightNotRecommendedDirection:Ljava/util/HashMap;

    invoke-static {v3, v10, v12, v13}, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->getDrawableForDirection(Lcom/here/android/mpa/guidance/LaneInformation$Direction;Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;Ljava/util/HashMap;Ljava/util/HashMap;)Landroid/graphics/drawable/Drawable;

    move-result-object v12

    aput-object v12, v5, v1

    move v1, v2

    .end local v2    # "counter":I
    .restart local v1    # "counter":I
    goto :goto_4

    .line 313
    .end local v3    # "d":Lcom/here/android/mpa/guidance/LaneInformation$Direction;
    .end local v10    # "tempPosition":Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;
    :cond_5
    move-object v9, v5

    goto :goto_1

    .line 325
    .end local v1    # "counter":I
    .end local v4    # "directionType":Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;
    .end local v5    # "drawables":[Landroid/graphics/drawable/Drawable;
    .restart local v6    # "i":I
    .restart local v7    # "nonNulls":I
    .restart local v8    # "nulls":I
    :cond_6
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 329
    :cond_7
    if-nez v7, :cond_8

    .line 330
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 333
    :cond_8
    if-lez v8, :cond_0

    .line 334
    new-array v5, v7, [Landroid/graphics/drawable/Drawable;

    .line 335
    .restart local v5    # "drawables":[Landroid/graphics/drawable/Drawable;
    const/4 v1, 0x0

    .line 336
    .restart local v1    # "counter":I
    const/4 v6, 0x0

    :goto_6
    array-length v11, v9

    if-ge v6, v11, :cond_a

    .line 337
    aget-object v11, v9, v6

    if-eqz v11, :cond_9

    .line 338
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "counter":I
    .restart local v2    # "counter":I
    aget-object v11, v9, v6

    aput-object v11, v5, v1

    move v1, v2

    .line 336
    .end local v2    # "counter":I
    .restart local v1    # "counter":I
    :cond_9
    add-int/lit8 v6, v6, 0x1

    goto :goto_6

    .line 341
    :cond_a
    move-object v9, v5

    goto/16 :goto_0

    .line 299
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static getDrawableForDirection(Lcom/here/android/mpa/guidance/LaneInformation$Direction;Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;Ljava/util/HashMap;Ljava/util/HashMap;)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p0, "direction"    # Lcom/here/android/mpa/guidance/LaneInformation$Direction;
    .param p1, "position"    # Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/here/android/mpa/guidance/LaneInformation$Direction;",
            "Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;",
            "Ljava/util/HashMap",
            "<",
            "Lcom/here/android/mpa/guidance/LaneInformation$Direction;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Lcom/here/android/mpa/guidance/LaneInformation$Direction;",
            "Ljava/lang/Integer;",
            ">;)",
            "Landroid/graphics/drawable/Drawable;"
        }
    .end annotation

    .prologue
    .line 353
    .local p2, "recommendedMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/here/android/mpa/guidance/LaneInformation$Direction;Ljava/lang/Integer;>;"
    .local p3, "notRecommendedMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/here/android/mpa/guidance/LaneInformation$Direction;Ljava/lang/Integer;>;"
    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;->ON_ROUTE:Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;

    if-ne p1, v1, :cond_0

    .line 354
    invoke-virtual {p2, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 359
    .local v0, "icon":Ljava/lang/Integer;
    :goto_0
    if-nez v0, :cond_1

    .line 360
    const/4 v1, 0x0

    .line 362
    :goto_1
    return-object v1

    .line 356
    .end local v0    # "icon":Ljava/lang/Integer;
    :cond_0
    invoke-virtual {p3, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "icon":Ljava/lang/Integer;
    goto :goto_0

    .line 362
    :cond_1
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/Drawable;

    goto :goto_1
.end method

.method public static getNumDirections(Ljava/util/EnumSet;Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;)I
    .locals 5
    .param p1, "type"    # Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/here/android/mpa/guidance/LaneInformation$Direction;",
            ">;",
            "Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;",
            ")I"
        }
    .end annotation

    .prologue
    .line 460
    .local p0, "directions":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/here/android/mpa/guidance/LaneInformation$Direction;>;"
    const/4 v1, 0x0

    .line 461
    .local v1, "n":I
    invoke-virtual {p0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    .line 462
    .local v0, "d":Lcom/here/android/mpa/guidance/LaneInformation$Direction;
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$1;->$SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction:[I

    invoke-virtual {v0}, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 466
    :pswitch_1
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;->RIGHT:Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;

    if-ne p1, v3, :cond_0

    .line 467
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 474
    :pswitch_2
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;->LEFT:Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder$DirectionType;

    if-ne p1, v3, :cond_0

    .line 475
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 480
    .end local v0    # "d":Lcom/here/android/mpa/guidance/LaneInformation$Direction;
    :cond_1
    return v1

    .line 462
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public static declared-synchronized init()V
    .locals 5

    .prologue
    .line 55
    const-class v2, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;

    monitor-enter v2

    :try_start_0
    sget-boolean v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->initialized:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 270
    .local v0, "resources":Landroid/content/res/Resources;
    :goto_0
    monitor-exit v2

    return-void

    .line 59
    .end local v0    # "resources":Landroid/content/res/Resources;
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 60
    const/4 v1, 0x1

    sput-boolean v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->initialized:Z

    .line 61
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 64
    .restart local v0    # "resources":Landroid/content/res/Resources;
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    .line 71
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sSingleRecommendedDirection:Ljava/util/HashMap;

    .line 73
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sSingleRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->STRAIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f02015f

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sSingleRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SLIGHTLY_RIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f02015e

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sSingleRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->RIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f02015c

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sSingleRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SHARP_RIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f02015a

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sSingleRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->U_TURN_RIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020161

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sSingleRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SECOND_RIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020121

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sSingleRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->MERGE_RIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020121

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sSingleRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->U_TURN_LEFT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020160

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sSingleRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SHARP_LEFT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020159

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sSingleRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->LEFT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f02015b

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sSingleRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SLIGHTLY_LEFT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f02015d

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sSingleRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SECOND_LEFT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020121

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sSingleRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->MERGE_LEFT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020121

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sSingleRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->MERGE_LANES:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020121

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f02015f

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f02015f

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f02015e

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f02015e

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f02015c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f02015c

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f02015a

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f02015a

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020161

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020161

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020160

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020160

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020159

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020159

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f02015b

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f02015b

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f02015d

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f02015d

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020121

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020121

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sSingleNotRecommendedDirection:Ljava/util/HashMap;

    .line 106
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sSingleNotRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->STRAIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020165

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sSingleNotRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SLIGHTLY_RIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020164

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sSingleNotRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->RIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020162

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sSingleNotRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SHARP_RIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020157

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sSingleNotRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->U_TURN_RIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020167

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sSingleNotRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SECOND_RIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020121

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sSingleNotRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->MERGE_RIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020121

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sSingleNotRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->U_TURN_LEFT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020166

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sSingleNotRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SHARP_LEFT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020156

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sSingleNotRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->LEFT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020158

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sSingleNotRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SLIGHTLY_LEFT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020163

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sSingleNotRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SECOND_LEFT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020121

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sSingleNotRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->MERGE_LEFT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020121

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sSingleNotRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->MERGE_LANES:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020121

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020165

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020165

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020164

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020164

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020162

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020162

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020157

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020157

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020167

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020167

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020166

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020166

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020156

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020156

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020158

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020158

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020163

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020163

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020121

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020121

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sRightOnlyRecommendedDirection:Ljava/util/HashMap;

    .line 139
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sRightOnlyRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->STRAIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020150

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sRightOnlyRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SLIGHTLY_RIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f02014f

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sRightOnlyRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->RIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f02014e

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sRightOnlyRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SHARP_RIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f02014d

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sRightOnlyRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->U_TURN_RIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020151

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sRightOnlyRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SECOND_RIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020121

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sRightOnlyRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->MERGE_RIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020121

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020150

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020150

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f02014f

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f02014f

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f02014e

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f02014e

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f02014d

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f02014d

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020151

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020151

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020121

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020121

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sRightOnlyNotRecommendedDirection:Ljava/util/HashMap;

    .line 161
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sRightOnlyNotRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->STRAIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020154

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sRightOnlyNotRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SLIGHTLY_RIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020153

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sRightOnlyNotRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->RIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020152

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sRightOnlyNotRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SHARP_RIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f02014c

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 165
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sRightOnlyNotRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->U_TURN_RIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020155

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sRightOnlyNotRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SECOND_RIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020121

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sRightOnlyNotRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->MERGE_RIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020121

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020154

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020154

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020153

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020153

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020152

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020152

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f02014c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f02014c

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020155

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020155

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020121

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020121

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftOnlyRecommendedDirection:Ljava/util/HashMap;

    .line 183
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftOnlyRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->STRAIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020147

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftOnlyRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SLIGHTLY_LEFT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020146

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftOnlyRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->LEFT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020145

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftOnlyRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SHARP_LEFT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020144

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftOnlyRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->U_TURN_LEFT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020148

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftOnlyRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SECOND_LEFT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020121

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 189
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftOnlyRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->MERGE_LEFT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020121

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020147

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020147

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020146

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020146

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020145

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020145

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020144

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020144

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020148

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020148

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020121

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020121

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftOnlyNotRecommendedDirection:Ljava/util/HashMap;

    .line 205
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftOnlyNotRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->STRAIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f02014a

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftOnlyNotRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SLIGHTLY_LEFT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020149

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftOnlyNotRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->LEFT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020143

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftOnlyNotRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SHARP_LEFT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020142

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftOnlyNotRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->U_TURN_LEFT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f02014b

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftOnlyNotRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SECOND_LEFT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020121

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftOnlyNotRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->MERGE_LEFT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020121

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f02014a

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f02014a

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020149

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020149

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020143

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020143

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 216
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020142

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020142

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f02014b

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f02014b

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020121

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020121

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftRightRecommendedDirection:Ljava/util/HashMap;

    .line 226
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftRightRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->STRAIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f02013d

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftRightRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SLIGHTLY_RIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f02013c

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftRightRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->RIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f02013a

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftRightRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SHARP_RIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020138

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftRightRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SECOND_RIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020121

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftRightRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->MERGE_RIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020121

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftRightRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SLIGHTLY_LEFT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f02013b

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftRightRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->LEFT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020139

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 234
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftRightRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SHARP_LEFT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020137

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 235
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftRightRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->MERGE_LEFT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020121

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 237
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f02013d

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f02013d

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f02013c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f02013c

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f02013a

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f02013a

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020138

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020138

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f02013b

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f02013b

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020139

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020139

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020137

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020137

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020121

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020121

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftRightNotRecommendedDirection:Ljava/util/HashMap;

    .line 252
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftRightNotRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->STRAIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020141

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 253
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftRightNotRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SLIGHTLY_RIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020140

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftRightNotRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->RIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f02013e

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 255
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftRightNotRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SHARP_RIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020135

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 256
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftRightNotRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->MERGE_RIGHT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020121

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftRightNotRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SLIGHTLY_LEFT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f02013f

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 258
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftRightNotRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->LEFT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020136

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftRightNotRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->SHARP_LEFT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020134

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 260
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLeftRightNotRecommendedDirection:Ljava/util/HashMap;

    sget-object v3, Lcom/here/android/mpa/guidance/LaneInformation$Direction;->MERGE_LEFT:Lcom/here/android/mpa/guidance/LaneInformation$Direction;

    const v4, 0x7f020121

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 262
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020141

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020141

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 263
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020140

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020140

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 264
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f02013e

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f02013e

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020135

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020135

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 266
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f02013f

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f02013f

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 267
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020136

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020136

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020134

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020134

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereLaneInfoBuilder;->sLaneDrawableCache:Ljava/util/HashMap;

    const v3, 0x7f020121

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f020121

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 55
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method
