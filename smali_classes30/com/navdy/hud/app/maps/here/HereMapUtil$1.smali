.class final Lcom/navdy/hud/app/maps/here/HereMapUtil$1;
.super Ljava/lang/Object;
.source "HereMapUtil.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereMapUtil;->loadImage(Lcom/here/android/mpa/common/Image;IILcom/navdy/hud/app/maps/here/HereMapUtil$IImageLoadCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$callback:Lcom/navdy/hud/app/maps/here/HereMapUtil$IImageLoadCallback;

.field final synthetic val$height:I

.field final synthetic val$image:Lcom/here/android/mpa/common/Image;

.field final synthetic val$width:I


# direct methods
.method constructor <init>(Lcom/here/android/mpa/common/Image;IILcom/navdy/hud/app/maps/here/HereMapUtil$IImageLoadCallback;)V
    .locals 0

    .prologue
    .line 1271
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapUtil$1;->val$image:Lcom/here/android/mpa/common/Image;

    iput p2, p0, Lcom/navdy/hud/app/maps/here/HereMapUtil$1;->val$width:I

    iput p3, p0, Lcom/navdy/hud/app/maps/here/HereMapUtil$1;->val$height:I

    iput-object p4, p0, Lcom/navdy/hud/app/maps/here/HereMapUtil$1;->val$callback:Lcom/navdy/hud/app/maps/here/HereMapUtil$IImageLoadCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 1275
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapUtil$1;->val$image:Lcom/here/android/mpa/common/Image;

    iget v3, p0, Lcom/navdy/hud/app/maps/here/HereMapUtil$1;->val$width:I

    iget v4, p0, Lcom/navdy/hud/app/maps/here/HereMapUtil$1;->val$height:I

    invoke-virtual {v2, v3, v4}, Lcom/here/android/mpa/common/Image;->getBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1276
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapUtil$1;->val$callback:Lcom/navdy/hud/app/maps/here/HereMapUtil$IImageLoadCallback;

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereMapUtil$1;->val$image:Lcom/here/android/mpa/common/Image;

    invoke-interface {v2, v3, v0}, Lcom/navdy/hud/app/maps/here/HereMapUtil$IImageLoadCallback;->result(Lcom/here/android/mpa/common/Image;Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1281
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-void

    .line 1277
    :catch_0
    move-exception v1

    .line 1278
    .local v1, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapUtil;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 1279
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapUtil$1;->val$callback:Lcom/navdy/hud/app/maps/here/HereMapUtil$IImageLoadCallback;

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereMapUtil$1;->val$image:Lcom/here/android/mpa/common/Image;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Lcom/navdy/hud/app/maps/here/HereMapUtil$IImageLoadCallback;->result(Lcom/here/android/mpa/common/Image;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method
