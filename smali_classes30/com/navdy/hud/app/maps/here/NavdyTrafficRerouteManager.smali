.class public Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;
.super Ljava/lang/Object;
.source "NavdyTrafficRerouteManager.java"


# static fields
.field private static final KILL_ROUTE_CALC:I = 0xafc8

.field private static final STALE_PERIOD:I = 0x493e0

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private final bus:Lcom/squareup/otto/Bus;

.field private fasterRouteCheck:Ljava/lang/Runnable;

.field private fasterRouteTrigger:Ljava/lang/Runnable;

.field private handler:Landroid/os/Handler;

.field private final hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

.field private final hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

.field private killRouteCalc:Ljava/lang/Runnable;

.field private routeCalculator:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

.field private volatile running:Z

.field private staleRoute:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 34
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-string v1, "NavdyTrafficRerout"

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereNavigationManager;Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;Lcom/squareup/otto/Bus;)V
    .locals 3
    .param p1, "hereNavigationManager"    # Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    .param p2, "hereTrafficRerouteListener"    # Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;
    .param p3, "bus"    # Lcom/squareup/otto/Bus;

    .prologue
    .line 365
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    sget-object v1, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;-><init>(Lcom/navdy/service/library/log/Logger;Z)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->routeCalculator:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    .line 47
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->handler:Landroid/os/Handler;

    .line 49
    new-instance v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$1;-><init>(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->staleRoute:Ljava/lang/Runnable;

    .line 60
    new-instance v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$2;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$2;-><init>(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->killRouteCalc:Ljava/lang/Runnable;

    .line 72
    new-instance v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$3;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$3;-><init>(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->fasterRouteTrigger:Ljava/lang/Runnable;

    .line 79
    new-instance v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$4;-><init>(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->fasterRouteCheck:Ljava/lang/Runnable;

    .line 366
    sget-object v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "NavdyTrafficRerouteManager:ctor"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 367
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    .line 368
    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    .line 369
    iput-object p3, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->bus:Lcom/squareup/otto/Bus;

    .line 370
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    return-object v0
.end method

.method static synthetic access$100()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereRouteCalculator;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->routeCalculator:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->fasterRouteCheck:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->killRouteCalc:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->cleanupRouteIfStale()V

    return-void
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    return-object v0
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->running:Z

    return v0
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->fasterRouteTrigger:Ljava/lang/Runnable;

    return-object v0
.end method

.method private cleanupRouteIfStale()V
    .locals 8

    .prologue
    .line 405
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->getCurrentProposedRoute()Lcom/here/android/mpa/routing/Route;

    move-result-object v0

    .line 406
    .local v0, "route":Lcom/here/android/mpa/routing/Route;
    if-eqz v0, :cond_1

    .line 407
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->getCurrentProposedRouteTime()J

    move-result-wide v6

    sub-long v2, v4, v6

    .line 408
    .local v2, "time":J
    const-wide/32 v4, 0x493e0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 409
    sget-object v1, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "clear stale route:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 410
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->dismissReroute()V

    .line 417
    .end local v2    # "time":J
    :goto_0
    return-void

    .line 412
    .restart local v2    # "time":J
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "route not stale:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 415
    .end local v2    # "time":J
    :cond_1
    sget-object v1, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "no proposed route"

    invoke-virtual {v1, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public onBandwidthSettingChanged(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$UserBandwidthSettingChanged;)V
    .locals 5
    .param p1, "event"    # Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$UserBandwidthSettingChanged;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 396
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->fasterRouteTrigger:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 397
    iget-boolean v2, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->running:Z

    if-eqz v2, :cond_0

    .line 398
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getRerouteInterval()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-long v0, v2

    .line 399
    .local v0, "interval":J
    sget-object v2, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "faster route time changed to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 400
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->fasterRouteTrigger:Ljava/lang/Runnable;

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 402
    .end local v0    # "interval":J
    :cond_0
    return-void
.end method

.method reCalculateCurrentRoute(Lcom/here/android/mpa/routing/Route;Lcom/here/android/mpa/routing/Route;ZLjava/lang/String;JLjava/lang/String;JJLjava/lang/String;Lcom/here/android/mpa/routing/RouteTta;)V
    .locals 35
    .param p1, "currentRoute"    # Lcom/here/android/mpa/routing/Route;
    .param p2, "fasterRoute"    # Lcom/here/android/mpa/routing/Route;
    .param p3, "notifFromHereTraffic"    # Z
    .param p4, "via"    # Ljava/lang/String;
    .param p5, "diff"    # J
    .param p7, "additionalVia"    # Ljava/lang/String;
    .param p8, "etaUtc"    # J
    .param p10, "distanceDiff"    # J
    .param p12, "currentVia"    # Ljava/lang/String;
    .param p13, "fasterRouteTta"    # Lcom/here/android/mpa/routing/RouteTta;

    .prologue
    .line 445
    :try_start_0
    sget-object v2, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "reCalculateCurrentRoute:"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 447
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->handler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->killRouteCalc:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 449
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 450
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->cleanupRouteIfStale()V

    .line 451
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    move/from16 v3, p3

    move-wide/from16 v4, p5

    move-object/from16 v6, p4

    move-object/from16 v7, p12

    invoke-virtual/range {v2 .. v7}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->fasterRouteNotFound(ZJLjava/lang/String;Ljava/lang/String;)V

    .line 452
    sget-object v2, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "reCalculateCurrentRoute:skip no n/w"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 650
    :goto_0
    return-void

    .line 456
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isNavigationModeOn()Z

    move-result v2

    if-nez v2, :cond_1

    .line 457
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->dismissReroute()V

    .line 458
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    move/from16 v3, p3

    move-wide/from16 v4, p5

    move-object/from16 v6, p4

    move-object/from16 v7, p12

    invoke-virtual/range {v2 .. v7}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->fasterRouteNotFound(ZJLjava/lang/String;Ljava/lang/String;)V

    .line 459
    sget-object v2, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "reCalculateCurrentRoute: not navigating"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 644
    :catch_0
    move-exception v29

    .line 645
    .local v29, "t":Ljava/lang/Throwable;
    sget-object v2, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 646
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->cleanupRouteIfStale()V

    .line 647
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    move/from16 v3, p3

    move-wide/from16 v4, p5

    move-object/from16 v6, p4

    move-object/from16 v7, p12

    invoke-virtual/range {v2 .. v7}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->fasterRouteNotFound(ZJLjava/lang/String;Ljava/lang/String;)V

    .line 648
    invoke-static {}, Lcom/navdy/hud/app/util/CrashReporter;->getInstance()Lcom/navdy/hud/app/util/CrashReporter;

    move-result-object v2

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Lcom/navdy/hud/app/util/CrashReporter;->reportNonFatalException(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 464
    .end local v29    # "t":Ljava/lang/Throwable;
    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isRerouting()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 465
    sget-object v2, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "reCalculateCurrentRoute: rerouting"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 466
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->dismissReroute()V

    .line 467
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    move/from16 v3, p3

    move-wide/from16 v4, p5

    move-object/from16 v6, p4

    move-object/from16 v7, p12

    invoke-virtual/range {v2 .. v7}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->fasterRouteNotFound(ZJLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 471
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hasArrived()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 472
    sget-object v2, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "reCalculateCurrentRoute: arrival mode on"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 473
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->dismissReroute()V

    .line 474
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    move/from16 v3, p3

    move-wide/from16 v4, p5

    move-object/from16 v6, p4

    move-object/from16 v7, p12

    invoke-virtual/range {v2 .. v7}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->fasterRouteNotFound(ZJLjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 478
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isOnGasRoute()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 479
    sget-object v2, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "reCalculateCurrentRoute: on gas route"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 480
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->dismissReroute()V

    .line 481
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    move/from16 v3, p3

    move-wide/from16 v4, p5

    move-object/from16 v6, p4

    move-object/from16 v7, p12

    invoke-virtual/range {v2 .. v7}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->fasterRouteNotFound(ZJLjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 485
    :cond_4
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v20

    .line 486
    .local v20, "hereMapsManager":Lcom/navdy/hud/app/maps/here/HereMapsManager;
    invoke-virtual/range {v20 .. v20}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v19

    .line 487
    .local v19, "hereLocationFixManager":Lcom/navdy/hud/app/maps/here/HereLocationFixManager;
    invoke-virtual/range {v19 .. v19}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v28

    .line 488
    .local v28, "startPoint":Lcom/here/android/mpa/common/GeoCoordinate;
    if-nez v28, :cond_5

    .line 489
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->cleanupRouteIfStale()V

    .line 490
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    move/from16 v3, p3

    move-wide/from16 v4, p5

    move-object/from16 v6, p4

    move-object/from16 v7, p12

    invoke-virtual/range {v2 .. v7}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->fasterRouteNotFound(ZJLjava/lang/String;Ljava/lang/String;)V

    .line 491
    sget-object v2, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "reCalculateCurrentRoute: no start point"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 495
    :cond_5
    invoke-virtual/range {p1 .. p1}, Lcom/here/android/mpa/routing/Route;->getDestination()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v17

    .line 496
    .local v17, "endPoint":Lcom/here/android/mpa/common/GeoCoordinate;
    if-nez v17, :cond_6

    .line 497
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->cleanupRouteIfStale()V

    .line 498
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    move/from16 v3, p3

    move-wide/from16 v4, p5

    move-object/from16 v6, p4

    move-object/from16 v7, p12

    invoke-virtual/range {v2 .. v7}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->fasterRouteNotFound(ZJLjava/lang/String;Ljava/lang/String;)V

    .line 499
    sget-object v2, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "reCalculateCurrentRoute: no end point"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 503
    :cond_6
    invoke-virtual/range {v20 .. v20}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getRouteOptions()Lcom/here/android/mpa/routing/RouteOptions;

    move-result-object v27

    .line 504
    .local v27, "routeOptions":Lcom/here/android/mpa/routing/RouteOptions;
    sget-object v2, Lcom/here/android/mpa/routing/RouteOptions$Type;->FASTEST:Lcom/here/android/mpa/routing/RouteOptions$Type;

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Lcom/here/android/mpa/routing/RouteOptions;->setRouteType(Lcom/here/android/mpa/routing/RouteOptions$Type;)Lcom/here/android/mpa/routing/RouteOptions;

    .line 506
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getNavController()Lcom/navdy/hud/app/maps/here/HereNavController;

    move-result-object v25

    .line 507
    .local v25, "navController":Lcom/navdy/hud/app/maps/here/HereNavController;
    invoke-virtual/range {v25 .. v25}, Lcom/navdy/hud/app/maps/here/HereNavController;->getNextManeuver()Lcom/here/android/mpa/routing/Maneuver;

    move-result-object v16

    .line 509
    .local v16, "currentManeuver":Lcom/here/android/mpa/routing/Maneuver;
    if-nez v16, :cond_7

    .line 510
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->cleanupRouteIfStale()V

    .line 511
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    move/from16 v3, p3

    move-wide/from16 v4, p5

    move-object/from16 v6, p4

    move-object/from16 v7, p12

    invoke-virtual/range {v2 .. v7}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->fasterRouteNotFound(ZJLjava/lang/String;Ljava/lang/String;)V

    .line 512
    sget-object v2, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "reCalculateCurrentRoute: no current maneuver"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 516
    :cond_7
    const/16 v26, -0x1

    .line 517
    .local v26, "offset":I
    invoke-virtual/range {p1 .. p1}, Lcom/here/android/mpa/routing/Route;->getManeuvers()Ljava/util/List;

    move-result-object v24

    .line 518
    .local v24, "maneuvers":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    const/16 v22, 0x0

    .line 519
    .local v22, "len":I
    if-eqz v24, :cond_8

    .line 520
    invoke-interface/range {v24 .. v24}, Ljava/util/List;->size()I

    move-result v22

    .line 521
    const/16 v21, 0x0

    .local v21, "i":I
    :goto_1
    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_8

    .line 522
    move-object/from16 v0, v24

    move/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/here/android/mpa/routing/Maneuver;

    move-object/from16 v0, v16

    invoke-static {v2, v0}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->areManeuverEqual(Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 524
    move/from16 v26, v21

    .line 530
    .end local v21    # "i":I
    :cond_8
    const/4 v2, -0x1

    move/from16 v0, v26

    if-ne v0, v2, :cond_a

    .line 531
    sget-object v2, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "reCalculateCurrentRoute: cannot find current maneuver in route"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 532
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->cleanupRouteIfStale()V

    .line 533
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    move/from16 v3, p3

    move-wide/from16 v4, p5

    move-object/from16 v6, p4

    move-object/from16 v7, p12

    invoke-virtual/range {v2 .. v7}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->fasterRouteNotFound(ZJLjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 521
    .restart local v21    # "i":I
    :cond_9
    add-int/lit8 v21, v21, 0x1

    goto :goto_1

    .line 537
    .end local v21    # "i":I
    :cond_a
    new-instance v30, Ljava/util/ArrayList;

    invoke-direct/range {v30 .. v30}, Ljava/util/ArrayList;-><init>()V

    .line 538
    .local v30, "waypoints":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/common/GeoCoordinate;>;"
    move/from16 v21, v26

    .restart local v21    # "i":I
    :goto_2
    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_c

    .line 539
    move-object/from16 v0, v24

    move/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lcom/here/android/mpa/routing/Maneuver;

    .line 540
    .local v23, "m":Lcom/here/android/mpa/routing/Maneuver;
    invoke-virtual/range {v23 .. v23}, Lcom/here/android/mpa/routing/Maneuver;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v18

    .line 541
    .local v18, "geoCoordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    if-eqz v18, :cond_b

    .line 542
    sget-object v2, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "reCalculateCurrentRoute: added waypoint:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 543
    move-object/from16 v0, v30

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 538
    :cond_b
    add-int/lit8 v21, v21, 0x1

    goto :goto_2

    .line 547
    .end local v18    # "geoCoordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v23    # "m":Lcom/here/android/mpa/routing/Maneuver;
    :cond_c
    sget-object v2, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "reCalculateCurrentRoute: waypoints:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface/range {v30 .. v30}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 549
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->routeCalculator:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->cancel()V

    .line 550
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->handler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->killRouteCalc:Ljava/lang/Runnable;

    const-wide/32 v4, 0xafc8

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 551
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->routeCalculator:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    move-object/from16 v31, v0

    const/16 v32, 0x0

    const/16 v33, 0x1

    new-instance v2, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;

    move-object/from16 v3, p0

    move/from16 v4, p3

    move-wide/from16 v5, p5

    move-object/from16 v7, p4

    move-object/from16 v8, p12

    move-object/from16 v9, p13

    move-wide/from16 v10, p8

    move-object/from16 v12, p2

    move-object/from16 v13, p7

    move-wide/from16 v14, p10

    invoke-direct/range {v2 .. v15}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager$5;-><init>(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;ZJLjava/lang/String;Ljava/lang/String;Lcom/here/android/mpa/routing/RouteTta;JLcom/here/android/mpa/routing/Route;Ljava/lang/String;J)V

    const/4 v10, 0x1

    const/4 v12, 0x1

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v3, v31

    move-object/from16 v4, v32

    move-object/from16 v5, v28

    move-object/from16 v6, v30

    move-object/from16 v7, v17

    move/from16 v8, v33

    move-object v9, v2

    move-object/from16 v11, v27

    invoke-virtual/range {v3 .. v14}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->calculateRoute(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/common/GeoCoordinate;Ljava/util/List;Lcom/here/android/mpa/common/GeoCoordinate;ZLcom/navdy/hud/app/maps/here/HereRouteCalculator$RouteCalculatorListener;ILcom/here/android/mpa/routing/RouteOptions;ZZZ)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public reset()V
    .locals 6

    .prologue
    .line 421
    :try_start_0
    sget-object v1, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "reset::"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 422
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->routeCalculator:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->cancel()V

    .line 423
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->hereTrafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->dismissReroute()V

    .line 424
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->fasterRouteTrigger:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 425
    iget-boolean v1, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->running:Z

    if-eqz v1, :cond_0

    .line 426
    sget-object v1, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "reset::timer reset"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 427
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->fasterRouteTrigger:Ljava/lang/Runnable;

    sget v3, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->ROUTE_CHECK_INITIAL_INTERVAL:I

    int-to-long v4, v3

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 432
    :cond_0
    :goto_0
    return-void

    .line 429
    :catch_0
    move-exception v0

    .line 430
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public start()V
    .locals 4

    .prologue
    .line 373
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->running:Z

    if-nez v0, :cond_0

    .line 374
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->running:Z

    .line 375
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->staleRoute:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 376
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->fasterRouteTrigger:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 377
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->fasterRouteTrigger:Ljava/lang/Runnable;

    sget v2, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->ROUTE_CHECK_INITIAL_INTERVAL:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 378
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 379
    sget-object v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "running"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 381
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 4

    .prologue
    .line 384
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->running:Z

    if-eqz v0, :cond_0

    .line 385
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->running:Z

    .line 386
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->fasterRouteTrigger:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 387
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->staleRoute:Ljava/lang/Runnable;

    const-wide/32 v2, 0x493e0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 388
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->unregister(Ljava/lang/Object;)V

    .line 389
    sget-object v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "stopped"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 391
    :cond_0
    return-void
.end method
