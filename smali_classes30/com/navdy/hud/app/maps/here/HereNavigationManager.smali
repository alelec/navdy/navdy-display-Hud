.class public Lcom/navdy/hud/app/maps/here/HereNavigationManager;
.super Ljava/lang/Object;
.source "HereNavigationManager.java"


# static fields
.field private static final ARRIVAL_EVENT:Lcom/navdy/hud/app/maps/MapEvents$ArrivalEvent;

.field private static final ARRIVAL_GEO_FENCE_THRESHOLD:I = 0x324

.field static final EMPTY_STR:Ljava/lang/String; = ""

.field private static final ETA_CALCULATION_INTERVAL:I = 0x7530

.field private static final ETA_UPDATE_THRESHOLD:I = 0xea60

.field private static LANGUAGE_HIERARCHY:Ljava/util/Map; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field static final SPEED_EXCEEDED:Lcom/navdy/hud/app/maps/MapEvents$SpeedWarning;

.field static final SPEED_NORMAL:Lcom/navdy/hud/app/maps/MapEvents$SpeedWarning;

.field private static final TAG_AUDIO_CALLBACK:Ljava/lang/String; = "[CB-AUDIO]"

.field private static final TAG_GPS_CALLBACK:Ljava/lang/String; = "[CB-GPS]"

.field private static final TAG_NAV_MANAGER_CALLBACK:Ljava/lang/String; = "[CB-NAVM]"

.field private static final TAG_NEW_MANEUVER_CALLBACK:Ljava/lang/String; = "[CB-NEWM]"

.field private static final TAG_POS_UPDATE_CALLBACK:Ljava/lang/String; = "[CB-POS-UPDATE]"

.field private static final TAG_REROUTE_CALLBACK:Ljava/lang/String; = "[CB-REROUTE]"

.field private static final TAG_SAFETY_CALLBACK:Ljava/lang/String; = "[CB-SAFE]"

.field private static final TAG_SHOWLANE_INFO_CALLBACK:Ljava/lang/String; = "[CB-LANEINFO]"

.field private static final TAG_SPEED_CALLBACK:Ljava/lang/String; = "[CB-SPD]"

.field private static final TAG_TRAFFIC_ETA_TRACKER:Ljava/lang/String; = "[CB-TRAFFIC-ETA]"

.field private static final TAG_TRAFFIC_REROUTE_CALLBACK:Ljava/lang/String; = "[CB-TRAFFIC-REROUTE]"

.field private static final TAG_TRAFFIC_UPDATE:Ljava/lang/String; = "[CB-TRAFFIC-UPDATE]"

.field private static final TAG_TRAFFIC_WARN:Ljava/lang/String; = "[CB-TRAFFIC-WARN]"

.field private static final TAG_TTS:Ljava/lang/String; = "[CB-TTS]"

.field private static final TBT_LANGUAGE_CODE:Ljava/lang/String; = "en-US"

.field private static final VERBOSE:Z

.field private static context:Landroid/content/Context;

.field private static final hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

.field private static final mapsEventHandler:Lcom/navdy/hud/app/maps/MapsEventHandler;

.field static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static final sSingleton:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

.field private static final speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

.field private static final trafficRerouteLock:Ljava/lang/Object;


# instance fields
.field final BUS_GPS_SIGNAL_LOST:Lcom/navdy/hud/app/maps/MapEvents$GpsStatusChange;

.field final BUS_GPS_SIGNAL_RESTORED:Lcom/navdy/hud/app/maps/MapEvents$GpsStatusChange;

.field private final HERE_TBT_AUDIO_DESTINATION_REACHED_PATTERN:Ljava/lang/String;

.field private INVALID_ROUTE_ID:Ljava/lang/String;

.field private INVALID_STATE:Ljava/lang/String;

.field private NO_CURRENT_POSITION:Ljava/lang/String;

.field TTS_KMS:Ljava/lang/String;

.field TTS_METERS:Ljava/lang/String;

.field TTS_MILES:Ljava/lang/String;

.field TTS_REROUTING:Lcom/navdy/hud/app/event/LocalSpeechRequest;

.field TTS_REROUTING_FAILED:Lcom/navdy/hud/app/event/LocalSpeechRequest;

.field private audioPlayerDelegate:Lcom/here/android/mpa/guidance/AudioPlayerDelegate;

.field private bandwidthController:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

.field private bus:Lcom/squareup/otto/Bus;

.field private currentManeuver:Lcom/here/android/mpa/routing/Maneuver;

.field private currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

.field private currentNavigationMode:Lcom/navdy/hud/app/maps/NavigationMode;

.field private currentSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

.field private debugManeuverDistance:Ljava/lang/String;

.field private debugManeuverIcon:I

.field private debugManeuverInstruction:Ljava/lang/String;

.field private debugManeuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

.field private debugNextManeuverIcon:I

.field private driverSessionPreferences:Lcom/navdy/hud/app/profile/DriverSessionPreferences;

.field private etaCalcRunnable:Ljava/lang/Runnable;

.field private generatePhoneticTTS:Ljava/lang/Boolean;

.field private gpsSignalListener:Lcom/navdy/hud/app/maps/here/HereGpsSignalListener;

.field private handler:Landroid/os/Handler;

.field private hereNavController:Lcom/navdy/hud/app/maps/here/HereNavController;

.field private hereRealisticViewListener:Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;

.field private laneInfoListener:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

.field private maneuverAfterCurrent:Lcom/here/android/mpa/routing/Maneuver;

.field private mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

.field private mapView:Lcom/here/android/mpa/mapping/MapView;

.field private navdyTrafficRerouteManager:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

.field private navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

.field private navigationManagerEventListener:Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;

.field private navigationView:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

.field private newManeuverEventListener:Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;

.field private positionUpdateListener:Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener;

.field private prevManeuver:Lcom/here/android/mpa/routing/Maneuver;

.field private reRouteListener:Lcom/navdy/hud/app/maps/here/HereRerouteListener;

.field private safetySpotListener:Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

.field private sharedPreferences:Landroid/content/SharedPreferences;

.field private speedWarningManager:Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;

.field private stringBuilder:Ljava/lang/StringBuilder;

.field private switchingToNewRoute:Z

.field private timeHelper:Lcom/navdy/hud/app/common/TimeHelper;

.field private trafficEtaTracker:Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;

.field private trafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

.field private trafficUpdater:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

.field private trafficUpdaterStarted:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 90
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 99
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->LANGUAGE_HIERARCHY:Ljava/util/Map;

    .line 104
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->LANGUAGE_HIERARCHY:Ljava/util/Map;

    const-string v1, "en-AU"

    const-string v2, "en-GB"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->LANGUAGE_HIERARCHY:Ljava/util/Map;

    const-string v1, "en-GB"

    const-string v2, "en"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->LANGUAGE_HIERARCHY:Ljava/util/Map;

    const-string v1, "en-US"

    const-string v2, "en"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->context:Landroid/content/Context;

    .line 118
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .line 119
    invoke-static {}, Lcom/navdy/hud/app/maps/MapsEventHandler;->getInstance()Lcom/navdy/hud/app/maps/MapsEventHandler;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->mapsEventHandler:Lcom/navdy/hud/app/maps/MapsEventHandler;

    .line 120
    invoke-static {}, Lcom/navdy/hud/app/manager/SpeedManager;->getInstance()Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    .line 141
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->trafficRerouteLock:Ljava/lang/Object;

    .line 143
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    invoke-direct {v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sSingleton:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    .line 150
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$ArrivalEvent;

    invoke-direct {v0}, Lcom/navdy/hud/app/maps/MapEvents$ArrivalEvent;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->ARRIVAL_EVENT:Lcom/navdy/hud/app/maps/MapEvents$ArrivalEvent;

    .line 205
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$SpeedWarning;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/maps/MapEvents$SpeedWarning;-><init>(Z)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->SPEED_EXCEEDED:Lcom/navdy/hud/app/maps/MapEvents$SpeedWarning;

    .line 206
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$SpeedWarning;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/maps/MapEvents$SpeedWarning;-><init>(Z)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->SPEED_NORMAL:Lcom/navdy/hud/app/maps/MapEvents$SpeedWarning;

    return-void
.end method

.method private constructor <init>()V
    .locals 6

    .prologue
    .line 314
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 173
    new-instance v4, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    invoke-direct {v4}, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;-><init>()V

    iput-object v4, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    .line 184
    invoke-static {}, Lcom/navdy/hud/app/maps/MapsEventHandler;->getInstance()Lcom/navdy/hud/app/maps/MapsEventHandler;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/MapsEventHandler;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->bus:Lcom/squareup/otto/Bus;

    .line 187
    invoke-static {}, Lcom/navdy/hud/app/maps/MapsEventHandler;->getInstance()Lcom/navdy/hud/app/maps/MapsEventHandler;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/MapsEventHandler;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 200
    new-instance v4, Lcom/navdy/hud/app/maps/MapEvents$GpsStatusChange;

    const/4 v5, 0x0

    invoke-direct {v4, v5}, Lcom/navdy/hud/app/maps/MapEvents$GpsStatusChange;-><init>(Z)V

    iput-object v4, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->BUS_GPS_SIGNAL_LOST:Lcom/navdy/hud/app/maps/MapEvents$GpsStatusChange;

    .line 201
    new-instance v4, Lcom/navdy/hud/app/maps/MapEvents$GpsStatusChange;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Lcom/navdy/hud/app/maps/MapEvents$GpsStatusChange;-><init>(Z)V

    iput-object v4, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->BUS_GPS_SIGNAL_RESTORED:Lcom/navdy/hud/app/maps/MapEvents$GpsStatusChange;

    .line 257
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v4, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->stringBuilder:Ljava/lang/StringBuilder;

    .line 275
    new-instance v4, Lcom/navdy/hud/app/maps/here/HereNavigationManager$1;

    invoke-direct {v4, p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager$1;-><init>(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)V

    iput-object v4, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->etaCalcRunnable:Ljava/lang/Runnable;

    .line 305
    invoke-static {}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->getInstance()Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->bandwidthController:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

    .line 308
    new-instance v4, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v4, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->handler:Landroid/os/Handler;

    .line 315
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 316
    .local v0, "language":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 317
    .local v3, "resources":Landroid/content/res/Resources;
    new-instance v4, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;

    invoke-direct {v4}, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;-><init>()V

    const v5, 0x7f0902c1

    .line 318
    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->words(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;

    move-result-object v4

    sget-object v5, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->SPEECH_REROUTE:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    .line 319
    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->category(Lcom/navdy/service/library/events/audio/SpeechRequest$Category;)Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;

    move-result-object v4

    .line 320
    invoke-virtual {v4, v0}, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->language(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;

    move-result-object v4

    .line 321
    invoke-virtual {v4}, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->build()Lcom/navdy/service/library/events/audio/SpeechRequest;

    move-result-object v1

    .line 322
    .local v1, "rerouting":Lcom/navdy/service/library/events/audio/SpeechRequest;
    new-instance v4, Lcom/navdy/hud/app/event/LocalSpeechRequest;

    invoke-direct {v4, v1}, Lcom/navdy/hud/app/event/LocalSpeechRequest;-><init>(Lcom/navdy/service/library/events/audio/SpeechRequest;)V

    iput-object v4, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->TTS_REROUTING:Lcom/navdy/hud/app/event/LocalSpeechRequest;

    .line 323
    new-instance v4, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;

    invoke-direct {v4}, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;-><init>()V

    const v5, 0x7f0902c2

    .line 324
    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->words(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;

    move-result-object v4

    sget-object v5, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->SPEECH_REROUTE:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    .line 325
    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->category(Lcom/navdy/service/library/events/audio/SpeechRequest$Category;)Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;

    move-result-object v4

    .line 326
    invoke-virtual {v4, v0}, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->language(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;

    move-result-object v4

    .line 327
    invoke-virtual {v4}, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->build()Lcom/navdy/service/library/events/audio/SpeechRequest;

    move-result-object v2

    .line 328
    .local v2, "reroutingFailed":Lcom/navdy/service/library/events/audio/SpeechRequest;
    new-instance v4, Lcom/navdy/hud/app/event/LocalSpeechRequest;

    invoke-direct {v4, v2}, Lcom/navdy/hud/app/event/LocalSpeechRequest;-><init>(Lcom/navdy/service/library/events/audio/SpeechRequest;)V

    iput-object v4, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->TTS_REROUTING_FAILED:Lcom/navdy/hud/app/event/LocalSpeechRequest;

    .line 329
    const v4, 0x7f0902bd

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->TTS_MILES:Ljava/lang/String;

    .line 330
    const v4, 0x7f0902ba

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->TTS_KMS:Ljava/lang/String;

    .line 331
    const v4, 0x7f0902bc

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->TTS_METERS:Ljava/lang/String;

    .line 332
    const v4, 0x7f09018e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->INVALID_STATE:Ljava/lang/String;

    .line 333
    const v4, 0x7f0901d9

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->NO_CURRENT_POSITION:Ljava/lang/String;

    .line 334
    const v4, 0x7f09018d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->INVALID_ROUTE_ID:Ljava/lang/String;

    .line 335
    const v4, 0x7f09028a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->HERE_TBT_AUDIO_DESTINATION_REACHED_PATTERN:Ljava/lang/String;

    .line 336
    sget-object v4, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_ENGINE_NOT_READY:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    iput-object v4, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 337
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getTimeHelper()Lcom/navdy/hud/app/common/TimeHelper;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->timeHelper:Lcom/navdy/hud/app/common/TimeHelper;

    .line 338
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->initNavigationManager()V

    .line 339
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Lcom/navdy/hud/app/maps/NavigationMode;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationMode:Lcom/navdy/hud/app/maps/NavigationMode;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Lcom/navdy/hud/app/maps/here/HereNavigationInfo;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/navdy/hud/app/maps/here/HereNavigationManager;Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    .param p1, "x1"    # Lcom/here/android/mpa/routing/Maneuver;
    .param p2, "x2"    # Lcom/here/android/mpa/routing/Maneuver;
    .param p3, "x3"    # Lcom/here/android/mpa/routing/Maneuver;
    .param p4, "x4"    # Z

    .prologue
    .line 89
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->updateNavigationInfoInternal(Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;Z)V

    return-void
.end method

.method static synthetic access$1100(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->setNavigationManagerUnit()V

    return-void
.end method

.method static synthetic access$1200()Lcom/navdy/hud/app/maps/MapEvents$ArrivalEvent;
    .locals 1

    .prologue
    .line 89
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->ARRIVAL_EVENT:Lcom/navdy/hud/app/maps/MapEvents$ArrivalEvent;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->postArrivedManeuver()V

    return-void
.end method

.method static synthetic access$1402(Lcom/navdy/hud/app/maps/here/HereNavigationManager;Lcom/navdy/service/library/events/navigation/NavigationSessionState;)Lcom/navdy/service/library/events/navigation/NavigationSessionState;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    .param p1, "x1"    # Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    return-object p1
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->HERE_TBT_AUDIO_DESTINATION_REACHED_PATTERN:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400()Landroid/content/Context;
    .locals 1

    .prologue
    .line 89
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Lcom/squareup/otto/Bus;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->bus:Lcom/squareup/otto/Bus;

    return-object v0
.end method

.method static synthetic access$600()Lcom/navdy/hud/app/maps/here/HereMapsManager;
    .locals 1

    .prologue
    .line 89
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    return-object v0
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Lcom/navdy/hud/app/maps/here/HereNavController;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereNavController:Lcom/navdy/hud/app/maps/here/HereNavController;

    return-object v0
.end method

.method static synthetic access$800()Lcom/navdy/hud/app/maps/MapsEventHandler;
    .locals 1

    .prologue
    .line 89
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->mapsEventHandler:Lcom/navdy/hud/app/maps/MapsEventHandler;

    return-object v0
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/maps/here/HereNavigationManager;Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    .param p1, "x1"    # Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;

    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->handleNavigationSessionRequestInternal(Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;)V

    return-void
.end method

.method private addDestinationMarker(Lcom/here/android/mpa/common/GeoCoordinate;I)V
    .locals 4
    .param p1, "geoCoordinate"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p2, "iconId"    # I

    .prologue
    .line 1729
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->mapDestinationMarker:Lcom/here/android/mpa/mapping/MapMarker;

    if-eqz v2, :cond_0

    .line 1730
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->removeDestinationMarker()V

    .line 1731
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    const/4 v3, 0x0

    iput-object v3, v2, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->mapDestinationMarker:Lcom/here/android/mpa/mapping/MapMarker;

    .line 1734
    :cond_0
    new-instance v0, Lcom/here/android/mpa/common/Image;

    invoke-direct {v0}, Lcom/here/android/mpa/common/Image;-><init>()V

    .line 1735
    .local v0, "image":Lcom/here/android/mpa/common/Image;
    invoke-virtual {v0, p2}, Lcom/here/android/mpa/common/Image;->setImageResource(I)V

    .line 1736
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    new-instance v3, Lcom/here/android/mpa/mapping/MapMarker;

    invoke-direct {v3, p1, v0}, Lcom/here/android/mpa/mapping/MapMarker;-><init>(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/Image;)V

    iput-object v3, v2, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->mapDestinationMarker:Lcom/here/android/mpa/mapping/MapMarker;

    .line 1737
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v3, v3, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->mapDestinationMarker:Lcom/here/android/mpa/mapping/MapMarker;

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/maps/here/HereMapController;->addMapObject(Lcom/here/android/mpa/mapping/MapObject;)V

    .line 1738
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->addMarkers(Lcom/navdy/hud/app/maps/here/HereMapController;)V

    .line 1739
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "added destination marker"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1743
    .end local v0    # "image":Lcom/here/android/mpa/common/Image;
    :goto_0
    return-void

    .line 1740
    :catch_0
    move-exception v1

    .line 1741
    .local v1, "t":Ljava/lang/Throwable;
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private dumpVoiceSkinInfo(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/guidance/VoiceSkin;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 521
    .local p1, "skins":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/guidance/VoiceSkin;>;"
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v2, "Supported voice skins: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 522
    .local v0, "buffer":Ljava/lang/StringBuffer;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/here/android/mpa/guidance/VoiceSkin;

    .line 523
    .local v1, "skin":Lcom/here/android/mpa/guidance/VoiceSkin;
    const-string v3, "[language:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 524
    invoke-virtual {v1}, Lcom/here/android/mpa/guidance/VoiceSkin;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 525
    const-string v3, ",code:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 526
    invoke-virtual {v1}, Lcom/here/android/mpa/guidance/VoiceSkin;->getLanguageCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 527
    const-string v3, "],"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 529
    .end local v1    # "skin":Lcom/here/android/mpa/guidance/VoiceSkin;
    :cond_0
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 530
    return-void
.end method

.method private findSkinSupporting(Ljava/util/List;Ljava/lang/String;)Lcom/here/android/mpa/guidance/VoiceSkin;
    .locals 6
    .param p2, "languageCode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/guidance/VoiceSkin;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/here/android/mpa/guidance/VoiceSkin;"
        }
    .end annotation

    .prologue
    .local p1, "skins":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/guidance/VoiceSkin;>;"
    const/4 v2, 0x0

    .line 506
    if-nez p1, :cond_0

    move-object v1, v2

    .line 517
    :goto_0
    return-object v1

    .line 509
    :cond_0
    if-nez p2, :cond_1

    move-object v1, v2

    .line 510
    goto :goto_0

    .line 512
    :cond_1
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 513
    .local v0, "desiredLanguage":Ljava/lang/String;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/here/android/mpa/guidance/VoiceSkin;

    .line 514
    .local v1, "skin":Lcom/here/android/mpa/guidance/VoiceSkin;
    invoke-virtual {v1}, Lcom/here/android/mpa/guidance/VoiceSkin;->getLanguageCode()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v4, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    goto :goto_0

    .end local v1    # "skin":Lcom/here/android/mpa/guidance/VoiceSkin;
    :cond_3
    move-object v1, v2

    .line 517
    goto :goto_0
.end method

.method private static getHereMapUpdateMode()Lcom/here/android/mpa/guidance/NavigationManager$MapUpdateMode;
    .locals 1

    .prologue
    .line 1149
    sget-object v0, Lcom/here/android/mpa/guidance/NavigationManager$MapUpdateMode;->NONE:Lcom/here/android/mpa/guidance/NavigationManager$MapUpdateMode;

    return-object v0
.end method

.method public static getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    .locals 1

    .prologue
    .line 146
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sSingleton:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    return-object v0
.end method

.method private declared-synchronized handleNavigationSessionRequestInternal(Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;)V
    .locals 17
    .param p1, "event"    # Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;

    .prologue
    .line 921
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 923
    :try_start_1
    sget-object v11, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "NavigationSessionRequest id["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->routeId:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "] newState["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->newState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "] label["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->label:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "] sim-speed["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->simulationSpeed:Ljava/lang/Integer;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "] currentState["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "]"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 926
    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->newState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    invoke-static {v11}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->isValidNavigationState(Lcom/navdy/service/library/events/navigation/NavigationSessionState;)Z

    move-result v11

    if-nez v11, :cond_1

    .line 927
    sget-object v11, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_INVALID_REQUEST:Lcom/navdy/service/library/events/RequestStatus;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->INVALID_STATE:Ljava/lang/String;

    const/4 v13, 0x0

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->routeId:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v12, v13, v14}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->returnResponse(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationSessionState;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1069
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 930
    :cond_1
    :try_start_2
    sget-object v11, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-virtual {v11}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v11

    invoke-virtual {v11}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v11

    if-nez v11, :cond_2

    .line 931
    sget-object v11, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_NO_LOCATION_SERVICE:Lcom/navdy/service/library/events/RequestStatus;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->NO_CURRENT_POSITION:Ljava/lang/String;

    const/4 v13, 0x0

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->routeId:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v12, v13, v14}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->returnResponse(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationSessionState;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1065
    :catch_0
    move-exception v10

    .line 1066
    .local v10, "t":Ljava/lang/Throwable;
    :try_start_3
    sget-object v11, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v12, "handleNavigationSessionRequest"

    invoke-virtual {v11, v12, v10}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1067
    sget-object v11, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SERVICE_ERROR:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v10}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->routeId:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v12, v13, v14}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->returnResponse(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationSessionState;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 921
    .end local v10    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v11

    monitor-exit p0

    throw v11

    .line 934
    :cond_2
    const/4 v9, 0x0

    .line 935
    .local v9, "routeInfo":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    const/4 v8, 0x0

    .line 936
    .local v8, "routeIdProvided":Z
    :try_start_4
    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->routeId:Ljava/lang/String;

    if-eqz v11, :cond_3

    .line 937
    const/4 v8, 0x1

    .line 938
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getInstance()Lcom/navdy/hud/app/maps/here/HereRouteCache;

    move-result-object v11

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->routeId:Ljava/lang/String;

    invoke-virtual {v11, v12}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getRoute(Ljava/lang/String;)Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;

    move-result-object v9

    .line 940
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isNavigationModeOn()Z

    move-result v11

    if-eqz v11, :cond_d

    .line 942
    sget-object v11, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v12, "navigation is active"

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 943
    sget-object v11, Lcom/navdy/hud/app/maps/here/HereNavigationManager$12;->$SwitchMap$com$navdy$service$library$events$navigation$NavigationSessionState:[I

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->newState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    invoke-virtual {v12}, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->ordinal()I

    move-result v12

    aget v11, v11, v12

    packed-switch v11, :pswitch_data_0

    goto :goto_0

    .line 945
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    sget-object v12, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_STARTED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    if-ne v11, v12, :cond_4

    .line 947
    sget-object v11, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v12, "pausing navigation"

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 948
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereNavController:Lcom/navdy/hud/app/maps/here/HereNavController;

    invoke-virtual {v11}, Lcom/navdy/hud/app/maps/here/HereNavController;->pause()V

    .line 949
    sget-object v11, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_PAUSED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 950
    sget-object v11, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    const-string v12, ""

    sget-object v13, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_PAUSED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->routeId:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v12, v13, v14}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->returnResponse(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationSessionState;Ljava/lang/String;)V

    .line 951
    const/4 v11, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->postNavigationSessionStatusEvent(Z)V

    goto/16 :goto_0

    .line 954
    :cond_4
    sget-object v11, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v12, "already paused"

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 955
    sget-object v11, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    const-string v12, ""

    sget-object v13, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_PAUSED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v12, v13, v14}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->returnResponse(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationSessionState;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 962
    :pswitch_1
    sget-object v11, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v12, "stopping navigation"

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 963
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 964
    .local v4, "error":Ljava/lang/StringBuilder;
    sget-object v11, Lcom/navdy/hud/app/maps/NavigationMode;->MAP:Lcom/navdy/hud/app/maps/NavigationMode;

    const/4 v12, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v12, v4}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->setNavigationMode(Lcom/navdy/hud/app/maps/NavigationMode;Lcom/navdy/hud/app/maps/here/HereNavigationInfo;Ljava/lang/StringBuilder;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 965
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->navigationView:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    invoke-virtual {v11}, Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;->resetMapOverview()V

    .line 966
    sget-object v11, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    const-string v12, ""

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->routeId:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v12, v13, v14}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->returnResponse(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationSessionState;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 968
    :cond_5
    sget-object v11, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SERVICE_ERROR:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->routeId:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v12, v13, v14}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->returnResponse(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationSessionState;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 974
    .end local v4    # "error":Ljava/lang/StringBuilder;
    :pswitch_2
    if-nez v9, :cond_6

    if-eqz v8, :cond_6

    .line 977
    sget-object v11, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_INVALID_REQUEST:Lcom/navdy/service/library/events/RequestStatus;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->INVALID_ROUTE_ID:Ljava/lang/String;

    const/4 v13, 0x0

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->routeId:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v12, v13, v14}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->returnResponse(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationSessionState;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 980
    :cond_6
    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->routeId:Ljava/lang/String;

    if-eqz v11, :cond_7

    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->routeId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v12, v12, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->routeId:Ljava/lang/String;

    invoke-static {v11, v12}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_8

    :cond_7
    const/4 v5, 0x1

    .line 981
    .local v5, "isRouteIdCurrent":Z
    :goto_1
    if-eqz v5, :cond_b

    .line 982
    sget-object v11, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v12, "same route"

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 983
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    sget-object v12, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_STARTED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    if-ne v11, v12, :cond_9

    .line 985
    sget-object v11, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    const-string v12, ""

    sget-object v13, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_STARTED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->routeId:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v12, v13, v14}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->returnResponse(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationSessionState;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 980
    .end local v5    # "isRouteIdCurrent":Z
    :cond_8
    const/4 v5, 0x0

    goto :goto_1

    .line 989
    .restart local v5    # "isRouteIdCurrent":Z
    :cond_9
    sget-object v11, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v12, "resume navigation"

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 990
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereNavController:Lcom/navdy/hud/app/maps/here/HereNavController;

    invoke-virtual {v11}, Lcom/navdy/hud/app/maps/here/HereNavController;->resume()Lcom/here/android/mpa/guidance/NavigationManager$Error;

    move-result-object v4

    .line 991
    .local v4, "error":Lcom/here/android/mpa/guidance/NavigationManager$Error;
    sget-object v11, Lcom/here/android/mpa/guidance/NavigationManager$Error;->NONE:Lcom/here/android/mpa/guidance/NavigationManager$Error;

    if-ne v4, v11, :cond_a

    .line 992
    sget-object v11, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_STARTED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 993
    sget-object v11, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    const-string v12, ""

    sget-object v13, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_STARTED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->routeId:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v12, v13, v14}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->returnResponse(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationSessionState;Ljava/lang/String;)V

    .line 994
    const/4 v11, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->postNavigationSessionStatusEvent(Z)V

    goto/16 :goto_0

    .line 997
    :cond_a
    sget-object v11, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "cannot resume navigation:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 998
    sget-object v11, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SERVICE_ERROR:Lcom/navdy/service/library/events/RequestStatus;

    sget-object v12, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->context:Landroid/content/Context;

    const v13, 0x7f0902c9

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v4, v14, v15

    invoke-virtual {v12, v13, v14}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    sget-object v13, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_STARTED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->routeId:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v12, v13, v14}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->returnResponse(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationSessionState;Ljava/lang/String;)V

    .line 999
    sget-object v11, Lcom/navdy/hud/app/maps/NavigationMode;->MAP:Lcom/navdy/hud/app/maps/NavigationMode;

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v12, v13}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->setNavigationMode(Lcom/navdy/hud/app/maps/NavigationMode;Lcom/navdy/hud/app/maps/here/HereNavigationInfo;Ljava/lang/StringBuilder;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 1000
    sget-object v11, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v12, "cannot even go to tracking mode"

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1006
    .end local v4    # "error":Lcom/here/android/mpa/guidance/NavigationManager$Error;
    :cond_b
    sget-object v11, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v12, "different route: switching to new route"

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1007
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 1008
    .local v4, "error":Ljava/lang/StringBuilder;
    sget-object v11, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v12, "stop navigation"

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1010
    :try_start_5
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    .line 1011
    .local v6, "l1":J
    const/4 v11, 0x1

    move-object/from16 v0, p0

    iput-boolean v11, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->switchingToNewRoute:Z

    .line 1012
    sget-object v11, Lcom/navdy/hud/app/maps/NavigationMode;->MAP:Lcom/navdy/hud/app/maps/NavigationMode;

    const/4 v12, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v12, v4}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->setNavigationMode(Lcom/navdy/hud/app/maps/NavigationMode;Lcom/navdy/hud/app/maps/here/HereNavigationInfo;Ljava/lang/StringBuilder;)Z

    move-result v11

    if-eqz v11, :cond_c

    .line 1013
    sget-object v11, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v12, "start navigation"

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1014
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v9}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->startNavigation(Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;)V

    .line 1015
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v12

    sub-long v2, v12, v6

    .line 1016
    .local v2, "diff":J
    sget-object v11, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "navigation took["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "]"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1023
    .end local v2    # "diff":J
    :goto_2
    const/4 v11, 0x0

    :try_start_6
    move-object/from16 v0, p0

    iput-boolean v11, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->switchingToNewRoute:Z
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0

    .line 1018
    :cond_c
    :try_start_7
    sget-object v11, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SERVICE_ERROR:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->routeId:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v12, v13, v14}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->returnResponse(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationSessionState;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_2

    .line 1020
    .end local v6    # "l1":J
    :catch_1
    move-exception v10

    .line 1021
    .restart local v10    # "t":Ljava/lang/Throwable;
    :try_start_8
    throw v10
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 1023
    .end local v10    # "t":Ljava/lang/Throwable;
    :catchall_1
    move-exception v11

    const/4 v12, 0x0

    :try_start_9
    move-object/from16 v0, p0

    iput-boolean v12, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->switchingToNewRoute:Z

    throw v11

    .line 1031
    .end local v4    # "error":Ljava/lang/StringBuilder;
    .end local v5    # "isRouteIdCurrent":Z
    :cond_d
    sget-object v11, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v12, "navigation is not active"

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1032
    sget-object v11, Lcom/navdy/hud/app/maps/here/HereNavigationManager$12;->$SwitchMap$com$navdy$service$library$events$navigation$NavigationSessionState:[I

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->newState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    invoke-virtual {v12}, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->ordinal()I

    move-result v12

    aget v11, v11, v12

    packed-switch v11, :pswitch_data_1

    goto/16 :goto_0

    .line 1059
    :pswitch_3
    sget-object v11, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "cannot pause navigation current state:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1060
    sget-object v11, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_INVALID_STATE:Lcom/navdy/service/library/events/RequestStatus;

    sget-object v12, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->context:Landroid/content/Context;

    const v13, 0x7f090051

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->name()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->newState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->name()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-virtual {v12, v13, v14}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->routeId:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v12, v13, v14}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->returnResponse(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationSessionState;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1034
    :pswitch_4
    if-nez v9, :cond_e

    .line 1036
    sget-object v11, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v12, "valid route id required"

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1037
    sget-object v11, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_INVALID_REQUEST:Lcom/navdy/service/library/events/RequestStatus;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->INVALID_ROUTE_ID:Ljava/lang/String;

    const/4 v13, 0x0

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->routeId:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v12, v13, v14}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->returnResponse(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationSessionState;Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_0

    .line 1042
    :cond_e
    :try_start_a
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    .line 1043
    .restart local v6    # "l1":J
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v9}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->startNavigation(Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;)V

    .line 1044
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v12

    sub-long v2, v12, v6

    .line 1045
    .restart local v2    # "diff":J
    sget-object v11, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "navigation took["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "]"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_0

    .line 1046
    .end local v2    # "diff":J
    .end local v6    # "l1":J
    :catch_2
    move-exception v10

    .line 1047
    .restart local v10    # "t":Ljava/lang/Throwable;
    :try_start_b
    throw v10

    .line 1053
    .end local v10    # "t":Ljava/lang/Throwable;
    :pswitch_5
    sget-object v11, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v12, "already stopped"

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1054
    sget-object v11, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    const-string v12, ""

    sget-object v13, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_STOPPED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v12, v13, v14}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->returnResponse(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationSessionState;Ljava/lang/String;)V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_0

    .line 943
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 1032
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method

.method private initNavigationManager()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 343
    invoke-static {}, Lcom/here/android/mpa/guidance/NavigationManager;->getInstance()Lcom/here/android/mpa/guidance/NavigationManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    .line 344
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereNavController;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->bus:Lcom/squareup/otto/Bus;

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/maps/here/HereNavController;-><init>(Lcom/here/android/mpa/guidance/NavigationManager;Lcom/squareup/otto/Bus;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereNavController:Lcom/navdy/hud/app/maps/here/HereNavController;

    .line 346
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getMapController()Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    .line 349
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "[CB-NAVM]"

    invoke-direct {v0, v1, v2, p0}, Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;-><init>(Lcom/navdy/service/library/log/Logger;Ljava/lang/String;Lcom/navdy/hud/app/maps/here/HereNavigationManager;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->navigationManagerEventListener:Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;

    .line 350
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    new-instance v1, Ljava/lang/ref/WeakReference;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->navigationManagerEventListener:Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;

    invoke-direct {v1, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/guidance/NavigationManager;->addNavigationManagerEventListener(Ljava/lang/ref/WeakReference;)V

    .line 352
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "[CB-NEWM]"

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereNavController:Lcom/navdy/hud/app/maps/here/HereNavController;

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->bus:Lcom/squareup/otto/Bus;

    move-object v5, p0

    invoke-direct/range {v0 .. v6}, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;-><init>(Lcom/navdy/service/library/log/Logger;Ljava/lang/String;ZLcom/navdy/hud/app/maps/here/HereNavController;Lcom/navdy/hud/app/maps/here/HereNavigationManager;Lcom/squareup/otto/Bus;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->newManeuverEventListener:Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;

    .line 353
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    new-instance v1, Ljava/lang/ref/WeakReference;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->newManeuverEventListener:Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;

    invoke-direct {v1, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/guidance/NavigationManager;->addNewInstructionEventListener(Ljava/lang/ref/WeakReference;)V

    .line 355
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereGpsSignalListener;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "[CB-GPS]"

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->bus:Lcom/squareup/otto/Bus;

    sget-object v4, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->mapsEventHandler:Lcom/navdy/hud/app/maps/MapsEventHandler;

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/maps/here/HereGpsSignalListener;-><init>(Lcom/navdy/service/library/log/Logger;Ljava/lang/String;Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/maps/MapsEventHandler;Lcom/navdy/hud/app/maps/here/HereNavigationManager;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->gpsSignalListener:Lcom/navdy/hud/app/maps/here/HereGpsSignalListener;

    .line 356
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    new-instance v1, Ljava/lang/ref/WeakReference;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->gpsSignalListener:Lcom/navdy/hud/app/maps/here/HereGpsSignalListener;

    invoke-direct {v1, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/guidance/NavigationManager;->addGpsSignalListener(Ljava/lang/ref/WeakReference;)V

    .line 358
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;

    const-string v1, "[CB-SPD]"

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->bus:Lcom/squareup/otto/Bus;

    sget-object v3, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->mapsEventHandler:Lcom/navdy/hud/app/maps/MapsEventHandler;

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;-><init>(Ljava/lang/String;Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/maps/MapsEventHandler;Lcom/navdy/hud/app/maps/here/HereNavigationManager;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->speedWarningManager:Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;

    .line 360
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->bus:Lcom/squareup/otto/Bus;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/maps/here/HereMapController;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->safetySpotListener:Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

    .line 361
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    new-instance v1, Ljava/lang/ref/WeakReference;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->safetySpotListener:Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

    invoke-direct {v1, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/guidance/NavigationManager;->addSafetySpotListener(Ljava/lang/ref/WeakReference;)V

    .line 363
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereRerouteListener;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "[CB-REROUTE]"

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->bus:Lcom/squareup/otto/Bus;

    invoke-direct {v0, v1, v2, p0, v3}, Lcom/navdy/hud/app/maps/here/HereRerouteListener;-><init>(Lcom/navdy/service/library/log/Logger;Ljava/lang/String;Lcom/navdy/hud/app/maps/here/HereNavigationManager;Lcom/squareup/otto/Bus;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->reRouteListener:Lcom/navdy/hud/app/maps/here/HereRerouteListener;

    .line 364
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    new-instance v1, Ljava/lang/ref/WeakReference;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->reRouteListener:Lcom/navdy/hud/app/maps/here/HereRerouteListener;

    invoke-direct {v1, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/guidance/NavigationManager;->addRerouteListener(Ljava/lang/ref/WeakReference;)V

    .line 366
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isUserBuild()Z

    move-result v0

    if-nez v0, :cond_0

    .line 367
    invoke-static {}, Lcom/navdy/hud/app/maps/MapSettings;->isLaneGuidanceEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 368
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->bus:Lcom/squareup/otto/Bus;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereNavController:Lcom/navdy/hud/app/maps/here/HereNavController;

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/maps/here/HereNavController;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->laneInfoListener:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    .line 372
    :cond_0
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->bus:Lcom/squareup/otto/Bus;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereNavController:Lcom/navdy/hud/app/maps/here/HereNavController;

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;-><init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/maps/here/HereNavController;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereRealisticViewListener:Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;

    .line 373
    new-instance v0, Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener;

    const-string v1, "[CB-POS-UPDATE]"

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereNavController:Lcom/navdy/hud/app/maps/here/HereNavController;

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->bus:Lcom/squareup/otto/Bus;

    invoke-direct {v0, v1, v2, p0, v3}, Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener;-><init>(Ljava/lang/String;Lcom/navdy/hud/app/maps/here/HereNavController;Lcom/navdy/hud/app/maps/here/HereNavigationManager;Lcom/squareup/otto/Bus;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->positionUpdateListener:Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener;

    .line 374
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    new-instance v1, Ljava/lang/ref/WeakReference;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->positionUpdateListener:Lcom/navdy/hud/app/maps/here/HerePositionUpdateListener;

    invoke-direct {v1, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/guidance/NavigationManager;->addPositionListener(Ljava/lang/ref/WeakReference;)V

    .line 376
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    const-string v1, "[CB-TRAFFIC-REROUTE]"

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereNavController:Lcom/navdy/hud/app/maps/here/HereNavController;

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->bus:Lcom/squareup/otto/Bus;

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;-><init>(Ljava/lang/String;ZLcom/navdy/hud/app/maps/here/HereNavController;Lcom/navdy/hud/app/maps/here/HereNavigationManager;Lcom/squareup/otto/Bus;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->trafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    .line 377
    new-instance v0, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->trafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->bus:Lcom/squareup/otto/Bus;

    invoke-direct {v0, p0, v1, v2}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;-><init>(Lcom/navdy/hud/app/maps/here/HereNavigationManager;Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;Lcom/squareup/otto/Bus;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->navdyTrafficRerouteManager:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    .line 378
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->trafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->navdyTrafficRerouteManager:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->setNavdyTrafficRerouteManager(Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;)V

    .line 380
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->bus:Lcom/squareup/otto/Bus;

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;-><init>(Lcom/squareup/otto/Bus;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->trafficUpdater:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    .line 382
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereNavController:Lcom/navdy/hud/app/maps/here/HereNavController;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->bus:Lcom/squareup/otto/Bus;

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;-><init>(Lcom/navdy/hud/app/maps/here/HereNavController;Lcom/squareup/otto/Bus;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->trafficEtaTracker:Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;

    .line 383
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    new-instance v1, Ljava/lang/ref/WeakReference;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->trafficEtaTracker:Lcom/navdy/hud/app/maps/here/HereTrafficETATracker;

    invoke-direct {v1, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/guidance/NavigationManager;->addNavigationManagerEventListener(Ljava/lang/ref/WeakReference;)V

    .line 386
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->setMapUpdateMode()V

    .line 389
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->setupNavigationTTS()V

    .line 392
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->setNavigationManagerUnit()V

    .line 403
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getDriverProfileManager()Lcom/navdy/hud/app/profile/DriverProfileManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getSessionPreferences()Lcom/navdy/hud/app/profile/DriverSessionPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->driverSessionPreferences:Lcom/navdy/hud/app/profile/DriverSessionPreferences;

    .line 404
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->bandwidthController:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->isLimitBandwidthModeOn()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 406
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "limitbandwidth: initial on"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 407
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->setBandwidthPreferences()V

    .line 412
    :goto_0
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->mapsEventHandler:Lcom/navdy/hud/app/maps/MapsEventHandler;

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/MapsEventHandler;->getNavigationPreferences()Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    move-result-object v1

    iget-object v1, v1, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->phoneticTurnByTurn:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->setGeneratePhoneticTTS(Z)V

    .line 414
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 416
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "start speed warning manager"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 417
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->speedWarningManager:Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereSpeedWarningManager;->start()V

    .line 419
    sget-object v0, Lcom/navdy/hud/app/maps/NavigationMode;->MAP:Lcom/navdy/hud/app/maps/NavigationMode;

    invoke-virtual {p0, v0, v7, v7}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->setNavigationMode(Lcom/navdy/hud/app/maps/NavigationMode;Lcom/navdy/hud/app/maps/here/HereNavigationInfo;Ljava/lang/StringBuilder;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 420
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "navigation mode cannot be switched to map"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 422
    :cond_1
    return-void

    .line 409
    :cond_2
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->mapsEventHandler:Lcom/navdy/hud/app/maps/MapsEventHandler;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/MapsEventHandler;->getNavigationPreferences()Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    move-result-object v0

    iget-object v0, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->rerouteForTraffic:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->setTrafficRerouteMode(Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;)V

    goto :goto_0
.end method

.method private postArrivedManeuver()V
    .locals 4

    .prologue
    .line 1903
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v0, v1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->arrivedManeuverDisplay:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    .line 1904
    .local v0, "maneuverDisplay":Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    if-nez v0, :cond_0

    .line 1905
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->getArrivedManeuverDisplay()Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    move-result-object v0

    .line 1906
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iput-object v0, v1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->arrivedManeuverDisplay:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    .line 1907
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "arrival maneuver created"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1910
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->lastManeuverPostTime:J

    .line 1911
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v1, v0}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 1912
    return-void
.end method

.method private removeDestinationMarker()V
    .locals 2

    .prologue
    .line 1746
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->mapDestinationMarker:Lcom/here/android/mpa/mapping/MapMarker;

    if-eqz v0, :cond_0

    .line 1747
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "removed destination marker"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1748
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v1, v1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->mapDestinationMarker:Lcom/here/android/mpa/mapping/MapMarker;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereMapController;->removeMapObject(Lcom/here/android/mpa/mapping/MapObject;)V

    .line 1749
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->removeMarkers(Lcom/navdy/hud/app/maps/here/HereMapController;)V

    .line 1751
    :cond_0
    return-void
.end method

.method private setGeneratePhoneticTTS(Z)V
    .locals 2
    .param p1, "phoneticTTS"    # Z

    .prologue
    .line 1682
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->generatePhoneticTTS:Ljava/lang/Boolean;

    .line 1683
    if-eqz p1, :cond_0

    sget-object v0, Lcom/here/android/mpa/guidance/NavigationManager$TtsOutputFormat;->NUANCE:Lcom/here/android/mpa/guidance/NavigationManager$TtsOutputFormat;

    .line 1684
    .local v0, "format":Lcom/here/android/mpa/guidance/NavigationManager$TtsOutputFormat;
    :goto_0
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v1, v0}, Lcom/here/android/mpa/guidance/NavigationManager;->setTtsOutputFormat(Lcom/here/android/mpa/guidance/NavigationManager$TtsOutputFormat;)V

    .line 1685
    return-void

    .line 1683
    .end local v0    # "format":Lcom/here/android/mpa/guidance/NavigationManager$TtsOutputFormat;
    :cond_0
    sget-object v0, Lcom/here/android/mpa/guidance/NavigationManager$TtsOutputFormat;->RAW:Lcom/here/android/mpa/guidance/NavigationManager$TtsOutputFormat;

    goto :goto_0
.end method

.method private setNavigationManagerUnit()V
    .locals 3

    .prologue
    .line 1713
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/SpeedManager;->getSpeedUnit()Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    move-result-object v0

    .line 1715
    .local v0, "speedUnit":Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager$12;->$SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit:[I

    invoke-virtual {v0}, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1721
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereNavController:Lcom/navdy/hud/app/maps/here/HereNavController;

    sget-object v2, Lcom/here/android/mpa/guidance/NavigationManager$UnitSystem;->IMPERIAL_US:Lcom/here/android/mpa/guidance/NavigationManager$UnitSystem;

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/maps/here/HereNavController;->setDistanceUnit(Lcom/here/android/mpa/guidance/NavigationManager$UnitSystem;)V

    .line 1724
    :goto_0
    return-void

    .line 1717
    :pswitch_0
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereNavController:Lcom/navdy/hud/app/maps/here/HereNavController;

    sget-object v2, Lcom/here/android/mpa/guidance/NavigationManager$UnitSystem;->METRIC:Lcom/here/android/mpa/guidance/NavigationManager$UnitSystem;

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/maps/here/HereNavController;->setDistanceUnit(Lcom/here/android/mpa/guidance/NavigationManager$UnitSystem;)V

    goto :goto_0

    .line 1715
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private setTrafficOverlay()V
    .locals 4

    .prologue
    .line 1636
    const/4 v0, 0x1

    .line 1637
    .local v0, "enableTraffic":Z
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "enableTraffic on map:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1638
    if-eqz v0, :cond_0

    .line 1639
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationMode:Lcom/navdy/hud/app/maps/NavigationMode;

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->setTrafficOverlay(Lcom/navdy/hud/app/maps/NavigationMode;)V

    .line 1643
    :goto_0
    return-void

    .line 1641
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->clearTrafficOverlay()V

    goto :goto_0
.end method

.method private setTrafficRerouteMode(Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;)V
    .locals 2
    .param p1, "mode"    # Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

    .prologue
    .line 1612
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    sget-object v1, Lcom/here/android/mpa/guidance/NavigationManager$TrafficAvoidanceMode;->MANUAL:Lcom/here/android/mpa/guidance/NavigationManager$TrafficAvoidanceMode;

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/guidance/NavigationManager;->setTrafficAvoidanceMode(Lcom/here/android/mpa/guidance/NavigationManager$TrafficAvoidanceMode;)Lcom/here/android/mpa/guidance/NavigationManager$Error;

    .line 1625
    return-void
.end method

.method private setupNavigationTTS()V
    .locals 9

    .prologue
    .line 533
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/navdy/hud/app/profile/HudLocale;->getCurrentLocale(Landroid/content/Context;)Ljava/util/Locale;

    move-result-object v1

    .line 535
    .local v1, "currentLocale":Ljava/util/Locale;
    invoke-virtual {v1}, Ljava/util/Locale;->toLanguageTag()Ljava/lang/String;

    move-result-object v3

    .line 536
    .local v3, "regionalLanguage":Ljava/lang/String;
    invoke-static {v3}, Lcom/navdy/hud/app/profile/HudLocale;->getBaseLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 538
    .local v0, "baseLanguage":Ljava/lang/String;
    invoke-static {}, Lcom/here/android/mpa/guidance/VoiceCatalog;->getInstance()Lcom/here/android/mpa/guidance/VoiceCatalog;

    move-result-object v5

    .line 539
    .local v5, "voiceCatalog":Lcom/here/android/mpa/guidance/VoiceCatalog;
    invoke-virtual {v5}, Lcom/here/android/mpa/guidance/VoiceCatalog;->getLocalVoiceSkins()Ljava/util/List;

    move-result-object v2

    .line 541
    .local v2, "localSkins":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/guidance/VoiceSkin;>;"
    const/4 v4, 0x0

    .line 542
    .local v4, "selectedSkin":Lcom/here/android/mpa/guidance/VoiceSkin;
    if-eqz v2, :cond_2

    .line 543
    :goto_0
    if-eqz v3, :cond_0

    if-nez v4, :cond_0

    .line 544
    invoke-direct {p0, v2, v3}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->findSkinSupporting(Ljava/util/List;Ljava/lang/String;)Lcom/here/android/mpa/guidance/VoiceSkin;

    move-result-object v4

    .line 545
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->LANGUAGE_HIERARCHY:Ljava/util/Map;

    invoke-interface {v6, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "regionalLanguage":Ljava/lang/String;
    check-cast v3, Ljava/lang/String;

    .restart local v3    # "regionalLanguage":Ljava/lang/String;
    goto :goto_0

    .line 547
    :cond_0
    if-nez v4, :cond_1

    .line 548
    invoke-direct {p0, v2, v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->findSkinSupporting(Ljava/util/List;Ljava/lang/String;)Lcom/here/android/mpa/guidance/VoiceSkin;

    move-result-object v4

    .line 550
    :cond_1
    if-nez v4, :cond_2

    .line 551
    const-string v6, "en-US"

    invoke-direct {p0, v2, v6}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->findSkinSupporting(Ljava/util/List;Ljava/lang/String;)Lcom/here/android/mpa/guidance/VoiceSkin;

    move-result-object v4

    .line 555
    :cond_2
    if-nez v4, :cond_3

    .line 556
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "No voice skin found for default locale:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/navdy/hud/app/maps/here/HereMapUtil;->TBT_ISO3_LANG_CODE:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 608
    :goto_1
    return-void

    .line 558
    :cond_3
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Found voice skin lang["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Lcom/here/android/mpa/guidance/VoiceSkin;->getLanguage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " code["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Lcom/here/android/mpa/guidance/VoiceSkin;->getLanguageCode()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] for locale:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 559
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v6, v4}, Lcom/here/android/mpa/guidance/NavigationManager;->setVoiceSkin(Lcom/here/android/mpa/guidance/VoiceSkin;)Lcom/here/android/mpa/guidance/NavigationManager$Error;

    .line 560
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-virtual {v6}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->setVoiceSkinsLoaded()V

    .line 561
    new-instance v6, Lcom/navdy/hud/app/maps/here/HereNavigationManager$2;

    invoke-direct {v6, p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager$2;-><init>(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)V

    iput-object v6, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->audioPlayerDelegate:Lcom/here/android/mpa/guidance/AudioPlayerDelegate;

    .line 606
    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v6}, Lcom/here/android/mpa/guidance/NavigationManager;->getAudioPlayer()Lcom/here/android/mpa/guidance/NavigationManager$AudioPlayer;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->audioPlayerDelegate:Lcom/here/android/mpa/guidance/AudioPlayerDelegate;

    invoke-virtual {v6, v7}, Lcom/here/android/mpa/guidance/NavigationManager$AudioPlayer;->setDelegate(Lcom/here/android/mpa/guidance/AudioPlayerDelegate;)V

    goto :goto_1
.end method

.method private startNavigation(Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;)V
    .locals 16
    .param p1, "event"    # Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;
    .param p2, "routeInfo"    # Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;

    .prologue
    .line 1072
    new-instance v6, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    invoke-direct {v6}, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;-><init>()V

    .line 1073
    .local v6, "navigationInfo":Lcom/navdy/hud/app/maps/here/HereNavigationInfo;
    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->routeRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iput-object v9, v6, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->navigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .line 1074
    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->routeStartPoint:Lcom/here/android/mpa/common/GeoCoordinate;

    iput-object v9, v6, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->startLocation:Lcom/here/android/mpa/common/GeoCoordinate;

    .line 1075
    sget-object v9, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-virtual {v9}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getRouteOptions()Lcom/here/android/mpa/routing/RouteOptions;

    move-result-object v9

    iput-object v9, v6, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->routeOptions:Lcom/here/android/mpa/routing/RouteOptions;

    .line 1076
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iput-object v9, v6, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->waypoints:Ljava/util/List;

    .line 1078
    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->routeRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v9, v9, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->waypoints:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/navdy/service/library/events/location/Coordinate;

    .line 1079
    .local v8, "waypoint":Lcom/navdy/service/library/events/location/Coordinate;
    iget-object v10, v6, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->waypoints:Ljava/util/List;

    new-instance v11, Lcom/here/android/mpa/common/GeoCoordinate;

    iget-object v12, v8, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v12}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v12

    iget-object v14, v8, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v14}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v14

    invoke-direct {v11, v12, v13, v14, v15}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    invoke-interface {v10, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1082
    .end local v8    # "waypoint":Lcom/navdy/service/library/events/location/Coordinate;
    :cond_0
    new-instance v9, Lcom/here/android/mpa/common/GeoCoordinate;

    move-object/from16 v0, p2

    iget-object v10, v0, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->routeRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v10, v10, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v10, v10, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v10}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v10

    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->routeRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v12, v12, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v12, v12, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v12}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v12

    invoke-direct {v9, v10, v11, v12, v13}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    iput-object v9, v6, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destination:Lcom/here/android/mpa/common/GeoCoordinate;

    .line 1083
    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->route:Lcom/here/android/mpa/routing/Route;

    iput-object v9, v6, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->route:Lcom/here/android/mpa/routing/Route;

    .line 1084
    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->simulationSpeed:Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    iput v9, v6, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->simulationSpeed:I

    .line 1085
    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->routeId:Ljava/lang/String;

    iput-object v9, v6, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->routeId:Ljava/lang/String;

    .line 1086
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v9

    iput-object v9, v6, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    .line 1088
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getLastKnownDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v4

    .line 1089
    .local v4, "lastKnownDeviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    iget-object v9, v6, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    if-nez v9, :cond_1

    .line 1094
    iput-object v4, v6, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    .line 1095
    sget-object v9, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "lastknown device id = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, v6, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1098
    :cond_1
    iget-object v9, v6, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    if-nez v9, :cond_2

    .line 1099
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getConnectionHandler()Lcom/navdy/hud/app/service/ConnectionHandler;

    move-result-object v9

    invoke-virtual {v9}, Lcom/navdy/hud/app/service/ConnectionHandler;->getLastConnectedDeviceInfo()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v2

    .line 1100
    .local v2, "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    if-eqz v2, :cond_2

    .line 1101
    new-instance v9, Lcom/navdy/service/library/device/NavdyDeviceId;

    iget-object v10, v2, Lcom/navdy/service/library/events/DeviceInfo;->deviceId:Ljava/lang/String;

    invoke-direct {v9, v10}, Lcom/navdy/service/library/device/NavdyDeviceId;-><init>(Ljava/lang/String;)V

    iput-object v9, v6, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    .line 1102
    sget-object v9, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "device id is null , adding last device:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, v2, Lcom/navdy/service/library/events/DeviceInfo;->deviceId:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1106
    .end local v2    # "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    :cond_2
    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->routeRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v9, v9, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination_identifier:Ljava/lang/String;

    if-eqz v9, :cond_3

    .line 1107
    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->routeRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v9, v9, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination_identifier:Ljava/lang/String;

    iput-object v9, v6, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destinationIdentifier:Ljava/lang/String;

    .line 1110
    :cond_3
    const/4 v7, 0x0

    .line 1111
    .local v7, "streetAddress":Ljava/lang/String;
    iget-object v5, v6, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->navigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .line 1112
    .local v5, "navRequest":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    if-eqz v5, :cond_4

    .line 1113
    iget-object v7, v5, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->streetAddress:Ljava/lang/String;

    .line 1114
    iget-object v9, v5, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->label:Ljava/lang/String;

    iput-object v9, v6, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destinationLabel:Ljava/lang/String;

    .line 1116
    :cond_4
    invoke-static {v7}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->parseStreetAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v6, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->streetAddress:Ljava/lang/String;

    .line 1117
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1118
    .local v3, "error":Ljava/lang/StringBuilder;
    sget-object v9, Lcom/navdy/hud/app/maps/NavigationMode;->MAP_ON_ROUTE:Lcom/navdy/hud/app/maps/NavigationMode;

    move-object/from16 v0, p0

    invoke-virtual {v0, v9, v6, v3}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->setNavigationMode(Lcom/navdy/hud/app/maps/NavigationMode;Lcom/navdy/hud/app/maps/here/HereNavigationInfo;Ljava/lang/StringBuilder;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1119
    sget-object v9, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->routeId:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v9, v10, v11, v12}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->returnResponse(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationSessionState;Ljava/lang/String;)V

    .line 1123
    :goto_1
    return-void

    .line 1121
    :cond_5
    sget-object v9, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SERVICE_ERROR:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->routeId:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v9, v10, v11, v12}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->returnResponse(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationSessionState;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private tearDownCurrentRoute()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 465
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->removeCurrentRoute()Z

    .line 466
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->removeDestinationMarker()V

    .line 467
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iput-object v1, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->mapDestinationMarker:Lcom/here/android/mpa/mapping/MapMarker;

    .line 468
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->trafficUpdater:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->setRoute(Lcom/here/android/mpa/routing/Route;)V

    .line 469
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->trafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->dismissReroute()V

    .line 470
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteDismissEvent;

    invoke-direct {v1}, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteDismissEvent;-><init>()V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 471
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficDismissEvent;

    invoke-direct {v1}, Lcom/navdy/hud/app/maps/MapEvents$LiveTrafficDismissEvent;-><init>()V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 472
    return-void
.end method

.method private declared-synchronized updateNavigationInfoInternal(Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;Z)V
    .locals 22
    .param p1, "nextManeuver"    # Lcom/here/android/mpa/routing/Maneuver;
    .param p2, "maneuverAfterNext"    # Lcom/here/android/mpa/routing/Maneuver;
    .param p3, "previous"    # Lcom/here/android/mpa/routing/Maneuver;
    .param p4, "clearPrevious"    # Z

    .prologue
    .line 1322
    monitor-enter p0

    const/4 v10, 0x1

    .line 1324
    .local v10, "calculateEarlyManeuver":Z
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-boolean v2, v2, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->hasArrived:Z

    if-eqz v2, :cond_2

    .line 1326
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-boolean v2, v2, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->ignoreArrived:Z

    if-eqz v2, :cond_0

    .line 1327
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "ignore arrived"

    invoke-virtual {v2, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1528
    :goto_0
    monitor-exit p0

    return-void

    .line 1333
    :cond_0
    :try_start_1
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLastGeoPosition()Lcom/here/android/mpa/common/GeoPosition;

    move-result-object v15

    .line 1334
    .local v15, "currentPos":Lcom/here/android/mpa/common/GeoPosition;
    if-eqz v15, :cond_1

    .line 1335
    invoke-virtual {v15}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v14

    .line 1336
    .local v14, "currentGeo":Lcom/here/android/mpa/common/GeoCoordinate;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destination:Lcom/here/android/mpa/common/GeoCoordinate;

    invoke-virtual {v14, v2}, Lcom/here/android/mpa/common/GeoCoordinate;->distanceTo(Lcom/here/android/mpa/common/GeoCoordinate;)D

    move-result-wide v6

    double-to-int v4, v6

    .line 1337
    .local v4, "distance":I
    const/16 v2, 0x324

    if-lt v4, v2, :cond_1

    .line 1338
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "we have exceeded geo threshold:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " , stopping nav"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1339
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->stopNavigation()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1343
    .end local v4    # "distance":I
    .end local v14    # "currentGeo":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v15    # "currentPos":Lcom/here/android/mpa/common/GeoPosition;
    :catch_0
    move-exception v21

    .line 1344
    .local v21, "t":Ljava/lang/Throwable;
    :try_start_2
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 1348
    .end local v21    # "t":Ljava/lang/Throwable;
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->postArrivedManeuver()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1322
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 1352
    :cond_2
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    sget-object v6, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_STARTED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    if-eq v2, v6, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    sget-object v6, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_PAUSED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    if-ne v2, v6, :cond_1c

    .line 1355
    :cond_3
    if-nez p1, :cond_14

    .line 1357
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentManeuver:Lcom/here/android/mpa/routing/Maneuver;

    move-object/from16 p1, v0

    .line 1358
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->maneuverAfterCurrent:Lcom/here/android/mpa/routing/Maneuver;

    move-object/from16 p2, v0

    .line 1359
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->prevManeuver:Lcom/here/android/mpa/routing/Maneuver;

    move-object/from16 p3, v0

    .line 1361
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->maneuverAfterCurrent:Lcom/here/android/mpa/routing/Maneuver;

    if-eqz v2, :cond_4

    .line 1363
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->maneuverAfterCurrent:Lcom/here/android/mpa/routing/Maneuver;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->maneuverAfterCurrent:Lcom/here/android/mpa/routing/Maneuver;

    if-ne v2, v6, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->maneuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->maneuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    sget-object v6, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->STAY:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    if-eq v2, v6, :cond_4

    .line 1366
    const/4 v10, 0x0

    .line 1383
    :cond_4
    :goto_1
    if-eqz p1, :cond_1b

    .line 1384
    const/4 v3, 0x0

    .line 1385
    .local v3, "last":Z
    invoke-virtual/range {p1 .. p1}, Lcom/here/android/mpa/routing/Maneuver;->getAction()Lcom/here/android/mpa/routing/Maneuver$Action;

    move-result-object v2

    sget-object v6, Lcom/here/android/mpa/routing/Maneuver$Action;->END:Lcom/here/android/mpa/routing/Maneuver$Action;

    if-eq v2, v6, :cond_5

    invoke-virtual/range {p1 .. p1}, Lcom/here/android/mpa/routing/Maneuver;->getIcon()Lcom/here/android/mpa/routing/Maneuver$Icon;

    move-result-object v2

    sget-object v6, Lcom/here/android/mpa/routing/Maneuver$Icon;->END:Lcom/here/android/mpa/routing/Maneuver$Icon;

    if-ne v2, v6, :cond_6

    .line 1386
    :cond_5
    const/4 v3, 0x1

    .line 1389
    :cond_6
    const/16 v18, 0x0

    .line 1390
    .local v18, "hasDirectionInfo":Z
    const/4 v13, 0x0

    .line 1392
    .local v13, "calculateDirection":Z
    if-eqz v3, :cond_7

    .line 1394
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destinationDirection:Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;

    if-eqz v2, :cond_16

    .line 1396
    const/16 v18, 0x1

    .line 1402
    :cond_7
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereNavController:Lcom/navdy/hud/app/maps/here/HereNavController;

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavController;->getNextManeuverDistance()J

    move-result-wide v4

    .line 1404
    .local v4, "distance":J
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentManeuver:Lcom/here/android/mpa/routing/Maneuver;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->prevManeuver:Lcom/here/android/mpa/routing/Maneuver;

    if-eqz v13, :cond_17

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v8, v8, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->navigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    :goto_3
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->maneuverAfterCurrent:Lcom/here/android/mpa/routing/Maneuver;

    const/4 v11, 0x1

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v12, v12, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destinationDirection:Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;

    invoke-static/range {v2 .. v12}, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->getManeuverDisplay(Lcom/here/android/mpa/routing/Maneuver;ZJLjava/lang/String;Lcom/here/android/mpa/routing/Maneuver;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/routing/Maneuver;ZZLcom/navdy/hud/app/maps/MapEvents$DestinationDirection;)Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    move-result-object v20

    .line 1417
    .local v20, "maneuverDisplay":Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    if-nez v3, :cond_c

    .line 1418
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->debugManeuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    if-eqz v2, :cond_8

    .line 1419
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->debugManeuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    move-object/from16 v0, v20

    iput-object v2, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->maneuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    .line 1421
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->debugManeuverInstruction:Ljava/lang/String;

    if-eqz v2, :cond_9

    .line 1422
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->debugManeuverInstruction:Ljava/lang/String;

    move-object/from16 v0, v20

    iput-object v2, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingRoad:Ljava/lang/String;

    .line 1424
    :cond_9
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->debugManeuverIcon:I

    if-eqz v2, :cond_a

    .line 1425
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->debugManeuverIcon:I

    move-object/from16 v0, v20

    iput v2, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->turnIconId:I

    .line 1427
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->debugManeuverDistance:Ljava/lang/String;

    if-eqz v2, :cond_b

    .line 1428
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->debugManeuverDistance:Ljava/lang/String;

    move-object/from16 v0, v20

    iput-object v2, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->distanceToPendingRoadText:Ljava/lang/String;

    .line 1430
    :cond_b
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->debugNextManeuverIcon:I

    if-eqz v2, :cond_c

    .line 1431
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->debugNextManeuverIcon:I

    move-object/from16 v0, v20

    iput v2, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->nextTurnIconId:I

    .line 1435
    :cond_c
    if-eqz v3, :cond_d

    .line 1436
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-boolean v2, v2, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->lastManeuver:Z

    if-nez v2, :cond_d

    .line 1437
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "last maneuver marked distance:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1438
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->setLastManeuver()V

    .line 1442
    :cond_d
    if-eqz v3, :cond_e

    .line 1443
    if-eqz v18, :cond_18

    .line 1445
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destinationDirection:Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;

    sget-object v6, Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;->UNKNOWN:Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;

    if-eq v2, v6, :cond_e

    .line 1446
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destinationDirectionStr:Ljava/lang/String;

    move-object/from16 v0, v20

    iput-object v2, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingRoad:Ljava/lang/String;

    .line 1447
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget v2, v2, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destinationIconId:I

    move-object/from16 v0, v20

    iput v2, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->destinationIconId:I

    .line 1466
    :cond_e
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget v2, v2, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->maneuverAfterCurrentIconid:I

    const/4 v6, -0x1

    if-ne v2, v6, :cond_19

    .line 1467
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    move-object/from16 v0, v20

    iget v6, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->nextTurnIconId:I

    iput v6, v2, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->maneuverAfterCurrentIconid:I

    .line 1468
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    move-object/from16 v0, v20

    iget-object v6, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->maneuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    iput-object v6, v2, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->maneuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    .line 1473
    :goto_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereNavController:Lcom/navdy/hud/app/maps/here/HereNavController;

    const/4 v6, 0x1

    sget-object v7, Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;->OPTIMAL:Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    invoke-virtual {v2, v6, v7}, Lcom/navdy/hud/app/maps/here/HereNavController;->getEta(ZLcom/here/android/mpa/routing/Route$TrafficPenaltyMode;)Ljava/util/Date;

    move-result-object v17

    .line 1474
    .local v17, "etaDate":Ljava/util/Date;
    invoke-static/range {v17 .. v17}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->isValidEtaDate(Ljava/util/Date;)Z

    move-result v2

    if-nez v2, :cond_f

    .line 1475
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereNavController:Lcom/navdy/hud/app/maps/here/HereNavController;

    const/4 v6, 0x1

    sget-object v7, Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;->OPTIMAL:Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    invoke-virtual {v2, v6, v7}, Lcom/navdy/hud/app/maps/here/HereNavController;->getTtaDate(ZLcom/here/android/mpa/routing/Route$TrafficPenaltyMode;)Ljava/util/Date;

    move-result-object v17

    .line 1476
    invoke-static/range {v17 .. v17}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->isValidEtaDate(Ljava/util/Date;)Z

    move-result v2

    if-nez v2, :cond_1a

    .line 1477
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "intermediate maneuver eta-tta date invalid = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1478
    const/16 v17, 0x0

    .line 1484
    :cond_f
    :goto_6
    if-eqz v17, :cond_10

    .line 1485
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->timeHelper:Lcom/navdy/hud/app/common/TimeHelper;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->stringBuilder:Ljava/lang/StringBuilder;

    move-object/from16 v0, v17

    invoke-virtual {v2, v0, v6}, Lcom/navdy/hud/app/common/TimeHelper;->formatTime(Ljava/util/Date;Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v20

    iput-object v2, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->eta:Ljava/lang/String;

    .line 1486
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->stringBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v20

    iput-object v2, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->etaAmPm:Ljava/lang/String;

    .line 1487
    move-object/from16 v0, v17

    move-object/from16 v1, v20

    iput-object v0, v1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->etaDate:Ljava/util/Date;

    .line 1490
    :cond_10
    const/16 v19, 0x0

    .line 1491
    .local v19, "isHighway":Z
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getCurrentRoadElement()Lcom/here/android/mpa/common/RoadElement;

    move-result-object v16

    .line 1492
    .local v16, "element":Lcom/here/android/mpa/common/RoadElement;
    if-eqz v16, :cond_11

    .line 1493
    invoke-static/range {v16 .. v16}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->isCurrentRoadHighway(Lcom/here/android/mpa/common/RoadElement;)Z

    move-result v19

    .line 1496
    :cond_11
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereNavController:Lcom/navdy/hud/app/maps/here/HereNavController;

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavController;->getNextManeuverDistance()J

    move-result-wide v6

    move/from16 v0, v19

    invoke-static {v6, v7, v0}, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->getManeuverState(JZ)Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    move-result-object v2

    sget-object v6, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->SOON:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    if-ne v2, v6, :cond_12

    .line 1498
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v6, Lcom/navdy/hud/app/maps/MapEvents$ManeuverSoonEvent;

    move-object/from16 v0, p1

    invoke-direct {v6, v0}, Lcom/navdy/hud/app/maps/MapEvents$ManeuverSoonEvent;-><init>(Lcom/here/android/mpa/routing/Maneuver;)V

    invoke-virtual {v2, v6}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 1500
    :cond_12
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v6, 0x2

    invoke-virtual {v2, v6}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 1501
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ManeuverDisplay-nav:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getHereNavigationState()Lcom/here/android/mpa/guidance/NavigationManager$NavigationMode;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v20 .. v20}, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1502
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[ManeuverDisplay-nav-short] "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v20

    iget-object v7, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingTurn:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v20

    iget-object v7, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingRoad:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1505
    :cond_13
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iput-wide v6, v2, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->lastManeuverPostTime:J

    .line 1506
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->bus:Lcom/squareup/otto/Bus;

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1370
    .end local v3    # "last":Z
    .end local v4    # "distance":J
    .end local v13    # "calculateDirection":Z
    .end local v16    # "element":Lcom/here/android/mpa/common/RoadElement;
    .end local v17    # "etaDate":Ljava/util/Date;
    .end local v18    # "hasDirectionInfo":Z
    .end local v19    # "isHighway":Z
    .end local v20    # "maneuverDisplay":Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    :cond_14
    if-eqz p4, :cond_15

    .line 1371
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "setNewRoute: cleared"

    invoke-virtual {v2, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1372
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->prevManeuver:Lcom/here/android/mpa/routing/Maneuver;

    .line 1376
    :goto_7
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentManeuver:Lcom/here/android/mpa/routing/Maneuver;

    .line 1377
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->maneuverAfterCurrent:Lcom/here/android/mpa/routing/Maneuver;

    .line 1378
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    move-object/from16 v0, p2

    iput-object v0, v2, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->maneuverAfterCurrent:Lcom/here/android/mpa/routing/Maneuver;

    .line 1379
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    const/4 v6, -0x1

    iput v6, v2, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->maneuverAfterCurrentIconid:I

    .line 1380
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    const/4 v6, 0x0

    iput-object v6, v2, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->maneuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    goto/16 :goto_1

    .line 1374
    :cond_15
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentManeuver:Lcom/here/android/mpa/routing/Maneuver;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->prevManeuver:Lcom/here/android/mpa/routing/Maneuver;

    goto :goto_7

    .line 1398
    .restart local v3    # "last":Z
    .restart local v13    # "calculateDirection":Z
    .restart local v18    # "hasDirectionInfo":Z
    :cond_16
    const/4 v13, 0x1

    goto/16 :goto_2

    .line 1404
    .restart local v4    # "distance":J
    :cond_17
    const/4 v8, 0x0

    goto/16 :goto_3

    .line 1450
    .restart local v20    # "maneuverDisplay":Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    :cond_18
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    move-object/from16 v0, v20

    iget-object v6, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->direction:Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;

    iput-object v6, v2, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destinationDirection:Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;

    .line 1451
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    move-object/from16 v0, v20

    iget-object v6, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->pendingRoad:Ljava/lang/String;

    iput-object v6, v2, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destinationDirectionStr:Ljava/lang/String;

    .line 1452
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    move-object/from16 v0, v20

    iget v6, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->destinationIconId:I

    iput v6, v2, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destinationIconId:I

    .line 1455
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destinationDirection:Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;

    sget-object v6, Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;->UNKNOWN:Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;

    if-eq v2, v6, :cond_e

    .line 1456
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "we have destination marker info: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v7, v7, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destinationDirection:Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1457
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->removeDestinationMarker()V

    .line 1458
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    const/4 v6, 0x0

    iput-object v6, v2, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->mapDestinationMarker:Lcom/here/android/mpa/mapping/MapMarker;

    .line 1459
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->route:Lcom/here/android/mpa/routing/Route;

    if-eqz v2, :cond_e

    .line 1460
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->route:Lcom/here/android/mpa/routing/Route;

    invoke-virtual {v2}, Lcom/here/android/mpa/routing/Route;->getDestination()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget v6, v6, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destinationIconId:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v6}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->addDestinationMarker(Lcom/here/android/mpa/common/GeoCoordinate;I)V

    goto/16 :goto_4

    .line 1470
    :cond_19
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget v2, v2, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->maneuverAfterCurrentIconid:I

    move-object/from16 v0, v20

    iput v2, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->nextTurnIconId:I

    goto/16 :goto_5

    .line 1480
    .restart local v17    # "etaDate":Ljava/util/Date;
    :cond_1a
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "intermediate maneuver eta-tta date = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_6

    .line 1508
    .end local v3    # "last":Z
    .end local v4    # "distance":J
    .end local v13    # "calculateDirection":Z
    .end local v17    # "etaDate":Ljava/util/Date;
    .end local v18    # "hasDirectionInfo":Z
    .end local v20    # "maneuverDisplay":Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    :cond_1b
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "there is no next maneuver"

    invoke-virtual {v2, v6}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1510
    :cond_1c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    sget-object v6, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_STOPPED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    if-ne v2, v6, :cond_1e

    .line 1512
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->prevManeuver:Lcom/here/android/mpa/routing/Maneuver;

    .line 1513
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentManeuver:Lcom/here/android/mpa/routing/Maneuver;

    .line 1514
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->maneuverAfterCurrent:Lcom/here/android/mpa/routing/Maneuver;

    .line 1515
    new-instance v20, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    invoke-direct/range {v20 .. v20}, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;-><init>()V

    .line 1516
    .restart local v20    # "maneuverDisplay":Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getCurrentRoadName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v20

    iput-object v2, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->currentRoad:Ljava/lang/String;

    .line 1517
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getCurrentSpeedLimit()F

    move-result v2

    move-object/from16 v0, v20

    iput v2, v0, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->currentSpeedLimit:F

    .line 1518
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v6, 0x2

    invoke-virtual {v2, v6}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 1519
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ManeuverDisplay:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getHereNavigationState()Lcom/here/android/mpa/guidance/NavigationManager$NavigationMode;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v20 .. v20}, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1521
    :cond_1d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->bus:Lcom/squareup/otto/Bus;

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1523
    .end local v20    # "maneuverDisplay":Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    :cond_1e
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->prevManeuver:Lcom/here/android/mpa/routing/Maneuver;

    .line 1524
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentManeuver:Lcom/here/android/mpa/routing/Maneuver;

    .line 1525
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->maneuverAfterCurrent:Lcom/here/android/mpa/routing/Maneuver;

    .line 1526
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Invalid state:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " maneuver="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0
.end method


# virtual methods
.method public declared-synchronized addCurrentRoute(Lcom/here/android/mpa/mapping/MapRoute;)V
    .locals 4
    .param p1, "mapRoute"    # Lcom/here/android/mpa/mapping/MapRoute;

    .prologue
    .line 492
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v1, v1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->mapRoute:Lcom/here/android/mpa/mapping/MapRoute;

    if-eqz v1, :cond_0

    .line 493
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->removeCurrentRoute()Z

    .line 495
    :cond_0
    if-eqz p1, :cond_1

    .line 496
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iput-object p1, v1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->mapRoute:Lcom/here/android/mpa/mapping/MapRoute;

    .line 497
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {v1, p1}, Lcom/navdy/hud/app/maps/here/HereMapController;->addMapObject(Lcom/here/android/mpa/mapping/MapObject;)V

    .line 498
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addCurrentRoute:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 503
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 500
    :catch_0
    move-exception v0

    .line 501
    .local v0, "t":Ljava/lang/Throwable;
    :try_start_1
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 492
    .end local v0    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public addNewArrivalRoute(Ljava/util/ArrayList;)Lcom/here/android/mpa/guidance/NavigationManager$Error;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteResult;",
            ">;)",
            "Lcom/here/android/mpa/guidance/NavigationManager$Error;"
        }
    .end annotation

    .prologue
    .line 1898
    .local p1, "outgoingResults":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/navigation/NavigationRouteResult;>;"
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 1899
    sget-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;->NAV_SESSION_ARRIVAL_REROUTE:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->addNewRoute(Ljava/util/ArrayList;Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)Lcom/here/android/mpa/guidance/NavigationManager$Error;

    move-result-object v0

    return-object v0
.end method

.method public addNewRoute(Ljava/util/ArrayList;Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)Lcom/here/android/mpa/guidance/NavigationManager$Error;
    .locals 6
    .param p2, "reason"    # Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;
    .param p3, "navigationRouteRequest"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteResult;",
            ">;",
            "Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;",
            ")",
            "Lcom/here/android/mpa/guidance/NavigationManager$Error;"
        }
    .end annotation

    .prologue
    .line 1855
    .local p1, "outgoingResults":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/navigation/NavigationRouteResult;>;"
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 1856
    const-wide/16 v4, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->addNewRoute(Ljava/util/ArrayList;Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;J)Lcom/here/android/mpa/guidance/NavigationManager$Error;

    move-result-object v0

    return-object v0
.end method

.method public addNewRoute(Ljava/util/ArrayList;Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;J)Lcom/here/android/mpa/guidance/NavigationManager$Error;
    .locals 14
    .param p2, "reason"    # Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;
    .param p3, "navigationRouteRequest"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .param p4, "simulationSpeed"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteResult;",
            ">;",
            "Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;",
            "J)",
            "Lcom/here/android/mpa/guidance/NavigationManager$Error;"
        }
    .end annotation

    .prologue
    .line 1864
    .local p1, "outgoingResults":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/navigation/NavigationRouteResult;>;"
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 1865
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 1866
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "addNewRoute calculated="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1867
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    iget-object v10, v2, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeId:Ljava/lang/String;

    .line 1868
    .local v10, "id":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getInstance()Lcom/navdy/hud/app/maps/here/HereRouteCache;

    move-result-object v2

    invoke-virtual {v2, v10}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getRoute(Ljava/lang/String;)Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;

    move-result-object v11

    .line 1869
    .local v11, "info":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    if-eqz v11, :cond_1

    .line 1870
    iget-object v3, v11, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->route:Lcom/here/android/mpa/routing/Route;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    iget-boolean v9, v11, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->traffic:Z

    move-object v2, p0

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    invoke-virtual/range {v2 .. v9}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->updateMapRoute(Lcom/here/android/mpa/routing/Route;Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 1874
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "setNavigationMode: set transition mode"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 1875
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereNavController:Lcom/navdy/hud/app/maps/here/HereNavController;

    iget-object v3, v11, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->route:Lcom/here/android/mpa/routing/Route;

    move-wide/from16 v0, p4

    long-to-int v4, v0

    invoke-virtual {v2, v3, v4}, Lcom/navdy/hud/app/maps/here/HereNavController;->startNavigation(Lcom/here/android/mpa/routing/Route;I)Lcom/here/android/mpa/guidance/NavigationManager$Error;

    move-result-object v12

    .line 1877
    .local v12, "navError":Lcom/here/android/mpa/guidance/NavigationManager$Error;
    sget-object v2, Lcom/here/android/mpa/guidance/NavigationManager$Error;->NONE:Lcom/here/android/mpa/guidance/NavigationManager$Error;

    if-ne v12, v2, :cond_0

    .line 1878
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "addNewRoute navigation started"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1893
    .end local v10    # "id":Ljava/lang/String;
    .end local v11    # "info":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    .end local v12    # "navError":Lcom/here/android/mpa/guidance/NavigationManager$Error;
    :goto_0
    return-object v12

    .line 1881
    .restart local v10    # "id":Ljava/lang/String;
    .restart local v11    # "info":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    .restart local v12    # "navError":Lcom/here/android/mpa/guidance/NavigationManager$Error;
    :cond_0
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "addNewRoute navigation started failed:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1882
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->stopNavigation()V

    goto :goto_0

    .line 1886
    .end local v12    # "navError":Lcom/here/android/mpa/guidance/NavigationManager$Error;
    :cond_1
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "addNewRoute calc could not found route:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1887
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->stopNavigation()V

    .line 1888
    sget-object v12, Lcom/here/android/mpa/guidance/NavigationManager$Error;->NOT_FOUND:Lcom/here/android/mpa/guidance/NavigationManager$Error;

    goto :goto_0

    .line 1891
    .end local v10    # "id":Ljava/lang/String;
    .end local v11    # "info":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    :cond_2
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "addNewRoute calc no result, end trip:"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1892
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->stopNavigation()V

    .line 1893
    sget-object v12, Lcom/here/android/mpa/guidance/NavigationManager$Error;->NOT_FOUND:Lcom/here/android/mpa/guidance/NavigationManager$Error;

    goto :goto_0
.end method

.method public arrived()V
    .locals 3

    .prologue
    .line 1759
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager$9;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager$9;-><init>(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)V

    const/16 v2, 0x14

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 1810
    return-void
.end method

.method public clearNavManeuver()V
    .locals 1

    .prologue
    .line 2153
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->newManeuverEventListener:Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->clearNavManeuver()V

    .line 2154
    return-void
.end method

.method public getCurrentDestinationIdentifier()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1674
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destinationIdentifier:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrentMapRoute()Lcom/here/android/mpa/mapping/MapRoute;
    .locals 1

    .prologue
    .line 1554
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->mapRoute:Lcom/here/android/mpa/mapping/MapRoute;

    return-object v0
.end method

.method public getCurrentNavigationDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;
    .locals 1

    .prologue
    .line 1566
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    return-object v0
.end method

.method public getCurrentNavigationMode()Lcom/navdy/hud/app/maps/NavigationMode;
    .locals 1

    .prologue
    .line 1153
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationMode:Lcom/navdy/hud/app/maps/NavigationMode;

    return-object v0
.end method

.method public getCurrentNavigationRouteRequest()Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .locals 1

    .prologue
    .line 1558
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->navigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    return-object v0
.end method

.method public getCurrentNavigationState()Lcom/navdy/service/library/events/navigation/NavigationSessionState;
    .locals 1

    .prologue
    .line 1157
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    return-object v0
.end method

.method public getCurrentRerouteReason()Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;
    .locals 1

    .prologue
    .line 2074
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->currentRerouteReason:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    return-object v0
.end method

.method public getCurrentRoute()Lcom/here/android/mpa/routing/Route;
    .locals 1

    .prologue
    .line 1546
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->route:Lcom/here/android/mpa/routing/Route;

    return-object v0
.end method

.method public getCurrentRouteId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1550
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->routeId:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrentTrafficRerouteCount()I
    .locals 1

    .prologue
    .line 1923
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->getTrafficReRouteCount()I

    move-result v0

    return v0
.end method

.method public getCurrentVia()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2130
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentRouteId()Ljava/lang/String;

    move-result-object v0

    .line 2131
    .local v0, "id":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 2132
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getInstance()Lcom/navdy/hud/app/maps/here/HereRouteCache;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getRoute(Ljava/lang/String;)Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;

    move-result-object v1

    .line 2133
    .local v1, "info":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    if-eqz v1, :cond_0

    iget-object v3, v1, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->routeResult:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    if-eqz v3, :cond_0

    .line 2134
    iget-object v2, v1, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->routeResult:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    iget-object v2, v2, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->via:Ljava/lang/String;

    .line 2139
    .end local v1    # "info":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    :cond_0
    return-object v2
.end method

.method public getDestinationLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1542
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destinationLabel:Ljava/lang/String;

    return-object v0
.end method

.method public getDestinationMarker()Lcom/here/android/mpa/mapping/MapMarker;
    .locals 1

    .prologue
    .line 1562
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->mapDestinationMarker:Lcom/here/android/mpa/mapping/MapMarker;

    return-object v0
.end method

.method public getDestinationStreetAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1538
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->streetAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getFirstManeuverShowntime()J
    .locals 2

    .prologue
    .line 2083
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-wide v0, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->firstManeuverShownTime:J

    return-wide v0
.end method

.method public getFullStreetAddress()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2144
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v0, v1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->navigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .line 2145
    .local v0, "req":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    if-eqz v0, :cond_0

    .line 2146
    iget-object v1, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->streetAddress:Ljava/lang/String;

    .line 2148
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getGeneratePhoneticTTS()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 1678
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->generatePhoneticTTS:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getHereNavigationState()Lcom/here/android/mpa/guidance/NavigationManager$NavigationMode;
    .locals 1

    .prologue
    .line 1915
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 1916
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v0}, Lcom/here/android/mpa/guidance/NavigationManager;->getNavigationMode()Lcom/here/android/mpa/guidance/NavigationManager$NavigationMode;

    move-result-object v0

    return-object v0
.end method

.method public getLastTrafficRerouteTime()J
    .locals 2

    .prologue
    .line 1927
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->getLastTrafficRerouteTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public getNavController()Lcom/navdy/hud/app/maps/here/HereNavController;
    .locals 1

    .prologue
    .line 2008
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereNavController:Lcom/navdy/hud/app/maps/here/HereNavController;

    return-object v0
.end method

.method public getNavManeuver()Lcom/here/android/mpa/routing/Maneuver;
    .locals 1

    .prologue
    .line 2157
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->newManeuverEventListener:Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->getNavManeuver()Lcom/here/android/mpa/routing/Maneuver;

    move-result-object v0

    return-object v0
.end method

.method public getNavigationSessionPreference()Lcom/navdy/hud/app/maps/NavSessionPreferences;
    .locals 1

    .prologue
    .line 1646
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->driverSessionPreferences:Lcom/navdy/hud/app/profile/DriverSessionPreferences;

    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverSessionPreferences;->getNavigationSessionPreference()Lcom/navdy/hud/app/maps/NavSessionPreferences;

    move-result-object v0

    return-object v0
.end method

.method public getNavigationState()Lcom/navdy/hud/app/maps/here/HereNavController$State;
    .locals 1

    .prologue
    .line 1920
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereNavController:Lcom/navdy/hud/app/maps/here/HereNavController;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavController;->getState()Lcom/navdy/hud/app/maps/here/HereNavController$State;

    move-result-object v0

    return-object v0
.end method

.method public getNavigationView()Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;
    .locals 1

    .prologue
    .line 1754
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->navigationView:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    return-object v0
.end method

.method public getNextManeuver()Lcom/here/android/mpa/routing/Maneuver;
    .locals 1

    .prologue
    .line 1962
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isNavigationModeOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1963
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentManeuver:Lcom/here/android/mpa/routing/Maneuver;

    .line 1965
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPrevManeuver()Lcom/here/android/mpa/routing/Maneuver;
    .locals 1

    .prologue
    .line 1970
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isNavigationModeOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1971
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->prevManeuver:Lcom/here/android/mpa/routing/Maneuver;

    .line 1973
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRerouteInterval()I
    .locals 1

    .prologue
    .line 2066
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->bandwidthController:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->isLimitBandwidthModeOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2067
    sget v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->ROUTE_CHECK_INTERVAL_LIMIT_BANDWIDTH:I

    .line 2069
    :goto_0
    return v0

    :cond_0
    sget v0, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->ROUTE_CHECK_INTERVAL:I

    goto :goto_0
.end method

.method public getRouteOptions()Lcom/here/android/mpa/routing/RouteOptions;
    .locals 1

    .prologue
    .line 2090
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->routeOptions:Lcom/here/android/mpa/routing/RouteOptions;

    return-object v0
.end method

.method public getSafetySpotListener()Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;
    .locals 1

    .prologue
    .line 2177
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->safetySpotListener:Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

    return-object v0
.end method

.method public getStartLocation()Lcom/here/android/mpa/common/GeoCoordinate;
    .locals 1

    .prologue
    .line 2173
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->startLocation:Lcom/here/android/mpa/common/GeoCoordinate;

    return-object v0
.end method

.method public handleConnectionState(Z)V
    .locals 4
    .param p1, "connected"    # Z

    .prologue
    .line 1126
    if-nez p1, :cond_1

    .line 1127
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "client not connected"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 1146
    :cond_0
    :goto_0
    return-void

    .line 1129
    :cond_1
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "client connected"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 1131
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isNavigationModeOn()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1132
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v0

    .line 1133
    .local v0, "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v1, v1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/device/NavdyDeviceId;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1134
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager$6;

    invoke-direct {v2, p0, v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager$6;-><init>(Lcom/navdy/hud/app/maps/here/HereNavigationManager;Lcom/navdy/service/library/device/NavdyDeviceId;)V

    const/16 v3, 0x14

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0

    .line 1142
    :cond_2
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "navigation still on, device id has not changed"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public handleNavigationSessionRequest(Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;

    .prologue
    .line 912
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager$5;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager$5;-><init>(Lcom/navdy/hud/app/maps/here/HereNavigationManager;Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;)V

    const/16 v2, 0x14

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 918
    return-void
.end method

.method public hasArrived()Z
    .locals 1

    .prologue
    .line 1813
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isNavigationModeOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1814
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-boolean v0, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->hasArrived:Z

    .line 1816
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTrafficRerouteOnce()Z
    .locals 1

    .prologue
    .line 2078
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-boolean v0, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->trafficRerouteOnce:Z

    return v0
.end method

.method public incrCurrentTrafficRerouteCount()V
    .locals 1

    .prologue
    .line 1925
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->incrTrafficRerouteCount()V

    return-void
.end method

.method public isLastManeuver()Z
    .locals 1

    .prologue
    .line 1847
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-boolean v0, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->lastManeuver:Z

    return v0
.end method

.method public isNavigationModeOn()Z
    .locals 2

    .prologue
    .line 1161
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereNavController:Lcom/navdy/hud/app/maps/here/HereNavController;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavController;->getState()Lcom/navdy/hud/app/maps/here/HereNavController$State;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavController$State;->NAVIGATING:Lcom/navdy/hud/app/maps/here/HereNavController$State;

    if-ne v0, v1, :cond_0

    .line 1162
    const/4 v0, 0x1

    .line 1164
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isOnGasRoute()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1828
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isNavigationModeOn()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1829
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v0, v2, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->navigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .line 1830
    .local v0, "request":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    if-nez v0, :cond_1

    .line 1838
    .end local v0    # "request":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    :cond_0
    :goto_0
    return v1

    .line 1833
    .restart local v0    # "request":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    :cond_1
    iget-object v2, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->routeAttributes:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 1836
    iget-object v1, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->routeAttributes:Ljava/util/List;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;->ROUTE_ATTRIBUTE_GAS:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public isRerouting()Z
    .locals 1

    .prologue
    .line 1843
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->reRouteListener:Lcom/navdy/hud/app/maps/here/HereRerouteListener;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->isRecalculating()Z

    move-result v0

    return v0
.end method

.method public isShownFirstManeuver()Z
    .locals 1

    .prologue
    .line 2081
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-boolean v0, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->firstManeuverShown:Z

    return v0
.end method

.method public isSwitchingToNewRoute()Z
    .locals 1

    .prologue
    .line 2092
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->switchingToNewRoute:Z

    return v0
.end method

.method public isTrafficConsidered(Ljava/lang/String;)Z
    .locals 3
    .param p1, "routeId"    # Ljava/lang/String;

    .prologue
    .line 2162
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getInstance()Lcom/navdy/hud/app/maps/here/HereRouteCache;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/navdy/hud/app/maps/here/HereRouteCache;->getRoute(Ljava/lang/String;)Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;

    move-result-object v0

    .line 2163
    .local v0, "info":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    if-eqz v0, :cond_0

    .line 2164
    iget-boolean v2, v0, Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;->traffic:Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 2169
    .end local v0    # "info":Lcom/navdy/hud/app/maps/here/HereRouteCache$RouteInfo;
    :goto_0
    return v2

    .line 2166
    :catch_0
    move-exception v1

    .line 2167
    .local v1, "t":Ljava/lang/Throwable;
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 2169
    .end local v1    # "t":Ljava/lang/Throwable;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isTrafficUpdaterRunning()Z
    .locals 1

    .prologue
    .line 1955
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->trafficUpdater:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->isRunning()Z

    move-result v0

    return v0
.end method

.method public onDriverProfileChanged(Lcom/navdy/hud/app/event/DriverProfileChanged;)V
    .locals 2
    .param p1, "profileChanged"    # Lcom/navdy/hud/app/event/DriverProfileChanged;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1689
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "driver profile changed"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1690
    return-void
.end method

.method public onNavigationPreferences(Lcom/navdy/service/library/events/preferences/NavigationPreferences;)V
    .locals 2
    .param p1, "preferences"    # Lcom/navdy/service/library/events/preferences/NavigationPreferences;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 434
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v1, p1, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->phoneticTurnByTurn:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->setGeneratePhoneticTTS(Z)V

    .line 435
    return-void
.end method

.method public onNotificationPreference(Lcom/navdy/service/library/events/preferences/NotificationPreferences;)V
    .locals 2
    .param p1, "preferences"    # Lcom/navdy/service/library/events/preferences/NotificationPreferences;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 439
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->enabled:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 440
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->isTrafficNotificationEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 441
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "glances, enabled, turn traffic notif on if not on"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 442
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->startTrafficRerouteListener()V

    .line 453
    :goto_0
    return-void

    .line 444
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "traffic glances disabled, turn traffic notif off"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 445
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->resetTrafficRerouteListener()V

    .line 446
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->stopTrafficRerouteListener()V

    goto :goto_0

    .line 449
    :cond_1
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "glances, disabled, turn traffic notif off"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 450
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->resetTrafficRerouteListener()V

    .line 451
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->stopTrafficRerouteListener()V

    goto :goto_0
.end method

.method public onSpeedUnitChanged(Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnitChanged;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnitChanged;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1702
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "speed unit setting has changed to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->speedManager:Lcom/navdy/hud/app/manager/SpeedManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/SpeedManager;->getSpeedUnit()Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", refreshing"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 1703
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->refreshNavigationInfo()V

    .line 1704
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager$8;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager$8;-><init>(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)V

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 1710
    return-void
.end method

.method public onTimeSettingsChange(Lcom/navdy/hud/app/common/TimeHelper$UpdateClock;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/common/TimeHelper$UpdateClock;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1694
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "time setting changed"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1695
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isNavigationModeOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1696
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->refreshNavigationInfo()V

    .line 1698
    :cond_0
    return-void
.end method

.method public onTrafficRerouteConfirm(Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteAction;)V
    .locals 1
    .param p1, "action"    # Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteAction;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 457
    sget-object v0, Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteAction;->REROUTE:Lcom/navdy/hud/app/maps/MapEvents$TrafficRerouteAction;

    if-ne p1, v0, :cond_0

    .line 458
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->trafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->confirmReroute()V

    .line 462
    :goto_0
    return-void

    .line 460
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->trafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->dismissReroute()V

    goto :goto_0
.end method

.method public postManeuverDisplay()V
    .locals 0

    .prologue
    .line 1958
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->refreshNavigationInfo()V

    .line 1959
    return-void
.end method

.method public postNavigationSessionStatusEvent(Lcom/navdy/service/library/events/navigation/NavigationSessionState;Z)V
    .locals 3
    .param p1, "sessionState"    # Lcom/navdy/service/library/events/navigation/NavigationSessionState;
    .param p2, "sendRouteResult"    # Z

    .prologue
    .line 773
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager$4;

    invoke-direct {v1, p0, p2, p1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager$4;-><init>(Lcom/navdy/hud/app/maps/here/HereNavigationManager;ZLcom/navdy/service/library/events/navigation/NavigationSessionState;)V

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 833
    return-void
.end method

.method public postNavigationSessionStatusEvent(Z)V
    .locals 1
    .param p1, "sendRouteResult"    # Z

    .prologue
    .line 769
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    invoke-virtual {p0, v0, p1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->postNavigationSessionStatusEvent(Lcom/navdy/service/library/events/navigation/NavigationSessionState;Z)V

    .line 770
    return-void
.end method

.method refreshNavigationInfo()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1302
    const/4 v0, 0x0

    invoke-virtual {p0, v1, v1, v1, v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->updateNavigationInfo(Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;Z)V

    .line 1303
    return-void
.end method

.method public declared-synchronized removeCurrentRoute()Z
    .locals 5

    .prologue
    .line 476
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v0, v2, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->mapRoute:Lcom/here/android/mpa/mapping/MapRoute;

    .line 477
    .local v0, "current":Lcom/here/android/mpa/mapping/MapRoute;
    if-eqz v0, :cond_0

    .line 478
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "removed map route:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 479
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    const/4 v3, 0x0

    iput-object v3, v2, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->mapRoute:Lcom/here/android/mpa/mapping/MapRoute;

    .line 480
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {v2, v0}, Lcom/navdy/hud/app/maps/here/HereMapController;->removeMapObject(Lcom/here/android/mpa/mapping/MapObject;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 481
    const/4 v2, 0x1

    .line 487
    .end local v0    # "current":Lcom/here/android/mpa/mapping/MapRoute;
    :goto_0
    monitor-exit p0

    return v2

    .line 483
    :catch_0
    move-exception v1

    .line 484
    .local v1, "t":Ljava/lang/Throwable;
    :try_start_1
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 487
    .end local v1    # "t":Ljava/lang/Throwable;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 476
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public resetTrafficRerouteListener()V
    .locals 2

    .prologue
    .line 1950
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->trafficRerouteLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1951
    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->navdyTrafficRerouteManager:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->reset()V

    .line 1952
    monitor-exit v1

    .line 1953
    return-void

    .line 1952
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected returnResponse(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationSessionState;Ljava/lang/String;)V
    .locals 3
    .param p1, "status"    # Lcom/navdy/service/library/events/RequestStatus;
    .param p2, "errorText"    # Ljava/lang/String;
    .param p3, "pendingState"    # Lcom/navdy/service/library/events/navigation/NavigationSessionState;
    .param p4, "routeId"    # Ljava/lang/String;

    .prologue
    .line 836
    sget-object v0, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    if-eq p1, v0, :cond_0

    .line 837
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 841
    :goto_0
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->mapsEventHandler:Lcom/navdy/hud/app/maps/MapsEventHandler;

    new-instance v1, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;-><init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationSessionState;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/MapsEventHandler;->sendEventToClient(Lcom/squareup/wire/Message;)V

    .line 842
    return-void

    .line 839
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Success"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public sendStartManeuver(Lcom/here/android/mpa/routing/Route;)V
    .locals 7
    .param p1, "route"    # Lcom/here/android/mpa/routing/Route;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 1570
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 1571
    if-nez p1, :cond_0

    .line 1606
    :goto_0
    return-void

    .line 1574
    :cond_0
    invoke-virtual {p1}, Lcom/here/android/mpa/routing/Route;->getManeuvers()Ljava/util/List;

    move-result-object v2

    .line 1575
    .local v2, "maneuvers":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lt v3, v6, :cond_5

    .line 1576
    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/here/android/mpa/routing/Maneuver;

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/here/android/mpa/routing/Maneuver;

    invoke-static {v3, v4}, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->getStartManeuverDisplay(Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;)Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    move-result-object v1

    .line 1577
    .local v1, "display":Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereNavController:Lcom/navdy/hud/app/maps/here/HereNavController;

    sget-object v4, Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;->OPTIMAL:Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    invoke-virtual {v3, v5, v4}, Lcom/navdy/hud/app/maps/here/HereNavController;->getEta(ZLcom/here/android/mpa/routing/Route$TrafficPenaltyMode;)Ljava/util/Date;

    move-result-object v0

    .line 1578
    .local v0, "date":Ljava/util/Date;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->isValidEtaDate(Ljava/util/Date;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 1580
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereNavController:Lcom/navdy/hud/app/maps/here/HereNavController;

    sget-object v4, Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;->OPTIMAL:Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    invoke-virtual {v3, v5, v4}, Lcom/navdy/hud/app/maps/here/HereNavController;->getTtaDate(ZLcom/here/android/mpa/routing/Route$TrafficPenaltyMode;)Ljava/util/Date;

    move-result-object v0

    .line 1581
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->isValidEtaDate(Ljava/util/Date;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1582
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "start maneuver eta-tta date = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1591
    :goto_1
    if-eqz v0, :cond_1

    .line 1592
    iput-object v0, v1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->etaDate:Ljava/util/Date;

    .line 1593
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->timeHelper:Lcom/navdy/hud/app/common/TimeHelper;

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->stringBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0, v4}, Lcom/navdy/hud/app/common/TimeHelper;->formatTime(Ljava/util/Date;Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->eta:Ljava/lang/String;

    .line 1594
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->stringBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->etaAmPm:Ljava/lang/String;

    .line 1597
    :cond_1
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v3, v6}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1598
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ManeuverDisplay:START:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getHereNavigationState()Lcom/here/android/mpa/guidance/NavigationManager$NavigationMode;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1601
    :cond_2
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, v3, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->lastManeuverPostTime:J

    .line 1602
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v3, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1584
    :cond_3
    invoke-static {p1}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getRouteTtaDate(Lcom/here/android/mpa/routing/Route;)Ljava/util/Date;

    move-result-object v0

    .line 1585
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "start maneuver tta date = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_1

    .line 1588
    :cond_4
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "start maneuver eta date = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1604
    .end local v0    # "date":Ljava/util/Date;
    .end local v1    # "display":Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;
    :cond_5
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "start maneuver not sent"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method setBandwidthPreferences()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    .line 2012
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->bandwidthController:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->isLimitBandwidthModeOn()Z

    move-result v0

    .line 2013
    .local v0, "limitBandwidthModeOn":Z
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getDriverProfileManager()Lcom/navdy/hud/app/profile/DriverProfileManager;

    move-result-object v1

    .line 2014
    .local v1, "profileManager":Lcom/navdy/hud/app/profile/DriverProfileManager;
    if-eqz v0, :cond_0

    .line 2015
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "limitbandwidth: on"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2018
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "limitbandwidth: turn map traffic off"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2019
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->clearMapTraffic()V

    .line 2020
    invoke-virtual {v1}, Lcom/navdy/hud/app/profile/DriverProfileManager;->disableTraffic()V

    .line 2023
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "limitbandwidth: turn HERE traffic listener off"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2024
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->trafficRerouteLock:Ljava/lang/Object;

    monitor-enter v3

    .line 2025
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->trafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->stop()V

    .line 2026
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2028
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v2

    new-instance v3, Lcom/navdy/hud/app/maps/here/HereNavigationManager$10;

    invoke-direct {v3, p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager$10;-><init>(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)V

    invoke-virtual {v2, v3, v5}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 2063
    :goto_0
    return-void

    .line 2026
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 2037
    :cond_0
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "limitbandwidth: off"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2040
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "limitbandwidth: turn map traffic on"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2041
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentNavigationMode()Lcom/navdy/hud/app/maps/NavigationMode;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->setTrafficOverlay(Lcom/navdy/hud/app/maps/NavigationMode;)V

    .line 2042
    invoke-virtual {v1}, Lcom/navdy/hud/app/profile/DriverProfileManager;->enableTraffic()V

    .line 2045
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->trafficRerouteLock:Ljava/lang/Object;

    monitor-enter v3

    .line 2046
    :try_start_2
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->isTrafficNotificationEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2047
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "limitbandwidth: turn HERE traffic listener on"

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2048
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->trafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->start()V

    .line 2052
    :goto_1
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2054
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v2

    new-instance v3, Lcom/navdy/hud/app/maps/here/HereNavigationManager$11;

    invoke-direct {v3, p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager$11;-><init>(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)V

    invoke-virtual {v2, v3, v5}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0

    .line 2050
    :cond_1
    :try_start_3
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "limitbandwidth: HERE traffic listener off, not enabled"

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_1

    .line 2052
    :catchall_1
    move-exception v2

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v2
.end method

.method public setDebugManeuverDistance(Ljava/lang/String;)V
    .locals 3
    .param p1, "distance"    # Ljava/lang/String;

    .prologue
    .line 2118
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "debugManeuverDistance:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2119
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->debugManeuverDistance:Ljava/lang/String;

    .line 2120
    return-void
.end method

.method public setDebugManeuverIcon(I)V
    .locals 3
    .param p1, "icon"    # I

    .prologue
    .line 2113
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "debugManeuverIcon:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2114
    iput p1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->debugManeuverIcon:I

    .line 2115
    return-void
.end method

.method public setDebugManeuverInstruction(Ljava/lang/String;)V
    .locals 3
    .param p1, "instruction"    # Ljava/lang/String;

    .prologue
    .line 2100
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "debugManeuverInstruction:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2101
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->debugManeuverInstruction:Ljava/lang/String;

    .line 2102
    return-void
.end method

.method public setDebugManeuverState(Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;)V
    .locals 3
    .param p1, "state"    # Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    .prologue
    .line 2095
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "debugManeuverState:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2096
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->debugManeuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    .line 2097
    return-void
.end method

.method public setDebugNextManeuverIcon(I)V
    .locals 3
    .param p1, "icon"    # I

    .prologue
    .line 2105
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "debugNextManeuverIcon:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2106
    if-nez p1, :cond_0

    .line 2107
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    const/4 v1, -0x1

    iput v1, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->maneuverAfterCurrentIconid:I

    .line 2109
    :cond_0
    iput p1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->debugNextManeuverIcon:I

    .line 2110
    return-void
.end method

.method public setIgnoreArrived(Z)V
    .locals 3
    .param p1, "b"    # Z

    .prologue
    .line 1821
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isNavigationModeOn()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-boolean v0, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->hasArrived:Z

    if-eqz v0, :cond_0

    .line 1822
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ignore arrived:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1823
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iput-boolean p1, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->ignoreArrived:Z

    .line 1825
    :cond_0
    return-void
.end method

.method public setLastManeuver()V
    .locals 2

    .prologue
    .line 1850
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->lastManeuver:Z

    return-void
.end method

.method public setLastTrafficRerouteTime(J)V
    .locals 1
    .param p1, "l"    # J

    .prologue
    .line 1929
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    invoke-virtual {v0, p1, p2}, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->setLastTrafficRerouteTime(J)V

    return-void
.end method

.method public declared-synchronized setMapUpdateMode()V
    .locals 4

    .prologue
    .line 1531
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 1532
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getHereMapUpdateMode()Lcom/here/android/mpa/guidance/NavigationManager$MapUpdateMode;

    move-result-object v0

    .line 1533
    .local v0, "mapUpdateMode":Lcom/here/android/mpa/guidance/NavigationManager$MapUpdateMode;
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setting here map update mode to["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1534
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v1, v0}, Lcom/here/android/mpa/guidance/NavigationManager;->setMapUpdateMode(Lcom/here/android/mpa/guidance/NavigationManager$MapUpdateMode;)Lcom/here/android/mpa/guidance/NavigationManager$Error;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1535
    monitor-exit p0

    return-void

    .line 1531
    .end local v0    # "mapUpdateMode":Lcom/here/android/mpa/guidance/NavigationManager$MapUpdateMode;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public setMapView(Lcom/here/android/mpa/mapping/MapView;Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;)V
    .locals 0
    .param p1, "mapView"    # Lcom/here/android/mpa/mapping/MapView;
    .param p2, "navigationView"    # Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    .prologue
    .line 425
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->mapView:Lcom/here/android/mpa/mapping/MapView;

    .line 426
    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->navigationView:Lcom/navdy/hud/app/ui/component/homescreen/NavigationView;

    .line 427
    return-void
.end method

.method declared-synchronized setNavigationMode(Lcom/navdy/hud/app/maps/NavigationMode;Lcom/navdy/hud/app/maps/here/HereNavigationInfo;Ljava/lang/StringBuilder;)Z
    .locals 21
    .param p1, "navigationMode"    # Lcom/navdy/hud/app/maps/NavigationMode;
    .param p2, "navigationInfo"    # Lcom/navdy/hud/app/maps/here/HereNavigationInfo;
    .param p3, "error"    # Ljava/lang/StringBuilder;

    .prologue
    .line 1171
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 1172
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setNavigationMode:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 1173
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationMode:Lcom/navdy/hud/app/maps/NavigationMode;

    move-object/from16 v0, p1

    if-ne v2, v0, :cond_0

    .line 1174
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setNavigationMode: already in "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1175
    const/4 v2, 0x1

    .line 1298
    :goto_0
    monitor-exit p0

    return v2

    .line 1177
    :cond_0
    if-nez p2, :cond_2

    :try_start_1
    sget-object v2, Lcom/navdy/hud/app/maps/NavigationMode;->MAP:Lcom/navdy/hud/app/maps/NavigationMode;

    move-object/from16 v0, p1

    if-eq v0, v2, :cond_2

    .line 1178
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "setNavigationMode: navigation info cannot be null"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1179
    if-eqz p3, :cond_1

    .line 1180
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->context:Landroid/content/Context;

    const v3, 0x7f09018c

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1182
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 1184
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    move-object/from16 v19, v0

    .line 1185
    .local v19, "prevState":Lcom/navdy/service/library/events/navigation/NavigationSessionState;
    const/16 v16, 0x1

    .line 1186
    .local v16, "changeNavigation":Z
    const/16 v20, 0x0

    .line 1188
    .local v20, "sendStartManeuver":Z
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationMode:Lcom/navdy/hud/app/maps/NavigationMode;

    if-eqz v2, :cond_5

    .line 1189
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationMode:Lcom/navdy/hud/app/maps/NavigationMode;

    sget-object v3, Lcom/navdy/hud/app/maps/NavigationMode;->MAP_ON_ROUTE:Lcom/navdy/hud/app/maps/NavigationMode;

    if-ne v2, v3, :cond_3

    sget-object v2, Lcom/navdy/hud/app/maps/NavigationMode;->TBT_ON_ROUTE:Lcom/navdy/hud/app/maps/NavigationMode;

    move-object/from16 v0, p1

    if-eq v0, v2, :cond_4

    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationMode:Lcom/navdy/hud/app/maps/NavigationMode;

    sget-object v3, Lcom/navdy/hud/app/maps/NavigationMode;->TBT_ON_ROUTE:Lcom/navdy/hud/app/maps/NavigationMode;

    if-ne v2, v3, :cond_9

    sget-object v2, Lcom/navdy/hud/app/maps/NavigationMode;->MAP_ON_ROUTE:Lcom/navdy/hud/app/maps/NavigationMode;

    move-object/from16 v0, p1

    if-ne v0, v2, :cond_9

    .line 1192
    :cond_4
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "navigation switch not reqd from= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationMode:Lcom/navdy/hud/app/maps/NavigationMode;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1193
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationMode:Lcom/navdy/hud/app/maps/NavigationMode;

    .line 1194
    const/16 v16, 0x0

    .line 1213
    :cond_5
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->handler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->etaCalcRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1214
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->clear()V

    .line 1215
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->debugManeuverState:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    .line 1217
    if-eqz v16, :cond_6

    .line 1219
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager$12;->$SwitchMap$com$navdy$hud$app$maps$NavigationMode:[I

    invoke-virtual/range {p1 .. p1}, Lcom/navdy/hud/app/maps/NavigationMode;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1288
    :cond_6
    :goto_2
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->setTrafficOverlay()V

    .line 1289
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v3, Lcom/navdy/hud/app/maps/MapEvents$NavigationModeChange;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationMode:Lcom/navdy/hud/app/maps/NavigationMode;

    invoke-direct {v3, v4}, Lcom/navdy/hud/app/maps/MapEvents$NavigationModeChange;-><init>(Lcom/navdy/hud/app/maps/NavigationMode;)V

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 1291
    if-eqz v20, :cond_7

    .line 1292
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "send start maneuver"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1293
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->route:Lcom/here/android/mpa/routing/Route;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sendStartManeuver(Lcom/here/android/mpa/routing/Route;)V

    .line 1295
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    move-object/from16 v0, v19

    if-eq v0, v2, :cond_8

    .line 1296
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->postNavigationSessionStatusEvent(Z)V

    .line 1298
    :cond_8
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1196
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isNavigationModeOn()Z

    move-result v17

    .line 1197
    .local v17, "isNavOn":Z
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "remove route before calling nav:stop navon="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1198
    if-eqz v17, :cond_a

    .line 1199
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "navigation stopped from:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1200
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v8}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->updateMapRoute(Lcom/here/android/mpa/routing/Route;Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 1201
    sget-object v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->removeRouteInfo(Lcom/navdy/service/library/log/Logger;Z)V

    .line 1203
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereNavController:Lcom/navdy/hud/app/maps/here/HereNavController;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/maps/here/HereNavController;->stopNavigation(Z)V

    .line 1204
    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_STOPPED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 1205
    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_STOPPED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    move-object/from16 v0, v19

    if-eq v0, v2, :cond_b

    .line 1206
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->postNavigationSessionStatusEvent(Z)V

    .line 1208
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    move-object/from16 v19, v0

    goto/16 :goto_1

    .line 1221
    .end local v17    # "isNavOn":Z
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereNavController:Lcom/navdy/hud/app/maps/here/HereNavController;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/maps/here/HereNavController;->stopNavigation(Z)V

    .line 1224
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v3, Lcom/navdy/hud/app/maps/MapEvents$GPSSpeedEvent;

    invoke-direct {v3}, Lcom/navdy/hud/app/maps/MapEvents$GPSSpeedEvent;-><init>()V

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 1226
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isUserBuild()Z

    move-result v2

    if-nez v2, :cond_c

    .line 1227
    invoke-static {}, Lcom/navdy/hud/app/maps/MapSettings;->isLaneGuidanceEnabled()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1228
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->laneInfoListener:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->stop()V

    .line 1232
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereRealisticViewListener:Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;->stop()V

    .line 1233
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->copy(Lcom/navdy/hud/app/maps/here/HereNavigationInfo;)V

    .line 1234
    sget-object v2, Lcom/navdy/hud/app/maps/NavigationMode;->MAP:Lcom/navdy/hud/app/maps/NavigationMode;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationMode:Lcom/navdy/hud/app/maps/NavigationMode;

    .line 1235
    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_STOPPED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 1237
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    sget-object v3, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getTrackingMapScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/maps/here/HereMapController;->setMapScheme(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_2

    .line 1171
    .end local v16    # "changeNavigation":Z
    .end local v19    # "prevState":Lcom/navdy/service/library/events/navigation/NavigationSessionState;
    .end local v20    # "sendStartManeuver":Z
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 1244
    .restart local v16    # "changeNavigation":Z
    .restart local v19    # "prevState":Lcom/navdy/service/library/events/navigation/NavigationSessionState;
    .restart local v20    # "sendStartManeuver":Z
    :pswitch_1
    :try_start_2
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->routeId:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isTrafficConsidered(Ljava/lang/String;)Z

    move-result v8

    .line 1245
    .local v8, "trafficIncluded":Z
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->route:Lcom/here/android/mpa/routing/Route;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v8}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->updateMapRoute(Lcom/here/android/mpa/routing/Route;Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 1246
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereNavController:Lcom/navdy/hud/app/maps/here/HereNavController;

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->route:Lcom/here/android/mpa/routing/Route;

    move-object/from16 v0, p2

    iget v4, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->simulationSpeed:I

    invoke-virtual {v2, v3, v4}, Lcom/navdy/hud/app/maps/here/HereNavController;->startNavigation(Lcom/here/android/mpa/routing/Route;I)Lcom/here/android/mpa/guidance/NavigationManager$Error;

    move-result-object v18

    .line 1248
    .local v18, "navError":Lcom/here/android/mpa/guidance/NavigationManager$Error;
    sget-object v2, Lcom/here/android/mpa/guidance/NavigationManager$Error;->NONE:Lcom/here/android/mpa/guidance/NavigationManager$Error;

    move-object/from16 v0, v18

    if-ne v0, v2, :cond_10

    .line 1249
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "switched navigation mode from:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationMode:Lcom/navdy/hud/app/maps/NavigationMode;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " to="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " simulating="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p2

    iget v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->simulationSpeed:I

    if-lez v2, :cond_e

    const-string v2, "true"

    :goto_3
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1250
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->copy(Lcom/navdy/hud/app/maps/here/HereNavigationInfo;)V

    .line 1251
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationMode:Lcom/navdy/hud/app/maps/NavigationMode;

    .line 1252
    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_STARTED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 1254
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isUserBuild()Z

    move-result v2

    if-nez v2, :cond_d

    .line 1255
    invoke-static {}, Lcom/navdy/hud/app/maps/MapSettings;->isLaneGuidanceEnabled()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1256
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->laneInfoListener:Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereLaneInfoListener;->start()V

    .line 1260
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereRealisticViewListener:Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereRealisticViewListener;->start()V

    .line 1261
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    sget-object v3, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-virtual {v3}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getEnrouteMapScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/maps/here/HereMapController;->setMapScheme(Ljava/lang/String;)V

    .line 1263
    const/16 v20, 0x1

    .line 1264
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->handler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->etaCalcRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x7530

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1266
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->navigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v2, v2, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->routeAttributes:Ljava/util/List;

    sget-object v3, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;->ROUTE_ATTRIBUTE_GAS:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;

    .line 1267
    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 1268
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->navigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    sget-object v4, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-static {v2, v3, v4}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->saveGasRouteInfo(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/navdy/service/library/device/NavdyDeviceId;Lcom/navdy/service/library/log/Logger;)V

    goto/16 :goto_2

    .line 1249
    :cond_e
    const-string v2, "false"

    goto :goto_3

    .line 1270
    :cond_f
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->navigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    sget-object v4, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-static {v2, v3, v4}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->saveRouteInfo(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/navdy/service/library/device/NavdyDeviceId;Lcom/navdy/service/library/log/Logger;)V

    goto/16 :goto_2

    .line 1274
    :cond_10
    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object/from16 v9, p0

    invoke-virtual/range {v9 .. v15}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->updateMapRoute(Lcom/here/android/mpa/routing/Route;Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 1275
    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_ERROR:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 1276
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cannot switch navigation mode from:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationMode:Lcom/navdy/hud/app/maps/NavigationMode;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " to="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " error="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " simulating="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p2

    iget v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->simulationSpeed:I

    if-lez v2, :cond_12

    const-string v2, "true"

    :goto_4
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1277
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->postNavigationSessionStatusEvent(Z)V

    .line 1278
    if-eqz p3, :cond_11

    .line 1279
    invoke-virtual/range {v18 .. v18}, Lcom/here/android/mpa/guidance/NavigationManager$Error;->name()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1281
    :cond_11
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1276
    :cond_12
    const-string v2, "false"
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4

    .line 1219
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public declared-synchronized setReroute(Lcom/here/android/mpa/routing/Route;Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 4
    .param p1, "route"    # Lcom/here/android/mpa/routing/Route;
    .param p2, "rerouteReason"    # Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;
    .param p3, "routeId"    # Ljava/lang/String;
    .param p4, "via"    # Ljava/lang/String;
    .param p5, "sendStartManeuver"    # Z
    .param p6, "trafficConsidered"    # Z

    .prologue
    .line 752
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setReroute:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 753
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isNavigationModeOn()Z

    move-result v1

    if-nez v1, :cond_0

    .line 754
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "setReroute: nav mode ended"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 766
    :goto_0
    monitor-exit p0

    return-void

    .line 758
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->clearNavManeuver()V

    .line 759
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereNavController:Lcom/navdy/hud/app/maps/here/HereNavController;

    invoke-virtual {v1, p1}, Lcom/navdy/hud/app/maps/here/HereNavController;->setRoute(Lcom/here/android/mpa/routing/Route;)Lcom/here/android/mpa/guidance/NavigationManager$Error;

    move-result-object v0

    .line 760
    .local v0, "error":Lcom/here/android/mpa/guidance/NavigationManager$Error;
    sget-object v1, Lcom/here/android/mpa/guidance/NavigationManager$Error;->NONE:Lcom/here/android/mpa/guidance/NavigationManager$Error;

    if-ne v0, v1, :cond_1

    .line 761
    invoke-virtual/range {p0 .. p6}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->updateMapRoute(Lcom/here/android/mpa/routing/Route;Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 762
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "setReroute: done"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 752
    .end local v0    # "error":Lcom/here/android/mpa/guidance/NavigationManager$Error;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 764
    .restart local v0    # "error":Lcom/here/android/mpa/guidance/NavigationManager$Error;
    :cond_1
    :try_start_2
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setReroute: route could not be set:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public setShownFirstManeuver(Z)V
    .locals 4
    .param p1, "b"    # Z

    .prologue
    .line 2086
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iput-boolean p1, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->firstManeuverShown:Z

    .line 2087
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->firstManeuverShownTime:J

    .line 2088
    return-void
.end method

.method public setTrafficToMode()V
    .locals 7

    .prologue
    .line 1978
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 1980
    :try_start_0
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {v4}, Lcom/navdy/hud/app/maps/here/HereMapController;->getState()Lcom/navdy/hud/app/maps/here/HereMapController$State;

    move-result-object v2

    .line 1981
    .local v2, "state":Lcom/navdy/hud/app/maps/here/HereMapController$State;
    sget-object v4, Lcom/navdy/hud/app/maps/here/HereNavigationManager$12;->$SwitchMap$com$navdy$hud$app$maps$here$HereMapController$State:[I

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereMapController$State;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 1999
    sget-object v4, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setTrafficToMode: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "::  no-op"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2005
    .end local v2    # "state":Lcom/navdy/hud/app/maps/here/HereMapController$State;
    :cond_0
    :goto_0
    return-void

    .line 1983
    .restart local v2    # "state":Lcom/navdy/hud/app/maps/here/HereMapController$State;
    :pswitch_0
    sget-object v4, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setTrafficToMode: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1984
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getDriverProfileManager()Lcom/navdy/hud/app/profile/DriverProfileManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/profile/DriverProfileManager;->isTrafficEnabled()Z

    move-result v0

    .line 1985
    .local v0, "isTrafficEnabled":Z
    if-nez v0, :cond_1

    .line 1986
    sget-object v4, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "setTrafficToMode: traffic is not enabled"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2002
    .end local v0    # "isTrafficEnabled":Z
    .end local v2    # "state":Lcom/navdy/hud/app/maps/here/HereMapController$State;
    :catch_0
    move-exception v3

    .line 2003
    .local v3, "t":Ljava/lang/Throwable;
    sget-object v4, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v4, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1989
    .end local v3    # "t":Ljava/lang/Throwable;
    .restart local v0    # "isTrafficEnabled":Z
    .restart local v2    # "state":Lcom/navdy/hud/app/maps/here/HereMapController$State;
    :cond_1
    :try_start_1
    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/maps/here/HereMapController;->setTrafficInfoVisible(Z)V

    .line 1990
    sget-object v4, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "setTrafficToMode: map traffic enabled"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1991
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentMapRoute()Lcom/here/android/mpa/mapping/MapRoute;

    move-result-object v1

    .line 1992
    .local v1, "mapRoute":Lcom/here/android/mpa/mapping/MapRoute;
    if-eqz v1, :cond_0

    .line 1993
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Lcom/here/android/mpa/mapping/MapRoute;->setTrafficEnabled(Z)Lcom/here/android/mpa/mapping/MapRoute;

    .line 1994
    sget-object v4, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "setTrafficToMode: route traffic enabled"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1981
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public startTrafficRerouteListener()V
    .locals 3

    .prologue
    .line 1932
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->trafficRerouteLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1933
    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->navdyTrafficRerouteManager:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->start()V

    .line 1934
    invoke-static {}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->getInstance()Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->isLimitBandwidthModeOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1935
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "startTrafficRerouteListener: limit b/w not starting here traffic reroute"

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1939
    :goto_0
    monitor-exit v1

    .line 1940
    return-void

    .line 1937
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->trafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->start()V

    goto :goto_0

    .line 1939
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method startTrafficUpdater()V
    .locals 1

    .prologue
    .line 2123
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->trafficUpdater:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->trafficUpdaterStarted:Z

    if-nez v0, :cond_0

    .line 2124
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->trafficUpdaterStarted:Z

    .line 2125
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->trafficUpdater:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->start()V

    .line 2127
    :cond_0
    return-void
.end method

.method public stopAndRemoveAllRoutes()V
    .locals 2

    .prologue
    .line 1650
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "stopAndRemoveAllRoutes"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1651
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 1652
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->removeRouteInfo(Lcom/navdy/service/library/log/Logger;Z)V

    .line 1653
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->stopNavigation()V

    .line 1654
    invoke-static {}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordNavigationCancelled()V

    .line 1655
    return-void
.end method

.method public stopNavigation()V
    .locals 7

    .prologue
    .line 1658
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isNavigationModeOn()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1659
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "stopNavigation: nav mode is on"

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1660
    const/4 v2, 0x0

    .line 1661
    .local v2, "label":Ljava/lang/String;
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v6, v1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->navigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .line 1662
    .local v6, "navRequest":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    if-eqz v6, :cond_0

    .line 1663
    iget-object v2, v6, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->label:Ljava/lang/String;

    .line 1665
    :cond_0
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_STOPPED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v3, v3, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->routeId:Ljava/lang/String;

    const/4 v4, 0x0

    .line 1666
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;-><init>(Lcom/navdy/service/library/events/navigation/NavigationSessionState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;)V

    .line 1667
    .local v0, "request":Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->handleNavigationSessionRequest(Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;)V

    .line 1671
    .end local v0    # "request":Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;
    .end local v2    # "label":Ljava/lang/String;
    .end local v6    # "navRequest":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    :goto_0
    return-void

    .line 1669
    :cond_1
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "stopNavigation: nav mode not on"

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public stopTrafficRerouteListener()V
    .locals 2

    .prologue
    .line 1943
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->trafficRerouteLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1944
    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->navdyTrafficRerouteManager:Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/NavdyTrafficRerouteManager;->stop()V

    .line 1945
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->trafficRerouteListener:Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereTrafficRerouteListener;->stop()V

    .line 1946
    monitor-exit v1

    .line 1947
    return-void

    .line 1946
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public declared-synchronized updateMapRoute(Lcom/here/android/mpa/routing/Route;Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 12
    .param p1, "newRoute"    # Lcom/here/android/mpa/routing/Route;
    .param p2, "reason"    # Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;
    .param p3, "navigationRouteRequest"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .param p4, "routeId"    # Ljava/lang/String;
    .param p5, "viaStr"    # Ljava/lang/String;
    .param p6, "sendStartManeuver"    # Z
    .param p7, "includedTraffic"    # Z

    .prologue
    .line 627
    monitor-enter p0

    const/4 v10, 0x0

    .line 629
    .local v10, "updateDisplay":Z
    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->mapRoute:Lcom/here/android/mpa/mapping/MapRoute;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->mapDestinationMarker:Lcom/here/android/mpa/mapping/MapMarker;

    if-eqz v0, :cond_2

    .line 630
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->tearDownCurrentRoute()V

    .line 631
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destinationDirection:Lcom/navdy/hud/app/maps/MapEvents$DestinationDirection;

    .line 632
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destinationDirectionStr:Ljava/lang/String;

    .line 633
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    const/4 v1, -0x1

    iput v1, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destinationIconId:I

    .line 635
    if-eqz p1, :cond_1

    if-nez p2, :cond_2

    .line 636
    :cond_1
    const/4 v10, 0x1

    .line 637
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->hasArrived:Z

    .line 638
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->ignoreArrived:Z

    .line 639
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->lastManeuver:Z

    .line 640
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->arrivedManeuverDisplay:Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;

    .line 644
    :cond_2
    if-eqz p1, :cond_9

    .line 645
    new-instance v9, Lcom/here/android/mpa/mapping/MapRoute;

    invoke-direct {v9, p1}, Lcom/here/android/mpa/mapping/MapRoute;-><init>(Lcom/here/android/mpa/routing/Route;)V

    .line 646
    .local v9, "mapRoute":Lcom/here/android/mpa/mapping/MapRoute;
    const/4 v0, 0x1

    invoke-virtual {v9, v0}, Lcom/here/android/mpa/mapping/MapRoute;->setTrafficEnabled(Z)Lcom/here/android/mpa/mapping/MapRoute;

    .line 647
    invoke-virtual {p0, v9}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->addCurrentRoute(Lcom/here/android/mpa/mapping/MapRoute;)V

    .line 648
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v0, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->mapRoute:Lcom/here/android/mpa/mapping/MapRoute;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/mapping/MapRoute;->setTrafficEnabled(Z)Lcom/here/android/mpa/mapping/MapRoute;

    .line 649
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/maps/MapEvents$NewRouteAdded;

    invoke-direct {v1, p2}, Lcom/navdy/hud/app/maps/MapEvents$NewRouteAdded;-><init>(Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 650
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->newManeuverEventListener:Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNewManeuverListener;->setNewRoute()V

    .line 651
    if-eqz p6, :cond_3

    .line 652
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sendStartManeuver(Lcom/here/android/mpa/routing/Route;)V

    .line 653
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "updateMapRoute:sending start maneuver"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 655
    :cond_3
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "map route added:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v2, v2, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->mapRoute:Lcom/here/android/mpa/mapping/MapRoute;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 657
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->removeDestinationMarker()V

    .line 658
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->mapDestinationMarker:Lcom/here/android/mpa/mapping/MapMarker;

    .line 659
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iput-object p2, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->currentRerouteReason:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    .line 661
    invoke-virtual {p1}, Lcom/here/android/mpa/routing/Route;->getDestination()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v0

    const v1, 0x7f0201a4

    invoke-direct {p0, v0, v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->addDestinationMarker(Lcom/here/android/mpa/common/GeoCoordinate;I)V

    .line 663
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->trafficUpdater:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v1, v1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->mapRoute:Lcom/here/android/mpa/mapping/MapRoute;

    invoke-virtual {v1}, Lcom/here/android/mpa/mapping/MapRoute;->getRoute()Lcom/here/android/mpa/routing/Route;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->setRoute(Lcom/here/android/mpa/routing/Route;)V

    .line 665
    if-eqz p2, :cond_5

    .line 667
    if-eqz p4, :cond_7

    move-object/from16 v4, p4

    .line 668
    .local v4, "id":Ljava/lang/String;
    :goto_0
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v6, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->routeId:Ljava/lang/String;

    .line 669
    .local v6, "oldRouteId":Ljava/lang/String;
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v8, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->navigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .line 671
    .local v8, "routeRequest":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    sget-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;->NAV_SESSION_FUEL_REROUTE:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    if-ne p2, v0, :cond_8

    .line 672
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iput-object p3, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->navigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .line 673
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getRouteOptions()Lcom/here/android/mpa/routing/RouteOptions;

    move-result-object v1

    iput-object v1, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->routeOptions:Lcom/here/android/mpa/routing/RouteOptions;

    .line 674
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v1, p3, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->streetAddress:Ljava/lang/String;

    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->parseStreetAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->streetAddress:Ljava/lang/String;

    .line 675
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->destinationLabel:Ljava/lang/String;

    .line 681
    :cond_4
    :goto_1
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v11

    new-instance v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$3;

    move-object v1, p0

    move-object/from16 v2, p5

    move-object v3, p1

    move/from16 v5, p7

    move-object v7, p2

    invoke-direct/range {v0 .. v8}, Lcom/navdy/hud/app/maps/here/HereNavigationManager$3;-><init>(Lcom/navdy/hud/app/maps/here/HereNavigationManager;Ljava/lang/String;Lcom/here/android/mpa/routing/Route;Ljava/lang/String;ZLjava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V

    const/4 v1, 0x2

    invoke-virtual {v11, v0, v1}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 741
    .end local v4    # "id":Ljava/lang/String;
    .end local v6    # "oldRouteId":Ljava/lang/String;
    .end local v8    # "routeRequest":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .end local v9    # "mapRoute":Lcom/here/android/mpa/mapping/MapRoute;
    :cond_5
    :goto_2
    if-eqz v10, :cond_6

    .line 742
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->refreshNavigationInfo()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 744
    :cond_6
    monitor-exit p0

    return-void

    .line 667
    .restart local v9    # "mapRoute":Lcom/here/android/mpa/mapping/MapRoute;
    :cond_7
    :try_start_1
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 676
    .restart local v4    # "id":Ljava/lang/String;
    .restart local v6    # "oldRouteId":Ljava/lang/String;
    .restart local v8    # "routeRequest":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    :cond_8
    sget-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;->NAV_SESSION_TRAFFIC_REROUTE:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    if-ne p2, v0, :cond_4

    .line 677
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->trafficRerouteOnce:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 627
    .end local v4    # "id":Ljava/lang/String;
    .end local v6    # "oldRouteId":Ljava/lang/String;
    .end local v8    # "routeRequest":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .end local v9    # "mapRoute":Lcom/here/android/mpa/mapping/MapRoute;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 738
    :cond_9
    :try_start_2
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->addMarkers(Lcom/navdy/hud/app/maps/here/HereMapController;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method public updateMapRoute(Lcom/here/android/mpa/routing/Route;Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 8
    .param p1, "newRoute"    # Lcom/here/android/mpa/routing/Route;
    .param p2, "reason"    # Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;
    .param p3, "routeId"    # Ljava/lang/String;
    .param p4, "via"    # Ljava/lang/String;
    .param p5, "sendStartManeuver"    # Z
    .param p6, "includedTraffic"    # Z

    .prologue
    .line 616
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    iget-object v3, v0, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->navigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    move v7, p6

    invoke-virtual/range {v0 .. v7}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->updateMapRoute(Lcom/here/android/mpa/routing/Route;Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 617
    return-void
.end method

.method updateNavigationInfo(Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;Z)V
    .locals 7
    .param p1, "nextManeuver"    # Lcom/here/android/mpa/routing/Maneuver;
    .param p2, "maneuverAfterNext"    # Lcom/here/android/mpa/routing/Maneuver;
    .param p3, "previous"    # Lcom/here/android/mpa/routing/Maneuver;
    .param p4, "clearPrevious"    # Z

    .prologue
    .line 1309
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v6

    new-instance v0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$7;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/maps/here/HereNavigationManager$7;-><init>(Lcom/navdy/hud/app/maps/here/HereNavigationManager;Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;Z)V

    const/4 v1, 0x4

    invoke-virtual {v6, v0, v1}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 1319
    return-void
.end method
