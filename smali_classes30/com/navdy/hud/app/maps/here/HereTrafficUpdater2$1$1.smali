.class Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1$1;
.super Ljava/lang/Object;
.source "HereTrafficUpdater2.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;->onStatusChanged(Lcom/here/android/mpa/guidance/TrafficUpdater$RequestState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;

    .prologue
    .line 108
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    .line 111
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 114
    .local v4, "l1":J
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v2

    .line 115
    .local v2, "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isNavigationModeOn()Z

    move-result v10

    if-nez v10, :cond_0

    .line 116
    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v10

    const-string v11, "nav is off"

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 141
    .end local v2    # "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    :goto_0
    return-void

    .line 119
    .restart local v2    # "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    :cond_0
    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getNextManeuver()Lcom/here/android/mpa/routing/Maneuver;

    move-result-object v3

    .line 120
    .local v3, "next":Lcom/here/android/mpa/routing/Maneuver;
    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getPrevManeuver()Lcom/here/android/mpa/routing/Maneuver;

    move-result-object v8

    .line 121
    .local v8, "prev":Lcom/here/android/mpa/routing/Maneuver;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getCurrentRoadElement()Lcom/here/android/mpa/common/RoadElement;

    move-result-object v1

    .line 122
    .local v1, "cre":Lcom/here/android/mpa/common/RoadElement;
    iget-object v10, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;

    iget-object v10, v10, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->route:Lcom/here/android/mpa/routing/Route;
    invoke-static {v10}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$100(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;)Lcom/here/android/mpa/routing/Route;

    move-result-object v10

    invoke-static {v10, v1, v8, v3}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getAheadRouteElements(Lcom/here/android/mpa/routing/Route;Lcom/here/android/mpa/common/RoadElement;Lcom/here/android/mpa/routing/Maneuver;Lcom/here/android/mpa/routing/Maneuver;)Ljava/util/List;

    move-result-object v0

    .line 123
    .local v0, "aheadRouteElements":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/RouteElement;>;"
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v10

    if-nez v10, :cond_2

    .line 124
    :cond_1
    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v10

    const-string v11, "onRequestListener: no ahead road elements"

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 125
    iget-object v10, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;

    iget-object v10, v10, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    const/4 v11, 0x0

    # setter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->currentRequestInfo:Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;
    invoke-static {v10, v11}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$202(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;)Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;

    .line 126
    iget-object v10, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;

    iget-object v10, v10, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    const-wide/16 v12, 0x0

    # setter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->lastRequestTime:J
    invoke-static {v10, v12, v13}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$302(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;J)J

    .line 127
    iget-object v10, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;

    iget-object v10, v10, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    iget-object v11, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;

    iget-object v11, v11, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    invoke-virtual {v11}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->getRefereshInterval()I

    move-result v11

    # invokes: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->reset(I)V
    invoke-static {v10, v11}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$400(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 139
    .end local v0    # "aheadRouteElements":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/RouteElement;>;"
    .end local v1    # "cre":Lcom/here/android/mpa/common/RoadElement;
    .end local v2    # "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    .end local v3    # "next":Lcom/here/android/mpa/routing/Maneuver;
    .end local v8    # "prev":Lcom/here/android/mpa/routing/Maneuver;
    :goto_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    .line 140
    .local v6, "l2":J
    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "ahead route build time ["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sub-long v12, v6, v4

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "]"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 129
    .end local v6    # "l2":J
    .restart local v0    # "aheadRouteElements":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/RouteElement;>;"
    .restart local v1    # "cre":Lcom/here/android/mpa/common/RoadElement;
    .restart local v2    # "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    .restart local v3    # "next":Lcom/here/android/mpa/routing/Maneuver;
    .restart local v8    # "prev":Lcom/here/android/mpa/routing/Maneuver;
    :cond_2
    :try_start_1
    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "onRequestListener: calling getevent size:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 130
    iget-object v10, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;

    iget-object v10, v10, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->trafficUpdater:Lcom/here/android/mpa/guidance/TrafficUpdater;
    invoke-static {v10}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$600(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;)Lcom/here/android/mpa/guidance/TrafficUpdater;

    move-result-object v10

    iget-object v11, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;

    iget-object v11, v11, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->onGetEventsListener:Lcom/here/android/mpa/guidance/TrafficUpdater$GetEventsListener;
    invoke-static {v11}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$500(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;)Lcom/here/android/mpa/guidance/TrafficUpdater$GetEventsListener;

    move-result-object v11

    invoke-virtual {v10, v0, v11}, Lcom/here/android/mpa/guidance/TrafficUpdater;->getEvents(Ljava/util/List;Lcom/here/android/mpa/guidance/TrafficUpdater$GetEventsListener;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 132
    .end local v0    # "aheadRouteElements":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/RouteElement;>;"
    .end local v1    # "cre":Lcom/here/android/mpa/common/RoadElement;
    .end local v2    # "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    .end local v3    # "next":Lcom/here/android/mpa/routing/Maneuver;
    .end local v8    # "prev":Lcom/here/android/mpa/routing/Maneuver;
    :catch_0
    move-exception v9

    .line 133
    .local v9, "t":Ljava/lang/Throwable;
    iget-object v10, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;

    iget-object v10, v10, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    const/4 v11, 0x0

    # setter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->currentRequestInfo:Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;
    invoke-static {v10, v11}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$202(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;)Lcom/here/android/mpa/guidance/TrafficUpdater$RequestInfo;

    .line 134
    iget-object v10, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;

    iget-object v10, v10, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    const-wide/16 v12, 0x0

    # setter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->lastRequestTime:J
    invoke-static {v10, v12, v13}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$302(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;J)J

    .line 135
    iget-object v10, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;

    iget-object v10, v10, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    iget-object v11, p0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1$1;->this$1:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;

    iget-object v11, v11, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$1;->this$0:Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;

    invoke-virtual {v11}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->getRefereshInterval()I

    move-result v11

    div-int/lit8 v11, v11, 0x2

    # invokes: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->reset(I)V
    invoke-static {v10, v11}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$400(Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;I)V

    .line 136
    # getter for: Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v10

    const-string v11, "onRequestListener"

    invoke-virtual {v10, v11, v9}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1
.end method
