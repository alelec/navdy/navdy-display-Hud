.class public final enum Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;
.super Ljava/lang/Enum;
.source "HereManeuverDisplayBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ManeuverState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

.field public static final enum NEXT:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

.field public static final enum NOW:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

.field public static final enum SOON:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

.field public static final enum STAY:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 154
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    const-string v1, "STAY"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->STAY:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    .line 155
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    const-string v1, "NEXT"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->NEXT:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    .line 156
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    const-string v1, "SOON"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->SOON:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    .line 157
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    const-string v1, "NOW"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->NOW:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    .line 153
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->STAY:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->NEXT:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->SOON:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->NOW:Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->$VALUES:[Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 153
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 153
    const-class v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;
    .locals 1

    .prologue
    .line 153
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->$VALUES:[Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;

    return-object v0
.end method
