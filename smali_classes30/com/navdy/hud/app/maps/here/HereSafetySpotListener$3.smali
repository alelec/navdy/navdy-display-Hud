.class Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$3;
.super Ljava/lang/Object;
.source "HereSafetySpotListener.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->setSafetySpotsEnabledOnMap(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

.field final synthetic val$enabled:Z


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

    .prologue
    .line 232
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

    iput-boolean p2, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$3;->val$enabled:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 235
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->canDrawOnTheMap:Z
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->access$100(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;)Z

    move-result v3

    iget-boolean v4, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$3;->val$enabled:Z

    if-eq v3, v4, :cond_1

    .line 236
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

    iget-boolean v4, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$3;->val$enabled:Z

    # setter for: Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->canDrawOnTheMap:Z
    invoke-static {v3, v4}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->access$102(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;Z)Z

    .line 237
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->canDrawOnTheMap:Z
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->access$100(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 238
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

    # invokes: Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->removeExpiredSpots()Ljava/util/List;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->access$000(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;)Ljava/util/List;

    move-result-object v1

    .line 239
    .local v1, "invalidSafetySpots":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;>;"
    # getter for: Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->access$300()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Enabling the safety spots on the Map, Total spots Valid : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->safetySpotNotifications:Ljava/util/List;
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->access$1100(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Invalid : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 241
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 242
    .local v2, "mapMarkers":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/mapping/MapObject;>;"
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->safetySpotNotifications:Ljava/util/List;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->access$1100(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;

    .line 243
    .local v0, "container":Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;
    # getter for: Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;->mapMarker:Lcom/here/android/mpa/mapping/MapMarker;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;->access$1000(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;)Lcom/here/android/mpa/mapping/MapMarker;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 245
    .end local v0    # "container":Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;
    :cond_0
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->access$1200(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;)Lcom/navdy/hud/app/maps/here/HereMapController;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/navdy/hud/app/maps/here/HereMapController;->addMapObjects(Ljava/util/List;)V

    .line 252
    .end local v1    # "invalidSafetySpots":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;>;"
    .end local v2    # "mapMarkers":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/mapping/MapObject;>;"
    :cond_1
    :goto_1
    return-void

    .line 248
    :cond_2
    # getter for: Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->access$300()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "Remove all the safety spots from the map"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 249
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->safetySpotNotifications:Ljava/util/List;
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->access$1100(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;)Ljava/util/List;

    move-result-object v4

    # invokes: Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->removeMapMarkers(Ljava/util/List;)V
    invoke-static {v3, v4}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->access$200(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;Ljava/util/List;)V

    goto :goto_1
.end method
