.class Lcom/navdy/hud/app/maps/here/HereMapsManager$1;
.super Ljava/lang/Object;
.source "HereMapsManager.java"

# interfaces
.implements Lcom/here/android/mpa/common/OnEngineInitListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/here/HereMapsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereMapsManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereMapsManager;

    .prologue
    .line 218
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEngineInitializationCompleted(Lcom/here/android/mpa/common/OnEngineInitListener$Error;)V
    .locals 8
    .param p1, "error"    # Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    .prologue
    .line 221
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 222
    .local v2, "t1":J
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MAP-ENGINE-INIT took ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapEngineStartTime:J
    invoke-static {v5}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$000(Lcom/navdy/hud/app/maps/here/HereMapsManager;)J

    move-result-wide v6

    sub-long v6, v2, v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 225
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MAP-ENGINE-INIT HERE-SDK version:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/here/android/mpa/common/Version;->getSdkVersion()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 227
    sget-object v1, Lcom/here/android/mpa/common/OnEngineInitListener$Error;->NONE:Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    if-ne p1, v1, :cond_1

    .line 228
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v1

    new-instance v4, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;

    invoke-direct {v4, p0, v2, v3}, Lcom/navdy/hud/app/maps/here/HereMapsManager$1$1;-><init>(Lcom/navdy/hud/app/maps/here/HereMapsManager$1;J)V

    const/4 v5, 0x3

    invoke-virtual {v1, v4, v5}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 384
    :cond_0
    :goto_0
    return-void

    .line 371
    :cond_1
    invoke-virtual {p1}, Lcom/here/android/mpa/common/OnEngineInitListener$Error;->toString()Ljava/lang/String;

    move-result-object v0

    .line 372
    .local v0, "errString":Ljava/lang/String;
    const-string v1, "HereMaps"

    invoke-static {v1, v0}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordKeyFailure(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MAP-ENGINE-INIT engine NOT initialized:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 374
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # setter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapError:Lcom/here/android/mpa/common/OnEngineInitListener$Error;
    invoke-static {v1, p1}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$2202(Lcom/navdy/hud/app/maps/here/HereMapsManager;Lcom/here/android/mpa/common/OnEngineInitListener$Error;)Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    .line 375
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->initLock:Ljava/lang/Object;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$1700(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4

    .line 376
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    const/4 v5, 0x0

    # setter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapInitializing:Z
    invoke-static {v1, v5}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$1802(Lcom/navdy/hud/app/maps/here/HereMapsManager;Z)Z

    .line 377
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    iget-object v1, v1, Lcom/navdy/hud/app/maps/here/HereMapsManager;->bus:Lcom/squareup/otto/Bus;

    new-instance v5, Lcom/navdy/hud/app/maps/MapEvents$MapEngineInitialize;

    iget-object v6, p0, Lcom/navdy/hud/app/maps/here/HereMapsManager$1;->this$0:Lcom/navdy/hud/app/maps/here/HereMapsManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapsManager;->mapInitialized:Z
    invoke-static {v6}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->access$1900(Lcom/navdy/hud/app/maps/here/HereMapsManager;)Z

    move-result v6

    invoke-direct {v5, v6}, Lcom/navdy/hud/app/maps/MapEvents$MapEngineInitialize;-><init>(Z)V

    invoke-virtual {v1, v5}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 378
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 379
    sget-object v1, Lcom/here/android/mpa/common/OnEngineInitListener$Error;->USAGE_EXPIRED:Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    if-eq p1, v1, :cond_2

    sget-object v1, Lcom/here/android/mpa/common/OnEngineInitListener$Error;->MISSING_APP_CREDENTIAL:Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    if-ne p1, v1, :cond_0

    .line 380
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Here Maps initialization failure: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/navdy/hud/app/ui/activity/Main;->handleLibraryInitializationError(Ljava/lang/String;)V

    goto :goto_0

    .line 378
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
