.class final Lcom/navdy/hud/app/maps/here/HereRouteManager$3;
.super Ljava/lang/Object;
.source "HereRouteManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereRouteManager;->routeSearchTraffic(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Lcom/here/android/mpa/common/GeoCoordinate;Ljava/util/List;Lcom/here/android/mpa/common/GeoCoordinate;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$traffic:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereRouteCalculator;)V
    .locals 0

    .prologue
    .line 422
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$3;->val$traffic:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 426
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cancel traffic based route,max time reached:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->MAX_ONLINE_ROUTE_CALCULATION_TIME:I
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$1200()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 427
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$3;->val$traffic:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereRouteCalculator;->cancel()V

    .line 428
    return-void
.end method
