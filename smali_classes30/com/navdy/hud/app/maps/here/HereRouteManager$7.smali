.class final Lcom/navdy/hud/app/maps/here/HereRouteManager$7;
.super Ljava/lang/Object;
.source "HereRouteManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereRouteManager;->clearActiveRouteCalc()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$pendingRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;


# direct methods
.method constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V
    .locals 0

    .prologue
    .line 776
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$7;->val$pendingRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 779
    # getter for: Lcom/navdy/hud/app/maps/here/HereRouteManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "launched pending request"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 780
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRouteManager$7;->val$pendingRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->handleRouteRequest(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V

    .line 781
    return-void
.end method
