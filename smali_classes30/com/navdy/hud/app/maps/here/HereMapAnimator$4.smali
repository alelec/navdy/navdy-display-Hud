.class Lcom/navdy/hud/app/maps/here/HereMapAnimator$4;
.super Ljava/lang/Object;
.source "HereMapAnimator.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereMapAnimator;->setTilt(F)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

.field final synthetic val$tilt:F


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereMapAnimator;F)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    .prologue
    .line 301
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    iput p2, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$4;->val$tilt:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 304
    const-wide/16 v0, 0x0

    .line 306
    .local v0, "interval":J
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapAnimator;->lastTiltUpdateTime:J
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->access$1500(Lcom/navdy/hud/app/maps/here/HereMapAnimator;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 307
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-object v4, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    # getter for: Lcom/navdy/hud/app/maps/here/HereMapAnimator;->lastTiltUpdateTime:J
    invoke-static {v4}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->access$1500(Lcom/navdy/hud/app/maps/here/HereMapAnimator;)J

    move-result-wide v4

    sub-long v0, v2, v4

    .line 310
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    iget v3, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$4;->val$tilt:F

    # invokes: Lcom/navdy/hud/app/maps/here/HereMapAnimator;->isValidTilt(F)Z
    invoke-static {v2, v3}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->access$1600(Lcom/navdy/hud/app/maps/here/HereMapAnimator;F)Z

    move-result v2

    if-nez v2, :cond_1

    .line 311
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapAnimator;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "filtering out spurious tilt: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$4;->val$tilt:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 320
    :goto_0
    return-void

    .line 314
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    # setter for: Lcom/navdy/hud/app/maps/here/HereMapAnimator;->lastTiltUpdateTime:J
    invoke-static {v2, v4, v5}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->access$1502(Lcom/navdy/hud/app/maps/here/HereMapAnimator;J)J

    .line 316
    # getter for: Lcom/navdy/hud/app/maps/here/HereMapAnimator;->tiltLock:Ljava/lang/Object;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->access$1700()Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 317
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    # setter for: Lcom/navdy/hud/app/maps/here/HereMapAnimator;->tiltUpdateInterval:J
    invoke-static {v2, v0, v1}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->access$1802(Lcom/navdy/hud/app/maps/here/HereMapAnimator;J)J

    .line 318
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$4;->this$0:Lcom/navdy/hud/app/maps/here/HereMapAnimator;

    iget v4, p0, Lcom/navdy/hud/app/maps/here/HereMapAnimator$4;->val$tilt:F

    # setter for: Lcom/navdy/hud/app/maps/here/HereMapAnimator;->currentTilt:F
    invoke-static {v2, v4}, Lcom/navdy/hud/app/maps/here/HereMapAnimator;->access$1902(Lcom/navdy/hud/app/maps/here/HereMapAnimator;F)F

    .line 319
    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method
