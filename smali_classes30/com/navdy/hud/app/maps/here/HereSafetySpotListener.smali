.class public Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;
.super Lcom/here/android/mpa/guidance/NavigationManager$SafetySpotListener;
.source "HereSafetySpotListener.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;
    }
.end annotation


# static fields
.field private static final CHECK_INTERVAL:I

.field private static final SAFETY_SPOT_HIDE_DISTANCE:I = 0x3e8

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private final bus:Lcom/squareup/otto/Bus;

.field private canDrawOnTheMap:Z

.field private final checkExpiredSpots:Ljava/lang/Runnable;

.field private final handler:Landroid/os/Handler;

.field private final mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

.field private safetySpotNotifications:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 37
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-string v1, "SafetySpotListener"

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 40
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->CHECK_INTERVAL:I

    return-void
.end method

.method constructor <init>(Lcom/squareup/otto/Bus;Lcom/navdy/hud/app/maps/here/HereMapController;)V
    .locals 2
    .param p1, "bus"    # Lcom/squareup/otto/Bus;
    .param p2, "mapController"    # Lcom/navdy/hud/app/maps/here/HereMapController;

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/here/android/mpa/guidance/NavigationManager$SafetySpotListener;-><init>()V

    .line 46
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->canDrawOnTheMap:Z

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->safetySpotNotifications:Ljava/util/List;

    .line 54
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->handler:Landroid/os/Handler;

    .line 56
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$1;-><init>(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->checkExpiredSpots:Ljava/lang/Runnable;

    .line 73
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "ctor"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 74
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->bus:Lcom/squareup/otto/Bus;

    .line 75
    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    .line 76
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->removeExpiredSpots()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->canDrawOnTheMap:Z

    return v0
.end method

.method static synthetic access$102(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;
    .param p1, "x1"    # Z

    .prologue
    .line 36
    iput-boolean p1, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->canDrawOnTheMap:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->safetySpotNotifications:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;)Lcom/navdy/hud/app/maps/here/HereMapController;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->removeMapMarkers(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$300()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$400()I
    .locals 1

    .prologue
    .line 36
    sget v0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->CHECK_INTERVAL:I

    return v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->checkExpiredSpots:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;Ljava/util/List;Lcom/here/android/mpa/common/GeoCoordinate;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;
    .param p1, "x1"    # Ljava/util/List;
    .param p2, "x2"    # Lcom/here/android/mpa/common/GeoCoordinate;

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->addSafetySpotMarkers(Ljava/util/List;Lcom/here/android/mpa/common/GeoCoordinate;)V

    return-void
.end method

.method private addSafetySpotMarkers(Ljava/util/List;Lcom/here/android/mpa/common/GeoCoordinate;)V
    .locals 19
    .param p2, "currentLocation"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/guidance/SafetySpotNotificationInfo;",
            ">;",
            "Lcom/here/android/mpa/common/GeoCoordinate;",
            ")V"
        }
    .end annotation

    .prologue
    .line 159
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/guidance/SafetySpotNotificationInfo;>;"
    const/4 v10, 0x0

    .line 160
    .local v10, "nearestType":Lcom/here/android/mpa/mapping/SafetySpotInfo$Type;
    const/4 v9, 0x0

    .line 161
    .local v9, "nearestDistance":I
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 162
    .local v2, "addList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;>;"
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 163
    .local v8, "mapMarkers":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/mapping/MapObject;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_0
    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_3

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/here/android/mpa/guidance/SafetySpotNotificationInfo;

    .line 165
    .local v7, "info":Lcom/here/android/mpa/guidance/SafetySpotNotificationInfo;
    :try_start_0
    new-instance v3, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-direct {v3, v0}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;-><init>(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$1;)V

    .line 166
    .local v3, "container":Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;
    # setter for: Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;->safetySpotNotificationInfo:Lcom/here/android/mpa/guidance/SafetySpotNotificationInfo;
    invoke-static {v3, v7}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;->access$602(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;Lcom/here/android/mpa/guidance/SafetySpotNotificationInfo;)Lcom/here/android/mpa/guidance/SafetySpotNotificationInfo;

    .line 167
    invoke-virtual {v7}, Lcom/here/android/mpa/guidance/SafetySpotNotificationInfo;->getSafetySpot()Lcom/here/android/mpa/mapping/SafetySpotInfo;

    move-result-object v11

    .line 168
    .local v11, "safetySpot":Lcom/here/android/mpa/mapping/SafetySpotInfo;
    invoke-virtual {v11}, Lcom/here/android/mpa/mapping/SafetySpotInfo;->getType()Lcom/here/android/mpa/mapping/SafetySpotInfo$Type;

    move-result-object v14

    .line 169
    .local v14, "type":Lcom/here/android/mpa/mapping/SafetySpotInfo$Type;
    invoke-virtual {v11}, Lcom/here/android/mpa/mapping/SafetySpotInfo;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v12

    .line 170
    .local v12, "safetySpotLocation":Lcom/here/android/mpa/common/GeoCoordinate;
    new-instance v6, Lcom/here/android/mpa/common/Image;

    invoke-direct {v6}, Lcom/here/android/mpa/common/Image;-><init>()V

    .line 171
    .local v6, "image":Lcom/here/android/mpa/common/Image;
    sget-object v16, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$4;->$SwitchMap$com$here$android$mpa$mapping$SafetySpotInfo$Type:[I

    invoke-virtual {v14}, Lcom/here/android/mpa/mapping/SafetySpotInfo$Type;->ordinal()I

    move-result v17

    aget v16, v16, v17

    packed-switch v16, :pswitch_data_0

    .line 177
    const v16, 0x7f0201cc

    move/from16 v0, v16

    invoke-virtual {v6, v0}, Lcom/here/android/mpa/common/Image;->setImageResource(I)V

    .line 180
    :goto_1
    new-instance v16, Lcom/here/android/mpa/mapping/MapMarker;

    move-object/from16 v0, v16

    invoke-direct {v0, v12, v6}, Lcom/here/android/mpa/mapping/MapMarker;-><init>(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/Image;)V

    move-object/from16 v0, v16

    # setter for: Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;->mapMarker:Lcom/here/android/mpa/mapping/MapMarker;
    invoke-static {v3, v0}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;->access$1002(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;Lcom/here/android/mpa/mapping/MapMarker;)Lcom/here/android/mpa/mapping/MapMarker;

    .line 181
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->canDrawOnTheMap:Z

    move/from16 v16, v0

    if-eqz v16, :cond_1

    .line 182
    # getter for: Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;->mapMarker:Lcom/here/android/mpa/mapping/MapMarker;
    invoke-static {v3}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;->access$1000(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;)Lcom/here/android/mpa/mapping/MapMarker;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 184
    :cond_1
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 185
    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Lcom/here/android/mpa/common/GeoCoordinate;->distanceTo(Lcom/here/android/mpa/common/GeoCoordinate;)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-int v5, v0

    .line 186
    .local v5, "distanceCalc":I
    invoke-virtual {v7}, Lcom/here/android/mpa/guidance/SafetySpotNotificationInfo;->getDistance()J

    move-result-wide v16

    move-wide/from16 v0, v16

    long-to-int v4, v0

    .line 187
    .local v4, "distance":I
    sget-object v16, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "add safety spot id["

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-static {v7}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "] type["

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "] distance calc["

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "] distance["

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "]"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 189
    if-eqz v10, :cond_2

    if-le v9, v4, :cond_0

    .line 190
    :cond_2
    move-object v10, v14

    .line 191
    move v9, v4

    goto/16 :goto_0

    .line 173
    .end local v4    # "distance":I
    .end local v5    # "distanceCalc":I
    :pswitch_0
    const v16, 0x7f0201f1

    move/from16 v0, v16

    invoke-virtual {v6, v0}, Lcom/here/android/mpa/common/Image;->setImageResource(I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 193
    .end local v3    # "container":Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;
    .end local v6    # "image":Lcom/here/android/mpa/common/Image;
    .end local v11    # "safetySpot":Lcom/here/android/mpa/mapping/SafetySpotInfo;
    .end local v12    # "safetySpotLocation":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v14    # "type":Lcom/here/android/mpa/mapping/SafetySpotInfo$Type;
    :catch_0
    move-exception v13

    .line 194
    .local v13, "t":Ljava/lang/Throwable;
    sget-object v16, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v17, "map marker error"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v13}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 197
    .end local v7    # "info":Lcom/here/android/mpa/guidance/SafetySpotNotificationInfo;
    .end local v13    # "t":Ljava/lang/Throwable;
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->canDrawOnTheMap:Z

    if-eqz v15, :cond_4

    .line 198
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {v15, v8}, Lcom/navdy/hud/app/maps/here/HereMapController;->addMapObjects(Ljava/util/List;)V

    .line 200
    :cond_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->safetySpotNotifications:Ljava/util/List;

    invoke-interface {v15, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 201
    if-eqz v10, :cond_5

    if-lez v9, :cond_5

    .line 202
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v9}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->sendTts(Lcom/here/android/mpa/mapping/SafetySpotInfo$Type;I)V

    .line 204
    :cond_5
    return-void

    .line 171
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private isSafetySpotRelevant(Lcom/here/android/mpa/guidance/SafetySpotNotificationInfo;Lcom/here/android/mpa/common/GeoCoordinate;)Z
    .locals 8
    .param p1, "safetySpotNotificationInfo"    # Lcom/here/android/mpa/guidance/SafetySpotNotificationInfo;
    .param p2, "currentLocation"    # Lcom/here/android/mpa/common/GeoCoordinate;

    .prologue
    .line 111
    invoke-virtual {p1}, Lcom/here/android/mpa/guidance/SafetySpotNotificationInfo;->getSafetySpot()Lcom/here/android/mpa/mapping/SafetySpotInfo;

    move-result-object v2

    .line 112
    .local v2, "safetySpotInfo":Lcom/here/android/mpa/mapping/SafetySpotInfo;
    invoke-virtual {p1}, Lcom/here/android/mpa/guidance/SafetySpotNotificationInfo;->getSafetySpot()Lcom/here/android/mpa/mapping/SafetySpotInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/here/android/mpa/mapping/SafetySpotInfo;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v3

    invoke-virtual {p2, v3}, Lcom/here/android/mpa/common/GeoCoordinate;->distanceTo(Lcom/here/android/mpa/common/GeoCoordinate;)D

    move-result-wide v4

    double-to-int v0, v4

    .line 113
    .local v0, "distanceCalc":I
    invoke-static {p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    .line 114
    .local v1, "id":I
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "check safety spot id["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] type["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/here/android/mpa/mapping/SafetySpotInfo;->getType()Lcom/here/android/mpa/mapping/SafetySpotInfo$Type;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] distance calc["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] distance["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 115
    invoke-virtual {p1}, Lcom/here/android/mpa/guidance/SafetySpotNotificationInfo;->getDistance()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 114
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 116
    const/16 v3, 0x3e8

    if-lt v0, v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private removeExpiredSpots()Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 83
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v0

    .line 84
    .local v0, "currentLocation":Lcom/here/android/mpa/common/GeoCoordinate;
    if-nez v0, :cond_1

    .line 85
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "check:no current location"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 107
    :cond_0
    :goto_0
    return-object v3

    .line 88
    :cond_1
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->safetySpotNotifications:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-eqz v5, :cond_0

    .line 91
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 92
    .local v4, "valid":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 93
    .local v3, "invalid":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;>;"
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->safetySpotNotifications:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;

    .line 94
    .local v2, "info":Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;
    # getter for: Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;->safetySpotNotificationInfo:Lcom/here/android/mpa/guidance/SafetySpotNotificationInfo;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;->access$600(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;)Lcom/here/android/mpa/guidance/SafetySpotNotificationInfo;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    .line 95
    .local v1, "id":I
    # getter for: Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;->safetySpotNotificationInfo:Lcom/here/android/mpa/guidance/SafetySpotNotificationInfo;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;->access$600(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;)Lcom/here/android/mpa/guidance/SafetySpotNotificationInfo;

    move-result-object v6

    invoke-direct {p0, v6, v0}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->isSafetySpotRelevant(Lcom/here/android/mpa/guidance/SafetySpotNotificationInfo;Lcom/here/android/mpa/common/GeoCoordinate;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 97
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "check remove:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto :goto_1

    .line 101
    :cond_2
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 102
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "check valid:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto :goto_1

    .line 105
    .end local v1    # "id":I
    .end local v2    # "info":Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;
    :cond_3
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->safetySpotNotifications:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 106
    iget-object v5, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->safetySpotNotifications:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method private removeMapMarkers(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 258
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;>;"
    if-eqz p1, :cond_1

    .line 259
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 260
    .local v1, "mapMarkers":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/mapping/MapObject;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;

    .line 261
    .local v0, "info":Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;
    # getter for: Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;->mapMarker:Lcom/here/android/mpa/mapping/MapMarker;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;->access$1000(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;)Lcom/here/android/mpa/mapping/MapMarker;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 263
    .end local v0    # "info":Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$SafetySpotContainer;
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->mapController:Lcom/navdy/hud/app/maps/here/HereMapController;

    invoke-virtual {v2, v1}, Lcom/navdy/hud/app/maps/here/HereMapController;->removeMapObjects(Ljava/util/List;)V

    .line 265
    .end local v1    # "mapMarkers":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/mapping/MapObject;>;"
    :cond_1
    return-void
.end method

.method private sendTts(Lcom/here/android/mpa/mapping/SafetySpotInfo$Type;I)V
    .locals 8
    .param p1, "type"    # Lcom/here/android/mpa/mapping/SafetySpotInfo$Type;
    .param p2, "distance"    # I

    .prologue
    const/4 v7, 0x1

    .line 207
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/profile/DriverProfile;->getNavigationPreferences()Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    move-result-object v2

    .line 208
    .local v2, "pref":Lcom/navdy/service/library/events/preferences/NavigationPreferences;
    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v6, v2, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->spokenCameraWarnings:Ljava/lang/Boolean;

    invoke-virtual {v5, v6}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 209
    new-instance v0, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;

    invoke-direct {v0}, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;-><init>()V

    .line 210
    .local v0, "distanceConverter":Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;
    invoke-static {}, Lcom/navdy/hud/app/manager/SpeedManager;->getInstance()Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/manager/SpeedManager;->getSpeedUnit()Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;

    move-result-object v5

    int-to-float v6, p2

    invoke-static {v5, v6, v0}, Lcom/navdy/hud/app/maps/util/DistanceConverter;->convertToDistance(Lcom/navdy/hud/app/manager/SpeedManager$SpeedUnit;FLcom/navdy/hud/app/maps/util/DistanceConverter$Distance;)V

    .line 211
    iget v5, v0, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;->value:F

    iget-object v6, v0, Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;->unit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    invoke-static {v5, v6, v7}, Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder;->getFormattedDistance(FLcom/navdy/service/library/events/navigation/DistanceUnit;Z)Ljava/lang/String;

    move-result-object v1

    .line 213
    .local v1, "formattedDistance":Ljava/lang/String;
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$4;->$SwitchMap$com$here$android$mpa$mapping$SafetySpotInfo$Type:[I

    invoke-virtual {p1}, Lcom/here/android/mpa/mapping/SafetySpotInfo$Type;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 223
    const v3, 0x7f09004d

    .line 226
    .local v3, "resId":I
    :goto_0
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    new-array v6, v7, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v1, v6, v7

    invoke-virtual {v5, v3, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 227
    .local v4, "tts":Ljava/lang/String;
    sget-object v5, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->SPEECH_CAMERA_WARNING:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Lcom/navdy/hud/app/framework/voice/TTSUtils;->sendSpeechRequest(Ljava/lang/String;Lcom/navdy/service/library/events/audio/SpeechRequest$Category;Ljava/lang/String;)V

    .line 229
    .end local v0    # "distanceConverter":Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;
    .end local v1    # "formattedDistance":Ljava/lang/String;
    .end local v3    # "resId":I
    .end local v4    # "tts":Ljava/lang/String;
    :cond_0
    return-void

    .line 215
    .restart local v0    # "distanceConverter":Lcom/navdy/hud/app/maps/util/DistanceConverter$Distance;
    .restart local v1    # "formattedDistance":Ljava/lang/String;
    :pswitch_0
    const v3, 0x7f09004e

    .line 216
    .restart local v3    # "resId":I
    goto :goto_0

    .line 219
    .end local v3    # "resId":I
    :pswitch_1
    const v3, 0x7f09004c

    .line 220
    .restart local v3    # "resId":I
    goto :goto_0

    .line 213
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public onSafetySpot(Lcom/here/android/mpa/guidance/SafetySpotNotification;)V
    .locals 2
    .param p1, "safetySpotNotification"    # Lcom/here/android/mpa/guidance/SafetySpotNotification;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$2;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$2;-><init>(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;Lcom/here/android/mpa/guidance/SafetySpotNotification;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 156
    return-void
.end method

.method public setSafetySpotsEnabledOnMap(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    .line 232
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$3;

    invoke-direct {v1, p0, p1}, Lcom/navdy/hud/app/maps/here/HereSafetySpotListener$3;-><init>(Lcom/navdy/hud/app/maps/here/HereSafetySpotListener;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 254
    return-void
.end method
