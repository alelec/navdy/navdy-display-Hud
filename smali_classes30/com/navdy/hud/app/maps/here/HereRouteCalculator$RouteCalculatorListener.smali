.class public interface abstract Lcom/navdy/hud/app/maps/here/HereRouteCalculator$RouteCalculatorListener;
.super Ljava/lang/Object;
.source "HereRouteCalculator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/here/HereRouteCalculator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "RouteCalculatorListener"
.end annotation


# virtual methods
.method public abstract error(Lcom/here/android/mpa/routing/RoutingError;Ljava/lang/Throwable;)V
.end method

.method public abstract postSuccess(Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteResult;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract preSuccess()V
.end method

.method public abstract progress(I)V
.end method
