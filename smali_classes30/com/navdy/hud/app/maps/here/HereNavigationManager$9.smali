.class Lcom/navdy/hud/app/maps/here/HereNavigationManager$9;
.super Ljava/lang/Object;
.source "HereNavigationManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereNavigationManager;->arrived()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    .prologue
    .line 1759
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$9;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 1762
    monitor-enter p0

    .line 1763
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$9;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isNavigationModeOn()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1765
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "arrived = true"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1766
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$9;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hereNavController:Lcom/navdy/hud/app/maps/here/HereNavController;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$700(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Lcom/navdy/hud/app/maps/here/HereNavController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereNavController;->arrivedAtDestination()V

    .line 1767
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->removeRouteInfo(Lcom/navdy/service/library/log/Logger;Z)V

    .line 1769
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$9;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$100(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    move-result-object v1

    iget-object v1, v1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->navigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    iget-object v1, v1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->routeAttributes:Ljava/util/List;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;->ROUTE_ATTRIBUTE_GAS:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;

    .line 1770
    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1771
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "arrived : regular"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1773
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$9;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$100(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    move-result-object v1

    iget-boolean v1, v1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->hasArrived:Z

    if-nez v1, :cond_0

    .line 1774
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$9;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$500(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Lcom/squareup/otto/Bus;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/maps/MapEvents$ManeuverEvent;

    sget-object v3, Lcom/navdy/hud/app/maps/MapEvents$ManeuverEvent$Type;->LAST:Lcom/navdy/hud/app/maps/MapEvents$ManeuverEvent$Type;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/navdy/hud/app/maps/MapEvents$ManeuverEvent;-><init>(Lcom/navdy/hud/app/maps/MapEvents$ManeuverEvent$Type;Lcom/here/android/mpa/routing/Maneuver;)V

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 1777
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$9;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$100(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    move-result-object v1

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->hasArrived:Z

    .line 1778
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$9;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentNavigationInfo:Lcom/navdy/hud/app/maps/here/HereNavigationInfo;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$100(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Lcom/navdy/hud/app/maps/here/HereNavigationInfo;

    move-result-object v1

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/navdy/hud/app/maps/here/HereNavigationInfo;->ignoreArrived:Z

    .line 1780
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$9;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$500(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Lcom/squareup/otto/Bus;

    move-result-object v1

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->ARRIVAL_EVENT:Lcom/navdy/hud/app/maps/MapEvents$ArrivalEvent;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$1200()Lcom/navdy/hud/app/maps/MapEvents$ArrivalEvent;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 1781
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$9;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    # invokes: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->postArrivedManeuver()V
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$1300(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)V

    .line 1782
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$9;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->removeCurrentRoute()Z

    .line 1783
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$9;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_STOPPED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    # setter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->currentSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;
    invoke-static {v1, v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$1402(Lcom/navdy/hud/app/maps/here/HereNavigationManager;Lcom/navdy/service/library/events/navigation/NavigationSessionState;)Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 1784
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "arrived: session stopped"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1786
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$9;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_ARRIVED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->postNavigationSessionStatusEvent(Lcom/navdy/service/library/events/navigation/NavigationSessionState;Z)V

    .line 1787
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$9;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->postNavigationSessionStatusEvent(Z)V

    .line 1788
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "arrived: posted arrival maneuver"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1807
    :cond_1
    :goto_0
    monitor-exit p0

    .line 1808
    return-void

    .line 1790
    :cond_2
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "arrived : gas"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1791
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getSavedRouteData(Lcom/navdy/service/library/log/Logger;)Lcom/navdy/hud/app/maps/here/HereMapUtil$SavedRouteData;

    move-result-object v1

    iget-object v0, v1, Lcom/navdy/hud/app/maps/here/HereMapUtil$SavedRouteData;->navigationRouteRequest:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .line 1792
    .local v0, "nextRoute":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$9;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    sget-object v2, Lcom/navdy/hud/app/maps/NavigationMode;->MAP:Lcom/navdy/hud/app/maps/NavigationMode;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->setNavigationMode(Lcom/navdy/hud/app/maps/NavigationMode;Lcom/navdy/hud/app/maps/here/HereNavigationInfo;Ljava/lang/StringBuilder;)Z

    .line 1793
    if-eqz v0, :cond_3

    .line 1794
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "arrived : gas, has next route"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1795
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationManager$9;->this$0:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    # getter for: Lcom/navdy/hud/app/maps/here/HereNavigationManager;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->access$200(Lcom/navdy/hud/app/maps/here/HereNavigationManager;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/maps/here/HereNavigationManager$9$1;

    invoke-direct {v2, p0, v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager$9$1;-><init>(Lcom/navdy/hud/app/maps/here/HereNavigationManager$9;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 1807
    .end local v0    # "nextRoute":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 1803
    .restart local v0    # "nextRoute":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    :cond_3
    :try_start_1
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "arrived : gas, no next route"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
