.class final Lcom/navdy/hud/app/maps/here/HerePlacesManager$4;
.super Ljava/lang/Object;
.source "HerePlacesManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HerePlacesManager;->handlePlacesSearchRequest(Lcom/navdy/service/library/events/places/PlacesSearchRequest;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$request:Lcom/navdy/service/library/events/places/PlacesSearchRequest;


# direct methods
.method constructor <init>(Lcom/navdy/service/library/events/places/PlacesSearchRequest;)V
    .locals 0

    .prologue
    .line 182
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4;->val$request:Lcom/navdy/service/library/events/places/PlacesSearchRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 18

    .prologue
    .line 185
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    .line 186
    .local v8, "l1":J
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4;->val$request:Lcom/navdy/service/library/events/places/PlacesSearchRequest;

    iget-object v4, v2, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->searchQuery:Ljava/lang/String;

    .line 187
    .local v4, "searchQuery":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4;->val$request:Lcom/navdy/service/library/events/places/PlacesSearchRequest;

    iget-object v2, v2, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->searchArea:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4;->val$request:Lcom/navdy/service/library/events/places/PlacesSearchRequest;

    iget-object v2, v2, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->searchArea:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 189
    .local v7, "searchArea":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4;->val$request:Lcom/navdy/service/library/events/places/PlacesSearchRequest;

    iget-object v2, v2, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->maxResults:Ljava/lang/Integer;

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4;->val$request:Lcom/navdy/service/library/events/places/PlacesSearchRequest;

    iget-object v2, v2, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->maxResults:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v11

    .line 190
    .local v11, "n":I
    :goto_1
    if-lez v11, :cond_0

    const/16 v2, 0xf

    if-le v11, v2, :cond_1

    .line 191
    :cond_0
    const/16 v11, 0xf

    .line 194
    :cond_1
    move v6, v11

    .line 197
    .local v6, "maxResults":I
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4;->val$request:Lcom/navdy/service/library/events/places/PlacesSearchRequest;

    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->printSearchRequest(Lcom/navdy/service/library/events/places/PlacesSearchRequest;)V

    .line 199
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$100()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v2

    if-nez v2, :cond_5

    .line 200
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "[search] engine not ready"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 201
    sget-object v2, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_NOT_READY:Lcom/navdy/service/library/events/RequestStatus;

    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->context:Landroid/content/Context;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$200()Landroid/content/Context;

    move-result-object v3

    const v15, 0x7f0901a2

    invoke-virtual {v3, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->returnSearchErrorResponse(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V
    invoke-static {v4, v2, v3}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$300(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 267
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 268
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v12

    .line 269
    .local v12, "l2":J
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "[search] handleSearchRequest- time-1 ="

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sub-long v16, v12, v8

    move-wide/from16 v0, v16

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 272
    .end local v12    # "l2":J
    :cond_2
    :goto_2
    return-void

    .line 187
    .end local v6    # "maxResults":I
    .end local v7    # "searchArea":I
    .end local v11    # "n":I
    :cond_3
    const/4 v7, -0x1

    goto :goto_0

    .line 189
    .restart local v7    # "searchArea":I
    :cond_4
    const/16 v11, 0xf

    goto :goto_1

    .line 205
    .restart local v6    # "maxResults":I
    .restart local v11    # "n":I
    :cond_5
    :try_start_1
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 206
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "[search] invalid search query"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 207
    sget-object v2, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_INVALID_REQUEST:Lcom/navdy/service/library/events/RequestStatus;

    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->context:Landroid/content/Context;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$200()Landroid/content/Context;

    move-result-object v3

    const v15, 0x7f0900ea

    invoke-virtual {v3, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->returnSearchErrorResponse(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V
    invoke-static {v4, v2, v3}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$300(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 267
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 268
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v12

    .line 269
    .restart local v12    # "l2":J
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "[search] handleSearchRequest- time-1 ="

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sub-long v16, v12, v8

    move-wide/from16 v0, v16

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_2

    .line 211
    .end local v12    # "l2":J
    :cond_6
    :try_start_2
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->hereMapsManager:Lcom/navdy/hud/app/maps/here/HereMapsManager;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$100()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLocationFixManager()Lcom/navdy/hud/app/maps/here/HereLocationFixManager;

    move-result-object v10

    .line 212
    .local v10, "hereLocationFixManager":Lcom/navdy/hud/app/maps/here/HereLocationFixManager;
    if-nez v10, :cond_7

    .line 213
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "[search] engine not ready, location fix manager n/a"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 214
    sget-object v2, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_NOT_READY:Lcom/navdy/service/library/events/RequestStatus;

    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->context:Landroid/content/Context;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$200()Landroid/content/Context;

    move-result-object v3

    const v15, 0x7f0901a2

    invoke-virtual {v3, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->returnSearchErrorResponse(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V
    invoke-static {v4, v2, v3}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$300(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 267
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 268
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v12

    .line 269
    .restart local v12    # "l2":J
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "[search] handleSearchRequest- time-1 ="

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sub-long v16, v12, v8

    move-wide/from16 v0, v16

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 218
    .end local v12    # "l2":J
    :cond_7
    :try_start_3
    invoke-virtual {v10}, Lcom/navdy/hud/app/maps/here/HereLocationFixManager;->getLastGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v5

    .line 219
    .local v5, "geoPosition":Lcom/here/android/mpa/common/GeoCoordinate;
    if-nez v5, :cond_8

    .line 220
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "start location n/a"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 221
    sget-object v2, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_NO_LOCATION_SERVICE:Lcom/navdy/service/library/events/RequestStatus;

    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->context:Landroid/content/Context;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$200()Landroid/content/Context;

    move-result-object v3

    const v15, 0x7f0901d9

    invoke-virtual {v3, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->returnSearchErrorResponse(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V
    invoke-static {v4, v2, v3}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$300(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 267
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 268
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v12

    .line 269
    .restart local v12    # "l2":J
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "[search] handleSearchRequest- time-1 ="

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sub-long v16, v12, v8

    move-wide/from16 v0, v16

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 225
    .end local v12    # "l2":J
    :cond_8
    :try_start_4
    new-instance v2, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v9}, Lcom/navdy/hud/app/maps/here/HerePlacesManager$4$1;-><init>(Lcom/navdy/hud/app/maps/here/HerePlacesManager$4;Ljava/lang/String;Lcom/here/android/mpa/common/GeoCoordinate;IIJ)V

    invoke-static {v5, v4, v7, v6, v2}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->searchPlaces(Lcom/here/android/mpa/common/GeoCoordinate;Ljava/lang/String;IILcom/navdy/hud/app/maps/here/HerePlacesManager$PlacesSearchCallback;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 267
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 268
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v12

    .line 269
    .restart local v12    # "l2":J
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "[search] handleSearchRequest- time-1 ="

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sub-long v16, v12, v8

    move-wide/from16 v0, v16

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 263
    .end local v5    # "geoPosition":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v10    # "hereLocationFixManager":Lcom/navdy/hud/app/maps/here/HereLocationFixManager;
    .end local v12    # "l2":J
    :catch_0
    move-exception v14

    .line 264
    .local v14, "t":Ljava/lang/Throwable;
    :try_start_5
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    invoke-virtual {v2, v14}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 265
    sget-object v2, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_UNKNOWN_ERROR:Lcom/navdy/service/library/events/RequestStatus;

    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->context:Landroid/content/Context;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$200()Landroid/content/Context;

    move-result-object v3

    const v15, 0x7f0902da

    invoke-virtual {v3, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->returnSearchErrorResponse(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V
    invoke-static {v4, v2, v3}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$300(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 267
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 268
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v12

    .line 269
    .restart local v12    # "l2":J
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "[search] handleSearchRequest- time-1 ="

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sub-long v16, v12, v8

    move-wide/from16 v0, v16

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 267
    .end local v12    # "l2":J
    .end local v14    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v2

    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const/4 v15, 0x2

    invoke-virtual {v3, v15}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 268
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v12

    .line 269
    .restart local v12    # "l2":J
    # getter for: Lcom/navdy/hud/app/maps/here/HerePlacesManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "[search] handleSearchRequest- time-1 ="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sub-long v16, v12, v8

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v3, v15}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 270
    .end local v12    # "l2":J
    :cond_9
    throw v2
.end method
