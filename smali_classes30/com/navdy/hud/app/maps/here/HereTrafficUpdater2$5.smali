.class synthetic Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$5;
.super Ljava/lang/Object;
.source "HereTrafficUpdater2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$here$android$mpa$guidance$TrafficUpdater$Error:[I

.field static final synthetic $SwitchMap$com$here$android$mpa$mapping$TrafficEvent$Severity:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 411
    invoke-static {}, Lcom/here/android/mpa/mapping/TrafficEvent$Severity;->values()[Lcom/here/android/mpa/mapping/TrafficEvent$Severity;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$5;->$SwitchMap$com$here$android$mpa$mapping$TrafficEvent$Severity:[I

    :try_start_0
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$5;->$SwitchMap$com$here$android$mpa$mapping$TrafficEvent$Severity:[I

    sget-object v1, Lcom/here/android/mpa/mapping/TrafficEvent$Severity;->BLOCKING:Lcom/here/android/mpa/mapping/TrafficEvent$Severity;

    invoke-virtual {v1}, Lcom/here/android/mpa/mapping/TrafficEvent$Severity;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_3

    :goto_0
    :try_start_1
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$5;->$SwitchMap$com$here$android$mpa$mapping$TrafficEvent$Severity:[I

    sget-object v1, Lcom/here/android/mpa/mapping/TrafficEvent$Severity;->VERY_HIGH:Lcom/here/android/mpa/mapping/TrafficEvent$Severity;

    invoke-virtual {v1}, Lcom/here/android/mpa/mapping/TrafficEvent$Severity;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_2

    :goto_1
    :try_start_2
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$5;->$SwitchMap$com$here$android$mpa$mapping$TrafficEvent$Severity:[I

    sget-object v1, Lcom/here/android/mpa/mapping/TrafficEvent$Severity;->HIGH:Lcom/here/android/mpa/mapping/TrafficEvent$Severity;

    invoke-virtual {v1}, Lcom/here/android/mpa/mapping/TrafficEvent$Severity;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1

    .line 308
    :goto_2
    invoke-static {}, Lcom/here/android/mpa/guidance/TrafficUpdater$Error;->values()[Lcom/here/android/mpa/guidance/TrafficUpdater$Error;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$5;->$SwitchMap$com$here$android$mpa$guidance$TrafficUpdater$Error:[I

    :try_start_3
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereTrafficUpdater2$5;->$SwitchMap$com$here$android$mpa$guidance$TrafficUpdater$Error:[I

    sget-object v1, Lcom/here/android/mpa/guidance/TrafficUpdater$Error;->NONE:Lcom/here/android/mpa/guidance/TrafficUpdater$Error;

    invoke-virtual {v1}, Lcom/here/android/mpa/guidance/TrafficUpdater$Error;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_0

    :goto_3
    return-void

    :catch_0
    move-exception v0

    goto :goto_3

    .line 411
    :catch_1
    move-exception v0

    goto :goto_2

    :catch_2
    move-exception v0

    goto :goto_1

    :catch_3
    move-exception v0

    goto :goto_0
.end method
