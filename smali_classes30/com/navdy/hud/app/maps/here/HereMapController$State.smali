.class public final enum Lcom/navdy/hud/app/maps/here/HereMapController$State;
.super Ljava/lang/Enum;
.source "HereMapController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/here/HereMapController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/maps/here/HereMapController$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/maps/here/HereMapController$State;

.field public static final enum AR_MODE:Lcom/navdy/hud/app/maps/here/HereMapController$State;

.field public static final enum NONE:Lcom/navdy/hud/app/maps/here/HereMapController$State;

.field public static final enum OVERVIEW:Lcom/navdy/hud/app/maps/here/HereMapController$State;

.field public static final enum ROUTE_PICKER:Lcom/navdy/hud/app/maps/here/HereMapController$State;

.field public static final enum TRANSITION:Lcom/navdy/hud/app/maps/here/HereMapController$State;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 44
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereMapController$State;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/maps/here/HereMapController$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereMapController$State;->NONE:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    .line 47
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereMapController$State;

    const-string v1, "TRANSITION"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/maps/here/HereMapController$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereMapController$State;->TRANSITION:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    .line 51
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereMapController$State;

    const-string v1, "ROUTE_PICKER"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/maps/here/HereMapController$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereMapController$State;->ROUTE_PICKER:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    .line 56
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereMapController$State;

    const-string v1, "OVERVIEW"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/app/maps/here/HereMapController$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereMapController$State;->OVERVIEW:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    .line 62
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereMapController$State;

    const-string v1, "AR_MODE"

    invoke-direct {v0, v1, v6}, Lcom/navdy/hud/app/maps/here/HereMapController$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereMapController$State;->AR_MODE:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    .line 42
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/navdy/hud/app/maps/here/HereMapController$State;

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapController$State;->NONE:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapController$State;->TRANSITION:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapController$State;->ROUTE_PICKER:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapController$State;->OVERVIEW:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapController$State;->AR_MODE:Lcom/navdy/hud/app/maps/here/HereMapController$State;

    aput-object v1, v0, v6

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereMapController$State;->$VALUES:[Lcom/navdy/hud/app/maps/here/HereMapController$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/maps/here/HereMapController$State;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 42
    const-class v0, Lcom/navdy/hud/app/maps/here/HereMapController$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/maps/here/HereMapController$State;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/maps/here/HereMapController$State;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapController$State;->$VALUES:[Lcom/navdy/hud/app/maps/here/HereMapController$State;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/maps/here/HereMapController$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/maps/here/HereMapController$State;

    return-object v0
.end method
