.class public Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;
.super Ljava/lang/Object;
.source "HereMapImageGenerator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$MapGeneratorParams;,
        Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$ICallback;
    }
.end annotation


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static final singleton:Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;


# instance fields
.field private handler:Landroid/os/Handler;

.field private final map:Lcom/here/android/mpa/mapping/Map;

.field private final mapOffScreenRenderer:Lcom/here/android/mpa/mapping/MapOffScreenRenderer;

.field private renderRunning:Z

.field private waitForRender:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 42
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;

    invoke-direct {v0}, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->singleton:Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->waitForRender:Ljava/lang/Object;

    .line 52
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->handler:Landroid/os/Handler;

    .line 55
    new-instance v0, Lcom/here/android/mpa/mapping/Map;

    invoke-direct {v0}, Lcom/here/android/mpa/mapping/Map;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->map:Lcom/here/android/mpa/mapping/Map;

    .line 56
    new-instance v0, Lcom/here/android/mpa/mapping/MapOffScreenRenderer;

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/here/android/mpa/mapping/MapOffScreenRenderer;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->mapOffScreenRenderer:Lcom/here/android/mpa/mapping/MapOffScreenRenderer;

    .line 57
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->map:Lcom/here/android/mpa/mapping/Map;

    const-string v1, "terrain.day"

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/mapping/Map;->setMapScheme(Ljava/lang/String;)Lcom/here/android/mpa/mapping/Map;

    .line 58
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->map:Lcom/here/android/mpa/mapping/Map;

    invoke-virtual {v0, v2}, Lcom/here/android/mpa/mapping/Map;->setExtrudedBuildingsVisible(Z)Z

    .line 59
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->map:Lcom/here/android/mpa/mapping/Map;

    invoke-virtual {v0, v2}, Lcom/here/android/mpa/mapping/Map;->setFadingAnimations(Z)Lcom/here/android/mpa/mapping/Map;

    .line 60
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->map:Lcom/here/android/mpa/mapping/Map;

    invoke-virtual {v0}, Lcom/here/android/mpa/mapping/Map;->getPositionIndicator()Lcom/here/android/mpa/mapping/PositionIndicator;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/here/android/mpa/mapping/PositionIndicator;->setVisible(Z)Lcom/here/android/mpa/mapping/PositionIndicator;

    .line 61
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->mapOffScreenRenderer:Lcom/here/android/mpa/mapping/MapOffScreenRenderer;

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->map:Lcom/here/android/mpa/mapping/Map;

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/mapping/MapOffScreenRenderer;->setMap(Lcom/here/android/mpa/mapping/Map;)Lcom/here/android/mpa/mapping/MapOffScreenRenderer;

    .line 62
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->mapOffScreenRenderer:Lcom/here/android/mpa/mapping/MapOffScreenRenderer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/mapping/MapOffScreenRenderer;->setBlockingRendering(Z)Lcom/here/android/mpa/mapping/MapOffScreenRenderer;

    .line 63
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->stopOffscreenRenderer()V

    return-void
.end method

.method static synthetic access$100()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$MapGeneratorParams;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;
    .param p1, "x1"    # Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$MapGeneratorParams;
    .param p2, "x2"    # Landroid/graphics/Bitmap;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->saveFileToDisk(Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$MapGeneratorParams;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method private generateMapSnapshotInternal(Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$MapGeneratorParams;)V
    .locals 8
    .param p1, "mapGeneratorParams"    # Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$MapGeneratorParams;

    .prologue
    .line 103
    const/4 v1, 0x0

    .line 105
    .local v1, "started":Z
    :try_start_0
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "generateMapSnapshotInternal lat:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p1, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$MapGeneratorParams;->latitude:D

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " lon:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p1, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$MapGeneratorParams;->longitude:D

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 106
    new-instance v0, Lcom/here/android/mpa/common/GeoCoordinate;

    iget-wide v4, p1, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$MapGeneratorParams;->latitude:D

    iget-wide v6, p1, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$MapGeneratorParams;->longitude:D

    invoke-direct {v0, v4, v5, v6, v7}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    .line 107
    .local v0, "geoCoordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->map:Lcom/here/android/mpa/mapping/Map;

    sget-object v4, Lcom/here/android/mpa/mapping/Map$Animation;->NONE:Lcom/here/android/mpa/mapping/Map$Animation;

    invoke-virtual {v3, v0, v4}, Lcom/here/android/mpa/mapping/Map;->setCenter(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;)V

    .line 108
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->mapOffScreenRenderer:Lcom/here/android/mpa/mapping/MapOffScreenRenderer;

    iget v4, p1, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$MapGeneratorParams;->width:I

    iget v5, p1, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$MapGeneratorParams;->height:I

    invoke-virtual {v3, v4, v5}, Lcom/here/android/mpa/mapping/MapOffScreenRenderer;->setSize(II)Lcom/here/android/mpa/mapping/MapOffScreenRenderer;

    .line 109
    if-nez v1, :cond_0

    .line 110
    const/4 v1, 0x1

    .line 111
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->mapOffScreenRenderer:Lcom/here/android/mpa/mapping/MapOffScreenRenderer;

    invoke-virtual {v3}, Lcom/here/android/mpa/mapping/MapOffScreenRenderer;->start()V

    .line 113
    :cond_0
    const/4 v1, 0x1

    .line 114
    iget-object v3, p0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->mapOffScreenRenderer:Lcom/here/android/mpa/mapping/MapOffScreenRenderer;

    new-instance v4, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$1;

    invoke-direct {v4, p0, p1}, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$1;-><init>(Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$MapGeneratorParams;)V

    invoke-virtual {v3, v4}, Lcom/here/android/mpa/mapping/MapOffScreenRenderer;->getScreenCapture(Lcom/here/android/mpa/common/OnScreenCaptureListener;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 132
    .end local v0    # "geoCoordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    :cond_1
    :goto_0
    return-void

    .line 126
    :catch_0
    move-exception v2

    .line 127
    .local v2, "t":Ljava/lang/Throwable;
    sget-object v3, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "renderMapSnapshot"

    invoke-virtual {v3, v4, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 128
    if-eqz v1, :cond_1

    .line 129
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->stopOffscreenRenderer()V

    goto :goto_0
.end method

.method public static getInstance()Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->singleton:Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;

    return-object v0
.end method

.method private notifyWaitingRenders()V
    .locals 2

    .prologue
    .line 145
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->waitForRender:Ljava/lang/Object;

    monitor-enter v1

    .line 146
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->renderRunning:Z

    .line 147
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->waitForRender:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 148
    monitor-exit v1

    .line 149
    return-void

    .line 148
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private saveFileToDisk(Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$MapGeneratorParams;Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "mapGeneratorParams"    # Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$MapGeneratorParams;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 152
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$2;

    invoke-direct {v1, p0, p2, p1}, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$2;-><init>(Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;Landroid/graphics/Bitmap;Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$MapGeneratorParams;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 184
    return-void
.end method

.method private stopOffscreenRenderer()V
    .locals 2

    .prologue
    .line 136
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->mapOffScreenRenderer:Lcom/here/android/mpa/mapping/MapOffScreenRenderer;

    invoke-virtual {v1}, Lcom/here/android/mpa/mapping/MapOffScreenRenderer;->stop()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 140
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->notifyWaitingRenders()V

    .line 142
    :goto_0
    return-void

    .line 137
    :catch_0
    move-exception v0

    .line 138
    .local v0, "t":Ljava/lang/Throwable;
    :try_start_1
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 140
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->notifyWaitingRenders()V

    goto :goto_0

    .end local v0    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v1

    invoke-direct {p0}, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->notifyWaitingRenders()V

    throw v1
.end method


# virtual methods
.method public generateMapSnapshot(Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$MapGeneratorParams;)V
    .locals 2
    .param p1, "mapGeneratorParams"    # Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$MapGeneratorParams;

    .prologue
    .line 73
    if-nez p1, :cond_0

    .line 74
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 76
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    .line 77
    sget-object v0, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "map engine not initialized"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 78
    iget-object v0, p1, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$MapGeneratorParams;->callback:Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$ICallback;

    if-eqz v0, :cond_1

    .line 79
    iget-object v0, p1, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$MapGeneratorParams;->callback:Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$ICallback;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$ICallback;->result(Landroid/graphics/Bitmap;)V

    .line 100
    :cond_1
    return-void
.end method

.method public getMapImageFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 66
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/profile/DriverProfile;->getPlacesImageDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".jpg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
