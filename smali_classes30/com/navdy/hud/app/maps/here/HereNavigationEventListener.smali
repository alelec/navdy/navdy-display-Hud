.class Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;
.super Lcom/here/android/mpa/guidance/NavigationManager$NavigationManagerEventListener;
.source "HereNavigationEventListener.java"


# instance fields
.field private final hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

.field private final logger:Lcom/navdy/service/library/log/Logger;

.field private final tag:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/service/library/log/Logger;Ljava/lang/String;Lcom/navdy/hud/app/maps/here/HereNavigationManager;)V
    .locals 0
    .param p1, "logger"    # Lcom/navdy/service/library/log/Logger;
    .param p2, "tag"    # Ljava/lang/String;
    .param p3, "hereNavigationManager"    # Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/here/android/mpa/guidance/NavigationManager$NavigationManagerEventListener;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;->logger:Lcom/navdy/service/library/log/Logger;

    .line 24
    iput-object p2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;->tag:Ljava/lang/String;

    .line 25
    iput-object p3, p0, Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    .line 26
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;->tag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method


# virtual methods
.method public onEnded(Lcom/here/android/mpa/guidance/NavigationManager$NavigationMode;)V
    .locals 3
    .param p1, "mode"    # Lcom/here/android/mpa/guidance/NavigationManager$NavigationMode;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onEnded:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 48
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isLastManeuver()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onEnded: last maneuver on, marking arrived"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->arrived()V

    .line 52
    :cond_0
    return-void
.end method

.method public onNavigationModeChanged()V
    .locals 3

    .prologue
    .line 30
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/maps/here/HereNavigationEventListener$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/maps/here/HereNavigationEventListener$1;-><init>(Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;)V

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 38
    return-void
.end method

.method public onRouteUpdated(Lcom/here/android/mpa/routing/Route;)V
    .locals 3
    .param p1, "updatedRoute"    # Lcom/here/android/mpa/routing/Route;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereNavigationEventListener;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onRouteUpdated:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 43
    return-void
.end method
