.class Lcom/navdy/hud/app/maps/here/HereRerouteListener$3;
.super Ljava/lang/Object;
.source "HereRerouteListener.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/maps/here/HereRerouteListener;->onRerouteFailed()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/maps/here/HereRerouteListener;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/maps/here/HereRerouteListener;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/maps/here/HereRerouteListener;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereRerouteListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 102
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereRerouteListener;

    const/4 v1, 0x0

    # setter for: Lcom/navdy/hud/app/maps/here/HereRerouteListener;->routeCalculationOn:Z
    invoke-static {v0, v1}, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->access$402(Lcom/navdy/hud/app/maps/here/HereRerouteListener;Z)Z

    .line 103
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereRerouteListener;

    const/4 v1, 0x0

    # setter for: Lcom/navdy/hud/app/maps/here/HereRerouteListener;->routeCalcId:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->access$002(Lcom/navdy/hud/app/maps/here/HereRerouteListener;Ljava/lang/String;)Ljava/lang/String;

    .line 104
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRerouteListener;->logger:Lcom/navdy/service/library/log/Logger;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->access$300(Lcom/navdy/hud/app/maps/here/HereRerouteListener;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRerouteListener;->tag:Ljava/lang/String;
    invoke-static {v2}, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->access$200(Lcom/navdy/hud/app/maps/here/HereRerouteListener;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " reroute-failed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 105
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRerouteListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->access$100(Lcom/navdy/hud/app/maps/here/HereRerouteListener;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getNavigationSessionPreference()Lcom/navdy/hud/app/maps/NavSessionPreferences;

    move-result-object v0

    iget-boolean v0, v0, Lcom/navdy/hud/app/maps/NavSessionPreferences;->spokenTurnByTurn:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereRerouteListener;

    .line 106
    # getter for: Lcom/navdy/hud/app/maps/here/HereRerouteListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->access$100(Lcom/navdy/hud/app/maps/here/HereRerouteListener;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->hasArrived()Z

    move-result v0

    if-nez v0, :cond_0

    .line 107
    invoke-static {}, Lcom/navdy/hud/app/maps/MapSettings;->isTtsRecalculationDisabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRerouteListener;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->access$500(Lcom/navdy/hud/app/maps/here/HereRerouteListener;)Lcom/squareup/otto/Bus;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRerouteListener;->hereNavigationManager:Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-static {v1}, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->access$100(Lcom/navdy/hud/app/maps/here/HereRerouteListener;)Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v1

    iget-object v1, v1, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->TTS_REROUTING_FAILED:Lcom/navdy/hud/app/event/LocalSpeechRequest;

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/maps/here/HereRerouteListener$3;->this$0:Lcom/navdy/hud/app/maps/here/HereRerouteListener;

    # getter for: Lcom/navdy/hud/app/maps/here/HereRerouteListener;->bus:Lcom/squareup/otto/Bus;
    invoke-static {v0}, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->access$500(Lcom/navdy/hud/app/maps/here/HereRerouteListener;)Lcom/squareup/otto/Bus;

    move-result-object v0

    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRerouteListener;->REROUTE_FAILED:Lcom/navdy/hud/app/maps/MapEvents$RerouteEvent;

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 111
    return-void
.end method
