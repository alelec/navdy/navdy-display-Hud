.class public Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator;
.super Ljava/lang/Object;
.source "HereRouteViaGenerator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;
    }
.end annotation


# static fields
.field private static final COMMA:Ljava/lang/String; = ", "

.field private static final EMPTY:Ljava/lang/String; = ""

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static final viaBuilder:Ljava/lang/StringBuilder;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 30
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator;->viaBuilder:Ljava/lang/StringBuilder;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static buildDistanceListDesc(Ljava/util/List;Ljava/util/List;Ljava/util/concurrent/atomic/AtomicInteger;Ljava/lang/String;)Ljava/util/List;
    .locals 13
    .param p2, "orderId"    # Ljava/util/concurrent/atomic/AtomicInteger;
    .param p3, "requestId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/routing/RouteResult;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/routing/Maneuver;",
            ">;",
            "Ljava/util/concurrent/atomic/AtomicInteger;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 181
    .local p0, "results":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/RouteResult;>;"
    .local p1, "maneuvers":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/Maneuver;>;"
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 182
    .local v4, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 183
    .local v2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;>;"
    const/4 v1, 0x0

    .line 184
    .local v1, "index":I
    if-eqz p0, :cond_2

    .line 186
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_5

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/here/android/mpa/routing/RouteResult;

    .line 187
    .local v9, "routeResult":Lcom/here/android/mpa/routing/RouteResult;
    invoke-virtual {v9}, Lcom/here/android/mpa/routing/RouteResult;->getRoute()Lcom/here/android/mpa/routing/Route;

    move-result-object v11

    invoke-virtual {v11}, Lcom/here/android/mpa/routing/Route;->getRouteElements()Lcom/here/android/mpa/routing/RouteElements;

    move-result-object v11

    invoke-virtual {v11}, Lcom/here/android/mpa/routing/RouteElements;->getElements()Ljava/util/List;

    move-result-object v8

    .line 188
    .local v8, "routeElements":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/RouteElement;>;"
    const/4 v0, 0x1

    .line 189
    .local v0, "counter":I
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_1

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/here/android/mpa/routing/RouteElement;

    .line 190
    .local v7, "routeElement":Lcom/here/android/mpa/routing/RouteElement;
    invoke-virtual {v7}, Lcom/here/android/mpa/routing/RouteElement;->getRoadElement()Lcom/here/android/mpa/common/RoadElement;

    move-result-object v5

    .line 191
    .local v5, "roadElement":Lcom/here/android/mpa/common/RoadElement;
    invoke-static {v5, v4, v1, p2}, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator;->processRoadElement(Lcom/here/android/mpa/common/RoadElement;Ljava/util/HashMap;ILjava/util/concurrent/atomic/AtomicInteger;)V

    .line 192
    rem-int/lit8 v12, v0, 0x5

    if-nez v12, :cond_0

    if-eqz p3, :cond_0

    invoke-static/range {p3 .. p3}, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator;->isRouteCancelled(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 213
    .end local v0    # "counter":I
    .end local v5    # "roadElement":Lcom/here/android/mpa/common/RoadElement;
    .end local v7    # "routeElement":Lcom/here/android/mpa/routing/RouteElement;
    .end local v8    # "routeElements":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/RouteElement;>;"
    .end local v9    # "routeResult":Lcom/here/android/mpa/routing/RouteResult;
    :goto_2
    return-object v2

    .line 195
    .restart local v0    # "counter":I
    .restart local v5    # "roadElement":Lcom/here/android/mpa/common/RoadElement;
    .restart local v7    # "routeElement":Lcom/here/android/mpa/routing/RouteElement;
    .restart local v8    # "routeElements":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/RouteElement;>;"
    .restart local v9    # "routeResult":Lcom/here/android/mpa/routing/RouteResult;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 196
    goto :goto_1

    .line 197
    .end local v5    # "roadElement":Lcom/here/android/mpa/common/RoadElement;
    .end local v7    # "routeElement":Lcom/here/android/mpa/routing/RouteElement;
    :cond_1
    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 198
    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    .line 199
    add-int/lit8 v1, v1, 0x1

    .line 200
    goto :goto_0

    .line 203
    .end local v0    # "counter":I
    .end local v8    # "routeElements":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/RouteElement;>;"
    .end local v9    # "routeResult":Lcom/here/android/mpa/routing/RouteResult;
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_4

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/here/android/mpa/routing/Maneuver;

    .line 204
    .local v3, "maneuver":Lcom/here/android/mpa/routing/Maneuver;
    invoke-virtual {v3}, Lcom/here/android/mpa/routing/Maneuver;->getRoadElements()Ljava/util/List;

    move-result-object v6

    .line 205
    .local v6, "roadElements":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/common/RoadElement;>;"
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/here/android/mpa/common/RoadElement;

    .line 206
    .restart local v5    # "roadElement":Lcom/here/android/mpa/common/RoadElement;
    invoke-static {v5, v4, v1, p2}, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator;->processRoadElement(Lcom/here/android/mpa/common/RoadElement;Ljava/util/HashMap;ILjava/util/concurrent/atomic/AtomicInteger;)V

    goto :goto_3

    .line 209
    .end local v3    # "maneuver":Lcom/here/android/mpa/routing/Maneuver;
    .end local v5    # "roadElement":Lcom/here/android/mpa/common/RoadElement;
    .end local v6    # "roadElements":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/common/RoadElement;>;"
    :cond_4
    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v10

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 212
    :cond_5
    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    goto :goto_2
.end method

.method public static generateVia(Ljava/util/List;Ljava/lang/String;)[Ljava/lang/String;
    .locals 26
    .param p1, "requestId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/routing/RouteResult;",
            ">;",
            "Ljava/lang/String;",
            ")[",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 102
    .local p0, "results":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/RouteResult;>;"
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "generating via for "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " routes"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 103
    new-instance v14, Ljava/util/concurrent/atomic/AtomicInteger;

    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-direct {v14, v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 104
    .local v14, "orderId":Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    .line 105
    .local v10, "l1":J
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v12

    .line 106
    .local v12, "len":I
    const/16 v19, 0x0

    .line 107
    .local v19, "viaIndex":I
    new-array v0, v12, [Ljava/lang/String;

    move-object/from16 v20, v0

    .line 108
    .local v20, "viaList":[Ljava/lang/String;
    new-array v0, v12, [D

    move-object/from16 v18, v0

    .line 112
    .local v18, "viaDistance":[D
    const/16 v21, 0x0

    :try_start_0
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, p1

    invoke-static {v0, v1, v14, v2}, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator;->buildDistanceListDesc(Ljava/util/List;Ljava/util/List;Ljava/util/concurrent/atomic/AtomicInteger;Ljava/lang/String;)Ljava/util/List;

    move-result-object v13

    .line 113
    .local v13, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;>;"
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/16 v22, 0x2

    invoke-virtual/range {v21 .. v22}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v21

    if-eqz v21, :cond_0

    .line 114
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v21

    :goto_0
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_0

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;

    .line 115
    .local v5, "container":Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;
    sget-object v22, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "["

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    iget v0, v5, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;->index:I

    move/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, "] road["

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    iget-object v0, v5, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;->via:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, "] distance["

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    iget-wide v0, v5, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;->val:D

    move-wide/from16 v24, v0

    invoke-virtual/range {v23 .. v25}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, "]"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 166
    .end local v5    # "container":Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;
    .end local v13    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;>;"
    :catch_0
    move-exception v16

    .line 167
    .local v16, "t":Ljava/lang/Throwable;
    :try_start_1
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator;->sLogger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 169
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "via algo took["

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v24

    sub-long v24, v24, v10

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "] len["

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "]"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 170
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v8, v0, :cond_9

    .line 171
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "via["

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    add-int/lit8 v23, v8, 0x1

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "] ["

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    aget-object v23, v20, v8

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "]["

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    aget-wide v24, v18, v8

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "]"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 170
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 118
    .end local v8    # "i":I
    .end local v16    # "t":Ljava/lang/Throwable;
    .restart local v13    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;>;"
    :cond_0
    :try_start_2
    new-instance v15, Ljava/util/HashSet;

    invoke-direct {v15}, Ljava/util/HashSet;-><init>()V

    .line 119
    .local v15, "seenVias":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    :goto_2
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v21

    if-lez v21, :cond_1

    .line 120
    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v21, v0
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move/from16 v0, v19

    move/from16 v1, v21

    if-ne v0, v1, :cond_2

    .line 169
    :cond_1
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "via algo took["

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v24

    sub-long v24, v24, v10

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "] len["

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "]"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 170
    const/4 v8, 0x0

    .restart local v8    # "i":I
    :goto_3
    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v8, v0, :cond_9

    .line 171
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "via["

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    add-int/lit8 v23, v8, 0x1

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "] ["

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    aget-object v23, v20, v8

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "]["

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    aget-wide v24, v18, v8

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "]"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 170
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 124
    .end local v8    # "i":I
    :cond_2
    :try_start_3
    invoke-static/range {p1 .. p1}, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator;->isRouteCancelled(Ljava/lang/String;)Z

    move-result v21

    if-nez v21, :cond_1

    .line 127
    const/16 v17, 0x0

    .line 128
    .local v17, "tmpIndex":I
    const/4 v6, -0x1

    .line 130
    .local v6, "currentIndex":I
    :goto_4
    move/from16 v0, v17

    invoke-interface {v13, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;

    .line 131
    .restart local v5    # "container":Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;
    const/16 v21, -0x1

    move/from16 v0, v21

    if-ne v6, v0, :cond_5

    .line 132
    iget v6, v5, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;->index:I

    .line 142
    :cond_3
    iget-object v0, v5, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;->via:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_7

    .line 143
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "already seen["

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    iget-object v0, v5, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;->via:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "]"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 144
    add-int/lit8 v21, v17, 0x1

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v22

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_6

    .line 145
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v22, "no more roadelements,use current"

    invoke-virtual/range {v21 .. v22}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 157
    :goto_5
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .line 158
    .local v9, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;>;"
    :cond_4
    :goto_6
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_8

    .line 159
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;

    .line 160
    .local v7, "d":Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;
    iget v0, v7, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;->index:I

    move/from16 v21, v0

    move/from16 v0, v21

    move/from16 v1, v19

    if-ne v0, v1, :cond_4

    .line 161
    invoke-interface {v9}, Ljava/util/Iterator;->remove()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_6

    .line 169
    .end local v5    # "container":Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;
    .end local v6    # "currentIndex":I
    .end local v7    # "d":Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;
    .end local v9    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;>;"
    .end local v13    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;>;"
    .end local v15    # "seenVias":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v17    # "tmpIndex":I
    :catchall_0
    move-exception v21

    sget-object v21, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "via algo took["

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v24

    sub-long v24, v24, v10

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "] len["

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "]"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 170
    const/4 v8, 0x0

    .restart local v8    # "i":I
    :goto_7
    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v8, v0, :cond_9

    .line 171
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "via["

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    add-int/lit8 v23, v8, 0x1

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "] ["

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    aget-object v23, v20, v8

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "]["

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    aget-wide v24, v18, v8

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "]"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 170
    add-int/lit8 v8, v8, 0x1

    goto :goto_7

    .line 134
    .end local v8    # "i":I
    .restart local v5    # "container":Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;
    .restart local v6    # "currentIndex":I
    .restart local v13    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;>;"
    .restart local v15    # "seenVias":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    .restart local v17    # "tmpIndex":I
    :cond_5
    :try_start_4
    iget v0, v5, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;->index:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-eq v6, v0, :cond_3

    .line 135
    sget-object v21, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v22, "no more roadelements,go back"

    invoke-virtual/range {v21 .. v22}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 136
    add-int/lit8 v21, v17, -0x1

    move/from16 v0, v21

    invoke-interface {v13, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;

    .line 137
    .local v4, "c":Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;
    iget-object v0, v4, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;->via:Ljava/lang/String;

    move-object/from16 v21, v0

    aput-object v21, v20, v19

    .line 138
    iget-wide v0, v4, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;->val:D

    move-wide/from16 v22, v0

    aput-wide v22, v18, v19

    goto/16 :goto_5

    .line 148
    .end local v4    # "c":Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;
    :cond_6
    add-int/lit8 v17, v17, 0x1

    .line 149
    goto/16 :goto_4

    .line 152
    :cond_7
    iget v0, v5, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;->index:I

    move/from16 v21, v0

    iget-object v0, v5, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;->via:Ljava/lang/String;

    move-object/from16 v22, v0

    aput-object v22, v20, v21

    .line 153
    iget v0, v5, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;->index:I

    move/from16 v21, v0

    iget-wide v0, v5, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;->val:D

    move-wide/from16 v22, v0

    aput-wide v22, v18, v21

    .line 154
    iget-object v0, v5, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;->via:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_5

    .line 164
    .restart local v9    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;>;"
    :cond_8
    add-int/lit8 v19, v19, 0x1

    .line 165
    goto/16 :goto_2

    .line 173
    .end local v5    # "container":Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;
    .end local v6    # "currentIndex":I
    .end local v9    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;>;"
    .end local v13    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;>;"
    .end local v15    # "seenVias":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v17    # "tmpIndex":I
    .restart local v8    # "i":I
    :cond_9
    return-object v20
.end method

.method private static getDisplayString(Lcom/here/android/mpa/common/RoadElement;)Ljava/lang/String;
    .locals 2
    .param p0, "roadElement"    # Lcom/here/android/mpa/common/RoadElement;

    .prologue
    .line 233
    if-nez p0, :cond_1

    .line 234
    const/4 v0, 0x0

    .line 240
    :cond_0
    :goto_0
    return-object v0

    .line 236
    :cond_1
    invoke-virtual {p0}, Lcom/here/android/mpa/common/RoadElement;->getRouteName()Ljava/lang/String;

    move-result-object v0

    .line 237
    .local v0, "str":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 238
    invoke-virtual {p0}, Lcom/here/android/mpa/common/RoadElement;->getRoadName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getViaString(Lcom/here/android/mpa/routing/Route;)Ljava/lang/String;
    .locals 1
    .param p0, "route"    # Lcom/here/android/mpa/routing/Route;

    .prologue
    .line 71
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator;->getViaString(Lcom/here/android/mpa/routing/Route;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getViaString(Lcom/here/android/mpa/routing/Route;I)Ljava/lang/String;
    .locals 9
    .param p0, "route"    # Lcom/here/android/mpa/routing/Route;
    .param p1, "elements"    # I

    .prologue
    const/4 v5, 0x0

    .line 75
    new-instance v3, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v3, v5}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 76
    .local v3, "orderId":Ljava/util/concurrent/atomic/AtomicInteger;
    sget-object v6, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator;->viaBuilder:Ljava/lang/StringBuilder;

    monitor-enter v6

    .line 77
    :try_start_0
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator;->viaBuilder:Ljava/lang/StringBuilder;

    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 78
    if-eqz p0, :cond_1

    .line 79
    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/here/android/mpa/routing/Route;->getManeuvers()Ljava/util/List;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v5, v7, v3, v8}, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator;->buildDistanceListDesc(Ljava/util/List;Ljava/util/List;Ljava/util/concurrent/atomic/AtomicInteger;Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 80
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;>;"
    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_1

    .line 81
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    .line 82
    .local v1, "len":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 83
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator;->viaBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-lez v5, :cond_0

    .line 84
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator;->viaBuilder:Ljava/lang/StringBuilder;

    const-string v7, ", "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    :cond_0
    sget-object v7, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator;->viaBuilder:Ljava/lang/StringBuilder;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;

    iget-object v5, v5, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;->via:Ljava/lang/String;

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    add-int/lit8 p1, p1, -0x1

    .line 88
    if-nez p1, :cond_2

    .line 94
    .end local v0    # "i":I
    .end local v1    # "len":I
    .end local v2    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;>;"
    :cond_1
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator;->viaBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 95
    .local v4, "viaStr":Ljava/lang/String;
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator;->viaBuilder:Ljava/lang/StringBuilder;

    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 96
    sget-object v5, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getViaString["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 97
    monitor-exit v6

    return-object v4

    .line 82
    .end local v4    # "viaStr":Ljava/lang/String;
    .restart local v0    # "i":I
    .restart local v1    # "len":I
    .restart local v2    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;>;"
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 98
    .end local v0    # "i":I
    .end local v1    # "len":I
    .end local v2    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;>;"
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5
.end method

.method private static isRouteCancelled(Ljava/lang/String;)Z
    .locals 4
    .param p0, "requestId"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 244
    if-nez p0, :cond_1

    .line 252
    :cond_0
    :goto_0
    return v1

    .line 247
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->getActiveRouteCalcId()Ljava/lang/String;

    move-result-object v0

    .line 248
    .local v0, "activeRouteCalcId":Ljava/lang/String;
    invoke-static {p0, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 249
    sget-object v1, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "route request ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] is not active anymore, current ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 250
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private static processRoadElement(Lcom/here/android/mpa/common/RoadElement;Ljava/util/HashMap;ILjava/util/concurrent/atomic/AtomicInteger;)V
    .locals 8
    .param p0, "roadElement"    # Lcom/here/android/mpa/common/RoadElement;
    .param p2, "index"    # I
    .param p3, "orderId"    # Ljava/util/concurrent/atomic/AtomicInteger;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/here/android/mpa/common/RoadElement;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;",
            ">;I",
            "Ljava/util/concurrent/atomic/AtomicInteger;",
            ")V"
        }
    .end annotation

    .prologue
    .line 219
    .local p1, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;>;"
    invoke-static {p0}, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator;->getDisplayString(Lcom/here/android/mpa/common/RoadElement;)Ljava/lang/String;

    move-result-object v6

    .line 220
    .local v6, "name":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 230
    :goto_0
    return-void

    .line 224
    :cond_0
    invoke-virtual {p1, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;

    .line 225
    .local v7, "distance":Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;
    if-nez v7, :cond_1

    .line 226
    new-instance v0, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;

    invoke-virtual {p0}, Lcom/here/android/mpa/common/RoadElement;->getGeometryLength()D

    move-result-wide v1

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v3

    int-to-long v4, v3

    move v3, p2

    invoke-direct/range {v0 .. v6}, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;-><init>(DIJLjava/lang/String;)V

    invoke-virtual {p1, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 228
    :cond_1
    iget-wide v0, v7, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;->val:D

    invoke-virtual {p0}, Lcom/here/android/mpa/common/RoadElement;->getGeometryLength()D

    move-result-wide v2

    add-double/2addr v0, v2

    iput-wide v0, v7, Lcom/navdy/hud/app/maps/here/HereRouteViaGenerator$DistanceContainer;->val:D

    goto :goto_0
.end method
