.class public final enum Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;
.super Ljava/lang/Enum;
.source "MapEvents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/MapEvents$LaneData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Position"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;

.field public static final enum OFF_ROUTE:Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;

.field public static final enum ON_ROUTE:Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 517
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;

    const-string v1, "ON_ROUTE"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;->ON_ROUTE:Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;

    .line 518
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;

    const-string v1, "OFF_ROUTE"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;->OFF_ROUTE:Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;

    .line 516
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;->ON_ROUTE:Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;->OFF_ROUTE:Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;->$VALUES:[Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 516
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 516
    const-class v0, Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;
    .locals 1

    .prologue
    .line 516
    sget-object v0, Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;->$VALUES:[Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/maps/MapEvents$LaneData$Position;

    return-object v0
.end method
