.class public final enum Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;
.super Ljava/lang/Enum;
.source "MapEvents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/MapEvents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RouteCalculationState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

.field public static final enum ABORT_NAVIGATION:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

.field public static final enum FINDING_NAV_COORDINATES:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

.field public static final enum IN_PROGRESS:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

.field public static final enum NONE:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

.field public static final enum STARTED:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

.field public static final enum STOPPED:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 372
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;->NONE:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    .line 373
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    const-string v1, "STARTED"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;->STARTED:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    .line 374
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    const-string v1, "STOPPED"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;->STOPPED:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    .line 375
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    const-string v1, "IN_PROGRESS"

    invoke-direct {v0, v1, v6}, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;->IN_PROGRESS:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    .line 376
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    const-string v1, "ABORT_NAVIGATION"

    invoke-direct {v0, v1, v7}, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;->ABORT_NAVIGATION:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    .line 377
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    const-string v1, "FINDING_NAV_COORDINATES"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;->FINDING_NAV_COORDINATES:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    .line 371
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;->NONE:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;->STARTED:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;->STOPPED:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;->IN_PROGRESS:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;->ABORT_NAVIGATION:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;->FINDING_NAV_COORDINATES:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;->$VALUES:[Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 371
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 371
    const-class v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;
    .locals 1

    .prologue
    .line 371
    sget-object v0, Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;->$VALUES:[Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

    return-object v0
.end method
