.class public final enum Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;
.super Ljava/lang/Enum;
.source "MapEvents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;

.field public static final enum IN:Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;

.field public static final enum OUT:Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 284
    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;

    const-string v1, "IN"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;->IN:Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;

    new-instance v0, Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;

    const-string v1, "OUT"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;->OUT:Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;->IN:Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;->OUT:Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;->$VALUES:[Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 284
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 284
    const-class v0, Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;
    .locals 1

    .prologue
    .line 284
    sget-object v0, Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;->$VALUES:[Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/maps/MapEvents$DialMapZoom$Type;

    return-object v0
.end method
