.class public Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;
.super Ljava/lang/Object;
.source "MapEvents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/MapEvents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DisplayTrafficIncident"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;,
        Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;
    }
.end annotation


# static fields
.field public static final ACCIDENT:Ljava/lang/String; = "ACCIDENT"

.field public static final CLOSURE:Ljava/lang/String; = "CLOSURE"

.field public static final CONGESTION:Ljava/lang/String; = "CONGESTION"

.field public static final FLOW:Ljava/lang/String; = "FLOW"

.field public static final OTHER:Ljava/lang/String; = "OTHER"

.field public static final ROADWORKS:Ljava/lang/String; = "ROADWORKS"

.field public static final UNDEFINED:Ljava/lang/String; = "UNDEFINED"


# instance fields
.field public affectedStreet:Ljava/lang/String;

.field public category:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

.field public description:Ljava/lang/String;

.field public distanceToIncident:J

.field public icon:Lcom/here/android/mpa/common/Image;

.field public reported:Ljava/util/Date;

.field public title:Ljava/lang/String;

.field public type:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

.field public updated:Ljava/util/Date;


# direct methods
.method public constructor <init>(Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;)V
    .locals 0
    .param p1, "type"    # Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;
    .param p2, "category"    # Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    .prologue
    .line 488
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 489
    iput-object p1, p0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;->type:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

    .line 490
    iput-object p2, p0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;->category:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    .line 491
    return-void
.end method

.method public constructor <init>(Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/util/Date;Ljava/util/Date;Lcom/here/android/mpa/common/Image;)V
    .locals 0
    .param p1, "type"    # Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;
    .param p2, "category"    # Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "description"    # Ljava/lang/String;
    .param p5, "affectedStreet"    # Ljava/lang/String;
    .param p6, "distanceToIncident"    # J
    .param p8, "reported"    # Ljava/util/Date;
    .param p9, "updated"    # Ljava/util/Date;
    .param p10, "icon"    # Lcom/here/android/mpa/common/Image;

    .prologue
    .line 501
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 502
    iput-object p1, p0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;->type:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Type;

    .line 503
    iput-object p2, p0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;->category:Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident$Category;

    .line 504
    iput-object p3, p0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;->title:Ljava/lang/String;

    .line 505
    iput-object p4, p0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;->description:Ljava/lang/String;

    .line 506
    iput-object p5, p0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;->affectedStreet:Ljava/lang/String;

    .line 507
    iput-wide p6, p0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;->distanceToIncident:J

    .line 508
    iput-object p8, p0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;->reported:Ljava/util/Date;

    .line 509
    iput-object p9, p0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;->updated:Ljava/util/Date;

    .line 510
    iput-object p10, p0, Lcom/navdy/hud/app/maps/MapEvents$DisplayTrafficIncident;->icon:Lcom/here/android/mpa/common/Image;

    .line 511
    return-void
.end method
