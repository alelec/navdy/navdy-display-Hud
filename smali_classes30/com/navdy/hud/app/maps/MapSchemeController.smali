.class public Lcom/navdy/hud/app/maps/MapSchemeController;
.super Ljava/lang/Object;
.source "MapSchemeController.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;
    }
.end annotation


# static fields
.field private static final AVERAGING_PERIOD:I = 0x12c

.field private static final DELAY_MILLIS:J = 0x2710L

.field private static final HI_THRESHOLD:F = 600.0f

.field private static final LO_THRESHOLD:F = 200.0f

.field public static final MAP_SCHEME_CARNAV_DAY:Ljava/lang/String; = "carnav.day"

.field public static final MAP_SCHEME_CARNAV_NIGHT:Ljava/lang/String; = "carnav.day"

.field public static final MESSAGE_PERIODIC:I

.field public static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static final singleton:Lcom/navdy/hud/app/maps/MapSchemeController;


# instance fields
.field private context:Landroid/content/Context;

.field private currentIdx:I

.field private lastSensorValue:F

.field private light:Landroid/hardware/Sensor;

.field private myHandler:Landroid/os/Handler;

.field private ringBufferFull:Z

.field private sensorManager:Landroid/hardware/SensorManager;

.field private sensorValues:[F

.field sharedPreferences:Landroid/content/SharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 28
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/maps/MapSchemeController;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/maps/MapSchemeController;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 30
    new-instance v0, Lcom/navdy/hud/app/maps/MapSchemeController;

    invoke-direct {v0}, Lcom/navdy/hud/app/maps/MapSchemeController;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/maps/MapSchemeController;->singleton:Lcom/navdy/hud/app/maps/MapSchemeController;

    return-void
.end method

.method public constructor <init>()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const/high16 v3, 0x43c80000    # 400.0f

    iput v3, p0, Lcom/navdy/hud/app/maps/MapSchemeController;->lastSensorValue:F

    .line 51
    const/16 v3, 0x1e

    new-array v3, v3, [F

    iput-object v3, p0, Lcom/navdy/hud/app/maps/MapSchemeController;->sensorValues:[F

    .line 52
    iput v6, p0, Lcom/navdy/hud/app/maps/MapSchemeController;->currentIdx:I

    .line 53
    iput-boolean v6, p0, Lcom/navdy/hud/app/maps/MapSchemeController;->ringBufferFull:Z

    .line 73
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/hud/app/maps/MapSchemeController;->context:Landroid/content/Context;

    .line 74
    iget-object v3, p0, Lcom/navdy/hud/app/maps/MapSchemeController;->context:Landroid/content/Context;

    invoke-static {v3, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 76
    iget-object v3, p0, Lcom/navdy/hud/app/maps/MapSchemeController;->context:Landroid/content/Context;

    const-string v4, "sensor"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/hardware/SensorManager;

    iput-object v3, p0, Lcom/navdy/hud/app/maps/MapSchemeController;->sensorManager:Landroid/hardware/SensorManager;

    .line 77
    iget-object v3, p0, Lcom/navdy/hud/app/maps/MapSchemeController;->sensorManager:Landroid/hardware/SensorManager;

    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Landroid/hardware/SensorManager;->getSensorList(I)Ljava/util/List;

    move-result-object v0

    .line 78
    .local v0, "deviceSensors":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Sensor;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 79
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/Sensor;

    .line 80
    .local v2, "sensor":Landroid/hardware/Sensor;
    sget-object v3, Lcom/navdy/hud/app/maps/MapSchemeController;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sensor: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Landroid/hardware/Sensor;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 81
    invoke-virtual {v2}, Landroid/hardware/Sensor;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "tsl2572"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 82
    iput-object v2, p0, Lcom/navdy/hud/app/maps/MapSchemeController;->light:Landroid/hardware/Sensor;

    .line 78
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 85
    .end local v2    # "sensor":Landroid/hardware/Sensor;
    :cond_1
    sget-object v3, Lcom/navdy/hud/app/maps/MapSchemeController;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "light sensor selected: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/maps/MapSchemeController;->light:Landroid/hardware/Sensor;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 86
    iget-object v3, p0, Lcom/navdy/hud/app/maps/MapSchemeController;->sensorManager:Landroid/hardware/SensorManager;

    iget-object v4, p0, Lcom/navdy/hud/app/maps/MapSchemeController;->light:Landroid/hardware/Sensor;

    const/4 v5, 0x3

    invoke-virtual {v3, p0, v4, v5}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 88
    new-instance v3, Lcom/navdy/hud/app/maps/MapSchemeController$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v3, p0, v4}, Lcom/navdy/hud/app/maps/MapSchemeController$1;-><init>(Lcom/navdy/hud/app/maps/MapSchemeController;Landroid/os/Looper;)V

    iput-object v3, p0, Lcom/navdy/hud/app/maps/MapSchemeController;->myHandler:Landroid/os/Handler;

    .line 99
    iget-object v3, p0, Lcom/navdy/hud/app/maps/MapSchemeController;->myHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x2710

    invoke-virtual {v3, v6, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 100
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/maps/MapSchemeController;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/MapSchemeController;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/MapSchemeController;->collectLightSensorSample()V

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/maps/MapSchemeController;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/maps/MapSchemeController;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/navdy/hud/app/maps/MapSchemeController;->evaluateCollectedSamples()V

    return-void
.end method

.method private collectLightSensorSample()V
    .locals 3

    .prologue
    .line 103
    iget-object v0, p0, Lcom/navdy/hud/app/maps/MapSchemeController;->sensorValues:[F

    iget v1, p0, Lcom/navdy/hud/app/maps/MapSchemeController;->currentIdx:I

    iget v2, p0, Lcom/navdy/hud/app/maps/MapSchemeController;->lastSensorValue:F

    aput v2, v0, v1

    .line 104
    iget v0, p0, Lcom/navdy/hud/app/maps/MapSchemeController;->currentIdx:I

    iget-object v1, p0, Lcom/navdy/hud/app/maps/MapSchemeController;->sensorValues:[F

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    .line 105
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/maps/MapSchemeController;->ringBufferFull:Z

    .line 107
    :cond_0
    iget v0, p0, Lcom/navdy/hud/app/maps/MapSchemeController;->currentIdx:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/navdy/hud/app/maps/MapSchemeController;->sensorValues:[F

    array-length v1, v1

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/navdy/hud/app/maps/MapSchemeController;->currentIdx:I

    .line 108
    return-void
.end method

.method private evaluateCollectedSamples()V
    .locals 6

    .prologue
    .line 111
    const/4 v2, 0x0

    .line 112
    .local v2, "s":F
    iget-boolean v3, p0, Lcom/navdy/hud/app/maps/MapSchemeController;->ringBufferFull:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/navdy/hud/app/maps/MapSchemeController;->sensorValues:[F

    array-length v1, v3

    .line 113
    .local v1, "l":I
    :goto_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_1

    .line 114
    iget-object v3, p0, Lcom/navdy/hud/app/maps/MapSchemeController;->sensorValues:[F

    aget v3, v3, v0

    add-float/2addr v2, v3

    .line 113
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 112
    .end local v0    # "i":I
    .end local v1    # "l":I
    :cond_0
    iget v1, p0, Lcom/navdy/hud/app/maps/MapSchemeController;->currentIdx:I

    goto :goto_0

    .line 116
    .restart local v0    # "i":I
    .restart local v1    # "l":I
    :cond_1
    int-to-float v3, v1

    div-float/2addr v2, v3

    .line 117
    sget-object v3, Lcom/navdy/hud/app/maps/MapSchemeController;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "evaluateCollectedSamples: avg lux: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 118
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/MapSchemeController;->getActiveScheme()Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;->DAY:Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;

    if-ne v3, v4, :cond_3

    const/high16 v3, 0x43480000    # 200.0f

    cmpg-float v3, v2, v3

    if-gez v3, :cond_3

    .line 119
    sget-object v3, Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;->NIGHT:Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;

    invoke-direct {p0, v3}, Lcom/navdy/hud/app/maps/MapSchemeController;->switchMapScheme(Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;)V

    .line 123
    :cond_2
    :goto_2
    return-void

    .line 120
    :cond_3
    invoke-virtual {p0}, Lcom/navdy/hud/app/maps/MapSchemeController;->getActiveScheme()Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;

    move-result-object v3

    sget-object v4, Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;->NIGHT:Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;

    if-ne v3, v4, :cond_2

    const/high16 v3, 0x44160000    # 600.0f

    cmpl-float v3, v2, v3

    if-lez v3, :cond_2

    .line 121
    sget-object v3, Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;->DAY:Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;

    invoke-direct {p0, v3}, Lcom/navdy/hud/app/maps/MapSchemeController;->switchMapScheme(Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;)V

    goto :goto_2
.end method

.method public static getInstance()Lcom/navdy/hud/app/maps/MapSchemeController;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/navdy/hud/app/maps/MapSchemeController;->singleton:Lcom/navdy/hud/app/maps/MapSchemeController;

    return-object v0
.end method

.method private switchMapScheme(Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;)V
    .locals 4
    .param p1, "scheme"    # Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;

    .prologue
    .line 126
    iget-object v1, p0, Lcom/navdy/hud/app/maps/MapSchemeController;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "map.scheme"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 127
    .local v0, "schemeName":Ljava/lang/String;
    const-string v1, "carnav.day"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;->NIGHT:Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;

    if-ne p1, v1, :cond_1

    .line 128
    const-string v0, "carnav.day"

    .line 132
    :cond_0
    :goto_0
    sget-object v1, Lcom/navdy/hud/app/maps/MapSchemeController;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "changing map scheme to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 133
    iget-object v1, p0, Lcom/navdy/hud/app/maps/MapSchemeController;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "map.scheme"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 134
    return-void

    .line 129
    :cond_1
    const-string v1, "carnav.day"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;->DAY:Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;

    if-ne p1, v1, :cond_0

    .line 130
    const-string v0, "carnav.day"

    goto :goto_0
.end method


# virtual methods
.method getActiveScheme()Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;
    .locals 4

    .prologue
    .line 64
    iget-object v1, p0, Lcom/navdy/hud/app/maps/MapSchemeController;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "map.scheme"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 65
    .local v0, "schemeName":Ljava/lang/String;
    const-string v1, "carnav.day"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 66
    sget-object v1, Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;->DAY:Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;

    .line 68
    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;->NIGHT:Lcom/navdy/hud/app/maps/MapSchemeController$MapScheme;

    goto :goto_0
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 3
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 147
    sget-object v0, Lcom/navdy/hud/app/maps/MapSchemeController;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onAccuracyChanged: accuracy: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 148
    return-void
.end method

.method public final onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 138
    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v2, 0x0

    aget v0, v1, v2

    .line 139
    .local v0, "lux":F
    sget-object v1, Lcom/navdy/hud/app/maps/MapSchemeController;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 140
    sget-object v1, Lcom/navdy/hud/app/maps/MapSchemeController;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onSensorChanged: lux: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 142
    :cond_0
    iput v0, p0, Lcom/navdy/hud/app/maps/MapSchemeController;->lastSensorValue:F

    .line 143
    return-void
.end method
