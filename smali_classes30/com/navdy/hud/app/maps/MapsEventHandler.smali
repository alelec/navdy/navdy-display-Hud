.class public final Lcom/navdy/hud/app/maps/MapsEventHandler;
.super Ljava/lang/Object;
.source "MapsEventHandler.java"


# static fields
.field private static final VERBOSE:Z

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static final sSingleton:Lcom/navdy/hud/app/maps/MapsEventHandler;


# instance fields
.field bus:Lcom/squareup/otto/Bus;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field connectionHandler:Lcom/navdy/hud/app/service/ConnectionHandler;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mDriverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field protected sharedPreferences:Landroid/content/SharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 47
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/maps/MapsEventHandler;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/maps/MapsEventHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 62
    new-instance v0, Lcom/navdy/hud/app/maps/MapsEventHandler;

    invoke-direct {v0}, Lcom/navdy/hud/app/maps/MapsEventHandler;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/maps/MapsEventHandler;->sSingleton:Lcom/navdy/hud/app/maps/MapsEventHandler;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 70
    iget-object v0, p0, Lcom/navdy/hud/app/maps/MapsEventHandler;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 71
    return-void
.end method

.method public static final getInstance()Lcom/navdy/hud/app/maps/MapsEventHandler;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/navdy/hud/app/maps/MapsEventHandler;->sSingleton:Lcom/navdy/hud/app/maps/MapsEventHandler;

    return-object v0
.end method

.method private handleConnectionStateChange(Lcom/navdy/service/library/events/connection/ConnectionStateChange;)V
    .locals 9
    .param p1, "event"    # Lcom/navdy/service/library/events/connection/ConnectionStateChange;

    .prologue
    const/4 v2, 0x0

    .line 156
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 157
    sget-object v0, Lcom/navdy/hud/app/maps/MapsEventHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "handleConnectionStateChange:engine not initiazed"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 166
    :goto_0
    return-void

    .line 160
    :cond_0
    const/4 v6, 0x0

    .line 161
    .local v6, "connected":Z
    iget-object v0, p1, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->state:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_VERIFIED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    if-ne v0, v1, :cond_1

    .line 162
    const/4 v6, 0x1

    .line 163
    iget-object v7, p0, Lcom/navdy/hud/app/maps/MapsEventHandler;->bus:Lcom/squareup/otto/Bus;

    new-instance v8, Lcom/navdy/hud/app/event/RemoteEvent;

    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_ENGINE_READY:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;-><init>(Lcom/navdy/service/library/events/navigation/NavigationSessionState;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationRouteResult;Ljava/lang/String;)V

    invoke-direct {v8, v0}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v7, v8}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 165
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->handleConnectionState(Z)V

    goto :goto_0
.end method

.method private handleNavigationRequest(Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;)V
    .locals 5
    .param p1, "request"    # Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;

    .prologue
    .line 169
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 170
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;

    sget-object v1, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_NOT_READY:Lcom/navdy/service/library/events/RequestStatus;

    .line 171
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0901a2

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->newState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    iget-object v4, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->routeId:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;-><init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationSessionState;Ljava/lang/String;)V

    .line 170
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/maps/MapsEventHandler;->sendEventToClient(Lcom/squareup/wire/Message;)V

    .line 175
    :goto_0
    return-void

    .line 174
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->handleNavigationSessionRequest(Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;)V

    goto :goto_0
.end method


# virtual methods
.method public getBus()Lcom/squareup/otto/Bus;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/navdy/hud/app/maps/MapsEventHandler;->bus:Lcom/squareup/otto/Bus;

    return-object v0
.end method

.method public getNavigationPreferences()Lcom/navdy/service/library/events/preferences/NavigationPreferences;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/navdy/hud/app/maps/MapsEventHandler;->mDriverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;

    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->getNavigationPreferences()Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    move-result-object v0

    return-object v0
.end method

.method public getSharedPreferences()Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/navdy/hud/app/maps/MapsEventHandler;->sharedPreferences:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method public onAutoCompleteRequest(Lcom/navdy/service/library/events/places/AutoCompleteRequest;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/service/library/events/places/AutoCompleteRequest;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 85
    invoke-static {p1}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->handleAutoCompleteRequest(Lcom/navdy/service/library/events/places/AutoCompleteRequest;)V

    .line 86
    return-void
.end method

.method public onConnectionStatusChange(Lcom/navdy/service/library/events/connection/ConnectionStateChange;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/service/library/events/connection/ConnectionStateChange;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/maps/MapsEventHandler;->handleConnectionStateChange(Lcom/navdy/service/library/events/connection/ConnectionStateChange;)V

    .line 76
    return-void
.end method

.method public onGetNavigationSessionState(Lcom/navdy/service/library/events/navigation/GetNavigationSessionState;)V
    .locals 8
    .param p1, "event"    # Lcom/navdy/service/library/events/navigation/GetNavigationSessionState;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 105
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 106
    sget-object v0, Lcom/navdy/hud/app/maps/MapsEventHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onGetNavigationSessionState:engine not ready"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 107
    iget-object v6, p0, Lcom/navdy/hud/app/maps/MapsEventHandler;->bus:Lcom/squareup/otto/Bus;

    new-instance v7, Lcom/navdy/hud/app/event/RemoteEvent;

    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_ENGINE_NOT_READY:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;-><init>(Lcom/navdy/service/library/events/navigation/NavigationSessionState;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationRouteResult;Ljava/lang/String;)V

    invoke-direct {v7, v0}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v6, v7}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 116
    :goto_0
    return-void

    .line 115
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->postNavigationSessionStatusEvent(Z)V

    goto :goto_0
.end method

.method public onGetRouteManeuverRequest(Lcom/navdy/service/library/events/navigation/RouteManeuverRequest;)V
    .locals 5
    .param p1, "routeManeuverRequest"    # Lcom/navdy/service/library/events/navigation/RouteManeuverRequest;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 121
    :try_start_0
    invoke-static {p1}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->handleRouteManeuverRequest(Lcom/navdy/service/library/events/navigation/RouteManeuverRequest;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 126
    :goto_0
    return-void

    .line 122
    :catch_0
    move-exception v0

    .line 123
    .local v0, "t":Ljava/lang/Throwable;
    new-instance v1, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;

    sget-object v2, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SERVICE_ERROR:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;-><init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/maps/MapsEventHandler;->sendEventToClient(Lcom/squareup/wire/Message;)V

    .line 124
    sget-object v1, Lcom/navdy/hud/app/maps/MapsEventHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onNavigationRequest(Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 100
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/maps/MapsEventHandler;->handleNavigationRequest(Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;)V

    .line 101
    return-void
.end method

.method public onPlaceSearchRequest(Lcom/navdy/service/library/events/places/PlacesSearchRequest;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/service/library/events/places/PlacesSearchRequest;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 80
    invoke-static {p1}, Lcom/navdy/hud/app/maps/here/HerePlacesManager;->handlePlacesSearchRequest(Lcom/navdy/service/library/events/places/PlacesSearchRequest;)V

    .line 81
    return-void
.end method

.method public onRouteCancelRequest(Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;)V
    .locals 1
    .param p1, "event"    # Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 95
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->handleRouteCancelRequest(Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;Z)Z

    .line 96
    return-void
.end method

.method public onRouteSearchRequest(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 90
    invoke-static {p1}, Lcom/navdy/hud/app/maps/here/HereRouteManager;->handleRouteRequest(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V

    .line 91
    return-void
.end method

.method public sendEventToClient(Lcom/squareup/wire/Message;)V
    .locals 3
    .param p1, "message"    # Lcom/squareup/wire/Message;

    .prologue
    .line 130
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/maps/MapsEventHandler;->connectionHandler:Lcom/navdy/hud/app/service/ConnectionHandler;

    invoke-virtual {v2}, Lcom/navdy/hud/app/service/ConnectionHandler;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v0

    .line 131
    .local v0, "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    if-eqz v0, :cond_0

    .line 132
    invoke-virtual {v0, p1}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/squareup/wire/Message;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 141
    .end local v0    # "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    :cond_0
    :goto_0
    return-void

    .line 138
    :catch_0
    move-exception v1

    .line 139
    .local v1, "t":Ljava/lang/Throwable;
    sget-object v2, Lcom/navdy/hud/app/maps/MapsEventHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
