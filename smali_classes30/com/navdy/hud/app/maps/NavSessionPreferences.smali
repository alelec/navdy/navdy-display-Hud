.class public Lcom/navdy/hud/app/maps/NavSessionPreferences;
.super Ljava/lang/Object;
.source "NavSessionPreferences.java"


# instance fields
.field public spokenTurnByTurn:Z


# direct methods
.method public constructor <init>(Lcom/navdy/service/library/events/preferences/NavigationPreferences;)V
    .locals 0
    .param p1, "prefs"    # Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/maps/NavSessionPreferences;->setDefault(Lcom/navdy/service/library/events/preferences/NavigationPreferences;)V

    .line 20
    return-void
.end method


# virtual methods
.method public setDefault(Lcom/navdy/service/library/events/preferences/NavigationPreferences;)V
    .locals 2
    .param p1, "prefs"    # Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    .prologue
    .line 23
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v1, p1, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->spokenTurnByTurn:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/navdy/hud/app/maps/NavSessionPreferences;->spokenTurnByTurn:Z

    .line 24
    return-void
.end method
