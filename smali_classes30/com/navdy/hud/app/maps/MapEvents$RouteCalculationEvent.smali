.class public Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;
.super Ljava/lang/Object;
.source "MapEvents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/maps/MapEvents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RouteCalculationEvent"
.end annotation


# instance fields
.field public abortOriginDisplay:Z

.field public end:Ljava/lang/Object;

.field public lookupDestination:Lcom/navdy/hud/app/framework/destinations/Destination;

.field public pendingNavigationRequestId:Ljava/lang/String;

.field public progress:I

.field public progressIncrement:I

.field public request:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

.field public response:Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

.field public routeOptions:Lcom/here/android/mpa/routing/RouteOptions;

.field public start:Ljava/lang/Object;

.field public state:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;

.field public stopped:Z

.field public waypoints:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 380
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
