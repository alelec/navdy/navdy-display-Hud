.class public Lcom/navdy/hud/app/ancs/AncsServiceConnector;
.super Lcom/navdy/hud/app/common/ServiceReconnector;
.source "AncsServiceConnector.java"


# static fields
.field private static final NOTIFICATION_EVENT_SEND_THRESHOLD:I = 0x3a98

.field private static final ONE_HOUR_IN_MS:I = 0x36ee80

.field private static blackList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private static enableNotificationTimeMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private categoryMap:[Lcom/navdy/service/library/events/notification/NotificationCategory;

.field private connectRunnable:Ljava/lang/Runnable;

.field private disconnectRunnable:Ljava/lang/Runnable;

.field private listener:Lcom/navdy/ancs/IAncsServiceListener;

.field private localeUpToDate:Z

.field protected mActionCache:Lcom/navdy/hud/app/util/NotificationActionCache;

.field protected mBus:Lcom/squareup/otto/Bus;

.field protected mContext:Landroid/content/Context;

.field private mDeviceInfo:Lcom/navdy/service/library/events/DeviceInfo;

.field protected mNotificationSettings:Lcom/navdy/hud/app/profile/NotificationSettings;

.field protected mService:Lcom/navdy/ancs/IAncsService;

.field private notificationsState:Lcom/navdy/service/library/events/notification/NotificationsState;

.field private reconnect:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 64
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/ancs/AncsServiceConnector;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 83
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->blackList:Ljava/util/HashMap;

    .line 87
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->enableNotificationTimeMap:Ljava/util/HashMap;

    .line 90
    sget-object v0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->blackList:Ljava/util/HashMap;

    const-string v1, "com.apple.mobilephone"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/squareup/otto/Bus;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bus"    # Lcom/squareup/otto/Bus;

    .prologue
    .line 94
    invoke-static {}, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->getServiceIntent()Landroid/content/Intent;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, p1, v1, v2}, Lcom/navdy/hud/app/common/ServiceReconnector;-><init>(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;)V

    .line 75
    sget-object v1, Lcom/navdy/service/library/events/notification/NotificationsState;->NOTIFICATIONS_STOPPED:Lcom/navdy/service/library/events/notification/NotificationsState;

    iput-object v1, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->notificationsState:Lcom/navdy/service/library/events/notification/NotificationsState;

    .line 125
    new-instance v1, Lcom/navdy/hud/app/ancs/AncsServiceConnector$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ancs/AncsServiceConnector$1;-><init>(Lcom/navdy/hud/app/ancs/AncsServiceConnector;)V

    iput-object v1, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->listener:Lcom/navdy/ancs/IAncsServiceListener;

    .line 496
    const/16 v1, 0xc

    new-array v1, v1, [Lcom/navdy/service/library/events/notification/NotificationCategory;

    const/4 v2, 0x0

    sget-object v3, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_OTHER:Lcom/navdy/service/library/events/notification/NotificationCategory;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_INCOMING_CALL:Lcom/navdy/service/library/events/notification/NotificationCategory;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_MISSED_CALL:Lcom/navdy/service/library/events/notification/NotificationCategory;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    sget-object v3, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_VOICE_MAIL:Lcom/navdy/service/library/events/notification/NotificationCategory;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    sget-object v3, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_SOCIAL:Lcom/navdy/service/library/events/notification/NotificationCategory;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    sget-object v3, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_SCHEDULE:Lcom/navdy/service/library/events/notification/NotificationCategory;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_EMAIL:Lcom/navdy/service/library/events/notification/NotificationCategory;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    sget-object v3, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_NEWS:Lcom/navdy/service/library/events/notification/NotificationCategory;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    sget-object v3, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_HEALTH_AND_FITNESS:Lcom/navdy/service/library/events/notification/NotificationCategory;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    sget-object v3, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_BUSINESS_AND_FINANCE:Lcom/navdy/service/library/events/notification/NotificationCategory;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    sget-object v3, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_LOCATION:Lcom/navdy/service/library/events/notification/NotificationCategory;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    sget-object v3, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_ENTERTAINMENT:Lcom/navdy/service/library/events/notification/NotificationCategory;

    aput-object v3, v1, v2

    iput-object v1, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->categoryMap:[Lcom/navdy/service/library/events/notification/NotificationCategory;

    .line 625
    new-instance v1, Lcom/navdy/hud/app/ancs/AncsServiceConnector$2;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ancs/AncsServiceConnector$2;-><init>(Lcom/navdy/hud/app/ancs/AncsServiceConnector;)V

    iput-object v1, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->connectRunnable:Ljava/lang/Runnable;

    .line 668
    new-instance v1, Lcom/navdy/hud/app/ancs/AncsServiceConnector$3;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/ancs/AncsServiceConnector$3;-><init>(Lcom/navdy/hud/app/ancs/AncsServiceConnector;)V

    iput-object v1, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->disconnectRunnable:Ljava/lang/Runnable;

    .line 96
    iput-object p1, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->mContext:Landroid/content/Context;

    .line 97
    iput-object p2, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->mBus:Lcom/squareup/otto/Bus;

    .line 99
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v0

    .line 100
    .local v0, "profile":Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfile;->getNotificationSettings()Lcom/navdy/hud/app/profile/NotificationSettings;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->mNotificationSettings:Lcom/navdy/hud/app/profile/NotificationSettings;

    .line 102
    new-instance v1, Lcom/navdy/hud/app/util/NotificationActionCache;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/navdy/hud/app/util/NotificationActionCache;-><init>(Ljava/lang/Class;)V

    iput-object v1, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->mActionCache:Lcom/navdy/hud/app/util/NotificationActionCache;

    .line 104
    invoke-virtual {p2, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 105
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/ancs/AncsServiceConnector;Lcom/navdy/service/library/events/notification/NotificationsState;Lcom/navdy/service/library/events/notification/NotificationsError;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ancs/AncsServiceConnector;
    .param p1, "x1"    # Lcom/navdy/service/library/events/notification/NotificationsState;
    .param p2, "x2"    # Lcom/navdy/service/library/events/notification/NotificationsError;

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->setNotificationsState(Lcom/navdy/service/library/events/notification/NotificationsState;Lcom/navdy/service/library/events/notification/NotificationsError;)V

    return-void
.end method

.method static synthetic access$100()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->blackList:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/ancs/AncsServiceConnector;Lcom/navdy/ancs/AppleNotification;)Lcom/navdy/service/library/events/glances/GlanceEvent;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ancs/AncsServiceConnector;
    .param p1, "x1"    # Lcom/navdy/ancs/AppleNotification;

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->convertToGlanceEvent(Lcom/navdy/ancs/AppleNotification;)Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->enableNotificationTimeMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/ancs/AncsServiceConnector;Lcom/navdy/ancs/AppleNotification;)Lcom/navdy/service/library/events/notification/NotificationEvent;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ancs/AncsServiceConnector;
    .param p1, "x1"    # Lcom/navdy/ancs/AppleNotification;

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->convertNotification(Lcom/navdy/ancs/AppleNotification;)Lcom/navdy/service/library/events/notification/NotificationEvent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/ancs/AncsServiceConnector;)Lcom/navdy/service/library/events/DeviceInfo;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ancs/AncsServiceConnector;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->mDeviceInfo:Lcom/navdy/service/library/events/DeviceInfo;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/ancs/AncsServiceConnector;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/ancs/AncsServiceConnector;

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->localeUpToDate:Z

    return v0
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/ancs/AncsServiceConnector;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/ancs/AncsServiceConnector;

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->stop()V

    return-void
.end method

.method private buildActions(Lcom/navdy/ancs/AppleNotification;)Ljava/util/List;
    .locals 3
    .param p1, "notification"    # Lcom/navdy/ancs/AppleNotification;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/ancs/AppleNotification;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/notification/NotificationAction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 464
    const/4 v0, 0x0

    .line 466
    .local v0, "actions":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/notification/NotificationAction;>;"
    invoke-virtual {p1}, Lcom/navdy/ancs/AppleNotification;->getAppId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.apple.MobileSMS"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 473
    :goto_0
    return-object v0

    .line 471
    :cond_0
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->getDefaultActions(Lcom/navdy/ancs/AppleNotification;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method private connect()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 678
    iput-boolean v2, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->reconnect:Z

    .line 679
    iget-object v0, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->mDeviceInfo:Lcom/navdy/service/library/events/DeviceInfo;

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->isIOS(Lcom/navdy/service/library/events/DeviceInfo;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 680
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->connectRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 682
    :cond_0
    return-void
.end method

.method private convertNotification(Lcom/navdy/ancs/AppleNotification;)Lcom/navdy/service/library/events/notification/NotificationEvent;
    .locals 14
    .param p1, "notification"    # Lcom/navdy/ancs/AppleNotification;

    .prologue
    const/4 v8, 0x0

    .line 437
    invoke-virtual {p1}, Lcom/navdy/ancs/AppleNotification;->getTitle()Ljava/lang/String;

    move-result-object v3

    .line 438
    .local v3, "title":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/navdy/ancs/AppleNotification;->getSubTitle()Ljava/lang/String;

    move-result-object v4

    .line 442
    .local v4, "subtitle":Ljava/lang/String;
    move-object v11, v3

    .line 444
    .local v11, "identification":Ljava/lang/String;
    new-instance v0, Lcom/navdy/service/library/events/notification/NotificationEvent;

    .line 445
    invoke-virtual {p1}, Lcom/navdy/ancs/AppleNotification;->getNotificationUid()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 446
    invoke-virtual {p1}, Lcom/navdy/ancs/AppleNotification;->getCategoryId()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->mapCategory(I)Lcom/navdy/service/library/events/notification/NotificationCategory;

    move-result-object v2

    .line 449
    invoke-virtual {p1}, Lcom/navdy/ancs/AppleNotification;->getMessage()Ljava/lang/String;

    move-result-object v5

    .line 450
    invoke-virtual {p1}, Lcom/navdy/ancs/AppleNotification;->getAppId()Ljava/lang/String;

    move-result-object v6

    .line 451
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->buildActions(Lcom/navdy/ancs/AppleNotification;)Ljava/util/List;

    move-result-object v7

    const/4 v9, 0x1

    .line 456
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    .line 457
    invoke-virtual {p1}, Lcom/navdy/ancs/AppleNotification;->getAppName()Ljava/lang/String;

    move-result-object v13

    move-object v9, v8

    move-object v10, v8

    invoke-direct/range {v0 .. v13}, Lcom/navdy/service/library/events/notification/NotificationEvent;-><init>(Ljava/lang/Integer;Lcom/navdy/service/library/events/notification/NotificationCategory;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 459
    .local v0, "event":Lcom/navdy/service/library/events/notification/NotificationEvent;
    return-object v0
.end method

.method private convertToGlanceEvent(Lcom/navdy/ancs/AppleNotification;)Lcom/navdy/service/library/events/glances/GlanceEvent;
    .locals 13
    .param p1, "notification"    # Lcom/navdy/ancs/AppleNotification;

    .prologue
    .line 235
    invoke-virtual {p1}, Lcom/navdy/ancs/AppleNotification;->getAppId()Ljava/lang/String;

    move-result-object v0

    .line 236
    .local v0, "appId":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/navdy/ancs/AppleNotification;->getNotificationUid()I

    move-result v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 237
    .local v1, "id":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/navdy/ancs/AppleNotification;->getTitle()Ljava/lang/String;

    move-result-object v2

    .line 238
    .local v2, "title":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/navdy/ancs/AppleNotification;->getSubTitle()Ljava/lang/String;

    move-result-object v3

    .line 239
    .local v3, "subtitle":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/navdy/ancs/AppleNotification;->getMessage()Ljava/lang/String;

    move-result-object v4

    .line 240
    .local v4, "message":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/navdy/ancs/AppleNotification;->getEventId()I

    move-result v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    .line 241
    .local v7, "eventId":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/navdy/ancs/AppleNotification;->getDate()Ljava/util/Date;

    move-result-object v5

    .line 244
    .local v5, "time":Ljava/util/Date;
    if-nez v2, :cond_0

    if-nez v4, :cond_0

    if-nez v3, :cond_0

    .line 245
    const/4 v8, 0x0

    .line 395
    :goto_0
    return-object v8

    .line 248
    :cond_0
    invoke-static {v0}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->getGlancesApp(Ljava/lang/String;)Lcom/navdy/hud/app/framework/glance/GlanceApp;

    move-result-object v6

    .line 251
    .local v6, "app":Lcom/navdy/hud/app/framework/glance/GlanceApp;
    if-nez v6, :cond_1

    .line 253
    sget-object v6, Lcom/navdy/hud/app/framework/glance/GlanceApp;->GENERIC:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    .line 254
    const/4 v9, 0x0

    .line 260
    .local v9, "whiteListed":Z
    :goto_1
    sget-object v10, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "[ancs-notif] appId["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "]"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " id["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "]"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " title["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "]"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " subTitle["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "]"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " message["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "]"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " eventId["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "]"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " time["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "]"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " whitelisted["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "]"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " app["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "]"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 271
    const/4 v8, 0x0

    .line 273
    .local v8, "glanceEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    sget-object v10, Lcom/navdy/hud/app/ancs/AncsServiceConnector$4;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp:[I

    invoke-virtual {v6}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->ordinal()I

    move-result v11

    aget v10, v10, v11

    packed-switch v10, :pswitch_data_0

    .line 391
    sget-object v10, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "[ancs-notif] app of type ["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "] not handled"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 257
    .end local v8    # "glanceEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    .end local v9    # "whiteListed":Z
    :cond_1
    const/4 v9, 0x1

    .restart local v9    # "whiteListed":Z
    goto/16 :goto_1

    .line 275
    .restart local v8    # "glanceEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    :pswitch_0
    invoke-static/range {v0 .. v5}, Lcom/navdy/hud/app/ancs/AncsGlanceHelper;->buildGoogleCalendarEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v8

    .line 281
    goto/16 :goto_0

    .line 284
    :pswitch_1
    invoke-static/range {v0 .. v5}, Lcom/navdy/hud/app/ancs/AncsGlanceHelper;->buildGoogleMailEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v8

    .line 290
    goto/16 :goto_0

    .line 292
    :pswitch_2
    invoke-static/range {v0 .. v5}, Lcom/navdy/hud/app/ancs/AncsGlanceHelper;->buildGenericMailEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v8

    .line 298
    goto/16 :goto_0

    .line 300
    :pswitch_3
    invoke-static/range {v0 .. v5}, Lcom/navdy/hud/app/ancs/AncsGlanceHelper;->buildGoogleHangoutEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v8

    .line 306
    goto/16 :goto_0

    .line 309
    :pswitch_4
    invoke-static/range {v0 .. v5}, Lcom/navdy/hud/app/ancs/AncsGlanceHelper;->buildSlackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v8

    .line 315
    goto/16 :goto_0

    .line 318
    :pswitch_5
    invoke-static/range {v0 .. v5}, Lcom/navdy/hud/app/ancs/AncsGlanceHelper;->buildWhatsappEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v8

    .line 324
    goto/16 :goto_0

    .line 327
    :pswitch_6
    invoke-static/range {v0 .. v5}, Lcom/navdy/hud/app/ancs/AncsGlanceHelper;->buildFacebookMessengerEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v8

    .line 333
    goto/16 :goto_0

    .line 336
    :pswitch_7
    invoke-static/range {v0 .. v5}, Lcom/navdy/hud/app/ancs/AncsGlanceHelper;->buildFacebookEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v8

    .line 342
    goto/16 :goto_0

    .line 345
    :pswitch_8
    invoke-static/range {v0 .. v5}, Lcom/navdy/hud/app/ancs/AncsGlanceHelper;->buildTwitterEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v8

    .line 351
    goto/16 :goto_0

    .line 354
    :pswitch_9
    invoke-static/range {v0 .. v5}, Lcom/navdy/hud/app/ancs/AncsGlanceHelper;->buildiMessageEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v8

    .line 360
    goto/16 :goto_0

    .line 363
    :pswitch_a
    invoke-static/range {v0 .. v5}, Lcom/navdy/hud/app/ancs/AncsGlanceHelper;->buildAppleMailEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v8

    .line 369
    goto/16 :goto_0

    .line 372
    :pswitch_b
    invoke-static/range {v0 .. v5}, Lcom/navdy/hud/app/ancs/AncsGlanceHelper;->buildAppleCalendarEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v8

    .line 378
    goto/16 :goto_0

    .line 382
    :pswitch_c
    invoke-static/range {v0 .. v5}, Lcom/navdy/hud/app/ancs/AncsGlanceHelper;->buildGenericEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v8

    .line 388
    goto/16 :goto_0

    .line 273
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method private disconnect()V
    .locals 3

    .prologue
    .line 685
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->reconnect:Z

    .line 686
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->disconnectRunnable:Ljava/lang/Runnable;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 687
    return-void
.end method

.method private getDefaultActions(Lcom/navdy/ancs/AppleNotification;)Ljava/util/List;
    .locals 6
    .param p1, "notification"    # Lcom/navdy/ancs/AppleNotification;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/ancs/AppleNotification;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/notification/NotificationAction;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 479
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 480
    .local v0, "actions":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/notification/NotificationAction;>;"
    invoke-virtual {p1}, Lcom/navdy/ancs/AppleNotification;->getNotificationUid()I

    move-result v1

    .line 481
    .local v1, "notificationId":I
    invoke-virtual {p1}, Lcom/navdy/ancs/AppleNotification;->hasPositiveAction()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 482
    iget-object v2, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->mActionCache:Lcom/navdy/hud/app/util/NotificationActionCache;

    invoke-virtual {p1}, Lcom/navdy/ancs/AppleNotification;->getPositiveActionLabel()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v5, v5, v3}, Lcom/navdy/hud/app/util/NotificationActionCache;->buildAction(IIILjava/lang/String;)Lcom/navdy/service/library/events/notification/NotificationAction;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 484
    :cond_0
    invoke-virtual {p1}, Lcom/navdy/ancs/AppleNotification;->hasNegativeAction()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 485
    iget-object v2, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->mActionCache:Lcom/navdy/hud/app/util/NotificationActionCache;

    const/4 v3, 0x1

    .line 488
    invoke-virtual {p1}, Lcom/navdy/ancs/AppleNotification;->getNegativeActionLabel()Ljava/lang/String;

    move-result-object v4

    .line 485
    invoke-virtual {v2, v1, v3, v5, v4}, Lcom/navdy/hud/app/util/NotificationActionCache;->buildAction(IIILjava/lang/String;)Lcom/navdy/service/library/events/notification/NotificationAction;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 490
    :cond_1
    return-object v0
.end method

.method private static getServiceIntent()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 108
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 109
    .local v1, "intent":Landroid/content/Intent;
    new-instance v0, Landroid/content/ComponentName;

    const-string v2, "com.navdy.ancs.app"

    const-string v3, "com.navdy.ancs.ANCSService"

    invoke-direct {v0, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    .local v0, "componentName":Landroid/content/ComponentName;
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 111
    return-object v1
.end method

.method private isIOS(Lcom/navdy/service/library/events/DeviceInfo;)Z
    .locals 2
    .param p1, "deviceInfo"    # Lcom/navdy/service/library/events/DeviceInfo;

    .prologue
    .line 572
    if-eqz p1, :cond_2

    iget-object v0, p1, Lcom/navdy/service/library/events/DeviceInfo;->platform:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/navdy/service/library/events/DeviceInfo;->platform:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    sget-object v1, Lcom/navdy/service/library/events/DeviceInfo$Platform;->PLATFORM_iOS:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    if-eq v0, v1, :cond_1

    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/DeviceInfo;->model:Ljava/lang/String;

    const-string v1, "iPhone"

    .line 574
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private mapCategory(I)Lcom/navdy/service/library/events/notification/NotificationCategory;
    .locals 1
    .param p1, "categoryId"    # I

    .prologue
    .line 513
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->categoryMap:[Lcom/navdy/service/library/events/notification/NotificationCategory;

    array-length v0, v0

    if-lt p1, v0, :cond_1

    .line 514
    :cond_0
    sget-object v0, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_OTHER:Lcom/navdy/service/library/events/notification/NotificationCategory;

    .line 516
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->categoryMap:[Lcom/navdy/service/library/events/notification/NotificationCategory;

    aget-object v0, v0, p1

    goto :goto_0
.end method

.method private setNotificationsState(Lcom/navdy/service/library/events/notification/NotificationsState;Lcom/navdy/service/library/events/notification/NotificationsError;)V
    .locals 4
    .param p1, "state"    # Lcom/navdy/service/library/events/notification/NotificationsState;
    .param p2, "error"    # Lcom/navdy/service/library/events/notification/NotificationsError;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->notificationsState:Lcom/navdy/service/library/events/notification/NotificationsState;

    if-eq v0, p1, :cond_0

    .line 116
    iput-object p1, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->notificationsState:Lcom/navdy/service/library/events/notification/NotificationsState;

    .line 117
    iget-object v0, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->mBus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/event/RemoteEvent;

    new-instance v2, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$Builder;-><init>()V

    iget-object v3, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->notificationsState:Lcom/navdy/service/library/events/notification/NotificationsState;

    .line 118
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$Builder;->state(Lcom/navdy/service/library/events/notification/NotificationsState;)Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$Builder;

    move-result-object v2

    sget-object v3, Lcom/navdy/service/library/events/notification/ServiceType;->SERVICE_ANCS:Lcom/navdy/service/library/events/notification/ServiceType;

    .line 119
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$Builder;->service(Lcom/navdy/service/library/events/notification/ServiceType;)Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$Builder;

    move-result-object v2

    .line 120
    invoke-virtual {v2, p2}, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$Builder;->errorDetails(Lcom/navdy/service/library/events/notification/NotificationsError;)Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$Builder;

    move-result-object v2

    .line 121
    invoke-virtual {v2}, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$Builder;->build()Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    .line 117
    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 123
    :cond_0
    return-void
.end method

.method private stop()V
    .locals 3

    .prologue
    .line 658
    sget-object v1, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Disconnecting ANCS connection"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 659
    iget-object v1, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->mService:Lcom/navdy/ancs/IAncsService;

    if-eqz v1, :cond_0

    .line 661
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->mService:Lcom/navdy/ancs/IAncsService;

    invoke-interface {v1}, Lcom/navdy/ancs/IAncsService;->disconnect()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 666
    :cond_0
    :goto_0
    return-void

    .line 662
    :catch_0
    move-exception v0

    .line 663
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Failed to disconnect ANCS"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private updateNotificationFilters()V
    .locals 10

    .prologue
    .line 544
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v4

    .line 545
    .local v4, "profile":Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-virtual {v4}, Lcom/navdy/hud/app/profile/DriverProfile;->getNotificationSettings()Lcom/navdy/hud/app/profile/NotificationSettings;

    move-result-object v5

    iput-object v5, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->mNotificationSettings:Lcom/navdy/hud/app/profile/NotificationSettings;

    .line 546
    iget-object v5, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->mService:Lcom/navdy/ancs/IAncsService;

    if-eqz v5, :cond_0

    .line 548
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const-wide/32 v8, 0x36ee80

    sub-long v2, v6, v8

    .line 549
    .local v2, "oneHourAgo":J
    iget-object v5, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->mNotificationSettings:Lcom/navdy/hud/app/profile/NotificationSettings;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->mNotificationSettings:Lcom/navdy/hud/app/profile/NotificationSettings;

    .line 550
    invoke-virtual {v5}, Lcom/navdy/hud/app/profile/NotificationSettings;->enabledApps()Ljava/util/List;

    move-result-object v1

    .line 551
    .local v1, "enabledApps":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_0
    sget-object v6, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Setting ANCS to filter apps: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    if-eqz v1, :cond_2

    .line 552
    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    :goto_1
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 551
    invoke-virtual {v6, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 553
    iget-object v5, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->mService:Lcom/navdy/ancs/IAncsService;

    invoke-interface {v5, v1, v2, v3}, Lcom/navdy/ancs/IAncsService;->setNotificationFilter(Ljava/util/List;J)V

    .line 558
    .end local v1    # "enabledApps":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v2    # "oneHourAgo":J
    :cond_0
    :goto_2
    return-void

    .line 550
    .restart local v2    # "oneHourAgo":J
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 552
    .restart local v1    # "enabledApps":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    const-string v5, "null"
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 554
    .end local v1    # "enabledApps":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v2    # "oneHourAgo":J
    :catch_0
    move-exception v0

    .line 555
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v5, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Failed to update notification filter"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_2
.end method


# virtual methods
.method protected onConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 206
    invoke-static {p2}, Lcom/navdy/ancs/IAncsService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/navdy/ancs/IAncsService;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->mService:Lcom/navdy/ancs/IAncsService;

    .line 208
    :try_start_0
    invoke-direct {p0}, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->updateNotificationFilters()V

    .line 209
    iget-object v1, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->mService:Lcom/navdy/ancs/IAncsService;

    iget-object v2, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->listener:Lcom/navdy/ancs/IAncsServiceListener;

    invoke-interface {v1, v2}, Lcom/navdy/ancs/IAncsService;->addListener(Lcom/navdy/ancs/IAncsServiceListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 213
    :goto_0
    iget-boolean v1, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->reconnect:Z

    if-eqz v1, :cond_0

    .line 214
    invoke-direct {p0}, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->connect()V

    .line 216
    :cond_0
    return-void

    .line 210
    :catch_0
    move-exception v0

    .line 211
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Failed to add notification listener"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onConnectionChange(Lcom/navdy/service/library/events/connection/ConnectionStateChange;)V
    .locals 2
    .param p1, "change"    # Lcom/navdy/service/library/events/connection/ConnectionStateChange;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 588
    sget-object v0, Lcom/navdy/hud/app/ancs/AncsServiceConnector$4;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

    iget-object v1, p1, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->state:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 599
    :goto_0
    return-void

    .line 590
    :pswitch_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->mDeviceInfo:Lcom/navdy/service/library/events/DeviceInfo;

    .line 591
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->localeUpToDate:Z

    .line 594
    invoke-direct {p0}, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->disconnect()V

    goto :goto_0

    .line 588
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onDeviceInfoAvailable(Lcom/navdy/hud/app/event/DeviceInfoAvailable;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/event/DeviceInfoAvailable;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 603
    sget-object v0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "device info updated"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 604
    iget-object v0, p1, Lcom/navdy/hud/app/event/DeviceInfoAvailable;->deviceInfo:Lcom/navdy/service/library/events/DeviceInfo;

    iput-object v0, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->mDeviceInfo:Lcom/navdy/service/library/events/DeviceInfo;

    .line 605
    return-void
.end method

.method protected onDisconnected(Landroid/content/ComponentName;)V
    .locals 1
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 220
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->mService:Lcom/navdy/ancs/IAncsService;

    .line 221
    return-void
.end method

.method public onDriverProfileChanged(Lcom/navdy/hud/app/event/DriverProfileChanged;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/hud/app/event/DriverProfileChanged;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 562
    invoke-direct {p0}, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->updateNotificationFilters()V

    .line 563
    return-void
.end method

.method public onInitPhase(Lcom/navdy/hud/app/event/InitEvents$InitPhase;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/event/InitEvents$InitPhase;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 609
    sget-object v0, Lcom/navdy/hud/app/ancs/AncsServiceConnector$4;->$SwitchMap$com$navdy$hud$app$event$InitEvents$Phase:[I

    iget-object v1, p1, Lcom/navdy/hud/app/event/InitEvents$InitPhase;->phase:Lcom/navdy/hud/app/event/InitEvents$Phase;

    invoke-virtual {v1}, Lcom/navdy/hud/app/event/InitEvents$Phase;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 623
    :cond_0
    :goto_0
    return-void

    .line 611
    :pswitch_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->localeUpToDate:Z

    .line 613
    iget-boolean v0, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->reconnect:Z

    if-eqz v0, :cond_0

    .line 614
    invoke-direct {p0}, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->connect()V

    goto :goto_0

    .line 618
    :pswitch_1
    invoke-virtual {p0}, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->shutdown()V

    goto :goto_0

    .line 609
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onNotificationAction(Lcom/navdy/service/library/events/notification/NotificationAction;)V
    .locals 3
    .param p1, "action"    # Lcom/navdy/service/library/events/notification/NotificationAction;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 579
    iget-object v0, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->mActionCache:Lcom/navdy/hud/app/util/NotificationActionCache;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/util/NotificationActionCache;->validAction(Lcom/navdy/service/library/events/notification/NotificationAction;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 580
    iget-object v0, p1, Lcom/navdy/service/library/events/notification/NotificationAction;->notificationId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p1, Lcom/navdy/service/library/events/notification/NotificationAction;->actionId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->performAction(II)V

    .line 581
    iget-object v0, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->mBus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/service/library/events/ui/DismissScreen;

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_NOTIFICATION:Lcom/navdy/service/library/events/ui/Screen;

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/ui/DismissScreen;-><init>(Lcom/navdy/service/library/events/ui/Screen;)V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 582
    iget-object v0, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->mActionCache:Lcom/navdy/hud/app/util/NotificationActionCache;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/util/NotificationActionCache;->markComplete(Lcom/navdy/service/library/events/notification/NotificationAction;)V

    .line 584
    :cond_0
    return-void
.end method

.method public onNotificationPreferences(Lcom/navdy/service/library/events/preferences/NotificationPreferences;)V
    .locals 0
    .param p1, "preferences"    # Lcom/navdy/service/library/events/preferences/NotificationPreferences;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 567
    invoke-direct {p0}, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->updateNotificationFilters()V

    .line 568
    return-void
.end method

.method public onNotificationsRequest(Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;)V
    .locals 4
    .param p1, "request"    # Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 521
    iget-object v0, p1, Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;->service:Lcom/navdy/service/library/events/notification/ServiceType;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/navdy/service/library/events/notification/ServiceType;->SERVICE_ANCS:Lcom/navdy/service/library/events/notification/ServiceType;

    iget-object v1, p1, Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;->service:Lcom/navdy/service/library/events/notification/ServiceType;

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/notification/ServiceType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 522
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Received NotificationsStatusRequest:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 523
    iget-object v0, p1, Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;->newState:Lcom/navdy/service/library/events/notification/NotificationsState;

    if-eqz v0, :cond_1

    .line 524
    sget-object v0, Lcom/navdy/hud/app/ancs/AncsServiceConnector$4;->$SwitchMap$com$navdy$service$library$events$notification$NotificationsState:[I

    iget-object v1, p1, Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;->newState:Lcom/navdy/service/library/events/notification/NotificationsState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/notification/NotificationsState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 536
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->mBus:Lcom/squareup/otto/Bus;

    new-instance v1, Lcom/navdy/hud/app/event/RemoteEvent;

    new-instance v2, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$Builder;-><init>()V

    iget-object v3, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->notificationsState:Lcom/navdy/service/library/events/notification/NotificationsState;

    .line 537
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$Builder;->state(Lcom/navdy/service/library/events/notification/NotificationsState;)Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$Builder;

    move-result-object v2

    sget-object v3, Lcom/navdy/service/library/events/notification/ServiceType;->SERVICE_ANCS:Lcom/navdy/service/library/events/notification/ServiceType;

    .line 538
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$Builder;->service(Lcom/navdy/service/library/events/notification/ServiceType;)Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$Builder;

    move-result-object v2

    .line 539
    invoke-virtual {v2}, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate$Builder;->build()Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    .line 536
    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 541
    :cond_2
    return-void

    .line 526
    :pswitch_0
    invoke-direct {p0}, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->connect()V

    goto :goto_0

    .line 529
    :pswitch_1
    invoke-direct {p0}, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->disconnect()V

    goto :goto_0

    .line 524
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onShutdown(Lcom/navdy/hud/app/event/Shutdown;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/event/Shutdown;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 652
    iget-object v0, p1, Lcom/navdy/hud/app/event/Shutdown;->state:Lcom/navdy/hud/app/event/Shutdown$State;

    sget-object v1, Lcom/navdy/hud/app/event/Shutdown$State;->CONFIRMED:Lcom/navdy/hud/app/event/Shutdown$State;

    if-ne v0, v1, :cond_0

    .line 653
    invoke-virtual {p0}, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->shutdown()V

    .line 655
    :cond_0
    return-void
.end method

.method public performAction(II)V
    .locals 4
    .param p1, "notificationUid"    # I
    .param p2, "actionId"    # I

    .prologue
    .line 224
    iget-object v1, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->mService:Lcom/navdy/ancs/IAncsService;

    if-eqz v1, :cond_0

    .line 226
    :try_start_0
    sget-object v1, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Performing action ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") for notification - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 227
    iget-object v1, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->mService:Lcom/navdy/ancs/IAncsService;

    invoke-interface {v1, p1, p2}, Lcom/navdy/ancs/IAncsService;->performNotificationAction(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 232
    :cond_0
    :goto_0
    return-void

    .line 228
    :catch_0
    move-exception v0

    .line 229
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Exception when performing notification action"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public shutdown()V
    .locals 1

    .prologue
    .line 644
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->reconnect:Z

    .line 645
    invoke-direct {p0}, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->stop()V

    .line 646
    invoke-super {p0}, Lcom/navdy/hud/app/common/ServiceReconnector;->shutdown()V

    .line 647
    return-void
.end method
