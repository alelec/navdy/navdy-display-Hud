.class Lcom/navdy/hud/app/ancs/AncsServiceConnector$2;
.super Ljava/lang/Object;
.source "AncsServiceConnector.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ancs/AncsServiceConnector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ancs/AncsServiceConnector;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ancs/AncsServiceConnector;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ancs/AncsServiceConnector;

    .prologue
    .line 625
    iput-object p1, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector$2;->this$0:Lcom/navdy/hud/app/ancs/AncsServiceConnector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 628
    sget-object v2, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Establishing ANCS connection"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 629
    iget-object v2, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector$2;->this$0:Lcom/navdy/hud/app/ancs/AncsServiceConnector;

    iget-object v2, v2, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->mService:Lcom/navdy/ancs/IAncsService;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector$2;->this$0:Lcom/navdy/hud/app/ancs/AncsServiceConnector;

    # getter for: Lcom/navdy/hud/app/ancs/AncsServiceConnector;->mDeviceInfo:Lcom/navdy/service/library/events/DeviceInfo;
    invoke-static {v2}, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->access$500(Lcom/navdy/hud/app/ancs/AncsServiceConnector;)Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector$2;->this$0:Lcom/navdy/hud/app/ancs/AncsServiceConnector;

    # getter for: Lcom/navdy/hud/app/ancs/AncsServiceConnector;->localeUpToDate:Z
    invoke-static {v2}, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->access$600(Lcom/navdy/hud/app/ancs/AncsServiceConnector;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 631
    :try_start_0
    new-instance v0, Lcom/navdy/service/library/device/NavdyDeviceId;

    iget-object v2, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector$2;->this$0:Lcom/navdy/hud/app/ancs/AncsServiceConnector;

    # getter for: Lcom/navdy/hud/app/ancs/AncsServiceConnector;->mDeviceInfo:Lcom/navdy/service/library/events/DeviceInfo;
    invoke-static {v2}, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->access$500(Lcom/navdy/hud/app/ancs/AncsServiceConnector;)Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v2

    iget-object v2, v2, Lcom/navdy/service/library/events/DeviceInfo;->deviceId:Ljava/lang/String;

    invoke-direct {v0, v2}, Lcom/navdy/service/library/device/NavdyDeviceId;-><init>(Ljava/lang/String;)V

    .line 633
    .local v0, "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    iget-object v2, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector$2;->this$0:Lcom/navdy/hud/app/ancs/AncsServiceConnector;

    iget-object v2, v2, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->mService:Lcom/navdy/ancs/IAncsService;

    new-instance v3, Landroid/os/ParcelUuid;

    iget-object v4, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector$2;->this$0:Lcom/navdy/hud/app/ancs/AncsServiceConnector;

    # getter for: Lcom/navdy/hud/app/ancs/AncsServiceConnector;->mDeviceInfo:Lcom/navdy/service/library/events/DeviceInfo;
    invoke-static {v4}, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->access$500(Lcom/navdy/hud/app/ancs/AncsServiceConnector;)Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v4

    iget-object v4, v4, Lcom/navdy/service/library/events/DeviceInfo;->deviceUuid:Ljava/lang/String;

    invoke-static {v4}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    .line 634
    invoke-virtual {v0}, Lcom/navdy/service/library/device/NavdyDeviceId;->getBluetoothAddress()Ljava/lang/String;

    move-result-object v4

    .line 633
    invoke-interface {v2, v3, v4}, Lcom/navdy/ancs/IAncsService;->connectToDevice(Landroid/os/ParcelUuid;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 639
    .end local v0    # "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    :cond_0
    :goto_0
    return-void

    .line 635
    :catch_0
    move-exception v1

    .line 636
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Failed to connect to navdy BTLE service"

    invoke-virtual {v2, v3, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
