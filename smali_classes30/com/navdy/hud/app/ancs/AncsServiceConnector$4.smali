.class synthetic Lcom/navdy/hud/app/ancs/AncsServiceConnector$4;
.super Ljava/lang/Object;
.source "AncsServiceConnector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ancs/AncsServiceConnector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$navdy$hud$app$event$InitEvents$Phase:[I

.field static final synthetic $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp:[I

.field static final synthetic $SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

.field static final synthetic $SwitchMap$com$navdy$service$library$events$notification$NotificationsState:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 609
    invoke-static {}, Lcom/navdy/hud/app/event/InitEvents$Phase;->values()[Lcom/navdy/hud/app/event/InitEvents$Phase;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/ancs/AncsServiceConnector$4;->$SwitchMap$com$navdy$hud$app$event$InitEvents$Phase:[I

    :try_start_0
    sget-object v0, Lcom/navdy/hud/app/ancs/AncsServiceConnector$4;->$SwitchMap$com$navdy$hud$app$event$InitEvents$Phase:[I

    sget-object v1, Lcom/navdy/hud/app/event/InitEvents$Phase;->LOCALE_UP_TO_DATE:Lcom/navdy/hud/app/event/InitEvents$Phase;

    invoke-virtual {v1}, Lcom/navdy/hud/app/event/InitEvents$Phase;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_11

    :goto_0
    :try_start_1
    sget-object v0, Lcom/navdy/hud/app/ancs/AncsServiceConnector$4;->$SwitchMap$com$navdy$hud$app$event$InitEvents$Phase:[I

    sget-object v1, Lcom/navdy/hud/app/event/InitEvents$Phase;->SWITCHING_LOCALE:Lcom/navdy/hud/app/event/InitEvents$Phase;

    invoke-virtual {v1}, Lcom/navdy/hud/app/event/InitEvents$Phase;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_10

    .line 588
    :goto_1
    invoke-static {}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->values()[Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/ancs/AncsServiceConnector$4;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

    :try_start_2
    sget-object v0, Lcom/navdy/hud/app/ancs/AncsServiceConnector$4;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_DISCONNECTED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_f

    .line 524
    :goto_2
    invoke-static {}, Lcom/navdy/service/library/events/notification/NotificationsState;->values()[Lcom/navdy/service/library/events/notification/NotificationsState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/ancs/AncsServiceConnector$4;->$SwitchMap$com$navdy$service$library$events$notification$NotificationsState:[I

    :try_start_3
    sget-object v0, Lcom/navdy/hud/app/ancs/AncsServiceConnector$4;->$SwitchMap$com$navdy$service$library$events$notification$NotificationsState:[I

    sget-object v1, Lcom/navdy/service/library/events/notification/NotificationsState;->NOTIFICATIONS_ENABLED:Lcom/navdy/service/library/events/notification/NotificationsState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/notification/NotificationsState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_e

    :goto_3
    :try_start_4
    sget-object v0, Lcom/navdy/hud/app/ancs/AncsServiceConnector$4;->$SwitchMap$com$navdy$service$library$events$notification$NotificationsState:[I

    sget-object v1, Lcom/navdy/service/library/events/notification/NotificationsState;->NOTIFICATIONS_STOPPED:Lcom/navdy/service/library/events/notification/NotificationsState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/notification/NotificationsState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_d

    .line 273
    :goto_4
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->values()[Lcom/navdy/hud/app/framework/glance/GlanceApp;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/hud/app/ancs/AncsServiceConnector$4;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp:[I

    :try_start_5
    sget-object v0, Lcom/navdy/hud/app/ancs/AncsServiceConnector$4;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp:[I

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceApp;->GOOGLE_CALENDAR:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_c

    :goto_5
    :try_start_6
    sget-object v0, Lcom/navdy/hud/app/ancs/AncsServiceConnector$4;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp:[I

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceApp;->GOOGLE_MAIL:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_b

    :goto_6
    :try_start_7
    sget-object v0, Lcom/navdy/hud/app/ancs/AncsServiceConnector$4;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp:[I

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceApp;->GENERIC_MAIL:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_a

    :goto_7
    :try_start_8
    sget-object v0, Lcom/navdy/hud/app/ancs/AncsServiceConnector$4;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp:[I

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceApp;->GOOGLE_HANGOUT:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_9

    :goto_8
    :try_start_9
    sget-object v0, Lcom/navdy/hud/app/ancs/AncsServiceConnector$4;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp:[I

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceApp;->SLACK:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_8

    :goto_9
    :try_start_a
    sget-object v0, Lcom/navdy/hud/app/ancs/AncsServiceConnector$4;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp:[I

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceApp;->WHATS_APP:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_7

    :goto_a
    :try_start_b
    sget-object v0, Lcom/navdy/hud/app/ancs/AncsServiceConnector$4;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp:[I

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceApp;->FACEBOOK_MESSENGER:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_6

    :goto_b
    :try_start_c
    sget-object v0, Lcom/navdy/hud/app/ancs/AncsServiceConnector$4;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp:[I

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceApp;->FACEBOOK:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_5

    :goto_c
    :try_start_d
    sget-object v0, Lcom/navdy/hud/app/ancs/AncsServiceConnector$4;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp:[I

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceApp;->TWITTER:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_4

    :goto_d
    :try_start_e
    sget-object v0, Lcom/navdy/hud/app/ancs/AncsServiceConnector$4;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp:[I

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceApp;->IMESSAGE:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_3

    :goto_e
    :try_start_f
    sget-object v0, Lcom/navdy/hud/app/ancs/AncsServiceConnector$4;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp:[I

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceApp;->APPLE_MAIL:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_2

    :goto_f
    :try_start_10
    sget-object v0, Lcom/navdy/hud/app/ancs/AncsServiceConnector$4;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp:[I

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceApp;->APPLE_CALENDAR:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_1

    :goto_10
    :try_start_11
    sget-object v0, Lcom/navdy/hud/app/ancs/AncsServiceConnector$4;->$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp:[I

    sget-object v1, Lcom/navdy/hud/app/framework/glance/GlanceApp;->GENERIC:Lcom/navdy/hud/app/framework/glance/GlanceApp;

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/glance/GlanceApp;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_0

    :goto_11
    return-void

    :catch_0
    move-exception v0

    goto :goto_11

    :catch_1
    move-exception v0

    goto :goto_10

    :catch_2
    move-exception v0

    goto :goto_f

    :catch_3
    move-exception v0

    goto :goto_e

    :catch_4
    move-exception v0

    goto :goto_d

    :catch_5
    move-exception v0

    goto :goto_c

    :catch_6
    move-exception v0

    goto :goto_b

    :catch_7
    move-exception v0

    goto :goto_a

    :catch_8
    move-exception v0

    goto :goto_9

    :catch_9
    move-exception v0

    goto :goto_8

    :catch_a
    move-exception v0

    goto/16 :goto_7

    :catch_b
    move-exception v0

    goto/16 :goto_6

    :catch_c
    move-exception v0

    goto/16 :goto_5

    .line 524
    :catch_d
    move-exception v0

    goto/16 :goto_4

    :catch_e
    move-exception v0

    goto/16 :goto_3

    .line 588
    :catch_f
    move-exception v0

    goto/16 :goto_2

    .line 609
    :catch_10
    move-exception v0

    goto/16 :goto_1

    :catch_11
    move-exception v0

    goto/16 :goto_0
.end method
