.class public Lcom/navdy/hud/app/ancs/AncsGlanceHelper;
.super Ljava/lang/Object;
.source "AncsGlanceHelper.java"


# static fields
.field private static sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    sput-object v0, Lcom/navdy/hud/app/ancs/AncsGlanceHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static buildAppleCalendarEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)Lcom/navdy/service/library/events/glances/GlanceEvent;
    .locals 8
    .param p0, "appId"    # Ljava/lang/String;
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "subtitle"    # Ljava/lang/String;
    .param p4, "message"    # Ljava/lang/String;
    .param p5, "time"    # Ljava/util/Date;

    .prologue
    .line 466
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->getId()Ljava/lang/String;

    move-result-object v2

    .line 467
    .local v2, "notifId":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 468
    .local v1, "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    if-eqz p2, :cond_0

    .line 469
    new-instance v3, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v4, Lcom/navdy/service/library/events/glances/CalendarConstants;->CALENDAR_TITLE:Lcom/navdy/service/library/events/glances/CalendarConstants;

    invoke-virtual {v4}, Lcom/navdy/service/library/events/glances/CalendarConstants;->name()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, p2}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 472
    :cond_0
    if-eqz p4, :cond_1

    .line 473
    new-instance v3, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v4, Lcom/navdy/service/library/events/glances/CalendarConstants;->CALENDAR_LOCATION:Lcom/navdy/service/library/events/glances/CalendarConstants;

    invoke-virtual {v4}, Lcom/navdy/service/library/events/glances/CalendarConstants;->name()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, p4}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 476
    :cond_1
    new-instance v3, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v4, Lcom/navdy/service/library/events/glances/CalendarConstants;->CALENDAR_TIME_STR:Lcom/navdy/service/library/events/glances/CalendarConstants;

    invoke-virtual {v4}, Lcom/navdy/service/library/events/glances/CalendarConstants;->name()Ljava/lang/String;

    move-result-object v4

    .line 477
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getTimeHelper()Lcom/navdy/hud/app/common/TimeHelper;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, p5, v6}, Lcom/navdy/hud/app/common/TimeHelper;->formatTime(Ljava/util/Date;Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 479
    new-instance v3, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v4, Lcom/navdy/service/library/events/glances/CalendarConstants;->CALENDAR_TIME:Lcom/navdy/service/library/events/glances/CalendarConstants;

    invoke-virtual {v4}, Lcom/navdy/service/library/events/glances/CalendarConstants;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p5}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 481
    new-instance v3, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    invoke-direct {v3}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;-><init>()V

    sget-object v4, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_CALENDAR:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    .line 482
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceType(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v3

    .line 483
    invoke-virtual {v3, p0}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->provider(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v3

    .line 484
    invoke-virtual {v3, v2}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->id(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v3

    .line 485
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->postTime(Ljava/lang/Long;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v3

    .line 486
    invoke-virtual {v3, v1}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceData(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->build()Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v0

    .line 488
    .local v0, "calendarEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    return-object v0
.end method

.method public static buildAppleMailEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)Lcom/navdy/service/library/events/glances/GlanceEvent;
    .locals 10
    .param p0, "appId"    # Ljava/lang/String;
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "subtitle"    # Ljava/lang/String;
    .param p4, "message"    # Ljava/lang/String;
    .param p5, "time"    # Ljava/util/Date;

    .prologue
    .line 419
    move-object v3, p2

    .line 420
    .local v3, "from":Ljava/lang/String;
    move-object v6, p3

    .line 421
    .local v6, "subject":Ljava/lang/String;
    move-object v0, p4

    .line 422
    .local v0, "body":Ljava/lang/String;
    const/4 v4, 0x0

    .line 424
    .local v4, "isEmailAddress":Z
    const-string v7, "@"

    invoke-virtual {v3, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    const/4 v8, -0x1

    if-eq v7, v8, :cond_0

    .line 425
    const/4 v4, 0x1

    .line 428
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->getId()Ljava/lang/String;

    move-result-object v5

    .line 429
    .local v5, "notifId":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 430
    .local v1, "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    if-eqz v4, :cond_2

    .line 431
    new-instance v7, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v8, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_FROM_EMAIL:Lcom/navdy/service/library/events/glances/EmailConstants;

    invoke-virtual {v8}, Lcom/navdy/service/library/events/glances/EmailConstants;->name()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8, v3}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 436
    :goto_0
    if-eqz v6, :cond_1

    .line 437
    new-instance v7, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v8, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_SUBJECT:Lcom/navdy/service/library/events/glances/EmailConstants;

    invoke-virtual {v8}, Lcom/navdy/service/library/events/glances/EmailConstants;->name()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8, v6}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 440
    :cond_1
    if-eqz v0, :cond_4

    .line 443
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    const-string v8, "this message has no content"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 444
    sget-object v7, Lcom/navdy/hud/app/ancs/AncsGlanceHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "invalid apple mail glance:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 445
    const/4 v2, 0x0

    .line 457
    :goto_1
    return-object v2

    .line 433
    :cond_2
    new-instance v7, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v8, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_FROM_NAME:Lcom/navdy/service/library/events/glances/EmailConstants;

    invoke-virtual {v8}, Lcom/navdy/service/library/events/glances/EmailConstants;->name()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8, v3}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 447
    :cond_3
    new-instance v7, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v8, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_BODY:Lcom/navdy/service/library/events/glances/EmailConstants;

    invoke-virtual {v8}, Lcom/navdy/service/library/events/glances/EmailConstants;->name()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8, v0}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 450
    :cond_4
    new-instance v7, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    invoke-direct {v7}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;-><init>()V

    sget-object v8, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_EMAIL:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    .line 451
    invoke-virtual {v7, v8}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceType(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v7

    .line 452
    invoke-virtual {v7, p0}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->provider(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v7

    .line 453
    invoke-virtual {v7, v5}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->id(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v7

    .line 454
    invoke-virtual {p5}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->postTime(Ljava/lang/Long;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v7

    .line 455
    invoke-virtual {v7, v1}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceData(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->build()Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v2

    .line 457
    .local v2, "emailEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    goto :goto_1
.end method

.method public static buildFacebookEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)Lcom/navdy/service/library/events/glances/GlanceEvent;
    .locals 6
    .param p0, "appId"    # Ljava/lang/String;
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "subtitle"    # Ljava/lang/String;
    .param p4, "message"    # Ljava/lang/String;
    .param p5, "time"    # Ljava/util/Date;

    .prologue
    .line 289
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->getId()Ljava/lang/String;

    move-result-object v2

    .line 290
    .local v2, "notifId":Ljava/lang/String;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 291
    .local v0, "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    new-instance v3, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v4, Lcom/navdy/service/library/events/glances/SocialConstants;->SOCIAL_MESSAGE:Lcom/navdy/service/library/events/glances/SocialConstants;

    invoke-virtual {v4}, Lcom/navdy/service/library/events/glances/SocialConstants;->name()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, p4}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 293
    new-instance v3, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    invoke-direct {v3}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;-><init>()V

    sget-object v4, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_SOCIAL:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    .line 294
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceType(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v3

    .line 295
    invoke-virtual {v3, p0}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->provider(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v3

    .line 296
    invoke-virtual {v3, v2}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->id(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v3

    .line 297
    invoke-virtual {p5}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->postTime(Ljava/lang/Long;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v3

    .line 298
    invoke-virtual {v3, v0}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceData(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->build()Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v1

    .line 300
    .local v1, "messageEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    return-object v1
.end method

.method public static buildFacebookMessengerEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)Lcom/navdy/service/library/events/glances/GlanceEvent;
    .locals 1
    .param p0, "appId"    # Ljava/lang/String;
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "subtitle"    # Ljava/lang/String;
    .param p4, "message"    # Ljava/lang/String;
    .param p5, "time"    # Ljava/util/Date;

    .prologue
    .line 280
    invoke-static {p0, p4, p5}, Lcom/navdy/hud/app/ancs/AncsGlanceHelper;->buildMessageEvent(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v0

    return-object v0
.end method

.method public static buildGenericEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)Lcom/navdy/service/library/events/glances/GlanceEvent;
    .locals 6
    .param p0, "appId"    # Ljava/lang/String;
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "subtitle"    # Ljava/lang/String;
    .param p4, "message"    # Ljava/lang/String;
    .param p5, "time"    # Ljava/util/Date;

    .prologue
    .line 497
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->getId()Ljava/lang/String;

    move-result-object v2

    .line 498
    .local v2, "notifId":Ljava/lang/String;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 499
    .local v0, "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    if-eqz p2, :cond_0

    .line 500
    new-instance v3, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v4, Lcom/navdy/service/library/events/glances/GenericConstants;->GENERIC_TITLE:Lcom/navdy/service/library/events/glances/GenericConstants;

    invoke-virtual {v4}, Lcom/navdy/service/library/events/glances/GenericConstants;->name()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, p2}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 503
    :cond_0
    if-eqz p4, :cond_1

    .line 504
    new-instance v3, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v4, Lcom/navdy/service/library/events/glances/GenericConstants;->GENERIC_MESSAGE:Lcom/navdy/service/library/events/glances/GenericConstants;

    invoke-virtual {v4}, Lcom/navdy/service/library/events/glances/GenericConstants;->name()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, p4}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 507
    :cond_1
    new-instance v3, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    invoke-direct {v3}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;-><init>()V

    sget-object v4, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_GENERIC:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    .line 508
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceType(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v3

    .line 509
    invoke-virtual {v3, p0}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->provider(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v3

    .line 510
    invoke-virtual {v3, v2}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->id(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v3

    .line 511
    invoke-virtual {p5}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->postTime(Ljava/lang/Long;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v3

    .line 512
    invoke-virtual {v3, v0}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceData(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->build()Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v1

    .line 514
    .local v1, "genericEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    return-object v1
.end method

.method public static buildGenericMailEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)Lcom/navdy/service/library/events/glances/GlanceEvent;
    .locals 2
    .param p0, "appId"    # Ljava/lang/String;
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "subtitle"    # Ljava/lang/String;
    .param p4, "message"    # Ljava/lang/String;
    .param p5, "time"    # Ljava/util/Date;

    .prologue
    const/4 v0, 0x0

    .line 167
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 174
    :cond_0
    :goto_0
    return-object v0

    .line 171
    :cond_1
    const-string v1, "com.microsoft.Office.Outlook"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 172
    invoke-static/range {p0 .. p5}, Lcom/navdy/hud/app/ancs/AncsGlanceHelper;->buildOutlookMailEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v0

    goto :goto_0
.end method

.method public static buildGoogleCalendarEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)Lcom/navdy/service/library/events/glances/GlanceEvent;
    .locals 16
    .param p0, "appId"    # Ljava/lang/String;
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "subtitle"    # Ljava/lang/String;
    .param p4, "message"    # Ljava/lang/String;
    .param p5, "time"    # Ljava/util/Date;

    .prologue
    .line 36
    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 37
    const/4 v2, 0x0

    .line 91
    :goto_0
    return-object v2

    .line 40
    :cond_0
    const/4 v7, 0x0

    .line 41
    .local v7, "meetingTitle":Ljava/lang/String;
    const/4 v6, 0x0

    .line 42
    .local v6, "meetingTime":Ljava/lang/String;
    const/4 v5, 0x0

    .line 44
    .local v5, "meetingLocation":Ljava/lang/String;
    new-instance v10, Ljava/util/StringTokenizer;

    const-string v11, "\n"

    move-object/from16 v0, p4

    invoke-direct {v10, v0, v11}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    .local v10, "tokenizer":Ljava/util/StringTokenizer;
    const/4 v3, 0x0

    .line 46
    .local v3, "counter":I
    :goto_1
    invoke-virtual {v10}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 47
    invoke-virtual {v10}, Ljava/util/StringTokenizer;->nextElement()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 49
    .local v9, "text":Ljava/lang/String;
    packed-switch v3, :pswitch_data_0

    .line 63
    :goto_2
    add-int/lit8 v3, v3, 0x1

    .line 64
    goto :goto_1

    .line 51
    :pswitch_0
    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    .line 52
    goto :goto_2

    .line 55
    :pswitch_1
    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 56
    goto :goto_2

    .line 59
    :pswitch_2
    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    goto :goto_2

    .line 66
    .end local v9    # "text":Ljava/lang/String;
    :cond_1
    sget-object v11, Lcom/navdy/hud/app/ancs/AncsGlanceHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "[ancs-gcalendar] title["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "] when["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "] location["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "]"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 68
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->getId()Ljava/lang/String;

    move-result-object v8

    .line 70
    .local v8, "notifId":Ljava/lang/String;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 71
    .local v4, "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    if-eqz v7, :cond_2

    .line 72
    new-instance v11, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v12, Lcom/navdy/service/library/events/glances/CalendarConstants;->CALENDAR_TITLE:Lcom/navdy/service/library/events/glances/CalendarConstants;

    invoke-virtual {v12}, Lcom/navdy/service/library/events/glances/CalendarConstants;->name()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12, v7}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 75
    :cond_2
    if-eqz v6, :cond_3

    .line 76
    new-instance v11, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v12, Lcom/navdy/service/library/events/glances/CalendarConstants;->CALENDAR_TIME_STR:Lcom/navdy/service/library/events/glances/CalendarConstants;

    invoke-virtual {v12}, Lcom/navdy/service/library/events/glances/CalendarConstants;->name()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12, v6}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    :cond_3
    new-instance v11, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v12, Lcom/navdy/service/library/events/glances/CalendarConstants;->CALENDAR_TIME:Lcom/navdy/service/library/events/glances/CalendarConstants;

    invoke-virtual {v12}, Lcom/navdy/service/library/events/glances/CalendarConstants;->name()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {p5 .. p5}, Ljava/util/Date;->getTime()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v11, v12, v13}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    if-eqz v5, :cond_4

    .line 81
    new-instance v11, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v12, Lcom/navdy/service/library/events/glances/CalendarConstants;->CALENDAR_LOCATION:Lcom/navdy/service/library/events/glances/CalendarConstants;

    invoke-virtual {v12}, Lcom/navdy/service/library/events/glances/CalendarConstants;->name()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12, v5}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    :cond_4
    new-instance v11, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    invoke-direct {v11}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;-><init>()V

    sget-object v12, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_CALENDAR:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    .line 85
    invoke-virtual {v11, v12}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceType(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v11

    .line 86
    move-object/from16 v0, p0

    invoke-virtual {v11, v0}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->provider(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v11

    .line 87
    invoke-virtual {v11, v8}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->id(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v11

    .line 88
    invoke-virtual/range {p5 .. p5}, Ljava/util/Date;->getTime()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->postTime(Ljava/lang/Long;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v11

    .line 89
    invoke-virtual {v11, v4}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceData(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v11

    invoke-virtual {v11}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->build()Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v2

    .line 91
    .local v2, "calendarEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    goto/16 :goto_0

    .line 49
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static buildGoogleHangoutEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)Lcom/navdy/service/library/events/glances/GlanceEvent;
    .locals 1
    .param p0, "appId"    # Ljava/lang/String;
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "subtitle"    # Ljava/lang/String;
    .param p4, "message"    # Ljava/lang/String;
    .param p5, "time"    # Ljava/util/Date;

    .prologue
    .line 242
    invoke-static {p0, p4, p5}, Lcom/navdy/hud/app/ancs/AncsGlanceHelper;->buildMessageEvent(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v0

    return-object v0
.end method

.method public static buildGoogleMailEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)Lcom/navdy/service/library/events/glances/GlanceEvent;
    .locals 14
    .param p0, "appId"    # Ljava/lang/String;
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "subtitle"    # Ljava/lang/String;
    .param p4, "message"    # Ljava/lang/String;
    .param p5, "time"    # Ljava/util/Date;

    .prologue
    .line 100
    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 101
    const/4 v4, 0x0

    .line 158
    :goto_0
    return-object v4

    .line 104
    :cond_0
    const/4 v5, 0x0

    .line 105
    .local v5, "from":Ljava/lang/String;
    const/4 v7, 0x0

    .line 106
    .local v7, "isEmailAddress":Z
    const/4 v9, 0x0

    .line 107
    .local v9, "subject":Ljava/lang/String;
    const/4 v2, 0x0

    .line 109
    .local v2, "body":Ljava/lang/String;
    const-string v10, "\n"

    move-object/from16 v0, p4

    invoke-virtual {v0, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 111
    .local v6, "index":I
    const/4 v10, -0x1

    if-eq v6, v10, :cond_2

    .line 113
    const/4 v10, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v10, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    .line 114
    const-string v10, "@"

    invoke-virtual {v5, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v10

    const/4 v11, -0x1

    if-eq v10, v11, :cond_1

    .line 115
    const/4 v7, 0x1

    .line 117
    :cond_1
    add-int/lit8 v10, v6, 0x1

    move-object/from16 v0, p4

    invoke-virtual {v0, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p4

    .line 120
    :cond_2
    const-string v10, "\u2022"

    move-object/from16 v0, p4

    invoke-virtual {v0, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 122
    const/4 v10, -0x1

    if-eq v6, v10, :cond_6

    .line 123
    const/4 v10, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v10, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    .line 124
    add-int/lit8 v10, v6, 0x1

    move-object/from16 v0, p4

    invoke-virtual {v0, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 129
    :goto_1
    sget-object v10, Lcom/navdy/hud/app/ancs/AncsGlanceHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "[ancs-gmail] from["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "] subject["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "] body["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "]"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 131
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->getId()Ljava/lang/String;

    move-result-object v8

    .line 133
    .local v8, "notifId":Ljava/lang/String;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 135
    .local v3, "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    if-eqz v5, :cond_3

    .line 136
    if-eqz v7, :cond_7

    .line 137
    new-instance v10, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v11, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_FROM_EMAIL:Lcom/navdy/service/library/events/glances/EmailConstants;

    invoke-virtual {v11}, Lcom/navdy/service/library/events/glances/EmailConstants;->name()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11, v5}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 143
    :cond_3
    :goto_2
    if-eqz v9, :cond_4

    .line 144
    new-instance v10, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v11, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_SUBJECT:Lcom/navdy/service/library/events/glances/EmailConstants;

    invoke-virtual {v11}, Lcom/navdy/service/library/events/glances/EmailConstants;->name()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11, v9}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 147
    :cond_4
    if-eqz v2, :cond_5

    .line 148
    new-instance v10, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v11, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_BODY:Lcom/navdy/service/library/events/glances/EmailConstants;

    invoke-virtual {v11}, Lcom/navdy/service/library/events/glances/EmailConstants;->name()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11, v2}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 151
    :cond_5
    new-instance v10, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    invoke-direct {v10}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;-><init>()V

    sget-object v11, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_EMAIL:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    .line 152
    invoke-virtual {v10, v11}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceType(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v10

    .line 153
    invoke-virtual {v10, p0}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->provider(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v10

    .line 154
    invoke-virtual {v10, v8}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->id(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v10

    .line 155
    invoke-virtual/range {p5 .. p5}, Ljava/util/Date;->getTime()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->postTime(Ljava/lang/Long;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v10

    .line 156
    invoke-virtual {v10, v3}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceData(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v10

    invoke-virtual {v10}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->build()Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v4

    .line 158
    .local v4, "emailEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    goto/16 :goto_0

    .line 126
    .end local v3    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    .end local v4    # "emailEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    .end local v8    # "notifId":Ljava/lang/String;
    :cond_6
    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 139
    .restart local v3    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    .restart local v8    # "notifId":Ljava/lang/String;
    :cond_7
    new-instance v10, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v11, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_FROM_NAME:Lcom/navdy/service/library/events/glances/EmailConstants;

    invoke-virtual {v11}, Lcom/navdy/service/library/events/glances/EmailConstants;->name()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11, v5}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method private static buildMessageEvent(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)Lcom/navdy/service/library/events/glances/GlanceEvent;
    .locals 10
    .param p0, "appId"    # Ljava/lang/String;
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "time"    # Ljava/util/Date;

    .prologue
    .line 518
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 519
    const/4 v4, 0x0

    .line 546
    :goto_0
    return-object v4

    .line 522
    :cond_0
    const/4 v2, 0x0

    .line 523
    .local v2, "from":Ljava/lang/String;
    const/4 v0, 0x0

    .line 525
    .local v0, "body":Ljava/lang/String;
    const-string v6, ":"

    invoke-virtual {p1, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 527
    .local v3, "index":I
    const/4 v6, -0x1

    if-eq v3, v6, :cond_1

    .line 528
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 529
    add-int/lit8 v6, v3, 0x1

    invoke-virtual {p1, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 534
    :goto_1
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->getId()Ljava/lang/String;

    move-result-object v5

    .line 535
    .local v5, "notifId":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 536
    .local v1, "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    new-instance v6, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v7, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_FROM:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v7}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7, v2}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 537
    new-instance v6, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v7, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_BODY:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v7}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7, v0}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 539
    new-instance v6, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    invoke-direct {v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;-><init>()V

    sget-object v7, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_MESSAGE:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    .line 540
    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceType(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v6

    .line 541
    invoke-virtual {v6, p0}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->provider(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v6

    .line 542
    invoke-virtual {v6, v5}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->id(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v6

    .line 543
    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->postTime(Ljava/lang/Long;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v6

    .line 544
    invoke-virtual {v6, v1}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceData(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->build()Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v4

    .line 546
    .local v4, "messageEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    goto :goto_0

    .line 531
    .end local v1    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    .end local v4    # "messageEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    .end local v5    # "notifId":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public static buildOutlookMailEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)Lcom/navdy/service/library/events/glances/GlanceEvent;
    .locals 10
    .param p0, "appId"    # Ljava/lang/String;
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "subtitle"    # Ljava/lang/String;
    .param p4, "message"    # Ljava/lang/String;
    .param p5, "time"    # Ljava/util/Date;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 182
    move-object v4, p2

    .line 183
    .local v4, "from":Ljava/lang/String;
    const/4 v6, 0x0

    .line 184
    .local v6, "subject":Ljava/lang/String;
    const/4 v0, 0x0

    .line 195
    .local v0, "body":Ljava/lang/String;
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 196
    const-string v7, "\n"

    invoke-virtual {p4, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 197
    .local v3, "fields":[Ljava/lang/String;
    array-length v7, v3

    if-le v7, v9, :cond_4

    .line 198
    aget-object v6, v3, v8

    .line 199
    aget-object v0, v3, v9

    .line 204
    .end local v3    # "fields":[Ljava/lang/String;
    :cond_0
    :goto_0
    sget-object v7, Lcom/navdy/hud/app/ancs/AncsGlanceHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[ancs-outlook-email] from["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "] subject["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "] body["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 206
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->getId()Ljava/lang/String;

    move-result-object v5

    .line 208
    .local v5, "notifId":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 210
    .local v1, "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 211
    const-string v7, "@"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 212
    new-instance v7, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v8, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_FROM_EMAIL:Lcom/navdy/service/library/events/glances/EmailConstants;

    invoke-virtual {v8}, Lcom/navdy/service/library/events/glances/EmailConstants;->name()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8, v4}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 218
    :cond_1
    :goto_1
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 219
    new-instance v7, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v8, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_SUBJECT:Lcom/navdy/service/library/events/glances/EmailConstants;

    invoke-virtual {v8}, Lcom/navdy/service/library/events/glances/EmailConstants;->name()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8, v6}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 222
    :cond_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 223
    new-instance v7, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v8, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_BODY:Lcom/navdy/service/library/events/glances/EmailConstants;

    invoke-virtual {v8}, Lcom/navdy/service/library/events/glances/EmailConstants;->name()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8, v0}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 226
    :cond_3
    new-instance v7, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    invoke-direct {v7}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;-><init>()V

    sget-object v8, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_EMAIL:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    .line 227
    invoke-virtual {v7, v8}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceType(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v7

    .line 228
    invoke-virtual {v7, p0}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->provider(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v7

    .line 229
    invoke-virtual {v7, v5}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->id(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v7

    .line 230
    invoke-virtual {p5}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->postTime(Ljava/lang/Long;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v7

    .line 231
    invoke-virtual {v7, v1}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceData(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->build()Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v2

    .line 233
    .local v2, "emailEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    return-object v2

    .line 201
    .end local v1    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    .end local v2    # "emailEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    .end local v5    # "notifId":Ljava/lang/String;
    .restart local v3    # "fields":[Ljava/lang/String;
    :cond_4
    aget-object v0, v3, v8

    goto/16 :goto_0

    .line 214
    .end local v3    # "fields":[Ljava/lang/String;
    .restart local v1    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    .restart local v5    # "notifId":Ljava/lang/String;
    :cond_5
    new-instance v7, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v8, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_FROM_NAME:Lcom/navdy/service/library/events/glances/EmailConstants;

    invoke-virtual {v8}, Lcom/navdy/service/library/events/glances/EmailConstants;->name()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8, v4}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public static buildSlackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)Lcom/navdy/service/library/events/glances/GlanceEvent;
    .locals 1
    .param p0, "appId"    # Ljava/lang/String;
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "subtitle"    # Ljava/lang/String;
    .param p4, "message"    # Ljava/lang/String;
    .param p5, "time"    # Ljava/util/Date;

    .prologue
    .line 252
    invoke-static {p0, p4, p5}, Lcom/navdy/hud/app/ancs/AncsGlanceHelper;->buildMessageEvent(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v0

    return-object v0
.end method

.method public static buildTwitterEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)Lcom/navdy/service/library/events/glances/GlanceEvent;
    .locals 14
    .param p0, "appId"    # Ljava/lang/String;
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "subtitle"    # Ljava/lang/String;
    .param p4, "message"    # Ljava/lang/String;
    .param p5, "time"    # Ljava/util/Date;

    .prologue
    .line 310
    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 311
    const/4 v8, 0x0

    .line 376
    :goto_0
    return-object v8

    .line 314
    :cond_0
    const/4 v4, 0x0

    .line 315
    .local v4, "from":Ljava/lang/String;
    const/4 v9, 0x0

    .line 316
    .local v9, "to":Ljava/lang/String;
    const/4 v2, 0x0

    .line 318
    .local v2, "body":Ljava/lang/String;
    const-string v10, ":"

    move-object/from16 v0, p4

    invoke-virtual {v0, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 319
    .local v6, "index":I
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->getId()Ljava/lang/String;

    move-result-object v7

    .line 320
    .local v7, "notifId":Ljava/lang/String;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 321
    .local v3, "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    const/4 v10, -0x1

    if-eq v6, v10, :cond_7

    .line 322
    const/4 v10, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v10, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 323
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_5

    .line 324
    const-string v10, "@"

    invoke-virtual {v4, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    .line 325
    .local v5, "i":I
    const/4 v10, -0x1

    if-eq v5, v10, :cond_1

    .line 326
    invoke-virtual {v4, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 331
    .end local v5    # "i":I
    :cond_1
    :goto_1
    add-int/lit8 v10, v6, 0x1

    move-object/from16 v0, p4

    invoke-virtual {v0, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p4

    .line 332
    const-string v10, " "

    move-object/from16 v0, p4

    invoke-virtual {v0, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 333
    const/4 v10, -0x1

    if-eq v6, v10, :cond_6

    .line 334
    const/4 v10, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v10, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    .line 335
    add-int/lit8 v10, v6, 0x1

    move-object/from16 v0, p4

    invoke-virtual {v0, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 355
    :cond_2
    :goto_2
    if-eqz v4, :cond_3

    .line 356
    new-instance v10, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v11, Lcom/navdy/service/library/events/glances/SocialConstants;->SOCIAL_FROM:Lcom/navdy/service/library/events/glances/SocialConstants;

    invoke-virtual {v11}, Lcom/navdy/service/library/events/glances/SocialConstants;->name()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11, v4}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 359
    :cond_3
    if-eqz v9, :cond_4

    .line 360
    new-instance v10, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v11, Lcom/navdy/service/library/events/glances/SocialConstants;->SOCIAL_TO:Lcom/navdy/service/library/events/glances/SocialConstants;

    invoke-virtual {v11}, Lcom/navdy/service/library/events/glances/SocialConstants;->name()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11, v9}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 363
    :cond_4
    if-eqz v2, :cond_a

    .line 364
    new-instance v10, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v11, Lcom/navdy/service/library/events/glances/SocialConstants;->SOCIAL_MESSAGE:Lcom/navdy/service/library/events/glances/SocialConstants;

    invoke-virtual {v11}, Lcom/navdy/service/library/events/glances/SocialConstants;->name()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11, v2}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 369
    :goto_3
    new-instance v10, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    invoke-direct {v10}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;-><init>()V

    sget-object v11, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_SOCIAL:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    .line 370
    invoke-virtual {v10, v11}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceType(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v10

    .line 371
    invoke-virtual {v10, p0}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->provider(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v10

    .line 372
    invoke-virtual {v10, v7}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->id(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v10

    .line 373
    invoke-virtual/range {p5 .. p5}, Ljava/util/Date;->getTime()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->postTime(Ljava/lang/Long;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v10

    .line 374
    invoke-virtual {v10, v3}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceData(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v10

    invoke-virtual {v10}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->build()Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v8

    .line 376
    .local v8, "socialEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    goto/16 :goto_0

    .line 329
    .end local v8    # "socialEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    :cond_5
    move-object/from16 v4, p2

    goto/16 :goto_1

    .line 337
    :cond_6
    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 339
    :cond_7
    const-string v10, "\n"

    move-object/from16 v0, p4

    invoke-virtual {v0, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    const/4 v10, -0x1

    if-eq v6, v10, :cond_8

    .line 341
    const/4 v10, -0x1

    if-eq v6, v10, :cond_2

    .line 342
    const/4 v10, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v10, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 343
    add-int/lit8 v10, v6, 0x1

    move-object/from16 v0, p4

    invoke-virtual {v0, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    .line 346
    :cond_8
    const-string v10, " "

    move-object/from16 v0, p4

    invoke-virtual {v0, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 347
    const/4 v10, -0x1

    if-eq v6, v10, :cond_9

    .line 348
    const/4 v10, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v10, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    .line 349
    add-int/lit8 v10, v6, 0x1

    move-object/from16 v0, p4

    invoke-virtual {v0, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    .line 351
    :cond_9
    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    .line 366
    :cond_a
    new-instance v10, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v11, Lcom/navdy/service/library/events/glances/SocialConstants;->SOCIAL_MESSAGE:Lcom/navdy/service/library/events/glances/SocialConstants;

    invoke-virtual {v11}, Lcom/navdy/service/library/events/glances/SocialConstants;->name()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p4

    invoke-direct {v10, v11, v0}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3
.end method

.method public static buildWhatsappEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)Lcom/navdy/service/library/events/glances/GlanceEvent;
    .locals 6
    .param p0, "appId"    # Ljava/lang/String;
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "subtitle"    # Ljava/lang/String;
    .param p4, "message"    # Ljava/lang/String;
    .param p5, "time"    # Ljava/util/Date;

    .prologue
    .line 261
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->getId()Ljava/lang/String;

    move-result-object v2

    .line 262
    .local v2, "notifId":Ljava/lang/String;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 263
    .local v0, "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    new-instance v3, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v4, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_FROM:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v4}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, p2}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 264
    new-instance v3, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v4, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_BODY:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v4}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, p4}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 265
    new-instance v3, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    invoke-direct {v3}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;-><init>()V

    sget-object v4, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_MESSAGE:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    .line 266
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceType(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v3

    .line 267
    invoke-virtual {v3, p0}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->provider(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v3

    .line 268
    invoke-virtual {v3, v2}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->id(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v3

    .line 269
    invoke-virtual {p5}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->postTime(Ljava/lang/Long;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v3

    .line 270
    invoke-virtual {v3, v0}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceData(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->build()Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v1

    .line 271
    .local v1, "messageEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    return-object v1
.end method

.method public static buildiMessageEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)Lcom/navdy/service/library/events/glances/GlanceEvent;
    .locals 8
    .param p0, "appId"    # Ljava/lang/String;
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "subtitle"    # Ljava/lang/String;
    .param p4, "message"    # Ljava/lang/String;
    .param p5, "time"    # Ljava/util/Date;

    .prologue
    .line 385
    invoke-static {}, Lcom/navdy/hud/app/framework/glance/GlanceHelper;->getId()Ljava/lang/String;

    move-result-object v3

    .line 386
    .local v3, "notifId":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 387
    .local v1, "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    invoke-static {p2}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->isValidNumber(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 388
    new-instance v4, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v5, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_FROM_NUMBER:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v5}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, p2}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 393
    :goto_0
    new-instance v4, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v5, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_BODY:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v5}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, p4}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 395
    new-instance v4, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    invoke-direct {v4}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;-><init>()V

    sget-object v5, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_MESSAGE:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    .line 396
    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceType(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v4

    .line 397
    invoke-virtual {v4, p0}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->provider(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v4

    .line 398
    invoke-virtual {v4, v3}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->id(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v4

    .line 399
    invoke-virtual {p5}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->postTime(Ljava/lang/Long;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v4

    .line 400
    invoke-virtual {v4, v1}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceData(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v2

    .line 402
    .local v2, "iMessageEventBuilder":Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;
    invoke-static {}, Lcom/navdy/hud/app/ui/component/UISettings;->supportsIosSms()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 405
    new-instance v0, Ljava/util/ArrayList;

    const/4 v4, 0x1

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 406
    .local v0, "action":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;>;"
    sget-object v4, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;->REPLY:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 407
    invoke-virtual {v2, v0}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->actions(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    .line 410
    .end local v0    # "action":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;>;"
    :cond_0
    invoke-virtual {v2}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->build()Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v4

    return-object v4

    .line 390
    .end local v2    # "iMessageEventBuilder":Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;
    :cond_1
    new-instance v4, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v5, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_FROM:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v5}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, p2}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
