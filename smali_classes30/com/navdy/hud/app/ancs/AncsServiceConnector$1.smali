.class Lcom/navdy/hud/app/ancs/AncsServiceConnector$1;
.super Lcom/navdy/ancs/IAncsServiceListener$Stub;
.source "AncsServiceConnector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/ancs/AncsServiceConnector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/ancs/AncsServiceConnector;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/ancs/AncsServiceConnector;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/ancs/AncsServiceConnector;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector$1;->this$0:Lcom/navdy/hud/app/ancs/AncsServiceConnector;

    invoke-direct {p0}, Lcom/navdy/ancs/IAncsServiceListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnectionStateChange(I)V
    .locals 5
    .param p1, "newState"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 128
    sget-object v2, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ANCS connection state change - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 129
    const/4 v1, 0x0

    .line 130
    .local v1, "mappedState":Lcom/navdy/service/library/events/notification/NotificationsState;
    const/4 v0, 0x0

    .line 131
    .local v0, "error":Lcom/navdy/service/library/events/notification/NotificationsError;
    packed-switch p1, :pswitch_data_0

    .line 152
    :goto_0
    :pswitch_0
    if-eqz v1, :cond_0

    .line 153
    iget-object v2, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector$1;->this$0:Lcom/navdy/hud/app/ancs/AncsServiceConnector;

    # invokes: Lcom/navdy/hud/app/ancs/AncsServiceConnector;->setNotificationsState(Lcom/navdy/service/library/events/notification/NotificationsState;Lcom/navdy/service/library/events/notification/NotificationsError;)V
    invoke-static {v2, v1, v0}, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->access$000(Lcom/navdy/hud/app/ancs/AncsServiceConnector;Lcom/navdy/service/library/events/notification/NotificationsState;Lcom/navdy/service/library/events/notification/NotificationsError;)V

    .line 155
    :cond_0
    return-void

    .line 133
    :pswitch_1
    sget-object v1, Lcom/navdy/service/library/events/notification/NotificationsState;->NOTIFICATIONS_STOPPED:Lcom/navdy/service/library/events/notification/NotificationsState;

    .line 134
    goto :goto_0

    .line 136
    :pswitch_2
    sget-object v1, Lcom/navdy/service/library/events/notification/NotificationsState;->NOTIFICATIONS_ENABLED:Lcom/navdy/service/library/events/notification/NotificationsState;

    .line 137
    goto :goto_0

    .line 139
    :pswitch_3
    sget-object v1, Lcom/navdy/service/library/events/notification/NotificationsState;->NOTIFICATIONS_CONNECTING:Lcom/navdy/service/library/events/notification/NotificationsState;

    .line 140
    goto :goto_0

    .line 142
    :pswitch_4
    sget-object v0, Lcom/navdy/service/library/events/notification/NotificationsError;->NOTIFICATIONS_ERROR_AUTH_FAILED:Lcom/navdy/service/library/events/notification/NotificationsError;

    .line 143
    sget-object v1, Lcom/navdy/service/library/events/notification/NotificationsState;->NOTIFICATIONS_PAIRING_FAILED:Lcom/navdy/service/library/events/notification/NotificationsState;

    .line 144
    goto :goto_0

    .line 146
    :pswitch_5
    sget-object v0, Lcom/navdy/service/library/events/notification/NotificationsError;->NOTIFICATIONS_ERROR_BOND_REMOVED:Lcom/navdy/service/library/events/notification/NotificationsError;

    .line 147
    sget-object v1, Lcom/navdy/service/library/events/notification/NotificationsState;->NOTIFICATIONS_PAIRING_FAILED:Lcom/navdy/service/library/events/notification/NotificationsState;

    .line 148
    goto :goto_0

    .line 131
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public onNotification(Lcom/navdy/ancs/AppleNotification;)V
    .locals 12
    .param p1, "notification"    # Lcom/navdy/ancs/AppleNotification;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 159
    invoke-virtual {p1}, Lcom/navdy/ancs/AppleNotification;->getAppId()Ljava/lang/String;

    move-result-object v0

    .line 161
    .local v0, "appId":Ljava/lang/String;
    sget-object v8, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Hud listener notified of apple notification id["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 163
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->isRemoteDeviceConnected()Z

    move-result v8

    if-nez v8, :cond_1

    .line 164
    sget-object v8, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "not connected"

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 201
    :cond_0
    :goto_0
    return-void

    .line 168
    :cond_1
    # getter for: Lcom/navdy/hud/app/ancs/AncsServiceConnector;->blackList:Ljava/util/HashMap;
    invoke-static {}, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->access$100()Ljava/util/HashMap;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    if-eqz v8, :cond_2

    const/4 v1, 0x1

    .line 169
    .local v1, "blacklisted":Z
    :goto_1
    if-nez v1, :cond_0

    invoke-virtual {p1}, Lcom/navdy/ancs/AppleNotification;->getEventId()I

    move-result v8

    if-nez v8, :cond_0

    .line 170
    iget-object v8, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector$1;->this$0:Lcom/navdy/hud/app/ancs/AncsServiceConnector;

    iget-object v8, v8, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->mNotificationSettings:Lcom/navdy/hud/app/profile/NotificationSettings;

    invoke-virtual {v8, p1}, Lcom/navdy/hud/app/profile/NotificationSettings;->enabled(Lcom/navdy/ancs/AppleNotification;)Ljava/lang/Boolean;

    move-result-object v4

    .line 171
    .local v4, "enabled":Ljava/lang/Boolean;
    sget-object v8, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v8, v4}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-virtual {p1}, Lcom/navdy/ancs/AppleNotification;->isPreExisting()Z

    move-result v8

    if-nez v8, :cond_4

    .line 172
    iget-object v8, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector$1;->this$0:Lcom/navdy/hud/app/ancs/AncsServiceConnector;

    # invokes: Lcom/navdy/hud/app/ancs/AncsServiceConnector;->convertToGlanceEvent(Lcom/navdy/ancs/AppleNotification;)Lcom/navdy/service/library/events/glances/GlanceEvent;
    invoke-static {v8, p1}, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->access$200(Lcom/navdy/hud/app/ancs/AncsServiceConnector;Lcom/navdy/ancs/AppleNotification;)Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v5

    .line 173
    .local v5, "event":Lcom/navdy/service/library/events/glances/GlanceEvent;
    if-nez v5, :cond_3

    .line 174
    sget-object v8, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "glance not converted"

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 198
    .end local v1    # "blacklisted":Z
    .end local v4    # "enabled":Ljava/lang/Boolean;
    .end local v5    # "event":Lcom/navdy/service/library/events/glances/GlanceEvent;
    :catch_0
    move-exception v7

    .line 199
    .local v7, "t":Ljava/lang/Throwable;
    sget-object v8, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "ancs-notif"

    invoke-virtual {v8, v9, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 168
    .end local v7    # "t":Ljava/lang/Throwable;
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 176
    .restart local v1    # "blacklisted":Z
    .restart local v4    # "enabled":Ljava/lang/Boolean;
    .restart local v5    # "event":Lcom/navdy/service/library/events/glances/GlanceEvent;
    :cond_3
    :try_start_1
    sget-object v8, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "posting notification"

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 177
    iget-object v8, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector$1;->this$0:Lcom/navdy/hud/app/ancs/AncsServiceConnector;

    iget-object v8, v8, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->mBus:Lcom/squareup/otto/Bus;

    invoke-virtual {v8, v5}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 180
    .end local v5    # "event":Lcom/navdy/service/library/events/glances/GlanceEvent;
    :cond_4
    if-nez v4, :cond_0

    .line 182
    # getter for: Lcom/navdy/hud/app/ancs/AncsServiceConnector;->enableNotificationTimeMap:Ljava/util/HashMap;
    invoke-static {}, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->access$300()Ljava/util/HashMap;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    .line 183
    .local v6, "lastEventTime":Ljava/lang/Long;
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 184
    .local v2, "clockTime":J
    if-eqz v6, :cond_5

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    sub-long v8, v2, v8

    const-wide/16 v10, 0x3a98

    cmp-long v8, v8, v10

    if-gez v8, :cond_5

    .line 185
    sget-object v8, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "not sending unknown notification to client, within threshold"

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 189
    :cond_5
    iget-object v8, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector$1;->this$0:Lcom/navdy/hud/app/ancs/AncsServiceConnector;

    # invokes: Lcom/navdy/hud/app/ancs/AncsServiceConnector;->convertNotification(Lcom/navdy/ancs/AppleNotification;)Lcom/navdy/service/library/events/notification/NotificationEvent;
    invoke-static {v8, p1}, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->access$400(Lcom/navdy/hud/app/ancs/AncsServiceConnector;Lcom/navdy/ancs/AppleNotification;)Lcom/navdy/service/library/events/notification/NotificationEvent;

    move-result-object v5

    .line 192
    .local v5, "event":Lcom/navdy/service/library/events/notification/NotificationEvent;
    sget-object v8, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "sending unknown notification to client - "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 193
    iget-object v8, p0, Lcom/navdy/hud/app/ancs/AncsServiceConnector$1;->this$0:Lcom/navdy/hud/app/ancs/AncsServiceConnector;

    iget-object v8, v8, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->mBus:Lcom/squareup/otto/Bus;

    new-instance v9, Lcom/navdy/hud/app/event/RemoteEvent;

    invoke-direct {v9, v5}, Lcom/navdy/hud/app/event/RemoteEvent;-><init>(Lcom/squareup/wire/Message;)V

    invoke-virtual {v8, v9}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 194
    # getter for: Lcom/navdy/hud/app/ancs/AncsServiceConnector;->enableNotificationTimeMap:Ljava/util/HashMap;
    invoke-static {}, Lcom/navdy/hud/app/ancs/AncsServiceConnector;->access$300()Ljava/util/HashMap;

    move-result-object v8

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v8, v0, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
