.class public final Lcom/navdy/hud/app/bluetooth/utils/BluetoothUtils;
.super Ljava/lang/Object;
.source "BluetoothUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getBondState(I)Ljava/lang/String;
    .locals 2
    .param p0, "bondState"    # I

    .prologue
    .line 29
    packed-switch p0, :pswitch_data_0

    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 31
    :pswitch_0
    const-string v0, "Bonded"

    goto :goto_0

    .line 34
    :pswitch_1
    const-string v0, "Bonding"

    goto :goto_0

    .line 37
    :pswitch_2
    const-string v0, "Not Bonded"

    goto :goto_0

    .line 29
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static getConnectedState(I)Ljava/lang/String;
    .locals 2
    .param p0, "state"    # I

    .prologue
    .line 45
    packed-switch p0, :pswitch_data_0

    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 47
    :pswitch_0
    const-string v0, "Connected"

    goto :goto_0

    .line 50
    :pswitch_1
    const-string v0, "Connecting"

    goto :goto_0

    .line 53
    :pswitch_2
    const-string v0, "Disconnected"

    goto :goto_0

    .line 56
    :pswitch_3
    const-string v0, "Disconnecting"

    goto :goto_0

    .line 45
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getDeviceType(I)Ljava/lang/String;
    .locals 2
    .param p0, "deviceType"    # I

    .prologue
    .line 12
    packed-switch p0, :pswitch_data_0

    .line 24
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 14
    :pswitch_0
    const-string v0, "Classic"

    goto :goto_0

    .line 17
    :pswitch_1
    const-string v0, "Dual"

    goto :goto_0

    .line 20
    :pswitch_2
    const-string v0, "LE"

    goto :goto_0

    .line 12
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
