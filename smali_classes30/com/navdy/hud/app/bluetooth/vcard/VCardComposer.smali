.class public Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;
.super Ljava/lang/Object;
.source "VCardComposer.java"


# static fields
.field private static final DEBUG:Z = false

.field public static final FAILURE_REASON_FAILED_TO_GET_DATABASE_INFO:Ljava/lang/String; = "Failed to get database information"

.field public static final FAILURE_REASON_NOT_INITIALIZED:Ljava/lang/String; = "The vCard composer object is not correctly initialized"

.field public static final FAILURE_REASON_NO_ENTRY:Ljava/lang/String; = "There\'s no exportable in the database"

.field public static final FAILURE_REASON_UNSUPPORTED_URI:Ljava/lang/String; = "The Uri vCard composer received is not supported by the composer."

.field private static final LOG_TAG:Ljava/lang/String; = "VCardComposer"

.field public static final NO_ERROR:Ljava/lang/String; = "No error"

.field private static final SHIFT_JIS:Ljava/lang/String; = "SHIFT_JIS"

.field private static final UTF_8:Ljava/lang/String; = "UTF-8"

.field private static final sContactsProjection:[Ljava/lang/String;

.field private static final sImMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mCharset:Ljava/lang/String;

.field private final mContentResolver:Landroid/content/ContentResolver;

.field private mContentUriForRawContactsEntity:Landroid/net/Uri;

.field private mCursor:Landroid/database/Cursor;

.field private mCursorSuppliedFromOutside:Z

.field private mErrorReason:Ljava/lang/String;

.field private mFirstVCardEmittedInDoCoMoCase:Z

.field private mIdColumn:I

.field private mInitDone:Z

.field private final mIsDoCoMo:Z

.field private mPhoneTranslationCallback:Lcom/navdy/hud/app/bluetooth/vcard/VCardPhoneNumberTranslationCallback;

.field private mTerminateCalled:Z

.field private final mVCardType:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 121
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->sImMap:Ljava/util/Map;

    .line 122
    sget-object v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->sImMap:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "X-AIM"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    sget-object v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->sImMap:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "X-MSN"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    sget-object v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->sImMap:Ljava/util/Map;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "X-YAHOO"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    sget-object v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->sImMap:Ljava/util/Map;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "X-ICQ"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    sget-object v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->sImMap:Ljava/util/Map;

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "X-JABBER"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    sget-object v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->sImMap:Ljava/util/Map;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "X-SKYPE-USERNAME"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->sContactsProjection:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 162
    sget v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardConfig;->VCARD_TYPE_DEFAULT:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;-><init>(Landroid/content/Context;ILjava/lang/String;Z)V

    .line 163
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "vcardType"    # I

    .prologue
    .line 169
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;-><init>(Landroid/content/Context;ILjava/lang/String;Z)V

    .line 170
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "vcardType"    # I
    .param p3, "charset"    # Ljava/lang/String;

    .prologue
    .line 173
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;-><init>(Landroid/content/Context;ILjava/lang/String;Z)V

    .line 174
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Z)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "vcardType"    # I
    .param p3, "charset"    # Ljava/lang/String;
    .param p4, "careHandlerErrors"    # Z

    .prologue
    .line 194
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;-><init>(Landroid/content/Context;Landroid/content/ContentResolver;ILjava/lang/String;Z)V

    .line 195
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IZ)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "vcardType"    # I
    .param p3, "careHandlerErrors"    # Z

    .prologue
    .line 181
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;-><init>(Landroid/content/Context;ILjava/lang/String;Z)V

    .line 182
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/ContentResolver;ILjava/lang/String;Z)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resolver"    # Landroid/content/ContentResolver;
    .param p3, "vcardType"    # I
    .param p4, "charset"    # Ljava/lang/String;
    .param p5, "careHandlerErrors"    # Z

    .prologue
    const/4 v0, 0x1

    .line 203
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149
    const-string v1, "No error"

    iput-object v1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mErrorReason:Ljava/lang/String;

    .line 155
    iput-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mTerminateCalled:Z

    .line 206
    iput p3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mVCardType:I

    .line 207
    iput-object p2, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mContentResolver:Landroid/content/ContentResolver;

    .line 209
    invoke-static {p3}, Lcom/navdy/hud/app/bluetooth/vcard/VCardConfig;->isDoCoMo(I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mIsDoCoMo:Z

    .line 211
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string p4, "UTF-8"

    .line 213
    :cond_0
    invoke-static {p3}, Lcom/navdy/hud/app/bluetooth/vcard/VCardConfig;->isVersion30(I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "UTF-8"

    invoke-virtual {v1, p4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 215
    .local v0, "shouldAppendCharsetParam":Z
    :cond_1
    :goto_0
    iget-boolean v1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mIsDoCoMo:Z

    if-nez v1, :cond_2

    if-eqz v0, :cond_6

    .line 216
    :cond_2
    const-string v1, "SHIFT_JIS"

    invoke-virtual {v1, p4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 217
    iput-object p4, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mCharset:Ljava/lang/String;

    .line 236
    :goto_1
    const-string v1, "VCardComposer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Use the charset \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mCharset:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    return-void

    .line 213
    .end local v0    # "shouldAppendCharsetParam":Z
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 222
    .restart local v0    # "shouldAppendCharsetParam":Z
    :cond_4
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 223
    const-string v1, "SHIFT_JIS"

    iput-object v1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mCharset:Ljava/lang/String;

    goto :goto_1

    .line 225
    :cond_5
    iput-object p4, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mCharset:Ljava/lang/String;

    goto :goto_1

    .line 229
    :cond_6
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 230
    const-string v1, "UTF-8"

    iput-object v1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mCharset:Ljava/lang/String;

    goto :goto_1

    .line 232
    :cond_7
    iput-object p4, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mCharset:Ljava/lang/String;

    goto :goto_1
.end method

.method private closeCursorIfAppropriate()V
    .locals 4

    .prologue
    .line 579
    iget-boolean v1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mCursorSuppliedFromOutside:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    .line 581
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 585
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mCursor:Landroid/database/Cursor;

    .line 587
    :cond_0
    return-void

    .line 582
    :catch_0
    move-exception v0

    .line 583
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    const-string v1, "VCardComposer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SQLiteException on Cursor#close(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private createOneEntryInternal(Ljava/lang/String;Ljava/lang/reflect/Method;)Ljava/lang/String;
    .locals 16
    .param p1, "contactId"    # Ljava/lang/String;
    .param p2, "getEntityIteratorMethod"    # Ljava/lang/reflect/Method;

    .prologue
    .line 460
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 465
    .local v9, "contentValuesListMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Landroid/content/ContentValues;>;>;"
    const/4 v12, 0x0

    .line 467
    .local v12, "entityIterator":Landroid/content/EntityIterator;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mContentUriForRawContactsEntity:Landroid/net/Uri;

    .line 468
    .local v2, "uri":Landroid/net/Uri;
    const-string v15, "contact_id=?"

    .line 469
    .local v15, "selection":Ljava/lang/String;
    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v5, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 470
    .local v5, "selectionArgs":[Ljava/lang/String;
    if-eqz p2, :cond_2

    .line 473
    const/4 v1, 0x0

    const/4 v3, 0x5

    :try_start_1
    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mContentResolver:Landroid/content/ContentResolver;

    aput-object v6, v3, v4

    const/4 v4, 0x1

    aput-object v2, v3, v4

    const/4 v4, 0x2

    const-string v6, "contact_id=?"

    aput-object v6, v3, v4

    const/4 v4, 0x3

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const/4 v6, 0x0

    aput-object v6, v3, v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Landroid/content/EntityIterator;

    move-object v12, v0
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 490
    :goto_0
    if-nez v12, :cond_3

    .line 491
    :try_start_2
    const-string v1, "VCardComposer"

    const-string v3, "EntityIterator is null"

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 492
    const-string v1, ""
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 517
    if-eqz v12, :cond_0

    .line 518
    invoke-interface {v12}, Landroid/content/EntityIterator;->close()V

    .line 522
    :cond_0
    :goto_1
    return-object v1

    .line 475
    :catch_0
    move-exception v10

    .line 476
    .local v10, "e":Ljava/lang/IllegalArgumentException;
    :try_start_3
    const-string v1, "VCardComposer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IllegalArgumentException has been thrown: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 477
    invoke-virtual {v10}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 476
    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 517
    .end local v2    # "uri":Landroid/net/Uri;
    .end local v5    # "selectionArgs":[Ljava/lang/String;
    .end local v10    # "e":Ljava/lang/IllegalArgumentException;
    .end local v15    # "selection":Ljava/lang/String;
    :catchall_0
    move-exception v1

    if-eqz v12, :cond_1

    .line 518
    invoke-interface {v12}, Landroid/content/EntityIterator;->close()V

    :cond_1
    throw v1

    .line 478
    .restart local v2    # "uri":Landroid/net/Uri;
    .restart local v5    # "selectionArgs":[Ljava/lang/String;
    .restart local v15    # "selection":Ljava/lang/String;
    :catch_1
    move-exception v10

    .line 479
    .local v10, "e":Ljava/lang/IllegalAccessException;
    :try_start_4
    const-string v1, "VCardComposer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IllegalAccessException has been thrown: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 480
    invoke-virtual {v10}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 479
    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 481
    .end local v10    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v10

    .line 482
    .local v10, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v1, "VCardComposer"

    const-string v3, "InvocationTargetException has been thrown: "

    invoke-static {v1, v3, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 483
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v3, "InvocationTargetException has been thrown"

    invoke-direct {v1, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 486
    .end local v10    # "e":Ljava/lang/reflect/InvocationTargetException;
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const-string v4, "contact_id=?"

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    invoke-static {v1}, Landroid/provider/ContactsContract$RawContacts;->newEntityIterator(Landroid/database/Cursor;)Landroid/content/EntityIterator;

    move-result-object v12

    goto :goto_0

    .line 495
    :cond_3
    invoke-interface {v12}, Landroid/content/EntityIterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_4

    .line 496
    const-string v1, "VCardComposer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Data does not exist. contactId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 497
    const-string v1, ""
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 517
    if-eqz v12, :cond_0

    .line 518
    invoke-interface {v12}, Landroid/content/EntityIterator;->close()V

    goto/16 :goto_1

    .line 500
    :cond_4
    :try_start_5
    invoke-interface {v12}, Landroid/content/EntityIterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 501
    invoke-interface {v12}, Landroid/content/EntityIterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/content/Entity;

    .line 502
    .local v11, "entity":Landroid/content/Entity;
    invoke-virtual {v11}, Landroid/content/Entity;->getSubValues()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/content/Entity$NamedContentValues;

    .line 503
    .local v14, "namedContentValues":Landroid/content/Entity$NamedContentValues;
    iget-object v7, v14, Landroid/content/Entity$NamedContentValues;->values:Landroid/content/ContentValues;

    .line 504
    .local v7, "contentValues":Landroid/content/ContentValues;
    const-string v3, "mimetype"

    invoke-virtual {v7, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 505
    .local v13, "key":Ljava/lang/String;
    if-eqz v13, :cond_5

    .line 507
    invoke-interface {v9, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/List;

    .line 508
    .local v8, "contentValuesList":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentValues;>;"
    if-nez v8, :cond_6

    .line 509
    new-instance v8, Ljava/util/ArrayList;

    .end local v8    # "contentValuesList":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentValues;>;"
    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 510
    .restart local v8    # "contentValuesList":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentValues;>;"
    invoke-interface {v9, v13, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 512
    :cond_6
    invoke-interface {v8, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 517
    .end local v7    # "contentValues":Landroid/content/ContentValues;
    .end local v8    # "contentValuesList":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentValues;>;"
    .end local v11    # "entity":Landroid/content/Entity;
    .end local v13    # "key":Ljava/lang/String;
    .end local v14    # "namedContentValues":Landroid/content/Entity$NamedContentValues;
    :cond_7
    if-eqz v12, :cond_8

    .line 518
    invoke-interface {v12}, Landroid/content/EntityIterator;->close()V

    .line 522
    :cond_8
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->buildVCard(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1
.end method

.method private initInterCursorCreationPart(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7
    .param p1, "contentUri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 400
    iput-boolean v6, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mCursorSuppliedFromOutside:Z

    .line 401
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mContentResolver:Landroid/content/ContentResolver;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mCursor:Landroid/database/Cursor;

    .line 403
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mCursor:Landroid/database/Cursor;

    if-nez v0, :cond_0

    .line 404
    const-string v0, "VCardComposer"

    const-string v1, "Cursor became null unexpectedly"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    const-string v0, "Failed to get database information"

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mErrorReason:Ljava/lang/String;

    move v0, v6

    .line 408
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private initInterFirstPart(Landroid/net/Uri;)Z
    .locals 2
    .param p1, "contentUriForRawContactsEntity"    # Landroid/net/Uri;

    .prologue
    .line 387
    if-eqz p1, :cond_0

    .end local p1    # "contentUriForRawContactsEntity":Landroid/net/Uri;
    :goto_0
    iput-object p1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mContentUriForRawContactsEntity:Landroid/net/Uri;

    .line 390
    iget-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mInitDone:Z

    if-eqz v0, :cond_1

    .line 391
    const-string v0, "VCardComposer"

    const-string v1, "init() is already called"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    const/4 v0, 0x0

    .line 394
    :goto_1
    return v0

    .line 387
    .restart local p1    # "contentUriForRawContactsEntity":Landroid/net/Uri;
    :cond_0
    sget-object p1, Landroid/provider/ContactsContract$RawContactsEntity;->CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    .line 394
    .end local p1    # "contentUriForRawContactsEntity":Landroid/net/Uri;
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method private initInterLastPart()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 425
    iput-boolean v1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mInitDone:Z

    .line 426
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mTerminateCalled:Z

    .line 427
    return v1
.end method

.method private initInterMainPart()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 412
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-nez v1, :cond_2

    .line 417
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->closeCursorIfAppropriate()V

    .line 421
    :cond_1
    :goto_0
    return v0

    .line 420
    :cond_2
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mCursor:Landroid/database/Cursor;

    const-string v2, "_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mIdColumn:I

    .line 421
    iget v1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mIdColumn:I

    if-ltz v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public buildVCard(Ljava/util/Map;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Landroid/content/ContentValues;",
            ">;>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 548
    .local p1, "contentValuesListMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Landroid/content/ContentValues;>;>;"
    if-nez p1, :cond_0

    .line 549
    const-string v1, "VCardComposer"

    const-string v2, "The given map is null. Ignore and return empty String"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 550
    const-string v1, ""

    .line 569
    :goto_0
    return-object v1

    .line 552
    :cond_0
    new-instance v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardBuilder;

    iget v1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mVCardType:I

    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mCharset:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/bluetooth/vcard/VCardBuilder;-><init>(ILjava/lang/String;)V

    .line 553
    .local v0, "builder":Lcom/navdy/hud/app/bluetooth/vcard/VCardBuilder;
    const-string v1, "vnd.android.cursor.item/name"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardBuilder;->appendNameProperties(Ljava/util/List;)Lcom/navdy/hud/app/bluetooth/vcard/VCardBuilder;

    move-result-object v2

    const-string v1, "vnd.android.cursor.item/nickname"

    .line 554
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-virtual {v2, v1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardBuilder;->appendNickNames(Ljava/util/List;)Lcom/navdy/hud/app/bluetooth/vcard/VCardBuilder;

    move-result-object v2

    const-string v1, "vnd.android.cursor.item/phone_v2"

    .line 555
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mPhoneTranslationCallback:Lcom/navdy/hud/app/bluetooth/vcard/VCardPhoneNumberTranslationCallback;

    invoke-virtual {v2, v1, v3}, Lcom/navdy/hud/app/bluetooth/vcard/VCardBuilder;->appendPhones(Ljava/util/List;Lcom/navdy/hud/app/bluetooth/vcard/VCardPhoneNumberTranslationCallback;)Lcom/navdy/hud/app/bluetooth/vcard/VCardBuilder;

    move-result-object v2

    const-string v1, "vnd.android.cursor.item/email_v2"

    .line 557
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-virtual {v2, v1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardBuilder;->appendEmails(Ljava/util/List;)Lcom/navdy/hud/app/bluetooth/vcard/VCardBuilder;

    move-result-object v2

    const-string v1, "vnd.android.cursor.item/postal-address_v2"

    .line 558
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-virtual {v2, v1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardBuilder;->appendPostals(Ljava/util/List;)Lcom/navdy/hud/app/bluetooth/vcard/VCardBuilder;

    move-result-object v2

    const-string v1, "vnd.android.cursor.item/organization"

    .line 559
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-virtual {v2, v1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardBuilder;->appendOrganizations(Ljava/util/List;)Lcom/navdy/hud/app/bluetooth/vcard/VCardBuilder;

    move-result-object v2

    const-string v1, "vnd.android.cursor.item/website"

    .line 560
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-virtual {v2, v1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardBuilder;->appendWebsites(Ljava/util/List;)Lcom/navdy/hud/app/bluetooth/vcard/VCardBuilder;

    .line 561
    iget v1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mVCardType:I

    const/high16 v2, 0x800000

    and-int/2addr v1, v2

    if-nez v1, :cond_1

    .line 562
    const-string v1, "vnd.android.cursor.item/photo"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardBuilder;->appendPhotos(Ljava/util/List;)Lcom/navdy/hud/app/bluetooth/vcard/VCardBuilder;

    .line 564
    :cond_1
    const-string v1, "vnd.android.cursor.item/note"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardBuilder;->appendNotes(Ljava/util/List;)Lcom/navdy/hud/app/bluetooth/vcard/VCardBuilder;

    move-result-object v2

    const-string v1, "vnd.android.cursor.item/contact_event"

    .line 565
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-virtual {v2, v1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardBuilder;->appendEvents(Ljava/util/List;)Lcom/navdy/hud/app/bluetooth/vcard/VCardBuilder;

    move-result-object v2

    const-string v1, "vnd.android.cursor.item/im"

    .line 566
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-virtual {v2, v1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardBuilder;->appendIms(Ljava/util/List;)Lcom/navdy/hud/app/bluetooth/vcard/VCardBuilder;

    move-result-object v2

    const-string v1, "vnd.android.cursor.item/sip_address"

    .line 567
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-virtual {v2, v1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardBuilder;->appendSipAddresses(Ljava/util/List;)Lcom/navdy/hud/app/bluetooth/vcard/VCardBuilder;

    move-result-object v2

    const-string v1, "vnd.android.cursor.item/relation"

    .line 568
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-virtual {v2, v1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardBuilder;->appendRelation(Ljava/util/List;)Lcom/navdy/hud/app/bluetooth/vcard/VCardBuilder;

    .line 569
    invoke-virtual {v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0
.end method

.method public createOneEntry()Ljava/lang/String;
    .locals 1

    .prologue
    .line 434
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->createOneEntry(Ljava/lang/reflect/Method;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public createOneEntry(Ljava/lang/reflect/Method;)Ljava/lang/String;
    .locals 3
    .param p1, "getEntityIteratorMethod"    # Ljava/lang/reflect/Method;

    .prologue
    .line 441
    iget-boolean v1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mIsDoCoMo:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mFirstVCardEmittedInDoCoMoCase:Z

    if-nez v1, :cond_0

    .line 442
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mFirstVCardEmittedInDoCoMoCase:Z

    .line 450
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mCursor:Landroid/database/Cursor;

    iget v2, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mIdColumn:I

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, p1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->createOneEntryInternal(Ljava/lang/String;Ljava/lang/reflect/Method;)Ljava/lang/String;

    move-result-object v0

    .line 452
    .local v0, "vcard":Ljava/lang/String;
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_1

    .line 453
    const-string v1, "VCardComposer"

    const-string v2, "Cursor#moveToNext() returned false"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 455
    :cond_1
    return-object v0
.end method

.method protected finalize()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 592
    :try_start_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mTerminateCalled:Z

    if-nez v0, :cond_0

    .line 593
    const-string v0, "VCardComposer"

    const-string v1, "finalized() is called before terminate() being called"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 596
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 598
    return-void

    .line 596
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 606
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mCursor:Landroid/database/Cursor;

    if-nez v0, :cond_0

    .line 607
    const-string v0, "VCardComposer"

    const-string v1, "This object is not ready yet."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 608
    const/4 v0, 0x0

    .line 610
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public getErrorReason()Ljava/lang/String;
    .locals 1

    .prologue
    .line 629
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mErrorReason:Ljava/lang/String;

    return-object v0
.end method

.method public init()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 249
    invoke-virtual {p0, v0, v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->init(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public init(Landroid/database/Cursor;)Z
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v0, 0x0

    .line 375
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->initInterFirstPart(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 383
    :cond_0
    :goto_0
    return v0

    .line 378
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mCursorSuppliedFromOutside:Z

    .line 379
    iput-object p1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mCursor:Landroid/database/Cursor;

    .line 380
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->initInterMainPart()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 383
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->initInterLastPart()Z

    move-result v0

    goto :goto_0
.end method

.method public init(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7
    .param p1, "contentUri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;
    .param p4, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 288
    sget-object v2, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->sContactsProjection:[Ljava/lang/String;

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v6}, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->init(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method public init(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)Z
    .locals 7
    .param p1, "contentUri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;
    .param p4, "sortOrder"    # Ljava/lang/String;
    .param p5, "contentUriForRawContactsEntity"    # Landroid/net/Uri;

    .prologue
    .line 307
    sget-object v2, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->sContactsProjection:[Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->init(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method public init(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)Z
    .locals 3
    .param p1, "contentUri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;
    .param p6, "contentUriForRawContactsEntity"    # Landroid/net/Uri;

    .prologue
    const/4 v0, 0x0

    .line 351
    const-string v1, "com.android.contacts"

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 353
    const-string v1, "The Uri vCard composer received is not supported by the composer."

    iput-object v1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mErrorReason:Ljava/lang/String;

    .line 367
    :cond_0
    :goto_0
    return v0

    .line 357
    :cond_1
    invoke-direct {p0, p6}, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->initInterFirstPart(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 360
    invoke-direct/range {p0 .. p5}, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->initInterCursorCreationPart(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 364
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->initInterMainPart()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 367
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->initInterLastPart()Z

    move-result v0

    goto :goto_0
.end method

.method public init(Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 7
    .param p1, "selection"    # Ljava/lang/String;
    .param p2, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 279
    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->sContactsProjection:[Ljava/lang/String;

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->init(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method public initWithRawContactsEntityUri(Landroid/net/Uri;)Z
    .locals 7
    .param p1, "contentUriForRawContactsEntity"    # Landroid/net/Uri;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 270
    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->sContactsProjection:[Ljava/lang/String;

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    move-object v6, p1

    invoke-virtual/range {v0 .. v6}, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->init(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method public isAfterLast()Z
    .locals 2

    .prologue
    .line 618
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mCursor:Landroid/database/Cursor;

    if-nez v0, :cond_0

    .line 619
    const-string v0, "VCardComposer"

    const-string v1, "This object is not ready yet."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 620
    const/4 v0, 0x0

    .line 622
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    goto :goto_0
.end method

.method public setPhoneNumberTranslationCallback(Lcom/navdy/hud/app/bluetooth/vcard/VCardPhoneNumberTranslationCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/navdy/hud/app/bluetooth/vcard/VCardPhoneNumberTranslationCallback;

    .prologue
    .line 540
    iput-object p1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mPhoneTranslationCallback:Lcom/navdy/hud/app/bluetooth/vcard/VCardPhoneNumberTranslationCallback;

    .line 541
    return-void
.end method

.method public terminate()V
    .locals 1

    .prologue
    .line 574
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->closeCursorIfAppropriate()V

    .line 575
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardComposer;->mTerminateCalled:Z

    .line 576
    return-void
.end method
