.class public Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryConstructor;
.super Ljava/lang/Object;
.source "VCardEntryConstructor.java"

# interfaces
.implements Lcom/navdy/hud/app/bluetooth/vcard/VCardInterpreter;


# static fields
.field private static LOG_TAG:Ljava/lang/String;


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private mCurrentEntry:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;

.field private final mEntryHandlers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final mEntryStack:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final mVCardType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-string v0, "vCard"

    sput-object v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryConstructor;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 55
    const/high16 v0, -0x40000000    # -2.0f

    invoke-direct {p0, v0, v1, v1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryConstructor;-><init>(ILandroid/accounts/Account;Ljava/lang/String;)V

    .line 56
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "vcardType"    # I

    .prologue
    const/4 v0, 0x0

    .line 59
    invoke-direct {p0, p1, v0, v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryConstructor;-><init>(ILandroid/accounts/Account;Ljava/lang/String;)V

    .line 60
    return-void
.end method

.method public constructor <init>(ILandroid/accounts/Account;)V
    .locals 1
    .param p1, "vcardType"    # I
    .param p2, "account"    # Landroid/accounts/Account;

    .prologue
    .line 63
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryConstructor;-><init>(ILandroid/accounts/Account;Ljava/lang/String;)V

    .line 64
    return-void
.end method

.method public constructor <init>(ILandroid/accounts/Account;Ljava/lang/String;)V
    .locals 1
    .param p1, "vcardType"    # I
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "targetCharset"    # Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryConstructor;->mEntryStack:Ljava/util/List;

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryConstructor;->mEntryHandlers:Ljava/util/List;

    .line 73
    iput p1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryConstructor;->mVCardType:I

    .line 74
    iput-object p2, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryConstructor;->mAccount:Landroid/accounts/Account;

    .line 75
    return-void
.end method


# virtual methods
.method public addEntryHandler(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryHandler;)V
    .locals 1
    .param p1, "entryHandler"    # Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryHandler;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryConstructor;->mEntryHandlers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryConstructor;->mCurrentEntry:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;

    .line 97
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryConstructor;->mEntryStack:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 98
    return-void
.end method

.method public onEntryEnded()V
    .locals 5

    .prologue
    .line 108
    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryConstructor;->mCurrentEntry:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;

    invoke-virtual {v3}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->consolidateFields()V

    .line 109
    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryConstructor;->mEntryHandlers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryHandler;

    .line 110
    .local v0, "entryHandler":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryHandler;
    iget-object v4, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryConstructor;->mCurrentEntry:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;

    invoke-interface {v0, v4}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryHandler;->onEntryCreated(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;)V

    goto :goto_0

    .line 113
    .end local v0    # "entryHandler":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryHandler;
    :cond_0
    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryConstructor;->mEntryStack:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    .line 114
    .local v2, "size":I
    const/4 v3, 0x1

    if-le v2, v3, :cond_1

    .line 115
    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryConstructor;->mEntryStack:Ljava/util/List;

    add-int/lit8 v4, v2, -0x2

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;

    .line 116
    .local v1, "parent":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;
    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryConstructor;->mCurrentEntry:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;

    invoke-virtual {v1, v3}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->addChild(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;)V

    .line 117
    iput-object v1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryConstructor;->mCurrentEntry:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;

    .line 121
    .end local v1    # "parent":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;
    :goto_1
    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryConstructor;->mEntryStack:Ljava/util/List;

    add-int/lit8 v4, v2, -0x1

    invoke-interface {v3, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 122
    return-void

    .line 119
    :cond_1
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryConstructor;->mCurrentEntry:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;

    goto :goto_1
.end method

.method public onEntryStarted()V
    .locals 3

    .prologue
    .line 102
    new-instance v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;

    iget v1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryConstructor;->mVCardType:I

    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryConstructor;->mAccount:Landroid/accounts/Account;

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;-><init>(ILandroid/accounts/Account;)V

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryConstructor;->mCurrentEntry:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;

    .line 103
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryConstructor;->mEntryStack:Ljava/util/List;

    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryConstructor;->mCurrentEntry:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    return-void
.end method

.method public onPropertyCreated(Lcom/navdy/hud/app/bluetooth/vcard/VCardProperty;)V
    .locals 1
    .param p1, "property"    # Lcom/navdy/hud/app/bluetooth/vcard/VCardProperty;

    .prologue
    .line 126
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryConstructor;->mCurrentEntry:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->addProperty(Lcom/navdy/hud/app/bluetooth/vcard/VCardProperty;)V

    .line 127
    return-void
.end method

.method public onVCardEnded()V
    .locals 3

    .prologue
    .line 90
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryConstructor;->mEntryHandlers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryHandler;

    .line 91
    .local v0, "entryHandler":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryHandler;
    invoke-interface {v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryHandler;->onEnd()V

    goto :goto_0

    .line 93
    .end local v0    # "entryHandler":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryHandler;
    :cond_0
    return-void
.end method

.method public onVCardStarted()V
    .locals 3

    .prologue
    .line 83
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryConstructor;->mEntryHandlers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryHandler;

    .line 84
    .local v0, "entryHandler":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryHandler;
    invoke-interface {v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryHandler;->onStart()V

    goto :goto_0

    .line 86
    .end local v0    # "entryHandler":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryHandler;
    :cond_0
    return-void
.end method
