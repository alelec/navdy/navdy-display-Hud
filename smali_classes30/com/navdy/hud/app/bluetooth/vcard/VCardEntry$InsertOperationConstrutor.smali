.class Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$InsertOperationConstrutor;
.super Ljava/lang/Object;
.source "VCardEntry.java"

# interfaces
.implements Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElementIterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InsertOperationConstrutor"
.end annotation


# instance fields
.field private final mBackReferenceIndex:I

.field private final mOperationList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;


# direct methods
.method public constructor <init>(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;Ljava/util/List;I)V
    .locals 0
    .param p3, "backReferenceIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 1714
    .local p2, "operationList":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentProviderOperation;>;"
    iput-object p1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$InsertOperationConstrutor;->this$0:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1715
    iput-object p2, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$InsertOperationConstrutor;->mOperationList:Ljava/util/List;

    .line 1716
    iput p3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$InsertOperationConstrutor;->mBackReferenceIndex:I

    .line 1717
    return-void
.end method


# virtual methods
.method public onElement(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElement;)Z
    .locals 2
    .param p1, "elem"    # Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElement;

    .prologue
    .line 1737
    invoke-interface {p1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElement;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1738
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$InsertOperationConstrutor;->mOperationList:Ljava/util/List;

    iget v1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$InsertOperationConstrutor;->mBackReferenceIndex:I

    invoke-interface {p1, v0, v1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElement;->constructInsertOperation(Ljava/util/List;I)V

    .line 1740
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onElementGroupEnded()V
    .locals 0

    .prologue
    .line 1733
    return-void
.end method

.method public onElementGroupStarted(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryLabel;)V
    .locals 0
    .param p1, "label"    # Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryLabel;

    .prologue
    .line 1729
    return-void
.end method

.method public onIterationEnded()V
    .locals 0

    .prologue
    .line 1725
    return-void
.end method

.method public onIterationStarted()V
    .locals 0

    .prologue
    .line 1721
    return-void
.end method
