.class public abstract Lcom/navdy/hud/app/bluetooth/vcard/VCardParser;
.super Ljava/lang/Object;
.source "VCardParser.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract addInterpreter(Lcom/navdy/hud/app/bluetooth/vcard/VCardInterpreter;)V
.end method

.method public abstract cancel()V
.end method

.method public abstract parse(Ljava/io/InputStream;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/navdy/hud/app/bluetooth/vcard/exception/VCardException;
        }
    .end annotation
.end method

.method public parse(Ljava/io/InputStream;Lcom/navdy/hud/app/bluetooth/vcard/VCardInterpreter;)V
    .locals 0
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "interpreter"    # Lcom/navdy/hud/app/bluetooth/vcard/VCardInterpreter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/navdy/hud/app/bluetooth/vcard/exception/VCardException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 84
    invoke-virtual {p0, p2}, Lcom/navdy/hud/app/bluetooth/vcard/VCardParser;->addInterpreter(Lcom/navdy/hud/app/bluetooth/vcard/VCardInterpreter;)V

    .line 85
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardParser;->parse(Ljava/io/InputStream;)V

    .line 86
    return-void
.end method

.method public abstract parseOne(Ljava/io/InputStream;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/navdy/hud/app/bluetooth/vcard/exception/VCardException;
        }
    .end annotation
.end method
