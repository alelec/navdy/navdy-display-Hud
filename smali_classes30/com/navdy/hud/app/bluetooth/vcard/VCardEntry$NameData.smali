.class public Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;
.super Ljava/lang/Object;
.source "VCardEntry.java"

# interfaces
.implements Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElement;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NameData"
.end annotation


# instance fields
.field public displayName:Ljava/lang/String;

.field private mFamily:Ljava/lang/String;

.field private mFormatted:Ljava/lang/String;

.field private mGiven:Ljava/lang/String;

.field private mMiddle:Ljava/lang/String;

.field private mPhoneticFamily:Ljava/lang/String;

.field private mPhoneticGiven:Ljava/lang/String;

.field private mPhoneticMiddle:Ljava/lang/String;

.field private mPrefix:Ljava/lang/String;

.field private mSortString:Ljava/lang/String;

.field private mSuffix:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$1000(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    .prologue
    .line 122
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mMiddle:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mMiddle:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    .prologue
    .line 122
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mGiven:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mGiven:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    .prologue
    .line 122
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mFamily:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mFamily:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    .prologue
    .line 122
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mFormatted:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mFormatted:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1402(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mSortString:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    .prologue
    .line 122
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticFamily:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$502(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticFamily:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    .prologue
    .line 122
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticMiddle:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$602(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticMiddle:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$700(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    .prologue
    .line 122
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticGiven:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$702(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticGiven:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    .prologue
    .line 122
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mSuffix:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$802(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mSuffix:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$900(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    .prologue
    .line 122
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPrefix:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$902(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPrefix:Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method public constructInsertOperation(Ljava/util/List;I)V
    .locals 4
    .param p2, "backReferenceIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 164
    .local p1, "operationList":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentProviderOperation;>;"
    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    .line 165
    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 166
    .local v0, "builder":Landroid/content/ContentProviderOperation$Builder;
    const-string v2, "raw_contact_id"

    invoke-virtual {v0, v2, p2}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 167
    const-string v2, "mimetype"

    const-string v3, "vnd.android.cursor.item/name"

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 169
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mGiven:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 170
    const-string v2, "data2"

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mGiven:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 172
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mFamily:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 173
    const-string v2, "data3"

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mFamily:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 175
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mMiddle:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 176
    const-string v2, "data5"

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mMiddle:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 178
    :cond_2
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPrefix:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 179
    const-string v2, "data4"

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPrefix:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 181
    :cond_3
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mSuffix:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 182
    const-string v2, "data6"

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mSuffix:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 185
    :cond_4
    const/4 v1, 0x0

    .line 187
    .local v1, "phoneticNameSpecified":Z
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticGiven:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 188
    const-string v2, "data7"

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticGiven:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 189
    const/4 v1, 0x1

    .line 191
    :cond_5
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticFamily:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 192
    const-string v2, "data9"

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticFamily:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 193
    const/4 v1, 0x1

    .line 195
    :cond_6
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticMiddle:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 196
    const-string v2, "data8"

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticMiddle:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 197
    const/4 v1, 0x1

    .line 202
    :cond_7
    if-nez v1, :cond_8

    .line 203
    const-string v2, "data7"

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mSortString:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 206
    :cond_8
    const-string v2, "data1"

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->displayName:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 207
    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 208
    return-void
.end method

.method public emptyPhoneticStructuredName()Z
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticFamily:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticGiven:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticMiddle:Ljava/lang/String;

    .line 158
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public emptyStructuredName()Z
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mFamily:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mGiven:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mMiddle:Ljava/lang/String;

    .line 152
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPrefix:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mSuffix:Ljava/lang/String;

    .line 153
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 221
    if-ne p0, p1, :cond_1

    .line 238
    :cond_0
    :goto_0
    return v1

    .line 224
    :cond_1
    instance-of v3, p1, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    if-nez v3, :cond_2

    move v1, v2

    .line 225
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 227
    check-cast v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    .line 229
    .local v0, "nameData":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;
    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mFamily:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mFamily:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mMiddle:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mMiddle:Ljava/lang/String;

    .line 230
    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mGiven:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mGiven:Ljava/lang/String;

    .line 231
    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPrefix:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPrefix:Ljava/lang/String;

    .line 232
    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mSuffix:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mSuffix:Ljava/lang/String;

    .line 233
    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mFormatted:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mFormatted:Ljava/lang/String;

    .line 234
    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticFamily:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticFamily:Ljava/lang/String;

    .line 235
    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticMiddle:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticMiddle:Ljava/lang/String;

    .line 236
    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticGiven:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticGiven:Ljava/lang/String;

    .line 237
    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mSortString:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mSortString:Ljava/lang/String;

    .line 238
    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public final getEntryLabel()Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryLabel;
    .locals 1

    .prologue
    .line 261
    sget-object v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryLabel;->NAME:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryLabel;

    return-object v0
.end method

.method public getFamily()Ljava/lang/String;
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mFamily:Ljava/lang/String;

    return-object v0
.end method

.method public getFormatted()Ljava/lang/String;
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mFormatted:Ljava/lang/String;

    return-object v0
.end method

.method public getGiven()Ljava/lang/String;
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mGiven:Ljava/lang/String;

    return-object v0
.end method

.method public getMiddle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mMiddle:Ljava/lang/String;

    return-object v0
.end method

.method public getPrefix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPrefix:Ljava/lang/String;

    return-object v0
.end method

.method public getSortString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mSortString:Ljava/lang/String;

    return-object v0
.end method

.method public getSuffix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mSuffix:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 243
    const/16 v3, 0xa

    new-array v2, v3, [Ljava/lang/String;

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mFamily:Ljava/lang/String;

    aput-object v3, v2, v4

    const/4 v3, 0x1

    iget-object v5, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mMiddle:Ljava/lang/String;

    aput-object v5, v2, v3

    const/4 v3, 0x2

    iget-object v5, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mGiven:Ljava/lang/String;

    aput-object v5, v2, v3

    const/4 v3, 0x3

    iget-object v5, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPrefix:Ljava/lang/String;

    aput-object v5, v2, v3

    const/4 v3, 0x4

    iget-object v5, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mSuffix:Ljava/lang/String;

    aput-object v5, v2, v3

    const/4 v3, 0x5

    iget-object v5, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mFormatted:Ljava/lang/String;

    aput-object v5, v2, v3

    const/4 v3, 0x6

    iget-object v5, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticFamily:Ljava/lang/String;

    aput-object v5, v2, v3

    const/4 v3, 0x7

    iget-object v5, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticMiddle:Ljava/lang/String;

    aput-object v5, v2, v3

    const/16 v3, 0x8

    iget-object v5, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticGiven:Ljava/lang/String;

    aput-object v5, v2, v3

    const/16 v3, 0x9

    iget-object v5, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mSortString:Ljava/lang/String;

    aput-object v5, v2, v3

    .line 246
    .local v2, "hashTargets":[Ljava/lang/String;
    const/4 v0, 0x0

    .line 247
    .local v0, "hash":I
    array-length v6, v2

    move v5, v4

    :goto_0
    if-ge v5, v6, :cond_1

    aget-object v1, v2, v5

    .line 248
    .local v1, "hashTarget":Ljava/lang/String;
    mul-int/lit8 v7, v0, 0x1f

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v3

    :goto_1
    add-int v0, v7, v3

    .line 247
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_0

    :cond_0
    move v3, v4

    .line 248
    goto :goto_1

    .line 250
    .end local v1    # "hashTarget":Ljava/lang/String;
    :cond_1
    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mFamily:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mMiddle:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mGiven:Ljava/lang/String;

    .line 213
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPrefix:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mSuffix:Ljava/lang/String;

    .line 214
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mFormatted:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticFamily:Ljava/lang/String;

    .line 215
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticMiddle:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticGiven:Ljava/lang/String;

    .line 216
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mSortString:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setFamily(Ljava/lang/String;)V
    .locals 0
    .param p1, "family"    # Ljava/lang/String;

    .prologue
    .line 293
    iput-object p1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mFamily:Ljava/lang/String;

    return-void
.end method

.method public setGiven(Ljava/lang/String;)V
    .locals 0
    .param p1, "given"    # Ljava/lang/String;

    .prologue
    .line 297
    iput-object p1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mGiven:Ljava/lang/String;

    return-void
.end method

.method public setMiddle(Ljava/lang/String;)V
    .locals 0
    .param p1, "middle"    # Ljava/lang/String;

    .prologue
    .line 295
    iput-object p1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mMiddle:Ljava/lang/String;

    return-void
.end method

.method public setPrefix(Ljava/lang/String;)V
    .locals 0
    .param p1, "prefix"    # Ljava/lang/String;

    .prologue
    .line 299
    iput-object p1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPrefix:Ljava/lang/String;

    return-void
.end method

.method public setSuffix(Ljava/lang/String;)V
    .locals 0
    .param p1, "suffix"    # Ljava/lang/String;

    .prologue
    .line 301
    iput-object p1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mSuffix:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 255
    const-string v0, "family: %s, given: %s, middle: %s, prefix: %s, suffix: %s"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mFamily:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mGiven:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mMiddle:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPrefix:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mSuffix:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
