.class public Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;
.super Ljava/lang/Object;
.source "VCardEntry.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$InsertOperationConstrutor;,
        Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$ToStringIterator;,
        Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$IsIgnorableIterator;,
        Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElementIterator;,
        Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$AndroidCustomData;,
        Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$SipData;,
        Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$AnniversaryData;,
        Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$BirthdayData;,
        Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$WebsiteData;,
        Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NoteData;,
        Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NicknameData;,
        Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhotoData;,
        Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$ImData;,
        Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$OrganizationData;,
        Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PostalData;,
        Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EmailData;,
        Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhoneData;,
        Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;,
        Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElement;,
        Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryLabel;
    }
.end annotation


# static fields
.field private static final DEFAULT_ORGANIZATION_TYPE:I = 0x1

.field private static final LOG_TAG:Ljava/lang/String; = "vCard"

.field private static final sEmptyList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sImMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private mAndroidCustomDataList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$AndroidCustomData;",
            ">;"
        }
    .end annotation
.end field

.field private mAnniversary:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$AnniversaryData;

.field private mBirthday:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$BirthdayData;

.field private mCallTime:Ljava/util/Date;

.field private mCallType:I

.field private mChildren:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mEmailList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EmailData;",
            ">;"
        }
    .end annotation
.end field

.field private mImList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$ImData;",
            ">;"
        }
    .end annotation
.end field

.field private final mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

.field private mNicknameList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NicknameData;",
            ">;"
        }
    .end annotation
.end field

.field private mNoteList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NoteData;",
            ">;"
        }
    .end annotation
.end field

.field private mOrganizationList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$OrganizationData;",
            ">;"
        }
    .end annotation
.end field

.field private mPhoneList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhoneData;",
            ">;"
        }
    .end annotation
.end field

.field private mPhotoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhotoData;",
            ">;"
        }
    .end annotation
.end field

.field private mPostalList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PostalData;",
            ">;"
        }
    .end annotation
.end field

.field private mSipList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$SipData;",
            ">;"
        }
    .end annotation
.end field

.field private final mVCardType:I

.field private mWebsiteList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$WebsiteData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x0

    .line 74
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->sImMap:Ljava/util/Map;

    .line 77
    sget-object v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->sImMap:Ljava/util/Map;

    const-string v1, "X-AIM"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    sget-object v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->sImMap:Ljava/util/Map;

    const-string v1, "X-MSN"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    sget-object v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->sImMap:Ljava/util/Map;

    const-string v1, "X-YAHOO"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    sget-object v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->sImMap:Ljava/util/Map;

    const-string v1, "X-ICQ"

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    sget-object v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->sImMap:Ljava/util/Map;

    const-string v1, "X-JABBER"

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    sget-object v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->sImMap:Ljava/util/Map;

    const-string v1, "X-SKYPE-USERNAME"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    sget-object v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->sImMap:Ljava/util/Map;

    const-string v1, "X-GOOGLE-TALK"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    sget-object v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->sImMap:Ljava/util/Map;

    const-string v1, "X-GOOGLE TALK"

    .line 85
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 84
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1857
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 1858
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->sEmptyList:Ljava/util/List;

    .line 1857
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1757
    const/high16 v0, -0x40000000    # -2.0f

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;-><init>(I)V

    .line 1758
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "vcardType"    # I

    .prologue
    .line 1761
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;-><init>(ILandroid/accounts/Account;)V

    .line 1762
    return-void
.end method

.method public constructor <init>(ILandroid/accounts/Account;)V
    .locals 1
    .param p1, "vcardType"    # I
    .param p2, "account"    # Landroid/accounts/Account;

    .prologue
    .line 1764
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1543
    new-instance v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    invoke-direct {v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    .line 1765
    iput p1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mVCardType:I

    .line 1766
    iput-object p2, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mAccount:Landroid/accounts/Account;

    .line 1767
    return-void
.end method

.method private addCallTime(ILjava/util/Date;)V
    .locals 0
    .param p1, "callType"    # I
    .param p2, "date"    # Ljava/util/Date;

    .prologue
    .line 1811
    iput p1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mCallType:I

    .line 1812
    iput-object p2, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mCallTime:Ljava/util/Date;

    .line 1813
    return-void
.end method

.method private addEmail(ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 2
    .param p1, "type"    # I
    .param p2, "data"    # Ljava/lang/String;
    .param p3, "label"    # Ljava/lang/String;
    .param p4, "isPrimary"    # Z

    .prologue
    .line 1830
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mEmailList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1831
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mEmailList:Ljava/util/List;

    .line 1833
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mEmailList:Ljava/util/List;

    new-instance v1, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EmailData;

    invoke-direct {v1, p2, p1, p3, p4}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EmailData;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1834
    return-void
.end method

.method private addIm(ILjava/lang/String;Ljava/lang/String;IZ)V
    .locals 7
    .param p1, "protocol"    # I
    .param p2, "customProtocol"    # Ljava/lang/String;
    .param p3, "propValue"    # Ljava/lang/String;
    .param p4, "type"    # I
    .param p5, "isPrimary"    # Z

    .prologue
    .line 1973
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mImList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1974
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mImList:Ljava/util/List;

    .line 1976
    :cond_0
    iget-object v6, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mImList:Ljava/util/List;

    new-instance v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$ImData;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$ImData;-><init>(ILjava/lang/String;Ljava/lang/String;IZ)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1977
    return-void
.end method

.method private addNewOrganization(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V
    .locals 8
    .param p1, "organizationName"    # Ljava/lang/String;
    .param p2, "departmentName"    # Ljava/lang/String;
    .param p3, "titleName"    # Ljava/lang/String;
    .param p4, "phoneticName"    # Ljava/lang/String;
    .param p5, "type"    # I
    .param p6, "isPrimary"    # Z

    .prologue
    .line 1850
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mOrganizationList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1851
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mOrganizationList:Ljava/util/List;

    .line 1853
    :cond_0
    iget-object v7, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mOrganizationList:Ljava/util/List;

    new-instance v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$OrganizationData;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$OrganizationData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1855
    return-void
.end method

.method private addNickName(Ljava/lang/String;)V
    .locals 2
    .param p1, "nickName"    # Ljava/lang/String;

    .prologue
    .line 1823
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNicknameList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1824
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNicknameList:Ljava/util/List;

    .line 1826
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNicknameList:Ljava/util/List;

    new-instance v1, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NicknameData;

    invoke-direct {v1, p1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NicknameData;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1827
    return-void
.end method

.method private addNote(Ljava/lang/String;)V
    .locals 2
    .param p1, "note"    # Ljava/lang/String;

    .prologue
    .line 1980
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNoteList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1981
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNoteList:Ljava/util/List;

    .line 1983
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNoteList:Ljava/util/List;

    new-instance v1, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NoteData;

    invoke-direct {v1, p1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NoteData;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1984
    return-void
.end method

.method private addPhone(ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 10
    .param p1, "type"    # I
    .param p2, "data"    # Ljava/lang/String;
    .param p3, "label"    # Ljava/lang/String;
    .param p4, "isPrimary"    # Z

    .prologue
    .line 1770
    iget-object v9, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mPhoneList:Ljava/util/List;

    if-nez v9, :cond_0

    .line 1771
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iput-object v9, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mPhoneList:Ljava/util/List;

    .line 1773
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1774
    .local v0, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    .line 1776
    .local v8, "trimmed":Ljava/lang/String;
    const/4 v9, 0x6

    if-eq p1, v9, :cond_1

    iget v9, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mVCardType:I

    invoke-static {v9}, Lcom/navdy/hud/app/bluetooth/vcard/VCardConfig;->refrainPhoneNumberFormatting(I)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1777
    :cond_1
    move-object v2, v8

    .line 1806
    .local v2, "formattedNumber":Ljava/lang/String;
    :goto_0
    new-instance v7, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhoneData;

    invoke-direct {v7, v2, p1, p3, p4}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhoneData;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    .line 1807
    .local v7, "phoneData":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhoneData;
    iget-object v9, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mPhoneList:Ljava/util/List;

    invoke-interface {v9, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1808
    return-void

    .line 1783
    .end local v2    # "formattedNumber":Ljava/lang/String;
    .end local v7    # "phoneData":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhoneData;
    :cond_2
    const/4 v4, 0x0

    .line 1784
    .local v4, "hasPauseOrWait":Z
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v6

    .line 1785
    .local v6, "length":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    if-ge v5, v6, :cond_a

    .line 1786
    invoke-virtual {v8, v5}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 1788
    .local v1, "ch":C
    const/16 v9, 0x70

    if-eq v1, v9, :cond_3

    const/16 v9, 0x50

    if-ne v1, v9, :cond_5

    .line 1789
    :cond_3
    const/16 v9, 0x2c

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1790
    const/4 v4, 0x1

    .line 1785
    :cond_4
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 1791
    :cond_5
    const/16 v9, 0x77

    if-eq v1, v9, :cond_6

    const/16 v9, 0x57

    if-ne v1, v9, :cond_7

    .line 1792
    :cond_6
    const/16 v9, 0x3b

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1793
    const/4 v4, 0x1

    goto :goto_2

    .line 1794
    :cond_7
    const/16 v9, 0x30

    if-gt v9, v1, :cond_8

    const/16 v9, 0x39

    if-le v1, v9, :cond_9

    :cond_8
    if-nez v5, :cond_4

    const/16 v9, 0x2b

    if-ne v1, v9, :cond_4

    .line 1795
    :cond_9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 1798
    .end local v1    # "ch":C
    :cond_a
    if-nez v4, :cond_b

    .line 1799
    iget v9, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mVCardType:I

    invoke-static {v9}, Lcom/navdy/hud/app/bluetooth/vcard/VCardUtils;->getPhoneNumberFormat(I)I

    move-result v3

    .line 1801
    .local v3, "formattingType":I
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1800
    invoke-static {v9, v3}, Lcom/navdy/hud/app/bluetooth/vcard/VCardUtils$PhoneNumberUtilsPort;->formatNumber(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    .line 1802
    .restart local v2    # "formattedNumber":Ljava/lang/String;
    goto :goto_0

    .line 1803
    .end local v2    # "formattedNumber":Ljava/lang/String;
    .end local v3    # "formattingType":I
    :cond_b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "formattedNumber":Ljava/lang/String;
    goto :goto_0
.end method

.method private addPhotoBytes(Ljava/lang/String;[BZ)V
    .locals 3
    .param p1, "formatName"    # Ljava/lang/String;
    .param p2, "photoBytes"    # [B
    .param p3, "isPrimary"    # Z

    .prologue
    .line 1987
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mPhotoList:Ljava/util/List;

    if-nez v1, :cond_0

    .line 1988
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mPhotoList:Ljava/util/List;

    .line 1990
    :cond_0
    new-instance v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhotoData;

    invoke-direct {v0, p1, p2, p3}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhotoData;-><init>(Ljava/lang/String;[BZ)V

    .line 1991
    .local v0, "photoData":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhotoData;
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mPhotoList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1992
    return-void
.end method

.method private addPostal(ILjava/util/List;Ljava/lang/String;Z)V
    .locals 2
    .param p1, "type"    # I
    .param p3, "label"    # Ljava/lang/String;
    .param p4, "isPrimary"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 1837
    .local p2, "propValueList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mPostalList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1838
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mPostalList:Ljava/util/List;

    .line 1840
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mPostalList:Ljava/util/List;

    iget v1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mVCardType:I

    invoke-static {p2, p1, p3, p4, v1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PostalData;->constructPostalData(Ljava/util/List;ILjava/lang/String;ZI)Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PostalData;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1842
    return-void
.end method

.method private addSip(Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 2
    .param p1, "sipData"    # Ljava/lang/String;
    .param p2, "type"    # I
    .param p3, "label"    # Ljava/lang/String;
    .param p4, "isPrimary"    # Z

    .prologue
    .line 1816
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mSipList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1817
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mSipList:Ljava/util/List;

    .line 1819
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mSipList:Ljava/util/List;

    new-instance v1, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$SipData;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$SipData;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1820
    return-void
.end method

.method public static buildFromResolver(Landroid/content/ContentResolver;)Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;
    .locals 1
    .param p0, "resolver"    # Landroid/content/ContentResolver;

    .prologue
    .line 2599
    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p0, v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->buildFromResolver(Landroid/content/ContentResolver;Landroid/net/Uri;)Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;

    move-result-object v0

    return-object v0
.end method

.method public static buildFromResolver(Landroid/content/ContentResolver;Landroid/net/Uri;)Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;
    .locals 1
    .param p0, "resolver"    # Landroid/content/ContentResolver;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 2603
    const/4 v0, 0x0

    return-object v0
.end method

.method private buildSinglePhoneticNameFromSortAsParam(Ljava/util/Map;)Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1861
    .local p1, "paramMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Collection<Ljava/lang/String;>;>;"
    const-string v4, "SORT-AS"

    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    .line 1862
    .local v2, "sortAsCollection":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    if-eqz v2, :cond_2

    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v4

    if-eqz v4, :cond_2

    .line 1863
    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_0

    .line 1864
    const-string v4, "vCard"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Incorrect multiple SORT_AS parameters detected: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 1866
    invoke-interface {v2}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v6

    invoke-static {v6}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1864
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1869
    :cond_0
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iget v5, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mVCardType:I

    .line 1868
    invoke-static {v4, v5}, Lcom/navdy/hud/app/bluetooth/vcard/VCardUtils;->constructListFromValue(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v3

    .line 1870
    .local v3, "sortNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1871
    .local v0, "builder":Ljava/lang/StringBuilder;
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1872
    .local v1, "elem":Ljava/lang/String;
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1874
    .end local v1    # "elem":Ljava/lang/String;
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1876
    .end local v0    # "builder":Ljava/lang/StringBuilder;
    .end local v3    # "sortNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_1
    return-object v4

    :cond_2
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private constructDisplayName()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2505
    const/4 v6, 0x0

    .line 2507
    .local v6, "displayName":Ljava/lang/String;
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    # getter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mFormatted:Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$1300(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2508
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    # getter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mFormatted:Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$1300(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;)Ljava/lang/String;

    move-result-object v6

    .line 2524
    :cond_0
    :goto_0
    if-nez v6, :cond_1

    .line 2525
    const-string v6, ""

    .line 2527
    :cond_1
    return-object v6

    .line 2509
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    invoke-virtual {v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->emptyStructuredName()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2510
    iget v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mVCardType:I

    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    # getter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mFamily:Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$1200(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    .line 2511
    # getter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mMiddle:Ljava/lang/String;
    invoke-static {v2}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$1000(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    # getter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mGiven:Ljava/lang/String;
    invoke-static {v3}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$1100(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    # getter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPrefix:Ljava/lang/String;
    invoke-static {v4}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$900(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    # getter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mSuffix:Ljava/lang/String;
    invoke-static {v5}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$800(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;)Ljava/lang/String;

    move-result-object v5

    .line 2510
    invoke-static/range {v0 .. v5}, Lcom/navdy/hud/app/bluetooth/vcard/VCardUtils;->constructNameFromElements(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    .line 2512
    :cond_3
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    invoke-virtual {v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->emptyPhoneticStructuredName()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2513
    iget v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mVCardType:I

    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    .line 2514
    # getter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticFamily:Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$500(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    # getter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticMiddle:Ljava/lang/String;
    invoke-static {v2}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$600(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    # getter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticGiven:Ljava/lang/String;
    invoke-static {v3}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$700(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;)Ljava/lang/String;

    move-result-object v3

    .line 2513
    invoke-static {v0, v1, v2, v3}, Lcom/navdy/hud/app/bluetooth/vcard/VCardUtils;->constructNameFromElements(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    .line 2515
    :cond_4
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mEmailList:Ljava/util/List;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mEmailList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_5

    .line 2516
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mEmailList:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EmailData;

    # getter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EmailData;->mAddress:Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EmailData;->access$1500(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EmailData;)Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    .line 2517
    :cond_5
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mPhoneList:Ljava/util/List;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mPhoneList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_6

    .line 2518
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mPhoneList:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhoneData;

    # getter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhoneData;->mNumber:Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhoneData;->access$1600(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhoneData;)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_0

    .line 2519
    :cond_6
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mPostalList:Ljava/util/List;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mPostalList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_7

    .line 2520
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mPostalList:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PostalData;

    iget v1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mVCardType:I

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PostalData;->getFormattedAddress(I)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_0

    .line 2521
    :cond_7
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mOrganizationList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mOrganizationList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 2522
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mOrganizationList:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$OrganizationData;

    invoke-virtual {v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$OrganizationData;->getFormattedString()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_0
.end method

.method private handleAndroidCustomProperty(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2494
    .local p1, "customPropertyList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mAndroidCustomDataList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 2495
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mAndroidCustomDataList:Ljava/util/List;

    .line 2497
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mAndroidCustomDataList:Ljava/util/List;

    .line 2498
    invoke-static {p1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$AndroidCustomData;->constructAndroidCustomData(Ljava/util/List;)Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$AndroidCustomData;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2499
    return-void
.end method

.method private handleNProperty(Ljava/util/List;Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .local p1, "paramValues":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "paramMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Collection<Ljava/lang/String;>;>;"
    const/4 v3, 0x1

    .line 2040
    invoke-direct {p0, p2}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->tryHandleSortAsName(Ljava/util/Map;)V

    .line 2044
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .local v0, "size":I
    if-ge v0, v3, :cond_1

    .line 2064
    .end local v0    # "size":I
    :cond_0
    :goto_0
    return-void

    .line 2047
    .restart local v0    # "size":I
    :cond_1
    const/4 v1, 0x5

    if-le v0, v1, :cond_2

    .line 2048
    const/4 v0, 0x5

    .line 2051
    :cond_2
    packed-switch v0, :pswitch_data_0

    .line 2062
    :goto_1
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    const/4 v1, 0x0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    # setter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mFamily:Ljava/lang/String;
    invoke-static {v2, v1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$1202(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 2054
    :pswitch_0
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    const/4 v1, 0x4

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    # setter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mSuffix:Ljava/lang/String;
    invoke-static {v2, v1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$802(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;Ljava/lang/String;)Ljava/lang/String;

    .line 2056
    :pswitch_1
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    const/4 v1, 0x3

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    # setter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPrefix:Ljava/lang/String;
    invoke-static {v2, v1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$902(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;Ljava/lang/String;)Ljava/lang/String;

    .line 2058
    :pswitch_2
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    const/4 v1, 0x2

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    # setter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mMiddle:Ljava/lang/String;
    invoke-static {v2, v1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$1002(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;Ljava/lang/String;)Ljava/lang/String;

    .line 2060
    :pswitch_3
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    # setter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mGiven:Ljava/lang/String;
    invoke-static {v2, v1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$1102(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1

    .line 2051
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private handleOrgValue(ILjava/util/List;Ljava/util/Map;Z)V
    .locals 11
    .param p1, "type"    # I
    .param p4, "isPrimary"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;>;Z)V"
        }
    .end annotation

    .prologue
    .local p2, "orgList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "paramMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Collection<Ljava/lang/String;>;>;"
    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 1889
    invoke-direct {p0, p3}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->buildSinglePhoneticNameFromSortAsParam(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v4

    .line 1890
    .local v4, "phoneticName":Ljava/lang/String;
    if-nez p2, :cond_0

    .line 1891
    sget-object p2, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->sEmptyList:Ljava/util/List;

    .line 1895
    :cond_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v10

    .line 1896
    .local v10, "size":I
    packed-switch v10, :pswitch_data_0

    .line 1908
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1911
    .local v1, "organizationName":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 1912
    .local v7, "builder":Ljava/lang/StringBuilder;
    const/4 v8, 0x1

    .local v8, "i":I
    :goto_0
    if-ge v8, v10, :cond_2

    .line 1913
    const/4 v0, 0x1

    if-le v8, v0, :cond_1

    .line 1914
    const/16 v0, 0x20

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1916
    :cond_1
    invoke-interface {p2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1912
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 1898
    .end local v1    # "organizationName":Ljava/lang/String;
    .end local v7    # "builder":Ljava/lang/StringBuilder;
    .end local v8    # "i":I
    :pswitch_0
    const-string v1, ""

    .line 1899
    .restart local v1    # "organizationName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 1921
    .local v2, "departmentName":Ljava/lang/String;
    :goto_1
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mOrganizationList:Ljava/util/List;

    if-nez v0, :cond_3

    move-object v0, p0

    move v5, p1

    move v6, p4

    .line 1924
    invoke-direct/range {v0 .. v6}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->addNewOrganization(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V

    .line 1944
    :goto_2
    return-void

    .line 1903
    .end local v1    # "organizationName":Ljava/lang/String;
    .end local v2    # "departmentName":Ljava/lang/String;
    :pswitch_1
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1904
    .restart local v1    # "organizationName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 1905
    .restart local v2    # "departmentName":Ljava/lang/String;
    goto :goto_1

    .line 1918
    .end local v2    # "departmentName":Ljava/lang/String;
    .restart local v7    # "builder":Ljava/lang/StringBuilder;
    .restart local v8    # "i":I
    :cond_2
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "departmentName":Ljava/lang/String;
    goto :goto_1

    .line 1928
    .end local v7    # "builder":Ljava/lang/StringBuilder;
    .end local v8    # "i":I
    :cond_3
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mOrganizationList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$OrganizationData;

    .line 1931
    .local v9, "organizationData":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$OrganizationData;
    # getter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$OrganizationData;->mOrganizationName:Ljava/lang/String;
    invoke-static {v9}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$OrganizationData;->access$100(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$OrganizationData;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_4

    .line 1932
    # getter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$OrganizationData;->mDepartmentName:Ljava/lang/String;
    invoke-static {v9}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$OrganizationData;->access$200(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$OrganizationData;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_4

    .line 1935
    # setter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$OrganizationData;->mOrganizationName:Ljava/lang/String;
    invoke-static {v9, v1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$OrganizationData;->access$102(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$OrganizationData;Ljava/lang/String;)Ljava/lang/String;

    .line 1936
    # setter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$OrganizationData;->mDepartmentName:Ljava/lang/String;
    invoke-static {v9, v2}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$OrganizationData;->access$202(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$OrganizationData;Ljava/lang/String;)Ljava/lang/String;

    .line 1937
    # setter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$OrganizationData;->mIsPrimary:Z
    invoke-static {v9, p4}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$OrganizationData;->access$302(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$OrganizationData;Z)Z

    goto :goto_2

    .end local v9    # "organizationData":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$OrganizationData;
    :cond_5
    move-object v0, p0

    move v5, p1

    move v6, p4

    .line 1943
    invoke-direct/range {v0 .. v6}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->addNewOrganization(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_2

    .line 1896
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private handlePhoneticNameFromSound(Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "elems":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2074
    iget-object v5, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    # getter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticFamily:Ljava/lang/String;
    invoke-static {v5}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$500(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    .line 2075
    # getter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticMiddle:Ljava/lang/String;
    invoke-static {v5}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$600(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    .line 2076
    # getter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticGiven:Ljava/lang/String;
    invoke-static {v5}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$700(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 2131
    :cond_0
    :goto_0
    return-void

    .line 2083
    :cond_1
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    .local v4, "size":I
    if-lt v4, v8, :cond_0

    .line 2090
    if-le v4, v10, :cond_2

    .line 2091
    const/4 v4, 0x3

    .line 2094
    :cond_2
    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_7

    .line 2095
    const/4 v3, 0x1

    .line 2096
    .local v3, "onlyFirstElemIsNonEmpty":Z
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_1
    if-ge v0, v4, :cond_3

    .line 2097
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_4

    .line 2098
    const/4 v3, 0x0

    .line 2102
    :cond_3
    if-eqz v3, :cond_7

    .line 2103
    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 2104
    .local v2, "namesArray":[Ljava/lang/String;
    array-length v1, v2

    .line 2105
    .local v1, "nameArrayLength":I
    if-ne v1, v10, :cond_5

    .line 2107
    iget-object v5, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    aget-object v6, v2, v7

    # setter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticFamily:Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$502(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;Ljava/lang/String;)Ljava/lang/String;

    .line 2108
    iget-object v5, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    aget-object v6, v2, v8

    # setter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticMiddle:Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$602(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;Ljava/lang/String;)Ljava/lang/String;

    .line 2109
    iget-object v5, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    aget-object v6, v2, v9

    # setter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticGiven:Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$702(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 2096
    .end local v1    # "nameArrayLength":I
    .end local v2    # "namesArray":[Ljava/lang/String;
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2110
    .restart local v1    # "nameArrayLength":I
    .restart local v2    # "namesArray":[Ljava/lang/String;
    :cond_5
    if-ne v1, v9, :cond_6

    .line 2113
    iget-object v5, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    aget-object v6, v2, v7

    # setter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticFamily:Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$502(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;Ljava/lang/String;)Ljava/lang/String;

    .line 2114
    iget-object v5, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    aget-object v6, v2, v8

    # setter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticGiven:Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$702(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 2116
    :cond_6
    iget-object v6, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    # setter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticGiven:Ljava/lang/String;
    invoke-static {v6, v5}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$702(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 2122
    .end local v0    # "i":I
    .end local v1    # "nameArrayLength":I
    .end local v2    # "namesArray":[Ljava/lang/String;
    .end local v3    # "onlyFirstElemIsNonEmpty":Z
    :cond_7
    packed-switch v4, :pswitch_data_0

    .line 2129
    :goto_2
    iget-object v6, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    # setter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticFamily:Ljava/lang/String;
    invoke-static {v6, v5}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$502(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 2125
    :pswitch_0
    iget-object v6, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    invoke-interface {p1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    # setter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticMiddle:Ljava/lang/String;
    invoke-static {v6, v5}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$602(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;Ljava/lang/String;)Ljava/lang/String;

    .line 2127
    :pswitch_1
    iget-object v6, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    invoke-interface {p1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    # setter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticGiven:Ljava/lang/String;
    invoke-static {v6, v5}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$702(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_2

    .line 2122
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private handleSipCase(Ljava/lang/String;Ljava/util/Collection;)V
    .locals 7
    .param p1, "propValue"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2448
    .local p2, "typeCollection":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2484
    :cond_0
    :goto_0
    return-void

    .line 2451
    :cond_1
    const-string v5, "sip:"

    invoke-virtual {p1, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2452
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 2453
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_0

    .line 2458
    :cond_2
    const/4 v2, -0x1

    .line 2459
    .local v2, "type":I
    const/4 v1, 0x0

    .line 2460
    .local v1, "label":Ljava/lang/String;
    const/4 v0, 0x0

    .line 2461
    .local v0, "isPrimary":Z
    if-eqz p2, :cond_8

    .line 2462
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2463
    .local v3, "typeStringOrg":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    .line 2464
    .local v4, "typeStringUpperCase":Ljava/lang/String;
    const-string v6, "PREF"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2465
    const/4 v0, 0x1

    goto :goto_1

    .line 2466
    :cond_4
    const-string v6, "HOME"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 2467
    const/4 v2, 0x1

    goto :goto_1

    .line 2468
    :cond_5
    const-string v6, "WORK"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 2469
    const/4 v2, 0x2

    goto :goto_1

    .line 2470
    :cond_6
    if-gez v2, :cond_3

    .line 2471
    const-string v6, "X-"

    invoke-virtual {v4, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 2472
    const/4 v6, 0x2

    invoke-virtual {v3, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 2476
    :goto_2
    const/4 v2, 0x0

    goto :goto_1

    .line 2474
    :cond_7
    move-object v1, v3

    goto :goto_2

    .line 2480
    .end local v3    # "typeStringOrg":Ljava/lang/String;
    .end local v4    # "typeStringUpperCase":Ljava/lang/String;
    :cond_8
    if-gez v2, :cond_9

    .line 2481
    const/4 v2, 0x3

    .line 2483
    :cond_9
    invoke-direct {p0, p1, v2, v1, v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->addSip(Ljava/lang/String;ILjava/lang/String;Z)V

    goto :goto_0
.end method

.method private handleTitleValue(Ljava/lang/String;)V
    .locals 8
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 1954
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mOrganizationList:Ljava/util/List;

    if-nez v0, :cond_0

    move-object v0, p0

    move-object v2, v1

    move-object v3, p1

    move-object v4, v1

    .line 1957
    invoke-direct/range {v0 .. v6}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->addNewOrganization(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V

    .line 1969
    :goto_0
    return-void

    .line 1960
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mOrganizationList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$OrganizationData;

    .line 1961
    .local v7, "organizationData":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$OrganizationData;
    # getter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$OrganizationData;->mTitle:Ljava/lang/String;
    invoke-static {v7}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$OrganizationData;->access$400(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$OrganizationData;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 1962
    # setter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$OrganizationData;->mTitle:Ljava/lang/String;
    invoke-static {v7, p1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$OrganizationData;->access$402(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$OrganizationData;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .end local v7    # "organizationData":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$OrganizationData;
    :cond_2
    move-object v0, p0

    move-object v2, v1

    move-object v3, p1

    move-object v4, v1

    .line 1968
    invoke-direct/range {v0 .. v6}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->addNewOrganization(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_0
.end method

.method private iterateOneList(Ljava/util/List;Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElementIterator;)V
    .locals 3
    .param p2, "iterator"    # Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElementIterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElement;",
            ">;",
            "Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElementIterator;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1621
    .local p1, "elemList":Ljava/util/List;, "Ljava/util/List<+Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElement;>;"
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 1622
    const/4 v1, 0x0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElement;

    invoke-interface {v1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElement;->getEntryLabel()Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryLabel;

    move-result-object v1

    invoke-interface {p2, v1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElementIterator;->onElementGroupStarted(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryLabel;)V

    .line 1623
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElement;

    .line 1624
    .local v0, "elem":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElement;
    invoke-interface {p2, v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElementIterator;->onElement(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElement;)Z

    goto :goto_0

    .line 1626
    .end local v0    # "elem":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElement;
    :cond_0
    invoke-interface {p2}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElementIterator;->onElementGroupEnded()V

    .line 1628
    :cond_1
    return-void
.end method

.method private listToString(Ljava/util/List;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v4, 0x1

    .line 2607
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    .line 2608
    .local v2, "size":I
    if-le v2, v4, :cond_2

    .line 2609
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2610
    .local v0, "builder":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .line 2611
    .local v1, "i":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2612
    .local v3, "type":Ljava/lang/String;
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613
    add-int/lit8 v5, v2, -0x1

    if-ge v1, v5, :cond_0

    .line 2614
    const-string v5, ";"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 2617
    .end local v3    # "type":Ljava/lang/String;
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2621
    .end local v0    # "builder":Ljava/lang/StringBuilder;
    .end local v1    # "i":I
    :goto_1
    return-object v4

    .line 2618
    :cond_2
    if-ne v2, v4, :cond_3

    .line 2619
    const/4 v4, 0x0

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    goto :goto_1

    .line 2621
    :cond_3
    const-string v4, ""

    goto :goto_1
.end method

.method private tryHandleSortAsName(Ljava/util/Map;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .local p1, "paramMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Collection<Ljava/lang/String;>;>;"
    const/4 v6, 0x1

    .line 2004
    iget v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mVCardType:I

    invoke-static {v3}, Lcom/navdy/hud/app/bluetooth/vcard/VCardConfig;->isVersion30(I)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    .line 2005
    # getter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticFamily:Ljava/lang/String;
    invoke-static {v3}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$500(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    .line 2006
    # getter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticMiddle:Ljava/lang/String;
    invoke-static {v3}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$600(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    .line 2007
    # getter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticGiven:Ljava/lang/String;
    invoke-static {v3}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$700(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2034
    :cond_0
    :goto_0
    return-void

    .line 2011
    :cond_1
    const-string v3, "SORT-AS"

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    .line 2012
    .local v1, "sortAsCollection":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v3

    if-eqz v3, :cond_0

    .line 2013
    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v3

    if-le v3, v6, :cond_2

    .line 2014
    const-string v3, "vCard"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Incorrect multiple SORT_AS parameters detected: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 2016
    invoke-interface {v1}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2014
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2019
    :cond_2
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget v4, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mVCardType:I

    .line 2018
    invoke-static {v3, v4}, Lcom/navdy/hud/app/bluetooth/vcard/VCardUtils;->constructListFromValue(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v2

    .line 2020
    .local v2, "sortNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 2021
    .local v0, "size":I
    const/4 v3, 0x3

    if-le v0, v3, :cond_3

    .line 2022
    const/4 v0, 0x3

    .line 2024
    :cond_3
    packed-switch v0, :pswitch_data_0

    .line 2030
    :goto_1
    iget-object v4, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    # setter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticFamily:Ljava/lang/String;
    invoke-static {v4, v3}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$502(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 2026
    :pswitch_0
    iget-object v4, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    const/4 v3, 0x2

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    # setter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticMiddle:Ljava/lang/String;
    invoke-static {v4, v3}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$602(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;Ljava/lang/String;)Ljava/lang/String;

    .line 2028
    :pswitch_1
    iget-object v4, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    # setter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticGiven:Ljava/lang/String;
    invoke-static {v4, v3}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$702(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1

    .line 2024
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public addChild(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;)V
    .locals 1
    .param p1, "child"    # Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;

    .prologue
    .line 2487
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mChildren:Ljava/util/List;

    if-nez v0, :cond_0

    .line 2488
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mChildren:Ljava/util/List;

    .line 2490
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mChildren:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2491
    return-void
.end method

.method public addProperty(Lcom/navdy/hud/app/bluetooth/vcard/VCardProperty;)V
    .locals 33
    .param p1, "property"    # Lcom/navdy/hud/app/bluetooth/vcard/VCardProperty;

    .prologue
    .line 2134
    invoke-virtual/range {p1 .. p1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardProperty;->getName()Ljava/lang/String;

    move-result-object v22

    .line 2135
    .local v22, "propertyName":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardProperty;->getParameterMap()Ljava/util/Map;

    move-result-object v17

    .line 2136
    .local v17, "paramMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Collection<Ljava/lang/String;>;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardProperty;->getValueList()Ljava/util/List;

    move-result-object v23

    .line 2137
    .local v23, "propertyValueList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardProperty;->getByteValue()[B

    move-result-object v21

    .line 2139
    .local v21, "propertyBytes":[B
    if-eqz v23, :cond_0

    invoke-interface/range {v23 .. v23}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_2

    :cond_0
    if-nez v21, :cond_2

    .line 2441
    :cond_1
    :goto_0
    return-void

    .line 2143
    :cond_2
    if-eqz v23, :cond_3

    .line 2144
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->listToString(Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 2147
    .local v6, "propValue":Ljava/lang/String;
    :goto_1
    const-string v3, "VERSION"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2149
    const-string v3, "FN"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2150
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    # setter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mFormatted:Ljava/lang/String;
    invoke-static {v3, v6}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$1302(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 2144
    .end local v6    # "propValue":Ljava/lang/String;
    :cond_3
    const/4 v6, 0x0

    goto :goto_1

    .line 2151
    .restart local v6    # "propValue":Ljava/lang/String;
    :cond_4
    const-string v3, "NAME"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2154
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    # getter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mFormatted:Ljava/lang/String;
    invoke-static {v3}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$1300(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2155
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    # setter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mFormatted:Ljava/lang/String;
    invoke-static {v3, v6}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$1302(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 2157
    :cond_5
    const-string v3, "N"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 2158
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->handleNProperty(Ljava/util/List;Ljava/util/Map;)V

    goto :goto_0

    .line 2159
    :cond_6
    const-string v3, "SORT-STRING"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 2160
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    # setter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mSortString:Ljava/lang/String;
    invoke-static {v3, v6}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$1402(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 2161
    :cond_7
    const-string v3, "NICKNAME"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    const-string v3, "X-NICKNAME"

    .line 2162
    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 2163
    :cond_8
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->addNickName(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2164
    :cond_9
    const-string v3, "SOUND"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 2165
    const-string v3, "TYPE"

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/util/Collection;

    .line 2166
    .local v25, "typeCollection":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    if-eqz v25, :cond_1

    const-string v3, "X-IRMC-N"

    .line 2167
    move-object/from16 v0, v25

    invoke-interface {v0, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2172
    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mVCardType:I

    invoke-static {v6, v3}, Lcom/navdy/hud/app/bluetooth/vcard/VCardUtils;->constructListFromValue(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v20

    .line 2174
    .local v20, "phoneticNameList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->handlePhoneticNameFromSound(Ljava/util/List;)V

    goto/16 :goto_0

    .line 2178
    .end local v20    # "phoneticNameList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v25    # "typeCollection":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    :cond_a
    const-string v3, "ADR"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_15

    .line 2179
    const/16 v32, 0x1

    .line 2180
    .local v32, "valuesAreAllEmpty":Z
    invoke-interface/range {v23 .. v23}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Ljava/lang/String;

    .line 2181
    .local v31, "value":Ljava/lang/String;
    invoke-static/range {v31 .. v31}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_b

    .line 2182
    const/16 v32, 0x0

    .line 2186
    .end local v31    # "value":Ljava/lang/String;
    :cond_c
    if-nez v32, :cond_1

    .line 2190
    const/4 v7, -0x1

    .line 2191
    .local v7, "type":I
    const/4 v15, 0x0

    .line 2192
    .local v15, "label":Ljava/lang/String;
    const/4 v8, 0x0

    .line 2193
    .local v8, "isPrimary":Z
    const-string v3, "TYPE"

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/util/Collection;

    .line 2194
    .restart local v25    # "typeCollection":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    if-eqz v25, :cond_13

    .line 2195
    invoke-interface/range {v25 .. v25}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_d
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_13

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Ljava/lang/String;

    .line 2196
    .local v28, "typeStringOrg":Ljava/lang/String;
    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v29

    .line 2197
    .local v29, "typeStringUpperCase":Ljava/lang/String;
    const-string v5, "PREF"

    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 2198
    const/4 v8, 0x1

    goto :goto_2

    .line 2199
    :cond_e
    const-string v5, "HOME"

    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 2200
    const/4 v7, 0x1

    .line 2201
    const/4 v15, 0x0

    goto :goto_2

    .line 2202
    :cond_f
    const-string v5, "WORK"

    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_10

    const-string v5, "COMPANY"

    .line 2204
    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_11

    .line 2208
    :cond_10
    const/4 v7, 0x2

    .line 2209
    const/4 v15, 0x0

    goto :goto_2

    .line 2210
    :cond_11
    const-string v5, "PARCEL"

    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_d

    const-string v5, "DOM"

    .line 2211
    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_d

    const-string v5, "INTL"

    .line 2212
    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_d

    .line 2214
    if-gez v7, :cond_d

    .line 2215
    const/4 v7, 0x0

    .line 2216
    const-string v5, "X-"

    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_12

    .line 2217
    const/4 v5, 0x2

    move-object/from16 v0, v28

    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v15

    goto :goto_2

    .line 2219
    :cond_12
    move-object/from16 v15, v28

    goto :goto_2

    .line 2225
    .end local v28    # "typeStringOrg":Ljava/lang/String;
    .end local v29    # "typeStringUpperCase":Ljava/lang/String;
    :cond_13
    if-gez v7, :cond_14

    .line 2226
    const/4 v7, 0x1

    .line 2229
    :cond_14
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v7, v1, v15, v8}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->addPostal(ILjava/util/List;Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 2230
    .end local v7    # "type":I
    .end local v8    # "isPrimary":Z
    .end local v15    # "label":Ljava/lang/String;
    .end local v25    # "typeCollection":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    .end local v32    # "valuesAreAllEmpty":Z
    :cond_15
    const-string v3, "EMAIL"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1e

    .line 2231
    const/4 v7, -0x1

    .line 2232
    .restart local v7    # "type":I
    const/4 v15, 0x0

    .line 2233
    .restart local v15    # "label":Ljava/lang/String;
    const/4 v8, 0x0

    .line 2234
    .restart local v8    # "isPrimary":Z
    const-string v3, "TYPE"

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/util/Collection;

    .line 2235
    .restart local v25    # "typeCollection":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    if-eqz v25, :cond_1c

    .line 2236
    invoke-interface/range {v25 .. v25}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_16
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Ljava/lang/String;

    .line 2237
    .restart local v28    # "typeStringOrg":Ljava/lang/String;
    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v29

    .line 2238
    .restart local v29    # "typeStringUpperCase":Ljava/lang/String;
    const-string v5, "PREF"

    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_17

    .line 2239
    const/4 v8, 0x1

    goto :goto_3

    .line 2240
    :cond_17
    const-string v5, "HOME"

    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_18

    .line 2241
    const/4 v7, 0x1

    goto :goto_3

    .line 2242
    :cond_18
    const-string v5, "WORK"

    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_19

    .line 2243
    const/4 v7, 0x2

    goto :goto_3

    .line 2244
    :cond_19
    const-string v5, "CELL"

    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1a

    .line 2245
    const/4 v7, 0x4

    goto :goto_3

    .line 2246
    :cond_1a
    if-gez v7, :cond_16

    .line 2247
    const-string v5, "X-"

    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1b

    .line 2248
    const/4 v5, 0x2

    move-object/from16 v0, v28

    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v15

    .line 2252
    :goto_4
    const/4 v7, 0x0

    goto :goto_3

    .line 2250
    :cond_1b
    move-object/from16 v15, v28

    goto :goto_4

    .line 2256
    .end local v28    # "typeStringOrg":Ljava/lang/String;
    .end local v29    # "typeStringUpperCase":Ljava/lang/String;
    :cond_1c
    if-gez v7, :cond_1d

    .line 2257
    const/4 v7, 0x3

    .line 2259
    :cond_1d
    move-object/from16 v0, p0

    invoke-direct {v0, v7, v6, v15, v8}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->addEmail(ILjava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 2260
    .end local v7    # "type":I
    .end local v8    # "isPrimary":Z
    .end local v15    # "label":Ljava/lang/String;
    .end local v25    # "typeCollection":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    :cond_1e
    const-string v3, "ORG"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_21

    .line 2262
    const/4 v7, 0x1

    .line 2263
    .restart local v7    # "type":I
    const/4 v8, 0x0

    .line 2264
    .restart local v8    # "isPrimary":Z
    const-string v3, "TYPE"

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/util/Collection;

    .line 2265
    .restart local v25    # "typeCollection":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    if-eqz v25, :cond_20

    .line 2266
    invoke-interface/range {v25 .. v25}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1f
    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_20

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/String;

    .line 2267
    .local v27, "typeString":Ljava/lang/String;
    const-string v5, "PREF"

    move-object/from16 v0, v27

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1f

    .line 2268
    const/4 v8, 0x1

    goto :goto_5

    .line 2272
    .end local v27    # "typeString":Ljava/lang/String;
    :cond_20
    const/4 v3, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, v17

    invoke-direct {v0, v3, v1, v2, v8}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->handleOrgValue(ILjava/util/List;Ljava/util/Map;Z)V

    goto/16 :goto_0

    .line 2273
    .end local v7    # "type":I
    .end local v8    # "isPrimary":Z
    .end local v25    # "typeCollection":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    :cond_21
    const-string v3, "TITLE"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_22

    .line 2274
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->handleTitleValue(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2275
    :cond_22
    const-string v3, "ROLE"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2278
    const-string v3, "PHOTO"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_23

    const-string v3, "LOGO"

    .line 2279
    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_28

    .line 2280
    :cond_23
    const-string v3, "VALUE"

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/util/Collection;

    .line 2281
    .local v18, "paramMapValue":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    if-eqz v18, :cond_24

    const-string v3, "URL"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2284
    :cond_24
    const-string v3, "TYPE"

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/util/Collection;

    .line 2285
    .restart local v25    # "typeCollection":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    const/4 v13, 0x0

    .line 2286
    .local v13, "formatName":Ljava/lang/String;
    const/4 v8, 0x0

    .line 2287
    .restart local v8    # "isPrimary":Z
    if-eqz v25, :cond_27

    .line 2288
    invoke-interface/range {v25 .. v25}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_25
    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_27

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Ljava/lang/String;

    .line 2289
    .local v30, "typeValue":Ljava/lang/String;
    const-string v5, "PREF"

    move-object/from16 v0, v30

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_26

    .line 2290
    const/4 v8, 0x1

    goto :goto_6

    .line 2291
    :cond_26
    if-nez v13, :cond_25

    .line 2292
    move-object/from16 v13, v30

    goto :goto_6

    .line 2296
    .end local v30    # "typeValue":Ljava/lang/String;
    :cond_27
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v13, v1, v8}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->addPhotoBytes(Ljava/lang/String;[BZ)V

    goto/16 :goto_0

    .line 2298
    .end local v8    # "isPrimary":Z
    .end local v13    # "formatName":Ljava/lang/String;
    .end local v18    # "paramMapValue":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    .end local v25    # "typeCollection":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    :cond_28
    const-string v3, "TEL"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2f

    .line 2299
    const/16 v19, 0x0

    .line 2300
    .local v19, "phoneNumber":Ljava/lang/String;
    const/4 v14, 0x0

    .line 2301
    .local v14, "isSip":Z
    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mVCardType:I

    invoke-static {v3}, Lcom/navdy/hud/app/bluetooth/vcard/VCardConfig;->isVersion40(I)Z

    move-result v3

    if-eqz v3, :cond_2b

    .line 2304
    const-string v3, "sip:"

    invoke-virtual {v6, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_29

    .line 2305
    const/4 v14, 0x1

    .line 2318
    :goto_7
    if-eqz v14, :cond_2c

    .line 2319
    const-string v3, "TYPE"

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/util/Collection;

    .line 2320
    .restart local v25    # "typeCollection":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-direct {v0, v6, v1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->handleSipCase(Ljava/lang/String;Ljava/util/Collection;)V

    goto/16 :goto_0

    .line 2306
    .end local v25    # "typeCollection":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    :cond_29
    const-string v3, "tel:"

    invoke-virtual {v6, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2a

    .line 2307
    const/4 v3, 0x4

    invoke-virtual {v6, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v19

    goto :goto_7

    .line 2312
    :cond_2a
    move-object/from16 v19, v6

    goto :goto_7

    .line 2315
    :cond_2b
    move-object/from16 v19, v6

    goto :goto_7

    .line 2322
    :cond_2c
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    .line 2326
    const-string v3, "TYPE"

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/util/Collection;

    .line 2327
    .restart local v25    # "typeCollection":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardUtils;->getPhoneTypeFromStrings(Ljava/util/Collection;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v26

    .line 2331
    .local v26, "typeObject":Ljava/lang/Object;
    move-object/from16 v0, v26

    instance-of v3, v0, Ljava/lang/Integer;

    if-eqz v3, :cond_2d

    .line 2332
    check-cast v26, Ljava/lang/Integer;

    .end local v26    # "typeObject":Ljava/lang/Object;
    invoke-virtual/range {v26 .. v26}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 2333
    .restart local v7    # "type":I
    const/4 v15, 0x0

    .line 2340
    .restart local v15    # "label":Ljava/lang/String;
    :goto_8
    if-eqz v25, :cond_2e

    const-string v3, "PREF"

    .line 2341
    move-object/from16 v0, v25

    invoke-interface {v0, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2e

    .line 2342
    const/4 v8, 0x1

    .line 2347
    .restart local v8    # "isPrimary":Z
    :goto_9
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v7, v1, v15, v8}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->addPhone(ILjava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 2335
    .end local v7    # "type":I
    .end local v8    # "isPrimary":Z
    .end local v15    # "label":Ljava/lang/String;
    .restart local v26    # "typeObject":Ljava/lang/Object;
    :cond_2d
    const/4 v7, 0x0

    .line 2336
    .restart local v7    # "type":I
    invoke-virtual/range {v26 .. v26}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v15

    .restart local v15    # "label":Ljava/lang/String;
    goto :goto_8

    .line 2344
    .end local v26    # "typeObject":Ljava/lang/Object;
    :cond_2e
    const/4 v8, 0x0

    .restart local v8    # "isPrimary":Z
    goto :goto_9

    .line 2349
    .end local v7    # "type":I
    .end local v8    # "isPrimary":Z
    .end local v14    # "isSip":Z
    .end local v15    # "label":Ljava/lang/String;
    .end local v19    # "phoneNumber":Ljava/lang/String;
    .end local v25    # "typeCollection":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    :cond_2f
    const-string v3, "X-IRMC-CALL-DATETIME"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_34

    .line 2350
    move-object v12, v6

    .line 2351
    .local v12, "dateTimeStr":Ljava/lang/String;
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2352
    const-string v3, "TYPE"

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/util/Collection;

    .line 2353
    .restart local v25    # "typeCollection":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    const/4 v9, 0x0

    .line 2354
    .local v9, "callType":I
    if-eqz v25, :cond_30

    invoke-interface/range {v25 .. v25}, Ljava/util/Collection;->size()I

    move-result v3

    if-lez v3, :cond_31

    .line 2355
    :cond_30
    invoke-interface/range {v25 .. v25}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    .line 2356
    .local v16, "o":Ljava/lang/Object;
    move-object/from16 v0, v16

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_31

    move-object/from16 v24, v16

    .line 2357
    check-cast v24, Ljava/lang/String;

    .line 2358
    .local v24, "str":Ljava/lang/String;
    const-string v3, "DIALED"

    move-object/from16 v0, v24

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_32

    .line 2359
    const/16 v9, 0xb

    .line 2367
    .end local v16    # "o":Ljava/lang/Object;
    .end local v24    # "str":Ljava/lang/String;
    :cond_31
    :goto_a
    invoke-static {v12}, Lcom/navdy/hud/app/util/DateUtil;->parseIrmcDateStr(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v11

    .line 2368
    .local v11, "date":Ljava/util/Date;
    if-eqz v11, :cond_1

    .line 2369
    move-object/from16 v0, p0

    invoke-direct {v0, v9, v11}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->addCallTime(ILjava/util/Date;)V

    goto/16 :goto_0

    .line 2360
    .end local v11    # "date":Ljava/util/Date;
    .restart local v16    # "o":Ljava/lang/Object;
    .restart local v24    # "str":Ljava/lang/String;
    :cond_32
    const-string v3, "MISSED"

    move-object/from16 v0, v24

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_33

    .line 2361
    const/16 v9, 0xc

    goto :goto_a

    .line 2362
    :cond_33
    const-string v3, "RECEIVED"

    move-object/from16 v0, v24

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_31

    .line 2363
    const/16 v9, 0xa

    goto :goto_a

    .line 2372
    .end local v9    # "callType":I
    .end local v12    # "dateTimeStr":Ljava/lang/String;
    .end local v16    # "o":Ljava/lang/Object;
    .end local v24    # "str":Ljava/lang/String;
    .end local v25    # "typeCollection":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    :cond_34
    const-string v3, "X-SKYPE-PSTNNUMBER"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_36

    .line 2374
    const-string v3, "TYPE"

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/util/Collection;

    .line 2375
    .restart local v25    # "typeCollection":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    const/4 v7, 0x7

    .line 2377
    .restart local v7    # "type":I
    if-eqz v25, :cond_35

    const-string v3, "PREF"

    .line 2378
    move-object/from16 v0, v25

    invoke-interface {v0, v3}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_35

    .line 2379
    const/4 v8, 0x1

    .line 2383
    .restart local v8    # "isPrimary":Z
    :goto_b
    const/4 v3, 0x7

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v6, v5, v8}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->addPhone(ILjava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 2381
    .end local v8    # "isPrimary":Z
    :cond_35
    const/4 v8, 0x0

    .restart local v8    # "isPrimary":Z
    goto :goto_b

    .line 2384
    .end local v7    # "type":I
    .end local v8    # "isPrimary":Z
    .end local v25    # "typeCollection":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    :cond_36
    sget-object v3, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->sImMap:Ljava/util/Map;

    move-object/from16 v0, v22

    invoke-interface {v3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3c

    .line 2385
    sget-object v3, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->sImMap:Ljava/util/Map;

    move-object/from16 v0, v22

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 2386
    .local v4, "protocol":I
    const/4 v8, 0x0

    .line 2387
    .restart local v8    # "isPrimary":Z
    const/4 v7, -0x1

    .line 2388
    .restart local v7    # "type":I
    const-string v3, "TYPE"

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/util/Collection;

    .line 2389
    .restart local v25    # "typeCollection":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    if-eqz v25, :cond_3a

    .line 2390
    invoke-interface/range {v25 .. v25}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_37
    :goto_c
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/String;

    .line 2391
    .restart local v27    # "typeString":Ljava/lang/String;
    const-string v5, "PREF"

    move-object/from16 v0, v27

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_38

    .line 2392
    const/4 v8, 0x1

    goto :goto_c

    .line 2393
    :cond_38
    if-gez v7, :cond_37

    .line 2394
    const-string v5, "HOME"

    move-object/from16 v0, v27

    invoke-virtual {v0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_39

    .line 2395
    const/4 v7, 0x1

    goto :goto_c

    .line 2396
    :cond_39
    const-string v5, "WORK"

    move-object/from16 v0, v27

    invoke-virtual {v0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_37

    .line 2397
    const/4 v7, 0x2

    goto :goto_c

    .line 2402
    .end local v27    # "typeString":Ljava/lang/String;
    :cond_3a
    if-gez v7, :cond_3b

    .line 2403
    const/4 v7, 0x1

    .line 2405
    :cond_3b
    const/4 v5, 0x0

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v8}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->addIm(ILjava/lang/String;Ljava/lang/String;IZ)V

    goto/16 :goto_0

    .line 2406
    .end local v4    # "protocol":I
    .end local v7    # "type":I
    .end local v8    # "isPrimary":Z
    .end local v25    # "typeCollection":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    :cond_3c
    const-string v3, "NOTE"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3d

    .line 2407
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->addNote(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2408
    :cond_3d
    const-string v3, "URL"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3f

    .line 2409
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mWebsiteList:Ljava/util/List;

    if-nez v3, :cond_3e

    .line 2410
    new-instance v3, Ljava/util/ArrayList;

    const/4 v5, 0x1

    invoke-direct {v3, v5}, Ljava/util/ArrayList;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mWebsiteList:Ljava/util/List;

    .line 2412
    :cond_3e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mWebsiteList:Ljava/util/List;

    new-instance v5, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$WebsiteData;

    invoke-direct {v5, v6}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$WebsiteData;-><init>(Ljava/lang/String;)V

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2413
    :cond_3f
    const-string v3, "BDAY"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_40

    .line 2414
    new-instance v3, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$BirthdayData;

    invoke-direct {v3, v6}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$BirthdayData;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mBirthday:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$BirthdayData;

    goto/16 :goto_0

    .line 2415
    :cond_40
    const-string v3, "ANNIVERSARY"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_41

    .line 2416
    new-instance v3, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$AnniversaryData;

    invoke-direct {v3, v6}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$AnniversaryData;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mAnniversary:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$AnniversaryData;

    goto/16 :goto_0

    .line 2417
    :cond_41
    const-string v3, "X-PHONETIC-FIRST-NAME"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_42

    .line 2418
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    # setter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticGiven:Ljava/lang/String;
    invoke-static {v3, v6}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$702(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0

    .line 2419
    :cond_42
    const-string v3, "X-PHONETIC-MIDDLE-NAME"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_43

    .line 2420
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    # setter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticMiddle:Ljava/lang/String;
    invoke-static {v3, v6}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$602(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0

    .line 2421
    :cond_43
    const-string v3, "X-PHONETIC-LAST-NAME"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_44

    .line 2422
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    # setter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->mPhoneticFamily:Ljava/lang/String;
    invoke-static {v3, v6}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->access$502(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0

    .line 2423
    :cond_44
    const-string v3, "IMPP"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_45

    .line 2425
    const-string v3, "sip:"

    invoke-virtual {v6, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2426
    const-string v3, "TYPE"

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/util/Collection;

    .line 2427
    .restart local v25    # "typeCollection":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-direct {v0, v6, v1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->handleSipCase(Ljava/lang/String;Ljava/util/Collection;)V

    goto/16 :goto_0

    .line 2429
    .end local v25    # "typeCollection":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    :cond_45
    const-string v3, "X-SIP"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_46

    .line 2430
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2431
    const-string v3, "TYPE"

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/util/Collection;

    .line 2432
    .restart local v25    # "typeCollection":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-direct {v0, v6, v1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->handleSipCase(Ljava/lang/String;Ljava/util/Collection;)V

    goto/16 :goto_0

    .line 2434
    .end local v25    # "typeCollection":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    :cond_46
    const-string v3, "X-ANDROID-CUSTOM"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2435
    move-object/from16 v0, p0

    iget v3, v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mVCardType:I

    invoke-static {v6, v3}, Lcom/navdy/hud/app/bluetooth/vcard/VCardUtils;->constructListFromValue(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v10

    .line 2437
    .local v10, "customPropertyList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->handleAndroidCustomProperty(Ljava/util/List;)V

    goto/16 :goto_0
.end method

.method public consolidateFields()V
    .locals 2

    .prologue
    .line 2534
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->constructDisplayName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->displayName:Ljava/lang/String;

    .line 2535
    return-void
.end method

.method public constructInsertOperations(Landroid/content/ContentResolver;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 6
    .param p1, "resolver"    # Landroid/content/ContentResolver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;"
        }
    .end annotation

    .prologue
    .local p2, "operationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    const/4 v5, 0x0

    .line 2568
    if-nez p2, :cond_0

    .line 2569
    new-instance p2, Ljava/util/ArrayList;

    .end local p2    # "operationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    .line 2572
    .restart local p2    # "operationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->isIgnorable()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2595
    :goto_0
    return-object p2

    .line 2576
    :cond_1
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 2580
    .local v0, "backReferenceIndex":I
    sget-object v4, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    .line 2581
    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    .line 2582
    .local v1, "builder":Landroid/content/ContentProviderOperation$Builder;
    iget-object v4, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mAccount:Landroid/accounts/Account;

    if-eqz v4, :cond_2

    .line 2583
    const-string v4, "account_name"

    iget-object v5, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mAccount:Landroid/accounts/Account;

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 2584
    const-string v4, "account_type"

    iget-object v5, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mAccount:Landroid/accounts/Account;

    iget-object v5, v5, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 2589
    :goto_1
    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2591
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 2592
    .local v3, "start":I
    new-instance v4, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$InsertOperationConstrutor;

    invoke-direct {v4, p0, p2, v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$InsertOperationConstrutor;-><init>(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;Ljava/util/List;I)V

    invoke-virtual {p0, v4}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->iterateAllData(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElementIterator;)V

    .line 2593
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 2595
    .local v2, "end":I
    goto :goto_0

    .line 2586
    .end local v2    # "end":I
    .end local v3    # "start":I
    :cond_2
    const-string v4, "account_name"

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 2587
    const-string v4, "account_type"

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    goto :goto_1
.end method

.method public final getBirthday()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2634
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mBirthday:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$BirthdayData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mBirthday:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$BirthdayData;

    # getter for: Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$BirthdayData;->mBirthday:Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$BirthdayData;->access$1800(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$BirthdayData;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCallTime()Ljava/util/Date;
    .locals 1

    .prologue
    .line 2688
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mCallTime:Ljava/util/Date;

    return-object v0
.end method

.method public getCallType()I
    .locals 1

    .prologue
    .line 2684
    iget v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mCallType:I

    return v0
.end method

.method public final getChildlen()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2673
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mChildren:Ljava/util/List;

    return-object v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2677
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->displayName:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2678
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->constructDisplayName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->displayName:Ljava/lang/String;

    .line 2680
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public final getEmailList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EmailData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2646
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mEmailList:Ljava/util/List;

    return-object v0
.end method

.method public final getImList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$ImData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2658
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mImList:Ljava/util/List;

    return-object v0
.end method

.method public final getNameData()Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;
    .locals 1

    .prologue
    .line 2626
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    return-object v0
.end method

.method public final getNickNameList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NicknameData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2630
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNicknameList:Ljava/util/List;

    return-object v0
.end method

.method public final getNotes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NoteData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2638
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNoteList:Ljava/util/List;

    return-object v0
.end method

.method public final getOrganizationList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$OrganizationData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2654
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mOrganizationList:Ljava/util/List;

    return-object v0
.end method

.method public final getPhoneList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhoneData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2642
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mPhoneList:Ljava/util/List;

    return-object v0
.end method

.method public final getPhotoList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhotoData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2662
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mPhotoList:Ljava/util/List;

    return-object v0
.end method

.method public final getPostalList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PostalData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2650
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mPostalList:Ljava/util/List;

    return-object v0
.end method

.method public final getWebsiteList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$WebsiteData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2666
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mWebsiteList:Ljava/util/List;

    return-object v0
.end method

.method public isIgnorable()Z
    .locals 2

    .prologue
    .line 2546
    new-instance v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$IsIgnorableIterator;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$IsIgnorableIterator;-><init>(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$1;)V

    .line 2547
    .local v0, "iterator":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$IsIgnorableIterator;
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->iterateAllData(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElementIterator;)V

    .line 2548
    invoke-virtual {v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$IsIgnorableIterator;->getResult()Z

    move-result v1

    return v1
.end method

.method public final iterateAllData(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElementIterator;)V
    .locals 1
    .param p1, "iterator"    # Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElementIterator;

    .prologue
    .line 1589
    invoke-interface {p1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElementIterator;->onIterationStarted()V

    .line 1590
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    invoke-virtual {v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->getEntryLabel()Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryLabel;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElementIterator;->onElementGroupStarted(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryLabel;)V

    .line 1591
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNameData:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    invoke-interface {p1, v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElementIterator;->onElement(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElement;)Z

    .line 1592
    invoke-interface {p1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElementIterator;->onElementGroupEnded()V

    .line 1594
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mPhoneList:Ljava/util/List;

    invoke-direct {p0, v0, p1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->iterateOneList(Ljava/util/List;Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElementIterator;)V

    .line 1595
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mEmailList:Ljava/util/List;

    invoke-direct {p0, v0, p1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->iterateOneList(Ljava/util/List;Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElementIterator;)V

    .line 1596
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mPostalList:Ljava/util/List;

    invoke-direct {p0, v0, p1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->iterateOneList(Ljava/util/List;Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElementIterator;)V

    .line 1597
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mOrganizationList:Ljava/util/List;

    invoke-direct {p0, v0, p1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->iterateOneList(Ljava/util/List;Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElementIterator;)V

    .line 1598
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mImList:Ljava/util/List;

    invoke-direct {p0, v0, p1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->iterateOneList(Ljava/util/List;Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElementIterator;)V

    .line 1599
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mPhotoList:Ljava/util/List;

    invoke-direct {p0, v0, p1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->iterateOneList(Ljava/util/List;Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElementIterator;)V

    .line 1600
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mWebsiteList:Ljava/util/List;

    invoke-direct {p0, v0, p1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->iterateOneList(Ljava/util/List;Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElementIterator;)V

    .line 1601
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mSipList:Ljava/util/List;

    invoke-direct {p0, v0, p1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->iterateOneList(Ljava/util/List;Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElementIterator;)V

    .line 1602
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNicknameList:Ljava/util/List;

    invoke-direct {p0, v0, p1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->iterateOneList(Ljava/util/List;Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElementIterator;)V

    .line 1603
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mNoteList:Ljava/util/List;

    invoke-direct {p0, v0, p1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->iterateOneList(Ljava/util/List;Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElementIterator;)V

    .line 1604
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mAndroidCustomDataList:Ljava/util/List;

    invoke-direct {p0, v0, p1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->iterateOneList(Ljava/util/List;Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElementIterator;)V

    .line 1606
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mBirthday:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$BirthdayData;

    if-eqz v0, :cond_0

    .line 1607
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mBirthday:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$BirthdayData;

    invoke-virtual {v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$BirthdayData;->getEntryLabel()Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryLabel;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElementIterator;->onElementGroupStarted(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryLabel;)V

    .line 1608
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mBirthday:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$BirthdayData;

    invoke-interface {p1, v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElementIterator;->onElement(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElement;)Z

    .line 1609
    invoke-interface {p1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElementIterator;->onElementGroupEnded()V

    .line 1611
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mAnniversary:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$AnniversaryData;

    if-eqz v0, :cond_1

    .line 1612
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mAnniversary:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$AnniversaryData;

    invoke-virtual {v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$AnniversaryData;->getEntryLabel()Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryLabel;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElementIterator;->onElementGroupStarted(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryLabel;)V

    .line 1613
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->mAnniversary:Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$AnniversaryData;

    invoke-interface {p1, v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElementIterator;->onElement(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElement;)Z

    .line 1614
    invoke-interface {p1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElementIterator;->onElementGroupEnded()V

    .line 1616
    :cond_1
    invoke-interface {p1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElementIterator;->onIterationEnded()V

    .line 1617
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1751
    new-instance v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$ToStringIterator;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$ToStringIterator;-><init>(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$1;)V

    .line 1752
    .local v0, "iterator":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$ToStringIterator;
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->iterateAllData(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EntryElementIterator;)V

    .line 1753
    invoke-virtual {v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$ToStringIterator;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
