.class Lcom/navdy/hud/app/bluetooth/vcard/VCardBuilder$PostalStruct;
.super Ljava/lang/Object;
.source "VCardBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/bluetooth/vcard/VCardBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PostalStruct"
.end annotation


# instance fields
.field final addressData:Ljava/lang/String;

.field final appendCharset:Z

.field final reallyUseQuotedPrintable:Z


# direct methods
.method public constructor <init>(ZZLjava/lang/String;)V
    .locals 0
    .param p1, "reallyUseQuotedPrintable"    # Z
    .param p2, "appendCharset"    # Z
    .param p3, "addressData"    # Ljava/lang/String;

    .prologue
    .line 1049
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1050
    iput-boolean p1, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardBuilder$PostalStruct;->reallyUseQuotedPrintable:Z

    .line 1051
    iput-boolean p2, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardBuilder$PostalStruct;->appendCharset:Z

    .line 1052
    iput-object p3, p0, Lcom/navdy/hud/app/bluetooth/vcard/VCardBuilder$PostalStruct;->addressData:Ljava/lang/String;

    .line 1053
    return-void
.end method
