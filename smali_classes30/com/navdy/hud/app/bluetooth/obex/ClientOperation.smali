.class public final Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;
.super Ljava/lang/Object;
.source "ClientOperation.java"

# interfaces
.implements Lcom/navdy/hud/app/bluetooth/obex/Operation;
.implements Lcom/navdy/hud/app/bluetooth/obex/BaseStream;


# static fields
.field private static final TAG:Ljava/lang/String; = "ClientOperation"

.field private static final V:Z


# instance fields
.field private mEndOfBodySent:Z

.field private mExceptionMessage:Ljava/lang/String;

.field private mGetFinalFlag:Z

.field private mGetOperation:Z

.field private mInputOpen:Z

.field private mMaxPacketSize:I

.field private mOperationDone:Z

.field private mParent:Lcom/navdy/hud/app/bluetooth/obex/ClientSession;

.field private mPrivateInput:Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;

.field private mPrivateInputOpen:Z

.field private mPrivateOutput:Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;

.field private mPrivateOutputOpen:Z

.field private mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

.field private mRequestHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

.field private mSendBodyHeader:Z

.field private mSrmActive:Z

.field private mSrmEnabled:Z

.field private mSrmWaitingForRemote:Z


# direct methods
.method public constructor <init>(ILcom/navdy/hud/app/bluetooth/obex/ClientSession;Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;Z)V
    .locals 7
    .param p1, "maxSize"    # I
    .param p2, "p"    # Lcom/navdy/hud/app/bluetooth/obex/ClientSession;
    .param p3, "header"    # Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    .param p4, "type"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x4

    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iput-boolean v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mSendBodyHeader:Z

    .line 87
    iput-boolean v5, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mSrmActive:Z

    .line 91
    iput-boolean v5, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mSrmEnabled:Z

    .line 95
    iput-boolean v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mSrmWaitingForRemote:Z

    .line 110
    iput-object p2, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mParent:Lcom/navdy/hud/app/bluetooth/obex/ClientSession;

    .line 111
    iput-boolean v5, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mEndOfBodySent:Z

    .line 112
    iput-boolean v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mInputOpen:Z

    .line 113
    iput-boolean v5, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mOperationDone:Z

    .line 114
    iput p1, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mMaxPacketSize:I

    .line 115
    iput-boolean p4, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mGetOperation:Z

    .line 116
    iput-boolean v5, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mGetFinalFlag:Z

    .line 118
    iput-boolean v5, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateInputOpen:Z

    .line 119
    iput-boolean v5, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateOutputOpen:Z

    .line 120
    iput-object v3, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateInput:Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;

    .line 121
    iput-object v3, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateOutput:Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;

    .line 123
    new-instance v2, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    invoke-direct {v2}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;-><init>()V

    iput-object v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    .line 125
    new-instance v2, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    invoke-direct {v2}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;-><init>()V

    iput-object v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mRequestHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    .line 127
    invoke-virtual {p3}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->getHeaderList()[I

    move-result-object v0

    .line 129
    .local v0, "headerList":[I
    if-eqz v0, :cond_0

    .line 131
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_0

    .line 132
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mRequestHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    aget v3, v0, v1

    aget v4, v0, v1

    invoke-virtual {p3, v4}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    .line 131
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 136
    .end local v1    # "i":I
    :cond_0
    iget-object v2, p3, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthChall:[B

    if-eqz v2, :cond_1

    .line 137
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mRequestHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget-object v3, p3, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthChall:[B

    array-length v3, v3

    new-array v3, v3, [B

    iput-object v3, v2, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthChall:[B

    .line 138
    iget-object v2, p3, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthChall:[B

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mRequestHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget-object v3, v3, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthChall:[B

    iget-object v4, p3, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthChall:[B

    array-length v4, v4

    invoke-static {v2, v5, v3, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 142
    :cond_1
    iget-object v2, p3, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    if-eqz v2, :cond_2

    .line 143
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mRequestHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget-object v3, p3, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    array-length v3, v3

    new-array v3, v3, [B

    iput-object v3, v2, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    .line 144
    iget-object v2, p3, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mRequestHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget-object v3, v3, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    iget-object v4, p3, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    array-length v4, v4

    invoke-static {v2, v5, v3, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 149
    :cond_2
    iget-object v2, p3, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mConnectionID:[B

    if-eqz v2, :cond_3

    .line 150
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mRequestHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    new-array v3, v6, [B

    iput-object v3, v2, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mConnectionID:[B

    .line 151
    iget-object v2, p3, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mConnectionID:[B

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mRequestHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget-object v3, v3, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mConnectionID:[B

    invoke-static {v2, v5, v3, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 155
    :cond_3
    return-void
.end method

.method private checkForSrm()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v5, 0x98

    const/4 v4, 0x1

    .line 592
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    const/16 v3, 0x97

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    .line 593
    .local v0, "srmMode":Ljava/lang/Byte;
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mParent:Lcom/navdy/hud/app/bluetooth/obex/ClientSession;

    invoke-virtual {v2}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->isSrmSupported()Z

    move-result v2

    if-ne v2, v4, :cond_0

    if-eqz v0, :cond_0

    .line 594
    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v2

    if-ne v2, v4, :cond_0

    .line 595
    iput-boolean v4, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mSrmEnabled:Z

    .line 605
    :cond_0
    iget-boolean v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mSrmEnabled:Z

    if-eqz v2, :cond_1

    .line 606
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mSrmWaitingForRemote:Z

    .line 607
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    invoke-virtual {v2, v5}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Byte;

    .line 608
    .local v1, "srmp":Ljava/lang/Byte;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Byte;->byteValue()B

    move-result v2

    if-ne v2, v4, :cond_1

    .line 609
    iput-boolean v4, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mSrmWaitingForRemote:Z

    .line 612
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v5, v3}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    .line 615
    .end local v1    # "srmp":Ljava/lang/Byte;
    :cond_1
    iget-boolean v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mSrmWaitingForRemote:Z

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mSrmEnabled:Z

    if-ne v2, v4, :cond_2

    .line 616
    iput-boolean v4, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mSrmActive:Z

    .line 618
    :cond_2
    return-void
.end method

.method private sendRequest(I)Z
    .locals 18
    .param p1, "opCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 442
    const/16 v16, 0x0

    .line 443
    .local v16, "returnValue":Z
    new-instance v15, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v15}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 444
    .local v15, "out":Ljava/io/ByteArrayOutputStream;
    const/4 v12, -0x1

    .line 445
    .local v12, "bodyLength":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mRequestHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->createHeader(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;Z)[B

    move-result-object v14

    .line 446
    .local v14, "headerArray":[B
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateOutput:Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;

    if-eqz v1, :cond_0

    .line 447
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateOutput:Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;

    invoke-virtual {v1}, Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;->size()I

    move-result v12

    .line 458
    :cond_0
    const/4 v10, 0x3

    .line 459
    .local v10, "MINIMUM_BODY_LENGTH":I
    array-length v1, v14

    add-int/lit8 v1, v1, 0x3

    add-int/lit8 v1, v1, 0x3

    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mMaxPacketSize:I

    if-le v1, v2, :cond_8

    .line 461
    const/4 v13, 0x0

    .line 462
    .local v13, "end":I
    const/16 v17, 0x0

    .line 465
    .local v17, "start":I
    :goto_0
    array-length v1, v14

    if-eq v13, v1, :cond_6

    .line 468
    move-object/from16 v0, p0

    iget v1, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mMaxPacketSize:I

    add-int/lit8 v1, v1, -0x3

    move/from16 v0, v17

    invoke-static {v14, v0, v1}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->findHeaderEnd([BII)I

    move-result v13

    .line 471
    const/4 v1, -0x1

    if-ne v13, v1, :cond_3

    .line 472
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mOperationDone:Z

    .line 473
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->abort()V

    .line 474
    const-string v1, "Header larger then can be sent in a packet"

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mExceptionMessage:Ljava/lang/String;

    .line 475
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mInputOpen:Z

    .line 477
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateInput:Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;

    if-eqz v1, :cond_1

    .line 478
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateInput:Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;

    invoke-virtual {v1}, Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;->close()V

    .line 481
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateOutput:Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;

    if-eqz v1, :cond_2

    .line 482
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateOutput:Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;

    invoke-virtual {v1}, Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;->close()V

    .line 484
    :cond_2
    new-instance v1, Ljava/io/IOException;

    const-string v2, "OBEX Packet exceeds max packet size"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 487
    :cond_3
    sub-int v1, v13, v17

    new-array v3, v1, [B

    .line 488
    .local v3, "sendHeader":[B
    const/4 v1, 0x0

    array-length v2, v3

    move/from16 v0, v17

    invoke-static {v14, v0, v3, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 489
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mParent:Lcom/navdy/hud/app/bluetooth/obex/ClientSession;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateInput:Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;

    const/4 v6, 0x0

    move/from16 v2, p1

    invoke-virtual/range {v1 .. v6}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->sendRequest(I[BLcom/navdy/hud/app/bluetooth/obex/HeaderSet;Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;Z)Z

    move-result v1

    if-nez v1, :cond_4

    .line 490
    const/4 v1, 0x0

    .line 588
    .end local v3    # "sendHeader":[B
    .end local v13    # "end":I
    .end local v17    # "start":I
    :goto_1
    return v1

    .line 493
    .restart local v3    # "sendHeader":[B
    .restart local v13    # "end":I
    .restart local v17    # "start":I
    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget v1, v1, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->responseCode:I

    const/16 v2, 0x90

    if-eq v1, v2, :cond_5

    .line 494
    const/4 v1, 0x0

    goto :goto_1

    .line 497
    :cond_5
    move/from16 v17, v13

    .line 498
    goto :goto_0

    .line 501
    .end local v3    # "sendHeader":[B
    :cond_6
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->checkForSrm()V

    .line 503
    if-lez v12, :cond_7

    .line 504
    const/4 v1, 0x1

    goto :goto_1

    .line 506
    :cond_7
    const/4 v1, 0x0

    goto :goto_1

    .line 510
    .end local v13    # "end":I
    .end local v17    # "start":I
    :cond_8
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mSendBodyHeader:Z

    if-nez v1, :cond_9

    .line 512
    move/from16 v0, p1

    or-int/lit16 v0, v0, 0x80

    move/from16 p1, v0

    .line 514
    :cond_9
    invoke-virtual {v15, v14}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 517
    if-lez v12, :cond_b

    .line 523
    move-object/from16 v0, p0

    iget v1, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mMaxPacketSize:I

    array-length v2, v14

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x6

    if-le v12, v1, :cond_a

    .line 524
    const/16 v16, 0x1

    .line 526
    move-object/from16 v0, p0

    iget v1, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mMaxPacketSize:I

    array-length v2, v14

    sub-int/2addr v1, v2

    add-int/lit8 v12, v1, -0x6

    .line 529
    :cond_a
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateOutput:Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;

    invoke-virtual {v1, v12}, Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;->readBytes(I)[B

    move-result-object v11

    .line 536
    .local v11, "body":[B
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateOutput:Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;

    invoke-virtual {v1}, Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;->isClosed()Z

    move-result v1

    if-eqz v1, :cond_d

    if-nez v16, :cond_d

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mEndOfBodySent:Z

    if-nez v1, :cond_d

    move/from16 v0, p1

    and-int/lit16 v1, v0, 0x80

    if-eqz v1, :cond_d

    .line 538
    const/16 v1, 0x49

    invoke-virtual {v15, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 539
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mEndOfBodySent:Z

    .line 544
    :goto_2
    add-int/lit8 v12, v12, 0x3

    .line 545
    shr-int/lit8 v1, v12, 0x8

    int-to-byte v1, v1

    invoke-virtual {v15, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 546
    int-to-byte v1, v12

    invoke-virtual {v15, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 548
    if-eqz v11, :cond_b

    .line 549
    invoke-virtual {v15, v11}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 553
    .end local v11    # "body":[B
    :cond_b
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateOutputOpen:Z

    if-eqz v1, :cond_c

    if-gtz v12, :cond_c

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mEndOfBodySent:Z

    if-nez v1, :cond_c

    .line 555
    move/from16 v0, p1

    and-int/lit16 v1, v0, 0x80

    if-nez v1, :cond_e

    .line 556
    const/16 v1, 0x48

    invoke-virtual {v15, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 562
    :goto_3
    const/4 v12, 0x3

    .line 563
    const/4 v1, 0x0

    int-to-byte v1, v1

    invoke-virtual {v15, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 564
    int-to-byte v1, v12

    invoke-virtual {v15, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 567
    :cond_c
    invoke-virtual {v15}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v1

    if-nez v1, :cond_10

    .line 568
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mParent:Lcom/navdy/hud/app/bluetooth/obex/ClientSession;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateInput:Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;

    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mSrmActive:Z

    move/from16 v5, p1

    invoke-virtual/range {v4 .. v9}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->sendRequest(I[BLcom/navdy/hud/app/bluetooth/obex/HeaderSet;Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;Z)Z

    move-result v1

    if-nez v1, :cond_f

    .line 569
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 541
    .restart local v11    # "body":[B
    :cond_d
    const/16 v1, 0x48

    invoke-virtual {v15, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    goto :goto_2

    .line 558
    .end local v11    # "body":[B
    :cond_e
    const/16 v1, 0x49

    invoke-virtual {v15, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 559
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mEndOfBodySent:Z

    goto :goto_3

    .line 572
    :cond_f
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->checkForSrm()V

    move/from16 v1, v16

    .line 573
    goto/16 :goto_1

    .line 575
    :cond_10
    invoke-virtual {v15}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v1

    if-lez v1, :cond_11

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mParent:Lcom/navdy/hud/app/bluetooth/obex/ClientSession;

    .line 576
    invoke-virtual {v15}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateInput:Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;

    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mSrmActive:Z

    move/from16 v5, p1

    invoke-virtual/range {v4 .. v9}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->sendRequest(I[BLcom/navdy/hud/app/bluetooth/obex/HeaderSet;Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;Z)Z

    move-result v1

    if-nez v1, :cond_11

    .line 578
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 581
    :cond_11
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->checkForSrm()V

    .line 585
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateOutput:Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;

    if-eqz v1, :cond_12

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateOutput:Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;

    invoke-virtual {v1}, Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;->size()I

    move-result v1

    if-lez v1, :cond_12

    .line 586
    const/16 v16, 0x1

    :cond_12
    move/from16 v1, v16

    .line 588
    goto/16 :goto_1
.end method

.method private declared-synchronized startProcessing()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v7, 0x90

    .line 628
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateInput:Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;

    if-nez v0, :cond_0

    .line 629
    new-instance v0, Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;-><init>(Lcom/navdy/hud/app/bluetooth/obex/BaseStream;)V

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateInput:Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;

    .line 631
    :cond_0
    const/4 v6, 0x1

    .line 633
    .local v6, "more":Z
    iget-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mGetOperation:Z

    if-eqz v0, :cond_5

    .line 634
    iget-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mOperationDone:Z

    if-nez v0, :cond_3

    .line 635
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    const/16 v1, 0x90

    iput v1, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->responseCode:I

    .line 636
    :goto_0
    if-eqz v6, :cond_1

    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->responseCode:I

    if-ne v0, v7, :cond_1

    .line 637
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->sendRequest(I)Z

    move-result v6

    goto :goto_0

    .line 642
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->responseCode:I

    if-ne v0, v7, :cond_2

    .line 643
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mParent:Lcom/navdy/hud/app/bluetooth/obex/ClientSession;

    const/16 v1, 0x83

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget-object v4, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateInput:Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;

    iget-boolean v5, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mSrmActive:Z

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->sendRequest(I[BLcom/navdy/hud/app/bluetooth/obex/HeaderSet;Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;Z)Z

    .line 646
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->responseCode:I

    if-eq v0, v7, :cond_4

    .line 647
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mOperationDone:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 670
    :cond_3
    :goto_1
    monitor-exit p0

    return-void

    .line 649
    :cond_4
    :try_start_1
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->checkForSrm()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 628
    .end local v6    # "more":Z
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 654
    .restart local v6    # "more":Z
    :cond_5
    :try_start_2
    iget-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mOperationDone:Z

    if-nez v0, :cond_6

    .line 655
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    const/16 v1, 0x90

    iput v1, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->responseCode:I

    .line 656
    :goto_2
    if-eqz v6, :cond_6

    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->responseCode:I

    if-ne v0, v7, :cond_6

    .line 657
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->sendRequest(I)Z

    move-result v6

    goto :goto_2

    .line 661
    :cond_6
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->responseCode:I

    if-ne v0, v7, :cond_7

    .line 662
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mParent:Lcom/navdy/hud/app/bluetooth/obex/ClientSession;

    const/16 v1, 0x82

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget-object v4, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateInput:Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;

    iget-boolean v5, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mSrmActive:Z

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->sendRequest(I[BLcom/navdy/hud/app/bluetooth/obex/HeaderSet;Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;Z)Z

    .line 666
    :cond_7
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->responseCode:I

    if-eq v0, v7, :cond_3

    .line 667
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mOperationDone:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method private validateConnection()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 424
    invoke-virtual {p0}, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->ensureOpen()V

    .line 427
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateInput:Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;

    if-nez v0, :cond_0

    .line 428
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->startProcessing()V

    .line 430
    :cond_0
    return-void
.end method


# virtual methods
.method public declared-synchronized abort()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v1, 0x90

    .line 174
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->ensureOpen()V

    .line 176
    iget-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mOperationDone:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->responseCode:I

    if-eq v0, v1, :cond_0

    .line 177
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Operation has already ended"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 174
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 180
    :cond_0
    :try_start_1
    const-string v0, "Operation aborted"

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mExceptionMessage:Ljava/lang/String;

    .line 181
    iget-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mOperationDone:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->responseCode:I

    if-ne v0, v1, :cond_2

    .line 182
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mOperationDone:Z

    .line 187
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mParent:Lcom/navdy/hud/app/bluetooth/obex/ClientSession;

    const/16 v1, 0xff

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->sendRequest(I[BLcom/navdy/hud/app/bluetooth/obex/HeaderSet;Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;Z)Z

    .line 189
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->responseCode:I

    const/16 v1, 0xa0

    if-eq v0, v1, :cond_1

    .line 190
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Invalid response code from server"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 193
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mExceptionMessage:Ljava/lang/String;

    .line 196
    :cond_2
    invoke-virtual {p0}, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 197
    monitor-exit p0

    return-void
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 346
    iput-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mInputOpen:Z

    .line 347
    iput-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateInputOpen:Z

    .line 348
    iput-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateOutputOpen:Z

    .line 349
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mParent:Lcom/navdy/hud/app/bluetooth/obex/ClientSession;

    invoke-virtual {v0}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->setRequestInactive()V

    .line 350
    return-void
.end method

.method public declared-synchronized continueOperation(ZZ)Z
    .locals 8
    .param p1, "sendEmpty"    # Z
    .param p2, "inStream"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v7, 0x90

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 686
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mGetOperation:Z

    if-eqz v1, :cond_5

    .line 687
    if-eqz p2, :cond_2

    iget-boolean v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mOperationDone:Z

    if-nez v1, :cond_2

    .line 689
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mParent:Lcom/navdy/hud/app/bluetooth/obex/ClientSession;

    const/16 v1, 0x83

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget-object v4, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateInput:Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;

    iget-boolean v5, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mSrmActive:Z

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->sendRequest(I[BLcom/navdy/hud/app/bluetooth/obex/HeaderSet;Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;Z)Z

    .line 694
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->responseCode:I

    if-eq v0, v7, :cond_1

    .line 695
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mOperationDone:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    move v0, v6

    .line 733
    :cond_0
    :goto_1
    monitor-exit p0

    return v0

    .line 697
    :cond_1
    :try_start_1
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->checkForSrm()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 686
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 702
    :cond_2
    if-nez p2, :cond_4

    :try_start_2
    iget-boolean v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mOperationDone:Z

    if-nez v1, :cond_4

    .line 705
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateInput:Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;

    if-nez v0, :cond_3

    .line 706
    new-instance v0, Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;-><init>(Lcom/navdy/hud/app/bluetooth/obex/BaseStream;)V

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateInput:Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;

    .line 708
    :cond_3
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->sendRequest(I)Z

    move v0, v6

    .line 709
    goto :goto_1

    .line 711
    :cond_4
    iget-boolean v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mOperationDone:Z

    if-eqz v1, :cond_0

    goto :goto_1

    .line 717
    :cond_5
    if-nez p2, :cond_7

    iget-boolean v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mOperationDone:Z

    if-nez v1, :cond_7

    .line 719
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->responseCode:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_6

    .line 720
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    const/16 v1, 0x90

    iput v1, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->responseCode:I

    .line 722
    :cond_6
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->sendRequest(I)Z

    move v0, v6

    .line 723
    goto :goto_1

    .line 724
    :cond_7
    if-eqz p2, :cond_8

    iget-boolean v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mOperationDone:Z

    if-eqz v1, :cond_0

    .line 728
    :cond_8
    iget-boolean v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mOperationDone:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v1, :cond_0

    goto :goto_1
.end method

.method public ensureNotDone()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 399
    iget-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mOperationDone:Z

    if-eqz v0, :cond_0

    .line 400
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Operation has completed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 402
    :cond_0
    return-void
.end method

.method public ensureOpen()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 409
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mParent:Lcom/navdy/hud/app/bluetooth/obex/ClientSession;

    invoke-virtual {v0}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->ensureOpen()V

    .line 411
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mExceptionMessage:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 412
    new-instance v0, Ljava/io/IOException;

    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mExceptionMessage:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 414
    :cond_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mInputOpen:Z

    if-nez v0, :cond_1

    .line 415
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Operation has already ended"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 417
    :cond_1
    return-void
.end method

.method public getEncoding()Ljava/lang/String;
    .locals 1

    .prologue
    .line 224
    const/4 v0, 0x0

    return-object v0
.end method

.method public getHeaderLength()I
    .locals 3

    .prologue
    .line 328
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mRequestHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->createHeader(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;Z)[B

    move-result-object v0

    .line 329
    .local v0, "headerArray":[B
    array-length v1, v0

    return v1
.end method

.method public getLength()J
    .locals 6

    .prologue
    const-wide/16 v2, -0x1

    .line 252
    :try_start_0
    iget-object v4, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    const/16 v5, 0xc3

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 254
    .local v1, "temp":Ljava/lang/Long;
    if-nez v1, :cond_0

    .line 261
    .end local v1    # "temp":Ljava/lang/Long;
    :goto_0
    return-wide v2

    .line 257
    .restart local v1    # "temp":Ljava/lang/Long;
    :cond_0
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    goto :goto_0

    .line 259
    .end local v1    # "temp":Ljava/lang/Long;
    :catch_0
    move-exception v0

    .line 261
    .local v0, "e":Ljava/io/IOException;
    goto :goto_0
.end method

.method public getMaxPacketSize()I
    .locals 2

    .prologue
    .line 323
    iget v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mMaxPacketSize:I

    add-int/lit8 v0, v0, -0x6

    invoke-virtual {p0}, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->getHeaderLength()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public getReceivedHeader()Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 360
    invoke-virtual {p0}, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->ensureOpen()V

    .line 362
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    return-object v0
.end method

.method public declared-synchronized getResponseCode()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 211
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->responseCode:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->responseCode:I

    const/16 v1, 0x90

    if-ne v0, v1, :cond_1

    .line 213
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->validateConnection()V

    .line 216
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->responseCode:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    .line 211
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getType()Ljava/lang/String;
    .locals 3

    .prologue
    .line 236
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    const/16 v2, 0x42

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 239
    :goto_0
    return-object v1

    .line 237
    :catch_0
    move-exception v0

    .line 239
    .local v0, "e":Ljava/io/IOException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public noBodyHeader()V
    .locals 1

    .prologue
    .line 830
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mSendBodyHeader:Z

    .line 831
    return-void
.end method

.method public openDataInputStream()Ljava/io/DataInputStream;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 296
    new-instance v0, Ljava/io/DataInputStream;

    invoke-virtual {p0}, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->openInputStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    return-object v0
.end method

.method public openDataOutputStream()Ljava/io/DataOutputStream;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 338
    new-instance v0, Ljava/io/DataOutputStream;

    invoke-virtual {p0}, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->openOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    return-object v0
.end method

.method public openInputStream()Ljava/io/InputStream;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 272
    invoke-virtual {p0}, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->ensureOpen()V

    .line 274
    iget-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateInputOpen:Z

    if-eqz v0, :cond_0

    .line 275
    new-instance v0, Ljava/io/IOException;

    const-string v1, "no more input streams available"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 276
    :cond_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mGetOperation:Z

    if-eqz v0, :cond_2

    .line 278
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->validateConnection()V

    .line 285
    :cond_1
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateInputOpen:Z

    .line 287
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateInput:Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;

    return-object v0

    .line 280
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateInput:Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;

    if-nez v0, :cond_1

    .line 281
    new-instance v0, Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;-><init>(Lcom/navdy/hud/app/bluetooth/obex/BaseStream;)V

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateInput:Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;

    goto :goto_0
.end method

.method public openOutputStream()Ljava/io/OutputStream;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 306
    invoke-virtual {p0}, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->ensureOpen()V

    .line 307
    invoke-virtual {p0}, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->ensureNotDone()V

    .line 309
    iget-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateOutputOpen:Z

    if-eqz v0, :cond_0

    .line 310
    new-instance v0, Ljava/io/IOException;

    const-string v1, "no more output streams available"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 312
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateOutput:Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;

    if-nez v0, :cond_1

    .line 314
    new-instance v0, Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;

    invoke-virtual {p0}, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->getMaxPacketSize()I

    move-result v1

    invoke-direct {v0, p0, v1}, Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;-><init>(Lcom/navdy/hud/app/bluetooth/obex/BaseStream;I)V

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateOutput:Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;

    .line 317
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateOutputOpen:Z

    .line 319
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateOutput:Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;

    return-object v0
.end method

.method public sendHeaders(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;)V
    .locals 5
    .param p1, "headers"    # Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 376
    invoke-virtual {p0}, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->ensureOpen()V

    .line 377
    iget-boolean v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mOperationDone:Z

    if-eqz v2, :cond_0

    .line 378
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Operation has already exchanged all data"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 381
    :cond_0
    if-nez p1, :cond_1

    .line 382
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Headers may not be null"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 385
    :cond_1
    invoke-virtual {p1}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->getHeaderList()[I

    move-result-object v0

    .line 386
    .local v0, "headerList":[I
    if-eqz v0, :cond_2

    .line 387
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_2

    .line 388
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mRequestHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    aget v3, v0, v1

    aget v4, v0, v1

    invoke-virtual {p1, v4}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    .line 387
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 391
    .end local v1    # "i":I
    :cond_2
    return-void
.end method

.method public setGetFinalFlag(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 163
    iput-boolean p1, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mGetFinalFlag:Z

    .line 164
    return-void
.end method

.method public streamClosed(Z)V
    .locals 10
    .param p1, "inStream"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, -0x1

    const/16 v1, 0x83

    const/4 v5, 0x0

    const/4 v9, 0x1

    const/16 v8, 0x90

    .line 743
    iget-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mGetOperation:Z

    if-nez v0, :cond_6

    .line 744
    if-nez p1, :cond_5

    iget-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mOperationDone:Z

    if-nez v0, :cond_5

    .line 747
    const/4 v7, 0x1

    .line 749
    .local v7, "more":Z
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateOutput:Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateOutput:Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;

    invoke-virtual {v0}, Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;->size()I

    move-result v0

    if-gtz v0, :cond_0

    .line 750
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mRequestHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    invoke-static {v0, v5}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->createHeader(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;Z)[B

    move-result-object v6

    .line 751
    .local v6, "headerArray":[B
    array-length v0, v6

    if-gtz v0, :cond_0

    .line 752
    const/4 v7, 0x0

    .line 755
    .end local v6    # "headerArray":[B
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->responseCode:I

    if-ne v0, v2, :cond_1

    .line 756
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iput v8, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->responseCode:I

    .line 759
    :cond_1
    :goto_0
    if-eqz v7, :cond_2

    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->responseCode:I

    if-ne v0, v8, :cond_2

    .line 760
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->sendRequest(I)Z

    move-result v7

    goto :goto_0

    .line 768
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->responseCode:I

    if-ne v0, v8, :cond_3

    .line 770
    const/16 v0, 0x82

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->sendRequest(I)Z

    goto :goto_1

    .line 772
    :cond_3
    iput-boolean v9, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mOperationDone:Z

    .line 827
    .end local v7    # "more":Z
    :cond_4
    :goto_2
    return-void

    .line 773
    :cond_5
    if-eqz p1, :cond_4

    iget-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mOperationDone:Z

    if-eqz v0, :cond_4

    .line 775
    iput-boolean v9, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mOperationDone:Z

    goto :goto_2

    .line 778
    :cond_6
    if-eqz p1, :cond_a

    iget-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mOperationDone:Z

    if-nez v0, :cond_a

    .line 783
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->responseCode:I

    if-ne v0, v2, :cond_7

    .line 784
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iput v8, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->responseCode:I

    .line 787
    :cond_7
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->responseCode:I

    if-ne v0, v8, :cond_8

    iget-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mOperationDone:Z

    if-nez v0, :cond_8

    .line 788
    invoke-direct {p0, v1}, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->sendRequest(I)Z

    move-result v0

    if-nez v0, :cond_7

    .line 792
    :cond_8
    :goto_3
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->responseCode:I

    if-ne v0, v8, :cond_9

    iget-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mOperationDone:Z

    if-nez v0, :cond_9

    .line 793
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mParent:Lcom/navdy/hud/app/bluetooth/obex/ClientSession;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget-object v4, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateInput:Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->sendRequest(I[BLcom/navdy/hud/app/bluetooth/obex/HeaderSet;Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;Z)Z

    goto :goto_3

    .line 797
    :cond_9
    iput-boolean v9, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mOperationDone:Z

    goto :goto_2

    .line 798
    :cond_a
    if-nez p1, :cond_4

    iget-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mOperationDone:Z

    if-nez v0, :cond_4

    .line 802
    const/4 v7, 0x1

    .line 804
    .restart local v7    # "more":Z
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateOutput:Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateOutput:Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;

    invoke-virtual {v0}, Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;->size()I

    move-result v0

    if-gtz v0, :cond_b

    .line 805
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mRequestHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    invoke-static {v0, v5}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->createHeader(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;Z)[B

    move-result-object v6

    .line 806
    .restart local v6    # "headerArray":[B
    array-length v0, v6

    if-gtz v0, :cond_b

    .line 807
    const/4 v7, 0x0

    .line 810
    .end local v6    # "headerArray":[B
    :cond_b
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateInput:Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;

    if-nez v0, :cond_c

    .line 811
    new-instance v0, Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;-><init>(Lcom/navdy/hud/app/bluetooth/obex/BaseStream;)V

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateInput:Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;

    .line 813
    :cond_c
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateOutput:Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mPrivateOutput:Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;

    invoke-virtual {v0}, Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;->size()I

    move-result v0

    if-gtz v0, :cond_d

    .line 814
    const/4 v7, 0x0

    .line 816
    :cond_d
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iput v8, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->responseCode:I

    .line 817
    :goto_4
    if-eqz v7, :cond_e

    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->responseCode:I

    if-ne v0, v8, :cond_e

    .line 818
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->sendRequest(I)Z

    move-result v7

    goto :goto_4

    .line 820
    :cond_e
    invoke-direct {p0, v1}, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->sendRequest(I)Z

    .line 822
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mReplyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->responseCode:I

    if-eq v0, v8, :cond_4

    .line 823
    iput-boolean v9, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->mOperationDone:Z

    goto/16 :goto_2
.end method
