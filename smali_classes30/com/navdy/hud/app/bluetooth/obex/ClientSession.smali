.class public final Lcom/navdy/hud/app/bluetooth/obex/ClientSession;
.super Lcom/navdy/hud/app/bluetooth/obex/ObexSession;
.source "ClientSession.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ClientSession"


# instance fields
.field private mConnectionId:[B

.field private final mInput:Ljava/io/InputStream;

.field private final mLocalSrmSupported:Z

.field private mMaxTxPacketSize:I

.field private mObexConnected:Z

.field private mOpen:Z

.field private final mOutput:Ljava/io/OutputStream;

.field private mRequestActive:Z

.field private final mTransport:Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;


# direct methods
.method public constructor <init>(Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;)V
    .locals 1
    .param p1, "trans"    # Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/obex/ObexSession;-><init>()V

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mConnectionId:[B

    .line 63
    const/16 v0, 0xff

    iput v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mMaxTxPacketSize:I

    .line 76
    invoke-interface {p1}, Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;->openInputStream()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mInput:Ljava/io/InputStream;

    .line 77
    invoke-interface {p1}, Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;->openOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mOutput:Ljava/io/OutputStream;

    .line 78
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mOpen:Z

    .line 79
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mRequestActive:Z

    .line 80
    invoke-interface {p1}, Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;->isSrmSupported()Z

    move-result v0

    iput-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mLocalSrmSupported:Z

    .line 81
    iput-object p1, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mTransport:Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;

    .line 82
    return-void
.end method

.method public constructor <init>(Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;Z)V
    .locals 1
    .param p1, "trans"    # Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;
    .param p2, "supportsSrm"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 91
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/obex/ObexSession;-><init>()V

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mConnectionId:[B

    .line 63
    const/16 v0, 0xff

    iput v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mMaxTxPacketSize:I

    .line 92
    invoke-interface {p1}, Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;->openInputStream()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mInput:Ljava/io/InputStream;

    .line 93
    invoke-interface {p1}, Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;->openOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mOutput:Ljava/io/OutputStream;

    .line 94
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mOpen:Z

    .line 95
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mRequestActive:Z

    .line 96
    iput-boolean p2, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mLocalSrmSupported:Z

    .line 97
    iput-object p1, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mTransport:Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;

    .line 98
    return-void
.end method

.method private declared-synchronized setRequestActive()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 438
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mRequestActive:Z

    if-eqz v0, :cond_0

    .line 439
    new-instance v0, Ljava/io/IOException;

    const-string v1, "OBEX request is already being performed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 438
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 441
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mRequestActive:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 442
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 608
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mOpen:Z

    .line 609
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mInput:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 610
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mOutput:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 611
    return-void
.end method

.method public connect(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;)Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    .locals 10
    .param p1, "header"    # Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/16 v4, 0x10

    const/4 v5, 0x0

    .line 101
    invoke-virtual {p0}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->ensureOpen()V

    .line 102
    iget-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mObexConnected:Z

    if-eqz v0, :cond_0

    .line 103
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Already connected to server"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 105
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->setRequestActive()V

    .line 107
    const/4 v8, 0x4

    .line 108
    .local v8, "totalLength":I
    const/4 v6, 0x0

    .line 111
    .local v6, "head":[B
    if-eqz p1, :cond_2

    .line 112
    iget-object v0, p1, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->nonce:[B

    if-eqz v0, :cond_1

    .line 113
    new-array v0, v4, [B

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mChallengeDigest:[B

    .line 114
    iget-object v0, p1, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->nonce:[B

    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mChallengeDigest:[B

    invoke-static {v0, v5, v1, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 116
    :cond_1
    invoke-static {p1, v5}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->createHeader(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;Z)[B

    move-result-object v6

    .line 117
    array-length v0, v6

    add-int/2addr v8, v0

    .line 128
    :cond_2
    new-array v2, v8, [B

    .line 129
    .local v2, "requestPacket":[B
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mTransport:Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;

    invoke-static {v0}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->getMaxRxPacketSize(Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;)I

    move-result v7

    .line 132
    .local v7, "maxRxPacketSize":I
    aput-byte v4, v2, v5

    .line 133
    aput-byte v5, v2, v9

    .line 134
    const/4 v0, 0x2

    shr-int/lit8 v1, v7, 0x8

    int-to-byte v1, v1

    aput-byte v1, v2, v0

    .line 135
    const/4 v0, 0x3

    and-int/lit16 v1, v7, 0xff

    int-to-byte v1, v1

    aput-byte v1, v2, v0

    .line 136
    if-eqz v6, :cond_3

    .line 137
    const/4 v0, 0x4

    array-length v1, v6

    invoke-static {v6, v5, v2, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 142
    :cond_3
    array-length v0, v2

    add-int/lit8 v0, v0, 0x3

    const v1, 0xfffe

    if-le v0, v1, :cond_4

    .line 143
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Packet size exceeds max packet size for connect"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 146
    :cond_4
    new-instance v3, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    invoke-direct {v3}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;-><init>()V

    .line 147
    .local v3, "returnHeaderSet":Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    const/16 v1, 0x80

    const/4 v4, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->sendRequest(I[BLcom/navdy/hud/app/bluetooth/obex/HeaderSet;Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;Z)Z

    .line 158
    iget v0, v3, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->responseCode:I

    const/16 v1, 0xa0

    if-ne v0, v1, :cond_5

    .line 159
    iput-boolean v9, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mObexConnected:Z

    .line 161
    :cond_5
    invoke-virtual {p0}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->setRequestInactive()V

    .line 163
    return-object v3
.end method

.method public delete(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;)Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    .locals 2
    .param p1, "header"    # Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 217
    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->put(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;)Lcom/navdy/hud/app/bluetooth/obex/Operation;

    move-result-object v0

    .line 218
    .local v0, "op":Lcom/navdy/hud/app/bluetooth/obex/Operation;
    invoke-interface {v0}, Lcom/navdy/hud/app/bluetooth/obex/Operation;->getResponseCode()I

    .line 219
    invoke-interface {v0}, Lcom/navdy/hud/app/bluetooth/obex/Operation;->getReceivedHeader()Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    move-result-object v1

    .line 220
    .local v1, "returnValue":Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    invoke-interface {v0}, Lcom/navdy/hud/app/bluetooth/obex/Operation;->close()V

    .line 222
    return-object v1
.end method

.method public disconnect(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;)Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    .locals 7
    .param p1, "header"    # Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v6, 0x10

    const/4 v4, 0x4

    const/4 v5, 0x0

    .line 226
    iget-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mObexConnected:Z

    if-nez v0, :cond_0

    .line 227
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Not connected to the server"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 229
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->setRequestActive()V

    .line 231
    invoke-virtual {p0}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->ensureOpen()V

    .line 233
    const/4 v2, 0x0

    .line 234
    .local v2, "head":[B
    if-eqz p1, :cond_3

    .line 235
    iget-object v0, p1, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->nonce:[B

    if-eqz v0, :cond_1

    .line 236
    new-array v0, v6, [B

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mChallengeDigest:[B

    .line 237
    iget-object v0, p1, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->nonce:[B

    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mChallengeDigest:[B

    invoke-static {v0, v5, v1, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 240
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mConnectionId:[B

    if-eqz v0, :cond_2

    .line 241
    new-array v0, v4, [B

    iput-object v0, p1, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mConnectionID:[B

    .line 242
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mConnectionId:[B

    iget-object v1, p1, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mConnectionID:[B

    invoke-static {v0, v5, v1, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 244
    :cond_2
    invoke-static {p1, v5}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->createHeader(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;Z)[B

    move-result-object v2

    .line 246
    array-length v0, v2

    add-int/lit8 v0, v0, 0x3

    iget v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mMaxTxPacketSize:I

    if-le v0, v1, :cond_4

    .line 247
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Packet size exceeds max packet size"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 251
    :cond_3
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mConnectionId:[B

    if-eqz v0, :cond_4

    .line 252
    const/4 v0, 0x5

    new-array v2, v0, [B

    .line 253
    const/16 v0, -0x35

    aput-byte v0, v2, v5

    .line 254
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mConnectionId:[B

    const/4 v1, 0x1

    invoke-static {v0, v5, v2, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 258
    :cond_4
    new-instance v3, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    invoke-direct {v3}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;-><init>()V

    .line 259
    .local v3, "returnHeaderSet":Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    const/16 v1, 0x81

    const/4 v4, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->sendRequest(I[BLcom/navdy/hud/app/bluetooth/obex/HeaderSet;Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;Z)Z

    .line 271
    monitor-enter p0

    .line 272
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mObexConnected:Z

    .line 273
    invoke-virtual {p0}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->setRequestInactive()V

    .line 274
    monitor-exit p0

    .line 276
    return-object v3

    .line 274
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public declared-synchronized ensureOpen()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 420
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mOpen:Z

    if-nez v0, :cond_0

    .line 421
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Connection closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 420
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 423
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public get(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;)Lcom/navdy/hud/app/bluetooth/obex/Operation;
    .locals 7
    .param p1, "header"    # Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v6, 0x10

    const/4 v5, 0x4

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 168
    iget-boolean v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mObexConnected:Z

    if-nez v1, :cond_0

    .line 169
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Not connected to the server"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 171
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->setRequestActive()V

    .line 173
    invoke-virtual {p0}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->ensureOpen()V

    .line 176
    if-nez p1, :cond_4

    .line 177
    new-instance v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    invoke-direct {v0}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;-><init>()V

    .line 186
    .local v0, "head":Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mConnectionId:[B

    if-eqz v1, :cond_2

    .line 187
    new-array v1, v5, [B

    iput-object v1, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mConnectionID:[B

    .line 188
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mConnectionId:[B

    iget-object v2, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mConnectionID:[B

    invoke-static {v1, v3, v2, v3, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 191
    :cond_2
    iget-boolean v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mLocalSrmSupported:Z

    if-eqz v1, :cond_3

    .line 192
    const/16 v1, 0x97

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    .line 202
    :cond_3
    new-instance v1, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;

    iget v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mMaxTxPacketSize:I

    invoke-direct {v1, v2, p0, v0, v4}, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;-><init>(ILcom/navdy/hud/app/bluetooth/obex/ClientSession;Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;Z)V

    return-object v1

    .line 179
    .end local v0    # "head":Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    :cond_4
    move-object v0, p1

    .line 180
    .restart local v0    # "head":Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    iget-object v1, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->nonce:[B

    if-eqz v1, :cond_1

    .line 181
    new-array v1, v6, [B

    iput-object v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mChallengeDigest:[B

    .line 182
    iget-object v1, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->nonce:[B

    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mChallengeDigest:[B

    invoke-static {v1, v3, v2, v3, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method public getConnectionID()J
    .locals 2

    .prologue
    .line 281
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mConnectionId:[B

    if-nez v0, :cond_0

    .line 282
    const-wide/16 v0, -0x1

    .line 284
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mConnectionId:[B

    invoke-static {v0}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->convertToLong([B)J

    move-result-wide v0

    goto :goto_0
.end method

.method public isSrmSupported()Z
    .locals 1

    .prologue
    .line 614
    iget-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mLocalSrmSupported:Z

    return v0
.end method

.method public put(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;)Lcom/navdy/hud/app/bluetooth/obex/Operation;
    .locals 6
    .param p1, "header"    # Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v5, 0x10

    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 288
    iget-boolean v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mObexConnected:Z

    if-nez v1, :cond_0

    .line 289
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Not connected to the server"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 291
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->setRequestActive()V

    .line 293
    invoke-virtual {p0}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->ensureOpen()V

    .line 295
    if-nez p1, :cond_4

    .line 296
    new-instance v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    invoke-direct {v0}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;-><init>()V

    .line 307
    .local v0, "head":Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mConnectionId:[B

    if-eqz v1, :cond_2

    .line 309
    new-array v1, v4, [B

    iput-object v1, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mConnectionID:[B

    .line 310
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mConnectionId:[B

    iget-object v2, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mConnectionID:[B

    invoke-static {v1, v3, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 313
    :cond_2
    iget-boolean v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mLocalSrmSupported:Z

    if-eqz v1, :cond_3

    .line 314
    const/16 v1, 0x97

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    .line 322
    :cond_3
    new-instance v1, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;

    iget v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mMaxTxPacketSize:I

    invoke-direct {v1, v2, p0, v0, v3}, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;-><init>(ILcom/navdy/hud/app/bluetooth/obex/ClientSession;Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;Z)V

    return-object v1

    .line 298
    .end local v0    # "head":Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    :cond_4
    move-object v0, p1

    .line 300
    .restart local v0    # "head":Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    iget-object v1, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->nonce:[B

    if-eqz v1, :cond_1

    .line 301
    new-array v1, v5, [B

    iput-object v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mChallengeDigest:[B

    .line 302
    iget-object v1, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->nonce:[B

    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mChallengeDigest:[B

    invoke-static {v1, v3, v2, v3, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method public sendRequest(I[BLcom/navdy/hud/app/bluetooth/obex/HeaderSet;Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;Z)Z
    .locals 17
    .param p1, "opCode"    # I
    .param p2, "head"    # [B
    .param p3, "header"    # Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    .param p4, "privateInput"    # Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;
    .param p5, "srmActive"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 462
    if-eqz p2, :cond_0

    .line 463
    move-object/from16 v0, p2

    array-length v2, v0

    add-int/lit8 v2, v2, 0x3

    const v3, 0xfffe

    if-le v2, v3, :cond_0

    .line 465
    new-instance v2, Ljava/io/IOException;

    const-string v3, "header too large "

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 469
    :cond_0
    const/4 v15, 0x0

    .line 470
    .local v15, "skipSend":Z
    const/4 v14, 0x0

    .line 471
    .local v14, "skipReceive":Z
    const/4 v2, 0x1

    move/from16 v0, p5

    if-ne v0, v2, :cond_1

    .line 472
    const/4 v2, 0x2

    move/from16 v0, p1

    if-ne v0, v2, :cond_3

    .line 474
    const/4 v14, 0x1

    .line 488
    :cond_1
    :goto_0
    new-instance v13, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v13}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 489
    .local v13, "out":Ljava/io/ByteArrayOutputStream;
    move/from16 v0, p1

    int-to-byte v2, v0

    invoke-virtual {v13, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 492
    if-nez p2, :cond_5

    .line 493
    const/4 v2, 0x0

    invoke-virtual {v13, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 494
    const/4 v2, 0x3

    invoke-virtual {v13, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 501
    :goto_1
    if-nez v15, :cond_2

    .line 503
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mOutput:Ljava/io/OutputStream;

    invoke-virtual {v13}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/OutputStream;->write([B)V

    .line 508
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mOutput:Ljava/io/OutputStream;

    invoke-virtual {v2}, Ljava/io/OutputStream;->flush()V

    .line 511
    :cond_2
    if-nez v14, :cond_10

    .line 512
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mInput:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->read()I

    move-result v2

    move-object/from16 v0, p3

    iput v2, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->responseCode:I

    .line 514
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mInput:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->read()I

    move-result v2

    shl-int/lit8 v2, v2, 0x8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mInput:Ljava/io/InputStream;

    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    move-result v3

    or-int v12, v2, v3

    .line 516
    .local v12, "length":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mTransport:Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;

    invoke-static {v2}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->getMaxRxPacketSize(Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;)I

    move-result v2

    if-le v12, v2, :cond_6

    .line 517
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Packet received exceeds packet size limit"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 475
    .end local v12    # "length":I
    .end local v13    # "out":Ljava/io/ByteArrayOutputStream;
    :cond_3
    const/4 v2, 0x3

    move/from16 v0, p1

    if-ne v0, v2, :cond_4

    .line 478
    const/4 v14, 0x1

    goto :goto_0

    .line 479
    :cond_4
    const/16 v2, 0x83

    move/from16 v0, p1

    if-ne v0, v2, :cond_1

    .line 482
    const/4 v15, 0x1

    goto :goto_0

    .line 496
    .restart local v13    # "out":Ljava/io/ByteArrayOutputStream;
    :cond_5
    move-object/from16 v0, p2

    array-length v2, v0

    add-int/lit8 v2, v2, 0x3

    shr-int/lit8 v2, v2, 0x8

    int-to-byte v2, v2

    invoke-virtual {v13, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 497
    move-object/from16 v0, p2

    array-length v2, v0

    add-int/lit8 v2, v2, 0x3

    int-to-byte v2, v2

    invoke-virtual {v13, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 498
    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    goto :goto_1

    .line 519
    .restart local v12    # "length":I
    :cond_6
    const/4 v2, 0x3

    if-le v12, v2, :cond_10

    .line 520
    const/4 v10, 0x0

    .line 521
    .local v10, "data":[B
    const/16 v2, 0x80

    move/from16 v0, p1

    if-ne v0, v2, :cond_a

    .line 523
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mInput:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->read()I

    move-result v16

    .line 525
    .local v16, "version":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mInput:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->read()I

    move-result v11

    .line 526
    .local v11, "flags":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mInput:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->read()I

    move-result v2

    shl-int/lit8 v2, v2, 0x8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mInput:Ljava/io/InputStream;

    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    move-result v3

    add-int/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mMaxTxPacketSize:I

    .line 529
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mMaxTxPacketSize:I

    const v3, 0xfc00

    if-le v2, v3, :cond_7

    .line 530
    const v2, 0xfc00

    move-object/from16 v0, p0

    iput v2, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mMaxTxPacketSize:I

    .line 534
    :cond_7
    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mMaxTxPacketSize:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mTransport:Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;

    invoke-static {v3}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->getMaxTxPacketSize(Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;)I

    move-result v3

    if-le v2, v3, :cond_8

    .line 537
    const-string v2, "ClientSession"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "An OBEX packet size of "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mMaxTxPacketSize:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "was"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " requested. Transport only allows: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mTransport:Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;

    .line 539
    invoke-static {v5}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->getMaxTxPacketSize(Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;)I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " Lowering limit to this value."

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 537
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 541
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mTransport:Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;

    invoke-static {v2}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->getMaxTxPacketSize(Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mMaxTxPacketSize:I

    .line 544
    :cond_8
    const/4 v2, 0x7

    if-le v12, v2, :cond_9

    .line 545
    add-int/lit8 v2, v12, -0x7

    new-array v10, v2, [B

    .line 547
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mInput:Ljava/io/InputStream;

    invoke-virtual {v2, v10}, Ljava/io/InputStream;->read([B)I

    move-result v9

    .line 548
    .local v9, "bytesReceived":I
    :goto_2
    add-int/lit8 v2, v12, -0x7

    if-eq v9, v2, :cond_c

    .line 549
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mInput:Ljava/io/InputStream;

    array-length v3, v10

    sub-int/2addr v3, v9

    invoke-virtual {v2, v10, v9, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    add-int/2addr v9, v2

    goto :goto_2

    .line 553
    .end local v9    # "bytesReceived":I
    :cond_9
    const/4 v2, 0x1

    .line 604
    .end local v10    # "data":[B
    .end local v11    # "flags":I
    .end local v12    # "length":I
    .end local v16    # "version":I
    :goto_3
    return v2

    .line 556
    .restart local v10    # "data":[B
    .restart local v12    # "length":I
    :cond_a
    add-int/lit8 v2, v12, -0x3

    new-array v10, v2, [B

    .line 557
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mInput:Ljava/io/InputStream;

    invoke-virtual {v2, v10}, Ljava/io/InputStream;->read([B)I

    move-result v9

    .line 559
    .restart local v9    # "bytesReceived":I
    :goto_4
    add-int/lit8 v2, v12, -0x3

    if-eq v9, v2, :cond_b

    .line 560
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mInput:Ljava/io/InputStream;

    array-length v3, v10

    sub-int/2addr v3, v9

    invoke-virtual {v2, v10, v9, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    add-int/2addr v9, v2

    goto :goto_4

    .line 562
    :cond_b
    const/16 v2, 0xff

    move/from16 v0, p1

    if-ne v0, v2, :cond_c

    .line 563
    const/4 v2, 0x1

    goto :goto_3

    .line 567
    :cond_c
    move-object/from16 v0, p3

    invoke-static {v0, v10}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->updateHeaderSet(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;[B)[B

    move-result-object v8

    .line 568
    .local v8, "body":[B
    if-eqz p4, :cond_d

    if-eqz v8, :cond_d

    .line 569
    const/4 v2, 0x1

    move-object/from16 v0, p4

    invoke-virtual {v0, v8, v2}, Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;->writeBytes([BI)V

    .line 572
    :cond_d
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mConnectionID:[B

    if-eqz v2, :cond_e

    .line 573
    const/4 v2, 0x4

    new-array v2, v2, [B

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mConnectionId:[B

    .line 574
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mConnectionID:[B

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mConnectionId:[B

    const/4 v6, 0x0

    const/4 v7, 0x4

    invoke-static {v2, v3, v5, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 577
    :cond_e
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    if-eqz v2, :cond_f

    .line 578
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->handleAuthResp([B)Z

    move-result v2

    if-nez v2, :cond_f

    .line 579
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->setRequestInactive()V

    .line 580
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Authentication Failed"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 584
    :cond_f
    move-object/from16 v0, p3

    iget v2, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->responseCode:I

    const/16 v3, 0xc1

    if-ne v2, v3, :cond_10

    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthChall:[B

    if-eqz v2, :cond_10

    .line 587
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->handleAuthChall(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 588
    const/16 v2, 0x4e

    invoke-virtual {v13, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 589
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    array-length v2, v2

    add-int/lit8 v2, v2, 0x3

    shr-int/lit8 v2, v2, 0x8

    int-to-byte v2, v2

    invoke-virtual {v13, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 590
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    array-length v2, v2

    add-int/lit8 v2, v2, 0x3

    int-to-byte v2, v2

    invoke-virtual {v13, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 591
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    invoke-virtual {v13, v2}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 592
    const/4 v2, 0x0

    move-object/from16 v0, p3

    iput-object v2, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthChall:[B

    .line 593
    const/4 v2, 0x0

    move-object/from16 v0, p3

    iput-object v2, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    .line 595
    invoke-virtual {v13}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x3

    new-array v4, v2, [B

    .line 596
    .local v4, "sendHeaders":[B
    invoke-virtual {v13}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    const/4 v3, 0x3

    const/4 v5, 0x0

    array-length v6, v4

    invoke-static {v2, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 598
    const/4 v7, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    invoke-virtual/range {v2 .. v7}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->sendRequest(I[BLcom/navdy/hud/app/bluetooth/obex/HeaderSet;Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;Z)Z

    move-result v2

    goto/16 :goto_3

    .line 604
    .end local v4    # "sendHeaders":[B
    .end local v8    # "body":[B
    .end local v9    # "bytesReceived":I
    .end local v10    # "data":[B
    .end local v12    # "length":I
    :cond_10
    const/4 v2, 0x1

    goto/16 :goto_3
.end method

.method public setAuthenticator(Lcom/navdy/hud/app/bluetooth/obex/Authenticator;)V
    .locals 2
    .param p1, "auth"    # Lcom/navdy/hud/app/bluetooth/obex/Authenticator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 326
    if-nez p1, :cond_0

    .line 327
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Authenticator may not be null"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 329
    :cond_0
    iput-object p1, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mAuthenticator:Lcom/navdy/hud/app/bluetooth/obex/Authenticator;

    .line 330
    return-void
.end method

.method public setConnectionID(J)V
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 209
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const-wide v0, 0xffffffffL

    cmp-long v0, p1, v0

    if-lez v0, :cond_1

    .line 210
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Connection ID is not in a valid range"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 212
    :cond_1
    invoke-static {p1, p2}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->convertToByteArray(J)[B

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mConnectionId:[B

    .line 213
    return-void
.end method

.method public setPath(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;ZZ)Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    .locals 11
    .param p1, "header"    # Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    .param p2, "backup"    # Z
    .param p3, "create"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x4

    const/16 v4, 0x10

    const/4 v5, 0x0

    .line 333
    iget-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mObexConnected:Z

    if-nez v0, :cond_0

    .line 334
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Not connected to the server"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 336
    :cond_0
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->setRequestActive()V

    .line 337
    invoke-virtual {p0}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->ensureOpen()V

    .line 339
    const/4 v9, 0x2

    .line 340
    .local v9, "totalLength":I
    const/4 v7, 0x0

    .line 342
    .local v7, "head":[B
    if-nez p1, :cond_4

    .line 343
    new-instance v8, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    invoke-direct {v8}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;-><init>()V

    .line 353
    .local v8, "headset":Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    :cond_1
    :goto_0
    iget-object v0, v8, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->nonce:[B

    if-eqz v0, :cond_2

    .line 354
    new-array v0, v4, [B

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mChallengeDigest:[B

    .line 355
    iget-object v0, v8, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->nonce:[B

    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mChallengeDigest:[B

    invoke-static {v0, v5, v1, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 359
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mConnectionId:[B

    if-eqz v0, :cond_3

    .line 360
    new-array v0, v10, [B

    iput-object v0, v8, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mConnectionID:[B

    .line 361
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mConnectionId:[B

    iget-object v1, v8, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mConnectionID:[B

    invoke-static {v0, v5, v1, v5, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 364
    :cond_3
    invoke-static {v8, v5}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->createHeader(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;Z)[B

    move-result-object v7

    .line 365
    array-length v0, v7

    add-int/2addr v9, v0

    .line 367
    iget v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mMaxTxPacketSize:I

    if-le v9, v0, :cond_5

    .line 368
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Packet size exceeds max packet size"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 345
    .end local v8    # "headset":Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    :cond_4
    move-object v8, p1

    .line 346
    .restart local v8    # "headset":Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    iget-object v0, v8, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->nonce:[B

    if-eqz v0, :cond_1

    .line 347
    new-array v0, v4, [B

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mChallengeDigest:[B

    .line 348
    iget-object v0, v8, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->nonce:[B

    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mChallengeDigest:[B

    invoke-static {v0, v5, v1, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    .line 371
    :cond_5
    const/4 v6, 0x0

    .line 375
    .local v6, "flags":I
    if-eqz p2, :cond_6

    .line 376
    add-int/lit8 v6, v6, 0x1

    .line 381
    :cond_6
    if-nez p3, :cond_7

    .line 382
    or-int/lit8 v6, v6, 0x2

    .line 393
    :cond_7
    new-array v2, v9, [B

    .line 394
    .local v2, "packet":[B
    int-to-byte v0, v6

    aput-byte v0, v2, v5

    .line 395
    const/4 v0, 0x1

    aput-byte v5, v2, v0

    .line 396
    if-eqz v8, :cond_8

    .line 397
    const/4 v0, 0x2

    array-length v1, v7

    invoke-static {v7, v5, v2, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 400
    :cond_8
    new-instance v3, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    invoke-direct {v3}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;-><init>()V

    .line 401
    .local v3, "returnHeaderSet":Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    const/16 v1, 0x85

    const/4 v4, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->sendRequest(I[BLcom/navdy/hud/app/bluetooth/obex/HeaderSet;Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;Z)Z

    .line 410
    invoke-virtual {p0}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->setRequestInactive()V

    .line 412
    return-object v3
.end method

.method declared-synchronized setRequestInactive()V
    .locals 1

    .prologue
    .line 430
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->mRequestActive:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 431
    monitor-exit p0

    return-void

    .line 430
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
