.class public final Lcom/navdy/hud/app/bluetooth/obex/ServerSession;
.super Lcom/navdy/hud/app/bluetooth/obex/ObexSession;
.source "ServerSession.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static final TAG:Ljava/lang/String; = "Obex ServerSession"

.field private static final V:Z


# instance fields
.field private mClosed:Z

.field private mInput:Ljava/io/InputStream;

.field private mListener:Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;

.field private mMaxPacketLength:I

.field private mOutput:Ljava/io/OutputStream;

.field private mProcessThread:Ljava/lang/Thread;

.field private mTransport:Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;


# direct methods
.method public constructor <init>(Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;Lcom/navdy/hud/app/bluetooth/obex/Authenticator;)V
    .locals 1
    .param p1, "trans"    # Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;
    .param p2, "handler"    # Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;
    .param p3, "auth"    # Lcom/navdy/hud/app/bluetooth/obex/Authenticator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/obex/ObexSession;-><init>()V

    .line 76
    iput-object p3, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mAuthenticator:Lcom/navdy/hud/app/bluetooth/obex/Authenticator;

    .line 77
    iput-object p1, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mTransport:Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;

    .line 78
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mTransport:Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;

    invoke-interface {v0}, Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;->openInputStream()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mInput:Ljava/io/InputStream;

    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mTransport:Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;

    invoke-interface {v0}, Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;->openOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mOutput:Ljava/io/OutputStream;

    .line 80
    iput-object p2, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mListener:Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;

    .line 81
    const/16 v0, 0x100

    iput v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mMaxPacketLength:I

    .line 83
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mClosed:Z

    .line 84
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mProcessThread:Ljava/lang/Thread;

    .line 85
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mProcessThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 86
    return-void
.end method

.method private handleAbortRequest()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 165
    const/16 v0, 0xa0

    .line 166
    .local v0, "code":I
    new-instance v4, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    invoke-direct {v4}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;-><init>()V

    .line 167
    .local v4, "request":Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    new-instance v3, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    invoke-direct {v3}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;-><init>()V

    .line 169
    .local v3, "reply":Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    iget-object v5, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mInput:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->read()I

    move-result v2

    .line 170
    .local v2, "length":I
    shl-int/lit8 v5, v2, 0x8

    iget-object v6, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mInput:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->read()I

    move-result v6

    add-int v2, v5, v6

    .line 171
    iget-object v5, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mTransport:Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;

    invoke-static {v5}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->getMaxRxPacketSize(Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;)I

    move-result v5

    if-le v2, v5, :cond_0

    .line 172
    const/16 v0, 0xce

    .line 181
    :goto_0
    const/4 v5, 0x0

    invoke-virtual {p0, v0, v5}, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->sendResponse(I[B)V

    .line 182
    return-void

    .line 174
    :cond_0
    const/4 v1, 0x3

    .local v1, "i":I
    :goto_1
    if-ge v1, v2, :cond_1

    .line 175
    iget-object v5, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mInput:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->read()I

    .line 174
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 177
    :cond_1
    iget-object v5, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mListener:Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;

    invoke-virtual {v5, v4, v3}, Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;->onAbort(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;)I

    move-result v0

    .line 178
    const-string v5, "Obex ServerSession"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onAbort request handler return value- "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->validateResponseCode(I)I

    move-result v0

    goto :goto_0
.end method

.method private handleConnectRequest()V
    .locals 25
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 536
    const/16 v18, 0x7

    .line 537
    .local v18, "totalLength":I
    const/4 v8, 0x0

    .line 538
    .local v8, "head":[B
    const/4 v5, -0x1

    .line 539
    .local v5, "code":I
    new-instance v16, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    invoke-direct/range {v16 .. v16}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;-><init>()V

    .line 540
    .local v16, "request":Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    new-instance v15, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    invoke-direct {v15}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;-><init>()V

    .line 549
    .local v15, "reply":Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mInput:Ljava/io/InputStream;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/io/InputStream;->read()I

    move-result v14

    .line 550
    .local v14, "packetLength":I
    shl-int/lit8 v20, v14, 0x8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mInput:Ljava/io/InputStream;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/io/InputStream;->read()I

    move-result v21

    add-int v14, v20, v21

    .line 553
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mInput:Ljava/io/InputStream;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/io/InputStream;->read()I

    move-result v19

    .line 554
    .local v19, "version":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mInput:Ljava/io/InputStream;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/io/InputStream;->read()I

    move-result v7

    .line 555
    .local v7, "flags":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mInput:Ljava/io/InputStream;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/io/InputStream;->read()I

    move-result v20

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mMaxPacketLength:I

    .line 556
    move-object/from16 v0, p0

    iget v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mMaxPacketLength:I

    move/from16 v20, v0

    shl-int/lit8 v20, v20, 0x8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mInput:Ljava/io/InputStream;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/io/InputStream;->read()I

    move-result v21

    add-int v20, v20, v21

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mMaxPacketLength:I

    .line 562
    move-object/from16 v0, p0

    iget v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mMaxPacketLength:I

    move/from16 v20, v0

    const v21, 0xfffe

    move/from16 v0, v20

    move/from16 v1, v21

    if-le v0, v1, :cond_0

    .line 563
    const v20, 0xfffe

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mMaxPacketLength:I

    .line 566
    :cond_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mMaxPacketLength:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mTransport:Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->getMaxTxPacketSize(Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;)I

    move-result v21

    move/from16 v0, v20

    move/from16 v1, v21

    if-le v0, v1, :cond_1

    .line 567
    const-string v20, "Obex ServerSession"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Requested MaxObexPacketSize "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mMaxPacketLength:I

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " is larger than the max size supported by the transport: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mTransport:Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;

    move-object/from16 v22, v0

    .line 569
    invoke-static/range {v22 .. v22}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->getMaxTxPacketSize(Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;)I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " Reducing to this size."

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 567
    invoke-static/range {v20 .. v21}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 571
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mTransport:Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->getMaxTxPacketSize(Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;)I

    move-result v20

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mMaxPacketLength:I

    .line 574
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mTransport:Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->getMaxRxPacketSize(Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;)I

    move-result v20

    move/from16 v0, v20

    if-le v14, v0, :cond_4

    .line 575
    const/16 v5, 0xce

    .line 576
    const/16 v18, 0x7

    .line 651
    :cond_2
    :goto_0
    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->convertToByteArray(J)[B

    move-result-object v12

    .line 659
    .local v12, "length":[B
    move/from16 v0, v18

    new-array v0, v0, [B

    move-object/from16 v17, v0

    .line 660
    .local v17, "sendData":[B
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mTransport:Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->getMaxRxPacketSize(Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;)I

    move-result v13

    .line 661
    .local v13, "maxRxLength":I
    const/16 v20, 0x0

    int-to-byte v0, v5

    move/from16 v21, v0

    aput-byte v21, v17, v20

    .line 662
    const/16 v20, 0x1

    const/16 v21, 0x2

    aget-byte v21, v12, v21

    aput-byte v21, v17, v20

    .line 663
    const/16 v20, 0x2

    const/16 v21, 0x3

    aget-byte v21, v12, v21

    aput-byte v21, v17, v20

    .line 664
    const/16 v20, 0x3

    const/16 v21, 0x10

    aput-byte v21, v17, v20

    .line 665
    const/16 v20, 0x4

    const/16 v21, 0x0

    aput-byte v21, v17, v20

    .line 666
    const/16 v20, 0x5

    shr-int/lit8 v21, v13, 0x8

    move/from16 v0, v21

    int-to-byte v0, v0

    move/from16 v21, v0

    aput-byte v21, v17, v20

    .line 667
    const/16 v20, 0x6

    and-int/lit16 v0, v13, 0xff

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-byte v0, v0

    move/from16 v21, v0

    aput-byte v21, v17, v20

    .line 669
    if-eqz v8, :cond_3

    .line 670
    const/16 v20, 0x0

    const/16 v21, 0x7

    array-length v0, v8

    move/from16 v22, v0

    move/from16 v0, v20

    move-object/from16 v1, v17

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-static {v8, v0, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 673
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mOutput:Ljava/io/OutputStream;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    .line 674
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mOutput:Ljava/io/OutputStream;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/io/OutputStream;->flush()V

    .line 675
    return-void

    .line 578
    .end local v12    # "length":[B
    .end local v13    # "maxRxLength":I
    .end local v17    # "sendData":[B
    :cond_4
    const/16 v20, 0x7

    move/from16 v0, v20

    if-le v14, v0, :cond_6

    .line 579
    add-int/lit8 v20, v14, -0x7

    move/from16 v0, v20

    new-array v9, v0, [B

    .line 580
    .local v9, "headers":[B
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mInput:Ljava/io/InputStream;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Ljava/io/InputStream;->read([B)I

    move-result v4

    .line 582
    .local v4, "bytesReceived":I
    :goto_1
    array-length v0, v9

    move/from16 v20, v0

    move/from16 v0, v20

    if-eq v4, v0, :cond_5

    .line 583
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mInput:Ljava/io/InputStream;

    move-object/from16 v20, v0

    array-length v0, v9

    move/from16 v21, v0

    sub-int v21, v21, v4

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v0, v9, v4, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v20

    add-int v4, v4, v20

    goto :goto_1

    .line 587
    :cond_5
    move-object/from16 v0, v16

    invoke-static {v0, v9}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->updateHeaderSet(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;[B)[B

    .line 590
    .end local v4    # "bytesReceived":I
    .end local v9    # "headers":[B
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mListener:Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;->getConnectionId()J

    move-result-wide v20

    const-wide/16 v22, -0x1

    cmp-long v20, v20, v22

    if-eqz v20, :cond_a

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mConnectionID:[B

    move-object/from16 v20, v0

    if-eqz v20, :cond_a

    .line 591
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mListener:Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;

    move-object/from16 v20, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mConnectionID:[B

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->convertToLong([B)J

    move-result-wide v22

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;->setConnectionId(J)V

    .line 596
    :goto_2
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    move-object/from16 v20, v0

    if-eqz v20, :cond_8

    .line 597
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->handleAuthResp([B)Z

    move-result v20

    if-nez v20, :cond_7

    .line 598
    const/16 v5, 0xc1

    .line 599
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mListener:Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    move-object/from16 v22, v0

    invoke-static/range {v21 .. v22}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->getTagValue(B[B)[B

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;->onAuthenticationFailure([B)V

    .line 602
    :cond_7
    const/16 v20, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    .line 605
    :cond_8
    const/16 v20, 0xc1

    move/from16 v0, v20

    if-eq v5, v0, :cond_2

    .line 606
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthChall:[B

    move-object/from16 v20, v0

    if-eqz v20, :cond_9

    .line 607
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->handleAuthChall(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;)Z

    .line 608
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    new-array v0, v0, [B

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iput-object v0, v15, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    .line 609
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    move-object/from16 v20, v0

    const/16 v21, 0x0

    iget-object v0, v15, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    move-object/from16 v22, v0

    const/16 v23, 0x0

    iget-object v0, v15, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    array-length v0, v0

    move/from16 v24, v0

    invoke-static/range {v20 .. v24}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 611
    const/16 v20, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthChall:[B

    .line 612
    const/16 v20, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    .line 616
    :cond_9
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mListener:Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1, v15}, Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;->onConnect(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;)I

    move-result v5

    .line 617
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->validateResponseCode(I)I

    move-result v5

    .line 619
    iget-object v0, v15, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->nonce:[B

    move-object/from16 v20, v0

    if-eqz v20, :cond_b

    .line 620
    const/16 v20, 0x10

    move/from16 v0, v20

    new-array v0, v0, [B

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mChallengeDigest:[B

    .line 621
    iget-object v0, v15, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->nonce:[B

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mChallengeDigest:[B

    move-object/from16 v22, v0

    const/16 v23, 0x0

    const/16 v24, 0x10

    invoke-static/range {v20 .. v24}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 625
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mListener:Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;->getConnectionId()J

    move-result-wide v10

    .line 626
    .local v10, "id":J
    const-wide/16 v20, -0x1

    cmp-long v20, v10, v20

    if-nez v20, :cond_c

    .line 627
    const/16 v20, 0x0

    move-object/from16 v0, v20

    iput-object v0, v15, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mConnectionID:[B

    .line 632
    :goto_4
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-static {v15, v0}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->createHeader(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;Z)[B

    move-result-object v8

    .line 633
    array-length v0, v8

    move/from16 v20, v0

    add-int v18, v18, v20

    .line 635
    move-object/from16 v0, p0

    iget v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mMaxPacketLength:I

    move/from16 v20, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move/from16 v0, v18

    move/from16 v1, v20

    if-le v0, v1, :cond_2

    .line 636
    const/16 v18, 0x7

    .line 637
    const/4 v8, 0x0

    .line 638
    const/16 v5, 0xd0

    goto/16 :goto_0

    .line 593
    .end local v10    # "id":J
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mListener:Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;

    move-object/from16 v20, v0

    const-wide/16 v22, 0x1

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;->setConnectionId(J)V

    goto/16 :goto_2

    .line 623
    :cond_b
    const/16 v20, 0x0

    :try_start_1
    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mChallengeDigest:[B

    goto :goto_3

    .line 640
    :catch_0
    move-exception v6

    .line 642
    .local v6, "e":Ljava/lang/Exception;
    const/16 v18, 0x7

    .line 643
    const/4 v8, 0x0

    .line 644
    const/16 v5, 0xd0

    goto/16 :goto_0

    .line 629
    .end local v6    # "e":Ljava/lang/Exception;
    .restart local v10    # "id":J
    :cond_c
    invoke-static {v10, v11}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->convertToByteArray(J)[B

    move-result-object v20

    move-object/from16 v0, v20

    iput-object v0, v15, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mConnectionID:[B
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4
.end method

.method private handleDisconnectRequest()V
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 426
    const/16 v3, 0xa0

    .line 427
    .local v3, "code":I
    const/4 v13, 0x3

    .line 428
    .local v13, "totalLength":I
    const/4 v5, 0x0

    .line 430
    .local v5, "head":[B
    new-instance v12, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    invoke-direct {v12}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;-><init>()V

    .line 431
    .local v12, "request":Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    new-instance v10, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    invoke-direct {v10}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;-><init>()V

    .line 433
    .local v10, "reply":Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mInput:Ljava/io/InputStream;

    invoke-virtual {v14}, Ljava/io/InputStream;->read()I

    move-result v7

    .line 434
    .local v7, "length":I
    shl-int/lit8 v14, v7, 0x8

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mInput:Ljava/io/InputStream;

    invoke-virtual {v15}, Ljava/io/InputStream;->read()I

    move-result v15

    add-int v7, v14, v15

    .line 436
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mTransport:Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;

    invoke-static {v14}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->getMaxRxPacketSize(Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;)I

    move-result v14

    if-le v7, v14, :cond_2

    .line 437
    const/16 v3, 0xce

    .line 438
    const/4 v13, 0x3

    .line 502
    :cond_0
    :goto_0
    if-eqz v5, :cond_a

    .line 503
    array-length v14, v5

    add-int/lit8 v14, v14, 0x3

    new-array v11, v14, [B

    .line 507
    .local v11, "replyData":[B
    :goto_1
    const/4 v14, 0x0

    int-to-byte v15, v3

    aput-byte v15, v11, v14

    .line 508
    const/4 v14, 0x1

    shr-int/lit8 v15, v13, 0x8

    int-to-byte v15, v15

    aput-byte v15, v11, v14

    .line 509
    const/4 v14, 0x2

    int-to-byte v15, v13

    aput-byte v15, v11, v14

    .line 510
    if-eqz v5, :cond_1

    .line 511
    const/4 v14, 0x0

    const/4 v15, 0x3

    array-length v0, v5

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-static {v5, v14, v11, v15, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 517
    :cond_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mOutput:Ljava/io/OutputStream;

    invoke-virtual {v14, v11}, Ljava/io/OutputStream;->write([B)V

    .line 518
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mOutput:Ljava/io/OutputStream;

    invoke-virtual {v14}, Ljava/io/OutputStream;->flush()V

    .line 519
    .end local v11    # "replyData":[B
    :goto_2
    return-void

    .line 440
    :cond_2
    const/4 v14, 0x3

    if-le v7, v14, :cond_4

    .line 441
    add-int/lit8 v14, v7, -0x3

    new-array v6, v14, [B

    .line 442
    .local v6, "headers":[B
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mInput:Ljava/io/InputStream;

    invoke-virtual {v14, v6}, Ljava/io/InputStream;->read([B)I

    move-result v2

    .line 444
    .local v2, "bytesReceived":I
    :goto_3
    array-length v14, v6

    if-eq v2, v14, :cond_3

    .line 445
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mInput:Ljava/io/InputStream;

    array-length v15, v6

    sub-int/2addr v15, v2

    invoke-virtual {v14, v6, v2, v15}, Ljava/io/InputStream;->read([BII)I

    move-result v14

    add-int/2addr v2, v14

    goto :goto_3

    .line 449
    :cond_3
    invoke-static {v12, v6}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->updateHeaderSet(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;[B)[B

    .line 452
    .end local v2    # "bytesReceived":I
    .end local v6    # "headers":[B
    :cond_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mListener:Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;

    invoke-virtual {v14}, Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;->getConnectionId()J

    move-result-wide v14

    const-wide/16 v16, -0x1

    cmp-long v14, v14, v16

    if-eqz v14, :cond_8

    iget-object v14, v12, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mConnectionID:[B

    if-eqz v14, :cond_8

    .line 453
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mListener:Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;

    iget-object v15, v12, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mConnectionID:[B

    invoke-static {v15}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->convertToLong([B)J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;->setConnectionId(J)V

    .line 458
    :goto_4
    iget-object v14, v12, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    if-eqz v14, :cond_6

    .line 459
    iget-object v14, v12, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->handleAuthResp([B)Z

    move-result v14

    if-nez v14, :cond_5

    .line 460
    const/16 v3, 0xc1

    .line 461
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mListener:Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;

    const/4 v15, 0x1

    iget-object v0, v12, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    move-object/from16 v16, v0

    invoke-static/range {v15 .. v16}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->getTagValue(B[B)[B

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;->onAuthenticationFailure([B)V

    .line 464
    :cond_5
    const/4 v14, 0x0

    iput-object v14, v12, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    .line 467
    :cond_6
    const/16 v14, 0xc1

    if-eq v3, v14, :cond_0

    .line 469
    iget-object v14, v12, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthChall:[B

    if-eqz v14, :cond_7

    .line 470
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->handleAuthChall(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;)Z

    .line 471
    const/4 v14, 0x0

    iput-object v14, v12, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthChall:[B

    .line 475
    :cond_7
    :try_start_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mListener:Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;

    invoke-virtual {v14, v12, v10}, Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;->onDisconnect(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 482
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mListener:Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;

    invoke-virtual {v14}, Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;->getConnectionId()J

    move-result-wide v8

    .line 483
    .local v8, "id":J
    const-wide/16 v14, -0x1

    cmp-long v14, v8, v14

    if-nez v14, :cond_9

    .line 484
    const/4 v14, 0x0

    iput-object v14, v10, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mConnectionID:[B

    .line 489
    :goto_5
    const/4 v14, 0x0

    invoke-static {v10, v14}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->createHeader(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;Z)[B

    move-result-object v5

    .line 490
    array-length v14, v5

    add-int/2addr v13, v14

    .line 492
    move-object/from16 v0, p0

    iget v14, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mMaxPacketLength:I

    if-le v13, v14, :cond_0

    .line 493
    const/4 v13, 0x3

    .line 494
    const/4 v5, 0x0

    .line 495
    const/16 v3, 0xd0

    goto/16 :goto_0

    .line 455
    .end local v8    # "id":J
    :cond_8
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mListener:Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;

    const-wide/16 v16, 0x1

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;->setConnectionId(J)V

    goto :goto_4

    .line 476
    :catch_0
    move-exception v4

    .line 478
    .local v4, "e":Ljava/lang/Exception;
    const/16 v14, 0xd0

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->sendResponse(I[B)V

    goto/16 :goto_2

    .line 486
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v8    # "id":J
    :cond_9
    invoke-static {v8, v9}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->convertToByteArray(J)[B

    move-result-object v14

    iput-object v14, v10, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mConnectionID:[B

    goto :goto_5

    .line 505
    .end local v8    # "id":J
    :cond_a
    const/4 v14, 0x3

    new-array v11, v14, [B

    .restart local v11    # "replyData":[B
    goto/16 :goto_1
.end method

.method private handleGetRequest(I)V
    .locals 8
    .param p1, "type"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 244
    new-instance v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;

    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mInput:Ljava/io/InputStream;

    iget v4, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mMaxPacketLength:I

    iget-object v5, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mListener:Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;

    move-object v1, p0

    move v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;-><init>(Lcom/navdy/hud/app/bluetooth/obex/ServerSession;Ljava/io/InputStream;IILcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;)V

    .line 246
    .local v0, "op":Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mListener:Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;->onGet(Lcom/navdy/hud/app/bluetooth/obex/Operation;)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->validateResponseCode(I)I

    move-result v7

    .line 248
    .local v7, "response":I
    iget-boolean v1, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->isAborted:Z

    if-nez v1, :cond_0

    .line 249
    invoke-virtual {v0, v7}, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->sendReply(I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 255
    .end local v7    # "response":I
    :cond_0
    :goto_0
    return-void

    .line 251
    :catch_0
    move-exception v6

    .line 253
    .local v6, "e":Ljava/lang/Exception;
    const/16 v1, 0xd0

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->sendResponse(I[B)V

    goto :goto_0
.end method

.method private handlePutRequest(I)V
    .locals 8
    .param p1, "type"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 198
    new-instance v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;

    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mInput:Ljava/io/InputStream;

    iget v4, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mMaxPacketLength:I

    iget-object v5, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mListener:Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;

    move-object v1, p0

    move v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;-><init>(Lcom/navdy/hud/app/bluetooth/obex/ServerSession;Ljava/io/InputStream;IILcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;)V

    .line 200
    .local v0, "op":Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;
    const/4 v7, -0x1

    .line 202
    .local v7, "response":I
    :try_start_0
    iget-boolean v1, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->finalBitSet:Z

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->isValidBody()Z

    move-result v1

    if-nez v1, :cond_1

    .line 203
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mListener:Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;

    iget-object v2, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->requestHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget-object v3, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->replyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    .line 204
    invoke-virtual {v1, v2, v3}, Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;->onDelete(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;)I

    move-result v1

    .line 203
    invoke-direct {p0, v1}, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->validateResponseCode(I)I

    move-result v7

    .line 208
    :goto_0
    const/16 v1, 0xa0

    if-eq v7, v1, :cond_2

    iget-boolean v1, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->isAborted:Z

    if-nez v1, :cond_2

    .line 209
    invoke-virtual {v0, v7}, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->sendReply(I)Z

    .line 228
    :cond_0
    :goto_1
    return-void

    .line 206
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mListener:Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;->onPut(Lcom/navdy/hud/app/bluetooth/obex/Operation;)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->validateResponseCode(I)I

    move-result v7

    goto :goto_0

    .line 210
    :cond_2
    iget-boolean v1, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->isAborted:Z

    if-nez v1, :cond_0

    .line 212
    :goto_2
    iget-boolean v1, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->finalBitSet:Z

    if-nez v1, :cond_3

    .line 213
    const/16 v1, 0x90

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->sendReply(I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 217
    :catch_0
    move-exception v6

    .line 224
    .local v6, "e":Ljava/lang/Exception;
    iget-boolean v1, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->isAborted:Z

    if-nez v1, :cond_0

    .line 225
    const/16 v1, 0xd0

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->sendResponse(I[B)V

    goto :goto_1

    .line 215
    .end local v6    # "e":Ljava/lang/Exception;
    :cond_3
    :try_start_1
    invoke-virtual {v0, v7}, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->sendReply(I)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method private handleSetPathRequest()V
    .locals 25
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 302
    const/16 v19, 0x3

    .line 303
    .local v19, "totalLength":I
    const/4 v11, 0x0

    .line 304
    .local v11, "head":[B
    const/4 v6, -0x1

    .line 306
    .local v6, "code":I
    new-instance v18, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    invoke-direct/range {v18 .. v18}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;-><init>()V

    .line 307
    .local v18, "request":Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    new-instance v16, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    invoke-direct/range {v16 .. v16}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;-><init>()V

    .line 309
    .local v16, "reply":Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mInput:Ljava/io/InputStream;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/io/InputStream;->read()I

    move-result v13

    .line 310
    .local v13, "length":I
    shl-int/lit8 v20, v13, 0x8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mInput:Ljava/io/InputStream;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/io/InputStream;->read()I

    move-result v21

    add-int v13, v20, v21

    .line 311
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mInput:Ljava/io/InputStream;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/io/InputStream;->read()I

    move-result v10

    .line 312
    .local v10, "flags":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mInput:Ljava/io/InputStream;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/io/InputStream;->read()I

    move-result v7

    .line 314
    .local v7, "constants":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mTransport:Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->getMaxRxPacketSize(Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;)I

    move-result v20

    move/from16 v0, v20

    if-le v13, v0, :cond_2

    .line 315
    const/16 v6, 0xce

    .line 316
    const/16 v19, 0x3

    .line 401
    :cond_0
    :goto_0
    move/from16 v0, v19

    new-array v0, v0, [B

    move-object/from16 v17, v0

    .line 402
    .local v17, "replyData":[B
    const/16 v20, 0x0

    int-to-byte v0, v6

    move/from16 v21, v0

    aput-byte v21, v17, v20

    .line 403
    const/16 v20, 0x1

    shr-int/lit8 v21, v19, 0x8

    move/from16 v0, v21

    int-to-byte v0, v0

    move/from16 v21, v0

    aput-byte v21, v17, v20

    .line 404
    const/16 v20, 0x2

    move/from16 v0, v19

    int-to-byte v0, v0

    move/from16 v21, v0

    aput-byte v21, v17, v20

    .line 405
    if-eqz v11, :cond_1

    .line 406
    const/16 v20, 0x0

    const/16 v21, 0x3

    array-length v0, v11

    move/from16 v22, v0

    move/from16 v0, v20

    move-object/from16 v1, v17

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-static {v11, v0, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 412
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mOutput:Ljava/io/OutputStream;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    .line 413
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mOutput:Ljava/io/OutputStream;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/io/OutputStream;->flush()V

    .line 414
    .end local v17    # "replyData":[B
    :goto_1
    return-void

    .line 318
    :cond_2
    const/16 v20, 0x5

    move/from16 v0, v20

    if-le v13, v0, :cond_5

    .line 319
    add-int/lit8 v20, v13, -0x5

    move/from16 v0, v20

    new-array v12, v0, [B

    .line 320
    .local v12, "headers":[B
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mInput:Ljava/io/InputStream;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Ljava/io/InputStream;->read([B)I

    move-result v5

    .line 322
    .local v5, "bytesReceived":I
    :goto_2
    array-length v0, v12

    move/from16 v20, v0

    move/from16 v0, v20

    if-eq v5, v0, :cond_3

    .line 323
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mInput:Ljava/io/InputStream;

    move-object/from16 v20, v0

    array-length v0, v12

    move/from16 v21, v0

    sub-int v21, v21, v5

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v0, v12, v5, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v20

    add-int v5, v5, v20

    goto :goto_2

    .line 327
    :cond_3
    move-object/from16 v0, v18

    invoke-static {v0, v12}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->updateHeaderSet(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;[B)[B

    .line 329
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mListener:Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;->getConnectionId()J

    move-result-wide v20

    const-wide/16 v22, -0x1

    cmp-long v20, v20, v22

    if-eqz v20, :cond_9

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mConnectionID:[B

    move-object/from16 v20, v0

    if-eqz v20, :cond_9

    .line 330
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mListener:Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;

    move-object/from16 v20, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mConnectionID:[B

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->convertToLong([B)J

    move-result-wide v22

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;->setConnectionId(J)V

    .line 335
    :goto_3
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    move-object/from16 v20, v0

    if-eqz v20, :cond_5

    .line 336
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->handleAuthResp([B)Z

    move-result v20

    if-nez v20, :cond_4

    .line 337
    const/16 v6, 0xc1

    .line 338
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mListener:Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    move-object/from16 v22, v0

    invoke-static/range {v21 .. v22}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->getTagValue(B[B)[B

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;->onAuthenticationFailure([B)V

    .line 341
    :cond_4
    const/16 v20, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    .line 345
    .end local v5    # "bytesReceived":I
    .end local v12    # "headers":[B
    :cond_5
    const/16 v20, 0xc1

    move/from16 v0, v20

    if-eq v6, v0, :cond_0

    .line 348
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthChall:[B

    move-object/from16 v20, v0

    if-eqz v20, :cond_6

    .line 349
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->handleAuthChall(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;)Z

    .line 350
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    new-array v0, v0, [B

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    .line 351
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    array-length v0, v0

    move/from16 v24, v0

    invoke-static/range {v20 .. v24}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 353
    const/16 v20, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthChall:[B

    .line 354
    const/16 v20, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    .line 356
    :cond_6
    const/4 v4, 0x0

    .line 357
    .local v4, "backup":Z
    const/4 v8, 0x1

    .line 358
    .local v8, "create":Z
    and-int/lit8 v20, v10, 0x1

    if-eqz v20, :cond_7

    .line 359
    const/4 v4, 0x1

    .line 361
    :cond_7
    and-int/lit8 v20, v10, 0x2

    if-eqz v20, :cond_8

    .line 362
    const/4 v8, 0x0

    .line 366
    :cond_8
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mListener:Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2, v4, v8}, Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;->onSetPath(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;ZZ)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    .line 373
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->validateResponseCode(I)I

    move-result v6

    .line 375
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->nonce:[B

    move-object/from16 v20, v0

    if-eqz v20, :cond_a

    .line 376
    const/16 v20, 0x10

    move/from16 v0, v20

    new-array v0, v0, [B

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mChallengeDigest:[B

    .line 377
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->nonce:[B

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mChallengeDigest:[B

    move-object/from16 v22, v0

    const/16 v23, 0x0

    const/16 v24, 0x10

    invoke-static/range {v20 .. v24}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 382
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mListener:Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;->getConnectionId()J

    move-result-wide v14

    .line 383
    .local v14, "id":J
    const-wide/16 v20, -0x1

    cmp-long v20, v14, v20

    if-nez v20, :cond_b

    .line 384
    const/16 v20, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mConnectionID:[B

    .line 389
    :goto_5
    const/16 v20, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-static {v0, v1}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->createHeader(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;Z)[B

    move-result-object v11

    .line 390
    array-length v0, v11

    move/from16 v20, v0

    add-int v19, v19, v20

    .line 392
    move-object/from16 v0, p0

    iget v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mMaxPacketLength:I

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    if-le v0, v1, :cond_0

    .line 393
    const/16 v19, 0x3

    .line 394
    const/4 v11, 0x0

    .line 395
    const/16 v6, 0xd0

    goto/16 :goto_0

    .line 332
    .end local v4    # "backup":Z
    .end local v8    # "create":Z
    .end local v14    # "id":J
    .restart local v5    # "bytesReceived":I
    .restart local v12    # "headers":[B
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mListener:Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;

    move-object/from16 v20, v0

    const-wide/16 v22, 0x1

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;->setConnectionId(J)V

    goto/16 :goto_3

    .line 367
    .end local v5    # "bytesReceived":I
    .end local v12    # "headers":[B
    .restart local v4    # "backup":Z
    .restart local v8    # "create":Z
    :catch_0
    move-exception v9

    .line 369
    .local v9, "e":Ljava/lang/Exception;
    const/16 v20, 0xd0

    const/16 v21, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->sendResponse(I[B)V

    goto/16 :goto_1

    .line 379
    .end local v9    # "e":Ljava/lang/Exception;
    :cond_a
    const/16 v20, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mChallengeDigest:[B

    goto :goto_4

    .line 386
    .restart local v14    # "id":J
    :cond_b
    invoke-static {v14, v15}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->convertToByteArray(J)[B

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mConnectionID:[B

    goto :goto_5
.end method

.method private validateResponseCode(I)I
    .locals 2
    .param p1, "code"    # I

    .prologue
    const/16 v0, 0xd0

    .line 713
    const/16 v1, 0xa0

    if-lt p1, v1, :cond_1

    const/16 v1, 0xa6

    if-gt p1, v1, :cond_1

    .line 732
    .end local p1    # "code":I
    :cond_0
    :goto_0
    return p1

    .line 716
    .restart local p1    # "code":I
    :cond_1
    const/16 v1, 0xb0

    if-lt p1, v1, :cond_2

    const/16 v1, 0xb5

    if-le p1, v1, :cond_0

    .line 720
    :cond_2
    const/16 v1, 0xc0

    if-lt p1, v1, :cond_3

    const/16 v1, 0xcf

    if-le p1, v1, :cond_0

    .line 724
    :cond_3
    if-lt p1, v0, :cond_4

    const/16 v1, 0xd5

    if-le p1, v1, :cond_0

    .line 728
    :cond_4
    const/16 v1, 0xe0

    if-lt p1, v1, :cond_5

    const/16 v1, 0xe1

    if-le p1, v1, :cond_0

    :cond_5
    move p1, v0

    .line 732
    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized close()V
    .locals 1

    .prologue
    .line 683
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mListener:Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;

    if-eqz v0, :cond_0

    .line 684
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mListener:Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;

    invoke-virtual {v0}, Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;->onClose()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 688
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mClosed:Z

    .line 689
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mInput:Ljava/io/InputStream;

    if-eqz v0, :cond_1

    .line 690
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mInput:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 691
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mOutput:Ljava/io/OutputStream;

    if-eqz v0, :cond_2

    .line 692
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mOutput:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 693
    :cond_2
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mTransport:Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;

    if-eqz v0, :cond_3

    .line 694
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mTransport:Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;

    invoke-interface {v0}, Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 698
    :cond_3
    :goto_0
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mTransport:Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;

    .line 699
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mInput:Ljava/io/InputStream;

    .line 700
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mOutput:Ljava/io/OutputStream;

    .line 701
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mListener:Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 702
    monitor-exit p0

    return-void

    .line 683
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 695
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public getTransport()Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;
    .locals 1

    .prologue
    .line 736
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mTransport:Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;

    return-object v0
.end method

.method public run()V
    .locals 7

    .prologue
    .line 95
    const/4 v0, 0x0

    .line 96
    .local v0, "done":Z
    :goto_0
    if-nez v0, :cond_0

    :try_start_0
    iget-boolean v5, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mClosed:Z

    if-nez v5, :cond_0

    .line 98
    iget-object v5, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mInput:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->read()I

    move-result v4

    .line 100
    .local v4, "requestType":I
    sparse-switch v4, :sswitch_data_0

    .line 138
    iget-object v5, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mInput:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->read()I

    move-result v3

    .line 139
    .local v3, "length":I
    shl-int/lit8 v5, v3, 0x8

    iget-object v6, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mInput:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->read()I

    move-result v6

    add-int v3, v5, v6

    .line 140
    const/4 v2, 0x3

    .local v2, "i":I
    :goto_1
    if-ge v2, v3, :cond_1

    .line 141
    iget-object v5, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mInput:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->read()I

    .line 140
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 102
    .end local v2    # "i":I
    .end local v3    # "length":I
    :sswitch_0
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->handleConnectRequest()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 147
    .end local v4    # "requestType":I
    :catch_0
    move-exception v1

    .line 148
    .local v1, "e":Ljava/lang/NullPointerException;
    const-string v5, "Obex ServerSession"

    const-string v6, "Exception occured - ignoring"

    invoke-static {v5, v6, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 152
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :cond_0
    :goto_2
    invoke-virtual {p0}, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->close()V

    .line 153
    return-void

    .line 106
    .restart local v4    # "requestType":I
    :sswitch_1
    :try_start_1
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->handleDisconnectRequest()V

    .line 107
    const/4 v0, 0x1

    .line 108
    goto :goto_0

    .line 112
    :sswitch_2
    invoke-direct {p0, v4}, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->handleGetRequest(I)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 149
    .end local v4    # "requestType":I
    :catch_1
    move-exception v1

    .line 150
    .local v1, "e":Ljava/lang/Exception;
    const-string v5, "Obex ServerSession"

    const-string v6, "Exception occured - ignoring"

    invoke-static {v5, v6, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 117
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v4    # "requestType":I
    :sswitch_3
    :try_start_2
    invoke-direct {p0, v4}, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->handlePutRequest(I)V

    goto :goto_0

    .line 121
    :sswitch_4
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->handleSetPathRequest()V

    goto :goto_0

    .line 124
    :sswitch_5
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->handleAbortRequest()V

    goto :goto_0

    .line 128
    :sswitch_6
    const/4 v0, 0x1

    .line 129
    goto :goto_0

    .line 143
    .restart local v2    # "i":I
    .restart local v3    # "length":I
    :cond_1
    const/16 v5, 0xd1

    const/4 v6, 0x0

    invoke-virtual {p0, v5, v6}, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->sendResponse(I[B)V
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 100
    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_6
        0x2 -> :sswitch_3
        0x3 -> :sswitch_2
        0x80 -> :sswitch_0
        0x81 -> :sswitch_1
        0x82 -> :sswitch_3
        0x83 -> :sswitch_2
        0x85 -> :sswitch_4
        0xff -> :sswitch_5
    .end sparse-switch
.end method

.method public sendResponse(I[B)V
    .locals 7
    .param p1, "code"    # I
    .param p2, "header"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 264
    const/4 v2, 0x3

    .line 265
    .local v2, "totalLength":I
    const/4 v0, 0x0

    .line 266
    .local v0, "data":[B
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->mOutput:Ljava/io/OutputStream;

    .line 267
    .local v1, "op":Ljava/io/OutputStream;
    if-nez v1, :cond_0

    .line 286
    :goto_0
    return-void

    .line 271
    :cond_0
    if-eqz p2, :cond_1

    .line 272
    array-length v3, p2

    add-int/2addr v2, v3

    .line 273
    new-array v0, v2, [B

    .line 274
    int-to-byte v3, p1

    aput-byte v3, v0, v5

    .line 275
    shr-int/lit8 v3, v2, 0x8

    int-to-byte v3, v3

    aput-byte v3, v0, v4

    .line 276
    int-to-byte v3, v2

    aput-byte v3, v0, v6

    .line 277
    const/4 v3, 0x3

    array-length v4, p2

    invoke-static {p2, v5, v0, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 284
    :goto_1
    invoke-virtual {v1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 285
    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V

    goto :goto_0

    .line 279
    :cond_1
    new-array v0, v2, [B

    .line 280
    int-to-byte v3, p1

    aput-byte v3, v0, v5

    .line 281
    aput-byte v5, v0, v4

    .line 282
    int-to-byte v3, v2

    aput-byte v3, v0, v6

    goto :goto_1
.end method
