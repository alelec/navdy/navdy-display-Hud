.class public final Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;
.super Ljava/lang/Object;
.source "ServerOperation.java"

# interfaces
.implements Lcom/navdy/hud/app/bluetooth/obex/Operation;
.implements Lcom/navdy/hud/app/bluetooth/obex/BaseStream;


# static fields
.field private static final TAG:Ljava/lang/String; = "ServerOperation"

.field private static final V:Z


# instance fields
.field public finalBitSet:Z

.field public isAborted:Z

.field private mClosed:Z

.field private mExceptionString:Ljava/lang/String;

.field private mGetOperation:Z

.field private mHasBody:Z

.field private mInput:Ljava/io/InputStream;

.field private mListener:Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;

.field private mMaxPacketLength:I

.field private mParent:Lcom/navdy/hud/app/bluetooth/obex/ServerSession;

.field private mPrivateInput:Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;

.field private mPrivateOutput:Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;

.field private mPrivateOutputOpen:Z

.field private mRequestFinished:Z

.field private mResponseSize:I

.field private mSendBodyHeader:Z

.field private mSrmActive:Z

.field private mSrmEnabled:Z

.field private mSrmLocalWait:Z

.field private mSrmResponseSent:Z

.field private mSrmWaitingForRemote:Z

.field private mTransport:Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;

.field public replyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

.field public requestHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;


# direct methods
.method public constructor <init>(Lcom/navdy/hud/app/bluetooth/obex/ServerSession;Ljava/io/InputStream;IILcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;)V
    .locals 7
    .param p1, "p"    # Lcom/navdy/hud/app/bluetooth/obex/ServerSession;
    .param p2, "in"    # Ljava/io/InputStream;
    .param p3, "request"    # I
    .param p4, "maxSize"    # I
    .param p5, "listen"    # Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v6, 0x83

    const/16 v5, 0x90

    const/4 v4, 0x3

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    iput-boolean v3, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mSendBodyHeader:Z

    .line 103
    iput-boolean v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mSrmEnabled:Z

    .line 105
    iput-boolean v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mSrmActive:Z

    .line 107
    iput-boolean v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mSrmResponseSent:Z

    .line 111
    iput-boolean v3, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mSrmWaitingForRemote:Z

    .line 113
    iput-boolean v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mSrmLocalWait:Z

    .line 128
    iput-boolean v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->isAborted:Z

    .line 129
    iput-object p1, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mParent:Lcom/navdy/hud/app/bluetooth/obex/ServerSession;

    .line 130
    iput-object p2, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mInput:Ljava/io/InputStream;

    .line 131
    iput p4, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mMaxPacketLength:I

    .line 132
    iput-boolean v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mClosed:Z

    .line 133
    new-instance v1, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    invoke-direct {v1}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->requestHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    .line 134
    new-instance v1, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    invoke-direct {v1}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;-><init>()V

    iput-object v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->replyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    .line 135
    new-instance v1, Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;-><init>(Lcom/navdy/hud/app/bluetooth/obex/BaseStream;)V

    iput-object v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mPrivateInput:Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;

    .line 136
    iput v4, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mResponseSize:I

    .line 137
    iput-object p5, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mListener:Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;

    .line 138
    iput-boolean v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mRequestFinished:Z

    .line 139
    iput-boolean v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mPrivateOutputOpen:Z

    .line 140
    iput-boolean v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mHasBody:Z

    .line 142
    invoke-virtual {p1}, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->getTransport()Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mTransport:Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;

    .line 147
    const/4 v1, 0x2

    if-eq p3, v1, :cond_0

    const/16 v1, 0x82

    if-ne p3, v1, :cond_3

    .line 152
    :cond_0
    iput-boolean v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mGetOperation:Z

    .line 157
    and-int/lit16 v1, p3, 0x80

    if-nez v1, :cond_2

    .line 158
    iput-boolean v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->finalBitSet:Z

    .line 180
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mInput:Ljava/io/InputStream;

    invoke-static {p3, v1}, Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;->read(ILjava/io/InputStream;)Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;

    move-result-object v0

    .line 185
    .local v0, "packet":Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;
    iget v1, v0, Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;->mLength:I

    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mTransport:Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;

    invoke-static {v2}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->getMaxRxPacketSize(Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;)I

    move-result v2

    if-le v1, v2, :cond_6

    .line 186
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mParent:Lcom/navdy/hud/app/bluetooth/obex/ServerSession;

    const/16 v2, 0xce

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->sendResponse(I[B)V

    .line 187
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Packet received was too large. Length: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;->mLength:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " maxLength: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mTransport:Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;

    .line 188
    invoke-static {v3}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->getMaxRxPacketSize(Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 160
    .end local v0    # "packet":Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;
    :cond_2
    iput-boolean v3, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->finalBitSet:Z

    .line 161
    iput-boolean v3, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mRequestFinished:Z

    goto :goto_0

    .line 163
    :cond_3
    if-eq p3, v4, :cond_4

    if-ne p3, v6, :cond_5

    .line 168
    :cond_4
    iput-boolean v3, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mGetOperation:Z

    .line 171
    iput-boolean v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->finalBitSet:Z

    .line 173
    if-ne p3, v6, :cond_1

    .line 174
    iput-boolean v3, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mRequestFinished:Z

    goto :goto_0

    .line 177
    :cond_5
    new-instance v1, Ljava/io/IOException;

    const-string v2, "ServerOperation can not handle such request"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 194
    .restart local v0    # "packet":Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;
    :cond_6
    iget v1, v0, Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;->mLength:I

    if-le v1, v4, :cond_a

    .line 195
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->handleObexPacket(Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 219
    :cond_7
    return-void

    .line 198
    :cond_8
    iget-boolean v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mHasBody:Z

    if-nez v1, :cond_a

    .line 199
    :cond_9
    iget-boolean v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mGetOperation:Z

    if-nez v1, :cond_a

    iget-boolean v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->finalBitSet:Z

    if-nez v1, :cond_a

    .line 200
    invoke-virtual {p0, v5}, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->sendReply(I)Z

    .line 201
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mPrivateInput:Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;

    invoke-virtual {v1}, Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;->available()I

    move-result v1

    if-lez v1, :cond_9

    .line 208
    :cond_a
    iget-boolean v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mGetOperation:Z

    if-nez v1, :cond_b

    iget-boolean v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->finalBitSet:Z

    if-nez v1, :cond_b

    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mPrivateInput:Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;

    invoke-virtual {v1}, Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;->available()I

    move-result v1

    if-nez v1, :cond_b

    .line 209
    invoke-virtual {p0, v5}, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->sendReply(I)Z

    .line 210
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mPrivateInput:Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;

    invoke-virtual {v1}, Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;->available()I

    move-result v1

    if-lez v1, :cond_a

    .line 216
    :cond_b
    :goto_1
    iget-boolean v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mGetOperation:Z

    if-eqz v1, :cond_7

    iget-boolean v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mRequestFinished:Z

    if-nez v1, :cond_7

    .line 217
    invoke-virtual {p0, v5}, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->sendReply(I)Z

    goto :goto_1
.end method

.method private checkForSrmWait(I)V
    .locals 4
    .param p1, "headerId"    # I

    .prologue
    const/4 v3, 0x1

    .line 299
    iget-boolean v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mSrmEnabled:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x3

    if-eq p1, v1, :cond_0

    const/16 v1, 0x83

    if-eq p1, v1, :cond_0

    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    .line 303
    :cond_0
    const/4 v1, 0x0

    :try_start_0
    iput-boolean v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mSrmWaitingForRemote:Z

    .line 304
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->requestHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    const/16 v2, 0x98

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    .line 305
    .local v0, "srmp":Ljava/lang/Byte;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v1

    if-ne v1, v3, :cond_1

    .line 306
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mSrmWaitingForRemote:Z

    .line 309
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->requestHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    const/16 v2, 0x98

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 313
    .end local v0    # "srmp":Ljava/lang/Byte;
    :cond_1
    :goto_0
    return-void

    .line 311
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private checkSrmRemoteAbort()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 598
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mInput:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->available()I

    move-result v1

    if-lez v1, :cond_0

    .line 599
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mInput:Ljava/io/InputStream;

    invoke-static {v1}, Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;->read(Ljava/io/InputStream;)Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;

    move-result-object v0

    .line 603
    .local v0, "packet":Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;
    iget v1, v0, Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;->mHeaderId:I

    const/16 v2, 0xff

    if-ne v1, v2, :cond_1

    .line 604
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->handleRemoteAbort()V

    .line 613
    .end local v0    # "packet":Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;
    :cond_0
    :goto_0
    return-void

    .line 609
    .restart local v0    # "packet":Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;
    :cond_1
    const-string v1, "ServerOperation"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Received unexpected request from client - discarding...\n   headerId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;->mHeaderId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " length: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;->mLength:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private handleObexPacket(Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;)Z
    .locals 9
    .param p1, "packet"    # Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v8, 0x0

    .line 229
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->updateRequestHeaders(Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;)[B

    move-result-object v0

    .line 231
    .local v0, "body":[B
    if-eqz v0, :cond_0

    .line 232
    iput-boolean v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mHasBody:Z

    .line 234
    :cond_0
    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mListener:Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;

    invoke-virtual {v3}, Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;->getConnectionId()J

    move-result-wide v4

    const-wide/16 v6, -0x1

    cmp-long v3, v4, v6

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->requestHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget-object v3, v3, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mConnectionID:[B

    if-eqz v3, :cond_1

    .line 235
    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mListener:Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;

    iget-object v4, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->requestHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget-object v4, v4, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mConnectionID:[B

    .line 236
    invoke-static {v4}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->convertToLong([B)J

    move-result-wide v4

    .line 235
    invoke-virtual {v3, v4, v5}, Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;->setConnectionId(J)V

    .line 241
    :goto_0
    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->requestHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget-object v3, v3, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    if-eqz v3, :cond_3

    .line 242
    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mParent:Lcom/navdy/hud/app/bluetooth/obex/ServerSession;

    iget-object v4, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->requestHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget-object v4, v4, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->handleAuthResp([B)Z

    move-result v3

    if-nez v3, :cond_2

    .line 243
    const-string v3, "Authentication Failed"

    iput-object v3, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mExceptionString:Ljava/lang/String;

    .line 244
    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mParent:Lcom/navdy/hud/app/bluetooth/obex/ServerSession;

    const/16 v4, 0xc1

    invoke-virtual {v3, v4, v8}, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->sendResponse(I[B)V

    .line 245
    iput-boolean v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mClosed:Z

    .line 246
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->requestHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iput-object v8, v2, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    .line 265
    :goto_1
    return v1

    .line 238
    :cond_1
    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mListener:Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;

    const-wide/16 v4, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;->setConnectionId(J)V

    goto :goto_0

    .line 249
    :cond_2
    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->requestHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iput-object v8, v3, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    .line 252
    :cond_3
    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->requestHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget-object v3, v3, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthChall:[B

    if-eqz v3, :cond_4

    .line 253
    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mParent:Lcom/navdy/hud/app/bluetooth/obex/ServerSession;

    iget-object v4, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->requestHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->handleAuthChall(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;)Z

    .line 255
    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->replyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget-object v4, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->requestHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget-object v4, v4, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    array-length v4, v4

    new-array v4, v4, [B

    iput-object v4, v3, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    .line 256
    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->requestHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget-object v3, v3, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    iget-object v4, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->replyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget-object v4, v4, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    iget-object v5, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->replyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget-object v5, v5, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    array-length v5, v5

    invoke-static {v3, v1, v4, v1, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 258
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->requestHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iput-object v8, v1, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    .line 259
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->requestHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iput-object v8, v1, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthChall:[B

    .line 262
    :cond_4
    if-eqz v0, :cond_5

    .line 263
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mPrivateInput:Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;

    invoke-virtual {v1, v0, v2}, Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;->writeBytes([BI)V

    :cond_5
    move v1, v2

    .line 265
    goto :goto_1
.end method

.method private handleRemoteAbort()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 621
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mParent:Lcom/navdy/hud/app/bluetooth/obex/ServerSession;

    const/16 v1, 0xa0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->sendResponse(I[B)V

    .line 622
    iput-boolean v3, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mClosed:Z

    .line 623
    iput-boolean v3, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->isAborted:Z

    .line 624
    const-string v0, "Abort Received"

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mExceptionString:Ljava/lang/String;

    .line 625
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Abort Received"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private updateRequestHeaders(Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;)[B
    .locals 5
    .param p1, "packet"    # Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 275
    const/4 v0, 0x0

    .line 276
    .local v0, "body":[B
    iget-object v2, p1, Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;->mPayload:[B

    if-eqz v2, :cond_0

    .line 277
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->requestHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    iget-object v3, p1, Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;->mPayload:[B

    invoke-static {v2, v3}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->updateHeaderSet(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;[B)[B

    move-result-object v0

    .line 279
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->requestHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    const/16 v3, 0x97

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Byte;

    .line 280
    .local v1, "srmMode":Ljava/lang/Byte;
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mTransport:Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;

    invoke-interface {v2}, Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;->isSrmSupported()Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    .line 281
    invoke-virtual {v1}, Ljava/lang/Byte;->byteValue()B

    move-result v2

    if-ne v2, v4, :cond_1

    .line 282
    iput-boolean v4, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mSrmEnabled:Z

    .line 285
    :cond_1
    iget v2, p1, Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;->mHeaderId:I

    invoke-direct {p0, v2}, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->checkForSrmWait(I)V

    .line 286
    iget-boolean v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mSrmWaitingForRemote:Z

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mSrmEnabled:Z

    if-eqz v2, :cond_2

    .line 288
    iput-boolean v4, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mSrmActive:Z

    .line 290
    :cond_2
    return-object v0
.end method


# virtual methods
.method public abort()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 636
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Called from a server"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 807
    invoke-virtual {p0}, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->ensureOpen()V

    .line 808
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mClosed:Z

    .line 809
    return-void
.end method

.method public declared-synchronized continueOperation(ZZ)Z
    .locals 4
    .param p1, "sendEmpty"    # Z
    .param p2, "inStream"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 333
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mGetOperation:Z

    if-nez v2, :cond_4

    .line 334
    iget-boolean v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->finalBitSet:Z

    if-nez v2, :cond_3

    .line 335
    if-eqz p1, :cond_0

    .line 336
    const/16 v1, 0x90

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->sendReply(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 351
    :goto_0
    monitor-exit p0

    return v0

    .line 339
    :cond_0
    :try_start_1
    iget v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mResponseSize:I

    const/4 v3, 0x3

    if-gt v2, v3, :cond_1

    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mPrivateOutput:Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;

    invoke-virtual {v2}, Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 340
    :cond_1
    const/16 v1, 0x90

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->sendReply(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 333
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    move v0, v1

    .line 343
    goto :goto_0

    :cond_3
    move v0, v1

    .line 347
    goto :goto_0

    .line 350
    :cond_4
    const/16 v1, 0x90

    :try_start_2
    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->sendReply(I)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public ensureNotDone()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 834
    return-void
.end method

.method public ensureOpen()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 816
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mExceptionString:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 817
    new-instance v0, Ljava/io/IOException;

    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mExceptionString:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 819
    :cond_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mClosed:Z

    if-eqz v0, :cond_1

    .line 820
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Operation has already ended"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 822
    :cond_1
    return-void
.end method

.method public getEncoding()Ljava/lang/String;
    .locals 1

    .prologue
    .line 695
    const/4 v0, 0x0

    return-object v0
.end method

.method public getHeaderLength()I
    .locals 6

    .prologue
    .line 739
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mListener:Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;

    invoke-virtual {v1}, Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;->getConnectionId()J

    move-result-wide v2

    .line 740
    .local v2, "id":J
    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 741
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->replyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    const/4 v4, 0x0

    iput-object v4, v1, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mConnectionID:[B

    .line 746
    :goto_0
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->replyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    const/4 v4, 0x0

    invoke-static {v1, v4}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->createHeader(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;Z)[B

    move-result-object v0

    .line 748
    .local v0, "headerArray":[B
    array-length v1, v0

    return v1

    .line 743
    .end local v0    # "headerArray":[B
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->replyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    invoke-static {v2, v3}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->convertToByteArray(J)[B

    move-result-object v4

    iput-object v4, v1, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mConnectionID:[B

    goto :goto_0
.end method

.method public getLength()J
    .locals 6

    .prologue
    const-wide/16 v2, -0x1

    .line 722
    :try_start_0
    iget-object v4, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->requestHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    const/16 v5, 0xc3

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 724
    .local v1, "temp":Ljava/lang/Long;
    if-nez v1, :cond_0

    .line 730
    .end local v1    # "temp":Ljava/lang/Long;
    :goto_0
    return-wide v2

    .line 727
    .restart local v1    # "temp":Ljava/lang/Long;
    :cond_0
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    goto :goto_0

    .line 729
    .end local v1    # "temp":Ljava/lang/Long;
    :catch_0
    move-exception v0

    .line 730
    .local v0, "e":Ljava/io/IOException;
    goto :goto_0
.end method

.method public getMaxPacketSize()I
    .locals 2

    .prologue
    .line 735
    iget v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mMaxPacketLength:I

    add-int/lit8 v0, v0, -0x6

    invoke-virtual {p0}, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->getHeaderLength()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public getReceivedHeader()Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 647
    invoke-virtual {p0}, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->ensureOpen()V

    .line 648
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->requestHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    return-object v0
.end method

.method public getResponseCode()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 687
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Called from a server"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getType()Ljava/lang/String;
    .locals 3

    .prologue
    .line 707
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->requestHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    const/16 v2, 0x42

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 709
    :goto_0
    return-object v1

    .line 708
    :catch_0
    move-exception v0

    .line 709
    .local v0, "e":Ljava/io/IOException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isValidBody()Z
    .locals 1

    .prologue
    .line 316
    iget-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mHasBody:Z

    return v0
.end method

.method public noBodyHeader()V
    .locals 1

    .prologue
    .line 849
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mSendBodyHeader:Z

    .line 850
    return-void
.end method

.method public openDataInputStream()Ljava/io/DataInputStream;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 767
    new-instance v0, Ljava/io/DataInputStream;

    invoke-virtual {p0}, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->openInputStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    return-object v0
.end method

.method public openDataOutputStream()Ljava/io/DataOutputStream;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 799
    new-instance v0, Ljava/io/DataOutputStream;

    invoke-virtual {p0}, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->openOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    return-object v0
.end method

.method public openInputStream()Ljava/io/InputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 757
    invoke-virtual {p0}, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->ensureOpen()V

    .line 758
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mPrivateInput:Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;

    return-object v0
.end method

.method public openOutputStream()Ljava/io/OutputStream;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 776
    invoke-virtual {p0}, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->ensureOpen()V

    .line 778
    iget-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mPrivateOutputOpen:Z

    if-eqz v0, :cond_0

    .line 779
    new-instance v0, Ljava/io/IOException;

    const-string v1, "no more input streams available, stream already opened"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 782
    :cond_0
    iget-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mRequestFinished:Z

    if-nez v0, :cond_1

    .line 783
    new-instance v0, Ljava/io/IOException;

    const-string v1, "no  output streams available ,request not finished"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 786
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mPrivateOutput:Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;

    if-nez v0, :cond_2

    .line 787
    new-instance v0, Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;

    invoke-virtual {p0}, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->getMaxPacketSize()I

    move-result v1

    invoke-direct {v0, p0, v1}, Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;-><init>(Lcom/navdy/hud/app/bluetooth/obex/BaseStream;I)V

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mPrivateOutput:Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;

    .line 789
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mPrivateOutputOpen:Z

    .line 790
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mPrivateOutput:Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;

    return-object v0
.end method

.method public sendHeaders(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;)V
    .locals 5
    .param p1, "headers"    # Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 661
    invoke-virtual {p0}, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->ensureOpen()V

    .line 663
    if-nez p1, :cond_0

    .line 664
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Headers may not be null"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 667
    :cond_0
    invoke-virtual {p1}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->getHeaderList()[I

    move-result-object v0

    .line 668
    .local v0, "headerList":[I
    if-eqz v0, :cond_1

    .line 669
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_1

    .line 670
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->replyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    aget v3, v0, v1

    aget v4, v0, v1

    invoke-virtual {p1, v4}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    .line 669
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 674
    .end local v1    # "i":I
    :cond_1
    return-void
.end method

.method public declared-synchronized sendReply(I)Z
    .locals 22
    .param p1, "type"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 366
    monitor-enter p0

    :try_start_0
    new-instance v12, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v12}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 367
    .local v12, "out":Ljava/io/ByteArrayOutputStream;
    const/16 v16, 0x0

    .line 368
    .local v16, "skipSend":Z
    const/4 v15, 0x0

    .line 369
    .local v15, "skipReceive":Z
    const/16 v17, 0x0

    .line 371
    .local v17, "srmRespSendPending":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mListener:Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;->getConnectionId()J

    move-result-wide v10

    .line 372
    .local v10, "id":J
    const-wide/16 v20, -0x1

    cmp-long v19, v10, v20

    if-nez v19, :cond_5

    .line 373
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->replyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mConnectionID:[B

    .line 378
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mSrmEnabled:Z

    move/from16 v19, v0

    if-eqz v19, :cond_0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mSrmResponseSent:Z

    move/from16 v19, v0

    if-nez v19, :cond_0

    .line 382
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->replyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    move-object/from16 v19, v0

    const/16 v20, 0x97

    const/16 v21, 0x1

    invoke-static/range {v21 .. v21}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    .line 383
    const/16 v17, 0x1

    .line 386
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mSrmEnabled:Z

    move/from16 v19, v0

    if-eqz v19, :cond_1

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mGetOperation:Z

    move/from16 v19, v0

    if-nez v19, :cond_1

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mSrmLocalWait:Z

    move/from16 v19, v0

    if-eqz v19, :cond_1

    .line 387
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->replyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    move-object/from16 v19, v0

    const/16 v20, 0x97

    const/16 v21, 0x1

    invoke-static/range {v21 .. v21}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    .line 390
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->replyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    invoke-static/range {v19 .. v20}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->createHeader(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;Z)[B

    move-result-object v7

    .line 391
    .local v7, "headerArray":[B
    const/4 v5, -0x1

    .line 392
    .local v5, "bodyLength":I
    const/4 v9, -0x1

    .line 394
    .local v9, "orginalBodyLength":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mPrivateOutput:Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;

    move-object/from16 v19, v0

    if-eqz v19, :cond_2

    .line 395
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mPrivateOutput:Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;->size()I

    move-result v5

    .line 396
    move v9, v5

    .line 399
    :cond_2
    array-length v0, v7

    move/from16 v19, v0

    add-int/lit8 v19, v19, 0x3

    move-object/from16 v0, p0

    iget v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mMaxPacketLength:I

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    if-le v0, v1, :cond_9

    .line 401
    const/4 v6, 0x0

    .line 402
    .local v6, "end":I
    const/16 v18, 0x0

    .line 404
    .local v18, "start":I
    :goto_1
    array-length v0, v7

    move/from16 v19, v0

    move/from16 v0, v19

    if-eq v6, v0, :cond_7

    .line 405
    move-object/from16 v0, p0

    iget v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mMaxPacketLength:I

    move/from16 v19, v0

    add-int/lit8 v19, v19, -0x3

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-static {v7, v0, v1}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->findHeaderEnd([BII)I

    move-result v6

    .line 407
    const/16 v19, -0x1

    move/from16 v0, v19

    if-ne v6, v0, :cond_6

    .line 409
    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mClosed:Z

    .line 411
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mPrivateInput:Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;

    move-object/from16 v19, v0

    if-eqz v19, :cond_3

    .line 412
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mPrivateInput:Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/navdy/hud/app/bluetooth/obex/PrivateInputStream;->close()V

    .line 415
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mPrivateOutput:Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;

    move-object/from16 v19, v0

    if-eqz v19, :cond_4

    .line 416
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mPrivateOutput:Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;->close()V

    .line 418
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mParent:Lcom/navdy/hud/app/bluetooth/obex/ServerSession;

    move-object/from16 v19, v0

    const/16 v20, 0xd0

    const/16 v21, 0x0

    invoke-virtual/range {v19 .. v21}, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->sendResponse(I[B)V

    .line 419
    new-instance v19, Ljava/io/IOException;

    const-string v20, "OBEX Packet exceeds max packet size"

    invoke-direct/range {v19 .. v20}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v19
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 366
    .end local v5    # "bodyLength":I
    .end local v6    # "end":I
    .end local v7    # "headerArray":[B
    .end local v9    # "orginalBodyLength":I
    .end local v10    # "id":J
    .end local v12    # "out":Ljava/io/ByteArrayOutputStream;
    .end local v15    # "skipReceive":Z
    .end local v16    # "skipSend":Z
    .end local v17    # "srmRespSendPending":Z
    .end local v18    # "start":I
    :catchall_0
    move-exception v19

    monitor-exit p0

    throw v19

    .line 375
    .restart local v10    # "id":J
    .restart local v12    # "out":Ljava/io/ByteArrayOutputStream;
    .restart local v15    # "skipReceive":Z
    .restart local v16    # "skipSend":Z
    .restart local v17    # "srmRespSendPending":Z
    :cond_5
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->replyHeader:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    move-object/from16 v19, v0

    invoke-static {v10, v11}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->convertToByteArray(J)[B

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mConnectionID:[B

    goto/16 :goto_0

    .line 421
    .restart local v5    # "bodyLength":I
    .restart local v6    # "end":I
    .restart local v7    # "headerArray":[B
    .restart local v9    # "orginalBodyLength":I
    .restart local v18    # "start":I
    :cond_6
    sub-int v19, v6, v18

    move/from16 v0, v19

    new-array v14, v0, [B

    .line 422
    .local v14, "sendHeader":[B
    const/16 v19, 0x0

    array-length v0, v14

    move/from16 v20, v0

    move/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-static {v7, v0, v14, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 424
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mParent:Lcom/navdy/hud/app/bluetooth/obex/ServerSession;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move/from16 v1, p1

    invoke-virtual {v0, v1, v14}, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->sendResponse(I[B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 425
    move/from16 v18, v6

    .line 426
    goto/16 :goto_1

    .line 428
    .end local v14    # "sendHeader":[B
    :cond_7
    if-lez v5, :cond_8

    .line 429
    const/16 v19, 0x1

    .line 577
    .end local v6    # "end":I
    .end local v18    # "start":I
    :goto_2
    monitor-exit p0

    return v19

    .line 431
    .restart local v6    # "end":I
    .restart local v18    # "start":I
    :cond_8
    const/16 v19, 0x0

    goto :goto_2

    .line 435
    .end local v6    # "end":I
    .end local v18    # "start":I
    :cond_9
    :try_start_2
    invoke-virtual {v12, v7}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 440
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mGetOperation:Z

    move/from16 v19, v0

    if-eqz v19, :cond_a

    const/16 v19, 0xa0

    move/from16 v0, p1

    move/from16 v1, v19

    if-ne v0, v1, :cond_a

    .line 441
    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->finalBitSet:Z

    .line 444
    :cond_a
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mSrmActive:Z

    move/from16 v19, v0

    if-eqz v19, :cond_b

    .line 445
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mGetOperation:Z

    move/from16 v19, v0

    if-nez v19, :cond_14

    const/16 v19, 0x90

    move/from16 v0, p1

    move/from16 v1, v19

    if-ne v0, v1, :cond_14

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mSrmResponseSent:Z

    move/from16 v19, v0

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_14

    .line 448
    const/16 v16, 0x1

    .line 460
    :cond_b
    :goto_3
    if-eqz v17, :cond_c

    .line 463
    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mSrmResponseSent:Z

    .line 466
    :cond_c
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->finalBitSet:Z

    move/from16 v19, v0

    if-nez v19, :cond_d

    array-length v0, v7

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mMaxPacketLength:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, -0x14

    move/from16 v0, v19

    move/from16 v1, v20

    if-ge v0, v1, :cond_10

    .line 467
    :cond_d
    if-lez v5, :cond_10

    .line 473
    move-object/from16 v0, p0

    iget v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mMaxPacketLength:I

    move/from16 v19, v0

    array-length v0, v7

    move/from16 v20, v0

    sub-int v19, v19, v20

    add-int/lit8 v19, v19, -0x6

    move/from16 v0, v19

    if-le v5, v0, :cond_e

    .line 474
    move-object/from16 v0, p0

    iget v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mMaxPacketLength:I

    move/from16 v19, v0

    array-length v0, v7

    move/from16 v20, v0

    sub-int v19, v19, v20

    add-int/lit8 v5, v19, -0x6

    .line 477
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mPrivateOutput:Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;->readBytes(I)[B

    move-result-object v4

    .line 484
    .local v4, "body":[B
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->finalBitSet:Z

    move/from16 v19, v0

    if-nez v19, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mPrivateOutput:Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/navdy/hud/app/bluetooth/obex/PrivateOutputStream;->isClosed()Z

    move-result v19

    if-eqz v19, :cond_16

    .line 485
    :cond_f
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mSendBodyHeader:Z

    move/from16 v19, v0

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_10

    .line 486
    const/16 v19, 0x49

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 487
    add-int/lit8 v5, v5, 0x3

    .line 488
    shr-int/lit8 v19, v5, 0x8

    move/from16 v0, v19

    int-to-byte v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 489
    int-to-byte v0, v5

    move/from16 v19, v0

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 490
    invoke-virtual {v12, v4}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 505
    .end local v4    # "body":[B
    :cond_10
    :goto_4
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->finalBitSet:Z

    move/from16 v19, v0

    if-eqz v19, :cond_11

    const/16 v19, 0xa0

    move/from16 v0, p1

    move/from16 v1, v19

    if-ne v0, v1, :cond_11

    if-gtz v9, :cond_11

    .line 506
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mSendBodyHeader:Z

    move/from16 v19, v0

    if-eqz v19, :cond_11

    .line 507
    const/16 v19, 0x49

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 508
    const/4 v9, 0x3

    .line 509
    const/16 v19, 0x0

    move/from16 v0, v19

    int-to-byte v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 510
    int-to-byte v0, v9

    move/from16 v19, v0

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 514
    :cond_11
    if-nez v16, :cond_12

    .line 515
    const/16 v19, 0x3

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mResponseSize:I

    .line 516
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mParent:Lcom/navdy/hud/app/bluetooth/obex/ServerSession;

    move-object/from16 v19, v0

    invoke-virtual {v12}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v20

    move-object/from16 v0, v19

    move/from16 v1, p1

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->sendResponse(I[B)V

    .line 519
    :cond_12
    const/16 v19, 0x90

    move/from16 v0, p1

    move/from16 v1, v19

    if-ne v0, v1, :cond_1e

    .line 521
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mGetOperation:Z

    move/from16 v19, v0

    if-eqz v19, :cond_17

    if-eqz v15, :cond_17

    .line 524
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->checkSrmRemoteAbort()V

    .line 575
    :cond_13
    :goto_5
    const/16 v19, 0x1

    goto/16 :goto_2

    .line 449
    :cond_14
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mGetOperation:Z

    move/from16 v19, v0

    if-eqz v19, :cond_15

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mRequestFinished:Z

    move/from16 v19, v0

    if-nez v19, :cond_15

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mSrmResponseSent:Z

    move/from16 v19, v0

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_15

    .line 451
    const/16 v16, 0x1

    goto/16 :goto_3

    .line 452
    :cond_15
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mGetOperation:Z

    move/from16 v19, v0

    if-eqz v19, :cond_b

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mRequestFinished:Z

    move/from16 v19, v0

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_b

    .line 455
    const/4 v15, 0x1

    goto/16 :goto_3

    .line 493
    .restart local v4    # "body":[B
    :cond_16
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mSendBodyHeader:Z

    move/from16 v19, v0

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_10

    .line 494
    const/16 v19, 0x48

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 495
    add-int/lit8 v5, v5, 0x3

    .line 496
    shr-int/lit8 v19, v5, 0x8

    move/from16 v0, v19

    int-to-byte v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 497
    int-to-byte v0, v5

    move/from16 v19, v0

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 498
    invoke-virtual {v12, v4}, Ljava/io/ByteArrayOutputStream;->write([B)V

    goto/16 :goto_4

    .line 528
    .end local v4    # "body":[B
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mInput:Ljava/io/InputStream;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;->read(Ljava/io/InputStream;)Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;

    move-result-object v13

    .line 530
    .local v13, "packet":Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;
    iget v8, v13, Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;->mHeaderId:I

    .line 531
    .local v8, "headerId":I
    const/16 v19, 0x2

    move/from16 v0, v19

    if-eq v8, v0, :cond_19

    const/16 v19, 0x82

    move/from16 v0, v19

    if-eq v8, v0, :cond_19

    const/16 v19, 0x3

    move/from16 v0, v19

    if-eq v8, v0, :cond_19

    const/16 v19, 0x83

    move/from16 v0, v19

    if-eq v8, v0, :cond_19

    .line 539
    const/16 v19, 0xff

    move/from16 v0, v19

    if-ne v8, v0, :cond_18

    .line 540
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->handleRemoteAbort()V

    goto/16 :goto_5

    .line 543
    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mParent:Lcom/navdy/hud/app/bluetooth/obex/ServerSession;

    move-object/from16 v19, v0

    const/16 v20, 0xc0

    const/16 v21, 0x0

    invoke-virtual/range {v19 .. v21}, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->sendResponse(I[B)V

    .line 544
    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mClosed:Z

    .line 545
    const-string v19, "Bad Request Received"

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mExceptionString:Ljava/lang/String;

    .line 546
    new-instance v19, Ljava/io/IOException;

    const-string v20, "Bad Request Received"

    invoke-direct/range {v19 .. v20}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v19

    .line 550
    :cond_19
    const/16 v19, 0x82

    move/from16 v0, v19

    if-ne v8, v0, :cond_1b

    .line 551
    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->finalBitSet:Z

    .line 559
    :cond_1a
    :goto_6
    iget v0, v13, Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;->mLength:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mTransport:Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->getMaxRxPacketSize(Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;)I

    move-result v20

    move/from16 v0, v19

    move/from16 v1, v20

    if-le v0, v1, :cond_1c

    .line 560
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mParent:Lcom/navdy/hud/app/bluetooth/obex/ServerSession;

    move-object/from16 v19, v0

    const/16 v20, 0xce

    const/16 v21, 0x0

    invoke-virtual/range {v19 .. v21}, Lcom/navdy/hud/app/bluetooth/obex/ServerSession;->sendResponse(I[B)V

    .line 561
    new-instance v19, Ljava/io/IOException;

    const-string v20, "Packet received was too large"

    invoke-direct/range {v19 .. v20}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v19

    .line 552
    :cond_1b
    const/16 v19, 0x83

    move/from16 v0, v19

    if-ne v8, v0, :cond_1a

    .line 553
    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mRequestFinished:Z

    goto :goto_6

    .line 567
    :cond_1c
    iget v0, v13, Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;->mLength:I

    move/from16 v19, v0

    const/16 v20, 0x3

    move/from16 v0, v19

    move/from16 v1, v20

    if-gt v0, v1, :cond_1d

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->mSrmEnabled:Z

    move/from16 v19, v0

    if-eqz v19, :cond_13

    iget v0, v13, Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;->mLength:I

    move/from16 v19, v0

    const/16 v20, 0x3

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_13

    .line 568
    :cond_1d
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/navdy/hud/app/bluetooth/obex/ServerOperation;->handleObexPacket(Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v19

    if-nez v19, :cond_13

    .line 569
    const/16 v19, 0x0

    goto/16 :goto_2

    .line 577
    .end local v8    # "headerId":I
    .end local v13    # "packet":Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;
    :cond_1e
    const/16 v19, 0x0

    goto/16 :goto_2
.end method

.method public streamClosed(Z)V
    .locals 0
    .param p1, "inStream"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 846
    return-void
.end method
