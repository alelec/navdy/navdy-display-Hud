.class public final Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;
.super Ljava/lang/Object;
.source "ObexHelper.java"


# static fields
.field public static final BASE_PACKET_LENGTH:I = 0x3

.field public static final LOWER_LIMIT_MAX_PACKET_SIZE:I = 0xff

.field public static final MAX_CLIENT_PACKET_SIZE:I = 0xfc00

.field public static final MAX_PACKET_SIZE_INT:I = 0xfffe

.field public static final OBEX_AUTH_REALM_CHARSET_ASCII:I = 0x0

.field public static final OBEX_AUTH_REALM_CHARSET_ISO_8859_1:I = 0x1

.field public static final OBEX_AUTH_REALM_CHARSET_ISO_8859_2:I = 0x2

.field public static final OBEX_AUTH_REALM_CHARSET_ISO_8859_3:I = 0x3

.field public static final OBEX_AUTH_REALM_CHARSET_ISO_8859_4:I = 0x4

.field public static final OBEX_AUTH_REALM_CHARSET_ISO_8859_5:I = 0x5

.field public static final OBEX_AUTH_REALM_CHARSET_ISO_8859_6:I = 0x6

.field public static final OBEX_AUTH_REALM_CHARSET_ISO_8859_7:I = 0x7

.field public static final OBEX_AUTH_REALM_CHARSET_ISO_8859_8:I = 0x8

.field public static final OBEX_AUTH_REALM_CHARSET_ISO_8859_9:I = 0x9

.field public static final OBEX_AUTH_REALM_CHARSET_UNICODE:I = 0xff

.field public static final OBEX_OPCODE_ABORT:I = 0xff

.field public static final OBEX_OPCODE_CONNECT:I = 0x80

.field public static final OBEX_OPCODE_DISCONNECT:I = 0x81

.field public static final OBEX_OPCODE_FINAL_BIT_MASK:I = 0x80

.field public static final OBEX_OPCODE_GET:I = 0x3

.field public static final OBEX_OPCODE_GET_FINAL:I = 0x83

.field public static final OBEX_OPCODE_PUT:I = 0x2

.field public static final OBEX_OPCODE_PUT_FINAL:I = 0x82

.field public static final OBEX_OPCODE_RESERVED:I = 0x4

.field public static final OBEX_OPCODE_RESERVED_FINAL:I = 0x84

.field public static final OBEX_OPCODE_SETPATH:I = 0x85

.field public static final OBEX_SRMP_WAIT:B = 0x1t

.field public static final OBEX_SRM_DISABLE:B = 0x0t

.field public static final OBEX_SRM_ENABLE:B = 0x1t

.field public static final OBEX_SRM_SUPPORT:B = 0x2t

.field private static final TAG:Ljava/lang/String; = "ObexHelper"

.field public static final VDBG:Z


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    return-void
.end method

.method public static computeAuthenticationChallenge([BLjava/lang/String;ZZ)[B
    .locals 9
    .param p0, "nonce"    # [B
    .param p1, "realm"    # Ljava/lang/String;
    .param p2, "access"    # Z
    .param p3, "userID"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x2

    const/16 v7, 0x10

    const/4 v6, 0x1

    const/16 v5, 0x14

    const/4 v4, 0x0

    .line 1001
    const/4 v0, 0x0

    .line 1003
    .local v0, "authChall":[B
    array-length v1, p0

    if-eq v1, v7, :cond_0

    .line 1004
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Nonce must be 16 bytes long"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1025
    :cond_0
    if-nez p1, :cond_3

    .line 1026
    const/16 v1, 0x15

    new-array v0, v1, [B

    .line 1039
    :goto_0
    aput-byte v4, v0, v4

    .line 1040
    aput-byte v7, v0, v6

    .line 1041
    invoke-static {p0, v4, v0, v8, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1044
    const/16 v1, 0x12

    aput-byte v6, v0, v1

    .line 1045
    const/16 v1, 0x13

    aput-byte v6, v0, v1

    .line 1046
    aput-byte v4, v0, v5

    .line 1048
    if-nez p2, :cond_1

    .line 1049
    aget-byte v1, v0, v5

    or-int/lit8 v1, v1, 0x2

    int-to-byte v1, v1

    aput-byte v1, v0, v5

    .line 1051
    :cond_1
    if-eqz p3, :cond_2

    .line 1052
    aget-byte v1, v0, v5

    or-int/lit8 v1, v1, 0x1

    int-to-byte v1, v1

    aput-byte v1, v0, v5

    .line 1055
    :cond_2
    return-object v0

    .line 1028
    :cond_3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0xff

    if-lt v1, v2, :cond_4

    .line 1029
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Realm must be less then 255 bytes"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1031
    :cond_4
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x18

    new-array v0, v1, [B

    .line 1032
    const/16 v1, 0x15

    aput-byte v8, v0, v1

    .line 1033
    const/16 v1, 0x16

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 1034
    const/16 v1, 0x17

    aput-byte v6, v0, v1

    .line 1035
    const-string v1, "ISO8859_1"

    invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    const/16 v2, 0x18

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-static {v1, v4, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method public static computeMd5Hash([B)[B
    .locals 3
    .param p0, "in"    # [B

    .prologue
    .line 978
    :try_start_0
    const-string v2, "MD5"

    invoke-static {v2}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 979
    .local v1, "md5":Ljava/security/MessageDigest;
    invoke-virtual {v1, p0}, Ljava/security/MessageDigest;->digest([B)[B
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    return-object v2

    .line 980
    .end local v1    # "md5":Ljava/security/MessageDigest;
    :catch_0
    move-exception v0

    .line 981
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public static convertToByteArray(J)[B
    .locals 6
    .param p0, "l"    # J

    .prologue
    const-wide/16 v4, 0xff

    .line 840
    const/4 v1, 0x4

    new-array v0, v1, [B

    .line 842
    .local v0, "b":[B
    const/4 v1, 0x0

    const/16 v2, 0x18

    shr-long v2, p0, v2

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 843
    const/4 v1, 0x1

    const/16 v2, 0x10

    shr-long v2, p0, v2

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 844
    const/4 v1, 0x2

    const/16 v2, 0x8

    shr-long v2, p0, v2

    and-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 845
    const/4 v1, 0x3

    and-long v2, v4, p0

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 847
    return-object v0
.end method

.method public static convertToLong([B)J
    .locals 10
    .param p0, "b"    # [B

    .prologue
    .line 817
    const-wide/16 v4, 0x0

    .line 818
    .local v4, "result":J
    const-wide/16 v6, 0x0

    .line 819
    .local v6, "value":J
    const-wide/16 v2, 0x0

    .line 821
    .local v2, "power":J
    array-length v1, p0

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 822
    aget-byte v1, p0, v0

    int-to-long v6, v1

    .line 823
    const-wide/16 v8, 0x0

    cmp-long v1, v6, v8

    if-gez v1, :cond_0

    .line 824
    const-wide/16 v8, 0x100

    add-long/2addr v6, v8

    .line 827
    :cond_0
    long-to-int v1, v2

    shl-long v8, v6, v1

    or-long/2addr v4, v8

    .line 828
    const-wide/16 v8, 0x8

    add-long/2addr v2, v8

    .line 821
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 831
    :cond_1
    return-wide v4
.end method

.method public static convertToUnicode([BZ)Ljava/lang/String;
    .locals 7
    .param p0, "b"    # [B
    .param p1, "includesNull"    # Z

    .prologue
    .line 936
    if-eqz p0, :cond_0

    array-length v5, p0

    if-nez v5, :cond_1

    .line 937
    :cond_0
    const/4 v5, 0x0

    .line 967
    :goto_0
    return-object v5

    .line 939
    :cond_1
    array-length v0, p0

    .line 940
    .local v0, "arrayLength":I
    rem-int/lit8 v5, v0, 0x2

    if-eqz v5, :cond_2

    .line 941
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "Byte array not of a valid form"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 943
    :cond_2
    shr-int/lit8 v0, v0, 0x1

    .line 944
    if-eqz p1, :cond_3

    .line 945
    add-int/lit8 v0, v0, -0x1

    .line 948
    :cond_3
    new-array v1, v0, [C

    .line 949
    .local v1, "c":[C
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v0, :cond_7

    .line 950
    mul-int/lit8 v5, v2, 0x2

    aget-byte v4, p0, v5

    .line 951
    .local v4, "upper":I
    mul-int/lit8 v5, v2, 0x2

    add-int/lit8 v5, v5, 0x1

    aget-byte v3, p0, v5

    .line 952
    .local v3, "lower":I
    if-gez v4, :cond_4

    .line 953
    add-int/lit16 v4, v4, 0x100

    .line 955
    :cond_4
    if-gez v3, :cond_5

    .line 956
    add-int/lit16 v3, v3, 0x100

    .line 960
    :cond_5
    if-nez v4, :cond_6

    if-nez v3, :cond_6

    .line 961
    new-instance v5, Ljava/lang/String;

    const/4 v6, 0x0

    invoke-direct {v5, v1, v6, v2}, Ljava/lang/String;-><init>([CII)V

    goto :goto_0

    .line 964
    :cond_6
    shl-int/lit8 v5, v4, 0x8

    or-int/2addr v5, v3

    int-to-char v5, v5

    aput-char v5, v1, v2

    .line 949
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 967
    .end local v3    # "lower":I
    .end local v4    # "upper":I
    :cond_7
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v1}, Ljava/lang/String;-><init>([C)V

    goto :goto_0
.end method

.method public static convertToUnicodeByteArray(Ljava/lang/String;)[B
    .locals 6
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 857
    if-nez p0, :cond_0

    .line 858
    const/4 v2, 0x0

    .line 872
    :goto_0
    return-object v2

    .line 861
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .line 862
    .local v0, "c":[C
    array-length v3, v0

    mul-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, 0x2

    new-array v2, v3, [B

    .line 863
    .local v2, "result":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v3, v0

    if-ge v1, v3, :cond_1

    .line 864
    mul-int/lit8 v3, v1, 0x2

    aget-char v4, v0, v1

    shr-int/lit8 v4, v4, 0x8

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    .line 865
    mul-int/lit8 v3, v1, 0x2

    add-int/lit8 v3, v3, 0x1

    aget-char v4, v0, v1

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    .line 863
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 869
    :cond_1
    array-length v3, v2

    add-int/lit8 v3, v3, -0x2

    aput-byte v5, v2, v3

    .line 870
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    aput-byte v5, v2, v3

    goto :goto_0
.end method

.method public static createHeader(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;Z)[B
    .locals 22
    .param p0, "head"    # Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    .param p1, "nullOut"    # Z

    .prologue
    .line 360
    const/4 v9, 0x0

    .line 361
    .local v9, "intHeader":Ljava/lang/Long;
    const/4 v14, 0x0

    .line 362
    .local v14, "stringHeader":Ljava/lang/String;
    const/4 v5, 0x0

    .line 363
    .local v5, "dateHeader":Ljava/util/Calendar;
    const/4 v4, 0x0

    .line 364
    .local v4, "byteHeader":Ljava/lang/Byte;
    const/4 v2, 0x0

    .line 365
    .local v2, "buffer":Ljava/lang/StringBuffer;
    const/16 v16, 0x0

    .line 366
    .local v16, "value":[B
    const/4 v13, 0x0

    .line 367
    .local v13, "result":[B
    const/16 v17, 0x2

    move/from16 v0, v17

    new-array v11, v0, [B

    .line 369
    .local v11, "lengthArray":[B
    const/4 v7, 0x0

    .line 370
    .local v7, "headImpl":Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    new-instance v12, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v12}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 371
    .local v12, "out":Ljava/io/ByteArrayOutputStream;
    move-object/from16 v7, p0

    .line 378
    :try_start_0
    iget-object v0, v7, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mConnectionID:[B

    move-object/from16 v17, v0

    if-eqz v17, :cond_0

    const/16 v17, 0x46

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    move-result-object v17

    if-nez v17, :cond_0

    .line 380
    const/16 v17, -0x35

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 381
    iget-object v0, v7, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mConnectionID:[B

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 385
    :cond_0
    const/16 v17, 0xc0

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    move-result-object v17

    move-object/from16 v0, v17

    check-cast v0, Ljava/lang/Long;

    move-object v9, v0

    .line 386
    if-eqz v9, :cond_1

    .line 387
    const/16 v17, -0x40

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 388
    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->convertToByteArray(J)[B

    move-result-object v16

    .line 389
    move-object/from16 v0, v16

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 390
    if-eqz p1, :cond_1

    .line 391
    const/16 v17, 0xc0

    const/16 v18, 0x0

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v7, v0, v1}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    .line 396
    :cond_1
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    move-result-object v17

    move-object/from16 v0, v17

    check-cast v0, Ljava/lang/String;

    move-object v14, v0

    .line 397
    if-eqz v14, :cond_5

    .line 398
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 399
    invoke-static {v14}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->convertToUnicodeByteArray(Ljava/lang/String;)[B

    move-result-object v16

    .line 400
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    add-int/lit8 v10, v17, 0x3

    .line 401
    .local v10, "length":I
    const/16 v17, 0x0

    shr-int/lit8 v18, v10, 0x8

    move/from16 v0, v18

    and-int/lit16 v0, v0, 0xff

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    aput-byte v18, v11, v17

    .line 402
    const/16 v17, 0x1

    and-int/lit16 v0, v10, 0xff

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    aput-byte v18, v11, v17

    .line 403
    invoke-virtual {v12, v11}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 404
    move-object/from16 v0, v16

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 405
    if-eqz p1, :cond_2

    .line 406
    const/16 v17, 0x1

    const/16 v18, 0x0

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v7, v0, v1}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    .line 416
    .end local v10    # "length":I
    :cond_2
    :goto_0
    const/16 v17, 0x42

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    move-result-object v17

    move-object/from16 v0, v17

    check-cast v0, Ljava/lang/String;

    move-object v14, v0

    .line 417
    if-eqz v14, :cond_3

    .line 418
    const/16 v17, 0x42

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 420
    :try_start_1
    const-string v17, "ISO8859_1"

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v16

    .line 425
    :try_start_2
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    add-int/lit8 v10, v17, 0x4

    .line 426
    .restart local v10    # "length":I
    const/16 v17, 0x0

    shr-int/lit8 v18, v10, 0x8

    move/from16 v0, v18

    and-int/lit16 v0, v0, 0xff

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    aput-byte v18, v11, v17

    .line 427
    const/16 v17, 0x1

    and-int/lit16 v0, v10, 0xff

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    aput-byte v18, v11, v17

    .line 428
    invoke-virtual {v12, v11}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 429
    move-object/from16 v0, v16

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 430
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 431
    if-eqz p1, :cond_3

    .line 432
    const/16 v17, 0x42

    const/16 v18, 0x0

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v7, v0, v1}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    .line 437
    .end local v10    # "length":I
    :cond_3
    const/16 v17, 0xc3

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    move-result-object v17

    move-object/from16 v0, v17

    check-cast v0, Ljava/lang/Long;

    move-object v9, v0

    .line 438
    if-eqz v9, :cond_4

    .line 439
    const/16 v17, -0x3d

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 440
    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->convertToByteArray(J)[B

    move-result-object v16

    .line 441
    move-object/from16 v0, v16

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 442
    if-eqz p1, :cond_4

    .line 443
    const/16 v17, 0xc3

    const/16 v18, 0x0

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v7, v0, v1}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    .line 448
    :cond_4
    const/16 v17, 0x44

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    move-result-object v17

    move-object/from16 v0, v17

    check-cast v0, Ljava/util/Calendar;

    move-object v5, v0

    .line 449
    if-eqz v5, :cond_e

    .line 455
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 456
    .end local v2    # "buffer":Ljava/lang/StringBuffer;
    .local v3, "buffer":Ljava/lang/StringBuffer;
    const/16 v17, 0x1

    :try_start_3
    move/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/util/Calendar;->get(I)I

    move-result v15

    .line 457
    .local v15, "temp":I
    move v8, v15

    .local v8, "i":I
    :goto_1
    const/16 v17, 0x3e8

    move/from16 v0, v17

    if-ge v8, v0, :cond_6

    .line 458
    const-string v17, "0"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 457
    mul-int/lit8 v8, v8, 0xa

    goto :goto_1

    .line 408
    .end local v3    # "buffer":Ljava/lang/StringBuffer;
    .end local v8    # "i":I
    .end local v15    # "temp":I
    .restart local v2    # "buffer":Ljava/lang/StringBuffer;
    :cond_5
    :try_start_4
    invoke-virtual {v7}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->getEmptyNameHeader()Z

    move-result v17

    if-eqz v17, :cond_2

    .line 409
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 410
    const/16 v17, 0x0

    const/16 v18, 0x0

    aput-byte v18, v11, v17

    .line 411
    const/16 v17, 0x1

    const/16 v18, 0x3

    aput-byte v18, v11, v17

    .line 412
    invoke-virtual {v12, v11}, Ljava/io/ByteArrayOutputStream;->write([B)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 719
    :catch_0
    move-exception v17

    .line 721
    :goto_2
    invoke-virtual {v12}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v13

    .line 723
    :try_start_5
    invoke-virtual {v12}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_5

    .line 728
    :goto_3
    return-object v13

    .line 421
    :catch_1
    move-exception v6

    .line 422
    .local v6, "e":Ljava/io/UnsupportedEncodingException;
    :try_start_6
    throw v6
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 721
    .end local v6    # "e":Ljava/io/UnsupportedEncodingException;
    :catchall_0
    move-exception v17

    :goto_4
    invoke-virtual {v12}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v13

    .line 723
    :try_start_7
    invoke-virtual {v12}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_6

    .line 725
    :goto_5
    throw v17

    .line 460
    .end local v2    # "buffer":Ljava/lang/StringBuffer;
    .restart local v3    # "buffer":Ljava/lang/StringBuffer;
    .restart local v8    # "i":I
    .restart local v15    # "temp":I
    :cond_6
    :try_start_8
    invoke-virtual {v3, v15}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 461
    const/16 v17, 0x2

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/util/Calendar;->get(I)I

    move-result v15

    .line 462
    const/16 v17, 0xa

    move/from16 v0, v17

    if-ge v15, v0, :cond_7

    .line 463
    const-string v17, "0"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 465
    :cond_7
    invoke-virtual {v3, v15}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 466
    const/16 v17, 0x5

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/util/Calendar;->get(I)I

    move-result v15

    .line 467
    const/16 v17, 0xa

    move/from16 v0, v17

    if-ge v15, v0, :cond_8

    .line 468
    const-string v17, "0"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 470
    :cond_8
    invoke-virtual {v3, v15}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 471
    const-string v17, "T"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 472
    const/16 v17, 0xb

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/util/Calendar;->get(I)I

    move-result v15

    .line 473
    const/16 v17, 0xa

    move/from16 v0, v17

    if-ge v15, v0, :cond_9

    .line 474
    const-string v17, "0"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 476
    :cond_9
    invoke-virtual {v3, v15}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 477
    const/16 v17, 0xc

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/util/Calendar;->get(I)I

    move-result v15

    .line 478
    const/16 v17, 0xa

    move/from16 v0, v17

    if-ge v15, v0, :cond_a

    .line 479
    const-string v17, "0"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 481
    :cond_a
    invoke-virtual {v3, v15}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 482
    const/16 v17, 0xd

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/util/Calendar;->get(I)I

    move-result v15

    .line 483
    const/16 v17, 0xa

    move/from16 v0, v17

    if-ge v15, v0, :cond_b

    .line 484
    const-string v17, "0"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 486
    :cond_b
    invoke-virtual {v3, v15}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 488
    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v17

    const-string v18, "UTC"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_c

    .line 489
    const-string v17, "Z"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 493
    :cond_c
    :try_start_9
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v17

    const-string v18, "ISO8859_1"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_9
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_9 .. :try_end_9} :catch_2
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    move-result-object v16

    .line 498
    :try_start_a
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    add-int/lit8 v10, v17, 0x3

    .line 499
    .restart local v10    # "length":I
    const/16 v17, 0x0

    shr-int/lit8 v18, v10, 0x8

    move/from16 v0, v18

    and-int/lit16 v0, v0, 0xff

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    aput-byte v18, v11, v17

    .line 500
    const/16 v17, 0x1

    and-int/lit16 v0, v10, 0xff

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    aput-byte v18, v11, v17

    .line 501
    const/16 v17, 0x44

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 502
    invoke-virtual {v12, v11}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 503
    move-object/from16 v0, v16

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 504
    if-eqz p1, :cond_d

    .line 505
    const/16 v17, 0x44

    const/16 v18, 0x0

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v7, v0, v1}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :cond_d
    move-object v2, v3

    .line 510
    .end local v3    # "buffer":Ljava/lang/StringBuffer;
    .end local v8    # "i":I
    .end local v10    # "length":I
    .end local v15    # "temp":I
    .restart local v2    # "buffer":Ljava/lang/StringBuffer;
    :cond_e
    const/16 v17, 0xc4

    :try_start_b
    move/from16 v0, v17

    invoke-virtual {v7, v0}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    move-result-object v17

    move-object/from16 v0, v17

    check-cast v0, Ljava/util/Calendar;

    move-object v5, v0

    .line 511
    if-eqz v5, :cond_f

    .line 512
    const/16 v17, 0xc4

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 521
    invoke-virtual {v5}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/util/Date;->getTime()J

    move-result-wide v18

    const-wide/16 v20, 0x3e8

    div-long v18, v18, v20

    invoke-static/range {v18 .. v19}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->convertToByteArray(J)[B

    move-result-object v16

    .line 522
    move-object/from16 v0, v16

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 523
    if-eqz p1, :cond_f

    .line 524
    const/16 v17, 0xc4

    const/16 v18, 0x0

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v7, v0, v1}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    .line 529
    :cond_f
    const/16 v17, 0x5

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    move-result-object v17

    move-object/from16 v0, v17

    check-cast v0, Ljava/lang/String;

    move-object v14, v0

    .line 530
    if-eqz v14, :cond_10

    .line 531
    const/16 v17, 0x5

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 532
    invoke-static {v14}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->convertToUnicodeByteArray(Ljava/lang/String;)[B

    move-result-object v16

    .line 533
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    add-int/lit8 v10, v17, 0x3

    .line 534
    .restart local v10    # "length":I
    const/16 v17, 0x0

    shr-int/lit8 v18, v10, 0x8

    move/from16 v0, v18

    and-int/lit16 v0, v0, 0xff

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    aput-byte v18, v11, v17

    .line 535
    const/16 v17, 0x1

    and-int/lit16 v0, v10, 0xff

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    aput-byte v18, v11, v17

    .line 536
    invoke-virtual {v12, v11}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 537
    move-object/from16 v0, v16

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 538
    if-eqz p1, :cond_10

    .line 539
    const/16 v17, 0x5

    const/16 v18, 0x0

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v7, v0, v1}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    .line 544
    .end local v10    # "length":I
    :cond_10
    const/16 v17, 0x46

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, [B

    move-object/from16 v0, v17

    check-cast v0, [B

    move-object/from16 v16, v0

    .line 545
    if-eqz v16, :cond_11

    .line 546
    const/16 v17, 0x46

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 547
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    add-int/lit8 v10, v17, 0x3

    .line 548
    .restart local v10    # "length":I
    const/16 v17, 0x0

    shr-int/lit8 v18, v10, 0x8

    move/from16 v0, v18

    and-int/lit16 v0, v0, 0xff

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    aput-byte v18, v11, v17

    .line 549
    const/16 v17, 0x1

    and-int/lit16 v0, v10, 0xff

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    aput-byte v18, v11, v17

    .line 550
    invoke-virtual {v12, v11}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 551
    move-object/from16 v0, v16

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 552
    if-eqz p1, :cond_11

    .line 553
    const/16 v17, 0x46

    const/16 v18, 0x0

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v7, v0, v1}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    .line 558
    .end local v10    # "length":I
    :cond_11
    const/16 v17, 0x47

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, [B

    move-object/from16 v0, v17

    check-cast v0, [B

    move-object/from16 v16, v0

    .line 559
    if-eqz v16, :cond_12

    .line 560
    const/16 v17, 0x47

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 561
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    add-int/lit8 v10, v17, 0x3

    .line 562
    .restart local v10    # "length":I
    const/16 v17, 0x0

    shr-int/lit8 v18, v10, 0x8

    move/from16 v0, v18

    and-int/lit16 v0, v0, 0xff

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    aput-byte v18, v11, v17

    .line 563
    const/16 v17, 0x1

    and-int/lit16 v0, v10, 0xff

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    aput-byte v18, v11, v17

    .line 564
    invoke-virtual {v12, v11}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 565
    move-object/from16 v0, v16

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 566
    if-eqz p1, :cond_12

    .line 567
    const/16 v17, 0x47

    const/16 v18, 0x0

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v7, v0, v1}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    .line 572
    .end local v10    # "length":I
    :cond_12
    const/16 v17, 0x4a

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, [B

    move-object/from16 v0, v17

    check-cast v0, [B

    move-object/from16 v16, v0

    .line 573
    if-eqz v16, :cond_13

    .line 574
    const/16 v17, 0x4a

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 575
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    add-int/lit8 v10, v17, 0x3

    .line 576
    .restart local v10    # "length":I
    const/16 v17, 0x0

    shr-int/lit8 v18, v10, 0x8

    move/from16 v0, v18

    and-int/lit16 v0, v0, 0xff

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    aput-byte v18, v11, v17

    .line 577
    const/16 v17, 0x1

    and-int/lit16 v0, v10, 0xff

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    aput-byte v18, v11, v17

    .line 578
    invoke-virtual {v12, v11}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 579
    move-object/from16 v0, v16

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 580
    if-eqz p1, :cond_13

    .line 581
    const/16 v17, 0x4a

    const/16 v18, 0x0

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v7, v0, v1}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    .line 586
    .end local v10    # "length":I
    :cond_13
    const/16 v17, 0x4c

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, [B

    move-object/from16 v0, v17

    check-cast v0, [B

    move-object/from16 v16, v0

    .line 587
    if-eqz v16, :cond_14

    .line 588
    const/16 v17, 0x4c

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 589
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    add-int/lit8 v10, v17, 0x3

    .line 590
    .restart local v10    # "length":I
    const/16 v17, 0x0

    shr-int/lit8 v18, v10, 0x8

    move/from16 v0, v18

    and-int/lit16 v0, v0, 0xff

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    aput-byte v18, v11, v17

    .line 591
    const/16 v17, 0x1

    and-int/lit16 v0, v10, 0xff

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    aput-byte v18, v11, v17

    .line 592
    invoke-virtual {v12, v11}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 593
    move-object/from16 v0, v16

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 594
    if-eqz p1, :cond_14

    .line 595
    const/16 v17, 0x4c

    const/16 v18, 0x0

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v7, v0, v1}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    .line 600
    .end local v10    # "length":I
    :cond_14
    const/16 v17, 0x4f

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, [B

    move-object/from16 v0, v17

    check-cast v0, [B

    move-object/from16 v16, v0

    .line 601
    if-eqz v16, :cond_15

    .line 602
    const/16 v17, 0x4f

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 603
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    add-int/lit8 v10, v17, 0x3

    .line 604
    .restart local v10    # "length":I
    const/16 v17, 0x0

    shr-int/lit8 v18, v10, 0x8

    move/from16 v0, v18

    and-int/lit16 v0, v0, 0xff

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    aput-byte v18, v11, v17

    .line 605
    const/16 v17, 0x1

    and-int/lit16 v0, v10, 0xff

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    aput-byte v18, v11, v17

    .line 606
    invoke-virtual {v12, v11}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 607
    move-object/from16 v0, v16

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 608
    if-eqz p1, :cond_15

    .line 609
    const/16 v17, 0x4f

    const/16 v18, 0x0

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v7, v0, v1}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    .line 614
    .end local v10    # "length":I
    :cond_15
    const/4 v8, 0x0

    .restart local v8    # "i":I
    :goto_6
    const/16 v17, 0x10

    move/from16 v0, v17

    if-ge v8, v0, :cond_1a

    .line 617
    add-int/lit8 v17, v8, 0x30

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    move-result-object v17

    move-object/from16 v0, v17

    check-cast v0, Ljava/lang/String;

    move-object v14, v0

    .line 618
    if-eqz v14, :cond_16

    .line 619
    int-to-byte v0, v8

    move/from16 v17, v0

    add-int/lit8 v17, v17, 0x30

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 620
    invoke-static {v14}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->convertToUnicodeByteArray(Ljava/lang/String;)[B

    move-result-object v16

    .line 621
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    add-int/lit8 v10, v17, 0x3

    .line 622
    .restart local v10    # "length":I
    const/16 v17, 0x0

    shr-int/lit8 v18, v10, 0x8

    move/from16 v0, v18

    and-int/lit16 v0, v0, 0xff

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    aput-byte v18, v11, v17

    .line 623
    const/16 v17, 0x1

    and-int/lit16 v0, v10, 0xff

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    aput-byte v18, v11, v17

    .line 624
    invoke-virtual {v12, v11}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 625
    move-object/from16 v0, v16

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 626
    if-eqz p1, :cond_16

    .line 627
    add-int/lit8 v17, v8, 0x30

    const/16 v18, 0x0

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v7, v0, v1}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    .line 632
    .end local v10    # "length":I
    :cond_16
    add-int/lit8 v17, v8, 0x70

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, [B

    move-object/from16 v0, v17

    check-cast v0, [B

    move-object/from16 v16, v0

    .line 633
    if-eqz v16, :cond_17

    .line 634
    int-to-byte v0, v8

    move/from16 v17, v0

    add-int/lit8 v17, v17, 0x70

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 635
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    add-int/lit8 v10, v17, 0x3

    .line 636
    .restart local v10    # "length":I
    const/16 v17, 0x0

    shr-int/lit8 v18, v10, 0x8

    move/from16 v0, v18

    and-int/lit16 v0, v0, 0xff

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    aput-byte v18, v11, v17

    .line 637
    const/16 v17, 0x1

    and-int/lit16 v0, v10, 0xff

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    aput-byte v18, v11, v17

    .line 638
    invoke-virtual {v12, v11}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 639
    move-object/from16 v0, v16

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 640
    if-eqz p1, :cond_17

    .line 641
    add-int/lit8 v17, v8, 0x70

    const/16 v18, 0x0

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v7, v0, v1}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    .line 646
    .end local v10    # "length":I
    :cond_17
    add-int/lit16 v0, v8, 0xb0

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    move-result-object v17

    move-object/from16 v0, v17

    check-cast v0, Ljava/lang/Byte;

    move-object v4, v0

    .line 647
    if-eqz v4, :cond_18

    .line 648
    int-to-byte v0, v8

    move/from16 v17, v0

    move/from16 v0, v17

    add-int/lit16 v0, v0, 0xb0

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 649
    invoke-virtual {v4}, Ljava/lang/Byte;->byteValue()B

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 650
    if-eqz p1, :cond_18

    .line 651
    add-int/lit16 v0, v8, 0xb0

    move/from16 v17, v0

    const/16 v18, 0x0

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v7, v0, v1}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    .line 656
    :cond_18
    add-int/lit16 v0, v8, 0xf0

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    move-result-object v17

    move-object/from16 v0, v17

    check-cast v0, Ljava/lang/Long;

    move-object v9, v0

    .line 657
    if-eqz v9, :cond_19

    .line 658
    int-to-byte v0, v8

    move/from16 v17, v0

    move/from16 v0, v17

    add-int/lit16 v0, v0, 0xf0

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 659
    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->convertToByteArray(J)[B

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 660
    if-eqz p1, :cond_19

    .line 661
    add-int/lit16 v0, v8, 0xf0

    move/from16 v17, v0

    const/16 v18, 0x0

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v7, v0, v1}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 614
    :cond_19
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_6

    .line 494
    .end local v2    # "buffer":Ljava/lang/StringBuffer;
    .restart local v3    # "buffer":Ljava/lang/StringBuffer;
    .restart local v15    # "temp":I
    :catch_2
    move-exception v6

    .line 495
    .restart local v6    # "e":Ljava/io/UnsupportedEncodingException;
    :try_start_c
    throw v6
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 719
    .end local v6    # "e":Ljava/io/UnsupportedEncodingException;
    .end local v8    # "i":I
    .end local v15    # "temp":I
    :catch_3
    move-exception v17

    move-object v2, v3

    .end local v3    # "buffer":Ljava/lang/StringBuffer;
    .restart local v2    # "buffer":Ljava/lang/StringBuffer;
    goto/16 :goto_2

    .line 667
    .restart local v8    # "i":I
    :cond_1a
    :try_start_d
    iget-object v0, v7, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthChall:[B

    move-object/from16 v17, v0

    if-eqz v17, :cond_1b

    .line 668
    const/16 v17, 0x4d

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 669
    iget-object v0, v7, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthChall:[B

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v17, v0

    add-int/lit8 v10, v17, 0x3

    .line 670
    .restart local v10    # "length":I
    const/16 v17, 0x0

    shr-int/lit8 v18, v10, 0x8

    move/from16 v0, v18

    and-int/lit16 v0, v0, 0xff

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    aput-byte v18, v11, v17

    .line 671
    const/16 v17, 0x1

    and-int/lit16 v0, v10, 0xff

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    aput-byte v18, v11, v17

    .line 672
    invoke-virtual {v12, v11}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 673
    iget-object v0, v7, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthChall:[B

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 674
    if-eqz p1, :cond_1b

    .line 675
    const/16 v17, 0x0

    move-object/from16 v0, v17

    iput-object v0, v7, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthChall:[B

    .line 680
    .end local v10    # "length":I
    :cond_1b
    iget-object v0, v7, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    move-object/from16 v17, v0

    if-eqz v17, :cond_1c

    .line 681
    const/16 v17, 0x4e

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 682
    iget-object v0, v7, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v17, v0

    add-int/lit8 v10, v17, 0x3

    .line 683
    .restart local v10    # "length":I
    const/16 v17, 0x0

    shr-int/lit8 v18, v10, 0x8

    move/from16 v0, v18

    and-int/lit16 v0, v0, 0xff

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    aput-byte v18, v11, v17

    .line 684
    const/16 v17, 0x1

    and-int/lit16 v0, v10, 0xff

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    aput-byte v18, v11, v17

    .line 685
    invoke-virtual {v12, v11}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 686
    iget-object v0, v7, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 687
    if-eqz p1, :cond_1c

    .line 688
    const/16 v17, 0x0

    move-object/from16 v0, v17

    iput-object v0, v7, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    .line 700
    .end local v10    # "length":I
    :cond_1c
    const/16 v17, 0x97

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    move-result-object v17

    move-object/from16 v0, v17

    check-cast v0, Ljava/lang/Byte;

    move-object v4, v0

    .line 701
    if-eqz v4, :cond_1d

    .line 702
    const/16 v17, -0x69

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 703
    invoke-virtual {v4}, Ljava/lang/Byte;->byteValue()B

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 704
    if-eqz p1, :cond_1d

    .line 705
    const/16 v17, 0x97

    const/16 v18, 0x0

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v7, v0, v1}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    .line 710
    :cond_1d
    const/16 v17, 0x98

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    move-result-object v17

    move-object/from16 v0, v17

    check-cast v0, Ljava/lang/Byte;

    move-object v4, v0

    .line 711
    if-eqz v4, :cond_1e

    .line 712
    const/16 v17, -0x68

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 713
    invoke-virtual {v4}, Ljava/lang/Byte;->byteValue()B

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 714
    if-eqz p1, :cond_1e

    .line 715
    const/16 v17, 0x98

    const/16 v18, 0x0

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v7, v0, v1}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_0
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 721
    :cond_1e
    invoke-virtual {v12}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v13

    .line 723
    :try_start_e
    invoke-virtual {v12}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_4

    goto/16 :goto_3

    .line 724
    :catch_4
    move-exception v17

    goto/16 :goto_3

    .end local v8    # "i":I
    :catch_5
    move-exception v17

    goto/16 :goto_3

    :catch_6
    move-exception v18

    goto/16 :goto_5

    .line 721
    .end local v2    # "buffer":Ljava/lang/StringBuffer;
    .restart local v3    # "buffer":Ljava/lang/StringBuffer;
    :catchall_1
    move-exception v17

    move-object v2, v3

    .end local v3    # "buffer":Ljava/lang/StringBuffer;
    .restart local v2    # "buffer":Ljava/lang/StringBuffer;
    goto/16 :goto_4
.end method

.method public static findHeaderEnd([BII)I
    .locals 6
    .param p0, "headerArray"    # [B
    .param p1, "start"    # I
    .param p2, "maxSize"    # I

    .prologue
    .line 744
    const/4 v0, 0x0

    .line 745
    .local v0, "fullLength":I
    const/4 v3, -0x1

    .line 746
    .local v3, "lastLength":I
    move v2, p1

    .line 747
    .local v2, "index":I
    const/4 v4, 0x0

    .line 751
    .local v4, "length":I
    :goto_0
    if-ge v0, p2, :cond_3

    array-length v5, p0

    if-ge v2, v5, :cond_3

    .line 752
    aget-byte v5, p0, v2

    if-gez v5, :cond_0

    aget-byte v5, p0, v2

    add-int/lit16 v1, v5, 0x100

    .line 753
    .local v1, "headerID":I
    :goto_1
    move v3, v0

    .line 755
    and-int/lit16 v5, v1, 0xc0

    sparse-switch v5, :sswitch_data_0

    goto :goto_0

    .line 761
    :sswitch_0
    add-int/lit8 v2, v2, 0x1

    .line 762
    aget-byte v5, p0, v2

    if-gez v5, :cond_1

    aget-byte v5, p0, v2

    add-int/lit16 v4, v5, 0x100

    .line 764
    :goto_2
    shl-int/lit8 v4, v4, 0x8

    .line 765
    add-int/lit8 v2, v2, 0x1

    .line 766
    aget-byte v5, p0, v2

    if-gez v5, :cond_2

    aget-byte v5, p0, v2

    add-int/lit16 v5, v5, 0x100

    :goto_3
    add-int/2addr v4, v5

    .line 768
    add-int/lit8 v4, v4, -0x3

    .line 769
    add-int/lit8 v2, v2, 0x1

    .line 770
    add-int/2addr v2, v4

    .line 771
    add-int/lit8 v5, v4, 0x3

    add-int/2addr v0, v5

    .line 772
    goto :goto_0

    .line 752
    .end local v1    # "headerID":I
    :cond_0
    aget-byte v1, p0, v2

    goto :goto_1

    .line 762
    .restart local v1    # "headerID":I
    :cond_1
    aget-byte v4, p0, v2

    goto :goto_2

    .line 766
    :cond_2
    aget-byte v5, p0, v2

    goto :goto_3

    .line 776
    :sswitch_1
    add-int/lit8 v2, v2, 0x1

    .line 777
    add-int/lit8 v2, v2, 0x1

    .line 778
    add-int/lit8 v0, v0, 0x2

    .line 779
    goto :goto_0

    .line 783
    :sswitch_2
    add-int/lit8 v2, v2, 0x5

    .line 784
    add-int/lit8 v0, v0, 0x5

    goto :goto_0

    .line 794
    .end local v1    # "headerID":I
    :cond_3
    if-nez v3, :cond_5

    .line 801
    if-ge v0, p2, :cond_4

    .line 802
    array-length v5, p0

    .line 807
    :goto_4
    return v5

    .line 804
    :cond_4
    const/4 v5, -0x1

    goto :goto_4

    .line 807
    :cond_5
    add-int v5, v3, p1

    goto :goto_4

    .line 755
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x40 -> :sswitch_0
        0x80 -> :sswitch_1
        0xc0 -> :sswitch_2
    .end sparse-switch
.end method

.method public static findTag(B[B)I
    .locals 4
    .param p0, "tag"    # B
    .param p1, "value"    # [B

    .prologue
    const/4 v2, -0x1

    .line 906
    const/4 v1, 0x0

    .line 908
    .local v1, "length":I
    if-nez p1, :cond_1

    move v0, v2

    .line 923
    :cond_0
    :goto_0
    return v0

    .line 912
    :cond_1
    const/4 v0, 0x0

    .line 914
    .local v0, "index":I
    :goto_1
    array-length v3, p1

    if-ge v0, v3, :cond_2

    aget-byte v3, p1, v0

    if-eq v3, p0, :cond_2

    .line 915
    add-int/lit8 v3, v0, 0x1

    aget-byte v3, p1, v3

    and-int/lit16 v1, v3, 0xff

    .line 916
    add-int/lit8 v3, v1, 0x2

    add-int/2addr v0, v3

    goto :goto_1

    .line 919
    :cond_2
    array-length v3, p1

    if-lt v0, v3, :cond_0

    move v0, v2

    .line 920
    goto :goto_0
.end method

.method public static getMaxRxPacketSize(Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;)I
    .locals 2
    .param p0, "transport"    # Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;

    .prologue
    .line 1075
    invoke-interface {p0}, Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;->getMaxReceivePacketSize()I

    move-result v0

    .line 1076
    .local v0, "size":I
    invoke-static {v0}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->validateMaxPacketSize(I)I

    move-result v1

    return v1
.end method

.method public static getMaxTxPacketSize(Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;)I
    .locals 2
    .param p0, "transport"    # Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;

    .prologue
    .line 1065
    invoke-interface {p0}, Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;->getMaxTransmitPacketSize()I

    move-result v0

    .line 1066
    .local v0, "size":I
    invoke-static {v0}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->validateMaxPacketSize(I)I

    move-result v1

    return v1
.end method

.method public static getTagValue(B[B)[B
    .locals 4
    .param p0, "tag"    # B
    .param p1, "triplet"    # [B

    .prologue
    .line 884
    invoke-static {p0, p1}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->findTag(B[B)I

    move-result v0

    .line 885
    .local v0, "index":I
    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    .line 886
    const/4 v2, 0x0

    .line 896
    :goto_0
    return-object v2

    .line 889
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 890
    aget-byte v3, p1, v0

    and-int/lit16 v1, v3, 0xff

    .line 892
    .local v1, "length":I
    new-array v2, v1, [B

    .line 893
    .local v2, "result":[B
    add-int/lit8 v0, v0, 0x1

    .line 894
    const/4 v3, 0x0

    invoke-static {p1, v0, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method public static updateHeaderSet(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;[B)[B
    .locals 18
    .param p0, "header"    # Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    .param p1, "headerArray"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 184
    const/4 v7, 0x0

    .line 185
    .local v7, "index":I
    const/4 v8, 0x0

    .line 187
    .local v8, "length":I
    const/4 v11, 0x0

    .line 188
    .local v11, "value":[B
    const/4 v2, 0x0

    .line 189
    .local v2, "body":[B
    move-object/from16 v6, p0

    .line 191
    .local v6, "headerImpl":Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    :goto_0
    :try_start_0
    move-object/from16 v0, p1

    array-length v12, v0

    if-ge v7, v12, :cond_7

    .line 192
    aget-byte v12, p1, v7

    and-int/lit16 v5, v12, 0xff

    .line 193
    .local v5, "headerID":I
    and-int/lit16 v12, v5, 0xc0

    sparse-switch v12, :sswitch_data_0

    goto :goto_0

    .line 206
    :sswitch_0
    const/4 v10, 0x1

    .line 207
    .local v10, "trimTail":Z
    add-int/lit8 v7, v7, 0x1

    .line 208
    aget-byte v12, p1, v7

    and-int/lit16 v8, v12, 0xff

    .line 209
    shl-int/lit8 v8, v8, 0x8

    .line 210
    add-int/lit8 v7, v7, 0x1

    .line 211
    aget-byte v12, p1, v7

    and-int/lit16 v12, v12, 0xff

    add-int/2addr v8, v12

    .line 212
    add-int/lit8 v8, v8, -0x3

    .line 213
    add-int/lit8 v7, v7, 0x1

    .line 214
    new-array v11, v8, [B

    .line 215
    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v7, v11, v12, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 216
    if-eqz v8, :cond_0

    if-lez v8, :cond_1

    add-int/lit8 v12, v8, -0x1

    aget-byte v12, v11, v12

    if-eqz v12, :cond_1

    .line 217
    :cond_0
    const/4 v10, 0x0

    .line 219
    :cond_1
    packed-switch v5, :pswitch_data_0

    .line 282
    :pswitch_0
    and-int/lit16 v12, v5, 0xc0

    if-nez v12, :cond_4

    .line 283
    const/4 v12, 0x1

    invoke-static {v11, v12}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->convertToUnicode([BZ)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v6, v5, v12}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 290
    :goto_1
    add-int/2addr v7, v8

    .line 291
    goto :goto_0

    .line 223
    :pswitch_1
    if-nez v10, :cond_2

    .line 224
    :try_start_1
    new-instance v12, Ljava/lang/String;

    const/4 v13, 0x0

    array-length v14, v11

    const-string v15, "ISO8859_1"

    invoke-direct {v12, v11, v13, v14, v15}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    invoke-virtual {v6, v5, v12}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 230
    :catch_0
    move-exception v4

    .line 231
    .local v4, "e":Ljava/io/UnsupportedEncodingException;
    :try_start_2
    throw v4
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 341
    .end local v4    # "e":Ljava/io/UnsupportedEncodingException;
    .end local v5    # "headerID":I
    .end local v10    # "trimTail":Z
    :catch_1
    move-exception v4

    .line 342
    .local v4, "e":Ljava/io/IOException;
    new-instance v12, Ljava/io/IOException;

    const-string v13, "Header was not formatted properly"

    invoke-direct {v12, v13, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v12

    .line 227
    .end local v4    # "e":Ljava/io/IOException;
    .restart local v5    # "headerID":I
    .restart local v10    # "trimTail":Z
    :cond_2
    :try_start_3
    new-instance v12, Ljava/lang/String;

    const/4 v13, 0x0

    array-length v14, v11

    add-int/lit8 v14, v14, -0x1

    const-string v15, "ISO8859_1"

    invoke-direct {v12, v11, v13, v14, v15}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    invoke-virtual {v6, v5, v12}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V
    :try_end_3
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 236
    :pswitch_2
    :try_start_4
    new-array v12, v8, [B

    iput-object v12, v6, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthChall:[B

    .line 237
    iget-object v12, v6, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthChall:[B

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v7, v12, v13, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_1

    .line 242
    :pswitch_3
    new-array v12, v8, [B

    iput-object v12, v6, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    .line 243
    iget-object v12, v6, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v7, v12, v13, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_1

    .line 250
    :pswitch_4
    add-int/lit8 v12, v8, 0x1

    new-array v2, v12, [B

    .line 251
    const/4 v12, 0x0

    int-to-byte v13, v5

    aput-byte v13, v2, v12

    .line 252
    const/4 v12, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v7, v2, v12, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 257
    :pswitch_5
    :try_start_5
    new-instance v3, Ljava/lang/String;

    const-string v12, "ISO8859_1"

    invoke-direct {v3, v11, v12}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 258
    .local v3, "dateString":Ljava/lang/String;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v9

    .line 259
    .local v9, "temp":Ljava/util/Calendar;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v12

    const/16 v13, 0x10

    if-ne v12, v13, :cond_3

    const/16 v12, 0xf

    .line 260
    invoke-virtual {v3, v12}, Ljava/lang/String;->charAt(I)C

    move-result v12

    const/16 v13, 0x5a

    if-ne v12, v13, :cond_3

    .line 261
    const-string v12, "UTC"

    invoke-static {v12}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v12

    invoke-virtual {v9, v12}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 263
    :cond_3
    const/4 v12, 0x1

    const/4 v13, 0x0

    const/4 v14, 0x4

    invoke-virtual {v3, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    invoke-virtual {v9, v12, v13}, Ljava/util/Calendar;->set(II)V

    .line 265
    const/4 v12, 0x2

    const/4 v13, 0x4

    const/4 v14, 0x6

    invoke-virtual {v3, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    invoke-virtual {v9, v12, v13}, Ljava/util/Calendar;->set(II)V

    .line 267
    const/4 v12, 0x5

    const/4 v13, 0x6

    const/16 v14, 0x8

    .line 268
    invoke-virtual {v3, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    .line 267
    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    invoke-virtual {v9, v12, v13}, Ljava/util/Calendar;->set(II)V

    .line 269
    const/16 v12, 0xb

    const/16 v13, 0x9

    const/16 v14, 0xb

    .line 270
    invoke-virtual {v3, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    .line 269
    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    invoke-virtual {v9, v12, v13}, Ljava/util/Calendar;->set(II)V

    .line 271
    const/16 v12, 0xc

    const/16 v13, 0xb

    const/16 v14, 0xd

    .line 272
    invoke-virtual {v3, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    .line 271
    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    invoke-virtual {v9, v12, v13}, Ljava/util/Calendar;->set(II)V

    .line 273
    const/16 v12, 0xd

    const/16 v13, 0xd

    const/16 v14, 0xf

    .line 274
    invoke-virtual {v3, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    .line 273
    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    invoke-virtual {v9, v12, v13}, Ljava/util/Calendar;->set(II)V

    .line 275
    const/16 v12, 0x44

    invoke-virtual {v6, v12, v9}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V
    :try_end_5
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_1

    .line 276
    .end local v3    # "dateString":Ljava/lang/String;
    .end local v9    # "temp":Ljava/util/Calendar;
    :catch_2
    move-exception v4

    .line 277
    .local v4, "e":Ljava/io/UnsupportedEncodingException;
    :try_start_6
    throw v4

    .line 286
    .end local v4    # "e":Ljava/io/UnsupportedEncodingException;
    :cond_4
    invoke-virtual {v6, v5, v11}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto/16 :goto_1

    .line 298
    .end local v10    # "trimTail":Z
    :sswitch_1
    add-int/lit8 v7, v7, 0x1

    .line 300
    :try_start_7
    aget-byte v12, p1, v7

    invoke-static {v12}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v12

    invoke-virtual {v6, v5, v12}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    .line 304
    :goto_2
    add-int/lit8 v7, v7, 0x1

    .line 305
    goto/16 :goto_0

    .line 313
    :sswitch_2
    add-int/lit8 v7, v7, 0x1

    .line 314
    const/4 v12, 0x4

    :try_start_8
    new-array v11, v12, [B

    .line 315
    const/4 v12, 0x0

    const/4 v13, 0x4

    move-object/from16 v0, p1

    invoke-static {v0, v7, v11, v12, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1

    .line 317
    const/16 v12, 0xc4

    if-eq v5, v12, :cond_6

    .line 320
    const/16 v12, 0xcb

    if-ne v5, v12, :cond_5

    .line 321
    const/4 v12, 0x4

    :try_start_9
    new-array v12, v12, [B

    iput-object v12, v6, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mConnectionID:[B

    .line 322
    const/4 v12, 0x0

    iget-object v13, v6, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mConnectionID:[B

    const/4 v14, 0x0

    const/4 v15, 0x4

    invoke-static {v11, v12, v13, v14, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 336
    :goto_3
    add-int/lit8 v7, v7, 0x4

    goto/16 :goto_0

    .line 325
    :cond_5
    invoke-static {v11}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->convertToLong([B)J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    .line 324
    invoke-virtual {v6, v5, v12}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1

    goto :goto_3

    .line 332
    :catch_3
    move-exception v4

    .line 334
    .local v4, "e":Ljava/lang/Exception;
    :try_start_a
    new-instance v12, Ljava/io/IOException;

    const-string v13, "Header was not formatted properly"

    invoke-direct {v12, v13, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v12
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1

    .line 328
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_6
    :try_start_b
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v9

    .line 329
    .restart local v9    # "temp":Ljava/util/Calendar;
    new-instance v12, Ljava/util/Date;

    invoke-static {v11}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->convertToLong([B)J

    move-result-wide v14

    const-wide/16 v16, 0x3e8

    mul-long v14, v14, v16

    invoke-direct {v12, v14, v15}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v9, v12}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 330
    const/16 v12, 0xc4

    invoke-virtual {v6, v12, v9}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_3
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_1

    goto :goto_3

    .line 345
    .end local v5    # "headerID":I
    .end local v9    # "temp":Ljava/util/Calendar;
    :cond_7
    return-object v2

    .line 301
    .restart local v5    # "headerID":I
    :catch_4
    move-exception v12

    goto :goto_2

    .line 193
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x40 -> :sswitch_0
        0x80 -> :sswitch_1
        0xc0 -> :sswitch_2
    .end sparse-switch

    .line 219
    :pswitch_data_0
    .packed-switch 0x42
        :pswitch_1
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static validateMaxPacketSize(I)I
    .locals 4
    .param p0, "size"    # I

    .prologue
    const/16 v3, 0xff

    .line 1083
    const/4 v0, -0x1

    if-eq p0, v0, :cond_0

    .line 1084
    if-ge p0, v3, :cond_1

    .line 1085
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is less that the lower limit: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1090
    :cond_0
    const p0, 0xfffe

    .end local p0    # "size":I
    :cond_1
    return p0
.end method
