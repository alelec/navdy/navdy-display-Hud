.class public Lcom/navdy/hud/app/bluetooth/obex/ObexSession;
.super Ljava/lang/Object;
.source "ObexSession.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ObexSession"

.field private static final V:Z


# instance fields
.field protected mAuthenticator:Lcom/navdy/hud/app/bluetooth/obex/Authenticator;

.field protected mChallengeDigest:[B


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleAuthChall(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;)Z
    .locals 18
    .param p1, "header"    # Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 68
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/bluetooth/obex/ObexSession;->mAuthenticator:Lcom/navdy/hud/app/bluetooth/obex/Authenticator;

    if-nez v13, :cond_0

    .line 69
    const/4 v13, 0x0

    .line 186
    :goto_0
    return v13

    .line 81
    :cond_0
    const/4 v13, 0x0

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthChall:[B

    invoke-static {v13, v14}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->getTagValue(B[B)[B

    move-result-object v1

    .line 82
    .local v1, "challenge":[B
    const/4 v13, 0x1

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthChall:[B

    invoke-static {v13, v14}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->getTagValue(B[B)[B

    move-result-object v7

    .line 83
    .local v7, "option":[B
    const/4 v13, 0x2

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthChall:[B

    invoke-static {v13, v14}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->getTagValue(B[B)[B

    move-result-object v2

    .line 85
    .local v2, "description":[B
    const/4 v9, 0x0

    .line 86
    .local v9, "realm":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 87
    array-length v13, v2

    add-int/lit8 v13, v13, -0x1

    new-array v10, v13, [B

    .line 88
    .local v10, "realmString":[B
    const/4 v13, 0x1

    const/4 v14, 0x0

    array-length v15, v10

    invoke-static {v2, v13, v10, v14, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 90
    const/4 v13, 0x0

    aget-byte v13, v2, v13

    and-int/lit16 v13, v13, 0xff

    sparse-switch v13, :sswitch_data_0

    .line 110
    new-instance v13, Ljava/io/IOException;

    const-string v14, "Unsupported Encoding Scheme"

    invoke-direct {v13, v14}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 98
    :sswitch_0
    :try_start_0
    new-instance v9, Ljava/lang/String;

    .end local v9    # "realm":Ljava/lang/String;
    const-string v13, "ISO8859_1"

    invoke-direct {v9, v10, v13}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 114
    .end local v10    # "realmString":[B
    .restart local v9    # "realm":Ljava/lang/String;
    :cond_1
    :goto_1
    const/4 v6, 0x0

    .line 115
    .local v6, "isUserIDRequired":Z
    const/4 v5, 0x1

    .line 116
    .local v5, "isFullAccess":Z
    if-eqz v7, :cond_3

    .line 117
    const/4 v13, 0x0

    aget-byte v13, v7, v13

    and-int/lit8 v13, v13, 0x1

    if-eqz v13, :cond_2

    .line 118
    const/4 v6, 0x1

    .line 121
    :cond_2
    const/4 v13, 0x0

    aget-byte v13, v7, v13

    and-int/lit8 v13, v13, 0x2

    if-eqz v13, :cond_3

    .line 122
    const/4 v5, 0x0

    .line 126
    :cond_3
    const/4 v11, 0x0

    .line 127
    .local v11, "result":Lcom/navdy/hud/app/bluetooth/obex/PasswordAuthentication;
    const/4 v13, 0x0

    move-object/from16 v0, p1

    iput-object v13, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthChall:[B

    .line 130
    :try_start_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/hud/app/bluetooth/obex/ObexSession;->mAuthenticator:Lcom/navdy/hud/app/bluetooth/obex/Authenticator;

    .line 131
    invoke-interface {v13, v9, v6, v5}, Lcom/navdy/hud/app/bluetooth/obex/Authenticator;->onAuthenticationChallenge(Ljava/lang/String;ZZ)Lcom/navdy/hud/app/bluetooth/obex/PasswordAuthentication;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v11

    .line 140
    if-nez v11, :cond_4

    .line 141
    const/4 v13, 0x0

    goto :goto_0

    .line 99
    .end local v5    # "isFullAccess":Z
    .end local v6    # "isUserIDRequired":Z
    .end local v9    # "realm":Ljava/lang/String;
    .end local v11    # "result":Lcom/navdy/hud/app/bluetooth/obex/PasswordAuthentication;
    .restart local v10    # "realmString":[B
    :catch_0
    move-exception v4

    .line 100
    .local v4, "e":Ljava/lang/Exception;
    new-instance v13, Ljava/io/IOException;

    const-string v14, "Unsupported Encoding Scheme"

    invoke-direct {v13, v14}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 106
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v9    # "realm":Ljava/lang/String;
    :sswitch_1
    const/4 v13, 0x0

    invoke-static {v10, v13}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->convertToUnicode([BZ)Ljava/lang/String;

    move-result-object v9

    .line 107
    goto :goto_1

    .line 132
    .end local v10    # "realmString":[B
    .restart local v5    # "isFullAccess":Z
    .restart local v6    # "isUserIDRequired":Z
    .restart local v11    # "result":Lcom/navdy/hud/app/bluetooth/obex/PasswordAuthentication;
    :catch_1
    move-exception v4

    .line 134
    .restart local v4    # "e":Ljava/lang/Exception;
    const/4 v13, 0x0

    goto :goto_0

    .line 144
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_4
    invoke-virtual {v11}, Lcom/navdy/hud/app/bluetooth/obex/PasswordAuthentication;->getPassword()[B

    move-result-object v8

    .line 145
    .local v8, "password":[B
    if-nez v8, :cond_5

    .line 146
    const/4 v13, 0x0

    goto :goto_0

    .line 149
    :cond_5
    invoke-virtual {v11}, Lcom/navdy/hud/app/bluetooth/obex/PasswordAuthentication;->getUserName()[B

    move-result-object v12

    .line 159
    .local v12, "userName":[B
    if-eqz v12, :cond_6

    .line 160
    array-length v13, v12

    add-int/lit8 v13, v13, 0x26

    new-array v13, v13, [B

    move-object/from16 v0, p1

    iput-object v13, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    .line 161
    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    const/16 v14, 0x24

    const/4 v15, 0x1

    aput-byte v15, v13, v14

    .line 162
    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    const/16 v14, 0x25

    array-length v15, v12

    int-to-byte v15, v15

    aput-byte v15, v13, v14

    .line 163
    const/4 v13, 0x0

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    const/16 v15, 0x26

    array-length v0, v12

    move/from16 v16, v0

    invoke-static/range {v12 .. v16}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 169
    :goto_2
    array-length v13, v1

    array-length v14, v8

    add-int/2addr v13, v14

    add-int/lit8 v13, v13, 0x1

    new-array v3, v13, [B

    .line 170
    .local v3, "digest":[B
    const/4 v13, 0x0

    const/4 v14, 0x0

    array-length v15, v1

    invoke-static {v1, v13, v3, v14, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 172
    array-length v13, v1

    const/16 v14, 0x3a

    aput-byte v14, v3, v13

    .line 173
    const/4 v13, 0x0

    array-length v14, v1

    add-int/lit8 v14, v14, 0x1

    array-length v15, v8

    invoke-static {v8, v13, v3, v14, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 176
    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    const/4 v14, 0x0

    const/4 v15, 0x0

    aput-byte v15, v13, v14

    .line 177
    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    const/4 v14, 0x1

    const/16 v15, 0x10

    aput-byte v15, v13, v14

    .line 179
    invoke-static {v3}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->computeMd5Hash([B)[B

    move-result-object v13

    const/4 v14, 0x0

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    const/16 v16, 0x2

    const/16 v17, 0x10

    invoke-static/range {v13 .. v17}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 182
    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    const/16 v14, 0x12

    const/4 v15, 0x2

    aput-byte v15, v13, v14

    .line 183
    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    const/16 v14, 0x13

    const/16 v15, 0x10

    aput-byte v15, v13, v14

    .line 184
    const/4 v13, 0x0

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    const/16 v15, 0x14

    const/16 v16, 0x10

    move/from16 v0, v16

    invoke-static {v1, v13, v14, v15, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 186
    const/4 v13, 0x1

    goto/16 :goto_0

    .line 165
    .end local v3    # "digest":[B
    :cond_6
    const/16 v13, 0x24

    new-array v13, v13, [B

    move-object/from16 v0, p1

    iput-object v13, v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->mAuthResp:[B

    goto :goto_2

    .line 90
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_0
        0xff -> :sswitch_1
    .end sparse-switch
.end method

.method public handleAuthResp([B)Z
    .locals 10
    .param p1, "authResp"    # [B

    .prologue
    const/4 v6, 0x1

    const/16 v9, 0x10

    const/4 v5, 0x0

    .line 197
    iget-object v7, p0, Lcom/navdy/hud/app/bluetooth/obex/ObexSession;->mAuthenticator:Lcom/navdy/hud/app/bluetooth/obex/Authenticator;

    if-nez v7, :cond_1

    .line 222
    :cond_0
    :goto_0
    return v5

    .line 201
    :cond_1
    iget-object v7, p0, Lcom/navdy/hud/app/bluetooth/obex/ObexSession;->mAuthenticator:Lcom/navdy/hud/app/bluetooth/obex/Authenticator;

    invoke-static {v6, p1}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->getTagValue(B[B)[B

    move-result-object v8

    invoke-interface {v7, v8}, Lcom/navdy/hud/app/bluetooth/obex/Authenticator;->onAuthenticationResponse([B)[B

    move-result-object v1

    .line 203
    .local v1, "correctPassword":[B
    if-eqz v1, :cond_0

    .line 207
    array-length v7, v1

    add-int/lit8 v7, v7, 0x10

    new-array v4, v7, [B

    .line 209
    .local v4, "temp":[B
    iget-object v7, p0, Lcom/navdy/hud/app/bluetooth/obex/ObexSession;->mChallengeDigest:[B

    invoke-static {v7, v5, v4, v5, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 210
    array-length v7, v1

    invoke-static {v1, v5, v4, v9, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 212
    invoke-static {v4}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->computeMd5Hash([B)[B

    move-result-object v2

    .line 213
    .local v2, "correctResponse":[B
    invoke-static {v5, p1}, Lcom/navdy/hud/app/bluetooth/obex/ObexHelper;->getTagValue(B[B)[B

    move-result-object v0

    .line 216
    .local v0, "actualResponse":[B
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v9, :cond_2

    .line 217
    aget-byte v7, v2, v3

    aget-byte v8, v0, v3

    if-ne v7, v8, :cond_0

    .line 216
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    move v5, v6

    .line 222
    goto :goto_0
.end method
