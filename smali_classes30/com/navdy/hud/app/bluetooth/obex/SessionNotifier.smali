.class public interface abstract Lcom/navdy/hud/app/bluetooth/obex/SessionNotifier;
.super Ljava/lang/Object;
.source "SessionNotifier.java"


# virtual methods
.method public abstract acceptAndOpen(Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;)Lcom/navdy/hud/app/bluetooth/obex/ObexSession;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract acceptAndOpen(Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;Lcom/navdy/hud/app/bluetooth/obex/Authenticator;)Lcom/navdy/hud/app/bluetooth/obex/ObexSession;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
