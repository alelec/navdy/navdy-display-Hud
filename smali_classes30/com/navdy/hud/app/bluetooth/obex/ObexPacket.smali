.class public Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;
.super Ljava/lang/Object;
.source "ObexPacket.java"


# instance fields
.field public mHeaderId:I

.field public mLength:I

.field public mPayload:[B


# direct methods
.method private constructor <init>(II)V
    .locals 1
    .param p1, "headerId"    # I
    .param p2, "length"    # I

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;->mPayload:[B

    .line 29
    iput p1, p0, Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;->mHeaderId:I

    .line 30
    iput p2, p0, Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;->mLength:I

    .line 31
    return-void
.end method

.method public static read(ILjava/io/InputStream;)Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;
    .locals 6
    .param p0, "headerId"    # I
    .param p1, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 54
    .local v1, "length":I
    shl-int/lit8 v4, v1, 0x8

    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v5

    add-int v1, v4, v5

    .line 56
    new-instance v2, Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;

    invoke-direct {v2, p0, v1}, Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;-><init>(II)V

    .line 59
    .local v2, "newPacket":Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;
    const/4 v3, 0x0

    .line 60
    .local v3, "temp":[B
    const/4 v4, 0x3

    if-le v1, v4, :cond_0

    .line 62
    add-int/lit8 v4, v1, -0x3

    new-array v3, v4, [B

    .line 63
    invoke-virtual {p1, v3}, Ljava/io/InputStream;->read([B)I

    move-result v0

    .line 64
    .local v0, "bytesReceived":I
    :goto_0
    array-length v4, v3

    if-eq v0, v4, :cond_0

    .line 65
    array-length v4, v3

    sub-int/2addr v4, v0

    invoke-virtual {p1, v3, v0, v4}, Ljava/io/InputStream;->read([BII)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_0

    .line 68
    .end local v0    # "bytesReceived":I
    :cond_0
    iput-object v3, v2, Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;->mPayload:[B

    .line 69
    return-object v2
.end method

.method public static read(Ljava/io/InputStream;)Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;
    .locals 2
    .param p0, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 41
    .local v0, "headerId":I
    invoke-static {v0, p0}, Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;->read(ILjava/io/InputStream;)Lcom/navdy/hud/app/bluetooth/obex/ObexPacket;

    move-result-object v1

    return-object v1
.end method
