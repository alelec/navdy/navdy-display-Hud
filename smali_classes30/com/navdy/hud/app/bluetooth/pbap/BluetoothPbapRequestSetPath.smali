.class final Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestSetPath;
.super Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;
.source "BluetoothPbapRequestSetPath.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestSetPath$SetPathDir;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "BTPbapReqSetPath"


# instance fields
.field private mDir:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestSetPath$SetPathDir;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;-><init>()V

    .line 39
    sget-object v0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestSetPath$SetPathDir;->DOWN:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestSetPath$SetPathDir;

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestSetPath;->mDir:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestSetPath$SetPathDir;

    .line 40
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestSetPath;->mHeaderSet:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1
    .param p1, "goUp"    # Z

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;-><init>()V

    .line 44
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestSetPath;->mHeaderSet:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    invoke-virtual {v0}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setEmptyNameHeader()V

    .line 45
    if-eqz p1, :cond_0

    .line 46
    sget-object v0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestSetPath$SetPathDir;->UP:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestSetPath$SetPathDir;

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestSetPath;->mDir:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestSetPath$SetPathDir;

    .line 50
    :goto_0
    return-void

    .line 48
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestSetPath$SetPathDir;->ROOT:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestSetPath$SetPathDir;

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestSetPath;->mDir:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestSetPath$SetPathDir;

    goto :goto_0
.end method


# virtual methods
.method public execute(Lcom/navdy/hud/app/bluetooth/obex/ClientSession;)V
    .locals 5
    .param p1, "session"    # Lcom/navdy/hud/app/bluetooth/obex/ClientSession;

    .prologue
    .line 54
    const-string v2, "BTPbapReqSetPath"

    const-string v3, "execute"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    const/4 v1, 0x0

    .line 59
    .local v1, "hs":Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    :try_start_0
    sget-object v2, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestSetPath$1;->$SwitchMap$com$navdy$hud$app$bluetooth$pbap$BluetoothPbapRequestSetPath$SetPathDir:[I

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestSetPath;->mDir:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestSetPath$SetPathDir;

    invoke-virtual {v3}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestSetPath$SetPathDir;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 69
    :goto_0
    invoke-virtual {v1}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->getResponseCode()I

    move-result v2

    iput v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestSetPath;->mResponseCode:I

    .line 73
    :goto_1
    return-void

    .line 62
    :pswitch_0
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestSetPath;->mHeaderSet:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {p1, v2, v3, v4}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->setPath(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;ZZ)Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    move-result-object v1

    .line 63
    goto :goto_0

    .line 65
    :pswitch_1
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestSetPath;->mHeaderSet:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {p1, v2, v3, v4}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->setPath(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;ZZ)Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 70
    :catch_0
    move-exception v0

    .line 71
    .local v0, "e":Ljava/io/IOException;
    const/16 v2, 0xd0

    iput v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestSetPath;->mResponseCode:I

    goto :goto_1

    .line 59
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
