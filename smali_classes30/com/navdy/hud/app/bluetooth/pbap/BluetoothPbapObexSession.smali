.class final Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;
.super Ljava/lang/Object;
.source "BluetoothPbapObexSession.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;
    }
.end annotation


# static fields
.field static final OBEX_SESSION_AUTHENTICATION_REQUEST:I = 0x69

.field static final OBEX_SESSION_AUTHENTICATION_TIMEOUT:I = 0x6a

.field static final OBEX_SESSION_CONNECTED:I = 0x64

.field static final OBEX_SESSION_DISCONNECTED:I = 0x66

.field static final OBEX_SESSION_FAILED:I = 0x65

.field static final OBEX_SESSION_REQUEST_COMPLETED:I = 0x67

.field static final OBEX_SESSION_REQUEST_FAILED:I = 0x68

.field private static final PBAP_TARGET:[B

.field private static final TAG:Ljava/lang/String; = "BTPbapObexSession"


# instance fields
.field private mAuth:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexAuthenticator;

.field private mObexClientThread:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;

.field private mSessionHandler:Landroid/os/Handler;

.field private final mTransport:Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const/16 v0, 0x10

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->PBAP_TARGET:[B

    return-void

    :array_0
    .array-data 1
        0x79t
        0x61t
        0x35t
        -0x10t
        -0x10t
        -0x3bt
        0x11t
        -0x28t
        0x9t
        0x66t
        0x8t
        0x0t
        0x20t
        0xct
        -0x66t
        0x66t
    .end array-data
.end method

.method public constructor <init>(Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;)V
    .locals 1
    .param p1, "transport"    # Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->mAuth:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexAuthenticator;

    .line 50
    iput-object p1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->mTransport:Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;

    .line 51
    return-void
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;)Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->mObexClientThread:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->mSessionHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;)Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->mTransport:Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;)Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexAuthenticator;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->mAuth:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexAuthenticator;

    return-object v0
.end method

.method static synthetic access$500()[B
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->PBAP_TARGET:[B

    return-object v0
.end method


# virtual methods
.method public abort()V
    .locals 2

    .prologue
    .line 88
    const-string v0, "BTPbapObexSession"

    const-string v1, "abort"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->mObexClientThread:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->mObexClientThread:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;

    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->mRequest:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;
    invoke-static {v0}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->access$000(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;)Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 95
    new-instance v0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$1;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$1;-><init>(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;)V

    .line 100
    invoke-virtual {v0}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$1;->run()V

    .line 102
    :cond_0
    return-void
.end method

.method public schedule(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;)Z
    .locals 3
    .param p1, "request"    # Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;

    .prologue
    .line 105
    const-string v0, "BTPbapObexSession"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "schedule: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->mObexClientThread:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;

    if-nez v0, :cond_0

    .line 108
    const-string v0, "BTPbapObexSession"

    const-string v1, "OBEX session not started"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    const/4 v0, 0x0

    .line 112
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->mObexClientThread:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->schedule(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;)Z

    move-result v0

    goto :goto_0
.end method

.method public setAuthReply(Ljava/lang/String;)Z
    .locals 3
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 116
    const-string v0, "BTPbapObexSession"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setAuthReply key="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->mAuth:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexAuthenticator;

    if-nez v0, :cond_0

    .line 119
    const/4 v0, 0x0

    .line 124
    :goto_0
    return v0

    .line 122
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->mAuth:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexAuthenticator;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexAuthenticator;->setReply(Ljava/lang/String;)V

    .line 124
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public start(Landroid/os/Handler;)V
    .locals 2
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 54
    const-string v0, "BTPbapObexSession"

    const-string v1, "start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    iput-object p1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->mSessionHandler:Landroid/os/Handler;

    .line 57
    new-instance v0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexAuthenticator;

    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->mSessionHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexAuthenticator;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->mAuth:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexAuthenticator;

    .line 59
    new-instance v0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;-><init>(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;)V

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->mObexClientThread:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;

    .line 60
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->mObexClientThread:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;

    invoke-virtual {v0}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->start()V

    .line 61
    return-void
.end method

.method public stop()V
    .locals 3

    .prologue
    .line 64
    const-string v1, "BTPbapObexSession"

    const-string v2, "stop"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->mObexClientThread:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;

    if-eqz v1, :cond_1

    .line 67
    const/4 v0, 0x0

    .line 69
    .local v0, "wait":Z
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->mObexClientThread:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;

    invoke-virtual {v1}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->mObexClientThread:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;

    invoke-virtual {v1}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->isRunning()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    .line 73
    :goto_0
    if-eqz v0, :cond_0

    .line 75
    :try_start_1
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->mObexClientThread:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;

    invoke-virtual {v1}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->interrupt()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 79
    :goto_1
    :try_start_2
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->mObexClientThread:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;

    invoke-virtual {v1}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->join()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 83
    :cond_0
    :goto_2
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->mObexClientThread:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;

    .line 85
    .end local v0    # "wait":Z
    :cond_1
    return-void

    .line 69
    .restart local v0    # "wait":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 80
    :catch_0
    move-exception v1

    goto :goto_2

    .line 76
    :catch_1
    move-exception v1

    goto :goto_1

    .line 70
    :catch_2
    move-exception v1

    goto :goto_0
.end method
