.class final Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullPhoneBook;
.super Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;
.source "BluetoothPbapRequestPullPhoneBook.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "BTPbapReqPullPBook"

.field private static final TYPE:Ljava/lang/String; = "x-bt/phonebook"


# instance fields
.field private final mFormat:B

.field private mNewMissedCalls:I

.field private mResponse:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapVcardList;


# direct methods
.method public constructor <init>(Ljava/lang/String;JBII)V
    .locals 8
    .param p1, "pbName"    # Ljava/lang/String;
    .param p2, "filter"    # J
    .param p4, "format"    # B
    .param p5, "maxListCount"    # I
    .param p6, "listStartOffset"    # I

    .prologue
    const v1, 0xffff

    const/4 v6, 0x4

    const/4 v5, 0x1

    const/4 v4, -0x1

    .line 43
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;-><init>()V

    .line 38
    iput v4, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullPhoneBook;->mNewMissedCalls:I

    .line 45
    if-ltz p5, :cond_0

    if-le p5, v1, :cond_1

    .line 46
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "maxListCount should be [0..65535]"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 49
    :cond_1
    if-ltz p6, :cond_2

    if-le p6, v1, :cond_3

    .line 50
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "listStartOffset should be [0..65535]"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 53
    :cond_3
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullPhoneBook;->mHeaderSet:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    invoke-virtual {v1, v5, p1}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    .line 55
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullPhoneBook;->mHeaderSet:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    const/16 v2, 0x42

    const-string v3, "x-bt/phonebook"

    invoke-virtual {v1, v2, v3}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    .line 57
    new-instance v0, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;

    invoke-direct {v0}, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;-><init>()V

    .line 60
    .local v0, "oap":Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;
    if-eqz p4, :cond_4

    if-eq p4, v5, :cond_4

    .line 62
    const/4 p4, 0x0

    .line 65
    :cond_4
    const-wide/16 v2, 0x0

    cmp-long v1, p2, v2

    if-eqz v1, :cond_5

    .line 66
    const/4 v1, 0x6

    invoke-virtual {v0, v1, p2, p3}, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->add(BJ)V

    .line 69
    :cond_5
    const/4 v1, 0x7

    invoke-virtual {v0, v1, p4}, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->add(BB)V

    .line 75
    if-lez p5, :cond_7

    .line 76
    int-to-short v1, p5

    invoke-virtual {v0, v6, v1}, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->add(BS)V

    .line 81
    :goto_0
    if-lez p6, :cond_6

    .line 82
    const/4 v1, 0x5

    int-to-short v2, p6

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->add(BS)V

    .line 85
    :cond_6
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullPhoneBook;->mHeaderSet:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->addToHeaderSet(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;)V

    .line 87
    iput-byte p4, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullPhoneBook;->mFormat:B

    .line 88
    return-void

    .line 78
    :cond_7
    invoke-virtual {v0, v6, v4}, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->add(BS)V

    goto :goto_0
.end method


# virtual methods
.method public getList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 109
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullPhoneBook;->mResponse:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapVcardList;

    invoke-virtual {v0}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapVcardList;->getList()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getNewMissedCalls()I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullPhoneBook;->mNewMissedCalls:I

    return v0
.end method

.method protected readResponse(Ljava/io/InputStream;)V
    .locals 2
    .param p1, "stream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 92
    const-string v0, "BTPbapReqPullPBook"

    const-string v1, "readResponse"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    new-instance v0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapVcardList;

    iget-byte v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullPhoneBook;->mFormat:B

    invoke-direct {v0, p1, v1}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapVcardList;-><init>(Ljava/io/InputStream;B)V

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullPhoneBook;->mResponse:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapVcardList;

    .line 95
    return-void
.end method

.method protected readResponseHeaders(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;)V
    .locals 4
    .param p1, "headerset"    # Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    .prologue
    const/16 v3, 0x9

    .line 99
    const-string v1, "BTPbapReqPullPBook"

    const-string v2, "readResponse"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    invoke-static {p1}, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->fromHeaderSet(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;)Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;

    move-result-object v0

    .line 103
    .local v0, "oap":Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;
    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->exists(B)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 104
    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->getByte(B)B

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullPhoneBook;->mNewMissedCalls:I

    .line 106
    :cond_0
    return-void
.end method
