.class final Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullVcardEntry;
.super Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;
.source "BluetoothPbapRequestPullVcardEntry.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "BTPbapReqPullVcardEntry"

.field private static final TYPE:Ljava/lang/String; = "x-bt/vcard"


# instance fields
.field private final mFormat:B

.field private mResponse:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapVcardList;


# direct methods
.method public constructor <init>(Ljava/lang/String;JB)V
    .locals 6
    .param p1, "handle"    # Ljava/lang/String;
    .param p2, "filter"    # J
    .param p4, "format"    # B

    .prologue
    const/4 v4, 0x1

    .line 40
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;-><init>()V

    .line 41
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullVcardEntry;->mHeaderSet:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    invoke-virtual {v1, v4, p1}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    .line 43
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullVcardEntry;->mHeaderSet:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    const/16 v2, 0x42

    const-string v3, "x-bt/vcard"

    invoke-virtual {v1, v2, v3}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    .line 46
    if-eqz p4, :cond_0

    if-eq p4, v4, :cond_0

    .line 48
    const/4 p4, 0x0

    .line 51
    :cond_0
    new-instance v0, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;

    invoke-direct {v0}, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;-><init>()V

    .line 53
    .local v0, "oap":Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;
    const-wide/16 v2, 0x0

    cmp-long v1, p2, v2

    if-eqz v1, :cond_1

    .line 54
    const/4 v1, 0x6

    invoke-virtual {v0, v1, p2, p3}, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->add(BJ)V

    .line 57
    :cond_1
    const/4 v1, 0x7

    invoke-virtual {v0, v1, p4}, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->add(BB)V

    .line 58
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullVcardEntry;->mHeaderSet:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->addToHeaderSet(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;)V

    .line 60
    iput-byte p4, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullVcardEntry;->mFormat:B

    .line 61
    return-void
.end method


# virtual methods
.method protected checkResponseCode(I)V
    .locals 3
    .param p1, "responseCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 71
    const-string v0, "BTPbapReqPullVcardEntry"

    const-string v1, "checkResponseCode"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullVcardEntry;->mResponse:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapVcardList;

    invoke-virtual {v0}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapVcardList;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 74
    const/16 v0, 0xc4

    if-eq p1, v0, :cond_0

    const/16 v0, 0xc6

    if-eq p1, v0, :cond_0

    .line 76
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid response received:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 78
    :cond_0
    const-string v0, "BTPbapReqPullVcardEntry"

    const-string v1, "Vcard Entry not found"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    :cond_1
    return-void
.end method

.method public getVcard()Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullVcardEntry;->mResponse:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapVcardList;

    invoke-virtual {v0}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapVcardList;->getFirst()Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;

    move-result-object v0

    return-object v0
.end method

.method protected readResponse(Ljava/io/InputStream;)V
    .locals 2
    .param p1, "stream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65
    const-string v0, "BTPbapReqPullVcardEntry"

    const-string v1, "readResponse"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    new-instance v0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapVcardList;

    iget-byte v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullVcardEntry;->mFormat:B

    invoke-direct {v0, p1, v1}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapVcardList;-><init>(Ljava/io/InputStream;B)V

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullVcardEntry;->mResponse:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapVcardList;

    .line 68
    return-void
.end method
