.class Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager$2;
.super Ljava/lang/Object;
.source "PBAPClientManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;

    .prologue
    .line 179
    iput-object p1, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager$2;->this$0:Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 183
    :try_start_0
    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "pbap hang detected, reattempting..."

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 184
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager$2;->this$0:Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;

    # invokes: Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->stopPbapClient()V
    invoke-static {v1}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->access$500(Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;)V

    .line 185
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager$2;->this$0:Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;

    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->access$200(Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager$2;->this$0:Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;

    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->syncRunnable:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->access$600(Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 186
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager$2;->this$0:Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;

    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->access$200(Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager$2;->this$0:Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;

    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->syncRunnable:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->access$600(Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 190
    :goto_0
    return-void

    .line 187
    :catch_0
    move-exception v0

    .line 188
    .local v0, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
