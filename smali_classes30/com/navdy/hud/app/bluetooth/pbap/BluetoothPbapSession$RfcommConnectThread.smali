.class Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession$RfcommConnectThread;
.super Ljava/lang/Thread;
.source "BluetoothPbapSession.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RfcommConnectThread"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "RfcommConnectThread"


# instance fields
.field private mSocket:Landroid/bluetooth/BluetoothSocket;

.field final synthetic this$0:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;


# direct methods
.method public constructor <init>(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;)V
    .locals 1

    .prologue
    .line 305
    iput-object p1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession$RfcommConnectThread;->this$0:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;

    .line 306
    const-string v0, "RfcommConnectThread"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 307
    return-void
.end method

.method private closeSocket()V
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession$RfcommConnectThread;->mSocket:Landroid/bluetooth/BluetoothSocket;

    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 333
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 311
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession$RfcommConnectThread;->this$0:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;

    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v2}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->access$000(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v2

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->isDiscovering()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 312
    const-string v2, "RfcommConnectThread"

    const-string v3, "pbap device currently discovering, might be slow to connect"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession$RfcommConnectThread;->this$0:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;

    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mDevice:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v2}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->access$100(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    const-string v3, "0000112f-0000-1000-8000-00805f9b34fb"

    invoke-static {v3}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/bluetooth/BluetoothDevice;->createRfcommSocketToServiceRecord(Ljava/util/UUID;)Landroid/bluetooth/BluetoothSocket;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession$RfcommConnectThread;->mSocket:Landroid/bluetooth/BluetoothSocket;

    .line 318
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession$RfcommConnectThread;->mSocket:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothSocket;->connect()V

    .line 321
    new-instance v1, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexTransport;

    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession$RfcommConnectThread;->mSocket:Landroid/bluetooth/BluetoothSocket;

    invoke-direct {v1, v2}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexTransport;-><init>(Landroid/bluetooth/BluetoothSocket;)V

    .line 323
    .local v1, "transport":Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexTransport;
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession$RfcommConnectThread;->this$0:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;

    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mSessionHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->access$200(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 329
    .end local v1    # "transport":Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexTransport;
    :goto_0
    return-void

    .line 324
    :catch_0
    move-exception v0

    .line 325
    .local v0, "e":Ljava/lang/Throwable;
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession$RfcommConnectThread;->closeSocket()V

    .line 326
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession$RfcommConnectThread;->this$0:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;

    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mSessionHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->access$200(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0
.end method
