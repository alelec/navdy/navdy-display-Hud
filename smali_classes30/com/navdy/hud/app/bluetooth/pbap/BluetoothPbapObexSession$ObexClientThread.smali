.class Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;
.super Ljava/lang/Thread;
.source "BluetoothPbapObexSession.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ObexClientThread"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ObexClientThread"


# instance fields
.field private mClientSession:Lcom/navdy/hud/app/bluetooth/obex/ClientSession;

.field private mRequest:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;

.field private volatile mRunning:Z

.field final synthetic this$0:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;


# direct methods
.method public constructor <init>(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 136
    iput-object p1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->this$0:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 134
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->mRunning:Z

    .line 138
    iput-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->mClientSession:Lcom/navdy/hud/app/bluetooth/obex/ClientSession;

    .line 139
    iput-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->mRequest:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;

    .line 140
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;)Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;

    .prologue
    .line 127
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->mRequest:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;

    return-object v0
.end method

.method private connect()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 208
    const-string v3, "ObexClientThread"

    const-string v4, "connect"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    :try_start_0
    new-instance v3, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;

    iget-object v4, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->this$0:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;

    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->mTransport:Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;
    invoke-static {v4}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->access$300(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;)Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;-><init>(Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;)V

    iput-object v3, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->mClientSession:Lcom/navdy/hud/app/bluetooth/obex/ClientSession;

    .line 212
    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->mClientSession:Lcom/navdy/hud/app/bluetooth/obex/ClientSession;

    iget-object v4, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->this$0:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;

    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->mAuth:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexAuthenticator;
    invoke-static {v4}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->access$400(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;)Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexAuthenticator;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->setAuthenticator(Lcom/navdy/hud/app/bluetooth/obex/Authenticator;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 217
    new-instance v1, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    invoke-direct {v1}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;-><init>()V

    .line 218
    .local v1, "hs":Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    const/16 v3, 0x46

    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->PBAP_TARGET:[B
    invoke-static {}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->access$500()[B

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    .line 221
    :try_start_1
    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->mClientSession:Lcom/navdy/hud/app/bluetooth/obex/ClientSession;

    invoke-virtual {v3, v1}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->connect(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;)Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    move-result-object v1

    .line 223
    invoke-virtual {v1}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->getResponseCode()I

    move-result v3

    const/16 v4, 0xa0

    if-eq v3, v4, :cond_0

    .line 224
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->disconnect()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 231
    .end local v1    # "hs":Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    :goto_0
    return v2

    .line 213
    :catch_0
    move-exception v0

    .line 214
    .local v0, "e":Ljava/lang/Throwable;
    goto :goto_0

    .line 227
    .end local v0    # "e":Ljava/lang/Throwable;
    .restart local v1    # "hs":Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;
    :catch_1
    move-exception v0

    .line 228
    .restart local v0    # "e":Ljava/lang/Throwable;
    goto :goto_0

    .line 231
    .end local v0    # "e":Ljava/lang/Throwable;
    :cond_0
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private disconnect()V
    .locals 2

    .prologue
    .line 235
    const-string v0, "ObexClientThread"

    const-string v1, "disconnect"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->mClientSession:Lcom/navdy/hud/app/bluetooth/obex/ClientSession;

    if-eqz v0, :cond_0

    .line 239
    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->mClientSession:Lcom/navdy/hud/app/bluetooth/obex/ClientSession;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->disconnect(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;)Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    .line 240
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->mClientSession:Lcom/navdy/hud/app/bluetooth/obex/ClientSession;

    invoke-virtual {v0}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 244
    :cond_0
    :goto_0
    return-void

    .line 241
    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public isRunning()Z
    .locals 1

    .prologue
    .line 143
    iget-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->mRunning:Z

    return v0
.end method

.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 148
    invoke-super {p0}, Ljava/lang/Thread;->run()V

    .line 150
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->connect()Z

    move-result v1

    if-nez v1, :cond_0

    .line 151
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->this$0:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;

    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->mSessionHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->access$200(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x65

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 192
    :goto_0
    return-void

    .line 155
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->this$0:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;

    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->mSessionHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->access$200(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x64

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 157
    :goto_1
    iget-boolean v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->mRunning:Z

    if-eqz v1, :cond_3

    .line 158
    monitor-enter p0

    .line 160
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->mRequest:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;

    if-nez v1, :cond_1

    .line 161
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 167
    :cond_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 169
    iget-boolean v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->mRunning:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->mRequest:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;

    if-eqz v1, :cond_2

    .line 171
    :try_start_2
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->mRequest:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;

    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->mClientSession:Lcom/navdy/hud/app/bluetooth/obex/ClientSession;

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;->execute(Lcom/navdy/hud/app/bluetooth/obex/ClientSession;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    .line 177
    :goto_2
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->mRequest:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;

    invoke-virtual {v1}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;->isSuccess()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 178
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->this$0:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;

    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->mSessionHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->access$200(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x67

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->mRequest:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 179
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 186
    :cond_2
    :goto_3
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->mRequest:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;

    goto :goto_1

    .line 163
    :catch_0
    move-exception v0

    .line 164
    .local v0, "e":Ljava/lang/InterruptedException;
    const/4 v1, 0x0

    :try_start_3
    iput-boolean v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->mRunning:Z

    .line 165
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 189
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_3
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->disconnect()V

    .line 191
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->this$0:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;

    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->mSessionHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->access$200(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x66

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 167
    :catchall_0
    move-exception v1

    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v1

    .line 172
    :catch_1
    move-exception v0

    .line 174
    .local v0, "e":Ljava/lang/Throwable;
    iput-boolean v4, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->mRunning:Z

    goto :goto_2

    .line 181
    .end local v0    # "e":Ljava/lang/Throwable;
    :cond_4
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->this$0:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;

    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->mSessionHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->access$200(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x68

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->mRequest:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 182
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_3
.end method

.method public declared-synchronized schedule(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;)Z
    .locals 3
    .param p1, "request"    # Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;

    .prologue
    .line 195
    monitor-enter p0

    :try_start_0
    const-string v0, "ObexClientThread"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "schedule: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->mRequest:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 198
    const/4 v0, 0x0

    .line 204
    :goto_0
    monitor-exit p0

    return v0

    .line 201
    :cond_0
    :try_start_1
    iput-object p1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession$ObexClientThread;->mRequest:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;

    .line 202
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 204
    const/4 v0, 0x1

    goto :goto_0

    .line 195
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
