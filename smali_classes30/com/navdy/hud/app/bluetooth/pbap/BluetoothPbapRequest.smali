.class abstract Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;
.super Ljava/lang/Object;
.source "BluetoothPbapRequest.java"


# static fields
.field protected static final OAP_TAGID_FILTER:B = 0x6t

.field protected static final OAP_TAGID_FORMAT:B = 0x7t

.field protected static final OAP_TAGID_LIST_START_OFFSET:B = 0x5t

.field protected static final OAP_TAGID_MAX_LIST_COUNT:B = 0x4t

.field protected static final OAP_TAGID_NEW_MISSED_CALLS:B = 0x9t

.field protected static final OAP_TAGID_ORDER:B = 0x1t

.field protected static final OAP_TAGID_PHONEBOOK_SIZE:B = 0x8t

.field protected static final OAP_TAGID_SEARCH_ATTRIBUTE:B = 0x3t

.field protected static final OAP_TAGID_SEARCH_VALUE:B = 0x2t

.field private static final TAG:Ljava/lang/String; = "BTPbapRequest"


# instance fields
.field private mAborted:Z

.field protected mHeaderSet:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

.field private mOp:Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;

.field protected mResponseCode:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;->mAborted:Z

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;->mOp:Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;

    .line 53
    new-instance v0, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    invoke-direct {v0}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;->mHeaderSet:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    .line 54
    return-void
.end method


# virtual methods
.method public abort()V
    .locals 3

    .prologue
    .line 105
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;->mAborted:Z

    .line 107
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;->mOp:Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;

    if-eqz v1, :cond_0

    .line 109
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;->mOp:Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;

    invoke-virtual {v1}, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->abort()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 114
    :cond_0
    :goto_0
    return-void

    .line 110
    :catch_0
    move-exception v0

    .line 111
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "BTPbapRequest"

    const-string v2, "Exception occured when trying to abort"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected checkResponseCode(I)V
    .locals 2
    .param p1, "responseCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 129
    const-string v0, "BTPbapRequest"

    const-string v1, "checkResponseCode"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    return-void
.end method

.method public execute(Lcom/navdy/hud/app/bluetooth/obex/ClientSession;)V
    .locals 6
    .param p1, "session"    # Lcom/navdy/hud/app/bluetooth/obex/ClientSession;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v5, 0xd0

    .line 61
    const-string v2, "BTPbapRequest"

    const-string v3, "execute"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    iget-boolean v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;->mAborted:Z

    if-eqz v2, :cond_0

    .line 65
    iput v5, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;->mResponseCode:I

    .line 102
    :goto_0
    return-void

    .line 70
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;->mHeaderSet:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    invoke-virtual {p1, v2}, Lcom/navdy/hud/app/bluetooth/obex/ClientSession;->get(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;)Lcom/navdy/hud/app/bluetooth/obex/Operation;

    move-result-object v2

    check-cast v2, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;

    iput-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;->mOp:Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;

    .line 73
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;->mOp:Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->setGetFinalFlag(Z)V

    .line 79
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;->mOp:Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->continueOperation(ZZ)Z

    .line 81
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;->mOp:Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;

    invoke-virtual {v2}, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->getReceivedHeader()Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;->readResponseHeaders(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;)V

    .line 83
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;->mOp:Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;

    invoke-virtual {v2}, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->openInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 85
    .local v1, "is":Ljava/io/InputStream;
    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;->readResponse(Ljava/io/InputStream;)V

    .line 87
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 89
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;->mOp:Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;

    invoke-virtual {v2}, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->close()V

    .line 91
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;->mOp:Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;

    invoke-virtual {v2}, Lcom/navdy/hud/app/bluetooth/obex/ClientOperation;->getResponseCode()I

    move-result v2

    iput v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;->mResponseCode:I

    .line 93
    const-string v2, "BTPbapRequest"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mResponseCode="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;->mResponseCode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    iget v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;->mResponseCode:I

    invoke-virtual {p0, v2}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;->checkResponseCode(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 96
    .end local v1    # "is":Ljava/io/InputStream;
    :catch_0
    move-exception v0

    .line 97
    .local v0, "e":Ljava/io/IOException;
    const-string v2, "BTPbapRequest"

    const-string v3, "IOException occured when processing request"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 98
    iput v5, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;->mResponseCode:I

    .line 100
    throw v0
.end method

.method public final isSuccess()Z
    .locals 2

    .prologue
    .line 57
    iget v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;->mResponseCode:I

    const/16 v1, 0xa0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected readResponse(Ljava/io/InputStream;)V
    .locals 2
    .param p1, "stream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 117
    const-string v0, "BTPbapRequest"

    const-string v1, "readResponse"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    return-void
.end method

.method protected readResponseHeaders(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;)V
    .locals 2
    .param p1, "headerset"    # Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    .prologue
    .line 123
    const-string v0, "BTPbapRequest"

    const-string v1, "readResponseHeaders"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    return-void
.end method
