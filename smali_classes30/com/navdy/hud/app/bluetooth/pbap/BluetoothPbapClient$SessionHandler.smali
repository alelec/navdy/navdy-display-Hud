.class Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient$SessionHandler;
.super Landroid/os/Handler;
.source "BluetoothPbapClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SessionHandler"
.end annotation


# instance fields
.field private final mClient:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;)V
    .locals 1
    .param p1, "client"    # Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;

    .prologue
    .line 385
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 386
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient$SessionHandler;->mClient:Ljava/lang/ref/WeakReference;

    .line 387
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 391
    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "handleMessage: what="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p1, Landroid/os/Message;->what:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 393
    iget-object v4, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient$SessionHandler;->mClient:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;

    .line 394
    .local v0, "client":Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;
    if-nez v0, :cond_1

    .line 482
    :cond_0
    :goto_0
    return-void

    .line 398
    :cond_1
    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 422
    :pswitch_0
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;

    .line 424
    .local v2, "req":Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;
    instance-of v4, v2, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullPhoneBookSize;

    if-eqz v4, :cond_7

    .line 425
    check-cast v2, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullPhoneBookSize;

    .end local v2    # "req":Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;
    invoke-virtual {v2}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullPhoneBookSize;->getSize()I

    move-result v3

    .line 426
    .local v3, "size":I
    const/4 v4, 0x5

    # invokes: Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->sendToClient(II)V
    invoke-static {v0, v4, v3}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->access$200(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;II)V

    goto :goto_0

    .line 401
    .end local v3    # "size":I
    :pswitch_1
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;

    .line 403
    .restart local v2    # "req":Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;
    instance-of v4, v2, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullPhoneBookSize;

    if-eqz v4, :cond_2

    .line 404
    const/16 v4, 0x69

    # invokes: Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->sendToClient(I)V
    invoke-static {v0, v4}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->access$100(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;I)V

    goto :goto_0

    .line 405
    :cond_2
    instance-of v4, v2, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullVcardListingSize;

    if-eqz v4, :cond_3

    .line 406
    const/16 v4, 0x6a

    # invokes: Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->sendToClient(I)V
    invoke-static {v0, v4}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->access$100(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;I)V

    goto :goto_0

    .line 407
    :cond_3
    instance-of v4, v2, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullPhoneBook;

    if-eqz v4, :cond_4

    .line 408
    const/16 v4, 0x66

    # invokes: Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->sendToClient(I)V
    invoke-static {v0, v4}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->access$100(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;I)V

    goto :goto_0

    .line 409
    :cond_4
    instance-of v4, v2, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullVcardListing;

    if-eqz v4, :cond_5

    .line 410
    const/16 v4, 0x67

    # invokes: Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->sendToClient(I)V
    invoke-static {v0, v4}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->access$100(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;I)V

    goto :goto_0

    .line 411
    :cond_5
    instance-of v4, v2, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullVcardEntry;

    if-eqz v4, :cond_6

    .line 412
    const/16 v4, 0x68

    # invokes: Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->sendToClient(I)V
    invoke-static {v0, v4}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->access$100(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;I)V

    goto :goto_0

    .line 413
    :cond_6
    instance-of v4, v2, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestSetPath;

    if-eqz v4, :cond_0

    .line 414
    const/16 v4, 0x65

    # invokes: Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->sendToClient(I)V
    invoke-static {v0, v4}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->access$100(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;I)V

    goto :goto_0

    .line 428
    :cond_7
    instance-of v4, v2, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullVcardListingSize;

    if-eqz v4, :cond_8

    .line 429
    check-cast v2, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullVcardListingSize;

    .end local v2    # "req":Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;
    invoke-virtual {v2}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullVcardListingSize;->getSize()I

    move-result v3

    .line 430
    .restart local v3    # "size":I
    const/4 v4, 0x6

    # invokes: Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->sendToClient(II)V
    invoke-static {v0, v4, v3}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->access$200(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;II)V

    goto :goto_0

    .line 432
    .end local v3    # "size":I
    .restart local v2    # "req":Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;
    :cond_8
    instance-of v4, v2, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullPhoneBook;

    if-eqz v4, :cond_9

    move-object v1, v2

    .line 433
    check-cast v1, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullPhoneBook;

    .line 434
    .local v1, "r":Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullPhoneBook;
    const/4 v4, 0x2

    invoke-virtual {v1}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullPhoneBook;->getNewMissedCalls()I

    move-result v5

    .line 435
    invoke-virtual {v1}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullPhoneBook;->getList()Ljava/util/ArrayList;

    move-result-object v6

    .line 434
    # invokes: Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->sendToClient(IILjava/lang/Object;)V
    invoke-static {v0, v4, v5, v6}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->access$300(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;IILjava/lang/Object;)V

    goto :goto_0

    .line 437
    .end local v1    # "r":Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullPhoneBook;
    :cond_9
    instance-of v4, v2, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullVcardListing;

    if-eqz v4, :cond_a

    move-object v1, v2

    .line 438
    check-cast v1, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullVcardListing;

    .line 439
    .local v1, "r":Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullVcardListing;
    const/4 v4, 0x3

    invoke-virtual {v1}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullVcardListing;->getNewMissedCalls()I

    move-result v5

    .line 440
    invoke-virtual {v1}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullVcardListing;->getList()Ljava/util/ArrayList;

    move-result-object v6

    .line 439
    # invokes: Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->sendToClient(IILjava/lang/Object;)V
    invoke-static {v0, v4, v5, v6}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->access$300(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;IILjava/lang/Object;)V

    goto/16 :goto_0

    .line 442
    .end local v1    # "r":Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullVcardListing;
    :cond_a
    instance-of v4, v2, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullVcardEntry;

    if-eqz v4, :cond_b

    move-object v1, v2

    .line 443
    check-cast v1, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullVcardEntry;

    .line 444
    .local v1, "r":Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullVcardEntry;
    const/4 v4, 0x4

    invoke-virtual {v1}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullVcardEntry;->getVcard()Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;

    move-result-object v5

    # invokes: Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->sendToClient(ILjava/lang/Object;)V
    invoke-static {v0, v4, v5}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->access$400(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 446
    .end local v1    # "r":Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullVcardEntry;
    :cond_b
    instance-of v4, v2, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestSetPath;

    if-eqz v4, :cond_0

    .line 447
    const/4 v4, 0x1

    # invokes: Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->sendToClient(I)V
    invoke-static {v0, v4}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->access$100(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;I)V

    goto/16 :goto_0

    .line 454
    .end local v2    # "req":Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;
    :pswitch_2
    const/16 v4, 0xcb

    # invokes: Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->sendToClient(I)V
    invoke-static {v0, v4}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->access$100(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;I)V

    goto/16 :goto_0

    .line 458
    :pswitch_3
    const/16 v4, 0xcc

    # invokes: Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->sendToClient(I)V
    invoke-static {v0, v4}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->access$100(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;I)V

    goto/16 :goto_0

    .line 469
    :pswitch_4
    sget-object v4, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient$ConnectionState;->CONNECTING:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient$ConnectionState;

    # setter for: Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->mConnectionState:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient$ConnectionState;
    invoke-static {v0, v4}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->access$502(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient$ConnectionState;)Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient$ConnectionState;

    goto/16 :goto_0

    .line 473
    :pswitch_5
    sget-object v4, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient$ConnectionState;->CONNECTED:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient$ConnectionState;

    # setter for: Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->mConnectionState:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient$ConnectionState;
    invoke-static {v0, v4}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->access$502(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient$ConnectionState;)Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient$ConnectionState;

    .line 474
    const/16 v4, 0xc9

    # invokes: Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->sendToClient(I)V
    invoke-static {v0, v4}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->access$100(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;I)V

    goto/16 :goto_0

    .line 478
    :pswitch_6
    sget-object v4, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient$ConnectionState;->DISCONNECTED:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient$ConnectionState;

    # setter for: Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->mConnectionState:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient$ConnectionState;
    invoke-static {v0, v4}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->access$502(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient$ConnectionState;)Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient$ConnectionState;

    .line 479
    const/16 v4, 0xca

    # invokes: Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->sendToClient(I)V
    invoke-static {v0, v4}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->access$100(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;I)V

    goto/16 :goto_0

    .line 398
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
