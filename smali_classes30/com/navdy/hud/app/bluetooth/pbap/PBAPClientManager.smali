.class public Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;
.super Ljava/lang/Object;
.source "PBAPClientManager.java"


# static fields
.field private static final CHECK_PBAP_CONNECTION_HANG_INTERVAL:I = 0xea60

.field private static final CHECK_PBAP_PULL_PHONEBOOK_HANG_INTERVAL:I = 0xafc8

.field private static final INITIAL_CONNECTION_DELAY:I = 0x7530

.field public static final MAX_RECENT_CALLS:I = 0x1e

.field private static final TYPE_HOME:I = 0x1

.field private static final TYPE_MOBILE:I = 0x2

.field private static final TYPE_WORK:I = 0x3

.field private static final TYPE_WORK_MOBILE:I = 0x11

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static final singleton:Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;


# instance fields
.field private bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private checkPbapConnectionHang:Ljava/lang/Runnable;

.field private handler:Landroid/os/Handler;

.field private handlerCallback:Landroid/os/Handler$Callback;

.field private handlerThread:Landroid/os/HandlerThread;

.field private lastSyncAttemptTime:J

.field private pbapClient:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;

.field private remoteDeviceManager:Lcom/navdy/hud/app/manager/RemoteDeviceManager;

.field private stopPbapClientRunnable:Ljava/lang/Runnable;

.field private syncRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 60
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 81
    new-instance v0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;

    invoke-direct {v0}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->singleton:Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    .line 207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    new-instance v1, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager$1;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager$1;-><init>(Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;)V

    iput-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->handlerCallback:Landroid/os/Handler$Callback;

    .line 179
    new-instance v1, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager$2;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager$2;-><init>(Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;)V

    iput-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->checkPbapConnectionHang:Ljava/lang/Runnable;

    .line 193
    new-instance v1, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager$3;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager$3;-><init>(Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;)V

    iput-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->syncRunnable:Ljava/lang/Runnable;

    .line 200
    new-instance v1, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager$4;

    invoke-direct {v1, p0}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager$4;-><init>(Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;)V

    iput-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->stopPbapClientRunnable:Ljava/lang/Runnable;

    .line 208
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->remoteDeviceManager:Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    .line 209
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->remoteDeviceManager:Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 210
    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "HudPBAPClientManager"

    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->handlerThread:Landroid/os/HandlerThread;

    .line 211
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->handlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 212
    new-instance v1, Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->handlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->handlerCallback:Landroid/os/Handler$Callback;

    invoke-direct {v1, v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->handler:Landroid/os/Handler;

    .line 213
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "bluetooth"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothManager;

    .line 214
    .local v0, "bluetoothManager":Landroid/bluetooth/BluetoothManager;
    if-eqz v0, :cond_0

    .line 215
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothManager;->getAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 217
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->syncRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 218
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->syncRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x7530

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 219
    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->checkPbapConnectionHang:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;)Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->pbapClient:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;Ljava/util/ArrayList;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->buildRecentCallList(Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$500(Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->stopPbapClient()V

    return-void
.end method

.method static synthetic access$600(Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->syncRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method private buildRecentCallList(Ljava/util/ArrayList;)V
    .locals 29
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 319
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;>;"
    :try_start_0
    sget-object v4, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "recent calls vcard-list size["

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "]"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 320
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->isUserBuild()Z

    move-result v4

    if-nez v4, :cond_4

    const/16 v27, 0x1

    .line 321
    .local v27, "verbose":Z
    :goto_0
    new-instance v13, Ljava/util/LinkedHashMap;

    invoke-direct {v13}, Ljava/util/LinkedHashMap;-><init>()V

    .line 322
    .local v13, "callMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    invoke-static {}, Lcom/google/i18n/phonenumbers/PhoneNumberUtil;->getInstance()Lcom/google/i18n/phonenumbers/PhoneNumberUtil;

    move-result-object v23

    .line 323
    .local v23, "phoneNumberUtil":Lcom/google/i18n/phonenumbers/PhoneNumberUtil;
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v15

    .line 324
    .local v15, "currentCountry":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v28

    :cond_0
    :goto_1
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;

    .line 325
    .local v18, "entry":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;
    if-eqz v27, :cond_1

    .line 326
    sget-object v4, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "vcard entry:"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 330
    :cond_1
    invoke-virtual/range {v18 .. v18}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->getPhoneList()Ljava/util/List;

    move-result-object v21

    .line 331
    .local v21, "phoneData":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhoneData;>;"
    const/4 v5, 0x0

    .line 332
    .local v5, "phoneNumber":Ljava/lang/String;
    const/16 v22, 0x0

    .line 333
    .local v22, "phoneNumberType":I
    if-eqz v21, :cond_3

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_3

    .line 334
    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhoneData;

    .line 335
    .local v16, "data":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhoneData;
    invoke-virtual/range {v16 .. v16}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhoneData;->getNumber()Ljava/lang/String;

    move-result-object v19

    .line 336
    .local v19, "number":Ljava/lang/String;
    invoke-static/range {v19 .. v19}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 340
    move-object/from16 v5, v19

    .line 342
    invoke-virtual/range {v16 .. v16}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhoneData;->getType()I

    move-result v22

    .line 346
    .end local v16    # "data":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhoneData;
    .end local v19    # "number":Ljava/lang/String;
    :cond_3
    if-nez v5, :cond_5

    .line 347
    sget-object v4, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "no phone number in vcard"

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 400
    .end local v5    # "phoneNumber":Ljava/lang/String;
    .end local v13    # "callMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    .end local v15    # "currentCountry":Ljava/lang/String;
    .end local v18    # "entry":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;
    .end local v21    # "phoneData":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhoneData;>;"
    .end local v22    # "phoneNumberType":I
    .end local v23    # "phoneNumberUtil":Lcom/google/i18n/phonenumbers/PhoneNumberUtil;
    .end local v27    # "verbose":Z
    :catch_0
    move-exception v26

    .line 401
    .local v26, "t":Ljava/lang/Throwable;
    :try_start_1
    sget-object v4, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 403
    sget-object v4, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "buildRecentCallList: stopping client"

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 404
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->stopPbapClient()V

    .line 406
    .end local v26    # "t":Ljava/lang/Throwable;
    :goto_2
    return-void

    .line 320
    :cond_4
    const/16 v27, 0x0

    goto/16 :goto_0

    .line 353
    .restart local v5    # "phoneNumber":Ljava/lang/String;
    .restart local v13    # "callMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    .restart local v15    # "currentCountry":Ljava/lang/String;
    .restart local v18    # "entry":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;
    .restart local v21    # "phoneData":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhoneData;>;"
    .restart local v22    # "phoneNumberType":I
    .restart local v23    # "phoneNumberUtil":Lcom/google/i18n/phonenumbers/PhoneNumberUtil;
    .restart local v27    # "verbose":Z
    :cond_5
    :try_start_2
    move-object/from16 v0, v23

    invoke-virtual {v0, v5, v15}, Lcom/google/i18n/phonenumbers/PhoneNumberUtil;->parse(Ljava/lang/String;Ljava/lang/String;)Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v20

    .line 358
    .local v20, "pNumber":Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;
    :try_start_3
    invoke-virtual/range {v20 .. v20}, Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;->getNationalNumber()J

    move-result-wide v10

    .line 359
    .local v10, "nPhoneNum":J
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v13, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/navdy/hud/app/framework/recentcall/RecentCall;

    .line 360
    .local v12, "call":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    if-eqz v12, :cond_6

    iget-object v4, v12, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->name:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 362
    if-eqz v27, :cond_0

    .line 363
    sget-object v4, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Ignoring entry with duplicate phone number: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1

    .line 403
    .end local v5    # "phoneNumber":Ljava/lang/String;
    .end local v10    # "nPhoneNum":J
    .end local v12    # "call":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    .end local v13    # "callMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    .end local v15    # "currentCountry":Ljava/lang/String;
    .end local v18    # "entry":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;
    .end local v20    # "pNumber":Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;
    .end local v21    # "phoneData":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhoneData;>;"
    .end local v22    # "phoneNumberType":I
    .end local v23    # "phoneNumberUtil":Lcom/google/i18n/phonenumbers/PhoneNumberUtil;
    .end local v27    # "verbose":Z
    :catchall_0
    move-exception v4

    sget-object v6, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "buildRecentCallList: stopping client"

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 404
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->stopPbapClient()V

    throw v4

    .line 354
    .restart local v5    # "phoneNumber":Ljava/lang/String;
    .restart local v13    # "callMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    .restart local v15    # "currentCountry":Ljava/lang/String;
    .restart local v18    # "entry":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;
    .restart local v21    # "phoneData":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhoneData;>;"
    .restart local v22    # "phoneNumberType":I
    .restart local v23    # "phoneNumberUtil":Lcom/google/i18n/phonenumbers/PhoneNumberUtil;
    .restart local v27    # "verbose":Z
    :catch_1
    move-exception v26

    .line 355
    .restart local v26    # "t":Ljava/lang/Throwable;
    :try_start_4
    sget-object v4, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 368
    .end local v26    # "t":Ljava/lang/Throwable;
    .restart local v10    # "nPhoneNum":J
    .restart local v12    # "call":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    .restart local v20    # "pNumber":Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;
    :cond_6
    invoke-virtual/range {v18 .. v18}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    .line 369
    .local v3, "displayName":Ljava/lang/String;
    invoke-static {v3, v5, v10, v11}, Lcom/navdy/hud/app/framework/contacts/ContactUtil;->isDisplayNameValid(Ljava/lang/String;Ljava/lang/String;J)Z

    move-result v4

    if-nez v4, :cond_7

    .line 370
    const/4 v3, 0x0

    .line 372
    :cond_7
    invoke-virtual/range {v18 .. v18}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->getCallType()I

    move-result v14

    .line 373
    .local v14, "callType":I
    invoke-virtual/range {v18 .. v18}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->getCallTime()Ljava/util/Date;

    move-result-object v7

    .line 374
    .local v7, "callTime":Ljava/util/Date;
    if-nez v7, :cond_8

    .line 376
    sget-object v4, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ignoring spam number:"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 379
    :cond_8
    new-instance v2, Lcom/navdy/hud/app/framework/recentcall/RecentCall;

    sget-object v4, Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;->PHONE_CALL:Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;

    .line 380
    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->getNumberType(I)Lcom/navdy/hud/app/framework/contacts/NumberType;

    move-result-object v6

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->getCallType(I)Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

    move-result-object v8

    const/4 v9, -0x1

    invoke-direct/range {v2 .. v11}, Lcom/navdy/hud/app/framework/recentcall/RecentCall;-><init>(Ljava/lang/String;Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;Ljava/lang/String;Lcom/navdy/hud/app/framework/contacts/NumberType;Ljava/util/Date;Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;IJ)V

    .line 382
    .local v2, "recentCall":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    if-eqz v27, :cond_9

    .line 383
    sget-object v4, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "adding recent call:"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 386
    :cond_9
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v13, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 388
    invoke-virtual {v13}, Ljava/util/HashMap;->size()I

    move-result v4

    const/16 v6, 0x1e

    if-ne v4, v6, :cond_0

    .line 389
    sget-object v4, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "exceeded max size"

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 393
    .end local v2    # "recentCall":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    .end local v3    # "displayName":Ljava/lang/String;
    .end local v5    # "phoneNumber":Ljava/lang/String;
    .end local v7    # "callTime":Ljava/util/Date;
    .end local v10    # "nPhoneNum":J
    .end local v12    # "call":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    .end local v14    # "callType":I
    .end local v18    # "entry":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;
    .end local v20    # "pNumber":Lcom/google/i18n/phonenumbers/Phonenumber$PhoneNumber;
    .end local v21    # "phoneData":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhoneData;>;"
    .end local v22    # "phoneNumberType":I
    :cond_a
    sget-object v4, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "recent calls map size["

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v13}, Ljava/util/HashMap;->size()I

    move-result v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "]"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 394
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v17

    .line 395
    .local v17, "driverProfile":Lcom/navdy/hud/app/profile/DriverProfile;
    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    .line 396
    .local v25, "recentCalls":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    invoke-virtual {v13}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_b

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/navdy/hud/app/framework/recentcall/RecentCall;

    .line 397
    .local v24, "rc":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 399
    .end local v24    # "rc":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    :cond_b
    invoke-virtual/range {v17 .. v17}, Lcom/navdy/hud/app/profile/DriverProfile;->getProfileName()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x1

    move-object/from16 v0, v25

    invoke-static {v4, v0, v6}, Lcom/navdy/hud/app/storage/db/helper/RecentCallPersistenceHelper;->storeRecentCalls(Ljava/lang/String;Ljava/util/List;Z)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 403
    sget-object v4, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "buildRecentCallList: stopping client"

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 404
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->stopPbapClient()V

    goto/16 :goto_2
.end method

.method private getCallType(I)Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 428
    packed-switch p1, :pswitch_data_0

    .line 439
    sget-object v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;->UNNKNOWN:Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

    :goto_0
    return-object v0

    .line 430
    :pswitch_0
    sget-object v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;->INCOMING:Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

    goto :goto_0

    .line 433
    :pswitch_1
    sget-object v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;->MISSED:Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

    goto :goto_0

    .line 436
    :pswitch_2
    sget-object v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;->OUTGOING:Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

    goto :goto_0

    .line 428
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public static getInstance()Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->singleton:Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;

    return-object v0
.end method

.method private getNumberType(I)Lcom/navdy/hud/app/framework/contacts/NumberType;
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 409
    sparse-switch p1, :sswitch_data_0

    .line 423
    sget-object v0, Lcom/navdy/hud/app/framework/contacts/NumberType;->OTHER:Lcom/navdy/hud/app/framework/contacts/NumberType;

    :goto_0
    return-object v0

    .line 411
    :sswitch_0
    sget-object v0, Lcom/navdy/hud/app/framework/contacts/NumberType;->HOME:Lcom/navdy/hud/app/framework/contacts/NumberType;

    goto :goto_0

    .line 414
    :sswitch_1
    sget-object v0, Lcom/navdy/hud/app/framework/contacts/NumberType;->WORK:Lcom/navdy/hud/app/framework/contacts/NumberType;

    goto :goto_0

    .line 417
    :sswitch_2
    sget-object v0, Lcom/navdy/hud/app/framework/contacts/NumberType;->MOBILE:Lcom/navdy/hud/app/framework/contacts/NumberType;

    goto :goto_0

    .line 420
    :sswitch_3
    sget-object v0, Lcom/navdy/hud/app/framework/contacts/NumberType;->WORK_MOBILE:Lcom/navdy/hud/app/framework/contacts/NumberType;

    goto :goto_0

    .line 409
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x3 -> :sswitch_1
        0x11 -> :sswitch_3
    .end sparse-switch
.end method

.method private printCallMap(Ljava/util/Collection;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/navdy/hud/app/framework/recentcall/RecentCall;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 444
    .local p1, "calls":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/navdy/hud/app/framework/recentcall/RecentCall;>;"
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;

    .line 445
    .local v0, "call":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    sget-object v2, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->category:Lcom/navdy/hud/app/framework/recentcall/RecentCall$Category;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->number:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->numberType:Lcom/navdy/hud/app/framework/contacts/NumberType;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->callTime:Ljava/util/Date;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/navdy/hud/app/framework/recentcall/RecentCall;->callType:Lcom/navdy/hud/app/framework/recentcall/RecentCall$CallType;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 453
    .end local v0    # "call":Lcom/navdy/hud/app/framework/recentcall/RecentCall;
    :cond_0
    return-void
.end method

.method private startPbapClient(Ljava/lang/String;)V
    .locals 4
    .param p1, "btAddress"    # Ljava/lang/String;

    .prologue
    .line 253
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->pbapClient:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;

    if-eqz v1, :cond_0

    .line 263
    :goto_0
    return-void

    .line 256
    :cond_0
    sget-object v1, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "startPbapClient"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 257
    new-instance v1, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;

    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v2, p1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->handler:Landroid/os/Handler;

    invoke-direct {v1, v2, v3}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;-><init>(Landroid/bluetooth/BluetoothDevice;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->pbapClient:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;

    .line 258
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->pbapClient:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;

    invoke-virtual {v1}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->connect()V

    .line 259
    sget-object v1, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "started pbap client"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 260
    :catch_0
    move-exception v0

    .line 261
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "could not start pbap client"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private stopPbapClient()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 267
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->pbapClient:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 276
    iput-object v3, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->pbapClient:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;

    .line 277
    iput-wide v4, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->lastSyncAttemptTime:J

    .line 278
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->checkPbapConnectionHang:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 280
    :goto_0
    return-void

    .line 270
    :cond_0
    :try_start_1
    sget-object v1, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "stopPbapClient"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 271
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->pbapClient:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;

    invoke-virtual {v1}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->disconnect()V

    .line 272
    sget-object v1, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "stopped pbap client"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 276
    iput-object v3, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->pbapClient:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;

    .line 277
    iput-wide v4, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->lastSyncAttemptTime:J

    .line 278
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->checkPbapConnectionHang:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 273
    :catch_0
    move-exception v0

    .line 274
    .local v0, "t":Ljava/lang/Throwable;
    :try_start_2
    sget-object v1, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "error stopping pbap client"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 276
    iput-object v3, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->pbapClient:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;

    .line 277
    iput-wide v4, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->lastSyncAttemptTime:J

    .line 278
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->checkPbapConnectionHang:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 276
    .end local v0    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v1

    iput-object v3, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->pbapClient:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;

    .line 277
    iput-wide v4, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->lastSyncAttemptTime:J

    .line 278
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->checkPbapConnectionHang:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    throw v1
.end method


# virtual methods
.method public onConnectionStatusChange(Lcom/navdy/service/library/events/connection/ConnectionStateChange;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/service/library/events/connection/ConnectionStateChange;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 223
    sget-object v0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager$5;->$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState:[I

    iget-object v1, p1, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->state:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 230
    :goto_0
    return-void

    .line 225
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->stopPbapClientRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 226
    invoke-static {}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->getInstance()Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->clearRecentCalls()V

    .line 227
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->getInstance()Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->clearFavoriteContacts()V

    goto :goto_0

    .line 223
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onDeviceSyncRequired(Lcom/navdy/hud/app/service/ConnectionHandler$DeviceSyncEvent;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/hud/app/service/ConnectionHandler$DeviceSyncEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 234
    iget v1, p1, Lcom/navdy/hud/app/service/ConnectionHandler$DeviceSyncEvent;->amount:I

    if-ne v1, v0, :cond_1

    .line 235
    .local v0, "fullSync":Z
    :goto_0
    if-eqz v0, :cond_0

    .line 236
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->syncRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 237
    sget-object v1, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "trigger PBAP download"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 238
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->syncRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 240
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->getInstance()Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->syncFavoriteContacts(Z)V

    .line 241
    return-void

    .line 234
    .end local v0    # "fullSync":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDriverProfileChanged(Lcom/navdy/hud/app/event/DriverProfileChanged;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/hud/app/event/DriverProfileChanged;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 245
    invoke-static {}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->getInstance()Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;

    move-result-object v0

    .line 246
    .local v0, "recentCallManager":Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;
    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->clearContactLookupMap()V

    .line 247
    invoke-virtual {v0}, Lcom/navdy/hud/app/framework/recentcall/RecentCallManager;->buildRecentCalls()V

    .line 248
    invoke-static {}, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->getInstance()Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/framework/contacts/FavoriteContactsManager;->buildFavoriteContacts()V

    .line 249
    return-void
.end method

.method public syncRecentCalls()V
    .locals 8

    .prologue
    .line 288
    :try_start_0
    sget-object v4, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "syncRecentCalls"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 289
    iget-wide v4, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->lastSyncAttemptTime:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_0

    .line 290
    sget-object v4, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "syncRecentCalls in progress"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 315
    :goto_0
    return-void

    .line 293
    :cond_0
    iget-object v4, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->handler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->checkPbapConnectionHang:Ljava/lang/Runnable;

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 294
    iget-object v4, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->remoteDeviceManager:Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    invoke-virtual {v4}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getRemoteDeviceInfo()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v2

    .line 295
    .local v2, "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    if-nez v2, :cond_1

    .line 296
    sget-object v4, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "no remote device, bailing out"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 311
    .end local v2    # "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    :catch_0
    move-exception v3

    .line 312
    .local v3, "t":Ljava/lang/Throwable;
    sget-object v4, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "syncRecentCalls"

    invoke-virtual {v4, v5, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 313
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->stopPbapClient()V

    goto :goto_0

    .line 299
    .end local v3    # "t":Ljava/lang/Throwable;
    .restart local v2    # "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    :cond_1
    :try_start_1
    new-instance v1, Lcom/navdy/service/library/device/NavdyDeviceId;

    iget-object v4, v2, Lcom/navdy/service/library/events/DeviceInfo;->deviceId:Ljava/lang/String;

    invoke-direct {v1, v4}, Lcom/navdy/service/library/device/NavdyDeviceId;-><init>(Ljava/lang/String;)V

    .line 300
    .local v1, "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    invoke-virtual {v1}, Lcom/navdy/service/library/device/NavdyDeviceId;->getBluetoothAddress()Ljava/lang/String;

    move-result-object v0

    .line 301
    .local v0, "btAddress":Ljava/lang/String;
    if-nez v0, :cond_2

    .line 302
    sget-object v4, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "BT Address n/a deviceId:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v2, Lcom/navdy/service/library/events/DeviceInfo;->deviceId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 305
    :cond_2
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->stopPbapClient()V

    .line 306
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->lastSyncAttemptTime:J

    .line 307
    sget-object v4, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "syncRecentCalls-attempting to connect"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 308
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->startPbapClient(Ljava/lang/String;)V

    .line 310
    iget-object v4, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->handler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->checkPbapConnectionHang:Ljava/lang/Runnable;

    const-wide/32 v6, 0xea60

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
