.class Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager$1;
.super Ljava/lang/Object;
.source "PBAPClientManager.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager$1;->this$0:Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 102
    :try_start_0
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    .line 175
    :goto_0
    const/4 v1, 0x0

    return v1

    .line 104
    :sswitch_0
    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "pbap client connected, pulling phonebook [telecom/cch.vcf]"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 105
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager$1;->this$0:Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;

    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->access$200(Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager$1;->this$0:Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;

    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->checkPbapConnectionHang:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->access$100(Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 106
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager$1;->this$0:Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;

    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->pbapClient:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;
    invoke-static {v1}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->access$300(Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;)Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;

    move-result-object v1

    const-string v2, "telecom/cch.vcf"

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapClient;->pullPhoneBook(Ljava/lang/String;)Z

    .line 107
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager$1;->this$0:Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;

    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->access$200(Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager$1;->this$0:Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;

    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->checkPbapConnectionHang:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->access$100(Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;)Ljava/lang/Runnable;

    move-result-object v2

    const-wide/32 v4, 0xafc8

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 172
    :catch_0
    move-exception v0

    .line 173
    .local v0, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 111
    .end local v0    # "t":Ljava/lang/Throwable;
    :sswitch_1
    :try_start_1
    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "pbap client dis-connected"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 115
    :sswitch_2
    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "pbap client auth-timeout"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 119
    :sswitch_3
    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "pbap client auth-requested"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 123
    :sswitch_4
    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "pbap client set phone book done"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 127
    :sswitch_5
    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "pbap client pull vcard entry done"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 131
    :sswitch_6
    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "pbap client vcard listing done"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 135
    :sswitch_7
    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "pbap client pull phone book done"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 136
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager$1;->this$0:Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;

    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->access$200(Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager$1;->this$0:Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;

    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->checkPbapConnectionHang:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->access$100(Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 137
    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager$1;->this$0:Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/util/ArrayList;

    # invokes: Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->buildRecentCallList(Ljava/util/ArrayList;)V
    invoke-static {v2, v1}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->access$400(Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 141
    :sswitch_8
    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pbap client vcard listing size done:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 145
    :sswitch_9
    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pbap client phone book size done:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 149
    :sswitch_a
    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "pbap client phone book size error"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 153
    :sswitch_b
    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "pbap client vcard listing size error"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 157
    :sswitch_c
    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "pbap client pull phone book error"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 161
    :sswitch_d
    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "pbap client vcard listing error"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 165
    :sswitch_e
    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "pbap client vcard entry error"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 169
    :sswitch_f
    # getter for: Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/bluetooth/pbap/PBAPClientManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "pbap client set phone book error"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 102
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_4
        0x2 -> :sswitch_7
        0x3 -> :sswitch_6
        0x4 -> :sswitch_5
        0x5 -> :sswitch_9
        0x6 -> :sswitch_8
        0x65 -> :sswitch_f
        0x66 -> :sswitch_c
        0x67 -> :sswitch_d
        0x68 -> :sswitch_e
        0x69 -> :sswitch_a
        0x6a -> :sswitch_b
        0xc9 -> :sswitch_0
        0xca -> :sswitch_1
        0xcb -> :sswitch_3
        0xcc -> :sswitch_2
    .end sparse-switch
.end method
