.class Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;
.super Ljava/lang/Object;
.source "BluetoothPbapSession.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession$RfcommConnectThread;
    }
.end annotation


# static fields
.field public static final ACTION_LISTING:I = 0xe

.field public static final ACTION_PHONEBOOK_SIZE:I = 0x10

.field public static final ACTION_VCARD:I = 0xf

.field public static final AUTH_REQUESTED:I = 0x8

.field public static final AUTH_TIMEOUT:I = 0x9

.field private static final PBAP_UUID:Ljava/lang/String; = "0000112f-0000-1000-8000-00805f9b34fb"

.field public static final REQUEST_COMPLETED:I = 0x3

.field public static final REQUEST_FAILED:I = 0x4

.field private static final RFCOMM_CONNECTED:I = 0x1

.field private static final RFCOMM_FAILED:I = 0x2

.field public static final SESSION_CONNECTED:I = 0x6

.field public static final SESSION_CONNECTING:I = 0x5

.field public static final SESSION_DISCONNECTED:I = 0x7

.field private static final TAG:Ljava/lang/String; = "BTPbapSession"


# instance fields
.field private final mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mConnectThread:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession$RfcommConnectThread;

.field private final mDevice:Landroid/bluetooth/BluetoothDevice;

.field private final mHandlerThread:Landroid/os/HandlerThread;

.field private mObexSession:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;

.field private final mParentHandler:Landroid/os/Handler;

.field private mPendingRequest:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;

.field private final mSessionHandler:Landroid/os/Handler;

.field private mTransport:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexTransport;


# direct methods
.method public constructor <init>(Landroid/bluetooth/BluetoothDevice;Landroid/os/Handler;)V
    .locals 3
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    const/4 v1, 0x0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mPendingRequest:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;

    .line 73
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 74
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-nez v0, :cond_0

    .line 75
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "No Bluetooth adapter in the system"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 78
    :cond_0
    iput-object p1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mDevice:Landroid/bluetooth/BluetoothDevice;

    .line 79
    iput-object p2, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mParentHandler:Landroid/os/Handler;

    .line 80
    iput-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mConnectThread:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession$RfcommConnectThread;

    .line 81
    iput-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mTransport:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexTransport;

    .line 82
    iput-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mObexSession:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;

    .line 84
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "PBAP session handler"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mHandlerThread:Landroid/os/HandlerThread;

    .line 86
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 87
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mSessionHandler:Landroid/os/Handler;

    .line 88
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;)Landroid/bluetooth/BluetoothAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;)Landroid/bluetooth/BluetoothDevice;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mDevice:Landroid/bluetooth/BluetoothDevice;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mSessionHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private startObexSession()V
    .locals 2

    .prologue
    .line 281
    const-string v0, "BTPbapSession"

    const-string v1, "startObexSession"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    new-instance v0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;

    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mTransport:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexTransport;

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;-><init>(Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;)V

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mObexSession:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;

    .line 284
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mObexSession:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;

    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mSessionHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->start(Landroid/os/Handler;)V

    .line 285
    return-void
.end method

.method private startRfcomm()V
    .locals 2

    .prologue
    .line 245
    const-string v0, "BTPbapSession"

    const-string v1, "startRfcomm"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mConnectThread:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession$RfcommConnectThread;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mObexSession:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;

    if-nez v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mParentHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 250
    new-instance v0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession$RfcommConnectThread;

    invoke-direct {v0, p0}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession$RfcommConnectThread;-><init>(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;)V

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mConnectThread:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession$RfcommConnectThread;

    .line 251
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mConnectThread:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession$RfcommConnectThread;

    invoke-virtual {v0}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession$RfcommConnectThread;->start()V

    .line 258
    :cond_0
    return-void
.end method

.method private stopObexSession()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 288
    const-string v1, "BTPbapSession"

    const-string v2, "stopObexSession"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    :try_start_0
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mObexSession:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;

    if-eqz v1, :cond_0

    .line 291
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mObexSession:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;

    invoke-virtual {v1}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->stop()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 296
    :cond_0
    iput-object v3, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mObexSession:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;

    .line 298
    :goto_0
    return-void

    .line 293
    :catch_0
    move-exception v0

    .line 294
    .local v0, "t":Ljava/lang/Throwable;
    :try_start_1
    const-string v1, "BTPbapSession"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 296
    iput-object v3, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mObexSession:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;

    goto :goto_0

    .end local v0    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v1

    iput-object v3, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mObexSession:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;

    throw v1
.end method

.method private stopRfcomm()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 261
    const-string v0, "BTPbapSession"

    const-string v1, "stopRfcomm"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mConnectThread:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession$RfcommConnectThread;

    if-eqz v0, :cond_0

    .line 265
    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mConnectThread:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession$RfcommConnectThread;

    invoke-virtual {v0}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession$RfcommConnectThread;->join()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 268
    :goto_0
    iput-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mConnectThread:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession$RfcommConnectThread;

    .line 271
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mTransport:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexTransport;

    if-eqz v0, :cond_1

    .line 273
    :try_start_1
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mTransport:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexTransport;

    invoke-virtual {v0}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexTransport;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 276
    :goto_1
    iput-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mTransport:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexTransport;

    .line 278
    :cond_1
    return-void

    .line 274
    :catch_0
    move-exception v0

    goto :goto_1

    .line 266
    :catch_1
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public abort()V
    .locals 3

    .prologue
    .line 193
    const-string v0, "BTPbapSession"

    const-string v1, "abort"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mPendingRequest:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mParentHandler:Landroid/os/Handler;

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mPendingRequest:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 198
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mPendingRequest:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;

    .line 201
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mObexSession:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;

    if-eqz v0, :cond_1

    .line 202
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mObexSession:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;

    invoke-virtual {v0}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->abort()V

    .line 204
    :cond_1
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v5, 0x7

    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 92
    const-string v0, "BTPbapSession"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Handler: msg: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 166
    const/4 v0, 0x0

    .line 169
    :goto_0
    return v0

    .line 96
    :sswitch_0
    iput-object v3, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mConnectThread:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession$RfcommConnectThread;

    .line 98
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mParentHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 100
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mPendingRequest:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mParentHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mPendingRequest:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;

    invoke-virtual {v0, v4, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 102
    iput-object v3, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mPendingRequest:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;

    .line 169
    :cond_0
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 107
    :sswitch_1
    iput-object v3, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mConnectThread:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession$RfcommConnectThread;

    .line 108
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexTransport;

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mTransport:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexTransport;

    .line 109
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->startObexSession()V

    goto :goto_1

    .line 113
    :sswitch_2
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->stopObexSession()V

    .line 115
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mParentHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 117
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mPendingRequest:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mParentHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mPendingRequest:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;

    invoke-virtual {v0, v4, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 119
    iput-object v3, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mPendingRequest:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;

    goto :goto_1

    .line 124
    :sswitch_3
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mParentHandler:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 126
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mPendingRequest:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mObexSession:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;

    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mPendingRequest:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->schedule(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;)Z

    .line 128
    iput-object v3, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mPendingRequest:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;

    goto :goto_1

    .line 133
    :sswitch_4
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mParentHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 134
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->stopRfcomm()V

    goto :goto_1

    .line 139
    :sswitch_5
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mParentHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_1

    .line 144
    :sswitch_6
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mParentHandler:Landroid/os/Handler;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0, v4, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_1

    .line 149
    :sswitch_7
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mParentHandler:Landroid/os/Handler;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 151
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mSessionHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mSessionHandler:Landroid/os/Handler;

    const/16 v2, 0x6a

    .line 154
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x7530

    .line 152
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_1

    .line 160
    :sswitch_8
    invoke-virtual {p0, v3}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->setAuthResponse(Ljava/lang/String;)Z

    .line 162
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mParentHandler:Landroid/os/Handler;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    .line 94
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_0
        0x64 -> :sswitch_3
        0x65 -> :sswitch_2
        0x66 -> :sswitch_4
        0x67 -> :sswitch_5
        0x68 -> :sswitch_6
        0x69 -> :sswitch_7
        0x6a -> :sswitch_8
    .end sparse-switch
.end method

.method public makeRequest(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;)Z
    .locals 3
    .param p1, "request"    # Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;

    .prologue
    .line 207
    const-string v0, "BTPbapSession"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "makeRequest: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mPendingRequest:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;

    if-eqz v0, :cond_0

    .line 210
    const-string v0, "BTPbapSession"

    const-string v1, "makeRequest: request already queued, exiting"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    const/4 v0, 0x0

    .line 227
    :goto_0
    return v0

    .line 214
    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mObexSession:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;

    if-nez v0, :cond_1

    .line 215
    iput-object p1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mPendingRequest:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;

    .line 222
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->startRfcomm()V

    .line 224
    const/4 v0, 0x1

    goto :goto_0

    .line 227
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mObexSession:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->schedule(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;)Z

    move-result v0

    goto :goto_0
.end method

.method public setAuthResponse(Ljava/lang/String;)Z
    .locals 3
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 231
    const-string v0, "BTPbapSession"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setAuthResponse key="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mSessionHandler:Landroid/os/Handler;

    const/16 v1, 0x6a

    .line 234
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 237
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mObexSession:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;

    if-nez v0, :cond_0

    .line 238
    const/4 v0, 0x0

    .line 241
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mObexSession:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;

    invoke-virtual {v0, p1}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapObexSession;->setAuthReply(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public start()V
    .locals 2

    .prologue
    .line 173
    const-string v0, "BTPbapSession"

    const-string v1, "start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->startRfcomm()V

    .line 176
    return-void
.end method

.method public stop()V
    .locals 4

    .prologue
    .line 179
    const-string v1, "BTPbapSession"

    const-string v2, "Stop"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->stopObexSession()V

    .line 182
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->stopRfcomm()V

    .line 184
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 185
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->quit()Z

    move-result v0

    .line 186
    .local v0, "quit":Z
    const-string v1, "BTPbapSession"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Stopped handler thread:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " id["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-static {v3}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    .end local v0    # "quit":Z
    :goto_0
    return-void

    .line 188
    :cond_0
    const-string v1, "BTPbapSession"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handler thread not running id["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapSession;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-static {v3}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
