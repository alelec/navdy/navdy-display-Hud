.class final Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullVcardListing;
.super Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;
.source "BluetoothPbapRequestPullVcardListing.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "BTPbapReqPullVcardL"

.field private static final TYPE:Ljava/lang/String; = "x-bt/vcard-listing"


# instance fields
.field private mNewMissedCalls:I

.field private mResponse:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapVcardListing;


# direct methods
.method public constructor <init>(Ljava/lang/String;BBLjava/lang/String;II)V
    .locals 5
    .param p1, "folder"    # Ljava/lang/String;
    .param p2, "order"    # B
    .param p3, "searchAttr"    # B
    .param p4, "searchVal"    # Ljava/lang/String;
    .param p5, "maxListCount"    # I
    .param p6, "listStartOffset"    # I

    .prologue
    const v2, 0xffff

    const/4 v4, 0x1

    .line 40
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;-><init>()V

    .line 35
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullVcardListing;->mResponse:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapVcardListing;

    .line 37
    const/4 v1, -0x1

    iput v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullVcardListing;->mNewMissedCalls:I

    .line 42
    if-ltz p5, :cond_0

    if-le p5, v2, :cond_1

    .line 43
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "maxListCount should be [0..65535]"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 46
    :cond_1
    if-ltz p6, :cond_2

    if-le p6, v2, :cond_3

    .line 47
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "listStartOffset should be [0..65535]"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 50
    :cond_3
    if-nez p1, :cond_4

    .line 51
    const-string p1, ""

    .line 54
    :cond_4
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullVcardListing;->mHeaderSet:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    invoke-virtual {v1, v4, p1}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    .line 56
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullVcardListing;->mHeaderSet:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    const/16 v2, 0x42

    const-string v3, "x-bt/vcard-listing"

    invoke-virtual {v1, v2, v3}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    .line 58
    new-instance v0, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;

    invoke-direct {v0}, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;-><init>()V

    .line 60
    .local v0, "oap":Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;
    if-ltz p2, :cond_5

    .line 61
    invoke-virtual {v0, v4, p2}, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->add(BB)V

    .line 64
    :cond_5
    if-eqz p4, :cond_6

    .line 65
    const/4 v1, 0x3

    invoke-virtual {v0, v1, p3}, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->add(BB)V

    .line 66
    const/4 v1, 0x2

    invoke-virtual {v0, v1, p4}, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->add(BLjava/lang/String;)V

    .line 73
    :cond_6
    if-lez p5, :cond_7

    .line 74
    const/4 v1, 0x4

    int-to-short v2, p5

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->add(BS)V

    .line 77
    :cond_7
    if-lez p6, :cond_8

    .line 78
    const/4 v1, 0x5

    int-to-short v2, p6

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->add(BS)V

    .line 81
    :cond_8
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullVcardListing;->mHeaderSet:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->addToHeaderSet(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;)V

    .line 82
    return-void
.end method


# virtual methods
.method public getList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapCard;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullVcardListing;->mResponse:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapVcardListing;

    invoke-virtual {v0}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapVcardListing;->getList()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getNewMissedCalls()I
    .locals 1

    .prologue
    .line 107
    iget v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullVcardListing;->mNewMissedCalls:I

    return v0
.end method

.method protected readResponse(Ljava/io/InputStream;)V
    .locals 2
    .param p1, "stream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 86
    const-string v0, "BTPbapReqPullVcardL"

    const-string v1, "readResponse"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    new-instance v0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapVcardListing;

    invoke-direct {v0, p1}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapVcardListing;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullVcardListing;->mResponse:Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapVcardListing;

    .line 89
    return-void
.end method

.method protected readResponseHeaders(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;)V
    .locals 4
    .param p1, "headerset"    # Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    .prologue
    const/16 v3, 0x9

    .line 93
    const-string v1, "BTPbapReqPullVcardL"

    const-string v2, "readResponseHeaders"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    invoke-static {p1}, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->fromHeaderSet(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;)Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;

    move-result-object v0

    .line 97
    .local v0, "oap":Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;
    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->exists(B)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 98
    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->getByte(B)B

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullVcardListing;->mNewMissedCalls:I

    .line 100
    :cond_0
    return-void
.end method
