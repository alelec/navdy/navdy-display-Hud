.class Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapVcardList;
.super Ljava/lang/Object;
.source "BluetoothPbapVcardList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapVcardList$CardEntryHandler;
    }
.end annotation


# instance fields
.field private final mCards:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/io/InputStream;B)V
    .locals 1
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "format"    # B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapVcardList;->mCards:Ljava/util/ArrayList;

    .line 52
    invoke-direct {p0, p1, p2}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapVcardList;->parse(Ljava/io/InputStream;B)V

    .line 53
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapVcardList;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapVcardList;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapVcardList;->mCards:Ljava/util/ArrayList;

    return-object v0
.end method

.method private parse(Ljava/io/InputStream;B)V
    .locals 6
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "format"    # B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    const/4 v5, 0x1

    if-ne p2, v5, :cond_0

    .line 59
    new-instance v4, Lcom/navdy/hud/app/bluetooth/vcard/VCardParser_V30;

    invoke-direct {v4}, Lcom/navdy/hud/app/bluetooth/vcard/VCardParser_V30;-><init>()V

    .line 64
    .local v4, "parser":Lcom/navdy/hud/app/bluetooth/vcard/VCardParser;
    :goto_0
    new-instance v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryConstructor;

    invoke-direct {v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryConstructor;-><init>()V

    .line 65
    .local v0, "constructor":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryConstructor;
    new-instance v1, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryCounter;

    invoke-direct {v1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryCounter;-><init>()V

    .line 66
    .local v1, "counter":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryCounter;
    new-instance v3, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapVcardList$CardEntryHandler;

    invoke-direct {v3, p0}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapVcardList$CardEntryHandler;-><init>(Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapVcardList;)V

    .line 68
    .local v3, "handler":Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapVcardList$CardEntryHandler;
    invoke-virtual {v0, v3}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryConstructor;->addEntryHandler(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryHandler;)V

    .line 70
    invoke-virtual {v4, v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardParser;->addInterpreter(Lcom/navdy/hud/app/bluetooth/vcard/VCardInterpreter;)V

    .line 71
    invoke-virtual {v4, v1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardParser;->addInterpreter(Lcom/navdy/hud/app/bluetooth/vcard/VCardInterpreter;)V

    .line 74
    :try_start_0
    invoke-virtual {v4, p1}, Lcom/navdy/hud/app/bluetooth/vcard/VCardParser;->parse(Ljava/io/InputStream;)V
    :try_end_0
    .catch Lcom/navdy/hud/app/bluetooth/vcard/exception/VCardException; {:try_start_0 .. :try_end_0} :catch_0

    .line 78
    :goto_1
    return-void

    .line 61
    .end local v0    # "constructor":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryConstructor;
    .end local v1    # "counter":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryCounter;
    .end local v3    # "handler":Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapVcardList$CardEntryHandler;
    .end local v4    # "parser":Lcom/navdy/hud/app/bluetooth/vcard/VCardParser;
    :cond_0
    new-instance v4, Lcom/navdy/hud/app/bluetooth/vcard/VCardParser_V21;

    invoke-direct {v4}, Lcom/navdy/hud/app/bluetooth/vcard/VCardParser_V21;-><init>()V

    .restart local v4    # "parser":Lcom/navdy/hud/app/bluetooth/vcard/VCardParser;
    goto :goto_0

    .line 75
    .restart local v0    # "constructor":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryConstructor;
    .restart local v1    # "counter":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntryCounter;
    .restart local v3    # "handler":Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapVcardList$CardEntryHandler;
    :catch_0
    move-exception v2

    .line 76
    .local v2, "e":Lcom/navdy/hud/app/bluetooth/vcard/exception/VCardException;
    invoke-virtual {v2}, Lcom/navdy/hud/app/bluetooth/vcard/exception/VCardException;->printStackTrace()V

    goto :goto_1
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapVcardList;->mCards:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getFirst()Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapVcardList;->mCards:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;

    return-object v0
.end method

.method public getList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapVcardList;->mCards:Ljava/util/ArrayList;

    return-object v0
.end method
