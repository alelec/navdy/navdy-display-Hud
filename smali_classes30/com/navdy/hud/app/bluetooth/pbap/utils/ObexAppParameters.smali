.class public final Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;
.super Ljava/lang/Object;
.source "ObexAppParameters.java"


# instance fields
.field private final mParams:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Byte;",
            "[B>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->mParams:Ljava/util/HashMap;

    .line 33
    return-void
.end method

.method public constructor <init>([B)V
    .locals 7
    .param p1, "raw"    # [B

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    iput-object v5, p0, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->mParams:Ljava/util/HashMap;

    .line 38
    if-eqz p1, :cond_0

    .line 39
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v5, p1

    if-ge v0, v5, :cond_0

    .line 40
    array-length v5, p1

    sub-int/2addr v5, v0

    const/4 v6, 0x2

    if-ge v5, v6, :cond_1

    .line 59
    .end local v0    # "i":I
    :cond_0
    return-void

    .line 44
    .restart local v0    # "i":I
    :cond_1
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    aget-byte v3, p1, v0

    .line 45
    .local v3, "tag":B
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    aget-byte v2, p1, v1

    .line 47
    .local v2, "len":B
    array-length v5, p1

    sub-int/2addr v5, v0

    sub-int/2addr v5, v2

    if-ltz v5, :cond_0

    .line 51
    new-array v4, v2, [B

    .line 53
    .local v4, "val":[B
    const/4 v5, 0x0

    invoke-static {p1, v0, v4, v5, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 54
    invoke-virtual {p0, v3, v4}, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->add(B[B)V

    .line 56
    add-int/2addr v0, v2

    .line 57
    goto :goto_0
.end method

.method public static fromHeaderSet(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;)Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;
    .locals 3
    .param p0, "headerset"    # Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    .prologue
    .line 63
    const/16 v2, 0x4c

    :try_start_0
    invoke-virtual {p0, v2}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    move-object v0, v2

    check-cast v0, [B

    move-object v1, v0

    .line 64
    .local v1, "raw":[B
    new-instance v2, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;

    invoke-direct {v2, v1}, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;-><init>([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 69
    .end local v1    # "raw":[B
    :goto_0
    return-object v2

    .line 65
    :catch_0
    move-exception v2

    .line 69
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public add(BB)V
    .locals 3
    .param p1, "tag"    # B
    .param p2, "val"    # B

    .prologue
    .line 105
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    .line 106
    .local v0, "bval":[B
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->mParams:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    return-void
.end method

.method public add(BI)V
    .locals 3
    .param p1, "tag"    # B
    .param p2, "val"    # I

    .prologue
    .line 115
    const/4 v1, 0x4

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    .line 116
    .local v0, "bval":[B
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->mParams:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    return-void
.end method

.method public add(BJ)V
    .locals 4
    .param p1, "tag"    # B
    .param p2, "val"    # J

    .prologue
    .line 120
    const/16 v1, 0x8

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    .line 121
    .local v0, "bval":[B
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->mParams:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    return-void
.end method

.method public add(BLjava/lang/String;)V
    .locals 3
    .param p1, "tag"    # B
    .param p2, "val"    # Ljava/lang/String;

    .prologue
    .line 125
    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 126
    .local v0, "bval":[B
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->mParams:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    return-void
.end method

.method public add(BS)V
    .locals 3
    .param p1, "tag"    # B
    .param p2, "val"    # S

    .prologue
    .line 110
    const/4 v1, 0x2

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    .line 111
    .local v0, "bval":[B
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->mParams:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    return-void
.end method

.method public add(B[B)V
    .locals 2
    .param p1, "tag"    # B
    .param p2, "bval"    # [B

    .prologue
    .line 130
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->mParams:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    return-void
.end method

.method public addToHeaderSet(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;)V
    .locals 2
    .param p1, "headerset"    # Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    .prologue
    .line 95
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->mParams:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 96
    const/16 v0, 0x4c

    invoke-virtual {p0}, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->getHeader()[B

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    .line 98
    :cond_0
    return-void
.end method

.method public exists(B)Z
    .locals 2
    .param p1, "tag"    # B

    .prologue
    .line 101
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->mParams:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getByte(B)B
    .locals 3
    .param p1, "tag"    # B

    .prologue
    .line 134
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->mParams:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 136
    .local v0, "bval":[B
    if-eqz v0, :cond_0

    array-length v1, v0

    const/4 v2, 0x1

    if-ge v1, v2, :cond_1

    .line 137
    :cond_0
    const/4 v1, 0x0

    .line 140
    :goto_0
    return v1

    :cond_1
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->get()B

    move-result v1

    goto :goto_0
.end method

.method public getByteArray(B)[B
    .locals 3
    .param p1, "tag"    # B

    .prologue
    .line 174
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->mParams:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 176
    .local v0, "bval":[B
    return-object v0
.end method

.method public getHeader()[B
    .locals 8

    .prologue
    .line 73
    const/4 v3, 0x0

    .line 75
    .local v3, "length":I
    iget-object v5, p0, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->mParams:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 76
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Byte;[B>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [B

    array-length v5, v5

    add-int/lit8 v5, v5, 0x2

    add-int/2addr v3, v5

    .line 77
    goto :goto_0

    .line 79
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Byte;[B>;"
    :cond_0
    new-array v4, v3, [B

    .line 81
    .local v4, "ret":[B
    const/4 v1, 0x0

    .line 82
    .local v1, "idx":I
    iget-object v5, p0, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->mParams:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 83
    .restart local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Byte;[B>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [B

    array-length v3, v5

    .line 85
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "idx":I
    .local v2, "idx":I
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Byte;

    invoke-virtual {v5}, Ljava/lang/Byte;->byteValue()B

    move-result v5

    aput-byte v5, v4, v1

    .line 86
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "idx":I
    .restart local v1    # "idx":I
    int-to-byte v5, v3

    aput-byte v5, v4, v2

    .line 87
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    const/4 v7, 0x0

    invoke-static {v5, v7, v4, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 88
    add-int/2addr v1, v3

    .line 89
    goto :goto_1

    .line 91
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Byte;[B>;"
    :cond_1
    return-object v4
.end method

.method public getInt(B)I
    .locals 3
    .param p1, "tag"    # B

    .prologue
    .line 154
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->mParams:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 156
    .local v0, "bval":[B
    if-eqz v0, :cond_0

    array-length v1, v0

    const/4 v2, 0x4

    if-ge v1, v2, :cond_1

    .line 157
    :cond_0
    const/4 v1, 0x0

    .line 160
    :goto_0
    return v1

    :cond_1
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    goto :goto_0
.end method

.method public getShort(B)S
    .locals 3
    .param p1, "tag"    # B

    .prologue
    .line 144
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->mParams:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 146
    .local v0, "bval":[B
    if-eqz v0, :cond_0

    array-length v1, v0

    const/4 v2, 0x2

    if-ge v1, v2, :cond_1

    .line 147
    :cond_0
    const/4 v1, 0x0

    .line 150
    :goto_0
    return v1

    :cond_1
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v1

    goto :goto_0
.end method

.method public getString(B)Ljava/lang/String;
    .locals 3
    .param p1, "tag"    # B

    .prologue
    .line 164
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->mParams:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 166
    .local v0, "bval":[B
    if-nez v0, :cond_0

    .line 167
    const/4 v1, 0x0

    .line 170
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->mParams:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
