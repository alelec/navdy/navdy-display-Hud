.class Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullPhoneBookSize;
.super Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;
.source "BluetoothPbapRequestPullPhoneBookSize.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "BTPbapReqPullPBookSize"

.field private static final TYPE:Ljava/lang/String; = "x-bt/phonebook"


# instance fields
.field private mSize:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 4
    .param p1, "pbName"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequest;-><init>()V

    .line 34
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullPhoneBookSize;->mHeaderSet:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p1}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    .line 36
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullPhoneBookSize;->mHeaderSet:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    const/16 v2, 0x42

    const-string v3, "x-bt/phonebook"

    invoke-virtual {v1, v2, v3}, Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    .line 38
    new-instance v0, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;

    invoke-direct {v0}, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;-><init>()V

    .line 39
    .local v0, "oap":Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;
    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->add(BS)V

    .line 40
    iget-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullPhoneBookSize;->mHeaderSet:Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->addToHeaderSet(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;)V

    .line 41
    return-void
.end method


# virtual methods
.method public getSize()I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullPhoneBookSize;->mSize:I

    return v0
.end method

.method protected readResponseHeaders(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;)V
    .locals 3
    .param p1, "headerset"    # Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;

    .prologue
    .line 45
    const-string v1, "BTPbapReqPullPBookSize"

    const-string v2, "readResponseHeaders"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    invoke-static {p1}, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->fromHeaderSet(Lcom/navdy/hud/app/bluetooth/obex/HeaderSet;)Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;

    move-result-object v0

    .line 49
    .local v0, "oap":Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/bluetooth/pbap/utils/ObexAppParameters;->getShort(B)S

    move-result v1

    iput v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapRequestPullPhoneBookSize;->mSize:I

    .line 50
    return-void
.end method
