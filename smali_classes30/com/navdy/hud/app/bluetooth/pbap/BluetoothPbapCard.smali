.class public Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapCard;
.super Ljava/lang/Object;
.source "BluetoothPbapCard.java"


# instance fields
.field public final N:Ljava/lang/String;

.field public final firstName:Ljava/lang/String;

.field public final handle:Ljava/lang/String;

.field public final lastName:Ljava/lang/String;

.field public final middleName:Ljava/lang/String;

.field public final prefix:Ljava/lang/String;

.field public final suffix:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "handle"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapCard;->handle:Ljava/lang/String;

    .line 44
    iput-object p2, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapCard;->N:Ljava/lang/String;

    .line 50
    const-string v1, ";"

    const/4 v3, 0x5

    invoke-virtual {p2, v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v0

    .line 52
    .local v0, "parsedName":[Ljava/lang/String;
    array-length v1, v0

    if-ge v1, v4, :cond_0

    move-object v1, v2

    :goto_0
    iput-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapCard;->lastName:Ljava/lang/String;

    .line 53
    array-length v1, v0

    if-ge v1, v5, :cond_1

    move-object v1, v2

    :goto_1
    iput-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapCard;->firstName:Ljava/lang/String;

    .line 54
    array-length v1, v0

    if-ge v1, v6, :cond_2

    move-object v1, v2

    :goto_2
    iput-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapCard;->middleName:Ljava/lang/String;

    .line 55
    array-length v1, v0

    if-ge v1, v7, :cond_3

    move-object v1, v2

    :goto_3
    iput-object v1, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapCard;->prefix:Ljava/lang/String;

    .line 56
    array-length v1, v0

    const/4 v3, 0x5

    if-ge v1, v3, :cond_4

    :goto_4
    iput-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapCard;->suffix:Ljava/lang/String;

    .line 57
    return-void

    .line 52
    :cond_0
    const/4 v1, 0x0

    aget-object v1, v0, v1

    goto :goto_0

    .line 53
    :cond_1
    aget-object v1, v0, v4

    goto :goto_1

    .line 54
    :cond_2
    aget-object v1, v0, v5

    goto :goto_2

    .line 55
    :cond_3
    aget-object v1, v0, v6

    goto :goto_3

    .line 56
    :cond_4
    aget-object v2, v0, v7

    goto :goto_4
.end method

.method public static jsonifyVcardEntry(Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;)Ljava/lang/String;
    .locals 13
    .param p0, "vcard"    # Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;

    .prologue
    .line 79
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 82
    .local v2, "json":Lorg/json/JSONObject;
    :try_start_0
    invoke-virtual {p0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->getNameData()Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;

    move-result-object v7

    .line 83
    .local v7, "name":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;
    const-string v10, "formatted"

    invoke-virtual {v7}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->getFormatted()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2, v10, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 84
    const-string v10, "family"

    invoke-virtual {v7}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->getFamily()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2, v10, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 85
    const-string v10, "given"

    invoke-virtual {v7}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->getGiven()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2, v10, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 86
    const-string v10, "middle"

    invoke-virtual {v7}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->getMiddle()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2, v10, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 87
    const-string v10, "prefix"

    invoke-virtual {v7}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->getPrefix()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2, v10, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 88
    const-string v10, "suffix"

    invoke-virtual {v7}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;->getSuffix()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2, v10, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_2

    .line 94
    .end local v7    # "name":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$NameData;
    :goto_0
    :try_start_1
    new-instance v6, Lorg/json/JSONArray;

    invoke-direct {v6}, Lorg/json/JSONArray;-><init>()V

    .line 96
    .local v6, "jsonPhones":Lorg/json/JSONArray;
    invoke-virtual {p0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->getPhoneList()Ljava/util/List;

    move-result-object v9

    .line 98
    .local v9, "phones":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhoneData;>;"
    if-eqz v9, :cond_0

    .line 99
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhoneData;

    .line 100
    .local v8, "phone":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhoneData;
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 101
    .local v5, "jsonPhone":Lorg/json/JSONObject;
    const-string v11, "type"

    invoke-virtual {v8}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhoneData;->getType()I

    move-result v12

    invoke-virtual {v5, v11, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 102
    const-string v11, "number"

    invoke-virtual {v8}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhoneData;->getNumber()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v5, v11, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 103
    const-string v11, "label"

    invoke-virtual {v8}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhoneData;->getLabel()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v5, v11, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 104
    const-string v11, "is_primary"

    invoke-virtual {v8}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhoneData;->isPrimary()Z

    move-result v12

    invoke-virtual {v5, v11, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 106
    invoke-virtual {v6, v5}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 111
    .end local v5    # "jsonPhone":Lorg/json/JSONObject;
    .end local v6    # "jsonPhones":Lorg/json/JSONArray;
    .end local v8    # "phone":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhoneData;
    .end local v9    # "phones":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhoneData;>;"
    :catch_0
    move-exception v10

    .line 116
    :cond_0
    :goto_2
    :try_start_2
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4}, Lorg/json/JSONArray;-><init>()V

    .line 118
    .local v4, "jsonEmails":Lorg/json/JSONArray;
    invoke-virtual {p0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry;->getEmailList()Ljava/util/List;

    move-result-object v1

    .line 120
    .local v1, "emails":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EmailData;>;"
    if-eqz v1, :cond_1

    .line 121
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_3

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EmailData;

    .line 122
    .local v0, "email":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EmailData;
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 123
    .local v3, "jsonEmail":Lorg/json/JSONObject;
    const-string v11, "type"

    invoke-virtual {v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EmailData;->getType()I

    move-result v12

    invoke-virtual {v3, v11, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 124
    const-string v11, "address"

    invoke-virtual {v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EmailData;->getAddress()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v3, v11, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 125
    const-string v11, "label"

    invoke-virtual {v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EmailData;->getLabel()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v3, v11, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 126
    const-string v11, "is_primary"

    invoke-virtual {v0}, Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EmailData;->isPrimary()Z

    move-result v12

    invoke-virtual {v3, v11, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 128
    invoke-virtual {v4, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_3

    .line 133
    .end local v0    # "email":Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EmailData;
    .end local v1    # "emails":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EmailData;>;"
    .end local v3    # "jsonEmail":Lorg/json/JSONObject;
    .end local v4    # "jsonEmails":Lorg/json/JSONArray;
    :catch_1
    move-exception v10

    .line 137
    :cond_1
    :goto_4
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v10

    return-object v10

    .line 109
    .restart local v6    # "jsonPhones":Lorg/json/JSONArray;
    .restart local v9    # "phones":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhoneData;>;"
    :cond_2
    :try_start_3
    const-string v10, "phones"

    invoke-virtual {v2, v10, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_2

    .line 131
    .end local v6    # "jsonPhones":Lorg/json/JSONArray;
    .end local v9    # "phones":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$PhoneData;>;"
    .restart local v1    # "emails":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EmailData;>;"
    .restart local v4    # "jsonEmails":Lorg/json/JSONArray;
    :cond_3
    :try_start_4
    const-string v10, "emails"

    invoke-virtual {v2, v10, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_4

    .line 89
    .end local v1    # "emails":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/hud/app/bluetooth/vcard/VCardEntry$EmailData;>;"
    .end local v4    # "jsonEmails":Lorg/json/JSONArray;
    :catch_2
    move-exception v10

    goto/16 :goto_0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 61
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 64
    .local v0, "json":Lorg/json/JSONObject;
    :try_start_0
    const-string v1, "handle"

    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapCard;->handle:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 65
    const-string v1, "N"

    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapCard;->N:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 66
    const-string v1, "lastName"

    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapCard;->lastName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 67
    const-string v1, "firstName"

    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapCard;->firstName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 68
    const-string v1, "middleName"

    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapCard;->middleName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 69
    const-string v1, "prefix"

    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapCard;->prefix:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 70
    const-string v1, "suffix"

    iget-object v2, p0, Lcom/navdy/hud/app/bluetooth/pbap/BluetoothPbapCard;->suffix:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 71
    :catch_0
    move-exception v1

    goto :goto_0
.end method
