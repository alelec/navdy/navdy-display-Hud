.class public final Lcom/navdy/hud/app/util/ReportIssueService$$InjectAdapter;
.super Ldagger/internal/Binding;
.source "ReportIssueService$$InjectAdapter.java"

# interfaces
.implements Ljavax/inject/Provider;
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldagger/internal/Binding",
        "<",
        "Lcom/navdy/hud/app/util/ReportIssueService;",
        ">;",
        "Ljavax/inject/Provider",
        "<",
        "Lcom/navdy/hud/app/util/ReportIssueService;",
        ">;",
        "Ldagger/MembersInjector",
        "<",
        "Lcom/navdy/hud/app/util/ReportIssueService;",
        ">;"
    }
.end annotation


# instance fields
.field private bus:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/squareup/otto/Bus;",
            ">;"
        }
    .end annotation
.end field

.field private driveRecorder:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/debug/DriveRecorder;",
            ">;"
        }
    .end annotation
.end field

.field private gestureService:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/hud/app/gesture/GestureServiceConnector;",
            ">;"
        }
    .end annotation
.end field

.field private mHttpManager:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/service/library/network/http/IHttpManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 31
    const-string v0, "com.navdy.hud.app.util.ReportIssueService"

    const-string v1, "members/com.navdy.hud.app.util.ReportIssueService"

    const/4 v2, 0x0

    const-class v3, Lcom/navdy/hud/app/util/ReportIssueService;

    invoke-direct {p0, v0, v1, v2, v3}, Ldagger/internal/Binding;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/Object;)V

    .line 32
    return-void
.end method


# virtual methods
.method public attach(Ldagger/internal/Linker;)V
    .locals 3
    .param p1, "linker"    # Ldagger/internal/Linker;

    .prologue
    .line 41
    const-string v0, "com.navdy.service.library.network.http.IHttpManager"

    const-class v1, Lcom/navdy/hud/app/util/ReportIssueService;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/util/ReportIssueService$$InjectAdapter;->mHttpManager:Ldagger/internal/Binding;

    .line 42
    const-string v0, "com.navdy.hud.app.gesture.GestureServiceConnector"

    const-class v1, Lcom/navdy/hud/app/util/ReportIssueService;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/util/ReportIssueService$$InjectAdapter;->gestureService:Ldagger/internal/Binding;

    .line 43
    const-string v0, "com.squareup.otto.Bus"

    const-class v1, Lcom/navdy/hud/app/util/ReportIssueService;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/util/ReportIssueService$$InjectAdapter;->bus:Ldagger/internal/Binding;

    .line 44
    const-string v0, "com.navdy.hud.app.debug.DriveRecorder"

    const-class v1, Lcom/navdy/hud/app/util/ReportIssueService;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/util/ReportIssueService$$InjectAdapter;->driveRecorder:Ldagger/internal/Binding;

    .line 45
    return-void
.end method

.method public get()Lcom/navdy/hud/app/util/ReportIssueService;
    .locals 1

    .prologue
    .line 65
    new-instance v0, Lcom/navdy/hud/app/util/ReportIssueService;

    invoke-direct {v0}, Lcom/navdy/hud/app/util/ReportIssueService;-><init>()V

    .line 66
    .local v0, "result":Lcom/navdy/hud/app/util/ReportIssueService;
    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/util/ReportIssueService$$InjectAdapter;->injectMembers(Lcom/navdy/hud/app/util/ReportIssueService;)V

    .line 67
    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/navdy/hud/app/util/ReportIssueService$$InjectAdapter;->get()Lcom/navdy/hud/app/util/ReportIssueService;

    move-result-object v0

    return-object v0
.end method

.method public getDependencies(Ljava/util/Set;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ldagger/internal/Binding",
            "<*>;>;",
            "Ljava/util/Set",
            "<",
            "Ldagger/internal/Binding",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 53
    .local p1, "getBindings":Ljava/util/Set;, "Ljava/util/Set<Ldagger/internal/Binding<*>;>;"
    .local p2, "injectMembersBindings":Ljava/util/Set;, "Ljava/util/Set<Ldagger/internal/Binding<*>;>;"
    iget-object v0, p0, Lcom/navdy/hud/app/util/ReportIssueService$$InjectAdapter;->mHttpManager:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 54
    iget-object v0, p0, Lcom/navdy/hud/app/util/ReportIssueService$$InjectAdapter;->gestureService:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 55
    iget-object v0, p0, Lcom/navdy/hud/app/util/ReportIssueService$$InjectAdapter;->bus:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 56
    iget-object v0, p0, Lcom/navdy/hud/app/util/ReportIssueService$$InjectAdapter;->driveRecorder:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 57
    return-void
.end method

.method public injectMembers(Lcom/navdy/hud/app/util/ReportIssueService;)V
    .locals 1
    .param p1, "object"    # Lcom/navdy/hud/app/util/ReportIssueService;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/navdy/hud/app/util/ReportIssueService$$InjectAdapter;->mHttpManager:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/network/http/IHttpManager;

    iput-object v0, p1, Lcom/navdy/hud/app/util/ReportIssueService;->mHttpManager:Lcom/navdy/service/library/network/http/IHttpManager;

    .line 77
    iget-object v0, p0, Lcom/navdy/hud/app/util/ReportIssueService$$InjectAdapter;->gestureService:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    iput-object v0, p1, Lcom/navdy/hud/app/util/ReportIssueService;->gestureService:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    .line 78
    iget-object v0, p0, Lcom/navdy/hud/app/util/ReportIssueService$$InjectAdapter;->bus:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/otto/Bus;

    iput-object v0, p1, Lcom/navdy/hud/app/util/ReportIssueService;->bus:Lcom/squareup/otto/Bus;

    .line 79
    iget-object v0, p0, Lcom/navdy/hud/app/util/ReportIssueService$$InjectAdapter;->driveRecorder:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/debug/DriveRecorder;

    iput-object v0, p1, Lcom/navdy/hud/app/util/ReportIssueService;->driveRecorder:Lcom/navdy/hud/app/debug/DriveRecorder;

    .line 80
    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Lcom/navdy/hud/app/util/ReportIssueService;

    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/util/ReportIssueService$$InjectAdapter;->injectMembers(Lcom/navdy/hud/app/util/ReportIssueService;)V

    return-void
.end method
