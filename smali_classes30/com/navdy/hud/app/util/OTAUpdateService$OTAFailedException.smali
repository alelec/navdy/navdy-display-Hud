.class public Lcom/navdy/hud/app/util/OTAUpdateService$OTAFailedException;
.super Ljava/lang/Exception;
.source "OTAUpdateService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/util/OTAUpdateService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "OTAFailedException"
.end annotation


# instance fields
.field public last_install:Ljava/lang/String;

.field public last_log:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "install"    # Ljava/lang/String;
    .param p2, "log"    # Ljava/lang/String;

    .prologue
    .line 109
    const-string v0, "OTA installation failed"

    invoke-direct {p0, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 110
    iput-object p1, p0, Lcom/navdy/hud/app/util/OTAUpdateService$OTAFailedException;->last_install:Ljava/lang/String;

    .line 111
    iput-object p2, p0, Lcom/navdy/hud/app/util/OTAUpdateService$OTAFailedException;->last_log:Ljava/lang/String;

    .line 112
    return-void
.end method
