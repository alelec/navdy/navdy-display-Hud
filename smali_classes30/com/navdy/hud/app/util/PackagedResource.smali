.class public Lcom/navdy/hud/app/util/PackagedResource;
.super Ljava/lang/Object;
.source "PackagedResource.java"


# static fields
.field private static final BUFFER_SIZE:I = 0x4000

.field private static final MD5_SUFFIX:Ljava/lang/String; = ".md5"

.field private static final lockObject:Ljava/lang/Object;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private basePath:Ljava/lang/String;

.field private destinationFilename:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 29
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/util/PackagedResource;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/util/PackagedResource;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 36
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/util/PackagedResource;->lockObject:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "destinationFilename"    # Ljava/lang/String;
    .param p2, "basePath"    # Ljava/lang/String;

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/navdy/hud/app/util/PackagedResource;->destinationFilename:Ljava/lang/String;

    .line 40
    iput-object p2, p0, Lcom/navdy/hud/app/util/PackagedResource;->basePath:Ljava/lang/String;

    .line 41
    return-void
.end method

.method private static extractFile(Ljava/util/zip/ZipInputStream;Ljava/lang/String;[B)V
    .locals 6
    .param p0, "zis"    # Ljava/util/zip/ZipInputStream;
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "buffer"    # [B

    .prologue
    .line 157
    const/4 v1, 0x0

    .line 161
    .local v1, "fout":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 163
    .end local v1    # "fout":Ljava/io/FileOutputStream;
    .local v2, "fout":Ljava/io/FileOutputStream;
    :goto_0
    :try_start_1
    invoke-virtual {p0, p2}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v0

    .local v0, "count":I
    const/4 v4, -0x1

    if-eq v0, v4, :cond_0

    .line 164
    const/4 v4, 0x0

    invoke-virtual {v2, p2, v4, v0}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 168
    .end local v0    # "count":I
    :catch_0
    move-exception v3

    move-object v1, v2

    .line 169
    .end local v2    # "fout":Ljava/io/FileOutputStream;
    .restart local v1    # "fout":Ljava/io/FileOutputStream;
    .local v3, "t":Ljava/lang/Throwable;
    :goto_1
    :try_start_2
    sget-object v4, Lcom/navdy/hud/app/util/PackagedResource;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "error extracting file"

    invoke-virtual {v4, v5, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 171
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 173
    .end local v3    # "t":Ljava/lang/Throwable;
    :goto_2
    return-void

    .line 167
    .end local v1    # "fout":Ljava/io/FileOutputStream;
    .restart local v0    # "count":I
    .restart local v2    # "fout":Ljava/io/FileOutputStream;
    :cond_0
    :try_start_3
    invoke-static {v2}, Lcom/navdy/service/library/util/IOUtils;->fileSync(Ljava/io/FileOutputStream;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 171
    invoke-static {v2}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    move-object v1, v2

    .line 172
    .end local v2    # "fout":Ljava/io/FileOutputStream;
    .restart local v1    # "fout":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 171
    .end local v0    # "count":I
    :catchall_0
    move-exception v4

    :goto_3
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v4

    .end local v1    # "fout":Ljava/io/FileOutputStream;
    .restart local v2    # "fout":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v4

    move-object v1, v2

    .end local v2    # "fout":Ljava/io/FileOutputStream;
    .restart local v1    # "fout":Ljava/io/FileOutputStream;
    goto :goto_3

    .line 168
    :catch_1
    move-exception v3

    goto :goto_1
.end method

.method private static unpack(Ljava/lang/String;Ljava/io/InputStream;)Z
    .locals 9
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "is"    # Ljava/io/InputStream;

    .prologue
    .line 120
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 122
    .local v1, "destDir":Ljava/io/File;
    const/16 v7, 0x4000

    new-array v0, v7, [B

    .line 124
    .local v0, "buffer":[B
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_0

    .line 125
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 128
    :cond_0
    new-instance v6, Ljava/util/zip/ZipInputStream;

    new-instance v7, Ljava/io/BufferedInputStream;

    invoke-direct {v7, p1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v6, v7}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .line 132
    .local v6, "zis":Ljava/util/zip/ZipInputStream;
    :goto_0
    :try_start_0
    invoke-virtual {v6}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v5

    .local v5, "ze":Ljava/util/zip/ZipEntry;
    if-eqz v5, :cond_2

    .line 133
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 135
    .local v4, "filePath":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/util/zip/ZipEntry;->isDirectory()Z

    move-result v7

    if-nez v7, :cond_1

    .line 137
    invoke-static {v6, v4, v0}, Lcom/navdy/hud/app/util/PackagedResource;->extractFile(Ljava/util/zip/ZipInputStream;Ljava/lang/String;[B)V

    .line 144
    :goto_1
    invoke-virtual {v6}, Ljava/util/zip/ZipInputStream;->closeEntry()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 146
    .end local v4    # "filePath":Ljava/lang/String;
    .end local v5    # "ze":Ljava/util/zip/ZipEntry;
    :catch_0
    move-exception v3

    .line 147
    .local v3, "e":Ljava/lang/Throwable;
    :try_start_1
    sget-object v7, Lcom/navdy/hud/app/util/PackagedResource;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "Error expanding zipEntry"

    invoke-virtual {v7, v8, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 148
    const/4 v7, 0x0

    .line 150
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 153
    .end local v3    # "e":Ljava/lang/Throwable;
    :goto_2
    return v7

    .line 140
    .restart local v4    # "filePath":Ljava/lang/String;
    .restart local v5    # "ze":Ljava/util/zip/ZipEntry;
    :cond_1
    :try_start_2
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 141
    .local v2, "dir":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 150
    .end local v2    # "dir":Ljava/io/File;
    .end local v4    # "filePath":Ljava/lang/String;
    .end local v5    # "ze":Ljava/util/zip/ZipEntry;
    :catchall_0
    move-exception v7

    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v7

    .restart local v5    # "ze":Ljava/util/zip/ZipEntry;
    :cond_2
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 153
    const/4 v7, 0x1

    goto :goto_2
.end method

.method public static unpackZip(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "zipname"    # Ljava/lang/String;

    .prologue
    .line 106
    const/4 v1, 0x0

    .line 109
    .local v1, "is":Ljava/io/InputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 110
    .end local v1    # "is":Ljava/io/InputStream;
    .local v2, "is":Ljava/io/InputStream;
    :try_start_1
    invoke-static {p0, v2}, Lcom/navdy/hud/app/util/PackagedResource;->unpack(Ljava/lang/String;Ljava/io/InputStream;)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v3

    .line 115
    invoke-static {v2}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    move-object v1, v2

    .end local v2    # "is":Ljava/io/InputStream;
    .restart local v1    # "is":Ljava/io/InputStream;
    :goto_0
    return v3

    .line 111
    :catch_0
    move-exception v0

    .line 112
    .local v0, "e":Ljava/lang/Throwable;
    :goto_1
    :try_start_2
    sget-object v3, Lcom/navdy/hud/app/util/PackagedResource;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Error reading resource"

    invoke-virtual {v3, v4, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 113
    const/4 v3, 0x0

    .line 115
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Throwable;
    :catchall_0
    move-exception v3

    :goto_2
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v3

    .end local v1    # "is":Ljava/io/InputStream;
    .restart local v2    # "is":Ljava/io/InputStream;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "is":Ljava/io/InputStream;
    .restart local v1    # "is":Ljava/io/InputStream;
    goto :goto_2

    .line 111
    .end local v1    # "is":Ljava/io/InputStream;
    .restart local v2    # "is":Ljava/io/InputStream;
    :catch_1
    move-exception v0

    move-object v1, v2

    .end local v2    # "is":Ljava/io/InputStream;
    .restart local v1    # "is":Ljava/io/InputStream;
    goto :goto_1
.end method


# virtual methods
.method public updateFromResources(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 44
    const/4 v0, -0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/navdy/hud/app/util/PackagedResource;->updateFromResources(Landroid/content/Context;II)V

    .line 45
    return-void
.end method

.method public updateFromResources(Landroid/content/Context;II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resId"    # I
    .param p3, "hashResId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/navdy/hud/app/util/PackagedResource;->updateFromResources(Landroid/content/Context;IIZ)V

    .line 49
    return-void
.end method

.method public updateFromResources(Landroid/content/Context;IIZ)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resId"    # I
    .param p3, "hashResId"    # I
    .param p4, "unzip"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 52
    sget-object v9, Lcom/navdy/hud/app/util/PackagedResource;->lockObject:Ljava/lang/Object;

    monitor-enter v9

    .line 53
    const/4 v1, 0x1

    .line 54
    .local v1, "copyFile":Z
    const/4 v4, 0x0

    .line 55
    .local v4, "md5InputStream":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 56
    .local v6, "resources":Landroid/content/res/Resources;
    new-instance v2, Ljava/io/File;

    iget-object v8, p0, Lcom/navdy/hud/app/util/PackagedResource;->basePath:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/navdy/hud/app/util/PackagedResource;->destinationFilename:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ".md5"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v2, v8, v10}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    .local v2, "md5File":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    iget-object v8, p0, Lcom/navdy/hud/app/util/PackagedResource;->basePath:Ljava/lang/String;

    iget-object v10, p0, Lcom/navdy/hud/app/util/PackagedResource;->destinationFilename:Ljava/lang/String;

    invoke-direct {v0, v8, v10}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    .local v0, "configFile":Ljava/io/File;
    sget-object v8, Lcom/navdy/hud/app/util/PackagedResource;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "updateFromResources ["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/navdy/hud/app/util/PackagedResource;->destinationFilename:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 62
    const/4 v3, 0x0

    .line 64
    .local v3, "md5InApk":Ljava/lang/String;
    if-lez p3, :cond_0

    .line 65
    :try_start_1
    invoke-virtual {v6, p3}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v4

    .line 66
    const-string v8, "UTF-8"

    invoke-static {v4, v8}, Lcom/navdy/service/library/util/IOUtils;->convertInputStreamToString(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 67
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 68
    const/4 v4, 0x0

    .line 71
    :cond_0
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_3

    if-nez p4, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->exists()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v8

    if-eqz v8, :cond_3

    .line 73
    :cond_1
    :try_start_2
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->convertFileToString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 74
    .local v5, "md5OnDevice":Ljava/lang/String;
    invoke-static {v5, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 75
    const/4 v1, 0x0

    .line 76
    sget-object v8, Lcom/navdy/hud/app/util/PackagedResource;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "no update required signature matches:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 86
    .end local v5    # "md5OnDevice":Ljava/lang/String;
    :goto_0
    if-nez v1, :cond_4

    .line 100
    :try_start_3
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    monitor-exit v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 103
    :goto_1
    return-void

    .line 78
    .restart local v5    # "md5OnDevice":Ljava/lang/String;
    :cond_2
    :try_start_4
    sget-object v8, Lcom/navdy/hud/app/util/PackagedResource;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "update required, device:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " apk:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 80
    .end local v5    # "md5OnDevice":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 81
    .local v7, "t":Ljava/lang/Throwable;
    :try_start_5
    sget-object v8, Lcom/navdy/hud/app/util/PackagedResource;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v8, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 100
    .end local v7    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v8

    :try_start_6
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v8

    .line 102
    .end local v0    # "configFile":Ljava/io/File;
    .end local v2    # "md5File":Ljava/io/File;
    .end local v3    # "md5InApk":Ljava/lang/String;
    .end local v6    # "resources":Landroid/content/res/Resources;
    :catchall_1
    move-exception v8

    monitor-exit v9
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v8

    .line 84
    .restart local v0    # "configFile":Ljava/io/File;
    .restart local v2    # "md5File":Ljava/io/File;
    .restart local v3    # "md5InApk":Ljava/lang/String;
    .restart local v6    # "resources":Landroid/content/res/Resources;
    :cond_3
    :try_start_7
    sget-object v8, Lcom/navdy/hud/app/util/PackagedResource;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "update required, no file"

    invoke-virtual {v8, v10}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 89
    :cond_4
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, p2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v10

    invoke-static {v8, v10}, Lcom/navdy/service/library/util/IOUtils;->copyFile(Ljava/lang/String;Ljava/io/InputStream;)I

    .line 90
    if-eqz p4, :cond_5

    .line 92
    iget-object v8, p0, Lcom/navdy/hud/app/util/PackagedResource;->basePath:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Lcom/navdy/hud/app/util/PackagedResource;->unpackZip(Ljava/lang/String;Ljava/lang/String;)Z

    .line 93
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-static {p1, v8}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 96
    :cond_5
    if-eqz v3, :cond_6

    .line 97
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v10

    invoke-static {v8, v10}, Lcom/navdy/service/library/util/IOUtils;->copyFile(Ljava/lang/String;[B)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 100
    :cond_6
    :try_start_8
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 102
    monitor-exit v9
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1
.end method
