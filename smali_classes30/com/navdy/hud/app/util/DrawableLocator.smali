.class public Lcom/navdy/hud/app/util/DrawableLocator;
.super Ljava/lang/Object;
.source "DrawableLocator.java"


# instance fields
.field private mDrawable:Landroid/graphics/drawable/Drawable;

.field private mParent:Landroid/view/View;

.field private mResourceId:I


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/content/res/TypedArray;II)V
    .locals 2
    .param p1, "parent"    # Landroid/view/View;
    .param p2, "array"    # Landroid/content/res/TypedArray;
    .param p3, "index"    # I
    .param p4, "defaultId"    # I

    .prologue
    const/4 v1, -0x1

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/hud/app/util/DrawableLocator;->mResourceId:I

    .line 25
    iput-object p1, p0, Lcom/navdy/hud/app/util/DrawableLocator;->mParent:Landroid/view/View;

    .line 26
    invoke-virtual {p1}, Landroid/view/View;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 30
    invoke-virtual {p2, p3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/util/DrawableLocator;->mDrawable:Landroid/graphics/drawable/Drawable;

    .line 36
    :cond_0
    :goto_0
    return-void

    .line 32
    :cond_1
    invoke-virtual {p2, p3, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/navdy/hud/app/util/DrawableLocator;->mResourceId:I

    .line 33
    iget v0, p0, Lcom/navdy/hud/app/util/DrawableLocator;->mResourceId:I

    if-ne v0, v1, :cond_0

    .line 34
    iput p4, p0, Lcom/navdy/hud/app/util/DrawableLocator;->mResourceId:I

    goto :goto_0
.end method

.method public static drawableToBitmap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;
    .locals 6
    .param p0, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    const/4 v5, 0x0

    .line 57
    instance-of v2, p0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v2, :cond_0

    .line 58
    check-cast p0, Landroid/graphics/drawable/BitmapDrawable;

    .end local p0    # "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 66
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    .local v1, "canvas":Landroid/graphics/Canvas;
    .restart local p0    # "drawable":Landroid/graphics/drawable/Drawable;
    :goto_0
    return-object v0

    .line 61
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "canvas":Landroid/graphics/Canvas;
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 62
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 63
    .restart local v1    # "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v3

    invoke-virtual {p0, v5, v5, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 64
    invoke-virtual {p0, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method


# virtual methods
.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Lcom/navdy/hud/app/util/DrawableLocator;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/navdy/hud/app/util/DrawableLocator;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-static {v0}, Lcom/navdy/hud/app/util/DrawableLocator;->drawableToBitmap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 51
    :goto_0
    return-object v0

    .line 47
    :cond_0
    iget v0, p0, Lcom/navdy/hud/app/util/DrawableLocator;->mResourceId:I

    if-nez v0, :cond_1

    .line 48
    const/4 v0, 0x0

    goto :goto_0

    .line 51
    :cond_1
    iget-object v0, p0, Lcom/navdy/hud/app/util/DrawableLocator;->mParent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/navdy/hud/app/util/DrawableLocator;->mResourceId:I

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public isSet()Z
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/navdy/hud/app/util/DrawableLocator;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    iget v0, p0, Lcom/navdy/hud/app/util/DrawableLocator;->mResourceId:I

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
