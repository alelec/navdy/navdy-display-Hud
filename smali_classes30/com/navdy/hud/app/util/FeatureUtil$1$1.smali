.class Lcom/navdy/hud/app/util/FeatureUtil$1$1;
.super Ljava/lang/Object;
.source "FeatureUtil.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/util/FeatureUtil$1;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/util/FeatureUtil$1;

.field final synthetic val$bundle:Landroid/os/Bundle;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/util/FeatureUtil$1;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/util/FeatureUtil$1;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/navdy/hud/app/util/FeatureUtil$1$1;->this$1:Lcom/navdy/hud/app/util/FeatureUtil$1;

    iput-object p2, p0, Lcom/navdy/hud/app/util/FeatureUtil$1$1;->val$bundle:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 64
    :try_start_0
    iget-object v4, p0, Lcom/navdy/hud/app/util/FeatureUtil$1$1;->val$bundle:Landroid/os/Bundle;

    invoke-virtual {v4}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 65
    .local v1, "featureName":Ljava/lang/String;
    iget-object v4, p0, Lcom/navdy/hud/app/util/FeatureUtil$1$1;->val$bundle:Landroid/os/Bundle;

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 66
    .local v2, "state":Z
    iget-object v4, p0, Lcom/navdy/hud/app/util/FeatureUtil$1$1;->this$1:Lcom/navdy/hud/app/util/FeatureUtil$1;

    iget-object v4, v4, Lcom/navdy/hud/app/util/FeatureUtil$1;->this$0:Lcom/navdy/hud/app/util/FeatureUtil;

    # invokes: Lcom/navdy/hud/app/util/FeatureUtil;->getFeatureFromName(Ljava/lang/String;)Lcom/navdy/hud/app/util/FeatureUtil$Feature;
    invoke-static {v4, v1}, Lcom/navdy/hud/app/util/FeatureUtil;->access$000(Lcom/navdy/hud/app/util/FeatureUtil;Ljava/lang/String;)Lcom/navdy/hud/app/util/FeatureUtil$Feature;

    move-result-object v0

    .line 67
    .local v0, "feature":Lcom/navdy/hud/app/util/FeatureUtil$Feature;
    if-nez v0, :cond_1

    .line 68
    # getter for: Lcom/navdy/hud/app/util/FeatureUtil;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/FeatureUtil;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "invalid feature:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 93
    .end local v0    # "feature":Lcom/navdy/hud/app/util/FeatureUtil$Feature;
    .end local v1    # "featureName":Ljava/lang/String;
    .end local v2    # "state":Z
    :cond_0
    :goto_0
    return-void

    .line 71
    .restart local v0    # "feature":Lcom/navdy/hud/app/util/FeatureUtil$Feature;
    .restart local v1    # "featureName":Ljava/lang/String;
    .restart local v2    # "state":Z
    :cond_1
    sget-object v4, Lcom/navdy/hud/app/util/FeatureUtil$Feature;->GESTURE_ENGINE:Lcom/navdy/hud/app/util/FeatureUtil$Feature;

    if-ne v0, v4, :cond_2

    .line 72
    iget-object v4, p0, Lcom/navdy/hud/app/util/FeatureUtil$1$1;->this$1:Lcom/navdy/hud/app/util/FeatureUtil$1;

    iget-object v4, v4, Lcom/navdy/hud/app/util/FeatureUtil$1;->this$0:Lcom/navdy/hud/app/util/FeatureUtil;

    # getter for: Lcom/navdy/hud/app/util/FeatureUtil;->sharedPreferences:Landroid/content/SharedPreferences;
    invoke-static {v4}, Lcom/navdy/hud/app/util/FeatureUtil;->access$200(Lcom/navdy/hud/app/util/FeatureUtil;)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "gesture.engine"

    invoke-interface {v4, v5, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 73
    # getter for: Lcom/navdy/hud/app/util/FeatureUtil;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/FeatureUtil;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "gesture engine setting set to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 74
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->supportsCamera()Z

    move-result v4

    if-nez v4, :cond_4

    .line 75
    # getter for: Lcom/navdy/hud/app/util/FeatureUtil;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/FeatureUtil;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "gesture engine cannot be enabled/disabled, no camera"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 90
    .end local v0    # "feature":Lcom/navdy/hud/app/util/FeatureUtil$Feature;
    .end local v1    # "featureName":Ljava/lang/String;
    .end local v2    # "state":Z
    :catch_0
    move-exception v3

    .line 91
    .local v3, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/util/FeatureUtil;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/FeatureUtil;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 78
    .end local v3    # "t":Ljava/lang/Throwable;
    .restart local v0    # "feature":Lcom/navdy/hud/app/util/FeatureUtil$Feature;
    .restart local v1    # "featureName":Ljava/lang/String;
    .restart local v2    # "state":Z
    :cond_2
    :try_start_1
    sget-object v4, Lcom/navdy/hud/app/util/FeatureUtil$Feature;->GESTURE_COLLECTOR:Lcom/navdy/hud/app/util/FeatureUtil$Feature;

    if-ne v0, v4, :cond_4

    .line 79
    # getter for: Lcom/navdy/hud/app/util/FeatureUtil;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/FeatureUtil;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "gesture collector setting set to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 80
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->supportsCamera()Z

    move-result v4

    if-nez v4, :cond_3

    .line 81
    # getter for: Lcom/navdy/hud/app/util/FeatureUtil;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/FeatureUtil;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "gesture collector cannot be enabled/disabled, no camera"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 85
    :cond_3
    iget-object v4, p0, Lcom/navdy/hud/app/util/FeatureUtil$1$1;->this$1:Lcom/navdy/hud/app/util/FeatureUtil$1;

    iget-object v4, v4, Lcom/navdy/hud/app/util/FeatureUtil$1;->this$0:Lcom/navdy/hud/app/util/FeatureUtil;

    # invokes: Lcom/navdy/hud/app/util/FeatureUtil;->isGestureCollectorInstalled()Z
    invoke-static {v4}, Lcom/navdy/hud/app/util/FeatureUtil;->access$300(Lcom/navdy/hud/app/util/FeatureUtil;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 89
    :cond_4
    iget-object v4, p0, Lcom/navdy/hud/app/util/FeatureUtil$1$1;->this$1:Lcom/navdy/hud/app/util/FeatureUtil$1;

    iget-object v4, v4, Lcom/navdy/hud/app/util/FeatureUtil$1;->this$0:Lcom/navdy/hud/app/util/FeatureUtil;

    # invokes: Lcom/navdy/hud/app/util/FeatureUtil;->setFeature(Lcom/navdy/hud/app/util/FeatureUtil$Feature;Z)V
    invoke-static {v4, v0, v2}, Lcom/navdy/hud/app/util/FeatureUtil;->access$400(Lcom/navdy/hud/app/util/FeatureUtil;Lcom/navdy/hud/app/util/FeatureUtil$Feature;Z)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
