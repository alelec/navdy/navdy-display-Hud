.class public Lcom/navdy/hud/app/util/CrashReportService;
.super Landroid/app/IntentService;
.source "CrashReportService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/util/CrashReportService$FilesModifiedTimeComparator;,
        Lcom/navdy/hud/app/util/CrashReportService$CrashType;
    }
.end annotation


# static fields
.field private static final ACTION_ADD_REPORT:Ljava/lang/String; = "AddReport"

.field private static final ACTION_DUMP_CRASH_REPORT:Ljava/lang/String; = "DumpCrashReport"

.field private static CRASH_INFO_TEXT_FILE_NAME:Ljava/lang/String; = null

.field private static CRASH_LOG_TEXT_FILE_NAME:Ljava/lang/String; = null

.field private static final DATE_FORMAT:Ljava/text/SimpleDateFormat;

.field public static final EXTRA_CRASH_TYPE:Ljava/lang/String; = "EXTRA_CRASH_TYPE"

.field public static final EXTRA_FILE_PATH:Ljava/lang/String; = "EXTRA_FILE_PATH"

.field private static LOG_FILE_SIZE:I = 0x0

.field private static final MAX_NON_FATAL_CRASH_REPORTS_OUT_STANDING:I = 0xa

.field private static final TIME_FORMAT_FOR_REPORT:Ljava/text/SimpleDateFormat;

.field private static mIsInitialized:Z

.field private static sCrashReportsFolder:Ljava/lang/String;

.field private static sCrashReportsToSend:Ljava/util/concurrent/PriorityBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/PriorityBlockingQueue",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private static sLogger:Lcom/navdy/service/library/log/Logger;

.field private static sOutStandingCrashReportsToBeCleared:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 33
    new-instance v0, Ljava/util/concurrent/PriorityBlockingQueue;

    const/16 v1, 0xa

    new-instance v2, Lcom/navdy/hud/app/util/CrashReportService$FilesModifiedTimeComparator;

    invoke-direct {v2}, Lcom/navdy/hud/app/util/CrashReportService$FilesModifiedTimeComparator;-><init>()V

    invoke-direct {v0, v1, v2}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>(ILjava/util/Comparator;)V

    sput-object v0, Lcom/navdy/hud/app/util/CrashReportService;->sCrashReportsToSend:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 34
    sput v3, Lcom/navdy/hud/app/util/CrashReportService;->sOutStandingCrashReportsToBeCleared:I

    .line 35
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/util/CrashReportService;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/util/CrashReportService;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 41
    const-string v0, "info.txt"

    sput-object v0, Lcom/navdy/hud/app/util/CrashReportService;->CRASH_INFO_TEXT_FILE_NAME:Ljava/lang/String;

    .line 42
    const-string v0, "log.txt"

    sput-object v0, Lcom/navdy/hud/app/util/CrashReportService;->CRASH_LOG_TEXT_FILE_NAME:Ljava/lang/String;

    .line 44
    const v0, 0xc800

    sput v0, Lcom/navdy/hud/app/util/CrashReportService;->LOG_FILE_SIZE:I

    .line 45
    sput-boolean v3, Lcom/navdy/hud/app/util/CrashReportService;->mIsInitialized:Z

    .line 46
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd\'_\'HH:mm:ss.SSS"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/navdy/hud/app/util/CrashReportService;->DATE_FORMAT:Ljava/text/SimpleDateFormat;

    .line 47
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "dd MM yyyy\' \'HH:mm:ss.SSS"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/navdy/hud/app/util/CrashReportService;->TIME_FORMAT_FOR_REPORT:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 65
    const-string v0, "CrashReportService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 66
    return-void
.end method

.method private addReport(Ljava/io/File;)V
    .locals 4
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 109
    sget-object v2, Lcom/navdy/hud/app/util/CrashReportService;->sCrashReportsToSend:Ljava/util/concurrent/PriorityBlockingQueue;

    monitor-enter v2

    .line 110
    :try_start_0
    sget-object v1, Lcom/navdy/hud/app/util/CrashReportService;->sCrashReportsToSend:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 111
    sget-object v1, Lcom/navdy/hud/app/util/CrashReportService;->sCrashReportsToSend:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/PriorityBlockingQueue;->size()I

    move-result v1

    const/16 v3, 0xa

    if-le v1, v3, :cond_1

    .line 112
    sget-object v1, Lcom/navdy/hud/app/util/CrashReportService;->sCrashReportsToSend:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/PriorityBlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 113
    .local v0, "oldestFile":Ljava/io/File;
    if-eqz v0, :cond_0

    .line 114
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 116
    :cond_0
    sget v1, Lcom/navdy/hud/app/util/CrashReportService;->sOutStandingCrashReportsToBeCleared:I

    if-lez v1, :cond_1

    .line 118
    sget v1, Lcom/navdy/hud/app/util/CrashReportService;->sOutStandingCrashReportsToBeCleared:I

    add-int/lit8 v1, v1, -0x1

    sput v1, Lcom/navdy/hud/app/util/CrashReportService;->sOutStandingCrashReportsToBeCleared:I

    .line 121
    .end local v0    # "oldestFile":Ljava/io/File;
    :cond_1
    monitor-exit v2

    .line 122
    return-void

    .line 121
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static addSnapshotAsync(Ljava/lang/String;)V
    .locals 3
    .param p0, "absolutePath"    # Ljava/lang/String;

    .prologue
    .line 243
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/hud/app/util/CrashReportService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 244
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "AddReport"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 245
    const-string v1, "EXTRA_FILE_PATH"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 246
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 247
    return-void
.end method

.method public static clearCrashReports()V
    .locals 4

    .prologue
    .line 231
    sget-object v2, Lcom/navdy/hud/app/util/CrashReportService;->sCrashReportsToSend:Ljava/util/concurrent/PriorityBlockingQueue;

    monitor-enter v2

    .line 232
    .local v0, "oldestFile":Ljava/io/File;
    :cond_0
    :goto_0
    :try_start_0
    sget v1, Lcom/navdy/hud/app/util/CrashReportService;->sOutStandingCrashReportsToBeCleared:I

    if-lez v1, :cond_1

    .line 233
    sget-object v1, Lcom/navdy/hud/app/util/CrashReportService;->sCrashReportsToSend:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/PriorityBlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "oldestFile":Ljava/io/File;
    check-cast v0, Ljava/io/File;

    .line 234
    .restart local v0    # "oldestFile":Ljava/io/File;
    if-eqz v0, :cond_0

    .line 235
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 236
    sget v1, Lcom/navdy/hud/app/util/CrashReportService;->sOutStandingCrashReportsToBeCleared:I

    add-int/lit8 v1, v1, -0x1

    sput v1, Lcom/navdy/hud/app/util/CrashReportService;->sOutStandingCrashReportsToBeCleared:I

    goto :goto_0

    .line 239
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 240
    return-void
.end method

.method public static compressCrashReportsToZip([Ljava/io/File;Ljava/lang/String;)V
    .locals 12
    .param p0, "additionalFiles"    # [Ljava/io/File;
    .param p1, "outputFilePath"    # Ljava/lang/String;

    .prologue
    .line 195
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 196
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 197
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9, p1}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 200
    :cond_0
    :try_start_0
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 204
    :goto_0
    const/4 v8, 0x0

    .line 205
    .local v8, "totalSize":I
    if-eqz p0, :cond_1

    array-length v9, p0

    if-lez v9, :cond_1

    .line 206
    array-length v8, p0

    .line 211
    :cond_1
    sget-object v10, Lcom/navdy/hud/app/util/CrashReportService;->sCrashReportsToSend:Ljava/util/concurrent/PriorityBlockingQueue;

    monitor-enter v10

    .line 212
    :try_start_1
    sget-object v9, Lcom/navdy/hud/app/util/CrashReportService;->sCrashReportsToSend:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v9}, Ljava/util/concurrent/PriorityBlockingQueue;->size()I

    move-result v9

    add-int/2addr v8, v9

    .line 213
    new-array v4, v8, [Ljava/io/File;

    .line 214
    .local v4, "files":[Ljava/io/File;
    const/4 v6, 0x0

    .line 215
    .local v6, "i":I
    if-eqz p0, :cond_3

    .line 216
    array-length v11, p0

    const/4 v9, 0x0

    move v7, v6

    .end local v6    # "i":I
    .local v7, "i":I
    :goto_1
    if-ge v9, v11, :cond_2

    aget-object v0, p0, v9

    .line 217
    .local v0, "additionalFile":Ljava/io/File;
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "i":I
    .restart local v6    # "i":I
    aput-object v0, v4, v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 216
    add-int/lit8 v9, v9, 0x1

    move v7, v6

    .end local v6    # "i":I
    .restart local v7    # "i":I
    goto :goto_1

    .line 201
    .end local v0    # "additionalFile":Ljava/io/File;
    .end local v4    # "files":[Ljava/io/File;
    .end local v7    # "i":I
    .end local v8    # "totalSize":I
    :catch_0
    move-exception v2

    .line 202
    .local v2, "e":Ljava/io/IOException;
    sget-object v9, Lcom/navdy/hud/app/util/CrashReportService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Failed to create new file at the specified output file path "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .end local v2    # "e":Ljava/io/IOException;
    .restart local v4    # "files":[Ljava/io/File;
    .restart local v7    # "i":I
    .restart local v8    # "totalSize":I
    :cond_2
    move v6, v7

    .line 220
    .end local v7    # "i":I
    .restart local v6    # "i":I
    :cond_3
    :try_start_2
    sget-object v9, Lcom/navdy/hud/app/util/CrashReportService;->sCrashReportsToSend:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v9}, Ljava/util/concurrent/PriorityBlockingQueue;->size()I

    move-result v9

    sput v9, Lcom/navdy/hud/app/util/CrashReportService;->sOutStandingCrashReportsToBeCleared:I

    .line 221
    sget-object v9, Lcom/navdy/hud/app/util/CrashReportService;->sCrashReportsToSend:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v9}, Ljava/util/concurrent/PriorityBlockingQueue;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "filesIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/io/File;>;"
    move v7, v6

    .line 222
    .end local v6    # "i":I
    .restart local v7    # "i":I
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 223
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    .line 224
    .local v1, "crashReport":Ljava/io/File;
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "i":I
    .restart local v6    # "i":I
    aput-object v1, v4, v7

    move v7, v6

    .line 225
    .end local v6    # "i":I
    .restart local v7    # "i":I
    goto :goto_2

    .line 226
    .end local v1    # "crashReport":Ljava/io/File;
    :cond_4
    monitor-exit v10
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 227
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9, v4, p1}, Lcom/navdy/service/library/util/IOUtils;->compressFilesToZip(Landroid/content/Context;[Ljava/io/File;Ljava/lang/String;)V

    .line 228
    return-void

    .line 226
    .end local v4    # "files":[Ljava/io/File;
    .end local v5    # "filesIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/io/File;>;"
    .end local v7    # "i":I
    :catchall_0
    move-exception v9

    :try_start_3
    monitor-exit v10
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v9
.end method

.method private dumpCrashReport(Ljava/lang/String;)V
    .locals 24
    .param p1, "crashName"    # Ljava/lang/String;

    .prologue
    .line 134
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 135
    .local v2, "amPmMarker":Ljava/lang/StringBuilder;
    new-instance v3, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    move-wide/from16 v0, v22

    invoke-direct {v3, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 136
    .local v3, "currentTime":Ljava/util/Date;
    sget-object v21, Lcom/navdy/hud/app/util/CrashReportService;->TIME_FORMAT_FOR_REPORT:Ljava/text/SimpleDateFormat;

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v19

    .line 137
    .local v19, "time":Ljava/lang/String;
    new-instance v13, Ljava/io/File;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v22, Lcom/navdy/hud/app/util/CrashReportService;->sCrashReportsFolder:Ljava/lang/String;

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    sget-object v22, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    sget-object v22, Lcom/navdy/hud/app/util/CrashReportService;->CRASH_INFO_TEXT_FILE_NAME:Ljava/lang/String;

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-direct {v13, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 138
    .local v13, "infoTextFile":Ljava/io/File;
    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v21

    if-eqz v21, :cond_0

    .line 139
    invoke-virtual {v13}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 141
    :cond_0
    invoke-virtual {v13}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v17

    .line 142
    .local v17, "snapShotDirectory":Ljava/io/File;
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->exists()Z

    move-result v21

    if-nez v21, :cond_1

    .line 143
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->mkdirs()Z

    .line 146
    :cond_1
    const/4 v8, 0x0

    .line 148
    .local v8, "fileWriter":Ljava/io/FileWriter;
    :try_start_0
    invoke-virtual {v13}, Ljava/io/File;->createNewFile()Z

    .line 149
    new-instance v9, Ljava/io/FileWriter;

    invoke-direct {v9, v13}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 150
    .end local v8    # "fileWriter":Ljava/io/FileWriter;
    .local v9, "fileWriter":Ljava/io/FileWriter;
    :try_start_1
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Crash Type : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "\n"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 151
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Report Time : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " , "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "\n"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 152
    invoke-static {}, Lcom/navdy/hud/app/util/os/PropsFileUpdater;->readProps()Ljava/lang/String;

    move-result-object v16

    .line 153
    .local v16, "props":Ljava/lang/String;
    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 154
    sget-object v21, Lcom/navdy/service/library/device/RemoteDevice;->sLastSeenDeviceInfo:Lcom/navdy/service/library/device/RemoteDevice$LastSeenDeviceInfo;

    invoke-virtual/range {v21 .. v21}, Lcom/navdy/service/library/device/RemoteDevice$LastSeenDeviceInfo;->getDeviceInfo()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v4

    .line 155
    .local v4, "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    if-eqz v4, :cond_2

    .line 156
    invoke-static {v4}, Lcom/navdy/hud/app/util/CrashReportService;->printDeviceInfo(Lcom/navdy/service/library/events/DeviceInfo;)Ljava/lang/String;

    move-result-object v5

    .line 157
    .local v5, "deviceInfoString":Ljava/lang/String;
    invoke-virtual {v9, v5}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 159
    .end local v5    # "deviceInfoString":Ljava/lang/String;
    :cond_2
    invoke-virtual {v9}, Ljava/io/FileWriter;->flush()V

    .line 160
    invoke-virtual {v9}, Ljava/io/FileWriter;->close()V

    .line 162
    new-instance v18, Ljava/io/File;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v22, Lcom/navdy/hud/app/util/CrashReportService;->sCrashReportsFolder:Ljava/lang/String;

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    sget-object v22, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "temp"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 163
    .local v18, "tempDirectory":Ljava/io/File;
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v21

    if-eqz v21, :cond_3

    .line 164
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/navdy/service/library/util/IOUtils;->deleteDirectory(Landroid/content/Context;Ljava/io/File;)V

    .line 166
    :cond_3
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->mkdirs()Z

    .line 167
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/navdy/service/library/util/LogUtils;->copySnapshotSystemLogs(Ljava/lang/String;)V

    .line 168
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v15

    .line 169
    .local v15, "logFiles":[Ljava/io/File;
    if-eqz v15, :cond_4

    array-length v0, v15

    move/from16 v21, v0

    add-int/lit8 v21, v21, 0x1

    :goto_0
    move/from16 v0, v21

    new-array v10, v0, [Ljava/io/File;

    .line 170
    .local v10, "files":[Ljava/io/File;
    const/16 v21, 0x0

    aput-object v13, v10, v21

    .line 171
    const/4 v11, 0x1

    .line 172
    .local v11, "i":I
    if-eqz v15, :cond_6

    .line 173
    array-length v0, v15

    move/from16 v22, v0

    const/16 v21, 0x0

    move v12, v11

    .end local v11    # "i":I
    .local v12, "i":I
    :goto_1
    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_5

    aget-object v14, v15, v21

    .line 174
    .local v14, "logFile":Ljava/io/File;
    add-int/lit8 v11, v12, 0x1

    .end local v12    # "i":I
    .restart local v11    # "i":I
    aput-object v14, v10, v12

    .line 173
    add-int/lit8 v21, v21, 0x1

    move v12, v11

    .end local v11    # "i":I
    .restart local v12    # "i":I
    goto :goto_1

    .line 169
    .end local v10    # "files":[Ljava/io/File;
    .end local v12    # "i":I
    .end local v14    # "logFile":Ljava/io/File;
    :cond_4
    const/16 v21, 0x1

    goto :goto_0

    .restart local v10    # "files":[Ljava/io/File;
    .restart local v12    # "i":I
    :cond_5
    move v11, v12

    .line 177
    .end local v12    # "i":I
    .restart local v11    # "i":I
    :cond_6
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v22, Lcom/navdy/hud/app/util/CrashReportService;->DATE_FORMAT:Ljava/text/SimpleDateFormat;

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "_"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ".zip"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 178
    .local v7, "fileName":Ljava/lang/String;
    new-instance v20, Ljava/io/File;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v22, Lcom/navdy/hud/app/util/CrashReportService;->sCrashReportsFolder:Ljava/lang/String;

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    sget-object v22, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 179
    .local v20, "zipFile":Ljava/io/File;
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v21

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-static {v0, v10, v1}, Lcom/navdy/service/library/util/IOUtils;->compressFilesToZip(Landroid/content/Context;[Ljava/io/File;Ljava/lang/String;)V

    .line 180
    invoke-virtual {v13}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 181
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/navdy/service/library/util/IOUtils;->deleteDirectory(Landroid/content/Context;Ljava/io/File;)V

    .line 182
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/util/CrashReportService;->addReport(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 187
    :try_start_2
    invoke-virtual {v9}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v8, v9

    .line 192
    .end local v4    # "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    .end local v7    # "fileName":Ljava/lang/String;
    .end local v9    # "fileWriter":Ljava/io/FileWriter;
    .end local v10    # "files":[Ljava/io/File;
    .end local v11    # "i":I
    .end local v15    # "logFiles":[Ljava/io/File;
    .end local v16    # "props":Ljava/lang/String;
    .end local v18    # "tempDirectory":Ljava/io/File;
    .end local v20    # "zipFile":Ljava/io/File;
    .restart local v8    # "fileWriter":Ljava/io/FileWriter;
    :goto_2
    return-void

    .line 188
    .end local v8    # "fileWriter":Ljava/io/FileWriter;
    .restart local v4    # "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    .restart local v7    # "fileName":Ljava/lang/String;
    .restart local v9    # "fileWriter":Ljava/io/FileWriter;
    .restart local v10    # "files":[Ljava/io/File;
    .restart local v11    # "i":I
    .restart local v15    # "logFiles":[Ljava/io/File;
    .restart local v16    # "props":Ljava/lang/String;
    .restart local v18    # "tempDirectory":Ljava/io/File;
    .restart local v20    # "zipFile":Ljava/io/File;
    :catch_0
    move-exception v6

    .line 189
    .local v6, "e":Ljava/io/IOException;
    sget-object v21, Lcom/navdy/hud/app/util/CrashReportService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v22, "Error closing the File Writer"

    invoke-virtual/range {v21 .. v22}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    move-object v8, v9

    .line 191
    .end local v9    # "fileWriter":Ljava/io/FileWriter;
    .restart local v8    # "fileWriter":Ljava/io/FileWriter;
    goto :goto_2

    .line 183
    .end local v4    # "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    .end local v6    # "e":Ljava/io/IOException;
    .end local v7    # "fileName":Ljava/lang/String;
    .end local v10    # "files":[Ljava/io/File;
    .end local v11    # "i":I
    .end local v15    # "logFiles":[Ljava/io/File;
    .end local v16    # "props":Ljava/lang/String;
    .end local v18    # "tempDirectory":Ljava/io/File;
    .end local v20    # "zipFile":Ljava/io/File;
    :catch_1
    move-exception v6

    .line 184
    .local v6, "e":Ljava/lang/Throwable;
    :goto_3
    :try_start_3
    sget-object v21, Lcom/navdy/hud/app/util/CrashReportService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v22, "Exception while creating the crash report "

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 187
    :try_start_4
    invoke-virtual {v8}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_2

    .line 188
    :catch_2
    move-exception v6

    .line 189
    .local v6, "e":Ljava/io/IOException;
    sget-object v21, Lcom/navdy/hud/app/util/CrashReportService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v22, "Error closing the File Writer"

    invoke-virtual/range {v21 .. v22}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_2

    .line 186
    .end local v6    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v21

    .line 187
    :goto_4
    :try_start_5
    invoke-virtual {v8}, Ljava/io/FileWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 190
    :goto_5
    throw v21

    .line 188
    :catch_3
    move-exception v6

    .line 189
    .restart local v6    # "e":Ljava/io/IOException;
    sget-object v22, Lcom/navdy/hud/app/util/CrashReportService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v23, "Error closing the File Writer"

    invoke-virtual/range {v22 .. v23}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_5

    .line 186
    .end local v6    # "e":Ljava/io/IOException;
    .end local v8    # "fileWriter":Ljava/io/FileWriter;
    .restart local v9    # "fileWriter":Ljava/io/FileWriter;
    :catchall_1
    move-exception v21

    move-object v8, v9

    .end local v9    # "fileWriter":Ljava/io/FileWriter;
    .restart local v8    # "fileWriter":Ljava/io/FileWriter;
    goto :goto_4

    .line 183
    .end local v8    # "fileWriter":Ljava/io/FileWriter;
    .restart local v9    # "fileWriter":Ljava/io/FileWriter;
    :catch_4
    move-exception v6

    move-object v8, v9

    .end local v9    # "fileWriter":Ljava/io/FileWriter;
    .restart local v8    # "fileWriter":Ljava/io/FileWriter;
    goto :goto_3
.end method

.method public static dumpCrashReportAsync(Lcom/navdy/hud/app/util/CrashReportService$CrashType;)V
    .locals 3
    .param p0, "type"    # Lcom/navdy/hud/app/util/CrashReportService$CrashType;

    .prologue
    .line 250
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/hud/app/util/CrashReportService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 251
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "DumpCrashReport"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 252
    const-string v1, "EXTRA_CRASH_TYPE"

    invoke-virtual {p0}, Lcom/navdy/hud/app/util/CrashReportService$CrashType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 253
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 254
    return-void
.end method

.method public static getCrashTypeForId(I)Lcom/navdy/hud/app/util/CrashReportService$CrashType;
    .locals 2
    .param p0, "id"    # I

    .prologue
    .line 125
    invoke-static {}, Lcom/navdy/hud/app/util/CrashReportService$CrashType;->values()[Lcom/navdy/hud/app/util/CrashReportService$CrashType;

    move-result-object v0

    .line 126
    .local v0, "values":[Lcom/navdy/hud/app/util/CrashReportService$CrashType;
    if-ltz p0, :cond_0

    array-length v1, v0

    if-ge p0, v1, :cond_0

    .line 127
    aget-object v1, v0, p0

    .line 129
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static populateFilesQueue([Ljava/io/File;Ljava/util/concurrent/PriorityBlockingQueue;I)V
    .locals 7
    .param p0, "files"    # [Ljava/io/File;
    .param p2, "maxSize"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/io/File;",
            "Ljava/util/concurrent/PriorityBlockingQueue",
            "<",
            "Ljava/io/File;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 257
    .local p1, "filesQueue":Ljava/util/concurrent/PriorityBlockingQueue;, "Ljava/util/concurrent/PriorityBlockingQueue<Ljava/io/File;>;"
    if-eqz p0, :cond_1

    .line 258
    array-length v3, p0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v0, p0, v2

    .line 259
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 260
    sget-object v4, Lcom/navdy/hud/app/util/CrashReportService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "File "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 261
    invoke-virtual {p1, v0}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 262
    invoke-virtual {p1}, Ljava/util/concurrent/PriorityBlockingQueue;->size()I

    move-result v4

    if-ne v4, p2, :cond_0

    .line 263
    invoke-virtual {p1}, Ljava/util/concurrent/PriorityBlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    .line 264
    .local v1, "oldestFile":Ljava/io/File;
    sget-object v4, Lcom/navdy/hud/app/util/CrashReportService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Deleting the old file "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 265
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 258
    .end local v1    # "oldestFile":Ljava/io/File;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 270
    .end local v0    # "file":Ljava/io/File;
    :cond_1
    return-void
.end method

.method public static printDeviceInfo(Lcom/navdy/service/library/events/DeviceInfo;)Ljava/lang/String;
    .locals 3
    .param p0, "deviceInfo"    # Lcom/navdy/service/library/events/DeviceInfo;

    .prologue
    .line 273
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 274
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v1, "\nDevice Information\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 275
    const-string v1, "-----------------------\n\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 276
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Platform :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/service/library/events/DeviceInfo;->platform:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/DeviceInfo$Platform;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 277
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Name :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/service/library/events/DeviceInfo;->deviceName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 278
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "System Version :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/service/library/events/DeviceInfo;->systemVersion:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 279
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Model :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/service/library/events/DeviceInfo;->model:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 280
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Device ID :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/service/library/events/DeviceInfo;->deviceId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 281
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Make :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/service/library/events/DeviceInfo;->deviceMake:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 282
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Build Type :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/service/library/events/DeviceInfo;->buildType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 283
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "API level :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/service/library/events/DeviceInfo;->systemApiLevel:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 284
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Protocol version :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/service/library/events/DeviceInfo;->protocolVersion:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 285
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Force Full Update Flag :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/service/library/events/DeviceInfo;->forceFullUpdate:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 286
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public onCreate()V
    .locals 4

    .prologue
    .line 70
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 71
    sget-boolean v1, Lcom/navdy/hud/app/util/CrashReportService;->mIsInitialized:Z

    if-nez v1, :cond_1

    .line 72
    invoke-static {}, Lcom/navdy/hud/app/storage/PathManager;->getInstance()Lcom/navdy/hud/app/storage/PathManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/storage/PathManager;->getNonFatalCrashReportDir()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/hud/app/util/CrashReportService;->sCrashReportsFolder:Ljava/lang/String;

    .line 73
    new-instance v1, Ljava/io/File;

    sget-object v2, Lcom/navdy/hud/app/util/CrashReportService;->sCrashReportsFolder:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 74
    .local v0, "files":[Ljava/io/File;
    sget-object v2, Lcom/navdy/hud/app/util/CrashReportService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Number of Fatal crash reports :"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz v0, :cond_2

    array-length v1, v0

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 75
    if-eqz v0, :cond_0

    .line 76
    sget-object v1, Lcom/navdy/hud/app/util/CrashReportService;->sCrashReportsToSend:Ljava/util/concurrent/PriorityBlockingQueue;

    const/16 v2, 0xa

    invoke-static {v0, v1, v2}, Lcom/navdy/hud/app/util/CrashReportService;->populateFilesQueue([Ljava/io/File;Ljava/util/concurrent/PriorityBlockingQueue;I)V

    .line 78
    :cond_0
    const/4 v1, 0x1

    sput-boolean v1, Lcom/navdy/hud/app/util/CrashReportService;->mIsInitialized:Z

    .line 80
    .end local v0    # "files":[Ljava/io/File;
    :cond_1
    return-void

    .line 74
    .restart local v0    # "files":[Ljava/io/File;
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 85
    :try_start_0
    sget-object v4, Lcom/navdy/hud/app/util/CrashReportService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onHandleIntent "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 86
    const-string v4, "DumpCrashReport"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 87
    const-string v4, "EXTRA_CRASH_TYPE"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 88
    .local v1, "crashName":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 89
    sget-object v4, Lcom/navdy/hud/app/util/CrashReportService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Dumping a crash "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 90
    invoke-direct {p0, v1}, Lcom/navdy/hud/app/util/CrashReportService;->dumpCrashReport(Ljava/lang/String;)V

    .line 106
    .end local v1    # "crashName":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 92
    .restart local v1    # "crashName":Ljava/lang/String;
    :cond_1
    sget-object v4, Lcom/navdy/hud/app/util/CrashReportService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Missing crash type"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 103
    .end local v1    # "crashName":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 104
    .local v3, "t":Ljava/lang/Throwable;
    sget-object v4, Lcom/navdy/hud/app/util/CrashReportService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Exception while handling intent "

    invoke-virtual {v4, v5, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 94
    .end local v3    # "t":Ljava/lang/Throwable;
    :cond_2
    :try_start_1
    const-string v4, "AddReport"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 95
    const-string v4, "EXTRA_FILE_PATH"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 96
    .local v0, "absolutePath":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 97
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 98
    .local v2, "reportFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->isFile()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 99
    invoke-direct {p0, v2}, Lcom/navdy/hud/app/util/CrashReportService;->addReport(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
