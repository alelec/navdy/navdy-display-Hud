.class public Lcom/navdy/hud/app/util/DateUtil;
.super Ljava/lang/Object;
.source "DateUtil.java"


# static fields
.field private static final HOUR_HAND_ANGLE_PER_HOUR:I = 0x1e

.field private static final MINUTE_HAND_ANGLE_PER_MINUTE:I = 0x6

.field private static dateLabelFormat:Ljava/text/SimpleDateFormat;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 18
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/util/DateUtil;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/util/DateUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 23
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "d MMMM"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/navdy/hud/app/util/DateUtil;->dateLabelFormat:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getClockAngleForHour(II)F
    .locals 3
    .param p0, "hour"    # I
    .param p1, "minutes"    # I

    .prologue
    .line 51
    rem-int/lit8 v1, p0, 0xc

    add-int/lit8 v1, v1, -0x3

    mul-int/lit8 v1, v1, 0x1e

    add-int/lit16 v1, v1, 0x168

    rem-int/lit16 v1, v1, 0x168

    int-to-float v0, v1

    .line 52
    .local v0, "hourAngle":F
    int-to-float v1, p1

    const/high16 v2, 0x42700000    # 60.0f

    div-float/2addr v1, v2

    const/high16 v2, 0x41f00000    # 30.0f

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    .line 53
    return v0
.end method

.method public static getClockAngleForMinutes(I)F
    .locals 1
    .param p0, "minutes"    # I

    .prologue
    .line 57
    add-int/lit8 v0, p0, -0xf

    mul-int/lit8 v0, v0, 0x6

    add-int/lit16 v0, v0, 0x168

    rem-int/lit16 v0, v0, 0x168

    int-to-float v0, v0

    return v0
.end method

.method public static getDateLabel(Ljava/util/Date;)Ljava/lang/String;
    .locals 11
    .param p0, "d"    # Ljava/util/Date;

    .prologue
    const/4 v10, 0x1

    .line 62
    :try_start_0
    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    invoke-static {v8, v9}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 64
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f090293

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 90
    :goto_0
    return-object v8

    .line 67
    :cond_0
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    .line 68
    .local v3, "now":Ljava/util/Date;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    .line 69
    .local v4, "nowCal":Ljava/util/Calendar;
    invoke-virtual {v4, v3}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 70
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 71
    .local v0, "dCal":Ljava/util/Calendar;
    invoke-virtual {v0, p0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 73
    const/4 v8, 0x1

    invoke-virtual {v4, v8}, Ljava/util/Calendar;->get(I)I

    move-result v6

    .line 74
    .local v6, "year1":I
    const/4 v8, 0x1

    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v7

    .line 75
    .local v7, "year2":I
    const/4 v8, 0x6

    invoke-virtual {v4, v8}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 76
    .local v1, "dayOfYear1":I
    const/4 v8, 0x6

    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 78
    .local v2, "dayOfYear2":I
    if-ne v6, v7, :cond_1

    sub-int v8, v1, v2

    if-ne v8, v10, :cond_1

    .line 80
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f09031f

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    goto :goto_0

    .line 83
    :cond_1
    sget-object v9, Lcom/navdy/hud/app/util/DateUtil;->dateLabelFormat:Ljava/text/SimpleDateFormat;

    monitor-enter v9
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    :try_start_1
    sget-object v8, Lcom/navdy/hud/app/util/DateUtil;->dateLabelFormat:Ljava/text/SimpleDateFormat;

    invoke-virtual {v8, p0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    monitor-exit v9

    goto :goto_0

    .line 85
    :catchall_0
    move-exception v8

    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v8
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 88
    .end local v0    # "dCal":Ljava/util/Calendar;
    .end local v1    # "dayOfYear1":I
    .end local v2    # "dayOfYear2":I
    .end local v3    # "now":Ljava/util/Date;
    .end local v4    # "nowCal":Ljava/util/Calendar;
    .end local v6    # "year1":I
    .end local v7    # "year2":I
    :catch_0
    move-exception v5

    .line 89
    .local v5, "t":Ljava/lang/Throwable;
    sget-object v8, Lcom/navdy/hud/app/util/DateUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v8, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 90
    const/4 v8, 0x0

    goto :goto_0
.end method

.method public static parseIrmcDateStr(Ljava/lang/String;)Ljava/util/Date;
    .locals 6
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 29
    if-nez p0, :cond_1

    .line 46
    :cond_0
    :goto_0
    return-object v2

    .line 32
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0xf

    if-ne v3, v4, :cond_0

    .line 35
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 36
    .local v0, "calendar":Ljava/util/Calendar;
    const-wide/16 v4, 0x0

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 37
    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x4

    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->set(II)V

    .line 38
    const/4 v3, 0x2

    const/4 v4, 0x4

    const/4 v5, 0x6

    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->set(II)V

    .line 39
    const/4 v3, 0x5

    const/4 v4, 0x6

    const/16 v5, 0x8

    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->set(II)V

    .line 40
    const/16 v3, 0xb

    const/16 v4, 0x9

    const/16 v5, 0xb

    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->set(II)V

    .line 41
    const/16 v3, 0xc

    const/16 v4, 0xb

    const/16 v5, 0xd

    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->set(II)V

    .line 42
    const/16 v3, 0xd

    const/16 v4, 0xd

    const/16 v5, 0xf

    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->set(II)V

    .line 43
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 44
    .end local v0    # "calendar":Ljava/util/Calendar;
    :catch_0
    move-exception v1

    .line 45
    .local v1, "t":Ljava/lang/Throwable;
    sget-object v3, Lcom/navdy/hud/app/util/DateUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v3, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
