.class public Lcom/navdy/hud/app/util/OTAUpdateService$OTADownloadIntentsReceiver;
.super Landroid/content/BroadcastReceiver;
.source "OTAUpdateService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/util/OTAUpdateService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "OTADownloadIntentsReceiver"
.end annotation


# instance fields
.field private sharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/SharedPreferences;)V
    .locals 0
    .param p1, "prefs"    # Landroid/content/SharedPreferences;

    .prologue
    .line 679
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 680
    iput-object p1, p0, Lcom/navdy/hud/app/util/OTAUpdateService$OTADownloadIntentsReceiver;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 681
    return-void
.end method

.method static synthetic access$800(Lcom/navdy/hud/app/util/OTAUpdateService$OTADownloadIntentsReceiver;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/util/OTAUpdateService$OTADownloadIntentsReceiver;

    .prologue
    .line 676
    iget-object v0, p0, Lcom/navdy/hud/app/util/OTAUpdateService$OTADownloadIntentsReceiver;->sharedPreferences:Landroid/content/SharedPreferences;

    return-object v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 684
    const-string v1, "path"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 685
    .local v0, "path":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v1

    new-instance v2, Lcom/navdy/hud/app/util/OTAUpdateService$OTADownloadIntentsReceiver$1;

    invoke-direct {v2, p0, v0, p1}, Lcom/navdy/hud/app/util/OTAUpdateService$OTADownloadIntentsReceiver$1;-><init>(Lcom/navdy/hud/app/util/OTAUpdateService$OTADownloadIntentsReceiver;Ljava/lang/String;Landroid/content/Context;)V

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 714
    return-void
.end method
