.class public Lcom/navdy/hud/app/util/ImageUtil;
.super Ljava/lang/Object;
.source "ImageUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static applySaturation(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;
    .locals 9
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "saturation"    # F

    .prologue
    const/4 v8, 0x0

    .line 60
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 62
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v7

    invoke-static {v5, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 63
    .local v3, "newBitmap":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 64
    .local v0, "canvasResult":Landroid/graphics/Canvas;
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    .line 65
    .local v4, "paint":Landroid/graphics/Paint;
    new-instance v1, Landroid/graphics/ColorMatrix;

    invoke-direct {v1}, Landroid/graphics/ColorMatrix;-><init>()V

    .line 66
    .local v1, "colorMatrix":Landroid/graphics/ColorMatrix;
    invoke-virtual {v1, p1}, Landroid/graphics/ColorMatrix;->setSaturation(F)V

    .line 67
    new-instance v2, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v2, v1}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    .line 68
    .local v2, "filter":Landroid/graphics/ColorMatrixColorFilter;
    invoke-virtual {v4, v2}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 69
    invoke-virtual {v0, p0, v8, v8, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 71
    return-object v3
.end method

.method public static blend(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 6
    .param p0, "base"    # Landroid/graphics/Bitmap;
    .param p1, "blend"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v5, 0x0

    .line 75
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->checkNotOnMainThread()V

    .line 77
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {p0, v3, v4}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 78
    .local v1, "newBitmap":Landroid/graphics/Bitmap;
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 79
    .local v2, "p":Landroid/graphics/Paint;
    new-instance v3, Landroid/graphics/PorterDuffXfermode;

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v3, v4}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 80
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 81
    .local v0, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v0, p1, v5, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 82
    return-object v1
.end method

.method public static hueShift(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;
    .locals 17
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "hue"    # F

    .prologue
    .line 29
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v14

    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v15

    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v16

    invoke-static/range {v14 .. v16}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 30
    .local v10, "newBitmap":Landroid/graphics/Bitmap;
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v10}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 31
    .local v2, "canvasResult":Landroid/graphics/Canvas;
    new-instance v11, Landroid/graphics/Paint;

    invoke-direct {v11}, Landroid/graphics/Paint;-><init>()V

    .line 32
    .local v11, "paint":Landroid/graphics/Paint;
    new-instance v3, Landroid/graphics/ColorMatrix;

    invoke-direct {v3}, Landroid/graphics/ColorMatrix;-><init>()V

    .line 34
    .local v3, "colorMatrix":Landroid/graphics/ColorMatrix;
    move/from16 v0, p1

    neg-float v14, v0

    const/high16 v15, 0x43340000    # 180.0f

    invoke-static {v14, v15}, Ljava/lang/Math;->max(FF)F

    move-result v14

    move/from16 v0, p1

    invoke-static {v0, v14}, Ljava/lang/Math;->min(FF)F

    move-result v14

    const/high16 v15, 0x43340000    # 180.0f

    div-float/2addr v14, v15

    const v15, 0x40490fdb    # (float)Math.PI

    mul-float v13, v14, v15

    .line 35
    .local v13, "value":F
    const/4 v14, 0x0

    cmpl-float v14, v13, v14

    if-nez v14, :cond_0

    .line 36
    const/4 v10, 0x0

    .line 56
    .end local v10    # "newBitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-object v10

    .line 39
    .restart local v10    # "newBitmap":Landroid/graphics/Bitmap;
    :cond_0
    float-to-double v14, v13

    invoke-static {v14, v15}, Ljava/lang/Math;->cos(D)D

    move-result-wide v14

    double-to-float v4, v14

    .line 40
    .local v4, "cosVal":F
    float-to-double v14, v13

    invoke-static {v14, v15}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    double-to-float v12, v14

    .line 41
    .local v12, "sinVal":F
    const v8, 0x3e5a1cac    # 0.213f

    .line 42
    .local v8, "lumR":F
    const v7, 0x3f370a3d    # 0.715f

    .line 43
    .local v7, "lumG":F
    const v6, 0x3d9374bc    # 0.072f

    .line 44
    .local v6, "lumB":F
    const/16 v14, 0x19

    new-array v9, v14, [F

    const/4 v14, 0x0

    const/high16 v15, 0x3f800000    # 1.0f

    sub-float/2addr v15, v8

    mul-float/2addr v15, v4

    add-float/2addr v15, v8

    neg-float v0, v8

    move/from16 v16, v0

    mul-float v16, v16, v12

    add-float v15, v15, v16

    aput v15, v9, v14

    const/4 v14, 0x1

    neg-float v15, v7

    mul-float/2addr v15, v4

    add-float/2addr v15, v7

    neg-float v0, v7

    move/from16 v16, v0

    mul-float v16, v16, v12

    add-float v15, v15, v16

    aput v15, v9, v14

    const/4 v14, 0x2

    neg-float v15, v6

    mul-float/2addr v15, v4

    add-float/2addr v15, v6

    const/high16 v16, 0x3f800000    # 1.0f

    sub-float v16, v16, v6

    mul-float v16, v16, v12

    add-float v15, v15, v16

    aput v15, v9, v14

    const/4 v14, 0x3

    const/4 v15, 0x0

    aput v15, v9, v14

    const/4 v14, 0x4

    const/4 v15, 0x0

    aput v15, v9, v14

    const/4 v14, 0x5

    neg-float v15, v8

    mul-float/2addr v15, v4

    add-float/2addr v15, v8

    const v16, 0x3e126e98    # 0.143f

    mul-float v16, v16, v12

    add-float v15, v15, v16

    aput v15, v9, v14

    const/4 v14, 0x6

    const/high16 v15, 0x3f800000    # 1.0f

    sub-float/2addr v15, v7

    mul-float/2addr v15, v4

    add-float/2addr v15, v7

    const v16, 0x3e0f5c29    # 0.14f

    mul-float v16, v16, v12

    add-float v15, v15, v16

    aput v15, v9, v14

    const/4 v14, 0x7

    neg-float v15, v6

    mul-float/2addr v15, v4

    add-float/2addr v15, v6

    const v16, -0x416f1aa0    # -0.283f

    mul-float v16, v16, v12

    add-float v15, v15, v16

    aput v15, v9, v14

    const/16 v14, 0x8

    const/4 v15, 0x0

    aput v15, v9, v14

    const/16 v14, 0x9

    const/4 v15, 0x0

    aput v15, v9, v14

    const/16 v14, 0xa

    neg-float v15, v8

    mul-float/2addr v15, v4

    add-float/2addr v15, v8

    const/high16 v16, 0x3f800000    # 1.0f

    sub-float v16, v16, v8

    move/from16 v0, v16

    neg-float v0, v0

    move/from16 v16, v0

    mul-float v16, v16, v12

    add-float v15, v15, v16

    aput v15, v9, v14

    const/16 v14, 0xb

    neg-float v15, v7

    mul-float/2addr v15, v4

    add-float/2addr v15, v7

    mul-float v16, v12, v7

    add-float v15, v15, v16

    aput v15, v9, v14

    const/16 v14, 0xc

    const/high16 v15, 0x3f800000    # 1.0f

    sub-float/2addr v15, v6

    mul-float/2addr v15, v4

    add-float/2addr v15, v6

    mul-float v16, v12, v6

    add-float v15, v15, v16

    aput v15, v9, v14

    const/16 v14, 0xd

    const/4 v15, 0x0

    aput v15, v9, v14

    const/16 v14, 0xe

    const/4 v15, 0x0

    aput v15, v9, v14

    const/16 v14, 0xf

    const/4 v15, 0x0

    aput v15, v9, v14

    const/16 v14, 0x10

    const/4 v15, 0x0

    aput v15, v9, v14

    const/16 v14, 0x11

    const/4 v15, 0x0

    aput v15, v9, v14

    const/16 v14, 0x12

    const/high16 v15, 0x3f800000    # 1.0f

    aput v15, v9, v14

    const/16 v14, 0x13

    const/4 v15, 0x0

    aput v15, v9, v14

    const/16 v14, 0x14

    const/4 v15, 0x0

    aput v15, v9, v14

    const/16 v14, 0x15

    const/4 v15, 0x0

    aput v15, v9, v14

    const/16 v14, 0x16

    const/4 v15, 0x0

    aput v15, v9, v14

    const/16 v14, 0x17

    const/4 v15, 0x0

    aput v15, v9, v14

    const/16 v14, 0x18

    const/high16 v15, 0x3f800000    # 1.0f

    aput v15, v9, v14

    .line 51
    .local v9, "mat":[F
    new-instance v14, Landroid/graphics/ColorMatrix;

    invoke-direct {v14, v9}, Landroid/graphics/ColorMatrix;-><init>([F)V

    invoke-virtual {v3, v14}, Landroid/graphics/ColorMatrix;->postConcat(Landroid/graphics/ColorMatrix;)V

    .line 52
    new-instance v5, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v5, v3}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    .line 53
    .local v5, "filter":Landroid/graphics/ColorMatrixColorFilter;
    invoke-virtual {v11, v5}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 54
    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v2, v0, v14, v15, v11}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0
.end method
