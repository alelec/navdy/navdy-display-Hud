.class public final enum Lcom/navdy/hud/app/util/FeatureUtil$Feature;
.super Ljava/lang/Enum;
.source "FeatureUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/util/FeatureUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Feature"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/util/FeatureUtil$Feature;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/util/FeatureUtil$Feature;

.field public static final enum FUEL_ROUTING:Lcom/navdy/hud/app/util/FeatureUtil$Feature;

.field public static final enum GESTURE_COLLECTOR:Lcom/navdy/hud/app/util/FeatureUtil$Feature;

.field public static final enum GESTURE_ENGINE:Lcom/navdy/hud/app/util/FeatureUtil$Feature;

.field public static final enum GESTURE_PROGRESS:Lcom/navdy/hud/app/util/FeatureUtil$Feature;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 30
    new-instance v0, Lcom/navdy/hud/app/util/FeatureUtil$Feature;

    const-string v1, "GESTURE_ENGINE"

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/util/FeatureUtil$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/util/FeatureUtil$Feature;->GESTURE_ENGINE:Lcom/navdy/hud/app/util/FeatureUtil$Feature;

    .line 31
    new-instance v0, Lcom/navdy/hud/app/util/FeatureUtil$Feature;

    const-string v1, "GESTURE_COLLECTOR"

    invoke-direct {v0, v1, v3}, Lcom/navdy/hud/app/util/FeatureUtil$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/util/FeatureUtil$Feature;->GESTURE_COLLECTOR:Lcom/navdy/hud/app/util/FeatureUtil$Feature;

    .line 32
    new-instance v0, Lcom/navdy/hud/app/util/FeatureUtil$Feature;

    const-string v1, "FUEL_ROUTING"

    invoke-direct {v0, v1, v4}, Lcom/navdy/hud/app/util/FeatureUtil$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/util/FeatureUtil$Feature;->FUEL_ROUTING:Lcom/navdy/hud/app/util/FeatureUtil$Feature;

    .line 33
    new-instance v0, Lcom/navdy/hud/app/util/FeatureUtil$Feature;

    const-string v1, "GESTURE_PROGRESS"

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/app/util/FeatureUtil$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/hud/app/util/FeatureUtil$Feature;->GESTURE_PROGRESS:Lcom/navdy/hud/app/util/FeatureUtil$Feature;

    .line 29
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/navdy/hud/app/util/FeatureUtil$Feature;

    sget-object v1, Lcom/navdy/hud/app/util/FeatureUtil$Feature;->GESTURE_ENGINE:Lcom/navdy/hud/app/util/FeatureUtil$Feature;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/util/FeatureUtil$Feature;->GESTURE_COLLECTOR:Lcom/navdy/hud/app/util/FeatureUtil$Feature;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/hud/app/util/FeatureUtil$Feature;->FUEL_ROUTING:Lcom/navdy/hud/app/util/FeatureUtil$Feature;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/hud/app/util/FeatureUtil$Feature;->GESTURE_PROGRESS:Lcom/navdy/hud/app/util/FeatureUtil$Feature;

    aput-object v1, v0, v5

    sput-object v0, Lcom/navdy/hud/app/util/FeatureUtil$Feature;->$VALUES:[Lcom/navdy/hud/app/util/FeatureUtil$Feature;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/util/FeatureUtil$Feature;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 29
    const-class v0, Lcom/navdy/hud/app/util/FeatureUtil$Feature;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/util/FeatureUtil$Feature;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/util/FeatureUtil$Feature;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/navdy/hud/app/util/FeatureUtil$Feature;->$VALUES:[Lcom/navdy/hud/app/util/FeatureUtil$Feature;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/util/FeatureUtil$Feature;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/util/FeatureUtil$Feature;

    return-object v0
.end method
