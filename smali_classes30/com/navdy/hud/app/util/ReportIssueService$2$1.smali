.class Lcom/navdy/hud/app/util/ReportIssueService$2$1;
.super Ljava/lang/Object;
.source "ReportIssueService.java"

# interfaces
.implements Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/util/ReportIssueService$2;->onSuccess(Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/hud/app/util/ReportIssueService$2;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/util/ReportIssueService$2;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/hud/app/util/ReportIssueService$2;

    .prologue
    .line 553
    iput-object p1, p0, Lcom/navdy/hud/app/util/ReportIssueService$2$1;->this$1:Lcom/navdy/hud/app/util/ReportIssueService$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 574
    iget-object v0, p0, Lcom/navdy/hud/app/util/ReportIssueService$2$1;->this$1:Lcom/navdy/hud/app/util/ReportIssueService$2;

    iget-object v0, v0, Lcom/navdy/hud/app/util/ReportIssueService$2;->this$0:Lcom/navdy/hud/app/util/ReportIssueService;

    # invokes: Lcom/navdy/hud/app/util/ReportIssueService;->syncLater()V
    invoke-static {v0}, Lcom/navdy/hud/app/util/ReportIssueService;->access$200(Lcom/navdy/hud/app/util/ReportIssueService;)V

    .line 575
    return-void
.end method

.method public onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 556
    # getter for: Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/ReportIssueService;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Files successfully attached, Assigning the ticket ot "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/util/ReportIssueService$2$1;->this$1:Lcom/navdy/hud/app/util/ReportIssueService$2;

    iget-object v2, v2, Lcom/navdy/hud/app/util/ReportIssueService$2;->val$assigneeName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Email : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/util/ReportIssueService$2$1;->this$1:Lcom/navdy/hud/app/util/ReportIssueService$2;

    iget-object v2, v2, Lcom/navdy/hud/app/util/ReportIssueService$2;->val$assigneeEmail:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 557
    iget-object v0, p0, Lcom/navdy/hud/app/util/ReportIssueService$2$1;->this$1:Lcom/navdy/hud/app/util/ReportIssueService$2;

    iget-object v0, v0, Lcom/navdy/hud/app/util/ReportIssueService$2;->this$0:Lcom/navdy/hud/app/util/ReportIssueService;

    iget-object v0, v0, Lcom/navdy/hud/app/util/ReportIssueService;->mJiraClient:Lcom/navdy/service/library/network/http/services/JiraClient;

    check-cast p1, Ljava/lang/String;

    .end local p1    # "object":Ljava/lang/Object;
    iget-object v1, p0, Lcom/navdy/hud/app/util/ReportIssueService$2$1;->this$1:Lcom/navdy/hud/app/util/ReportIssueService$2;

    iget-object v1, v1, Lcom/navdy/hud/app/util/ReportIssueService$2;->val$assigneeName:Ljava/lang/String;

    iget-object v2, p0, Lcom/navdy/hud/app/util/ReportIssueService$2$1;->this$1:Lcom/navdy/hud/app/util/ReportIssueService$2;

    iget-object v2, v2, Lcom/navdy/hud/app/util/ReportIssueService$2;->val$assigneeEmail:Ljava/lang/String;

    new-instance v3, Lcom/navdy/hud/app/util/ReportIssueService$2$1$1;

    invoke-direct {v3, p0}, Lcom/navdy/hud/app/util/ReportIssueService$2$1$1;-><init>(Lcom/navdy/hud/app/util/ReportIssueService$2$1;)V

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/navdy/service/library/network/http/services/JiraClient;->assignTicketForName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;)V

    .line 570
    return-void
.end method
