.class public Lcom/navdy/hud/app/util/picasso/PicassoLruCache;
.super Lcom/squareup/picasso/LruCache;
.source "PicassoLruCache.java"

# interfaces
.implements Lcom/navdy/hud/app/util/picasso/PicassoItemCacheListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/picasso/LruCache;",
        "Lcom/navdy/hud/app/util/picasso/PicassoItemCacheListener",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private keyMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "maxSize"    # I

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/squareup/picasso/LruCache;-><init>(I)V

    .line 15
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/util/picasso/PicassoLruCache;->keyMap:Ljava/util/HashMap;

    .line 19
    return-void
.end method

.method private getKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 46
    if-nez p1, :cond_1

    .line 47
    const/4 p1, 0x0

    .line 53
    .end local p1    # "key":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p1

    .line 49
    .restart local p1    # "key":Ljava/lang/String;
    :cond_1
    const-string v1, "\n"

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 50
    .local v0, "index":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 51
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 58
    monitor-enter p0

    if-nez p1, :cond_1

    .line 65
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v1

    .line 61
    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/util/picasso/PicassoLruCache;->keyMap:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 62
    .local v0, "mappedKey":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 65
    invoke-super {p0, v0}, Lcom/squareup/picasso/LruCache;->get(Ljava/lang/String;)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    goto :goto_0

    .line 58
    .end local v0    # "mappedKey":Ljava/lang/String;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public init()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 22
    new-instance v1, Lcom/navdy/hud/app/util/picasso/PicassoCacheMap;

    const/4 v2, 0x0

    const/high16 v3, 0x3f400000    # 0.75f

    invoke-direct {v1, v2, v3}, Lcom/navdy/hud/app/util/picasso/PicassoCacheMap;-><init>(IF)V

    .line 23
    .local v1, "map":Lcom/navdy/hud/app/util/picasso/PicassoCacheMap;, "Lcom/navdy/hud/app/util/picasso/PicassoCacheMap<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    invoke-virtual {v1, p0}, Lcom/navdy/hud/app/util/picasso/PicassoCacheMap;->setListener(Lcom/navdy/hud/app/util/picasso/PicassoItemCacheListener;)V

    .line 24
    const-class v2, Lcom/squareup/picasso/LruCache;

    const-string v3, "map"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 25
    .local v0, "f":Ljava/lang/reflect/Field;
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 26
    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 27
    return-void
.end method

.method public bridge synthetic itemAdded(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 13
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/util/picasso/PicassoLruCache;->itemAdded(Ljava/lang/String;)V

    return-void
.end method

.method public declared-synchronized itemAdded(Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 31
    monitor-enter p0

    if-nez p1, :cond_0

    .line 35
    :goto_0
    monitor-exit p0

    return-void

    .line 34
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/util/picasso/PicassoLruCache;->keyMap:Ljava/util/HashMap;

    invoke-direct {p0, p1}, Lcom/navdy/hud/app/util/picasso/PicassoLruCache;->getKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 31
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public bridge synthetic itemRemoved(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 13
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/navdy/hud/app/util/picasso/PicassoLruCache;->itemRemoved(Ljava/lang/String;)V

    return-void
.end method

.method public declared-synchronized itemRemoved(Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 39
    monitor-enter p0

    if-nez p1, :cond_0

    .line 43
    :goto_0
    monitor-exit p0

    return-void

    .line 42
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/navdy/hud/app/util/picasso/PicassoLruCache;->keyMap:Ljava/util/HashMap;

    invoke-direct {p0, p1}, Lcom/navdy/hud/app/util/picasso/PicassoLruCache;->getKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 39
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 69
    monitor-enter p0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 75
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 72
    :cond_1
    :try_start_0
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/util/picasso/PicassoLruCache;->getKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 73
    .local v0, "normalizedKey":Ljava/lang/String;
    iget-object v1, p0, Lcom/navdy/hud/app/util/picasso/PicassoLruCache;->keyMap:Ljava/util/HashMap;

    invoke-virtual {v1, v0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    invoke-super {p0, p1, p2}, Lcom/squareup/picasso/LruCache;->set(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 69
    .end local v0    # "normalizedKey":Ljava/lang/String;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
