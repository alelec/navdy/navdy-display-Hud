.class public Lcom/navdy/hud/app/util/picasso/PicassoUtil;
.super Ljava/lang/Object;
.source "PicassoUtil.java"


# static fields
.field private static final DISK_CACHE_SCHEME:Ljava/lang/String; = "diskcache://"

.field private static final DISK_CACHE_SCHEME_NAME:Ljava/lang/String; = "diskcache"

.field private static final DISK_CACHE_SIZE:I = 0xa00000

.field public static final IMAGE_MEMORY_CACHE:I = 0x800000

.field private static diskLruCache:Lcom/navdy/hud/app/storage/cache/DiskLruCache;

.field private static volatile initialized:Z

.field private static lockObj:Ljava/lang/Object;

.field private static lruCache:Lcom/navdy/hud/app/util/picasso/PicassoLruCache;

.field private static picasso:Lcom/squareup/picasso/Picasso;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 29
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/util/picasso/PicassoUtil;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 48
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->lockObj:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100()Lcom/navdy/hud/app/storage/cache/DiskLruCache;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->diskLruCache:Lcom/navdy/hud/app/storage/cache/DiskLruCache;

    return-object v0
.end method

.method public static clearCache()V
    .locals 1

    .prologue
    .line 108
    sget-object v0, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->lruCache:Lcom/navdy/hud/app/util/picasso/PicassoLruCache;

    if-eqz v0, :cond_0

    .line 109
    sget-object v0, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->lruCache:Lcom/navdy/hud/app/util/picasso/PicassoLruCache;

    invoke-virtual {v0}, Lcom/navdy/hud/app/util/picasso/PicassoLruCache;->clear()V

    .line 111
    :cond_0
    return-void
.end method

.method public static getBitmapfromCache(Ljava/io/File;)Landroid/graphics/Bitmap;
    .locals 3
    .param p0, "file"    # Ljava/io/File;

    .prologue
    .line 114
    sget-object v0, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->lruCache:Lcom/navdy/hud/app/util/picasso/PicassoLruCache;

    if-eqz v0, :cond_0

    .line 115
    sget-object v0, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->lruCache:Lcom/navdy/hud/app/util/picasso/PicassoLruCache;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "file://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/util/picasso/PicassoLruCache;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 117
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getBitmapfromCache(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 122
    sget-object v0, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->lruCache:Lcom/navdy/hud/app/util/picasso/PicassoLruCache;

    if-eqz v0, :cond_0

    .line 123
    sget-object v0, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->lruCache:Lcom/navdy/hud/app/util/picasso/PicassoLruCache;

    invoke-virtual {v0, p0}, Lcom/navdy/hud/app/util/picasso/PicassoLruCache;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 125
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getDiskLruCache()Lcom/navdy/hud/app/storage/cache/DiskLruCache;
    .locals 1

    .prologue
    .line 151
    sget-object v0, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->diskLruCache:Lcom/navdy/hud/app/storage/cache/DiskLruCache;

    return-object v0
.end method

.method public static getDiskcacheUri(Ljava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 155
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "diskcache://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getInstance()Lcom/squareup/picasso/Picasso;
    .locals 1

    .prologue
    .line 136
    sget-boolean v0, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->initialized:Z

    if-nez v0, :cond_0

    .line 137
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->initPicasso(Landroid/content/Context;)V

    .line 139
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->picasso:Lcom/squareup/picasso/Picasso;

    return-object v0
.end method

.method public static initPicasso(Landroid/content/Context;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    sget-boolean v3, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->initialized:Z

    if-nez v3, :cond_1

    .line 54
    sget-object v4, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->lockObj:Ljava/lang/Object;

    monitor-enter v4

    .line 55
    :try_start_0
    sget-boolean v3, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->initialized:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v3, :cond_0

    .line 57
    :try_start_1
    new-instance v3, Lcom/navdy/hud/app/util/picasso/PicassoLruCache;

    const/high16 v5, 0x800000

    invoke-direct {v3, v5}, Lcom/navdy/hud/app/util/picasso/PicassoLruCache;-><init>(I)V

    sput-object v3, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->lruCache:Lcom/navdy/hud/app/util/picasso/PicassoLruCache;

    .line 58
    sget-object v3, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->lruCache:Lcom/navdy/hud/app/util/picasso/PicassoLruCache;

    invoke-virtual {v3}, Lcom/navdy/hud/app/util/picasso/PicassoLruCache;->init()V

    .line 60
    invoke-static {}, Lcom/navdy/hud/app/storage/PathManager;->getInstance()Lcom/navdy/hud/app/storage/PathManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/storage/PathManager;->getImageDiskCacheFolder()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 62
    .local v0, "imageDiskCacheFolder":Ljava/lang/String;
    :try_start_2
    new-instance v3, Lcom/navdy/hud/app/storage/cache/DiskLruCache;

    const-string v5, "imagecache"

    const/high16 v6, 0xa00000

    invoke-direct {v3, v5, v0, v6}, Lcom/navdy/hud/app/storage/cache/DiskLruCache;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v3, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->diskLruCache:Lcom/navdy/hud/app/storage/cache/DiskLruCache;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 73
    :goto_0
    :try_start_3
    new-instance v3, Lcom/squareup/picasso/Picasso$Builder;

    invoke-direct {v3, p0}, Lcom/squareup/picasso/Picasso$Builder;-><init>(Landroid/content/Context;)V

    sget-object v5, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->lruCache:Lcom/navdy/hud/app/util/picasso/PicassoLruCache;

    .line 74
    invoke-virtual {v3, v5}, Lcom/squareup/picasso/Picasso$Builder;->memoryCache(Lcom/squareup/picasso/Cache;)Lcom/squareup/picasso/Picasso$Builder;

    move-result-object v3

    new-instance v5, Lcom/navdy/hud/app/util/picasso/PicassoUtil$1;

    invoke-direct {v5}, Lcom/navdy/hud/app/util/picasso/PicassoUtil$1;-><init>()V

    .line 75
    invoke-virtual {v3, v5}, Lcom/squareup/picasso/Picasso$Builder;->addRequestHandler(Lcom/squareup/picasso/RequestHandler;)Lcom/squareup/picasso/Picasso$Builder;

    move-result-object v3

    .line 96
    invoke-virtual {v3}, Lcom/squareup/picasso/Picasso$Builder;->build()Lcom/squareup/picasso/Picasso;

    move-result-object v3

    sput-object v3, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->picasso:Lcom/squareup/picasso/Picasso;

    .line 97
    sget-object v3, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->picasso:Lcom/squareup/picasso/Picasso;

    invoke-static {v3}, Lcom/squareup/picasso/Picasso;->setSingletonInstance(Lcom/squareup/picasso/Picasso;)V

    .line 98
    const/4 v3, 0x1

    sput-boolean v3, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->initialized:Z
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 103
    .end local v0    # "imageDiskCacheFolder":Ljava/lang/String;
    :cond_0
    :goto_1
    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 105
    :cond_1
    return-void

    .line 63
    .restart local v0    # "imageDiskCacheFolder":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 64
    .local v1, "t":Ljava/lang/Throwable;
    :try_start_5
    sget-object v3, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error creating disk cache: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 66
    :try_start_6
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v3}, Lcom/navdy/service/library/util/IOUtils;->deleteDirectory(Landroid/content/Context;Ljava/io/File;)V

    .line 67
    new-instance v3, Lcom/navdy/hud/app/storage/cache/DiskLruCache;

    const-string v5, "imagecache"

    const/high16 v6, 0xa00000

    invoke-direct {v3, v5, v0, v6}, Lcom/navdy/hud/app/storage/cache/DiskLruCache;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v3, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->diskLruCache:Lcom/navdy/hud/app/storage/cache/DiskLruCache;
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    .line 68
    :catch_1
    move-exception v2

    .line 69
    .local v2, "t2":Ljava/lang/Throwable;
    :try_start_7
    sget-object v3, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error re-creating disk cache: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_0

    .line 99
    .end local v0    # "imageDiskCacheFolder":Ljava/lang/String;
    .end local v1    # "t":Ljava/lang/Throwable;
    .end local v2    # "t2":Ljava/lang/Throwable;
    :catch_2
    move-exception v1

    .line 100
    .restart local v1    # "t":Ljava/lang/Throwable;
    :try_start_8
    sget-object v3, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v3, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 103
    .end local v1    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    throw v3
.end method

.method public static isImageAvailableInCache(Ljava/io/File;)Z
    .locals 1
    .param p0, "file"    # Ljava/io/File;

    .prologue
    .line 143
    invoke-static {p0}, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->getBitmapfromCache(Ljava/io/File;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 144
    const/4 v0, 0x1

    .line 146
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static setBitmapInCache(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 130
    sget-object v0, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->lruCache:Lcom/navdy/hud/app/util/picasso/PicassoLruCache;

    if-eqz v0, :cond_0

    .line 131
    sget-object v0, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->lruCache:Lcom/navdy/hud/app/util/picasso/PicassoLruCache;

    invoke-virtual {v0, p0, p1}, Lcom/navdy/hud/app/util/picasso/PicassoLruCache;->setBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 133
    :cond_0
    return-void
.end method
