.class public Lcom/navdy/hud/app/util/picasso/PicassoCacheMap;
.super Ljava/util/LinkedHashMap;
.source "PicassoCacheMap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/LinkedHashMap",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field private listener:Lcom/navdy/hud/app/util/picasso/PicassoItemCacheListener;


# direct methods
.method constructor <init>(IF)V
    .locals 1
    .param p1, "initialCapacity"    # I
    .param p2, "loadFactor"    # F

    .prologue
    .line 12
    .local p0, "this":Lcom/navdy/hud/app/util/picasso/PicassoCacheMap;, "Lcom/navdy/hud/app/util/picasso/PicassoCacheMap<TK;TV;>;"
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    .line 13
    return-void
.end method


# virtual methods
.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 27
    .local p0, "this":Lcom/navdy/hud/app/util/picasso/PicassoCacheMap;, "Lcom/navdy/hud/app/util/picasso/PicassoCacheMap<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    .local p2, "value":Ljava/lang/Object;, "TV;"
    invoke-super {p0, p1, p2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 28
    .local v0, "ret":Ljava/lang/Object;, "TV;"
    iget-object v1, p0, Lcom/navdy/hud/app/util/picasso/PicassoCacheMap;->listener:Lcom/navdy/hud/app/util/picasso/PicassoItemCacheListener;

    invoke-interface {v1, p1}, Lcom/navdy/hud/app/util/picasso/PicassoItemCacheListener;->itemAdded(Ljava/lang/Object;)V

    .line 29
    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "key"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 21
    .local p0, "this":Lcom/navdy/hud/app/util/picasso/PicassoCacheMap;, "Lcom/navdy/hud/app/util/picasso/PicassoCacheMap<TK;TV;>;"
    iget-object v0, p0, Lcom/navdy/hud/app/util/picasso/PicassoCacheMap;->listener:Lcom/navdy/hud/app/util/picasso/PicassoItemCacheListener;

    invoke-interface {v0, p1}, Lcom/navdy/hud/app/util/picasso/PicassoItemCacheListener;->itemRemoved(Ljava/lang/Object;)V

    .line 22
    invoke-super {p0, p1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method setListener(Lcom/navdy/hud/app/util/picasso/PicassoItemCacheListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/navdy/hud/app/util/picasso/PicassoItemCacheListener;

    .prologue
    .line 16
    .local p0, "this":Lcom/navdy/hud/app/util/picasso/PicassoCacheMap;, "Lcom/navdy/hud/app/util/picasso/PicassoCacheMap<TK;TV;>;"
    iput-object p1, p0, Lcom/navdy/hud/app/util/picasso/PicassoCacheMap;->listener:Lcom/navdy/hud/app/util/picasso/PicassoItemCacheListener;

    .line 17
    return-void
.end method
