.class final Lcom/navdy/hud/app/util/picasso/PicassoUtil$1;
.super Lcom/squareup/picasso/RequestHandler;
.source "PicassoUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/util/picasso/PicassoUtil;->initPicasso(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/squareup/picasso/RequestHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public canHandleRequest(Lcom/squareup/picasso/Request;)Z
    .locals 2
    .param p1, "data"    # Lcom/squareup/picasso/Request;

    .prologue
    .line 78
    iget-object v1, p1, Lcom/squareup/picasso/Request;->uri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 79
    .local v0, "scheme":Ljava/lang/String;
    const-string v1, "diskcache"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method public load(Lcom/squareup/picasso/Request;I)Lcom/squareup/picasso/RequestHandler$Result;
    .locals 5
    .param p1, "request"    # Lcom/squareup/picasso/Request;
    .param p2, "networkPolicy"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    iget-object v2, p1, Lcom/squareup/picasso/Request;->uri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    .line 85
    .local v1, "resourceName":Ljava/lang/String;
    # getter for: Lcom/navdy/hud/app/util/picasso/PicassoUtil;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "load:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 86
    # getter for: Lcom/navdy/hud/app/util/picasso/PicassoUtil;->diskLruCache:Lcom/navdy/hud/app/storage/cache/DiskLruCache;
    invoke-static {}, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->access$100()Lcom/navdy/hud/app/storage/cache/DiskLruCache;

    move-result-object v2

    if-nez v2, :cond_0

    .line 87
    new-instance v2, Ljava/io/IOException;

    invoke-direct {v2}, Ljava/io/IOException;-><init>()V

    throw v2

    .line 89
    :cond_0
    # getter for: Lcom/navdy/hud/app/util/picasso/PicassoUtil;->diskLruCache:Lcom/navdy/hud/app/storage/cache/DiskLruCache;
    invoke-static {}, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->access$100()Lcom/navdy/hud/app/storage/cache/DiskLruCache;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->get(Ljava/lang/String;)[B

    move-result-object v0

    .line 90
    .local v0, "data":[B
    if-nez v0, :cond_1

    .line 91
    new-instance v2, Ljava/io/IOException;

    invoke-direct {v2}, Ljava/io/IOException;-><init>()V

    throw v2

    .line 93
    :cond_1
    new-instance v2, Lcom/squareup/picasso/RequestHandler$Result;

    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-direct {v3, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    sget-object v4, Lcom/squareup/picasso/Picasso$LoadedFrom;->DISK:Lcom/squareup/picasso/Picasso$LoadedFrom;

    invoke-direct {v2, v3, v4}, Lcom/squareup/picasso/RequestHandler$Result;-><init>(Ljava/io/InputStream;Lcom/squareup/picasso/Picasso$LoadedFrom;)V

    return-object v2
.end method
