.class public Lcom/navdy/hud/app/util/ScreenConductor;
.super Ljava/lang/Object;
.source "ScreenConductor.java"

# interfaces
.implements Lcom/navdy/hud/app/util/CanShowScreen;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S::",
        "Lmortar/Blueprint;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/navdy/hud/app/util/CanShowScreen",
        "<TS;>;"
    }
.end annotation


# instance fields
.field private final container:Landroid/view/ViewGroup;

.field private final context:Landroid/content/Context;

.field homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

.field private final logger:Lcom/navdy/service/library/log/Logger;

.field uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/navdy/hud/app/ui/framework/UIStateManager;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "uiStateManager"    # Lcom/navdy/hud/app/ui/framework/UIStateManager;

    .prologue
    .line 46
    .local p0, "this":Lcom/navdy/hud/app/util/ScreenConductor;, "Lcom/navdy/hud/app/util/ScreenConductor<TS;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/hud/app/util/ScreenConductor;->logger:Lcom/navdy/service/library/log/Logger;

    .line 47
    iput-object p1, p0, Lcom/navdy/hud/app/util/ScreenConductor;->context:Landroid/content/Context;

    .line 48
    iput-object p2, p0, Lcom/navdy/hud/app/util/ScreenConductor;->container:Landroid/view/ViewGroup;

    .line 49
    iput-object p3, p0, Lcom/navdy/hud/app/util/ScreenConductor;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    .line 50
    return-void
.end method


# virtual methods
.method public createScreens()V
    .locals 7

    .prologue
    .line 53
    .local p0, "this":Lcom/navdy/hud/app/util/ScreenConductor;, "Lcom/navdy/hud/app/util/ScreenConductor<TS;>;"
    iget-object v4, p0, Lcom/navdy/hud/app/util/ScreenConductor;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    if-nez v4, :cond_0

    .line 54
    new-instance v3, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen;

    invoke-direct {v3}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen;-><init>()V

    .line 55
    .local v3, "screen":Lcom/navdy/hud/app/screen/BaseScreen;
    iget-object v4, p0, Lcom/navdy/hud/app/util/ScreenConductor;->context:Landroid/content/Context;

    invoke-static {v4}, Lmortar/Mortar;->getScope(Landroid/content/Context;)Lmortar/MortarScope;

    move-result-object v1

    .line 56
    .local v1, "myScope":Lmortar/MortarScope;
    invoke-interface {v1, v3}, Lmortar/MortarScope;->requireChild(Lmortar/Blueprint;)Lmortar/MortarScope;

    move-result-object v2

    .line 57
    .local v2, "newChildScope":Lmortar/MortarScope;
    iget-object v4, p0, Lcom/navdy/hud/app/util/ScreenConductor;->context:Landroid/content/Context;

    invoke-interface {v2, v4}, Lmortar/MortarScope;->createContext(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    .line 58
    .local v0, "childContext":Landroid/content/Context;
    invoke-static {v0, v3}, Lflow/Layouts;->createView(Landroid/content/Context;Ljava/lang/Object;)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    iput-object v4, p0, Lcom/navdy/hud/app/util/ScreenConductor;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    .line 59
    iget-object v4, p0, Lcom/navdy/hud/app/util/ScreenConductor;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->setVisibility(I)V

    .line 60
    iget-object v4, p0, Lcom/navdy/hud/app/util/ScreenConductor;->container:Landroid/view/ViewGroup;

    iget-object v5, p0, Lcom/navdy/hud/app/util/ScreenConductor;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 61
    iget-object v4, p0, Lcom/navdy/hud/app/util/ScreenConductor;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    iget-object v5, p0, Lcom/navdy/hud/app/util/ScreenConductor;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->setHomescreenView(Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;)V

    .line 62
    iget-object v4, p0, Lcom/navdy/hud/app/util/ScreenConductor;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "createScreens:homescreen created and added"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 64
    .end local v0    # "childContext":Landroid/content/Context;
    .end local v1    # "myScope":Lmortar/MortarScope;
    .end local v2    # "newChildScope":Lmortar/MortarScope;
    .end local v3    # "screen":Lcom/navdy/hud/app/screen/BaseScreen;
    :cond_0
    return-void
.end method

.method public getChildView()Landroid/view/View;
    .locals 4

    .prologue
    .local p0, "this":Lcom/navdy/hud/app/util/ScreenConductor;, "Lcom/navdy/hud/app/util/ScreenConductor<TS;>;"
    const/4 v2, 0x0

    .line 217
    iget-object v3, p0, Lcom/navdy/hud/app/util/ScreenConductor;->container:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 218
    .local v0, "count":I
    if-nez v0, :cond_1

    move-object v1, v2

    .line 229
    :cond_0
    :goto_0
    return-object v1

    .line 221
    :cond_1
    add-int/lit8 v0, v0, -0x1

    .line 222
    :goto_1
    if-ltz v0, :cond_2

    .line 223
    iget-object v3, p0, Lcom/navdy/hud/app/util/ScreenConductor;->container:Landroid/view/ViewGroup;

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 224
    .local v1, "view":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-eqz v3, :cond_0

    .line 227
    add-int/lit8 v0, v0, -0x1

    .line 228
    goto :goto_1

    .end local v1    # "view":Landroid/view/View;
    :cond_2
    move-object v1, v2

    .line 229
    goto :goto_0
.end method

.method protected setAnimation(Lflow/Flow$Direction;Landroid/view/View;Landroid/view/View;Lcom/navdy/hud/app/screen/BaseScreen;Lcom/navdy/hud/app/screen/BaseScreen;II)V
    .locals 5
    .param p1, "direction"    # Lflow/Flow$Direction;
    .param p2, "oldChild"    # Landroid/view/View;
    .param p3, "newChild"    # Landroid/view/View;
    .param p4, "oldScreen"    # Lcom/navdy/hud/app/screen/BaseScreen;
    .param p5, "newScreen"    # Lcom/navdy/hud/app/screen/BaseScreen;
    .param p6, "animIn"    # I
    .param p7, "animOut"    # I

    .prologue
    .local p0, "this":Lcom/navdy/hud/app/util/ScreenConductor;, "Lcom/navdy/hud/app/util/ScreenConductor<TS;>;"
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, -0x1

    .line 151
    if-nez p2, :cond_1

    .line 153
    if-eqz p5, :cond_0

    .line 154
    invoke-virtual {p5}, Lcom/navdy/hud/app/screen/BaseScreen;->onAnimationInStart()V

    .line 155
    iget-object v1, p0, Lcom/navdy/hud/app/util/ScreenConductor;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v1, v3, p5, v4}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->postScreenAnimationEvent(ZLcom/navdy/hud/app/screen/BaseScreen;Lcom/navdy/hud/app/screen/BaseScreen;)V

    .line 156
    invoke-virtual {p5}, Lcom/navdy/hud/app/screen/BaseScreen;->onAnimationInEnd()V

    .line 157
    iget-object v1, p0, Lcom/navdy/hud/app/util/ScreenConductor;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, p5, v4}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->postScreenAnimationEvent(ZLcom/navdy/hud/app/screen/BaseScreen;Lcom/navdy/hud/app/screen/BaseScreen;)V

    .line 214
    :cond_0
    :goto_0
    return-void

    .line 161
    :cond_1
    if-ne p6, v2, :cond_2

    if-ne p7, v2, :cond_2

    .line 162
    iget-object v1, p0, Lcom/navdy/hud/app/util/ScreenConductor;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v1, v3, p5, p4}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->postScreenAnimationEvent(ZLcom/navdy/hud/app/screen/BaseScreen;Lcom/navdy/hud/app/screen/BaseScreen;)V

    goto :goto_0

    .line 165
    :cond_2
    if-eq p7, v2, :cond_4

    .line 166
    iget-object v1, p0, Lcom/navdy/hud/app/util/ScreenConductor;->context:Landroid/content/Context;

    invoke-static {v1, p7}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 167
    .local v0, "animation":Landroid/view/animation/Animation;
    if-eqz p4, :cond_3

    .line 168
    new-instance v1, Lcom/navdy/hud/app/util/ScreenConductor$1;

    invoke-direct {v1, p0, p4, p6, p5}, Lcom/navdy/hud/app/util/ScreenConductor$1;-><init>(Lcom/navdy/hud/app/util/ScreenConductor;Lcom/navdy/hud/app/screen/BaseScreen;ILcom/navdy/hud/app/screen/BaseScreen;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 190
    :cond_3
    invoke-virtual {p2, v0}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    .line 192
    .end local v0    # "animation":Landroid/view/animation/Animation;
    :cond_4
    if-eq p6, v2, :cond_0

    .line 193
    iget-object v1, p0, Lcom/navdy/hud/app/util/ScreenConductor;->context:Landroid/content/Context;

    invoke-static {v1, p6}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 194
    .restart local v0    # "animation":Landroid/view/animation/Animation;
    if-eqz p5, :cond_5

    .line 195
    new-instance v1, Lcom/navdy/hud/app/util/ScreenConductor$2;

    invoke-direct {v1, p0, p5, p4}, Lcom/navdy/hud/app/util/ScreenConductor$2;-><init>(Lcom/navdy/hud/app/util/ScreenConductor;Lcom/navdy/hud/app/screen/BaseScreen;Lcom/navdy/hud/app/screen/BaseScreen;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 212
    :cond_5
    invoke-virtual {p3, v0}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public showScreen(Lmortar/Blueprint;Lflow/Flow$Direction;II)V
    .locals 17
    .param p2, "direction"    # Lflow/Flow$Direction;
    .param p3, "animIn"    # I
    .param p4, "animOut"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;",
            "Lflow/Flow$Direction;",
            "II)V"
        }
    .end annotation

    .prologue
    .line 67
    .local p0, "this":Lcom/navdy/hud/app/util/ScreenConductor;, "Lcom/navdy/hud/app/util/ScreenConductor<TS;>;"
    .local p1, "screen":Lmortar/Blueprint;, "TS;"
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/util/ScreenConductor;->context:Landroid/content/Context;

    invoke-static {v1}, Lmortar/Mortar;->getScope(Landroid/content/Context;)Lmortar/MortarScope;

    move-result-object v13

    .line 68
    .local v13, "myScope":Lmortar/MortarScope;
    move-object/from16 v0, p1

    invoke-interface {v13, v0}, Lmortar/MortarScope;->requireChild(Lmortar/Blueprint;)Lmortar/MortarScope;

    move-result-object v14

    .line 70
    .local v14, "newChildScope":Lmortar/MortarScope;
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/hud/app/util/ScreenConductor;->getChildView()Landroid/view/View;

    move-result-object v3

    .line 71
    .local v3, "oldChild":Landroid/view/View;
    const/4 v4, 0x0

    .line 73
    .local v4, "newChild":Landroid/view/View;
    if-eqz v3, :cond_1

    .line 74
    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lmortar/Mortar;->getScope(Landroid/content/Context;)Lmortar/MortarScope;

    move-result-object v15

    .line 75
    .local v15, "oldChildScope":Lmortar/MortarScope;
    invoke-interface {v15}, Lmortar/MortarScope;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface/range {p1 .. p1}, Lmortar/Blueprint;->getMortarScopeName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 77
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    move-object/from16 v9, p1

    .line 78
    check-cast v9, Lcom/navdy/hud/app/screen/BaseScreen;

    .line 79
    .local v9, "baseScreen":Lcom/navdy/hud/app/screen/BaseScreen;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/util/ScreenConductor;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v9, v9}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->postScreenAnimationEvent(ZLcom/navdy/hud/app/screen/BaseScreen;Lcom/navdy/hud/app/screen/BaseScreen;)V

    .line 80
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/util/ScreenConductor;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v9, v9}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->postScreenAnimationEvent(ZLcom/navdy/hud/app/screen/BaseScreen;Lcom/navdy/hud/app/screen/BaseScreen;)V

    .line 148
    .end local v9    # "baseScreen":Lcom/navdy/hud/app/screen/BaseScreen;
    .end local v15    # "oldChildScope":Lmortar/MortarScope;
    .end local p1    # "screen":Lmortar/Blueprint;, "TS;"
    :cond_0
    :goto_0
    return-void

    .line 86
    .restart local p1    # "screen":Lmortar/Blueprint;, "TS;"
    :cond_1
    move-object/from16 v0, p1

    instance-of v11, v0, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen;

    .line 87
    .local v11, "homescreen":Z
    const/4 v12, 0x0

    .line 89
    .local v12, "homescreenCreated":Z
    if-eqz v11, :cond_2

    .line 90
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/util/ScreenConductor;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    if-eqz v1, :cond_2

    .line 91
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/util/ScreenConductor;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "reusing homescreen view"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 92
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/hud/app/util/ScreenConductor;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    .line 95
    :cond_2
    if-nez v4, :cond_3

    .line 97
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/util/ScreenConductor;->context:Landroid/content/Context;

    invoke-interface {v14, v1}, Lmortar/MortarScope;->createContext(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v10

    .line 98
    .local v10, "childContext":Landroid/content/Context;
    move-object/from16 v0, p1

    invoke-static {v10, v0}, Lflow/Layouts;->createView(Landroid/content/Context;Ljava/lang/Object;)Landroid/view/View;

    move-result-object v4

    .line 99
    if-eqz v11, :cond_3

    move-object v1, v4

    .line 100
    check-cast v1, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/navdy/hud/app/util/ScreenConductor;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    .line 101
    const/4 v12, 0x1

    .line 105
    .end local v10    # "childContext":Landroid/content/Context;
    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/util/ScreenConductor;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getCurrentScreen()Lcom/navdy/hud/app/screen/BaseScreen;

    move-result-object v5

    .local v5, "oldScreen":Lcom/navdy/hud/app/screen/BaseScreen;
    move-object/from16 v6, p1

    .line 106
    check-cast v6, Lcom/navdy/hud/app/screen/BaseScreen;

    .local v6, "newScreen":Lcom/navdy/hud/app/screen/BaseScreen;
    move-object/from16 v1, p0

    move-object/from16 v2, p2

    move/from16 v7, p3

    move/from16 v8, p4

    .line 108
    invoke-virtual/range {v1 .. v8}, Lcom/navdy/hud/app/util/ScreenConductor;->setAnimation(Lflow/Flow$Direction;Landroid/view/View;Landroid/view/View;Lcom/navdy/hud/app/screen/BaseScreen;Lcom/navdy/hud/app/screen/BaseScreen;II)V

    .line 111
    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/util/ScreenConductor;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    if-eq v3, v1, :cond_4

    .line 112
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/util/ScreenConductor;->container:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 114
    :cond_4
    if-eqz v11, :cond_8

    .line 115
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/util/ScreenConductor;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    if-eqz v1, :cond_5

    .line 116
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/util/ScreenConductor;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->setVisibility(I)V

    .line 118
    :cond_5
    if-eqz v12, :cond_6

    .line 119
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/util/ScreenConductor;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "home screen added"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 120
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/util/ScreenConductor;->container:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 122
    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/util/ScreenConductor;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->onResume()V

    .line 123
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/util/ScreenConductor;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->setVisibility(I)V

    .line 136
    :goto_1
    invoke-static {}, Lcom/navdy/hud/app/util/GenericUtil;->clearInputMethodManagerFocusLeak()V

    .line 138
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/hud/app/util/ScreenConductor;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-object/from16 v1, p1

    check-cast v1, Lcom/navdy/hud/app/screen/BaseScreen;

    invoke-virtual {v2, v1}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->setMainActiveScreen(Lcom/navdy/hud/app/screen/BaseScreen;)V

    move-object/from16 v1, p1

    .line 139
    check-cast v1, Lcom/navdy/hud/app/screen/BaseScreen;

    invoke-virtual {v1}, Lcom/navdy/hud/app/screen/BaseScreen;->getScreen()Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v16

    .line 140
    .local v16, "scr":Lcom/navdy/service/library/events/ui/Screen;
    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_BACK:Lcom/navdy/service/library/events/ui/Screen;

    move-object/from16 v0, v16

    if-eq v0, v1, :cond_7

    .line 141
    check-cast p1, Lcom/navdy/hud/app/screen/BaseScreen;

    .end local p1    # "screen":Lmortar/Blueprint;, "TS;"
    invoke-static/range {p1 .. p1}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordScreen(Lcom/navdy/hud/app/screen/BaseScreen;)V

    .line 144
    :cond_7
    const/4 v1, -0x1

    move/from16 v0, p3

    if-ne v0, v1, :cond_0

    const/4 v1, -0x1

    move/from16 v0, p4

    if-ne v0, v1, :cond_0

    .line 146
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/util/ScreenConductor;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v6, v5}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->postScreenAnimationEvent(ZLcom/navdy/hud/app/screen/BaseScreen;Lcom/navdy/hud/app/screen/BaseScreen;)V

    goto/16 :goto_0

    .line 125
    .end local v16    # "scr":Lcom/navdy/service/library/events/ui/Screen;
    .restart local p1    # "screen":Lmortar/Blueprint;, "TS;"
    :cond_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/util/ScreenConductor;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    if-eqz v1, :cond_9

    .line 126
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/util/ScreenConductor;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-object/from16 v1, p1

    check-cast v1, Lcom/navdy/hud/app/screen/BaseScreen;

    invoke-virtual {v1}, Lcom/navdy/hud/app/screen/BaseScreen;->getScreen()Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v1

    invoke-static {v1}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->isPauseHomescreen(Lcom/navdy/service/library/events/ui/Screen;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 127
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/util/ScreenConductor;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    invoke-virtual {v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->onPause()V

    .line 128
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/util/ScreenConductor;->homeScreenView:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->setVisibility(I)V

    .line 131
    :cond_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/hud/app/util/ScreenConductor;->container:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_1
.end method
