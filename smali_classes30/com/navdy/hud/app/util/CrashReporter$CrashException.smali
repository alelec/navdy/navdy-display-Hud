.class public Lcom/navdy/hud/app/util/CrashReporter$CrashException;
.super Ljava/lang/Exception;
.source "CrashReporter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/util/CrashReporter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CrashException"
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "detailMessage"    # Ljava/lang/String;

    .prologue
    .line 81
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 82
    return-void
.end method


# virtual methods
.method extractCrashLocation(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 106
    const-string v0, ""

    .line 107
    .local v0, "crashLocation":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/navdy/hud/app/util/CrashReporter$CrashException;->getLocationPattern()Ljava/util/regex/Pattern;

    move-result-object v3

    .line 108
    .local v3, "pattern":Ljava/util/regex/Pattern;
    if-eqz v3, :cond_0

    if-eqz p1, :cond_0

    .line 109
    invoke-virtual {v3, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 110
    .local v2, "match":Ljava/util/regex/Matcher;
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 111
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v4

    const/4 v5, 0x1

    if-lt v4, v5, :cond_2

    .line 112
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v4

    if-gt v1, v4, :cond_0

    .line 113
    invoke-virtual {v2, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 114
    invoke-virtual {v2, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    .line 123
    .end local v1    # "i":I
    .end local v2    # "match":Ljava/util/regex/Matcher;
    :cond_0
    :goto_1
    return-object v0

    .line 112
    .restart local v1    # "i":I
    .restart local v2    # "match":Ljava/util/regex/Matcher;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 119
    .end local v1    # "i":I
    :cond_2
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public filter(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 89
    return-object p1
.end method

.method public getCrashLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/navdy/hud/app/util/CrashReporter$CrashException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/navdy/hud/app/util/CrashReporter$CrashException;->extractCrashLocation(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLocationPattern()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 98
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    .line 99
    .local v2, "name":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/navdy/hud/app/util/CrashReporter$CrashException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v1

    .line 100
    .local v1, "msg":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/util/CrashReporter$CrashException;->filter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 102
    .local v0, "filteredMsg":Ljava/lang/String;
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/navdy/hud/app/util/CrashReporter$CrashException;->getCrashLocation()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 100
    .end local v0    # "filteredMsg":Ljava/lang/String;
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method
