.class public Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;
.super Ljava/lang/Object;
.source "GForceRawSensorDataProcessor.java"


# static fields
.field private static logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private calibrated:Z

.field private rotationMatrix:Landroid/renderscript/Matrix3f;

.field private v:[F

.field public xAccel:F

.field public yAccel:F

.field public zAccel:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;->logger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/4 v0, 0x3

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;->v:[F

    return-void
.end method

.method private calculateRotationMatrix([F)Landroid/renderscript/Matrix3f;
    .locals 20
    .param p1, "v"    # [F

    .prologue
    .line 70
    new-instance v6, Landroid/renderscript/Matrix3f;

    invoke-direct {v6}, Landroid/renderscript/Matrix3f;-><init>()V

    .line 71
    .local v6, "result":Landroid/renderscript/Matrix3f;
    const/16 v16, 0x0

    aget v13, p1, v16

    .line 72
    .local v13, "x":F
    const/16 v16, 0x1

    aget v14, p1, v16

    .line 73
    .local v14, "y":F
    const/16 v16, 0x2

    aget v15, p1, v16

    .line 74
    .local v15, "z":F
    float-to-double v0, v14

    move-wide/from16 v16, v0

    float-to-double v0, v15

    move-wide/from16 v18, v0

    invoke-static/range {v16 .. v19}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v8

    .line 75
    .local v8, "roll":D
    neg-float v0, v13

    move/from16 v16, v0

    move/from16 v0, v16

    float-to-double v0, v0

    move-wide/from16 v16, v0

    mul-float v18, v14, v14

    mul-float v19, v15, v15

    add-float v18, v18, v19

    move/from16 v0, v18

    float-to-double v0, v0

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v18

    invoke-static/range {v16 .. v19}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    .line 78
    .local v4, "pitch":D
    neg-double v8, v8

    .line 79
    neg-double v4, v4

    .line 83
    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v3, v0

    .line 84
    .local v3, "cr":F
    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v12, v0

    .line 85
    .local v12, "sr":F
    const/16 v16, 0x9

    move/from16 v0, v16

    new-array v7, v0, [F

    const/16 v16, 0x0

    const/high16 v17, 0x3f800000    # 1.0f

    aput v17, v7, v16

    const/16 v16, 0x1

    const/16 v17, 0x0

    aput v17, v7, v16

    const/16 v16, 0x2

    const/16 v17, 0x0

    aput v17, v7, v16

    const/16 v16, 0x3

    const/16 v17, 0x0

    aput v17, v7, v16

    const/16 v16, 0x4

    aput v3, v7, v16

    const/16 v16, 0x5

    aput v12, v7, v16

    const/16 v16, 0x6

    const/16 v17, 0x0

    aput v17, v7, v16

    const/16 v16, 0x7

    neg-float v0, v12

    move/from16 v17, v0

    aput v17, v7, v16

    const/16 v16, 0x8

    aput v3, v7, v16

    .line 90
    .local v7, "rx":[F
    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v2, v0

    .line 91
    .local v2, "cp":F
    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v11, v0

    .line 92
    .local v11, "sp":F
    const/16 v16, 0x9

    move/from16 v0, v16

    new-array v10, v0, [F

    const/16 v16, 0x0

    aput v2, v10, v16

    const/16 v16, 0x1

    const/16 v17, 0x0

    aput v17, v10, v16

    const/16 v16, 0x2

    neg-float v0, v11

    move/from16 v17, v0

    aput v17, v10, v16

    const/16 v16, 0x3

    const/16 v17, 0x0

    aput v17, v10, v16

    const/16 v16, 0x4

    const/high16 v17, 0x3f800000    # 1.0f

    aput v17, v10, v16

    const/16 v16, 0x5

    const/16 v17, 0x0

    aput v17, v10, v16

    const/16 v16, 0x6

    aput v11, v10, v16

    const/16 v16, 0x7

    const/16 v17, 0x0

    aput v17, v10, v16

    const/16 v16, 0x8

    aput v2, v10, v16

    .line 98
    .local v10, "ry":[F
    new-instance v16, Landroid/renderscript/Matrix3f;

    move-object/from16 v0, v16

    invoke-direct {v0, v7}, Landroid/renderscript/Matrix3f;-><init>([F)V

    new-instance v17, Landroid/renderscript/Matrix3f;

    move-object/from16 v0, v17

    invoke-direct {v0, v10}, Landroid/renderscript/Matrix3f;-><init>([F)V

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v6, v0, v1}, Landroid/renderscript/Matrix3f;->loadMultiply(Landroid/renderscript/Matrix3f;Landroid/renderscript/Matrix3f;)V

    .line 99
    sget-object v16, Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "acceleration vector: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, ","

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, ","

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " derived rotation matrix: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v6}, Landroid/renderscript/Matrix3f;->getArray()[F

    move-result-object v18

    invoke-static/range {v18 .. v18}, Ljava/util/Arrays;->toString([F)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 100
    return-object v6
.end method

.method private magnitude([F)F
    .locals 4
    .param p1, "v"    # [F

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 47
    aget v0, p1, v1

    aget v1, p1, v1

    mul-float/2addr v0, v1

    aget v1, p1, v2

    aget v2, p1, v2

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    aget v1, p1, v3

    aget v2, p1, v3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method private multiply(Landroid/renderscript/Matrix3f;[F)V
    .locals 12
    .param p1, "matrix"    # Landroid/renderscript/Matrix3f;
    .param p2, "v"    # [F

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 52
    invoke-virtual {p1}, Landroid/renderscript/Matrix3f;->getArray()[F

    move-result-object v0

    .line 53
    .local v0, "r":[F
    aget v4, p2, v9

    .line 54
    .local v4, "x":F
    aget v5, p2, v10

    .line 55
    .local v5, "y":F
    aget v6, p2, v11

    .line 57
    .local v6, "z":F
    aget v7, v0, v9

    mul-float/2addr v7, v4

    aget v8, v0, v10

    mul-float/2addr v8, v5

    add-float/2addr v7, v8

    aget v8, v0, v11

    mul-float/2addr v8, v6

    add-float v1, v7, v8

    .line 58
    .local v1, "rx":F
    const/4 v7, 0x3

    aget v7, v0, v7

    mul-float/2addr v7, v4

    const/4 v8, 0x4

    aget v8, v0, v8

    mul-float/2addr v8, v5

    add-float/2addr v7, v8

    const/4 v8, 0x5

    aget v8, v0, v8

    mul-float/2addr v8, v6

    add-float v2, v7, v8

    .line 59
    .local v2, "ry":F
    const/4 v7, 0x6

    aget v7, v0, v7

    mul-float/2addr v7, v4

    const/4 v8, 0x7

    aget v8, v0, v8

    mul-float/2addr v8, v5

    add-float/2addr v7, v8

    const/16 v8, 0x8

    aget v8, v0, v8

    mul-float/2addr v8, v6

    add-float v3, v7, v8

    .line 61
    .local v3, "rz":F
    aput v1, p2, v9

    .line 62
    aput v2, p2, v10

    .line 63
    aput v3, p2, v11

    .line 64
    return-void
.end method


# virtual methods
.method public isCalibrated()Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;->calibrated:Z

    return v0
.end method

.method public onRawData(Lcom/navdy/hud/app/device/gps/RawSensorData;)V
    .locals 9
    .param p1, "sensorData"    # Lcom/navdy/hud/app/device/gps/RawSensorData;

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 23
    iget-object v1, p0, Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;->v:[F

    iget v2, p1, Lcom/navdy/hud/app/device/gps/RawSensorData;->x:F

    aput v2, v1, v7

    .line 24
    iget-object v1, p0, Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;->v:[F

    iget v2, p1, Lcom/navdy/hud/app/device/gps/RawSensorData;->y:F

    aput v2, v1, v6

    .line 25
    iget-object v1, p0, Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;->v:[F

    iget v2, p1, Lcom/navdy/hud/app/device/gps/RawSensorData;->z:F

    aput v2, v1, v8

    .line 26
    iget-boolean v1, p0, Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;->calibrated:Z

    if-nez v1, :cond_0

    .line 27
    iget-object v1, p0, Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;->v:[F

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;->magnitude([F)F

    move-result v0

    .line 31
    .local v0, "m":F
    float-to-double v2, v0

    const-wide v4, 0x3feccccccccccccdL    # 0.9

    cmpl-double v1, v2, v4

    if-lez v1, :cond_0

    float-to-double v2, v0

    const-wide v4, 0x3ff199999999999aL    # 1.1

    cmpg-double v1, v2, v4

    if-gez v1, :cond_0

    .line 32
    iget-object v1, p0, Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;->v:[F

    invoke-direct {p0, v1}, Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;->calculateRotationMatrix([F)Landroid/renderscript/Matrix3f;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;->rotationMatrix:Landroid/renderscript/Matrix3f;

    .line 33
    iput-boolean v6, p0, Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;->calibrated:Z

    .line 37
    .end local v0    # "m":F
    :cond_0
    iget-boolean v1, p0, Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;->calibrated:Z

    if-eqz v1, :cond_1

    .line 38
    iget-object v1, p0, Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;->rotationMatrix:Landroid/renderscript/Matrix3f;

    iget-object v2, p0, Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;->v:[F

    invoke-direct {p0, v1, v2}, Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;->multiply(Landroid/renderscript/Matrix3f;[F)V

    .line 40
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;->v:[F

    aget v1, v1, v7

    iput v1, p0, Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;->xAccel:F

    .line 41
    iget-object v1, p0, Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;->v:[F

    aget v1, v1, v6

    iput v1, p0, Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;->yAccel:F

    .line 42
    iget-object v1, p0, Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;->v:[F

    aget v1, v1, v8

    iput v1, p0, Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;->zAccel:F

    .line 43
    return-void
.end method

.method public setCalibrated(Z)V
    .locals 0
    .param p1, "calibrated"    # Z

    .prologue
    .line 104
    iput-boolean p1, p0, Lcom/navdy/hud/app/util/GForceRawSensorDataProcessor;->calibrated:Z

    .line 105
    return-void
.end method
