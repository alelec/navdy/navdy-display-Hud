.class public Lcom/navdy/hud/app/util/NavdyNativeLibWrapper;
.super Ljava/lang/Object;
.source "NavdyNativeLibWrapper.java"


# static fields
.field private static loaded:Z

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 10
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/util/NavdyNativeLibWrapper;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/util/NavdyNativeLibWrapper;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native crashSegv()V
.end method

.method public static native crashSegvOnNewThread()V
.end method

.method public static declared-synchronized loadlibrary()V
    .locals 4

    .prologue
    .line 21
    const-class v2, Lcom/navdy/hud/app/util/NavdyNativeLibWrapper;

    monitor-enter v2

    :try_start_0
    sget-boolean v1, Lcom/navdy/hud/app/util/NavdyNativeLibWrapper;->loaded:Z

    if-nez v1, :cond_0

    .line 22
    const/4 v1, 0x1

    sput-boolean v1, Lcom/navdy/hud/app/util/NavdyNativeLibWrapper;->loaded:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 24
    :try_start_1
    sget-object v1, Lcom/navdy/hud/app/util/NavdyNativeLibWrapper;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "loading navdy lib"

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 25
    const-string v1, "Navdy"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 26
    sget-object v1, Lcom/navdy/hud/app/util/NavdyNativeLibWrapper;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "loaded navdy lib"

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 31
    .local v0, "t":Ljava/lang/Throwable;
    :cond_0
    :goto_0
    monitor-exit v2

    return-void

    .line 27
    .end local v0    # "t":Ljava/lang/Throwable;
    :catch_0
    move-exception v0

    .line 28
    .restart local v0    # "t":Ljava/lang/Throwable;
    :try_start_2
    sget-object v1, Lcom/navdy/hud/app/util/NavdyNativeLibWrapper;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 21
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public static synchronized native declared-synchronized startCpuHog()V
.end method

.method public static synchronized native declared-synchronized stopCpuHog()V
.end method
