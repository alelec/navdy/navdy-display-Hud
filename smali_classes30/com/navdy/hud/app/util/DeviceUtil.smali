.class public Lcom/navdy/hud/app/util/DeviceUtil;
.super Ljava/lang/Object;
.source "DeviceUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;
    }
.end annotation


# static fields
.field public static final BUILD_TYPE_USER:Ljava/lang/String; = "user"

.field private static final CURRENT_HERE_SDK:Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;

.field public static final TELEMETRY_FILE_NAME:Ljava/lang/String; = "telemetry"

.field private static hasCamera:Ljava/lang/Boolean;

.field private static hudDevice:Z

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 24
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/util/DeviceUtil;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/util/DeviceUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 28
    sget-object v0, Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;->SDK:Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;

    sput-object v0, Lcom/navdy/hud/app/util/DeviceUtil;->CURRENT_HERE_SDK:Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;

    .line 37
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "NAVDY_HUD-MX6DL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "Display"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 38
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/navdy/hud/app/util/DeviceUtil;->hudDevice:Z

    .line 39
    sget-object v0, Lcom/navdy/hud/app/util/DeviceUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Running on Navdy Device:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 43
    :goto_0
    return-void

    .line 41
    :cond_1
    sget-object v0, Lcom/navdy/hud/app/util/DeviceUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Not running on Navdy Device:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static copyHEREMapsDataInfo(Ljava/lang/String;)V
    .locals 7
    .param p0, "destination"    # Ljava/lang/String;

    .prologue
    .line 87
    invoke-static {}, Lcom/navdy/hud/app/storage/PathManager;->getInstance()Lcom/navdy/hud/app/storage/PathManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/hud/app/storage/PathManager;->getHereMapsDataDirectory()Ljava/lang/String;

    move-result-object v2

    .line 88
    .local v2, "hereMapsDataDirectory":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 89
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_0

    .line 90
    sget-object v5, Lcom/navdy/hud/app/util/DeviceUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Here maps data directory on the maps partition does not exists"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 104
    :goto_0
    return-void

    .line 93
    :cond_0
    new-instance v3, Ljava/io/File;

    const-string v5, "meta.json"

    invoke-direct {v3, v0, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 94
    .local v3, "metaData":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_1

    .line 95
    sget-object v5, Lcom/navdy/hud/app/util/DeviceUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Meta json file not found"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 98
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "HERE_meta.json"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 100
    .local v4, "targetFile1":Ljava/lang/String;
    :try_start_0
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v4}, Lcom/navdy/service/library/util/IOUtils;->copyFile(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 101
    :catch_0
    move-exception v1

    .line 102
    .local v1, "e":Ljava/io/IOException;
    sget-object v5, Lcom/navdy/hud/app/util/DeviceUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Error copying the HERE maps data meta json file"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getCurrentHereSdkTimestamp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    sget-object v0, Lcom/navdy/hud/app/util/DeviceUtil;->CURRENT_HERE_SDK:Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;

    # getter for: Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;->folderName:Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;->access$100(Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getCurrentHereSdkVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    sget-object v0, Lcom/navdy/hud/app/util/DeviceUtil;->CURRENT_HERE_SDK:Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;

    # getter for: Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;->version:Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;->access$000(Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getHEREMapsDataInfo()Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 107
    invoke-static {}, Lcom/navdy/hud/app/storage/PathManager;->getInstance()Lcom/navdy/hud/app/storage/PathManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/hud/app/storage/PathManager;->getHereMapsDataDirectory()Ljava/lang/String;

    move-result-object v4

    .line 108
    .local v4, "hereMapsDataDirectory":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 109
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_1

    .line 110
    sget-object v7, Lcom/navdy/hud/app/util/DeviceUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "Here maps data directory on the maps partition does not exists"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 132
    :cond_0
    :goto_0
    return-object v6

    .line 113
    :cond_1
    new-instance v5, Ljava/io/File;

    const-string v7, "meta.json"

    invoke-direct {v5, v0, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 114
    .local v5, "metaData":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_2

    .line 115
    sget-object v7, Lcom/navdy/hud/app/util/DeviceUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "Meta json file not found"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 118
    :cond_2
    const/4 v2, 0x0

    .line 120
    .local v2, "fis":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v5}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 121
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .local v3, "fis":Ljava/io/FileInputStream;
    :try_start_1
    const-string v7, "UTF-8"

    invoke-static {v3, v7}, Lcom/navdy/service/library/util/IOUtils;->convertInputStreamToString(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v6

    .line 127
    .local v6, "metaDataContent":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 129
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 130
    :catch_0
    move-exception v1

    .line 131
    .local v1, "e":Ljava/io/IOException;
    sget-object v7, Lcom/navdy/hud/app/util/DeviceUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "Error closing the FileInputStream"

    invoke-virtual {v7, v8, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 123
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .end local v6    # "metaDataContent":Ljava/lang/String;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    :catch_1
    move-exception v1

    .line 124
    .local v1, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    sget-object v7, Lcom/navdy/hud/app/util/DeviceUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "Error reading the Meta data,"

    invoke-virtual {v7, v8, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 127
    if-eqz v2, :cond_0

    .line 129
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 130
    :catch_2
    move-exception v1

    .line 131
    .local v1, "e":Ljava/io/IOException;
    sget-object v7, Lcom/navdy/hud/app/util/DeviceUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "Error closing the FileInputStream"

    invoke-virtual {v7, v8, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 127
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    :goto_2
    if-eqz v2, :cond_3

    .line 129
    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 132
    :cond_3
    :goto_3
    throw v7

    .line 130
    :catch_3
    move-exception v1

    .line 131
    .restart local v1    # "e":Ljava/io/IOException;
    sget-object v8, Lcom/navdy/hud/app/util/DeviceUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "Error closing the FileInputStream"

    invoke-virtual {v8, v9, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 127
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v7

    move-object v2, v3

    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    goto :goto_2

    .line 123
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    :catch_4
    move-exception v1

    move-object v2, v3

    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    goto :goto_1
.end method

.method public static isNavdyDevice()Z
    .locals 1

    .prologue
    .line 46
    sget-boolean v0, Lcom/navdy/hud/app/util/DeviceUtil;->hudDevice:Z

    return v0
.end method

.method public static isUserBuild()Z
    .locals 2

    .prologue
    .line 83
    const-string v0, "user"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static supportsCamera()Z
    .locals 6

    .prologue
    .line 52
    sget-object v3, Lcom/navdy/hud/app/util/DeviceUtil;->hasCamera:Ljava/lang/Boolean;

    if-nez v3, :cond_0

    .line 53
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v1

    .line 54
    .local v1, "cameras":I
    sget-object v3, Lcom/navdy/hud/app/util/DeviceUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Found "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " cameras"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 55
    const/4 v0, 0x0

    .line 57
    .local v0, "camera":Landroid/hardware/Camera;
    const/4 v3, 0x0

    :try_start_0
    invoke-static {v3}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    move-result-object v0

    .line 58
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    sput-object v3, Lcom/navdy/hud/app/util/DeviceUtil;->hasCamera:Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63
    if-eqz v0, :cond_0

    .line 64
    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    .line 68
    :cond_0
    :goto_0
    sget-object v3, Lcom/navdy/hud/app/util/DeviceUtil;->hasCamera:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    return v3

    .line 59
    :catch_0
    move-exception v2

    .line 60
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v3, Lcom/navdy/hud/app/util/DeviceUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Failed to open camera"

    invoke-virtual {v3, v4, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 61
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    sput-object v3, Lcom/navdy/hud/app/util/DeviceUtil;->hasCamera:Ljava/lang/Boolean;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 63
    if-eqz v0, :cond_0

    .line 64
    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    goto :goto_0

    .line 63
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v0, :cond_1

    .line 64
    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    :cond_1
    throw v3
.end method

.method public static takeDeviceScreenShot(Ljava/lang/String;)V
    .locals 5
    .param p0, "absolutePath"    # Ljava/lang/String;

    .prologue
    .line 73
    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "screencap -p "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v1

    .line 74
    .local v1, "process":Ljava/lang/Process;
    invoke-virtual {v1}, Ljava/lang/Process;->waitFor()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 80
    .end local v1    # "process":Ljava/lang/Process;
    :goto_0
    return-void

    .line 75
    :catch_0
    move-exception v0

    .line 76
    .local v0, "e":Ljava/io/IOException;
    sget-object v2, Lcom/navdy/hud/app/util/DeviceUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Error while taking screen shot"

    invoke-virtual {v2, v3, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 77
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 78
    .local v0, "e":Ljava/lang/InterruptedException;
    sget-object v2, Lcom/navdy/hud/app/util/DeviceUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Error while taking screen shot"

    invoke-virtual {v2, v3, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
