.class Lcom/navdy/hud/app/util/CrashReporter$4;
.super Ljava/lang/Object;
.source "CrashReporter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/util/CrashReporter;->checkForKernelCrashes([Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/util/CrashReporter;

.field final synthetic val$kernelCrashFiles:[Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/util/CrashReporter;[Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/util/CrashReporter;

    .prologue
    .line 350
    iput-object p1, p0, Lcom/navdy/hud/app/util/CrashReporter$4;->this$0:Lcom/navdy/hud/app/util/CrashReporter;

    iput-object p2, p0, Lcom/navdy/hud/app/util/CrashReporter$4;->val$kernelCrashFiles:[Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 354
    :try_start_0
    # getter for: Lcom/navdy/hud/app/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/CrashReporter;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    const-string v6, "checking for kernel crashes"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 355
    const/4 v1, 0x0

    .line 356
    .local v1, "file":Ljava/io/File;
    iget-object v6, p0, Lcom/navdy/hud/app/util/CrashReporter$4;->val$kernelCrashFiles:[Ljava/lang/String;

    array-length v7, v6

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v7, :cond_0

    aget-object v3, v6, v5

    .line 357
    .local v3, "str":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 358
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 359
    move-object v1, v0

    .line 360
    # getter for: Lcom/navdy/hud/app/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/CrashReporter;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "found file:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 364
    .end local v0    # "f":Ljava/io/File;
    .end local v3    # "str":Ljava/lang/String;
    :cond_0
    if-eqz v1, :cond_3

    .line 365
    iget-object v5, p0, Lcom/navdy/hud/app/util/CrashReporter$4;->this$0:Lcom/navdy/hud/app/util/CrashReporter;

    # invokes: Lcom/navdy/hud/app/util/CrashReporter;->filterKernelCrash(Ljava/io/File;)Ljava/lang/String;
    invoke-static {v5, v1}, Lcom/navdy/hud/app/util/CrashReporter;->access$900(Lcom/navdy/hud/app/util/CrashReporter;Ljava/io/File;)Ljava/lang/String;

    move-result-object v3

    .line 367
    .restart local v3    # "str":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 368
    new-instance v2, Lcom/navdy/hud/app/util/CrashReporter$KernelCrashException;

    invoke-direct {v2, v3}, Lcom/navdy/hud/app/util/CrashReporter$KernelCrashException;-><init>(Ljava/lang/String;)V

    .line 369
    .local v2, "kernelCrashException":Lcom/navdy/hud/app/util/CrashReporter$KernelCrashException;
    iget-object v5, p0, Lcom/navdy/hud/app/util/CrashReporter$4;->this$0:Lcom/navdy/hud/app/util/CrashReporter;

    invoke-virtual {v5, v2}, Lcom/navdy/hud/app/util/CrashReporter;->reportNonFatalException(Ljava/lang/Throwable;)V

    .line 370
    # getter for: Lcom/navdy/hud/app/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/CrashReporter;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    const-string v6, "kernel crash created"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 373
    .end local v2    # "kernelCrashException":Lcom/navdy/hud/app/util/CrashReporter$KernelCrashException;
    :cond_1
    iget-object v5, p0, Lcom/navdy/hud/app/util/CrashReporter$4;->this$0:Lcom/navdy/hud/app/util/CrashReporter;

    iget-object v6, p0, Lcom/navdy/hud/app/util/CrashReporter$4;->val$kernelCrashFiles:[Ljava/lang/String;

    # invokes: Lcom/navdy/hud/app/util/CrashReporter;->cleanupKernelCrashes([Ljava/lang/String;)V
    invoke-static {v5, v6}, Lcom/navdy/hud/app/util/CrashReporter;->access$1000(Lcom/navdy/hud/app/util/CrashReporter;[Ljava/lang/String;)V

    .line 380
    .end local v1    # "file":Ljava/io/File;
    .end local v3    # "str":Ljava/lang/String;
    :goto_1
    return-void

    .line 356
    .restart local v0    # "f":Ljava/io/File;
    .restart local v1    # "file":Ljava/io/File;
    .restart local v3    # "str":Ljava/lang/String;
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 375
    .end local v0    # "f":Ljava/io/File;
    .end local v3    # "str":Ljava/lang/String;
    :cond_3
    # getter for: Lcom/navdy/hud/app/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/CrashReporter;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    const-string v6, "no kernel crash files"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 377
    .end local v1    # "file":Ljava/io/File;
    :catch_0
    move-exception v4

    .line 378
    .local v4, "t":Ljava/lang/Throwable;
    # getter for: Lcom/navdy/hud/app/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/CrashReporter;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_1
.end method
