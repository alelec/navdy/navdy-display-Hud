.class public Lcom/navdy/hud/app/util/RemoteCapabilitiesUtil;
.super Ljava/lang/Object;
.source "RemoteCapabilitiesUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getRemoteCapabilities()Lcom/navdy/service/library/events/Capabilities;
    .locals 2

    .prologue
    .line 36
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getRemoteDeviceInfo()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v0

    .line 37
    .local v0, "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/navdy/service/library/events/DeviceInfo;->capabilities:Lcom/navdy/service/library/events/Capabilities;

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static supportsNavigationCoordinateLookup()Z
    .locals 3

    .prologue
    .line 41
    invoke-static {}, Lcom/navdy/hud/app/util/RemoteCapabilitiesUtil;->getRemoteCapabilities()Lcom/navdy/service/library/events/Capabilities;

    move-result-object v0

    .line 43
    .local v0, "capabilities":Lcom/navdy/service/library/events/Capabilities;
    if-eqz v0, :cond_0

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v2, v0, Lcom/navdy/service/library/events/Capabilities;->navCoordsLookup:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static supportsPlaceSearch()Z
    .locals 3

    .prologue
    .line 26
    invoke-static {}, Lcom/navdy/hud/app/util/RemoteCapabilitiesUtil;->getRemoteCapabilities()Lcom/navdy/service/library/events/Capabilities;

    move-result-object v0

    .line 27
    .local v0, "capabilities":Lcom/navdy/service/library/events/Capabilities;
    if-eqz v0, :cond_0

    .line 28
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v2, v0, Lcom/navdy/service/library/events/Capabilities;->placeTypeSearch:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 31
    :goto_0
    return v1

    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v1

    sget-object v2, Lcom/navdy/service/library/events/LegacyCapability;->CAPABILITY_PLACE_TYPE_SEARCH:Lcom/navdy/service/library/events/LegacyCapability;

    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->doesRemoteDeviceHasCapability(Lcom/navdy/service/library/events/LegacyCapability;)Z

    move-result v1

    goto :goto_0
.end method

.method public static supportsVoiceSearch()Z
    .locals 3

    .prologue
    .line 16
    invoke-static {}, Lcom/navdy/hud/app/util/RemoteCapabilitiesUtil;->getRemoteCapabilities()Lcom/navdy/service/library/events/Capabilities;

    move-result-object v0

    .line 17
    .local v0, "capabilities":Lcom/navdy/service/library/events/Capabilities;
    if-eqz v0, :cond_0

    .line 18
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v2, v0, Lcom/navdy/service/library/events/Capabilities;->voiceSearch:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 21
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static supportsVoiceSearchNewIOSPauseBehaviors()Z
    .locals 3

    .prologue
    .line 47
    invoke-static {}, Lcom/navdy/hud/app/util/RemoteCapabilitiesUtil;->getRemoteCapabilities()Lcom/navdy/service/library/events/Capabilities;

    move-result-object v0

    .line 48
    .local v0, "capabilities":Lcom/navdy/service/library/events/Capabilities;
    if-eqz v0, :cond_0

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v2, v0, Lcom/navdy/service/library/events/Capabilities;->voiceSearchNewIOSPauseBehaviors:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
