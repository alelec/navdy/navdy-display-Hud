.class public Lcom/navdy/hud/app/util/CrashReporter$NavdyCrashListener;
.super Lnet/hockeyapp/android/CrashManagerListener;
.source "CrashReporter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/util/CrashReporter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NavdyCrashListener"
.end annotation


# instance fields
.field public exception:Ljava/lang/Throwable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 179
    invoke-direct {p0}, Lnet/hockeyapp/android/CrashManagerListener;-><init>()V

    .line 180
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/util/CrashReporter$NavdyCrashListener;->exception:Ljava/lang/Throwable;

    return-void
.end method


# virtual methods
.method public getContact()Ljava/lang/String;
    .locals 1

    .prologue
    .line 194
    # getter for: Lcom/navdy/hud/app/util/CrashReporter;->driverProfileManager:Lcom/navdy/hud/app/profile/DriverProfileManager;
    invoke-static {}, Lcom/navdy/hud/app/util/CrashReporter;->access$100()Lcom/navdy/hud/app/profile/DriverProfileManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getLastUserEmail()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 4

    .prologue
    const v3, 0x8000

    .line 199
    iget-object v2, p0, Lcom/navdy/hud/app/util/CrashReporter$NavdyCrashListener;->exception:Ljava/lang/Throwable;

    instance-of v2, v2, Lcom/navdy/hud/app/util/CrashReporter$TombstoneException;

    if-eqz v2, :cond_0

    .line 200
    iget-object v1, p0, Lcom/navdy/hud/app/util/CrashReporter$NavdyCrashListener;->exception:Ljava/lang/Throwable;

    check-cast v1, Lcom/navdy/hud/app/util/CrashReporter$TombstoneException;

    .line 201
    .local v1, "tombstone":Lcom/navdy/hud/app/util/CrashReporter$TombstoneException;
    iget-object v2, v1, Lcom/navdy/hud/app/util/CrashReporter$TombstoneException;->description:Ljava/lang/String;

    .line 210
    .end local v1    # "tombstone":Lcom/navdy/hud/app/util/CrashReporter$TombstoneException;
    :goto_0
    return-object v2

    .line 202
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/util/CrashReporter$NavdyCrashListener;->exception:Ljava/lang/Throwable;

    instance-of v2, v2, Lcom/navdy/hud/app/util/CrashReporter$AnrException;

    if-eqz v2, :cond_1

    .line 203
    const-string v2, "Wrote stack traces to \'/data/anr/traces.txt\'"

    invoke-static {v3, v2}, Lcom/navdy/service/library/util/LogUtils;->systemLogStr(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 204
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/util/CrashReporter$NavdyCrashListener;->exception:Ljava/lang/Throwable;

    instance-of v2, v2, Lcom/navdy/hud/app/util/CrashReporter$KernelCrashException;

    if-eqz v2, :cond_2

    .line 205
    const-string v2, "--------- beginning of main"

    invoke-static {v3, v2}, Lcom/navdy/service/library/util/LogUtils;->systemLogStr(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 206
    :cond_2
    iget-object v2, p0, Lcom/navdy/hud/app/util/CrashReporter$NavdyCrashListener;->exception:Ljava/lang/Throwable;

    instance-of v2, v2, Lcom/navdy/hud/app/util/OTAUpdateService$OTAFailedException;

    if-eqz v2, :cond_3

    .line 207
    iget-object v0, p0, Lcom/navdy/hud/app/util/CrashReporter$NavdyCrashListener;->exception:Ljava/lang/Throwable;

    check-cast v0, Lcom/navdy/hud/app/util/OTAUpdateService$OTAFailedException;

    .line 208
    .local v0, "e":Lcom/navdy/hud/app/util/OTAUpdateService$OTAFailedException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, v0, Lcom/navdy/hud/app/util/OTAUpdateService$OTAFailedException;->last_install:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/navdy/hud/app/util/OTAUpdateService$OTAFailedException;->last_log:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 210
    .end local v0    # "e":Lcom/navdy/hud/app/util/OTAUpdateService$OTAFailedException;
    :cond_3
    const/4 v2, 0x0

    invoke-static {v3, v2}, Lcom/navdy/service/library/util/LogUtils;->systemLogStr(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public getUserID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 189
    # getter for: Lcom/navdy/hud/app/util/CrashReporter;->userId:Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/util/CrashReporter;->access$000()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public saveException(Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 216
    iput-object p1, p0, Lcom/navdy/hud/app/util/CrashReporter$NavdyCrashListener;->exception:Ljava/lang/Throwable;

    .line 217
    invoke-static {p1, p0}, Lnet/hockeyapp/android/ExceptionHandler;->saveException(Ljava/lang/Throwable;Lnet/hockeyapp/android/CrashManagerListener;)V

    .line 218
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/hud/app/util/CrashReporter$NavdyCrashListener;->exception:Ljava/lang/Throwable;

    .line 219
    return-void
.end method

.method public shouldAutoUploadCrashes()Z
    .locals 1

    .prologue
    .line 184
    const/4 v0, 0x1

    return v0
.end method
