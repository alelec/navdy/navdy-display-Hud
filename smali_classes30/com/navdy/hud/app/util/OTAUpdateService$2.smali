.class Lcom/navdy/hud/app/util/OTAUpdateService$2;
.super Ljava/lang/Object;
.source "OTAUpdateService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/util/OTAUpdateService;->checkForUpdate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/util/OTAUpdateService;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/util/OTAUpdateService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/util/OTAUpdateService;

    .prologue
    .line 232
    iput-object p1, p0, Lcom/navdy/hud/app/util/OTAUpdateService$2;->this$0:Lcom/navdy/hud/app/util/OTAUpdateService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 235
    # getter for: Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/OTAUpdateService;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "Checking the update"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 236
    iget-object v2, p0, Lcom/navdy/hud/app/util/OTAUpdateService$2;->this$0:Lcom/navdy/hud/app/util/OTAUpdateService;

    iget-object v3, p0, Lcom/navdy/hud/app/util/OTAUpdateService$2;->this$0:Lcom/navdy/hud/app/util/OTAUpdateService;

    iget-object v3, v3, Lcom/navdy/hud/app/util/OTAUpdateService;->sharedPreferences:Landroid/content/SharedPreferences;

    # invokes: Lcom/navdy/hud/app/util/OTAUpdateService;->checkUpdateFile(Landroid/content/Context;Landroid/content/SharedPreferences;)Ljava/io/File;
    invoke-static {v2, v3}, Lcom/navdy/hud/app/util/OTAUpdateService;->access$100(Landroid/content/Context;Landroid/content/SharedPreferences;)Ljava/io/File;

    move-result-object v1

    .line 237
    .local v1, "file":Ljava/io/File;
    if-nez v1, :cond_0

    .line 238
    # getter for: Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/OTAUpdateService;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "No update file found"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 239
    iget-object v2, p0, Lcom/navdy/hud/app/util/OTAUpdateService$2;->this$0:Lcom/navdy/hud/app/util/OTAUpdateService;

    const/4 v3, 0x0

    # invokes: Lcom/navdy/hud/app/util/OTAUpdateService;->checkIfIncrementalUpdatesFailed(Ljava/io/File;)Z
    invoke-static {v2, v3}, Lcom/navdy/hud/app/util/OTAUpdateService;->access$400(Lcom/navdy/hud/app/util/OTAUpdateService;Ljava/io/File;)Z

    .line 240
    iget-object v2, p0, Lcom/navdy/hud/app/util/OTAUpdateService$2;->this$0:Lcom/navdy/hud/app/util/OTAUpdateService;

    iget-object v3, p0, Lcom/navdy/hud/app/util/OTAUpdateService$2;->this$0:Lcom/navdy/hud/app/util/OTAUpdateService;

    iget-object v3, v3, Lcom/navdy/hud/app/util/OTAUpdateService;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-static {v1, v2, v3}, Lcom/navdy/hud/app/util/OTAUpdateService;->clearUpdate(Ljava/io/File;Landroid/content/Context;Landroid/content/SharedPreferences;)V

    .line 261
    :goto_0
    return-void

    .line 243
    :cond_0
    iget-object v2, p0, Lcom/navdy/hud/app/util/OTAUpdateService$2;->this$0:Lcom/navdy/hud/app/util/OTAUpdateService;

    # invokes: Lcom/navdy/hud/app/util/OTAUpdateService;->checkIfIncrementalUpdatesFailed(Ljava/io/File;)Z
    invoke-static {v2, v1}, Lcom/navdy/hud/app/util/OTAUpdateService;->access$400(Lcom/navdy/hud/app/util/OTAUpdateService;Ljava/io/File;)Z

    move-result v0

    .line 244
    .local v0, "currentUpdateValid":Z
    # getter for: Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/OTAUpdateService;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Current update valid ? :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 245
    if-eqz v0, :cond_3

    .line 247
    iget-object v2, p0, Lcom/navdy/hud/app/util/OTAUpdateService$2;->this$0:Lcom/navdy/hud/app/util/OTAUpdateService;

    iget-object v2, v2, Lcom/navdy/hud/app/util/OTAUpdateService;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-static {v2}, Lcom/navdy/hud/app/util/OTAUpdateService;->isOTAUpdateVerified(Landroid/content/SharedPreferences;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 248
    iget-object v2, p0, Lcom/navdy/hud/app/util/OTAUpdateService$2;->this$0:Lcom/navdy/hud/app/util/OTAUpdateService;

    iget-object v3, p0, Lcom/navdy/hud/app/util/OTAUpdateService$2;->this$0:Lcom/navdy/hud/app/util/OTAUpdateService;

    iget-object v3, v3, Lcom/navdy/hud/app/util/OTAUpdateService;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-virtual {v2, v1, v3}, Lcom/navdy/hud/app/util/OTAUpdateService;->bVerifyUpdate(Ljava/io/File;Landroid/content/SharedPreferences;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 249
    iget-object v2, p0, Lcom/navdy/hud/app/util/OTAUpdateService$2;->this$0:Lcom/navdy/hud/app/util/OTAUpdateService;

    iget-object v3, p0, Lcom/navdy/hud/app/util/OTAUpdateService$2;->this$0:Lcom/navdy/hud/app/util/OTAUpdateService;

    iget-object v3, v3, Lcom/navdy/hud/app/util/OTAUpdateService;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-static {v1, v2, v3}, Lcom/navdy/hud/app/util/OTAUpdateService;->clearUpdate(Ljava/io/File;Landroid/content/Context;Landroid/content/SharedPreferences;)V

    goto :goto_0

    .line 252
    :cond_1
    iget-object v2, p0, Lcom/navdy/hud/app/util/OTAUpdateService$2;->this$0:Lcom/navdy/hud/app/util/OTAUpdateService;

    iget-object v2, v2, Lcom/navdy/hud/app/util/OTAUpdateService;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "verified"

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 253
    iget-object v2, p0, Lcom/navdy/hud/app/util/OTAUpdateService$2;->this$0:Lcom/navdy/hud/app/util/OTAUpdateService;

    iget-object v2, v2, Lcom/navdy/hud/app/util/OTAUpdateService;->bus:Lcom/squareup/otto/Bus;

    new-instance v3, Lcom/navdy/hud/app/util/OTAUpdateService$UpdateVerified;

    invoke-direct {v3}, Lcom/navdy/hud/app/util/OTAUpdateService$UpdateVerified;-><init>()V

    invoke-virtual {v2, v3}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 256
    :cond_2
    iget-object v2, p0, Lcom/navdy/hud/app/util/OTAUpdateService$2;->this$0:Lcom/navdy/hud/app/util/OTAUpdateService;

    iget-object v2, v2, Lcom/navdy/hud/app/util/OTAUpdateService;->sharedPreferences:Landroid/content/SharedPreferences;

    # invokes: Lcom/navdy/hud/app/util/OTAUpdateService;->getUpdateVersion(Landroid/content/SharedPreferences;)Ljava/lang/String;
    invoke-static {v2}, Lcom/navdy/hud/app/util/OTAUpdateService;->access$600(Landroid/content/SharedPreferences;)Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/navdy/hud/app/util/OTAUpdateService;->swVersion:Ljava/lang/String;
    invoke-static {v2}, Lcom/navdy/hud/app/util/OTAUpdateService;->access$502(Ljava/lang/String;)Ljava/lang/String;

    .line 257
    # getter for: Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/OTAUpdateService;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "verified swVersion:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    # getter for: Lcom/navdy/hud/app/util/OTAUpdateService;->swVersion:Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/util/OTAUpdateService;->access$500()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 259
    :cond_3
    iget-object v2, p0, Lcom/navdy/hud/app/util/OTAUpdateService$2;->this$0:Lcom/navdy/hud/app/util/OTAUpdateService;

    iget-object v3, p0, Lcom/navdy/hud/app/util/OTAUpdateService$2;->this$0:Lcom/navdy/hud/app/util/OTAUpdateService;

    iget-object v3, v3, Lcom/navdy/hud/app/util/OTAUpdateService;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-static {v1, v2, v3}, Lcom/navdy/hud/app/util/OTAUpdateService;->clearUpdate(Ljava/io/File;Landroid/content/Context;Landroid/content/SharedPreferences;)V

    goto/16 :goto_0
.end method
