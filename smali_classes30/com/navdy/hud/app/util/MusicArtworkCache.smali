.class public Lcom/navdy/hud/app/util/MusicArtworkCache;
.super Ljava/lang/Object;
.source "MusicArtworkCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;,
        Lcom/navdy/hud/app/util/MusicArtworkCache$Callback;
    }
.end annotation


# static fields
.field private static final SEPARATOR:Ljava/lang/String; = "_"

.field private static collectionTypeMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCollectionType;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private final dbLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 39
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/util/MusicArtworkCache;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/util/MusicArtworkCache;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 49
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/navdy/hud/app/util/MusicArtworkCache;->collectionTypeMap:Ljava/util/Map;

    .line 52
    sget-object v0, Lcom/navdy/hud/app/util/MusicArtworkCache;->collectionTypeMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_PLAYLISTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    const-string v2, "playlist"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    sget-object v0, Lcom/navdy/hud/app/util/MusicArtworkCache;->collectionTypeMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_ARTISTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    const-string v2, "music"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    sget-object v0, Lcom/navdy/hud/app/util/MusicArtworkCache;->collectionTypeMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_ALBUMS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    const-string v2, "music"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    sget-object v0, Lcom/navdy/hud/app/util/MusicArtworkCache;->collectionTypeMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_PODCASTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    const-string v2, "podcast"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    sget-object v0, Lcom/navdy/hud/app/util/MusicArtworkCache;->collectionTypeMap:Ljava/util/Map;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_AUDIOBOOKS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    const-string v2, "audiobook"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/navdy/hud/app/util/MusicArtworkCache;->dbLock:Ljava/lang/Object;

    return-void
.end method

.method static synthetic access$000()Ljava/util/Map;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/navdy/hud/app/util/MusicArtworkCache;->collectionTypeMap:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$100()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/navdy/hud/app/util/MusicArtworkCache;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/util/MusicArtworkCache;Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/util/MusicArtworkCache;
    .param p1, "x1"    # Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/util/MusicArtworkCache;->createDbEntry(Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;)V

    return-void
.end method

.method static synthetic access$400(Lcom/navdy/hud/app/util/MusicArtworkCache;Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/util/MusicArtworkCache;
    .param p1, "x1"    # Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/util/MusicArtworkCache;->getFileNameFromDb(Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private createDbEntry(Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;)V
    .locals 10
    .param p1, "cacheEntryHelper"    # Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;

    .prologue
    .line 166
    sget-object v4, Lcom/navdy/hud/app/util/MusicArtworkCache;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "createDbEntry: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 167
    iget-object v4, p1, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->author:Ljava/lang/String;

    if-nez v4, :cond_0

    iget-object v4, p1, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->album:Ljava/lang/String;

    if-nez v4, :cond_0

    iget-object v4, p1, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->name:Ljava/lang/String;

    if-nez v4, :cond_0

    .line 168
    sget-object v4, Lcom/navdy/hud/app/util/MusicArtworkCache;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Not cacheable"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 195
    :goto_0
    return-void

    .line 171
    :cond_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 172
    .local v1, "values":Landroid/content/ContentValues;
    const-string v4, "type"

    iget-object v5, p1, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->type:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    iget-object v4, p1, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->author:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 174
    const-string v4, "author"

    iget-object v5, p1, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->author:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    :cond_1
    iget-object v4, p1, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->album:Ljava/lang/String;

    if-eqz v4, :cond_2

    .line 177
    const-string v4, "album"

    iget-object v5, p1, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->album:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    :cond_2
    iget-object v4, p1, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->author:Ljava/lang/String;

    if-eqz v4, :cond_3

    .line 180
    const-string v4, "name"

    iget-object v5, p1, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->name:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    :cond_3
    const-string v4, "file_name"

    # invokes: Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->getFileName()Ljava/lang/String;
    invoke-static {p1}, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->access$200(Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    iget-object v5, p0, Lcom/navdy/hud/app/util/MusicArtworkCache;->dbLock:Ljava/lang/Object;

    monitor-enter v5

    .line 185
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/storage/db/HudDatabase;->getInstance()Lcom/navdy/hud/app/storage/db/HudDatabase;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/hud/app/storage/db/HudDatabase;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 186
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v0, :cond_4

    .line 187
    sget-object v4, Lcom/navdy/hud/app/util/MusicArtworkCache;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Couldn\'t get db"

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 188
    monitor-exit v5

    goto :goto_0

    .line 194
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 190
    .restart local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :cond_4
    :try_start_1
    const-string v4, "music_artwork_cache"

    const/4 v6, 0x0

    const/4 v7, 0x4

    invoke-virtual {v0, v4, v6, v1, v7}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move-result-wide v2

    .line 191
    .local v2, "rowId":J
    const-wide/16 v6, -0x1

    cmp-long v4, v2, v6

    if-nez v4, :cond_5

    .line 192
    const-string v4, "music_artwork_cache"

    const-string v6, "rowid = ?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v0, v4, v1, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 194
    :cond_5
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private getArtworkInternal(Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;Lcom/navdy/hud/app/util/MusicArtworkCache$Callback;)V
    .locals 3
    .param p1, "cacheEntryHelper"    # Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "callback"    # Lcom/navdy/hud/app/util/MusicArtworkCache$Callback;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 209
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/util/MusicArtworkCache$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/navdy/hud/app/util/MusicArtworkCache$2;-><init>(Lcom/navdy/hud/app/util/MusicArtworkCache;Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;Lcom/navdy/hud/app/util/MusicArtworkCache$Callback;)V

    const/16 v2, 0x16

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 232
    return-void
.end method

.method private getFileNameFromDb(Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;)Ljava/lang/String;
    .locals 14
    .param p1, "cacheEntryHelper"    # Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v12, 0x0

    .line 235
    sget-object v1, Lcom/navdy/hud/app/util/MusicArtworkCache;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getFileNameFromDb: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 236
    const/4 v10, 0x0

    .line 238
    .local v10, "cursor":Landroid/database/Cursor;
    const/4 v1, 0x1

    :try_start_0
    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "file_name"

    aput-object v3, v2, v1

    .line 239
    .local v2, "columns":[Ljava/lang/String;
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 240
    .local v9, "args":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v1, "type = ?"

    invoke-direct {v11, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 241
    .local v11, "selection":Ljava/lang/StringBuilder;
    iget-object v1, p1, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->type:Ljava/lang/String;

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 242
    iget-object v1, p1, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->author:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 243
    const-string v1, " AND author = ?"

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 244
    iget-object v1, p1, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->author:Ljava/lang/String;

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 246
    :cond_0
    iget-object v1, p1, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->album:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 247
    const-string v1, " AND album = ?"

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 248
    iget-object v1, p1, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->album:Ljava/lang/String;

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 250
    :cond_1
    iget-object v1, p1, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->type:Ljava/lang/String;

    const-string v3, "music"

    invoke-static {v1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p1, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->name:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p1, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->album:Ljava/lang/String;

    if-nez v1, :cond_3

    .line 252
    :cond_2
    const-string v1, " AND name = ?"

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 253
    iget-object v1, p1, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->name:Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, p1, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->name:Ljava/lang/String;

    :goto_0
    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 256
    :cond_3
    iget-object v13, p0, Lcom/navdy/hud/app/util/MusicArtworkCache;->dbLock:Ljava/lang/Object;

    monitor-enter v13
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 257
    :try_start_1
    invoke-static {}, Lcom/navdy/hud/app/storage/db/HudDatabase;->getInstance()Lcom/navdy/hud/app/storage/db/HudDatabase;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/storage/db/HudDatabase;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 258
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v0, :cond_5

    .line 259
    sget-object v1, Lcom/navdy/hud/app/util/MusicArtworkCache;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Couldn\'t get db"

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 260
    monitor-exit v13
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 268
    invoke-static {v10}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    move-object v1, v12

    .line 270
    :goto_1
    return-object v1

    .line 253
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :cond_4
    :try_start_2
    const-string v1, ""
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 262
    .restart local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :cond_5
    :try_start_3
    const-string v1, "music_artwork_cache"

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v8, "1"

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 263
    if-eqz v10, :cond_6

    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 264
    const-string v1, "file_name"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    monitor-exit v13
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 268
    invoke-static {v10}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    goto :goto_1

    .line 266
    :cond_6
    :try_start_4
    monitor-exit v13
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 268
    invoke-static {v10}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    move-object v1, v12

    .line 270
    goto :goto_1

    .line 266
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_0
    move-exception v1

    :try_start_5
    monitor-exit v13
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 268
    .end local v2    # "columns":[Ljava/lang/String;
    .end local v9    # "args":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v11    # "selection":Ljava/lang/StringBuilder;
    :catchall_1
    move-exception v1

    invoke-static {v10}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    throw v1
.end method

.method private putArtworkInternal(Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;[B)V
    .locals 3
    .param p1, "cacheEntryHelper"    # Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;
    .param p2, "data"    # [B

    .prologue
    .line 145
    # invokes: Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->getFileName()Ljava/lang/String;
    invoke-static {p1}, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->access$200(Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 146
    sget-object v0, Lcom/navdy/hud/app/util/MusicArtworkCache;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Couldn\'t generate a filename for: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 163
    :goto_0
    return-void

    .line 150
    :cond_0
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/hud/app/util/MusicArtworkCache$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/navdy/hud/app/util/MusicArtworkCache$1;-><init>(Lcom/navdy/hud/app/util/MusicArtworkCache;Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;[B)V

    const/16 v2, 0x16

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method


# virtual methods
.method public getArtwork(Lcom/navdy/service/library/events/audio/MusicCollectionInfo;Lcom/navdy/hud/app/util/MusicArtworkCache$Callback;)V
    .locals 1
    .param p1, "musicCollectionInfo"    # Lcom/navdy/service/library/events/audio/MusicCollectionInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "callback"    # Lcom/navdy/hud/app/util/MusicArtworkCache$Callback;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 204
    new-instance v0, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;

    invoke-direct {v0, p0, p1}, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;-><init>(Lcom/navdy/hud/app/util/MusicArtworkCache;Lcom/navdy/service/library/events/audio/MusicCollectionInfo;)V

    .line 205
    .local v0, "cacheEntryHelper":Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;
    invoke-direct {p0, v0, p2}, Lcom/navdy/hud/app/util/MusicArtworkCache;->getArtworkInternal(Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;Lcom/navdy/hud/app/util/MusicArtworkCache$Callback;)V

    .line 206
    return-void
.end method

.method public getArtwork(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/hud/app/util/MusicArtworkCache$Callback;)V
    .locals 1
    .param p1, "artist"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "album"    # Ljava/lang/String;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "callback"    # Lcom/navdy/hud/app/util/MusicArtworkCache$Callback;

    .prologue
    .line 198
    new-instance v0, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;-><init>(Lcom/navdy/hud/app/util/MusicArtworkCache;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    .local v0, "cacheEntryHelper":Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;
    invoke-direct {p0, v0, p4}, Lcom/navdy/hud/app/util/MusicArtworkCache;->getArtworkInternal(Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;Lcom/navdy/hud/app/util/MusicArtworkCache$Callback;)V

    .line 200
    return-void
.end method

.method public putArtwork(Lcom/navdy/service/library/events/audio/MusicCollectionInfo;[B)V
    .locals 1
    .param p1, "collectionInfo"    # Lcom/navdy/service/library/events/audio/MusicCollectionInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "artwork"    # [B

    .prologue
    .line 140
    new-instance v0, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;

    invoke-direct {v0, p0, p1}, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;-><init>(Lcom/navdy/hud/app/util/MusicArtworkCache;Lcom/navdy/service/library/events/audio/MusicCollectionInfo;)V

    .line 141
    .local v0, "cacheEntryHelper":Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;
    invoke-direct {p0, v0, p2}, Lcom/navdy/hud/app/util/MusicArtworkCache;->putArtworkInternal(Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;[B)V

    .line 142
    return-void
.end method

.method public putArtwork(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 1
    .param p1, "author"    # Ljava/lang/String;
    .param p2, "album"    # Ljava/lang/String;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "artwork"    # [B

    .prologue
    .line 135
    new-instance v0, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;-><init>(Lcom/navdy/hud/app/util/MusicArtworkCache;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    .local v0, "cacheEntryHelper":Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;
    invoke-direct {p0, v0, p4}, Lcom/navdy/hud/app/util/MusicArtworkCache;->putArtworkInternal(Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;[B)V

    .line 137
    return-void
.end method
