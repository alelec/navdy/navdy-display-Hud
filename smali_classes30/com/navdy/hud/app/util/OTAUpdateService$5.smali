.class Lcom/navdy/hud/app/util/OTAUpdateService$5;
.super Ljava/lang/Object;
.source "OTAUpdateService.java"

# interfaces
.implements Landroid/os/RecoverySystem$ProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/util/OTAUpdateService;->bVerifyUpdate(Ljava/io/File;Landroid/content/SharedPreferences;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/util/OTAUpdateService;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/util/OTAUpdateService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/util/OTAUpdateService;

    .prologue
    .line 586
    iput-object p1, p0, Lcom/navdy/hud/app/util/OTAUpdateService$5;->this$0:Lcom/navdy/hud/app/util/OTAUpdateService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgress(I)V
    .locals 5
    .param p1, "progress"    # I

    .prologue
    .line 588
    # getter for: Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/OTAUpdateService;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "Verify progress: %d%% completed"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 589
    return-void
.end method
