.class Lcom/navdy/hud/app/util/ReportIssueService$2;
.super Ljava/lang/Object;
.source "ReportIssueService.java"

# interfaces
.implements Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/util/ReportIssueService;->sync()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/util/ReportIssueService;

.field final synthetic val$assigneeEmail:Ljava/lang/String;

.field final synthetic val$assigneeName:Ljava/lang/String;

.field final synthetic val$attachmentFile:Ljava/io/File;

.field final synthetic val$fileToSync:Ljava/io/File;

.field final synthetic val$jsonObject:Lorg/json/JSONObject;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/util/ReportIssueService;Lorg/json/JSONObject;Ljava/io/File;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/util/ReportIssueService;

    .prologue
    .line 527
    iput-object p1, p0, Lcom/navdy/hud/app/util/ReportIssueService$2;->this$0:Lcom/navdy/hud/app/util/ReportIssueService;

    iput-object p2, p0, Lcom/navdy/hud/app/util/ReportIssueService$2;->val$jsonObject:Lorg/json/JSONObject;

    iput-object p3, p0, Lcom/navdy/hud/app/util/ReportIssueService$2;->val$fileToSync:Ljava/io/File;

    iput-object p4, p0, Lcom/navdy/hud/app/util/ReportIssueService$2;->val$attachmentFile:Ljava/io/File;

    iput-object p5, p0, Lcom/navdy/hud/app/util/ReportIssueService$2;->val$assigneeName:Ljava/lang/String;

    iput-object p6, p0, Lcom/navdy/hud/app/util/ReportIssueService$2;->val$assigneeEmail:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/Throwable;)V
    .locals 3
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 581
    # getter for: Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/ReportIssueService;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error during sync "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 582
    iget-object v0, p0, Lcom/navdy/hud/app/util/ReportIssueService$2;->this$0:Lcom/navdy/hud/app/util/ReportIssueService;

    # invokes: Lcom/navdy/hud/app/util/ReportIssueService;->syncLater()V
    invoke-static {v0}, Lcom/navdy/hud/app/util/ReportIssueService;->access$200(Lcom/navdy/hud/app/util/ReportIssueService;)V

    .line 583
    return-void
.end method

.method public onSuccess(Ljava/lang/Object;)V
    .locals 8
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 530
    const/4 v2, 0x0

    .line 531
    .local v2, "ticketId":Ljava/lang/String;
    instance-of v5, p1, Ljava/lang/String;

    if-eqz v5, :cond_0

    move-object v2, p1

    .line 532
    check-cast v2, Ljava/lang/String;

    .line 533
    # getter for: Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/ReportIssueService;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Issue reported "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 537
    :cond_0
    :try_start_0
    iget-object v5, p0, Lcom/navdy/hud/app/util/ReportIssueService$2;->val$jsonObject:Lorg/json/JSONObject;

    const-string v6, "ticketId"

    invoke-virtual {v5, v6, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 538
    iget-object v5, p0, Lcom/navdy/hud/app/util/ReportIssueService$2;->val$jsonObject:Lorg/json/JSONObject;

    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 539
    .local v0, "data":Ljava/lang/String;
    const/4 v3, 0x0

    .line 541
    .local v3, "writer":Ljava/io/FileWriter;
    :try_start_1
    new-instance v4, Ljava/io/FileWriter;

    iget-object v5, p0, Lcom/navdy/hud/app/util/ReportIssueService$2;->val$fileToSync:Ljava/io/File;

    invoke-direct {v4, v5}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 542
    .end local v3    # "writer":Ljava/io/FileWriter;
    .local v4, "writer":Ljava/io/FileWriter;
    :try_start_2
    invoke-virtual {v4, v0}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 543
    invoke-virtual {v4}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 547
    :try_start_3
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1

    move-object v3, v4

    .line 552
    .end local v0    # "data":Ljava/lang/String;
    .end local v4    # "writer":Ljava/io/FileWriter;
    :goto_0
    # getter for: Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/ReportIssueService;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    const-string v6, "Attaching files to the submitted ticket"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 553
    iget-object v5, p0, Lcom/navdy/hud/app/util/ReportIssueService$2;->this$0:Lcom/navdy/hud/app/util/ReportIssueService;

    iget-object v6, p0, Lcom/navdy/hud/app/util/ReportIssueService$2;->val$attachmentFile:Ljava/io/File;

    new-instance v7, Lcom/navdy/hud/app/util/ReportIssueService$2$1;

    invoke-direct {v7, p0}, Lcom/navdy/hud/app/util/ReportIssueService$2$1;-><init>(Lcom/navdy/hud/app/util/ReportIssueService$2;)V

    # invokes: Lcom/navdy/hud/app/util/ReportIssueService;->attachFilesToTheJiraTicket(Ljava/lang/String;Ljava/io/File;Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;)V
    invoke-static {v5, v2, v6, v7}, Lcom/navdy/hud/app/util/ReportIssueService;->access$300(Lcom/navdy/hud/app/util/ReportIssueService;Ljava/lang/String;Ljava/io/File;Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;)V

    .line 577
    return-void

    .line 544
    .restart local v0    # "data":Ljava/lang/String;
    .restart local v3    # "writer":Ljava/io/FileWriter;
    :catch_0
    move-exception v1

    .line 545
    .local v1, "e":Ljava/io/IOException;
    :goto_1
    :try_start_4
    # getter for: Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/ReportIssueService;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Error while dumping the issue "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 547
    :try_start_5
    invoke-static {v3}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_0

    .line 549
    .end local v0    # "data":Ljava/lang/String;
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "writer":Ljava/io/FileWriter;
    :catch_1
    move-exception v1

    .line 550
    .local v1, "e":Lorg/json/JSONException;
    # getter for: Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/ReportIssueService;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    const-string v6, "JSONException while writing the meta data to the file"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 547
    .end local v1    # "e":Lorg/json/JSONException;
    .restart local v0    # "data":Ljava/lang/String;
    .restart local v3    # "writer":Ljava/io/FileWriter;
    :catchall_0
    move-exception v5

    :goto_2
    :try_start_6
    invoke-static {v3}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v5
    :try_end_6
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_6} :catch_1

    .end local v3    # "writer":Ljava/io/FileWriter;
    .restart local v4    # "writer":Ljava/io/FileWriter;
    :catchall_1
    move-exception v5

    move-object v3, v4

    .end local v4    # "writer":Ljava/io/FileWriter;
    .restart local v3    # "writer":Ljava/io/FileWriter;
    goto :goto_2

    .line 544
    .end local v3    # "writer":Ljava/io/FileWriter;
    .restart local v4    # "writer":Ljava/io/FileWriter;
    :catch_2
    move-exception v1

    move-object v3, v4

    .end local v4    # "writer":Ljava/io/FileWriter;
    .restart local v3    # "writer":Ljava/io/FileWriter;
    goto :goto_1
.end method
