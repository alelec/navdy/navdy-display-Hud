.class Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;
.super Ljava/lang/Object;
.source "MusicArtworkCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/util/MusicArtworkCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CacheEntryHelper"
.end annotation


# instance fields
.field album:Ljava/lang/String;

.field author:Ljava/lang/String;

.field fileName:Ljava/lang/String;

.field name:Ljava/lang/String;

.field final synthetic this$0:Lcom/navdy/hud/app/util/MusicArtworkCache;

.field type:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/util/MusicArtworkCache;Lcom/navdy/service/library/events/audio/MusicCollectionInfo;)V
    .locals 2
    .param p1    # Lcom/navdy/hud/app/util/MusicArtworkCache;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "collectionInfo"    # Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->this$0:Lcom/navdy/hud/app/util/MusicArtworkCache;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    # getter for: Lcom/navdy/hud/app/util/MusicArtworkCache;->collectionTypeMap:Ljava/util/Map;
    invoke-static {}, Lcom/navdy/hud/app/util/MusicArtworkCache;->access$000()Ljava/util/Map;

    move-result-object v0

    iget-object v1, p2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->type:Ljava/lang/String;

    .line 75
    sget-object v0, Lcom/navdy/hud/app/util/MusicArtworkCache$3;->$SwitchMap$com$navdy$service$library$events$audio$MusicCollectionType:[I

    iget-object v1, p2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/audio/MusicCollectionType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 95
    iget-object v0, p2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->name:Ljava/lang/String;

    .line 97
    :goto_0
    :pswitch_0
    return-void

    .line 77
    :pswitch_1
    # getter for: Lcom/navdy/hud/app/util/MusicArtworkCache;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/MusicArtworkCache;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "Unknown type"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 80
    :pswitch_2
    iget-object v0, p2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->name:Ljava/lang/String;

    goto :goto_0

    .line 83
    :pswitch_3
    iget-object v0, p2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->author:Ljava/lang/String;

    goto :goto_0

    .line 86
    :pswitch_4
    iget-object v0, p2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->album:Ljava/lang/String;

    .line 87
    iget-object v0, p2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->subtitle:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->author:Ljava/lang/String;

    goto :goto_0

    .line 90
    :pswitch_5
    iget-object v0, p2, Lcom/navdy/service/library/events/audio/MusicCollectionInfo;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->album:Ljava/lang/String;

    goto :goto_0

    .line 75
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
    .end packed-switch
.end method

.method constructor <init>(Lcom/navdy/hud/app/util/MusicArtworkCache;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p2, "author"    # Ljava/lang/String;
    .param p3, "album"    # Ljava/lang/String;
    .param p4, "name"    # Ljava/lang/String;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->this$0:Lcom/navdy/hud/app/util/MusicArtworkCache;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    const-string v0, "music"

    iput-object v0, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->type:Ljava/lang/String;

    .line 68
    iput-object p2, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->author:Ljava/lang/String;

    .line 69
    iput-object p3, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->album:Ljava/lang/String;

    .line 70
    iput-object p4, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->name:Ljava/lang/String;

    .line 71
    return-void
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->getFileName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private formatIdentifier(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 3
    .param p1, "unformatted"    # Ljava/lang/CharSequence;

    .prologue
    .line 120
    const-string v2, "[^A-Za-z0-9_]"

    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    .line 121
    .local v1, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 122
    .local v0, "matcher":Ljava/util/regex/Matcher;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 123
    const-string v2, "_"

    invoke-virtual {v0, v2}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 125
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getFileName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 100
    iget-object v1, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->fileName:Ljava/lang/String;

    if-nez v1, :cond_3

    .line 101
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->type:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 102
    .local v0, "builder":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->author:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 103
    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    iget-object v1, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->author:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->album:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 107
    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    iget-object v1, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->album:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    :cond_1
    iget-object v1, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->name:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 111
    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    iget-object v1, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    :cond_2
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->formatIdentifier(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->fileName:Ljava/lang/String;

    .line 116
    .end local v0    # "builder":Ljava/lang/StringBuilder;
    :cond_3
    iget-object v1, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->fileName:Ljava/lang/String;

    return-object v1
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 130
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->author:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->album:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
