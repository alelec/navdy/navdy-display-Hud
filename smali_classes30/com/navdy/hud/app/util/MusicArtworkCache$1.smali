.class Lcom/navdy/hud/app/util/MusicArtworkCache$1;
.super Ljava/lang/Object;
.source "MusicArtworkCache.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/util/MusicArtworkCache;->putArtworkInternal(Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;[B)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/util/MusicArtworkCache;

.field final synthetic val$cacheEntryHelper:Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;

.field final synthetic val$data:[B


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/util/MusicArtworkCache;Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;[B)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/util/MusicArtworkCache;

    .prologue
    .line 150
    iput-object p1, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$1;->this$0:Lcom/navdy/hud/app/util/MusicArtworkCache;

    iput-object p2, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$1;->val$cacheEntryHelper:Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;

    iput-object p3, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$1;->val$data:[B

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 153
    invoke-static {}, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->getDiskLruCache()Lcom/navdy/hud/app/storage/cache/DiskLruCache;

    move-result-object v0

    .line 154
    .local v0, "diskLruCache":Lcom/navdy/hud/app/storage/cache/DiskLruCache;
    if-nez v0, :cond_0

    .line 155
    # getter for: Lcom/navdy/hud/app/util/MusicArtworkCache;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/MusicArtworkCache;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "No disk cache"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 161
    :goto_0
    return-void

    .line 158
    :cond_0
    iget-object v1, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$1;->val$cacheEntryHelper:Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;

    # invokes: Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->getFileName()Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;->access$200(Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$1;->val$data:[B

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->put(Ljava/lang/String;[B)V

    .line 160
    iget-object v1, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$1;->this$0:Lcom/navdy/hud/app/util/MusicArtworkCache;

    iget-object v2, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$1;->val$cacheEntryHelper:Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;

    # invokes: Lcom/navdy/hud/app/util/MusicArtworkCache;->createDbEntry(Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;)V
    invoke-static {v1, v2}, Lcom/navdy/hud/app/util/MusicArtworkCache;->access$300(Lcom/navdy/hud/app/util/MusicArtworkCache;Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;)V

    goto :goto_0
.end method
