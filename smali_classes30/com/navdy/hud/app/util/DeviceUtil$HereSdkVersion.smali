.class final enum Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;
.super Ljava/lang/Enum;
.source "DeviceUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/util/DeviceUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "HereSdkVersion"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;

.field public static final enum SDK:Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;


# instance fields
.field private folderName:Ljava/lang/String;

.field private version:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 146
    new-instance v0, Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;

    const-string v1, "SDK"

    const-string v2, "3.3.1"

    const-string v3, "00a584764c918d799f376cc41e46674ba558f8b4"

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;->SDK:Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;

    .line 145
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;

    sget-object v1, Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;->SDK:Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;

    aput-object v1, v0, v4

    sput-object v0, Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;->$VALUES:[Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p3, "version"    # Ljava/lang/String;
    .param p4, "timestamp"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 151
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 152
    iput-object p3, p0, Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;->version:Ljava/lang/String;

    .line 153
    iput-object p4, p0, Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;->folderName:Ljava/lang/String;

    .line 154
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;

    .prologue
    .line 145
    iget-object v0, p0, Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;->version:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;

    .prologue
    .line 145
    iget-object v0, p0, Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;->folderName:Ljava/lang/String;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 145
    const-class v0, Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;
    .locals 1

    .prologue
    .line 145
    sget-object v0, Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;->$VALUES:[Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/util/DeviceUtil$HereSdkVersion;

    return-object v0
.end method
