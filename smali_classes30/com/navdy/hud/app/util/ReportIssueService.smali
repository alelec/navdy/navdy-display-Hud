.class public Lcom/navdy/hud/app/util/ReportIssueService;
.super Landroid/app/IntentService;
.source "ReportIssueService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/hud/app/util/ReportIssueService$FilesModifiedTimeComparator;,
        Lcom/navdy/hud/app/util/ReportIssueService$IssueType;
    }
.end annotation


# static fields
.field private static final ACTION_DUMP:Ljava/lang/String; = "Dump"

.field private static final ACTION_SNAPSHOT:Ljava/lang/String; = "Snapshot"

.field private static final ACTION_SUBMIT_JIRA:Ljava/lang/String; = "Jira"

.field private static final ACTION_SYNC:Ljava/lang/String; = "Sync"

.field public static final ASSIGNEE:Ljava/lang/String; = "assignee"

.field public static final ASSIGNEE_EMAIL:Ljava/lang/String; = "assigneeEmail"

.field public static final ATTACHMENT:Ljava/lang/String; = "attachment"

.field private static final DATE_FORMAT:Ljava/text/SimpleDateFormat;

.field public static final ENVIRONMENT:Ljava/lang/String; = "Environment"

.field public static final EXTRA_ISSUE_TYPE:Ljava/lang/String; = "EXTRA_ISSUE_TYPE"

.field public static final EXTRA_SNAPSHOT_TITLE:Ljava/lang/String; = "EXTRA_SNAPSHOT_TITLE"

.field private static final HUD_ISSUE_TYPE:Ljava/lang/String; = "Issue"

.field private static final HUD_PROJECT:Ljava/lang/String; = "HUD"

.field private static final ISSUE_TYPE:Ljava/lang/String; = "Task"

.field private static final JIRA_CREDENTIALS_META:Ljava/lang/String; = "JIRA_CREDENTIALS"

.field private static final MAX_FILES_OUT_STANDING:I = 0xa

.field private static final NAME:Ljava/lang/String; = "REPORT_ISSUE"

.field private static final PROJECT_NAME:Ljava/lang/String; = "NP"

.field public static final RETRY_INTERVAL:J

.field private static final SNAPSHOT_DATE_FORMAT:Ljava/text/SimpleDateFormat;

.field private static final SNAPSHOT_JIRA_TICKET_DATE_FORMAT:Ljava/text/SimpleDateFormat;

.field public static final SUMMARY:Ljava/lang/String; = "Summary"

.field public static final SYNC_REQ:I = 0x80

.field public static final TICKET_ID:Ljava/lang/String; = "ticketId"

.field private static lastSnapShotFile:Ljava/io/File;

.field private static mIsInitialized:Z

.field private static mIssuesToSync:Ljava/util/concurrent/PriorityBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/PriorityBlockingQueue",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private static mLastIssueSubmittedAt:J

.field private static mSyncing:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private static navigationIssuesFolder:Ljava/lang/String;

.field private static sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field bus:Lcom/squareup/otto/Bus;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field driveRecorder:Lcom/navdy/hud/app/debug/DriveRecorder;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field gestureService:Lcom/navdy/hud/app/gesture/GestureServiceConnector;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mHttpManager:Lcom/navdy/service/library/network/http/IHttpManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mJiraClient:Lcom/navdy/service/library/network/http/services/JiraClient;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 104
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd\'_\'HH:mm:ss.SSS"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/navdy/hud/app/util/ReportIssueService;->DATE_FORMAT:Ljava/text/SimpleDateFormat;

    .line 116
    new-instance v0, Ljava/util/concurrent/PriorityBlockingQueue;

    const/4 v1, 0x5

    new-instance v2, Lcom/navdy/hud/app/util/ReportIssueService$FilesModifiedTimeComparator;

    invoke-direct {v2}, Lcom/navdy/hud/app/util/ReportIssueService$FilesModifiedTimeComparator;-><init>()V

    invoke-direct {v0, v1, v2}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>(ILjava/util/Comparator;)V

    sput-object v0, Lcom/navdy/hud/app/util/ReportIssueService;->mIssuesToSync:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 120
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/hud/app/util/ReportIssueService;->RETRY_INTERVAL:J

    .line 121
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/navdy/hud/app/util/ReportIssueService;->mLastIssueSubmittedAt:J

    .line 122
    sput-boolean v4, Lcom/navdy/hud/app/util/ReportIssueService;->mIsInitialized:Z

    .line 123
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, Lcom/navdy/hud/app/util/ReportIssueService;->mSyncing:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 124
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/hud/app/util/ReportIssueService;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 125
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd-HH-mm-ss"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/navdy/hud/app/util/ReportIssueService;->SNAPSHOT_DATE_FORMAT:Ljava/text/SimpleDateFormat;

    .line 127
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MMM,dd hh:mm a"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/navdy/hud/app/util/ReportIssueService;->SNAPSHOT_JIRA_TICKET_DATE_FORMAT:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 185
    const-string v0, "REPORT_ISSUE"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 186
    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 99
    sget-object v0, Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/hud/app/util/ReportIssueService;Ljava/io/File;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/util/ReportIssueService;
    .param p1, "x1"    # Ljava/io/File;

    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/util/ReportIssueService;->onSyncComplete(Ljava/io/File;)V

    return-void
.end method

.method static synthetic access$200(Lcom/navdy/hud/app/util/ReportIssueService;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/util/ReportIssueService;

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/navdy/hud/app/util/ReportIssueService;->syncLater()V

    return-void
.end method

.method static synthetic access$300(Lcom/navdy/hud/app/util/ReportIssueService;Ljava/lang/String;Ljava/io/File;Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/hud/app/util/ReportIssueService;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/io/File;
    .param p3, "x3"    # Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

    .prologue
    .line 99
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/hud/app/util/ReportIssueService;->attachFilesToTheJiraTicket(Ljava/lang/String;Ljava/io/File;Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;)V

    return-void
.end method

.method private attachFilesToTheJiraTicket(Ljava/lang/String;Ljava/io/File;Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;)V
    .locals 21
    .param p1, "ticket"    # Ljava/lang/String;
    .param p2, "snapShotFile"    # Ljava/io/File;
    .param p3, "callback"    # Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

    .prologue
    .line 647
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->exists()Z

    move-result v17

    if-eqz v17, :cond_3

    .line 648
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v17

    const-string v18, ".zip"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 650
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v12

    .line 651
    .local v12, "parentDir":Ljava/io/File;
    new-instance v16, Ljava/io/File;

    const-string v17, "Staging"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-direct {v0, v12, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 652
    .local v16, "stagingDir":Ljava/io/File;
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->exists()Z

    move-result v17

    if-eqz v17, :cond_0

    .line 653
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->isDirectory()Z

    move-result v17

    if-eqz v17, :cond_1

    .line 654
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/navdy/service/library/util/IOUtils;->deleteDirectory(Landroid/content/Context;Ljava/io/File;)V

    .line 659
    :cond_0
    :goto_0
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->mkdir()Z

    move-result v4

    .line 661
    .local v4, "created":Z
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Lcom/navdy/service/library/util/IOUtils;->deCompressZipToDirectory(Landroid/content/Context;Ljava/io/File;Ljava/io/File;)V

    .line 662
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v5

    .line 663
    .local v5, "decompressedFiles":[Ljava/io/File;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 664
    .local v8, "logFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 665
    .local v15, "pngFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    array-length v0, v5

    move/from16 v18, v0

    const/16 v17, 0x0

    :goto_1
    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_4

    aget-object v7, v5, v17

    .line 666
    .local v7, "file":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v19

    const-string v20, ".png"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_2

    .line 667
    invoke-virtual {v15, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 665
    :goto_2
    add-int/lit8 v17, v17, 0x1

    goto :goto_1

    .line 656
    .end local v4    # "created":Z
    .end local v5    # "decompressedFiles":[Ljava/io/File;
    .end local v7    # "file":Ljava/io/File;
    .end local v8    # "logFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .end local v15    # "pngFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v17

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_0

    .line 669
    .restart local v4    # "created":Z
    .restart local v5    # "decompressedFiles":[Ljava/io/File;
    .restart local v7    # "file":Ljava/io/File;
    .restart local v8    # "logFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .restart local v15    # "pngFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    :cond_2
    :try_start_1
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 688
    .end local v5    # "decompressedFiles":[Ljava/io/File;
    .end local v7    # "file":Ljava/io/File;
    .end local v8    # "logFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .end local v15    # "pngFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    :catch_0
    move-exception v6

    .line 689
    .local v6, "e":Ljava/io/IOException;
    sget-object v17, Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v18, "Error decompressing the zip file to get the attachments"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 693
    .end local v4    # "created":Z
    .end local v6    # "e":Ljava/io/IOException;
    .end local v12    # "parentDir":Ljava/io/File;
    .end local v16    # "stagingDir":Ljava/io/File;
    :cond_3
    :goto_3
    return-void

    .line 672
    .restart local v4    # "created":Z
    .restart local v5    # "decompressedFiles":[Ljava/io/File;
    .restart local v8    # "logFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .restart local v12    # "parentDir":Ljava/io/File;
    .restart local v15    # "pngFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .restart local v16    # "stagingDir":Ljava/io/File;
    :cond_4
    :try_start_2
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v17

    move/from16 v0, v17

    new-array v9, v0, [Ljava/io/File;

    .line 673
    .local v9, "logFilesArray":[Ljava/io/File;
    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "logFilesArray":[Ljava/io/File;
    check-cast v9, [Ljava/io/File;

    .line 674
    .restart local v9    # "logFilesArray":[Ljava/io/File;
    new-instance v10, Ljava/io/File;

    const-string v17, "logs.zip"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-direct {v10, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 675
    .local v10, "logFilesZip":Ljava/io/File;
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v17

    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v9, v1}, Lcom/navdy/service/library/util/IOUtils;->compressFilesToZip(Landroid/content/Context;[Ljava/io/File;Ljava/lang/String;)V

    .line 676
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 677
    .local v3, "attachments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;>;"
    new-instance v11, Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;

    invoke-direct {v11}, Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;-><init>()V

    .line 678
    .local v11, "logFilesZipAttachment":Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;
    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v11, Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;->filePath:Ljava/lang/String;

    .line 679
    const-string v17, "application/zip"

    move-object/from16 v0, v17

    iput-object v0, v11, Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;->mimeType:Ljava/lang/String;

    .line 680
    invoke-virtual {v3, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 681
    invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :goto_4
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_5

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/io/File;

    .line 682
    .local v13, "pngFile":Ljava/io/File;
    new-instance v14, Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;

    invoke-direct {v14}, Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;-><init>()V

    .line 683
    .local v14, "pngFileAttachment":Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;
    invoke-virtual {v13}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    iput-object v0, v14, Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;->filePath:Ljava/lang/String;

    .line 684
    const-string v18, "image/png"

    move-object/from16 v0, v18

    iput-object v0, v14, Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;->mimeType:Ljava/lang/String;

    .line 685
    invoke-virtual {v3, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 687
    .end local v13    # "pngFile":Ljava/io/File;
    .end local v14    # "pngFileAttachment":Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/util/ReportIssueService;->mJiraClient:Lcom/navdy/service/library/network/http/services/JiraClient;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v3, v2}, Lcom/navdy/service/library/network/http/services/JiraClient;->attachFilesToTicket(Ljava/lang/String;Ljava/util/List;Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_3
.end method

.method private buildDescription()Ljava/lang/String;
    .locals 50

    .prologue
    .line 706
    invoke-static/range {p0 .. p0}, Lcom/navdy/service/library/device/NavdyDeviceId;->getThisDevice(Landroid/content/Context;)Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v13

    .line 707
    .local v13, "displayDeviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 710
    .local v4, "builder":Ljava/lang/StringBuilder;
    const-string v47, "\n#############\n"

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 711
    const-string v47, "HERE metadata: "

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 712
    const-string v47, "\n#############\n\n"

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 714
    const-string v33, ""

    .line 715
    .local v33, "prettyJsonString":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->getCurrentHereSdkVersion()Ljava/lang/String;

    move-result-object v19

    .line 716
    .local v19, "hereSdkVersion":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/util/DeviceUtil;->getHEREMapsDataInfo()Ljava/lang/String;

    move-result-object v30

    .line 718
    .local v30, "metaData":Ljava/lang/String;
    if-eqz v30, :cond_0

    .line 720
    :try_start_0
    new-instance v47, Lcom/google/gson/GsonBuilder;

    invoke-direct/range {v47 .. v47}, Lcom/google/gson/GsonBuilder;-><init>()V

    invoke-virtual/range {v47 .. v47}, Lcom/google/gson/GsonBuilder;->setPrettyPrinting()Lcom/google/gson/GsonBuilder;

    move-result-object v47

    invoke-virtual/range {v47 .. v47}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v16

    .line 721
    .local v16, "gson":Lcom/google/gson/Gson;
    new-instance v24, Lcom/google/gson/JsonParser;

    invoke-direct/range {v24 .. v24}, Lcom/google/gson/JsonParser;-><init>()V

    .line 722
    .local v24, "jp":Lcom/google/gson/JsonParser;
    move-object/from16 v0, v24

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/google/gson/JsonParser;->parse(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v23

    .line 723
    .local v23, "je":Lcom/google/gson/JsonElement;
    move-object/from16 v0, v16

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/gson/Gson;->toJson(Lcom/google/gson/JsonElement;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v33

    .line 730
    .end local v16    # "gson":Lcom/google/gson/Gson;
    .end local v23    # "je":Lcom/google/gson/JsonElement;
    .end local v24    # "jp":Lcom/google/gson/JsonParser;
    :cond_0
    :goto_0
    invoke-static/range {v19 .. v19}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v47

    if-nez v47, :cond_1

    .line 731
    const-string v47, "HERE mSDK for Android v"

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    move-object/from16 v0, v47

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    const-string v48, "\n"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 734
    :cond_1
    invoke-static/range {v33 .. v33}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v47

    if-nez v47, :cond_2

    .line 735
    const-string v47, "HERE Map Data:\n"

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    move-object/from16 v0, v47

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    const-string v48, "\n"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 739
    :cond_2
    const-string v47, "\n#############\n"

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 740
    const-string v47, "Navigation details:"

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 741
    const-string v47, "\n#############\n\n"

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 743
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v47

    invoke-virtual/range {v47 .. v47}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getLastGeoPosition()Lcom/here/android/mpa/common/GeoPosition;

    move-result-object v32

    .line 745
    .local v32, "position":Lcom/here/android/mpa/common/GeoPosition;
    if-eqz v32, :cond_b

    invoke-virtual/range {v32 .. v32}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v47

    if-eqz v47, :cond_b

    .line 746
    const-string v47, "Current Location: "

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    invoke-virtual/range {v32 .. v32}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v48

    invoke-virtual/range {v48 .. v48}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v48

    invoke-virtual/range {v47 .. v49}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v47

    const-string v48, ","

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    invoke-virtual/range {v32 .. v32}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v48

    invoke-virtual/range {v48 .. v48}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v48

    invoke-virtual/range {v47 .. v49}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v47

    const-string v48, "\n"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 750
    :goto_1
    const-string v47, "Current Road: "

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getCurrentRoadName()Ljava/lang/String;

    move-result-object v48

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    const-string v48, "\n"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 752
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getCurrentRoadElement()Lcom/here/android/mpa/common/RoadElement;

    move-result-object v34

    .line 753
    .local v34, "roadElement":Lcom/here/android/mpa/common/RoadElement;
    if-eqz v34, :cond_3

    .line 754
    const-string v47, "Current road element ID: "

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    invoke-virtual/range {v34 .. v34}, Lcom/here/android/mpa/common/RoadElement;->getIdentifier()Lcom/here/android/mpa/common/Identifier;

    move-result-object v48

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v47

    const-string v48, "\n"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 757
    :cond_3
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v14

    .line 758
    .local v14, "driverProfileHelper":Lcom/navdy/hud/app/framework/DriverProfileHelper;
    invoke-virtual {v14}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v7

    .line 760
    .local v7, "currentProfile":Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v47

    invoke-virtual/range {v47 .. v47}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isNavigationModeOn()Z

    move-result v47

    if-eqz v47, :cond_10

    .line 761
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereNavigationManager;

    move-result-object v18

    .line 763
    .local v18, "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    invoke-virtual/range {v18 .. v18}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentRoute()Lcom/here/android/mpa/routing/Route;

    move-result-object v35

    .line 764
    .local v35, "route":Lcom/here/android/mpa/routing/Route;
    invoke-virtual/range {v18 .. v18}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getNavController()Lcom/navdy/hud/app/maps/here/HereNavController;

    move-result-object v17

    .line 766
    .local v17, "hereNavController":Lcom/navdy/hud/app/maps/here/HereNavController;
    if-eqz v35, :cond_9

    .line 767
    sget-object v47, Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;->OPTIMAL:Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    const/16 v48, 0x1

    move-object/from16 v0, v17

    move-object/from16 v1, v47

    move/from16 v2, v48

    invoke-virtual {v0, v1, v2}, Lcom/navdy/hud/app/maps/here/HereNavController;->getTta(Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;Z)Lcom/here/android/mpa/routing/RouteTta;

    move-result-object v36

    .line 768
    .local v36, "routeTta":Lcom/here/android/mpa/routing/RouteTta;
    invoke-virtual/range {v18 .. v18}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getStartLocation()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v38

    .line 769
    .local v38, "startingPoint":Lcom/here/android/mpa/common/GeoCoordinate;
    if-eqz v38, :cond_c

    .line 770
    const-string v47, "Route Starting point: "

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    invoke-virtual/range {v38 .. v38}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v48

    invoke-virtual/range {v47 .. v49}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v47

    const-string v48, ","

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    invoke-virtual/range {v38 .. v38}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v48

    invoke-virtual/range {v47 .. v49}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v47

    const-string v48, "\n"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 775
    :goto_2
    invoke-virtual/range {v35 .. v35}, Lcom/here/android/mpa/routing/Route;->getDestination()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v9

    .line 776
    .local v9, "destination":Lcom/here/android/mpa/common/GeoCoordinate;
    if-eqz v9, :cond_d

    .line 777
    const-string v47, "Route Ending point: "

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    invoke-virtual {v9}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v48

    invoke-virtual/range {v47 .. v49}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v47

    const-string v48, ","

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    invoke-virtual {v9}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v48

    invoke-virtual/range {v47 .. v49}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v47

    const-string v48, "\n"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 782
    :goto_3
    invoke-virtual/range {v18 .. v18}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getCurrentRouteId()Ljava/lang/String;

    move-result-object v47

    move-object/from16 v0, v18

    move-object/from16 v1, v47

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->isTrafficConsidered(Ljava/lang/String;)Z

    move-result v40

    .line 783
    .local v40, "trafficConsidered":Z
    const-string v47, "Traffic Used: "

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    move-object/from16 v0, v47

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v47

    const-string v48, "\n"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 785
    invoke-static {}, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->getInstance()Lcom/navdy/hud/app/analytics/NavigationQualityTracker;

    move-result-object v47

    invoke-virtual/range {v47 .. v47}, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->getTripStartUtc()J

    move-result-wide v42

    .line 787
    .local v42, "tripStartedTimestamp":J
    const-wide/16 v48, 0x0

    cmp-long v47, v42, v48

    if-lez v47, :cond_4

    .line 788
    new-instance v8, Ljava/text/SimpleDateFormat;

    const-string v47, "MMMM dd HH:mm:ss zzzz yyyy Z"

    sget-object v48, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v47

    move-object/from16 v1, v48

    invoke-direct {v8, v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 789
    .local v8, "dateFormat":Ljava/text/SimpleDateFormat;
    const-string v47, "Trip started: "

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    new-instance v48, Ljava/util/Date;

    move-object/from16 v0, v48

    move-wide/from16 v1, v42

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    move-object/from16 v0, v48

    invoke-virtual {v8, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v48

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    const-string v48, "\n"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 792
    .end local v8    # "dateFormat":Ljava/text/SimpleDateFormat;
    :cond_4
    invoke-static {}, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->getInstance()Lcom/navdy/hud/app/analytics/NavigationQualityTracker;

    move-result-object v31

    .line 793
    .local v31, "navigationQualityTracker":Lcom/navdy/hud/app/analytics/NavigationQualityTracker;
    invoke-virtual/range {v31 .. v31}, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->getActualDurationSoFar()I

    move-result v15

    .line 794
    .local v15, "durationSoFar":I
    invoke-virtual/range {v31 .. v31}, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->getExpectedDuration()I

    move-result v22

    .line 795
    .local v22, "initialDuration":I
    invoke-virtual/range {v31 .. v31}, Lcom/navdy/hud/app/analytics/NavigationQualityTracker;->getExpectedDistance()I

    move-result v21

    .line 797
    .local v21, "initialDistance":I
    invoke-virtual/range {v17 .. v17}, Lcom/navdy/hud/app/maps/here/HereNavController;->getDestinationDistance()J

    move-result-wide v10

    .line 799
    .local v10, "destinationDistance":J
    if-lez v22, :cond_5

    .line 800
    const-string v47, "Expected initial ETA according to HERE: "

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    move-object/from16 v0, v47

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v47

    const-string v48, " seconds\n"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 803
    :cond_5
    if-lez v21, :cond_6

    .line 804
    const-string v47, "Expected initial distance according to HERE: "

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    move-object/from16 v0, v47

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v47

    const-string v48, " meters\n"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 807
    :cond_6
    if-eqz v36, :cond_7

    .line 808
    invoke-virtual/range {v36 .. v36}, Lcom/here/android/mpa/routing/RouteTta;->getDuration()I

    move-result v41

    .line 809
    .local v41, "tta":I
    const-string v47, "Time To Arrival remaining according to HERE: "

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    move-object/from16 v0, v47

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v47

    const-string v48, " seconds\n"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 812
    .end local v41    # "tta":I
    :cond_7
    const-wide/16 v48, 0x0

    cmp-long v47, v10, v48

    if-lez v47, :cond_8

    .line 813
    const-string v47, "Distance remaining according to HERE: "

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    move-object/from16 v0, v47

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v47

    const-string v48, " meters\n"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 816
    :cond_8
    if-lez v15, :cond_9

    .line 817
    const-string v47, "Duration of the trip so far: "

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    move-object/from16 v0, v47

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v47

    const-string v48, " seconds\n"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 820
    .end local v9    # "destination":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v10    # "destinationDistance":J
    .end local v15    # "durationSoFar":I
    .end local v21    # "initialDistance":I
    .end local v22    # "initialDuration":I
    .end local v31    # "navigationQualityTracker":Lcom/navdy/hud/app/analytics/NavigationQualityTracker;
    .end local v36    # "routeTta":Lcom/here/android/mpa/routing/RouteTta;
    .end local v38    # "startingPoint":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v40    # "trafficConsidered":Z
    .end local v42    # "tripStartedTimestamp":J
    :cond_9
    invoke-virtual/range {v18 .. v18}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getDestinationLabel()Ljava/lang/String;

    move-result-object v12

    .line 821
    .local v12, "destinationLabel":Ljava/lang/String;
    const-string v47, "Destination: "

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    move-object/from16 v0, v47

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    const-string v48, ", "

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    invoke-virtual/range {v18 .. v18}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getDestinationStreetAddress()Ljava/lang/String;

    move-result-object v48

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    const-string v48, "\n"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 824
    const-string v47, "\n#############\n"

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 825
    const-string v47, "User route preferences:"

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 826
    const-string v47, "\n#############\n\n"

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 827
    invoke-virtual {v7}, Lcom/navdy/hud/app/profile/DriverProfile;->getNavigationPreferences()Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    move-result-object v46

    .line 828
    .local v46, "userNavPrefs":Lcom/navdy/service/library/events/preferences/NavigationPreferences;
    move-object/from16 v0, v46

    iget-object v0, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->routingType:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;

    move-object/from16 v47, v0

    sget-object v48, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;->ROUTING_FASTEST:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;

    move-object/from16 v0, v47

    move-object/from16 v1, v48

    if-ne v0, v1, :cond_e

    const-string v37, "fastest"

    .line 829
    .local v37, "routingType":Ljava/lang/String;
    :goto_4
    const-string v47, "Route calculation: "

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    move-object/from16 v0, v47

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    const-string v48, "\n"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 830
    const-string v47, "Highways: "

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    move-object/from16 v0, v46

    iget-object v0, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowHighways:Ljava/lang/Boolean;

    move-object/from16 v48, v0

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v47

    const-string v48, "\n"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 831
    const-string v47, "Toll roads: "

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    move-object/from16 v0, v46

    iget-object v0, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowTollRoads:Ljava/lang/Boolean;

    move-object/from16 v48, v0

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v47

    const-string v48, "\n"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 832
    const-string v47, "Ferries: "

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    move-object/from16 v0, v46

    iget-object v0, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowFerries:Ljava/lang/Boolean;

    move-object/from16 v48, v0

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v47

    const-string v48, "\n"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 833
    const-string v47, "Tunnels: "

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    move-object/from16 v0, v46

    iget-object v0, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowTunnels:Ljava/lang/Boolean;

    move-object/from16 v48, v0

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v47

    const-string v48, "\n"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 834
    const-string v47, "Unpaved Roads: "

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    move-object/from16 v0, v46

    iget-object v0, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowUnpavedRoads:Ljava/lang/Boolean;

    move-object/from16 v48, v0

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v47

    const-string v48, "\n"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 835
    const-string v47, "Auto Trains: "

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    move-object/from16 v0, v46

    iget-object v0, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowAutoTrains:Ljava/lang/Boolean;

    move-object/from16 v48, v0

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v47

    const-string v48, "\n"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 838
    const-string v47, "\n#############\n"

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 839
    const-string v47, "Maneuvers (entire route):"

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 840
    const-string v47, "\n#############\n\n"

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 841
    const/16 v47, 0x0

    move-object/from16 v0, v35

    move-object/from16 v1, v47

    invoke-static {v0, v12, v1, v4}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->printRouteDetails(Lcom/here/android/mpa/routing/Route;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/StringBuilder;)V

    .line 844
    const-string v47, "\n#############\n"

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 845
    const-string v47, "Waypoints (entire route):"

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 846
    const-string v47, "\n#############\n\n"

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 847
    const/16 v20, 0x0

    .line 848
    .local v20, "idx":I
    invoke-virtual/range {v35 .. v35}, Lcom/here/android/mpa/routing/Route;->getManeuvers()Ljava/util/List;

    move-result-object v47

    invoke-interface/range {v47 .. v47}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v47

    :cond_a
    :goto_5
    invoke-interface/range {v47 .. v47}, Ljava/util/Iterator;->hasNext()Z

    move-result v48

    if-eqz v48, :cond_f

    invoke-interface/range {v47 .. v47}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/here/android/mpa/routing/Maneuver;

    .line 849
    .local v25, "maneuver":Lcom/here/android/mpa/routing/Maneuver;
    invoke-virtual/range {v25 .. v25}, Lcom/here/android/mpa/routing/Maneuver;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v5

    .line 850
    .local v5, "coordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    if-eqz v5, :cond_a

    .line 851
    invoke-virtual {v5}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v26

    .line 852
    .local v26, "lat":D
    invoke-virtual {v5}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v28

    .line 853
    .local v28, "lng":D
    const-string v48, "waypoint"

    move-object/from16 v0, v48

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v48

    move-object/from16 v0, v48

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v48

    const-string v49, ": "

    invoke-virtual/range {v48 .. v49}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v48

    move-object/from16 v0, v48

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v48

    const-string v49, ","

    invoke-virtual/range {v48 .. v49}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v48

    move-object/from16 v0, v48

    move-wide/from16 v1, v28

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v48

    const-string v49, "\n"

    invoke-virtual/range {v48 .. v49}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 854
    add-int/lit8 v20, v20, 0x1

    goto :goto_5

    .line 724
    .end local v5    # "coordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v7    # "currentProfile":Lcom/navdy/hud/app/profile/DriverProfile;
    .end local v12    # "destinationLabel":Ljava/lang/String;
    .end local v14    # "driverProfileHelper":Lcom/navdy/hud/app/framework/DriverProfileHelper;
    .end local v17    # "hereNavController":Lcom/navdy/hud/app/maps/here/HereNavController;
    .end local v18    # "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    .end local v20    # "idx":I
    .end local v25    # "maneuver":Lcom/here/android/mpa/routing/Maneuver;
    .end local v26    # "lat":D
    .end local v28    # "lng":D
    .end local v32    # "position":Lcom/here/android/mpa/common/GeoPosition;
    .end local v34    # "roadElement":Lcom/here/android/mpa/common/RoadElement;
    .end local v35    # "route":Lcom/here/android/mpa/routing/Route;
    .end local v37    # "routingType":Ljava/lang/String;
    .end local v46    # "userNavPrefs":Lcom/navdy/service/library/events/preferences/NavigationPreferences;
    :catch_0
    move-exception v39

    .line 725
    .local v39, "t":Ljava/lang/Throwable;
    sget-object v47, Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v0, v47

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 748
    .end local v39    # "t":Ljava/lang/Throwable;
    .restart local v32    # "position":Lcom/here/android/mpa/common/GeoPosition;
    :cond_b
    const-string v47, "Current Location: N/A\n"

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 772
    .restart local v7    # "currentProfile":Lcom/navdy/hud/app/profile/DriverProfile;
    .restart local v14    # "driverProfileHelper":Lcom/navdy/hud/app/framework/DriverProfileHelper;
    .restart local v17    # "hereNavController":Lcom/navdy/hud/app/maps/here/HereNavController;
    .restart local v18    # "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    .restart local v34    # "roadElement":Lcom/here/android/mpa/common/RoadElement;
    .restart local v35    # "route":Lcom/here/android/mpa/routing/Route;
    .restart local v36    # "routeTta":Lcom/here/android/mpa/routing/RouteTta;
    .restart local v38    # "startingPoint":Lcom/here/android/mpa/common/GeoCoordinate;
    :cond_c
    const-string v47, "Route Starting point: N/A\n"

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 779
    .restart local v9    # "destination":Lcom/here/android/mpa/common/GeoCoordinate;
    :cond_d
    const-string v47, "Route Ending point: N/A\n"

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 828
    .end local v9    # "destination":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v36    # "routeTta":Lcom/here/android/mpa/routing/RouteTta;
    .end local v38    # "startingPoint":Lcom/here/android/mpa/common/GeoCoordinate;
    .restart local v12    # "destinationLabel":Ljava/lang/String;
    .restart local v46    # "userNavPrefs":Lcom/navdy/service/library/events/preferences/NavigationPreferences;
    :cond_e
    const-string v37, "shortest"

    goto/16 :goto_4

    .line 858
    .restart local v20    # "idx":I
    .restart local v37    # "routingType":Ljava/lang/String;
    :cond_f
    const-string v47, "\n#############\n"

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 859
    const-string v47, "Maneuvers (remaining route):"

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 860
    const-string v47, "\n#############\n\n"

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 861
    invoke-virtual/range {v18 .. v18}, Lcom/navdy/hud/app/maps/here/HereNavigationManager;->getNextManeuver()Lcom/here/android/mpa/routing/Maneuver;

    move-result-object v6

    .line 862
    .local v6, "currentManeuver":Lcom/here/android/mpa/routing/Maneuver;
    const/16 v47, 0x0

    move-object/from16 v0, v35

    move-object/from16 v1, v47

    invoke-static {v0, v12, v1, v4, v6}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->printRouteDetails(Lcom/here/android/mpa/routing/Route;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;Ljava/lang/StringBuilder;Lcom/here/android/mpa/routing/Maneuver;)V

    .line 865
    .end local v6    # "currentManeuver":Lcom/here/android/mpa/routing/Maneuver;
    .end local v12    # "destinationLabel":Ljava/lang/String;
    .end local v17    # "hereNavController":Lcom/navdy/hud/app/maps/here/HereNavController;
    .end local v18    # "hereNavigationManager":Lcom/navdy/hud/app/maps/here/HereNavigationManager;
    .end local v20    # "idx":I
    .end local v35    # "route":Lcom/here/android/mpa/routing/Route;
    .end local v37    # "routingType":Ljava/lang/String;
    .end local v46    # "userNavPrefs":Lcom/navdy/service/library/events/preferences/NavigationPreferences;
    :cond_10
    const-string v47, "\n#############\n"

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 866
    const-string v47, "Navdy info:"

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 867
    const-string v47, "\n#############\n\n"

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 869
    const-string v47, "Device ID: "

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    invoke-virtual {v13}, Lcom/navdy/service/library/device/NavdyDeviceId;->toString()Ljava/lang/String;

    move-result-object v48

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    const-string v48, "\n"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 870
    const-string v47, "HUD app version: 1.3.3052-ce5463a\n"

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 871
    const-string v47, "Serial number: "

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    sget-object v48, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    const-string v48, "\n"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 872
    const-string v47, "Model: "

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    sget-object v48, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    const-string v48, "\n"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 873
    const-string v47, "API Level: "

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    sget v48, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v47

    const-string v48, "\n"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 874
    const-string v47, "Build type: "

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    sget-object v48, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    const-string v48, "\n"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 880
    invoke-virtual {v7}, Lcom/navdy/hud/app/profile/DriverProfile;->isDefaultProfile()Z

    move-result v47

    if-nez v47, :cond_12

    .line 881
    invoke-virtual {v7}, Lcom/navdy/hud/app/profile/DriverProfile;->getDriverName()Ljava/lang/String;

    move-result-object v45

    .line 882
    .local v45, "userName":Ljava/lang/String;
    invoke-virtual {v7}, Lcom/navdy/hud/app/profile/DriverProfile;->getDriverEmail()Ljava/lang/String;

    move-result-object v44

    .line 888
    .local v44, "userEmail":Ljava/lang/String;
    :goto_6
    invoke-static/range {v45 .. v45}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v47

    if-nez v47, :cond_11

    invoke-static/range {v44 .. v44}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v47

    if-nez v47, :cond_11

    .line 889
    const-string v47, "Username: "

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    move-object/from16 v0, v47

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    const-string v48, "\n"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 890
    const-string v47, "Email: "

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    move-object/from16 v0, v47

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v47

    const-string v48, "\n"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 893
    :cond_11
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v47

    return-object v47

    .line 884
    .end local v44    # "userEmail":Ljava/lang/String;
    .end local v45    # "userName":Ljava/lang/String;
    :cond_12
    invoke-virtual {v14}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getDriverProfileManager()Lcom/navdy/hud/app/profile/DriverProfileManager;

    move-result-object v47

    invoke-virtual/range {v47 .. v47}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getLastUserName()Ljava/lang/String;

    move-result-object v45

    .line 885
    .restart local v45    # "userName":Ljava/lang/String;
    invoke-virtual {v14}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getDriverProfileManager()Lcom/navdy/hud/app/profile/DriverProfileManager;

    move-result-object v47

    invoke-virtual/range {v47 .. v47}, Lcom/navdy/hud/app/profile/DriverProfileManager;->getLastUserEmail()Ljava/lang/String;

    move-result-object v44

    .restart local v44    # "userEmail":Ljava/lang/String;
    goto :goto_6
.end method

.method public static canReportIssue()Z
    .locals 2

    .prologue
    .line 951
    sget-object v0, Lcom/navdy/hud/app/util/ReportIssueService;->mIssuesToSync:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/PriorityBlockingQueue;->size()I

    move-result v0

    const/16 v1, 0xa

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static dispatchReportNewIssue(Lcom/navdy/hud/app/util/ReportIssueService$IssueType;)V
    .locals 3
    .param p0, "issueType"    # Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    .prologue
    .line 915
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/hud/app/util/ReportIssueService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 916
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "Dump"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 918
    const-string v1, "EXTRA_ISSUE_TYPE"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 919
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 920
    return-void
.end method

.method public static dispatchSync()V
    .locals 3

    .prologue
    .line 936
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/hud/app/util/ReportIssueService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 937
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "Sync"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 938
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 939
    return-void
.end method

.method private dumpIssue(Lcom/navdy/hud/app/util/ReportIssueService$IssueType;)V
    .locals 5
    .param p1, "issueType"    # Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    .prologue
    .line 412
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isNavigationModeOn()Z

    move-result v1

    if-nez v1, :cond_0

    .line 421
    :goto_0
    return-void

    .line 415
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 416
    .local v0, "descriptionBuilder":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->ordinal()I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "\n"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 417
    invoke-direct {p0}, Lcom/navdy/hud/app/util/ReportIssueService;->buildDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 418
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x0

    invoke-direct {p0, v1, v4}, Lcom/navdy/hud/app/util/ReportIssueService;->saveJiraTicketDescriptionToFile(Ljava/lang/String;Z)V

    .line 419
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 420
    .local v2, "time":J
    sput-wide v2, Lcom/navdy/hud/app/util/ReportIssueService;->mLastIssueSubmittedAt:J

    goto :goto_0
.end method

.method private dumpSnapshot()V
    .locals 33

    .prologue
    .line 278
    sget-object v29, Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v30, "Creating the snapshot of the HUD for support ticket"

    invoke-virtual/range {v29 .. v30}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 279
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v29

    invoke-static {}, Lcom/navdy/hud/app/device/light/LightManager;->getInstance()Lcom/navdy/hud/app/device/light/LightManager;

    move-result-object v30

    const/16 v31, 0x0

    invoke-static/range {v29 .. v31}, Lcom/navdy/hud/app/device/light/HUDLightUtils;->showSnapshotCollection(Landroid/content/Context;Lcom/navdy/hud/app/device/light/LightManager;Z)V

    .line 281
    :try_start_0
    invoke-static {}, Lcom/navdy/hud/app/storage/PathManager;->getInstance()Lcom/navdy/hud/app/storage/PathManager;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/navdy/hud/app/storage/PathManager;->getNonFatalCrashReportDir()Ljava/lang/String;

    move-result-object v14

    .line 282
    .local v14, "folder":Ljava/lang/String;
    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v29

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    sget-object v30, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, "screen.png"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v29 .. v29}, Lcom/navdy/hud/app/util/DeviceUtil;->takeDeviceScreenShot(Ljava/lang/String;)V

    .line 283
    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    .line 284
    .local v24, "snapshotFiles":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    sget-object v29, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string v30, "eng"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_0

    .line 285
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/util/ReportIssueService;->gestureService:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->dumpRecording()Ljava/lang/String;

    .line 299
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/util/ReportIssueService;->driveRecorder:Lcom/navdy/hud/app/debug/DriveRecorder;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Lcom/navdy/hud/app/debug/DriveRecorder;->flushRecordings()V

    .line 300
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/hud/app/util/ReportIssueService;->gestureService:Lcom/navdy/hud/app/gesture/GestureServiceConnector;

    move-object/from16 v29, v0

    const-string v30, "/data/misc/swiped/camera.png"

    invoke-virtual/range {v29 .. v30}, Lcom/navdy/hud/app/gesture/GestureServiceConnector;->takeSnapShot(Ljava/lang/String;)V

    .line 303
    new-instance v8, Ljava/io/File;

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v29

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    sget-object v30, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, "deviceInfo.txt"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-direct {v8, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 304
    .local v8, "deviceInfoTextFile":Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v29

    if-eqz v29, :cond_1

    .line 305
    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-static {v0, v1}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 309
    :cond_1
    new-instance v12, Ljava/io/FileWriter;

    invoke-direct {v12, v8}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V

    .line 310
    .local v12, "fileWriter":Ljava/io/FileWriter;
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getConnectionHandler()Lcom/navdy/hud/app/service/ConnectionHandler;

    move-result-object v4

    .line 311
    .local v4, "connectionHandler":Lcom/navdy/hud/app/service/ConnectionHandler;
    if-eqz v4, :cond_2

    .line 312
    invoke-virtual {v4}, Lcom/navdy/hud/app/service/ConnectionHandler;->getLastConnectedDeviceInfo()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v15

    .line 313
    .local v15, "lastConnectedDeviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    if-eqz v15, :cond_2

    .line 314
    invoke-static {v15}, Lcom/navdy/hud/app/util/CrashReportService;->printDeviceInfo(Lcom/navdy/service/library/events/DeviceInfo;)Ljava/lang/String;

    move-result-object v7

    .line 315
    .local v7, "deviceInfoString":Ljava/lang/String;
    invoke-virtual {v12, v7}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 318
    .end local v7    # "deviceInfoString":Ljava/lang/String;
    .end local v15    # "lastConnectedDeviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    :cond_2
    invoke-virtual {v12}, Ljava/io/FileWriter;->flush()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 320
    :try_start_1
    invoke-virtual {v12}, Ljava/io/FileWriter;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 326
    :goto_0
    :try_start_2
    new-instance v27, Ljava/io/File;

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v29

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    sget-object v30, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, "temp"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v27

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 327
    .local v27, "tempDirectory":Ljava/io/File;
    invoke-virtual/range {v27 .. v27}, Ljava/io/File;->exists()Z

    move-result v29

    if-eqz v29, :cond_3

    .line 328
    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-static {v0, v1}, Lcom/navdy/service/library/util/IOUtils;->deleteDirectory(Landroid/content/Context;Ljava/io/File;)V

    .line 330
    :cond_3
    invoke-virtual/range {v27 .. v27}, Ljava/io/File;->mkdirs()Z

    move-result v5

    .line 332
    .local v5, "couldCreateDir":Z
    if-nez v5, :cond_4

    .line 333
    sget-object v29, Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v30, "could not create tempDirectory"

    invoke-virtual/range {v29 .. v30}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 402
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v29

    invoke-static {}, Lcom/navdy/hud/app/device/light/LightManager;->getInstance()Lcom/navdy/hud/app/device/light/LightManager;

    move-result-object v30

    const/16 v31, 0x1

    invoke-static/range {v29 .. v31}, Lcom/navdy/hud/app/device/light/HUDLightUtils;->showSnapshotCollection(Landroid/content/Context;Lcom/navdy/hud/app/device/light/LightManager;Z)V

    .line 404
    .end local v4    # "connectionHandler":Lcom/navdy/hud/app/service/ConnectionHandler;
    .end local v5    # "couldCreateDir":Z
    .end local v8    # "deviceInfoTextFile":Ljava/io/File;
    .end local v12    # "fileWriter":Ljava/io/FileWriter;
    .end local v14    # "folder":Ljava/lang/String;
    .end local v24    # "snapshotFiles":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    .end local v27    # "tempDirectory":Ljava/io/File;
    :goto_1
    return-void

    .line 321
    .restart local v4    # "connectionHandler":Lcom/navdy/hud/app/service/ConnectionHandler;
    .restart local v8    # "deviceInfoTextFile":Ljava/io/File;
    .restart local v12    # "fileWriter":Ljava/io/FileWriter;
    .restart local v14    # "folder":Ljava/lang/String;
    .restart local v24    # "snapshotFiles":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    :catch_0
    move-exception v10

    .line 322
    .local v10, "e":Ljava/io/IOException;
    :try_start_3
    sget-object v29, Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v30, "Error closing the file writer for log file"

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    invoke-virtual {v0, v1, v10}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 399
    .end local v4    # "connectionHandler":Lcom/navdy/hud/app/service/ConnectionHandler;
    .end local v8    # "deviceInfoTextFile":Ljava/io/File;
    .end local v10    # "e":Ljava/io/IOException;
    .end local v12    # "fileWriter":Ljava/io/FileWriter;
    .end local v14    # "folder":Ljava/lang/String;
    .end local v24    # "snapshotFiles":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    :catch_1
    move-exception v26

    .line 400
    .local v26, "t":Ljava/lang/Throwable;
    :try_start_4
    sget-object v29, Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v30, "Error creating a snapshot "

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 402
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v29

    invoke-static {}, Lcom/navdy/hud/app/device/light/LightManager;->getInstance()Lcom/navdy/hud/app/device/light/LightManager;

    move-result-object v30

    const/16 v31, 0x1

    invoke-static/range {v29 .. v31}, Lcom/navdy/hud/app/device/light/HUDLightUtils;->showSnapshotCollection(Landroid/content/Context;Lcom/navdy/hud/app/device/light/LightManager;Z)V

    goto :goto_1

    .line 336
    .end local v26    # "t":Ljava/lang/Throwable;
    .restart local v4    # "connectionHandler":Lcom/navdy/hud/app/service/ConnectionHandler;
    .restart local v5    # "couldCreateDir":Z
    .restart local v8    # "deviceInfoTextFile":Ljava/io/File;
    .restart local v12    # "fileWriter":Ljava/io/FileWriter;
    .restart local v14    # "folder":Ljava/lang/String;
    .restart local v24    # "snapshotFiles":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    .restart local v27    # "tempDirectory":Ljava/io/File;
    :cond_4
    :try_start_5
    invoke-virtual/range {v27 .. v27}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v25

    .line 337
    .local v25, "stagingPath":Ljava/lang/String;
    invoke-static/range {v25 .. v25}, Lcom/navdy/service/library/util/LogUtils;->copySnapshotSystemLogs(Ljava/lang/String;)V

    .line 338
    invoke-static {}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->getInstance()Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/device/gps/GpsDeadReckoningManager;->dumpGpsInfo(Ljava/lang/String;)V

    .line 339
    invoke-static {}, Lcom/navdy/hud/app/storage/PathManager;->getInstance()Lcom/navdy/hud/app/storage/PathManager;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/storage/PathManager;->collectEnvironmentInfo(Ljava/lang/String;)V

    .line 340
    invoke-virtual/range {v27 .. v27}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v18

    .line 341
    .local v18, "logFiles":[Ljava/io/File;
    new-instance v20, Ljava/io/File;

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v29

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    sget-object v30, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, "route.log"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v20

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 343
    .local v20, "routeLogFile":Ljava/io/File;
    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->exists()Z

    move-result v29

    if-eqz v29, :cond_5

    .line 344
    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-static {v0, v1}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 346
    :cond_5
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/util/ReportIssueService;->buildDescription()Ljava/lang/String;

    move-result-object v19

    .line 347
    .local v19, "routeDescription":Ljava/lang/String;
    new-instance v12, Ljava/io/FileWriter;

    .end local v12    # "fileWriter":Ljava/io/FileWriter;
    move-object/from16 v0, v20

    invoke-direct {v12, v0}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V

    .line 348
    .restart local v12    # "fileWriter":Ljava/io/FileWriter;
    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 349
    invoke-virtual {v12}, Ljava/io/FileWriter;->flush()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 351
    :try_start_6
    invoke-virtual {v12}, Ljava/io/FileWriter;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 355
    :goto_2
    :try_start_7
    new-instance v29, Ljava/io/File;

    const-string v30, "/data/misc/swiped/camera.png"

    invoke-direct/range {v29 .. v30}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 356
    new-instance v29, Ljava/io/File;

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v30

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    sget-object v31, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "screen.png"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-direct/range {v29 .. v30}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v24

    move-object/from16 v1, v29

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 357
    move-object/from16 v0, v24

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 358
    move-object/from16 v0, v24

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 359
    if-eqz v18, :cond_6

    .line 360
    move-object/from16 v0, v24

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 364
    :cond_6
    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    .line 365
    .local v9, "driveLogFilesSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/io/File;>;"
    const/16 v29, 0x1

    invoke-static/range {v29 .. v29}, Lcom/navdy/hud/app/util/ReportIssueService;->getLatestDriveLogFiles(I)Ljava/util/List;

    move-result-object v17

    .line 366
    .local v17, "latestFiles":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    if-eqz v17, :cond_7

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v29

    if-lez v29, :cond_7

    .line 367
    move-object/from16 v0, v24

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 368
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v29

    :goto_3
    invoke-interface/range {v29 .. v29}, Ljava/util/Iterator;->hasNext()Z

    move-result v30

    if-eqz v30, :cond_7

    invoke-interface/range {v29 .. v29}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/io/File;

    .line 369
    .local v16, "latestFile":Ljava/io/File;
    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_3

    .line 402
    .end local v4    # "connectionHandler":Lcom/navdy/hud/app/service/ConnectionHandler;
    .end local v5    # "couldCreateDir":Z
    .end local v8    # "deviceInfoTextFile":Ljava/io/File;
    .end local v9    # "driveLogFilesSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/io/File;>;"
    .end local v12    # "fileWriter":Ljava/io/FileWriter;
    .end local v14    # "folder":Ljava/lang/String;
    .end local v16    # "latestFile":Ljava/io/File;
    .end local v17    # "latestFiles":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    .end local v18    # "logFiles":[Ljava/io/File;
    .end local v19    # "routeDescription":Ljava/lang/String;
    .end local v20    # "routeLogFile":Ljava/io/File;
    .end local v24    # "snapshotFiles":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    .end local v25    # "stagingPath":Ljava/lang/String;
    .end local v27    # "tempDirectory":Ljava/io/File;
    :catchall_0
    move-exception v29

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v30

    invoke-static {}, Lcom/navdy/hud/app/device/light/LightManager;->getInstance()Lcom/navdy/hud/app/device/light/LightManager;

    move-result-object v31

    const/16 v32, 0x1

    invoke-static/range {v30 .. v32}, Lcom/navdy/hud/app/device/light/HUDLightUtils;->showSnapshotCollection(Landroid/content/Context;Lcom/navdy/hud/app/device/light/LightManager;Z)V

    throw v29

    .line 352
    .restart local v4    # "connectionHandler":Lcom/navdy/hud/app/service/ConnectionHandler;
    .restart local v5    # "couldCreateDir":Z
    .restart local v8    # "deviceInfoTextFile":Ljava/io/File;
    .restart local v12    # "fileWriter":Ljava/io/FileWriter;
    .restart local v14    # "folder":Ljava/lang/String;
    .restart local v18    # "logFiles":[Ljava/io/File;
    .restart local v19    # "routeDescription":Ljava/lang/String;
    .restart local v20    # "routeLogFile":Ljava/io/File;
    .restart local v24    # "snapshotFiles":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    .restart local v25    # "stagingPath":Ljava/lang/String;
    .restart local v27    # "tempDirectory":Ljava/io/File;
    :catch_2
    move-exception v10

    .line 353
    .restart local v10    # "e":Ljava/io/IOException;
    :try_start_8
    sget-object v29, Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v30, "Error closing the file writer for route log file"

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    invoke-virtual {v0, v1, v10}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 372
    .end local v10    # "e":Ljava/io/IOException;
    .restart local v9    # "driveLogFilesSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/io/File;>;"
    .restart local v17    # "latestFiles":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    :cond_7
    new-instance v6, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v30

    move-wide/from16 v0, v30

    invoke-direct {v6, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 373
    .local v6, "date":Ljava/util/Date;
    sget-object v29, Lcom/navdy/hud/app/util/ReportIssueService;->SNAPSHOT_DATE_FORMAT:Ljava/text/SimpleDateFormat;

    move-object/from16 v0, v29

    invoke-virtual {v0, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v28

    .line 374
    .local v28, "timeString":Ljava/lang/String;
    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "snapshot-"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    sget-object v30, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, "-"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    sget-object v30, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, "_"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, ".zip"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    .line 375
    .local v23, "snapshotFileName":Ljava/lang/String;
    new-instance v21, Ljava/io/File;

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v29

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    sget-object v30, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v21

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 376
    .local v21, "snapShotFile":Ljava/io/File;
    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->exists()Z

    move-result v29

    if-eqz v29, :cond_8

    .line 377
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v29

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 379
    :cond_8
    invoke-interface/range {v24 .. v24}, Ljava/util/List;->size()I

    move-result v29

    move/from16 v0, v29

    new-array v13, v0, [Ljava/io/File;

    .line 380
    .local v13, "files":[Ljava/io/File;
    move-object/from16 v0, v24

    invoke-interface {v0, v13}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 381
    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->createNewFile()Z

    move-result v22

    .line 383
    .local v22, "snapshotFileCreated":Z
    if-nez v22, :cond_9

    .line 384
    sget-object v29, Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v30, "snapshot file could not be created"

    invoke-virtual/range {v29 .. v30}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 402
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v29

    invoke-static {}, Lcom/navdy/hud/app/device/light/LightManager;->getInstance()Lcom/navdy/hud/app/device/light/LightManager;

    move-result-object v30

    const/16 v31, 0x1

    invoke-static/range {v29 .. v31}, Lcom/navdy/hud/app/device/light/HUDLightUtils;->showSnapshotCollection(Landroid/content/Context;Lcom/navdy/hud/app/device/light/LightManager;Z)V

    goto/16 :goto_1

    .line 388
    :cond_9
    :try_start_9
    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-static {v0, v13, v1}, Lcom/navdy/service/library/util/IOUtils;->compressFilesToZip(Landroid/content/Context;[Ljava/io/File;Ljava/lang/String;)V

    .line 389
    invoke-interface/range {v24 .. v24}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v29

    :cond_a
    :goto_4
    invoke-interface/range {v29 .. v29}, Ljava/util/Iterator;->hasNext()Z

    move-result v30

    if-eqz v30, :cond_b

    invoke-interface/range {v29 .. v29}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/io/File;

    .line 390
    .local v11, "file":Ljava/io/File;
    invoke-virtual {v9, v11}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v30

    if-nez v30, :cond_a

    .line 393
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v30

    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_4

    .line 395
    .end local v11    # "file":Ljava/io/File;
    :cond_b
    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-static {v0, v1}, Lcom/navdy/service/library/util/IOUtils;->deleteDirectory(Landroid/content/Context;Ljava/io/File;)V

    .line 396
    sput-object v21, Lcom/navdy/hud/app/util/ReportIssueService;->lastSnapShotFile:Ljava/io/File;

    .line 397
    sget-object v29, Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "Snapshot File created "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    sget-object v31, Lcom/navdy/hud/app/util/ReportIssueService;->lastSnapShotFile:Ljava/io/File;

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 398
    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v29 .. v29}, Lcom/navdy/hud/app/util/CrashReportService;->addSnapshotAsync(Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 402
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v29

    invoke-static {}, Lcom/navdy/hud/app/device/light/LightManager;->getInstance()Lcom/navdy/hud/app/device/light/LightManager;

    move-result-object v30

    const/16 v31, 0x1

    invoke-static/range {v29 .. v31}, Lcom/navdy/hud/app/device/light/HUDLightUtils;->showSnapshotCollection(Landroid/content/Context;Lcom/navdy/hud/app/device/light/LightManager;Z)V

    goto/16 :goto_1
.end method

.method public static dumpSnapshotAsync()V
    .locals 3

    .prologue
    .line 923
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/hud/app/util/ReportIssueService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 924
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "Snapshot"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 925
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 926
    return-void
.end method

.method public static getIssueTypeForId(I)Lcom/navdy/hud/app/util/ReportIssueService$IssueType;
    .locals 2
    .param p0, "id"    # I

    .prologue
    .line 906
    invoke-static {}, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->values()[Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    move-result-object v0

    .line 907
    .local v0, "values":[Lcom/navdy/hud/app/util/ReportIssueService$IssueType;
    if-ltz p0, :cond_0

    array-length v1, v0

    if-ge p0, v1, :cond_0

    .line 908
    aget-object v1, v0, p0

    .line 910
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getLatestDriveLogFiles(I)Ljava/util/List;
    .locals 6
    .param p0, "numberOfSets"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 976
    if-gtz p0, :cond_1

    .line 996
    :cond_0
    return-object v3

    .line 980
    :cond_1
    const-string v5, ""

    invoke-static {v5}, Lcom/navdy/hud/app/debug/DriveRecorder;->getDriveLogsDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 981
    .local v1, "driveLogsDirectory":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 982
    .local v0, "driveLogFiles":[Ljava/io/File;
    if-eqz v0, :cond_0

    array-length v5, v0

    if-eqz v5, :cond_0

    .line 985
    mul-int/lit8 v4, p0, 0x4

    .line 986
    .local v4, "n":I
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 987
    .local v3, "latestDriveLogFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    new-instance v5, Lcom/navdy/hud/app/util/ReportIssueService$4;

    invoke-direct {v5}, Lcom/navdy/hud/app/util/ReportIssueService$4;-><init>()V

    invoke-static {v0, v5}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 993
    array-length v5, v0

    add-int/lit8 v2, v5, -0x1

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_0

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v5, v4, :cond_0

    .line 994
    aget-object v5, v0, v2

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 993
    add-int/lit8 v2, v2, -0x1

    goto :goto_0
.end method

.method private getSummary(Lcom/navdy/hud/app/util/ReportIssueService$IssueType;)Ljava/lang/String;
    .locals 2
    .param p1, "issue"    # Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    .prologue
    .line 696
    invoke-virtual {p1}, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->getMessageStringResource()I

    move-result v0

    if-eqz v0, :cond_0

    .line 697
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Navigation issue: ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->getIssueTypeCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->getTitleStringResource()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/util/ReportIssueService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 698
    invoke-virtual {p1}, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->getMessageStringResource()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/util/ReportIssueService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 700
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Navigation issue: ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->getIssueTypeCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->getTitleStringResource()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/navdy/hud/app/util/ReportIssueService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static initialize()V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 955
    sget-boolean v3, Lcom/navdy/hud/app/util/ReportIssueService;->mIsInitialized:Z

    if-nez v3, :cond_3

    .line 956
    sget-object v3, Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Initializing the service statically"

    invoke-virtual {v3, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 957
    invoke-static {}, Lcom/navdy/hud/app/storage/PathManager;->getInstance()Lcom/navdy/hud/app/storage/PathManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/hud/app/storage/PathManager;->getNavigationIssuesDir()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/navdy/hud/app/util/ReportIssueService;->navigationIssuesFolder:Ljava/lang/String;

    .line 958
    new-instance v3, Ljava/io/File;

    sget-object v5, Lcom/navdy/hud/app/util/ReportIssueService;->navigationIssuesFolder:Ljava/lang/String;

    invoke-direct {v3, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 959
    .local v1, "files":[Ljava/io/File;
    sget-object v5, Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Number of Files waiting for sync :"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    if-eqz v1, :cond_1

    array-length v3, v1

    :goto_0
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 960
    if-eqz v1, :cond_2

    .line 961
    array-length v3, v1

    :goto_1
    if-ge v4, v3, :cond_2

    aget-object v0, v1, v4

    .line 962
    .local v0, "file":Ljava/io/File;
    sget-object v5, Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "File "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 963
    sget-object v5, Lcom/navdy/hud/app/util/ReportIssueService;->mIssuesToSync:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v5, v0}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 964
    sget-object v5, Lcom/navdy/hud/app/util/ReportIssueService;->mIssuesToSync:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v5}, Ljava/util/concurrent/PriorityBlockingQueue;->size()I

    move-result v5

    const/16 v6, 0xa

    if-ne v5, v6, :cond_0

    .line 965
    sget-object v5, Lcom/navdy/hud/app/util/ReportIssueService;->mIssuesToSync:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v5}, Ljava/util/concurrent/PriorityBlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    .line 966
    .local v2, "oldestFile":Ljava/io/File;
    sget-object v5, Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Deleting the old file "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 967
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 961
    .end local v2    # "oldestFile":Ljava/io/File;
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .end local v0    # "file":Ljava/io/File;
    :cond_1
    move v3, v4

    .line 959
    goto :goto_0

    .line 971
    :cond_2
    const/4 v3, 0x1

    sput-boolean v3, Lcom/navdy/hud/app/util/ReportIssueService;->mIsInitialized:Z

    .line 973
    :cond_3
    return-void
.end method

.method private onSyncComplete(Ljava/io/File;)V
    .locals 3
    .param p1, "ticketFile"    # Ljava/io/File;

    .prologue
    .line 449
    if-eqz p1, :cond_0

    .line 450
    sget-object v0, Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Removed the file from list :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/navdy/hud/app/util/ReportIssueService;->mIssuesToSync:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v2, p1}, Ljava/util/concurrent/PriorityBlockingQueue;->remove(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 451
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 452
    invoke-static {}, Lcom/navdy/hud/app/util/ReportIssueService;->dispatchSync()V

    .line 454
    :cond_0
    sget-object v0, Lcom/navdy/hud/app/util/ReportIssueService;->mSyncing:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 455
    return-void
.end method

.method private saveJiraTicketDescriptionToFile(Ljava/lang/String;Z)V
    .locals 11
    .param p1, "data"    # Ljava/lang/String;
    .param p2, "isJson"    # Z

    .prologue
    .line 424
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 425
    .local v4, "time":J
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/navdy/hud/app/util/ReportIssueService;->navigationIssuesFolder:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/navdy/hud/app/util/ReportIssueService;->DATE_FORMAT:Ljava/text/SimpleDateFormat;

    new-instance v10, Ljava/util/Date;

    invoke-direct {v10, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v9, v10}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    if-eqz p2, :cond_2

    const-string v8, ".json"

    :goto_0
    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 426
    .local v3, "fileName":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 427
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 428
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-static {p0, v8}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 430
    :cond_0
    const/4 v6, 0x0

    .line 432
    .local v6, "writer":Ljava/io/FileWriter;
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    move-result v2

    .line 434
    .local v2, "fileCreated":Z
    if-eqz v2, :cond_1

    .line 435
    new-instance v7, Ljava/io/FileWriter;

    invoke-direct {v7, v1}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 436
    .end local v6    # "writer":Ljava/io/FileWriter;
    .local v7, "writer":Ljava/io/FileWriter;
    :try_start_1
    invoke-virtual {v7, p1}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 437
    invoke-virtual {v7}, Ljava/io/FileWriter;->close()V

    .line 438
    sget-object v8, Lcom/navdy/hud/app/util/ReportIssueService;->mIssuesToSync:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v8, v1}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z

    move-object v6, v7

    .line 439
    .end local v7    # "writer":Ljava/io/FileWriter;
    .restart local v6    # "writer":Ljava/io/FileWriter;
    invoke-static {}, Lcom/navdy/hud/app/util/ReportIssueService;->dispatchSync()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 444
    :cond_1
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 446
    .end local v2    # "fileCreated":Z
    :goto_1
    return-void

    .line 425
    .end local v1    # "file":Ljava/io/File;
    .end local v3    # "fileName":Ljava/lang/String;
    .end local v6    # "writer":Ljava/io/FileWriter;
    :cond_2
    const-string v8, ""

    goto :goto_0

    .line 441
    .restart local v1    # "file":Ljava/io/File;
    .restart local v3    # "fileName":Ljava/lang/String;
    .restart local v6    # "writer":Ljava/io/FileWriter;
    :catch_0
    move-exception v0

    .line 442
    .local v0, "e":Ljava/io/IOException;
    :goto_2
    :try_start_2
    sget-object v8, Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Error while dumping the issue "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 444
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_1

    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    :goto_3
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v8

    .end local v6    # "writer":Ljava/io/FileWriter;
    .restart local v2    # "fileCreated":Z
    :catchall_1
    move-exception v8

    move-object v6, v7

    .restart local v6    # "writer":Ljava/io/FileWriter;
    goto :goto_3

    .line 441
    .end local v6    # "writer":Ljava/io/FileWriter;
    :catch_1
    move-exception v0

    move-object v6, v7

    .restart local v6    # "writer":Ljava/io/FileWriter;
    goto :goto_2
.end method

.method public static scheduleSync()V
    .locals 8

    .prologue
    .line 942
    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    const-class v6, Lcom/navdy/hud/app/util/ReportIssueService;

    invoke-direct {v1, v3, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 943
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "Sync"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 944
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    const/16 v6, 0x80

    const/high16 v7, 0x10000000

    invoke-static {v3, v6, v1, v7}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 945
    .local v2, "pendingIntent":Landroid/app/PendingIntent;
    sget-wide v4, Lcom/navdy/hud/app/util/ReportIssueService;->RETRY_INTERVAL:J

    .line 946
    .local v4, "triggerTimeMillis":J
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    const-string v6, "alarm"

    invoke-virtual {v3, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 947
    .local v0, "am":Landroid/app/AlarmManager;
    const/4 v3, 0x3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    add-long/2addr v6, v4

    invoke-virtual {v0, v3, v6, v7, v2}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 948
    return-void
.end method

.method public static showSnapshotMenu()V
    .locals 6

    .prologue
    .line 1000
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1001
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "MENU_MODE"

    sget-object v3, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;->SNAPSHOT_TITLE_PICKER:Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;

    invoke-virtual {v3}, Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$MenuMode;->ordinal()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1002
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getBus()Lcom/squareup/otto/Bus;

    move-result-object v1

    .line 1003
    .local v1, "bus":Lcom/squareup/otto/Bus;
    new-instance v2, Lcom/navdy/hud/app/event/ShowScreenWithArgs;

    sget-object v3, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_MAIN_MENU:Lcom/navdy/service/library/events/ui/Screen;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {v2, v3, v0, v4, v5}, Lcom/navdy/hud/app/event/ShowScreenWithArgs;-><init>(Lcom/navdy/service/library/events/ui/Screen;Landroid/os/Bundle;Ljava/lang/Object;Z)V

    invoke-virtual {v1, v2}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 1004
    return-void
.end method

.method private submitJiraTicket(Ljava/lang/String;)V
    .locals 22
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 224
    sget-object v19, Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Submitting jira ticket with title "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 226
    :try_start_0
    new-instance v6, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    move-wide/from16 v0, v20

    invoke-direct {v6, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 227
    .local v6, "date":Ljava/util/Date;
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getConnectionHandler()Lcom/navdy/hud/app/service/ConnectionHandler;

    move-result-object v5

    .line 229
    .local v5, "connectionHandler":Lcom/navdy/hud/app/service/ConnectionHandler;
    new-instance v12, Lorg/json/JSONObject;

    invoke-direct {v12}, Lorg/json/JSONObject;-><init>()V

    .line 230
    .local v12, "jsonObject":Lorg/json/JSONObject;
    const-string v19, "attachment"

    sget-object v20, Lcom/navdy/hud/app/util/ReportIssueService;->lastSnapShotFile:Ljava/io/File;

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v12, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 231
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    .line 232
    .local v16, "subjectBuilder":Ljava/lang/StringBuilder;
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getUiStateManager()Lcom/navdy/hud/app/ui/framework/UIStateManager;

    move-result-object v18

    .line 233
    .local v18, "uiStateManager":Lcom/navdy/hud/app/ui/framework/UIStateManager;
    sget-object v19, Lcom/navdy/hud/app/util/ReportIssueService;->SNAPSHOT_JIRA_TICKET_DATE_FORMAT:Ljava/text/SimpleDateFormat;

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v17

    .line 234
    .local v17, "timeStamp":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v8

    .line 235
    .local v8, "driverProfile":Lcom/navdy/hud/app/profile/DriverProfile;
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "[Snapshot "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "] "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    sget-object v19, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->MAP:Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    invoke-virtual/range {v18 .. v18}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->getHomescreenView()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;->getDisplayMode()Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/navdy/hud/app/ui/component/homescreen/HomeScreen$DisplayMode;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_5

    const-string v19, "Map"

    :goto_0
    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 236
    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapUtil;->getCurrentRoadName()Ljava/lang/String;

    move-result-object v15

    .line 237
    .local v15, "roadName":Ljava/lang/String;
    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_0

    .line 238
    const-string v19, " "

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 240
    :cond_0
    invoke-virtual {v8}, Lcom/navdy/hud/app/profile/DriverProfile;->isDefaultProfile()Z

    move-result v19

    if-nez v19, :cond_1

    .line 241
    const-string v19, " , From "

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v8}, Lcom/navdy/hud/app/profile/DriverProfile;->getDriverEmail()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 243
    :cond_1
    const-string v19, "Summary"

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v12, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 245
    invoke-virtual {v8}, Lcom/navdy/hud/app/profile/DriverProfile;->isDefaultProfile()Z

    move-result v19

    if-nez v19, :cond_3

    .line 246
    invoke-virtual {v8}, Lcom/navdy/hud/app/profile/DriverProfile;->getDriverName()Ljava/lang/String;

    move-result-object v7

    .line 247
    .local v7, "driverName":Ljava/lang/String;
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_2

    .line 249
    const-string v19, " "

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v14

    .line 250
    .local v14, "nameParts":[Ljava/lang/String;
    const/16 v19, 0x0

    aget-object v19, v14, v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 251
    .local v4, "assigneeName":Ljava/lang/String;
    sget-object v19, Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Assignee name for the snapshot "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 252
    const-string v19, "assignee"

    move-object/from16 v0, v19

    invoke-virtual {v12, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 254
    .end local v4    # "assigneeName":Ljava/lang/String;
    .end local v14    # "nameParts":[Ljava/lang/String;
    :cond_2
    invoke-virtual {v8}, Lcom/navdy/hud/app/profile/DriverProfile;->getDriverEmail()Ljava/lang/String;

    move-result-object v10

    .line 255
    .local v10, "email":Ljava/lang/String;
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_3

    .line 256
    const-string v19, "assigneeEmail"

    move-object/from16 v0, v19

    invoke-virtual {v12, v0, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 260
    .end local v7    # "driverName":Ljava/lang/String;
    .end local v10    # "email":Ljava/lang/String;
    :cond_3
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 261
    .local v11, "environmentBuilder":Ljava/lang/StringBuilder;
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "HUD "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    sget-object v20, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\n"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 262
    if-eqz v5, :cond_4

    .line 263
    invoke-virtual {v5}, Lcom/navdy/hud/app/service/ConnectionHandler;->getLastConnectedDeviceInfo()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v13

    .line 264
    .local v13, "lastConnectedDeviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    if-eqz v13, :cond_4

    .line 265
    sget-object v19, Lcom/navdy/service/library/events/DeviceInfo$Platform;->PLATFORM_iOS:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    iget-object v0, v13, Lcom/navdy/service/library/events/DeviceInfo;->platform:Lcom/navdy/service/library/events/DeviceInfo$Platform;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Lcom/navdy/service/library/events/DeviceInfo$Platform;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_6

    const-string v19, "iPhone"

    :goto_1
    move-object/from16 v0, v19

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, " V"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    iget-object v0, v13, Lcom/navdy/service/library/events/DeviceInfo;->clientVersion:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\n"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 269
    .end local v13    # "lastConnectedDeviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    :cond_4
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Language : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/navdy/hud/app/profile/HudLocale;->getCurrentLocale(Landroid/content/Context;)Ljava/util/Locale;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    const-string v19, "Environment"

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v12, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 271
    invoke-virtual {v12}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v19

    const/16 v20, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/navdy/hud/app/util/ReportIssueService;->saveJiraTicketDescriptionToFile(Ljava/lang/String;Z)V

    .line 275
    .end local v5    # "connectionHandler":Lcom/navdy/hud/app/service/ConnectionHandler;
    .end local v6    # "date":Ljava/util/Date;
    .end local v8    # "driverProfile":Lcom/navdy/hud/app/profile/DriverProfile;
    .end local v11    # "environmentBuilder":Ljava/lang/StringBuilder;
    .end local v12    # "jsonObject":Lorg/json/JSONObject;
    .end local v15    # "roadName":Ljava/lang/String;
    .end local v16    # "subjectBuilder":Ljava/lang/StringBuilder;
    .end local v17    # "timeStamp":Ljava/lang/String;
    .end local v18    # "uiStateManager":Lcom/navdy/hud/app/ui/framework/UIStateManager;
    :goto_2
    return-void

    .line 235
    .restart local v5    # "connectionHandler":Lcom/navdy/hud/app/service/ConnectionHandler;
    .restart local v6    # "date":Ljava/util/Date;
    .restart local v8    # "driverProfile":Lcom/navdy/hud/app/profile/DriverProfile;
    .restart local v12    # "jsonObject":Lorg/json/JSONObject;
    .restart local v16    # "subjectBuilder":Ljava/lang/StringBuilder;
    .restart local v17    # "timeStamp":Ljava/lang/String;
    .restart local v18    # "uiStateManager":Lcom/navdy/hud/app/ui/framework/UIStateManager;
    :cond_5
    const-string v19, "Dash"

    goto/16 :goto_0

    .line 265
    .restart local v11    # "environmentBuilder":Ljava/lang/StringBuilder;
    .restart local v13    # "lastConnectedDeviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    .restart local v15    # "roadName":Ljava/lang/String;
    :cond_6
    const-string v19, "Android"
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 272
    .end local v5    # "connectionHandler":Lcom/navdy/hud/app/service/ConnectionHandler;
    .end local v6    # "date":Ljava/util/Date;
    .end local v8    # "driverProfile":Lcom/navdy/hud/app/profile/DriverProfile;
    .end local v11    # "environmentBuilder":Ljava/lang/StringBuilder;
    .end local v12    # "jsonObject":Lorg/json/JSONObject;
    .end local v13    # "lastConnectedDeviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    .end local v15    # "roadName":Ljava/lang/String;
    .end local v16    # "subjectBuilder":Ljava/lang/StringBuilder;
    .end local v17    # "timeStamp":Ljava/lang/String;
    .end local v18    # "uiStateManager":Lcom/navdy/hud/app/ui/framework/UIStateManager;
    :catch_0
    move-exception v9

    .line 273
    .local v9, "e":Lorg/json/JSONException;
    invoke-virtual {v9}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2
.end method

.method public static submitJiraTicketAsync(Ljava/lang/String;)V
    .locals 3
    .param p0, "title"    # Ljava/lang/String;

    .prologue
    .line 929
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/hud/app/util/ReportIssueService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 930
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "Jira"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 931
    const-string v1, "EXTRA_SNAPSHOT_TITLE"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 932
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 933
    return-void
.end method

.method private sync()V
    .locals 42

    .prologue
    .line 464
    invoke-static {}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->getInstance()Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;

    move-result-object v2

    sget-object v3, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;->JIRA:Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;

    invoke-virtual {v2, v3}, Lcom/navdy/hud/app/framework/network/NetworkBandwidthController;->isNetworkAccessAllowed(Lcom/navdy/hud/app/framework/network/NetworkBandwidthController$Component;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 465
    invoke-static {}, Lcom/navdy/hud/app/util/ReportIssueService;->scheduleSync()V

    .line 644
    :cond_0
    :goto_0
    return-void

    .line 468
    :cond_1
    invoke-static {}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->getInstance()Lcom/navdy/hud/app/manager/RemoteDeviceManager;

    move-result-object v39

    .line 469
    .local v39, "remoteDeviceManager":Lcom/navdy/hud/app/manager/RemoteDeviceManager;
    invoke-virtual/range {v39 .. v39}, Lcom/navdy/hud/app/manager/RemoteDeviceManager;->isRemoteDeviceConnected()Z

    move-result v26

    .line 470
    .local v26, "deviceConnected":Z
    invoke-static/range {p0 .. p0}, Lcom/navdy/hud/app/framework/network/NetworkStateManager;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v30

    .line 471
    .local v30, "internetConnected":Z
    sget-object v2, Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Trying to sync , already syncing:"

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v9, Lcom/navdy/hud/app/util/ReportIssueService;->mSyncing:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v9

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v9, ", Navigation mode on:"

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->getInstance()Lcom/navdy/hud/app/maps/here/HereMapsManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/navdy/hud/app/maps/here/HereMapsManager;->isNavigationModeOn()Z

    move-result v9

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v9, ", Device connected:"

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v9, ", Internet connected :"

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 473
    if-eqz v26, :cond_f

    if-eqz v30, :cond_f

    .line 474
    sget-object v2, Lcom/navdy/hud/app/util/ReportIssueService;->mSyncing:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x0

    const/4 v9, 0x1

    invoke-virtual {v2, v3, v9}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 475
    sget-object v2, Lcom/navdy/hud/app/util/ReportIssueService;->mIssuesToSync:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/PriorityBlockingQueue;->peek()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/io/File;

    .line 476
    .local v5, "fileToSync":Ljava/io/File;
    if-eqz v5, :cond_e

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 477
    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 479
    :try_start_0
    sget-object v2, Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Submitting ticket from JSON file"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 480
    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/navdy/service/library/util/IOUtils;->convertFileToString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    .line 481
    .local v33, "jsonData":Ljava/lang/String;
    new-instance v4, Lorg/json/JSONObject;

    move-object/from16 v0, v33

    invoke-direct {v4, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 482
    .local v4, "jsonObject":Lorg/json/JSONObject;
    const-string v2, "Summary"

    invoke-virtual {v4, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 484
    .local v12, "summary":Ljava/lang/String;
    const-string v2, "Environment"

    invoke-virtual {v4, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "Environment"

    invoke-virtual {v4, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 485
    .local v14, "environment":Ljava/lang/String;
    :goto_1
    const-string v2, "assignee"

    invoke-virtual {v4, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "assignee"

    invoke-virtual {v4, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 486
    .local v22, "assigneedNameTemp":Ljava/lang/String;
    :goto_2
    const-string v2, "assigneeEmail"

    invoke-virtual {v4, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, "assigneeEmail"

    invoke-virtual {v4, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 487
    .local v21, "assigneeEmailTemp":Ljava/lang/String;
    :goto_3
    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 488
    invoke-static {}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getInstance()Lcom/navdy/hud/app/framework/DriverProfileHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/hud/app/framework/DriverProfileHelper;->getCurrentProfile()Lcom/navdy/hud/app/profile/DriverProfile;

    move-result-object v36

    .line 489
    .local v36, "profile":Lcom/navdy/hud/app/profile/DriverProfile;
    invoke-virtual/range {v36 .. v36}, Lcom/navdy/hud/app/profile/DriverProfile;->getDriverName()Ljava/lang/String;

    move-result-object v27

    .line 490
    .local v27, "driverName":Ljava/lang/String;
    invoke-static/range {v27 .. v27}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 492
    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v35

    .line 493
    .local v35, "nameParts":[Ljava/lang/String;
    const/4 v2, 0x0

    aget-object v22, v35, v2

    .line 495
    .end local v35    # "nameParts":[Ljava/lang/String;
    :cond_2
    invoke-virtual/range {v36 .. v36}, Lcom/navdy/hud/app/profile/DriverProfile;->getDriverEmail()Ljava/lang/String;

    move-result-object v21

    .line 497
    .end local v27    # "driverName":Ljava/lang/String;
    .end local v36    # "profile":Lcom/navdy/hud/app/profile/DriverProfile;
    :cond_3
    move-object/from16 v7, v22

    .line 498
    .local v7, "assigneeName":Ljava/lang/String;
    move-object/from16 v8, v21

    .line 499
    .local v8, "assigneeEmail":Ljava/lang/String;
    sget-object v2, Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Assignee name "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 500
    const-string v2, "attachment"

    invoke-virtual {v4, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v2, "attachment"

    invoke-virtual {v4, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 501
    .local v23, "attachment":Ljava/lang/String;
    :goto_4
    sget-object v2, Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Attachment "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v23

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 502
    new-instance v6, Ljava/io/File;

    move-object/from16 v0, v23

    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 503
    .local v6, "attachmentFile":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_8

    .line 504
    sget-object v2, Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Snapshot file has been deleted, not creating a ticket "

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 505
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/navdy/hud/app/util/ReportIssueService;->onSyncComplete(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_0

    .line 586
    .end local v4    # "jsonObject":Lorg/json/JSONObject;
    .end local v6    # "attachmentFile":Ljava/io/File;
    .end local v7    # "assigneeName":Ljava/lang/String;
    .end local v8    # "assigneeEmail":Ljava/lang/String;
    .end local v12    # "summary":Ljava/lang/String;
    .end local v14    # "environment":Ljava/lang/String;
    .end local v21    # "assigneeEmailTemp":Ljava/lang/String;
    .end local v22    # "assigneedNameTemp":Ljava/lang/String;
    .end local v23    # "attachment":Ljava/lang/String;
    .end local v33    # "jsonData":Ljava/lang/String;
    :catch_0
    move-exception v28

    .line 587
    .local v28, "e":Ljava/io/IOException;
    sget-object v2, Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Not able to read json file "

    move-object/from16 v0, v28

    invoke-virtual {v2, v3, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 588
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/util/ReportIssueService;->syncLater()V

    goto/16 :goto_0

    .line 484
    .end local v28    # "e":Ljava/io/IOException;
    .restart local v4    # "jsonObject":Lorg/json/JSONObject;
    .restart local v12    # "summary":Ljava/lang/String;
    .restart local v33    # "jsonData":Ljava/lang/String;
    :cond_4
    :try_start_1
    const-string v14, ""

    goto/16 :goto_1

    .line 485
    .restart local v14    # "environment":Ljava/lang/String;
    :cond_5
    const-string v22, ""

    goto/16 :goto_2

    .line 486
    .restart local v22    # "assigneedNameTemp":Ljava/lang/String;
    :cond_6
    const-string v21, ""

    goto/16 :goto_3

    .line 500
    .restart local v7    # "assigneeName":Ljava/lang/String;
    .restart local v8    # "assigneeEmail":Ljava/lang/String;
    .restart local v21    # "assigneeEmailTemp":Ljava/lang/String;
    :cond_7
    const-string v23, ""

    goto :goto_4

    .line 508
    .restart local v6    # "attachmentFile":Ljava/io/File;
    .restart local v23    # "attachment":Ljava/lang/String;
    :cond_8
    const-string v2, "ticketId"

    invoke-virtual {v4, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    const-string v2, "ticketId"

    invoke-virtual {v4, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v41

    .line 509
    .local v41, "ticketId":Ljava/lang/String;
    :goto_5
    invoke-static/range {v41 .. v41}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 510
    sget-object v2, Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Ticket already exists "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v41

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v9, " Attaching the files to the ticket"

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 512
    new-instance v2, Lcom/navdy/hud/app/util/ReportIssueService$1;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v5}, Lcom/navdy/hud/app/util/ReportIssueService$1;-><init>(Lcom/navdy/hud/app/util/ReportIssueService;Ljava/io/File;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v41

    invoke-direct {v0, v1, v6, v2}, Lcom/navdy/hud/app/util/ReportIssueService;->attachFilesToTheJiraTicket(Ljava/lang/String;Ljava/io/File;Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 590
    .end local v4    # "jsonObject":Lorg/json/JSONObject;
    .end local v6    # "attachmentFile":Ljava/io/File;
    .end local v7    # "assigneeName":Ljava/lang/String;
    .end local v8    # "assigneeEmail":Ljava/lang/String;
    .end local v12    # "summary":Ljava/lang/String;
    .end local v14    # "environment":Ljava/lang/String;
    .end local v21    # "assigneeEmailTemp":Ljava/lang/String;
    .end local v22    # "assigneedNameTemp":Ljava/lang/String;
    .end local v23    # "attachment":Ljava/lang/String;
    .end local v33    # "jsonData":Ljava/lang/String;
    .end local v41    # "ticketId":Ljava/lang/String;
    :catch_1
    move-exception v32

    .line 591
    .local v32, "je":Lorg/json/JSONException;
    sget-object v2, Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Not able to read json file "

    move-object/from16 v0, v32

    invoke-virtual {v2, v3, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 592
    invoke-direct/range {p0 .. p0}, Lcom/navdy/hud/app/util/ReportIssueService;->syncLater()V

    goto/16 :goto_0

    .line 508
    .end local v32    # "je":Lorg/json/JSONException;
    .restart local v4    # "jsonObject":Lorg/json/JSONObject;
    .restart local v6    # "attachmentFile":Ljava/io/File;
    .restart local v7    # "assigneeName":Ljava/lang/String;
    .restart local v8    # "assigneeEmail":Ljava/lang/String;
    .restart local v12    # "summary":Ljava/lang/String;
    .restart local v14    # "environment":Ljava/lang/String;
    .restart local v21    # "assigneeEmailTemp":Ljava/lang/String;
    .restart local v22    # "assigneedNameTemp":Ljava/lang/String;
    .restart local v23    # "attachment":Ljava/lang/String;
    .restart local v33    # "jsonData":Ljava/lang/String;
    :cond_9
    :try_start_2
    const-string v41, ""

    goto :goto_5

    .line 526
    .restart local v41    # "ticketId":Ljava/lang/String;
    :cond_a
    sget-object v2, Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Submitting a new ticket from JSON file"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 527
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/navdy/hud/app/util/ReportIssueService;->mJiraClient:Lcom/navdy/service/library/network/http/services/JiraClient;

    const-string v10, "HUD"

    const-string v11, "Task"

    const-string v13, "Snapshot, fill in the details"

    new-instance v2, Lcom/navdy/hud/app/util/ReportIssueService$2;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v8}, Lcom/navdy/hud/app/util/ReportIssueService$2;-><init>(Lcom/navdy/hud/app/util/ReportIssueService;Lorg/json/JSONObject;Ljava/io/File;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    move-object v15, v2

    invoke-virtual/range {v9 .. v15}, Lcom/navdy/service/library/network/http/services/JiraClient;->submitTicket(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 596
    .end local v4    # "jsonObject":Lorg/json/JSONObject;
    .end local v6    # "attachmentFile":Ljava/io/File;
    .end local v7    # "assigneeName":Ljava/lang/String;
    .end local v8    # "assigneeEmail":Ljava/lang/String;
    .end local v12    # "summary":Ljava/lang/String;
    .end local v14    # "environment":Ljava/lang/String;
    .end local v21    # "assigneeEmailTemp":Ljava/lang/String;
    .end local v22    # "assigneedNameTemp":Ljava/lang/String;
    .end local v23    # "attachment":Ljava/lang/String;
    .end local v33    # "jsonData":Ljava/lang/String;
    .end local v41    # "ticketId":Ljava/lang/String;
    :cond_b
    const/16 v37, 0x0

    .line 598
    .local v37, "reader":Ljava/io/FileReader;
    :try_start_3
    new-instance v38, Ljava/io/FileReader;

    move-object/from16 v0, v38

    invoke-direct {v0, v5}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 599
    .end local v37    # "reader":Ljava/io/FileReader;
    .local v38, "reader":Ljava/io/FileReader;
    :try_start_4
    new-instance v24, Ljava/io/BufferedReader;

    move-object/from16 v0, v24

    move-object/from16 v1, v38

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 600
    .local v24, "buffReader":Ljava/io/BufferedReader;
    invoke-virtual/range {v24 .. v24}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v40

    .line 601
    .local v40, "summaryLine":Ljava/lang/String;
    invoke-virtual/range {v40 .. v40}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v29

    .line 602
    .local v29, "id":I
    invoke-static/range {v29 .. v29}, Lcom/navdy/hud/app/util/ReportIssueService;->getIssueTypeForId(I)Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    move-result-object v31

    .line 603
    .local v31, "issueType":Lcom/navdy/hud/app/util/ReportIssueService$IssueType;
    if-nez v31, :cond_c

    .line 604
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Bad file format"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    .line 630
    .end local v24    # "buffReader":Ljava/io/BufferedReader;
    .end local v29    # "id":I
    .end local v31    # "issueType":Lcom/navdy/hud/app/util/ReportIssueService$IssueType;
    .end local v40    # "summaryLine":Ljava/lang/String;
    :catch_2
    move-exception v28

    move-object/from16 v37, v38

    .line 631
    .end local v38    # "reader":Ljava/io/FileReader;
    .local v28, "e":Ljava/lang/Exception;
    .restart local v37    # "reader":Ljava/io/FileReader;
    :goto_6
    invoke-static/range {v37 .. v37}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 632
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/navdy/hud/app/util/ReportIssueService;->onSyncComplete(Ljava/io/File;)V

    goto/16 :goto_0

    .line 606
    .end local v28    # "e":Ljava/lang/Exception;
    .end local v37    # "reader":Ljava/io/FileReader;
    .restart local v24    # "buffReader":Ljava/io/BufferedReader;
    .restart local v29    # "id":I
    .restart local v31    # "issueType":Lcom/navdy/hud/app/util/ReportIssueService$IssueType;
    .restart local v38    # "reader":Ljava/io/FileReader;
    .restart local v40    # "summaryLine":Ljava/lang/String;
    :cond_c
    :try_start_5
    move-object/from16 v0, p0

    move-object/from16 v1, v31

    invoke-direct {v0, v1}, Lcom/navdy/hud/app/util/ReportIssueService;->getSummary(Lcom/navdy/hud/app/util/ReportIssueService$IssueType;)Ljava/lang/String;

    move-result-object v12

    .line 607
    .restart local v12    # "summary":Ljava/lang/String;
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    .line 609
    .local v25, "builder":Ljava/lang/StringBuilder;
    :goto_7
    invoke-virtual/range {v24 .. v24}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v34

    .local v34, "line":Ljava/lang/String;
    if-eqz v34, :cond_d

    .line 610
    move-object/from16 v0, v25

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7

    .line 612
    :cond_d
    invoke-static/range {v38 .. v38}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 613
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 614
    .local v19, "description":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/hud/app/util/ReportIssueService;->mJiraClient:Lcom/navdy/service/library/network/http/services/JiraClient;

    const-string v16, "NP"

    const-string v17, "Task"

    new-instance v20, Lcom/navdy/hud/app/util/ReportIssueService$3;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v5}, Lcom/navdy/hud/app/util/ReportIssueService$3;-><init>(Lcom/navdy/hud/app/util/ReportIssueService;Ljava/io/File;)V

    move-object/from16 v18, v12

    invoke-virtual/range {v15 .. v20}, Lcom/navdy/service/library/network/http/services/JiraClient;->submitTicket(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    move-object/from16 v37, v38

    .line 633
    .end local v38    # "reader":Ljava/io/FileReader;
    .restart local v37    # "reader":Ljava/io/FileReader;
    goto/16 :goto_0

    .line 636
    .end local v12    # "summary":Ljava/lang/String;
    .end local v19    # "description":Ljava/lang/String;
    .end local v24    # "buffReader":Ljava/io/BufferedReader;
    .end local v25    # "builder":Ljava/lang/StringBuilder;
    .end local v29    # "id":I
    .end local v31    # "issueType":Lcom/navdy/hud/app/util/ReportIssueService$IssueType;
    .end local v34    # "line":Ljava/lang/String;
    .end local v37    # "reader":Ljava/io/FileReader;
    .end local v40    # "summaryLine":Ljava/lang/String;
    :cond_e
    sget-object v2, Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Ticket file does not exist anymore"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 637
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/navdy/hud/app/util/ReportIssueService;->onSyncComplete(Ljava/io/File;)V

    goto/16 :goto_0

    .line 641
    .end local v5    # "fileToSync":Ljava/io/File;
    :cond_f
    sget-object v2, Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Already sync is running, scheduling for a later time"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 642
    invoke-static {}, Lcom/navdy/hud/app/util/ReportIssueService;->scheduleSync()V

    goto/16 :goto_0

    .line 630
    .restart local v5    # "fileToSync":Ljava/io/File;
    .restart local v37    # "reader":Ljava/io/FileReader;
    :catch_3
    move-exception v28

    goto :goto_6
.end method

.method private syncLater()V
    .locals 2

    .prologue
    .line 458
    sget-object v0, Lcom/navdy/hud/app/util/ReportIssueService;->mSyncing:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 459
    invoke-static {}, Lcom/navdy/hud/app/util/ReportIssueService;->scheduleSync()V

    .line 460
    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x78

    .line 190
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 191
    invoke-static {}, Lcom/navdy/hud/app/HudApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, p0}, Lmortar/Mortar;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 192
    sget-object v2, Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "ReportIssue service started"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 193
    iget-object v2, p0, Lcom/navdy/hud/app/util/ReportIssueService;->mHttpManager:Lcom/navdy/service/library/network/http/IHttpManager;

    invoke-interface {v2}, Lcom/navdy/service/library/network/http/IHttpManager;->getClientCopy()Lokhttp3/OkHttpClient$Builder;

    move-result-object v2

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v4, v5, v3}, Lokhttp3/OkHttpClient$Builder;->readTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v2

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v4, v5, v3}, Lokhttp3/OkHttpClient$Builder;->connectTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object v0

    .line 194
    .local v0, "client":Lokhttp3/OkHttpClient;
    new-instance v2, Lcom/navdy/service/library/network/http/services/JiraClient;

    invoke-direct {v2, v0}, Lcom/navdy/service/library/network/http/services/JiraClient;-><init>(Lokhttp3/OkHttpClient;)V

    iput-object v2, p0, Lcom/navdy/hud/app/util/ReportIssueService;->mJiraClient:Lcom/navdy/service/library/network/http/services/JiraClient;

    .line 195
    const-string v2, "JIRA_CREDENTIALS"

    invoke-static {p0, v2}, Lcom/navdy/service/library/util/CredentialUtil;->getCredentials(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 196
    .local v1, "encodedCredential":Ljava/lang/String;
    iget-object v2, p0, Lcom/navdy/hud/app/util/ReportIssueService;->mJiraClient:Lcom/navdy/service/library/network/http/services/JiraClient;

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/network/http/services/JiraClient;->setEncodedCredentials(Ljava/lang/String;)V

    .line 197
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 202
    :try_start_0
    sget-object v3, Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onHandleIntent "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 203
    const-string v3, "Dump"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 204
    const-string v3, "EXTRA_ISSUE_TYPE"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    .line 205
    .local v0, "issueType":Lcom/navdy/hud/app/util/ReportIssueService$IssueType;
    sget-object v3, Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Dumping an issue "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 206
    if-eqz v0, :cond_0

    .line 207
    invoke-direct {p0, v0}, Lcom/navdy/hud/app/util/ReportIssueService;->dumpIssue(Lcom/navdy/hud/app/util/ReportIssueService$IssueType;)V

    .line 221
    .end local v0    # "issueType":Lcom/navdy/hud/app/util/ReportIssueService$IssueType;
    :cond_0
    :goto_0
    return-void

    .line 209
    :cond_1
    const-string v3, "Snapshot"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 210
    invoke-direct {p0}, Lcom/navdy/hud/app/util/ReportIssueService;->dumpSnapshot()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 218
    :catch_0
    move-exception v1

    .line 219
    .local v1, "t":Ljava/lang/Throwable;
    sget-object v3, Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Exception while handling intent "

    invoke-virtual {v3, v4, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 211
    .end local v1    # "t":Ljava/lang/Throwable;
    :cond_2
    :try_start_1
    const-string v3, "Jira"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 212
    const-string v3, "EXTRA_SNAPSHOT_TITLE"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 213
    .local v2, "title":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v2, "HUD"

    .end local v2    # "title":Ljava/lang/String;
    :cond_3
    invoke-direct {p0, v2}, Lcom/navdy/hud/app/util/ReportIssueService;->submitJiraTicket(Ljava/lang/String;)V

    goto :goto_0

    .line 215
    :cond_4
    sget-object v3, Lcom/navdy/hud/app/util/ReportIssueService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Handling Sync request"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 216
    invoke-direct {p0}, Lcom/navdy/hud/app/util/ReportIssueService;->sync()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
