.class public final Lcom/navdy/hud/app/util/HeadingDataUtil;
.super Ljava/lang/Object;
.source "HeadingDataUtil.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x1
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\u0006\n\u0002\u0008\t\n\u0002\u0010\t\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0017\u001a\u00020\u0018R\u0014\u0010\u0003\u001a\u00020\u0004X\u0086D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R$\u0010\t\u001a\u00020\u00082\u0006\u0010\u0007\u001a\u00020\u0008@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\n\u0010\u000b\"\u0004\u0008\u000c\u0010\rR\u001a\u0010\u000e\u001a\u00020\u0008X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000f\u0010\u000b\"\u0004\u0008\u0010\u0010\rR\u001a\u0010\u0011\u001a\u00020\u0012X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014\"\u0004\u0008\u0015\u0010\u0016\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/navdy/hud/app/util/HeadingDataUtil;",
        "",
        "()V",
        "HEADING_EXPIRE_INTERVAL",
        "",
        "getHEADING_EXPIRE_INTERVAL",
        "()I",
        "value",
        "",
        "heading",
        "getHeading",
        "()D",
        "setHeading",
        "(D)V",
        "lastHeading",
        "getLastHeading",
        "setLastHeading",
        "lastHeadingSampleTime",
        "",
        "getLastHeadingSampleTime",
        "()J",
        "setLastHeadingSampleTime",
        "(J)V",
        "reset",
        "",
        "app_hudRelease"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x6
    }
.end annotation


# instance fields
.field private final HEADING_EXPIRE_INTERVAL:I

.field private heading:D

.field private lastHeading:D

.field private lastHeadingSampleTime:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const/16 v0, 0xbb8

    iput v0, p0, Lcom/navdy/hud/app/util/HeadingDataUtil;->HEADING_EXPIRE_INTERVAL:I

    return-void
.end method


# virtual methods
.method public final getHEADING_EXPIRE_INTERVAL()I
    .locals 1

    .prologue
    .line 14
    iget v0, p0, Lcom/navdy/hud/app/util/HeadingDataUtil;->HEADING_EXPIRE_INTERVAL:I

    return v0
.end method

.method public final getHeading()D
    .locals 2

    .prologue
    .line 18
    iget-wide v0, p0, Lcom/navdy/hud/app/util/HeadingDataUtil;->heading:D

    return-wide v0
.end method

.method public final getLastHeading()D
    .locals 2

    .prologue
    .line 16
    iget-wide v0, p0, Lcom/navdy/hud/app/util/HeadingDataUtil;->lastHeading:D

    return-wide v0
.end method

.method public final getLastHeadingSampleTime()J
    .locals 2

    .prologue
    .line 15
    iget-wide v0, p0, Lcom/navdy/hud/app/util/HeadingDataUtil;->lastHeadingSampleTime:J

    return-wide v0
.end method

.method public final reset()V
    .locals 2

    .prologue
    .line 30
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/navdy/hud/app/util/HeadingDataUtil;->lastHeading:D

    .line 31
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/navdy/hud/app/util/HeadingDataUtil;->lastHeadingSampleTime:J

    .line 32
    return-void
.end method

.method public final setHeading(D)V
    .locals 9
    .param p1, "value"    # D

    .prologue
    .line 20
    invoke-static {}, Lcom/navdy/hud/app/manager/SpeedManager;->getInstance()Lcom/navdy/hud/app/manager/SpeedManager;

    move-result-object v2

    .line 21
    .local v2, "speedManager":Lcom/navdy/hud/app/manager/SpeedManager;
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 22
    .local v0, "currentTime":J
    invoke-virtual {v2}, Lcom/navdy/hud/app/manager/SpeedManager;->getCurrentSpeed()I

    move-result v3

    const/4 v4, 0x1

    if-lt v3, v4, :cond_1

    iget-wide v4, p0, Lcom/navdy/hud/app/util/HeadingDataUtil;->lastHeading:D

    const-wide/16 v6, 0x0

    cmpg-double v3, v4, v6

    if-eqz v3, :cond_0

    iget-wide v4, p0, Lcom/navdy/hud/app/util/HeadingDataUtil;->heading:D

    iget-wide v6, p0, Lcom/navdy/hud/app/util/HeadingDataUtil;->lastHeading:D

    sub-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    const/16 v3, 0x78

    int-to-double v6, v3

    cmpg-double v3, v4, v6

    if-ltz v3, :cond_0

    iget-wide v4, p0, Lcom/navdy/hud/app/util/HeadingDataUtil;->lastHeadingSampleTime:J

    sub-long v4, v0, v4

    iget v3, p0, Lcom/navdy/hud/app/util/HeadingDataUtil;->HEADING_EXPIRE_INTERVAL:I

    int-to-long v6, v3

    cmp-long v3, v4, v6

    if-lez v3, :cond_1

    .line 23
    :cond_0
    iput-wide p1, p0, Lcom/navdy/hud/app/util/HeadingDataUtil;->lastHeading:D

    .line 24
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/navdy/hud/app/util/HeadingDataUtil;->lastHeadingSampleTime:J

    .line 25
    iput-wide p1, p0, Lcom/navdy/hud/app/util/HeadingDataUtil;->heading:D

    .line 27
    :cond_1
    return-void
.end method

.method public final setLastHeading(D)V
    .locals 1
    .param p1, "<set-?>"    # D

    .prologue
    .line 16
    iput-wide p1, p0, Lcom/navdy/hud/app/util/HeadingDataUtil;->lastHeading:D

    return-void
.end method

.method public final setLastHeadingSampleTime(J)V
    .locals 1
    .param p1, "<set-?>"    # J

    .prologue
    .line 15
    iput-wide p1, p0, Lcom/navdy/hud/app/util/HeadingDataUtil;->lastHeadingSampleTime:J

    return-void
.end method
