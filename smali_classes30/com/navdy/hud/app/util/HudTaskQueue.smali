.class public Lcom/navdy/hud/app/util/HudTaskQueue;
.super Lcom/navdy/service/library/util/TaskQueue;
.source "HudTaskQueue.java"


# static fields
.field public static final DRIVE_LOGGING:I = 0x9

.field public static final FILE_DOWNLOAD:I = 0x5

.field public static final GENERAL_SERIAL:I = 0xa

.field public static final GLANCE_SERIAL:I = 0xe

.field public static final HERE_BACKGROUND:I = 0x2

.field public static final HERE_BACKGROUND_SERIAL:I = 0x3

.field public static final HERE_FUEL_MANAGER_SERIAL:I = 0x15

.field public static final HERE_LANE_INFO_SERIAL:I = 0xf

.field public static final HERE_MAP_ANIMATION_SERIAL:I = 0x11

.field public static final HERE_NAVIGATION_MANAGER_SERIAL:I = 0x14

.field public static final HERE_POSITION_SERIAL:I = 0x12

.field public static final HERE_ROUTE_MANAGER_SERIAL:I = 0x13

.field public static final HERE_TBT_SERIAL:I = 0x4

.field public static final HERE_ZOOM_SERIAL:I = 0x10

.field public static final IMAGE_CACHE_PROCESSING:I = 0x16

.field public static final IMAGE_DOWNLOAD:I = 0x7

.field public static final NETWORK_SERIAL:I = 0x17

.field public static final OBD_CONFIGURATION_SERIAL:I = 0xd

.field public static final OBD_STATUS:I = 0x6

.field public static final REMOTE_SEND_SERIAL:I = 0xb

.field public static final SPEED_WARNING_SERIAL:I = 0xc

.field public static final TRIP_TRACKING:I = 0x8


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/navdy/service/library/util/TaskQueue;-><init>()V

    return-void
.end method
