.class Lcom/navdy/hud/app/util/MusicArtworkCache$2;
.super Ljava/lang/Object;
.source "MusicArtworkCache.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/util/MusicArtworkCache;->getArtworkInternal(Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;Lcom/navdy/hud/app/util/MusicArtworkCache$Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/util/MusicArtworkCache;

.field final synthetic val$cacheEntryHelper:Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;

.field final synthetic val$callback:Lcom/navdy/hud/app/util/MusicArtworkCache$Callback;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/util/MusicArtworkCache;Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;Lcom/navdy/hud/app/util/MusicArtworkCache$Callback;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/util/MusicArtworkCache;

    .prologue
    .line 209
    iput-object p1, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$2;->this$0:Lcom/navdy/hud/app/util/MusicArtworkCache;

    iput-object p2, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$2;->val$cacheEntryHelper:Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;

    iput-object p3, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$2;->val$callback:Lcom/navdy/hud/app/util/MusicArtworkCache$Callback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 212
    iget-object v3, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$2;->this$0:Lcom/navdy/hud/app/util/MusicArtworkCache;

    iget-object v4, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$2;->val$cacheEntryHelper:Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;

    # invokes: Lcom/navdy/hud/app/util/MusicArtworkCache;->getFileNameFromDb(Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;)Ljava/lang/String;
    invoke-static {v3, v4}, Lcom/navdy/hud/app/util/MusicArtworkCache;->access$400(Lcom/navdy/hud/app/util/MusicArtworkCache;Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;)Ljava/lang/String;

    move-result-object v2

    .line 213
    .local v2, "fileName":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 214
    # getter for: Lcom/navdy/hud/app/util/MusicArtworkCache;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/MusicArtworkCache;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "No entry for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$2;->val$cacheEntryHelper:Lcom/navdy/hud/app/util/MusicArtworkCache$CacheEntryHelper;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 215
    iget-object v3, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$2;->val$callback:Lcom/navdy/hud/app/util/MusicArtworkCache$Callback;

    invoke-interface {v3}, Lcom/navdy/hud/app/util/MusicArtworkCache$Callback;->onMiss()V

    .line 230
    :goto_0
    return-void

    .line 218
    :cond_0
    invoke-static {}, Lcom/navdy/hud/app/util/picasso/PicassoUtil;->getDiskLruCache()Lcom/navdy/hud/app/storage/cache/DiskLruCache;

    move-result-object v1

    .line 219
    .local v1, "diskLruCache":Lcom/navdy/hud/app/storage/cache/DiskLruCache;
    if-nez v1, :cond_1

    .line 220
    # getter for: Lcom/navdy/hud/app/util/MusicArtworkCache;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/MusicArtworkCache;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "No disk cache"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 221
    iget-object v3, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$2;->val$callback:Lcom/navdy/hud/app/util/MusicArtworkCache$Callback;

    invoke-interface {v3}, Lcom/navdy/hud/app/util/MusicArtworkCache$Callback;->onMiss()V

    goto :goto_0

    .line 224
    :cond_1
    invoke-virtual {v1, v2}, Lcom/navdy/hud/app/storage/cache/DiskLruCache;->get(Ljava/lang/String;)[B

    move-result-object v0

    .line 225
    .local v0, "data":[B
    if-eqz v0, :cond_2

    .line 226
    iget-object v3, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$2;->val$callback:Lcom/navdy/hud/app/util/MusicArtworkCache$Callback;

    invoke-interface {v3, v0}, Lcom/navdy/hud/app/util/MusicArtworkCache$Callback;->onHit([B)V

    goto :goto_0

    .line 228
    :cond_2
    iget-object v3, p0, Lcom/navdy/hud/app/util/MusicArtworkCache$2;->val$callback:Lcom/navdy/hud/app/util/MusicArtworkCache$Callback;

    invoke-interface {v3}, Lcom/navdy/hud/app/util/MusicArtworkCache$Callback;->onMiss()V

    goto :goto_0
.end method
