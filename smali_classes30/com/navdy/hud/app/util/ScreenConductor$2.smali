.class Lcom/navdy/hud/app/util/ScreenConductor$2;
.super Ljava/lang/Object;
.source "ScreenConductor.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/util/ScreenConductor;->setAnimation(Lflow/Flow$Direction;Landroid/view/View;Landroid/view/View;Lcom/navdy/hud/app/screen/BaseScreen;Lcom/navdy/hud/app/screen/BaseScreen;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/util/ScreenConductor;

.field final synthetic val$newScreen:Lcom/navdy/hud/app/screen/BaseScreen;

.field final synthetic val$oldScreen:Lcom/navdy/hud/app/screen/BaseScreen;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/util/ScreenConductor;Lcom/navdy/hud/app/screen/BaseScreen;Lcom/navdy/hud/app/screen/BaseScreen;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/util/ScreenConductor;

    .prologue
    .line 195
    .local p0, "this":Lcom/navdy/hud/app/util/ScreenConductor$2;, "Lcom/navdy/hud/app/util/ScreenConductor$2;"
    iput-object p1, p0, Lcom/navdy/hud/app/util/ScreenConductor$2;->this$0:Lcom/navdy/hud/app/util/ScreenConductor;

    iput-object p2, p0, Lcom/navdy/hud/app/util/ScreenConductor$2;->val$newScreen:Lcom/navdy/hud/app/screen/BaseScreen;

    iput-object p3, p0, Lcom/navdy/hud/app/util/ScreenConductor$2;->val$oldScreen:Lcom/navdy/hud/app/screen/BaseScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 4
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 204
    .local p0, "this":Lcom/navdy/hud/app/util/ScreenConductor$2;, "Lcom/navdy/hud/app/util/ScreenConductor$2;"
    iget-object v0, p0, Lcom/navdy/hud/app/util/ScreenConductor$2;->val$newScreen:Lcom/navdy/hud/app/screen/BaseScreen;

    invoke-virtual {v0}, Lcom/navdy/hud/app/screen/BaseScreen;->onAnimationInEnd()V

    .line 205
    iget-object v0, p0, Lcom/navdy/hud/app/util/ScreenConductor$2;->this$0:Lcom/navdy/hud/app/util/ScreenConductor;

    iget-object v0, v0, Lcom/navdy/hud/app/util/ScreenConductor;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/navdy/hud/app/util/ScreenConductor$2;->val$newScreen:Lcom/navdy/hud/app/screen/BaseScreen;

    iget-object v3, p0, Lcom/navdy/hud/app/util/ScreenConductor$2;->val$oldScreen:Lcom/navdy/hud/app/screen/BaseScreen;

    invoke-virtual {v0, v1, v2, v3}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->postScreenAnimationEvent(ZLcom/navdy/hud/app/screen/BaseScreen;Lcom/navdy/hud/app/screen/BaseScreen;)V

    .line 206
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 209
    .local p0, "this":Lcom/navdy/hud/app/util/ScreenConductor$2;, "Lcom/navdy/hud/app/util/ScreenConductor$2;"
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 4
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 198
    .local p0, "this":Lcom/navdy/hud/app/util/ScreenConductor$2;, "Lcom/navdy/hud/app/util/ScreenConductor$2;"
    iget-object v0, p0, Lcom/navdy/hud/app/util/ScreenConductor$2;->val$newScreen:Lcom/navdy/hud/app/screen/BaseScreen;

    invoke-virtual {v0}, Lcom/navdy/hud/app/screen/BaseScreen;->onAnimationInStart()V

    .line 199
    iget-object v0, p0, Lcom/navdy/hud/app/util/ScreenConductor$2;->this$0:Lcom/navdy/hud/app/util/ScreenConductor;

    iget-object v0, v0, Lcom/navdy/hud/app/util/ScreenConductor;->uiStateManager:Lcom/navdy/hud/app/ui/framework/UIStateManager;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/navdy/hud/app/util/ScreenConductor$2;->val$newScreen:Lcom/navdy/hud/app/screen/BaseScreen;

    iget-object v3, p0, Lcom/navdy/hud/app/util/ScreenConductor$2;->val$oldScreen:Lcom/navdy/hud/app/screen/BaseScreen;

    invoke-virtual {v0, v1, v2, v3}, Lcom/navdy/hud/app/ui/framework/UIStateManager;->postScreenAnimationEvent(ZLcom/navdy/hud/app/screen/BaseScreen;Lcom/navdy/hud/app/screen/BaseScreen;)V

    .line 200
    return-void
.end method
