.class public Lcom/navdy/hud/app/util/CrashReporter$KernelCrashException;
.super Lcom/navdy/hud/app/util/CrashReporter$CrashException;
.source "CrashReporter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/util/CrashReporter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "KernelCrashException"
.end annotation


# static fields
.field private static final stackMatcher:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 131
    const-string v0, "PC is at\\s+([^\n]+\n)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/navdy/hud/app/util/CrashReporter$KernelCrashException;->stackMatcher:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 134
    invoke-direct {p0, p1}, Lcom/navdy/hud/app/util/CrashReporter$CrashException;-><init>(Ljava/lang/String;)V

    .line 135
    return-void
.end method


# virtual methods
.method public getLocationPattern()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 139
    sget-object v0, Lcom/navdy/hud/app/util/CrashReporter$KernelCrashException;->stackMatcher:Ljava/util/regex/Pattern;

    return-object v0
.end method
