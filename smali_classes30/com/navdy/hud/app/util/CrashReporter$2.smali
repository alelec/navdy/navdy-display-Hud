.class Lcom/navdy/hud/app/util/CrashReporter$2;
.super Ljava/lang/Object;
.source "CrashReporter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/util/CrashReporter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/util/CrashReporter;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/util/CrashReporter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/util/CrashReporter;

    .prologue
    .line 247
    iput-object p1, p0, Lcom/navdy/hud/app/util/CrashReporter$2;->this$0:Lcom/navdy/hud/app/util/CrashReporter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 250
    const/4 v0, 0x1

    .line 252
    .local v0, "checkAgain":Z
    :try_start_0
    iget-object v2, p0, Lcom/navdy/hud/app/util/CrashReporter$2;->this$0:Lcom/navdy/hud/app/util/CrashReporter;

    # invokes: Lcom/navdy/hud/app/util/CrashReporter;->checkForCrashes()Z
    invoke-static {v2}, Lcom/navdy/hud/app/util/CrashReporter;->access$300(Lcom/navdy/hud/app/util/CrashReporter;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 256
    if-eqz v0, :cond_0

    .line 257
    iget-object v2, p0, Lcom/navdy/hud/app/util/CrashReporter$2;->this$0:Lcom/navdy/hud/app/util/CrashReporter;

    # getter for: Lcom/navdy/hud/app/util/CrashReporter;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/navdy/hud/app/util/CrashReporter;->access$700(Lcom/navdy/hud/app/util/CrashReporter;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/util/CrashReporter$2;->this$0:Lcom/navdy/hud/app/util/CrashReporter;

    # getter for: Lcom/navdy/hud/app/util/CrashReporter;->periodicRunnable:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/navdy/hud/app/util/CrashReporter;->access$500(Lcom/navdy/hud/app/util/CrashReporter;)Ljava/lang/Runnable;

    move-result-object v3

    # getter for: Lcom/navdy/hud/app/util/CrashReporter;->PERIOD_DELIVERY_CHECK_INTERVAL:I
    invoke-static {}, Lcom/navdy/hud/app/util/CrashReporter;->access$600()I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 260
    :cond_0
    :goto_0
    return-void

    .line 253
    :catch_0
    move-exception v1

    .line 254
    .local v1, "t":Ljava/lang/Throwable;
    :try_start_1
    # getter for: Lcom/navdy/hud/app/util/CrashReporter;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/CrashReporter;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 256
    if-eqz v0, :cond_0

    .line 257
    iget-object v2, p0, Lcom/navdy/hud/app/util/CrashReporter$2;->this$0:Lcom/navdy/hud/app/util/CrashReporter;

    # getter for: Lcom/navdy/hud/app/util/CrashReporter;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/navdy/hud/app/util/CrashReporter;->access$700(Lcom/navdy/hud/app/util/CrashReporter;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/hud/app/util/CrashReporter$2;->this$0:Lcom/navdy/hud/app/util/CrashReporter;

    # getter for: Lcom/navdy/hud/app/util/CrashReporter;->periodicRunnable:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/navdy/hud/app/util/CrashReporter;->access$500(Lcom/navdy/hud/app/util/CrashReporter;)Ljava/lang/Runnable;

    move-result-object v3

    # getter for: Lcom/navdy/hud/app/util/CrashReporter;->PERIOD_DELIVERY_CHECK_INTERVAL:I
    invoke-static {}, Lcom/navdy/hud/app/util/CrashReporter;->access$600()I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 256
    .end local v1    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_1

    .line 257
    iget-object v3, p0, Lcom/navdy/hud/app/util/CrashReporter$2;->this$0:Lcom/navdy/hud/app/util/CrashReporter;

    # getter for: Lcom/navdy/hud/app/util/CrashReporter;->handler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/navdy/hud/app/util/CrashReporter;->access$700(Lcom/navdy/hud/app/util/CrashReporter;)Landroid/os/Handler;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/hud/app/util/CrashReporter$2;->this$0:Lcom/navdy/hud/app/util/CrashReporter;

    # getter for: Lcom/navdy/hud/app/util/CrashReporter;->periodicRunnable:Ljava/lang/Runnable;
    invoke-static {v4}, Lcom/navdy/hud/app/util/CrashReporter;->access$500(Lcom/navdy/hud/app/util/CrashReporter;)Ljava/lang/Runnable;

    move-result-object v4

    # getter for: Lcom/navdy/hud/app/util/CrashReporter;->PERIOD_DELIVERY_CHECK_INTERVAL:I
    invoke-static {}, Lcom/navdy/hud/app/util/CrashReporter;->access$600()I

    move-result v5

    int-to-long v6, v5

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_1
    throw v2
.end method
