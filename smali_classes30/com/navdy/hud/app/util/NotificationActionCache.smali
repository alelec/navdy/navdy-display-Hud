.class public Lcom/navdy/hud/app/util/NotificationActionCache;
.super Ljava/lang/Object;
.source "NotificationActionCache.java"


# instance fields
.field private handler:Ljava/lang/String;

.field private lastId:I


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 1
    .param p1, "handler"    # Ljava/lang/Class;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/4 v0, 0x1

    iput v0, p0, Lcom/navdy/hud/app/util/NotificationActionCache;->lastId:I

    .line 21
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/hud/app/util/NotificationActionCache;->handler:Ljava/lang/String;

    .line 22
    return-void
.end method


# virtual methods
.method public buildAction(III)Lcom/navdy/service/library/events/notification/NotificationAction;
    .locals 1
    .param p1, "notificationId"    # I
    .param p2, "actionId"    # I
    .param p3, "labelId"    # I

    .prologue
    .line 35
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/navdy/hud/app/util/NotificationActionCache;->buildAction(IIILjava/lang/String;)Lcom/navdy/service/library/events/notification/NotificationAction;

    move-result-object v0

    return-object v0
.end method

.method public buildAction(IIII)Lcom/navdy/service/library/events/notification/NotificationAction;
    .locals 9
    .param p1, "notificationId"    # I
    .param p2, "actionId"    # I
    .param p3, "iconId"    # I
    .param p4, "focusedIconId"    # I

    .prologue
    const/4 v5, 0x0

    .line 46
    new-instance v0, Lcom/navdy/service/library/events/notification/NotificationAction;

    iget-object v1, p0, Lcom/navdy/hud/app/util/NotificationActionCache;->handler:Ljava/lang/String;

    iget v2, p0, Lcom/navdy/hud/app/util/NotificationActionCache;->lastId:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/navdy/hud/app/util/NotificationActionCache;->lastId:I

    .line 47
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    move-object v6, v5

    invoke-direct/range {v0 .. v8}, Lcom/navdy/service/library/events/notification/NotificationAction;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 48
    .local v0, "action":Lcom/navdy/service/library/events/notification/NotificationAction;
    return-object v0
.end method

.method public buildAction(IIILjava/lang/String;)Lcom/navdy/service/library/events/notification/NotificationAction;
    .locals 9
    .param p1, "notificationId"    # I
    .param p2, "actionId"    # I
    .param p3, "labelId"    # I
    .param p4, "label"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 39
    new-instance v0, Lcom/navdy/service/library/events/notification/NotificationAction;

    iget-object v1, p0, Lcom/navdy/hud/app/util/NotificationActionCache;->handler:Ljava/lang/String;

    iget v2, p0, Lcom/navdy/hud/app/util/NotificationActionCache;->lastId:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/navdy/hud/app/util/NotificationActionCache;->lastId:I

    .line 40
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object v6, p4

    move-object v8, v7

    invoke-direct/range {v0 .. v8}, Lcom/navdy/service/library/events/notification/NotificationAction;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 41
    .local v0, "action":Lcom/navdy/service/library/events/notification/NotificationAction;
    return-object v0
.end method

.method public markComplete(Lcom/navdy/service/library/events/notification/NotificationAction;)V
    .locals 0
    .param p1, "action"    # Lcom/navdy/service/library/events/notification/NotificationAction;

    .prologue
    .line 32
    return-void
.end method

.method public validAction(Lcom/navdy/service/library/events/notification/NotificationAction;)Z
    .locals 2
    .param p1, "action"    # Lcom/navdy/service/library/events/notification/NotificationAction;

    .prologue
    .line 26
    iget-object v0, p1, Lcom/navdy/service/library/events/notification/NotificationAction;->handler:Ljava/lang/String;

    iget-object v1, p0, Lcom/navdy/hud/app/util/NotificationActionCache;->handler:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
