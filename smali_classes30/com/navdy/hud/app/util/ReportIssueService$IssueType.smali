.class public final enum Lcom/navdy/hud/app/util/ReportIssueService$IssueType;
.super Ljava/lang/Enum;
.source "ReportIssueService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/hud/app/util/ReportIssueService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "IssueType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/hud/app/util/ReportIssueService$IssueType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

.field public static final enum INEFFICIENT_ROUTE_ETA_TRAFFIC:Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

.field public static final enum INEFFICIENT_ROUTE_SELECTED:Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

.field public static final enum OTHER:Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

.field public static final enum ROAD_CLOSED:Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

.field public static final enum ROAD_CLOSED_PERMANENT:Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

.field public static final enum ROAD_NAME:Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

.field public static final enum WRONG_DIRECTION:Lcom/navdy/hud/app/util/ReportIssueService$IssueType;


# instance fields
.field private issueTypeCode:I

.field private messageStringResource:I

.field private titleStringResource:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 131
    new-instance v0, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    const-string v1, "INEFFICIENT_ROUTE_ETA_TRAFFIC"

    const v3, 0x7f0900f4

    const/16 v5, 0x6b

    move v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->INEFFICIENT_ROUTE_ETA_TRAFFIC:Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    .line 132
    new-instance v3, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    const-string v4, "INEFFICIENT_ROUTE_SELECTED"

    const v6, 0x7f090244

    const/16 v8, 0x6a

    move v5, v9

    move v7, v2

    invoke-direct/range {v3 .. v8}, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->INEFFICIENT_ROUTE_SELECTED:Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    .line 133
    new-instance v3, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    const-string v4, "ROAD_CLOSED"

    const v6, 0x7f090230

    const v7, 0x7f090231

    const/16 v8, 0x67

    move v5, v10

    invoke-direct/range {v3 .. v8}, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->ROAD_CLOSED:Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    .line 134
    new-instance v3, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    const-string v4, "WRONG_DIRECTION"

    const v6, 0x7f09031e

    const/16 v8, 0x66

    move v5, v11

    move v7, v2

    invoke-direct/range {v3 .. v8}, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->WRONG_DIRECTION:Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    .line 135
    new-instance v3, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    const-string v4, "ROAD_NAME"

    const v6, 0x7f090232

    const/16 v8, 0x68

    move v5, v12

    move v7, v2

    invoke-direct/range {v3 .. v8}, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->ROAD_NAME:Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    .line 136
    new-instance v3, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    const-string v4, "ROAD_CLOSED_PERMANENT"

    const/4 v5, 0x5

    const v6, 0x7f0901f6

    const/16 v8, 0x6c

    move v7, v2

    invoke-direct/range {v3 .. v8}, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->ROAD_CLOSED_PERMANENT:Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    .line 137
    new-instance v3, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    const-string v4, "OTHER"

    const/4 v5, 0x6

    const v6, 0x7f0901f3

    const/16 v8, 0x69

    move v7, v2

    invoke-direct/range {v3 .. v8}, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->OTHER:Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    .line 130
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    sget-object v1, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->INEFFICIENT_ROUTE_ETA_TRAFFIC:Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->INEFFICIENT_ROUTE_SELECTED:Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    aput-object v1, v0, v9

    sget-object v1, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->ROAD_CLOSED:Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    aput-object v1, v0, v10

    sget-object v1, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->WRONG_DIRECTION:Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    aput-object v1, v0, v11

    sget-object v1, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->ROAD_NAME:Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    aput-object v1, v0, v12

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->ROAD_CLOSED_PERMANENT:Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->OTHER:Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->$VALUES:[Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIII)V
    .locals 0
    .param p3, "title"    # I
    .param p4, "message"    # I
    .param p5, "id"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)V"
        }
    .end annotation

    .prologue
    .line 143
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 144
    iput p3, p0, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->titleStringResource:I

    .line 145
    iput p4, p0, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->messageStringResource:I

    .line 146
    iput p5, p0, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->issueTypeCode:I

    .line 147
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/hud/app/util/ReportIssueService$IssueType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 130
    const-class v0, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    return-object v0
.end method

.method public static values()[Lcom/navdy/hud/app/util/ReportIssueService$IssueType;
    .locals 1

    .prologue
    .line 130
    sget-object v0, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->$VALUES:[Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    invoke-virtual {v0}, [Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/hud/app/util/ReportIssueService$IssueType;

    return-object v0
.end method


# virtual methods
.method public getIssueTypeCode()I
    .locals 1

    .prologue
    .line 158
    iget v0, p0, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->issueTypeCode:I

    return v0
.end method

.method public getMessageStringResource()I
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->messageStringResource:I

    return v0
.end method

.method public getTitleStringResource()I
    .locals 1

    .prologue
    .line 150
    iget v0, p0, Lcom/navdy/hud/app/util/ReportIssueService$IssueType;->titleStringResource:I

    return v0
.end method
