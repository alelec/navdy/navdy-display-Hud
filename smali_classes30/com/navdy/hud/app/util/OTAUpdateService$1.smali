.class Lcom/navdy/hud/app/util/OTAUpdateService$1;
.super Ljava/lang/Object;
.source "OTAUpdateService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/hud/app/util/OTAUpdateService;->update(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/hud/app/util/OTAUpdateService;

.field final synthetic val$postUpdateCommand:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/hud/app/util/OTAUpdateService;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/hud/app/util/OTAUpdateService;

    .prologue
    .line 185
    iput-object p1, p0, Lcom/navdy/hud/app/util/OTAUpdateService$1;->this$0:Lcom/navdy/hud/app/util/OTAUpdateService;

    iput-object p2, p0, Lcom/navdy/hud/app/util/OTAUpdateService$1;->val$postUpdateCommand:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 189
    :try_start_0
    # getter for: Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/OTAUpdateService;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "Applying the update"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 190
    iget-object v4, p0, Lcom/navdy/hud/app/util/OTAUpdateService$1;->this$0:Lcom/navdy/hud/app/util/OTAUpdateService;

    iget-object v5, p0, Lcom/navdy/hud/app/util/OTAUpdateService$1;->this$0:Lcom/navdy/hud/app/util/OTAUpdateService;

    iget-object v5, v5, Lcom/navdy/hud/app/util/OTAUpdateService;->sharedPreferences:Landroid/content/SharedPreferences;

    # invokes: Lcom/navdy/hud/app/util/OTAUpdateService;->checkUpdateFile(Landroid/content/Context;Landroid/content/SharedPreferences;)Ljava/io/File;
    invoke-static {v4, v5}, Lcom/navdy/hud/app/util/OTAUpdateService;->access$100(Landroid/content/Context;Landroid/content/SharedPreferences;)Ljava/io/File;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 191
    .local v1, "file":Ljava/io/File;
    if-eqz v1, :cond_0

    .line 193
    :try_start_1
    iget-object v4, p0, Lcom/navdy/hud/app/util/OTAUpdateService$1;->this$0:Lcom/navdy/hud/app/util/OTAUpdateService;

    iget-object v4, v4, Lcom/navdy/hud/app/util/OTAUpdateService;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v5, "retry_count"

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 194
    .local v2, "retryCount":I
    iget-object v4, p0, Lcom/navdy/hud/app/util/OTAUpdateService$1;->this$0:Lcom/navdy/hud/app/util/OTAUpdateService;

    iget-object v4, v4, Lcom/navdy/hud/app/util/OTAUpdateService;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "retry_count"

    add-int/lit8 v6, v2, 0x1

    .line 195
    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "update_from_version"

    sget-object v6, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    .line 196
    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 197
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 198
    # getter for: Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/OTAUpdateService;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Trying to the install the update, retry count :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int/lit8 v6, v2, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 200
    :try_start_2
    iget-object v4, p0, Lcom/navdy/hud/app/util/OTAUpdateService$1;->this$0:Lcom/navdy/hud/app/util/OTAUpdateService;

    iget-object v4, v4, Lcom/navdy/hud/app/util/OTAUpdateService;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-static {v4}, Lcom/navdy/hud/app/analytics/AnalyticsSupport;->recordInstallOTAUpdate(Landroid/content/SharedPreferences;)V

    .line 201
    invoke-static {}, Lcom/navdy/hud/app/util/CrashReporter;->getInstance()Lcom/navdy/hud/app/util/CrashReporter;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/util/CrashReporter;->stopCrashReporting(Z)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 205
    :goto_0
    :try_start_3
    # getter for: Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/OTAUpdateService;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "NAVDY_NOT_A_CRASH"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 206
    invoke-static {}, Lcom/navdy/hud/app/obd/ObdDeviceConfigurationManager;->turnOffOBDSleep()V

    .line 207
    invoke-static {}, Lcom/navdy/hud/app/device/dial/DialManager;->getInstance()Lcom/navdy/hud/app/device/dial/DialManager;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/navdy/hud/app/device/dial/DialManager;->requestDialReboot(Z)V

    .line 208
    const-wide/16 v4, 0x7d0

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V

    .line 209
    iget-object v4, p0, Lcom/navdy/hud/app/util/OTAUpdateService$1;->this$0:Lcom/navdy/hud/app/util/OTAUpdateService;

    iget-object v5, p0, Lcom/navdy/hud/app/util/OTAUpdateService$1;->this$0:Lcom/navdy/hud/app/util/OTAUpdateService;

    iget-object v6, p0, Lcom/navdy/hud/app/util/OTAUpdateService$1;->val$postUpdateCommand:Ljava/lang/String;

    # invokes: Lcom/navdy/hud/app/util/OTAUpdateService;->installPackage(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;)V
    invoke-static {v4, v5, v1, v6}, Lcom/navdy/hud/app/util/OTAUpdateService;->access$200(Lcom/navdy/hud/app/util/OTAUpdateService;Landroid/content/Context;Ljava/io/File;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 221
    .end local v2    # "retryCount":I
    :goto_1
    iget-object v4, p0, Lcom/navdy/hud/app/util/OTAUpdateService$1;->this$0:Lcom/navdy/hud/app/util/OTAUpdateService;

    # setter for: Lcom/navdy/hud/app/util/OTAUpdateService;->installing:Z
    invoke-static {v4, v7}, Lcom/navdy/hud/app/util/OTAUpdateService;->access$302(Lcom/navdy/hud/app/util/OTAUpdateService;Z)Z

    .line 223
    .end local v1    # "file":Ljava/io/File;
    :goto_2
    return-void

    .line 202
    .restart local v1    # "file":Ljava/io/File;
    .restart local v2    # "retryCount":I
    :catch_0
    move-exception v3

    .line 203
    .local v3, "t":Ljava/lang/Throwable;
    :try_start_4
    # getter for: Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/OTAUpdateService;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "Exception while reporting the update"

    invoke-virtual {v4, v5, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 210
    .end local v2    # "retryCount":I
    .end local v3    # "t":Ljava/lang/Throwable;
    :catch_1
    move-exception v0

    .line 211
    .local v0, "e":Ljava/lang/Throwable;
    :try_start_5
    # getter for: Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/OTAUpdateService;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "Exception while running update OTA package"

    invoke-virtual {v4, v5, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 212
    invoke-static {}, Lcom/navdy/hud/app/util/CrashReporter;->getInstance()Lcom/navdy/hud/app/util/CrashReporter;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/navdy/hud/app/util/CrashReporter;->reportNonFatalException(Ljava/lang/Throwable;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 221
    iget-object v4, p0, Lcom/navdy/hud/app/util/OTAUpdateService$1;->this$0:Lcom/navdy/hud/app/util/OTAUpdateService;

    # setter for: Lcom/navdy/hud/app/util/OTAUpdateService;->installing:Z
    invoke-static {v4, v7}, Lcom/navdy/hud/app/util/OTAUpdateService;->access$302(Lcom/navdy/hud/app/util/OTAUpdateService;Z)Z

    goto :goto_2

    .line 216
    .end local v0    # "e":Ljava/lang/Throwable;
    :cond_0
    :try_start_6
    iget-object v4, p0, Lcom/navdy/hud/app/util/OTAUpdateService$1;->this$0:Lcom/navdy/hud/app/util/OTAUpdateService;

    iget-object v5, p0, Lcom/navdy/hud/app/util/OTAUpdateService$1;->this$0:Lcom/navdy/hud/app/util/OTAUpdateService;

    iget-object v5, v5, Lcom/navdy/hud/app/util/OTAUpdateService;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-static {v1, v4, v5}, Lcom/navdy/hud/app/util/OTAUpdateService;->clearUpdate(Ljava/io/File;Landroid/content/Context;Landroid/content/SharedPreferences;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_1

    .line 218
    .end local v1    # "file":Ljava/io/File;
    :catch_2
    move-exception v0

    .line 219
    .local v0, "e":Ljava/lang/Exception;
    :try_start_7
    # getter for: Lcom/navdy/hud/app/util/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;
    invoke-static {}, Lcom/navdy/hud/app/util/OTAUpdateService;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "Failed to install OTA package"

    invoke-virtual {v4, v5, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 221
    iget-object v4, p0, Lcom/navdy/hud/app/util/OTAUpdateService$1;->this$0:Lcom/navdy/hud/app/util/OTAUpdateService;

    # setter for: Lcom/navdy/hud/app/util/OTAUpdateService;->installing:Z
    invoke-static {v4, v7}, Lcom/navdy/hud/app/util/OTAUpdateService;->access$302(Lcom/navdy/hud/app/util/OTAUpdateService;Z)Z

    goto :goto_2

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    iget-object v5, p0, Lcom/navdy/hud/app/util/OTAUpdateService$1;->this$0:Lcom/navdy/hud/app/util/OTAUpdateService;

    # setter for: Lcom/navdy/hud/app/util/OTAUpdateService;->installing:Z
    invoke-static {v5, v7}, Lcom/navdy/hud/app/util/OTAUpdateService;->access$302(Lcom/navdy/hud/app/util/OTAUpdateService;Z)Z

    throw v4
.end method
